/**********************************************************************************************
Author      : Himanshu Tariyal (A0994)
Date        : 25th September'2020
Description : Test class for BatchPopulateVeevaHierarchy
Revision(s) : v1.0
**********************************************************************************************/
@isTest
private class BatchPopulateVeevaHierarchyTest 
{
    static testMethod void testMethod1() 
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        AxtriaSalesIQTM__Organization_Master__c orgmas1 = TestDataFactory.createOrganizationMaster();
        orgmas1.Name = 'SnT';
        insert orgmas1;

        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        orgmas.AxtriaSalesIQTM__Parent_Organization_Name__c = orgmas1.id;
        orgmas.Name = 'abcd';
        orgmas.AxtriaSalesIQTM__Org_Level__c = 'Global';
        orgmas.AxtriaSalesIQTM__Parent_Country_Level__c = true;
        insert orgmas;

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Load_Type__c = 'Full Load';
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;
        Account acc= TestDataFactory.createAccount();
        acc.AccountNumber ='BH10643156';
        insert acc;
        
        AxtriaSalesIQTM__Affiliation_Network__c affnet = TestDataFactory.createAffliNet(countr);
        insert affnet;

        AxtriaSalesIQTM__Account_Affiliation__c accaff = TestDataFactory.createAcctAffli(acc,affnet);
        insert accaff;
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        pos.AxtriaSalesIQTM__Effective_End_Date__c = date.today().addDays(10);
        pos.AxtriaSalesIQTM__IsMaster__c = true;
        pos.AxtriaSalesIQTM__Parent_Position__c = null;
        pos.AxtriaSalesIQTM__Client_Position_Code__c ='N003';
        insert pos;

        AxtriaSalesIQTM__Position__c pos1 = TestDataFactory.createPosition(team,teamins);
        pos1.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        pos1.AxtriaSalesIQTM__Effective_End_Date__c = date.today().addDays(10);
        pos1.AxtriaSalesIQTM__IsMaster__c=true;
        pos1.AxtriaSalesIQTM__Parent_Position__c = pos.Id;
        pos1.AxtriaSalesIQTM__Client_Position_Code__c ='T090';
        insert pos1;

        Veeva_Position_Hierarchy__c v = new Veeva_Position_Hierarchy__c();
        v.Status__c='Added';
        v.Position_Code__c='T090';
        v.Parent_Pos_Code__c='N003';
        insert v;

        Veeva_Position_Hierarchy__c v2 = new Veeva_Position_Hierarchy__c();
        v2.Status__c='Added';
        v2.Position_Code__c='N003';
        insert v2;

        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;

        AxtriaSalesIQTM__ETL_Config__c etl = new AxtriaSalesIQTM__ETL_Config__c();
        etl.Name = 'Veeva Full Load';
        insert etl;

        insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
        new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'UpdatePositionCode')};

        System.Test.startTest();
        System.runAs(loggedInUser)
        {
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            Database.executeBatch(new BatchPopulateVeevaHierarchy());
        }
        System.Test.stopTest();
    }

    static testMethod void testMethod2() 
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        AxtriaSalesIQTM__Organization_Master__c orgmas1 = TestDataFactory.createOrganizationMaster();
        orgmas1.Name = 'SnT';
        insert orgmas1;

        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        orgmas.AxtriaSalesIQTM__Parent_Organization_Name__c = orgmas1.id;
        orgmas.Name = 'abcd';
        orgmas.AxtriaSalesIQTM__Org_Level__c = 'Global';
        orgmas.AxtriaSalesIQTM__Parent_Country_Level__c = true;
        insert orgmas;

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Load_Type__c = 'Full Load';
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;

        Account acc= TestDataFactory.createAccount();
        acc.AccountNumber ='BH10643156';
        insert acc;
        
        AxtriaSalesIQTM__Affiliation_Network__c affnet = TestDataFactory.createAffliNet(countr);
        insert affnet;
        AxtriaSalesIQTM__Account_Affiliation__c accaff = TestDataFactory.createAcctAffli(acc,affnet);
        insert accaff;
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        pos.AxtriaSalesIQTM__Effective_End_Date__c = date.today().addDays(10);
        pos.AxtriaSalesIQTM__IsMaster__c = true;
        pos.AxtriaSalesIQTM__Parent_Position__c = null;
        pos.AxtriaSalesIQTM__Client_Position_Code__c ='N003';
        insert pos;

        AxtriaSalesIQTM__Position__c pos1 = TestDataFactory.createPosition(team,teamins);
        pos1.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        pos1.AxtriaSalesIQTM__Effective_End_Date__c = date.today().addDays(10);
        pos1.AxtriaSalesIQTM__IsMaster__c=true;
        pos1.AxtriaSalesIQTM__Parent_Position__c = pos.Id;
        pos1.AxtriaSalesIQTM__Client_Position_Code__c ='T090';
        insert pos1;

        Veeva_Position_Hierarchy__c v = new Veeva_Position_Hierarchy__c();
        v.Status__c='Added';
        v.Position_Code__c='T090';
        v.Parent_Pos_Code__c='N003';
        insert v;

        Veeva_Position_Hierarchy__c v2 = new Veeva_Position_Hierarchy__c();
        v2.Status__c='Added';
        v2.Position_Code__c='N003';
        insert v2;

        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;

        AxtriaSalesIQTM__ETL_Config__c etl = new AxtriaSalesIQTM__ETL_Config__c();
        etl.Name = 'Veeva Full Load';
        insert etl;

        insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
        new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'UpdatePositionCode')};

        System.Test.startTest();

        System.runAs(loggedInUser)
        {
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            Database.executeBatch(new BatchPopulateVeevaHierarchy(false));
        }
        System.Test.stopTest();
    }
}