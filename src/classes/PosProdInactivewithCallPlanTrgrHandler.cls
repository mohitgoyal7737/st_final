public with sharing class PosProdInactivewithCallPlanTrgrHandler {

     public static void InactivePosProd(List<AxtriaSalesIQTM__Position_Product__c> PosProdList,Map<id,AxtriaSalesIQTM__Position_Product__c> oldMap){
         Map<id,string> mapPosProd = new Map<id,string>();
         for(AxtriaSalesIQTM__Position_Product__c PosProd : [Select id,Name,AxtriaSalesIQTM__Position__r.Name from AxtriaSalesIQTM__Position_Product__c where id in: PosProdList]){

             mapPosProd.put(PosProd.id,PosProd.AxtriaSalesIQTM__Position__r.Name);

         }
        Set<String> positionCallplan = new Set<String>();
        Set<String> brandCallplan  = new Set<String>();
        Set<String> teaminstanceCallplan  = new Set<String>();
        Set<String> combinationPosProd = new Set<String>();
         Map<string,id> abc = new Map<string,id>();
        List<AxtriaSalesIQTM__Position_Product__c> lstcalinst= new List<AxtriaSalesIQTM__Position_Product__c>();

        Set<id> PPid = new Set<id>();
        map<String,String> productId2Name = new map<String,String>();
        
        for(AxtriaSalesIQTM__Position_Product__c pacpRec : posProdlist)
        {
           brandCallplan.add(pacpRec.Product_Catalog__c); 
        }

        List<Product_Catalog__c> proCatalogList = [Select Id, Name from Product_Catalog__c where id in :brandCallplan];
        for(Product_Catalog__c proCatalogRec : proCatalogList)
        {
            productId2Name.put(proCatalogRec.Id, proCatalogRec.NAme);
        }
        
         for(AxtriaSalesIQTM__Position_Product__c pacpRec : posProdlist){
            if(oldMap.containsKey(pacpRec.id) && oldMap.get(pacpRec.id).AxtriaSalesIQTM__isActive__c && !pacpRec.AxtriaSalesIQTM__isActive__c){
                positionCallplan.add(pacpRec.AxtriaSalesIQTM__Position__c);
                brandCallplan.add(productId2Name.get(pacpRec.Product_Catalog__c));
                teaminstanceCallplan.add(pacpRec.AxtriaSalesIQTM__Team_Instance__c);
                //String keycombination = pacpRec.AxtriaSalesIQTM__Position__c +'_' + productId2Name.get(pacpRec.Product_Catalog__c) + '_' + pacpRec.AxtriaSalesIQTM__Team_Instance__c;
                //CombinationPosProd.add(keycombination);
                //abc.put(keycombination,pacpRec.id);            
            }  
         }
         
       List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> PACPList = [SELECT Id, Name, P1__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__lastApprovedTarget__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__lastApprovedTarget__c = true and AxtriaSalesIQTM__isincludedCallPlan__c = true and P1__c in :brandCallplan and AxtriaSalesIQTM__Position__c in :positionCallplan and AxtriaSalesIQTM__Team_Instance__c in :teaminstanceCallplan];
         set<string> combinationSet = new Set<string>();   
            system.debug(PACPList + 'abchjkh');
             if(PACPList.size() != null){
        for( AxtriaSalesIQTM__Position_Account_Call_Plan__c PACP : PACPList){
             String CallPlantkey = PACP.AxtriaSalesIQTM__Position__c +'_'+ PACP.P1__c +'_'+ PACP.AxtriaSalesIQTM__Team_Instance__c;
             combinationSet.add(CallPlantkey);
            /* if(CombinationPosProd.contains(CallPlantkey)){
                PPid.add(abc.get(CallPlantkey));
                AxtriaSalesIQTM__Position_Product__c  pp = new AxtriaSalesIQTM__Position_Product__c (id = abc.get(CallPlantkey));
                system.debug(pp+ 'abccccc');
                pp.adderror('njhbj');
           }   */   
         }
         
          
         for(AxtriaSalesIQTM__Position_Product__c pacpRec : posProdlist){
       
             String keycombination = pacpRec.AxtriaSalesIQTM__Position__c +'_' + productId2Name.get(pacpRec.Product_Catalog__c) + '_' + pacpRec.AxtriaSalesIQTM__Team_Instance__c;

               if(combinationSet.contains(keycombination)){
                pacpRec.adderror('Deactivate the Call Plan first then inactive Position Product for Position' + mapPosProd.get(pacpRec.id));
             } 
           }
         }
        }
      }