global class batchScenarioPast implements Database.Batchable<sObject>,Schedulable

{

    global DateTime Scenariodate;

     global batchScenarioPast(){

        Scenariodate = null;
    }

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String query = 'SELECT Id, AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Scenario_Stage__c FROM AxtriaSalesIQTM__Scenario__c where AxtriaSalesIQTM__Scenario_Stage__c =\'Live\' ';
        return Database.getQueryLocator(query);
    }
    
     
    global void execute(Database.BatchableContext BC, List<AxtriaSalesIQTM__Scenario__c> scope)
    {

        list<AxtriaSalesIQTM__Scenario__c> lst = new list<AxtriaSalesIQTM__Scenario__c>();
         for(AxtriaSalesIQTM__Scenario__c a : scope)
         {
              Scenariodate = a.AxtriaSalesIQTM__Effective_End_Date__c;

              AxtriaSalesIQTM__Scenario__c cv = new AxtriaSalesIQTM__Scenario__c();

              cv = a;

              if(Scenariodate!= null && Scenariodate< System.today()){

                cv.AxtriaSalesIQTM__Scenario_Stage__c = 'Past';
                lst.add(cv);
              }       
         }

         if(lst!=null && lst.size()>0)
         
            update lst;
    }  
    
    global  void execute(System.SchedulableContext SC){
         database.executeBatch(new batchScenarioPast(),2000);
    }
    global void finish(Database.BatchableContext BC)
    {
    }
}