public with sharing class GenerateSntReport {
    
    /*  method name:ruledrivencallplan
        purpose: This method will take the rule id as input and updates the Rule_Report__c record with type="Snt Rule"
        @author:   SIVA
        Date :   24-12-2018

    */

    public  static void Ruledrivencallplan(string ruleis){
        string ruleid= ruleis;
        system.debug('============Rule Driven id is::::'+ruleid);
        set<string>uniq= new set<string>();
        list<Rule_Report__c>reportlist= new list<Rule_Report__c>();
        list<Product_Catalog__c>products = new list<Product_Catalog__c>();
        set<string>productset = new set<string>();

        list<Measure_Master__c>rulelist = [select id,Brand_Lookup__r.name,Brand_Lookup__c,Team_Instance__c,Team_Instance__r.name,Country__c,Team_Instance__r.Country_Name__c from Measure_Master__c where id=:ruleid];
        system.debug('==============rulelist ::::'+rulelist);
        if(rulelist!=null && rulelist.size()>0){
            for(Measure_Master__c rule : rulelist)
            {
                productset.add(rule.Brand_Lookup__c);
                string unq = rule.Team_Instance__c+'_'+rule.Brand_Lookup__c;
                if(!uniq.contains(unq))
                {
                    Rule_Report__c rp = new Rule_Report__c();
                    rp.Team_Instance__c = rule.Team_Instance__r.name;
                    rp.Product_Name__c = rule.Brand_Lookup__r.name;
                    rp.Country_Name__c = rule.Team_Instance__r.Country_Name__c;
                    rp.Type__c = 'SnT Rule';
                    rp.Uniq__c = rule.Team_Instance__c+'_'+rule.Brand_Lookup__c;
                    reportlist.add(rp);
                    uniq.add(unq);
                }
            }
            system.debug('===========reportlist:'+reportlist);
            try{
                Schema.SObjectField unq = Rule_Report__c.Fields.Uniq__c;
                database.upsert(reportlist,unq, false);
            }
            catch(Exception ex){

            }
            
        }
        //productcatlogue();
    }

    /*  
        Method name : Directloadcallplan
        purpose: This method will take the team instance name,set of product names  as input and updates the Rule_Report__c record with type="Snt Rule"
        @author:   SIVA
        Date :   24-12-2018

    */

    public static void Directloadcallplan(string teaminstance,set<string>productset){
        system.debug('============teaminstance::::'+teaminstance+':::productset:::'+productset);
        string teaminstancename = teaminstance;
        set<string>setproduct = new set<string>();
        setproduct.addall(productset);

        set<string>uniq= new set<string>();
        list<Rule_Report__c>reportlist= new list<Rule_Report__c>();
        list<Product_Catalog__c>productlist = new list<Product_Catalog__c>();
        //set<string>productset = new set<string>();

        productlist = [select id,name ,Team_Instance__c,Team_Instance__r.name ,Team_Instance__r.Country_Name__c from Product_Catalog__c where name In:setproduct and Team_Instance__r.name = :teaminstancename and AxtriaARSnT__IsActive__c=true];
        if(productlist !=null && productlist.size()>0)
        {
            for(Product_Catalog__c prod : productlist)
            {
                Rule_Report__c rp = new Rule_Report__c();
                rp.Team_Instance__c = prod.Team_Instance__r.name;
                rp.Product_Name__c = prod.name;
                rp.Country_Name__c = prod.Team_Instance__r.Country_Name__c;
                rp.Type__c = 'Direct Load';
                rp.Uniq__c = prod.Team_Instance__c+'_'+prod.id;
                reportlist.add(rp);
            }
            try{
                Schema.SObjectField unq = Rule_Report__c.Fields.Uniq__c;
                database.upsert(reportlist,unq, false);
            }
            catch(Exception ex){

            }
        }
    }
}