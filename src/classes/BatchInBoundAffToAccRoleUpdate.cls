global class BatchInBoundAffToAccRoleUpdate implements Database.Batchable<sObject>, Database.Stateful,schedulable{
    public Integer recordsProcessed=0;
    public String batchID;
    global DateTime lastjobDate=null;
    global String query;
    public String cycle {get;set;}
    
    


    global BatchInBoundAffToAccRoleUpdate(){


        List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
        schLogList=[Select Id,CreatedDate,Created_Date__c,Created_Date2__c from Scheduler_Log__c where Job_Name__c='Account Role from Affiliation' and Job_Status__c='Successful' Order By Created_Date2__c desc];
                                            
                             
        if(schLogList.size()>0){
            lastjobDate=schLogList[0].Created_Date2__c;  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }

        else{
            lastjobDate=null;       //else we set the lastjobDate to null
        }

        System.debug('last job'+lastjobDate);
        //Last Bacth run ID

        Scheduler_Log__c sJob = new Scheduler_Log__c();    
        sJob.Job_Name__c = 'Account Role from Affiliation';
        sJob.Job_Status__c = 'Failed';
        sJob.Job_Type__c='Inbound';
        sJob.Created_Date2__c = DateTime.now();
        if(cycle!=null && cycle!='')
        sJob.Cycle__c=cycle;
        insert sJob;
        batchID = sJob.Id;
        recordsProcessed =0;
        query = 'SELECT Account_Number__c,Role_Name__c FROM AxtriaSalesIQTM__Account_Affiliation__c where AxtriaSalesIQTM__Active__c = true AND  Role_Name__c NOT IN (\'NA\',Null) ';

        if(lastjobDate!=null){
          query = query + 'AND LastModifiedDate  >=:  lastjobDate '; 
        }
        
        
        System.debug('query'+ query);
                //Create a new record for Scheduler Batch with values, Job_Type, Job_Status__c as Failed, Created_Date__c as Today’s Date.

    }

    global Database.QueryLocator  start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }


    public void execute(System.SchedulableContext SC){
         database.executeBatch(new BatchInBoundAffToAccRoleUpdate(),2000);
    }




    global void execute(Database.BatchableContext bc, List<AxtriaSalesIQTM__Account_Affiliation__c> records){
         List<Account> Acc = new List<Account>();
        Map<String,Set<String>> MapAccNumber =new Map<String,Set<String>>();
        System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>records>>>>>>>>>>>>>'+records.size());
        
        
        for(AxtriaSalesIQTM__Account_Affiliation__c accAff : records)
        {
            if(MapAccNumber.containsKey(accAff.Account_Number__c))
            {
                MapAccNumber.get(accAff.Account_Number__c).add(accAff.Role_Name__c);
            }

            else
            {
                Set<String> temp=new Set<String>();
                temp.add(accAff.Role_Name__c);
                MapAccNumber.put(accAff.Account_Number__c,temp);
            }
        }
        System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>MapAccNumber>>>>'+MapAccNumber.size());


       for(Account AccRole : [Select id,AccountNumber,Role_Name__c from Account where AccountNumber IN : MapAccNumber.keySet()])
       {
          Set<String> RoleSet= new Set<String>();
          RoleSet=MapAccNumber.get(AccRole.AccountNumber);
           
          System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>RoleSet>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'+RoleSet);
          if(AccRole.Role_Name__c!=null){
                    String tempRole =AccRole.Role_Name__c+';'+String.join(new List<String>(RoleSet),';');
                    List<String> tempRoleList=new List<String>();
                    tempRoleList=tempRole.split(';');
                    Set<String> tempRoleSet = new Set<String>();
                    tempRoleSet.addAll(tempRoleList);
                    AccRole.Role_Name__c =String.join(new List<String>(tempRoleSet),';');
           }
          else {
                    String tempRole =String.join(new List<String>(RoleSet),';');
                    List<String> tempRoleList=new List<String>();
                    tempRoleList=tempRole.split(';');
                    Set<String> tempRoleSet = new Set<String>();
                    tempRoleSet.addAll(tempRoleList);
                    AccRole.Role_Name__c =String.join(new List<String>(tempRoleSet),';');
           }
           
          System.debug('>>>>>AccRole>>>>>'+AccRole);
          acc.add(AccRole);
          system.debug('recordsProcessed+'+recordsProcessed);
          recordsProcessed++;
          

       }
       Set<Account> AccSet=new Set<Account>();
       AccSet.addAll(acc);
       System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>acc>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'+Acc.size()+'>>AccSet Size>>>'+AccSet.size());
       acc.clear();
       acc.addAll(AccSet);
       System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>acc>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'+Acc.size()+'>>AccSet Size>>>'+AccSet.size());
        if(Acc!=null){
        
            update Acc;
            acc.clear();
        }

    }    

    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
                System.debug(recordsProcessed + ' records processed. ');
                Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID); 
                system.debug('schedulerObj++++before'+sJob);
                //Update the scheduler log with successful
                 sjob.Object_Name__c = 'Account';                                                
                sJob.No_Of_Records_Processed__c=recordsProcessed;
                sJob.Job_Status__c='Successful';
                system.debug('sJob++++++++'+sJob);
                update sJob;

    } 
    
}