global class BatchOutBoundGeography2 implements Database.Batchable<sObject>,Database.stateful,schedulable {
    public String query;
    public Integer recordsProcessed=0;
    public String batchID;
    global DateTime lastjobDate=null;
    global Date today=Date.today();
    public map<String,String>Countrymap {get;set;}
    public map<String,String>mapVeeva2Mktcode {get;set;}
    public set<String> Uniqueset {get;set;}    
    public set<String> mkt {get;set;} 
    public list<Custom_Scheduler__c> mktList {get;set;}  
    public list<Custom_Scheduler__c> lsCustomSchedulerUpdate {get;set;}
    public String cycle {get;set;}
    public set<string>livecyclelist {get;set;}
    public list<AxtriaSalesIQTM__Team_Instance__c>Teaminstlist {get;set;}
                            

    global BatchOutBoundGeography2() {}

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
    public void execute(System.SchedulableContext SC){
       database.executeBatch(new BatchOutBoundGeography2());
    }

 global void execute(Database.BatchableContext BC, list<SIQ_Geography_Interface__c> scope) {}

    global void finish(Database.BatchableContext BC) {}
}