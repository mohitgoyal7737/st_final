global class BatchOutBoundPositionProduct2 implements Database.Batchable<sObject>,Database.Stateful,Schedulable {
    public String query;
    public list<AxtriaSalesIQTM__Position_Product__c>posrodlist ;
    public map<String,String>Countrymap {get;set;}
    public map<string,string>mcmap {get;set;}
    public List<String> posCodeList=new List<String>();
    public set<String> mkt {get;set;} 
    public list<Custom_Scheduler__c> mktList {get;set;}                                       
    public List<AxtriaSalesIQTM__Position__c> nationPosition {get;set;}  
    public list<Custom_Scheduler__c> lsCustomSchedulerUpdate {get;set;} 
    public String cycle {get;set;}
    global Date today=Date.today();
    global DateTime lastjobDate=null;
    public String batchID;
    public Integer recordsProcessed=0;
    global boolean flag=true;
    global string Country_1;
    public list<String> CountryList;
    public  List<String> DeltaCountry ;

    
    global BatchOutBoundPositionProduct2(String Country1){

        /*Country_1=Country1;
        CountryList=new list<String>();
        if(Country_1.contains(','))
        {
            CountryList=Country_1.split(',');
        }
        else
        {
            CountryList.add(Country_1);
        }
        System.debug('<<<<<<<<<--Country List-->>>>>>>>>>>>'+CountryList);
        flag=false;
        posCodeList.add('0');
        posCodeList.add('00000');
        posCodeList.add('Unassigned');
        posCodeList.add('Unassigned Territory');

        mkt = new set<String>();
        mktList=new list<Custom_Scheduler__c>();
        mktList=Custom_Scheduler__c.getall().values();
        lsCustomSchedulerUpdate = new list<Custom_Scheduler__c>();
        Countrymap = new map<string,string>();
        mcmap = new map<string,string>();
        
        

        query='select id,CreatedDate,LastModifiedDate,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,Product_Catalog__c,Product_Catalog__r.Name,Product_Catalog__r.Veeva_External_ID__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.name,AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Country_Name__c,AxtriaSalesIQTM__isActive__c from AxtriaSalesIQTM__Position_Product__c'+
        ' where AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=\'current\' and (AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c=\'Live\' or AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c=\'Published\') and  AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c not in: posCodeList and AxtriaSalesIQTM__Team_Instance__r.Country_Name__c IN :CountryList and Product_Catalog__c!=null and AxtriaSalesIQTM__Position__c != null and Product_Catalog__r.Veeva_External_ID__c != null ';
        
         System.debug('====================query'+ query);

                
        list<AxtriaSalesIQTM__Country__c>CountryList = [select id,Name,AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c];
        list<SIQ_MC_Country_Mapping__c>SIQCountry = [SELECT SIQ_BOT_Market_Code__c,SIQ_Country_Code__c,SIQ_MC_Code__c,SIQ_Veeva_Country_Code__c FROM SIQ_MC_Country_Mapping__c];

        //Fill the maps
        for(AxtriaSalesIQTM__Country__c country : CountryList){
            if(!Countrymap.containskey(country.name)){
                Countrymap.put(Country.Name,country.AxtriaSalesIQTM__Country_Code__c);
            }
        }

        if(SIQCountry !=null && SIQCountry.size() >0){
            for(SIQ_MC_Country_Mapping__c mc : SIQCountry){
                mcmap.put(mc.SIQ_Veeva_Country_Code__c ,mc.SIQ_MC_Code__c);
            }
        }*/
       
        
        
    }



    global BatchOutBoundPositionProduct2() {
        //query='';
        /*posCodeList.add('0');
        posCodeList.add('00000');
        posCodeList.add('Unassigned');
        posCodeList.add('Unassigned Territory');

        mkt = new set<String>();
        mktList=new list<Custom_Scheduler__c>();
        mktList=Custom_Scheduler__c.getall().values();
        lsCustomSchedulerUpdate = new list<Custom_Scheduler__c>();
        Countrymap = new map<string,string>();
        mcmap = new map<string,string>();
        
        for(Custom_Scheduler__c cs:mktList){
          if(cs.Status__c==true && cs.Schedule_date__c!=null){
            if(cs.Schedule_date__c>today.addDays(1)){
               mkt.add(cs.Marketing_Code__c);
            }else{
               //update Custom scheduler record
               cs.Status__c = False;
               lsCustomSchedulerUpdate.add(cs);
            }
          }
        }
        List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
        List<AxtriaSalesIQTM__Team_Instance__c> cycleList = new List<AxtriaSalesIQTM__Team_Instance__c>();
        cycleList=[Select Name,Cycle__r.Name from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current' and (AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c='Live' or AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c='Published')];
        if(cycleList!=null){
            for(AxtriaSalesIQTM__Team_Instance__c t1: cycleList)
            {
                if(t1.Cycle__r.Name !=null && t1.Cycle__r.Name !='')
                    cycle = t1.Cycle__r.Name;
            }
            
        }
        schLogList=[Select Id,CreatedDate,Created_Date2__c from Scheduler_Log__c where Job_Name__c='OutBound Position Product Delta' and Job_Status__c='Successful' Order By Created_Date2__c desc];
        if(schLogList.size()>0){
            lastjobDate=schLogList[0].Created_Date2__c;  
        }
        else{
            lastjobDate=null;
        }
        System.debug('last job'+lastjobDate);

        DeltaCountry = new List<String>();
        DeltaCountry = StaticTeaminstanceList.getSFEDeltaCountries();

        System.debug('>>>>>>>>>>>>>>>>>>>>>>DeltaCountry>>>>>>>>>>>>>>>>>>>' +DeltaCountry);
        

        query='select id,CreatedDate,LastModifiedDate,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,Product_Catalog__c,Product_Catalog__r.Name,Product_Catalog__r.Veeva_External_ID__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.name,AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Country_Name__c,AxtriaSalesIQTM__isActive__c from AxtriaSalesIQTM__Position_Product__c'+
        ' where AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=\'current\' and (AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c=\'Live\' or AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c=\'Published\') and  AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c not in: posCodeList and AxtriaSalesIQTM__Position__c != null and Product_Catalog__r.Veeva_External_ID__c != null and Product_Catalog__c != null and AxtriaSalesIQTM__Team_Instance__r.Country_Name__c IN: DeltaCountry ';
        
        if(lastjobDate!=null){
            query = query + ' and LastModifiedDate  >=:  lastjobDate '; 
        }
        if(mkt.size()>0){
            query=query + ' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c not in: mkt ' ;
        }
        this.query = query;
        
        list<AxtriaSalesIQTM__Country__c>CountryList = [select id,Name,AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c];
        list<SIQ_MC_Country_Mapping__c>SIQCountry = [SELECT SIQ_BOT_Market_Code__c,SIQ_Country_Code__c,SIQ_MC_Code__c,SIQ_Veeva_Country_Code__c FROM SIQ_MC_Country_Mapping__c];

        //Fill the maps
        for(AxtriaSalesIQTM__Country__c country : CountryList){
            if(!Countrymap.containskey(country.name)){
                Countrymap.put(Country.Name,country.AxtriaSalesIQTM__Country_Code__c);
            }
        }

        if(SIQCountry !=null && SIQCountry.size() >0){
            for(SIQ_MC_Country_Mapping__c mc : SIQCountry){
                mcmap.put(mc.SIQ_Veeva_Country_Code__c ,mc.SIQ_MC_Code__c);
            }
        }
        Scheduler_Log__c sJob = new Scheduler_Log__c();
        
        sJob.Job_Name__c = 'OutBound Position Product Delta';
        sJob.Job_Status__c = 'Failed';
        sJob.Job_Type__c='Outbound';
         if(cycle!=null && cycle!='')
         sJob.Cycle__c=cycle;
        sJob.Created_Date2__c = DateTime.now();
        insert sJob;
        batchID = sJob.Id;
        recordsProcessed =0;*/
        
        
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Product__c> scope) {
        /*list<SIQ_Position_Product_O__c>Posproduct = new list<SIQ_Position_Product_O__c>();
        set<string>uniqueset = new set<string>();
        for(AxtriaSalesIQTM__Position_Product__c pos : scope){
            string key = Countrymap.get(pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Country_Name__c)+'_'+ pos.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c+'_'+pos.Product_Catalog__r.Veeva_External_ID__c+'_'+pos.AxtriaSalesIQTM__Team_Instance__r.Name;
            if(!uniqueset.contains(key)){
                SIQ_Position_Product_O__c obj = new SIQ_Position_Product_O__c();
                obj.SIQ_Position_ID__c = pos.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
                obj.SIQ_Product_Id__c = pos.Product_Catalog__r.Veeva_External_ID__c;
                obj.SIQ_Product_Name__c = pos.Product_Catalog__r.Name;
                obj.SIQ_Salesforce_Name__c = pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.name;
                obj.SIQ_Team_Instance__c = pos.AxtriaSalesIQTM__Team_Instance__r.Name;
                obj.SIQ_Team_Name__c = pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.name;
                obj.SIQ_Created_Date__c = pos.CreatedDate;
                obj.SIQ_Updated_Date__c = pos.LastModifiedDate;
                obj.SIQ_Effective_Start_Date__c=pos.AxtriaSalesIQTM__Effective_Start_Date__c;
                if(pos.AxtriaSalesIQTM__isActive__c == true){
                    obj.SIQ_Effective_End_Date__c=pos.AxtriaSalesIQTM__Effective_End_Date__c; 
                }
                else
                {
                    obj.SIQ_Effective_End_Date__c=System.today().addDays(-1); 
                }
                
                obj.SIQ_Country_Code__c = Countrymap.get(pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Country_Name__c);
                if(mcmap!=null ){
                    String Countrycode = Countrymap.get(pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Country_Name__c);
                    obj.SIQ_Marketing_Code__c = mcmap.get(Countrycode) !=null ? mcmap.get(Countrycode) : Countrymap.get(pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Country_Name__c);
                }
                obj.Unique_Id__c =Countrymap.get(pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Country_Name__c)+'_'+ pos.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c+'_'+pos.Product_Catalog__r.Veeva_External_ID__c+'_'+pos.AxtriaSalesIQTM__Team_Instance__r.Name;
                Posproduct.add(obj);
                uniqueset.add(key);
                recordsProcessed++;
            }

        }
        if(Posproduct!=null && Posproduct.size()>0)
            Upsert Posproduct Unique_Id__c;*/
        
    }

    global void finish(Database.BatchableContext BC) {
        /*if(flag)
        {
            System.debug(recordsProcessed + ' records processed. ');
        Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID); 
        system.debug('schedulerObj++++before'+sJob);
        sJob.No_Of_Records_Processed__c=recordsProcessed;
        sJob.Job_Status__c='Successful';
        system.debug('sJob++++++++'+sJob);
        update sJob;
        
        Set<String> updMkt = new set<String>();
         for(Custom_Scheduler__c cs:mktList){
           if(cs.Status__c==true && cs.Schedule_date__c!=null){
             if(cs.Schedule_date__c==today.addDays(2)){
                updMkt.add(cs.Marketing_Code__c);
             }
            }
          }
         if(updMkt.size()>0){
            List<AxtriaSalesIQTM__Position_Product__c> posList=[Select Id FROM AxtriaSalesIQTM__Position_Product__c where AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c in: updMkt]; 
            update posList;
        }
        }*/

    }

    global void execute(System.SchedulableContext SC){
       //PositionProductOutBound ppo=new PositionProductOutBound ();
        Database.executeBatch(new BatchOutBoundPositionProduct2(), 2000);
    } 
}