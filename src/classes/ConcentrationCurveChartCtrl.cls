public class ConcentrationCurveChartCtrl {
    
    public List<graphWrapper> curvedata    {get;set;}
    public List<SelectOption> allTeamInstance                {get;set;}
    public List<AxtriaSalesIQTM__User_Access_Permission__c> loggedInUserData  {get;set;}
    public String selectedTeamInstance  {get;set;}
    public String countryID {get;set;}
    Map<string,string> successErrorMap;
     Public AxtriaSalesIQTM__Country__c Country {get;set;}
    
    public ConcentrationCurveChartCtrl(){
        
        curvedata = new List<graphWrapper>();
        loggedInUserData     = new List<AxtriaSalesIQTM__User_Access_Permission__c>();
        allTeamInstance      = new List<SelectOption>();
        
        //Fetch Country ID from cookie
        countryID = SalesIQUtility.getCookie(SalesIQUtility.getCountryCookieName());
        system.debug('##### countryID ' + countryID);
        successErrorMap = SalesIQUtility.checkCountryAccess(countryID);
        system.debug('############ successErrorMap ' + successErrorMap);
        if(successErrorMap.containsKey('Success')){
           countryID = successErrorMap.get('Success');               
           system.debug('########## countryID from Map ' + countryID);
           //As soon as we get Country ID set it in the cookie and it will applicable for whole application.
           SalesIQUtility.setCookieString('CountryID',countryID);
            Country = new AxtriaSalesIQTM__Country__c();
            Country = [select AxtriaSalesIQTM__Country_Flag__c,Name from AxtriaSalesIQTM__Country__c where id =:countryID limit 1];
        }
        
        loggedInUserData = SalesIQUtility.getUserAccessPermistion(Userinfo.getUserId(), countryID);
        system.debug('+++++++++ Logged in User '+loggedInUserData);
        
        for(AxtriaSalesIQTM__User_Access_Permission__c userInfo : loggedInUserData)
        {
            allTeamInstance.add(new SelectOption(userInfo.AxtriaSalesIQTM__Team_Instance__c, userInfo.AxtriaSalesIQTM__Team_Instance__r.Name));
        }
        
        populateCurvedata();
        
    }
    
    public void populateCurvedata(){//String TeamInstance
    
        List<Config_Potential_Field_Meaning__c> potentialFieldMappingList = new List<Config_Potential_Field_Meaning__c>();
        potentialFieldMappingList = [select id,Potential_Field_Name__c,Potential_Value_Name__c,Potential_Value_Number__c,Team_Instance__c from Config_Potential_Field_Meaning__c where Team_Instance__c = :selectedTeamInstance];
        Map<String,Decimal> potentialValueToNumberMap = new Map<String,Decimal>();
        String potentialFieldAPIName = '';
        
        if(potentialFieldMappingList.size()>0){
            
            potentialFieldAPIName = potentialFieldMappingList[0].Potential_Field_Name__c;
            for (Config_Potential_Field_Meaning__c cpfm : potentialFieldMappingList){
                potentialValueToNumberMap.put((cpfm.Potential_Value_Name__c).toUppercase(),cpfm.Potential_Value_Number__c);
            }
            System.debug('potentialValueToNumberMap'+potentialValueToNumberMap);
            
            curvedata = new List<graphWrapper>();
            string dynamicACFSQL='Select id, Physician_2__c, Physician_2__r.AccountNumber, '+potentialFieldAPIName+', Potential__c, Measure_Master__c, Measure_Master__r.Team_Instance__c From Account_Compute_Final__c where Measure_Master__r.Team_Instance__c = :selectedTeamInstance ';
            
            List<Account_Compute_Final__c> acfList = new List<Account_Compute_Final__c>();
            acfList = Database.query(dynamicACFSQL);
            
            //graphWrapper gw;
            Integer i=1;
            Decimal cumulativeSum=0;
            Decimal temp = 0;
            for (Account_Compute_Final__c acf: acfList){
                //if((acf.Potential__c).isNumeric()){
                    //System.debug('acf.get(potentialFieldAPIName)'+acf.get(potentialFieldAPIName));
                    temp = 0;
                    if(potentialValueToNumberMap.containsKey((String.valueOf(acf.get(potentialFieldAPIName))).toUppercase())){
                        temp = potentialValueToNumberMap.get(String.valueOf(acf.get(potentialFieldAPIName)).toUppercase());
                    }/*
                    else{
                        temp = 0;
                    }*/
                    cumulativeSum = cumulativeSum + temp;
                    //System.debug('temp'+temp);
                    //System.debug('cumulativeSum'+cumulativeSum);
                    curvedata.add(new graphWrapper(String.valueOf(i), cumulativeSum));
                    i++;
                //}
            }
        }
        else{
            System.debug('Please add Config_Potential_Field_Meaning__c for concentration curve');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Please add Config_Potential_Field_Meaning__c for concentration curve'));
        }
    }
    
    // Wrapper class
    public class graphWrapper {
        public String name { get; set; }
        public Decimal data1 { get; set; }
        public graphWrapper(String name, Decimal data1) {
            this.name = name;
            this.data1 = data1;
        }
    }
}