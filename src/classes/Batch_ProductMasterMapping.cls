global with sharing class Batch_ProductMasterMapping implements Database.Batchable<sObject> {
    list<Staging_Product_Master__c> listProd;
    map<string, Staging_Product_Master__c> mapProd;
    string jobType = util.getJobType('Product');
    list<string> allTeamInstance = jobType == 'Full Load'?util.getFullLoadTeamInstancesCallPlan():util.getDeltaLoadTeamInstancesCallPlan();
    map<string, boolean> mapProdStatus;
    string query = 'Select Id, Name, IsActive__c, Effective_End_Date__c, Effective_Start_Date__c, Country_Lookup__r.AxtriaSalesIQTM__Country_Code__c, Product_Code__c, Product_Type__c From Product_Catalog__c WHERE Team_Instance__c in :allTeamInstance WITH SECURITY_ENFORCED Order by Product_Code__c ASC, IsActive__c DESC';
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return database.getQueryLocator(query);
    }
    
    public void initiateVariables(){
        listProd = new list<Staging_Product_Master__c>();
        mapProd = new map<string, Staging_Product_Master__c>();
        mapProdStatus = new map<string,boolean>();
    }
    
    public void createMaps(){
        for(Staging_Product_Master__c p : [Select Id, Name, Country__c, Country_Code__c, IsActive__c, Product_Code__c, Product_Type__c, External_Id__c from Staging_Product_Master__c WHERE Name != NULL WITH SECURITY_ENFORCED]){
            mapProd.put(p.External_Id__c, p);
        }
        for(Product_Catalog__c pc : [Select Country_Lookup__r.AxtriaSalesIQTM__Country_Code__c, IsActive__c, Product_Code__c From Product_Catalog__c WHERE Product_Code__c != NULL WITH SECURITY_ENFORCED Order by Country_Lookup__r.AxtriaSalesIQTM__Country_Code__c, Product_Code__c ASC, IsActive__c DESC]){
            if(!mapProdStatus.containsKey(pc.Country_Lookup__r.AxtriaSalesIQTM__Country_Code__c+'_'+pc.Product_Code__c)){
                mapProdStatus.put(pc.Country_Lookup__r.AxtriaSalesIQTM__Country_Code__c+'_'+pc.Product_Code__c, pc.IsActive__c);
            }
        }
    }
        
    global void execute(Database.BatchableContext bc, List<Product_Catalog__c> scope){
        set<string> setProd = new set<string>();
        initiateVariables();
        createMaps();
        for(Product_Catalog__c pe : scope){
            string externalId = pe.Country_Lookup__r.AxtriaSalesIQTM__Country_Code__c+'_'+pe.Product_Code__c;
            if(!setProd.contains(externalId)){
                setProd.add(externalId);
                Staging_Product_Master__c prod = new Staging_Product_Master__c();
                if(!mapProd.containsKey(externalId)){
                    prod.IsActive__c = mapProdStatus.get(externalId);
                    prod.Country_Code__c = pe.Country_Lookup__r.AxtriaSalesIQTM__Country_Code__c;
                    prod.Product_Code__c = pe.Product_Code__c;
                    prod.Product_Type__c = 'Marketing Product';
                    prod.Country__c = pe.Country_Lookup__c;
                    prod.Record_Status__c = 'Updated';
                    prod.Name = pe.Name;
                    prod.External_Id__c = externalId;
                    listProd.add(prod);
                }
                else if(mapProd.get(externalId).IsActive__c != mapProdStatus.get(externalId)){
                    prod.IsActive__c = mapProdStatus.get(externalId);
                    prod.Id = mapProd.get(externalId).Id;
                    prod.Record_Status__c = 'Updated';
                    listProd.add(Prod);
                }
            }  
        }
        if(listProd.size()>0){
            //upsert listProd;
            SnTDMLSecurityUtil.upsertRecords(listProd, 'Batch_ProductMasterMapping');
        }
    }
    
    global void finish(Database.BatchableContext bc){
    }
}