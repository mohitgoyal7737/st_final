global class batchMergeAccountInactive implements Database.Batchable<sObject> {
    public String query;

    global batchMergeAccountInactive() {
        query = '';
        query = 'Select Id,GUID_2_ID__c from Merge_Unmerge__c where GUID_2_ID__c != null';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Merge_Unmerge__c> scope) {

        System.debug('======Query::::::::::' +scope);
        Set<String> loosingIDSet=new Set<String>();

        for(Merge_Unmerge__c mergeRec : scope)
        {
            loosingIDSet.add(mergeRec.GUID_2_ID__c);
        }
        System.debug('===loosingIDSet::::::' +loosingIDSet);
        
        System.debug('Inactive affiliation Record for Looser==================================================================================');

        String query2 = 'SELECT id, AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Affiliation_Network__c,AxtriaSalesIQTM__Parent_Account__c,AxtriaSalesIQTM__Root_Account__c,Affiliation_Status__c,Affiliation_End_Date__c,AxtriaSalesIQTM__Active__c FROM AxtriaSalesIQTM__Account_Affiliation__c where (AxtriaSalesIQTM__Account__c IN:loosingIDSet or AxtriaSalesIQTM__Parent_Account__c IN:loosingIDSet) and Affiliation_Status__c = \'Active\'';

        List<AxtriaSalesIQTM__Account_Affiliation__c> losAccAffList2 =  Database.query(query2);

        if(losAccAffList2!=null && losAccAffList2.size() >0)
        {
            for(AxtriaSalesIQTM__Account_Affiliation__c af: losAccAffList2) 
            {
                af.Affiliation_Status__c = 'Inactive';
                af.Affiliation_End_Date__c = Date.today().addDays(-1);
                af.AxtriaSalesIQTM__Active__c = false;
            }
            update losAccAffList2;   
        }

        list<Account>updateAclist = new list<Account>();
        set<String> lossingaccounts = new set<String>();
        list<Account> accList = [Select Id from Account where Id in :loosingIDSet and Status__c = 'Active'];
        if(accList != null)
        {
            for(Account acc : accList)
            {
                lossingaccounts.add(acc.Id);   
            }       
        }


        if(lossingaccounts!=null &&  lossingaccounts.size()>0)
        {
          for(string sfdcid : lossingaccounts)
          {
            if(sfdcid!=null)
            {
                    Account acc = new Account(id=sfdcid);
                    system.debug('==========DEACTIVATING ACCOUNTS::'+acc);
                    acc.Status__c = 'Inactive';
                    acc.AxtriaSalesIQTM__Active__c = 'Inactive';
                    acc.Active__c = 'Inactive';
                    updateAclist.add(acc);
                    System.debug('acc:::::' +acc);
              }
          }
        }

          if(updateAclist!=null && updateAclist.size()>0)
             Update updateAclist;
    }

    global void finish(Database.BatchableContext BC) {

    }
}