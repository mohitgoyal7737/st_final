public with sharing class BatchPopulateCimConfig implements Database.Batchable <sObject>, Database.Stateful
{
    public String query;
    public String teamInstance;
    public String Ids;
    public integer recordsUpdated;
    public integer totalRecords;
    String teamInstanceName;
    Boolean flag = true;
    public map <string, string> mapChangeRequest = new map <string, string>();
    public map <string, string> mapTeamInstanceName = new map <string, string>();
    public List<AxtriaSalesIQTM__Change_Request__c> changeRequestCreation = new List<AxtriaSalesIQTM__Change_Request__c>();

    //Added by HT(A0994) on 17th June 2020
    public String changeReqID;
    public Boolean flagValue = false;

    public BatchPopulateCimConfig(String teamInstance)
    {
        this.teamInstance = teamInstance;
        this.Ids = Ids;
        recordsUpdated = 0;
        totalRecords = 0;
        list <AxtriaSalesIQTM__Change_Request_Type__c> changerequest = [select id, name from AxtriaSalesIQTM__Change_Request_Type__c where name != NULL WITH SECURITY_ENFORCED];
        for (AxtriaSalesIQTM__Change_Request_Type__c change : changerequest)
        {
            mapChangeRequest.put(change.name, change.Id);
        }

        list <AxtriaSalesIQTM__Team_Instance__c> teaminstancelist = [select id, name from AxtriaSalesIQTM__Team_Instance__c  WHERE name != NULL];
        for (AxtriaSalesIQTM__Team_Instance__c teamins : teaminstancelist)
        {
            mapTeamInstanceName.put(teamins.name, teamins.Id);
        }
        if (teamInstance != 'All' && teamInstance != '')
        {
            teamInstanceName = [SELECT Name FROM AxtriaSalesIQTM__Team_Instance__c WHERE Id = : teamInstance].Name;
            query = 'SELECT Id,Name,Attribute_Display_Name__c,Team_Instance_Name__c, Agg_API_Name__c,Agg_Cond_Value__c,Agg_Object__c,Agg_Type__c,API_Name__c,Display_Name__c,Type_Name__c,Display_Column_Order__c,Enable__c,Enforcable__c,Error_Message__c,isOptimum__c,Custom_Metric__c,Metric_Name__c,Object_Name__c,Threshold_Max__c,Threshold_Min__c,Warning_Max__c,Warning_Min__c,Tooltip_Message__c,Visible__c,Approved_Value_Display_Name__c,Cust_Type__c,Display_Approved_Value__c ,Display_Optimum_Value__c ,Display_Original_Value__c ,Display_Proposed_Value__c ,isCallCapacity__c ,Optimum_Value_Display_Name__c ,Original_Value_Display_Name__c ,Proposed_Value_Display_Name__c,Change_Request__c, isError__c, SalesIQ_Error_Message__c from temp_Obj__c WHERE Team_Instance_Name__c = :teamInstanceName AND Object__c =\'CIM Config\' AND Status__c = \'New\'';

        }
        else
        {
            SnTDMLSecurityUtil.printDebugMessage('inside else');
            teamInstanceName = 'All';
            query = 'SELECT Id,Name,Attribute_Display_Name__c,Team_Instance_Name__c,Agg_API_Name__c,Agg_Cond_Value__c,Agg_Object__c,Agg_Type__c,API_Name__c,Display_Name__c,Type_Name__c,Display_Column_Order__c,Enable__c,Enforcable__c,Error_Message__c,isOptimum__c,Custom_Metric__c,Metric_Name__c,Object_Name__c,Threshold_Max__c,Threshold_Min__c,Warning_Max__c,Warning_Min__c,Tooltip_Message__c,Visible__c,Approved_Value_Display_Name__c,Cust_Type__c,Display_Approved_Value__c ,Display_Optimum_Value__c ,Display_Original_Value__c ,Display_Proposed_Value__c ,isCallCapacity__c ,Optimum_Value_Display_Name__c ,Original_Value_Display_Name__c ,Proposed_Value_Display_Name__c,Change_Request__c, isError__c, SalesIQ_Error_Message__c  from temp_Obj__c WHERE Object__c =\'CIM Config\' AND Status__c = \'New\'';

        }
    }

    public BatchPopulateCimConfig(String teamInstance, String Ids)
    {
        this.teamInstance = teamInstance;
        this.Ids = Ids;
        recordsUpdated = 0;
        totalRecords = 0;
        changeRequestCreation = [Select id,AxtriaSalesIQTM__Request_Type_Change__c,Records_Created__c from AxtriaSalesIQTM__Change_Request__c where id =: Ids ];
        list <AxtriaSalesIQTM__Change_Request_Type__c> changerequest = [select id, name from AxtriaSalesIQTM__Change_Request_Type__c where name != NULL WITH SECURITY_ENFORCED];
        for (AxtriaSalesIQTM__Change_Request_Type__c change : changerequest)
        {
            mapChangeRequest.put(change.name, change.Id);
        }

        list <AxtriaSalesIQTM__Team_Instance__c> teaminstancelist = [select id, name from AxtriaSalesIQTM__Team_Instance__c  WHERE name != NULL];
        for (AxtriaSalesIQTM__Team_Instance__c teamins : teaminstancelist)
        {
            mapTeamInstanceName.put(teamins.name, teamins.Id);
        }
        SnTDMLSecurityUtil.printDebugMessage('teaninstancenamemap is :: ' + mapTeamInstanceName);
        SnTDMLSecurityUtil.printDebugMessage('mapChangeRequest is :: ' + mapChangeRequest);
        if (teamInstance != 'All' && teamInstance != '')
        {
            teamInstanceName = [SELECT Name FROM AxtriaSalesIQTM__Team_Instance__c WHERE Id = : teamInstance].Name;
            SnTDMLSecurityUtil.printDebugMessage('teamInstanceName--'+teamInstanceName);
            query = 'SELECT Id,Name,Attribute_Display_Name__c,Team_Instance_Name__c, Agg_API_Name__c,Agg_Cond_Value__c,Agg_Object__c,Agg_Type__c,API_Name__c,Display_Name__c,Type_Name__c,Display_Column_Order__c,Enable__c,Enforcable__c,isOptimum__c,Custom_Metric__c,Metric_Name__c,Object_Name__c,Threshold_Max__c,Threshold_Min__c,Warning_Max__c,Warning_Min__c,Tooltip_Message__c,Visible__c,Approved_Value_Display_Name__c,Cust_Type__c,Display_Approved_Value__c ,Display_Optimum_Value__c ,Display_Original_Value__c ,Display_Proposed_Value__c ,isCallCapacity__c ,Optimum_Value_Display_Name__c ,Original_Value_Display_Name__c ,Proposed_Value_Display_Name__c,Change_Request__c, isError__c, SalesIQ_Error_Message__c, Error_message__c   from temp_Obj__c WHERE Team_Instance_Name__c = :teamInstanceName AND Object__c =\'CIM Config\' AND Status__c = \'New\'';
            //query += 'and Agg_API_Name__c!=null and Agg_Cond_Value__c!=null and Agg_Type__c!=null and Type_Name__c!=null and Display_Name__c!=null and API_Name__c!=null and Object_Name__c!=null and Team_Instance_Name__c!=null and Threshold_Max__c!=null and Threshold_Min__c!=null and Warning_Max__c!=null and Warning_Min__c!=null';
        }
        else
        {
            SnTDMLSecurityUtil.printDebugMessage('inside else');
            teamInstanceName = 'All';
            query = 'SELECT Id,Name,Attribute_Display_Name__c,Team_Instance_Name__c,Agg_API_Name__c,Agg_Cond_Value__c,Agg_Object__c,Agg_Type__c,API_Name__c,Display_Name__c,Type_Name__c,Display_Column_Order__c,Enable__c,Enforcable__c,Error_Message__c,isOptimum__c,Custom_Metric__c,Metric_Name__c,Object_Name__c,Threshold_Max__c,Threshold_Min__c,Warning_Max__c,Warning_Min__c,Tooltip_Message__c,Visible__c,Approved_Value_Display_Name__c,Cust_Type__c,Display_Approved_Value__c ,Display_Optimum_Value__c ,Display_Original_Value__c ,Display_Proposed_Value__c ,isCallCapacity__c ,Optimum_Value_Display_Name__c ,Original_Value_Display_Name__c ,Proposed_Value_Display_Name__c, Change_Request__c, isError__c, SalesIQ_Error_Message__c from temp_Obj__c WHERE Object__c =\'CIM Config\' AND Status__c = \'New\'';
            //query += 'and Agg_API_Name__c!=null and Agg_Cond_Value__c!=null and Agg_Type__c!=null and Type_Name__c!=null and Display_Name__c!=null and API_Name__c!=null and Object_Name__c!=null and Team_Instance_Name__c!=null and Threshold_Max__c!=null and Threshold_Min__c!=null and Warning_Max__c!=null and Warning_Min__c!=null';
        }
    }

    //Added by HT(A0944) on 12th June 2020
    public BatchPopulateCimConfig(String teamInstance, String Ids,Boolean flag)
    {
        this.teamInstance = teamInstance;
        this.Ids = Ids;
        flagValue = flag;
        changeReqID = Ids;
        recordsUpdated = 0;
        totalRecords = 0;
        changeRequestCreation = [Select id,AxtriaSalesIQTM__Request_Type_Change__c,Records_Created__c from AxtriaSalesIQTM__Change_Request__c where id =: Ids ];
        list <AxtriaSalesIQTM__Change_Request_Type__c> changerequest = [select id, name from AxtriaSalesIQTM__Change_Request_Type__c where name != NULL WITH SECURITY_ENFORCED];
        for (AxtriaSalesIQTM__Change_Request_Type__c change : changerequest)
        {
            mapChangeRequest.put(change.name, change.Id);
        }

        list <AxtriaSalesIQTM__Team_Instance__c> teaminstancelist = [select id, name from AxtriaSalesIQTM__Team_Instance__c  WHERE name != NULL];
        for (AxtriaSalesIQTM__Team_Instance__c teamins : teaminstancelist)
        {
            mapTeamInstanceName.put(teamins.name, teamins.Id);
        }
        SnTDMLSecurityUtil.printDebugMessage('teaninstancenamemap is :: ' + mapTeamInstanceName);
        SnTDMLSecurityUtil.printDebugMessage('mapChangeRequest is :: ' + mapChangeRequest);
        if (teamInstance != 'All' && teamInstance != '')
        {
            teamInstanceName = [SELECT Name FROM AxtriaSalesIQTM__Team_Instance__c WHERE Id = : teamInstance].Name;
            SnTDMLSecurityUtil.printDebugMessage('teamInstanceName--'+teamInstanceName);
            query = 'SELECT Id,Name,Attribute_Display_Name__c,Team_Instance_Name__c, Agg_API_Name__c,Agg_Cond_Value__c,Agg_Object__c,Agg_Type__c,API_Name__c,Display_Name__c,Type_Name__c,Display_Column_Order__c,Enable__c,Enforcable__c,isOptimum__c,Custom_Metric__c,Metric_Name__c,Object_Name__c,Threshold_Max__c,Threshold_Min__c,Warning_Max__c,Warning_Min__c,Tooltip_Message__c,Visible__c,Approved_Value_Display_Name__c,Cust_Type__c,Display_Approved_Value__c ,Display_Optimum_Value__c ,Display_Original_Value__c ,Display_Proposed_Value__c ,isCallCapacity__c ,Optimum_Value_Display_Name__c ,Original_Value_Display_Name__c ,Proposed_Value_Display_Name__c,Change_Request__c, isError__c, SalesIQ_Error_Message__c, Error_message__c   from temp_Obj__c WHERE Team_Instance_Name__c = :teamInstanceName AND Object__c =\'CIM Config\' AND Status__c = \'New\' and Change_Request__c =: changeReqID';
            //query += 'and Agg_API_Name__c!=null and Agg_Cond_Value__c!=null and Agg_Type__c!=null and Type_Name__c!=null and Display_Name__c!=null and API_Name__c!=null and Object_Name__c!=null and Team_Instance_Name__c!=null and Threshold_Max__c!=null and Threshold_Min__c!=null and Warning_Max__c!=null and Warning_Min__c!=null';
        }
        else
        {
            SnTDMLSecurityUtil.printDebugMessage('inside else');
            teamInstanceName = 'All';
            query = 'SELECT Id,Name,Attribute_Display_Name__c,Team_Instance_Name__c,Agg_API_Name__c,Agg_Cond_Value__c,Agg_Object__c,Agg_Type__c,API_Name__c,Display_Name__c,Type_Name__c,Display_Column_Order__c,Enable__c,Enforcable__c,Error_Message__c,isOptimum__c,Custom_Metric__c,Metric_Name__c,Object_Name__c,Threshold_Max__c,Threshold_Min__c,Warning_Max__c,Warning_Min__c,Tooltip_Message__c,Visible__c,Approved_Value_Display_Name__c,Cust_Type__c,Display_Approved_Value__c ,Display_Optimum_Value__c ,Display_Original_Value__c ,Display_Proposed_Value__c ,isCallCapacity__c ,Optimum_Value_Display_Name__c ,Original_Value_Display_Name__c ,Proposed_Value_Display_Name__c, Change_Request__c, isError__c, SalesIQ_Error_Message__c from temp_Obj__c WHERE Object__c =\'CIM Config\' AND Status__c = \'New\' and Change_Request__c =: changeReqID';
            //query += 'and Agg_API_Name__c!=null and Agg_Cond_Value__c!=null and Agg_Type__c!=null and Type_Name__c!=null and Display_Name__c!=null and API_Name__c!=null and Object_Name__c!=null and Team_Instance_Name__c!=null and Threshold_Max__c!=null and Threshold_Min__c!=null and Warning_Max__c!=null and Warning_Min__c!=null';
        }
    }

    public Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List <temp_Obj__c> tempObjList)
    {
        List <temp_Obj__c> finalTempObjList = new List <temp_Obj__c> ();
        List <AxtriaSalesIQTM__CIM_Config__c> cimInsertList = new List <AxtriaSalesIQTM__CIM_Config__c> ();
        totalRecords += tempObjList.size();
        for (temp_Obj__c tempObj : tempObjList)
        {
            if (String.isNotBlank(tempObj.Agg_API_Name__c) && String.isNotBlank(tempObj.Agg_Cond_Value__c) && String.isNotBlank(tempObj.Agg_Type__c) && String.isNotBlank(tempObj.Type_Name__c) && String.isNotBlank(tempObj.Display_Name__c) && String.isNotBlank(tempObj.API_Name__c) && String.isNotBlank(tempObj.Object_Name__c) && String.isNotBlank(tempObj.Team_Instance_Name__c) && String.isNotBlank(tempObj.Threshold_Max__c) && String.isNotBlank(tempObj.Threshold_Min__c) && String.isNotBlank(tempObj.Warning_Max__c) && String.isNotBlank(tempObj.Warning_Min__c) )
            {

                AxtriaSalesIQTM__CIM_Config__c cimconfig = new AxtriaSalesIQTM__CIM_Config__c();

                cimconfig.Name = tempObj.Name;
                cimconfig.AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c = tempObj.Agg_API_Name__c;
                cimconfig.AxtriaSalesIQTM__Aggregation_Condition_Attribute_Value__c = tempObj.Agg_Cond_Value__c;
                cimconfig.AxtriaSalesIQTM__Aggregation_Object_Name__c = tempObj.Agg_Object__c;
                cimconfig.AxtriaSalesIQTM__Aggregation_Type__c = tempObj.Agg_Type__c;

                if (mapChangeRequest != null)
                {
                    cimconfig.AxtriaSalesIQTM__Change_Request_Type__c = mapChangeRequest.get(tempObj.Type_Name__c);
                }

                cimconfig.AxtriaSalesIQTM__Display_Column_Order__c = tempObj.Display_Column_Order__c;
                cimconfig.AxtriaSalesIQTM__Attribute_Display_Name__c = tempObj.Attribute_Display_Name__c;
                cimconfig.AxtriaSalesIQTM__Display_Name__c = tempObj.Display_Name__c;
                cimconfig.AxtriaSalesIQTM__Enable__c = tempObj.Enable__c;
                cimconfig.AxtriaSalesIQTM__Enforcable__c = tempObj.Enforcable__c;
                cimconfig.AxtriaSalesIQTM__Error_Message__c = tempObj.Error_Message__c;
                cimconfig.AxtriaSalesIQTM__isOptimum__c = tempObj.isOptimum__c;
                cimconfig.AxtriaSalesIQTM__Is_Custom_Metric__c = tempObj.Custom_Metric__c;
                cimconfig.AxtriaSalesIQTM__Metric_Name__c = tempObj.Metric_Name__c;
                cimconfig.AxtriaSalesIQTM__Attribute_API_Name__c = tempObj.API_Name__c;
                cimconfig.AxtriaSalesIQTM__Object_Name__c = tempObj.Object_Name__c;

                if (mapTeamInstanceName != null)
                {
                    cimconfig.AxtriaSalesIQTM__Team_Instance__c = mapTeamInstanceName.get(tempObj.Team_Instance_Name__c);
                }


                cimconfig.AxtriaSalesIQTM__Threshold_Max__c = tempObj.Threshold_Max__c;
                cimconfig.AxtriaSalesIQTM__Threshold_Min__c = tempObj.Threshold_Min__c;
                cimconfig.AxtriaSalesIQTM__Threshold_Warning_Max__c = tempObj.Warning_Max__c;
                cimconfig.AxtriaSalesIQTM__Threshold_Warning_Min__c = tempObj.Warning_Min__c;
                cimconfig.AxtriaSalesIQTM__Tooltip_Message__c = tempObj.Tooltip_Message__c;
                cimconfig.AxtriaSalesIQTM__Visible__c = tempObj.Visible__c;
                cimconfig.Approved_Value_Display_Name__c = tempObj.Approved_Value_Display_Name__c;
                cimconfig.Cust_Type__c = tempObj.Cust_Type__c;

                cimconfig.Display_Approved_Value__c = tempObj.Display_Approved_Value__c;
                cimconfig.Display_Optimum_Value__c = tempObj.Display_Optimum_Value__c;
                cimconfig.Display_Original_Value__c = tempObj.Display_Original_Value__c;
                cimconfig.Display_Proposed_Value__c = tempObj.Display_Proposed_Value__c;
                cimconfig.isCallCapacity__c = tempObj.isCallCapacity__c;
                cimconfig.Optimum_Value_Display_Name__c = tempObj.Optimum_Value_Display_Name__c;
                cimconfig.Original_Value_Display_Name__c = tempObj.Original_Value_Display_Name__c;
                cimconfig.Proposed_Value_Display_Name__c = tempObj.Proposed_Value_Display_Name__c;
                tempObj.status__c = 'Processed';
                tempObj.Change_Request__c = Ids;
                tempObj.isError__c = false;
                SnTDMLSecurityUtil.printDebugMessage('Updating Cim record ::::::::' + cimconfig);
                finalTempObjList.add(tempObj);
                cimInsertList.add(cimconfig);
                SnTDMLSecurityUtil.printDebugMessage('list to update ::::::::' + cimInsertList);
            }
            else
            {
                tempObj.status__c = 'Rejected';
                tempObj.Change_Request__c = Ids;
                tempObj.isError__c = true;
                tempObj.Error_Message__c = ' Mandatory field missing or Incorrect';
                tempObj.SalesIQ_Error_Message__c = ' Mandatory field missing or Incorrect';
                finalTempObjList.add(tempObj);
            }
        }

        if (!cimInsertList.IsEmpty())
        {
            //database.saveresult[] ds = Database.insert(cimInsertList, false);
            //Changed for security review
            SObjectAccessDecision securityDecision = Security.stripInaccessible(AccessType.CREATABLE,cimInsertList);
            //list<AxtriaSalesIQTM__CIM_Config__c> temp = securityDecision.getRecords();
            database.saveresult[] ds = Database.insert(securityDecision.getRecords(), false);
            //throw exception if permission is missing 
            if(!securityDecision.getRemovedFields().isEmpty() ){
               throw new SnTException(securityDecision, 'AxtriaSalesIQTM__CIM_Config__c', SnTException.OperationType.C ); 
            }
            //End of security review change

            for(database.SaveResult d : ds)
            {
                if(d.issuccess())
                {
                    recordsUpdated++;
                }
                else
                {
                    flag = false;
                }
            }
        }
        
        //recordsFailed += (totalRecrods - recordsCreated);
        if(finalTempObjList.size()!=0)
        {
            //Database.update(finalTempObjList);
            //Changed for security review
            SObjectAccessDecision securityDecision = Security.stripInaccessible(AccessType.UPDATABLE,finalTempObjList);
            //insert securityDecision.getRecords();
            //list<temp_Obj__c> temp = securityDecision.getRecords();
            Database.update(securityDecision.getRecords());
            //throw exception if permission is missing 
            if(!securityDecision.getRemovedFields().isEmpty() ){
               throw new SnTException(securityDecision, 'temp_Obj__c', SnTException.OperationType.U ); 
            }
            //End of security review change
        }
    }

    public void finish(Database.BatchableContext BC)
    {
        Boolean noJobErrors;
        String changeReqStatus;

        if(Ids != null)
        {
            AxtriaSalesIQTM__Change_Request__c changerequestinfinish = new AxtriaSalesIQTM__Change_Request__c();
            changerequestinfinish.id = Ids;
            noJobErrors = ST_Utility.getJobStatus(BC.getJobId());
            changeReqStatus = flag && noJobErrors ? 'Done' : 'Error';

            /*if(flag && ST_Utility.getJobStatus(BC.getJobId()))
            {
                changerequestinfinish.Job_Status__c = 'Done';
            }
            else
            {
                changerequestinfinish.Job_Status__c = 'Error';
            }*/
            changerequestinfinish.Records_Updated__c = recordsUpdated;
            if(changeRequestCreation.size()>0 && changeRequestCreation[0].AxtriaSalesIQTM__Request_Type_Change__c == 'Data Load Backend')
            {
                changerequestinfinish.Records_Created__c = totalRecords;
            }

            BatchUpdateTempObjRecsCR batchCall = new BatchUpdateTempObjRecsCR(Ids,true,'Mandatory Field Missing or Incorrect Team Instance',changeReqStatus);
            Database.executeBatch(batchCall,2000);
             
            //update changerequestinfinish;
            SnTDMLSecurityUtil.updateRecords(changerequestinfinish, 'BatchPopulateCimConfig');
            //Direct_Load_Records_Status_Update.Direct_Load_Records_Status_Update(Ids,'BatchPopulateCimConfig');
        }
    }
}