global class batchdelete3 implements Database.Batchable<sObject>, Database.Stateful  {
 public String batchStatus {get;set;}
     public Boolean pollerbool {get;set;}

    global string query ;
    global string selectedobj ;
    global string teamID;
    global string teamInstance;
    globaL string teamname {get;set;}
    global list<sObject> todlt=new list<sObject>();
    global List<String> stdAcc{get;set;}
    
    
    global batchdelete3 (String Team,String obj){
        
        teamInstance=Team;
        selectedobj=obj;
         teamname =[select name from AxtriasalesIQTM__Team__c Where id =:teamInstance][0].name;
           System.debug( 'teamname =='+teamname ); 
           System.debug('selectedobj---'+selectedobj);  
        if(selectedobj == 'Account'){
       stdAcc = new List<String> {'CD451796','CD656092','CD439877','CD355118','CC947211','CD736025','CD355119-A','CD355120-B','CC978213'};
       query='select id from Account where AccountNumber NOT IN : stdAcc';   
         }else{
            if(selectedobj=='temp_Acc_Terr__c')
           {query='select id from '+selectedobj+' where TeamName__c =:teamname and Territory__c != null' ;
           System.debug('Query==='+query  );
           }
           else
           query='select id from '+selectedobj+' where Team_Name__c=:teamname ';
           
           }
              }
    
    global Database.Querylocator start(Database.BatchableContext bc){
        return Database.getQueryLocator(query);
        
    } 
    
    global void execute (Database.BatchableContext BC, List<sObject> todlt){
        
        delete todlt;
        System.debug('Data Deleted for '+selectedobj+' Object ');
         
        
    }
    
          global void finish(Database.BatchableContext BC){

            AsyncApexJob a = [Select Id, Status,ExtendedStatus,NumberOfErrors,JobItemsProcessed,TotalJobItems, CreatedBy.Email
                    from AsyncApexJob where Id =:BC.getJobId()];
                    
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {a.CreatedBy.Email};
            mail.setToAddresses(toAddresses);
            mail.setSubject(selectedobj+' '+ a.Status);
            mail.setPlainTextBody('records processed ' + a.TotalJobItems +
           'with '+ a.NumberOfErrors + ' failures.');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });                  
}
}