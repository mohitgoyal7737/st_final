global with sharing class Batch_ObjectTerritoryMapping implements Database.Batchable<sObject> {
    list<Staging_Position_Account__c> objTerr;
    map<string, Staging_Position_Account__c> mapObjTerr;
    string jobType = util.getJobType('ObjectTerritory');
    list<string> allTeamInstance = jobType == 'Full Load'?util.getFullLoadTeamInstancesCallPlan():util.getDeltaLoadTeamInstancesCallPlan();
    //dateTime syncTime = util.getSyncTime('ObjectTerritory');
    //dateTime updateSyncTime = [select LastModifiedDate from AxtriaSalesIQTM__Position_Account__c Where AxtriaSalesIQTM__Team_Instance__c in :allTeamInstance Order by LastModifiedDate desc Limit 1].LastModifiedDate;
   // map<string, AxtriaSalesIQTM__Position_Account__c> mapPosAcc;
   // set<string> setExtId;
    string query; 
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
         if(jobType == 'Full Load'){
            query = 'Select Id, AxtriaSalesIQTM__Assignment_Status__c, AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Account__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c From AxtriaSalesIQTM__Position_Account__c Where AxtriaSalesIQTM__Assignment_Status__c IN (\'Active\') AND AxtriaSalesIQTM__Team_Instance__c in :allTeamInstance WITH SECURITY_ENFORCED Order by AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Assignment_Status__c';
        }
        else{    
            query = 'Select Id, AxtriaSalesIQTM__Assignment_Status__c, AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Account__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c From AxtriaSalesIQTM__Position_Account__c Where AxtriaSalesIQTM__Assignment_Status__c IN (\'Active\',\'Inactive\') AND AxtriaSalesIQTM__Team_Instance__c in :allTeamInstance WITH SECURITY_ENFORCED order by AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Assignment_Status__c';
        }
        return database.getQueryLocator(query);
    }
    
    public void initiateVariables(){
        objTerr = new list<Staging_Position_Account__c>();
        mapObjTerr = new map<string, Staging_Position_Account__c>();
     //   mapPosAcc = new map<string, AxtriaSalesIQTM__Position_Account__c>();
     //   setExtId = new set<string>();
    }
    
    public void createMaps(set<string> setExternalId){
        for(Staging_Position_Account__c ut : [Select Id, PositionCode__c, AccountNumber__c, External_Id__c, Account__c, Account_Type__c, Assignment_Status__c, Association_Cause__c, Country__c, Position__c from Staging_Position_Account__c Where External_Id__c In :setExternalId WITH SECURITY_ENFORCED]){
            mapObjTerr.put(ut.External_Id__c, ut);
        }
    }
        
    global void execute(Database.BatchableContext bc, List<AxtriaSalesIQTM__Position_Account__c> scope){
        set<string> setExternalId = new set<string>();
        set<string> setIds = new set<string>();
        for(AxtriaSalesIQTM__Position_Account__c pe : scope){
            setExternalId.add(pe.AxtriaSalesIQTM__Account__r.AccountNumber+'_'+pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);    
        }
        initiateVariables();
        createMaps(setExternalId);
        for(AxtriaSalesIQTM__Position_Account__c pe : scope){
            Staging_Position_Account__c ut = new Staging_Position_Account__c();
            string externalId = pe.AxtriaSalesIQTM__Account__r.AccountNumber+'_'+pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
            if(!setIds.contains(externalId)){
                setIds.add(externalId);
                if(!mapObjTerr.containsKey(externalId) && pe.AxtriaSalesIQTM__Assignment_Status__c == 'Active'){
                    ut.Account__c = pe.AxtriaSalesIQTM__Account__c;
                    ut.AccountNumber__c = pe.AxtriaSalesIQTM__Account__r.AccountNumber;
                    ut.PositionCode__c = pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
                    ut.Account_Type__c = 'Account';
                    ut.Assignment_Status__c = 'Active';
                    ut.Country__c = pe.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c;
                    ut.Position__c = pe.AxtriaSalesIQTM__Position__c;
                    ut.External_Id__c = externalId;
                    ut.Record_Status__c = 'Updated';
                    objTerr.add(ut);
                }
                else if(mapObjTerr.containsKey(externalId) && ((pe.AxtriaSalesIQTM__Assignment_Status__c == 'Active' && mapObjTerr.get(externalId).Assignment_Status__c == 'Inactive') || (pe.AxtriaSalesIQTM__Assignment_Status__c == 'Inactive' && mapObjTerr.get(externalId).Assignment_Status__c == 'Active'))){
                    ut.Assignment_Status__c = pe.AxtriaSalesIQTM__Assignment_Status__c;
                    ut.Record_Status__c = 'Updated';
                    ut.Id = mapObjTerr.get(externalId).Id;
                    objTerr.add(ut);
                }
                else {}
            }
        }
        //upsert(objTerr);
        SnTDMLSecurityUtil.upsertRecords(objTerr, 'Batch_ObjectTerritoryMapping');

    }
    
    global void finish(Database.BatchableContext bc){
        //util.updateSyncTime('ObjectTerritory',updateSyncTime);
    }
}