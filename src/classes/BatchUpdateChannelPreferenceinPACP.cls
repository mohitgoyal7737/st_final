global with sharing class BatchUpdateChannelPreferenceinPACP implements Database.Batchable<sObject> 
{
    public String query;
    public List<String> list_ti;
    public string ruleidis ;
    public boolean directFlag=false;
    public boolean directFlag2=false;
    public boolean chained = false;
    public String teaminstanceis;

    //Added by HT(A0944) on 12th June 2020
    public String alignNmsp;
    public String batchName;
    public String callPlanLoggerID;
    public Boolean proceedNextBatch = true;

    //Added by HT(A0944) on 12th June 2020
    global BatchUpdateChannelPreferenceinPACP(List<String> list_team_ins,String loggerID,String alignNmspPrefix) 
    {
        callPlanLoggerID = loggerID;
        alignNmsp = alignNmspPrefix;
        SnTDMLSecurityUtil.printDebugMessage('callPlanLoggerID--'+callPlanLoggerID);
        SnTDMLSecurityUtil.printDebugMessage('alignNmsp--'+alignNmsp);

        if(list_team_ins.size()>0){
            list_ti = list_team_ins;                  
        }
        this.chained = false;
        this.query = createQuery(list_team_ins[0]);
    }

    global BatchUpdateChannelPreferenceinPACP(List<String> list_team_ins) {
        if(list_team_ins.size()>0){
            list_ti = list_team_ins;
                              
        }
        this.chained = false;
        this.query = createQuery(list_team_ins[0]);
    }

    //Added by HT(A0944) on 12th June 2020
    global BatchUpdateChannelPreferenceinPACP(List<String> list_team_ins, String ruleID, Boolean flag_1, 
                                                Boolean flag_2,String loggerID,String alignNmspPrefix) 
    {
        if(list_team_ins.size()>0){
            list_ti = list_team_ins;
        }

        callPlanLoggerID = loggerID;
        alignNmsp = alignNmspPrefix;
        SnTDMLSecurityUtil.printDebugMessage('callPlanLoggerID--'+callPlanLoggerID);
        SnTDMLSecurityUtil.printDebugMessage('alignNmsp--'+alignNmsp);

        this.ruleidis = ruleID;
        this.directFlag = flag_1;
        this.directFlag2 = flag_2;
        this.chained = true;
        this.teaminstanceis = list_team_ins[0];
        this.query = createQuery(list_team_ins[0]);
    }

    global BatchUpdateChannelPreferenceinPACP(List<String> list_team_ins, String ruleID, Boolean flag_1, Boolean flag_2) {
        if(list_team_ins.size()>0){
            list_ti = list_team_ins;
                              
        }
        this.ruleidis = ruleID;
        this.directFlag = flag_1;
        this.directFlag2 = flag_2;
        this.chained = true;
        this.teaminstanceis = list_team_ins[0];
        this.query = createQuery(list_team_ins[0]);
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scope) 
    {
        try
        {
            updateChannelPreferenceinPACP objj = new updateChannelPreferenceinPACP();
            objj.updateChannelPreferenceinPACP(scope,false);
        }
        catch(Exception e)
        {
            //Added by HT(A0994) on 12th June 2020
            SnTDMLSecurityUtil.printDebugMessage('Error in BatchUpdateChannelPreferenceinPACP : execute()-->'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack Trace-->'+e.getStackTraceString());
            proceedNextBatch = false;
            SnTDMLSecurityUtil.printDebugMessage('callPlanLoggerID--'+callPlanLoggerID);

            if(callPlanLoggerID!=null && callPlanLoggerID!=''){
                SalesIQUtility.updateCallPlanLoggerRecord(callPlanLoggerID,'Failed at updating Channel Preference','Error',alignNmsp);
            }
            //End
        } 
    }

    global void finish(Database.BatchableContext BC) 
    {
        try
        {
            list_ti.remove(0); 
            if(list_ti.size()>0){
                if(callPlanLoggerID!=null && callPlanLoggerID!='')
                {
                    if(proceedNextBatch)
                        Database.executeBatch(new BatchUpdateChannelPreferenceinPACP(list_ti,callPlanLoggerID,alignNmsp),2000);
                }
                else
                    Database.executeBatch(new BatchUpdateChannelPreferenceinPACP(list_ti),2000);
            }
            
            else if(chained)
            {
                /*if(directFlag2==true){
                    Database.executeBatch(new BatchCallPlanSummaryReports(), 500);
                }
                else if(directFlag==true)
                {    
                    if(callPlanLoggerID!=null && callPlanLoggerID!='')
                    {
                        if(proceedNextBatch)
                            Database.executeBatch(new BatchCallPlanSummaryReports(teaminstanceis,callPlanLoggerID,alignNmsp),2000);
                    }
                    else
                        Database.executeBatch(new BatchCallPlanSummaryReports(teaminstanceis), 500);    
                }
                else{
                    Database.executeBatch(new BatchCallPlanSummaryReports(teaminstanceis,ruleidis), 500);   
                } */  
                List<String> cimConfigList = new List<String>();
                for(AxtriaSalesIQTM__CIM_Config__c cim :[Select id from AxtriaSalesIQTM__CIM_Config__c where AxtriaSalesIQTM__CR_Type_Name__c = :SalesIQGlobalConstants.CR_TYPE_CALL_PLAN and AxtriaSalesIQTM__Team_Instance__c = :teaminstanceis WITH SECURITY_ENFORCED ]){
                  cimConfigList.add(cim.ID);

                }
                String team_ins = teaminstanceis;
                if(cimConfigList.size()>0){
                    if(callPlanLoggerID!=null && callPlanLoggerID!=''){
                        if(proceedNextBatch){
                            Database.executeBatch(new BatchCIMPositionMatrixSummaryUpdate(cimConfigList,team_ins,ruleidis,directFlag,
                                                    directFlag2,callPlanLoggerID,alignNmsp),2000);
                        }
                    }
                    else{
                        Database.executeBatch(new BatchCIMPositionMatrixSummaryUpdate(cimConfigList,team_ins,ruleidis,directFlag,directFlag2),2000);  
                    }
                    //Database.executeBatch(new BatchCIMPositionMatrixSummaryUpdate(cimConfigList,team_ins),2000);                
                }
            }
        }
        catch(Exception e)
        {
            //Added by HT(A0994) on 12th June 2020
            SnTDMLSecurityUtil.printDebugMessage('Error in BatchUpdateParentPacpOnPacp : finish()-->'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack Trace-->'+e.getStackTraceString());
            SnTDMLSecurityUtil.printDebugMessage('callPlanLoggerID--'+callPlanLoggerID);

            if(callPlanLoggerID!=null && callPlanLoggerID!=''){
                SalesIQUtility.updateCallPlanLoggerRecord(callPlanLoggerID,'Failed at updating Channel Preference','Error',alignNmsp);
            }
            //End
        }
    }

    public String createQuery(String team_ins_id){
        String query = 'SELECT P1__c,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.Type,Channel_1_Preference__c,'+
                        'Channel_2_Preference__c,Channel_3_Preference__c,Channel_4_Preference__c,Channel_5_Preference__c,'+
                        'AxtriaSalesIQTM__Team_Instance__c FROM AxtriaSalesIQTM__Position_Account_Call_Plan__c where '+
                        'AxtriaSalesIQTM__Team_Instance__c =\''+team_ins_id+'\'';
        return query;
    }
}