@isTest
private  class BatchCreateRuleResultsDownload_Test {
static testMethod void testMethod1() 
 {
    System.Test.startTest();
    ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
    String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
    List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
    System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
    System.Test.stopTest();
    /*User loggedInUser = new User(id=UserInfo.getUserId());
    
    AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    insert orgmas;
    AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
    insert countr;
    Account acc= TestDataFactory.createAccount();
    acc.AxtriaSalesIQTM__Speciality__c ='Cardiology';
    acc.AxtriaSalesIQTM__Country__c = countr.id;
    acc.AccountNumber ='A12324567';
    acc.AxtriaSalesIQTM__External_Account_Number__c = 'A12324567';
    insert acc;
    
    AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
    team.Name = 'Oncology';
    team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
    insert team;
    AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
    insert teamins1;
    
    AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
    workspace.AxtriaSalesIQTM__Country__c = countr.id;
    insert workspace;
    
    
    AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
    scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
    insert scen;
    
    AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
    teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
    teamins.AxtriaSalesIQTM__Team_Cycle_Name__c = 'Current';
    teamins.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
    teamins.IsHCOSegmentationEnabled__c = false;
    insert teamins;
    
    Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
    insert pcc;
    
    Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
    mmc.Team_Instance__c = teamins.id;
    mmc.Measure_Type__c = 'HCO';
    insert mmc;
    Measure_Master__c mmc1 = TestDataFactory.createMeasureMaster(pcc, team, teamins);
    mmc1.Team_Instance__c = teamins.id;
    mmc1.Brand_Lookup__c = mmc.Brand_Lookup__c;
    mmc1.is_complete__c = true;
    mmc1.State__c = 'Complete';
    insert mmc1;
    Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
    pp.Name ='MARKET_ CRC';
    insert pp;
    
    Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
    rp.Parameter__c = pp.id;
    insert rp;
    
    AxtriaSalesIQTM__Employee__c emp = TestDataFactory.createEmployee('firstName', 'lastName');
    insert emp;
    AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
    pos.AxtriaSalesIQTM__Employee__c = emp.id;
    insert pos;
    
    AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
    posAccount.AxtriaSalesIQTM__Account__c = acc.id;
    posAccount.AxtriaSalesIQTM__IsShared__c = true;
    posAccount.AxtriaSalesIQTM__SharedWith__c ='None';
    insert posAccount;
    
    
    
    BU_Response__c bu = TestDataFactory.createBuResponse(posAccount,pcc,teamins,team,acc);
    insert bu;
    
    
    Account_Compute_Final__c compFinal = TestDataFactory.createComputeFinal(mmc,acc,posAccount,bu,pos);
    for(integer i=1;i<=50;i++){
        compFinal.put('OUTPUT_NAME_'+i+'__c',pp.Name);
    }
    compFinal.Measure_Master__c = mmc.id;
    compFinal.Physician__c = null;
    insert compFinal;
    
    AxtriaSalesIQTM__ETL_Config__c etl = TestDataFactory.configureETL();
    etl.Name = 'ComparativeAnalysis';
    insert etl;
    
    Test.startTest();
    System.runAs(loggedInUser){
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        BatchCreateRuleResultsDownload obj = new BatchCreateRuleResultsDownload(mmc.Id);
        obj.query  =  'SELECT Id,Physician_2__c,Physician_2__r.Name,Physician_2__r.AccountNumber ,Physician__c,Position__c,Physician__r.AxtriaSalesIQTM__SharedWith__c,Physician__r.AxtriaSalesIQTM__IsShared__c,Assigned_Rep__c,CustonerSegVal__c,OUTPUT_NAME_1__c, OUTPUT_NAME_2__c, OUTPUT_NAME_3__c, OUTPUT_NAME_4__c, OUTPUT_NAME_5__c, OUTPUT_NAME_7__c,OUTPUT_NAME_6__c, OUTPUT_NAME_8__c,OUTPUT_NAME_9__c, OUTPUT_NAME_10__c, OUTPUT_NAME_11__c, OUTPUT_NAME_12__c,OUTPUT_NAME_13__c, OUTPUT_NAME_14__c, OUTPUT_NAME_15__c, OUTPUT_NAME_16__c,OUTPUT_NAME_17__c, OUTPUT_NAME_18__c, OUTPUT_NAME_19__c, OUTPUT_NAME_20__c, OUTPUT_NAME_21__c,OUTPUT_NAME_22__c, OUTPUT_NAME_23__c, OUTPUT_NAME_24__c, OUTPUT_NAME_25__c, OUTPUT_NAME_26__c,OUTPUT_NAME_27__c, OUTPUT_NAME_28__c, OUTPUT_NAME_29__c, OUTPUT_NAME_30__c,OUTPUT_NAME_31__c,OUTPUT_NAME_32__c,OUTPUT_NAME_33__c,OUTPUT_NAME_34__c,OUTPUT_NAME_35__c,OUTPUT_NAME_36__c,OUTPUT_NAME_37__c,OUTPUT_NAME_38__c,OUTPUT_NAME_39__c,OUTPUT_NAME_40__c,OUTPUT_NAME_41__c,OUTPUT_NAME_42__c,OUTPUT_NAME_43__c,OUTPUT_NAME_44__c,OUTPUT_NAME_45__c,OUTPUT_NAME_46__c,OUTPUT_NAME_47__c,OUTPUT_NAME_48__c,OUTPUT_NAME_49__c,OUTPUT_NAME_50__c,OUTPUT_VALUE_1__c,  OUTPUT_VALUE_2__c, OUTPUT_VALUE_3__c, OUTPUT_VALUE_4__c, OUTPUT_VALUE_5__c, OUTPUT_VALUE_6__c, OUTPUT_VALUE_7__c,  OUTPUT_VALUE_8__c, OUTPUT_VALUE_9__c, OUTPUT_VALUE_10__c, OUTPUT_VALUE_11__c, OUTPUT_VALUE_12__c, OUTPUT_VALUE_13__c,OUTPUT_VALUE_14__c, OUTPUT_VALUE_15__c, OUTPUT_VALUE_16__c, OUTPUT_VALUE_17__c, OUTPUT_VALUE_18__c, OUTPUT_VALUE_19__c, OUTPUT_VALUE_20__c,  OUTPUT_VALUE_21__c, OUTPUT_VALUE_22__c, OUTPUT_VALUE_23__c, OUTPUT_VALUE_24__c, OUTPUT_VALUE_25__c, OUTPUT_VALUE_26__c, OUTPUT_VALUE_27__c,  OUTPUT_VALUE_28__c, OUTPUT_VALUE_29__c, OUTPUT_VALUE_30__c,OUTPUT_VALUE_31__c,OUTPUT_VALUE_32__c,OUTPUT_VALUE_33__c,OUTPUT_VALUE_34__c,OUTPUT_VALUE_35__c,OUTPUT_VALUE_36__c,OUTPUT_VALUE_37__c,OUTPUT_VALUE_38__c,OUTPUT_VALUE_39__c,OUTPUT_VALUE_40__c,OUTPUT_VALUE_41__c,OUTPUT_VALUE_42__c,OUTPUT_VALUE_43__c,OUTPUT_VALUE_44__c,OUTPUT_VALUE_45__c,OUTPUT_VALUE_46__c,OUTPUT_VALUE_47__c,OUTPUT_VALUE_48__c,OUTPUT_VALUE_49__c,OUTPUT_VALUE_50__c FROM Account_Compute_Final__c ';            
        Database.executeBatch(obj);
    }
    Test.stopTest();
    */
}
static testMethod void testMethod2() 
{
    System.Test.startTest();
    ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
    String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
    List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
    System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
    System.Test.stopTest();
   /* User loggedInUser = new User(id=UserInfo.getUserId());
    
    AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    insert orgmas;
    AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
    insert countr;
    Account acc= TestDataFactory.createAccount();
    acc.AxtriaSalesIQTM__Speciality__c ='Cardiology';
    acc.AxtriaSalesIQTM__Country__c = countr.id;
    acc.AccountNumber ='A12324567';
    acc.AxtriaSalesIQTM__External_Account_Number__c = 'A12324567';
    insert acc;
    
    AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
    team.Name = 'Oncology';
    team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
    insert team;
    AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
    insert teamins1;
    
    AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
    workspace.AxtriaSalesIQTM__Country__c = countr.id;
    insert workspace;
    
    
    AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
    scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
    insert scen;
    
    AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
    teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
    teamins.AxtriaSalesIQTM__Team_Cycle_Name__c = 'Current';
    teamins.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
    teamins.IsHCOSegmentationEnabled__c = true;
    insert teamins;
    
    Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
    insert pcc;
    
    Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
    mmc.Team_Instance__c = teamins.id;
    mmc.Measure_Type__c = 'HCO';
    insert mmc;
    Measure_Master__c mmc1 = TestDataFactory.createMeasureMaster(pcc, team, teamins);
    mmc1.Team_Instance__c = teamins.id;
    mmc1.Brand_Lookup__c = mmc.Brand_Lookup__c;
    mmc1.is_complete__c = true;
    mmc1.State__c = 'Complete';
    insert mmc1;
    Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
    pp.Name ='MARKET_ CRC';
    insert pp;
    
    Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
    rp.Parameter__c = pp.id;
    insert rp;
    
    AxtriaSalesIQTM__Employee__c emp = TestDataFactory.createEmployee('firstName', 'lastName');
    insert emp;
    AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
    pos.AxtriaSalesIQTM__Employee__c = emp.id;
    insert pos;
    
    AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
    posAccount.AxtriaSalesIQTM__Account__c = acc.id;
    posAccount.AxtriaSalesIQTM__IsShared__c = true;
    posAccount.AxtriaSalesIQTM__SharedWith__c ='None';
    insert posAccount;
    
    
    
    BU_Response__c bu = TestDataFactory.createBuResponse(posAccount,pcc,teamins,team,acc);
    insert bu;
    
    
    Account_Compute_Final__c compFinal = TestDataFactory.createComputeFinal(mmc,acc,posAccount,bu,pos);
    for(integer i=1;i<=50;i++){
        compFinal.put('OUTPUT_NAME_'+i+'__c',pp.Name);
    }
    compFinal.Measure_Master__c = mmc.id;
    insert compFinal;
    
    AxtriaSalesIQTM__ETL_Config__c etl = TestDataFactory.configureETL();
    etl.Name = 'ComparativeAnalysis';
    insert etl;
    
    Test.startTest();
    System.runAs(loggedInUser){
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        BatchCreateRuleResultsDownload obj = new BatchCreateRuleResultsDownload(mmc.Id,'All');
        obj.query  =  'SELECT Id,Physician_2__c,Physician_2__r.Name,Physician_2__r.AccountNumber ,Physician__c,Position__c,Physician__r.AxtriaSalesIQTM__SharedWith__c,Physician__r.AxtriaSalesIQTM__IsShared__c,Assigned_Rep__c,CustonerSegVal__c,OUTPUT_NAME_1__c, OUTPUT_NAME_2__c, OUTPUT_NAME_3__c, OUTPUT_NAME_4__c, OUTPUT_NAME_5__c, OUTPUT_NAME_7__c,OUTPUT_NAME_6__c, OUTPUT_NAME_8__c,OUTPUT_NAME_9__c, OUTPUT_NAME_10__c, OUTPUT_NAME_11__c, OUTPUT_NAME_12__c,OUTPUT_NAME_13__c, OUTPUT_NAME_14__c, OUTPUT_NAME_15__c, OUTPUT_NAME_16__c,OUTPUT_NAME_17__c, OUTPUT_NAME_18__c, OUTPUT_NAME_19__c, OUTPUT_NAME_20__c, OUTPUT_NAME_21__c,OUTPUT_NAME_22__c, OUTPUT_NAME_23__c, OUTPUT_NAME_24__c, OUTPUT_NAME_25__c, OUTPUT_NAME_26__c,OUTPUT_NAME_27__c, OUTPUT_NAME_28__c, OUTPUT_NAME_29__c, OUTPUT_NAME_30__c,OUTPUT_NAME_31__c,OUTPUT_NAME_32__c,OUTPUT_NAME_33__c,OUTPUT_NAME_34__c,OUTPUT_NAME_35__c,OUTPUT_NAME_36__c,OUTPUT_NAME_37__c,OUTPUT_NAME_38__c,OUTPUT_NAME_39__c,OUTPUT_NAME_40__c,OUTPUT_NAME_41__c,OUTPUT_NAME_42__c,OUTPUT_NAME_43__c,OUTPUT_NAME_44__c,OUTPUT_NAME_45__c,OUTPUT_NAME_46__c,OUTPUT_NAME_47__c,OUTPUT_NAME_48__c,OUTPUT_NAME_49__c,OUTPUT_NAME_50__c,OUTPUT_VALUE_1__c,  OUTPUT_VALUE_2__c, OUTPUT_VALUE_3__c, OUTPUT_VALUE_4__c, OUTPUT_VALUE_5__c, OUTPUT_VALUE_6__c, OUTPUT_VALUE_7__c,  OUTPUT_VALUE_8__c, OUTPUT_VALUE_9__c, OUTPUT_VALUE_10__c, OUTPUT_VALUE_11__c, OUTPUT_VALUE_12__c, OUTPUT_VALUE_13__c,OUTPUT_VALUE_14__c, OUTPUT_VALUE_15__c, OUTPUT_VALUE_16__c, OUTPUT_VALUE_17__c, OUTPUT_VALUE_18__c, OUTPUT_VALUE_19__c, OUTPUT_VALUE_20__c,  OUTPUT_VALUE_21__c, OUTPUT_VALUE_22__c, OUTPUT_VALUE_23__c, OUTPUT_VALUE_24__c, OUTPUT_VALUE_25__c, OUTPUT_VALUE_26__c, OUTPUT_VALUE_27__c,  OUTPUT_VALUE_28__c, OUTPUT_VALUE_29__c, OUTPUT_VALUE_30__c,OUTPUT_VALUE_31__c,OUTPUT_VALUE_32__c,OUTPUT_VALUE_33__c,OUTPUT_VALUE_34__c,OUTPUT_VALUE_35__c,OUTPUT_VALUE_36__c,OUTPUT_VALUE_37__c,OUTPUT_VALUE_38__c,OUTPUT_VALUE_39__c,OUTPUT_VALUE_40__c,OUTPUT_VALUE_41__c,OUTPUT_VALUE_42__c,OUTPUT_VALUE_43__c,OUTPUT_VALUE_44__c,OUTPUT_VALUE_45__c,OUTPUT_VALUE_46__c,OUTPUT_VALUE_47__c,OUTPUT_VALUE_48__c,OUTPUT_VALUE_49__c,OUTPUT_VALUE_50__c FROM Account_Compute_Final__c ';            
        Database.executeBatch(obj);
    }
    Test.stopTest();*/
}

}