@isTest
private class Batch_ProductMasterMapping_Test{
    static testMethod void testMethod1() {
    String className = 'Batch_ProductMasterMapping_Test';
        
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__AccountType__c ='HCO';
        acc.AccountNumber = '12345';
        insert acc;
        
        AxtriaSalesIQTM__Organization_Master__c orgmas1 = TestDataFactory.createOrganizationMaster();
        orgmas1.Name = 'SnT';
        insert orgmas1;
        
        AxtriaSalesIQTM__Employee__c emp = TestDataFactory.createEmployee('test','test');
        insert emp;
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(); 
        country.Load_Type__c = 'Full Load';
        country.AxtriaSalesIQTM__Parent_Organization__c = orgmas1.id;
        country.AxtriaSalesIQTM__Status__c = 'Active';
        insert country;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(country);
        team.Name = 'ONCO';        
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        insert teamins;
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__Client_Position_Code__c = 'N003';
        pos.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        pos.AxtriaSalesIQTM__Effective_Start_Date__c= date.today();
        pos.AxtriaSalesIQTM__Inactive__c = false;
        pos.AxtriaSalesIQTM__Team_iD__c = team.Id;
        pos.AxtriaSalesIQTM__Employee__c = emp.id;
        //pos.AxtriaSalesIQTM__Parent_Position__c = pos1.id;
        pos.AXTRIASALESIQTM__HIERARCHY_LEVEL__C = '1';
        //pos.AxtriaSalesIQTM__Position_Status__c = 'Active';
        insert pos;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team,teamins,country);
        insert pcc;
        
        AxtriaSalesIQTM__Position_Product__c posProd =  TestDataFactory.createPositionProduct(teamins,pos,pcc);
        insert posProd;
        
        Staging_Position_Account__c stagPosAcc = new Staging_Position_Account__c ();
        stagPosAcc.Assignment_Status__c = 'Active';
        stagPosAcc.Record_Status__c = 'Updated';
        stagPosAcc.PositionCode__c = 'test';
        stagPosAcc.AccountNumber__c = 'test';
        stagPosAcc.External_Id__c = 'test';
        stagPosAcc.Account__c = acc.id;
        stagPosAcc.Account_Type__c = 'test';
        stagPosAcc.Association_Cause__c = 'test';
        stagPosAcc.Country__c = country.id;
        stagPosAcc.Position__c = pos.id;
        insert stagPosAcc;
        
        SIQ_My_Setup_Products_vod_O__c siq = new SIQ_My_Setup_Products_vod_O__c();
        //siq.Name = 'Test';
        siq.External_ID__c = '12345_CRC;HCC;DTC;RCC;GIST';
        siq.Position_Product__c = 'N003_CRC;HCC;DTC;RCC;GIST';
        siq.SIQ_Product_vod__c = 'Test';
        siq.SIQ_Country__c = 'Test';
        siq.SIQ_Employee_PRID__c = 'Test';
        siq.Status__c = 'New';
        siq.Type__c ='Test';
        siq.SIQ_Favorite_vod__c = 'TKI Portfolio_';
        insert siq;
        
        Scheduler_Log__c sclogs= new Scheduler_Log__c();
        sclogs.Cycle__c = 'Full Load';
        sclogs.Job_Name__c = 'Product';
        sclogs.Country__c = country.id;
        insert sclogs;
        
        Test.startTest();
        System.runAs(loggedInUser)
        {
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = :className];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        Batch_ProductMasterMapping o3=new Batch_ProductMasterMapping();
        Database.executeBatch(o3);
        }
        Test.stopTest();
    }

}