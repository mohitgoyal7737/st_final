@istest
public with sharing class RedirectToReportTest {
    
    static testMethod void redirecttest(){
        
        User loggedInUser = [select id,userrole.name from User where id=:UserInfo.getUserId()];
        //Report rp  = [SELECT id,Name FROM Report where Name='Call Plan Report'];
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc.AxtriaSalesIQTM__Country__c = countr.id;
        insert acc;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        insert scen;
        
        AxtriaSalesIQTM__Team_Instance__c ti = TestDataFactory.createTeamInstance(team);
        ti.AxtriaSalesIQTM__Scenario__c = scen.id;
        ti.AxtriaSalesIQTM__Team_Cycle_Name__c = 'Current';
        ti.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        insert ti;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, ti, countr);
        insert pcc;
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,ti);
        pos.Name = 'test';
        insert pos;
        AxtriaSalesIQTM__User_Access_Permission__c u = TestDataFactory.createUserAccessPerm(pos, ti, UserInfo.getUserId());
        u.AxtriaSalesIQTM__Position__c=pos.id;
        u.AxtriaSalesIQTM__Team_Instance__c = ti.id;
        u.AxtriaSalesIQTM__User__c = loggedInUser.id;        
        insert u;
        
        string loggedInPosition = 'abc';
        string reportname = 'TierByHCPPage';
        string buSelected = ti.id;
        Report_Page_Configuration__c r = new Report_Page_Configuration__c();
        r.Report_Label__c ='test';
        r.Report_Name__c ='test';
        r.Country__c = true;
        r.TeamInstance__c = true;
        r.Brand__c = true;
        r.Type__c = 'Call Plan' ;
        r.Active__c = true;
        r.Profile__c = 'System Administrator';
        insert r;
        Report_Page_Configuration__c r1 = new Report_Page_Configuration__c();
        r1.Report_Label__c ='test';
        r1.Report_Name__c ='test';
        r1.Country__c = true;
        r1.TeamInstance__c = true;
        r1.Brand__c = true;
        r1.Type__c = 'Call Plan' ;
        r1.Active__c = true;
        r1.Profile__c = 'System Administrator';
        r1.Type__c = 'Alignment' ;
        insert r1;
        Report_Page_Configuration__c r11 = new Report_Page_Configuration__c();
        r11.Report_Label__c ='test';
        r11.Report_Name__c ='test';
        r11.Country__c = true;
        r11.TeamInstance__c = true;
        r11.Brand__c = true;
        r11.Type__c = 'Roster' ;
        r11.Active__c = true;
        r11.Profile__c = 'System Administrator';
        insert r11;
        Report_Page_Configuration__c r111 = new Report_Page_Configuration__c();
        r111.Report_Label__c ='test';
        r111.Report_Name__c ='test';
        r111.Country__c = true;
        r111.TeamInstance__c = true;
        r111.Brand__c = true;
        r111.Type__c = 'System' ;
        r111.Active__c = true;
        r111.Profile__c = 'System Administrator';
        insert r111;
        
        SalesIQGlobalConstants salesiq = new SalesIQGlobalConstants();
        
        Report rep = new Report();
        //rep.Name ='Calls Across Segments by Area_HO';
        //insert rep;
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            /*
            RedirectToReport report = new RedirectToReport();
            report.reportname ='Visite per Seg, Area HO';
            PageReference pageRef = Page.Report_Page;
            Test.setCurrentPage(pageRef);
            report.reportname = 'Rep_Call_Plan_Submission_Report';
            report.disableLine();
            report.reportname = 'TierByHCPPage';
            report.disableLine();
            report.buSelected = ti.id;
            report.buSelectedName = ti.Name;
            report.buSelectedNameA = ti.Name;
            report.brandSelected= 'All Brands';
            report.brandSelected= 'All Brands';
            report.lineSelectedName = 'test';
            //report.reportname = 'Visite per Seg, Area HO';
            report.reportname = 'CenterHospitalViewPage';                                                 
            report.openReport();
            report.reportname = 'Stato_Call_Plan';                                                 
            report.openReport();
            report.reportname = 'Potential_per_HCPVisite';                                                 
            report.openReport();
            report.reportname = 'Adoption_per_HCPVisite';                                                 
            report.openReport();
            report.reportname = 'TierByHCPPage';                                                 
            report.openReport();
            report.reportname = 'potentialreport';                                                 
            report.openReport();
            report.reportname = 'CenterHospitalViewPage';                                                 
            report.openReport();
            report.loggedINuserPosition = pos.Name;
            report.gProfileName = 'System Administrator';                                                
            report.buChanged();
            report.buChangedA();
            report.brandChanged();
            report.lineChanged();
            report.gProfileName = 'HO1';
            report.lineChanged();
            report.gProfileName = 'DM1';
            report.lineChanged();
            report.gProfileName = 'RM';
            report.lineChanged();
            report.gProfileName = 'Rep1';
            report.lineChanged();
            report.selectARReports();
            report.gProfileName = 'HO1';
            report.selectARReports();
            report.selectRReports();
            report.selectSystemReports();
            */
        }
        Test.stopTest();
        
    }
    
}