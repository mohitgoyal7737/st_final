public class MetaData_Input_Ctrl {
    public list<MetaData_Input__c> datalist {get;set;}
    String Fields;
   // public list<Sobject> finaldata {get;set;}
    public list<Sobject>objdata {get;set;}
    public static list<datawrapper>datawrpr {set;get;}
    //public String Query;
    public String sObjectName {set;get;}
   // public list<String>FieldList {set;get;}
   //public string soqlObject; 
   // public String Fields {get;set;}
    
    //Public String objidd {get;set;}
   public boolean show {get;set;}
    public MetaData_Input_Ctrl(){
        datalist=new list<MetaData_Input__c>();
       // finaldata= new list<Sobject>();
        objdata = new list<Sobject>();
        
        //Query='';
        Fields = '';
        show=false; 
        
    }
    public void onload1(){
        datalist = [select id,Name,Cycle_Id__c,Cycle_Id__r.Name,Is_Active__c,Object_Name__c,Insert_Date__c,Record_Count__c,Template__c,Cycle__r.Name from MetaData_Input__c order by Insert_Date__c];
        //datalist.sort();
    }
    @RemoteAction
    public Static  datawrapper fetchdata(String obj){
        System.debug('---Fetch Data Called-------');
       // String objidd=Apexpages.currentPage().getParameters().get('objidd');
       String objidd=obj;
        //show=true;
        list<Sobject> finaldata ;
        String ObjName;
    	String Teaminst;
        String Fields;
        String Query;
        list<String>FieldList;
        set<String>Exceptset;
        List<columnwrapper> colwrpr=new list<columnwrapper>();
        system.debug('---objidd is:'+objidd);
        ObjName='';
        Teaminst='';
        Fields = '';
        Query='';
        FieldList = new list<String>();
        finaldata= new list<Sobject>();
        datawrapper datawrpr = new datawrapper();
        Exceptset=new set<String>{'CreatedById','LastModifiedBy','OwnerId','IsDeleted','LastModifiedDate','LastModifiedById','SystemModstamp','LastViewedDate','LastReferencedDate','CreatedDate','Id'};//,'Id'
        list<MetaData_Input__c> md = [select id,Object_Name__c,Cycle_Id__c,Cycle_Id__r.name from MetaData_Input__c where id=:objidd limit 1];
        if(md.size()>0){
           ObjName = md[0].Object_Name__c;
           Teaminst= md[0].Cycle_Id__r.name;
        }
        system.debug('Objname is:'+ObjName);
        system.debug('Teaminst is:'+Teaminst);
        Query='Select';
     	//system.debug('Objname is:'+sObjectName);
        List<Schema.SObjectField> sss=new list<Schema.SObjectField>();
        if(ObjName != null && ObjName != '' )
        {    
        Schema.SObjectType gd = Schema.getGlobalDescribe().get(ObjName); 
        system.debug('----gd Contains:'+gd);
        Schema.DescribeSobjectResult a11=gd.getDescribe();
        system.debug('----a11 Contains:'+a11);
        Map<String, Schema.SObjectField> M = a11.fields.getMap(); 
       // FieldList.addAll(M.keySet());
        system.debug('----M Contains:'+M);
        system.debug('----M keyset():'+M.keySet());
        system.debug('----M Values():'+M.values());
        Fields = '';
        String titl='';
        String temp='';
        for(Schema.SObjectField s1:m.values()){  
			temp=''+s1;
			system.debug('---- S1 Contains::::'+s1);
			Schema.DescribeFieldResult f = M.get(s1.getDescribe().getName()).getDescribe();
			titl= f.getLabel();
			system.debug('****Title is:'+titl);
			
			if(!Exceptset.contains(temp) ){//&& f.isCreateable() == true
				String fldtype=string.valueOf(f.getType());
				System.debug('***FieldType is::'+fldtype);
				if(fldtype!='REFERENCE'){
					Fields =  Fields+s1+',';
					colwrpr.add(new columnwrapper(titl,temp));
				}
				if(fldtype=='REFERENCE'){
					System.debug('&&&&inreference block:'+s1);
					String f1=''+s1;
					f1=f1.removeEnd('c');
					f1=f1+'r.name,';
					String f2=f1.removeEnd(',');
					System.debug('&&&&RelationShip API:'+f1);
					Fields = Fields+f1;
					colwrpr.add(new columnwrapper(titl,f2));
				}
				FieldList.add(temp);
				//colwrpr.add(new columnwrapper(titl,temp));
	      		 //colwrpr=new columnwrapper(titl,temp);
            }
             //Fields =  Fields+s1+',';
            //temp=''+s1;
            //if(temp.contains('__c')){
            //Fields =  Fields+s1+',';
            //}
            system.debug('---- F Contains:'+f);
            system.debug('------Colwrapper:inside loop:'+colwrpr);
        }
            system.debug('&&&&&Colwrapper::'+colwrpr);
            FieldList.sort();
        // Fields =Fields +' '+'Id';   
           
         Fields=Fields.removeEnd(',');   
        system.debug('---- Final Fields Are:'+Fields);
        system.debug('---- Fields List is---:'+FieldList);
        Query = Query +' '+Fields+' '+'From'+' '+ObjName;
        system.debug('---Final Query is:'+Query);
        String Q1=Query;
        Q1+=' Where Cycle_ID__c = \'' +Teaminst+'\'';
        
        system.debug('*****Q1 Query is::'+Q1);
            
            try{
                finaldata=Database.query(Q1); 
                finaldata.sort();
            }
            Catch(Exception e){
                System.debug('----Exception Caught---'+e);
            }
         system.debug('--finaldata size();'+finaldata.size());
         system.debug('--finaldata :'+finaldata);
        // system.debug('---Show value is:'+show);
            
        }
        if(finaldata.size() >0 || finaldata!=NULL){
           // datawrpr.add(new datawrapper(finaldata,colwrpr));
            datawrpr=new datawrapper(finaldata,colwrpr);
        }
        System.debug('---Datawrpr ::'+datawrpr);
        
        return datawrpr;
    
}

	@RemoteAction
	public static datawrapper fetchdata2(String obj){
		String sfdcid=obj;
		String ObjName;
    	String Teaminst;
    	String TeaminstncName;
    	list<Sobject> finaldata ;
    	List<columnwrapper> colwrpr=new list<columnwrapper>();
    	datawrapper datawrpr = new datawrapper();
    	list<MetaData_Input__c> md = [select id,Object_Name__c,Cycle_Id__c,Cycle_Id__r.name from MetaData_Input__c where id=:sfdcid limit 1];
        if(md.size()>0){
           ObjName = md[0].Object_Name__c;
           Teaminst= md[0].Cycle_Id__c;
           TeaminstncName= md[0].Cycle_ID__r.Name;
        }
        String soql = 'select ';
        String allFields ='';
        string soqlObject; 
        system.debug('Objname is:'+ObjName);
        system.debug('Teaminst is:'+Teaminst);
        List<AxtriaSalesIQTM__Team_Instance_Object_Attribute__c> fetchAllRequiredColumns;
        fetchAllRequiredColumns = [select AxtriaSalesIQTM__Attribute_API_Name__c, AxtriaSalesIQTM__Attribute_Display_Name__c, AxtriaSalesIQTM__Data_Type__c, AxtriaSalesIQTM__Display_Column_Order__c, AxtriaSalesIQTM__Object_Name__c  from AxtriaSalesIQTM__Team_Instance_Object_Attribute__c where AxtriaSalesIQTM__isRequired__c = true and AxtriaSalesIQTM__Team_Instance__c =:Teaminst and AxtriaSalesIQTM__Object_Name__c =: ObjName and AxtriaSalesIQTM__Interface_Name__c = 'Meta Data' order by AxtriaSalesIQTM__Display_Column_Order__c];
        system.debug('***fetchAllRequiredColumns:'+fetchAllRequiredColumns);
        if(fetchAllRequiredColumns != null && fetchAllRequiredColumns.size() > 0){
        	 for(AxtriaSalesIQTM__Team_Instance_Object_Attribute__c teamInstanceObj : fetchAllRequiredColumns)
            {
            	//if(teamInstanceObj.AxtriaSalesIQTM__Object_Name__c == ObjName){
	        	soql = soql + teamInstanceObj.AxtriaSalesIQTM__Attribute_API_Name__c + ', ';
	            allFields = allFields + teamInstanceObj.AxtriaSalesIQTM__Attribute_API_Name__c + ',';
	            colwrpr.add(new columnwrapper(teamInstanceObj.AxtriaSalesIQTM__Attribute_Display_Name__c,teamInstanceObj.AxtriaSalesIQTM__Attribute_API_Name__c));
            	//}
            }
        }
        system.debug('++ Hey before removing '+soql);
        soql = soql.removeEnd(', ');
	    allFields = allFields.removeEnd(',');
	    system.debug('++ Hey after removing '+soql);
	    soql = soql + ' from '+ objName + ' where  Cycle_ID__c = :TeaminstncName limit 15000';//fetchAllRequiredColumns[0].AxtriaSalesIQTM__Object_Name__c
	    soqlObject = fetchAllRequiredColumns[0].AxtriaSalesIQTM__Object_Name__c;
	    system.debug('===colwrpr is::'+colwrpr);
		system.debug('===inside fetch2 method query is::'+soql);
		 try{
                finaldata=Database.query(soql); 
                finaldata.sort();
        }
        Catch(Exception e){
            System.debug('----Exception Caught---'+e);
        }
        system.debug('--finaldata size();'+finaldata.size());
        system.debug('--finaldata :'+finaldata);
        if(finaldata.size() >0 || finaldata!=NULL){
           // datawrpr.add(new datawrapper(finaldata,colwrpr));
            datawrpr=new datawrapper(finaldata,colwrpr);
        }
        System.debug('---Datawrpr ::'+datawrpr);
        
        return datawrpr;
		
	}
    public class columnwrapper{
       public String title {get;set;}
       public String data {get;set;}
        public String defaultContent {get;set;}
        public columnwrapper(String s1,String s2){
            title=s1;
            data=s2;
            defaultContent='';
        }
        public columnwrapper(){}
        
    }
        public class datawrapper{
        public list<Sobject>Dlist {set;get;}
        public List<columnwrapper>colist {set;get;}
        public datawrapper(list<Sobject>Dlist1,list<columnwrapper> Flist1){
            Dlist=new list<Sobject>();
            colist=new list<columnwrapper>();
            Dlist=Dlist1;
            colist=Flist1;
            //System.debug('---Datawrpr.Flist ::'+Flist1);
            System.debug('---Datawrpr.Dlist ::'+Dlist1);
        }
            public datawrapper(){}
    }
}