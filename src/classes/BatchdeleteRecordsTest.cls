@isTest
public class BatchdeleteRecordsTest {
   
    
    static  testmethod void TestMovement(){
         User loggedInUser = new User(id=UserInfo.getUserId());
        AxtriaSalesIQTM__Organization_Master__c orgmas = new AxtriaSalesIQTM__Organization_Master__c();
        orgmas.Name = 'abcd';
        orgmas.AxtriaSalesIQTM__Org_Level__c = 'Global';
        orgmas.AxtriaSalesIQTM__Parent_Country_Level__c = true;
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c countr = new AxtriaSalesIQTM__Country__c();
        countr.Name = 'Spain';
        orgmas.AxtriaSalesIQTM__Org_Level__c = 'Global';
        countr.AxtriaSalesIQTM__Parent_Organization__c = orgmas.id;
        countr.AxtriaSalesIQTM__Status__c = 'Active';
        insert  countr;
        
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
        team.Name='HTN';
        team.AxtriaSalesIQTM__Country__c = countr.id;
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = new AxtriaSalesIQTM__Team_Instance__c();
        teamins.Name = 'MKT';
        teamins.AxtriaSalesIQTM__Team__c = team.id;
        insert teamins;
        
        AxtriaSalesIQTM__Team_Instance__c teamins1 = new AxtriaSalesIQTM__Team_Instance__c();
        teamins1.Name = 'MKT1';
        teamins1.AxtriaSalesIQTM__Team__c = team.id;
        insert teamins1;
        
        AxtriaSalesIQTM__Team_Instance__c teamins2 = new AxtriaSalesIQTM__Team_Instance__c();
        teamins2.Name = 'MKT2';
        teamins2.AxtriaSalesIQTM__Team__c = team.id;
        insert teamins2;
        
        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();
        pos.Name = 'dfg';
        pos.AxtriaSalesIQTM__Parent_Position__c = pos.id;
        pos.AxtriaSalesIQTM__Team_iD__c = team.id;
        pos.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        insert pos;
        
        AxtriaSalesIQTM__Position__c pos1 = new AxtriaSalesIQTM__Position__c();
        pos1.Name = 'dfgjh';
        pos1.AxtriaSalesIQTM__Parent_Position__c = pos.id;
        pos1.AxtriaSalesIQTM__Team_iD__c = team.id;
        pos1.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        insert pos1;
        
        AxtriaSalesIQTM__Employee__c emp = new AxtriaSalesIQTM__Employee__c();
        emp.Name = 'emp123';
        emp.AxtriaSalesIQTM__Country_Name__c  = countr.id;
        emp.Current_Position__c = pos.id;
        emp.AxtriaSalesIQTM__FirstName__c = 'Raju';
        emp.AxtriaSalesIQTM__Last_Name__c = 'uhj';
        insert emp;
        
        AxtriaSalesIQTM__Geography__c geo = new AxtriaSalesIQTM__Geography__c();
        geo.name = 'Test';
        insert geo;
        
        AxtriaSalesIQTM__Position_Team_Instance__c posteamins = new AxtriaSalesIQTM__Position_Team_Instance__c();
        posteamins.AxtriaSalesIQTM__Employee_ID__c = emp.id;
        posteamins.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.id;
        posteamins.AxtriaSalesIQTM__Position_ID__c = pos.id;
        posteamins.AxtriaSalesIQTM__Effective_Start_Date__c = system.today();
        posteamins.AxtriaSalesIQTM__Effective_End_Date__c = system.today() + 1;
        insert posteamins;
        
        AxtriaSalesIQTM__Position_Geography__c posgeo = new     AxtriaSalesIQTM__Position_Geography__c();
        posgeo.Name = 'abc';
        posgeo.AxtriaSalesIQTM__Effective_End_Date__c = system.today() +1;
        posgeo.AxtriaSalesIQTM__Effective_Start_Date__c = system.today();
        posgeo.AxtriaSalesIQTM__Geography__c = geo.id;
        posgeo.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        insert posgeo;

        AxtriaSalesIQTM__Position_Account__c posacc = new AxtriaSalesIQTM__Position_Account__c();
        posacc.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        insert posacc;
        system.debug('===posAcc:' + posAcc);
        
        AxtriaSalesIQTM__Position_Account__c posacc1 = new AxtriaSalesIQTM__Position_Account__c();
        posacc1.AxtriaSalesIQTM__Team_Instance__c = teamins1.id;
        insert posacc1;
        
        AxtriaSalesIQTM__Team_Instance_Object_Attribute__c tob = new AxtriaSalesIQTM__Team_Instance_Object_Attribute__c();
        tob.AxtriaSalesIQTM__Attribute_API_Name__c = 'edfgth';
        tob.AxtriaSalesIQTM__Object_Name__c = 'tgdh';
        insert tob;
        
        AxtriaSalesIQTM__Team_Instance_Object_Attribute_Detail__c   tobd  = new AxtriaSalesIQTM__Team_Instance_Object_Attribute_Detail__c();
        tobd.AxtriaSalesIQTM__Object_Attibute_Team_Instance__c = tob.id;
        tobd.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        insert tobd;
        
        AxtriaSalesIQTM__CR_Team_Instance_Config__c crt = new AxtriaSalesIQTM__CR_Team_Instance_Config__c();
        crt.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        insert crt;
        
        AxtriaSalesIQTM__CR_Approval_Config__c cra = new AxtriaSalesIQTM__CR_Approval_Config__c();
        cra.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.id;
        insert cra;
        
        AxtriaSalesIQTM__CIM_Config__c cim = new AxtriaSalesIQTM__CIM_Config__c();
        cim.AxtriaSalesIQTM__Metric_Name__c = 'abdcg';
        cim.AxtriaSalesIQTM__Attribute_API_Name__c = 'iojk';
        cim.AxtriaSalesIQTM__Object_Name__c = 'thyunk';
        insert cim;
        
        AxtriaSalesIQTM__CIM_Position_Metric_Summary__c cpms = new AxtriaSalesIQTM__CIM_Position_Metric_Summary__c();
        cpms.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        cpms.AxtriaSalesIQTM__CIM_Config__c = cim.id;
        insert cpms;
        
        AxtriaSalesIQTM__Workspace__c wo = new AxtriaSalesIQTM__Workspace__c();
        wo.AxtriaSalesIQTM__Workspace_Description__c = 'uiokl';
        wo.AxtriaSalesIQTM__Workspace_Start_Date__c = system.today();
        wo.AxtriaSalesIQTM__Workspace_End_Date__c = system.today().addDays(20);
        wo.Name = 'Test';
        insert wo;
        
        AxtriaSalesIQTM__Scenario__c sce = new  AxtriaSalesIQTM__Scenario__c();
        sce.AxtriaSalesIQTM__Workspace__c = wo.id;
        insert sce;
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            Database.executeBatch(new BatchdeleteRecords('AxtriaSalesIQTM__Position_Geography__c','AxtriaSalesIQTM__Team_Instance__c',teamins.id),200);
            Database.executeBatch(new BatchdeleteRecords('AxtriaSalesIQTM__Position_Account__c','AxtriaSalesIQTM__Team_Instance__c',teamins1.id),200);
       // Database.executeBatch(new BatchdeleteRecords('AxtriaSalesIQTM__Position__c','AxtriaSalesIQTM__Team_Instance__c',teamins2.id),200);
        //BatchdeleteRecords bdr = new 
        }
        Test.stopTest();
        
    }
}