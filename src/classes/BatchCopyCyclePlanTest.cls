/**********************************************************************************************
@author       : SnT Team
@modifiedBy   : Himanshu Tariyal (A0994)
@modifiedDate : 10th June'2020
@description  : Test class for covering BatchCopyCyclePlan class
@Revison(s)   : v1.1 Modified on 02 Feb,2021 by RT to increase the coverage and put assert statements
**********************************************************************************************/
@isTest
public class BatchCopyCyclePlanTest 
{
    @istest 
    static void BatchCopyCyclePlanTest()
    {
        String className = 'BatchCopyCyclePlanTest';
        User loggedInUser = new User(id=UserInfo.getUserId());

        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__AccountType__c ='HCP';
        SnTDMLSecurityUtil.insertRecords(acc,className);
        //insert acc;

        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);
        //insert orgmas;

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);
        //insert countr;

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'ONCO';
        SnTDMLSecurityUtil.insertRecords(team,className);
        //insert team;

        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins1,className);
        //insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        //insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        SnTDMLSecurityUtil.insertRecords(scen,className);
        //insert scen;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        SnTDMLSecurityUtil.insertRecords(teamins,className);
        //insert teamins;

        AxtriaSalesIQTM__Team_Instance__c teamins2 = TestDataFactory.createTeamInstance(team);
        teamins2.AxtriaSalesIQTM__Scenario__c = scen.id;
        SnTDMLSecurityUtil.insertRecords(teamins2,className);
        //insert teamins2;

        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        SnTDMLSecurityUtil.insertRecords(pcc,className);
        //insert pcc;
   

        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        SnTDMLSecurityUtil.insertRecords(mmc,className);
        //insert mmc;

        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        SnTDMLSecurityUtil.insertRecords(pos,className);
        //insert pos;

        AxtriaSalesIQTM__User_Access_Permission__c u = TestDataFactory.createUserAccessPerm(pos, teamins, UserInfo.getUserId());
        u.AxtriaSalesIQTM__Position__c=pos.id;
        u.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        u.AxtriaSalesIQTM__User__c = loggedInUser.id;  
        SnTDMLSecurityUtil.insertRecords(u,className);      
        //insert u;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        SnTDMLSecurityUtil.insertRecords(posAccount,className);
        //insert posAccount;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        SnTDMLSecurityUtil.insertRecords(pPriority,className);
        //insert pPriority;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        positionAccountCallPlan.AxtriaSalesIQTM__isIncludedCallPlan__c = true;
        positionAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c = true;
		
        SnTDMLSecurityUtil.insertRecords(positionAccountCallPlan,className);
		List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacpList = [Select Id,AxtriaSalesIQTM__lastApprovedTarget__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Team_Instance__c=:teamins.id];
        System.assertEquals(pacpList[0].AxtriaSalesIQTM__lastApprovedTarget__c,true);
        Test.startTest();

        System.runAs(loggedInUser)
        {
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            //copyCyclePlan obj = new copyCyclePlan(teamins.id,teamins2.id);

            String cols = 'Parent_Account__c,Previous_Segment__c,Party_ID__c,CurrencyIsoCode,Previous_Calls__c,P1__c,Adoption__c,AxtriaSalesIQTM__Metric1_Approved__c,'+
                            'Potential__c,Segment_Approved__c,Final_TCF_Approved__c,Segment1__c,Segment2__c,'+
                            'Segment3__c,Segment4__c,Segment5__c,Segment6__c,Segment7__c,Segment8__c,Segment9__c,Segment10__c,'+
                            'AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position_Team_Instance__c,AxtriaSalesIQTM__Account__c,'+
                            'AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__lastApprovedTarget__c,'+
                            'AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,'+
                            'AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Segment10__c,'+
                            'AxtriaSalesIQTM__Segment1_Approved__c,AxtriaSalesIQTM__Segment2_Approved__c,'+
                            'AxtriaSalesIQTM__Segment3_Approved__c,AxtriaSalesIQTM__Segment4_Approved__c,'+
                            'AxtriaSalesIQTM__Segment5_Approved__c,AxtriaSalesIQTM__CheckBox5_Approved__c,'+
                            'AxtriaSalesIQTM__CheckBox6_Approved__c,AxtriaSalesIQTM__CheckBox7_Approved__c,'+
                            'AxtriaSalesIQTM__CheckBox8_Approved__c,AxtriaSalesIQTM__CheckBox9_Approved__c,'+
                            'AxtriaSalesIQTM__CheckBox10_Approved__c,AxtriaSalesIQTM__Metric6_Approved__c,'+
							'AxtriaSalesIQTM__Metric2_Approved__c,AxtriaSalesIQTM__Metric3_Approved__c,'+
							'AxtriaSalesIQTM__Metric4_Approved__c,AxtriaSalesIQTM__Metric5_Approved__c,'+
                            'AxtriaSalesIQTM__Metric7_Approved__c,AxtriaSalesIQTM__Metric8_Approved__c,'+
                            'AxtriaSalesIQTM__Metric9_Approved__c,AxtriaSalesIQTM__Metric10_Approved__c';
			String loggerId = 'callPlanLoggerID';
            BatchCopyCyclePlan obj = new BatchCopyCyclePlan(teamins.id,teamins2.id,cols);
            //obj.query = 'SELECT P1__c, Adoption__c,Potential__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__Account__r.AccountNumber,Final_TCF__c,Final_TCF_Approved__c,Segment__c,Previous_Calls__c,Segment_Approved__c FROM AxtriaSalesIQTM__Position_Account_Call_Plan__c';
            obj.query =  'select '+cols+' FROM AxtriaSalesIQTM__Position_Account_Call_Plan__c where '+
                'AxtriaSalesIQTM__Team_Instance__c = :sourceTeamInst and AxtriaSalesIQTM__lastApprovedTarget__c=true ';
			Database.executeBatch(obj);
			 BatchCopyCyclePlan obj2 = new BatchCopyCyclePlan(teamins.id,teamins2.id,cols,loggerId);
            Database.executeBatch(obj2);
			
        }
        Test.stopTest();
    }
	

}