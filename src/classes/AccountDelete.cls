global class AccountDelete implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT Id,Name,First_Last_Name__c FROM Account';
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Account> scope) {
         for(Account a : scope)
         {
             a.First_Last_Name__c = a.Name;            
         }
         update scope;
    }   
    
    global void finish(Database.BatchableContext BC) {
    }
}