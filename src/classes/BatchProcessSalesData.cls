global class BatchProcessSalesData implements Database.Batchable<sObject> {
    public String query;
    public String teamInstance;
    public String marketCode;

    global BatchProcessSalesData(String teamInstance) {
        this.teamInstance = teamInstance;

        marketCode = [select AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name from AxtriaSalesIQTM__Team_Instance__c where id = :teamInstance].AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name;
        this.query = 'select SIQ_AZ_Brand_Code__c, SIQ_Product_Class_Total__c from SIQ_Sales_Data__c where SIQ_Market_Code__c = :marketCode order';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<SIQ_Sales_Data__c> scope) {
        List<SalesDataVsBR__c> salesDataBR = new List<SalesDataVsBR__c>();


        List<Integer> allPotentials = new List<Integer>();

        for(SIQ_Sales_Data__c salesData : scope)
        {
            allPotentials.add(Integer.valueof(salesData.SIQ_Product_Class_Total__c));
        }

        List<AggregateResult> sumPotential = [select count(id) countRec, SIQ_Product_Class_Total__c potential, SIQ_AZ_Brand_Code__c product from SIQ_Sales_Data__c where SIQ_Product_Class_Total__c in :allPotentials group by SIQ_AZ_Brand_Code__c , SIQ_Product_Class_Total__c];

        for(AggregateResult agg : sumPotential)
        {
            SalesDataVsBR__c sales = new SalesDataVsBR__c();
            sales.Team_Instance__c = teamInstance;
            sales.Potential__c = String.valueof(agg.get('potential'));

            sales.Product_Catalog__c = String.valueof(agg.get('product'));
           
            sales.Count_Sales__c = Integer.valueof(agg.get('countRec'));

            sales.External_ID__c = sales.Potential__c + '_' + sales.Product_Catalog__c + '_' + sales.Team_Instance__c;
            salesDataBR.add(sales);
        }

        upsert salesDataBR External_ID__c;

        /*for(SIQ_Sales_Data__c salesData : scope)
        {
            SalesDataVsBR__c sales = new SalesDataVsBR__c();
            sales.Team_Instance__c = teamInstance;
            sales.Potential__c = salesData.SIQ_AZ_Brand_Total__c;

            sdb.Product_Catalog__c = SIQ_AZ_Brand_Code__c;
           
            sdb.Count_Sales__c = Product_Class_Total__c;
            sdb.External_ID__c = sdb.Potential__c + '_' + sdb.Product_Catalog__c + '_' + sdb.Team_Instance__c;

        } */      
    }

    global void finish(Database.BatchableContext BC) {

    }
}