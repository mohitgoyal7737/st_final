global with sharing class BatchHandleSalesTeamAttribute implements Database.Batchable<sObject> {
    public String query;
    public Set<String> country;

    global BatchHandleSalesTeamAttribute(Set<String> countrySet) 
    {
        query = '';
        country=new Set<String>();
        country.addAll(countrySet);
        SnTDMLSecurityUtil.printDebugMessage('countrySet:::::::::' +countrySet);
        SnTDMLSecurityUtil.printDebugMessage('country:::::::::' +country);
        query='Select id,Name,Employee__c,Position__c,Group__c,start_date__c,end_date__c,ExternalUserID__c from UserGroups__c where Group_Type__c = \'Sales Team Attribute\' and Position__r.AxtriaSalesIQTM__IsMaster__c=true and  Position__r.AxtriaSalesIQTM__Team_iD__r.AxtriaSalesIQTM__Country__c in :country';   //Added master check

    }

    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<UserGroups__c> scope) 
    {
        SnTDMLSecurityUtil.printDebugMessage('====Query::::' +scope);

        List<UserGroups__c> delUserGrpList=new List<UserGroups__c>();
        
        for(UserGroups__c delUserGrpRec : scope)
        {
            delUserGrpRec.Event__c = 'Delete';   
            delUserGrpList.add(delUserGrpRec);
        }

        SnTDMLSecurityUtil.printDebugMessage('====delUserGrpList.size()::::' +delUserGrpList.size());

        if(delUserGrpList.size() > 0){
            //update delUserGrpList;
            SnTDMLSecurityUtil.updateRecords(delUserGrpList, 'BatchHandleSalesTeamAttribute'); 
        }
        

    }

    global void finish(Database.BatchableContext BC) 
    {
        BatchActiveUSerGroupEvent activeUserGroup = new BatchActiveUSerGroupEvent(country);
        Database.executeBatch(activeUserGroup,2000);
    }
}