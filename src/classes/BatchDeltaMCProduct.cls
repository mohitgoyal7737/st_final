global class BatchDeltaMCProduct implements DAtabase.Stateful,Database.Batchable<sObject> 
{
    public String query;
    public Datetime Lmd;
    List<String> allTeamInstances;
    List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacpProRecs;
    List<SIQ_MC_Cycle_Plan_Product_vod_O__c> mcCyclePlanProduct;
    List<String> allChannels ;
    string teamInstanceSelected;
    public List<string> allTeamInstancesinit;
    public id schedularid;
    public integer errorcount{get;set;}
       public String nonProcessedAcs {get;set;}
       public Integer recordsProcessed=0;
    public Set<String> uniqueRecs;
    public List<Segment_Veeva_Mapping__c> segmentVeevaMapping;
    public Map<String,String> segmentVeevaMappingMap;
    Set<String> activityLogIDSet;
    List<String> teamProdAccConcat;
    public Map<String,Decimal> teamSellValues;
    public Map<String,Decimal> teamSellValues1;

    global BatchDeltaMCProduct(Date lastModifiedDate,string teamInstanceSelectedTemp, List<String> allChannelsTemp)
    
    {
    }

     global BatchDeltaMCProduct(Date lastModifiedDate,List<string> teamInstanceSelectedTemp, List<String> allChannelsTemp)
    
    {
         
    }
     global BatchDeltaMCProduct(Datetime lastjobDate,string teamInstanceSelectedTemp, List<String> allChannelsTemp,string batchID)    
    {
        uniqueRecs = new Set<String>();
        system.debug('++++teamInstanceSelectedTemp'+teamInstanceSelectedTemp);
        teamInstanceSelected = teamInstanceSelectedTemp;
        //schedularid= batchID;
        system.debug('++++teamInstanceSelected'+teamInstanceSelected);
        List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
        schLogList=[Select Id,CreatedDate,Created_Date2__c from Scheduler_Log__c where Job_Name__c='MCCP Delta' and Job_Status__c='Failed'  Order By CreatedDate desc LIMIT 1];
        system.debug('schLogList'+schLogList);
         if(schLogList!=null)
         {   
            schedularid=schLogList[0].id;
         }
        system.debug('schedularid++'+schedularid);
        Lmd= lastjobDate;
        query = 'select id, Segment_Approved__c, Segment__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c, Final_TCF__c,AxtriaARSnT__Final_TCF_Approved__c,AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Account__r.AccountNumber ,  P1__c, AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Position__r.Original_Country_Code__c,Segment10__c,AxtriaSalesIQTM__lastApprovedTarget__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaARSnT__Cluster_Information__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaARSnT__Country_Veeva_Code__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where LastModifiedDate >: Lmd and AxtriaSalesIQTM__Team_Instance__c = :teamInstanceSelected and AxtriaSalesIQTM__Position__c !=null and P1__c != null AND Parent_Pacp__c != null order by AxtriaSalesIQTM__lastApprovedTarget__c desc';
        allChannels = allChannelsTemp;
    }

     global BatchDeltaMCProduct(Datetime lastjobDate,List<string> teamInstanceSelectedTemp, List<String> allChannelsTemp,string batchID)
    {
        uniqueRecs = new Set<String>();
        allTeamInstancesinit = new List<String>(teamInstanceSelectedTemp);
        system.debug('++++teamInstanceSelectedTemp'+teamInstanceSelectedTemp);
        system.debug('++++allTeamInstancesinit'+allTeamInstancesinit);
        //schedularid= batchID;
        List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
        schLogList=[Select Id,CreatedDate,Created_Date2__c from Scheduler_Log__c where Job_Name__c='MCCP Delta' and Job_Status__c='Failed'  Order By CreatedDate desc LIMIT 1];
        system.debug('schLogList'+schLogList);
         if(schLogList!=null)
         {   
            schedularid=schLogList[0].id;
         }
        system.debug('schedularid++'+schedularid);
        Lmd= lastjobDate;
        query = 'select id, Segment_Approved__c, Segment__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c, Final_TCF__c,AxtriaARSnT__Final_TCF_Approved__c,AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Account__r.AccountNumber ,  P1__c, AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Position__r.Original_Country_Code__c,Segment10__c,AxtriaSalesIQTM__lastApprovedTarget__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaARSnT__Cluster_Information__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaARSnT__Country_Veeva_Code__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where LastModifiedDate >: Lmd and AxtriaSalesIQTM__Team_Instance__c = :allTeamInstancesinit and AxtriaSalesIQTM__Position__c !=null and P1__c != null AND Parent_Pacp__c != null order by AxtriaSalesIQTM__lastApprovedTarget__c desc';
       //teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;
    }
    global BatchDeltaMCProduct(Datetime lastjobDate,List<string> teamInstanceSelectedTemp, List<String> allChannelsTemp,string batchID,Set<String> activityLogSet)
    {
        uniqueRecs = new Set<String>();
        activityLogIDSet = new Set<String>();
        activityLogIDSet.addAll(activityLogSet);
        allTeamInstancesinit = new List<String>(teamInstanceSelectedTemp);
        system.debug('++++teamInstanceSelectedTemp'+teamInstanceSelectedTemp);
        system.debug('++++allTeamInstancesinit'+allTeamInstancesinit);
        //schedularid= batchID;
        List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
        schLogList=[Select Id,CreatedDate,Created_Date2__c from Scheduler_Log__c where Job_Name__c='MCCP Delta' and Job_Status__c='Failed'  Order By CreatedDate desc LIMIT 1];
        system.debug('schLogList'+schLogList);
         if(schLogList!=null)
         {   
            schedularid=schLogList[0].id;
         }
        system.debug('schedularid++'+schedularid);
        Lmd= lastjobDate;
        query = 'select id, Segment_Approved__c, Segment__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c, Final_TCF__c,AxtriaARSnT__Final_TCF_Approved__c,AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Account__r.AccountNumber ,  P1__c, AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Position__r.Original_Country_Code__c,Segment10__c,AxtriaSalesIQTM__lastApprovedTarget__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaARSnT__Cluster_Information__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaARSnT__Country_Veeva_Code__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where LastModifiedDate >: Lmd and AxtriaSalesIQTM__Team_Instance__c = :allTeamInstancesinit and AxtriaSalesIQTM__Position__c !=null and P1__c != null AND Parent_Pacp__c != null order by AxtriaSalesIQTM__lastApprovedTarget__c desc';
       //teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;
    }
    
    global BatchDeltaMCProduct(Datetime lastjobDate,List<string> teamInstanceSelectedTemp, List<String> allChannelsTemp,string batchID,Boolean chain)
    
    {
        /*uniqueRecs = new Set<String>();
         allTeamInstancesinit = new List<String>(teamInstanceSelectedTemp);
         system.debug('++++teamInstanceSelectedTemp'+teamInstanceSelectedTemp);
         system.debug('++++allTeamInstancesinit'+allTeamInstancesinit);
         schedularid= batchID;
        Lmd= lastjobDate;
       query = 'select id, Segment__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c, Final_TCF__c,AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Account__r.AccountNumber ,  P1__c, AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Position__r.Original_Country_Code__c,Segment10__c,AxtriaSalesIQTM__lastApprovedTarget__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where LastModifiedDate >: Lmd and AxtriaSalesIQTM__Team_Instance__c = :allTeamInstancesinit and AxtriaSalesIQTM__Position__c !=null and P1__c != null AND Parent_Pacp__c != null order by AxtriaSalesIQTM__lastApprovedTarget__c desc';
       //teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;*/
    }


    public void create_MC_Cycle_Plan_Product_vod( List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scopePacpProRecs)
    {
       
       segmentVeevaMapping = new List<Segment_Veeva_Mapping__c>();
       segmentVeevaMappingMap = new Map<String, String>();

        segmentVeevaMapping = [select id, Segment_Axtria__c, Segment_Veeva__c, Team_Instance__c from Segment_Veeva_Mapping__c where Team_Instance__c in :allTeamInstancesinit];

        for(Segment_Veeva_Mapping__c svm : segmentVeevaMapping)
        {
            segmentVeevaMappingMap.put(svm.Segment_Axtria__c+ '_' + svm.Team_Instance__c, svm.Segment_Veeva__c);
        }

        allTeamInstances= new List<String>();

        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacprecords: scopePacpProRecs)
        {
            allTeamInstances.add(pacprecords.AxtriaSalesIQTM__Team_Instance__c);
        }

        system.debug('+++allTeamInstances'+allTeamInstances);
        String selectedMarket = [select AxtriaSalesIQTM__Team__r.Country_Name__c from AxtriaSalesIQTM__Team_Instance__c where id = :allTeamInstances[0]].AxtriaSalesIQTM__Team__r.Country_Name__c;
        
        List<Veeva_Market_Specific__c> veevaCriteria = [select MCCP_Target_logic__c,Channel_Criteria__c,Market__c,MC_Cycle_Threshold_Max__c,MC_Cycle_Threshold_Min__c,MC_Cycle_Channel_Record_Type__c,MC_Cycle_Plan_Channel_Record_Type__c,MC_Cycle_Plan_Product_Record_Type__c,MC_Cycle_Plan_Record_Type__c,MC_Cycle_Plan_Target_Record_Type__c,MC_Cycle_Product_Record_Type__c,MC_Cycle_Record_Type__c from Veeva_Market_Specific__c where Market__c = :selectedMarket];
        

        mcCyclePlanProduct = new List<SIQ_MC_Cycle_Plan_Product_vod_O__c>();
        //pacpProRecs = scopePacpProRecs;
        Set<String> allRecs = new Set<String>();
        Set<String> allPros = new Set<String>();
        
        
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacpPro : scopePacpProRecs)
        {
            allPros.add(pacpPro.P1__c);
        }
         system.debug('+++allPros'+allPros);
        List<Product_Catalog__c> pc = [select id, Name, Veeva_External_ID__c, Team_Instance__c from Product_Catalog__c where Name  in :allPros and Team_Instance__c = :allTeamInstances];
        Map<String, String> proToID = new Map<String,String>();
        
        for(Product_Catalog__c pcRec : pc)
        {
            proToID.put(pcRec.Name + '_'+pcRec.Team_Instance__c, pcRec.Veeva_External_ID__c);
        }
        system.debug('+++proToID'+proToID);
        
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacpPro : scopePacpProRecs)
        {
            string teamPos = pacpPro.AxtriaSalesIQTM__Team_Instance__r.Name +'_' + pacpPro.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
            string cyclePlan         =   pacpPro.AxtriaSalesIQTM__Team_Instance__r.Name +'_'+pacpPro.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
            string cyclePlanTarget   =   teamPos + '_' + pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber;
            
            String accNumPos = pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber + pacpPro.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;

            Integer f2fCalls = Integer.valueOf(pacpPro.AxtriaARSnT__Final_TCF_Approved__c);
            String channel = 'F2F';
            String productExternalID= proToID.get(pacpPro.P1__c + '_' + pacpPro.AxtriaSalesIQTM__Team_Instance__c);
            String productName = pacpPro.P1__c;

            SIQ_MC_Cycle_Plan_Product_vod_O__c mcpp = new SIQ_MC_Cycle_Plan_Product_vod_O__c();
                        
            string cycleChannel;
            
            cycleChannel      =   pacpPro.AxtriaSalesIQTM__Team_Instance__r.Name + '-' + channel + '-Call2_vod__c';
            
            mcpp.SIQ_Cycle_Plan_Channel_vod__c = teamPos+ '-'  + channel + '-' + pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber;

                if(channel == 'F2F')
                {
                    mcpp.SIQ_Product_Activity_Goal_vod__c = Decimal.valueof(f2fCalls);    
                }
           
            if(pacpPro.AxtriaSalesIQTM__lastApprovedTarget__c==true)
            {
                system.debug('++hey');
                mcpp.SIQ_Cycle_Product_vod__c = pacpPro.AxtriaSalesIQTM__Team_Instance__r.Name + '__' + channel + '__' +productExternalID + '_'+ productName;
                mcpp.SIQ_Product_Activity_Max_vod__c = mcpp.SIQ_Product_Activity_Goal_vod__c;       
                mcpp.SIQ_External_Id_vod__c = mcpp.SIQ_Cycle_Plan_Channel_vod__c + '_'+ productExternalID;
                if(segmentVeevaMappingMap.containsKey(pacpPro.Segment_Approved__c +'_'+pacpPro.AxtriaSalesIQTM__Team_Instance__c))
                {
                    mcpp.SIQ_Segment_AZ__c   = segmentVeevaMappingMap.get(pacpPro.Segment_Approved__c +'_'+pacpPro.AxtriaSalesIQTM__Team_Instance__c);
                }
                else
                {
                    mcpp.SIQ_Segment_AZ__c   = pacpPro.Segment_Approved__c;
                }

                    //mcpp.SIQ_Segment_AZ__c = pacpPro.Segment__c;
                mcpp.Team_Instance__c = pacpPro.AxtriaSalesIQTM__Team_Instance__c;
                mcpp.External_ID_Axtria__c = mcpp.SIQ_External_Id_vod__c;
                mcpp.Rec_Status__c = 'Updated';
                mcpp.Account__c = pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber;
                mcpp.Position__c = pacpPro.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
                 if(pacpPro.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaARSnT__Cluster_Information__c=='No Cluster')
                {
                    mcpp.Country__c = pacpPro.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                }
                else if(pacpPro.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaARSnT__Cluster_Information__c=='SalesIQ Cluster')
                {
                    mcpp.Country__c = pacpPro.AxtriaSalesIQTM__Position__r.AxtriaARSnT__Original_Country_Code__c;
                }
                else if(pacpPro.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaARSnT__Cluster_Information__c=='Veeva Cluster')
                {
                    mcpp.Country__c = pacpPro.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaARSnT__Country_Veeva_Code__c;
                }
                //mcpp.Country__c = pacpPro.AxtriaSalesIQTM__Position__r.Original_Country_Code__c;
                mcpp.RecordTypeId__c = veevaCriteria[0].MC_Cycle_Plan_Product_Record_Type__c;
                
                if(!allRecs.contains(mcpp.SIQ_External_Id_vod__c))
                {
                    allRecs.add(mcpp.SIQ_External_Id_vod__c);
                    mcCyclePlanProduct.add(mcpp);
                }  
                system.debug('++mcCyclePlanProduct'+mcCyclePlanProduct);     
            }     
            else
            {
                system.debug('++hey');
                Map<string,string> posaccseg = new Map<string,string>();
                string accpos= pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber+'_'+pacpPro.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_'+ pacpPro.AxtriaSalesIQTM__Team_Instance__c;
                 system.debug('+++accpos'+accpos);
                 posaccseg.put(accpos,pacpPro.Segment10__c);
                 system.debug('+++posaccseg'+posaccseg);
                 //List<String> tempList1 = (teamPos+ '-'  + channel + '-' + pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber).split('_');
                String pos = pacpPro.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
                String acc = pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber; 
                String accPosition = acc + '_' + pos + '_' + pacpPro.AxtriaSalesIQTM__Team_Instance__c;
                system.debug('+++accPosition'+accPosition);

                mcpp.Account__c = pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber;
                mcpp.Position__c = pacpPro.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
                 if(pacpPro.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaARSnT__Cluster_Information__c=='No Cluster')
                {
                    mcpp.Country__c = pacpPro.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                }
                else if(pacpPro.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaARSnT__Cluster_Information__c=='SalesIQ Cluster')
                {
                    mcpp.Country__c = pacpPro.AxtriaSalesIQTM__Position__r.AxtriaARSnT__Original_Country_Code__c;
                }
                else if(pacpPro.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaARSnT__Cluster_Information__c=='Veeva Cluster')
                {
                    mcpp.Country__c = pacpPro.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaARSnT__Country_Veeva_Code__c;
                }
               //mcpp.Country__c = pacpPro.AxtriaSalesIQTM__Position__r.Original_Country_Code__c;
                mcpp.RecordTypeId__c = veevaCriteria[0].MC_Cycle_Plan_Product_Record_Type__c;
                if(segmentVeevaMappingMap.containsKey(pacpPro.Segment_Approved__c +'_'+pacpPro.AxtriaSalesIQTM__Team_Instance__c))
                {
                    mcpp.SIQ_Segment_AZ__c   = segmentVeevaMappingMap.get(pacpPro.Segment_Approved__c +'_'+pacpPro.AxtriaSalesIQTM__Team_Instance__c);
                }
                else
                {
                    mcpp.SIQ_Segment_AZ__c   = pacpPro.Segment_Approved__c;
                }
                if(posaccseg.containsKey(accPosition))
                 {
                    if(posaccseg.get(accPosition) ==null)
                    {
                      mcpp.Rec_Status__c ='Deleted';
                    }
                    else if(posaccseg.get(accPosition) =='Inactive')
                    {
                      mcpp.Rec_Status__c ='Updated';
                    }
                    else if(posaccseg.get(accPosition) =='Merged')
                    {
                      mcpp.Rec_Status__c ='Updated'; 
                    }
                    else
                    {
                      mcpp.Rec_Status__c ='Deleted';
                    }   
                  } 
                  else
                    {
                      mcpp.Rec_Status__c ='Deleted';
                    } 
                  mcpp.External_ID_Axtria__c=   teamPos+ '-'  + channel + '-' + pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber+ '_'+ productExternalID;
                  
                  if(!allRecs.contains(mcpp.External_ID_Axtria__c))
                    {
                        allRecs.add(mcpp.External_ID_Axtria__c);
                        mcCyclePlanProduct.add(mcpp);
                    }  
                    //mcCyclePlanProduct.add(mcpp);
              }       
        }    
      upsert mcCyclePlanProduct External_ID_Axtria__c; 
    }
    

    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
      return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scopePacpProRecs) 
    {
      create_MC_Cycle_Plan_Product_vod(scopePacpProRecs);   
    }

    global void finish(Database.BatchableContext BC) 
    {
        Scheduler_Log__c sJob = new Scheduler_Log__c(id = schedularid);  
        system.debug('schedulerObj++++before'+sJob);      
        //Update the scheduler log with successful
        sJob.No_Of_Records_Processed__c=recordsProcessed;
        sjob.Object_Name__c = 'MCCP Delta';     
        sJob.Job_Status__c='Successful';           
        system.debug('sJob++++++++'+sJob);
        update sJob;
    }
}