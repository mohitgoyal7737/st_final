global class UpdateAccountAffiliation implements Database.Batchable<sObject>, Database.Stateful,schedulable{
	 public Integer recordsProcessed=0;
     public String batchID;
     global DateTime lastjobDate=null;
     global String query;
     public map<String, String> mapCountryCode ;
     public set<String> AclList {get;set;}
     public map<String,String>Accnoid {get;set;}
     //public list<AxtriaSalesIQTM__Country__c>countrylist {get;set;}
     
     global UpdateAccountAffiliation(){//set<String> Accountid
     	/*AclList.addAll(Accountid);
     	for(String S : Accountid){
     		String[] arr=s.split('_');
     		String Key = arr[0];
     		String Value=arr[1];
     		if(!Accnoid.containskey(Key)){
     			Accnoid.put(key,Value);
     		}
     	}*/
    	//mapCountryCode = new map<String, String>();
       // countrylist=[Select Id,AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c];
        //for(AxtriaSalesIQTM__Country__c c : countrylist){
        //	if(!mapCountryCode.containskey(c.AxtriaSalesIQTM__Country_Code__c)){
        //		mapCountryCode.put(c.AxtriaSalesIQTM__Country_Code__c,c.id);
        //	}
       // }
        
        /*List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
        schLogList=[Select Id,CreatedDate,Created_Date__c from Scheduler_Log__c where Job_Name__c='Affiliation Delta' and Job_Status__c='Successful' Order By Created_Date__c desc];
        if(schLogList.size()>0){
            lastjobDate=schLogList[0].CreatedDate;  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
	    else{
	    	lastjobDate=null;       //else we set the lastjobDate to null
  		}
  		System.debug('last job'+lastjobDate);
        //Last Bacth run ID
		Scheduler_Log__c sJob = new Scheduler_Log__c();
		
		sJob.Job_Name__c = 'Affiliation Delta';
		sJob.Job_Status__c = 'Failed';
		sJob.Job_Type__c='Inbound';
		//sJob.CreatedDate = System.today();
	
		insert sJob;
	    batchID = sJob.Id;
	    
	    recordsProcessed =0;
	   query = 'SELECT ConnectionReceivedId,ConnectionSentId,CreatedById,CreatedDate,CurrencyIsoCode, ' +
	    'Id,IsDeleted,LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,Name, ' +
	    'OwnerId,SIQ_Account_Id__c,SIQ_Account_Number__c,SIQ_Affiliation_End_Date__c,SIQ_Affiliation_Hierarchy__c, ' +
	    'SIQ_Affiliation_Id__c,SIQ_Affiliation_Name__c,SIQ_Affiliation_Start_Date__c,SIQ_Affiliation_Status__c, ' + 
	    'SIQ_Affiliation_Sub_Type__c,SIQ_Affiliation_Type__c,SIQ_Country_Code__c,SIQ_External_ID__c, ' +
	    'SIQ_Last_Modified_Date__c,SIQ_Marketing_Code__c,SIQ_Parent_Account_Id__c,SIQ_Parent_Account_Number__c, ' +
	    'SIQ_Primary_Affiliation_Indicator__c,SIQ_Publish_Date__c,SIQ_Publish_Event__c,SIQ_Role_Name__c,SystemModstamp ' +
	    'FROM SIQ_Account_Affiliation__c ' ;
	  
        
        if(lastjobDate!=null){
        	query = query + 'Where LastModifiedDate  >=:  lastjobDate '; 
        }
                System.debug('query'+ query);
		//Create a new record for Scheduler Batch with values, Job_Type, Job_Status__c as Failed, Created_Date__c as Today’s Date.
		
		*/
	        
    }
    
    
 	global Database.QueryLocator  start(Database.BatchableContext bc) {
         return Database.getQueryLocator(query);
    }
     public void execute(System.SchedulableContext SC){
       
    }
     global void execute(Database.BatchableContext bc, List<SIQ_Account_Affiliation__c> records){
        /*// process each batch of records
        AclList = new Set<String>();
        map<String,String>AccId = new Map<String,String>();
        for(SIQ_Account_Affiliation__c SAA : records){
            AclList.add(SAA.SIQ_Account_Number__c);
            AclList.add(SAA.SIQ_Parent_Account_Number__c);

        }
        List<Account>AccountList = [select id,External_Account_Number__c from Account where External_Account_Number__c IN :AclList]; 
        for(Account Acc : AccountList){
            if(!AccId.containsKey(Acc.External_Account_Number__c)){
                AccId.put(Acc.External_Account_Number__c,Acc.id);
            }
        }

        List<AxtriaSalesIQTM__Account_Affiliation__c> accounts = new List<AxtriaSalesIQTM__Account_Affiliation__c>();
        for (SIQ_Account_Affiliation__c acc : records) {
            if(acc.SIQ_Parent_Account_Number__c!=''){
               AxtriaSalesIQTM__Account_Affiliation__c account=new AxtriaSalesIQTM__Account_Affiliation__c();
                account.AxtriaSalesIQTM__Affiliation_Network__c='a1l5E000000851tQAA';
                if(AccId.containsKey(acc.SIQ_Account_Number__c)){
                account.AxtriaSalesIQTM__Account__c=AccId.get(acc.SIQ_Account_Number__c);
                }
                account.CurrencyIsoCode=acc.CurrencyIsoCode;
                account.Account_Number__c=acc.SIQ_Account_Number__c;
                account.Affiliation_Status__c=acc.SIQ_Affiliation_Status__c;
                account.Affiliation_Sub_Type__c=acc.SIQ_Affiliation_Sub_Type__c;
                account.Affiliation_End_Date__c=acc.SIQ_Affiliation_End_Date__c;
                account.Affiliation_Hierarchy__c=acc.SIQ_Affiliation_Hierarchy__c;
                account.Affiliation_Id__c=acc.SIQ_Affiliation_Id__c;
                account.Affiliation_Name__c=acc.SIQ_Affiliation_Name__c;
                account.Affiliation_Start_Date__c=acc.SIQ_Affiliation_Start_Date__c;
                account.Affiliation_Type__c=acc.SIQ_Affiliation_Type__c;
                account.Country_Code__c=acc.SIQ_Country_Code__c;
                account.External_Id__c=acc.SIQ_External_ID__c;
                account.Last_Modified_Date__c=acc.SIQ_Last_Modified_Date__c;
                account.Marketing_Code__c=acc.SIQ_Marketing_Code__c;
                
                if(AccId.containsKey(acc.SIQ_Account_Number__c)){
              	 account.Parent_Account_Id__c=AccId.get(acc.SIQ_Account_Number__c);
              	 account.AxtriaSalesIQTM__Parent_Account__c=AccId.get(acc.SIQ_Account_Number__c);
                }
                account.Parent_Account_Number__c=acc.SIQ_Parent_Account_Number__c;
                account.Primary_Affiliation_Indicator__c=acc.SIQ_Primary_Affiliation_Indicator__c;
                account.Publish_Date__c=acc.SIQ_Publish_Date__c;
                account.Publish_Event__c=acc.SIQ_Publish_Event__c;
              	account.Role_Name__c=acc.SIQ_Role_Name__c;
              	account.SIQ_Account_Affiliation_Id__c =acc.Name;
                	//AxtriaSalesIQTM__Account__c
               
                
            //	if(mapCountryCode.get(acc.SIQ_Country__c)!=null){
            //	account.AxtriaSalesIQTM__Country__c =mapCountryCode.get(acc.SIQ_Country__c);
            //	}
            //	else
            //	{
            //	account.AxtriaSalesIQTM__Country__c=null;
            //	}
             
                accounts.add(account);
                system.debug('recordsProcessed+'+recordsProcessed);
                recordsProcessed++;
                //comments
            }
        }
        try{
        upsert accounts;
        }
        catch(Exception ex)

		{
			recordsProcessed=0;
		}*/
        
    }    
    global void finish(Database.BatchableContext bc){
        /*// execute any post-processing operations
         System.debug(recordsProcessed + ' records processed. ');
                Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID); 
                system.debug('schedulerObj++++before'+sJob);
                //Update the scheduler log with successful
                sJob.No_Of_Records_Processed__c=recordsProcessed;
                sJob.Job_Status__c='Successful';
                system.debug('sJob++++++++'+sJob);
                update sJob;
        */
    }   
}