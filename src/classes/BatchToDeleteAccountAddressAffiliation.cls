global class BatchToDeleteAccountAddressAffiliation implements Database.Batchable<sObject>, Schedulable{
    
    public String query; 
    public String objectName;
    public Static String batchId;  
    public Static Set<String> countryCodeSet;
    public Static String deleteResult;
    public Static Map<String,Integer> deleteCounts;
    public Static  List<String> mailset;
    public BatchToDeleteAccountAddressAffiliation(){
        if(objectName == null){
            objectName = 'Account';
        }
        if(batchId == NULL){
            Scheduler_Log__c schedulerLog = new Scheduler_Log__c();
            schedulerLog.Job_Name__c = 'Delete Account, Address and Affiliation data';
            schedulerLog.Job_Status__c = 'Failed';
            schedulerLog.Job_Type__c='Delete_Staging_Acc_Add_Aff';
            schedulerLog.Created_Date2__c = DateTime.now();
            insert schedulerLog;
            batchId = schedulerLog.Id;
        }
        list<CountryName__mdt> countryCodeList = [Select Id, Label From CountryName__mdt];
        countryCodeSet = new Set<String>();
        for(CountryName__mdt cmdt : countryCodeList){
            countryCodeSet.add(cmdt.label);
        }
        
        query = 'Select Id From ' + objectName + ' where Country_Code__c IN :countryCodeSet';
        deleteResult = '';
        deleteCounts = new Map<String,Integer>();
    }

    private BatchToDeleteAccountAddressAffiliation(String objectName){
        this.objectName = objectName;
        query = 'Select Id From ' + this.objectName + ' where Country_Code__c IN :countryCodeSet';
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List<sObject> scope) {
        if(scope != null && scope.size()>0){
            List<Database.DeleteResult> result = Database.delete(scope, false);
            deleteResult += '"' + objectName + '"' + '\n';
            deleteResult += '"ID","Errors"\n';
            for(Database.DeleteResult dr : result) {
                if (!dr.isSuccess()) {
                    deleteResult += '"' + dr.id + '","' + dr.errors + '"\n';
                }
            }
        }
    }
    
    public void execute(SchedulableContext sc){
        Database.executeBatch(new BatchToDeleteAccountAddressAffiliation(), 2000);
    }
    
    public void finish(Database.BatchableContext BC) {
        if(objectName == 'Account') {
            BatchToDeleteAccountAddressAffiliation obj = new BatchToDeleteAccountAddressAffiliation('AxtriaARSnt__Account_Address__c');
            Database.executeBatch(obj, 2000);
        }else if(objectName == 'AxtriaARSnt__Account_Address__c') {
            BatchToDeleteAccountAddressAffiliation obj1 = new BatchToDeleteAccountAddressAffiliation('AxtriaARSnt__Account_Affiliation__c');
            Database.executeBatch(obj1, 2000);
        }else if(objectName == 'AxtriaARSnt__Account_Affiliation__c') {
            Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID); 
            //sJob.No_Of_Records_Processed__c=recordsProcessed;
            sJob.Job_Status__c = 'Successful';
            update sJob;


        list<mailreceipent__mdt> mailist = [Select Id, Label From mailreceipent__mdt];
        mailset = new list<String>();
        for(mailreceipent__mdt maildt : mailist){
            mailset.add(maildt.label);
        }
            
            
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String bodyMessage = '';
       

            mail.setSubject('Job_Name__c :: ' + objectName + ' Run Successful');
            mail.setToAddresses(mailset);
            mail.setSenderDisplayName('Job_Name__c : ' + objectName + ' Run Successful');
            bodyMessage  = 'Deleted Account_Address_Affiliation for ' + countryCodeSet  ;
            //String csvRow = '"TerritoryName","PreviousCount","CurrentCount","%Change","Limit"\n';
            Messaging.Emailfileattachment attachment = new Messaging.Emailfileattachment();
            attachment.setFileName('DeleteRecords.csv');
            blob excelData = blob.valueOf(deleteResult);
            attachment.setBody(excelData);
            System.debug('bodyMessage is ' + bodyMessage);
            mail.setHtmlBody(bodyMessage);
            List<Messaging.Emailfileattachment> attachmentList = new List<Messaging.Emailfileattachment>();
            attachmentList.add(attachment);
            mail.setFileAttachments(attachmentList);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            
        }
    }
}