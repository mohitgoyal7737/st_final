global with sharing class PositionProductDirectLoad implements Database.Batchable<sObject>, Database.Stateful
{
    public String query;
    public String selectedTeamInstance;
    Map<String, Id> teamInstanceNametoIDMap;
    Map<String, string> teamInstanceNametoTeamNameMap;
    public  List<temp_Obj__c> posp;
    String Ids;
    public integer recordsProcessed;
    public integer recordsTotal;
    Boolean flag = true;
    public String selectedTeam;
    public String teamInstanceName1;
    public Date startDate;
    public Date endDate;
    public String teamInsName;
    public boolean fullload {get; set;}
    Set<String> allproduct;
    Set<String> positionset;
    Set<String> teamProductPositionSet;
    List<AxtriaSalesIQTM__CIM_Config__c> insertlist;

    //Added by HT(A0994) on 17th June 2020
    global String changeReqID;
    global Boolean flagValue = false;

    global PositionProductDirectLoad(string teaminstance)
    {
        recordsProcessed = 0;
        recordsTotal = 0;
        teamInstanceNametoIDMap = new Map<String, Id>();
        teamInstanceNametoTeamNameMap = new Map<String, String>();
        selectedTeamInstance = teamInstance;
        //this.Ids = Ids;

        for(AxtriaSalesIQTM__Team_Instance__c teamIns : [Select Id, Name, AxtriaSalesIQTM__IC_EffstartDate__c, AxtriaSalesIQTM__IC_EffendDate__c, AxtriaSalesIQTM__Team__r.name From AxtriaSalesIQTM__Team_Instance__c WHERE ID = :teamInstance])
        {
            teamInsName = teamIns.Name;
            teamInstanceNametoIDMap.put(teamIns.Name, teamIns.Id);
            teamInstanceNametoTeamNameMap.put(teamIns.Name, teamIns.AxtriaSalesIQTM__Team__r.name);
            startDate = teamIns.AxtriaSalesIQTM__IC_EffstartDate__c;
            endDate = teamIns.AxtriaSalesIQTM__IC_EffendDate__c;
        }
        system.debug('++++teamInsName++' + teamInsName);
        system.debug('++++teamInstanceNametoIDMap++' + teamInstanceNametoIDMap);
        system.debug('++++teamInstanceNametoTeamNameMap++' + teamInstanceNametoTeamNameMap);

        selectedTeam = teamInstanceNametoTeamNameMap.get(teamInsName);
        system.debug('++++selectedTeam++' + selectedTeam);


        //query = 'Select id,Position_Code__c, Product_Code__c,Product_Name__c,Team_Instance__c,Holidays__c,Other_Days_Off__c,Vacation_Days__c,Status__c,Calls_Per_Days__c From Staging_Position_Product__c  where Team_Instance__c = \'' + teamInsName + '\' and Status__c = \'New\'';
        // chenge to temp_Obj__c by Mayank Pathak on 19/03/2020
        query = 'Select id,Position_Code__c, Product_Code__c,Product_Name__c,Team_Instance_Name__c,Holidays__c,Other_Days_Off__c,Vacation_Days__c,Status__c,Calls_Per_Days__c,Change_Request__c, isError__c, SalesIQ_Error_Message__c, Error_message__c From temp_Obj__c  where Team_Instance_Name__c = \'' + teamInsName + '\' and Status__c = \'New\' and Object__c = \'Staging_Position_Product__c\'  ';
    }

    global PositionProductDirectLoad(string teaminstance, String Ids)
    {
        recordsProcessed = 0;
        recordsTotal = 0;
        teamInstanceNametoIDMap = new Map<String, Id>();
        teamInstanceNametoTeamNameMap = new Map<String, String>();
        selectedTeamInstance = teamInstance;
        this.Ids = Ids;

        for(AxtriaSalesIQTM__Team_Instance__c teamIns : [Select Id, Name, AxtriaSalesIQTM__IC_EffstartDate__c, AxtriaSalesIQTM__IC_EffendDate__c, AxtriaSalesIQTM__Team__r.name From AxtriaSalesIQTM__Team_Instance__c WHERE ID = :teamInstance])
        {
            teamInsName = teamIns.Name;
            teamInstanceNametoIDMap.put(teamIns.Name, teamIns.Id);
            teamInstanceNametoTeamNameMap.put(teamIns.Name, teamIns.AxtriaSalesIQTM__Team__r.name);
            startDate = teamIns.AxtriaSalesIQTM__IC_EffstartDate__c;
            endDate = teamIns.AxtriaSalesIQTM__IC_EffendDate__c;
        }
        system.debug('++++teamInsName++' + teamInsName);
        system.debug('++++teamInstanceNametoIDMap++' + teamInstanceNametoIDMap);
        system.debug('++++teamInstanceNametoTeamNameMap++' + teamInstanceNametoTeamNameMap);

        selectedTeam = teamInstanceNametoTeamNameMap.get(teamInsName);
        system.debug('++++selectedTeam++' + selectedTeam);

        //query = 'Select id,Position_Code__c, Product_Code__c,Product_Name__c,Team_Instance__c,Holidays__c,Other_Days_Off__c,Vacation_Days__c,Status__c,Calls_Per_Days__c From Staging_Position_Product__c  where Team_Instance__c = \'' + teamInsName + '\' and Status__c = \'New\'';
        // chenge to temp_Obj__c by Mayank Pathak on 19/03/2020
        query = 'Select id,Position_Code__c, Product_Code__c,Product_Name__c,Team_Instance_Name__c,Holidays__c,Other_Days_Off__c,Vacation_Days__c,Status__c,Calls_Per_Days__c,Change_Request__c, isError__c, SalesIQ_Error_Message__c, Error_message__c From temp_Obj__c  where Team_Instance_Name__c = \'' + teamInsName + '\' and Status__c = \'New\' and Object__c = \'Staging_Position_Product__c\' and Team_Instance_Name__c!=null and Product_Name__c!=null and Position_Code__c!=null and Product_Code__c!=null ';
    }

    //Added by HT(A0994) on 17th June 2020
    global PositionProductDirectLoad(string teaminstance, String Ids,Boolean flag)
    {
        recordsProcessed = 0;
        recordsTotal = 0;
        teamInstanceNametoIDMap = new Map<String, Id>();
        teamInstanceNametoTeamNameMap = new Map<String, String>();
        selectedTeamInstance = teamInstance;
        this.Ids = Ids;
        changeReqID = Ids;
        flagValue = flag;

        for(AxtriaSalesIQTM__Team_Instance__c teamIns : [Select Id, Name, AxtriaSalesIQTM__IC_EffstartDate__c, AxtriaSalesIQTM__IC_EffendDate__c, AxtriaSalesIQTM__Team__r.name From AxtriaSalesIQTM__Team_Instance__c WHERE ID = :teamInstance WITH SECURITY_ENFORCED])
        {
            teamInsName = teamIns.Name;
            teamInstanceNametoIDMap.put(teamIns.Name, teamIns.Id);
            teamInstanceNametoTeamNameMap.put(teamIns.Name, teamIns.AxtriaSalesIQTM__Team__r.name);
            startDate = teamIns.AxtriaSalesIQTM__IC_EffstartDate__c;
            endDate = teamIns.AxtriaSalesIQTM__IC_EffendDate__c;
        }
        system.debug('++++teamInsName++' + teamInsName);
        system.debug('++++teamInstanceNametoIDMap++' + teamInstanceNametoIDMap);
        system.debug('++++teamInstanceNametoTeamNameMap++' + teamInstanceNametoTeamNameMap);

        selectedTeam = teamInstanceNametoTeamNameMap.get(teamInsName);
        system.debug('++++selectedTeam++' + selectedTeam);

        //query = 'Select id,Position_Code__c, Product_Code__c,Product_Name__c,Team_Instance__c,Holidays__c,Other_Days_Off__c,Vacation_Days__c,Status__c,Calls_Per_Days__c From Staging_Position_Product__c  where Team_Instance__c = \'' + teamInsName + '\' and Status__c = \'New\'';
        // chenge to temp_Obj__c by Mayank Pathak on 19/03/2020
        query = 'Select id,Position_Code__c, Product_Code__c,Product_Name__c,Team_Instance_Name__c,'+
                'Holidays__c,Other_Days_Off__c,Vacation_Days__c,Status__c,Calls_Per_Days__c,'+
                'Change_Request__c, isError__c, SalesIQ_Error_Message__c, Error_message__c '+
                'From temp_Obj__c  where Team_Instance_Name__c = \'' + teamInsName + '\' and '+
                'Status__c = \'New\' and Object__c = \'Staging_Position_Product__c\' and '+
                'Team_Instance_Name__c!=null and Product_Name__c!=null and Position_Code__c!=null '+
                'and Product_Code__c!=null and Change_Request__c =:changeReqID WITH SECURITY_ENFORCED';
    }

    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<temp_Obj__c> posprod)
    {

        List<String> product = new List<String>();
        insertlist = new List<AxtriaSalesIQTM__CIM_Config__c>();
        List<String> allposition = new List<String>();
        List<AxtriaSalesIQTM__Position_Product__c> posToInsert = new List<AxtriaSalesIQTM__Position_Product__c>();
        Map<String, Decimal> TeamInstanceProductIDtoHolidaysMap = new Map<String, Decimal>();
        Map<String, Decimal> TeamInstanceProductIDtoVacationDaysMap = new Map<String, Decimal>();
        Map<String, Decimal> TeamInstanceProductIDtoOtherDaysOffMap = new Map<String, Decimal>();
        Map<String, Decimal> TeamInstanceProductIDtoCallsperDayMap = new Map<String, Decimal>();
        Map<String, Id> mapproducttoID = new Map<String, Id>();
        Map<String, string> mapproducttoName = new Map<String, string>();
        Map<String, Id> mappostoId = new Map<String, Id>();
        allproduct = new Set<String>();
        positionset = new Set<String>();
        teamProductPositionSet = new Set<String>();
        recordsTotal += posprod.size();
        //set for position and product
        for(temp_Obj__c stagPosProd : posprod)
        {
            allproduct.add(stagPosProd.Product_Name__c);
            positionset.add(stagPosProd.Position_Code__c);
        }

        //query for products
        List<Product_Catalog__c> prodC = [SELECT Id, Veeva_External_ID__c, Product_Code__c, Name, Team_Instance__c FROM Product_Catalog__c where Team_Instance__c = :selectedTeamInstance and IsActive__c = true];
        
        /*List<AxtriaSalesIQTM__Team_Instance__c> teamrec = [Select id,IsHCOSegmentationEnabled__c,Customer_Types__c from AxtriaSalesIQTM__Team_Instance__c where id=:selectedTeamInstance];
        
        String custtype = teamrec[0].Customer_Types__c;
        List<String> listcusttype = custtype.split(';');
        System.debug(listcusttype);*/
        
        for(Product_Catalog__c pro : prodC)
        {
            product.add(pro.Veeva_External_ID__c);
            mapproducttoID.put(pro.Veeva_External_ID__c, pro.id);
            mapproducttoName.put(pro.Veeva_External_ID__c,pro.Name);
        }
        system.debug('++++product++' + product);
        system.debug('++++mapproducttoID++' + mapproducttoID);

        //query for positions
        List<AxtriaSalesIQTM__Position__c> position = [Select id, AxtriaSalesIQTM__Client_Position_Code__c, Name from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_Instance__c = :selectedTeamInstance];
        for(AxtriaSalesIQTM__Position__c pos : position)
        {
            allposition.add(pos.AxtriaSalesIQTM__Client_Position_Code__c);
            mappostoId.put(pos.AxtriaSalesIQTM__Client_Position_Code__c, pos.id);
        }
        system.debug('++++allposition++' + allposition);
        system.debug('++++mappostoId++' + mappostoId);
        //query for team instance product
        List<Team_Instance_Product_AZ__c> allposproducts = [SELECT id, Effective_Days_in_Field_Formula__c, Vacation_Days__c, Other_Days_Off__c, Product_Catalogue__c, Product_Catalogue__r.Name, Holidays__c, Team_instance__c, Calls_Day__c FROM Team_Instance_Product_AZ__c where Team_instance__c = :selectedTeamInstance];
        for(Team_Instance_Product_AZ__c tipAZ : allposproducts)
        {
            string teamInstance = String.valueOf(tipAZ.Team_Instance__c);
            teamInstanceName1 = teamInstance;
            TeamInstanceProductIDtoHolidaysMap.put(teamInstance + tipAZ.Product_Catalogue__r.Name, tipAZ.Holidays__c);
            TeamInstanceProductIDtoVacationDaysMap.put(teamInstance + tipAZ.Product_Catalogue__r.Name, tipAZ.Vacation_Days__c);
            TeamInstanceProductIDtoOtherDaysOffMap.put(teamInstance + tipAZ.Product_Catalogue__r.Name, tipAZ.Other_Days_Off__c);
            TeamInstanceProductIDtoCallsperDayMap.put(teamInstance + tipAZ.Product_Catalogue__r.Name, tipAZ.Calls_Day__c);
        }
        system.debug('++++TeamInstanceProductIDtoHolidaysMap++' + TeamInstanceProductIDtoHolidaysMap);
        system.debug('++++teamInstanceName1++' + teamInstanceName1);

        //query for position product
        List<AxtriaSalesIQTM__Position_Product__c> positionProduct1 = [SELECT id, Product_Catalog__r.name, Product_Catalog__r.Product_Code__c, AxtriaSalesIQTM__Position__r.name, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Team_Instance__r.name, Product_Catalog__c from AxtriaSalesIQTM__Position_Product__c where AxtriaSalesIQTM__Team_Instance__c = : selectedTeamInstance and Product_Catalog__r.name in:allproduct and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c in:positionset ];

        Map<String, String> posProdRecsMap = new Map<String, String>();

        for(AxtriaSalesIQTM__Position_Product__c positionProductSet : positionProduct1)
        {
            String key = positionProductSet.AxtriaSalesIQTM__Team_Instance__c + '_' + positionProductSet.AxtriaSalesIQTM__Position__c + '_' + positionProductSet.Product_Catalog__c;

            posProdRecsMap.put(key, positionProductSet.ID);

            teamProductPositionSet.add(positionProductSet.AxtriaSalesIQTM__Team_Instance__c + '_' + positionProductSet.AxtriaSalesIQTM__Position__c + '_' + positionProductSet.Product_Catalog__c);
        }
        
        Map<String,String> cimmap = new Map<String,String>();
        
        List<AxtriaSalesIQTM__Change_Request_Type__c> crrequest = [Select id,AxtriaSalesIQTM__CR_Type_Name__c,AxtriaSalesIQTM__Change_Request_Code__c from AxtriaSalesIQTM__Change_Request_Type__c where AxtriaSalesIQTM__Change_Request_Code__c = 'Call_Plan_Change'];
         
         /*List<AxtriaSalesIQTM__CIM_Config__c> existingrec = [Select id,AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c,AxtriaSalesIQTM__Team_Instance__c,isCallCapacity__c from AxtriaSalesIQTM__CIM_Config__c where AxtriaSalesIQTM__Team_Instance__c=:selectedTeamInstance and isCallCapacity__c = true and AxtriaSalesIQTM__Enable__c = true];
         
         for(AxtriaSalesIQTM__CIM_Config__c cm : existingrec)
         {
            if(cm.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c.contains('P1__c=\''))
            {
                String prodname = '';
                prodname = cm.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c.substringafter('P1__c = \'');
                prodname = prodname.substringBefore('\'');
                System.debug('check prodname'+prodname);
                cimmap.put(cm.AxtriaSalesIQTM__Team_Instance__c+'_'+prodname,cm.AxtriaSalesIQTM__Team_Instance__c); 
            }
         }*/
        
        Set<String> updatedteamProductPositionSet = new Set<String>();
        for(temp_Obj__c stag : posprod)
        {
            if(String.isNotBlank(stag.Team_Instance_Name__c) && String.isNotBlank(stag.Product_Name__c) && String.isNotBlank(stag.Position_Code__c) && String.isNotBlank(stag.Product_Code__c)){
                if(product.contains(stag.Product_Code__c) && allposition.contains(stag.Position_Code__c)) {

                    string key = teamInstanceNametoIDMap.get(stag.Team_Instance_Name__c) + '_' + mappostoId.get(stag.Position_Code__c) + '_' + mapproducttoID.get(stag.Product_Code__c);
                    system.debug('++key++' + key);

                    AxtriaSalesIQTM__Position_Product__c positionProduct;

                    if(!teamProductPositionSet.contains(key))
                    {
                        positionProduct = new AxtriaSalesIQTM__Position_Product__c();
                    }
                    else
                    {
                        positionProduct = new AxtriaSalesIQTM__Position_Product__c(id = posProdRecsMap.get(key));
                    }

                    positionProduct.AxtriaSalesIQTM__Position__c = mappostoId.get(stag.Position_Code__c);
                    System.debug('2222222222');
                    System.debug('teamInstanceNametoIDMap.get(stag.Team_Instance_Name__c)  :: ' + teamInstanceNametoIDMap.get(stag.Team_Instance_Name__c));
                    positionProduct.AxtriaSalesIQTM__Team_Instance__c = teamInstanceNametoIDMap.get(stag.Team_Instance_Name__c);
                    System.debug('1111111111111');
                    positionProduct.Product_Catalog__c = mapproducttoID.get(stag.Product_Code__c);
                    positionProduct.AxtriaSalesIQTM__Effective_Start_Date__c = startDate;
                    positionProduct.AxtriaSalesIQTM__Effective_End_Date__c = endDate;
                    positionProduct.AxtriaSalesIQTM__isActive__c = true;
                    positionProduct.AxtriaSalesIQTM__Product_Weight__c = 0;

                    //Added by Prince
                    positionProduct.External_ID__c = mappostoId.get(stag.Position_Code__c) + '_' + mapproducttoID.get(stag.Product_Code__c);

                    positionProduct.Vacation_Days__c =  stag.Vacation_Days__c != null ?  stag.Vacation_Days__c : TeamInstanceProductIDtoVacationDaysMap.get(teamInstanceName1 + stag.Product_Name__c);
                    positionProduct.Holidays__c = stag.Holidays__c != null ? stag.Holidays__c :  TeamInstanceProductIDtoHolidaysMap.get(teamInstanceName1 + stag.Product_Name__c);
                    //positionProduct.Holidays__c=TeamInstanceProductIDtoHolidaysMap.get(teamInstanceName1+stag.Product_Name__c);
                    positionProduct.Other_Days_Off__c = stag.Other_Days_Off__c != null ? stag.Other_Days_Off__c : TeamInstanceProductIDtoOtherDaysOffMap.get(teamInstanceName1 + stag.Product_Name__c);
                    // positionProduct.Other_Days_Off__c=TeamInstanceProductIDtoOtherDaysOffMap.get(teamInstanceName1+stag.Product_Name__c);
                    // positionProduct.Calls_Day__c=TeamInstanceProductIDtoCallsperDayMap.get(teamInstanceName1+stag.Product_Name__c);
                    positionProduct.Calls_Day__c = stag.Calls_Per_Days__c != null ? stag.Calls_Per_Days__c : TeamInstanceProductIDtoCallsperDayMap.get(teamInstanceName1 + stag.Product_Name__c);

                    positionProduct.External_ID__c = positionProduct.AxtriaSalesIQTM__Position__c + '_' + positionProduct.Product_Catalog__c;
                    posToInsert.add(positionProduct);
                    stag.Status__c = 'Processed';
                    stag.Change_Request__c = Ids;//A1422
                    stag.isError__c = false;//A1422
                    teamProductPositionSet.add(key);
                    
                   /* if(!cimmap.containsKey(teamInstanceNametoIDMap.get(stag.Team_Instance_Name__c)+'_'+stag.Product_Name__c))
                    {
                        if(teamrec[0].IsHCOSegmentationEnabled__c)
                        {
                            for(integer i= 0; i<listcusttype.size();i++)
                            {
                            AxtriaSalesIQTM__CIM_Config__c cmrec = new AxtriaSalesIQTM__CIM_Config__c();
                            cmrec.isCallCapacity__c = true;
                            cmrec.AxtriaSalesIQTM__Team_Instance__c = selectedTeamInstance;
                            cmrec.Name = 'Call Capacity'+' '+mapproducttoName.get(stag.Product_Code__c);
                            cmrec.AxtriaSalesIQTM__Change_Request_Type__c = crrequest[0].id;
                            cmrec.AxtriaSalesIQTM__Attribute_API_Name__c = 'Final_TCF__c';
                            cmrec.AxtriaSalesIQTM__Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
                            cmrec.AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c = 'AxtriaSalesIQTM__Position_Team_Instance__c';
                            cmrec.AxtriaSalesIQTM__Aggregation_Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
                            cmrec.AxtriaSalesIQTM__Aggregation_Type__c = 'SUM';
                            cmrec.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c = 'AxtriaSalesIQTM__isincludedCallPlan__c = true and P1__c = \''+mapproducttoName.get(stag.Product_Code__c)+'\'';
                            cmrec.AxtriaSalesIQTM__Metric_Name__c = 'Call Capacity'+' '+mapproducttoName.get(stag.Product_Code__c);
                            cmrec.AxtriaSalesIQTM__Attribute_Display_Name__c = 'Call Capacity'+' '+mapproducttoName.get(stag.Product_Code__c);
                            cmrec.AxtriaSalesIQTM__Display_Name__c = 'Call Capacity'+' '+mapproducttoName.get(stag.Product_Code__c);
                            cmrec.AxtriaSalesIQTM__Visible__c = true;
                            cmrec.AxtriaSalesIQTM__Enable__c = true;
                            cmrec.Cust_Type__c = listcusttype[i];
                            cmrec.AxtriaSalesIQTM__MetricCalculationType__c = 'Percentage';
                            cmrec.AxtriaSalesIQTM__Threshold_Min__c = '-20';
                            cmrec.AxtriaSalesIQTM__Threshold_Max__c = '20';
                            cmrec.AxtriaSalesIQTM__Threshold_Warning_Min__c = '-15';
                            cmrec.AxtriaSalesIQTM__Threshold_Warning_Max__c = '15';
                            cmrec.Display_Original_Value__c = false;
                            insertlist.add(cmrec);
                            String prodname1 = '';
                        prodname1 = cmrec.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c.substringafter('P1__c = \'');
                        prodname1 = prodname1.substringBefore('\'');
                        System.debug('check prodname after-->'+prodname1);
                        cimmap.put(cmrec.AxtriaSalesIQTM__Team_Instance__c+'_'+prodname1,cmrec.AxtriaSalesIQTM__Team_Instance__c); 
                            }
                        }
                        else
                        {
                            AxtriaSalesIQTM__CIM_Config__c cmrec = new AxtriaSalesIQTM__CIM_Config__c();
                            cmrec.isCallCapacity__c = true;
                            cmrec.AxtriaSalesIQTM__Team_Instance__c = selectedTeamInstance;
                            cmrec.Name = 'Call Capacity'+' '+mapproducttoName.get(stag.Product_Code__c);
                            cmrec.AxtriaSalesIQTM__Change_Request_Type__c = crrequest[0].id;
                            cmrec.AxtriaSalesIQTM__Attribute_API_Name__c = 'Final_TCF__c';
                            cmrec.AxtriaSalesIQTM__Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
                            cmrec.AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c = 'AxtriaSalesIQTM__Position_Team_Instance__c';
                            cmrec.AxtriaSalesIQTM__Aggregation_Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
                            cmrec.AxtriaSalesIQTM__Aggregation_Type__c = 'SUM';
                            cmrec.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c = 'AxtriaSalesIQTM__isincludedCallPlan__c = true and P1__c = \''+mapproducttoName.get(stag.Product_Code__c)+'\'';
                            cmrec.AxtriaSalesIQTM__Metric_Name__c = 'Call Capacity'+' '+mapproducttoName.get(stag.Product_Code__c);
                            cmrec.AxtriaSalesIQTM__Attribute_Display_Name__c = 'Call Capacity'+' '+mapproducttoName.get(stag.Product_Code__c);
                            cmrec.AxtriaSalesIQTM__Display_Name__c = 'Call Capacity'+' '+mapproducttoName.get(stag.Product_Code__c);
                            cmrec.AxtriaSalesIQTM__Visible__c = true;
                            cmrec.AxtriaSalesIQTM__Enable__c = true;
                            cmrec.AxtriaSalesIQTM__MetricCalculationType__c = 'Percentage';
                            cmrec.AxtriaSalesIQTM__Threshold_Min__c = '-20';
                            cmrec.AxtriaSalesIQTM__Threshold_Max__c = '20';
                            //cmrec1.Cust_Type__c = 'HCO';
                            cmrec.AxtriaSalesIQTM__Threshold_Warning_Min__c = '-15';
                            cmrec.AxtriaSalesIQTM__Threshold_Warning_Max__c = '15';
                            cmrec.Display_Original_Value__c = false;
                            insertlist.add(cmrec);
                            String prodname1 = '';
                        prodname1 = cmrec.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c.substringafter('P1__c = \'');
                        prodname1 = prodname1.substringBefore('\'');
                        System.debug('check prodname after-->'+prodname1);
                        cimmap.put(cmrec.AxtriaSalesIQTM__Team_Instance__c+'_'+prodname1,cmrec.AxtriaSalesIQTM__Team_Instance__c); 
                        }
                        
                        
                    }*/
                    
                }
                else
                {
                    stag.Status__c = 'Rejected';
                    stag.SalesIQ_Error_Message__c = 'Product doesnot exist or position doesnot exist';
                    stag.Change_Request__c = Ids;//A1422
                    stag.isError__c = true;//A1422
                }
            } else{
                stag.Status__c = 'Rejected';
                stag.SalesIQ_Error_Message__c = 'Mandatory Fields are Missing';
                stag.Change_Request__c = Ids;
                stag.isError__c = true;
            }

        }



        if(!posToInsert.isEmpty())
        {
            //upsert posToInsert id;
            //List<AxtriaSalesIQTM__Position_Product__c> posToInserttemp = new List<AxtriaSalesIQTM__Position_Product__c>();
            //database.UpsertResult[] ds = Database.upsert(posToInsert, AxtriaSalesIQTM__Position_Product__c.id, false);

            SObjectAccessDecision securityDecision = Security.stripInaccessible(AccessType.UPSERTABLE, posToInsert);
            //upsert securityDecision.getRecords() External_ID__c;
            List<AxtriaSalesIQTM__Position_Product__c> temp = securityDecision.getRecords();
            database.UpsertResult[] ds = Database.upsert(temp, AxtriaSalesIQTM__Position_Product__c.id, false);

        //throw exception if permission is missing
            if(!securityDecision.getRemovedFields().isEmpty() )
            {
                if(posToInsert[0].id != null)
                    SnTDMLSecurityUtil.printDMLMessage(securityDecision, 'PositionProductDirectLoad', 'C' );
                else
                    SnTDMLSecurityUtil.printDMLMessage(securityDecision, 'PositionProductDirectLoad', 'U' );
            }



            for(database.UpsertResult d : ds)
            {
                if(d.issuccess())
                {
                    recordsProcessed++;
                }
                else
                {
                    flag = false;
                }
            }
            
            //List<AxtriaSalesIQTM__Position_Product__c> afterinsertlist = [Select id,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Product__c,Product_Catalog__c,Product_Catalog__r.Name,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position_Product__c where id in : securityDecision.getRecords().id];
            
        }
       /* if(!insertlist.isEmpty())
        {
            SnTDMLSecurityUtil.insertRecords(insertlist, 'PositionProductDirectLoad');
        }*/
        //update posprod;
        SnTDMLSecurityUtil.updateRecords(posprod, 'PositionProductDirectLoad');

    }

    global void finish(Database.BatchableContext BC)
    {
        Boolean noJobErrors;
        String changeReqStatus;

        if(Ids != null)
        {
            AxtriaSalesIQTM__Change_Request__c changerequest = [select AxtriaSalesIQTM__Request_Type_Change__c,Job_Status__c, Records_Updated__c, Records_Failed__c from AxtriaSalesIQTM__Change_Request__c where id = : Ids];
            changerequest.Id = ids;
            noJobErrors = ST_Utility.getJobStatus(BC.getJobId());
            changeReqStatus = flag && noJobErrors ? 'Done' : 'Error';

            /*if(flag && ST_Utility.getJobStatus(BC.getJobId()))
            {
                changerequest.Job_Status__c = 'Done';
            }
            else
            {
                changerequest.Job_Status__c = 'Error';
            }*/
            changerequest.AxtriaSalesIQTM__Team_Instance_ID__c = selectedTeamInstance;
            changerequest.Records_Updated__c = recordsProcessed;
            if(changerequest.AxtriaSalesIQTM__Request_Type_Change__c == 'Data Load Backend')
                changerequest.Records_Created__c = recordsTotal;
            SnTDMLSecurityUtil.updateRecords(changerequest, 'PositionProductDirectLoad');

            BatchUpdateTempObjRecsCR batchCall = new BatchUpdateTempObjRecsCR(Ids,true,'Mandatory Field Missing or Incorrect Team Instance',changeReqStatus);
            Database.executeBatch(batchCall,2000);
            //Direct_Load_Records_Status_Update.Direct_Load_Records_Status_Update(Ids,'PositionProductDirectLoad');
            //update changerequest;
        }
    }
}