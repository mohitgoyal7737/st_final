global with sharing class UserReport {
    public list<AxtriaSalesIQTM__Organization_Master__c>Omlist ;
    public list<GroupMember>Grouplist ;
    public set<string>useridlist ;
    public set<string>useridlist2 ;
    public list<User_Type__c>usertypelist ;
    public set<string>groupnames ;
    public list<User>userlist;
    public list<User>userlist2;
    public set<string>prids ;
    public list<AxtriaSalesIQTM__Position_Employee__c>posemp;
    public map<string,StandardPositions__c>standardpositions ;
    public set<string>positionlist ;
    public set<string>topnodeorgmster ;

    global UserReport() {
        Omlist = new list<AxtriaSalesIQTM__Organization_Master__c>();
        Grouplist = new list<GroupMember>();
        useridlist = new set<string>();
        useridlist2 = new set<string>();
        usertypelist = new list<User_Type__c>();
        groupnames = new set<string>();
        userlist = new list<user>();
        userlist2 = new list<user>();
        prids = new set<string>();
        posemp = new list<AxtriaSalesIQTM__Position_Employee__c>();
        standardpositions = new map<string,StandardPositions__c>();
        positionlist = new set<string>();
        topnodeorgmster = new set<string>();
        fetchdata();
    }

    global void fetchdata(){
        system.debug('================ENTERED Fetchdata Method()=========');
        Omlist = [select id,Marketing_Code__c,AxtriaSalesIQTM__Org_Level__c,AxtriaSalesIQTM__Parent_Country_Level__c,AxtriaSalesIQTM__Parent_Organization_Name__c,Public_Group__c,Public_Group_link__c,Public_Group_SFDC_id__c from AxtriaSalesIQTM__Organization_Master__c ];

        if(Omlist !=null && Omlist.size()>0){
            for(AxtriaSalesIQTM__Organization_Master__c org : Omlist){
                if(org.Public_Group_SFDC_id__c !=null && org.AxtriaSalesIQTM__Parent_Organization_Name__c!=null){
                    groupnames.add(org.Public_Group_SFDC_id__c);
                }
                if(org.Public_Group_SFDC_id__c !=null && org.AxtriaSalesIQTM__Parent_Organization_Name__c==null){
                    topnodeorgmster.add(org.Public_Group_SFDC_id__c);
                }
            }
        }

        system.debug('=========topnodeorgmster::'+topnodeorgmster);
        system.debug('=========topnodeorgmster.Size()::'+topnodeorgmster.size());
        
        if(topnodeorgmster!=null && topnodeorgmster.size() >0){
            Grouplist = [SELECT GroupId,Id,UserOrGroupId FROM GroupMember where GroupId IN : topnodeorgmster];
            if(Grouplist !=null && Grouplist.size() >0){
                for(GroupMember grp : Grouplist){
                    useridlist.add(grp.UserOrGroupId);
                }
            }
        }

        system.debug('=======Grouplist::'+Grouplist.size());
        system.debug('=======useridlist::'+useridlist.size());
        if(useridlist !=null && useridlist.size() >0){
            userlist = [select id,Name,Country,FederationIdentifier  from user where id IN : useridlist];
            system.debug('userlist======'+userlist);
            system.debug('=======userlist.size():'+userlist.size());
            if(userlist!=null && userlist.size()>0){
                for(user u :userlist){
                    if(u.FederationIdentifier !=null){
                        User_Type__c newutype = new User_Type__c();
                        newutype.Type__c = 'Standard User';
                        newutype.Name__c = u.Name;
                        newutype.Country__c = u.Country;
                        newutype.PRID__c= u.FederationIdentifier;
                        usertypelist.add(newutype);
                        prids.add(u.FederationIdentifier);
                    }
                }
            }
            system.debug('=========groupnames::'+groupnames);
            system.debug('=========groupnames.Size()::'+groupnames.size());
            list<Groupmember>Grouplist2= new list<Groupmember>();
            //list<user>useridlist2 = new list<user>();
            if(groupnames!=null && groupnames.size() >0){
                Grouplist2 = [SELECT GroupId,Id,UserOrGroupId FROM GroupMember where GroupId IN : groupnames];
                if(Grouplist2 !=null && Grouplist2.size() >0){
                    for(GroupMember grp : Grouplist2){
                        useridlist2.add(grp.UserOrGroupId);
                    }
                }
            }
            system.debug('=======Grouplist2::'+Grouplist.size());
            system.debug('=======useridlist2::'+useridlist2.size());
            if(useridlist2 !=null && useridlist2.size() >0){
                userlist2 = [select id,Name,Country,FederationIdentifier  from user where id IN : useridlist2];
                system.debug('userlist2======'+userlist2);
                system.debug('=======userlist2.size():'+userlist2.size());
                if(userlist2!=null && userlist2.size()>0){
                    for(user u :userlist2){
                        if(u.FederationIdentifier !=null){
                            User_Type__c newutype = new User_Type__c();
                            newutype.Type__c = 'CRM User';
                            newutype.Name__c = u.Name;
                            newutype.Country__c = u.Country;
                            newutype.PRID__c= u.FederationIdentifier;
                            usertypelist.add(newutype);
                            prids.add(u.FederationIdentifier);
                        }
                    }
                }
            }
            
            system.debug('==========usertypelist::'+usertypelist.size());
            if(usertypelist !=null && usertypelist.size()>0){
                upsert usertypelist PRID__c;
            }
            
        
            
        }

        /* ************************STAND ORGANISATION MASTER END********************/

        /* ************************Identifying the user type from position employee********************/
        set<string>hierarchyset = new set<string>();
        standardpositions = StandardPositions__c.getAll();
        if(standardpositions !=null && standardpositions.size() >0){
            //positionlist.addAll(standardpositions.keyset());
            for(StandardPositions__c sp: standardpositions.values()){
                if(sp.Type__c =='CRM')
                    hierarchyset.add(sp.Name);
                if(sp.Type__c =='Standard')
                    positionlist.add((sp.Name).toUpperCase());
            }
            
        }
        
        system.debug('=========positionlist:'+positionlist);
        system.debug('=========positionlist.Size():'+positionlist.size());
        system.debug('============hierarchyset::'+hierarchyset);
        
        /*hierarchyset.add('REP');
        hierarchyset.add('FSLM');
        hierarchyset.add('USER');
        //hierarchyset.add('NATION');
        hierarchyset.add('Territory');
        hierarchyset.add('District');
        //hierarchyset.add('REGION');
        //hierarchyset.add('REP');
        */
        set<string>crmprids = new set<string>();
        set<string>standardprids = new set<string>();
        posemp = [SELECT AxtriaSalesIQTM__Employee__c,AxtriaSalesIQTM__Employee__r.Name,AxtriaSalesIQTM__Employee__r.Employee_PRID__c,AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Country_Name__r.AxtriaSalesIQTM__Country_Code__c,AxtriaSalesIQTM__Position__r.Name,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c,Country_Code__c,Employee_ID1__c,Employee_ID__c,Id,Territory_ID__c FROM AxtriaSalesIQTM__Position_Employee__c ];   

        system.debug('===============posemp Size is:'+posemp.size());
        usertypelist = new list<User_Type__c>();
        set<string>priduniverse = new set<string>();
        for(AxtriaSalesIQTM__Position_Employee__c pemp : posemp ){
            if(!crmprids.contains(pemp.AxtriaSalesIQTM__Employee__r.Employee_PRID__c)){
                if(!positionlist.contains(pemp.AxtriaSalesIQTM__Position__r.Name) && !positionlist.contains(pemp.Territory_ID__c)){
                    User_Type__c newutype = new User_Type__c();
                    newutype.Type__c = 'CRM User';
                    newutype.Name__c = pemp.AxtriaSalesIQTM__Employee__r.Name;
                    newutype.Country__c = pemp.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Country_Name__r.AxtriaSalesIQTM__Country_Code__c;
                    newutype.PRID__c= pemp.AxtriaSalesIQTM__Employee__r.Employee_PRID__c;
                    usertypelist.add(newutype);
                    crmprids.add(pemp.AxtriaSalesIQTM__Employee__r.Employee_PRID__c);
                }
            }
        }
        

    /*  for(AxtriaSalesIQTM__Position_Employee__c pemp : posemp ){
            if(!priduniverse.contains(pemp.AxtriaSalesIQTM__Employee__r.Employee_PRID__c)){
                if(positionlist.contains(pemp.AxtriaSalesIQTM__Position__r.Name) && !prids.contains(pemp.AxtriaSalesIQTM__Employee__r.Employee_PRID__c)){
                    system.debug('============1St POS EMP STANDARD USER LOOP====='+pemp.AxtriaSalesIQTM__Employee__r.Employee_PRID__c);
                    if(pemp.AxtriaSalesIQTM__Employee__r.Employee_PRID__c !=null){  
                        User_Type__c newutype = new User_Type__c();
                        newutype.Type__c = 'Standard User';
                        newutype.Name__c = pemp.AxtriaSalesIQTM__Employee__r.Name;
                        newutype.Country__c = pemp.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Country_Name__r.AxtriaSalesIQTM__Country_Code__c;
                        newutype.PRID__c= pemp.AxtriaSalesIQTM__Employee__r.Employee_PRID__c;
                        usertypelist.add(newutype);
                        prids.add(pemp.AxtriaSalesIQTM__Employee__r.Employee_PRID__c);
                    }
     
                }
                else if(hierarchyset.contains(pemp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c) && !crmprids.contains(pemp.AxtriaSalesIQTM__Employee__r.Employee_PRID__c)){
                            system.debug('============CRM USER LOOP====='+pemp.AxtriaSalesIQTM__Employee__r.Employee_PRID__c);
                            if(pemp.AxtriaSalesIQTM__Employee__r.Employee_PRID__c !=null){  
                                User_Type__c newutype = new User_Type__c();
                                newutype.Type__c = 'CRM User';
                                newutype.Name__c = pemp.AxtriaSalesIQTM__Employee__r.Name;
                                newutype.Country__c = pemp.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Country_Name__r.AxtriaSalesIQTM__Country_Code__c;
                                newutype.PRID__c= pemp.AxtriaSalesIQTM__Employee__r.Employee_PRID__c;
                                usertypelist.add(newutype);
                                crmprids.add(pemp.AxtriaSalesIQTM__Employee__r.Employee_PRID__c);
                            }
                        }

                    //}
                else{
                        if(!standardprids.contains(pemp.AxtriaSalesIQTM__Employee__r.Employee_PRID__c)){
                            system.debug('============2St POS EMP STANDARD USER LOOP====='+pemp.AxtriaSalesIQTM__Employee__r.Employee_PRID__c);
                            if(pemp.AxtriaSalesIQTM__Employee__r.Employee_PRID__c !=null){  
                                User_Type__c newutype = new User_Type__c();
                                newutype.Type__c = 'Standard User';
                                newutype.Name__c = pemp.AxtriaSalesIQTM__Employee__r.Name;
                                newutype.Country__c = pemp.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Country_Name__r.AxtriaSalesIQTM__Country_Code__c;
                                newutype.PRID__c= pemp.AxtriaSalesIQTM__Employee__r.Employee_PRID__c;
                                usertypelist.add(newutype);
                                standardprids.add(pemp.AxtriaSalesIQTM__Employee__r.Employee_PRID__c);
                            }
                        }
                }
            } 
            priduniverse.add(pemp.AxtriaSalesIQTM__Employee__r.Employee_PRID__c);       
        }

*/
        system.debug('=============usertypelist.size():::'+usertypelist.size());
        system.debug('=============usertypelist recs :::'+usertypelist);
        if(usertypelist!=null && usertypelist.size() >0){
            upsert usertypelist PRID__c;
        }
    }
}