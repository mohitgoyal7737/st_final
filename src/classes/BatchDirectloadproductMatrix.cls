global class BatchDirectloadproductMatrix implements Database.Batchable<sObject> {
   
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT Id,Name,Account_ID__c,Scenario_Name__c,AxtriaARSnT__POSITION__c,AxtriaARSnT__Objective__c,AxtriaARSnT__Target__c,Product_Name__c,Workspace__c,Adoption__c,Potential__c,Product_ID__c,Segment__c FROM AxtriaARSnT__Direct_Load_Product_Matrix__c where Status__c = \'New\'';
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<AxtriaARSnT__Direct_Load_Product_Matrix__c> scope) {
        
        Set<String> workSpace_set = new Set<String>();
        Set<String> workSpace_ProductID = new Set<String>();
        List<AxtriaARSnT__Direct_Load_Product_Matrix__c> updateScope = new List<AxtriaARSnT__Direct_Load_Product_Matrix__c>();

        for(AxtriaARSnT__Direct_Load_Product_Matrix__c work : scope){
            workSpace_set.add(work.Workspace__c);
            workSpace_ProductID.add(work.Product_ID__c);
        }
        
        List<AxtriaSalesIQTM__Workspace__c> partworkspacelist = [select Id,Name,Countrycode__c from AxtriaSalesIQTM__Workspace__c where Name in: workSpace_set];
        Map<String,String> workspacemap = new Map<String,String>(); 
     
        for(AxtriaSalesIQTM__Workspace__c WS : partworkspacelist){
            workspacemap.put(WS.Name,WS.Countrycode__c);
        }
    
         List<ProductToDetailGroup__c> partproductdetailslist = [select Id,Veeva_External_ID__c,Detail_Group__c from ProductToDetailGroup__c where Veeva_External_ID__c in: workSpace_ProductID];
         Map<String,String> productdetailsmap = new Map<String,String >(); 
     
        for(ProductToDetailGroup__c pdrec : partproductdetailslist){
            productdetailsmap.put(pdrec.Veeva_External_ID__c,pdrec.Detail_Group__c);
        }
    
    
        List<SIQ_Product_Metrics_vod_O__c> partOrderList = New List<SIQ_Product_Metrics_vod_O__c>();
           
        for(AxtriaARSnT__Direct_Load_Product_Matrix__c fixNon_Target : scope){ 
    
             SIQ_Product_Metrics_vod_O__c part_ProductMatrix = New SIQ_Product_Metrics_vod_O__c();
             part_ProductMatrix.Name = fixNon_Target.Name;
             part_ProductMatrix.SIQ_Account_vod__c = fixNon_Target.Account_ID__c;
             part_ProductMatrix.SIQ_Account_Number__c= fixNon_Target.Account_ID__c;
             part_ProductMatrix.SIQ_Adoption_AZ__c = fixNon_Target.Adoption__c;
             part_ProductMatrix.SIQ_Potential_AZ__c = fixNon_Target.Potential__c;
             part_ProductMatrix.SIQ_Products_vod__c = fixNon_Target.Product_ID__c;
             part_ProductMatrix.SIQ_Segment__c = fixNon_Target.Segment__c;
             part_ProductMatrix.SIQ_Country__c = workspacemap.get(fixNon_Target.Workspace__c);
             part_ProductMatrix.SIQ_Product_Name__c = fixNon_Target.Product_Name__c;
             part_ProductMatrix.Detail_Group__c=productdetailsmap.get(fixNon_Target.Product_ID__c);
             part_ProductMatrix.SIQ_Status__c='Updated';
             part_ProductMatrix.SIQ_External_ID__c=workspacemap.get(fixNon_Target.Workspace__c) + '_' + fixNon_Target.Account_ID__c + '_' + fixNon_Target.Product_ID__c + '_' + productdetailsmap.get(fixNon_Target.Product_ID__c);
             partOrderList.add(part_ProductMatrix);
        }

        if(partOrderList.size() > 0)
           upsert partOrderList part_ProductMatrix.SIQ_External_ID__c;

       for(AxtriaARSnT__Direct_Load_Product_Matrix__c inputRec : scope)
       {
          inputRec.Status__c = 'Processed';
          updateScope.add(inputRec);
       }
       if(partOrderList.size() > 0)
           update updateScope;
  }  
    
    global void finish(Database.BatchableContext BC) {
     //database.executeBatch(new batchCustmoreSegmentOutboundload(), 2000);// commented since batchCustmoreSegmentOutboundload has been deprecated
    }
}