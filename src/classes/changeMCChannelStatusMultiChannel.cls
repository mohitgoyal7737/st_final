global class changeMCChannelStatusMultiChannel implements Database.Batchable<sObject> {
    

    List<String> allChannels;
    String teamInstanceSelected;
    String queryString;
    List<String> allTeamInstances;
    public Boolean flag = true;
    public Date lmd;
    
    //List<Parent_PACP__c> pacpRecs;
    
    global changeMCChannelStatusMultiChannel(string teamInstanceSelectedTemp, List<String> allChannelsTemp,Boolean check)
    { 

       queryString = 'select id, Rec_Status__c from SIQ_MC_Cycle_Plan_Channel_vod_O__c where Team_Instance__c = :teamInstanceSelected';

        teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;
         flag=check;
    }
    
    global changeMCChannelStatusMultiChannel(List<String> teamInstanceSelectedTemp, List<String> allChannelsTemp,Boolean check)
    { 

       queryString = 'select id, Rec_Status__c from SIQ_MC_Cycle_Plan_Channel_vod_O__c where Team_Instance__c in :allTeamInstances';
       allTeamInstances = new List<String>(teamInstanceSelectedTemp);

        //teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;
         flag=check;
    }
    global changeMCChannelStatusMultiChannel(List<String> teamInstanceSelectedTemp, List<String> allChannelsTemp)
    { 

       queryString = 'select id, Rec_Status__c from SIQ_MC_Cycle_Plan_Channel_vod_O__c where Team_Instance__c in :allTeamInstances';
       allTeamInstances = new List<String>(teamInstanceSelectedTemp);

        //teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;
         //flag=check;
    }

    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        return Database.getQueryLocator(queryString);
    }
    
    
    global void execute(Database.BatchableContext BC, List<SIQ_MC_Cycle_Plan_Channel_vod_O__c> scopePacpProRecs)
    {
        for(SIQ_MC_Cycle_Plan_Channel_vod_O__c mcTarget : scopePacpProRecs)
        {
            mcTarget.Rec_Status__c = '';
        }
        
        update scopePacpProRecs;

    }

    global void finish(Database.BatchableContext BC)
    {
        if(flag=true)
        {
        AZ_Integration_New_Utility_MP_EU_Multi u1 = new AZ_Integration_New_Utility_MP_EU_Multi(allTeamInstances, allChannels);
        
        Database.executeBatch(u1, 2000);
        //Database.executeBatch(new MarkMCCPtargetDeleted());
        }
        else
        {
            lmd=Date.Today();
            BatchDeltaMCChannelStatus_MultiChannel u2 = new BatchDeltaMCChannelStatus_MultiChannel(lmd,allTeamInstances, allChannels);//new BatchDeltaMCChannelStatus(lmd);
            database.executeBatch(u2,2000);
        }

    }
}