global class batchAccountUpdate implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT Id,Name, AxtriaSalesIQTM__FirstName__c FROM Account';
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Account> scope) {
         for(Account a : scope)
         {
             a.AxtriaSalesIQTM__FirstName__c = a.Name;            
         }
         update scope;
    }   
    
    global void finish(Database.BatchableContext BC) {
    }
}