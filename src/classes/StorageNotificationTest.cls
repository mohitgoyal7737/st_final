@isTest 
global class StorageNotificationTest {

    static testMethod void testAlert(){
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
     
            Test.startTest();
                StorageNotification obj=new StorageNotification();
                String sch = '0 0 23 * * ?'; system.schedule('Test StorageNotification Check', sch, obj); 
                StorageNotification.SendAlerts();
            Test.stopTest();
    }
    
    
    static testMethod void testAlert1(){
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        Logger_Email__c objLoggerEmail = new Logger_Email__c(Name='Test@gmail.com',Type__c='StorageNotification');
        insert objLoggerEmail ;
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
     
            Test.startTest();
                StorageNotification obj=new StorageNotification();
                String sch = '0 0 23 * * ?'; system.schedule('Test StorageNotification Check', sch, obj); 
                StorageNotification.SendAlerts();
            Test.stopTest();
    }
    }