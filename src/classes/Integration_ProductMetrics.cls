public with sharing class Integration_ProductMetrics {
    
    public Integration_ProductMetrics() 
    {
    	List<String> allCountries = new List<String>();
    	Database.executeBatch(new Batch_Integration_ProductMetrics(allCountries)); 
    }
}