global with sharing class Populate_RuleType implements Database.Batchable<sObject>, Database.Stateful {
    public String query;
    public Set<String> setTDDeassignID;
    public Set<String> setBUDeassignID;
    public Boolean flag{get;set;}
    public String countryID;
    public String countryName;

    global Populate_RuleType(Boolean flagg, String country) {
        query = '';
        countryID='';
        countryName='';
        flag=flagg;
        countryID=country;
        SnTDMLSecurityUtil.printDebugMessage('====countryID:::::::' +countryID);
        SnTDMLSecurityUtil.printDebugMessage('====flag:::::::' +flag);
        setTDDeassignID=new Set<String>();
        setBUDeassignID=new Set<String>();
        //query='select id,Account__c,Position__c,Team_Instance__c,Status__c,Account_Type__c,Rule_Type__c,Country_Name__c from Deassign_Postiton_Account__c where Status__c=\'New\'';
        // chenge to temp_Obj__c by Mayank Pathak on 19/03/2020
        query='select id,Account_Text__c,Position_Text__c,Team_Instance_Text__c,Status__c,Account_Type__c,Rule_Type__c,Country__c from temp_Obj__c where Status__c=\'New\' and Object__c = \'Deassign_Postiton_Account__c\'  ';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<sObject> scope) {
        
        SnTDMLSecurityUtil.printDebugMessage('====Query:::::::' +scope);

        Set<String> setAccount=new Set<String>();
        Set<String> setTeamIns=new Set<String>();
        Set<String> setScenario=new Set<String>();
        Map<String,String> mapAccNum2AccType=new Map<String,String>();
        Map<String,String> mapTeamIns2Scenario=new Map<String,String>();
        Map<String,String> mapScenario2RuleType=new Map<String,String>();
        Map<String,String> mapTeamIns2RuleType=new Map<String,String>();
        Map<String,String> mapTeamIns2Country=new Map<String,String>();
        List<temp_Obj__c> deassignList=new List<temp_Obj__c>();

        List<AxtriaSalesIQTM__Country__c> countryList=[Select Id,Name from AxtriaSalesIQTM__Country__c where id = :countryID];
        countryName=countryList[0].Name;
        SnTDMLSecurityUtil.printDebugMessage('====countryName:::::::' +countryName);

        for(SObject deassignRec : scope)
        {
            setTeamIns.add((String)deassignRec.get('Team_Instance_Text__c'));
            setAccount.add((String)deassignRec.get('Account_Text__c'));
        }

        SnTDMLSecurityUtil.printDebugMessage('====setTeamIns:::::::' +setTeamIns);
        SnTDMLSecurityUtil.printDebugMessage('====setAccount:::::::' +setAccount);

        List<Account> accList = [select Id,AccountNumber,Type from Account where AccountNumber in :setAccount and AccountNumber != null and AxtriaSalesIQTM__Active__c='Active' and Type!=null];

        for(Account acc : accList)
        {
            mapAccNum2AccType.put(acc.AccountNumber,acc.Type);
        }

        SnTDMLSecurityUtil.printDebugMessage('====mapAccNum2AccType:::::::' +mapAccNum2AccType);

        List<AxtriaSalesIQTM__Team_Instance__c> teamInsList = [select Id,Name,AxtriaSalesIQTM__Scenario__c,Country_Name__c from AxtriaSalesIQTM__Team_Instance__c where Name in :setTeamIns and AxtriaSalesIQTM__Scenario__c != null];

        for(AxtriaSalesIQTM__Team_Instance__c tiRec : teamInsList)
        {
            mapTeamIns2Scenario.put(tiRec.Name,tiRec.AxtriaSalesIQTM__Scenario__c);
            mapTeamIns2Country.put(tiRec.Name,tiRec.Country_Name__c);
            setScenario.add(tiRec.AxtriaSalesIQTM__Scenario__c);
        }

        SnTDMLSecurityUtil.printDebugMessage('====mapTeamIns2Scenario:::::::' +mapTeamIns2Scenario);
        SnTDMLSecurityUtil.printDebugMessage('====setScenario:::::::' +setScenario);

        List<AxtriaSalesIQTM__Business_Rules__c> bussRuleList = [select Id, AxtriaSalesIQTM__Rule_Type__c,AxtriaSalesIQTM__Scenario__c from AxtriaSalesIQTM__Business_Rules__c where AxtriaSalesIQTM__Scenario__c in :setScenario and AxtriaSalesIQTM__Rule_Type__c!=null];

         for(AxtriaSalesIQTM__Business_Rules__c busRule : bussRuleList)
         {
            mapScenario2RuleType.put(busRule.AxtriaSalesIQTM__Scenario__c,busRule.AxtriaSalesIQTM__Rule_Type__c);
         }

         SnTDMLSecurityUtil.printDebugMessage('====mapScenario2RuleType:::::::' +mapScenario2RuleType);

         for(String teamIns : mapTeamIns2Scenario.keySet())
         {
            if(mapTeamIns2Scenario.get(teamIns) != null)
            {
                String scenario = mapTeamIns2Scenario.get(teamIns);
                SnTDMLSecurityUtil.printDebugMessage('====scenario:::::::' +scenario);
                String ruleType=mapScenario2RuleType.get(scenario);
                SnTDMLSecurityUtil.printDebugMessage('====ruleType:::::::' +ruleType);
                mapTeamIns2RuleType.put(teamIns,ruleType);
            }
         }
         SnTDMLSecurityUtil.printDebugMessage('====mapTeamIns2RuleType:::::::' +mapTeamIns2RuleType);

         for(sObject deassignRec : scope)
         {
            /*deassignRec.Account_Type__c=mapAccNum2AccType.get(deassignRec.Account_Text__c);
            deassignRec.Rule_Type__c=mapTeamIns2RuleType.get(deassignRec.Team_Instance_Text__c);
            deassignRec.Country_Name__c=mapTeamIns2Country.get(deassignRec.Team_Instance_Text__c);
*/
            deassignRec.put('Account_Type__c',mapAccNum2AccType.get((String)deassignRec.get('Account_Text__c')));
            deassignRec.put('Rule_Type__c',mapTeamIns2RuleType.get((String)deassignRec.get('Team_Instance_Text__c')));
            deassignRec.put('Country_Name__c',mapTeamIns2Country.get((String)deassignRec.get('Team_Instance_Text__c')));
            //setDeassignID.add(deassignRec.Id);
            deassignList.add((temp_Obj__c)deassignRec);
            SnTDMLSecurityUtil.printDebugMessage('====deassignRec:::::::' +deassignRec);
         }

         SnTDMLSecurityUtil.printDebugMessage('====deassignList.size():::::::' +deassignList.size());

         if(deassignList.size() > 0){
            //update deassignList;
            SnTDMLSecurityUtil.updateRecords(deassignList, 'Populate_RuleType');
        }

        for(temp_Obj__c deassignRec : deassignList)
        {
            if(deassignRec.Rule_Type__c == 'Top Down')
            {
                setTDDeassignID.add(deassignRec.Id);
            }
            if(deassignRec.Rule_Type__c == 'Bottom Up')
            {
                setBUDeassignID.add(deassignRec.Id);
            }
        }
        SnTDMLSecurityUtil.printDebugMessage('====setTDDeassignID:::::::' +setTDDeassignID);
        SnTDMLSecurityUtil.printDebugMessage('====setBUDeassignID:::::::' +setBUDeassignID);
    }

    global void finish(Database.BatchableContext BC) {

        
        if(setTDDeassignID != null)
        {
            SnTDMLSecurityUtil.printDebugMessage('=========Calling Deassignment Position Account Batch for Top Down Rule Type:::::::::::::::::::::::::::');
            Deassign_PositionAccount_2 posAcc = new Deassign_PositionAccount_2(setTDDeassignID,flag,countryName);
            Database.executeBatch(posAcc,10);
        }

        if(setBUDeassignID != null)
        {
            SnTDMLSecurityUtil.printDebugMessage('=========Calling Deassignment Position Account Batch for Bottom Up Rule Type:::::::::::::::::::::::::::');
            DeassignBottomUpPA buPosAcc = new DeassignBottomUpPA(setBUDeassignID,flag,countryName);
            Database.executeBatch(buPosAcc,10);
        }
    }

}