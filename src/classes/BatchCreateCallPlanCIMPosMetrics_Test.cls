@isTest
private class BatchCreateCallPlanCIMPosMetrics_Test { 
    
    
    static testMethod void testMethod1() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        //User loggedInUser = [select id,userrole.name from User where id=:UserInfo.getUserId()];
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc.AccountNumber = 'BH11810999';
        acc.External_Account_Number__c = 'BH11810999';
        acc.AxtriaSalesIQTM__Country__c = countr.id;
        insert acc;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        insert scen;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.AxtriaSalesIQTM__Team_Cycle_Name__c = 'Current';
        //teamins.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        insert teamins;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        
        AxtriaSalesIQTM__Position_Team_Instance__c posteamins = TestDataFactory.createPositionTeamInstance(pos.id, null, teamins.id);
        insert posteamins;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        positionAccountCallPlan.AxtriaSalesIQTM__Position_Team_Instance__c = posteamins.id;
        positionAccountCallPlan.AxtriaSalesIQTM__isIncludedCallPlan__c = true;
        //positionAccountCallPlan.Account_HCP_HCO_Number__c = 'BH11810999_';
        insert positionAccountCallPlan;
        AxtriaSalesIQTM__CIM_Config__c cimcc = TestDataFactory.createCIMConfig(teamins);
        cimcc.name = 'Total Targets Test';
        cimcc.AxtriaSalesIQTM__Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
        cimcc.AxtriaSalesIQTM__Attribute_API_Name__c = 'Account_HCP_HCO_Number__c';
        cimcc.AxtriaSalesIQTM__Aggregation_Type__c = 'COUNT_DISTINCT';
        cimcc.AxtriaSalesIQTM__Aggregation_Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
        //cimcc.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c = null;
        cimcc.AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c = 'AxtriaSalesIQTM__Position_Team_Instance__c';//
        cimcc.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c = 'AxtriaSalesIQTM__isincludedCallPlan__c=true';//
        //cimcc.AxtriaSalesIQTM__Aggregation_Condition_Attribute_Value__c = ' ';
        cimcc.AxtriaSalesIQTM__team_instance__c = teamins.id;
        cimcc.AxtriaSalesIQTM__Enable__c = true ;
        insert cimcc;
        AxtriaSalesIQTM__CIM_Position_Metric_Summary__c cimconfigsum = TestDataFactory.createCIMPosMetSum(cimcc,posteamins,teamins);
        insert cimconfigsum;
        
        List<String> list_cim = new List<String>();
        list_cim.add(cimcc.id);
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            
            BatchCreateCallPlanCIMPosMetrics obj=new BatchCreateCallPlanCIMPosMetrics(list_cim,teamins.Id);
            String objectToBeQueried = cimcc.AxtriaSalesIQTM__Attribute_API_Name__c;
            String aggregationAttributeAPIName = cimcc.AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c;
            String aggregationObjectName = cimcc.AxtriaSalesIQTM__Aggregation_Object_Name__c;
            obj.theQuery = 'select ' + cimcc.AxtriaSalesIQTM__Attribute_API_Name__c + ', ' + cimcc.AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c + ' from ' + cimcc.AxtriaSalesIQTM__Aggregation_Object_Name__c;
            
            Database.executeBatch(obj);
            
        }
        Test.stopTest();
    }
    static testMethod void testMethod2() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc.AccountNumber = 'BH11810999';
        acc.External_Account_Number__c = 'BH11810999';
        acc.AxtriaSalesIQTM__Country__c = countr.id;
        insert acc;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        insert scen;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.AxtriaSalesIQTM__Team_Cycle_Name__c = 'Current';
        //teamins.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        insert teamins;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        
        AxtriaSalesIQTM__Position_Team_Instance__c posteamins = TestDataFactory.createPositionTeamInstance(pos.id, null, teamins.id);
        insert posteamins;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        //positionAccountCallPlan.AxtriaSalesIQTM__Position_Team_Instance__c = posteamins.id;
        positionAccountCallPlan.AxtriaSalesIQTM__isIncludedCallPlan__c = true;
        //positionAccountCallPlan.Account_HCP_HCO_Number__c = 'BH11810999_';
        insert positionAccountCallPlan;
        AxtriaSalesIQTM__CIM_Config__c cimcc = TestDataFactory.createCIMConfig(teamins);
        cimcc.name = 'Total Targets Test';
        cimcc.AxtriaSalesIQTM__Object_Name__c = 'AxtriaSalesIQTM__Account__c';
        cimcc.AxtriaSalesIQTM__Attribute_API_Name__c = 'AccountNumber';
        cimcc.AxtriaSalesIQTM__Aggregation_Type__c = 'COUNT_DISTINCT';
        cimcc.AxtriaSalesIQTM__Aggregation_Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
        //cimcc.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c = null;
        cimcc.AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c = 'AxtriaSalesIQTM__Position_Team_Instance__c';//
        cimcc.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c = null;//
        //cimcc.AxtriaSalesIQTM__Aggregation_Condition_Attribute_Value__c = ' ';
        cimcc.AxtriaSalesIQTM__team_instance__c = teamins.id;
        cimcc.AxtriaSalesIQTM__Enable__c = true ;
        insert cimcc;
        AxtriaSalesIQTM__CIM_Config__c cimcc1 = TestDataFactory.createCIMConfig(teamins);
        cimcc1.name = 'Total Targets Test';
        cimcc1.AxtriaSalesIQTM__Object_Name__c = 'AxtriaSalesIQTM__Team_Instance__c';
        cimcc1.AxtriaSalesIQTM__Attribute_API_Name__c = 'AxtriaSalesIQTM__Team_Cycle_Name__c';
        cimcc1.AxtriaSalesIQTM__Aggregation_Type__c = 'Sum';
        cimcc1.AxtriaSalesIQTM__Aggregation_Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
        //cimcc.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c = null;
        cimcc1.AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c = 'AxtriaSalesIQTM__Position_Team_Instance__c';//
        cimcc1.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c = null;//
        //cimcc.AxtriaSalesIQTM__Aggregation_Condition_Attribute_Value__c = ' ';
        cimcc1.AxtriaSalesIQTM__team_instance__c = teamins.id;
        cimcc1.AxtriaSalesIQTM__Enable__c = true ;
        insert cimcc1;
        AxtriaSalesIQTM__CIM_Position_Metric_Summary__c cimconfigsum = TestDataFactory.createCIMPosMetSum(cimcc,posteamins,teamins);
        insert cimconfigsum;
        
        List<String> list_cim = new List<String>();
        list_cim.add(cimcc.id);
        list_cim.add(cimcc1.id);
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            
            BatchCreateCallPlanCIMPosMetrics obj=new BatchCreateCallPlanCIMPosMetrics(list_cim,teamins.Id,teamins.id);
            String objectToBeQueried ;
            String aggregationAttributeAPIName = cimcc.AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c;
            String aggregationObjectName = cimcc.AxtriaSalesIQTM__Aggregation_Object_Name__c;
            objectToBeQueried =   cimcc.AxtriaSalesIQTM__Object_Name__c.removeEnd('c');
            objectToBeQueried = objectToBeQueried + 'r.'+cimcc.AxtriaSalesIQTM__Attribute_API_Name__c;  
            Database.executeBatch(obj);
            
        }
        Test.stopTest();
    }
    static testMethod void testMethod3() {
    
    
    
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc.AccountNumber = 'BH11810999';
        acc.External_Account_Number__c = 'BH11810999';
        acc.AxtriaSalesIQTM__Country__c = countr.id;
        insert acc;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        insert scen;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.AxtriaSalesIQTM__Team_Cycle_Name__c = 'Current';
        //teamins.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        insert teamins;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        
        AxtriaSalesIQTM__Position_Team_Instance__c posteamins = TestDataFactory.createPositionTeamInstance(pos.id, null, teamins.id);
        insert posteamins;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        //positionAccountCallPlan.AxtriaSalesIQTM__Position_Team_Instance__c = posteamins.id;
        positionAccountCallPlan.AxtriaSalesIQTM__isIncludedCallPlan__c = true;
        //positionAccountCallPlan.Account_HCP_HCO_Number__c = 'BH11810999_';
        insert positionAccountCallPlan;
        AxtriaSalesIQTM__CIM_Config__c cimcc = TestDataFactory.createCIMConfig(teamins);
        
        cimcc.name = 'Total Targets Test';
        cimcc.AxtriaSalesIQTM__Object_Name__c = 'AxtriaSalesIQTM__Account__c';
        cimcc.AxtriaSalesIQTM__Attribute_API_Name__c = 'AccountNumber';
        cimcc.AxtriaSalesIQTM__Aggregation_Type__c = 'COUNT_DISTINCT';
        cimcc.AxtriaSalesIQTM__Aggregation_Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
        //cimcc.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c = null;
        cimcc.AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c = 'AxtriaSalesIQTM__Position_Team_Instance__c';//
        cimcc.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c = null;//
        //cimcc.AxtriaSalesIQTM__Aggregation_Condition_Attribute_Value__c = ' ';
        cimcc.AxtriaSalesIQTM__team_instance__c = teamins.id;
        cimcc.AxtriaSalesIQTM__Enable__c = true ;
        insert cimcc;
        AxtriaSalesIQTM__CIM_Config__c cimcc1 = TestDataFactory.createCIMConfig(teamins);
        cimcc1.name = 'Total Targets Test';
        cimcc1.AxtriaSalesIQTM__Object_Name__c = 'AxtriaSalesIQTM__Team_Instance__c';
        cimcc1.AxtriaSalesIQTM__Attribute_API_Name__c = 'AxtriaSalesIQTM__Team_Cycle_Name__c';
        cimcc1.AxtriaSalesIQTM__Aggregation_Type__c = 'Sum';
        cimcc1.AxtriaSalesIQTM__Aggregation_Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
        //cimcc.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c = null;
        cimcc1.AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c = 'AxtriaSalesIQTM__Position_Team_Instance__c';//
        cimcc1.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c = null;//
        //cimcc.AxtriaSalesIQTM__Aggregation_Condition_Attribute_Value__c = ' ';
        cimcc1.AxtriaSalesIQTM__team_instance__c = teamins.id;
        cimcc1.AxtriaSalesIQTM__Enable__c = true ;
        insert cimcc1;
        AxtriaSalesIQTM__CIM_Position_Metric_Summary__c cimconfigsum = TestDataFactory.createCIMPosMetSum(cimcc,posteamins,teamins);
        insert cimconfigsum;
        
        List<String> list_cim = new List<String>();
        list_cim.add(cimcc.id);
        list_cim.add(cimcc1.id);
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            String loggerID;
            BatchCreateCallPlanCIMPosMetrics obj=new BatchCreateCallPlanCIMPosMetrics(list_cim,teamins.Id,teamins.id,loggerID);
            
          //  System.assertEquals(null, obj.teamInstanceID);
           // System.assertEquals(null, obj.thequery);
            
            
            String objectToBeQueried ;
            String aggregationAttributeAPIName = cimcc.AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c;
            String aggregationObjectName = cimcc.AxtriaSalesIQTM__Aggregation_Object_Name__c;
            objectToBeQueried =   cimcc.AxtriaSalesIQTM__Object_Name__c.removeEnd('c');
            objectToBeQueried = objectToBeQueried + 'r.'+cimcc.AxtriaSalesIQTM__Attribute_API_Name__c;  
            Database.executeBatch(obj);
            
        }
        Test.stopTest();
    }
}