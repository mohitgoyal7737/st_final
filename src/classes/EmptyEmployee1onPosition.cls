global with sharing class EmptyEmployee1onPosition implements Database.Batchable<sObject> {
    public String query;
    public List<AxtriaSalesIQTM__Position__c> updateEmptyEmpList;
    public AxtriaSalesIQTM__TriggerContol__c customsetting1;
    public List<String> posCodeList=new List<String>();
    //public AxtriaSalesIQTM__TriggerContol__c customsetting2;
    public list<AxtriaSalesIQTM__TriggerContol__c>customsettinglist1 {get;set;}
    public boolean errorBatches=false;
    //public list<AxtriaSalesIQTM__TriggerContol__c>customsettinglist2 {get;set;}

    global EmptyEmployee1onPosition() 
    {
        query = '';
        customsetting1 = new AxtriaSalesIQTM__TriggerContol__c();
        customsettinglist1 = new list<AxtriaSalesIQTM__TriggerContol__c>();
        SnTDMLSecurityUtil.printDebugMessage('=====Custom Setting enable=====');
        customsetting1 = AxtriaSalesIQTM__TriggerContol__c.getValues('UpdatePositionCode');
        customsetting1.AxtriaSalesIQTM__IsStopTrigger__c = true ;
        customsettinglist1.add(customsetting1);
        update customsettinglist1;

        updateEmptyEmpList=new List<AxtriaSalesIQTM__Position__c>();

        posCodeList.add('0');
        posCodeList.add('00000');
        posCodeList.add('Unassigned');
        posCodeList.add('Unassigned Territory');

        query='select id,Employee1__c,AxtriaSalesIQTM__Employee__c,Employee1_Assignment_Type__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__inactive__c=false and AxtriaSalesIQTM__Client_Position_Code__c not in: posCodeList and AxtriaSalesIQTM__Client_Position_Code__c != null and (AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c= \'Current\' or AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = \'Future\')';  //added Master check AxtriaSalesIQTM__IsMaster__c=true and
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position__c> emptyPosList)
    {
       /* try
        {*/
            for(AxtriaSalesIQTM__Position__c emptyempRec : emptyPosList)
            {
                emptyempRec.Employee1__c = null;
                emptyempRec.Employee1_Assignment_Type__c=null;
                updateEmptyEmpList.add(emptyempRec);
            }

            SnTDMLSecurityUtil.printDebugMessage('Size of updateEmptyEmpList=====' +updateEmptyEmpList.size());
            if(updateEmptyEmpList.size() > 0){
                //update updateEmptyEmpList;  
                SnTDMLSecurityUtil.updateRecords(updateEmptyEmpList, 'EmptyEmployee1onPosition');
            }
       /* }   
        catch(Exception e)
       {
            errorBatches=true;ś
            SnTDMLSecurityUtil.printDebugMessage('++inside catch');
            String Header='Empty Employee on Position batch is failed, so delta has been haulted of this org for Today';
            tryCatchEmailAlert.veevaLoad(Header);
       }*/
    }

    global void finish(Database.BatchableContext BC) 
    {
        
          EmployeeUpdateonPositionBatch obj = new EmployeeUpdateonPositionBatch();
          Database.executeBatch(obj,2000);
        
    }
}