global class Batch_Integration_TSF_PositionAccount implements Database.Batchable<sObject>, Database.Stateful{
    public String query;
    public List<String> allProcessedIds = new List<String>();
    public List<String> allTeamInstances;

    public List<SIQ_TSF_vod_O__c> allStagingTSF;
    public Boolean chain = false;

    global Batch_Integration_TSF_PositionAccount (List<String> allTeamInstances) {
        this.allTeamInstances  = allTeamInstances;
        allProcessedIds = new List<String>();

        system.debug('+++++++++++ Hey All Team Instances are '+ allTeamInstances);
        //this.query = 'select id, AxtriaSalesIQTM__Segment10__c, P1__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__r.Name, Segment__c, AxtriaSalesIQTM__Account__r.AZ_VeevaID__c , AxtriaSalesIQTM__Account__r.AccountNumber ,Adoption__c, Potential__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name, Country__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, Party_ID__r.Accessibility_Range__c, AxtriaSalesIQTM__Team_Instance__c  from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c != null and AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances and AxtriaSalesIQTM__lastApprovedTarget__c = true';
          this.query='SELECT Id,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__r.Name,Name,AxtriaSalesIQTM__Position__r.Employee1__c,  AxtriaSalesIQTM__Account_Alignment_Type__c, AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Assignment_Status__c, AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Position_Team_Instance__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Team_Instance__c, Accessibility_Range__c, Speciality__c, Acc_Pos__c, Country__c, District__c, Region__c, AccountNumber__c FROM AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Position__c != null and AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances and AxtriaSalesIQTM__Assignment_Status__c = \'Active\' or  AxtriaSalesIQTM__Assignment_Status__c = \'Future Active\'';

       
        // Account_vod__c  Territory   AZ External Id  My Target   Accessibility';
    }

    /*global Batch_Integration_TSF_PositionAccount (List<String> allTeamInstances, Boolean chaining) {
        
        chain = chaining;

        this.allTeamInstances  = allTeamInstances;
        allProcessedIds = new List<String>();

        system.debug('+++++++++++ Hey All Team Instances are '+ allTeamInstances);
       // this.query = 'select id, AxtriaSalesIQTM__Segment10__c, P1__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__r.Name, Segment__c, AxtriaSalesIQTM__Account__r.AZ_VeevaID__c , AxtriaSalesIQTM__Account__r.AccountNumber ,Adoption__c, Potential__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name, Country__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, Party_ID__r.Accessibility_Range__c, AxtriaSalesIQTM__Team_Instance__c  from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c != null and AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances and AxtriaSalesIQTM__lastApprovedTarget__c = true';
          this.query='SELECT Id,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__r.Name,Name,AxtriaSalesIQTM__Position__r.Employee1__c,  AxtriaSalesIQTM__Account_Alignment_Type__c, AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Assignment_Status__c, AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Position_Team_Instance__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Team_Instance__c, Accessibility_Range__c, Speciality__c, Acc_Pos__c, Country__c, District__c, Region__c, AccountNumber__c FROM AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Position__c != null and AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances and AxtriaSalesIQTM__Assignment_Status__c = \'Active\' or  AxtriaSalesIQTM__Assignment_Status__c = \'Future Active\'';
       
        // Account_vod__c  Territory   AZ External Id  My Target   Accessibility';
    }
    
    */
    global Database.QueryLocator start(Database.BatchableContext bc) {
        system.debug('+++++++++++ Hey All Team Instances are '+ allTeamInstances);
       this.query='SELECT Id,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__r.Name,Name,AxtriaSalesIQTM__Position__r.Employee1__c,  AxtriaSalesIQTM__Account_Alignment_Type__c, AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Assignment_Status__c, AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Position_Team_Instance__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Team_Instance__c, Accessibility_Range__c, Speciality__c, Acc_Pos__c, Country__c, District__c, Region__c, AccountNumber__c FROM AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Position__c != null and (AxtriaSalesIQTM__Assignment_Status__c = \'Active\' or  AxtriaSalesIQTM__Assignment_Status__c = \'Future Active\')';

        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Account__c> scope) {

         allStagingTSF = new List<SIQ_TSF_vod_O__c>();

         for(AxtriaSalesIQTM__Position_Account__c   pacp : scope)
         {
            SIQ_TSF_vod_O__c stc = new SIQ_TSF_vod_O__c();
            if(pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__c != null)
            stc.Name = pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__r.Name;
            else
            stc.Name = 'Vacant';//pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
            
            stc.SIQ_My_Target_vod__c = false;
            stc.SIQ_Account_Number__c = pacp.AxtriaSalesIQTM__Account__r.AccountNumber;
            stc.SIQ_Account_vod__c = pacp.AxtriaSalesIQTM__Account__r.AccountNumber;//pacp.AxtriaSalesIQTM__Account__r.AZ_VeevaID__c;
            stc.SIQ_Territory_vod__c = pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;

            if(pacp.Accessibility_Range__c !=null)
            stc.SIQ_Accessibility_AZ_EU__c = pacp.Accessibility_Range__c;
            stc.Team_Instance__c = pacp.AxtriaSalesIQTM__Team_Instance__c;
            stc.Team_Instance_Lookup__c = pacp.AxtriaSalesIQTM__Team_Instance__c;
            //stc.Position_Account_Call_Plan__c = pacp.ID;
            stc.SIQ_External_ID_AZ__c = stc.SIQ_Account_Number__c +'__'+ stc.SIQ_Territory_vod__c;
            stc.Status__c = 'Updated';
            
            
            if(!allProcessedIds.contains(stc.SIQ_External_ID_AZ__c)){
                allStagingTSF.add(stc);
                allProcessedIds.add(stc.SIQ_External_ID_AZ__c);
            }
         }

         upsert allStagingTSF SIQ_External_ID_AZ__c;
         system.debug('===================='+allStagingTSF);
    }

    global void finish(Database.BatchableContext BC) {
       // database.executeBatch(new Batch_Integration_TSF_Extension_NEW(allTeamInstances, chain),2000);
    }
}