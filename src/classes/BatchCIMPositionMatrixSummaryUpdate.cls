/**********************************************************************************************
@author       : SnT Team
@modifiedBy   : A1422
@modifiedDate : July 15th'2020
@description  : Batch class for creating CIM Position Metric Summary for a particular Team Instance
@changes done : This batch is now chained with direct load/call plan publish. Also it will now work fine            incase of direct load done in mid-cycle of call plan. Assumption for mid cycle - AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c should have only extra conditions other than AxtriaSalesIQTM__isAccountTarget__c ,AxtriaSalesIQTM__isincludedCallPlan__c, AxtriaSalesIQTM__lastApprovedTarget__c
@Revison(s)   : v1.1
**********************************************************************************************/
global with sharing class BatchCIMPositionMatrixSummaryUpdate implements Database.Batchable<sObject>, Database.Stateful
{
    public String thequery;
    String teamInstanceID;
    String cimConfigId;
    String objectName;
    String attributeAPIname;
    String aggregationType;
    String aggregationObjectName;
    String aggregationAttributeAPIName;
    String aggregationConditionAttributeAPIName;
    String objectToBeQueried;
    String cr_type_name;
    // SR-183
    String attributeAPInameoriginal;
    String attributeAPInameproposed;
    String attributeAPInameapproved;
    
    String aggrConditionAttributeAPINameoriginal;
    String aggrConditionAttributeAPINameproposed;
    String aggrConditionAttributeAPINameApproved;
    
    String objectToBeQueriedoriginal;
    String objectToBeQueriedproposed;
    String objectToBeQueriedApproved;
    
    String  Apinameorig;
    String Operatororig;
    String Valueorig ; 
    
    String  Apinameprop;
    String Operatorprop;
    String Valueprop ;
     
    String  ApinameAppr;
    String OperatorAppr;
    String ValueAppr ; 
    
    Boolean result = false;  
    Boolean result1 = false;
   
    //Boolean AggrConditionprop = False;
    ///End SR-183
    Map<String, Map<String, Decimal>> mapAggregate;
    Map<String, Map<String, Set<String>>> mapAggregateSet;
    Map<String, Map<String, Decimal>> mapCount;
    Map<String, Map<String, Decimal>> mapSum;
     List<AxtriaSalesIQTM__CIM_Config__c> cimConfigList; // --- Added by RT for STIMPS-238 ---
    Set<String> teamInstIds = new Set<String>(); // --- Added by RT for STIMPS-238 --
    
    
    Boolean isProcessBuilderFlag = false;  // --- Added by RT for STIMPS-238 --
    List<AxtriaSalesIQTM__CIM_Config__c> list_cimConfig  = new List<AxtriaSalesIQTM__CIM_Config__c>() ; // --- Added by RT for STIMPS-238 ---
    List<String> list_cimID = new  List<String>();
    public String sourceTeamInst;
    public String destTeamInst;
    Boolean conditionCallPlan;
    final String ORIGINAL = 'ORIGINAL';
    final String PROPOSED = 'PROPOSED';
    final String APPROVED = 'APPROVED';

    public String alignNmsp;
    public String batchName;
    public String callPlanLoggerID;
    public Boolean proceedNextBatch = true;

    public String ruleidis ;
    public boolean directFlag=false;
    public boolean directFlag2=false;
    public boolean chained = false;

    //Added by HT(A0994) on 2nd June 2020
    global BatchCIMPositionMatrixSummaryUpdate(List<String> list_cim, String teamInst, String sourceTeamInstance)
    {
        teamInstanceID  = teamInst;
        sourceTeamInst = sourceTeamInstance;
        destTeamInst = teamInst;

        cimConfigId = list_cim[0];
        list_cimID = list_cim;
        list_cimID.remove(0);


        initialiseDefaultValues();
        initialiseCIM_Attributes();
        buildQuery();

    }
    
    global BatchCIMPositionMatrixSummaryUpdate()
    {
       

    }
    global BatchCIMPositionMatrixSummaryUpdate(List<String> list_cim, String teamInst)
    {

        teamInstanceID  = teamInst;

        cimConfigId = list_cim[0];
        list_cimID = list_cim;
        list_cimID.remove(0);

        initialiseDefaultValues();
        initialiseCIM_Attributes();
        buildQuery();

    }
    global BatchCIMPositionMatrixSummaryUpdate(List<String> list_cim, String teamInst,String loggerID,String alignNmspPrefix)
    {
        callPlanLoggerID = loggerID;
        alignNmsp = alignNmspPrefix;

        teamInstanceID  = teamInst;

        cimConfigId = list_cim[0];
        list_cimID = list_cim;
        list_cimID.remove(0);

        initialiseDefaultValues();
        initialiseCIM_Attributes();
        buildQuery();

    }
    global BatchCIMPositionMatrixSummaryUpdate(List<String> list_cim, String teamInst, String ruleID, Boolean flag_1, 
                                                Boolean flag_2,String loggerID,String alignNmspPrefix)
    {
        this.ruleidis = ruleID;
        this.directFlag = flag_1;
        this.directFlag2 = flag_2;
        this.chained = true;
        callPlanLoggerID = loggerID;
        alignNmsp = alignNmspPrefix;

        teamInstanceID  = teamInst;

        cimConfigId = list_cim[0];
        list_cimID = list_cim;
        list_cimID.remove(0);

        initialiseDefaultValues();
        initialiseCIM_Attributes();
        buildQuery();

    }
     // -- Added by RT for STIMPS-238 ---
    /* ************************************************************************************
    Method Name : BatchCIMPositionMatrixSummaryUpdate
    Description : To process the summary cards for inactive PACP corresponding to Inactive accounts on basis of affected 
                team instance Ids which we fetched in another class UpdateProfileParameterAccounts

    Input Parameters  : List<AxtriaSalesIQTM__CIM_Config__c> CIM Config list corresponding to team instance fetched from Inactive PACP to be passed to finish method
    Return type : NA 
    ******************************************************************************************** */
    global BatchCIMPositionMatrixSummaryUpdate(List<AxtriaSalesIQTM__CIM_Config__c> cimConfigList){
      
          
        System.debug('list_cimConfig>> '+ list_cimConfig);
        System.debug('Batch RT cimConfigList>>>' + cimConfigList);
        System.debug(' cimConfigList size>>> ' + cimConfigList.size());
        isProcessBuilderFlag = true;
        cimConfigId = cimConfigList[0].Id;
        teamInstanceID = cimConfigList[0].AxtriaSalesIQTM__Team_Instance__c;
        list_cimConfig = cimConfigList;
        list_cimConfig.remove(0);
            
        initialiseDefaultValues();
        initialiseCIM_Attributes();
        buildQuery();
    }
    // -- Added by RT for STIMPS-238 ends here ---
    
    // Not referenced anywhere just for Managed package, as this global method can't be removed from package,hence created new one above
    global BatchCIMPositionMatrixSummaryUpdate(Set<String> teamInstIds,List<AxtriaSalesIQTM__CIM_Config__c> cimConfigList){
         
    }
    
 
    global BatchCIMPositionMatrixSummaryUpdate(List<String> list_cim, String teamInst, String ruleID, Boolean flag_1, 
                                                Boolean flag_2)
    {
        this.ruleidis = ruleID;
        this.directFlag = flag_1;
        this.directFlag2 = flag_2;
        this.chained = true;
        

        teamInstanceID  = teamInst;

        cimConfigId = list_cim[0];
        list_cimID = list_cim;
        list_cimID.remove(0);

        initialiseDefaultValues();
        initialiseCIM_Attributes();
        buildQuery();

    }

    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator(theQuery);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
    
        String key, value_string,value_stringoriginal,value_stringProposed,value_stringApproved;
        Boolean originalFlag = true, proposedFlag = true, approvedFlag = true,AggrConditionorig = true,AggrConditionprop = true,AggrConditionappr =true;
        Integer i = 0, j = scope.size();

        for(i = 0; i < j; i++)
        {

            key = String.valueOf(scope[i].get(aggregationAttributeAPIName));
            
           
            
            if(conditionCallPlan)
            {
               if(Apinameorig != null){
                    AggrConditionorig= Boolean.valueOf(evalCondition(string.valueof(Scope[i].get(Apinameorig)),Valueorig,operatororig));    
                    system.debug('-----AggrConditionorig------->'+evalCondition( string.valueof(Scope[i].get(Apinameorig)),  Valueorig,  operatororig));
                }
                if(Apinameprop != null){  // SR-454
                    AggrConditionprop= Boolean.valueOf(evalCondition1( string.valueof(Scope[i].get(Apinameprop)),Valueprop,Operatorprop));
                }
                if(ApinameAppr != null){
                    AggrConditionappr= Boolean.valueOf(evalCondition2( string.valueof(Scope[i].get(ApinameAppr)),ValueAppr,OperatorAppr));
                }
             
                
                originalFlag = Boolean.valueOf(scope[i].get('AxtriaSalesIQTM__isAccountTarget__c'));
                proposedFlag = Boolean.valueOf(scope[i].get('AxtriaSalesIQTM__isincludedCallPlan__c'));
                approvedFlag = Boolean.valueOf(scope[i].get('AxtriaSalesIQTM__lastApprovedTarget__c'));
                //AggrConditionorig = Boolean.valueOf(scope[i].get('AxtriaSalesIQTM__isTarget__c'));
            }

          
            
           if(objectToBeQueriedoriginal != null && objectToBeQueriedproposed != null && objectToBeQueriedApproved != null){
             System.debug('---objectToBeQueriedoriginal----->'+objectToBeQueriedoriginal);
                if(objectToBeQueriedoriginal.contains('__r') )    // SR-183 changes  Original
                {
                    SnTDMLSecurityUtil.printDebugMessage('objectToBeQueriedoriginal.split==' + objectToBeQueriedoriginal.split('\\.'));
                    value_stringoriginal = String.valueOf(scope[i].getsObject(objectToBeQueriedoriginal.split('\\.')[0]).get(objectToBeQueriedoriginal.split('\\.')[1]));
                }
                else
                {
                    value_stringoriginal = String.valueOf(scope[i].get(objectToBeQueriedoriginal));
                }
           // }
           // if(objectToBeQueriedproposed != null){
                if(objectToBeQueriedproposed.contains('__r'))    // SR-183 changes  Proposed
                {
                    SnTDMLSecurityUtil.printDebugMessage('objectToBeQueriedproposed.split==' + objectToBeQueriedproposed.split('\\.'));
                    value_stringProposed = String.valueOf(scope[i].getsObject(objectToBeQueriedproposed.split('\\.')[0]).get(objectToBeQueriedproposed.split('\\.')[1]));
                }
                else
                {
                    value_stringProposed = String.valueOf(scope[i].get(objectToBeQueriedproposed));
                }
           // }
           // if(objectToBeQueriedApproved != null){
                if(objectToBeQueriedApproved.contains('__r'))    // SR-183 changes Approved
                {
                    SnTDMLSecurityUtil.printDebugMessage('objectToBeQueriedApproved.split==' + objectToBeQueriedApproved.split('\\.'));
                    value_stringApproved = String.valueOf(scope[i].getsObject(objectToBeQueriedApproved.split('\\.')[0]).get(objectToBeQueriedApproved.split('\\.')[1]));
                }
                else
                {
                    value_stringApproved = String.valueOf(scope[i].get(objectToBeQueriedApproved));
                }
            
            }else{
                System.debug('else part of objectToBeQueried Attribute API Name-----> '+objectToBeQueried);
                    if(objectToBeQueried.contains('__r'))
                    {
                        SnTDMLSecurityUtil.printDebugMessage('objectToBeQueried.split==' + objectToBeQueried.split('\\.'));
                        value_string = String.valueOf(scope[i].getsObject(objectToBeQueried.split('\\.')[0]).get(objectToBeQueried.split('\\.')[1]));
                    }
                    else
                    {
                        value_string = String.valueOf(scope[i].get(objectToBeQueried));
                    }
            }
            
           
           // System.debug('-----originalFlag--->'+originalFlag);
            //System.debug('---AggrConditionorig----->'+AggrConditionorig);
            //System.debug('----objectToBeQueriedoriginal---->'+objectToBeQueriedoriginal);
           // System.debug('----string.valueof(Scope[i]---->'+string.valueof(Scope[i].get(Apinameorig)));
            
            if(objectToBeQueriedoriginal != null && originalFlag && AggrConditionorig)
              
            {
            system.debug('------------value_stringoriginal----->'+value_stringoriginal);
                
                contributeValueInAggregation(value_stringoriginal, ORIGINAL, key);
            }
            if(objectToBeQueriedproposed != null && proposedFlag && AggrConditionprop)
            {
               system.debug('------------value_stringProposed----->'+value_stringProposed); 
                contributeValueInAggregation(value_stringProposed, PROPOSED, key);
            }
            if(objectToBeQueriedApproved != null && approvedFlag && AggrConditionappr)
            {
                system.debug('------------value_stringApproved----->'+value_stringApproved);
                contributeValueInAggregation(value_stringApproved, APPROVED, key);
            }
            
            // When Attribute API Name Original and Attribute API Name proposed and Attribute API Name Approved field value null on cim config object
            
            if(originalFlag && AggrConditionorig && objectToBeQueriedoriginal == null)   // add else part in SR-183
            {
               system.debug('------------value_string----->'+value_string);
                contributeValueInAggregation(value_string, ORIGINAL, key);
            }
           
            if(proposedFlag  && AggrConditionprop && objectToBeQueriedproposed == null)
            {

                contributeValueInAggregation(value_string, PROPOSED, key);
            }
            
            if(approvedFlag &&  AggrConditionappr && objectToBeQueriedApproved == null)
            {

                contributeValueInAggregation(value_string, APPROVED, key);
            } 
        }
    }

    global void finish(Database.BatchableContext BC)
    {
        String originalVal, proposedVal, approvedVal;
        List<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c> updateListPostionMetric = new list<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c>();
        List<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c> existingCIM_Summary = [Select id from AxtriaSalesIQTM__CIM_Position_Metric_Summary__c where AxtriaSalesIQTM__CIM_Config__c = :cimConfigId and AxtriaSalesIQTM__Team_Instance__c = :teamInstanceID WITH SECURITY_ENFORCED];
        if(existingCIM_Summary.size() > 0)
        {
            //delete existingCIM_Summary;
            SnTDMLSecurityUtil.deleteRecords(existingCIM_Summary, 'BatchCIMPositionMatrixSummaryUpdate');
        }

        for(AxtriaSalesIQTM__Position_Team_Instance__c pti :  [select ID from AxtriaSalesIQTM__position_team_instance__c
                where AxtriaSalesIQTM__Team_Instance_ID__c = :teamInstanceID
                        WITH SECURITY_ENFORCED])
        {
            originalVal = '0';
            proposedVal = '0';
            approvedVal = '0';
            if(mapAggregate.containsKey(pti.ID))
            {
                originalVal = String.valueOf(mapAggregate.get(pti.ID).get(ORIGINAL));
                proposedVal = String.valueOf(mapAggregate.get(pti.ID).get(PROPOSED));
                approvedVal = String.valueOf(mapAggregate.get(pti.ID).get(APPROVED));

                originalVal = replaceNullWithZero(originalVal);
                proposedVal = replaceNullWithZero(proposedVal);
                approvedVal = replaceNullWithZero(approvedVal);

            }
            AxtriaSalesIQTM__CIM_Position_Metric_Summary__c objCIMPositionMetric = new AxtriaSalesIQTM__CIM_Position_Metric_Summary__c(AxtriaSalesIQTM__CIM_Config__c = cimConfigId, AxtriaSalesIQTM__Original__c = originalVal, AxtriaSalesIQTM__Team_Instance__c = teamInstanceID, AxtriaSalesIQTM__Proposed__c = proposedVal, AxtriaSalesIQTM__Approved__c = approvedVal, AxtriaSalesIQTM__Position_Team_Instance__c = pti.Id);
            updateListPostionMetric.add(objCIMPositionMetric);
        }
        if(updateListPostionMetric.size() > 0)
        {
            //insert updateListPostionMetric;
            SnTDMLSecurityUtil.insertRecords(updateListPostionMetric, 'BatchCIMPositionMatrixSummaryUpdate');
        }
        
        
                
        //Else clause added by HT(A0994) on 2nd June 2020 for chaining functions
       if(list_cimID.size() > 0 && isProcessBuilderFlag == false)
        {
            if(String.isNotBlank(sourceTeamInst))
            {
                Database.executeBatch(new  BatchCIMPositionMatrixSummaryUpdate(list_cimID, destTeamInst, sourceTeamInst), 2000);
            }
            else if(String.isNotBlank(teamInstanceID) && String.isNotBlank(ruleidis) && String.isNotBlank(callPlanLoggerID))
            {
                
                Database.executeBatch(new  BatchCIMPositionMatrixSummaryUpdate(list_cimID, teamInstanceID, ruleidis,  directFlag, 
                                                 directFlag2, callPlanLoggerID, alignNmsp),2000);

            }
            else if(String.isNotBlank(teamInstanceID) && String.isNotBlank(ruleidis))
            {
                
                Database.executeBatch(new  BatchCIMPositionMatrixSummaryUpdate(list_cimID, teamInstanceID, ruleidis,  directFlag, 
                                                 directFlag2),2000);

            }
           
            else if(String.isNotBlank(teamInstanceID)){
                Database.executeBatch(new  BatchCIMPositionMatrixSummaryUpdate(list_cimID, teamInstanceID), 2000);
            }

        }
        else if(String.isNotBlank(sourceTeamInst) && String.isNotBlank(destTeamInst)) // added null check on source and destination team ins by A!422 so that batch is not called unnecessarily
        {   

            CopyCallPlanScenarioTriggerHandler copy = new CopyCallPlanScenarioTriggerHandler(sourceTeamInst, destTeamInst);
            copy.copyDependencyControl();
        }
        /*SNT-515 starts*/
        else{
            if(directFlag2==true){
                Database.executeBatch(new BatchCallPlanSummaryReports(), 1000);
            }
            else if(directFlag==true)
            {    
                if(callPlanLoggerID!=null && callPlanLoggerID!='')
                {
                    if(proceedNextBatch)
                        Database.executeBatch(new BatchCallPlanSummaryReports(teamInstanceID,callPlanLoggerID,alignNmsp),2000);
                }
                else
                    Database.executeBatch(new BatchCallPlanSummaryReports(teamInstanceID), 1000);    
            }
            else if(String.isNotBlank(ruleidis)){
                Database.executeBatch(new BatchCallPlanSummaryReports(teamInstanceID,ruleidis), 1000);   
            }
            // -- Added by RT for STIMPS-238 --
            else if(!list_cimConfig.isEmpty()){
                Database.executeBatch(new  BatchCIMPositionMatrixSummaryUpdate(list_cimConfig), 2000);
                
            }
            // -- ends here ---
        }
        /*SNT-515 ends*/
    }

    public String isNumber(String str)
    {
        try
        {
            Decimal i = Decimal.valueOf(str);
            return str;
        }
        catch(Exception e)
        {
            return '0';
        }
    }
    public void initialiseCIM_Attributes()
    {
        AxtriaSalesIQTM__CIM_Config__c cim = new AxtriaSalesIQTM__CIM_Config__c();
        System.debug('cimConfigId' + cimConfigId);
        System.debug('teamInstanceID' + teamInstanceID);
        cim = [select id, name, AxtriaSalesIQTM__Object_Name__c, AxtriaSalesIQTM__Attribute_API_Name__c,
               AxtriaSalesIQTM__Aggregation_Type__c, AxtriaSalesIQTM__Aggregation_Object_Name__c,
               AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c,
               AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c,
               AxtriaSalesIQTM__Aggregation_Condition_Attribute_Value__c, AxtriaSalesIQTM__CR_Type_Name__c,
               Attribute_API_Name_Original__c,Attribute_API_Name_Proposed__c,Attribute_API_Name_Approved__c,
               Aggr_Condition_Attribute_API_Name_Orig__c,Aggr_Condition_Attribute_API_Name_Pro__c,Aggr_Condition_Attribute_API_Name_Appro__c
               from AxtriaSalesIQTM__CIM_Config__c where id = :cimConfigId and
                       AxtriaSalesIQTM__team_instance__c = :teamInstanceID and AxtriaSalesIQTM__Enable__c =true and
                               AxtriaSalesIQTM__Aggregation_Object_Name__c != null and
                               AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c != null WITH SECURITY_ENFORCED limit 1]; // -- Updated by RT for STIMPS-238 though this check should be there in place in general, filter added AxtriaSalesIQTM__Enable__c =true 

        objectName = cim.AxtriaSalesIQTM__Object_Name__c;
        attributeAPIname = cim.AxtriaSalesIQTM__Attribute_API_Name__c;
        aggregationType = cim.AxtriaSalesIQTM__Aggregation_Type__c.toUpperCase();
        aggregationObjectName = cim.AxtriaSalesIQTM__Aggregation_Object_Name__c;
        aggregationAttributeAPIName = cim.AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c;
        aggregationConditionAttributeAPIName = cim.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c;
        cr_type_name = cim.AxtriaSalesIQTM__CR_Type_Name__c;
        
        attributeAPInameoriginal = cim.Attribute_API_Name_Original__c;   // SR-183
        attributeAPInameproposed = cim.Attribute_API_Name_Proposed__c;   // SR-183
        attributeAPInameapproved = cim.Attribute_API_Name_Approved__c;  // SR-183
            
        aggrConditionAttributeAPINameoriginal = cim.Aggr_Condition_Attribute_API_Name_Orig__c;
        aggrConditionAttributeAPINameproposed = cim.Aggr_Condition_Attribute_API_Name_Pro__c;
        aggrConditionAttributeAPINameApproved = cim.Aggr_Condition_Attribute_API_Name_Appro__c;
        
        
        
        // Split where condation on the basic of space to use original ,proposed ,and Approved 
         if(aggrConditionAttributeAPINameoriginal != null){ 
                String val  = aggrConditionAttributeAPINameoriginal;  //Aggr. Condition Attribute API Name Orig  field condation use in OriginalFlag
                   
                String[] splitval = val.split(' '); 
                  Apinameorig =     splitval[0];   //AxtriaSalesIQTM__isAccountTarget__c
                 Operatororig =     splitval[1];   //==
                 Valueorig    =     splitval[2];   // True
            }
            if(aggrConditionAttributeAPINameproposed != null){ 
                String val  = aggrConditionAttributeAPINameproposed;  
                     
                String[] splitval = val.split(' '); 
                 Apinameprop  =     splitval[0];   
                 Operatorprop =     splitval[1];   
                 Valueprop    =     splitval[2];   
            }
            if(aggrConditionAttributeAPINameApproved != null){ 
                String val  = aggrConditionAttributeAPINameApproved;  
                     
                String[] splitval = val.split(' '); 
                 ApinameAppr  =     splitval[0];   
                 OperatorAppr  =     splitval[1];   
                 ValueAppr    =     splitval[2];   
            }
           
        
        conditionCallPlan = (cr_type_name == SalesIQGlobalConstants.CR_TYPE_CALL_PLAN && aggregationObjectName == 'AxtriaSalesIQTM__Position_Account_Call_Plan__c') ? true : false;
        System.debug('conditionCallPlan =>' + conditionCallPlan);

        if(objectName != aggregationObjectName && attributeAPIname != null)  // SR-183 changes
        {
            objectToBeQueried =  objectName.removeEnd('c');
            objectToBeQueried = objectToBeQueried + 'r.' + attributeAPIname;
        }
        else
        {
            objectToBeQueried = attributeAPIname;

        }
        if(objectName != aggregationObjectName && attributeAPInameoriginal != null)
        {
            objectToBeQueriedoriginal =  objectName.removeEnd('c');
            objectToBeQueriedoriginal = objectToBeQueriedoriginal + 'r.' + attributeAPInameoriginal;
        }
        else{
        
             objectToBeQueriedoriginal = attributeAPInameoriginal;
             
        }
        if(objectName != aggregationObjectName && attributeAPInameproposed != null)
        {
            objectToBeQueriedproposed =  objectName.removeEnd('c');
            objectToBeQueriedproposed = objectToBeQueriedproposed + 'r.' + attributeAPInameproposed;
        }
        else{
           objectToBeQueriedproposed = attributeAPInameproposed;
        }
        if(objectName != aggregationObjectName && attributeAPInameapproved != null)
        {
            objectToBeQueriedApproved =  objectName.removeEnd('c');
            objectToBeQueriedApproved = objectToBeQueriedApproved + 'r.' + attributeAPInameapproved;
        }
        else{
            objectToBeQueriedApproved = attributeAPInameapproved;
        }
        
    }
    public void initialiseDefaultValues()
    {
        mapAggregate = new  Map<String, Map<String, Decimal>>();
        mapCount = new Map<String, Map<String, Decimal>>();
        mapSum = new Map<String, Map<String, Decimal>>();
        mapAggregateSet = new Map<String, Map<String, Set<String>>>();


    }
    public void buildQuery()
    {
        theQuery = 'Select ' + aggregationAttributeAPIName ;
        
        if(objectToBeQueried != null)   // SR-183 changes
        {
            theQuery += ',' +objectToBeQueried;
        }
        if(objectToBeQueriedoriginal != null)   // SR-183 changes
        {
            theQuery += ',' +objectToBeQueriedoriginal;
        }
        if(objectToBeQueriedproposed != null)   // SR-183 changes
        {
            theQuery += ',' +objectToBeQueriedproposed;
        }
        if(objectToBeQueriedApproved != null)   // SR-183 changes
        {
            theQuery += ',' +objectToBeQueriedApproved;
        }
        
        if(conditionCallPlan)
        {

            theQuery += ',AxtriaSalesIQTM__isAccountTarget__c, AxtriaSalesIQTM__isincludedCallPlan__c, AxtriaSalesIQTM__lastApprovedTarget__c ';
        }
        if(aggrConditionAttributeAPINameoriginal != null){
            
            theQuery +=','+Apinameorig+ '';
        }
        if(aggrConditionAttributeAPINameproposed != null){
            
            theQuery +=','+Apinameprop+ '';
        }
        if(aggrConditionAttributeAPINameApproved != null){
            
            theQuery +=','+ApinameAppr+ '';
        }
        theQuery += ' from ' + aggregationObjectName + ' where AxtriaSalesIQTM__team_instance__c =:teamInstanceID ';
        if(conditionCallPlan)
        {

            theQuery += 'and (AxtriaSalesIQTM__isAccountTarget__c = true or AxtriaSalesIQTM__isincludedCallPlan__c=true or AxtriaSalesIQTM__lastApprovedTarget__c = true) ';
        }
        if(aggregationConditionAttributeAPIName != null)
        {

          theQuery += ' and (' + aggregationConditionAttributeAPIName + ')';
            SnTDMLSecurityUtil.printDebugMessage('======= ' + aggregationConditionAttributeAPIName);

        }
       /* if(aggrConditionAttributeAPINameoriginal != null){
            theQuery += ' and (' + aggrConditionAttributeAPINameoriginal + '';
        }
        if(aggrConditionAttributeAPINameproposed != null){
            theQuery += ' OR ' + aggrConditionAttributeAPINameproposed + '';
        }
        if(aggrConditionAttributeAPINameApproved != null){
            theQuery += ' OR ' + aggrConditionAttributeAPINameApproved + ')';
        }*/
        if(aggrConditionAttributeAPINameoriginal != null){
            theQuery += ' and (' + aggrConditionAttributeAPINameoriginal + '';
        }
        if(aggrConditionAttributeAPINameoriginal != null && aggrConditionAttributeAPINameproposed != null){
             theQuery += ' OR ' + aggrConditionAttributeAPINameproposed + '';
        }
        
        else{
            if(aggrConditionAttributeAPINameproposed != null){
                theQuery += ' AND (' + aggrConditionAttributeAPINameproposed + '';
            }
        }
        if(aggrConditionAttributeAPINameproposed != null && aggrConditionAttributeAPINameApproved != null){
            theQuery += ' OR ' + aggrConditionAttributeAPINameApproved + ')';
        }
        else if(aggrConditionAttributeAPINameoriginal != null && aggrConditionAttributeAPINameApproved != null){
            theQuery += ' OR ' + aggrConditionAttributeAPINameApproved + ')';
        }
        else if(aggrConditionAttributeAPINameoriginal != null &&  aggrConditionAttributeAPINameproposed == null && aggrConditionAttributeAPINameApproved == null){
             theQuery += ')';
        }
        else if(aggrConditionAttributeAPINameoriginal == null &&  aggrConditionAttributeAPINameproposed != null && aggrConditionAttributeAPINameApproved == null){
             theQuery += ')';
        }
        else if(aggrConditionAttributeAPINameoriginal != null &&  aggrConditionAttributeAPINameproposed != null && aggrConditionAttributeAPINameApproved == null){
             theQuery += ')';
        }
        
        else{
            if(aggrConditionAttributeAPINameApproved != null){
                theQuery += ' AND ' + aggrConditionAttributeAPINameApproved + '';
            }
        }
        System.debug ('Final query PACP object -------->' + theQuery);
        SnTDMLSecurityUtil.printDebugMessage('the Query ' + theQuery);
        
    }

    public String replaceNullWithZero(String value)
    {
        return (String.isBlank(value) ? '0' : value);
    }
    
    public Boolean evalCondition(String Apinameorig, String Valueorig, String operatororig){
    Boolean result = false;
    try{
        Decimal ex0 = decimal.valueOf(Apinameorig);
        decimal ex1 = decimal.valueOf(Valueorig);
        if(operatororig == '=='){
            if(ex0 == ex1){
            result = true;
            }
        }
        else if(operatororig == '!='){
            if(ex0 != ex1){
            result = true;
            }
        }
        else if(operatororig == '<'){
            if(ex0 < ex1){
            result = true;
            }
        }
        else if(operatororig == '>'){
            if(ex0 > ex1){
            result = true;
            }
        }
        else if(operatororig == '<='){
            if(ex0 <= ex1){
            result = true;
            }
        }
        else if(operatororig == '>='){
            if(ex0 >= ex1){
            result = true;
            }
    }
    }catch(Exception e){
        
        String ex0 = Apinameorig;
        String ex1 = Valueorig;
        if(operatororig == '='){
            if(ex0 == ex1){
            result = true;
        }
        }
        else if(operatororig == '!='){
            if(ex0 != ex1){
            result = true;
            }
        }
        else if(operatororig == '<'){
            if(ex0 < ex1){
            result = true;
        }
        }
        else if(operatororig == '>'){
            if(ex0 > ex1){
            result = true;
            }
        }
        else if(operatororig == '<='){
            if(ex0 <= ex1){
            result = true;
            }
        }
        else if(operatororig == '>='){
            if(ex0 >= ex1){
            result = true;
            }
        }
    }
    return result;
  }
  public Boolean evalCondition1(String Apinameprop, String Valueprop, String Operatorprop){  // SR-454
    Boolean result = false;
    try{
        Decimal ex0 = decimal.valueOf(Apinameprop);
        decimal ex1 = decimal.valueOf(Valueprop);
        if(Operatorprop == '=='){
            if(ex0 == ex1){
            result = true;
            }
        }
        else if(Operatorprop == '!='){
            if(ex0 != ex1){
            result = true;
            }
        }
        else if(Operatorprop == '<'){
            if(ex0 < ex1){
            result = true;
            }
        }
        else if(Operatorprop == '>'){
            if(ex0 > ex1){
            result = true;
            }
        }
        else if(Operatorprop == '<='){
            if(ex0 <= ex1){
            result = true;
            }
        }
        else if(Operatorprop == '>='){
            if(ex0 >= ex1){
            result = true;
            }
    }
    }catch(Exception e){
        
        String ex0 = Apinameprop;
        String ex1 = Valueprop;
        if(Operatorprop == '='){
            if(ex0 == ex1){
            result = true;
        }
        }
        else if(Operatorprop == '!='){
            if(ex0 != ex1){
            result = true;
            }
        }
        else if(Operatorprop == '<'){
            if(ex0 < ex1){
            result = true;
        }
        }
        else if(Operatorprop == '>'){
            if(ex0 > ex1){
            result = true;
            }
        }
        else if(Operatorprop == '<='){
            if(ex0 <= ex1){
            result = true;
            }
        }
        else if(Operatorprop == '>='){
            if(ex0 >= ex1){
            result = true;
            }
        }
    }
    return result;
  }
  public Boolean evalCondition2(String ApinameAppr, String ValueAppr, String OperatorAppr){
    Boolean result = false;
    try{
        Decimal ex0 = decimal.valueOf(ApinameAppr);
        decimal ex1 = decimal.valueOf(ValueAppr);
        if(OperatorAppr == '=='){
            if(ex0 == ex1){
            result = true;
            }
        }
        else if(OperatorAppr == '!='){
            if(ex0 != ex1){
            result = true;
            }
        }
        else if(OperatorAppr == '<'){
            if(ex0 < ex1){
            result = true;
            }
        }
        else if(OperatorAppr == '>'){
            if(ex0 > ex1){
            result = true;
            }
        }
        else if(OperatorAppr == '<='){
            if(ex0 <= ex1){
            result = true;
            }
        }
        else if(OperatorAppr == '>='){
            if(ex0 >= ex1){
            result = true;
            }
    }
    }catch(Exception e){
        
        String ex0 = ApinameAppr;
        String ex1 = ValueAppr;
        if(OperatorAppr == '='){
            if(ex0 == ex1){
            result = true;
        }
        }
        else if(OperatorAppr == '!='){
            if(ex0 != ex1){
            result = true;
            }
        }
        else if(OperatorAppr == '<'){
            if(ex0 < ex1){
            result = true;
        }
        }
        else if(OperatorAppr == '>'){
            if(ex0 > ex1){
            result = true;
            }
        }
        else if(OperatorAppr == '<='){
            if(ex0 <= ex1){
            result = true;
            }
        }
        else if(OperatorAppr == '>='){
            if(ex0 >= ex1){
            result = true;
            }
        }
    }
    return result;
  }
   public void contributeValueInAggregation(String value_string, String valueType, String key)
    {
        Set<String> tempSet = new Set<String>();
        Decimal value = 0, agg_val = 0, agg_count = 0, agg_sum = 0, tempVal = 1;
        Map<String, Decimal> tempMap = new Map<String, Decimal>();
        Map<String, Set<String>> tempSetMap = new Map<String, Set<String>>();

        value = value_string == null ? 0 : Decimal.valueOf(isNumber(value_string));
        if(!mapAggregate.containsKey(key))
        {
            tempVal = (aggregationType == 'COUNT' || aggregationType == 'COUNT_DISTINCT') ? 1 : value;
            mapAggregate.put(key, new Map<String, Decimal> {valueType => tempVal});
            mapCount.put(key, new Map<String, Decimal> {valueType => 1});
            mapSum.put(key, new Map<String, Decimal> {valueType => value});
            tempSet = new Set<String>();
        }

        else if(mapAggregate.containsKey(key) && !mapAggregate.get(key).containsKey(valueType))
        {
            tempMap = mapAggregate.get(key);
            tempVal = (aggregationType == 'COUNT' || aggregationType == 'COUNT_DISTINCT') ? 1 : value;
            tempMap.put(valueType, tempVal);
            mapAggregate.put(key, tempMap);

            tempMap = mapCount.get(key);
            tempMap.put(valueType, 1);
            mapCount.put(key, tempMap);

            tempMap = mapSum.get(key);
            tempMap.put(valueType, value);
            mapSum.put(key, tempMap);

            tempSet = new Set<String>();
        }
        else
        {
            tempSet = mapAggregateSet.get(key).get(valueType);
            tempSet.add(value_string);

            agg_val = mapAggregate.get(key).get(valueType);
            agg_count = mapCount.get(key).get(valueType);
            agg_sum = mapSum.get(key).get(valueType);

            tempMap = mapCount.get(key);
            tempMap.put(valueType, agg_count + 1);
            mapCount.put(key, tempMap);

            tempMap = mapSum.get(key);
            tempMap.put(valueType, agg_sum + value);
            mapSum.put(key, tempMap);


            switch on aggregationType
            {
                when 'SUM'{
                    agg_val += value;
                }
                when 'MAX'{
                    agg_val = Math.max(agg_val, value);
                }
                when 'MIN'{
                    agg_val = Math.min(agg_val, value);
                }
                when 'COUNT'{
                    agg_val = mapCount.get(key).get(valueType);
                }
                when 'AVG'{
                    agg_val = mapSum.get(key).get(valueType) / mapCount.get(key).get(valueType);
                }
                when 'COUNT_DISTINCT'{
                    agg_val = tempSet.size();
                }
            }

            tempMap = mapAggregate.get(key);
            tempMap.put(valueType, agg_val);
            mapAggregate.put(key, tempMap);

        }
        tempSet.add(value_string);

        if(!mapAggregateSet.containsKey(key))
        {
            mapAggregateSet.put(key, new Map<String, Set<String>> {valueType => tempSet});
        }
        else
        {
            tempSetMap = mapAggregateSet.get(key);
            tempSetMap.put(valueType, tempSet);
            mapAggregateSet.put(key, tempSetMap);
        }


    }
}