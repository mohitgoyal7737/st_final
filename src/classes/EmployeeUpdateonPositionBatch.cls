global with sharing class EmployeeUpdateonPositionBatch implements Database.Batchable<sObject>, Database.Stateful {
    public String query;
    public Map<String,String> mapPos2Emp;
    public Map<String,String> mapPos2EmpCP;
    public Set<String> posSet;
    public List<AxtriaSalesIQTM__Position__c> updatePosList;
    public List<AxtriaSalesIQTM__Position__c> secPosList;
    public List<AxtriaSalesIQTM__Position__c> updateEmptyEmpList;
    public Set<ID> duplicateRecs;
    //public AxtriaSalesIQTM__TriggerContol__c customsetting1;
    public AxtriaSalesIQTM__TriggerContol__c customsetting2;
    //public list<AxtriaSalesIQTM__TriggerContol__c>customsettinglist1 {get;set;}
    public list<AxtriaSalesIQTM__TriggerContol__c>customsettinglist2 {get;set;}
    public Map<String,String> posToEmpMap = new Map<String,String>();
    public Boolean errorBatches=false;
    public List<String> posCodeList=new List<String>();

    public Set<String> posSecondarySet;

    global EmployeeUpdateonPositionBatch() 
    {
        query = '';
        duplicateRecs = new Set<ID>();
        //customsetting1 = new AxtriaSalesIQTM__TriggerContol__c();
        //customsettinglist1 = new list<AxtriaSalesIQTM__TriggerContol__c>();
        
        mapPos2Emp=new Map<String,String>();
        mapPos2EmpCP=new Map<String,String>();

        posSet=new Set<String>();
        updatePosList=new List<AxtriaSalesIQTM__Position__c>();
        updateEmptyEmpList=new List<AxtriaSalesIQTM__Position__c>();
        secPosList=new List<AxtriaSalesIQTM__Position__c>();

        posSecondarySet =new Set<String>();
        /*SnTDMLSecurityUtil.printDebugMessage('=====Custom Setting enable=====');
        customsetting1 = AxtriaSalesIQTM__TriggerContol__c.getValues('UpdatePositionCode');
        customsetting1.AxtriaSalesIQTM__IsStopTrigger__c = true ;
        customsettinglist1.add(customsetting1);
        update customsettinglist1;*/

        posCodeList.add('0');
        posCodeList.add('00000');
        posCodeList.add('Unassigned');
        posCodeList.add('Unassigned Territory');

        query='select id,AxtriaSalesIQTM__Assignment_Type__c,AxtriaSalesIQTM__Employee__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c  from AxtriaSalesIQTM__Position_Employee__c where AxtriaSalesIQTM__Assignment_Status__c=\'Active\' and AxtriaSalesIQTM__Position__c != null and AxtriaSalesIQTM__Employee__c != null and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__IsMaster__c=true and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__inactive__c=false and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c not in: posCodeList';   //added master check //and AxtriaSalesIQTM__Assignment_Type__c=\'Primary\'
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Employee__c> scope)
    {
       /* try
        {*/
            posSet=new Set<String>();
            updatePosList=new List<AxtriaSalesIQTM__Position__c>();
            updateEmptyEmpList=new List<AxtriaSalesIQTM__Position__c>();
            secPosList=new List<AxtriaSalesIQTM__Position__c>();
            posSecondarySet =new Set<String>();
            

            SnTDMLSecurityUtil.printDebugMessage('=======Query:::::' +scope);

            SnTDMLSecurityUtil.printDebugMessage('Blank Employee1 on Position initially');


            SnTDMLSecurityUtil.printDebugMessage('================================Fill Employee1 field on Positon Tab==========================================');
            for(AxtriaSalesIQTM__Position_Employee__c posEmpRec : scope)
            {
                if(posEmpRec.AxtriaSalesIQTM__Assignment_Type__c == 'Primary')
                {
                    mapPos2EmpCP.put(posEmpRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,posEmpRec.AxtriaSalesIQTM__Employee__c);
                //mapPos2EmpCP.put(posEmpRec.AxtriaSalesIQTM__Position__c,posEmpRec.AxtriaSalesIQTM__Employee__c);
                mapPos2Emp.put(posEmpRec.AxtriaSalesIQTM__Position__c,posEmpRec.AxtriaSalesIQTM__Employee__c);
                SnTDMLSecurityUtil.printDebugMessage('========posEmpRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c::::' +posEmpRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
                SnTDMLSecurityUtil.printDebugMessage('========posEmpRec.AxtriaSalesIQTM__Employee__c::::' +posEmpRec.AxtriaSalesIQTM__Employee__c);
                //posSet.add(posEmpRec.AxtriaSalesIQTM__Position__c);
                }
                else if(posEmpRec.AxtriaSalesIQTM__Assignment_Type__c == 'Secondary')
                {
                    posSecondarySet.add(posEmpRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
                }
            }
            //SnTDMLSecurityUtil.printDebugMessage('=======mapPos2Emp::::' +mapPos2Emp);
            //SnTDMLSecurityUtil.printDebugMessage('========Position set::::' +mapPos2EmpCP);

            SnTDMLSecurityUtil.printDebugMessage('========Primary Assignment Employee Update=======');
            List<AxtriaSalesIQTM__Position__c> posList = [select id,Employee1__c,AxtriaSalesIQTM__Employee__c,Employee1_Assignment_Type__c, AxtriaSalesIQTM__Client_Position_Code__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Client_Position_Code__c in :mapPos2EmpCP.keySet() and AxtriaSalesIQTM__inactive__c=false and (AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c= 'Current' or AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = 'Future')];//AxtriaSalesIQTM__IsMaster__c=true];   //added master check


            for(AxtriaSalesIQTM__Position__c posRec : posList)
            {
                posRec.Employee1__c = mapPos2EmpCP.get(posRec.AxtriaSalesIQTM__Client_Position_Code__c);
                posRec.AxtriaSalesIQTM__Employee__c = mapPos2Emp.get(posRec.id);
                posRec.Employee1_Assignment_Type__c='Primary';
                updatePosList.add(posRec);
            }
            if(updatePosList.size() > 0){
                //update updatePosList;
                SnTDMLSecurityUtil.updateRecords(updatePosList, 'EmployeeUpdateonPositionBatch');
            }

            SnTDMLSecurityUtil.printDebugMessage('========Secondary Assignment Employee Update=======');
            List<AxtriaSalesIQTM__Position__c> secondaryPosList = [select id, AxtriaSalesIQTM__Client_Position_Code__c, Employee1__c,Employee1_Assignment_Type__c from AxtriaSalesIQTM__Position__c where Employee1__c = null and AxtriaSalesIQTM__Client_Position_Code__c in :posSecondarySet and AxtriaSalesIQTM__inactive__c=false and (AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c= 'Current' or AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = 'Future')];//AxtriaSalesIQTM__IsMaster__c=true];   //added master check
            
            for(AxtriaSalesIQTM__Position__c pos : secondaryPosList)
            {
                posSet.add(pos.AxtriaSalesIQTM__Client_Position_Code__c);
            }
            SnTDMLSecurityUtil.printDebugMessage('========Position set where Employee1 field is null::::' +posSet);

            List<AxtriaSalesIQTM__Position_Employee__c> secondaryPosEmpList = [select id,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Employee__c,AxtriaSalesIQTM__Position__c from AxtriaSalesIQTM__Position_Employee__c where AxtriaSalesIQTM__Assignment_Status__c = 'Active'  and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c in :posSet and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__IsMaster__c=true];   //added master check

            for(AxtriaSalesIQTM__Position_Employee__c secondaryPosEmprec : secondaryPosEmpList)
            {
                mapPos2EmpCP.put(secondaryPosEmprec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,secondaryPosEmprec.AxtriaSalesIQTM__Employee__c);
            }

            for(AxtriaSalesIQTM__Position__c pos : secondaryPosList)
            {
                pos.Employee1__c = mapPos2EmpCP.get(pos.AxtriaSalesIQTM__Client_Position_Code__c);
                pos.Employee1_Assignment_Type__c='Secondary';
                secPosList.add(pos);
            }

            SnTDMLSecurityUtil.printDebugMessage('=======secPosList size::::' +secPosList.size());
            if(secPosList.size() > 0){
                //update secPosList;
                SnTDMLSecurityUtil.updateRecords(secPosList, 'EmployeeUpdateonPositionBatch');
            }
        /*}
         catch(Exception e)
       {
            errorBatches=true;
            SnTDMLSecurityUtil.printDebugMessage('++inside catch');
            String Header='Employee Update on Position batch is failed, so delta has been haulted of this org for Today';
            tryCatchEmailAlert.veevaLoad(Header);
       }
        */
    }

    global void finish(Database.BatchableContext BC) 
    {
        SnTDMLSecurityUtil.printDebugMessage('++errorBatches'+errorBatches);
       /* if(!errorBatches)
        {*/
            PositionUpdateonEmployeeBatch obj = new PositionUpdateonEmployeeBatch();
            Database.executeBatch(obj,2000);
            Set<String> countryListis = new Set<String>();
            Set<String> countrySet = new Set<String>(); 
            countryListis = StaticTeaminstanceList.getCountriesSet();
            countrySet.addAll(countryListis);
            SnTDMLSecurityUtil.printDebugMessage('countrySet:::::::::' +countrySet);

            EmptyStatusonManagerDelta obj1 = new EmptyStatusonManagerDelta(countrySet);
            Database.executeBatch(obj1,2000);

            /*Set<String> countryListis = new Set<String>();
            Set<String> countrySet = new Set<String>(); 
            countryListis = StaticTeaminstanceList.getCountriesSet();
            countrySet.addAll(countryListis);
            SnTDMLSecurityUtil.printDebugMessage('countrySet:::::::::' +countrySet);*/
            Database.executeBatch(new BatchHandleSalesTeamAttribute(countrySet),1000);
            
            customsetting2 = new AxtriaSalesIQTM__TriggerContol__c();
            customsettinglist2 = new list<AxtriaSalesIQTM__TriggerContol__c>();
            customsetting2 = AxtriaSalesIQTM__TriggerContol__c.getValues('UpdatePositionCode');
            customsetting2.AxtriaSalesIQTM__IsStopTrigger__c = false;
            customsettinglist2.add(customsetting2);
            update customsettinglist2;
       /* }*/
    }
}