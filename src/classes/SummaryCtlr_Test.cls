@isTest
public class SummaryCtlr_Test {
    @istest static void SummaryCtlr_Test() {
        User loggedInUser = new User(id = UserInfo.getUserId());

        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;

        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;

        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;

        Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
        insert pp;
        Parameter__c pp1 = TestDataFactory.parameter(pcc, team, teamins);
        insert pp1;

        Measure_Master__c mMaster = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        insert mMaster;

        Grid_Master__c gMaster = TestDataFactory.gridMaster(countr);
        insert gMaster;

        Rule_Parameter__c rps = new Rule_Parameter__c();
        rps.Measure_Master__c = mMaster.id;
        rps.Parameter__c = pp.id;
        rps.CurrencyIsoCode = 'USD';
        insert rps;

        Compute_Master__c ccMaster = TestDataFactory.createComputeMaster(rps);
        insert ccMaster;

        Step__c steps = TestDataFactory.step(ccMaster, gMaster, mMaster, rps);
        steps.Measure_Master__c = mMaster.id;
        insert steps;
        
        List<String> testString = new List<String>();
        testString.add('Compute Values');
        testString.add('Compute Segment');
        testString.add('Compute TCF');
        testString.add('Compute Accessibility');
        testString.add('Product Priority');
        //insert testString;
        
        List<Step__c> myList = new List<Step__c>();
        myList.add(steps);
        
        Map<String, List<Step__c>> rulemap = new Map<String, List<Step__c>>();
        rulemap.put('test', myList);
        
        Rule_Parameter__c rps1 = TestDataFactory.ruleParameter(mMaster, pp, steps);
        insert rps1;
        
        Test.startTest();
        System.runAs(loggedinuser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            
            SummaryCtlr obj = new SummaryCtlr();
            obj.ruleId = mMaster.id;
            obj.countryID = countr.id;
            obj.pageName = 'Test';
            obj.selectedParameters = '["' + pp1.id + '"]';
            obj.selectedParam = 'Test';
            obj.selectedParam1 = 'Test';
            obj.productPriorityEnabled = true;
            obj.mode= 'Edit';
            obj.ruleSteps = rulemap;
            obj.summaryPath= testString;
            obj.ruleObject = mMaster;
            obj.save();
            obj.saveandnext();
            obj.hidePopup();
            obj.redirectToPageRefer();
            obj.saveFinalCopy();
            obj.save1();
        }
        Test.stopTest();
    }
}