/**
* Description   : Class containing method to insert user position records for user profiles mentioned in Total approval custom setting's 
                  GlobalAdminProfiles record.These profiles should be semicolon separated.
* Author        : Gaurav
* Since         : 5-June-2018
*/
global class InsertUserPositionRecords {
    
     global list<string> listOfProfiles = new list<string>();
     
     /*Method to create user position records for users of profiles that are set in custom setting*/
          global void insertUserPositionsBasesOnProfiles(){           
                 if(AxtriaSalesIQTM__TotalApproval__c.getValues('GlobalAdminProfiles') != null){
                        string profiles = AxtriaSalesIQTM__TotalApproval__c.getValues('GlobalAdminProfiles').AxtriaSalesIQTM__Subscriber_Profile__c;
                        system.debug('##### profiles ' + profiles);
                        if(profiles != ''){
                           listOfProfiles = profiles.split(';');
                        }
                        system.debug('##### listOfProfiles ' + listOfProfiles);
                 }
                 if(listOfProfiles.size() > 0){
                        list<user> userList = [select ID,Name from User where Profile.Name in :listOfProfiles];
                        list<string> userIds = new list<string>();
                        for(User u : userList){
                            userIds.add(u.id);
                        }
                        system.debug('##### userIds ' + userIds);
                         //Groups directly associated to user
                        map<string,list<string>> groupwithUser = new map<string,list<string>>();                  
                        //Populating the Group with User with GroupId we are filtering only  for Group of Type Regular,Role and RoleAndSubordinates
                        for(GroupMember  u :[select groupId,UserOrGroupId from GroupMember where UserOrGroupId in:userIds])
                         {
                            //groupwithUser.put(u.groupId,u.UserOrGroupId);
                            if(groupwithUser.containsKey(u.groupId)){
                                groupwithUser.get(u.groupId).add(u.UserOrGroupId);
                            }else{
                                groupwithUser.put(u.groupId,new list<string>{u.UserOrGroupId});
                            }
                         }
                        system.debug('############# groupwithUser ' + groupwithUser);       
                         set<string> groups = groupwithUser.keySet();
                         string soql = 'SELECT Id, ParentId,UserOrGroupId FROM AxtriaSalesIQTM__Country__Share where UserOrGroupId in : groups'; 
                         List<AxtriaSalesIQTM__Country__Share> sharedCountries = database.query(soql);
                         system.debug('############### sharedCountries ' + sharedCountries);    
                         
                         set<string> countryIds = new set<string>();                            
                         if(sharedCountries != null &&  sharedCountries.size()>0){  
                                 //map of userIds and list of countries assigned to them in sharing rule
                                 map<string,list<string>> mapUserIDAndCountryIds = new map<string,list<string>>();
                                 for(AxtriaSalesIQTM__Country__Share countryShare : sharedCountries){    
                                    system.debug('##### countryShare ' + countryShare);
                                    countryIds.add(countryShare.ParentId);
                                    if(groupwithUser.containsKey(countryShare.UserOrGroupId)){
                                        list<string> listUserIDs = groupwithUser.get(countryShare.UserOrGroupId);
                                        for(string userId : listUserIDs){
                                            if(mapUserIDAndCountryIds.containsKey(userId)){
                                                mapUserIDAndCountryIds.get(userId).add(countryShare.ParentId);
                                            }else{
                                                mapUserIDAndCountryIds.put(userId,new list<string>{countryShare.ParentId});
                                            }
                                        }
                                    }
                                 } 
                                 system.debug('############### mapUserIDAndCountryIds ' + mapUserIDAndCountryIds);
                                 // map of countryID and list of team instances
                                 map<string,list<string>> mapCountryIdAndTeamInstanceIds = new map<string,list<string>>();
                                 set<string> teamInstanceIDs = new set<string>();
                                 if(countryIds != null && countryIds.size() >0){
                                    list<AxtriaSalesIQTM__Team_Instance__c> lstTeamInst = [select id ,AxtriaSalesIQTM__Country__c from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Country__c in: countryIds and AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Status__c != 'Inactive']; 
                                    for(AxtriaSalesIQTM__Team_Instance__c teamInst : lstTeamInst){
                                        teamInstanceIDs.add(teamInst.id);
                                        if(mapCountryIdAndTeamInstanceIds.containsKey(teamInst.AxtriaSalesIQTM__Country__c)){
                                            mapCountryIdAndTeamInstanceIds.get(teamInst.AxtriaSalesIQTM__Country__c).add(teamInst.id);
                                        }else{
                                            mapCountryIdAndTeamInstanceIds.put(teamInst.AxtriaSalesIQTM__Country__c,new list<string>{teamInst.id});
                                        }
                                    } 
                                 }
                                 
                                 string whereClause = '';
                                 if(teamInstanceIDs != null && teamInstanceIDs.size() > 0){                             
                                    List<aggregateResult> results = [select Max(AxtriaSalesIQTM__Hierarchy_Level_Number__c)level,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__CR_Team_Instance_Config__c  where AxtriaSalesIQTM__Team_Instance__c in: teamInstanceIDs and AxtriaSalesIQTM__Configuration_Type__c = 'Hierarchy Configuration' Group By AxtriaSalesIQTM__Team_Instance__c ];
                                    system.debug('######### results ' + results);
                                    for(AggregateResult config : results){                                                          
                                        whereClause += '(AxtriaSalesIQTM__Team_Instance__c = \''+config.get('AxtriaSalesIQTM__Team_Instance__c')+'\' and AxtriaSalesIQTM__Hierarchy_Level__c = \''+config.get('level') +'\') OR ';
                                    }
                                    whereClause = whereClause.removeEnd('OR ');                             
                                    system.debug('####### Where Clause ' + whereClause);
    
                                    string query = '';
                                    query += 'Select id,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position__c where ';
                                    query += whereClause;
                                    system.debug('######### query ' + query);
                                    
                                    list<AxtriaSalesIQTM__Position__c> positions = Database.query(query);
                                    system.debug('####### positions ' + positions);
                                    //map of team instance and position id
                                    map<string,string> mapTeamInstanceIdAndPosId = new map<string,string>();
                                    for(AxtriaSalesIQTM__Position__c pos : positions){
                                        mapTeamInstanceIdAndPosId.put(pos.AxtriaSalesIQTM__Team_Instance__c,pos.id);
                                    }
                                    
                                    
                                    //Piece of code to fetch user position records for user,country,teaminstance and position   
                                    set<string> allUserIds = new set<string>();
                                    set<string> allTeamInsIds = new set<string>();
                                    set<string> allPositionIds = new set<string>();
                                    for(string user : mapUserIDAndCountryIds.keySet()){
                                        allUserIds.add(user);
                                        list<string> countriesOfUser = mapUserIDAndCountryIds.get(user);
                                        for(string country : countriesOfUser){
                                            list<string> teamInstIds = mapCountryIdAndTeamInstanceIds.get(country);
                                            if(teamInstIds != null && teamInstIds.size()>0){
                                                for(string teamInst : teamInstIds){
                                                    allTeamInsIds.add(teamInst);
                                                    string position = mapTeamInstanceIdAndPosId.get(teamInst);
                                                    if(position != null)
                                                      allPositionIds.add(position);
                                                }
                                            }
                                        }
                                    }

                                    //query on user position to fetch records that contain user,country,teaminst and position fetched above
                                    list<AxtriaSalesIQTM__User_Access_Permission__c> userPos= [select id,AxtriaSalesIQTM__User__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Country__c
                                                                              from AxtriaSalesIQTM__User_Access_Permission__c where AxtriaSalesIQTM__User__c in: allUserIds and 
                                                                              AxtriaSalesIQTM__Team_Instance__c in: allTeamInsIds and AxtriaSalesIQTM__Position__c in: allPositionIds];
                                    system.debug('####### userPos ' + userPos);                                 
                                    
                                    map<string,map<string,map<string,string>>> mapOfUserCountryTeamInstPosID = new map<string,map<string,map<string,string>>>(); // this map will contain user and then map of country and then map of team instance and position
                                    map<string,string> teamInstPosMap = new map<string,string>();
                                    map<string,map<string,string>> countryTeamInstPosMap = new map<string,map<string,string>>();
                                    
                                    for(AxtriaSalesIQTM__User_Access_Permission__c userAccess : userPos){
                                        teamInstPosMap.put(userAccess.AxtriaSalesIQTM__Team_Instance__c,userAccess.AxtriaSalesIQTM__Position__c);
                                        countryTeamInstPosMap.put(userAccess.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Country__c,teamInstPosMap);
                                        mapOfUserCountryTeamInstPosID.put(userAccess.AxtriaSalesIQTM__User__c,countryTeamInstPosMap);
                                    }
                                    system.debug('###### mapOfUserCountryTeamInstPosID ' + mapOfUserCountryTeamInstPosID);
                                    
                                                                    
                                    
                                    //Making User Position Records
                                    /* Till here we have got three maps 
                                       1. map that contains user and its countries.  2. map that contains country and its team instances.
                                       3. map that contains team instance and root level position id.
                                       In below code iterating through these maps and making user positions records list that will be inserted.
                                       excluding the records which are already there which we have collected in mapOfUserCountryTeamInstPosID.
                                    */
                                    list<AxtriaSalesIQTM__User_Access_Permission__c> toBeInsertedUserPositions = new list<AxtriaSalesIQTM__User_Access_Permission__c>();
                                    for(string userID : mapUserIDAndCountryIds.keySet()){                                   
                                        list<string> userCountries = mapUserIDAndCountryIds.get(userID);                                    
                                        for(string countryID : userCountries){                                      
                                            list<string> teamInstances = mapCountryIdAndTeamInstanceIds.get(countryID);
                                            if(teamInstances != null && teamInstances.size() > 0){
                                                for(string teamInstanceId : teamInstances){                                             
                                                    string positionID = mapTeamInstanceIdAndPosId.get(teamInstanceId);
                                                    /*fetching position from the map we formed from User Position*/
                                                    string positionExistInUserPos = '';
                                                    if(mapOfUserCountryTeamInstPosID.get(userID) != null && mapOfUserCountryTeamInstPosID.get(userID).get(countryID) != null &&
                                                       mapOfUserCountryTeamInstPosID.get(userID).get(countryID).get(teamInstanceId) != null){
                                                            positionExistInUserPos = mapOfUserCountryTeamInstPosID.get(userID).get(countryID).get(teamInstanceId);
                                                       }
                                                    /*Checking whether that user position record exist using the positions 
                                                      retieved via this loop and map formed by querying on User Position.
                                                      If it is not matched then make new user Position record and add to list.
                                                    */
                                                    if(positionID != null && positionExistInUserPos != positionID){
                                                        system.debug('########## positionID ' + positionID);    
                                                        AxtriaSalesIQTM__User_Access_Permission__c newUserPos = new AxtriaSalesIQTM__User_Access_Permission__c();
                                                        newUserPos.Name = 'User Access '+SalesIQUtility.generateRandomNumber();
                                                        newUserPos.AxtriaSalesIQTM__Position__c = positionID;
                                                        newUserPos.AxtriaSalesIQTM__Map_Access_Position__c  = positionID;
                                                        newUserPos.AxtriaSalesIQTM__Is_Active__c = true;
                                                        newUserPos.AxtriaSalesIQTM__Team_Instance__c = teamInstanceId;
                                                        newUserPos.AxtriaSalesIQTM__User__c = userID;
                                                        newUserPos.AxtriaSalesIQTM__Effective_Start_Date__c = Date.today();
                                                        newUserPos.AxtriaSalesIQTM__Effective_End_Date__c = Date.today().addYears(20);
                                                        
                                                        system.debug('######## newUserPos ' + newUserPos);
                                                        toBeInsertedUserPositions.add(newUserPos);
                                                        system.debug('######### toBeInsertedUserPositions ' + toBeInsertedUserPositions);                                               
    
                                                    }                                               
                                                }
                                            }
                                        }
                                    }
                                    
                                        if(toBeInsertedUserPositions != null && toBeInsertedUserPositions.size()>0){
                                            insert toBeInsertedUserPositions;
                                        }
                                }
                          }              
                    }
            }
   }