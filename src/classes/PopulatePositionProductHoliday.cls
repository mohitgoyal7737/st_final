global with sharing class PopulatePositionProductHoliday implements Database.Batchable<sObject>,Database.Stateful
{
    public String query;
    //List<Emp_holiday_code__c> query= new List<Emp_holiday_code__c> ();
    public String selectedTeamInstance;
    Map<String,Decimal> posCodeHolidays = new Map<String,Decimal>();
    Boolean isHolidayOverridden= false;

    global PopulatePositionProductHoliday(String teamInstance)
    {
        //this.query = query;
     selectedTeamInstance = teamInstance;
     query='Select Holiday__c,Position_code__c,PRID__c, Team_instance__c,Team_instance_Lookup__r.name from Emp_holiday_code__c where Team_instance_Lookup__c = :selectedTeamInstance';
    }
   
    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Emp_holiday_code__c> scope) 

    { 

     List<AxtriaSalesIQTM__Position_Product__c> poslist = [SELECT Id,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,Holidays__c,AxtriaSalesIQTM__Team_Instance__c FROM AxtriaSalesIQTM__Position_Product__c where AxtriaSalesIQTM__Team_Instance__c =: selectedTeamInstance];
     SnTDMLSecurityUtil.printDebugMessage('+++++poslist+++'+poslist);
     List<AxtriaSalesIQTM__Position_Product__c> positionDetails = new  List<AxtriaSalesIQTM__Position_Product__c>();
     
        for(Emp_holiday_code__c emp :scope)
        {
            posCodeHolidays.put(emp.Position_code__c,Decimal.valueOf(emp.Holiday__c));
            SnTDMLSecurityUtil.printDebugMessage('+++++posCodeHolidays+++'+posCodeHolidays);
        }
        
        for(AxtriaSalesIQTM__Position_Product__c posp : poslist)
        {
            if(posCodeHolidays.containsKey(posp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c)) 
            {
                isHolidayOverridden=true;
                posp.Holidays__c = posCodeHolidays.get(posp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c); 
                positionDetails.add(posp);
                SnTDMLSecurityUtil.printDebugMessage('+++++positionDetails+++'+positionDetails);
            } 
            
            else
            {
                   
            }
        }
        //update poslist;
        SnTDMLSecurityUtil.updateRecords(poslist, 'PopulatePositionProductHoliday');
    }

    global void finish(Database.BatchableContext BC) 
    {
     
    }
}