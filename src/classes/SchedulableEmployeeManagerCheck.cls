global class SchedulableEmployeeManagerCheck implements Schedulable{
    
    public List<AxtriaSalesIQTM__Employee__c> updateEmpList = new List<AxtriaSalesIQTM__Employee__c>();
    public List<AxtriaSalesIQTM__Employee__c> updateSecondaryEmpList = new List<AxtriaSalesIQTM__Employee__c>();
    public Map<String,List<AxtriaSalesIQTM__Position_Employee__c>> mapPos2EmpSet = new Map<String,List<AxtriaSalesIQTM__Position_Employee__c>>();
    public  SchedulableEmployeeManagerCheck(){
        /*system.debug('=========INSIDE CONSTRUCTOR+++++++++++++');
        fillmanager();
        system.debug('==================COMPLETE============');*/
    }

    global void fillmanager() {

       /*System.debug('=====Employee List========');
       List<AxtriaSalesIQTM__Employee__c> empList = [select Id,AxtriaSalesIQTM__Current_Territory__c,AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Name,AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Employee_PRID__c,Current_Position__c, CurrentPositionId__c, AxtriaSalesIQTM__Manager__c,ReportingToWorkerName__c,ReportsToAssociateOID__c,AxtriaSalesIQTM__Field_Status__c,AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__c  from AxtriaSalesIQTM__Employee__c];

       List<AxtriaSalesIQTM__Position_Employee__c> posEmpList = [select Id,AxtriaSalesIQTM__Employee__c,AxtriaSalesIQTM__Employee__r.Name,AxtriaSalesIQTM__Employee__r.Employee_PRID__c,AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Assignment_Type__c from AxtriaSalesIQTM__Position_Employee__c where AxtriaSalesIQTM__Employee__c!= null and AxtriaSalesIQTM__Position__c != null and AxtriaSalesIQTM__Assignment_Status__c= 'Active'];
       for(AxtriaSalesIQTM__Position_Employee__c posEmpRec : posEmpList){
         if(mapPos2EmpSet.containsKey(posEmpRec.AxtriaSalesIQTM__Position__c)){
            
            mapPos2EmpSet.get(posEmpRec.AxtriaSalesIQTM__Position__c).add(posEmpRec);
          }
          else{
            List<AxtriaSalesIQTM__Position_Employee__c> peList = new List<AxtriaSalesIQTM__Position_Employee__c>();
            peList.add(posEmpRec);
            mapPos2EmpSet.put(posEmpRec.AxtriaSalesIQTM__Position__c,peList);
            }
        }


        System.debug('=====Employee List========' +empList.size());
        for(AxtriaSalesIQTM__Employee__c empRec : empList){
            if(empRec.AxtriaSalesIQTM__Field_Status__c == 'Unassigned'){
               if(empRec.AxtriaSalesIQTM__Current_Territory__c == null || empRec.Current_Position__c == null){
                  empRec.AxtriaSalesIQTM__Manager__c = null;
                  //empRec.Manager1__c=null;
                  empRec.ReportingToWorkerName__c = null;
                  empRec.ReportsToAssociateOID__c = null;
                  updateEmpList.add(empRec);
                  system.debug('Employee Record======' +empRec);
               }
            }
            else if(empRec.AxtriaSalesIQTM__Field_Status__c == 'Assigned'){
               
                List<AxtriaSalesIQTM__Position_Employee__c> peRec = new List<AxtriaSalesIQTM__Position_Employee__c>();
                String parentPos=empRec.AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Parent_Position__c;
                if(mapPos2EmpSet.size() > 0 && mapPos2EmpSet.containsKey(parentPos)){
                    peRec = mapPos2EmpSet.get(parentPos);
                 }
                if(peRec.size() == 1){
                   empRec.AxtriaSalesIQTM__Manager__c = empRec.AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__c;
                   System.debug('Assigned Employee' +empRec.AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__c);
                   System.debug('Assigned Employee ID' +emprec.Id);
                   empRec.ReportingToWorkerName__c = empRec.AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Name;
                   empRec.ReportsToAssociateOID__c = empRec.AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Employee_PRID__c;
                }
                else{
                    for(AxtriaSalesIQTM__Position_Employee__c pe : peRec){
                        if(pe.AxtriaSalesIQTM__Assignment_Type__c == 'Primary'){
                           empRec.AxtriaSalesIQTM__Manager__c = pe.AxtriaSalesIQTM__Employee__c;
                           System.debug('Primary Employee' +pe.AxtriaSalesIQTM__Employee__c);
                           empRec.ReportingToWorkerName__c = pe.AxtriaSalesIQTM__Employee__r.Name;
                           empRec.ReportsToAssociateOID__c = pe.AxtriaSalesIQTM__Employee__r.Employee_PRID__c;
                           //updateEmpList.add(empRec);
                        }
                        
                    }
                }
                updateEmpList.add(empRec);
                system.debug('Employee Record======' +empRec);
            }
            
         }

       if(updateEmpList.size() > 0)
          update updateEmpList;
          //update updateSecondaryEmpList;

        System.debug('==============Secondary Employee Assignment Manager Update===========');
        List<AxtriaSalesIQTM__Employee__c> secEmpList = [select Id,AxtriaSalesIQTM__Current_Territory__c,AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Name,AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Employee_PRID__c,Current_Position__c, CurrentPositionId__c, AxtriaSalesIQTM__Manager__c,ReportingToWorkerName__c,ReportsToAssociateOID__c,AxtriaSalesIQTM__Field_Status__c,AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__c  from AxtriaSalesIQTM__Employee__c where AxtriaSalesIQTM__Manager__c=null and ReportingToWorkerName__c=null and ReportsToAssociateOID__c=null];

        if(secEmpList.size() > 0){
          for(AxtriaSalesIQTM__Employee__c eRec : secEmpList){
             eRec.AxtriaSalesIQTM__Manager__c = eRec.AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Parent_Position__r.Employee1__c;
             System.debug('Assigned Employee' +eRec.AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Parent_Position__r.Employee1__c);
             System.debug('Assigned Employee ID' +eRec.Id);
             eRec.ReportingToWorkerName__c = eRec.AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Parent_Position__r.Employee1__r.Name;
             eRec.ReportsToAssociateOID__c = eRec.AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Parent_Position__r.Employee1__r.Employee_PRID__c;
             updateSecondaryEmpList.add(eRec);
             system.debug('Secondary Employee Record======' +eRec);
          }
        }

        if(updateEmpList.size() > 0)
           update updateSecondaryEmpList;*/
   }
   public void execute(SchedulableContext sc){
        /*SchedulableEmployeeManagerCheck sem = new SchedulableEmployeeManagerCheck();
*/
    }
}