global class  DeletePositionEmployee 
{  
    global static Boolean triggerflag = false;
  
    public  static void deletePEwithEmployee(list<string>posId){
        list<AxtriaSalesIQTM__Position_Employee__c>deleteList=new  list<AxtriaSalesIQTM__Position_Employee__c>();
        list<AxtriaSalesIQTM__Position_Employee__c>PE=[select id,
                                                              AxtriaSalesIQTM__Effective_End_Date__c
                                                              ,AxtriaSalesIQTM__Effective_Start_Date__c
                                                              ,AxtriaSalesIQTM__position__c
                                                              ,AxtriaSalesIQTM__position__r.AxtriaSalesIQTM__Effective_Start_Date__c
                                                              ,AxtriaSalesIQTM__position__r.AxtriaSalesIQTM__Effective_End_Date__c,
                                                              AxtriaSalesIQTM__Employee__c
                                                              from AxtriaSalesIQTM__Position_Employee__c
                                                              where AxtriaSalesIQTM__position__c in: posId
                                                              and AxtriaSalesIQTM__Employee__c=null];
                                                              
       if(PE.size() > 0){ 
        delete PE;
       }
    }
}