/**********************************************************************************************
@author       : Himanshu Tariyal (A0994)
@createdDate  : 14th August 2020
@description  : Controller for deleting MCCP DataLoad recs based on Record types 
@Revision(s)  : v1.0
**********************************************************************************************/
global with sharing class BatchDeleteMCCPLoadData implements Database.Batchable<sObject>,Database.Stateful
{
    public String query;
    public String batchName;
    public String crID;
    public String wsID;
    public String wsCountryID;
    public String mccpRecordTypeName;
    public String alignNmsp;
    public String mccpRecTypeID;
    public Boolean isFileUploaded;
    public String securityQuery = 'WITH SECURITY_ENFORCED';

    public Boolean dataLoadDeletable = true;

    public List<SObject> crRec;

    global BatchDeleteMCCPLoadData(String changeReqID,String recordType,String workspaceID,
                                    String countryID,Boolean fileUploaded) 
    {
        batchName = 'BatchDeleteMCCPLoadData';
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : constructor invoked--');

        //Initialise Lists
        crRec = new List<SObject>();

        //Get ART Namespace
        alignNmsp = MCCP_Utility.alignmentNamespace();
        SnTDMLSecurityUtil.printDebugMessage('alignNmsp--'+alignNmsp);

        crID = changeReqID;
        mccpRecordTypeName = recordType;
        wsID = workspaceID;
        wsCountryID = countryID;
        isFileUploaded = fileUploaded;
        if(mccpRecordTypeName!=null && mccpRecordTypeName!=''){
            mccpRecTypeID = Schema.SObjectType.MCCP_DataLoad__c.getRecordTypeInfosByName().get(recordType).getRecordTypeId();
        }
        SnTDMLSecurityUtil.printDebugMessage('crID--'+crID);
        SnTDMLSecurityUtil.printDebugMessage('recordType--'+recordType);
        SnTDMLSecurityUtil.printDebugMessage('mccpRecTypeID--'+mccpRecTypeID);
        SnTDMLSecurityUtil.printDebugMessage('wsID--'+wsID);
        SnTDMLSecurityUtil.printDebugMessage('wsCountryID--'+wsCountryID);

        //Get CR associated with the Direct Load activity
        String crQuery = 'select id,'+alignNmsp+'Request_Type_Change__c,Records_Created__c '+
                         'from '+alignNmsp+'Change_Request__c where id =:crID '+securityQuery;
        crRec = Database.query(crQuery);
        SnTDMLSecurityUtil.printDebugMessage('crRec size--'+crRec.size());

        if(recordType=='Product'){
            query = 'select Id from MCCP_DataLoad__c where RecordTypeId=:mccpRecTypeID and '+
                    'Workspace__c=:wsID '+securityQuery;
        }
        else{
            query = 'select Id from MCCP_DataLoad__c where RecordTypeId=:mccpRecTypeID and '+
                    'Country__c=:wsCountryID '+securityQuery;
        }
    }

    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : start() invoked--');
        SnTDMLSecurityUtil.printDebugMessage('query--'+query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC,List<MCCP_DataLoad__c> dataLoadList) 
    {
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : execute() invoked--');

        try
        {
            if(!dataLoadList.isEmpty())
            {
                if(Schema.sObjectType.MCCP_DataLoad__c.isDeletable()){
                    SnTDMLSecurityUtil.deleteRecords(dataLoadList,batchName);
                }
                else{
                    dataLoadDeletable = false;
                }
            }
        }
        catch(Exception e)
        {
            SnTDMLSecurityUtil.printDebugMessage(batchName+' : Error in execute()--'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack trace--'+e.getStackTraceString());
            dataLoadDeletable = false;
        }
    }

    global void finish(Database.BatchableContext BC) 
    {
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : finish() invoked--');

        Boolean noJobErrors;
        String changeReqStatus;

        if(!crRec.isEmpty())
        {
            if(dataLoadDeletable)
            {
                if(mccpRecordTypeName=='Product')
                {
                    if(isFileUploaded){
                        BatchPopulateProductSegment batch = new BatchPopulateProductSegment(crID,wsCountryID,wsID);
                        Database.executeBatch(batch,2000);
                    }
                    else{
                        BatchPopulateProductSegment batch = new BatchPopulateProductSegment(crID,'MCCP Product Segment',wsCountryID,wsID);
                        Database.executeBatch(batch,2000);
                    }
                }
                else if(mccpRecordTypeName=='Channel Consent')
                {
                    if(isFileUploaded){
                        BatchPopulateChannelConsent batch = new BatchPopulateChannelConsent(crID,wsCountryID);
                        Database.executeBatch(batch,2000);
                    }
                    else{
                        BatchPopulateChannelConsent batch = new BatchPopulateChannelConsent(crID,'MCCP Channel Consent',wsCountryID);
                        Database.executeBatch(batch,2000);
                    }
                }
                else if(mccpRecordTypeName=='Channel Preference')
                {
                    if(isFileUploaded){
                        BatchPopulateChannelPreference batch = new BatchPopulateChannelPreference(crID,wsCountryID);
                        Database.executeBatch(batch,2000);
                    }
                    else{
                        BatchPopulateChannelPreference batch = new BatchPopulateChannelPreference(crID,'MCCP Channel Preference',wsCountryID);
                        Database.executeBatch(batch,2000);
                    }
                }
            }
            else
            {
                //Update temp obj recs as well as CR rec
                BatchUpdateTempObjRecsCR batchCall = new BatchUpdateTempObjRecsCR(crID,false,'Please provide Delete access to the MCCP_DataLoad__c object','Error');
                Database.executeBatch(batchCall,2000);
            }
        }
    }
}