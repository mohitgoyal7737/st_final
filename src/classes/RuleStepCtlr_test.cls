@isTest
public class RuleStepCtlr_test {
    @istest static void ruletest()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        AxtriaSalesIQTM__Organization_Master__c om=new AxtriaSalesIQTM__Organization_Master__c();
        om.name='ES';
        om.CurrencyIsoCode='USD';
        om.AxtriaSalesIQTM__Parent_Country_Level__c=true;
        om.AxtriaSalesIQTM__Org_Level__c='Global';
        insert om;
        
        AxtriaSalesIQTM__Country__c c=new AxtriaSalesIQTM__Country__c();
        c.name='ES';
        c.CurrencyIsoCode='USD';
        c.AxtriaSalesIQTM__Parent_Organization__c=om.id;
        c.AxtriaSalesIQTM__Status__c='Active';
        insert c;
        
        Grid_Master__c gm=new Grid_Master__c();
        gm.name='test';
        gm.Country__c=c.id;
        gm.CurrencyIsoCode='USD';
        insert gm;
        
        Step__c s = new Step__c();
        s.Matrix__c = gm.Id;
        insert s;
       
        
        Test.startTest();
        System.runAs(loggedinuser){
            RuleStepCtlr obj=new RuleStepCtlr();
            obj.stepObj = s;
            obj.showGridFields();
        }

        Test.stopTest();
    }
}