@isTest

Public class MasterdataSyncSchedulerTest {
public static String CRON_EXP = '0 0 0 28 2 ? 2022';

static testmethod void testschedule(){
   
    AxtriaSalesIQTM__Data_Set__c Dataset = new AxtriaSalesIQTM__Data_Set__c();
Dataset.AxtriaSalesIQTM__Data_Set_Object_Name__c  = 'abcd';
Dataset.AxtriaSalesIQTM__destination__c = 'Asia';
Dataset.AxtriaSalesIQTM__is_internal__c = false;
Dataset.AxtriaSalesIQTM__Is_Master__c = True;
Dataset.AxtriaSalesIQTM__SalesIQ_Internal__c =  True;
 //   Dataset.AxtriaSalesIQTM__Data_Objects__c = datobj.id;
insert Dataset;
    
    AxtriaSalesIQTM__Data_Object__c  datobj = new AxtriaSalesIQTM__Data_Object__c();
datobj.name = 'XYZ';
    datobj.AxtriaSalesIQTM__dataset_id__c = Dataset.Id;
    insert datobj;





AxtriaSalesIQTM__ETL_Config__c ETLConf = new AxtriaSalesIQTM__ETL_Config__c();
ETLConf.name = 'BRMS';
ETLConf.AxtriaSalesIQTM__SF_UserName__c = 'axtria@gmail.com';
ETLConf.AxtriaSalesIQTM__SF_Password__c = 'axtria@123';
ETLConf.AxtriaSalesIQTM__S3_Security_Token__c = 'asdsd1234';
insert ETLConf;

Test.StartTest();

String jobId = System.schedule('MasterdataSyncScheduler',
CRON_EXP,
new MasterdataSyncScheduler());

Test.StopTest();
}
}