public with sharing class UpdateLevel5Position {

        public void updateLevel5field(ID ti){

                List<AxtriaSalesIQTM__Position__c> listPos  = new List<AxtriaSalesIQTM__Position__c>();
                List<AxtriaSalesIQTM__Position__c> POS1 = new List<AxtriaSalesIQTM__Position__c>();

                string level5Position='';

                POS1 = [Select id from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_Instance__c =:ti and AxtriaSalesIQTM__Hierarchy_Level__c ='5' WITH SECURITY_ENFORCED ];
                SnTDMLSecurityUtil.printDebugMessage('+++++level 5 id'+level5Position);
                for( AxtriaSalesIQTM__Position__c pp : POS1){

                        level5Position+=pp.id+',';
                }
                level5Position=level5Position.removeEnd(',');

                for(AxtriaSalesIQTM__Position__c pos : [Select id, AxtriaSalesIQTM__Level_5_Position__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_Instance__c =:ti and  (not AxtriaSalesIQTM__Hierarchy_Level__c in ('1','2','3','4','5')) WITH SECURITY_ENFORCED]){
                        SnTDMLSecurityUtil.printDebugMessage('+++++POS1 '+POS1);
                        if(POS1 != null){

                                pos.AxtriaSalesIQTM__Level_5_Position__c = level5Position;
                                listPos.add(pos);
                                SnTDMLSecurityUtil.printDebugMessage('+++++listPos'+listPos);
                        }
                }
        //update listPos;
                SnTDMLSecurityUtil.updateRecords(listPos, 'UpdateLevel5Position');

        }    
}