public with sharing class Veeva_Jobs {

     public String selectedJob  {get;set;}
     public List<SelectOption> jobs  {get;set;}
     //public List<SelectOption> jobs1  {get;set;}
         
     public Veeva_Jobs()
     {
        jobs= new List<SelectOption>();
        //jobs1= new List<SelectOption>();
        fetchdata();
        //fetchTalendList();
     }

       
     public void fetchdata()
     {
       jobs= new List<SelectOption>();
       jobs.add(new SelectOption('','-None'));
       jobs.add(new SelectOption('GAS Assignment','GAS Assignment'));
       jobs.add(new SelectOption('Product Metrics','Product Metrics'));
       jobs.add(new SelectOption('TSF','TSF'));
       jobs.add(new SelectOption('ATL','ATL'));
       jobs.add(new SelectOption('Veeva Hierarchy','Veeva Hierarchy'));
       jobs.add(new SelectOption('Call Plan','Call Plan'));
       jobs.add(new SelectOption('MySetUp Product','MySetUp Product'));
       jobs.add(new SelectOption('User Role Manager Job','User Role Manager Job'));
       
     }

     /*public void fetchTalendList()
     {
       jobs1= new List<SelectOption>();
       jobs1.add(new SelectOption('','-None'));
       jobs1.add(new SelectOption('GAS Assignment','GAS Assignment'));
     }*/

     public void Full_Job()
     {
        system.debug('++++++selectedJob+++'+selectedJob);    
        if(selectedJob=='GAS Assignment')
        {
            system.debug('++++++selectedJob+++'+selectedJob);
            Integration_Batch_GAS_History_Data2 gas= new Integration_Batch_GAS_History_Data2();
            Database.executeBatch(gas,2000);
            System.debug('<><><>Full_Job<><><>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Please check apex jobs for Integration_Batch_GAS_History_Data2 !!'));
        }
        else if(selectedJob=='Product Metrics')
        {
            list<string> teaminstancelistis = new list<string>();
            teaminstancelistis = StaticTeaminstanceList.getFullLoadTeamInstancesCallPlan();
            system.debug('++++++teaminstancelistis+++'+teaminstancelistis);
            Batch_Integration_ProductMetrics_EU prod= new Batch_Integration_ProductMetrics_EU(teaminstancelistis);
            Database.executeBatch(prod,2000);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm,'Please check apex jobs for Batch_Integration_ProductMetrics_EU !!'));
        }
        else if(selectedJob=='TSF')
        {
            List<String> teaminstancelistis = new List<String>();
            teaminstancelistis = StaticTeaminstanceList.getFullLoadTeamInstancesCallPlan();
            system.debug('++++++teaminstancelistis+++'+teaminstancelistis);
            Batch_Integration_TSF_New tsf= new Batch_Integration_TSF_New(teaminstancelistis);
            Database.executeBatch(tsf,2000);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm,'Please check apex jobs for Batch_Integration_TSF_New !!'));
        }
        //Commented since ATL_TransformationBatch has been removed
        /*else if(selectedJob=='ATL')
        {
            List<String> countryList = new List<String>();
            countryList = StaticTeaminstanceList.getFulloadCountries();
            system.debug('++++++countryList+++'+countryList);
            ATL_TransformationBatch atl= new ATL_TransformationBatch(countryList);
            Database.executeBatch(atl,2000);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm,'Please check apex jobs for ATL_TransformationBatch !!'));
        }*/
        else if(selectedJob=='Veeva Hierarchy')
        {
            BatchPopulateVeevaHierarchy hier= new BatchPopulateVeevaHierarchy();
            Database.executeBatch(hier,2000);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm,'Please check apex jobs for BatchPopulateVeevaHierarchy !!'));
        }
        else if(selectedJob=='Call Plan')
        {
            List<String> teaminstancelistis = new List<String>();
            teaminstancelistis = StaticTeaminstanceList.filldataCallPlan();   
            system.debug('++++++teaminstancelistis+++'+teaminstancelistis);
            AZ_Integration_Pack pack= new AZ_Integration_Pack(teaminstancelistis);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm,'Please check apex jobs for AZ_Integration_Pack !!'));
            
        }
        else if(selectedJob=='MySetUp Product')
        {
            List<String> teaminstancelistis = new List<String>();
            teaminstancelistis = StaticTeaminstanceList.filldataCallPlan();
            system.debug('++++++teaminstancelistis+++'+teaminstancelistis);
            MySetUpProd_Insert_Utility prod= new MySetUpProd_Insert_Utility(teaminstancelistis);
            Database.executeBatch(prod,2000); 
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm,'Please check apex jobs for MySetUpProd_Insert_Utility !!'));         
        }
        else if(selectedJob=='User Role Manager Job')
        {
            EmptyEmployee1onPosition manager= new EmptyEmployee1onPosition();
            Database.executeBatch(manager,2000); 
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm,'Please check apex jobs for EmptyEmployee1onPosition !!'));
        }

     }
    
     public void Delta_Job()
     {
         system.debug('++++++selectedJob+++'+selectedJob);    
        // if(selectedJob=='GAS Assignment')
        // {
        //     system.debug('++++++selectedJob+++'+selectedJob);
        //     Integration_Batch_GAS_History_Data2 gas= new Integration_Batch_GAS_History_Data2(teaminstancelist);
        //     Database.executeBatch(gas,2000);
        // }
        // else if(selectedJob=='Product Metrics')
        // {
        //     Batch_Integration_ProductMetrics prod= new Batch_Integration_ProductMetrics(teaminstancelist);
        //     Database.executeBatch(prod,2000);
        // }
        // else if(selectedJob=='TSF')
        // {
        //     Batch_Integration_TSF_New tsf= new Batch_Integration_TSF_New(teaminstancelist);
        //     Database.executeBatch(tsf,2000);
        // }
        // else if(selectedJob=='ATL')
        // {
        //     ATL_TransformationBatch atl= new ATL_TransformationBatch(countryList);
        //     Database.executeBatch(atl,2000);
        // }
        // else if(selectedJob=='Veeva Hierarchy')
        // {
        //     BatchPopulateVeevaHierarchy hier= new BatchPopulateVeevaHierarchy();
        //     Database.executeBatch(hier,2000);
        // }
        // else if(selectedJob=='Call Plan')
        // {
        //     AZ_Integration_Pack pack= new AZ_Integration_Pack(teaminstancelist);
            
        // }
        // else if(selectedJob=='MySetUp Product')
        // {
        //     MySetUpProd_Insert_Utility prod= new MySetUpProd_Insert_Utility(teaminstancelist);
        //     Database.executeBatch(prod,2000);        
        // }
     }

     /*public void Hit_TalendJob()
     {
         system.debug('++++++selectedJob+++'+selectedJob);    
        // if(selectedJob=='GAS Assignment')
        // {
        //     system.debug('++++++selectedJob+++'+selectedJob);
        //     Integration_Batch_GAS_History_Data2 gas= new Integration_Batch_GAS_History_Data2(teaminstancelist);
        //     Database.executeBatch(gas,2000);
        // }
     }*/
}