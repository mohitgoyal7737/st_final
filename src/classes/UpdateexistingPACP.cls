global with sharing class UpdateexistingPACP implements Database.Batchable<sObject>, Database.Stateful{
integer counter = 0;
AxtriaSalesIQTM__TriggerContol__c customsetting = new AxtriaSalesIQTM__TriggerContol__c();
ErrorMessageCallCapacity__c customcount = new ErrorMessageCallCapacity__c();
    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        String query = 'Select id,Sequence__c,AxtriaSalesIQTM__isincludedCallPlan__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__isincludedCallPlan__c = true'; 
        return Database.getQueryLocator(query);
    }
     global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scope) {
         customsetting = AxtriaSalesIQTM__TriggerContol__c.getValues('CallPlanSummaryTrigger');
	 if(customsetting != null)
         {
         customsetting.AxtriaSalesIQTM__IsStopTrigger__c = true ;
         update customsetting;
	}
	else
         {
             customsetting = new AxtriaSalesIQTM__TriggerContol__c();
             customsetting.Name = 'CallPlanSummaryTrigger';
             customsetting.AxtriaSalesIQTM__IsStopTrigger__c = true;
             insert customsetting;
         }
         customcount = ErrorMessageCallCapacity__c.getValues('Sequence');
         if(scope.size()>0)
         {
             if(customcount!= null)
             {
                 for(integer i=0;i<scope.size();i++)
                 {
                     counter++;
                     scope[i].Sequence__c = counter;
                     customcount.ErrorMessage__c = String.valueOf(scope[i].Sequence__c);
                 }
                 update customcount;
             }
             else
             {
                 customcount = new ErrorMessageCallCapacity__c();
                 customcount.Name = 'Sequence';
                 for(integer i=0;i<scope.size();i++)
                 {
                     counter++;
                     scope[i].Sequence__c = counter;
                     customcount.ErrorMessage__c = String.valueOf(scope[i].Sequence__c);
                 }
                 insert customcount;
             }
         SnTDMLSecurityUtil.updateRecords(scope, 'UpdateexistingPACP');
         }
     }
     global void finish(Database.BatchableContext BC) {
         customsetting.AxtriaSalesIQTM__IsStopTrigger__c = customsetting.AxtriaSalesIQTM__IsStopTrigger__c ? false : customsetting.AxtriaSalesIQTM__IsStopTrigger__c;
         update customsetting;
     }
}