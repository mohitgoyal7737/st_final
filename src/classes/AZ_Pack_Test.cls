global class AZ_Pack_Test implements Schedulable
 {
    string teamInstanceSelected;
    String status;
    List<Veeva_Market_Specific__c>  veevaCriteria;
    List<String> allTeamInstances;
    list<string>teaminstancelistis;
    public Boolean check ;
    public String cycle{get;set;}
    public Set<String> teamInsSet;
    public integer errorcount{get;set;}
    public String nonProcessedAcs {get;set;}
    public String batchID;
    global DateTime lastjobDate=null;
    public Integer recordsProcessed=0;
    public date lmd;
	public Boolean chaining = false;
    global AZ_Pack_Test(string teamInstance)
    {

        teamInstanceSelected = teamInstance;
        
       create_MC_Cycle();
    }
     
    global AZ_Pack_Test(List<String> teamInstance)
    {
         //lmd= lastModifiedDate;
         List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
          List<AxtriaSalesIQTM__Team_Instance__c> cycleList = new List<AxtriaSalesIQTM__Team_Instance__c>();
         cycleList=[Select Id,Name, Cycle__r.Name from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current' and Id in :teamInsSet];
          if(cycleList!=null)
          {
            for(AxtriaSalesIQTM__Team_Instance__c t1: cycleList)
            {
                if(t1.Cycle__r.Name !=null && t1.Cycle__r.Name !='')
                cycle = t1.Cycle__r.Name;
            }
        }
         
        System.debug(cycle);
         schLogList=[Select Id,CreatedDate,Created_Date2__c from Scheduler_Log__c where Job_Name__c='MCCP Delta' and Job_Status__c='Successful'  Order By CreatedDate desc];
        if(schLogList.size()>0)
        {
            lastjobDate=schLogList[0].Created_Date2__c.addDays(-1);  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
        else
        {
            lastjobDate=System.today() - 3;       //else we set the lastjobDate to null
        }
        System.debug('last job'+lastjobDate);
        Scheduler_Log__c sJob = new Scheduler_Log__c();        
        sJob.Job_Name__c = 'MCCP Delta';
        sJob.Job_Status__c = 'Failed';
        sJob.Job_Type__c='Outbound';
        if(cycle!=null && cycle!='')
           sJob.Cycle__c=cycle;
        sJob.Created_Date2__c = datetime.now();
        system.debug(datetime.now());
        system.debug(sJob);
        insert sJob;
        batchID = sJob.Id;
        recordsProcessed =0;
    
        allTeamInstances = new List<String>(teamInstance);
        
       create_MC_Cycle();
    }
     
     global AZ_Pack_Test(List<String> teamInstance, Boolean chain)
    {
        chaining = chain;
         //lmd= lastModifiedDate;
         List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
          List<AxtriaSalesIQTM__Team_Instance__c> cycleList = new List<AxtriaSalesIQTM__Team_Instance__c>();
         cycleList=[Select Id,Name, Cycle__r.Name from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current' and Id in :teamInsSet];
          if(cycleList!=null)
          {
            for(AxtriaSalesIQTM__Team_Instance__c t1: cycleList)
            {
                if(t1.Cycle__r.Name !=null && t1.Cycle__r.Name !='')
                cycle = t1.Cycle__r.Name;
            }
        }
         
        System.debug(cycle);
         schLogList=[Select Id,CreatedDate,Created_Date2__c from Scheduler_Log__c where Job_Name__c='MCCP Delta' and Job_Status__c='Successful'  Order By CreatedDate desc];
        if(schLogList.size()>0)
        {
            lastjobDate=schLogList[0].Created_Date2__c.addDays(-1);  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
        else
        {
            lastjobDate=System.today() - 3;       //else we set the lastjobDate to null
        }
        System.debug('last job'+lastjobDate);
        Scheduler_Log__c sJob = new Scheduler_Log__c();        
        sJob.Job_Name__c = 'MCCP Delta';
        sJob.Job_Status__c = 'Failed';
        sJob.Job_Type__c='Outbound';
        if(cycle!=null && cycle!='')
           sJob.Cycle__c=cycle;
        sJob.Created_Date2__c = datetime.now();
        system.debug(datetime.now());
        system.debug(sJob);
        insert sJob;
        batchID = sJob.Id;
        recordsProcessed =0;
    
        allTeamInstances = new List<String>(teamInstance);
        
       create_MC_Cycle();
    }

    global AZ_Pack_Test(string teamInstance, Integer count)
    {
        teamInstanceSelected = teamInstance;
        
      // create_MC_Cycle_Plan_Target_vod();
    }
   
      global void create_MC_Cycle()
    {
        List<AxtriaSalesIQTM__Team_Instance__c> allTeamInstanceRecs = new List<AxtriaSalesIQTM__Team_Instance__c>();
        veevaCriteria= new List<Veeva_Market_Specific__c>();
        
        allTeamInstanceRecs = [select Name,AxtriaSalesIQTM__Team__r.Name, AxtriaSalesIQTM__IC_EffstartDate__c, AxtriaARSnT__Increment_Field__c, AxtriaSalesIQTM__IC_EffendDate__c, AxtriaSalesIQTM__Team_Cycle_Name__c, AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Base_Team_Name__c, AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Team_Instance__c where id in :allTeamInstances];

        String selectedMarket = [select AxtriaSalesIQTM__Team__r.Country_Name__c from AxtriaSalesIQTM__Team_Instance__c where id = :allTeamInstances[0]].AxtriaSalesIQTM__Team__r.Country_Name__c;
        
        veevaCriteria = [select Channel_Criteria__c,Market__c,MC_Cycle_Threshold_Max__c,MC_Cycle_Threshold_Min__c,MC_Cycle_Channel_Record_Type__c,MC_Cycle_Plan_Channel_Record_Type__c,MC_Cycle_Plan_Product_Record_Type__c,MC_Cycle_Plan_Record_Type__c,MC_Cycle_Plan_Target_Record_Type__c,MC_Cycle_Product_Record_Type__c,MC_Cycle_Record_Type__c from Veeva_Market_Specific__c where Market__c = :selectedMarket];
        
        List<SIQ_MC_Cycle_vod_O__c> mcRecs = new List<SIQ_MC_Cycle_vod_O__c>();
         
        for(AxtriaSalesIQTM__Team_Instance__c teamInstanceRecs : allTeamInstanceRecs)
        {
            AxtriaARSnT__SIQ_MC_Cycle_vod_O__c mc = new AxtriaARSnT__SIQ_MC_Cycle_vod_O__c();
            mc.Name                          =   teamInstanceRecs.Name;             // Do Modifications in Name (Add Year to Name with Underscores)
            
            
            DateTime dtStart                 =   teamInstanceRecs.AxtriaSalesIQTM__IC_EffstartDate__c;
            Date myDateStart                 =   date.newinstance(dtStart.year(), dtStart.month(), dtStart.day());

            DateTime dtEnd                   =   teamInstanceRecs.AxtriaSalesIQTM__IC_EffendDate__c;
            Date myDateEnd                   =   date.newinstance(dtEnd.year(), dtEnd.month(), dtEnd.day());

            String dateWithoutTimeStamp      =   dtStart.year() + '-0' + dtStart.month() + '-' + dtStart.day();
            
            mc.AxtriaARSnT__SIQ_Start_Date_vod__c             =   myDateStart;
            mc.AxtriaARSnT__SIQ_End_Date_vod__c               =   myDateEnd;
            ///mc.AZ_Business_Unit__c          =   teamInstanceRecs.Team_Cycle_Name__c; // CNS or Immunolgy : Take Business Unit of AZ
            //mc.AZ_Country_Code__c           =   teamInstanceRecs.Team__r.Base_Team_Name__c;        // Take from Orbit tables_For Callmax File
            //mc.Calculate_Pull_Through_vod__c =   true;  
            mc.AxtriaARSnT__SIQ_Description_vod_del__c            =   'Cycle for MCCP';
            //mc.AZ_External_ID__c            =   mc.AZ_Country_Code__c + '_' + 'CY' + '_' + dateWithoutTimeStamp + '_' + String.valueof(teamInstanceRecs.Increment_Field__c);
            mc.AxtriaARSnT__SIQ_Country_Code_AZ__c            =   teamInstanceRecs.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
            mc.SIQ_Record_Type__c                =   veevaCriteria[0].MC_Cycle_Record_Type__c;
            mc.Over_Reached_Threshold_vod__c     =   veevaCriteria[0].MC_Cycle_Threshold_Max__c;
            mc.Under_Reached_Threshold_vod__c    =   veevaCriteria[0].MC_Cycle_Threshold_Min__c;
                
            if(myDateEnd.daysBetween(Date.today()) > 0)
            {
                mc.AxtriaARSnT__SIQ_Status_vod__c                 =   'Planned_vod';
                mc.AxtriaARSnT__SIQ_Calculate_Pull_Through_vod__c      = true;
                
            }
            else
            {
                mc.AxtriaARSnT__SIQ_Status_vod__c                 =   'In_Progress_vod';
                mc.AxtriaARSnT__SIQ_Calculate_Pull_Through_vod__c      = false;

            }
            status =  mc.AxtriaARSnT__SIQ_Status_vod__c ;
            mc.AxtriaARSnT__SIQ_External_Id_vod__c            =   mc.AxtriaARSnT__SIQ_Country_Code_AZ__c  + '_'+ 'CY' + '_' + dateWithoutTimeStamp + '_' + String.valueof(teamInstanceRecs.AxtriaARSnT__Increment_Field__c);
            mcRecs.add(mc);
        }
        
        upsert mcRecs AxtriaARSnT__SIQ_External_Id_vod__c;
        
        createMC_Cycle_Plan();
    }
    
    global void createMC_Cycle_Plan() 
    {
        List<AxtriaSalesIQTM__Position__c> posTeamInstanceRecs = [select id, AxtriaARSnT__Original_Country_Code__c, AxtriaSalesIQTM__Team_Instance__r.Name , AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name, AxtriaSalesIQTM__Team_Instance__r.AxtriaARSnT__Increment_Field__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c , AxtriaSalesIQTM__Client_Position_Code__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances and AxtriaSalesIQTM__Hierarchy_Level__c ='1' and (AxtriaSalesIQTM__Client_Position_Code__c != '000000' and AxtriaSalesIQTM__Client_Position_Code__c != '0' and AxtriaSalesIQTM__Client_Position_Code__c != '00000') and AxtriaSalesIQTM__Inactive__c = false];
        
        List<AxtriaARSnT__SIQ_MC_Cycle_Plan_vod_O__c> mcCyclePlan = new List<AxtriaARSnT__SIQ_MC_Cycle_Plan_vod_O__c>();
        
        List<AxtriaSalesIQTM__Position_Employee__c> uap = [select AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaARSnT__Team_Instance_Name__c, AxtriaSalesIQTM__Employee__r.AxtriaARSnT__Employee_PRID__c from AxtriaSalesIQTM__Position_Employee__c where AxtriaARSnT__Team_Instance__c in :allTeamInstances and AxtriaSalesIQTM__Assignment_Type__c  = 'Primary' and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Hierarchy_Level__c = '1' and (AxtriaSalesIQTM__Assignment_Status__c = 'Active' or AxtriaSalesIQTM__Assignment_Status__c = 'Future Active')];
        
        Map<String,String> posTeamToUser = new Map<String,String>();
         
        for(AxtriaSalesIQTM__Position_Employee__c u: uap)
        {
            string teamInstancePos = u.AxtriaARSnT__Team_Instance_Name__c +'_'+ u.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
            posTeamToUser.put(teamInstancePos ,u.AxtriaSalesIQTM__Employee__r.AxtriaARSnT__Employee_PRID__c);
        }
        
        system.debug('++++++++ '+posTeamToUser);
        Set<String> allTeams = new Set<String>();
        Set<String> uniqueRec = new Set<String>();

        for(AxtriaSalesIQTM__Position__c pti : posTeamInstanceRecs)
        {
            AxtriaARSnT__SIQ_MC_Cycle_Plan_vod_O__c mcp = new AxtriaARSnT__SIQ_MC_Cycle_Plan_vod_O__c();
            string teamInstancePos = pti.AxtriaSalesIQTM__Team_Instance__r.Name +'_' + pti.AxtriaSalesIQTM__Client_Position_Code__c; // Use Client Position Code for Territory ID
            string userID = posTeamToUser.get(teamInstancePos);
            //userID = 'U1234';
            DateTime dtStart                 =   pti.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c;
            Date myDateStart                 =   date.newinstance(dtStart.year(), dtStart.month(), dtStart.day());

            String dateWithoutTimeStamp      =   dtStart.year() + '-0' + dtStart.month() + '-' + dtStart.day();
            
            mcp.Name                    =   teamInstancePos;
            mcp.AxtriaARSnT__SIQ_Country_Code_AZ__c = pti.AxtriaARSnT__Original_Country_Code__c;
            
            //mcp.AZ_Country_Code__c     =   pti.Team_Instance_ID__r.Team__r.Base_Team_Name__c; // Same as Earlier in MC Cycle
            mcp.AxtriaARSnT__SIQ_Cycle_vod__c            =   pti.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c + '_' + 'CY' + '_' + dateWithoutTimeStamp + '_' + String.valueof(pti.AxtriaSalesIQTM__Team_Instance__r.AxtriaARSnT__Increment_Field__c);
            if(userID == null)
            {
                userID = '#NA';
            }
            mcp.AxtriaARSnT__SIQ_Owner_Id__c             =   userID;                                 // Federation ID
            mcp.AxtriaARSnT__SIQ_Description_vod__c      =   pti.AxtriaSalesIQTM__Team_Instance__r.Name ;       // Give Cycle Name Data
            mcp.AxtriaARSnT__SIQ_Territory_vod__c        =   pti.AxtriaSalesIQTM__Client_Position_Code__c;   // Add Client Position Code instead of Salesforce ID
           // mcp.AZ_External_ID_vod__c  =   mcp.Name +'_'+mcp.Territory_vod__c;                          // Create this new field
            mcp.AxtriaARSnT__SIQ_External_Id_vod__c      =   teamInstancePos;
            mcp.AxtriaARSnT__SIQ_Lock_vod__c             =   'false';                              // Add this new field
            mcp.AxtriaARSnT__SIQ_Channel_Interaction_Status_vod__c    = 'On_Schedule_vod';
            mcp.AxtriaARSnT__SIQ_Product_Interaction_Status_vod__c     = 'On_Schedule_vod'; 
           mcp.AxtriaARSnT__SIQ_RecordType__c             = veevaCriteria[0].MC_Cycle_Plan_Record_Type__c;
            mcp.AxtriaARSnT__SIQ_Status_vod__c                 =   status;
            
            if(!uniqueRec.contains(mcp.AxtriaARSnT__SIQ_External_Id_vod__c))
            {
                uniqueRec.add(mcp.AxtriaARSnT__SIQ_External_Id_vod__c);
                mcCyclePlan.add(mcp);    
            }
           
                
            
        }
        
        upsert mcCyclePlan AxtriaARSnT__SIQ_External_Id_vod__c;
        
        createMC_Cycle_Channel_vod();
    }
    
    global void createMC_Cycle_Channel_vod()
    {
        List<AxtriaSalesIQTM__Team_Instance__c> teamInstanceRecs = [select id, Name, AxtriaSalesIQTM__IC_EffstartDate__c, AxtriaARSnT__Increment_Field__c, AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Base_Team_Name__c, AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Team_Instance__c where id in :allTeamInstances];
        //List<Channel_Weights__c> channelAndWeights = [select Name, Weights__c, Channel_Criteria_vod__c from Channel_Weights__c];

        //String selectedMarket = [select AxtriaSalesIQTM__Team__r.AxtriaARSnT__Country_Name__c from AxtriaSalesIQTM__Team_Instance__c where id = :teamInstanceSelected].AxtriaSalesIQTM__Team__r.AxtriaARSnT__Country_Name__c;
        
        //List<AxtriaARSnT__Veeva_Market_Specific__c>  veevaCriteria = [select AxtriaARSnT__Channel_Criteria__c,AxtriaARSnT__Market__c from AxtriaARSnT__Veeva_Market_Specific__c where AxtriaARSnT__Market__c = :selectedMarket];
        
        String selectedCriteria;

        if(veevaCriteria.size()>0)
        {
            selectedCriteria = veevaCriteria[0].AxtriaARSnT__Channel_Criteria__c;
        }
        else
        {
            selectedCriteria = 'Not Found';
        }
        Set<String> uniqueCheck = new Set<String>();

        List<AxtriaARSnT__SIQ_MC_Cycle_Channel_vod_O__c> mcCycleChannel = new List<AxtriaARSnT__SIQ_MC_Cycle_Channel_vod_O__c>();
        
        for(AxtriaSalesIQTM__Team_Instance__c teamInstance : teamInstanceRecs)
        {
            //for(Channel_Weights__c cw : channelAndWeights)
            //{
                AxtriaARSnT__SIQ_MC_Cycle_Channel_vod_O__c mcc = new AxtriaARSnT__SIQ_MC_Cycle_Channel_vod_O__c();
                
                DateTime dtStart                 =   teamInstance.AxtriaSalesIQTM__IC_EffstartDate__c;
                Date myDateStart                 =   date.newinstance(dtStart.year(), dtStart.month(), dtStart.day());

                String dateWithoutTimeStamp      =   dtStart.year() + '-0' + dtStart.month() + '-' + dtStart.day(); 
                
                //mcc.CHANNEL_LABEL_VOD__C    =   'f2f';
               /* 
                if(cw.Name.contains('Email'))
                {
                    mcc.SIQ_Channel_Object_vod__c   =   'Sent_Email_vod__c';
                }
                else*/
                mcc.AxtriaARSnT__SIQ_Channel_Object_vod__c   =   'Call2_vod__c';
                mcc.AxtriaARSnT__SIQ_Country_Code_AZ__c      =    teamInstance.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                String country = teamInstance.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;  
                mcc.AxtriaARSnT__SIQ_Channel_Criteria_vod__c =   selectedCriteria;//the Channels, Add this field in Channel Weights object
                mcc.AxtriaARSnT__SIQ_Channel_Weight_vod__c   =   1;
                mcc.AxtriaARSnT__SIQ_Cycle_vod__c            =   country +'_' + 'CY' + '_' + dateWithoutTimeStamp + '_' + String.valueof(teamInstance.AxtriaARSnT__Increment_Field__c);
                mcc.AxtriaARSnT__SIQ_External_Id_vod__c      =   teamInstance.Name + '-f2f-' + mcc.AxtriaARSnT__SIQ_Channel_Object_vod__c;
                //mcc.AZ_External_Id__c      =   teamInstance.Name + '-' + cw.Name + '-' + mcc.SIQ_Channel_Object_vod__c; // Not Required as of now. May be used in future;
                mcc.AxtriaARSnT__SIQ_RecordTypeId__c         =   veevaCriteria[0].MC_Cycle_Channel_Record_Type__c;        // Add this static Field
                mcc.AxtriaARSnT__External_ID_Axtria__c = mcc.AxtriaARSnT__SIQ_External_Id_vod__c;

                if(!uniqueCheck.contains(mcc.AxtriaARSnT__SIQ_External_Id_vod__c))
                {
                    mcCycleChannel.add(mcc);
                    uniqueCheck.add(mcc.AxtriaARSnT__SIQ_External_Id_vod__c);
                }
            //}
        }
        
        upsert mcCycleChannel AxtriaARSnT__External_ID_Axtria__c; 
        createMC_Cycle_Product_vod();
        
    }
    
    List<String> allChannels = new List<String>{'F2F'};    

    global void createMC_Cycle_Product_vod()
    {
        //String teamInstanceName = [select Name from AxtriaSalesIQTM__Team_Instance__c where id = :teamInstanceSelected LIMIT 1].Name;

        List<AxtriaARSnT__Product_Catalog__c> allProductsAndWeights = [select Name, AxtriaARSnT__Weights__c,Detail_Group__c, AxtriaARSnT__Veeva_External_ID__c, AxtriaARSnT__Team_Instance__c from  AxtriaARSnT__Product_Catalog__c where AxtriaARSnT__Team_Instance__c in :allTeamInstances];

        Map<String, List<AxtriaARSnT__Product_Catalog__c>> teamInstanceWisePC = new Map<String, List<AxtriaARSnT__Product_Catalog__c>>();

        for(AxtriaARSnT__Product_Catalog__c pc : allProductsAndWeights)
        {
            List<AxtriaARSnT__Product_Catalog__c> tempPC = new List<AxtriaARSnT__Product_Catalog__c>();

            if(teamInstanceWisePC.containsKey(pc.AxtriaARSnT__Team_Instance__c))
            {
                tempPC = teamInstanceWisePC.get(pc.AxtriaARSnT__Team_Instance__c);
            }
            tempPC.add(pc);
            teamInstanceWisePC.put(pc.AxtriaARSnT__Team_Instance__c, tempPC);
        }


        List<AxtriaSalesIQTM__Team_Instance__c> teamInstance = [select id,AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Base_Team_Name__c, AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,Name from AxtriaSalesIQTM__Team_Instance__c where id in :allTeamInstances];

        List<AxtriaARSnT__SIQ_MC_Cycle_Product_vod_O__c> mcCycleProduct = new List<AxtriaARSnT__SIQ_MC_Cycle_Product_vod_O__c>();
        
        Set<String> allUnique = new Set<String>();


        for(AxtriaSalesIQTM__Team_Instance__c teamInstanceRec : teamInstance)
        {
            system.debug('+++++++++++ Team Instance '+ teamInstanceRec);
            system.debug('++++++++ teamInstanceWisePC' + teamInstanceWisePC);
            if(teamInstanceWisePC.containsKey(teamInstanceRec.id))
            {
                 for(AxtriaARSnT__Product_Catalog__c pro : teamInstanceWisePC.get(teamInstanceRec.id))         // What all products we have for this team instance //Get all product code from  File (Master List)
                {
                    
                    
                    string object1;
                    
                    for(String channel : allChannels)
                    {
                        AxtriaARSnT__SIQ_MC_Cycle_Product_vod_O__c mcp = new AxtriaARSnT__SIQ_MC_Cycle_Product_vod_O__c();
                        
                        if(channel == 'Email')
                        object1 = 'Sent_Email_vod__c';
                        else 
                        object1 = 'Call2_vod__c';
                            
                        
                        //mcp.External_Id_vod__c = teamInstance.Name + '-' + channel + '-' + object1;
                        //mcp.Channel_Label_vod__c = channel;
                        mcp.AxtriaARSnT__SIQ_Cycle_Channel_vod__c = teamInstanceRec.Name + '-' + channel + '-' + object1;  
                        mcp.SIQ_Detail_Group__c = pro.Detail_Group__c;
                        mcp.AxtriaARSnT__SIQ_Product_vod__c = pro.AxtriaARSnT__Veeva_External_ID__c;       // Product Code is unique here
                        mcp.AxtriaARSnT__SIQ_Product_Weight_vod__c = 1;        // Keep it Static, 1 for all
                        //mcp.AZ_External_Id__c = teamInstance.Name + '__' + channel + '__' +pro.External_ID__c + '_'+pro.Name;
                        mcp.AxtriaARSnT__SIQ_External_Id_vod__c = teamInstanceRec.Name + '__' + channel + '__' +pro.AxtriaARSnT__Veeva_External_ID__c + '_'+pro.Name;
                        mcp.AxtriaARSnT__External_ID_Axtria__c = mcp.AxtriaARSnT__SIQ_External_Id_vod__c;
                        mcp.AxtriaARSnT__SIQ_Record_Type__c = veevaCriteria[0].MC_Cycle_Product_Record_Type__c;
                         mcp.AxtriaARSnT__Country_ID__c = teamInstanceRec.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;

                        if(!allUnique.contains(mcp.AxtriaARSnT__External_ID_Axtria__c))
                        {
                            allUnique.add(mcp.AxtriaARSnT__External_ID_Axtria__c);
                            mcCycleProduct.add(mcp);
                        }
                        
                    }
                    
                }
            }
           
            
            system.debug('+++++++++++'+ mcCycleProduct);
                       
        }

        upsert mcCycleProduct AxtriaARSnT__External_ID_Axtria__c; 
        runMCCPDelta();
    } 

    global void runMCCPDelta()
    {
        if(chaining)
        {
            BatchParentPacpUpdate u2 = new BatchParentPacpUpdate(lmd,allTeamInstances,allChannels,true);
            database.executeBatch(u2,200);
        }
    }  
    
     global void create_MC_Cycle_Plan_Target_vod()
    {

        /*changeMCTtest u1 = new changeMCTtest(lastjobDate,allTeamInstances, allChannels);
        
        Database.executeBatch(u1,2000);

        create_MC_Cycle_Plan_Channel_vod();*/
    }
    
    global void create_MC_Cycle_Plan_Channel_vod() 
    {
        /*changeMCChanneltest u1 = new changeMCChanneltest(lastjobDate,allTeamInstances, allChannels);
        
        Database.executeBatch(u1,2000);
        
        
        create_MC_Cycle_Plan_Product_vod();*/
    }
    
    global  void create_MC_Cycle_Plan_Product_vod()
    {
        /*changeMCproductTest u1 = new changeMCproductTest(lastjobDate,allTeamInstances, allChannels,batchID);
        
        Database.executeBatch(u1,2000);*/
        
    }
    global void execute(System.SchedulableContext SC)
    {
        teaminstancelistis = new list<string>();
        teaminstancelistis = StaticTeaminstanceList.filldataCallPlan();
        //database.executeBatch(new changeTSFStatus(teaminstancelistis));
        AZ_Pack_Test aip = new AZ_Pack_Test(teaminstancelistis);
    }
}