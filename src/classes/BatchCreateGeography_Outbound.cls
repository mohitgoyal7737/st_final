global class BatchCreateGeography_Outbound implements Database.Batchable<sObject>,Schedulable,Database.stateful {
    global String query;
    global string countrycode ='';
    global list<AxtriaSalesIQTM__Team_Instance__c>teaminstlist ;
    global map<string,set<string>>geotype2teammap ;
    global map<string,set<string>>team2teaminstance ;
    global set<string>geotype ;
    global map<String,String>Countrymap {get;set;}
    global map<String,String>mapVeeva2Mktcode {get;set;}
    global Integer recordsProcessed=0;
    global set<String> Uniqueset {get;set;}
    global list<String>CountryList ;
    global DateTime lastjobDate=null;
    global String batchID;
    global boolean directoutbound=false;
    global map<string,Date>team2startdate ;
    global map<string,Date>team2enddate ;

    global BatchCreateGeography_Outbound(string country)
    {
        /*directoutbound =true;
        countrycode = country;
        teaminstlist = new list<AxtriaSalesIQTM__Team_Instance__c>();
        geotype2teammap = new map<string,set<string>>();
        team2teaminstance = new map<string,set<string>>();
        geotype = new set<string>();
        CountryList=new list<String>();
        Countrymap = new map<string,string>();
        mapVeeva2Mktcode = new map<string,string>();
        Uniqueset = new set<string>();
        team2startdate = new map<string,Date>();
        team2enddate = new map<string,Date>();

         if(countrycode.contains(','))
        {
            CountryList=countrycode.split(',');
        }
        else
        {
            CountryList.add(countrycode);
        }
        System.debug('<<<<<<<<<--Country List-->>>>>>>>>>>>'+CountryList);

        for(AxtriaSalesIQTM__Country__c countries: [select id,Name,AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c])
        {
            if(!Countrymap.containskey(countries.name))
            {
                Countrymap.put(countries.name,countries.AxtriaSalesIQTM__Country_Code__c);
            }
        }

        
        for(AxtriaARSnT__SIQ_MC_Country_Mapping__c countrymap1: [select id,Name,AxtriaARSnT__SIQ_Veeva_Country_Code__c,AxtriaARSnT__SIQ_MC_Code__c from AxtriaARSnT__SIQ_MC_Country_Mapping__c])
        {
            if(!mapVeeva2Mktcode.containskey(countrymap1.AxtriaARSnT__SIQ_Veeva_Country_Code__c))
            {
                mapVeeva2Mktcode.put(countrymap1.AxtriaARSnT__SIQ_Veeva_Country_Code__c,countrymap1.AxtriaARSnT__SIQ_MC_Code__c);
            }
        }



        teaminstlist = [select id,name,AxtriaSalesIQTM__IC_EffstartDate__c,AxtriaSalesIQTM__IC_EffEndDate__c,AxtriaSalesIQTM__Geography_Type_Name__r.name,AxtriaSalesIQTM__Geography_Type_Name__c,AxtriaSalesIQTM__Team__c,AxtriaSalesIQTM__Team__r.name from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c IN  :CountryList and AxtriaSalesIQTM__Alignment_Period__c='Current'];
        if(teaminstlist !=null && teaminstlist.size() >0)
        {
            for(AxtriaSalesIQTM__Team_Instance__c t : teaminstlist)
            {   
                team2startdate.put(t.name,t.AxtriaSalesIQTM__IC_EffstartDate__c);
                team2enddate.put(t.name,t.AxtriaSalesIQTM__IC_EffEndDate__c);
                if(t.AxtriaSalesIQTM__Geography_Type_Name__c!=null )
                {
                    geotype.add(t.AxtriaSalesIQTM__Geography_Type_Name__c);

                    if(!geotype2teammap.containskey(t.AxtriaSalesIQTM__Geography_Type_Name__c))
                    {
                        geotype2teammap.put(t.AxtriaSalesIQTM__Geography_Type_Name__c,new set<string>{t.AxtriaSalesIQTM__Team__r.name});
                    }
                    else
                    {
                        geotype2teammap.get(t.AxtriaSalesIQTM__Geography_Type_Name__c).add(t.AxtriaSalesIQTM__Team__r.name);
                    }
                }
                

                if(!team2teaminstance.containskey(t.AxtriaSalesIQTM__Team__r.name))
                {
                    team2teaminstance.put(t.AxtriaSalesIQTM__Team__r.name,new set<string>{t.name});
                }
                else
                {
                    team2teaminstance.get(t.AxtriaSalesIQTM__Team__r.name).add(t.name);
                }
            }
            
        }
        system.debug('=============geotype2teammap::::::'+geotype2teammap);
        system.debug('========team2teaminstance:::'+team2teaminstance);
        system.debug('=========geotype=========='+geotype);
        system.debug('=========CountryList:::::'+CountryList);

        query='Select id,name,CreatedDate,LastModifiedDate,AxtriaARSnT__Country_ID__c,AxtriaARSnT__Geography_Type1__c,AxtriaARSnT__Geo_Code__c,AxtriaARSnT__Team_Instance_Name__c,AxtriaARSnT__Team_Name__c,AxtriaSalesIQTM__External_Country_Id__c,AxtriaSalesIQTM__External_Geo_Type__c,AxtriaSalesIQTM__Geography_Type__c,AxtriaSalesIQTM__Parent_Zip_Code__c,AxtriaSalesIQTM__Parent_Zip__c,AxtriaSalesIQTM__State__c,AxtriaSalesIQTM__Zip_Name__c,AxtriaSalesIQTM__Zip_Type__c,AxtriaSalesIQTM__Geography_Type__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,AxtriaSalesIQTM__Parent_Zip_Code__r.Name,AxtriaSalesIQTM__Geography_Type__r.name FROM AxtriaSalesIQTM__Geography__c where  AxtriaSalesIQTM__Geography_Type__c In :geotype'; //AxtriaARSnT__Country_Code__c In :CountryList and

        this.query = query;*/
    }

    global BatchCreateGeography_Outbound() {
        /*directoutbound =false;
        List<AxtriaARSnT__Scheduler_Log__c> schLogList = new List<AxtriaARSnT__Scheduler_Log__c>();
        teaminstlist = new list<AxtriaSalesIQTM__Team_Instance__c>();
        geotype2teammap = new map<string,set<string>>();
        team2teaminstance = new map<string,set<string>>();
        geotype = new set<string>();
        CountryList=new list<String>();
        batchID = '';
        Countrymap = new map<string,string>();
        mapVeeva2Mktcode = new map<string,string>();
        Uniqueset = new set<string>();
        team2startdate = new map<string,Date>();
        team2enddate = new map<string,Date>();

        schLogList=[Select Id,CreatedDate,AxtriaARSnT__Created_Date2__c from AxtriaARSnT__Scheduler_Log__c where AxtriaARSnT__Job_Name__c='OutBound Geography Delta' and AxtriaARSnT__Job_Status__c='Successful' Order By AxtriaARSnT__Created_Date2__c desc];
        if(schLogList.size()>0)
        {
            lastjobDate=schLogList[0].AxtriaARSnT__Created_Date2__c;  
        }
        else
        {
            lastjobDate=null;
        }
        System.debug('+++++++++++++++++++last job:'+lastjobDate);


        AxtriaARSnT__Scheduler_Log__c sJob = new AxtriaARSnT__Scheduler_Log__c();
        sJob.AxtriaARSnT__Job_Name__c = 'OutBound Geography Delta';
        sJob.AxtriaARSnT__Job_Status__c = 'Failed';
        sJob.AxtriaARSnT__Job_Type__c='Outbound';
        sJob.AxtriaARSnT__Created_Date2__c = DateTime.now();
        insert sJob;
        
        batchID = sJob.Id;
        recordsProcessed =0;
        

        for(Veeva_Job_Scheduling__c countries : [select Country_Name__c,Country__r.AxtriaSalesIQTM__Country_Code__c from Veeva_Job_Scheduling__c where SFE_Load__c = 'Delta Load'])
        {
            if(!Countrymap.containskey(countries.Country_Name__c))
            {
                Countrymap.put(countries.Country_Name__c,countries.Country__r.AxtriaSalesIQTM__Country_Code__c);
            }
        }

        if(Countrymap.size() > 0){

            CountryList.addall(Countrymap.values());
        }

        
        for(AxtriaARSnT__SIQ_MC_Country_Mapping__c countrymap1: [select id,Name,AxtriaARSnT__SIQ_Veeva_Country_Code__c,AxtriaARSnT__SIQ_MC_Code__c from AxtriaARSnT__SIQ_MC_Country_Mapping__c])
        {
            if(!mapVeeva2Mktcode.containskey(countrymap1.AxtriaARSnT__SIQ_Veeva_Country_Code__c))
            {
                mapVeeva2Mktcode.put(countrymap1.AxtriaARSnT__SIQ_Veeva_Country_Code__c,countrymap1.AxtriaARSnT__SIQ_MC_Code__c);
            }
        }



        teaminstlist = [select id,name,AxtriaSalesIQTM__IC_EffstartDate__c,AxtriaSalesIQTM__IC_EffEndDate__c,AxtriaSalesIQTM__Geography_Type_Name__r.name,AxtriaSalesIQTM__Geography_Type_Name__c,AxtriaSalesIQTM__Team__c,AxtriaSalesIQTM__Team__r.name from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c IN  :CountryList and AxtriaSalesIQTM__Alignment_Period__c='Current'];
        system.debug('=============teaminstlist:::'+teaminstlist);
        if(teaminstlist !=null && teaminstlist.size() >0)
        {
            for(AxtriaSalesIQTM__Team_Instance__c t : teaminstlist)
            {   
                team2startdate.put(t.name,t.AxtriaSalesIQTM__IC_EffstartDate__c);
                team2enddate.put(t.name,t.AxtriaSalesIQTM__IC_EffEndDate__c);
                if(t.AxtriaSalesIQTM__Geography_Type_Name__c!=null )
                {
                    geotype.add(t.AxtriaSalesIQTM__Geography_Type_Name__c);

                    if(!geotype2teammap.containskey(t.AxtriaSalesIQTM__Geography_Type_Name__c))
                    {
                        geotype2teammap.put(t.AxtriaSalesIQTM__Geography_Type_Name__c,new set<string>{t.AxtriaSalesIQTM__Team__r.name});
                    }
                    else
                    {
                        geotype2teammap.get(t.AxtriaSalesIQTM__Geography_Type_Name__c).add(t.AxtriaSalesIQTM__Team__r.name);
                    }
                }
                

                if(!team2teaminstance.containskey(t.AxtriaSalesIQTM__Team__r.name))
                {
                    team2teaminstance.put(t.AxtriaSalesIQTM__Team__r.name,new set<string>{t.name});
                }
                else
                {
                    team2teaminstance.get(t.AxtriaSalesIQTM__Team__r.name).add(t.name);
                }
            }
            
        }
        system.debug('=============geotype2teammap::::::'+geotype2teammap);
        system.debug('========team2teaminstance:::'+team2teaminstance);
        system.debug('=========geotype=========='+geotype);
        system.debug('=========CountryList:::::'+CountryList);

        query='Select id,name,CreatedDate,LastModifiedDate,AxtriaARSnT__Country_ID__c,AxtriaARSnT__Geography_Type1__c,AxtriaARSnT__Geo_Code__c,AxtriaARSnT__Team_Instance_Name__c,AxtriaARSnT__Team_Name__c,AxtriaSalesIQTM__External_Country_Id__c,AxtriaSalesIQTM__External_Geo_Type__c,AxtriaSalesIQTM__Geography_Type__c,AxtriaSalesIQTM__Parent_Zip_Code__c,AxtriaSalesIQTM__Parent_Zip__c,AxtriaSalesIQTM__State__c,AxtriaSalesIQTM__Zip_Name__c,AxtriaSalesIQTM__Zip_Type__c,AxtriaSalesIQTM__Geography_Type__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,AxtriaSalesIQTM__Parent_Zip_Code__r.Name,AxtriaSalesIQTM__Geography_Type__r.name FROM AxtriaSalesIQTM__Geography__c where  AxtriaSalesIQTM__Geography_Type__c In :geotype'; //AxtriaARSnT__Country_Code__c In :CountryList and
        if(lastjobDate!=null)
        {
            query = query + ' and LastModifiedDate  >=:  lastjobDate '; 
        }

        this.query = query;*/
    }
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Geography__c> scope) {

        /*Date myDateTime = Date.newInstance(4000, 12, 31);
        Uniqueset = new set<string>();
        list<AxtriaARSnT__SIQ_Geography_O__c>GeographyList = new list<AxtriaARSnT__SIQ_Geography_O__c>();
        for(AxtriaSalesIQTM__Geography__c pos : scope)
        {   

            for(string team : geotype2teammap.get(pos.AxtriaSalesIQTM__Geography_Type__c))
            {
                for(string teaminstnace: team2teaminstance.get(team))
                {
                    String key =teaminstnace+'_'+pos.id;
                    if(!Uniqueset.contains(Key))
                    {
                        AxtriaARSnT__SIQ_Geography_O__c obj = new AxtriaARSnT__SIQ_Geography_O__c(); 
                        String code=' '; 
                        obj.AxtriaARSnT__SIQ_Country_Code__c=pos.AxtriaSalesIQTM__Geography_Type__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                        code=pos.AxtriaSalesIQTM__Geography_Type__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                        if(mapVeeva2Mktcode.get(code) != null)
                        {
                            obj.AxtriaARSnT__SIQ_Marketing_Code__c = mapVeeva2Mktcode.get(code);
                        }
                        else
                        {
                            obj.AxtriaARSnT__SIQ_Marketing_Code__c = 'MC code does not exist';
                        }
                        obj.AxtriaARSnT__SIQ_Team__c=team;
                        obj.AxtriaARSnT__SIQ_Team_Instance__c=teaminstnace;
                        obj.AxtriaARSnT__SIQ_ZIP__c=pos.Name;
                        obj.AxtriaARSnT__SIQ_ZIP_Name__c=pos.AxtriaSalesIQTM__Zip_Name__c;
                        obj.AxtriaARSnT__SIQ_Parent_ZIP_c__c=pos.AxtriaSalesIQTM__Parent_Zip_Code__r.Name;
                        obj.AxtriaARSnT__SIQ_ZIP_Type__c=pos.AxtriaSalesIQTM__Zip_type__c;
                        obj.AxtriaARSnT__SIQ_Created_Dat__c=pos.CreatedDate;
                        obj.AxtriaARSnT__SIQ_Updated_Dat__c =pos.LastModifiedDate;
                        obj.AxtriaARSnT__SIQ_Effective_Start_Dat__c=team2startdate.get(teaminstnace);
                        obj.AxtriaARSnT__SIQ_Effective_End_Dat__c=team2enddate.get(teaminstnace) ;
                        obj.AxtriaARSnT__Unique_Id__c=teaminstnace+'_'+pos.id;
                        GeographyList.add(obj);
                        recordsProcessed++;
                        Uniqueset.add(Key);                    
                    }
                }
            }
            
        }

        Upsert GeographyList AxtriaARSnT__Unique_Id__c;*/

    }

    global void finish(Database.BatchableContext BC) 
    {
        /*if(!directoutbound)
        {
            System.debug(recordsProcessed + ' records processed. ');
            AxtriaARSnT__Scheduler_Log__c sJob = new AxtriaARSnT__Scheduler_Log__c(id = batchID); 
            system.debug('schedulerObj++++before'+sJob);
            sJob.AxtriaARSnT__No_Of_Records_Processed__c=recordsProcessed;
            sJob.AxtriaARSnT__Job_Status__c='Successful';
            system.debug('sJob++++++++'+sJob);
            update sJob;
           
        }*/
    }
    global void execute(SchedulableContext sc){
        
        Database.executeBatch( new BatchCreateGeography_Outbound(),200);
    }
}