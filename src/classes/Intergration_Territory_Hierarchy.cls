public with sharing class Intergration_Territory_Hierarchy {
    
	List<Staging_Terr_Hierarchy__c> allTerrHierarchy;
	List<AxtriaSalesIQTM__Position__c> posRecs;
    public Intergration_Territory_Hierarchy(List<String> allTeamInstances) {
        
        delete [select id from Staging_Terr_Hierarchy__c where Team_Instance__c in :allTeamInstances];
        
        posRecs = new List<AxtriaSalesIQTM__Position__c>();

        posRecs = [select id, Name, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.CurrencyIsoCode  from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances];

        createTerrTable();
    }

    public void createTerrTable()
    {
    	allTerrHierarchy = new List<Staging_Terr_Hierarchy__c>();

    	for(AxtriaSalesIQTM__Position__c pos : posRecs)
    	{
    		Staging_Terr_Hierarchy__c th = new Staging_Terr_Hierarchy__c();
    		th.Name = pos.Name;
    		th.Territory_Code__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
    		th.Parent_Territory_Code__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
    		th.Currency_ISO_Code__c = pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.CurrencyIsoCode;
    		th.Team_Instance__c = pos.AxtriaSalesIQTM__Team_Instance__c;

    		allTerrHierarchy.add(th);
    	}

    	insert allTerrHierarchy;
    }
}