/**********************************************************************************************
Author      : Himanshu Tariyal (A0994)
Date        : 24th September'2020
Description : Test class for PositionProductDirectLoad
Revision(s) : v1.0
**********************************************************************************************/
@isTest
private class PositionProductLoadTest 
{ 
    static testMethod void testMethod1() 
    {
        String className = 'PositionProductLoadTest';
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);

        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc.AxtriaSalesIQTM__Country__c = countr.id;
        acc.AccountNumber = 'BH10461999';
        acc.AxtriaSalesIQTM__AccountType__c = 'HCP';
        acc.Status__c = 'Active';
        acc.Type = 'HCP';
        SnTDMLSecurityUtil.insertRecords(acc,className);

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'test';
        SnTDMLSecurityUtil.insertRecords(team,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins1 = new AxtriaSalesIQTM__Team_Instance__c();
        teamins1.Name = 'abcde';
        teamins1.IsHCOSegmentationEnabled__c = true;
        teamins1.AxtriaSalesIQTM__Team__c = team.id;
        //teamins.Cycle__c = cycle.id;
        teamins1.Include_Secondary_Affiliations__c = true;
        teamins1.Include_Territory__c = true;
	teamins1.Customer_Types__c = 'HCP;HCO';
        teamins1.Product_Priority__c= true;
        teamins1.AxtriaSalesIQTM__IC_EffEndDate__c = date.today()+1;
        teamins1.AxtriaSalesIQTM__IC_EffstartDate__c = date.today();
        SnTDMLSecurityUtil.insertRecords(teamins1,className);
        
        AxtriaSalesIQTM__Workspace__c workspace = new AxtriaSalesIQTM__Workspace__c();
        workspace.Name = 'HCO';
        workspace.AxtriaSalesIQTM__Workspace_Description__c = 'Some Description';
        workspace.AxtriaSalesIQTM__Workspace_Start_Date__c = date.today();
        workspace.AxtriaSalesIQTM__Workspace_End_Date__c = date.today()+1;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        AxtriaSalesIQTM__Scenario__c scenario = TestDataFactory.newcreateScenario(teamins1,team,workspace);
        SnTDMLSecurityUtil.insertRecords(scenario,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.Name = 'Oncology-Q4-2019';
        teamins.AxtriaSalesIQTM__Team__c = team.id;
	teamins.Customer_Types__c = 'HCP;HCO';
        teamins.AxtriaSalesIQTM__Scenario__c = scenario.id;
        teamins.AxtriaSalesIQTM__IC_EffEndDate__c = date.today()+1;
        teamins.AxtriaSalesIQTM__IC_EffstartDate__c = date.today();
        SnTDMLSecurityUtil.insertRecords(teamins,className);        
        
        AxtriaSalesIQTM__Position__c pos1 = TestDataFactory.createPosition(team,teamins);
        pos1.AxtriaSalesIQTM__Team_iD__c = team.Id;
        SnTDMLSecurityUtil.insertRecords(pos1,className);
        
        AxtriaSalesIQTM__User_Access_Permission__c uAccessPerm = TestDataFactory.createUserAccessPerm(pos1,teamins,UserInfo.getUserId());
        uAccessPerm.AxtriaSalesIQTM__Position__c=pos1.id;
        uAccessPerm.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        uAccessPerm.AxtriaSalesIQTM__User__c = Userinfo.getUserId();
        SnTDMLSecurityUtil.insertRecords(uAccessPerm,className);
        
        Product2 pp2= TestDataFactory.newprod();
        pp2.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        SnTDMLSecurityUtil.insertRecords(pp2,className);

        AxtriaSalesIQTM__TriggerContol__c ccd1= new AxtriaSalesIQTM__TriggerContol__c();
        ccd1.Name = 'TeamInstanceProductTrigger';
        ccd1.AxtriaSalesIQTM__IsStopTrigger__c = True;
        SnTDMLSecurityUtil.insertRecords(ccd1,className);
        
        Product_Catalog__c prodCat = TestDataFactory.productCatalog(team,teamins,countr);
        prodCat.Team__c= team.id;
        prodCat.Team_Instance__c = teamins.id;
        prodCat.IsActive__c=true;
        prodCat.Country_Lookup__c = countr.id;
        prodCat.Product_Code__c = 'GIST';
        prodCat.Name ='GIST';
        prodCat.Veeva_External_ID__c ='GIST';
        /*prodCat.Full_Load__c = true;
        prodCat.Product_Type__c = 'Product';*/
        SnTDMLSecurityUtil.insertRecords(prodCat,className);
        
        AxtriaSalesIQTM__Product__c prod = TestDataFactory.createProduct(team,teamins);
        prod.Team__c = team.id;
        prod.Team_Instance__c = teamins.id; 
        SnTDMLSecurityUtil.insertRecords(prod,className);
        
        AxtriaSalesIQTM__Position_Product__c ppc = TestDataFactory.createPositionProduct(teamins,pos1,prodCat);
        ppc.AxtriaSalesIQTM__Position__c = pos1.id;
        ppc.Product_Catalog__c = prodCat.id;
        ppc.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        ppc.AxtriaSalesIQTM__Product_Master__c = prod.id;
        SnTDMLSecurityUtil.insertRecords(ppc,className);
        
        AxtriaSalesIQTM__Team_Instance_Product__c teaminsproduct = TestDataFactory.teamInstanceProduct(team,teamins,prod);
        teaminsproduct.AxtriaSalesIQTM__Product__c = pp2.id;
        teaminsproduct.AxtriaSalesIQTM__Product_Master__c = prod.id;
        teaminsproduct.AxtriaSalesIQTM__Team__c = team.id;
        teaminsproduct.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        SnTDMLSecurityUtil.insertRecords(teaminsproduct,className);
        
        Team_Instance_Product_AZ__c teaminsprod = TestDataFactory.teamInstanceProductAZ(prodCat,teamins);
        teaminsprod.Team_Instance__c = teamins.id;
        teaminsprod.Product_Catalogue__c = prodCat.id;
        SnTDMLSecurityUtil.insertRecords(teaminsprod,className);
        
        AxtriaSalesIQTM__TriggerContol__c ccd= new AxtriaSalesIQTM__TriggerContol__c();
        ccd.Name = 'FillExternalIDonPosProduct';
        ccd.AxtriaSalesIQTM__IsStopTrigger__c = false;
        SnTDMLSecurityUtil.insertRecords(ccd,className);

        AxtriaSalesIQTM__Position__c pos = TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__Team_iD__c = team.Id;
        SnTDMLSecurityUtil.insertRecords(pos,className);        
	AxtriaSalesIQTM__Change_Request_Type__c  crtype = new AxtriaSalesIQTM__Change_Request_Type__c ();
        crtype.AxtriaSalesIQTM__CR_Type_Name__c = 'testrec';
        crtype.AxtriaSalesIQTM__Change_Request_Code__c  = 'Call_Plan_Change';
        insert crtype;
        
        temp_Obj__c zt = new temp_Obj__c();
        zt.Status__c ='New';//
        zt.Country__c = 'USA';
        zt.Event__c = 'Delete';
        zt.Team_Instance__c = teamins.id;
        zt.AccountNumber__c = acc.AccountNumber;
        zt.Account_Type__c = acc.AxtriaSalesIQTM__AccountType__c ;
        zt.Team_Instance_Name__c = teamins.Name;//
        zt.Position_Code__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;//2 positionset
        zt.Object__c ='Staging_Position_Product__c';
        zt.Territory_ID__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        zt.Account_Type__c = acc.AxtriaSalesIQTM__AccountType__c ;
        zt.Team_Name__c = team.Name;
        zt.Product_Code__c = 'GIST';//
        zt.Product_Name__c = prodCat.id;//1 allproduct
        zt.Holidays__c = 1;//
        zt.Other_Days_Off__c = 1;//
        zt.Vacation_Days__c = 1;//
        zt.Calls_Per_Days__c  = 1;//
        SnTDMLSecurityUtil.insertRecords(zt,className);

        System.Test.startTest();

        String nameSpace = MCCP_Utility.sntNamespace(className);
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        
        Database.executeBatch(new PositionProductDirectLoad(teamins.Id));

        System.Test.stopTest();
    }

    static testMethod void testMethod2() 
    {
        String className = 'PositionProductLoadTest';
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);

        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc.AxtriaSalesIQTM__Country__c = countr.id;
        acc.AccountNumber = 'BH10461999';
        acc.AxtriaSalesIQTM__AccountType__c = 'HCP';
        acc.Status__c = 'Active';
        acc.Type = 'HCP';
        SnTDMLSecurityUtil.insertRecords(acc,className);

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'test';
        SnTDMLSecurityUtil.insertRecords(team,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins1 = new AxtriaSalesIQTM__Team_Instance__c();
        teamins1.Name = 'abcde';
        teamins1.IsHCOSegmentationEnabled__c = true;
        teamins1.AxtriaSalesIQTM__Team__c = team.id;
        //teamins.Cycle__c = cycle.id;
        teamins1.Include_Secondary_Affiliations__c = true;
        teamins1.Include_Territory__c = true;
        teamins1.Product_Priority__c= true;
	teamins1.Customer_Types__c = 'HCP;HCO';
        teamins1.AxtriaSalesIQTM__IC_EffEndDate__c = date.today()+1;
        teamins1.AxtriaSalesIQTM__IC_EffstartDate__c = date.today();
        SnTDMLSecurityUtil.insertRecords(teamins1,className);
        
        AxtriaSalesIQTM__Workspace__c workspace = new AxtriaSalesIQTM__Workspace__c();
        workspace.Name = 'HCO';
        workspace.AxtriaSalesIQTM__Workspace_Description__c = 'Some Description';
        workspace.AxtriaSalesIQTM__Workspace_Start_Date__c = date.today();
        workspace.AxtriaSalesIQTM__Workspace_End_Date__c = date.today()+1;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        AxtriaSalesIQTM__Scenario__c scenario = TestDataFactory.newcreateScenario(teamins1,team,workspace);
        SnTDMLSecurityUtil.insertRecords(scenario,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.Name = 'Oncology-Q4-2019';
        teamins.AxtriaSalesIQTM__Team__c = team.id;
        teamins.AxtriaSalesIQTM__Scenario__c = scenario.id;
        teamins.AxtriaSalesIQTM__IC_EffEndDate__c = date.today()+1;
	teamins.Customer_Types__c = 'HCP;HCO';
        teamins.AxtriaSalesIQTM__IC_EffstartDate__c = date.today();
        SnTDMLSecurityUtil.insertRecords(teamins,className);        
        
        AxtriaSalesIQTM__Position__c pos1 = TestDataFactory.createPosition(team,teamins);
        pos1.AxtriaSalesIQTM__Team_iD__c = team.Id;
        SnTDMLSecurityUtil.insertRecords(pos1,className);
	AxtriaSalesIQTM__Change_Request_Type__c  crtype = new AxtriaSalesIQTM__Change_Request_Type__c ();
        crtype.AxtriaSalesIQTM__CR_Type_Name__c = 'testrec';
        crtype.AxtriaSalesIQTM__Change_Request_Code__c  = 'Call_Plan_Change';
        insert crtype;
        
        AxtriaSalesIQTM__User_Access_Permission__c uAccessPerm = TestDataFactory.createUserAccessPerm(pos1,teamins,UserInfo.getUserId());
        uAccessPerm.AxtriaSalesIQTM__Position__c=pos1.id;
        uAccessPerm.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        uAccessPerm.AxtriaSalesIQTM__User__c = Userinfo.getUserId();
        SnTDMLSecurityUtil.insertRecords(uAccessPerm,className);
        
        Product2 pp2= TestDataFactory.newprod();
        pp2.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        SnTDMLSecurityUtil.insertRecords(pp2,className);

        AxtriaSalesIQTM__TriggerContol__c ccd1= new AxtriaSalesIQTM__TriggerContol__c();
        ccd1.Name = 'TeamInstanceProductTrigger';
        ccd1.AxtriaSalesIQTM__IsStopTrigger__c = True;
        SnTDMLSecurityUtil.insertRecords(ccd1,className);
        
        Product_Catalog__c prodCat = TestDataFactory.productCatalog(team,teamins,countr);
        prodCat.Team__c= team.id;
        prodCat.Team_Instance__c = teamins.id;
        prodCat.IsActive__c=true;
        prodCat.Country_Lookup__c = countr.id;
        prodCat.Product_Code__c = 'GIST';
        prodCat.Name ='GIST';
        prodCat.Veeva_External_ID__c ='GIST';
        /*prodCat.Full_Load__c = true;
        prodCat.Product_Type__c = 'Product';*/
        SnTDMLSecurityUtil.insertRecords(prodCat,className);
        
        AxtriaSalesIQTM__Product__c prod = TestDataFactory.createProduct(team,teamins);
        prod.Team__c = team.id;
        prod.Team_Instance__c = teamins.id; 
        SnTDMLSecurityUtil.insertRecords(prod,className);
        
        AxtriaSalesIQTM__Position_Product__c ppc = TestDataFactory.createPositionProduct(teamins,pos1,prodCat);
        ppc.AxtriaSalesIQTM__Position__c = pos1.id;
        ppc.Product_Catalog__c = prodCat.id;
        ppc.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        ppc.AxtriaSalesIQTM__Product_Master__c = prod.id;
        SnTDMLSecurityUtil.insertRecords(ppc,className);
        
        AxtriaSalesIQTM__Team_Instance_Product__c teaminsproduct = TestDataFactory.teamInstanceProduct(team,teamins,prod);
        teaminsproduct.AxtriaSalesIQTM__Product__c = pp2.id;
        teaminsproduct.AxtriaSalesIQTM__Product_Master__c = prod.id;
        teaminsproduct.AxtriaSalesIQTM__Team__c = team.id;
        teaminsproduct.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        SnTDMLSecurityUtil.insertRecords(teaminsproduct,className);
        
        Team_Instance_Product_AZ__c teaminsprod = TestDataFactory.teamInstanceProductAZ(prodCat,teamins);
        teaminsprod.Team_Instance__c = teamins.id;
        teaminsprod.Product_Catalogue__c = prodCat.id;
        SnTDMLSecurityUtil.insertRecords(teaminsprod,className);
        
        AxtriaSalesIQTM__TriggerContol__c ccd= new AxtriaSalesIQTM__TriggerContol__c();
        ccd.Name = 'FillExternalIDonPosProduct';
        ccd.AxtriaSalesIQTM__IsStopTrigger__c = false;
        SnTDMLSecurityUtil.insertRecords(ccd,className);

        AxtriaSalesIQTM__Position__c pos = TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__Team_iD__c = team.Id;
        SnTDMLSecurityUtil.insertRecords(pos,className);    

        AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
        cr.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.Id;
        SnTDMLSecurityUtil.insertRecords(cr,className);
        
        temp_Obj__c zt = new temp_Obj__c();
        zt.Change_Request__c = cr.Id;
        zt.Status__c ='New';//
        zt.Country__c = 'USA';
        zt.Event__c = 'Delete';
        zt.Team_Instance__c = teamins.id;
        zt.AccountNumber__c = acc.AccountNumber;
        zt.Account_Type__c = acc.AxtriaSalesIQTM__AccountType__c ;
        zt.Team_Instance_Name__c = teamins.Name;//
        zt.Position_Code__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;//2 positionset
        zt.Object__c ='Staging_Position_Product__c';
        zt.Territory_ID__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        zt.Account_Type__c = acc.AxtriaSalesIQTM__AccountType__c ;
        zt.Team_Name__c = team.Name;
        zt.Product_Code__c = 'GIST';//
        zt.Product_Name__c = prodCat.id;//1 allproduct
        zt.Holidays__c = 1;//
        zt.Other_Days_Off__c = 1;//
        zt.Vacation_Days__c = 1;//
        zt.Calls_Per_Days__c  = 1;//
        SnTDMLSecurityUtil.insertRecords(zt,className);

        System.Test.startTest();

        String nameSpace = MCCP_Utility.sntNamespace(className);
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        
        Database.executeBatch(new PositionProductDirectLoad(teamins.Id,cr.Id));

        System.Test.stopTest();
    }

    static testMethod void testMethod3() 
    {
        String className = 'PositionProductLoadTest';
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);

        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc.AxtriaSalesIQTM__Country__c = countr.id;
        acc.AccountNumber = 'BH10461999';
        acc.AxtriaSalesIQTM__AccountType__c = 'HCP';
        acc.Status__c = 'Active';
        acc.Type = 'HCP';
        SnTDMLSecurityUtil.insertRecords(acc,className);

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'test';
        SnTDMLSecurityUtil.insertRecords(team,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins1 = new AxtriaSalesIQTM__Team_Instance__c();
        teamins1.Name = 'abcde';
        teamins1.IsHCOSegmentationEnabled__c = true;
        teamins1.AxtriaSalesIQTM__Team__c = team.id;
	teamins1.Customer_Types__c = 'HCP;HCO';
        //teamins.Cycle__c = cycle.id;
        teamins1.Include_Secondary_Affiliations__c = true;
        teamins1.Include_Territory__c = true;
        teamins1.Product_Priority__c= true;
        teamins1.AxtriaSalesIQTM__IC_EffEndDate__c = date.today()+1;
        teamins1.AxtriaSalesIQTM__IC_EffstartDate__c = date.today();
        SnTDMLSecurityUtil.insertRecords(teamins1,className);
        
        AxtriaSalesIQTM__Workspace__c workspace = new AxtriaSalesIQTM__Workspace__c();
        workspace.Name = 'HCO';
        workspace.AxtriaSalesIQTM__Workspace_Description__c = 'Some Description';
        workspace.AxtriaSalesIQTM__Workspace_Start_Date__c = date.today();
        workspace.AxtriaSalesIQTM__Workspace_End_Date__c = date.today()+1;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        AxtriaSalesIQTM__Scenario__c scenario = TestDataFactory.newcreateScenario(teamins1,team,workspace);
        SnTDMLSecurityUtil.insertRecords(scenario,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.Name = 'Oncology-Q4-2019';
        teamins.AxtriaSalesIQTM__Team__c = team.id;
        teamins.AxtriaSalesIQTM__Scenario__c = scenario.id;
	teamins.Customer_Types__c = 'HCP;HCO';
        teamins.AxtriaSalesIQTM__IC_EffEndDate__c = date.today()+1;
        teamins.AxtriaSalesIQTM__IC_EffstartDate__c = date.today();
        SnTDMLSecurityUtil.insertRecords(teamins,className);        
        
        AxtriaSalesIQTM__Position__c pos1 = TestDataFactory.createPosition(team,teamins);
        pos1.AxtriaSalesIQTM__Team_iD__c = team.Id;
        SnTDMLSecurityUtil.insertRecords(pos1,className);
	AxtriaSalesIQTM__Change_Request_Type__c  crtype = new AxtriaSalesIQTM__Change_Request_Type__c ();
        crtype.AxtriaSalesIQTM__CR_Type_Name__c = 'testrec';
        crtype.AxtriaSalesIQTM__Change_Request_Code__c  = 'Call_Plan_Change';
        insert crtype;
        
        AxtriaSalesIQTM__User_Access_Permission__c uAccessPerm = TestDataFactory.createUserAccessPerm(pos1,teamins,UserInfo.getUserId());
        uAccessPerm.AxtriaSalesIQTM__Position__c=pos1.id;
        uAccessPerm.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        uAccessPerm.AxtriaSalesIQTM__User__c = Userinfo.getUserId();
        SnTDMLSecurityUtil.insertRecords(uAccessPerm,className);
        
        Product2 pp2= TestDataFactory.newprod();
        pp2.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        SnTDMLSecurityUtil.insertRecords(pp2,className);

        AxtriaSalesIQTM__TriggerContol__c ccd1= new AxtriaSalesIQTM__TriggerContol__c();
        ccd1.Name = 'TeamInstanceProductTrigger';
        ccd1.AxtriaSalesIQTM__IsStopTrigger__c = True;
        SnTDMLSecurityUtil.insertRecords(ccd1,className);
        
        Product_Catalog__c prodCat = TestDataFactory.productCatalog(team,teamins,countr);
        prodCat.Team__c= team.id;
        prodCat.Team_Instance__c = teamins.id;
        prodCat.IsActive__c=true;
        prodCat.Country_Lookup__c = countr.id;
        prodCat.Product_Code__c = 'GIST';
        prodCat.Name ='GIST';
        prodCat.Veeva_External_ID__c ='GIST';
        /*prodCat.Full_Load__c = true;
        prodCat.Product_Type__c = 'Product';*/
        SnTDMLSecurityUtil.insertRecords(prodCat,className);
        
        AxtriaSalesIQTM__Product__c prod = TestDataFactory.createProduct(team,teamins);
        prod.Team__c = team.id;
        prod.Team_Instance__c = teamins.id; 
        SnTDMLSecurityUtil.insertRecords(prod,className);
        
        AxtriaSalesIQTM__Position_Product__c ppc = TestDataFactory.createPositionProduct(teamins,pos1,prodCat);
        ppc.AxtriaSalesIQTM__Position__c = pos1.id;
        ppc.Product_Catalog__c = prodCat.id;
        ppc.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        ppc.AxtriaSalesIQTM__Product_Master__c = prod.id;
        SnTDMLSecurityUtil.insertRecords(ppc,className);
        
        AxtriaSalesIQTM__Team_Instance_Product__c teaminsproduct = TestDataFactory.teamInstanceProduct(team,teamins,prod);
        teaminsproduct.AxtriaSalesIQTM__Product__c = pp2.id;
        teaminsproduct.AxtriaSalesIQTM__Product_Master__c = prod.id;
        teaminsproduct.AxtriaSalesIQTM__Team__c = team.id;
        teaminsproduct.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        SnTDMLSecurityUtil.insertRecords(teaminsproduct,className);
        
        Team_Instance_Product_AZ__c teaminsprod = TestDataFactory.teamInstanceProductAZ(prodCat,teamins);
        teaminsprod.Team_Instance__c = teamins.id;
        teaminsprod.Product_Catalogue__c = prodCat.id;
        SnTDMLSecurityUtil.insertRecords(teaminsprod,className);
        
        AxtriaSalesIQTM__TriggerContol__c ccd= new AxtriaSalesIQTM__TriggerContol__c();
        ccd.Name = 'FillExternalIDonPosProduct';
        ccd.AxtriaSalesIQTM__IsStopTrigger__c = false;
        SnTDMLSecurityUtil.insertRecords(ccd,className);

        AxtriaSalesIQTM__Position__c pos = TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__Team_iD__c = team.Id;
        SnTDMLSecurityUtil.insertRecords(pos,className);        
        
        AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
        cr.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.Id;
        SnTDMLSecurityUtil.insertRecords(cr,className);
        
        temp_Obj__c zt = new temp_Obj__c();
        zt.Change_Request__c = cr.Id;
        zt.Status__c ='New';//
        zt.Country__c = 'USA';
        zt.Event__c = 'Delete';
        zt.Team_Instance__c = teamins.id;
        zt.AccountNumber__c = acc.AccountNumber;
        zt.Account_Type__c = acc.AxtriaSalesIQTM__AccountType__c ;
        zt.Team_Instance_Name__c = teamins.Name;//
        zt.Position_Code__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;//2 positionset
        zt.Object__c ='Staging_Position_Product__c';
        zt.Territory_ID__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        zt.Account_Type__c = acc.AxtriaSalesIQTM__AccountType__c ;
        zt.Team_Name__c = team.Name;
        zt.Product_Code__c = 'GIST';//
        zt.Product_Name__c = prodCat.id;//1 allproduct
        zt.Holidays__c = 1;//
        zt.Other_Days_Off__c = 1;//
        zt.Vacation_Days__c = 1;//
        zt.Calls_Per_Days__c  = 1;//
        SnTDMLSecurityUtil.insertRecords(zt,className);

        System.Test.startTest();

        String nameSpace = MCCP_Utility.sntNamespace(className);
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        
        Database.executeBatch(new PositionProductDirectLoad(teamins.Id,cr.Id,true));

        System.Test.stopTest();
    }
}