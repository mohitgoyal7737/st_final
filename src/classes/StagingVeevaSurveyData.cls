/*
Name        :   StagingVeevaSurveyData
Modified by :   Himanshu Tariyal
Mod Date    :   16/01/2019
Description :   StagingVeevaSurveyData used to transform Veeva data from Staging_Veeva_Cust_Data__c object to Staging_Cust_Survey_Profiling__c object
Process flow :
1. Veeva data is pushed into the Staging_Veeva_Cust_Data__c object.
2. Single Staging_Veeva_Cust_Data__c record is transformed into a single Staging_Cust_Survey_Profiling__c record, each having a certain QID with Question
   text and response. All data is pushed if no Schedular log record is present for the data, or delta changes are pushed.
   3. Certain other processes are applied to get meaningful data into the BU Response object serving as the starting point for the Simulation module*/
   global with sharing class StagingVeevaSurveyData implements Database.Batchable<sObject>, Database.Stateful
   {
    public String teamInstance;
    public String batchID;
    public String query;

    public Boolean scheduleJob;

    public Integer recordsProcessed = 0;
    public DateTime lastjobDate = null;

    public Map<String, String> mapProductCodeToTIName;
    public Set<String> externalid;
    public boolean dailyJob = true;
    public Boolean schedulerLog = false;
    public list<String> teamInst_ProdList; //Added by dhiren
    public String teamInstance_Prod;//Added by dhiren
    public String product;//Added by dhiren
    public boolean ondemand = false; //Added by dhiren
    Map<String, DateTime> mapKeyDate = new Map<String, DateTime>();

    global StagingVeevaSurveyData(String teamInstanceProd, String differentiator)
    {
        teamInstance_Prod = teamInstanceProd;
        SnTDMLSecurityUtil.printDebugMessage('<><><><><><><><>' + teamInstance_Prod);
        externalid = new Set<String>();
        mapProductCodeToTIName = new Map<String, String>();
        teamInst_ProdList = new list<String>();
        if(teamInstance_Prod.contains(';'))
        {
            teamInst_ProdList = teamInstance_Prod.split(';');
        }

        SnTDMLSecurityUtil.printDebugMessage('<><>teamInst_ProdList<><<>' + teamInst_ProdList);
        teamInstance = teamInst_ProdList[0];
        product = teamInst_ProdList[1];
        //type ='file';
        ondemand = true;
        dailyJob = false;

        SnTDMLSecurityUtil.printDebugMessage('<>>>>>>><>teamInstance<><><><><><><> ' + teamInstance + '  <>>>>>>>>product>>>>>>>>>> ' + product);

        query = 'SELECT id,BRAND_ID__c,BRAND_NAME__c,PARTY_ID__c,QUESTION_ID__c,QUESTION_SHORT_TEXT__c,Response_Num__c,RESPONSE__c,SURVEY_ID__c,' +
        'SURVEY_NAME__c,Team_Instance__c,Last_Updated__c FROM Staging_Veeva_Cust_Data__c Where BRAND_ID__c =: product WITH SECURITY_ENFORCED';
    }

    global StagingVeevaSurveyData()
    {
        teamInstance = '';
        scheduleJob = false;
        dailyJob = false;
        externalid = new Set<String>();
        mapProductCodeToTIName = new Map<String, String>();

        query = 'SELECT id,BRAND_ID__c,BRAND_NAME__c,PARTY_ID__c,QUESTION_ID__c,QUESTION_SHORT_TEXT__c,Response_Num__c,RESPONSE__c,SURVEY_ID__c,' +
        'SURVEY_NAME__c,Team_Instance__c,Last_Updated__c FROM Staging_Veeva_Cust_Data__c WITH SECURITY_ENFORCED';
    }

    global StagingVeevaSurveyData(Boolean scheduleJob)
    {
        this.scheduleJob = scheduleJob;
        teamInstance = '';
        dailyJob = false;
        externalid = new Set<String>();
        mapProductCodeToTIName = new Map<String, String>();
        /*List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
        schLogList=[Select Id,CreatedDate,Created_Date2__c from Scheduler_Log__c where Job_Name__c='Survey Data from Veeva' and Job_Status__c='Successful' Order By Created_Date2__c desc];
        if(schLogList.size()>0 && schLogList!=null)
        {
            lastjobDate=schLogList[0].Created_Date2__c;   //CreatedDate;  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
        else
        {
            lastjobDate=null;       //else we set the lastjobDate to null
        }
        SnTDMLSecurityUtil.printDebugMessage('StagingVeevaSurveyData(): Last job Date-->'+lastjobDate);
        Scheduler_Log__c sJob = new Scheduler_Log__c();
        sJob.Job_Name__c = 'Survey Data from Veeva';
        sJob.Job_Status__c = 'Failed';
        sJob.Job_Type__c='Inbound';
        sJob.Created_Date2__c = DateTime.now();
        insert sJob;
        batchID = sJob.Id;*/

        /*List<Product_Catalog__c> pcList = [select Team_Instance__c,Team_Instance__r.Name,Product_Code__c from Product_Catalog__c];
        if(pcList!=null && pcList.size()>0)
        {
            for(Product_Catalog__c pc : pcList)
            {
                if(pc.Product_Code__c!=null && pc.Team_Instance__c!=null)
                    mapProductCodeToTIName.put(pc.Product_Code__c,pc.Team_Instance__r.Name);
            }
        }*/
        query = 'SELECT id,BRAND_ID__c,BRAND_NAME__c,PARTY_ID__c,QUESTION_ID__c,QUESTION_SHORT_TEXT__c,Response_Num__c,RESPONSE__c,SURVEY_ID__c,' +
        'SURVEY_NAME__c,Team_Instance__c ,Last_Updated__c FROM Staging_Veeva_Cust_Data__c WITH SECURITY_ENFORCED';
    }

    global StagingVeevaSurveyData(String teamInstance)
    {
        this.teamInstance = teamInstance;
        //scheduleJob = false;
        dailyJob = false;
        ondemand = false;
        externalid = new Set<String>();
        mapProductCodeToTIName = new Map<String, String>();

        query = 'SELECT id,BRAND_ID__c,BRAND_NAME__c,PARTY_ID__c,QUESTION_ID__c,QUESTION_SHORT_TEXT__c,Response_Num__c,RESPONSE__c,SURVEY_ID__c,' +
        'SURVEY_NAME__c,Team_Instance__c,Last_Updated__c FROM Staging_Veeva_Cust_Data__c WITH SECURITY_ENFORCED';
    }
    global StagingVeevaSurveyData(Boolean dailyJob, String survey)
    {
        externalid = new Set<String>();
        mapProductCodeToTIName = new Map<String, String>();

        this.dailyJob = dailyJob;
        scheduleJob = true;
        schedulerLog = true;
        query = 'SELECT id,BRAND_ID__c,BRAND_NAME__c,PARTY_ID__c,QUESTION_ID__c,QUESTION_SHORT_TEXT__c,Response_Num__c,RESPONSE__c,SURVEY_ID__c,' +
        'SURVEY_NAME__c,Team_Instance__c,Last_Updated__c FROM Staging_Veeva_Cust_Data__c WITH SECURITY_ENFORCED';
    }

    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        SnTDMLSecurityUtil.printDebugMessage('query-->' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Staging_Veeva_Cust_Data__c> scope)
    {
        String teamInstanceName;
        Staging_Cust_Survey_Profiling__c tempStagSurveyRespRec;
        String key = '';
        DateTime lastSubmittedDate;
        Boolean upsert_flag;
        List<Staging_Cust_Survey_Profiling__c> surveyResponseList = new List<Staging_Cust_Survey_Profiling__c>();
        Map<String, Staging_Cust_Survey_Profiling__c> surveyResponseMap = new Map<String, Staging_Cust_Survey_Profiling__c>();
        for(Staging_Veeva_Cust_Data__c veeva : scope)
        {
            tempStagSurveyRespRec = new Staging_Cust_Survey_Profiling__c();
            key = veeva.PARTY_ID__c + veeva.BRAND_ID__c + veeva.QUESTION_SHORT_TEXT__c;
            lastSubmittedDate = veeva.Last_Updated__c;
            upsert_flag = false;
            if(mapKeyDate.containsKey(key))
            {
                if(lastSubmittedDate != null && (mapKeyDate.get(key) == null || (mapKeyDate.get(key) != null && lastSubmittedDate > mapKeyDate.get(key))))
                {
                    mapKeyDate.put(key, lastSubmittedDate);
                    upsert_flag = true;
                }
            }
            else
            {
                mapKeyDate.put(key, lastSubmittedDate);
                upsert_flag = true;
            }
            //External id = AccountNumber+ProductCode+ShortQuestionText
            //if(externalid!=null && !externalid.contains(key))
            if(upsert_flag)
            {
                tempStagSurveyRespRec.BRAND_ID__c = veeva.BRAND_ID__c;
                tempStagSurveyRespRec.BRAND_NAME__c = veeva.BRAND_NAME__c;
                tempStagSurveyRespRec.External_ID__c = veeva.PARTY_ID__c + '_' + veeva.BRAND_ID__c + '_' + veeva.QUESTION_SHORT_TEXT__c;
                tempStagSurveyRespRec.PARTY_ID__c = veeva.PARTY_ID__c;
                tempStagSurveyRespRec.QUESTION_ID__c = veeva.QUESTION_ID__c;
                tempStagSurveyRespRec.QUESTION_SHORT_TEXT__c = veeva.QUESTION_SHORT_TEXT__c;
                tempStagSurveyRespRec.RESPONSE__c = veeva.RESPONSE__c;
                tempStagSurveyRespRec.SURVEY_ID__c = veeva.SURVEY_ID__c;
                tempStagSurveyRespRec.SURVEY_NAME__c = veeva.SURVEY_NAME__c;
                tempStagSurveyRespRec.Last_Updated__c = veeva.Last_Updated__c;
                /*teamInstanceName = tempStagSurveyRespRec.BRAND_ID__c!=null ? tempStagSurveyRespRec.BRAND_ID__c : '';
                tempStagSurveyRespRec.Team_Instance__c=mapProductCodeToTIName.get(teamInstanceName)!=null ? mapProductCodeToTIName.get(teamInstanceName) : teamInstance;*/
                //surveyResponseList.add(tempStagSurveyRespRec);
                surveyResponseMap.put(key, tempStagSurveyRespRec);
                externalid.add(key);
            }
        }
        //SnTDMLSecurityUtil.printDebugMessage('StagingVeevaSurveyData() execute() :set externalid-->'+externalid);
        //SnTDMLSecurityUtil.printDebugMessage('StagingVeevaSurveyData() execute() :set externalid size-->'+externalid.size());
        SnTDMLSecurityUtil.printDebugMessage('StagingVeevaSurveyData() execute() :surveyResponseList-->' + surveyResponseList);
        SnTDMLSecurityUtil.printDebugMessage('StagingVeevaSurveyData() execute() :surveyResponseList size-->' + surveyResponseList.size());
        if(surveyResponseMap.values() != null && surveyResponseMap.values().size() > 0)
        {
            /*recordsProcessed+=surveyResponseList.size();
            upsert surveyResponseList External_ID__c;*/
            recordsProcessed += surveyResponseMap.values().size();

            //upsert surveyResponseMap.values() External_ID__c;
            if (Schema.sObjectType.Staging_Cust_Survey_Profiling__c.fields.External_ID__c.isUpdateable() && Schema.sObjectType.Staging_Cust_Survey_Profiling__c.fields.External_ID__c.isCreateable() ) {

                SObjectAccessDecision securityDecision = Security.stripInaccessible(AccessType.UPSERTABLE, surveyResponseMap.values());
                List<Staging_Cust_Survey_Profiling__c> buRespUpsertList = securityDecision.getRecords();
                upsert buRespUpsertList External_ID__c;

            //throw exception if permission is missing
                if(!securityDecision.getRemovedFields().isEmpty() )
                {
                    if(surveyResponseMap.values()[0].External_ID__c != null)
                        SnTDMLSecurityUtil.printDMLMessage(securityDecision, 'StagingVeevaSurveyData', 'C' );
                    else
                        SnTDMLSecurityUtil.printDMLMessage(securityDecision, 'StagingVeevaSurveyData', 'U' );
                }
            }
            else{
                SnTDMLSecurityUtil.printDebugMessage('You dont have permission to access External_Id');
            }
        }
    }

    global void finish(Database.BatchableContext BC)
    {

        if(ondemand)
        {
            SnTDMLSecurityUtil.printDebugMessage('++inside ondemand' + ondemand);
            SnTDMLSecurityUtil.printDebugMessage('+++teamInstance_Prod' + teamInstance_Prod);
            Survey_Def_Prof_Para_Meta_Insert_Utility pacpBatch = new Survey_Def_Prof_Para_Meta_Insert_Utility(teamInstance_Prod, 'Dummy Test');
            Database.executeBatch(pacpBatch, 2000);
        }

        else
        {
            SnTDMLSecurityUtil.printDebugMessage('++inside normal');
            /*if(!schedulerLog)
            {
                SnTDMLSecurityUtil.printDebugMessage('StagingVeevaSurveyData() finish() : '+recordsProcessed + ' records processed.');
                //Update the scheduler log with successful
                Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID);
                SnTDMLSecurityUtil.printDebugMessage('sJob rec before-->'+sJob);
                sJob.No_Of_Records_Processed__c=recordsProcessed;
                sJob.Object_Name__c = 'Staging Veeva Cust Data';
                sJob.Job_Status__c='Successful';
                SnTDMLSecurityUtil.printDebugMessage('sJob++++++++'+sJob);
                update sJob;

                SnTDMLSecurityUtil.printDebugMessage('StagingVeevaSurveyData finish(): scheduleJob-->'+scheduleJob);
            }
            */
            /*if(!scheduleJob)
            {*/
                if(!System.Test.isRunningTest())
                {
                    SnTDMLSecurityUtil.printDebugMessage('+++teamInstance' + teamInstance);
                    Survey_Def_Prof_Para_Meta_Insert_Utility pacpBatch = new Survey_Def_Prof_Para_Meta_Insert_Utility(teamInstance);
                    Database.executeBatch(pacpBatch, 2000);
                }
            }

            if(dailyJob)
            {
                SnTDMLSecurityUtil.printDebugMessage('++dailyJob' + dailyJob);
                BatchVeevaSurveyCreation survey = new BatchVeevaSurveyCreation();
                database.executeBatch(survey, 200);
            }
        }
    }