global with sharing class BatchCreateAlignedCustomerFlag implements Database.Batchable<sObject>
{
    public String query;
    public String selectedTeamInstanceIs;
    public String teamInstance;
    public String batchID;
    public DateTime lastjobDate = null;
    public Boolean scheduleJob;
    public list<String> teamInst_ProdList; //Added by dhiren
    public String teamInstance_Prod;//Added by dhiren
    public String product;//Added by dhiren
    public boolean ondemand = false; //Added by dhiren
    public String loadType = '';
    public String selectedType = '';//Shivansh - A1450 -- for running transformation based on specific type

    //Added by HT(A0094) on 26th October 2020 for SR-299
    public String changeReqID;
    public Boolean proceedBatch = true;

    global BatchCreateAlignedCustomerFlag(String teamInstance)
    {
        scheduleJob = false;
        selectedTeamInstanceIs = teamInstance;
        query = 'Select Id, Name, CurrencyIsoCode, Last_Updated__c, Sales_Cycle__c, SURVEY_ID__c, SURVEY_NAME__c, Team_Instance__c, Team__c, Account_Number__c,' + 'Product_Code__c, Short_Question_Text1__c, Response1__c, Short_Question_Text2__c, Response2__c, Short_Question_Text3__c, Response3__c, Response4__c,Question_ID21__c,Question_ID22__c,Question_ID23__c,Question_ID24__c,Question_ID25__c,' + 'Response5__c, Response6__c, Response7__c, Response9__c, Response10__c, Response8__c,Response11__c,Response12__c,Response13__c,Response14__c,Response15__c,Response19__c,Response16__c,Response17__c,Response18__c,Response20__c,Short_Question_Text4__c, Short_Question_Text5__c,Short_Question_Text21__c,Short_Question_Text22__c,Short_Question_Text23__c,Short_Question_Text24__c,Short_Question_Text25__c,'
                + 'Short_Question_Text6__c, Short_Question_Text10__c, Short_Question_Text9__c, Short_Question_Text8__c, Short_Question_Text7__c, Short_Question_Text11__c,Short_Question_Text12__c,Short_Question_Text13__c,Short_Question_Text14__c,Short_Question_Text15__c,Short_Question_Text16__c,Short_Question_Text17__c,Short_Question_Text18__c,Short_Question_Text19__c,Short_Question_Text20__c,Question_ID1__c,' +
                'Question_ID2__c, Question_ID3__c, Question_ID4__c, Question_ID5__c, Question_ID6__c, Question_ID7__c, Question_ID8__c, Question_ID9__c, Question_ID10__c,Question_ID11__c,Question_ID12__c,Question_ID13__c,Question_ID14__c,Question_ID15__c,Question_ID16__c,Question_ID17__c,Question_ID18__c,Question_ID19__c,Question_ID20__c,Response21__c,Response22__c,Response23__c,Response24__c,Response25__c,' +
                ' Type__c, Product_Name__c, Position_Code__c FROM Staging_Survey_Data__c Where Team_Instance__c = \'' + selectedTeamInstanceIs + '\' WITH SECURITY_ENFORCED'; // Where Team_Instance__c = \'' + teamInstance + '\'
        this.query = query;
    }

    global BatchCreateAlignedCustomerFlag(String teamInstanceProd, String differentatior)
    {
        teamInstance_Prod = teamInstanceProd;
        SnTDMLSecurityUtil.printDebugMessage('<><><><><><><><>' + teamInstance_Prod);
        teamInst_ProdList = new list<String>();
        if(teamInstance_Prod.contains(';'))
        {
            teamInst_ProdList = teamInstance_Prod.split(';');
        }

        SnTDMLSecurityUtil.printDebugMessage('<><>teamInst_ProdList<><<>' + teamInst_ProdList);
        teamInstance = teamInst_ProdList[0];
        product = teamInst_ProdList[1];
        ondemand = true;

        SnTDMLSecurityUtil.printDebugMessage('<>>>>>>><>teamInstance<><><><><><><> ' + teamInstance + '  <>>>>>>>>product>>>>>>>>>> ' + product);

        scheduleJob = false;
        selectedTeamInstanceIs = teamInstance;

        query = 'Select Id, Name, CurrencyIsoCode, Last_Updated__c, Sales_Cycle__c, SURVEY_ID__c, SURVEY_NAME__c, Team_Instance__c, Team__c, Account_Number__c,' + 'Product_Code__c, Short_Question_Text1__c, Response1__c, Short_Question_Text2__c, Response2__c, Short_Question_Text3__c, Response3__c, Response4__c,Question_ID21__c,Question_ID22__c,Question_ID23__c,Question_ID24__c,Question_ID25__c,' + 'Response5__c, Response6__c, Response7__c, Response9__c, Response10__c, Response8__c,Response11__c,Response12__c,Response13__c,Response14__c,Response15__c,Response19__c,Response16__c,Response17__c,Response18__c,Response20__c,Short_Question_Text4__c, Short_Question_Text5__c,Short_Question_Text21__c,Short_Question_Text22__c,Short_Question_Text23__c,Short_Question_Text24__c,Short_Question_Text25__c,'
                + 'Short_Question_Text6__c, Short_Question_Text10__c, Short_Question_Text9__c, Short_Question_Text8__c, Short_Question_Text7__c, Short_Question_Text11__c,Short_Question_Text12__c,Short_Question_Text13__c,Short_Question_Text14__c,Short_Question_Text15__c,Short_Question_Text16__c,Short_Question_Text17__c,Short_Question_Text18__c,Short_Question_Text19__c,Short_Question_Text20__c,Question_ID1__c,' +
                'Question_ID2__c, Question_ID3__c, Question_ID4__c, Question_ID5__c, Question_ID6__c, Question_ID7__c, Question_ID8__c, Question_ID9__c, Question_ID10__c,Question_ID11__c,Question_ID12__c,Question_ID13__c,Question_ID14__c,Question_ID15__c,Question_ID16__c,Question_ID17__c,Question_ID18__c,Question_ID19__c,Question_ID20__c,Response21__c,Response22__c,Response23__c,Response24__c,Response25__c,' +
                ' Type__c, Product_Name__c, Position_Code__c FROM Staging_Survey_Data__c where Product_Code__c =: product and Team_Instance__c = \' WITH SECURITY_ENFORCED' + selectedTeamInstanceIs + '\''; // Where Team_Instance__c = \'' + teamInstance + '\'
        this.query = query;
    }

    global BatchCreateAlignedCustomerFlag(String teamInstance, String differentatior, String type)
    {
        loadType = type;
        scheduleJob = false;
        selectedTeamInstanceIs = teamInstance;

        query = 'Select Id, Name, CurrencyIsoCode, Last_Updated__c, Sales_Cycle__c, SURVEY_ID__c, SURVEY_NAME__c, Team_Instance__c, Team__c, Account_Number__c,' + 'Product_Code__c, Short_Question_Text1__c, Response1__c, Short_Question_Text2__c, Response2__c, Short_Question_Text3__c, Response3__c, Response4__c,Question_ID21__c,Question_ID22__c,Question_ID23__c,Question_ID24__c,Question_ID25__c,' + 'Response5__c, Response6__c, Response7__c, Response9__c, Response10__c, Response8__c,Response11__c,Response12__c,Response13__c,Response14__c,Response15__c,Response19__c,Response16__c,Response17__c,Response18__c,Response20__c,Short_Question_Text4__c, Short_Question_Text5__c,Short_Question_Text21__c,Short_Question_Text22__c,Short_Question_Text23__c,Short_Question_Text24__c,Short_Question_Text25__c,'
                + 'Short_Question_Text6__c, Short_Question_Text10__c, Short_Question_Text9__c, Short_Question_Text8__c, Short_Question_Text7__c, Short_Question_Text11__c,Short_Question_Text12__c,Short_Question_Text13__c,Short_Question_Text14__c,Short_Question_Text15__c,Short_Question_Text16__c,Short_Question_Text17__c,Short_Question_Text18__c,Short_Question_Text19__c,Short_Question_Text20__c,Question_ID1__c,' +
                'Question_ID2__c, Question_ID3__c, Question_ID4__c, Question_ID5__c, Question_ID6__c, Question_ID7__c, Question_ID8__c, Question_ID9__c, Question_ID10__c,Question_ID11__c,Question_ID12__c,Question_ID13__c,Question_ID14__c,Question_ID15__c,Question_ID16__c,Question_ID17__c,Question_ID18__c,Question_ID19__c,Question_ID20__c,Response21__c,Response22__c,Response23__c,Response24__c,Response25__c,' +
                'Type__c,Product_Name__c, Position_Code__c FROM Staging_Survey_Data__c Where Team_Instance__c = \'' + selectedTeamInstanceIs + '\' WITH SECURITY_ENFORCED'; // Where Team_Instance__c = \'' + teamInstance + '\'
        this.query = query;
    }

    //Added by HT(A0094) on 26th October 2020 for SR-299
    global BatchCreateAlignedCustomerFlag(String teamInstance, String differentatior, String type,String crID)
    {
        loadType = type;
        scheduleJob = false;
        selectedTeamInstanceIs = teamInstance;
        changeReqID = crID;

        query = 'Select Id, Name, CurrencyIsoCode, Last_Updated__c, Sales_Cycle__c, SURVEY_ID__c, SURVEY_NAME__c, Team_Instance__c, Team__c, Account_Number__c,' + 'Product_Code__c, Short_Question_Text1__c, Response1__c, Short_Question_Text2__c, Response2__c, Short_Question_Text3__c, Response3__c, Response4__c,Question_ID21__c,Question_ID22__c,Question_ID23__c,Question_ID24__c,Question_ID25__c,' + 'Response5__c, Response6__c, Response7__c, Response9__c, Response10__c, Response8__c,Response11__c,Response12__c,Response13__c,Response14__c,Response15__c,Response19__c,Response16__c,Response17__c,Response18__c,Response20__c,Short_Question_Text4__c, Short_Question_Text5__c,Short_Question_Text21__c,Short_Question_Text22__c,Short_Question_Text23__c,Short_Question_Text24__c,Short_Question_Text25__c,'
                + 'Short_Question_Text6__c, Short_Question_Text10__c, Short_Question_Text9__c, Short_Question_Text8__c, Short_Question_Text7__c, Short_Question_Text11__c,Short_Question_Text12__c,Short_Question_Text13__c,Short_Question_Text14__c,Short_Question_Text15__c,Short_Question_Text16__c,Short_Question_Text17__c,Short_Question_Text18__c,Short_Question_Text19__c,Short_Question_Text20__c,Question_ID1__c,' +
                'Question_ID2__c, Question_ID3__c, Question_ID4__c, Question_ID5__c, Question_ID6__c, Question_ID7__c, Question_ID8__c, Question_ID9__c, Question_ID10__c,Question_ID11__c,Question_ID12__c,Question_ID13__c,Question_ID14__c,Question_ID15__c,Question_ID16__c,Question_ID17__c,Question_ID18__c,Question_ID19__c,Question_ID20__c,Response21__c,Response22__c,Response23__c,Response24__c,Response25__c,' +
                'Type__c,Product_Name__c, Position_Code__c FROM Staging_Survey_Data__c Where Team_Instance__c = \'' + selectedTeamInstanceIs + '\' WITH SECURITY_ENFORCED'; // Where Team_Instance__c = \'' + teamInstance + '\'
        this.query = query;
    }

    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Staging_Survey_Data__c> scope)
    {
        try
        {
            Set<string> accountNumberSet = new Set<string>(); //set for unique acc number
            Map<string, id> accountNumberIdMap = new Map<string, id>(); //map acc number to acc id

            // Changes for SNT - 66 by Shivansh A1450
            Set<String> accountNumberPosIdSet = new Set<String>(); //map of account number with list of Pos Acc recs

            for(Staging_Survey_Data__c stagingSurvData : scope)
            {
                if(!string.IsBlank(stagingSurvData.Account_Number__c))
                {
                    //Accounts Set
                    accountNumberSet.add(stagingSurvData.Account_Number__c);
                }
            }

            List<AxtriaSalesIQTM__Team_Instance__c> allTeamInstances = [SELECT Id, Include_Territory__c, Segmentation_Universe__c, Condition_For_Position_Account__c FROM AxtriaSalesIQTM__Team_Instance__c WHERE Name = :selectedTeamInstanceIs WITH SECURITY_ENFORCED];

            if(allTeamInstances != null && allTeamInstances.size() > 0)
            {
                if(allTeamInstances[0].Segmentation_Universe__c == 'Full S&T Input Customers')
                {
                    List<Staging_Veeva_Cust_Data__c> surveyVeevaDataList = [Select id, PARTY_ID__c, BRAND_ID__c, Position_Code__c From Staging_Veeva_Cust_Data__c Where PARTY_ID__c IN: accountNumberIdMap.keySet()];
                    SnTDMLSecurityUtil.printDebugMessage('SurveyVeevaDataList size :: ' + surveyVeevaDataList.size());

                    Map<String, String> accountNoToPosClientCodeMap = new Map<String, String>();
                    for(Staging_Veeva_Cust_Data__c surveyVeevaData : surveyVeevaDataList)
                    {
                        if(surveyVeevaData.PARTY_ID__c != null && surveyVeevaData.Position_Code__c != null && surveyVeevaData.Position_Code__c != '')
                        {
                            accountNoToPosClientCodeMap.put(surveyVeevaData.PARTY_ID__c, surveyVeevaData.Position_Code__c);
                        }
                    }

                    String queryString = 'SELECT id,AxtriaSalesIQTM__Account__r.AccountNumber,Position_Code__c FROM AxtriaSalesIQTM__Position_Account__c';
                    queryString += ' WHERE AxtriaSalesIQTM__Account__r.AccountNumber IN :accountNumberSet and (AXTRIASALESIQTM__ASSIGNMENT_STATUS__C = \'Active\' OR AXTRIASALESIQTM__ASSIGNMENT_STATUS__C = \'Future Active\' ) and AxtriaSalesIQTM__Team_Instance__r.name = \'' + selectedTeamInstanceIs + '\' and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c != \'00000\' ';

                    if(allTeamInstances[0].Condition_For_Position_Account__c != null && allTeamInstances[0].Condition_For_Position_Account__c != '')
                    {
                        queryString += 'and ( ' + allTeamInstances[0].Condition_For_Position_Account__c + ' )' ;
                    }

                    queryString += ' WITH SECURITY_ENFORCED';

                    List<AxtriaSalesIQTM__Position_Account__c> posAccList = new List<AxtriaSalesIQTM__Position_Account__c>();
                    posAccList = Database.query(queryString);
                    if(allTeamInstances[0].Include_Territory__c)
                    {
                        for(AxtriaSalesIQTM__Position_Account__c acc : posAccList)
                        {
                            if(acc.Position_Code__c == accountNoToPosClientCodeMap.get(acc.AxtriaSalesIQTM__Account__r.AccountNumber))
                            {
                                if(!accountNumberPosIdSet.contains(acc.AxtriaSalesIQTM__Account__r.AccountNumber))
                                {
                                    accountNumberPosIdSet.add(acc.AxtriaSalesIQTM__Account__r.AccountNumber);
                                }
                            }
                        }
                        SnTDMLSecurityUtil.printDebugMessage('1. accountNumberPosIdSet size :: ' + accountNumberPosIdSet.size());
                    }
                    else
                    {
                        for(AxtriaSalesIQTM__Position_Account__c acc : posAccList)
                        {
                            if(!accountNumberPosIdSet.contains(acc.AxtriaSalesIQTM__Account__r.AccountNumber))
                            {
                                accountNumberPosIdSet.add(acc.AxtriaSalesIQTM__Account__r.AccountNumber);
                            }
                        }
                        SnTDMLSecurityUtil.printDebugMessage('2. accountNumberPosIdSet size :: ' + accountNumberPosIdSet.size());
                    }

                    for(Staging_Survey_Data__c stagingSurvData : scope)
                    {
                        if(!string.IsBlank(stagingSurvData.Account_Number__c))
                        {
                            //Accounts Set
                            stagingSurvData.Question_ID25__c = 25;
                            stagingSurvData.Short_Question_Text25__c = 'Aligned Customer Flag';
                            if(accountNumberPosIdSet.contains(stagingSurvData.Account_Number__c))
                            {
                                stagingSurvData.Response25__c = 'TRUE';
                            }
                            else
                            {
                                stagingSurvData.Response25__c = 'FALSE';
                            }
                        }
                    }

                    //update scope;
                    SnTDMLSecurityUtil.updateRecords(scope, 'BatchCreateAlignedCustomerFlag');
                }
            }
        }
        catch(Exception e)
        {
            SnTDMLSecurityUtil.printDebugMessage('Error in BatchCreateAlignedCustomerFlag : execute()--'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack trace--'+e.getStackTraceString());
            proceedBatch = false;
        }
    }

    global void finish(Database.BatchableContext BC)
    {
        if(loadType == 'File' || loadType == 'Both')
        {
            if(ondemand)
            {
                AxtriaSalesIQTM__Team_Instance__c teamInst = [ Select id, isVeevaSurveyScheduled__c from AxtriaSalesIQTM__Team_Instance__c where Name = : selectedTeamInstanceIs WITH SECURITY_ENFORCED];
                Boolean flagteamInst = teamInst.isVeevaSurveyScheduled__c;

                if(flagteamInst)
                {
                    StagingVeevaSurveyData batch = new StagingVeevaSurveyData(teamInstance_Prod, 'Dummy String');
                    Database.executeBatch(batch, 1000);
                }
                else
                {
                    Survey_Data_Transformation_Utility batch = new Survey_Data_Transformation_Utility(teamInstance_Prod, 'Dummy String');
                    Database.executeBatch(batch, 400);
                }
            }
            else
            {
                //Added by HT(A0994) on 26th October 2020 for SR-299
                if(changeReqID!=null && changeReqID!='')
                {
                    if(proceedBatch)
                    {
                        Survey_Data_Transformation_Utility batch = new Survey_Data_Transformation_Utility(selectedTeamInstanceIs,Id.valueOf(changeReqID));
                        Database.executeBatch(batch, 400);
                    }
                    else{
                        SalesIQUtility.updateCRStatusDirectLoad(changeReqID,'Error',0);
                    }
                }
                else
                {
                    //In case of ty
                    Survey_Data_Transformation_Utility batch = new Survey_Data_Transformation_Utility(selectedTeamInstanceIs);
                    Database.executeBatch(batch, 400);
                }
            }
        }
        else
        {
            //This is for the Scheduled Veeva batch
            StagingVeevaSurveyData batch = new StagingVeevaSurveyData(teamInstance);
            Database.executeBatch(batch, 1000);
        }
    }
}