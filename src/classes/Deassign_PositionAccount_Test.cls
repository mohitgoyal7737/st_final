/*
@author - Ayush Rastogi (A2412)
@description - Test class to test Deassign_PositionAccount.
*/
@isTest
public class Deassign_PositionAccount_Test{
    
    static testMethod void testMethod1() {
        String className = 'Deassign_PositionAccount_Test';
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Name ='USA';
        insert countr;
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc.AxtriaSalesIQTM__Country__c = countr.id;
        acc.AccountNumber = 'BH10461999';
        acc.AxtriaSalesIQTM__AccountType__c = 'HCP';
        acc.Status__c = 'Active';
        acc.Type = 'HCP';
        insert acc;
        Account acc1= TestDataFactory.createAccount();
        acc1.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc1.AxtriaSalesIQTM__Country__c = countr.id;
        acc1.AccountNumber = 'BH10461969';
        acc1.AxtriaSalesIQTM__AccountType__c = 'HCA';
        acc1.Status__c = 'Active';
        acc1.Type = 'HCA';
        insert acc1;
        Account acc11= TestDataFactory.createAccount();
        acc11.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc11.AxtriaSalesIQTM__Country__c = countr.id;
        acc11.AccountNumber = 'BH10462969';
        acc11.AxtriaSalesIQTM__AccountType__c = 'HCA';
        acc11.Status__c = 'Active';
        acc11.Type = 'HCA';
        insert acc11;
        AxtriaSalesIQTM__Affiliation_Network__c affnet = TestDataFactory.createAffliNet(countr);
        insert affnet;
        AxtriaSalesIQTM__Account_Affiliation__c accaff = TestDataFactory.createAcctAffli(acc,affnet);
        accaff.Account_Number__c = 'BH10461999';
        accaff.AxtriaSalesIQTM__Root_Account__c = acc.id;
        accaff.AxtriaSalesIQTM__Parent_Account__c = acc.Id;
        accaff.AxtriaSalesIQTM__Account__c = acc.Id;
        accaff.Affiliation_Status__c ='Active';
        accaff.AxtriaSalesIQTM__Is_Primary__c = true;
        accaff.AxtriaSalesIQTM__Active__c = true;
        insert accaff;
        AxtriaSalesIQTM__Account_Affiliation__c accaff1 = TestDataFactory.createAcctAffli(acc1,affnet);
        accaff1.Account_Number__c = 'BH10461969';
        accaff1.AxtriaSalesIQTM__Root_Account__c = acc.id;
        accaff1.AxtriaSalesIQTM__Parent_Account__c = acc.Id;
        accaff1.AxtriaSalesIQTM__Account__c = acc1.Id;
        accaff1.Affiliation_Status__c ='Active';
        accaff1.AxtriaSalesIQTM__Is_Primary__c = true;
        accaff1.AxtriaSalesIQTM__Active__c = true;
        insert accaff1;
        AxtriaSalesIQTM__Account_Affiliation__c accaff11 = TestDataFactory.createAcctAffli(acc,affnet);
        accaff11.Account_Number__c = 'BH10462969';
        accaff11.AxtriaSalesIQTM__Root_Account__c = acc11.id;
        accaff11.AxtriaSalesIQTM__Parent_Account__c = acc11.Id;
        accaff11.AxtriaSalesIQTM__Account__c = acc.Id;
        accaff11.Affiliation_Status__c ='Active';
        accaff11.AxtriaSalesIQTM__Is_Primary__c = true;
        accaff11.AxtriaSalesIQTM__Active__c = true;
        insert accaff11;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        insert scen;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Name = 'test';
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        //teamins.AxtriaSalesIQTM__Team_Cycle_Name__c = 'Current';
        //teamins.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        insert teamins;
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__inactive__c = false;
        insert pos;
         AxtriaSalesIQTM__Scenario__c scen2 = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen2.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S12';
        insert scen2;
        
        AxtriaSalesIQTM__Team_Instance__c teamins2 = TestDataFactory.createTeamInstance(team);
        teamins2.AxtriaSalesIQTM__Scenario__c = scen2.id;
        teamins2.Name = 'test2';
        teamins2.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        //teamins.AxtriaSalesIQTM__Team_Cycle_Name__c = 'Current';
        //teamins.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        insert teamins2;
        AxtriaSalesIQTM__Position__c pos2= TestDataFactory.createPosition(team,teamins2);
        pos.AxtriaSalesIQTM__inactive__c = false;
        insert pos2;
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        posAccount.AxtriaSalesIQTM__Position__c = pos.id;
        posAccount.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        //AxtriaSalesIQTM__Assignment_Status__c 
        posAccount.AxtriaSalesIQTM__Account__c = acc.id;
        posAccount.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        posAccount.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        insert posAccount;
        
        AxtriaSalesIQTM__Position_Account__c posAccount1 = TestDataFactory.createPositionAccount(acc1,pos,teamins);
        posAccount1.AxtriaSalesIQTM__Position__c = pos.id;
        posAccount1.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        posAccount1.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        posAccount1.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        insert posAccount1;
        
        AxtriaSalesIQTM__Position_Account__c posAccount11 = TestDataFactory.createPositionAccount(acc1,pos2,teamins2);
        posAccount11.AxtriaSalesIQTM__Position__c = pos.id;
        posAccount11.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        posAccount11.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        posAccount11.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        insert posAccount11;
        
        AxtriaSalesIQTM__Position_Account__c posAccount111 = TestDataFactory.createPositionAccount(acc11,pos,teamins);
        posAccount111.AxtriaSalesIQTM__Position__c = pos.id;
        posAccount111.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        posAccount111.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        posAccount111.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        insert posAccount111;
        
        Deassign_Postiton_Account__c daposacc = new Deassign_Postiton_Account__c();
        daposacc.Account_Type__c = acc.AxtriaSalesIQTM__AccountType__c;
        daposacc.Account__c = acc.id;
        daposacc.Country_Name__c = countr.Name;
        daposacc.CurrencyIsoCode = 'USD';
        daposacc.Position__c = pos.id;
        daposacc.Rule_Type__c = 'test';
        daposacc.Status__c = 'New';
        daposacc.Team_Instance__c = teamins.Id;
        daposacc.Name ='test';
        insert daposacc;
        
        temp_Obj__c zt = new temp_Obj__c();
        zt.Status__c ='New';
        zt.Account_Text__c = acc1.AccountNumber;
        zt.Position_Text__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        zt.country__c = 'USA';
        zt.Team_Instance_Text__c = teamins.Name;
        zt.Team_Instance__c = teamins.id;
        zt.AccountNumber__c = acc1.AccountNumber;
        zt.Account_Type__c = acc1.AxtriaSalesIQTM__AccountType__c ;
        zt.Team_Instance_Name__c = teamins.Name;
        zt.Position_Code__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        zt.Rule_Type__c = 'Bottom Up';
        zt.Object__c ='Deassign_Postiton_Account__c';
        insert zt;

        zt = new temp_Obj__c();
        zt.Status__c ='New';
        zt.Account_Text__c = acc.AccountNumber;
        zt.Position_Text__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        zt.country__c = 'USA';
        zt.Team_Instance_Text__c = teamins.Name;
        zt.Team_Instance__c = teamins.id;
        zt.AccountNumber__c = acc.AccountNumber;
        zt.Account_Type__c = acc.AxtriaSalesIQTM__AccountType__c ;
        zt.Team_Instance_Name__c = teamins.Name;
        zt.Position_Code__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        zt.Rule_Type__c = 'Bottom Up';
        zt.Object__c ='Deassign_Postiton_Account__c';
        insert zt;

        zt = new temp_Obj__c();
        zt.Status__c ='New';
        zt.Account_Text__c = acc11.AccountNumber;
        zt.Position_Text__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        zt.country__c = 'USA';
        zt.Team_Instance_Text__c = teamins.Name;
        zt.Team_Instance__c = teamins.id;
        zt.AccountNumber__c = acc11.AccountNumber;
        zt.Account_Type__c = acc11.AxtriaSalesIQTM__AccountType__c ;
        zt.Team_Instance_Name__c = teamins.Name;
        zt.Position_Code__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        zt.Rule_Type__c = 'Bottom Up';
        zt.Object__c ='Deassign_Postiton_Account__c';
        insert zt;


        
        AxtriaSalesIQTM__Business_Rules__c b = new AxtriaSalesIQTM__Business_Rules__c();
        b.AxtriaSalesIQTM__Scenario__c = scen.id;
        b.AxtriaSalesIQTM__Rule_Type__c = 'Bottom Up';
        insert b;
        Set<String> setDeassignID =new Set<String>();
        setDeassignID.add(zt.id);
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = :className];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> FIELD_LIST = new List<String>{nameSpace+'AccountNumber__c',nameSpace+'Account_Text__c'};
            //System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(temp_Obj__c.SObjectType, FIELD_LIST, false));
            Deassign_PositionAccount obj=new Deassign_PositionAccount();
            Database.executeBatch(obj);
            
        }
        Test.stopTest();
    }
}