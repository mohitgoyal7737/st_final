public class UpdateProfileParametersTriggerhandler {
       public static set<id> accountId=new set<id>();
    public static void fetchaccounts(list<Account>triggernew, Map<Id, Account> triggerOldMap){
        
        List<Account> acclist = new list<Account>();
        List<Account> inactivelist = new list<Account>();
        set<String> acIdList=new set<String>();
        set<String> acIds=new set<String>();
        set<String> parAcIdList=new set<String>();
          List<Account> updatelist = new list<Account>();
        List<Account> updateAccList=new list<Account>();
        List<AxtriaSalesIQTM__Account_Affiliation__c> accaffList=new List<AxtriaSalesIQTM__Account_Affiliation__c>();
          List<AxtriaSalesIQTM__Account_Affiliation__c> accaffList1=new List<AxtriaSalesIQTM__Account_Affiliation__c>();
          
          
        for(Account a : triggernew){
            acIdList.add(a.id);
        }
        System.debug('acIdList'+acIdList);
        accaffList=[Select Id,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Parent_Account__c from AxtriaSalesIQTM__Account_Affiliation__c  where AxtriaSalesIQTM__Account__c IN:acIdList or AxtriaSalesIQTM__Parent_Account__c IN:acIdList];
        
        for(AxtriaSalesIQTM__Account_Affiliation__c aff:accaffList){
        if(aff.AxtriaSalesIQTM__Parent_Account__c!=null){
        acIds.add(aff.AxtriaSalesIQTM__Parent_Account__c);
        }
        if(aff.AxtriaSalesIQTM__Account__c!=null){
        acIds.add(aff.AxtriaSalesIQTM__Account__c);
        }
        //parAcIdList.add(aff.AxtriaSalesIQTM__Parent_Account__c );
        }
        System.debug('acIds'+acIds);
        accaffList1=[Select Id,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Parent_Account__c from AxtriaSalesIQTM__Account_Affiliation__c  where AxtriaSalesIQTM__Account__c IN:acIds or AxtriaSalesIQTM__Parent_Account__c IN:acIds];
        for(AxtriaSalesIQTM__Account_Affiliation__c aff:accaffList1){
        if(aff.AxtriaSalesIQTM__Parent_Account__c!=null){
            acIds.add(aff.AxtriaSalesIQTM__Parent_Account__c);
        }
        if(aff.AxtriaSalesIQTM__Account__c!=null){
         acIds.add(aff.AxtriaSalesIQTM__Account__c);
        }
        }
        System.debug('acIds'+acIds);
        updatelist =[Select Id from Account where Id IN:acIds ];
        System.debug('updatelist '+updatelist );
        if(updatelist.size()>0){
        for(Account a:updatelist){
        if(!accountId.contains(a.id)){
        updateAccList.add(a);
        accountId.add(a.id);
        }     
        }
        }
        System.debug('+++');
        if(updateAccList.size()>0){
        System.debug('size'+updateAccList.size());
        update updateAccList;
        }
        System.debug('----Called fetch Accounts---');
        for(Account a : triggernew){
            Account oldAccount = triggerOldMap.get(a.Id);
            if((String.isBlank(a.Profile_Consent__c) || a.Profile_Consent__c=='NO') && oldAccount.Profile_Consent__c == 'YES'){
                acclist.add(a);
            }
            system.debug('---Profile Consent is:'+a.Profile_Consent__c);
        }
        system.debug('---Account in trigger are:'+acclist);
        if(acclist.size()>0){
            //updatecallplan(acclist);
        }
        
        for(Account a : triggernew){
            Account oldAccount = triggerOldMap.get(a.Id);
            if((a.Status__c=='Inactive' && oldAccount.Status__c == 'Active') ||(a.Right_to_be_forgotten__c == 'True' && oldAccount.Right_to_be_forgotten__c != 'True')){
                inactivelist.add(a);
            }
        }
        system.debug('---Account in second trigger are:'+inactivelist);
        if(inactivelist.size()>0){
            deletecallplan(inactivelist);
            updateposacct(inactivelist);
        }
        
    }
        public static void updateposacct(list<Account>updatedaccounts){
         system.debug('---Updateposacct called--');
         list<id> accounts = new list<id>();
         list<AxtriaSalesIQTM__Position_Account__c>posacctupdate = new list<AxtriaSalesIQTM__Position_Account__c>();
         list<AxtriaSalesIQTM__Position_Account__c>posacct = new list<AxtriaSalesIQTM__Position_Account__c>();
         
         for(Account ac : updatedaccounts){
           accounts.add(ac.id);
         }
         system.debug('---Account id are:'+accounts);
         posacct=[select id,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Account__c in:accounts];
       
        if(posacct.size()>0){
            for(AxtriaSalesIQTM__Position_Account__c p : posacct){
                if(p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c.contains('Future')){
                    p.AxtriaSalesIQTM__Effective_End_Date__c= p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c.addDays(-1);    
                }
                else{
                    p.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addDays(-1);
                }
        }
        update posacct;
        }
        
       
       
    }
    
    public static void deletecallplan(list<Account>updatedaccounts){
         system.debug('---Updatecallplan1 called--');
       list<id> accounts = new list<id>(); 
       list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>callplandelete = new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
       for(Account ac : updatedaccounts){
           accounts.add(ac.id);
       }
       system.debug('---Account id are:'+accounts);
       callplandelete=[select id from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Account__c in:accounts];
       
       delete callplandelete;
    }
    
 /* The below code is commented as now it is not required*/
 /*
    public static void updatecallplan(list<Account>updatedaccounts){
        system.debug('---Updatecallplan called--');
       list<id> accounts = new list<id>(); 
       list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>pacp = new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
       list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>updatepacp = new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
       for(Account ac : updatedaccounts){
           accounts.add(ac.id);
       }
       system.debug('---Account id are:'+accounts);
       pacp=[select id,segment__c,Final_TCF__c,Final_TCF_Approved__c,Final_TCF_Original__c,Proposed_TCF__c,Proposed_TCF2__c,Calculated_TCF__c,Calculated_TCF2__c,Segment2__c,Parameter1__c,Parameter2__c,Parameter3__c,Parameter4__c,Parameter5__c,Parameter6__c,Parameter7__c,Parameter8__c,P2_Parameter1__c,P2_Parameter2__c,P2_Parameter3__c,P2_Parameter4__c,P2_Parameter5__c,P2_Parameter6__c,P2_Parameter7__c,P2_Parameter8__c,P3_Parameter1__c,P3_Parameter2__c,P3_Parameter3__c,P3_Parameter4__c,P3_Parameter5__c,P3_Parameter6__c,P3_Parameter7__c,P3_Parameter8__c,P4_Parameter1__c,P4_Parameter2__c,P4_Parameter3__c,P4_Parameter4__c,P4_Parameter5__c,P4_Parameter6__c,P4_Parameter7__c,P4_Parameter8__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.name from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Account__c in:accounts];
       if(pacp.size()>0){
           for(AxtriaSalesIQTM__Position_Account_Call_Plan__c p : pacp){
                AxtriaSalesIQTM__Position_Account_Call_Plan__c pac = new AxtriaSalesIQTM__Position_Account_Call_Plan__c();
                    pac.id= p.id;
                
                    pac.P2_Parameter1__c = ''; 
                    pac.P2_Parameter2__c = '';
                    pac.P2_Parameter3__c = '';
                    pac.P2_Parameter4__c = '';
                    pac.P2_Parameter5__c = '';
                    pac.P2_Parameter6__c = '';
                    pac.P2_Parameter7__c = '';
                    pac.P2_Parameter8__c = '';
                   // pac.P3__c = '';
                    pac.P3_Parameter1__c = '';
                    pac.P3_Parameter2__c = '';
                    pac.P3_Parameter3__c = '';
                    pac.P3_Parameter4__c = '';
                    pac.P3_Parameter5__c = '';
                    pac.P3_Parameter6__c = '';
                    pac.P3_Parameter7__c = '';
                    pac.P3_Parameter8__c = '';
                    
                    //pac.P4__c = '';
                    pac.P4_Parameter1__c = '';
                    pac.P4_Parameter2__c = '';
                    pac.P4_Parameter3__c  = '';
                    pac.P4_Parameter4__c = '';
                    pac.P4_Parameter6__c = '';
                    pac.P4_Parameter7__c = '';
                    pac.P4_Parameter8__c = '';
                    
                    
                    pac.Parameter3__c = '';
                    pac.Parameter4__c = '';
                    pac.Parameter5__c = '';
                    pac.Parameter6__c = '';
                    pac.Parameter7__c = '';
                    pac.Parameter8__c= '';
                    if(pac.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.name !='ONCOLOGY'){
                      pac.Parameter1__c = '';
                      pac.Parameter2__c = ''; 
                    }



                    pac.segment__c = '';
                    pac.Final_TCF__c=null;
                    pac.Final_TCF_Approved__c = null;
                    pac.Final_TCF_Original__c = null;
                    pac.Proposed_TCF__c = null;
                    pac.Proposed_TCF2__c = null;
                    pac.Calculated_TCF__c = null;
                    pac.Calculated_TCF2__c = null;
                    
                    pac.segment__c = 'ND';
                    
                    
                    
                    
                    pac.Segment2__c = '';
                    updatepacp.add(pac);
               //System.debug('----Pacp Cout is:'+pacp.size());
               //System.debug('----Pacp  is:'+pacp);
           }
       }
       */
       /*
       list<BU_Response__c> bu= [select id,Brand__c,Business_Unit__c,Cycle__c,Cycle2__c,Line__c,Physician__c,Response1__c,Response2__c,Response3__c,Response4__c,Response5__c,Response6__c,Response7__c,Response8__c,Response9__c,Response10__c from BU_Response__c where Physician__c in:Accounts];
       list<BU_Response__c> updatebu = new list<BU_Response__c>();
       if(bu.size()>0){
        for(BU_Response__c b : bu){
            b.Response1__c = '';
            b.Response2__c = '';
            b.Response3__c = '';
            b.Response4__c = '';
            b.Response5__c = '';
            b.Response6__c = '';
            b.Response7__c = '';
            b.Response8__c = '';
            b.Response9__c = '';
            b.Response10__c = '';
           // b.Brand__c = '';
           // b.Business_Unit__c = '';
           // b.Cycle__c='';
           // b.Cycle2__c = '';
           // b.Line__c = '';
           // b.Physician__c = '';
            
        }
           update bu;
       }
       */
       
       /*list<Survey_Response__c> sr = [select id,Cycle__c,Physician__c,Response__c,Cycle_ID__c from Survey_Response__c where Physician__c in :Accounts ];
       for(Survey_Response__c s : sr){
           s.Response__c = '';
       }
       update sr;
       
       list<Rating_Response__c> rr = [select id,Physician__c,Response__c from Rating_Response__c where Physician__c in: Accounts];
       for(Rating_Response__c r : rr){
           r.Response__c = '';
       }
       update rr;
       
       try{
           CallPlanSummaryTriggerHandler.execute_trigger = false;
           update updatepacp;
           CallPlanSummaryTriggerHandler.execute_trigger = true;
           System.debug('----updatepacp  is:'+updatepacp);
           list<String> updateCallPlanSegment = new list<String>();
           if(updatepacp != null && updatepacp.size() > 0){
                for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pa: updatepacp){
                    updateCallPlanSegment.add(pa.Id);
                }
            }
            if(updateCallPlanSegment != null && updateCallPlanSegment.size() > 0){
                system.debug('----Calling Execute helper---');
                ruleExecuteHelper.updateSegment(updateCallPlanSegment, true);
                
                system.debug('----Called Execute helper---');
                
            }
           
       }
       catch(Exception e){
       System.debug('---Exception Caught in updating the profile perametrs---');    
        System.debug('--- error: ' + e.getMessage());
        System.debug('--- error: ' + e.getStackTraceString());
       }
        
    }*/

}