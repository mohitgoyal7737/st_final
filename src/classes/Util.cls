public class Util{

    //Added by HT(A0994) on 2nd July 2020 for SNT-486
    public static Set<String> getFullDeltaLoadCountries()
    {
        List<AxtriaSalesIQTM__Country__c> allCountries = [select id from AxtriaSalesIQTM__Country__c where (Load_Type__c = 'Full Load' or Load_Type__c='Delta') WITH SECURITY_ENFORCED];
        Set<String> allCountriesList = new Set<String>();

        for(AxtriaSalesIQTM__Country__c vcs : allCountries){
            allCountriesList.add(vcs.Id);
        }
        return allCountriesList;
    }

    //Added by HT(A0994) on 2nd July 2020 for SNT-486
    public static Set<String> getFullDeltaTeamInstances()
    {
        Set<String> allTeamInstances = new Set<String>();
        Set<String> countriesList = new Set<String>();
        countriesList = getFullDeltaLoadCountries();

        if(countriesList.size() > 0)
        {
            for(AxtriaSalesIQTM__Team_Instance__c ti : [select id from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c in :countriesList and AxtriaSalesIQTM__Alignment_Period__c = 'Current' WITH SECURITY_ENFORCED]) //and AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Live' and AxtriaSalesIQTM__Team__r.AxtriaSalesIQST__hasCallPlan__c = true])
            {
                allTeamInstances.add(ti.ID);
            }
        }
        return allTeamInstances;
    }

    public static set<String> getFulloadCountries()
    {
        List<AxtriaSalesIQTM__Country__c> allCountries = [select id from AxtriaSalesIQTM__Country__c where Load_Type__c = 'Full Load' WITH SECURITY_ENFORCED];

        set<String> allCountriesList = new set<String>();

        for(AxtriaSalesIQTM__Country__c vcs : allCountries)
        {
            allCountriesList.add(vcs.Id);
        }

        return allCountriesList;
    }
    
    public static set<String> getFulloadCountryCode()
    {
        List<AxtriaSalesIQTM__Country__c> allCountries = [select id, AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c where Load_Type__c = 'Full Load' WITH SECURITY_ENFORCED];

        set<String> allCountriesList = new set<String>();

        for(AxtriaSalesIQTM__Country__c vcs : allCountries)
        {
            allCountriesList.add(vcs.AxtriaSalesIQTM__Country_Code__c);
        }

        return allCountriesList;
    }



    public static List<String> getFullLoadTeamInstancesCallPlan()
    {
        List<String> allTeamInstances = new List<String>();
        set<String> countriesList = new set<String>();
        countriesList = getFulloadCountries();

        if(countriesList.size() > 0)
        {
            for(AxtriaSalesIQTM__Team_Instance__c ti : [select id from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c in :countriesList and AxtriaSalesIQTM__Alignment_Period__c = 'Current' WITH SECURITY_ENFORCED]) //and AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Live' and AxtriaSalesIQTM__Team__r.AxtriaSalesIQST__hasCallPlan__c = true])
            {
                allTeamInstances.add(ti.ID);
            }
        }

        return allTeamInstances;
    }
    
    public static set<String> getDeltaloadCountries()
    {
        List<AxtriaSalesIQTM__Country__c> allCountries = [select id from AxtriaSalesIQTM__Country__c where Load_Type__c = 'Delta' WITH SECURITY_ENFORCED];

        set<String> allCountriesList = new set<String>();

        for(AxtriaSalesIQTM__Country__c vcs : allCountries)
        {
            allCountriesList.add(vcs.Id);
        }

        return allCountriesList;
    }
    
    public static set<String> getDeltaLoadCountryCode()
    {
        List<AxtriaSalesIQTM__Country__c> allCountries = [select id, AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c where Load_Type__c = 'Delta' WITH SECURITY_ENFORCED];

        set<String> allCountriesList = new set<String>();

        for(AxtriaSalesIQTM__Country__c vcs : allCountries)
        {
            allCountriesList.add(vcs.AxtriaSalesIQTM__Country_Code__c);
        }

        return allCountriesList;
    }



    public static List<String> getDeltaLoadTeamInstancesCallPlan()
    {
        List<String> allTeamInstances = new List<String>();
        set<String> countriesList = new set<String>();
        countriesList = getDeltaloadCountries();

        if(countriesList.size() > 0)
        {
            for(AxtriaSalesIQTM__Team_Instance__c ti : [select id from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c in :countriesList and AxtriaSalesIQTM__Alignment_Period__c = 'Current' and AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Live' and AxtriaSalesIQTM__Team__r.hasCallPlan__c = true WITH SECURITY_ENFORCED])
            {
                allTeamInstances.add(ti.ID);
            }
        }

        return allTeamInstances;
    }
    
    public static string getJobType(String jobName){

        string jobType = [SELECT Cycle__c FROM Scheduler_Log__c WHERE Job_Name__c = :jobName WITH SECURITY_ENFORCED Limit 1].Cycle__c;
        return jobType;
    }
    
    public static dateTime getSyncTime(String jobName){

        dateTime syncTime = [SELECT Created_Date2__c FROM Scheduler_Log__c WHERE Job_Name__c = :jobName WITH SECURITY_ENFORCED Limit 1].Created_Date2__c;
        return syncTime;
    }
    
    public static void updateSyncTime(String jobName, dateTime syncTime){
        list<Scheduler_Log__c> listsced = new list<Scheduler_Log__c>();
        for(Scheduler_Log__c sl : [SELECT Created_Date2__c, Id FROM Scheduler_Log__c WHERE Job_Name__c = :jobName WITH SECURITY_ENFORCED Limit 1]){
            sl.Created_Date2__c = syncTime;
            listsced.add(sl);
        }
        update listsced;
    }

}