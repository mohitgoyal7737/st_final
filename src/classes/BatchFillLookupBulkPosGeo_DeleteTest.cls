@isTest
public class BatchFillLookupBulkPosGeo_DeleteTest {
    
    static testMethod void testMethod1() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Name ='USA';
        insert countr;
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc.AxtriaSalesIQTM__Country__c = countr.id;
        acc.AccountNumber = 'BH10461999';
        acc.AxtriaSalesIQTM__AccountType__c = 'HCP';
        acc.Status__c = 'Active';
        acc.Type = 'HCP';
        insert acc;

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        insert scen;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Name = 'TESTTEAMINS';
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        //teamins.AxtriaSalesIQTM__Team_Cycle_Name__c = 'Current';
        //teamins.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        insert teamins;
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        
        insert pos;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        posAccount.AxtriaSalesIQTM__Account__c = acc.id;
        posAccount.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        posAccount.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        insert posAccount;
        
        AxtriaSalesIQTM__Employee__c emp = new AxtriaSalesIQTM__Employee__c();
        emp.Name = 'emp123';
        emp.AxtriaSalesIQTM__Country_Name__c  = countr.id;
        emp.Current_Position__c = pos.id;
        emp.AxtriaSalesIQTM__FirstName__c = 'Raju';
        emp.AxtriaSalesIQTM__Last_Name__c = 'uhj';
        insert emp;
        
        AxtriaSalesIQTM__Geography__c geo = new AxtriaSalesIQTM__Geography__c();
        geo.name = 'TESTGEO';
        insert geo;
        
        AxtriaSalesIQTM__Position_Team_Instance__c posteamins = new AxtriaSalesIQTM__Position_Team_Instance__c();
        posteamins.AxtriaSalesIQTM__Employee_ID__c = emp.id;
        posteamins.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.id;
        posteamins.AxtriaSalesIQTM__Position_ID__c = pos.id;
        posteamins.AxtriaSalesIQTM__Effective_Start_Date__c = system.today();
        posteamins.AxtriaSalesIQTM__Effective_End_Date__c = system.today() + 1;
        insert posteamins;
        AxtriaSalesIQTM__Position_Geography__c posgeo = new     AxtriaSalesIQTM__Position_Geography__c();
        posgeo.Name = 'abc';
        posgeo.AxtriaSalesIQTM__Effective_End_Date__c = system.today() +1;
        posgeo.AxtriaSalesIQTM__Effective_Start_Date__c = system.today();
        posgeo.AxtriaSalesIQTM__Geography__c = geo.id;
        posgeo.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        posgeo.AxtriaSalesIQTM__Position__c = pos.Id;
        insert posgeo;
        temp_Obj__c zt = new temp_Obj__c();
        zt.Status__c ='New';
        zt.Country__c = 'USA';
        zt.Event__c = 'Delete';
        zt.Team_Instance__c = teamins.id;
        zt.AccountNumber__c = acc.AccountNumber;
        zt.Account_Type__c = acc.AxtriaSalesIQTM__AccountType__c ;
        zt.Team_Instance_Name__c = teamins.Name;
        zt.Position_Code__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        zt.Object__c ='Zip_Terr';
        zt.Territory_ID__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        zt.Zip_Name__c = geo.Name;
        zt.Account_Type__c = acc.AxtriaSalesIQTM__AccountType__c ;
        zt.Team_Name__c = team.Name;
        insert zt;
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            BatchFillLookupPosGeo_Del obj=new BatchFillLookupPosGeo_Del('USA');
            //BatchFillLookupBulkPosGeo_Delete.query = 'Select Id,Country__c,Event__c,Team_Instance_Name__c,Team_Name__c,Status__c,Territory_ID__c,Zip_Name__c from temp_Obj__c';
            Database.executeBatch(obj);
            
        }
        Test.stopTest();
    }
    
}