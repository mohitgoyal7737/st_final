public with sharing class RuleStepCtlr {
    public Step__c stepObj{get;set;}
    public Grid_Master__c gridMap {get;set;}
    public list<SelectOption> prametersList{get;set;}
    public boolean showFields {get;set;}

    public void showGridFields(){
        showFields = true;
        gridMap = [SELECT Id, Name, Dimension_1_Name__c, Dimension_2_Name__c, Grid_Type__c FROM Grid_Master__c WHERE Id =:stepObj.Matrix__c];
        prametersList = new list<SelectOption>();
        prametersList.add(new SelectOption('None', '--None--'));
        for(Rule_Parameter__c param : [SELECT Id, Name FROM Rule_Parameter__c]){
            prametersList.add(new SelectOption(param.Id, param.Name));
        }
    }
}