public class RedirectChange {
     
    public String gProfileName                                            {get;set;}
    public List<AxtriaSalesIQTM__User_Access_Permission__c> loggedInUserData  {get;set;}
    public Boolean buttonLock                                                 {get;set;}
    public String selectedApp                     {get;set;}
    public string appId;
    
         
       // allTeamInstance      = new List<SelectOption>();
        //allreports           = new List<SelectOption>();
        
        //loggedInUserData = SalesIQUtility.getUserAccessPermistion(Userinfo.getUserId());
        
   
   
    Public pagereference redirectToChange(){
    
    loggedInUserData     = new List<AxtriaSalesIQTM__User_Access_Permission__c>();
    gProfileName = [Select Id,Name from Profile where Id=:UserInfo.getProfileId()].Name;
    PageReference retURL;
    
   if(!Test.isRunningTest()){
        String tabId = ApexPages.currentPage().getParameters().get('ltn_app_id');
        system.debug('===tabId===='+tabId);
        String appId = tabId.substring(12);
        system.debug('===appId===='+appId);
        for(AppMenuItem  appMenu : [SELECT ApplicationId,Name, CreatedDate, CreatedBy.Name FROM AppMenuItem]){
        if(((string)appMenu.ApplicationId).contains(appId)){
          selectedApp=appMenu.Name;
          break;
        }
    }
    }
    else {
    selectedApp='Alignment SalesIQ';
    }
    
    
    if(selectedApp == 'Alignment_SalesIQ'){
        If(gProfileName =='SalesIQ AD' || gProfileName =='SalesIQ Home Office' || gProfileName =='System Administrator'){
              
              retURL = new PageReference('/one/one.app#/sObject/AxtriaSalesIQTM__Change_Request__c/list?filterName=Request_To_Approve');
             //retURL = new PageReference('/a0V');
         
            }
             
          else If(gProfileName =='SalesIQ RD'  || gProfileName =='SalesIQ VP MA' ){
                
                retURL = new PageReference('/one/one.app#/sObject/AxtriaSalesIQTM__Change_Request__c/list?filterName=My_Pending_Request');
                
          }
          else If(gProfileName =='SalesIQ VP'){
                
                retURL = new PageReference('/one/one.app#/sObject/AxtriaSalesIQTM__Change_Request__c/list?filterName=All_Request');
                
          }
      }else if(selectedApp == 'Target_Feedback_SalesIQ'){
        If(gProfileName =='SalesIQ RD' || gProfileName =='SalesIQ Home Office' || gProfileName =='System Administrator'){
            retURL = new PageReference('/one/one.app#/sObject/AxtriaSalesIQTM__Change_Request__c/list?filterName=Target_Feedback_CRs');
        //retURL = new PageReference('/a0V');
        
        }
        else If(gProfileName =='SalesIQ VP' || gProfileName =='SalesIQ AD'){
            retURL = new PageReference('/one/one.app#/sObject/AxtriaSalesIQTM__Change_Request__c/list?filterName=Target_Feedback_CRs');
        
        }else if(gProfileName =='SalesIQ TS'){
            retURL = new PageReference('/one/one.app#/sObject/AxtriaSalesIQTM__Change_Request__c/list?filterName=Target_Feedback_CRs');
        }
      }else if(selectedApp == 'SalesIQ_Roster'){
            retURL = new PageReference('/one/one.app#/sObject/AxtriaSalesIQTM__Change_Request__c/list?filterName=Emp_Assign_CR');
        
      }else{
          retURL = new PageReference('/one/one.app#/sObject/AxtriaSalesIQTM__Change_Request__c/list?filterName=AxtriaSalesIQTM__ZIP_Movement');
      }
      retURL.setRedirect(true);
            return retURL;  
    }
}