@isTest
private class Test_Update_Veeva_Push{
    @isTest static void Update_Veeva_Push_Test() {
        
      insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
      new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'UpdateVeevaPush')};
      

      
      AxtriaSalesIQTM__Organization_Master__c OrgMaster = New AxtriaSalesIQTM__Organization_Master__c();
      OrgMaster.Name = 'ABCTest';
      OrgMaster.AxtriaSalesIQTM__Org_Level__c = 'Global';
      OrgMaster.AxtriaSalesIQTM__Parent_Country_Level__c = True;
      Insert OrgMaster;
      
      AxtriaSalesIQTM__Country__c CT = new AxtriaSalesIQTM__Country__c ();
      CT.Name = 'ABC';
      CT.AxtriaSalesIQTM__Status__c = 'Active';
      CT.AxtriaSalesIQTM__Parent_Organization__c = OrgMaster.id;
      insert CT;
      ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
      String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
      List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
      System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
      CT.Load_Type__c = 'Full Load';
      update CT;

      CT.Load_Type__c = 'No Load';
      update CT;
      
        Veeva_Job_Scheduling__c VPS = New Veeva_Job_Scheduling__c();
            VPS.Name = 'ABC';
            VPS.Load_Type__c = 'Full Load'; 
            VPS.Country__c = CT.id;           
            Insert VPS;
            
            Veeva_Job_Scheduling__c VPS1 = New Veeva_Job_Scheduling__c();
            VPS1.Name = 'ABCD';
            VPS1.Load_Type__c = 'No Load'; 
            VPS1.Country__c = CT.id;           
            Insert VPS1;
            
        }
    }