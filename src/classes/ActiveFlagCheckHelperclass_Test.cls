/*
@author - Prince Richard Augustin (A2661)
@description - Test class to test ActiveFlagCheckHelperclass Controller.
*/

@isTest
private class ActiveFlagCheckHelperclass_Test { 
    static testMethod void testMethod1() {
        User loggedInUser = new User(id=UserInfo.getUserId());  
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        Measure_Master__c mmc1 = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc1.Measure_Type__c = 'HCO';
        mmc1.Brand__c = mmc.Brand__c;
        insert mmc1;
        Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
        pp.isActive__c = FALSE;
        insert pp;
        Parameter__c pp1 = TestDataFactory.parameter(pcc, team, teamins);
        pp1.isActive__c = TRUE;        
        insert pp1;
        update pp1;
        Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
        rp.Parameter__c = pp.id;
        insert rp;
        Rule_Parameter__c rp1= TestDataFactory.ruleParameterWithoutSteps(mmc, pp1);
        rp1.Parameter__c = pp1.id;
        insert rp1;
        list<Parameter__c>triggernew = new list<Parameter__c>();
        triggernew.add(pp);
        map<id,Parameter__c>triggeroldmap =new map<id,Parameter__c>();
        triggeroldmap.put(pp.id,pp1);
        Test.startTest();
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';

        System.runAs(loggedInUser){
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            ActiveFlagCheckHelperclass c = new ActiveFlagCheckHelperclass();
            ActiveFlagCheckHelperclass.check(triggernew,triggeroldmap);
        }
        Test.stopTest();
    }
}