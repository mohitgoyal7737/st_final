public with sharing class updateChannelPreferenceinPACP {
    Map<String,Team_Instance_Config__c> map_custtype_ti_config = new Map<String,Team_Instance_Config__c>();
    Map<String,Product_Priority__c> mapAccount_ChannelPreference = new Map<String,Product_Priority__c>();
    Map<String,Product_Priority__c> mapAccountProduct_ChannelPreference = new Map<String,Product_Priority__c>();
    Map<ID,Account> accountMap = new Map<ID,Account>();
    List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacpexisting = new List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
    Set<String> setAccounts = new Set<String>();
    integer count1;
    boolean insertcheck = false;
    String namespace;

    public void updateChannelPreferenceinPACP(List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacpList, Boolean calledFromTrigger){
        ErrorMessageCallCapacity__c customcount = new ErrorMessageCallCapacity__c();
        ErrorMessageCallCapacity__c customcount1 = new ErrorMessageCallCapacity__c();
        customcount = ErrorMessageCallCapacity__c.getValues('Sequence');
        integer counter = 0;
        if(customcount == null)
        {
            pacpexisting = [Select id, Sequence__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where Sequence__c!=null ORDER BY Sequence__c DESC LIMIT 1];
            
        }
        else
        {
            count1 = Integer.valueOf(customcount.ErrorMessage__c);
        }   
        
            for(integer i = 0;i<pacpList.size();i++)
            {
                if(pacpList[i].Sequence__c == null)
                {
                    counter++;
                    if(customcount!= null)
                    {
                        pacpList[i].Sequence__c = count1 + counter;
                        customcount.ErrorMessage__c = String.valueOf(pacpList[i].Sequence__c);
                    }
                    else
                    {
                        if(pacpexisting.size()>0)
                        {
                            if(pacpexisting[0].Sequence__c != null)
                            pacpList[i].Sequence__c = pacpexisting[0].Sequence__c + counter;
                            customcount1 = new ErrorMessageCallCapacity__c();
                            customcount1.Name = 'Sequence';
                            customcount1.ErrorMessage__c = String.valueOf(pacpList[i].Sequence__c);
			    insertcheck = true;
                        }
                        else
                        {
                            pacpList[i].Sequence__c = counter;
                            customcount1 = new ErrorMessageCallCapacity__c();
                            customcount1.Name = 'Sequence';
                            customcount1.ErrorMessage__c = String.valueOf(pacpList[i].Sequence__c);
			    insertcheck = true;
                        }
                    }
                }
            } 
            if(customcount != null)
            {
                update customcount;
            }
            else
            {
		 if(insertcheck == true)
                {
                insert customcount1;
		}
            }
                
        String team_instance_id = '',acctype = '',concat ='',filter_criteria='';
        AxtriaSalesIQTM__Team_Instance__c team_instance = new AxtriaSalesIQTM__Team_Instance__c();
        Boolean proceed_flag = false,isProductIndependent = true;
        List<String> channels = new List<String>();
        List<Team_Instance_Config__c> list_ti_config = new List<Team_Instance_Config__c>();
        Product_Priority__c channelPreference = new Product_Priority__c();
        Set<String> filter_criteria_set =  new Set<String>();
        List<String> filter_criteria_list =  new List<String>();
        namespace = ST_Utility.fetchNamespace('updateChannelPreferenceinPACP');
        if(pacpList.size() > 0){
            team_instance_id = pacpList[0].AxtriaSalesIQTM__Team_Instance__c;
            if(String.isNotBlank(team_instance_id)){
                team_instance = [Select ID, Name, MultiChannel__c,Enable_Channel_Preference__c from AxtriaSalesIQTM__Team_Instance__c where id = :team_instance_id WITH SECURITY_ENFORCED limit 1 ];
                if(team_instance.MultiChannel__c && team_instance.Enable_Channel_Preference__c){
                    list_ti_config = [Select Cutomer_Type__c,IsChannelPreferenceProductIndependent__c,Channel_Preferene_Filter_Criteria__c from Team_Instance_Config__c where Team_Instance__c =:team_instance_id];
                    if(list_ti_config.size()>0){
                        proceed_flag = true;
                    }
                }
            }
        }
        if(proceed_flag){
            List<Veeva_Channels__mdt> channelList = [Select id,Channels__c,Metric_Mapping__c,Team_Instance_Name__c from Veeva_Channels__mdt where Team_Instance_Name__c=:team_instance.Name and Channels__c !=null  WITH SECURITY_ENFORCED LIMIT 1];
            Set<String> tempSet = new Set<String>();
            if(channelList!=null && channelList.size()>0){
                if(String.isNotBlank(channelList[0].channels__c)){
                    channels = channelList[0].channels__c.split(',');
                    for(String ch :channels){
                        tempSet.add(ch.trim());
                    }
                    channels.clear();
                    channels.addAll(tempSet);   
                }

            }
            else{
                proceed_flag = false;
            }
        }
        if(proceed_flag){
            fetchAccounts(pacpList);
            createChannelPreferenceMap(list_ti_config);
            createAccountMap();
            for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp: pacpList){
                channelPreference = new Product_Priority__c();
                filter_criteria = '';
                concat ='';
                filter_criteria_set = new Set<String>();
                filter_criteria_list =  new List<String>();
                acctype = accountMap.get(pacp.AxtriaSalesIQTM__Account__c).Type;
                if(String.isBlank(acctype)){
                    acctype = 'All';
                }
                if(!map_custtype_ti_config.containsKey(acctype) && map_custtype_ti_config.containsKey('All'))
                {
                    acctype = 'All';
                }
                if(map_custtype_ti_config.containsKey(acctype)){
                    isProductIndependent = map_custtype_ti_config.get(acctype).IsChannelPreferenceProductIndependent__c;
                    filter_criteria = map_custtype_ti_config.get(acctype).Channel_Preferene_Filter_Criteria__c;
                    if(String.isNotBlank(filter_criteria)){
                        filter_criteria_list = filter_criteria.split(',');
                        for(String filter :filter_criteria_list){
                            filter_criteria_set.add(filter.trim());
                        }
                        
                    }
                    if(isProductIndependent){
                        if(mapAccount_ChannelPreference.containsKey(pacp.AxtriaSalesIQTM__Account__c)){
                            channelPreference = mapAccount_ChannelPreference.get(pacp.AxtriaSalesIQTM__Account__c);
                        }
                    }
                    else{
                        if(mapAccountProduct_ChannelPreference.containsKey(pacp.AxtriaSalesIQTM__Account__c+pacp.P1__c)){
                            channelPreference = mapAccountProduct_ChannelPreference.get(pacp.AxtriaSalesIQTM__Account__c+pacp.P1__c);
                        }
                    }
                }
                
                
                for(Integer i = 0,j=channels.size(); i< j;i++){
                    SnTDMLSecurityUtil.printDebugMessage('--channel-- '+channels[i]);
                    SnTDMLSecurityUtil.printDebugMessage('--channel pref-- '+channelPreference);
                    try{

                        if(channels[i]== channelPreference.Channel_1_Name__c){
                            pacp.put(namespace+'Channel_'+(i+1)+'_Preference__c',channelPreference.Channel_1_Preference__c);
                            if(filter_criteria_set.size()==0 || filter_criteria_set.contains(channelPreference.Channel_1_Preference__c)){
                                concat+=channels[i]+'-';
                            }
                        }
                        else if(channels[i]== channelPreference.Channel_2_Name__c){
                            pacp.put(namespace+'Channel_'+(i+1)+'_Preference__c',channelPreference.Channel_2_Preference__c);
                            if(filter_criteria_set.size()==0 || filter_criteria_set.contains(channelPreference.Channel_2_Preference__c)){
                                concat+=channels[i]+'-';
                            }
                        }
                        else if(channels[i]== channelPreference.Channel_3_Name__c){
                            pacp.put(namespace+'Channel_'+(i+1)+'_Preference__c',channelPreference.Channel_3_Preference__c);
                            if(filter_criteria_set.size()==0 || filter_criteria_set.contains(channelPreference.Channel_3_Preference__c)){
                                concat+=channels[i]+'-';
                            }
                        }
                        else if(channels[i]== channelPreference.Channel_4_Name__c){
                            pacp.put(namespace+'Channel_'+(i+1)+'_Preference__c',channelPreference.Channel_4_Preference__c);
                            if(filter_criteria_set.size()==0 || filter_criteria_set.contains(channelPreference.Channel_4_Preference__c)){
                                concat+=channels[i]+'-';
                            }
                        }
                        else if(channels[i]== channelPreference.Channel_5_Name__c){
                            pacp.put(namespace+'Channel_'+(i+1)+'_Preference__c',channelPreference.Channel_5_Preference__c);
                            if(filter_criteria_set.size()==0 || filter_criteria_set.contains(channelPreference.Channel_5_Preference__c)){
                                concat+=channels[i]+'-';
                            }
                        }
                        else{
                            pacp.put(namespace+'Channel_'+(i+1)+'_Preference__c',null);
                        }
                        
    
                    }
                    catch(Exception e){
                        SnTDMLSecurityUtil.printDebugMessage('---exception --'+e.getCause() +'--'+e.getLineNumber()+'--'+e.getMessage()+'--'+e.getStackTraceString());
                    }
                    
                }

                concat = concat.removeEnd('-');
                try{
                    pacp.put(namespace+'Channel_Preference__c',concat); 
                }
                catch(Exception e){
                    SnTDMLSecurityUtil.printDebugMessage('---exception --'+e.getCause() +'--'+e.getLineNumber()+'--'+e.getMessage()+'--'+e.getStackTraceString());
                }
                
            
            }
            if(!calledFromTrigger){
                Map<String,Boolean> triggerNameMap = new Map<String,Boolean>();
                triggerNameMap.put('ParentPacp',true);
                triggerNameMap.put('CallPlanSummaryTrigger',true);
                ST_utility.enabledisableTrigger(triggerNameMap);

                update pacpList;

                triggerNameMap.put('ParentPacp',false);
                triggerNameMap.put('CallPlanSummaryTrigger',false);
                ST_utility.enabledisableTrigger(triggerNameMap);
            }
        }
        
        
        
        
        
    }
    public void createChannelPreferenceMap(List<Team_Instance_Config__c> ti_config){
        
        map_custtype_ti_config = new Map<String,Team_Instance_Config__c>();
        
        mapAccountProduct_ChannelPreference = new Map<String,Product_Priority__c>();
        List<Product_Priority__c> channelPreferenceData = new List<Product_Priority__c>();
        String condition ='';
        String query = '';
        for(Team_Instance_Config__c ti : ti_config){
            if(String.isBlank(ti.Cutomer_Type__c)){
                ti.Cutomer_Type__c = 'All';
                
                if(ti.IsChannelPreferenceProductIndependent__c){
                    condition+='(Product_Name__c = null)';
                }
                else{
                    condition+='(Product_Name__c != null)';
                }
            }
            else{
                
                if(ti.IsChannelPreferenceProductIndependent__c){
                    condition+='(Account__r.Type = \''+ti.Cutomer_Type__c+'\' and Product_Name__c = null)';
                }
                else{
                    condition+='(Account__r.Type = \''+ti.Cutomer_Type__c+'\' and Product_Name__c != null)';
                }
            }
            condition+= ' OR ';
            map_custtype_ti_config.put(ti.Cutomer_Type__c,ti);
            

        }
        condition = condition.removeEnd(' OR ');

        query+= 'SELECT Product_Name__c, Type__c,Account__c,Account__r.Type, Account_Number__c, Channel_1_Name__c, Channel_1_Preference__c, Channel_2_Name__c, Channel_2_Preference__c, Channel_3_Name__c, Channel_3_Preference__c, Channel_4_Name__c, Channel_4_Preference__c, Channel_5_Name__c, Channel_5_Preference__c  FROM Product_Priority__c where Type__c = \'Channel Preference\' and Account__c in :setAccounts';
        if(String.isNotBlank(condition))
            query+=' AND ('+condition+')';

        query+=' WITH SECURITY_ENFORCED LIMIT 2000';
        try{
            SnTDMLSecurityUtil.printDebugMessage('--channel Preference query---'+query);
            channelPreferenceData = Database.query(query);
        }
        catch(Exception e){
            /*error*/
            SnTDMLSecurityUtil.printDebugMessage('---exception --'+e.getCause() +'--'+e.getLineNumber()+'--'+e.getMessage()+'--'+e.getStackTraceString());
        }
        mapAccount_ChannelPreference = new Map<String,Product_Priority__c>();
        for(Product_Priority__c pp : channelPreferenceData){
            
            mapAccount_ChannelPreference.put(pp.Account__c,pp);
            mapAccountProduct_ChannelPreference.put(pp.Account__c+pp.Product_Name__c,pp);   
        }

    }  
    public void fetchAccounts(List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacpList){
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : pacpList){
            setAccounts.add(pacp.AxtriaSalesIQTM__Account__c);
        }
    }
    public void createAccountMap(){
        accountMap = new Map<Id,Account>([Select ID, Type from Account where ID in:setAccounts WITH SECURITY_ENFORCED LIMIT 2000]);
    }
}