global with sharing class alignmentDataRefresh{
    public static String id {get ; set;}
    public static  string CountryCode {get ; set;}
    public static  string CountryName {get ; set;}
    public static  string CountryId {get ; set;}


  
    public list<selectOption> teamInstanceList {get;set;}
    public string selectedteaminst {get;set;}

    public list<selectOption> CycleList {get;set;}
    public string selectedcycle {get;set;}

    public list<selectOption> ProductList {get;set;}
    public string selectedProduct {get;set;}
    public  Boolean flag1 {get;set;}
    public  Boolean flagSurvey {get;set;}
  
    //Added by Ayushi
    public DateTime lastRunSurveyLoad {get;set;}
    //Till here.....

    global alignmentDataRefresh(){

      CycleList = new list<selectOption>();
      CycleList.add(new SelectOption('','--None--'));
      selectedcycle = '--None--';
      selectedProduct = '--None--';
      selectedteaminst = '--None--';
      flag1= false;
      flagSurvey = false;

      /*SnTDMLSecurityUtil.printDebugMessage('flag1<>>>>>>>>>>>>>>>>>>>>< '+flag1);
*/
      

        id = userInfo.getUserId();

        CountryCode = [select Country from user where Id =: id].Country;

        CountryName= [Select Name from AxtriaSalesIQTM__Country__c where AxtriaSalesIQTM__Country_Code__c =: CountryCode].Name;

        CountryId = [select id from AxtriaSalesIQTM__Country__c where Name =: CountryName].id;

        //Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c
        /*for(Cycle__c cyc : [Select Name from Cycle__c where  Country__c =: CountryId]){
          CycleList.add(new SelectOption(cyc.Name,cyc.Name));
        }*/
        for(AxtriaSalesIQTM__Workspace__c cyc : [Select Name from AxtriaSalesIQTM__Workspace__c where  AxtriaSalesIQTM__Country__c =: CountryId]){
          CycleList.add(new SelectOption(cyc.Name,cyc.Name));
        }
        SnTDMLSecurityUtil.printDebugMessage(' '+CycleList);

        // Shivansh - A1450 Changes Ends here
      

      //Added by Ayushi

       List<Scheduler_Log__c> schedulerLogRec = [Select LastModifiedDate from Scheduler_Log__c where Job_Type__c = 'Survey Data Load' and Job_Status__c = 'Success' order by CreatedDate desc limit 1];
       if(schedulerLogRec.size() > 0)
          lastRunSurveyLoad = schedulerLogRec[0].LastModifiedDate;

       else
          lastRunSurveyLoad = null;
      //till here...

    }

 
    /*public List<SelectOption> getCycle()
    {

      
    }*/

    public List<SelectOption> getTeamInstance()
    {

      /*SnTDMLSecurityUtil.printDebugMessage('>>>>>>>>>>>>>>>>>inside getTeamInstance <<<<<<<<<<<<<<<<');
      SnTDMLSecurityUtil.printDebugMessage('>>>>>>>>>>>>>>>>>inside selectedcycle <<<<<<<<<<<<<<<<'+selectedcycle);*/

      teamInstanceList = new list<selectOption>();
      teamInstanceList.add(new SelectOption('','--None--'));
    
      //Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c
      /*for(AxtriaSalesIQTM__Team_Instance__c teamIns : [Select Name from AxtriaSalesIQTM__Team_Instance__c where Cycle__r.name =: selectedcycle]){
        teamInstanceList.add(new SelectOption(teamIns.Name,teamIns.Name));
      }*/

      for(AxtriaSalesIQTM__Team_Instance__c teamIns : [Select Name from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.name =: selectedcycle]){
        teamInstanceList.add(new SelectOption(teamIns.Name,teamIns.Name));
      }

      // Shivansh - A1450 Changes Ends here      

       /*SnTDMLSecurityUtil.printDebugMessage('>>>>>>>>>>>>>>>>> teamInstanceList <<<<<<<<<<<<<<<<'+teamInstanceList);

       SnTDMLSecurityUtil.printDebugMessage('??????????? flag1 ???????????????/ '+flag1);
      flag1 = true; */

       return teamInstanceList;
    }

    public List<SelectOption> getProduct()
    {

      /*SnTDMLSecurityUtil.printDebugMessage('>>>>>>>>>>>>>>>>>inside getProduct <<<<<<<<<<<<<<<<');
      SnTDMLSecurityUtil.printDebugMessage('>>>>>>>>>>>>>>>>>inside selectedteaminst <<<<<<<<<<<<<<<<'+selectedteaminst);*/
      productList = new list<selectOption>();
      productList.add(new SelectOption('','--None--'));
      
    
      for(Product_Catalog__c prod : [Select Name from Product_Catalog__c where Team_Instance__r.name =: selectedteaminst and IsActive__c = true ]){
        productList.add(new SelectOption(prod.Name,prod.Name));
      }
       /*SnTDMLSecurityUtil.printDebugMessage('>>>>>>>>>>>>>>>>> productList <<<<<<<<<<<<<<<<'+productList);*/
       return productList;
    }



    public void SaveButton()
    {

      SnTDMLSecurityUtil.printDebugMessage('Unfortunately tuchi class called');
      String status='';
      String dataRefresh='';

      String user='';

      user = userInfo.getUserId();
      /*SnTDMLSecurityUtil.printDebugMessage('??????????? flag1 ???????????????/ '+flag1);
      
      SnTDMLSecurityUtil.printDebugMessage('>>>>>>>>>>>>>>>>>selectedteaminst <<<<<<<<<<<<<<<<'+selectedteaminst);
      SnTDMLSecurityUtil.printDebugMessage('>>>>>>>>>>>>>>>>>selectedProduct <<<<<<<<<<<<<<<<'+selectedProduct); */ 

      List<Scheduler_Log__c> schedulerLogRec = [Select id,Job_Status__c from Scheduler_Log__c where Job_Type__c = 'Survey Data Load' order by CreatedDate desc LIMIT 1];

      List<Scheduler_Log__c> dataRefreshRec = [Select id,Job_Status__c from Scheduler_Log__c where Job_Type__c = 'Alignment Data Refresh' and Object_Name__c =:user order by CreatedDate desc LIMIT 1];

      SnTDMLSecurityUtil.printDebugMessage('schedulerLogRec SaveButton::::::' +schedulerLogRec);

      if(schedulerLogRec.size() > 0)
      {

        status = schedulerLogRec[0].Job_Status__c;
      }
      else
      {
         status = 'Success';
      }


      if(dataRefreshRec.size() > 0)
      {

        dataRefresh = dataRefreshRec[0].Job_Status__c;
      }
      else
      {
         dataRefresh = 'Success';
      } 

        SnTDMLSecurityUtil.printDebugMessage('status SaveButton::::::' +status);
        if(status == 'Success' && dataRefresh == 'Success')
        {

            SnTDMLSecurityUtil.printDebugMessage('status is Success in SaveButton');
            if((selectedteaminst!= null) && (selectedProduct!=null))
            {
              flag1 = true;
              String teaminstance_prod;

              String productId= [Select Veeva_External_ID__c from Product_Catalog__c where Name =:selectedProduct and Team_Instance__r.Name =:selectedteaminst and IsActive__c=true limit 1].Veeva_External_ID__c;
              teaminstance_prod = selectedteaminst+';'+productId;

              
               Database.executeBatch(new Delete_Staging_Cust_Survey_Profiling(teaminstance_prod),500);
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm,'The Refresh has started  for Selected Team Instance and Product.'));

               Scheduler_Log__c logRec = new Scheduler_Log__c();
               logRec.Job_Type__c = 'Alignment Data Refresh';
               logRec.Job_Status__c = 'Failed';
               logRec.Job_Name__c = 'Alignment Data Refresh';
               logrec.Object_Name__c = user;
               logRec.Created_Date__c= System.today();
               //insert logrec;
               SnTDMLSecurityUtil.insertRecords(logrec, 'alignmentDataRefresh');



            }
            else 
            {
                    flag1 = false; 
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please Select Team Instance and Product'));
            }

        }

         else
         {

            if(status != 'Success' && dataRefresh == 'Success')
            {
                SnTDMLSecurityUtil.printDebugMessage('status is Failed in SaveButton');
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Survery Data Load is in Progress. Please wait for sometime.'));
            }

            else if(status == 'Success' && dataRefresh != 'Success')
            {
                SnTDMLSecurityUtil.printDebugMessage('status is Failed for Data Refresh');
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Alignment Data Refresh is in Progress. Please wait for sometime.'));
            }
         }
    }


    public void deleteSurveyData()
    {

        Date lastjobrun = System.today().addDays(-2);

        List<Scheduler_Log__c> schedulerLogRec = [Select id,Job_Status__c from Scheduler_Log__c where Job_Type__c = 'Survey Data Load' order by CreatedDate desc LIMIT 1];

        SnTDMLSecurityUtil.printDebugMessage('schedulerLogRec deleteSurveyData::::::' +schedulerLogRec);

        //List<Scheduler_Log__c> dataRefreshRec = [Select id,Job_Status__c from Scheduler_Log__c where Job_Type__c = 'Alignment Data Refresh' and CreatedDate >= :lastjobrun];
        String dataRefresh='Success';
        String status = '';

        for(Scheduler_Log__c sc : [Select id,Job_Status__c from Scheduler_Log__c where Job_Type__c = 'Alignment Data Refresh' and CreatedDate >= :lastjobrun])
        {
          if(sc.Job_Status__c=='Failed')
          {
            dataRefresh = 'Failed';
            break;
          }

        }

        if(schedulerLogRec.size() > 0)
        {

          status = schedulerLogRec[0].Job_Status__c;
        }
        else
        {
           status = 'Success';
        }

        /*if(schedulerLogRec.size() > 0)
        {
            String status = schedulerLogRec[0].Job_Status__c;*/
            if(status == 'Success' && dataRefresh=='Success')
            {
              SnTDMLSecurityUtil.printDebugMessage('Status deleteSurveyData::::::' +status);
              Database.executeBatch(new BatchDeleteSurveyData(),2000);

              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm,'Survey Data Load has been started.'));

              Scheduler_Log__c logRec = new Scheduler_Log__c();
              logRec.Job_Type__c = 'Survey Data Load';
              logRec.Job_Status__c = 'Failed';
              logRec.Job_Name__c = 'Survey Data Load';
              logRec.Created_Date__c= System.today();
              //insert logrec;
              SnTDMLSecurityUtil.insertRecords(logrec, 'alignmentDataRefresh');
            }

            else
            {
              if(status != 'Success' && dataRefresh=='Success')
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Survery Data Load is already in Progress !!'));

              else if(status == 'Success' && dataRefresh !='Success')
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Alignment Data Refresh is in Progress !!'));
            }

        // }
        // else
        // {
        //     Database.executeBatch(new BatchDeleteSurveyData(),2000);

        //     Scheduler_Log__c logRec1 = new Scheduler_Log__c();
        //     logRec1.Job_Type__c = 'Survey Data Load';
        //     logRec1.Job_Status__c = 'Failed';
        //     logRec1.Job_Name__c = 'Survey Data Load';
        //     logRec1.Created_Date__c= System.today();
        //     insert logrec1;

        // }

    }

}