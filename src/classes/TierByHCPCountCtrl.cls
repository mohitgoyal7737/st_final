public with sharing class TierByHCPCountCtrl {

   String buName;
   String lineName;
   public List<ShowSegmentData> allAdoptionData {get;set;}
   public String allData {get;set;}
   public String userLocale {get;set;}
   public String brandName;
   public String fileDelimiter {get;set;}

   public TierByHCPCountCtrl() 
   {
      
      userLocale = UserInfo.getLocale();
      system.debug('@@@@@@@@@qwqw' + userLocale);
      List<Country__c> countryInfo = [select Delimiter__c from Country__c where name =: userLocale];
      
      if(countryInfo.size()>0)
      {
          fileDelimiter = countryInfo[0].Delimiter__c;
      }
      else
      {
          fileDelimiter = ',';
      }

      allAdoptionData = new List<ShowSegmentData>();

      buName = ApexPages.currentPage().getParameters().get('buSelectedName'); 
     // lineName = ApexPages.currentPage().getParameters().get('lineSelectedName');
      brandName = ApexPages.currentPage().getParameters().get('selectedBrandName');
      getAllPosAdoptionMap();
   }
   
   public Integer sumTotalTCF = 0;
   public Integer totalHCP = 0;
   public decimal totalfreq = 0;
   public decimal percHCP = 0;
   

   public void getAllPosAdoptionMap()
   {
        List<AxtriaSalesIQTM__User_Access_Permission__c> allUserAccessPerm = [select AxtriaSalesIQTM__Position__c from AxtriaSalesIQTM__User_Access_Permission__c where AxtriaSalesIQTM__User__c = :UserInfo.getUserId() ]; //and AxtriaSalesIQTM__Position__r.Line__r.Name = :lineName
        String selectedPosition;
        
        map<string,integer> mapTierToTotalTcf=new map<string,integer>();
        map<string,integer>mapTierToTotalseg=new map<string,integer>();
        map<string,integer>mapTierToTotalHCO=new map<string,integer>();

        if(allUserAccessPerm.size() > 0 )
        {
            selectedPosition = allUserAccessPerm[0].AxtriaSalesIQTM__Position__c;
        }

        system.debug('++++++++++++ Hey Selected Position is '+ selectedPosition);
        system.debug('============SelectedBrandname::'+brandName);
       
       // system.debug('++++++++++++ Hey Selected lineName is '+ lineName);

        map<String, set<String>> tier2ParentAccount    = new map<String, set<String>>();
        map<String, set<String>> tierSeg2ParentAccount = new map<String, set<String>>();
        List<AggregateResult> allRecsSumParent;
        if(brandName == null)
        {
            brandName = 'Temp';
        }
        if((brandName.split(',')).size() > 1)
          allRecsSumParent = [select Segment__c segment, count(id) countRecs, sum(Final_TCF_Approved__c) countAppro, potential__C tier, AxtriaSalesIQTM__Account__r.ParentId hco  from AxtriaSalesIQTM__Position_Account_Call_Plan__c where  (AxtriaSalesIQTM__Position__c = :selectedPosition or AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__c = :selectedPosition or AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__c =:selectedPosition or AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__c =:selectedPosition) and  AxtriaSalesIQTM__Position__c != '' and AxtriaSalesIQTM__lastApprovedTarget__c = true group by Segment__c,potential__c, AxtriaSalesIQTM__Account__r.ParentId order by potential__c]; //Line__r.Name = :lineName
        else
          allRecsSumParent = [select Segment__c segment, count(id) countRecs, sum(Final_TCF_Approved__c) countAppro, potential__C tier, AxtriaSalesIQTM__Account__r.ParentId hco  from AxtriaSalesIQTM__Position_Account_Call_Plan__c where  (AxtriaSalesIQTM__Position__c = :selectedPosition or AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__c = :selectedPosition or AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__c =:selectedPosition or AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__c =:selectedPosition) and  AxtriaSalesIQTM__Position__c != '' and AxtriaSalesIQTM__lastApprovedTarget__c = true and P1__c = :brandName group by Segment__c,potential__c, AxtriaSalesIQTM__Account__r.ParentId order by potential__c]; //Line__r.Name = :lineName

        for(AggregateResult agg : allRecsSumParent){
            string tier    = string.valueof(agg.get('tier')); //Getting tier of the result
            string segment = string.valueof(agg.get('segment')); //Getting segment of the result
            String key     = tier + '_' + segment; //Create a unique key
            set<String> temp1;
            if(tier2ParentAccount.containsKey(tier)){
                temp1 = tier2ParentAccount.get(tier);
            }else{
                temp1 = new set<String>();
            }
            temp1.add(string.valueof(agg.get('hco')));
            tier2ParentAccount.put(tier, temp1);
            
            set<String> temp2;
            if(tierSeg2ParentAccount.containsKey(key)){
                temp2 = tierSeg2ParentAccount.get(key);
            }else{
                temp2 = new set<String>();
            }
            temp2.add(string.valueof(agg.get('hco')));
            tierSeg2ParentAccount.put(key, temp2);
        }


        List<AggregateResult> allRecsSum;

        if((brandName.split(',')).size() > 1)
          allRecsSum = [select Segment__c name, count(id) countRecs, sum(Final_TCF_Approved__c) countAppro, potential__C tier,count(AxtriaSalesIQTM__Account__r.ParentId) hco  from AxtriaSalesIQTM__Position_Account_Call_Plan__c where  (AxtriaSalesIQTM__Position__c = :selectedPosition or AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__c = :selectedPosition or AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__c =:selectedPosition or AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__c =:selectedPosition) and  AxtriaSalesIQTM__Position__c != '' and AxtriaSalesIQTM__lastApprovedTarget__c = true group by Segment__c,potential__C order by potential__C ];  //Line__r.Name = :lineName
        else
          allRecsSum = [select Segment__c name, count(id) countRecs, sum(Final_TCF_Approved__c) countAppro, potential__C tier,count(AxtriaSalesIQTM__Account__r.ParentId) hco  from AxtriaSalesIQTM__Position_Account_Call_Plan__c where  (AxtriaSalesIQTM__Position__c = :selectedPosition or AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__c = :selectedPosition or AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__c =:selectedPosition or AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__c =:selectedPosition) and  AxtriaSalesIQTM__Position__c != '' and AxtriaSalesIQTM__lastApprovedTarget__c = true and P1__c = :brandName group by Segment__c,potential__C order by potential__C ]; //Line__r.Name = :lineName
        for(AggregateResult agg : allRecsSum)
        {
            string key=string.valueof(agg.get('tier'));
            if(mapTierToTotalTcf.containsKey(key)){
                
                integer sum=mapTierToTotalTcf.get(key);
                sum=sum+Integer.valueof(agg.get('countAppro'));
                
                integer segSum=mapTierToTotalseg.get(key);
                segSum=segSum+Integer.valueof(agg.get('countRecs'));
                
                integer totalHco=mapTierToTotalHCO.get(key);
                totalHco=totalHco+Integer.valueof(agg.get('hco'));
                
                mapTierToTotalTcf.put(key,sum);
                mapTierToTotalseg.put(key,segSum);
                mapTierToTotalHCO.put(key,totalHco);
                
            }
            else
            {
                
                 integer sum=Integer.valueof(agg.get('countAppro'));
                 mapTierToTotalTcf.put(key,sum);
                 
                 integer segSum=Integer.valueof(agg.get('countRecs'));
                 mapTierToTotalseg.put(key,segSum);
                 
                 integer totalHco=Integer.valueof(agg.get('hco'));
                 mapTierToTotalHCO.put(key,totalHco);
                
            }
            
           
        }

         string oldTierNmae='select';
        
        Integer i=0;
        for(AggregateResult agg : allRecsSum)
        {
              i++;
            String tierName = String.valueof(agg.get('tier'));
            if(oldTierNmae=='select'){
               
               oldTierNmae=tierName;
           }
          
            String segmentName = String.valueof(agg.get('name'));
            Integer segmentCount = Integer.valueof(agg.get('countRecs'));
            Integer sumTCF = Integer.valueof(agg.get('countAppro'));
            Decimal avgTCF = (sumTCF)/segmentCount;
            String key = tierName+'_'+segmentName;
            Integer hcoCount= tierSeg2ParentAccount.get(key).size(); //Integer.valueof(agg.get('hco'));
          
            decimal percTCF =((sumTCF * 100.0)/mapTierToTotalTcf.get(tierName));
            
            
            
            if(oldTierNmae!=tierName || allRecsSum.size()==i){
                 string temp;
               if(oldTierNmae==null)
                 temp=''+ ' Total';
                else
                 temp=oldTierNmae+ ' Total';
                integer avgtCFRound=mapTierToTotalTcf.get(oldTierNmae)/(mapTierToTotalseg.get(oldTierNmae));
              
               if(allRecsSum.size()==i)
               {
                    allAdoptionData.add(new ShowSegmentData(tierName,segmentName, segmentCount, sumTCF, avgTCF, percTCF,hcoCount));
                    allAdoptionData.add(new ShowSegmentData(temp,'',mapTierToTotalseg.get(oldTierNmae), mapTierToTotalTcf.get(oldTierNmae),avgtCFRound,100,tier2ParentAccount.get(oldTierNmae).size()));
                    oldTierNmae=tierName;
                   
               }
               else
               {
                    allAdoptionData.add(new ShowSegmentData(temp,'',mapTierToTotalseg.get(oldTierNmae), mapTierToTotalTcf.get(oldTierNmae),avgtCFRound,100,tier2ParentAccount.get(oldTierNmae).size()));
                     allAdoptionData.add(new ShowSegmentData(tierName,segmentName, segmentCount, sumTCF, avgTCF, percTCF,hcoCount));
                     oldTierNmae=tierName;
               }
                
                
            }
            else{
                
                allAdoptionData.add(new ShowSegmentData(tierName,segmentName, segmentCount, sumTCF, avgTCF, percTCF,hcoCount));
                
                
            }
            
           
            
          

            
        }
       

       
        allData = JSON.serialize(allAdoptionData);
    }

   public class ShowSegmentData
   {
    public String  tierName {get;set;}
    public String  segmentName {get;set;}
    public Integer segmentCount  {get;set;}  
    public Integer HCACount  {get;set;} 
    public Integer sumTCF {get;set;} 
    public Decimal avgTCF {get;set;}
    public Decimal percTCF {get;set;} 
    
    public ShowSegmentData(String tierName,String segmentName,Integer segmentCount ,Integer sumTCF ,Decimal avgTCF ,Decimal percTCF,Integer HCACount)
    {
        this.tierName = tierName;
        this.segmentName = segmentName;
        this.segmentCount = segmentCount;
        this.sumTCF = sumTCF;
        this.avgTCF = avgTCF.setScale(0);
        this.percTCF = percTCF.setScale(0);
        this.HCACount=HCACount;
    }
   }
}