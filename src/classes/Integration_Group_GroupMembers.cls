public with sharing class Integration_Group_GroupMembers {

	List<Group> allGroups;
	List<GroupMember> groupMembers;

    public Integration_Group_GroupMembers() 
    {
 		allGroups = new List<Group>([select id, Name,DeveloperName,DoesIncludeBosses,Type FROM Group]);
 		groupMembers = new List<GroupMember>([select  Group.Name,Id,UserOrGroupiD FROM GroupMember]);

 		createOrUpdateGroups();
 		createOrUpdateGroupMembers();
    }

    public void createOrUpdateGroups()
    {
    	List<Staging_Group__c> stagingGroup = new List<Staging_Group__c>();

    	for(Group gp : allGroups)
    	{
    		Staging_Group__c sg = new Staging_Group__c();
    		sg.Name = gp.Name;
    		sg.Developer_Name__c = gp.DeveloperName;
    		sg.DoesIncludeBosses__c = gp.DoesIncludeBosses;
    		sg.External_ID__c = gp.Name;
 			stagingGroup.add(sg);
    	}

    	upsert stagingGroup External_ID__c;
    }

    public void createOrUpdateGroupMembers()
    {
    	List<Staging_Group_Member__c> stagingGroup = new List<Staging_Group_Member__c>();
    	Map<Id, User> idToUserMap = new Map<Id,User>([select id, FederationIdentifier From User]);

    	for(GroupMember gm : groupMembers)
    	{
    		Staging_Group_Member__c sgm = new Staging_Group_Member__c();
    		sgm.GroupName__c = gm.Group.Name;
    		sgm.UserID__c = idToUserMap.get(gm.UserOrGroupId).FederationIdentifier;

 			stagingGroup.add(sgm);
    	}

    	insert stagingGroup;
    }

}