global with sharing class BatchDeassignPositionAccounts_New implements Database.Batchable<sObject>, Database.Stateful {
    public String query;
   

    global BatchDeassignPositionAccounts_New(Set<String> setDeassignID,Boolean flagg, String country) {
        query = '';
   }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<sObject> scope) {


    }

    global void finish(Database.BatchableContext BC) {

    }
}