/*
@author - Prince Richard Augustin (A2661)
@description - Test class to test the BatchCreateHCOParameterValueMap.
*/

@isTest
private class BatchCreateHCOParameterValueMapTest {
    static testMethod void testMethod1() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;
        Account acc= TestDataFactory.createAccount();
        acc.Type = 'HCO';
        insert acc;
        Account acc1= TestDataFactory.createAccount();
        acc1.Type = 'HCP';
        insert acc1;
        Account acc2= TestDataFactory.createAccount();
        acc2.Type = 'HCO';
        insert acc2;

        AxtriaSalesIQTM__Affiliation_Network__c affnet = TestDataFactory.createAffliNet(countr);
        insert affnet;
        AxtriaSalesIQTM__Account_Affiliation__c accaff = TestDataFactory.createAcctAffli(acc,affnet);
        accaff.BU_Rank__c = 1.00;
        accaff.AxtriaSalesIQTM__Parent_Account__c = acc1.Id;
        accaff.AxtriaSalesIQTM__Account__c = acc.Id;
        insert accaff;
        /*AxtriaSalesIQTM__Account_Affiliation__c accaff1 = TestDataFactory.createAcctAffli(acc2,affnet);
        accaff1.BU_Rank__c = 3.00;
        accaff1.AxtriaSalesIQTM__Parent_Account__c = acc.Id;
        accaff1.AxtriaSalesIQTM__Account__c = acc2.Id;
        insert accaff1;*/
        

        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        Measure_Master__c mmc1 = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc1.Measure_Type__c = 'HCO';
        mmc1.Brand__c = mmc.Brand__c;
        insert mmc1;
        Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
        pp.Name ='MARKET_ CRC';
        insert pp;
        Parameter__c pp1 = TestDataFactory.parameter(pcc, team, teamins);
        pp1.Name ='MARKET_ CRC';
        insert pp1;
        Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
        rp.Parameter__c = pp.id;
        insert rp;
        Rule_Parameter__c rp1= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
        rp1.Parameter__c = pp1.id;
        insert rp1;
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        posAccount.AxtriaSalesIQTM__Account__c = acc.id;
        insert posAccount;
        AxtriaSalesIQTM__Position_Account__c posAccount1 = TestDataFactory.createPositionAccount(acc1,pos,teamins);
        posAccount1.AxtriaSalesIQTM__Account__c = acc1.id;
        insert posAccount1;

        BU_Response__c bu = TestDataFactory.createBuResponse(posAccount,pcc,teamins,team,acc);
        insert bu;
        HCO_Parameters__c hcoParam = TestDataFactory.createHCOParameter(rp,mmc);
        hcoParam.Source_Rule__c = mmc.id;
        hcoParam.Destination_Rule__c = mmc1.id;
        hcoParam.Source_Rule_Parameter__c = rp.Id;
        hcoParam.Affiliation_Level__c = 3;
        
        insert hcoParam;
        
        Account_Compute_Final__c compFinal = TestDataFactory.createComputeFinal(mmc,acc,posAccount,bu,pos);
        for(integer i=1;i<=50;i++){
            compFinal.put('OUTPUT_NAME_'+i+'__c',pp.Name);
        }
        compFinal.Measure_Master__c = mmc.id;
        insert compFinal;
        
        BU_Response__c bu1 = TestDataFactory.createBuResponse(posAccount,pcc,teamins,team,acc1);
        insert bu1;
        
        Account_Compute_Final__c compFinal1 = TestDataFactory.createComputeFinal(mmc,acc1,posAccount1,bu1,pos);
        for(integer i=1;i<=50;i++){
            compFinal1.put('OUTPUT_NAME_'+i+'__c',pp.Name);
        }
        compFinal1.Measure_Master__c = mmc.id;
        insert compFinal1;
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            BatchCreateHCOParameterValueMap obj=new BatchCreateHCOParameterValueMap(mmc.id,mmc1.Id);
            
            String query = 'Select Id,Measure_Master__c,Physician_2__c,Measure_Master__r.Brand_Lookup__r.Product_Code__c,Measure_Master__r.Brand_Lookup__r.Name,Physician__c,Physician__r.AxtriaSalesIQTM__Account__r.AccountNumber, ';
            query += ' Physician__r.AxtriaSalesIQTM__Account__c,Physician__r.Position_Code__c, Measure_Master__r.Team__r.Name, Measure_Master__r.Team_Instance__r.Name,';
            query += ' Output_Name_1__c,Output_Name_2__c,Output_Name_3__c,Output_Name_4__c,Output_Name_5__c,Output_Name_6__c, ';
            query += ' Output_Name_7__c,Output_Name_8__c,Output_Name_9__c,Output_Name_10__c,Output_Name_11__c,Output_Name_12__c, ';
            query += ' Output_Name_13__c,Output_Name_14__c,Output_Name_15__c,Output_Name_16__c,Output_Name_17__c,Output_Name_18__c, ';
            query += ' Output_Name_19__c,Output_Name_20__c,Output_Name_21__c,Output_Name_22__c,Output_Name_23__c,Output_Name_24__c, ';
            query += ' Output_Name_25__c,Output_Name_26__c,Output_Name_27__c,Output_Name_28__c,Output_Name_29__c,Output_Name_30__c, ';
            query += ' Output_Name_31__c,Output_Name_32__c,Output_Name_33__c,Output_Name_34__c,Output_Name_35__c,Output_Name_36__c, ';
            query += ' Output_Name_37__c,Output_Name_38__c,Output_Name_39__c,Output_Name_40__c,Output_Name_41__c,Output_Name_42__c, ';
            query += ' Output_Name_43__c,Output_Name_44__c,Output_Name_45__c,Output_Name_46__c,Output_Name_47__c,Output_Name_48__c, ';
            query += ' Output_Name_49__c,Output_Name_50__c,Output_Value_1__c,Output_Value_2__c,Output_Value_3__c,Output_Value_4__c,Output_Value_5__c,Output_Value_6__c, ';
            query += ' Output_Value_7__c,Output_Value_8__c,Output_Value_9__c,Output_Value_10__c,Output_Value_11__c,Output_Value_12__c, ';
            query += ' Output_Value_13__c,Output_Value_14__c,Output_Value_15__c,Output_Value_16__c,Output_Value_17__c,Output_Value_18__c, ';
            query += ' Output_Value_19__c,Output_Value_20__c,Output_Value_21__c,Output_Value_22__c,Output_Value_23__c,Output_Value_24__c, ';
            query += ' Output_Value_25__c,Output_Value_26__c,Output_Value_27__c,Output_Value_28__c,Output_Value_29__c,Output_Value_30__c, ';
            query += ' Output_Value_31__c,Output_Value_32__c,Output_Value_33__c,Output_Value_34__c,Output_Value_35__c,Output_Value_36__c, ';
            query += ' Output_Value_37__c,Output_Value_38__c,Output_Value_39__c,Output_Value_40__c,Output_Value_41__c,Output_Value_42__c, ';
            query += ' Output_Value_43__c,Output_Value_44__c,Output_Value_45__c,Output_Value_46__c,Output_Value_47__c,Output_Value_48__c, ';
            query += ' Output_Value_49__c,Output_Value_50__c from Account_Compute_Final__c ';
            obj.query = query;
            Database.executeBatch(obj);
        }
        Test.stopTest();
    }
    
    static testMethod void testMethod2() {
        
        User loggedInUser = new User(id=UserInfo.getUserId());
              
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
                
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;
        
        Account acc= TestDataFactory.createAccount();
        acc.Type = 'HCO';
        insert acc;
        
        Account acc1= TestDataFactory.createAccount();
        acc1.Type = 'HCP';
        insert acc1;
        
        Account acc2= TestDataFactory.createAccount();
        acc2.Type = 'HCO';
        insert acc2;

        AxtriaSalesIQTM__Affiliation_Network__c affnet = TestDataFactory.createAffliNet(countr);
        insert affnet;
        
        AxtriaSalesIQTM__Account_Affiliation__c accaff = TestDataFactory.createAcctAffli(acc,affnet);
        accaff.BU_Rank__c = 1.00;
        accaff.AxtriaSalesIQTM__Parent_Account__c = acc1.Id;
        accaff.AxtriaSalesIQTM__Account__c = acc.Id;
        accaff.AxtriaSalesIQTM__Active__c = true;
        insert accaff;
        /*AxtriaSalesIQTM__Account_Affiliation__c accaff1 = TestDataFactory.createAcctAffli(acc2,affnet);
        accaff1.BU_Rank__c = 3.00;
        accaff1.AxtriaSalesIQTM__Parent_Account__c = acc.Id;
        accaff1.AxtriaSalesIQTM__Account__c = acc2.Id;
        insert accaff1;*/
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        
        Measure_Master__c mmc1 = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc1.Measure_Type__c = 'HCO';
        mmc1.Brand__c = mmc.Brand__c;
        insert mmc1;
        
        Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
        pp.Name ='MARKET_ CRC';
        insert pp;
        
        Parameter__c pp1 = TestDataFactory.parameter(pcc, team, teamins);
        pp1.Name ='MARKET_ CRC';
        insert pp1;
        
        Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
        rp.Parameter__c = pp.id;
        insert rp;
        
        Rule_Parameter__c rp1= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
        rp1.Parameter__c = pp1.id;
        insert rp1;
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        posAccount.AxtriaSalesIQTM__Account__c = acc.id;
        insert posAccount;
        
        AxtriaSalesIQTM__Position_Account__c posAccount1 = TestDataFactory.createPositionAccount(acc1,pos,teamins);
        posAccount1.AxtriaSalesIQTM__Account__c = acc1.id;
        insert posAccount1;

        BU_Response__c bu = TestDataFactory.createBuResponse(posAccount,pcc,teamins,team,acc);
        insert bu;
        
        HCO_Parameters__c hcoParam = TestDataFactory.createHCOParameter(rp,mmc);
        hcoParam.Source_Rule__c = mmc.id;
        hcoParam.Destination_Rule__c = mmc1.id;
        hcoParam.Source_Rule_Parameter__c = rp.Id;
        hcoParam.Affiliation_Level__c = 3;
        
        insert hcoParam;
        
        Account_Compute_Final__c compFinal = TestDataFactory.createComputeFinal(mmc,acc,posAccount,bu,pos);
        for(integer i=1;i<=50;i++){
            compFinal.put('OUTPUT_NAME_'+i+'__c',pp.Name);
        }
        for(integer i=1;i<=50;i++){
            compFinal.put('Output_Value_'+i+'__c','0'+i);
        }
        compFinal.Measure_Master__c = mmc.id;
        insert compFinal;
        
        BU_Response__c bu1 = TestDataFactory.createBuResponse(posAccount,pcc,teamins,team,acc1);
        insert bu1;
        
        Account_Compute_Final__c compFinal1 = TestDataFactory.createComputeFinal(mmc,acc1,posAccount1,bu1,pos);
        for(integer i=1;i<=50;i++){
            compFinal1.put('OUTPUT_NAME_'+i+'__c',pp.Name);
        }
        for(integer i=1;i<=50;i++){
            compFinal1.put('Output_Value_'+i+'__c','0'+i);
        }
        compFinal1.Measure_Master__c = mmc.id;
        insert compFinal1;
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            BatchCreateHCOParameterValueMap obj=new BatchCreateHCOParameterValueMap(mmc.id,mmc1.Id,teamins.id,'HCO',true);
            //BatchCreateHCOParameterValueMap.HCPParamWrapperClass bh = new BatchCreateHCOParameterValueMap.HCPParamWrapperClass('test',1,'test');
            
            String query = 'Select Id,Measure_Master__c,Physician_2__c,Measure_Master__r.Brand_Lookup__r.Product_Code__c,Measure_Master__r.Brand_Lookup__r.Name,Physician__c,Physician__r.AxtriaSalesIQTM__Account__r.AccountNumber, ';
            query += ' Physician__r.AxtriaSalesIQTM__Account__c,Physician__r.Position_Code__c, Measure_Master__r.Team__r.Name, Measure_Master__r.Team_Instance__r.Name,';
            query += ' Output_Name_1__c,Output_Name_2__c,Output_Name_3__c,Output_Name_4__c,Output_Name_5__c,Output_Name_6__c, ';
            query += ' Output_Name_7__c,Output_Name_8__c,Output_Name_9__c,Output_Name_10__c,Output_Name_11__c,Output_Name_12__c, ';
            query += ' Output_Name_13__c,Output_Name_14__c,Output_Name_15__c,Output_Name_16__c,Output_Name_17__c,Output_Name_18__c, ';
            query += ' Output_Name_19__c,Output_Name_20__c,Output_Name_21__c,Output_Name_22__c,Output_Name_23__c,Output_Name_24__c, ';
            query += ' Output_Name_25__c,Output_Name_26__c,Output_Name_27__c,Output_Name_28__c,Output_Name_29__c,Output_Name_30__c, ';
            query += ' Output_Name_31__c,Output_Name_32__c,Output_Name_33__c,Output_Name_34__c,Output_Name_35__c,Output_Name_36__c, ';
            query += ' Output_Name_37__c,Output_Name_38__c,Output_Name_39__c,Output_Name_40__c,Output_Name_41__c,Output_Name_42__c, ';
            query += ' Output_Name_43__c,Output_Name_44__c,Output_Name_45__c,Output_Name_46__c,Output_Name_47__c,Output_Name_48__c, ';
            query += ' Output_Name_49__c,Output_Name_50__c,Output_Value_1__c,Output_Value_2__c,Output_Value_3__c,Output_Value_4__c,Output_Value_5__c,Output_Value_6__c, ';
            query += ' Output_Value_7__c,Output_Value_8__c,Output_Value_9__c,Output_Value_10__c,Output_Value_11__c,Output_Value_12__c, ';
            query += ' Output_Value_13__c,Output_Value_14__c,Output_Value_15__c,Output_Value_16__c,Output_Value_17__c,Output_Value_18__c, ';
            query += ' Output_Value_19__c,Output_Value_20__c,Output_Value_21__c,Output_Value_22__c,Output_Value_23__c,Output_Value_24__c, ';
            query += ' Output_Value_25__c,Output_Value_26__c,Output_Value_27__c,Output_Value_28__c,Output_Value_29__c,Output_Value_30__c, ';
            query += ' Output_Value_31__c,Output_Value_32__c,Output_Value_33__c,Output_Value_34__c,Output_Value_35__c,Output_Value_36__c, ';
            query += ' Output_Value_37__c,Output_Value_38__c,Output_Value_39__c,Output_Value_40__c,Output_Value_41__c,Output_Value_42__c, ';
            query += ' Output_Value_43__c,Output_Value_44__c,Output_Value_45__c,Output_Value_46__c,Output_Value_47__c,Output_Value_48__c, ';
            query += ' Output_Value_49__c,Output_Value_50__c from Account_Compute_Final__c ';
            obj.query = query;
            Database.executeBatch(obj);
        }
        Test.stopTest();
    }
}