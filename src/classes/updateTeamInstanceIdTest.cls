@isTest
private class updateTeamInstanceIdTest{
    private static testMethod void firstTest(){
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        List<Master_List_AccProd__c> testList = new  List<Master_List_AccProd__c>();
        Master_List_AccProd__c testrec = new Master_List_AccProd__c();
        
        testrec.Scenario_Name__c = 'test1';
        testrec.Account_Name__c = 'test2';
        
        testList.add(testrec);
        
        List<String> aa = new List<String>();
        for(Master_List_AccProd__c Accprod :  testList)
        {
            aa.add(Accprod.Scenario_Name__c);
            
        }
        
        map<String,string> mapteaminst = new map<String,string>();
        
        for(AxtriaSalesIQTM__Team_Instance__c teamInst :  [Select Id,Name from AxtriaSalesIQTM__Team_Instance__c where Name IN: aa])
        {
            mapteaminst.put(teamInst.name,teamInst.id);
        }
        
        insert testList;
        
                    /*List<String> aa = new List<String>();
                      for(Master_List_AccProd__c Accprod :  testList)
                    {
                        aa.add(Accprod.Scenario_Name__c);
                    }
                    
                    map<String,string> mapteaminst = new map<String,string>();
                    
                     for(AxtriaSalesIQTM__Team_Instance__c teamInst :  [Select Id,Name from AxtriaSalesIQTM__Team_Instance__c where Name IN: aa])
                    {
                        mapteaminst.put(teamInst.name,teamInst.id);
                    }*/
                    
                }
            }