global with sharing class BatchPopulatePosGeoMetrics implements Database.Batchable<sObject>, Database.Stateful
{
    public String query;
    public String teamInstance;
    String teamInstanceName;
    public String Ids;
    Boolean flag1 = true;
    public integer recordsProcessed;
    public Integer recordsCreated;
    List<String> allFields;
    Map<String, Map<String, Decimal>> zipTerrMetricsMap;
    Map<String, Decimal> valMap;
    Set<String> allPositions;
    Set<String> allZips;
    Set<String> allTeamInstances;
    String namespace;
    public List<AxtriaSalesIQTM__Change_Request__c> cr = new List<AxtriaSalesIQTM__Change_Request__c>();

    //Added by HT(A0994) on 17th June 2020
    global String changeReqID;
    global Boolean flagValue = false;

    //Added by HT(A0994) on 1st July 2020 for STIMPS-153,166
    Map<String,String> mapTeamInstNameToSFID = new Map<String,String>();

    global BatchPopulatePosGeoMetrics(String teamInstance)
    {
        this.teamInstance = teamInstance;
        recordsProcessed =0;
        recordsCreated = 0;
        //this.Ids = Ids;
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'BatchPopulatePosGeoMetrics'];
        nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';

        if(teamInstance != 'All' && teamInstance != '')
        {
            teamInstanceName = [SELECT Name FROM AxtriaSalesIQTM__Team_Instance__c WHERE Id = :teamInstance].Name;
            this.query = 'SELECT Id, Position_Code__c, Team_Instance_Name__c, Zip_Name__c, Metric1__c, Metric2__c, Metric3__c, Metric4__c, Metric5__c, Metric6__c, Metric7__c, Metric8__c, Metric9__c, Metric10__c,Segment_10__c, Segment_1__c, Segment_2__c, Segment_3__c, Segment_4__c, Segment_5__c, Segment_6__c, Segment_7__c, Segment_8__c, Segment_9__c,Metric11__c,Metric12__c,Metric13__c,Metric14__c,Metric15__c,Change_Request__c, isError__c, SalesIQ_Error_Message__c, Error_message__c FROM temp_Obj__c WHERE Team_Instance_Name__c = :teamInstanceName AND Object__c =\'ZIP Terr Metrics Load\' AND Status__c = \'New\'';
        }
        else
        {
            teamInstanceName = 'All';
            this.query = 'SELECT Id, Position_Code__c, Team_Instance_Name__c, Zip_Name__c, Metric1__c, Metric2__c, Metric3__c, Metric4__c, Metric5__c, Metric6__c, Metric7__c, Metric8__c, Metric9__c, Metric10__c,Segment_10__c, Segment_1__c, Segment_2__c, Segment_3__c, Segment_4__c, Segment_5__c, Segment_6__c, Segment_7__c, Segment_8__c, Segment_9__c,Metric11__c,Metric12__c,Metric13__c,Metric14__c,Metric15__c,Change_Request__c, isError__c, SalesIQ_Error_Message__c, Error_message__c from temp_Obj__c WHERE Object__c =\'ZIP Terr Metrics Load\' AND Status__c = \'New\'';
        }

    }

    global BatchPopulatePosGeoMetrics(String teamInstance, String Ids)
    {
        this.teamInstance = teamInstance;
        this.Ids = Ids;
        recordsProcessed = 0;
        recordsCreated = 0;
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'BatchPopulatePosGeoMetrics'];
        nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        cr = [Select Id, AxtriaSalesIQTM__Request_Type_Change__c,Records_Created__c from AxtriaSalesIQTM__Change_Request__c where id = :IDs];


        if(teamInstance != 'All' && teamInstance != '')
        {
            teamInstanceName = [SELECT Name FROM AxtriaSalesIQTM__Team_Instance__c WHERE Id = :teamInstance].Name;
            this.query = 'SELECT Id, Position_Code__c, Team_Instance_Name__c, Zip_Name__c, Metric1__c, Metric2__c, Metric3__c, Metric4__c, Metric5__c, Metric6__c, Metric7__c, Metric8__c, Metric9__c, Metric10__c,Segment_10__c, Segment_1__c, Segment_2__c, Segment_3__c, Segment_4__c, Segment_5__c, Segment_6__c, Segment_7__c, Segment_8__c, Segment_9__c,Metric11__c,Metric12__c,Metric13__c,Metric14__c,Metric15__c,Change_Request__c, isError__c, SalesIQ_Error_Message__c, Error_message__c FROM temp_Obj__c WHERE Team_Instance_Name__c = :teamInstanceName AND Object__c =\'ZIP Terr Metrics Load\' AND Status__c = \'New\'';
        }
        else
        {
            teamInstanceName = 'All';
            this.query = 'SELECT Id, Position_Code__c, Team_Instance_Name__c, Zip_Name__c, Metric1__c, Metric2__c, Metric3__c, Metric4__c, Metric5__c, Metric6__c, Metric7__c, Metric8__c, Metric9__c, Metric10__c,Segment_10__c, Segment_1__c, Segment_2__c, Segment_3__c, Segment_4__c, Segment_5__c, Segment_6__c, Segment_7__c, Segment_8__c, Segment_9__c,Metric11__c,Metric12__c,Metric13__c,Metric14__c,Metric15__c,Change_Request__c, isError__c, SalesIQ_Error_Message__c, Error_message__c from temp_Obj__c WHERE Object__c =\'ZIP Terr Metrics Load\' AND Status__c = \'New\'';
        }

    }

    //Added by HT(A0994) on 17th June 2020
    global BatchPopulatePosGeoMetrics(String teamInstance, String Ids,Boolean flag)
    {
        this.teamInstance = teamInstance;
        this.Ids = Ids;
        changeReqID = Ids;
        flagValue = flag;
        recordsProcessed = 0;
        recordsCreated = 0;
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'BatchPopulatePosGeoMetrics'];
        nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        cr = [Select Id, AxtriaSalesIQTM__Request_Type_Change__c,Records_Created__c from 
                AxtriaSalesIQTM__Change_Request__c where id = :IDs WITH SECURITY_ENFORCED];

        if(teamInstance != 'All' && teamInstance != '')
        {
            teamInstanceName = [SELECT Name FROM AxtriaSalesIQTM__Team_Instance__c 
                                WHERE Id = :teamInstance WITH SECURITY_ENFORCED].Name;
            this.query = 'SELECT Id, Position_Code__c, Team_Instance_Name__c, Zip_Name__c, Metric1__c, '+
                        'Metric2__c, Metric3__c, Metric4__c, Metric5__c, Metric6__c, Metric7__c, '+
                        'Metric8__c, Metric9__c, Metric10__c,Segment_10__c, Segment_1__c, Segment_2__c, '+
                        'Segment_3__c, Segment_4__c, Segment_5__c, Segment_6__c, Segment_7__c, '+
                        'Segment_8__c, Segment_9__c,Metric11__c,Metric12__c,Metric13__c,Metric14__c,'+
                        'Metric15__c,Change_Request__c, isError__c, SalesIQ_Error_Message__c, '+
                        'Error_message__c FROM temp_Obj__c WHERE Team_Instance_Name__c = :teamInstanceName '+
                        'AND Object__c =\'ZIP Terr Metrics Load\' AND Status__c = \'New\' and '+
                        'Change_Request__c =:changeReqID WITH SECURITY_ENFORCED';
        }
        else
        {
            teamInstanceName = 'All';
            this.query = 'SELECT Id, Position_Code__c, Team_Instance_Name__c, Zip_Name__c, Metric1__c, '+
                        'Metric2__c, Metric3__c, Metric4__c, Metric5__c, Metric6__c, Metric7__c, Metric8__c, '+
                        'Metric9__c, Metric10__c,Segment_10__c, Segment_1__c, Segment_2__c, Segment_3__c, '+
                        'Segment_4__c, Segment_5__c, Segment_6__c, Segment_7__c, Segment_8__c, Segment_9__c,'+
                        'Metric11__c,Metric12__c,Metric13__c,Metric14__c,Metric15__c,Change_Request__c, '+
                        'isError__c, SalesIQ_Error_Message__c, Error_message__c from temp_Obj__c '+
                        'WHERE Object__c =\'ZIP Terr Metrics Load\' AND Status__c = \'New\' and '+
                        'Change_Request__c =:changeReqID WITH SECURITY_ENFORCED';
        }
    }

    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator(query);
    }

    public Boolean isDecimal(String str)
    {
        try
        {
            Decimal d = Decimal.valueOf(str);
            return true;
        }
        catch(Exception e)
        {
            return false;
        }
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {   
        recordsCreated+=scope.size();
        allPositions = new Set<String>();
        allZips = new Set<String>();
        allTeamInstances = new Set<String>();

        zipTerrMetricsMap = new Map<String, Map<String, Decimal>>();

        //Added by HT(A0994) on 1st July 2020 for STIMPS-153,166
        String teamInstName;
        String teamInstSFID;

        for(Integer i = 0, j = scope.size(); i < j; i++)
        {
            //Added by HT(A0994) on 1st July 2020 for STIMPS-153,166
            teamInstName = scope[i].get(namespace+'Team_Instance_Name__c')!=null ? 
                           (String)scope[i].get(namespace+'Team_Instance_Name__c') : '';

            if(teamInstName!='' && teamInstName!=''){
                allTeamInstances.add(teamInstName);
            }
        }

        //Added by HT(A0994) on 1st July 2020 for STIMPS-153,166
        if(allTeamInstances.size()>0 && allTeamInstances!=null)
        {
            String teamInstQuery = 'select Id,Name from AxtriaSalesIQTM__Team_Instance__c '+
                                        'where Name in :allTeamInstances';
            List<sObject> teamInstList = Database.query(teamInstQuery);

            if(teamInstList!=null && teamInstList.size()>0)
            {
                for(sObject rec : teamInstList)
                {
                    teamInstName = (String)rec.get('Name');
                    teamInstSFID = (String)rec.get('Id');
                    if(!mapTeamInstNameToSFID.containsKey(teamInstName)){
                        mapTeamInstNameToSFID.put(teamInstName,teamInstSFID);
                    }
                }
            }
        }

        List<Source_to_Destination_Mapping__c> mappingList = [SELECT Id, Destination_Object_Field__c, Source_Object_Field__c, Team_Instance_Name__c FROM Source_to_Destination_Mapping__c WHERE Team_Instance_Name__c IN :allTeamInstances AND Load_Type__c = 'ZIP Terr Metrics Load'];

        Map<String, Map<String, String>> sourceToDestMapping = new Map<String, Map<String, String>>();
        Map<String, String> tempMap = new Map<String, String>();

        for(Integer i = 0, j = mappingList.size(); i < j; i++)
        {
            tempMap = new Map<String, String>();
            if(sourceToDestMapping.containsKey(mappingList[i].Team_Instance_Name__c))
            {
                tempMap = sourceToDestMapping.get(mappingList[i].Team_Instance_Name__c);
            }
            if(!mappingList[i].Source_Object_Field__c.startsWith(namespace))
            {
                mappingList[i].Source_Object_Field__c = namespace + mappingList[i].Source_Object_Field__c;
            }
            tempMap.put(mappingList[i].Source_Object_Field__c, mappingList[i].Destination_Object_Field__c);
            sourceToDestMapping.put(mappingList[i].Team_Instance_Name__c, tempMap);
        }

        SnTDMLSecurityUtil.printDebugMessage('sourceToDestMapping  -> ');
        System.debug(sourceToDestMapping);

        String key;
        Boolean flag, flagTemp;
        String zipName, posCode, fieldName;
        String error = '';
        String value;
        for(Integer i = 0, j = scope.size(); i < j; i++)
        {
            teamInstanceName = (String)scope[i].get(namespace + 'Team_Instance_Name__c');
            zipName = (String)scope[i].get(namespace + 'Zip_Name__c');
            posCode = (String)scope[i].get(namespace + 'Position_Code__c');
            //mapTeamInstNameToSFID.containsKey(teamInstanceName) added by HT(A0994) on 1st July 2020 for STIMPS-153,166
            if(String.isNotBlank(zipName) && String.isNotBlank(posCode) && String.isNotBlank(teamInstanceName) 
                && mapTeamInstNameToSFID.containsKey(teamInstanceName))
            {
                flag = true;
                error = '';
                valMap = new Map<String, Decimal>();
                tempMap = new Map<String, String>();
                
                SnTDMLSecurityUtil.printDebugMessage('teamInstanceName ---> ' + teamInstanceName);
                if(sourceToDestMapping.containsKey(teamInstanceName)){
                    tempMap = sourceToDestMapping.get(teamInstanceName);
                }
                allFields = new List<String>();
                allFields.addAll(tempMap.keySet());

                for(Integer k = 0, l = allFields.size(); k < l; k++)
                {
                    fieldName = allFields[k];
                    value = String.valueOf(scope[i].get(fieldName));

                    //value check not null - Added by HT(A0994) on 1st July 2020 for STIMPS-153,166
                    if(value!=null && value!='')
                    {
                        flagTemp = isDecimal(value);
                        if(flagTemp){
                            valMap.put(allFields[k], Decimal.valueOf(value));
                        }
                        else
                        {
                            error += fieldName + ' is non-Decimal; ';
                            flag = flagTemp;
                        }
                    }
                    //Added by HT(A0994) on 1st July 2020 for STIMPS-153,166
                    //Note required since 0 will be pushed only when User adds 0 to the metrics
                    /*else{
                        valMap.put(allFields[k], Decimal.valueOf('0'));
                    }*/
                }

                if(flag)
                {
                    key = posCode + zipName + teamInstanceName;
                    allPositions.add(posCode);
                    allZips.add(zipName);
                    zipTerrMetricsMap.put(key, valMap);
                    scope[i].put('Status__c', 'Processed');
                    scope[i].put('Change_Request__c', Ids); //A1422
                    scope[i].put('isError__c', false);
                }
                else
                {
                    scope[i].put('Status__c', 'Rejected');
                    scope[i].put('SalesIQ_Error_Message__c', error);
                    scope[i].put('isError__c', true);
                    scope[i].put('Change_Request__c', Ids); //A1422
                }   
            }
            else{
                error = 'Mandatory Field Missing or Incorrect Team Instance';
                scope[i].put('Status__c', 'Rejected');
                scope[i].put('SalesIQ_Error_Message__c', error);
                scope[i].put('isError__c', true);
                scope[i].put('Change_Request__c', Ids); 
            }
        }

        List<AxtriaSalesIQTM__Position_Geography__c> posGeoList = [SELECT Id, AxtriaSalesIQTM__Geography__r.AxtriaSalesIQTM__Zip_Name__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,
                                                     AxtriaSalesIQTM__Team_Instance__r.Name FROM
                                                     AxtriaSalesIQTM__Position_Geography__c WHERE
                                                     AxtriaSalesIQTM__Team_Instance__r.Name IN :allTeamInstances
                                                     AND AxtriaSalesIQTM__Geography__r.AxtriaSalesIQTM__Zip_Name__c IN :allZips
                                                     AND AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c IN:allPositions
                                                     AND AxtriaSalesIQTM__Assignment_Status__c IN ('Active', 'Future Active')];

        List<AxtriaSalesIQTM__Position_Geography__c> posGeoToBeUpdated = new List<AxtriaSalesIQTM__Position_Geography__c>();
        Map<String, Decimal> dataMap;
        AxtriaSalesIQTM__Position_Geography__c posGeo;
        String destField, key2;
        for(Integer i = 0, j = posGeoList.size(); i < j; i++)
        {
            key = posGeoList[i].AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + posGeoList[i].AxtriaSalesIQTM__Geography__r.AxtriaSalesIQTM__Zip_Name__c + posGeoList[i].AxtriaSalesIQTM__Team_Instance__r.Name;
            if(zipTerrMetricsMap.containsKey(key))
            {
                dataMap = zipTerrMetricsMap.get(key);
                posGeo = new AxtriaSalesIQTM__Position_Geography__c(id = posGeoList[i].Id);
                SnTDMLSecurityUtil.printDebugMessage('key --> ' + key);
                SnTDMLSecurityUtil.printDebugMessage('dataMap --> ' + dataMap);
                SnTDMLSecurityUtil.printDebugMessage('teamInstanceName ---> ' + teamInstanceName);
                if(sourceToDestMapping.containsKey(teamInstanceName))
                {
                    tempMap = sourceToDestMapping.get(teamInstanceName);
                }
                allFields = new List<String>();
                allFields.addAll(tempMap.keySet());
                for(Integer k = 0, l = allFields.size(); k < l; k++) //String sourceField : allFields
                {
                    destField = tempMap.get(allFields[k]);
                    posGeo.put(destField, dataMap.get(allFields[k]));
                }
                posGeoToBeUpdated.add(posGeo);
            }
        }

        //Added by HT(A0994) for avoiding error if list is blank
        if(posGeoToBeUpdated!=null && posGeoToBeUpdated.size()>0)
        {
            SnTDMLSecurityUtil.printDebugMessage('posGeoToBeUpdated  -> ');
            System.debug(posGeoToBeUpdated);
            database.saveresult[] ds = Database.update(posGeoToBeUpdated, false);
            for(database.SaveResult d : ds)
            {
                if(d.issuccess()){
                    recordsProcessed++;
                }
                else{
                    flag1 = false;
                }
            }
        }
        
        //update scope;
        if(scope.size()>0 && scope!=null)
            SnTDMLSecurityUtil.updateRecords(scope, 'BatchPopulatePosGeoMetrics');
    }

    global void finish(Database.BatchableContext BC)
    {
        Boolean noJobErrors;
        String changeReqStatus;

        if(Ids != null)
        {
            AxtriaSalesIQTM__Change_Request__c changerequest = new AxtriaSalesIQTM__Change_Request__c();
            noJobErrors = ST_Utility.getJobStatus(BC.getJobId());
            changeReqStatus = flag1 && noJobErrors ? 'Done' : 'Error';
            /*if(flag1 && ST_Utility.getJobStatus(BC.getJobId()))
            {
                changerequest.Job_Status__c = 'Done';
            }
            else
            {
                changerequest.Job_Status__c = 'Error';
            }*/
            changerequest.Records_Updated__c = recordsProcessed;
            changerequest.Id = IDs;
            if(cr.size()>0 && cr[0].AxtriaSalesIQTM__Request_Type_Change__c == 'Data Load Backend'){
                changerequest.Records_Created__c = recordsCreated;
            }
            //update changerequest;
            SnTDMLSecurityUtil.updateRecords(changerequest, 'BatchPopulatePosGeoMetrics');

            BatchUpdateTempObjRecsCR batchCall = new BatchUpdateTempObjRecsCR(Ids,true,'Mandatory Field Missing or Incorrect Team Instance',changeReqStatus);
            Database.executeBatch(batchCall,2000);
            //Direct_Load_Records_Status_Update.Direct_Load_Records_Status_Update(Ids,'BatchPopulatePosGeoMetrics');  
        }
    }
}