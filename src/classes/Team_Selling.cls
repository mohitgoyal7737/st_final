public with sharing class Team_Selling 
{
    public static String teamInstanceSelected;
    public static String hcpaccount;
    public static String pName;
    public static String cycleselected;
    List<SIQ_MC_Cycle_Plan_Product_vod_O__c> mcCyclePlanProduct;
    public static Set<String> accNumber;
    public static Set<String> cycle;
    public static Set<String> prodName;
    public static List<String> allteamprodaccount;
    public static Map<String,Decimal> cycleProdAccMap;
    public static String currentcycle;
    public static String currentprod;
    public static String currentacc;

  public static Map<String,Decimal> updateIndividualGoals(List<string> teampaccproduct)
  {         
         cycle=new Set<String>();
         prodName= new Set<String>();
         accNumber= new Set<String>();
         cycleProdAccMap= new Map<String,Decimal>();

         for(String temp : teampaccproduct)
        {
           List<String> allVals = temp.split('&');
           system.debug('+++allVals'+allVals[0]);
           currentcycle=allVals[0];
           currentacc=allVals[1];
           currentprod=allVals[2];
           cycle.add(currentcycle);
           prodName.add(currentprod);
           accNumber.add(currentacc);
        }

        system.debug('++cycle'+cycle);
        system.debug('++prodName'+prodName);
        system.debug('++accNumber'+accNumber);



     List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacp = new List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
     Decimal indValue;
     pacp=[select id, Segment_Approved__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c, Final_TCF__c,Final_TCF_Approved__c,AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Account__r.AccountNumber ,  P1__c, AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Position__r.Original_Country_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c  in :cycle and  AxtriaSalesIQTM__lastApprovedTarget__c = true and AxtriaSalesIQTM__Position__c !=null and P1__c != null AND Parent_Pacp__c != null and P1__c in :prodName and AxtriaSalesIQTM__Account__r.AccountNumber in: accNumber];

     for(AxtriaSalesIQTM__Position_Account_Call_Plan__c ppacp : pacp)
       {
         system.debug('++pacp'+ppacp.Final_TCF_Approved__c);
            Integer f2fCalls = Integer.valueOf(ppacp.Final_TCF_Approved__c);
            indValue= Decimal.valueOf(f2fCalls); 
            system.debug('++indValue'+indValue);

               cycleProdAccMap.put(ppacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c +'&'+ppacp.AxtriaSalesIQTM__Account__r.AccountNumber+'&'+ppacp.P1__c,indValue);            
       }
       system.debug('+++cycleProdAccMap++'+cycleProdAccMap);

        return cycleProdAccMap;
  }   
  public static  Map<String,Decimal> updateSumTeamGoals(List<string> teamprodaccount)
  {
     cycle=new Set<String>();
         prodName= new Set<String>();
         accNumber= new Set<String>();
         cycleProdAccMap= new Map<String,Decimal>();

         for(String temp : teamprodaccount)
        {
           List<String> allVals = temp.split('&');
           system.debug('+++allVals'+allVals[0]);
           currentcycle=allVals[0];
           currentacc=allVals[1];
           currentprod=allVals[2];
           cycle.add(currentcycle);
           prodName.add(currentprod);
           accNumber.add(currentacc);
        }
         system.debug('++cycle'+cycle);
         system.debug('++prodName'+prodName);
         system.debug('++accNumber'+accNumber);

     List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacp = new List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
     decimal sum=0;
     pacp=[select id, Segment_Approved__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c, Final_TCF__c,Final_TCF_Approved__c,AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Account__r.AccountNumber ,  P1__c, AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Position__r.Original_Country_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c in: cycle and  AxtriaSalesIQTM__lastApprovedTarget__c = true and AxtriaSalesIQTM__Position__c !=null and P1__c != null AND Parent_Pacp__c != null and P1__c in:prodName and AxtriaSalesIQTM__Account__r.AccountNumber in : accNumber and AxtriaSalesIQTM__Team_Instance__r.Team_Selling__c=true];

       for(AxtriaSalesIQTM__Position_Account_Call_Plan__c ppacp : pacp)
       {
          system.debug('++pacp'+ppacp.Final_TCF_Approved__c);

          if(cycleProdAccMap.containsKey(ppacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c +'&'+ppacp.AxtriaSalesIQTM__Account__r.AccountNumber+'&'+ppacp.P1__c))
          {
              sum = cycleProdAccMap.get(ppacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c +'&'+ppacp.AxtriaSalesIQTM__Account__r.AccountNumber+'&'+ppacp.P1__c);
          }
          else
          {
              sum = 0;
          }
               sum = sum + ppacp.Final_TCF_Approved__c; 
               cycleProdAccMap.put(ppacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c +'&'+ppacp.AxtriaSalesIQTM__Account__r.AccountNumber+'&'+ppacp.P1__c,sum);  
       }
       system.debug('+++sum'+sum);
       system.debug('+++cycleProdAccMap++'+cycleProdAccMap);
        return cycleProdAccMap;
  }  

  public static  Map<String,Decimal> updateMaxTeamGoals(List<string> teamprodaccount)
  {
     cycle=new Set<String>();
         prodName= new Set<String>();
         accNumber= new Set<String>();
         cycleProdAccMap= new Map<String,Decimal>();

         for(String temp : teamprodaccount)
        {
           List<String> allVals = temp.split('&');
           system.debug('+++allVals'+allVals[0]);
           currentcycle=allVals[0];
           currentacc=allVals[1];
           currentprod=allVals[2];
           cycle.add(currentcycle);
           prodName.add(currentprod);
           accNumber.add(currentacc);
        }
         system.debug('++cycle'+cycle);
         system.debug('++prodName'+prodName);
         system.debug('++accNumber'+accNumber);

     List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacp = new List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
     decimal max=0;
     pacp=[select id, Segment_Approved__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c, Final_TCF__c,Final_TCF_Approved__c,AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Account__r.AccountNumber ,  P1__c, AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Position__r.Original_Country_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c in: cycle and  AxtriaSalesIQTM__lastApprovedTarget__c = true and AxtriaSalesIQTM__Position__c !=null and P1__c != null AND Parent_Pacp__c != null and P1__c in:prodName and AxtriaSalesIQTM__Account__r.AccountNumber in : accNumber and AxtriaSalesIQTM__Team_Instance__r.Team_Selling__c=true];

       for(AxtriaSalesIQTM__Position_Account_Call_Plan__c ppacp : pacp)
       {
          if(cycleProdAccMap.containsKey(ppacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c +'&'+ppacp.AxtriaSalesIQTM__Account__r.AccountNumber+'&'+ppacp.P1__c))
          {
              max = cycleProdAccMap.get(ppacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c +'&'+ppacp.AxtriaSalesIQTM__Account__r.AccountNumber+'&'+ppacp.P1__c);
          }
          else
          {
              max = 0;
          }
          if(max<ppacp.Final_TCF_Approved__c)
          {
            max=ppacp.Final_TCF_Approved__c;
          }
               cycleProdAccMap.put(ppacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c +'&'+ppacp.AxtriaSalesIQTM__Account__r.AccountNumber+'&'+ppacp.P1__c,max);  
       }
       system.debug('+++max'+max);
       system.debug('+++cycleProdAccMap++'+cycleProdAccMap);
          return cycleProdAccMap;
  }  
}