global with sharing class PreviewResults {
    public string ruleName{get;set;}
    public PreviewResults(){
        string ruleId = ApexPages.currentPage().getParameters().get('rid');
        list<Measure_Master__c> rules = [SELECT Name FROM Measure_Master__c WHERE ID=:ruleId WITH SECURITY_ENFORCED];
        ruleName = rules[0].Name;
    }

    @RemoteAction
    global static FinalResultWrapper getComputedResults(String rid){
        string ruleId = rid;
        FinalResultWrapper result = new FinalResultWrapper();
        list<Measure_Master__c> rules = [SELECT Name FROM Measure_Master__c WHERE ID=:ruleId];
        list<DataWrappwer> FinalResult = new list<DataWrappwer>();
        if(rules != null && rules.size() > 0){
            list<ColumNameWrapper> columnsNames = new list<ColumNameWrapper>();
            list<Account_Compute_Final__c> computeFinals = [SELECT Id, Physician_2__r.AxtriaSalesIQTM__External_Account_Number__c, Physician_2__r.AccountNumber, Physician_2__r.Name, Physician_2__c, Measure_Master__c, Measure_Master__r.Name, Physician__r.AxtriaSalesIQTM__SharedWith__c,Physician__r.AxtriaSalesIQTM__IsShared__c,OUTPUT_NAME_1__c, OUTPUT_NAME_2__c, OUTPUT_NAME_3__c, OUTPUT_NAME_4__c, OUTPUT_NAME_5__c, OUTPUT_NAME_7__c, OUTPUT_NAME_6__c, OUTPUT_NAME_8__c,OUTPUT_NAME_9__c, OUTPUT_NAME_10__c, OUTPUT_NAME_11__c, OUTPUT_NAME_12__c, OUTPUT_NAME_13__c, OUTPUT_NAME_14__c, OUTPUT_NAME_15__c, OUTPUT_NAME_16__c, OUTPUT_NAME_17__c, OUTPUT_NAME_18__c, OUTPUT_NAME_19__c, OUTPUT_NAME_20__c, OUTPUT_NAME_21__c, OUTPUT_NAME_22__c, OUTPUT_NAME_23__c, OUTPUT_NAME_24__c, OUTPUT_NAME_25__c, OUTPUT_NAME_26__c, OUTPUT_NAME_27__c, OUTPUT_NAME_28__c, OUTPUT_NAME_29__c, OUTPUT_NAME_30__c, OUTPUT_NAME_31__c,OUTPUT_NAME_32__c,OUTPUT_NAME_33__c,OUTPUT_NAME_34__c,OUTPUT_NAME_35__c, OUTPUT_NAME_36__c,OUTPUT_NAME_37__c,OUTPUT_NAME_38__c,OUTPUT_NAME_39__c,OUTPUT_NAME_40__c, OUTPUT_NAME_41__c,OUTPUT_NAME_42__c,OUTPUT_NAME_43__c,OUTPUT_NAME_44__c,OUTPUT_NAME_45__c, OUTPUT_NAME_46__c,OUTPUT_NAME_47__c,OUTPUT_NAME_48__c,OUTPUT_NAME_49__c,OUTPUT_NAME_50__c,  OUTPUT_VALUE_1__c, OUTPUT_VALUE_2__c, OUTPUT_VALUE_3__c, OUTPUT_VALUE_4__c, OUTPUT_VALUE_5__c, OUTPUT_VALUE_6__c, OUTPUT_VALUE_7__c, OUTPUT_VALUE_8__c, OUTPUT_VALUE_9__c, OUTPUT_VALUE_10__c, OUTPUT_VALUE_11__c, OUTPUT_VALUE_12__c, OUTPUT_VALUE_13__c, OUTPUT_VALUE_14__c, OUTPUT_VALUE_15__c, OUTPUT_VALUE_16__c, OUTPUT_VALUE_17__c, OUTPUT_VALUE_18__c, OUTPUT_VALUE_19__c, OUTPUT_VALUE_20__c, OUTPUT_VALUE_21__c, OUTPUT_VALUE_22__c, OUTPUT_VALUE_23__c,Assigned_Rep__c, OUTPUT_VALUE_24__c, OUTPUT_VALUE_25__c, OUTPUT_VALUE_26__c, OUTPUT_VALUE_27__c, OUTPUT_VALUE_28__c, OUTPUT_VALUE_29__c, OUTPUT_VALUE_30__c,OUTPUT_VALUE_31__c,OUTPUT_VALUE_32__c,OUTPUT_VALUE_33__c,OUTPUT_VALUE_34__c,OUTPUT_VALUE_35__c,OUTPUT_VALUE_36__c,OUTPUT_VALUE_37__c,OUTPUT_VALUE_38__c,OUTPUT_VALUE_39__c,OUTPUT_VALUE_40__c,OUTPUT_VALUE_41__c,OUTPUT_VALUE_42__c,OUTPUT_VALUE_43__c,OUTPUT_VALUE_44__c,OUTPUT_VALUE_45__c,OUTPUT_VALUE_46__c,OUTPUT_VALUE_47__c,OUTPUT_VALUE_48__c,OUTPUT_VALUE_49__c,OUTPUT_VALUE_50__c,Position__c FROM Account_Compute_Final__c WHERE Measure_Master__c =: ruleId WITH SECURITY_ENFORCED LIMIT 5000];
            Boolean isFirst = true;
            if(computeFinals != null && computeFinals.size() > 0){
                for(Account_Compute_Final__c acf: computeFinals){
                    FinalResult.add(new DataWrappwer(acf));
                    if(isFirst){
                        columnsNames.add(new ColumNameWrapper('ID', 'Id','', false));
                        columnsNames.add(new ColumNameWrapper('Account ID', 'accountId', '', false));
                         columnsNames.add(new ColumNameWrapper('Physician', 'physicianName', '', true));
                        columnsNames.add(new ColumNameWrapper('HCP Number', 'hcpNumber', '', true));                       
                        columnsNames.add(new ColumNameWrapper('Rule ID', 'ruleId', '', false));
                        columnsNames.add(new ColumNameWrapper('Rule Name', 'ruleName', '', true));
                         columnsNames.add(new ColumNameWrapper('Position', 'position', '', true));
                         columnsNames.add(new ColumNameWrapper('Shared With', 'Sharedwith', '', true));
                         columnsNames.add(new ColumNameWrapper('Is Shared', 'IsShared', '', true));
                         columnsNames.add(new ColumNameWrapper('Assigned Rep', 'AssignedRep', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_1__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_1__c, 'output1', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_2__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_2__c, 'output2', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_3__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_3__c, 'output3', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_4__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_4__c, 'output4', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_5__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_5__c, 'output5', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_6__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_6__c, 'output6', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_7__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_7__c, 'output7', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_8__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_8__c, 'output8', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_9__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_9__c, 'output9', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_10__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_10__c, 'output10', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_11__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_11__c, 'output11', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_12__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_12__c, 'output12', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_13__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_13__c, 'output13', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_14__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_14__c, 'output14', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_15__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_15__c, 'output15', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_16__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_16__c, 'output16', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_17__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_17__c, 'output17', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_18__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_18__c, 'output18', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_19__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_19__c, 'output19', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_20__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_20__c, 'output20', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_21__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_21__c, 'output21', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_22__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_22__c, 'output22', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_23__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_23__c, 'output23', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_24__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_24__c, 'output24', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_25__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_25__c, 'output25', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_26__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_26__c, 'output26', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_27__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_27__c, 'output27', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_28__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_28__c, 'output28', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_29__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_29__c, 'output29', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_30__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_30__c, 'output30', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_31__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_31__c, 'output31', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_32__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_32__c, 'output32', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_33__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_33__c, 'output33', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_34__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_34__c, 'output34', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_35__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_35__c, 'output35', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_36__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_36__c, 'output36', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_37__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_37__c, 'output37', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_38__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_38__c, 'output38', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_39__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_39__c, 'output39', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_40__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_40__c, 'output40', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_41__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_41__c, 'output41', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_42__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_42__c, 'output42', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_43__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_43__c, 'output43', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_44__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_44__c, 'output44', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_45__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_45__c, 'output45', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_46__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_46__c, 'output46', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_47__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_47__c, 'output47', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_48__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_48__c, 'output48', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_49__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_49__c, 'output49', '', true));
                        if(String.isNotBlank(acf.OUTPUT_NAME_50__c))
                            columnsNames.add(new ColumNameWrapper(acf.OUTPUT_NAME_50__c, 'output50', '', true));
                        //columns = JSON.serialize(columnsNames);
                        isFirst = false;
                    }
                }
            }
            result = new FinalResultWrapper();
            result.columns = columnsNames;
            result.results = FinalResult;
        }
        return result;
    }

    global class FinalResultWrapper{
        public list<ColumNameWrapper> columns{get;set;}
        public list<DataWrappwer> results {get;set;}
    }

    global class DataWrappwer
    {
        public string Id{get;set;}
        public string hcpNumber{get;set;}
        public string accountId{get;set;}
        public string Sharedwith{get;set;}
        public boolean IsShared{get;set;}
        public string AssignedRep{get;set;}
        public string position{get;set;}
        public string physicianName{get;set;}
        public string ruleId{get;set;}
        public string ruleName{get;set;}
        public string output1{get;set;}
        public string output2{get;set;}
        public string output3{get;set;}
        public string output4{get;set;}
        public string output5{get;set;}
        public string output6{get;set;}
        public string output7{get;set;}
        public string output8{get;set;}
        public string output9{get;set;}
        public string output10{get;set;}
        public string output11{get;set;}
        public string output12{get;set;}
        public string output13{get;set;}
        public string output14{get;set;}
        public string output15{get;set;}
        public string output16{get;set;}
        public string output17{get;set;}
        public string output18{get;set;}
        public string output19{get;set;}
        public string output20{get;set;}
        public string output21{get;set;}
        public string output22{get;set;}
        public string output23{get;set;}
        public string output24{get;set;}
        public string output25{get;set;}
        public string output26{get;set;}
        public string output27{get;set;}
        public string output28{get;set;}
        public string output29{get;set;}
        public string output30{get;set;}
        public string output31{get;set;}
        public string output32{get;set;}
        public string output33{get;set;}
        public string output34{get;set;}
        public string output35{get;set;}
        public string output36{get;set;}
        public string output37{get;set;}
        public string output38{get;set;}
        public string output39{get;set;}
        public string output40{get;set;}
        public string output41{get;set;}
        public string output42{get;set;}
        public string output43{get;set;}
        public string output44{get;set;}
        public string output45{get;set;}
        public string output46{get;set;}
        public string output47{get;set;}
        public string output48{get;set;}
        public string output49{get;set;}
        public string output50{get;set;}



        public DataWrappwer(Account_Compute_Final__c acf)
        {
            this.Id = acf.Id;
            this.hcpNumber = acf.Physician_2__r.AccountNumber;
            this.accountId = acf.Physician_2__c;
            this.physicianName = acf.Physician_2__r.Name;
            this.position = acf.Position__c;
            this.Sharedwith = acf.Physician__r.AxtriaSalesIQTM__SharedWith__c;
            this.IsShared = acf.Physician__r.AxtriaSalesIQTM__IsShared__c;   
            this.AssignedRep = acf.Assigned_Rep__c;          
            this.ruleId = acf.Measure_Master__c;
            this.ruleName = acf.Measure_Master__r.Name;
            this.output1 = acf.OUTPUT_VALUE_1__c != null ? acf.OUTPUT_VALUE_1__c : '';
            this.output2 = acf.OUTPUT_VALUE_2__c != null ? acf.OUTPUT_VALUE_2__c : '';
            this.output3 = acf.OUTPUT_VALUE_3__c != null ? acf.OUTPUT_VALUE_3__c : '';
            this.output4 = acf.OUTPUT_VALUE_4__c != null ? acf.OUTPUT_VALUE_4__c : '';
            this.output5 = acf.OUTPUT_VALUE_5__c != null ? acf.OUTPUT_VALUE_5__c : '';
            this.output6 = acf.OUTPUT_VALUE_6__c != null ? acf.OUTPUT_VALUE_6__c : '';
            this.output7 = acf.OUTPUT_VALUE_7__c != null ? acf.OUTPUT_VALUE_7__c : '';
            this.output8 = acf.OUTPUT_VALUE_8__c != null ? acf.OUTPUT_VALUE_8__c : '';
            this.output9 = acf.OUTPUT_VALUE_9__c != null ? acf.OUTPUT_VALUE_9__c : '';
            this.output10 = acf.OUTPUT_VALUE_10__c != null ? acf.OUTPUT_VALUE_10__c : '';
            this.output11 = acf.OUTPUT_VALUE_11__c != null ? acf.OUTPUT_VALUE_11__c : '';
            this.output12 = acf.OUTPUT_VALUE_12__c != null ? acf.OUTPUT_VALUE_12__c : '';
            this.output13 = acf.OUTPUT_VALUE_13__c != null ? acf.OUTPUT_VALUE_13__c : '';
            this.output14 = acf.OUTPUT_VALUE_14__c != null ? acf.OUTPUT_VALUE_14__c : '';
            this.output15 = acf.OUTPUT_VALUE_15__c != null ? acf.OUTPUT_VALUE_15__c : '';
            this.output16 = acf.OUTPUT_VALUE_16__c != null ? acf.OUTPUT_VALUE_16__c : '';
            this.output17 = acf.OUTPUT_VALUE_17__c != null ? acf.OUTPUT_VALUE_17__c : '';
            this.output18 = acf.OUTPUT_VALUE_18__c != null ? acf.OUTPUT_VALUE_18__c : '';
            this.output19 = acf.OUTPUT_VALUE_19__c != null ? acf.OUTPUT_VALUE_19__c : '';
            this.output20 = acf.OUTPUT_VALUE_20__c != null ? acf.OUTPUT_VALUE_20__c : '';
            this.output21 = acf.OUTPUT_VALUE_21__c != null ? acf.OUTPUT_VALUE_21__c : '';
            this.output22 = acf.OUTPUT_VALUE_22__c != null ? acf.OUTPUT_VALUE_22__c : '';
            this.output23 = acf.OUTPUT_VALUE_23__c != null ? acf.OUTPUT_VALUE_23__c : '';
            this.output24 = acf.OUTPUT_VALUE_24__c != null ? acf.OUTPUT_VALUE_24__c : '';
            this.output25 = acf.OUTPUT_VALUE_25__c != null ? acf.OUTPUT_VALUE_25__c : '';
            this.output26 = acf.OUTPUT_VALUE_26__c != null ? acf.OUTPUT_VALUE_26__c : '';
            this.output27 = acf.OUTPUT_VALUE_27__c != null ? acf.OUTPUT_VALUE_27__c : '';
            this.output28 = acf.OUTPUT_VALUE_28__c != null ? acf.OUTPUT_VALUE_28__c : '';
            this.output29 = acf.OUTPUT_VALUE_29__c != null ? acf.OUTPUT_VALUE_29__c : '';
            this.output30 = acf.OUTPUT_VALUE_30__c != null ? acf.OUTPUT_VALUE_30__c : '';
            this.output31 = acf.OUTPUT_VALUE_31__c != null ? acf.OUTPUT_VALUE_31__c : '';
            this.output32 = acf.OUTPUT_VALUE_32__c != null ? acf.OUTPUT_VALUE_32__c : '';
            this.output33 = acf.OUTPUT_VALUE_33__c != null ? acf.OUTPUT_VALUE_33__c : '';
            this.output34 = acf.OUTPUT_VALUE_34__c != null ? acf.OUTPUT_VALUE_34__c : '';
            this.output35 = acf.OUTPUT_VALUE_35__c != null ? acf.OUTPUT_VALUE_35__c : '';
            this.output36 = acf.OUTPUT_VALUE_36__c != null ? acf.OUTPUT_VALUE_36__c : '';
            this.output37 = acf.OUTPUT_VALUE_37__c != null ? acf.OUTPUT_VALUE_37__c : '';
            this.output38 = acf.OUTPUT_VALUE_38__c != null ? acf.OUTPUT_VALUE_38__c : '';
            this.output39 = acf.OUTPUT_VALUE_39__c != null ? acf.OUTPUT_VALUE_39__c : '';
            this.output40 = acf.OUTPUT_VALUE_40__c != null ? acf.OUTPUT_VALUE_40__c : '';
            this.output41 = acf.OUTPUT_VALUE_41__c != null ? acf.OUTPUT_VALUE_41__c : '';
            this.output42 = acf.OUTPUT_VALUE_42__c != null ? acf.OUTPUT_VALUE_42__c : '';
            this.output43 = acf.OUTPUT_VALUE_43__c != null ? acf.OUTPUT_VALUE_43__c : '';
            this.output44 = acf.OUTPUT_VALUE_44__c != null ? acf.OUTPUT_VALUE_44__c : '';
            this.output45 = acf.OUTPUT_VALUE_45__c != null ? acf.OUTPUT_VALUE_45__c : '';
            this.output46 = acf.OUTPUT_VALUE_46__c != null ? acf.OUTPUT_VALUE_46__c : '';
            this.output47 = acf.OUTPUT_VALUE_47__c != null ? acf.OUTPUT_VALUE_47__c : '';
            this.output48 = acf.OUTPUT_VALUE_48__c != null ? acf.OUTPUT_VALUE_48__c : '';
            this.output49 = acf.OUTPUT_VALUE_49__c != null ? acf.OUTPUT_VALUE_49__c : '';
            this.output50 = acf.OUTPUT_VALUE_50__c != null ? acf.OUTPUT_VALUE_50__c : '';
        }
    }  
    

    global class ColumNameWrapper{
        public String title {get;set;}
        public String data{get;set;}
        public String defaultContent{get;set;}
        public Boolean visible{get;set;}
        public ColumNameWrapper(String fieldTitle, String fieldApiName, String df, Boolean vis){
            this.title = fieldTitle;
            this.data = fieldApiName;
            this.defaultContent = df;
            this.visible = vis;
        }
    }
}