global with sharing class MarkMCCPproductDeleted_EU implements Database.Batchable<sObject> {
    public String query;
    public String selectedTeamInstance;
    public List<String> allTeamInstances;
    Set<String> activityLogIDSet;

    global MarkMCCPproductDeleted_EU() 
    {
        this.query = 'select id from SIQ_MC_Cycle_Plan_Product_vod_O__c where Rec_Status__c != \'Updated\' ';
    }

    global MarkMCCPproductDeleted_EU(String teamInstance) 
    {
        selectedTeamInstance = teamInstance;
        this.query = 'select id from SIQ_MC_Cycle_Plan_Product_vod_O__c where Rec_Status__c != \'Updated\' and Team_Instance__c = :selectedTeamInstance ';
    }

    global MarkMCCPproductDeleted_EU(List<String> teamInstanceList) 
    {
        allTeamInstances = new List<String>(teamInstanceList);

        this.query = 'select id, Team_Instance__c, Account__c, Position__c, SIQ_Cycle_Plan_Channel_vod__c from SIQ_MC_Cycle_Plan_Product_vod_O__c where Rec_Status__c != \'Updated\' and Team_Instance__c in :allTeamInstances ';
    }

    //Added by Ayushi
    global MarkMCCPproductDeleted_EU(List<String> teamInstanceList,Set<String> activityLogSet) 
    {
        allTeamInstances = new List<String>(teamInstanceList);
        activityLogIDSet = new Set<String>();
        activityLogIDSet.addAll(activityLogSet);

        this.query = 'select id, Team_Instance__c, Account__c, Position__c, SIQ_Cycle_Plan_Channel_vod__c from SIQ_MC_Cycle_Plan_Product_vod_O__c where Rec_Status__c != \'Updated\' and Team_Instance__c in :allTeamInstances ';
    }
    //Till here........


    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<SIQ_MC_Cycle_Plan_Product_vod_O__c> scope)
     {

        Set<String> allTargets = new Set<String>();
        Set<String> allPositions = new Set<String>();
        for(SIQ_MC_Cycle_Plan_Product_vod_O__c mt : scope)
        {
            allPositions.add(mt.Position__c);
            allTargets.add(mt.Account__c);
            //List<String> tempList1 = (mt.SIQ_Cycle_Plan_Target_vod__c).split('_');
            
        }
        List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacp= [select ID, Segment10__c ,AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c FROM AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances and AxtriaSalesIQTM__Account__r.AccountNumber IN  :allTargets and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c IN :allPositions];

        Map<string,string> posaccseg = new Map<string,string>();

        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pp : pacp)
        {
            string accpos= pp.AxtriaSalesIQTM__Account__r.AccountNumber+'_'+pp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_'+ pp.AxtriaSalesIQTM__Team_Instance__c;
            posaccseg.put(accpos,pp.Segment10__c);
        }

            for(SIQ_MC_Cycle_Plan_Product_vod_O__c mt : scope)
            {   

                //List<String> tempList1 = (mt.SIQ_Cycle_Plan_Channel_vod__c).split('_');
                String pos = mt.Position__c;
                String acc = mt.Account__c;    
                String accPos = acc + '_' + pos + '_' + mt.Team_Instance__c;
                if(posaccseg.containsKey(accPos))
                {
                    if(posaccseg.get(accPos) ==null )
                 {
                   
                   mt.Rec_Status__c ='Deleted';
                 }
                   else if(posaccseg.get(accPos) =='Inactive')
                 {
                   
                   mt.Rec_Status__c ='Updated';
                 }
                  else if(posaccseg.get(accPos) =='Merged')
                 {
                   mt.Rec_Status__c ='Updated'; 
                 }
                   else
                 {
                    mt.Rec_Status__c ='Deleted';
                 }   
            } 
                   else
                 {
                   mt.Rec_Status__c ='Deleted';
                 }   
            }   

            //update scope;
            SnTDMLSecurityUtil.updateRecords(scope, 'MarkMCCPproductDeleted_EU');
            }

    global void finish(Database.BatchableContext BC) 
    {
        if(activityLogIDSet != null)
        {
            List<Scheduler_Log__c> activityLogList = new List<Scheduler_Log__c>();

            AxtriaSalesIQTM__TriggerContol__c exeTrigger = new AxtriaSalesIQTM__TriggerContol__c();
            exeTrigger = AxtriaSalesIQTM__TriggerContol__c.getValues('VeevaFullLoad')!= null ? AxtriaSalesIQTM__TriggerContol__c.getValues('VeevaFullLoad'):null;

            SnTDMLSecurityUtil.printDebugMessage('execute trigger' +exeTrigger );
            //public static Boolean executeTrigger; 
            if(exeTrigger != null)
            {
                if(exeTrigger.AxtriaSalesIQTM__IsStopTrigger__c != true)
                {
                    List<Scheduler_Log__c> logList = [select Id,Team_Instance__c,Job_Status__c from Scheduler_Log__c where Id in :activityLogIDSet and Team_Instance__c in :allTeamInstances and Job_Type__c = 'Veeva Full Load' and Job_Status__c = 'Failed'];

                    for(Scheduler_Log__c rec : logList)
                    {
                        rec.Job_Status__c = 'Success';
                        activityLogList.add(rec);
                    }

                    //update activityLogList;
                    SnTDMLSecurityUtil.updateRecords(activityLogList, 'MarkMCCPproductDeleted_EU');
                }
            }

            SnTDMLSecurityUtil.printDebugMessage('====activityLogIDSet :::::::::' +activityLogIDSet);
            SnTDMLSecurityUtil.printDebugMessage('====allTeamInstances :::::::::' +allTeamInstances);

            SnTDMLSecurityUtil.printDebugMessage('Check all teamIns Status');
            List<Scheduler_Log__c> finallogList = [select Id,Team_Instance__c, Job_Status__c from Scheduler_Log__c where Id in :activityLogIDSet and Job_Type__c = 'Veeva Full Load' and Job_Status__c = 'Success'];

            SnTDMLSecurityUtil.printDebugMessage('====finallogList.size() :::::::::' +finallogList.size());
            SnTDMLSecurityUtil.printDebugMessage('====activityLogIDSet.size() :::::::::' +activityLogIDSet.size());

            Integer activityLogSize = activityLogIDSet.size();
            Integer finalLogSize = finallogList.size();
            if(activityLogSize == finalLogSize)
            {

                ID jobID = System.enqueueJob(new TalendHitFullLoadQueueable());
                SnTDMLSecurityUtil.printDebugMessage('====jobID :::::::::' +jobID);
            }
        }

    }
}