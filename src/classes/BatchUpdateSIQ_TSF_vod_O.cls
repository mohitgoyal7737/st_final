global class BatchUpdateSIQ_TSF_vod_O implements schedulable,Database.Batchable<sObject>, Database.Stateful{
    public String query;
    public List<String> allProcessedIds = new List<String>();
    public List<String> allTeamInstances;
    
    global BatchUpdateSIQ_TSF_vod_O (List<String> allTeamInstances) {
        this.allTeamInstances  = allTeamInstances;  
    }
       
    global Database.QueryLocator start(Database.BatchableContext bc) {
        this.query='select id,Status__c from SIQ_TSF_vod_O__c where Team_Instance__c in : allTeamInstances' ; 

        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<SIQ_TSF_vod_O__c > scope) {

        
         for(SIQ_TSF_vod_O__c siq : scope)
         {
           siq.status__c='Deleted';
         }
         update scope;
    }
    
    global void execute(System.SchedulableContext SC){
        allTeamInstances= new list<string>();
        allTeamInstances= StaticTeaminstanceList.getCountries();
        database.executebatch(new BatchUpdateSIQ_TSF_vod_O (allTeamInstances));
    } 
    
    

    global void finish(Database.BatchableContext BC) {
    
          database.executebatch(new Batch_Integration_TSF_PositionAccount (allTeamInstances));
 
    }
}