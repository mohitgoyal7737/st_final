/**********************************************************************************************
Author      : Himanshu Tariyal (A0994)
Date        : 6th October'2020
Description : Test class for CreateHCOSegBatch
Revision(s) : v1.0
**********************************************************************************************/
@isTest
private class CreateHCOSegBatchTest 
{
    static testMethod void testMethod1() 
    {
    	String className = 'CreateHCOSegBatchTest';

    	Account acc = new Account();
    	acc.Name = 'ACC_Name';
    	acc.AccountNumber = 'ACC_Number';
    	acc.Marketing_Code__c = 'IT';
    	SnTDMLSecurityUtil.insertRecords(acc,className);

    	AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    	SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);

        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today().addDays(90));
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);

        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins,className);

        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        SnTDMLSecurityUtil.insertRecords(scen,className);

        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        pcc.Product_Code__c='PROD1';
        pcc.Veeva_External_ID__c='PROD1';
        SnTDMLSecurityUtil.insertRecords(pcc,className);

        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        mmc.State__c ='In Progress';
        mmc.Target_Count_Maximum__c = 100;
        mmc.Target_Count_Minimun__c = 20;
        mmc.Workload_Hours__c = 8;
        mmc.Desired_Range__c = '-20';
        mmc.Single_Product_Rule__c = true;
        mmc.Rule_Type__c = 'MCCP';
        SnTDMLSecurityUtil.insertRecords(mmc,className);

        Measure_Master__c mmc2 = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc2.Team_Instance__c = teamins.id;
        mmc2.State__c ='In Progress';
        mmc2.Target_Count_Maximum__c = 100;
        mmc2.Target_Count_Minimun__c = 20;
        mmc2.Workload_Hours__c = 8;
        mmc2.Desired_Range__c = '-20';
        mmc2.Single_Product_Rule__c = true;
        mmc2.Rule_Type__c = 'MCCP';
        SnTDMLSecurityUtil.insertRecords(mmc2,className);

        System.Test.startTest();

        String nameSpace = MCCP_Utility.sntNamespace(className);
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'State__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Measure_Master__c.SObjectType, RULEPARAMETER_READ_FIELD, false));

        Map<String,Map<String,Decimal>> testMap = new Map<String,Map<String,Decimal>>();
        Map<String,Decimal> innerMap = new Map<String,Decimal>();
        innerMap.put('Param1',12.0);
        testMap.put(acc.Id,innerMap);

        CreateHCOSegBatch batchCall = new CreateHCOSegBatch(mmc.Id,mmc2.Id,testMap);
        Database.executeBatch(batchCall,2000);

        System.Test.stopTest();
    }

    static testMethod void testMethod2() 
    {
    	String className = 'CreateHCOSegBatchTest';

    	Account acc = new Account();
    	acc.Name = 'ACC_Name';
    	acc.AccountNumber = 'ACC_Number';
    	acc.Marketing_Code__c = 'IT';
    	SnTDMLSecurityUtil.insertRecords(acc,className);

    	AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    	SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);

        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today().addDays(90));
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);

        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins,className);

        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        SnTDMLSecurityUtil.insertRecords(scen,className);

        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        pcc.Product_Code__c='PROD1';
        pcc.Veeva_External_ID__c='PROD1';
        SnTDMLSecurityUtil.insertRecords(pcc,className);

        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        mmc.State__c ='In Progress';
        mmc.Target_Count_Maximum__c = 100;
        mmc.Target_Count_Minimun__c = 20;
        mmc.Workload_Hours__c = 8;
        mmc.Desired_Range__c = '-20';
        mmc.Single_Product_Rule__c = true;
        mmc.Rule_Type__c = 'MCCP';
        SnTDMLSecurityUtil.insertRecords(mmc,className);

        Measure_Master__c mmc2 = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc2.Team_Instance__c = teamins.id;
        mmc2.State__c ='In Progress';
        mmc2.Target_Count_Maximum__c = 100;
        mmc2.Target_Count_Minimun__c = 20;
        mmc2.Workload_Hours__c = 8;
        mmc2.Desired_Range__c = '-20';
        mmc2.Single_Product_Rule__c = true;
        mmc2.Rule_Type__c = 'MCCP';
        SnTDMLSecurityUtil.insertRecords(mmc2,className);

        System.Test.startTest();

        String nameSpace = MCCP_Utility.sntNamespace(className);
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'State__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Measure_Master__c.SObjectType, RULEPARAMETER_READ_FIELD, false));

        Map<String,Map<String,Decimal>> testMap = new Map<String,Map<String,Decimal>>();
        Map<String,Decimal> innerMap = new Map<String,Decimal>();
        innerMap.put('Param1',12.0);
        testMap.put(acc.Id,innerMap);

        CreateHCOSegBatch batchCall = new CreateHCOSegBatch(mmc.Id,mmc2.Id,testMap,teamins.Id,'',false);
        Database.executeBatch(batchCall,2000);

        System.Test.stopTest();
    }
}