public Interface Step {
    String solveStep(Step__c step, Map<String, String> nameFieldMap, Map<String, String> acfMap, BU_Response__c bu, List<BU_Response__c> allBuResponses, List<String> allAggregateFunc);
}