global  with sharing class Batch_DeleteTeamInstance implements Database.Batchable<sObject> {
    list<SIQ_MC_Cycle_vod_O__c> listTI;
    list<SIQ_MC_Cycle_vod_O__c> delListTI;
    string jobType = util.getJobType('TeamInstance');
    set<string> allCountries = jobType == 'Full Load'?util.getFulloadCountryCode():util.getDeltaLoadCountryCode();
   // map<string, boolean> mapProdStatus;
    string query = 'Select Id, SIQ_Status_vod__c, SIQ_Country_Code_AZ__c, Record_Status__c from SIQ_MC_Cycle_vod_O__c WITH SECURITY_ENFORCED';
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return database.getQueryLocator(query);
    }
            
    global void execute(Database.BatchableContext bc, List<SIQ_MC_Cycle_vod_O__c> scope){
        listTI = new list<SIQ_MC_Cycle_vod_O__c>();
        delListTI = new list<SIQ_MC_Cycle_vod_O__c>();
        if(jobType == 'Full Load'){
            for(SIQ_MC_Cycle_vod_O__c pe : scope){
                if(allCountries.contains(pe.SIQ_Country_Code_AZ__c)){
                    delListTI.add(pe);
                }
                else{
                    pe.Record_Status__c = 'No Change';
                    listTI.add(pe);    
                }
            }
        }
        else{
            for(SIQ_MC_Cycle_vod_O__c pe : scope){ 
                pe.Record_Status__c = 'No Change';
                listTI.add(pe);
            }    
        }
        if(listTI.size()>0){
            //update listTI;
            SnTDMLSecurityUtil.updateRecords(listTI,'Batch_DeleteTeamInstance');
        }
        if(delListTI.size()>0){
            //delete delListTI;
            SnTDMLSecurityUtil.deleteRecords(delListTI,'Batch_DeleteTeamInstance');
        }
    }
    
    global void finish(Database.BatchableContext bc){
        if(!System.Test.isRunningTest())
            database.executeBatch(new Batch_TeamInstanceMapping(), 2000);
    }
}