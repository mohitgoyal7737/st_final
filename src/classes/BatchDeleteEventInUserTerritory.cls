global with sharing class BatchDeleteEventInUserTerritory implements Database.Batchable<sObject>, Database.Stateful {
    public String query;

    global BatchDeleteEventInUserTerritory() {
        this.query = query;
        query = 'select id,User__c,Territory__c,Event__c from User_Territory__c';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<User_Territory__c> scope) {

        SnTDMLSecurityUtil.printDebugMessage('=======Query::::::::::' +scope);
        SnTDMLSecurityUtil.printDebugMessage('=======Query size::::::::::' +scope.size());
        
        for(User_Territory__c userRec : scope)
        {
            userRec.Event__c = 'Delete';
        }

        //update scope;
        SnTDMLSecurityUtil.updateRecords(scope, 'BatchDeleteEventInUserTerritory');
        
    }

    global void finish(Database.BatchableContext BC) {

        BatchUserTerritoryFlow userTerr = new BatchUserTerritoryFlow();
        Database.executeBatch(userTerr,2000);
    }
}