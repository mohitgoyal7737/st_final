/*Author - Himanshu Tariyal(A0994)
Date : 24th January 2018
Description : ScheduleStagingVeevaSurveyData is a nightly batch that is used to transform Veeva data from Staging_Veeva_Cust_Data__c object
              to Staging_Cust_Survey_Profiling__c object every day(whether whole data load or Delta changes)*/
global with sharing class ScheduleStagingVeevaSurveyData implements Schedulable
{
    global void execute(SchedulableContext sc)
    {
        StagingVeevaSurveyData svsd = new StagingVeevaSurveyData(true);
        Database.executeBatch(svsd, 1000);
    }
}