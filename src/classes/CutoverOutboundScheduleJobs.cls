public class CutoverOutboundScheduleJobs {

    //List<Veeva_Job_Scheduling__c> allCountries = new List<Veeva_Job_Scheduling__c>();
    public static String CountryList = null;
     public static String CountryCodeList = null;

    public CutoverOutboundScheduleJobs(){

    }

    public static void CountryWiseOutboundJobs(){
        /*
        List<Veeva_Job_Scheduling__c> SfeList = new List<Veeva_Job_Scheduling__c>();

        for(Veeva_Job_Scheduling__c vcs : [select Country_Name__c,Country__r.AxtriaSalesIQTM__Country_Code__c from Veeva_Job_Scheduling__c where SFE_Load__c = 'Full Load'])
        {
            if(CountryList == null)
            {
                CountryList = vcs.Country_Name__c;
                CountryCodeList = vcs.Country__r.AxtriaSalesIQTM__Country_Code__c;
            }

            else
            {
                CountryList = CountryList+','+vcs.Country_Name__c;
                CountryCodeList = CountryCodeList+','+vcs.Country__r.AxtriaSalesIQTM__Country_Code__c;
            }
        }
    
        System.debug('>>>>>>>>>>>>>>>CountryWiseOutboundJobs method Called<<<<<<<<<<<<<<');
        System.debug('>>>>>>>>>>>>>>>CountryList in method<<<<<<<<<<<<<<'+CountryList);
        System.debug('>>>>>>>>>>>>>>>CountryCodeList in method<<<<<<<<<<<<<<'+CountryCodeList);


        if(CountryList != Null){
            
            database.executebatch(new BatchOutboundWorkspace(CountryList));
            database.executebatch(new BatchOutboundUpdTeamInstance(CountryList));
            database.executebatch(new BatchOutboundPosition(CountryList));
            database.executebatch(new BatchOutboundEmployee(CountryList));
            database.executebatch(new BatchOutboundPosEmp(CountryList));
            database.executebatch(new BatchOutBoundCustomerSegment(CountryList),2000);
            database.executebatch(new BatchOutBounUpdPositionAccount(CountryList,'any string'),2000);
            database.executebatch(new BatchoutBoundPositionProduct2(CountryList));
            database.executebatch(new BatchOutboundAllLevelPositionGeography(CountryList));
            database.executebatch(new BatchCreateGeography_Outbound (CountryCodeList));

    

        }
        */

    }

}