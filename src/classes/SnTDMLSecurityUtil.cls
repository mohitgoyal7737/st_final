//SnTDMLSecurityUtil -- for SnT Package all methods and class with public access
global with sharing class SnTDMLSecurityUtil
{

    global static void printDebugMessage(String clsName, String methodName, String strMsg, boolean isEnabled)
    {
        boolean isLogEnabled = true; //customsetting.logActivated
        if(isEnabled || isLogEnabled)
        {
            String msg = '';
            msg += (clsName == null ? '' : ' Class :' + clsName + ' | ');
            msg += (methodName == null ? '' : ' methodName :' + methodName + ' | ');
            msg += 'Message : ' + strMsg;
            system.debug(msg);
        }
    }
    global static void printDebugMessage(String clsName, String strMsg, boolean isEnabled)
    {
        printDebugMessage(clsName, null, strMsg, isEnabled);
    }
    global static void printDebugMessage(String strMsg, boolean isEnabled)
    {
        printDebugMessage(null, null, strMsg, isEnabled);
    }
    global static void printDebugMessage(String clsName, String methodName, String strMsg)
    {
        printDebugMessage(clsName, methodName, strMsg, false);
    }
    global static void printDebugMessage(String clsName, String strMsg)
    {
        printDebugMessage(clsName, null, strMsg, false);
    }
    global static void printDebugMessage(String strMsg)
    {
        printDebugMessage(null, null, strMsg, false);
    }

    global static void printDMLMessage(SObjectAccessDecision securityDecision, String clsName, String operation)
    {
        String errorFields = ' Fields : ' + securityDecision.getRemovedFields();
        String msg = '';

        if(operation == 'C')
            msg = System.Label.Security_Error_Object_Not_Insertable;
        else if(operation == 'R')
            msg = System.Label.Security_Error_Object_Not_Readable;
        else if(operation == 'U')
            msg = System.Label.Security_Error_Object_Not_Updateable;
        else if(operation == 'D')
            msg = System.Label.Security_Error_Object_Not_Deletable;

        msg = 'Failed in ' + clsName + ', ' + String.format( msg, new List<String> {errorFields });
        printDebugMessage(msg);
    }

    global static void insertRecords(List<sObject> sObjList, String clsName)
    {
        SObjectAccessDecision securityDecision = Security.stripInaccessible(AccessType.CREATABLE, sObjList);

        printDebugMessage(clsName, ' in insertRecords ');
        insert securityDecision.getRecords();
        printDebugMessage(clsName, ' in getRemovedFields::: ' + securityDecision.getRemovedFields());

        //throw exception if permission is missing
        if(!securityDecision.getRemovedFields().isEmpty() )
        {
            throw new SnTException(securityDecision, clsName, SnTException.OperationType.C );
        }
        else
        {
            integer ind = 0;
            List<sObject> sObjUpdatedList = securityDecision.getRecords();
            for(sObject obj : sObjList)
            {
                obj.put('Id', sObjUpdatedList[ind].get('Id'));
                //printDebugMessage('security obj--'+sObjUpdatedList[ind]);
                printDebugMessage('obj--' + obj.id);
                ind++;

            }
        }
    }

    global static void insertRecords(sObject sObj, String clsName)
    {
        insertRecords(new List<sObject> {sObj}, clsName);
    }

    global static void upsertRecords(List<sObject> sObjList, String clsName)
    {
        SObjectAccessDecision securityDecision = Security.stripInaccessible(AccessType.UPSERTABLE, sObjList);
        upsert securityDecision.getRecords();

        //throw exception if permission is missing
        if(!securityDecision.getRemovedFields().isEmpty() )
        {
            if(sObjList[0].id != null)
                throw new SnTException(securityDecision, clsName, SnTException.OperationType.C );
            else
                throw new SnTException(securityDecision, clsName, SnTException.OperationType.U );
        }
        else
        {
            integer ind = 0;
            List<sObject> sObjUpdatedList = securityDecision.getRecords();
            for(sObject obj : sObjList)
            {
                if(obj.get('Id') == null)
                {
                    obj.put('Id', sObjUpdatedList[ind].get('Id'));
                    ind++;
                }
            }
        }
    }

    global static void upsertRecords(sObject sObj, String clsName)
    {
        List<sObject> sObjList = new List<sObject> {sObj};
        if(sObj.Id == null)
            insertRecords(sObjList, clsName);
        else
            updateRecords(sObjList, clsName);

    }

    global static void queryRecords(List<sObject> sObjList, String clsName)
    {
        SObjectAccessDecision securityDecision = Security.stripInaccessible(AccessType.READABLE, sObjList);
        if(!securityDecision.getRemovedFields().isEmpty() )
        {
            throw new SnTException(securityDecision, clsName, SnTException.OperationType.R);
        }
    }

    global static void queryRecords(sObject sObj, String clsName)
    {
        queryRecords(new List<sObject> {sObj}, clsName);
    }

    global static void updateRecords(List<sObject> sObjList, String clsName)
    {

        printDebugMessage(sObjList + '');

        SObjectAccessDecision securityDecision = Security.stripInaccessible(AccessType.UPDATABLE, sObjList);
        printDebugMessage(securityDecision.getRecords() + '');

        update securityDecision.getRecords();

        //throw exception if permission is missing
        if(!securityDecision.getRemovedFields().isEmpty() )
        {
            //throw new SnTException(securityDecision, clsName, SnTException.OperationType.U );
            printDMLMessage(securityDecision, clsName, 'U');

            // String errorFields = ' Fields : '+securityDecision.getRemovedFields();
            // String msg = System.Label.Security_Error_Object_Not_Updateable;
            // msg = 'Failed in '+clsName+', '+String.format( msg, new List<String>{errorFields });
            // printDebugMessage(msg);
        }
    }

    global static void updateRecords(sObject sObj, String clsName)
    {
        updateRecords(new List<sObject> {sObj}, clsName);
    }


    global static void deleteRecords(List<sObject> sObjList, String clsName)
    {
        if(!sObjList.isEmpty()){
            // Get the sObject token from the first ID
            // (the List contains IDs of sObjects of the same type).
            Schema.SObjectType token = sObjList[0].getSObjectType();
            // Using the token, do a describe
            // and construct a query dynamically.
            Schema.DescribeSObjectResult objectDescribe = token.getDescribe();

            if (objectDescribe.isDeletable()) {
                delete sObjList;
            }
            else{
                printDebugMessage('Failed in '+clsName+', '+ System.Label.Security_Error_Object_Not_Deletable + objectDescribe);
            }
        }

    }

    global static void deleteRecords(sObject sObj, String clsName)
    {
        deleteRecords(new List<sObject> {sObj}, clsName);
    }

    /*

    global static Database.SaveResult[] insertDatabase(List<sObject> sObjList, String clsName, boolean allOrNone){
        SObjectAccessDecision securityDecision = Security.stripInaccessible(AccessType.CREATABLE,sObjList);

        printDebugMessage(clsName,' in insertDatabase '); 
        Database.SaveResult[] result = Database.insert( securityDecision.getRecords(),allOrNone);
        printDebugMessage(clsName,' in getRemovedFields::: '+securityDecision.getRemovedFields()); 
        
        //throw exception if permission is missing 
        if(!securityDecision.getRemovedFields().isEmpty() ){
           throw new SnTException(securityDecision, clsName, SnTException.OperationType.R);
       }
       else{
        integer ind =0;
        List<sObject> sObjUpdatedList = securityDecision.getRecords();
        for(sObject obj :sObjList){
            obj.put('Id', sObjUpdatedList[ind].get('Id'));
            printDebugMessage('obj--'+obj.id);
            ind++;
        }
    }
    return result;
}
global static Database.SaveResult insertDatabase(sObject sObj, String clsName, boolean allOrNone){
    return insertDatabase(new List<sObject>{sObj},clsName,allOrNone)[0];
}

global static Database.SaveResult[] updateDatabase(List<sObject> sObjList, String clsName, boolean allOrNone){
    SObjectAccessDecision securityDecision = Security.stripInaccessible(AccessType.UPDATABLE,sObjList);

    printDebugMessage(clsName,' in updateDatabase'); 
    Database.SaveResult[] result = Database.update( securityDecision.getRecords(),allOrNone);
    system.debug(securityDecision.getRecords());
    
    update securityDecision.getRecords();
    system.debug(securityDecision.getRemovedFields() );

        //throw exception if permission is missing 
    if(!securityDecision.getRemovedFields().isEmpty() ){
        throw new SnTException(securityDecision, clsName, SnTException.OperationType.U);
    }
    return result;
}
global static Database.SaveResult updateDatabase(sObject sObj, String clsName, boolean allOrNone){
    return updateDatabase(new List<sObject>{sObj},clsName,allOrNone)[0];
}
*/

}