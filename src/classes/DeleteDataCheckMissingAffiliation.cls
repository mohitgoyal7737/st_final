global class DeleteDataCheckMissingAffiliation implements Database.Batchable<sObject> , Database.Stateful {
    public String query;
    public String teamInsID;
    public Set<String> teamInsName;


    @Deprecated
    global DeleteDataCheckMissingAffiliation(Set<String> teamIns) {
    }

    global DeleteDataCheckMissingAffiliation(String teamIns) {
        this.query = query;
        teamInsID='';
        teamInsName=new Set<string>();
        teamInsID=teamIns;
        List<AxtriaSalesIQTM__Team_Instance__c> teamInsList = [Select Name from AxtriaSalesIQTM__Team_Instance__c where id = :teamInsID];
        for(AxtriaSalesIQTM__Team_Instance__c ti : teamInsList)
        {
            teamInsName.add(ti.Name);
        }
        query='select Id from Check_Missing_Affiliation__c where Team_Instance__c in :teamInsName';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Check_Missing_Affiliation__c> scope) {

        delete scope;
        
    }

    global void finish(Database.BatchableContext BC) {

        System.debug('==========Batch Calling===========');

        BatchCheckOrphanHCP obj = new BatchCheckOrphanHCP(teamInsID);
        Database.executeBatch(obj,2000);

    }
}