global with sharing class BatchCallPlanHandlingforPosAccStatus implements Database.Batchable<sObject> {
    public String query;
    public Map<String,Set<String>> mapAccTIkey2PosSet;
    public Set<String> teamInsSet;
    public Set<String> setInactivePos;
    public Set<String> setInactiveAcc;
    public Map<String,Set<String>> mapInaACC2POSset;

    global BatchCallPlanHandlingforPosAccStatus(Set<String> ti, Map<String,Set<String>> mapInactiveAcc2PosSet,Map<String,Set<String>> mapInactiveAccTIkey2PosSet) {
        query = '';
        mapAccTIkey2PosSet=new Map<String,Set<String>>();
        teamInsSet=new Set<String>();
        setInactivePos=new Set<String>();
        setInactiveAcc=new Set<String>();
        teamInsSet.addAll(ti);
        mapInaACC2POSset = new Map<String,Set<String>>();

        SnTDMLSecurityUtil.printDebugMessage('====Account+Team Ins key to Position Code for Inactive Status====');

        for(String key : mapInactiveAccTIkey2PosSet.keyset())
        {
            if(mapInactiveAccTIkey2PosSet.get(key) != null)
            {
                for(String pos : mapInactiveAccTIkey2PosSet.get(key))
                {
                    if(!mapAccTIkey2PosSet.containsKey(key))
                    {
                        Set<String> tempPos = new Set<string>();
                        tempPos.add(pos);
                        mapAccTIkey2PosSet.put(key,tempPos);
                    }
                    else
                    {
                        mapAccTIkey2PosSet.get(key).add(pos);
                    }
                }
            }  
        }
        
        SnTDMLSecurityUtil.printDebugMessage('====mapAccTIkey2PosSet====' +mapAccTIkey2PosSet);

        for(String acc : mapInactiveAcc2PosSet.keySet())
        {
            setInactiveAcc.add(acc);
            if(mapInactiveAcc2PosSet.get(acc) != null)
            {
                for(String inactivePos : mapInactiveAcc2PosSet.get(acc))
                {
                    setInactivePos.add(inactivePos);
                
                    if(!mapInaACC2POSset.containsKey(acc))
                    {
                        Set<String> tempPos = new Set<string>();
                        tempPos.add(inactivePos);
                        mapInaACC2POSset.put(acc,tempPos);
                    }
                    else
                    {
                        mapInaACC2POSset.get(acc).add(inactivePos);
                    }
                }
            } 
        }

        SnTDMLSecurityUtil.printDebugMessage('====mapInaACC2POSset====' +mapInaACC2POSset);
        SnTDMLSecurityUtil.printDebugMessage('====setInactivePos====' +setInactivePos);
        //removed Reference to BTI in below query
        query='select id, AxtriaSalesIQTM__Account__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__isincludedCallPlan__c, AxtriaSalesIQTM__isTarget__c, AxtriaSalesIQTM__isTarget_Approved__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Team_Instance__c in :teamInsSet and AxtriaSalesIQTM__Account__c != null and AxtriaSalesIQTM__Position__c != null and AxtriaSalesIQTM__Team_Instance__c!=null and AxtriaSalesIQTM__Account__r.AccountNumber in :setInactiveAcc and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c in :setInactivePos';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scope) {

        SnTDMLSecurityUtil.printDebugMessage('====Call Plan Handling for Position Accounts====' +scope);

        List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> inactiveCallPlanList = new List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();

        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c callPlanRec : scope)
        {
            String key = callPlanRec.AxtriaSalesIQTM__Account__r.AccountNumber + '_' + callPlanRec.AxtriaSalesIQTM__Team_Instance__r.Name;
            SnTDMLSecurityUtil.printDebugMessage('====key====' +key);
            if(mapAccTIkey2PosSet.containsKey(key))
            {
                Set<String> inactivePosSet = new Set<String>();
                inactivePosSet=mapAccTIkey2PosSet.get(key);
                SnTDMLSecurityUtil.printDebugMessage('====inactivePosSet====' +inactivePosSet);
                if(inactivePosSet.contains(callPlanRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c))
                {
                    SnTDMLSecurityUtil.printDebugMessage('Call plan handling for inactive Account');
                    callPlanRec.AxtriaSalesIQTM__isincludedCallPlan__c = false;
                    callPlanRec.AxtriaSalesIQTM__lastApprovedTarget__c = false;
                    
                    inactiveCallPlanList.add(callPlanRec);
                }
            }
         }

         SnTDMLSecurityUtil.printDebugMessage('====inactiveCallPlanList.size()====' +inactiveCallPlanList.size());
         SnTDMLSecurityUtil.printDebugMessage('====inactiveCallPlanList====' +inactiveCallPlanList);
         if(inactiveCallPlanList.size() > 0){
            //update inactiveCallPlanList;
            SnTDMLSecurityUtil.updateRecords(inactiveCallPlanList, 'BatchCallPlanHandlingforPosAccStatus');
        }
        
    }

    global void finish(Database.BatchableContext BC) {

    }
}