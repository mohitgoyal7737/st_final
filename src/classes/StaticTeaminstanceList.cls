public with sharing class StaticTeaminstanceList
{
    public static List<String> filldata()
    {
        List<String>teaminstancelist = new List<String>();
        Set<String>teaminstanceset = new Set<String>();
        List<AxtriaSalesIQTM__Team_Instance__c>teamlist = new List<AxtriaSalesIQTM__Team_Instance__c>();
        teamlist = [Select id, Name  from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c = 'Current' and (AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Live' or AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Published') WITH SECURITY_ENFORCED];

        if(teamlist != null && teamlist.size() > 0)
        {
            for(AxtriaSalesIQTM__Team_Instance__c ti : teamlist)
            {
                teaminstanceset.add(ti.id);
            }
            teaminstancelist.addall(teaminstanceset);
        }

        return teaminstancelist;

    }

    public static List<String> filldataCallPlan()
    {
        List<String>teaminstancelist = new List<String>();
        Set<String>teaminstanceset = new Set<String>();
        List<AxtriaSalesIQTM__Team_Instance__c>teamlist = new List<AxtriaSalesIQTM__Team_Instance__c>();
        teamlist = [Select id, Name  from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c = 'Current' and (AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Live' or AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Published') and AxtriaSalesIQTM__Team__r.hasCallPlan__c = true WITH SECURITY_ENFORCED];

        if(teamlist != null && teamlist.size() > 0)
        {
            for(AxtriaSalesIQTM__Team_Instance__c ti : teamlist)
            {
                teaminstanceset.add(ti.id);
            }
            teaminstancelist.addall(teaminstanceset);
        }

        return teaminstancelist;

    }

    public static List<String> getAllCountries()
    {
        List<String>countryList = new List<String>();
        Set<String>countrySet = new Set<String>();

        List<AxtriaSalesIQTM__Team_Instance__c>teamlist = new List<AxtriaSalesIQTM__Team_Instance__c>();
        teamlist = [Select id, Name, AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c  from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c = 'Current' and  (AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Live' or AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Published' ) WITH SECURITY_ENFORCED];

        if(teamlist != null && teamlist.size() > 0)
        {
            for(AxtriaSalesIQTM__Team_Instance__c ti : teamlist)
            {
                countrySet.add(ti.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c);
            }
            countryList.addall(countrySet);
        }

        return countryList;

    }


    public static List<String> getCountries()
    {
        List<String>countryList = new List<String>();
        Set<String>countrySet = new Set<String>();
        Set<String> veevaCountrySet = new Set<String>();

        for( Veeva_Market_Specific__c  veevaMarket : [select id, market__c, Add_Alignment_in_TSF__c FROM Veeva_Market_Specific__c where Add_Alignment_in_TSF__c = true WITH SECURITY_ENFORCED])
        {
            veevaCountrySet.add(veevaMarket.market__c);
        }


        List<AxtriaSalesIQTM__Team_Instance__c>teamlist = new List<AxtriaSalesIQTM__Team_Instance__c>();
        teamlist = [Select id, Name, AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c  from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c = 'Current' and Country_Name__c in: veevaCountrySet and  (AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Live' or AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Published' ) WITH SECURITY_ENFORCED];

        if(teamlist != null && teamlist.size() > 0)
        {
            for(AxtriaSalesIQTM__Team_Instance__c ti : teamlist)
            {
                countrySet.add(ti.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c);
            }
            countryList.addall(countrySet);
        }

        return countryList;

    }

    public static List<String> getDeltaCountries()
    {
        /*List<Veeva_Job_Scheduling__c> allCountries = [select id, Country__c from Veeva_Job_Scheduling__c where Load_Type__c = 'Delta'];

        List<String> allCountriesList = new List<String>();

        for(Veeva_Job_Scheduling__c vcs : allCountries)
        {
            allCountriesList.add(vcs.Country__c);
        }*/
        /*Replaced Veeva_Job_Scheduling__c with field at country*/
        List<AxtriaSalesIQTM__Country__c> allCountries = [select id from AxtriaSalesIQTM__Country__c where Load_Type__c = 'Delta' WITH SECURITY_ENFORCED];

        List<String> allCountriesList = new List<String>();

        for(AxtriaSalesIQTM__Country__c vcs : allCountries)
        {
            allCountriesList.add(vcs.id);
        }

        return allCountriesList;
    }

    //New added class by Ayushi

    public static Set<String> getCountriesSet()
    {
        /* List<Veeva_Job_Scheduling__c> allCountries =[select id, Country__c from Veeva_Job_Scheduling__c where Load_Type__c = 'Delta' or Load_Type__c = 'Full Load'];

         Set<String> allCountriesList = new Set<String>();

         for(Veeva_Job_Scheduling__c vcs : allCountries)
         {
             allCountriesList.add(vcs.Country__c);
         }*/
        List<AxtriaSalesIQTM__Country__c> allCountries = [select id from AxtriaSalesIQTM__Country__c where Load_Type__c = 'Delta' or Load_Type__c = 'Full Load' WITH SECURITY_ENFORCED];

        Set<String> allCountriesList = new Set<String>();

        for(AxtriaSalesIQTM__Country__c vcs : allCountries)
        {
            allCountriesList.add(vcs.ID);
        }

        return allCountriesList;
    }

    //till here................

    public static List<String> getFulloadCountries()
    {
        /*List<Veeva_Job_Scheduling__c> allCountries = [select id, Country__c from Veeva_Job_Scheduling__c where Load_Type__c = 'Full Load'];

        List<String> allCountriesList = new List<String>();

        for(Veeva_Job_Scheduling__c vcs : allCountries)
        {
            allCountriesList.add(vcs.Country__c);
        }*/
        List<AxtriaSalesIQTM__Country__c> allCountries = [select id from AxtriaSalesIQTM__Country__c where Load_Type__c = 'Full Load' WITH SECURITY_ENFORCED];

        List<String> allCountriesList = new List<String>();

        for(AxtriaSalesIQTM__Country__c vcs : allCountries)
        {
            allCountriesList.add(vcs.ID);
        }

        return allCountriesList;
    }

    public static List<String> getDeltaTeamInstances()
    {
        List<String> allTeamInstances = new List<String>();
        List<String> countriesList = new List<String>();
        countriesList = getDeltaCountries();

        if(countriesList.size() > 0)
        {
            for(AxtriaSalesIQTM__Team_Instance__c ti : [select id from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c in :countriesList and AxtriaSalesIQTM__Alignment_Period__c = 'Current' and AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Live' WITH SECURITY_ENFORCED])
            {
                allTeamInstances.add(ti.ID);
            }
        }

        return allTeamInstances;
    }

    public static List<String> getFullLoadTeamInstances()
    {
        List<String> allTeamInstances = new List<String>();
        List<String> countriesList = new List<String>();
        countriesList = getFulloadCountries();

        if(countriesList.size() > 0)
        {
            for(AxtriaSalesIQTM__Team_Instance__c ti : [select id from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c in :countriesList and AxtriaSalesIQTM__Alignment_Period__c = 'Current' and AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Live' WITH SECURITY_ENFORCED])
            {
                allTeamInstances.add(ti.ID);
            }
        }

        return allTeamInstances;
    }

    public static List<String> getDeltaTeamInstancesCallPlan()
    {
        List<String> allTeamInstances = new List<String>();
        List<String> countriesList = new List<String>();
        countriesList = getDeltaCountries();

        if(countriesList.size() > 0)
        {
            for(AxtriaSalesIQTM__Team_Instance__c ti : [select id from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c in :countriesList and AxtriaSalesIQTM__Alignment_Period__c = 'Current' and AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Live' and AxtriaSalesIQTM__Team__r.hasCallPlan__c = true])
            {
                allTeamInstances.add(ti.ID);
            }
        }

        return allTeamInstances;
    }

    public static List<String> getFullLoadTeamInstancesCallPlan()
    {
        List<String> allTeamInstances = new List<String>();
        List<String> countriesList = new List<String>();
        countriesList = getFulloadCountries();

        if(countriesList.size() > 0)
        {
            for(AxtriaSalesIQTM__Team_Instance__c ti : [select id from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c in :countriesList and AxtriaSalesIQTM__Alignment_Period__c = 'Current' and AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Live' and AxtriaSalesIQTM__Team__r.hasCallPlan__c = true WITH SECURITY_ENFORCED])
            {
                allTeamInstances.add(ti.ID);
            }
        }

        return allTeamInstances;
    }

    public static Set<String> getCompleteRuleTeamInstances()
    {
        Set<String> teaminstancelist = new Set<String>();
        List<Measure_Master__c> mm = [Select id, State__c, Team_Instance__c from Measure_Master__c where State__c = 'Complete' and Rule_Type__c != 'MCCP' WITH SECURITY_ENFORCED];
        if(mm != null && mm.size() > 0)
        {
            for(Measure_Master__c measure : mm)
            {
                teaminstancelist.add(measure.Team_Instance__c);
            }
        }
        return teaminstancelist;
    }

    public static List<String> getSFEDeltaCountries()
    {
        List<String> DeltaCountry = new List<String>();

        /*for(Veeva_Job_Scheduling__c vjs : [select Country_Name__c from Veeva_Job_Scheduling__c where SFE_Load__c = 'Delta Load']){
            DeltaCountry.add(vjs.Country_Name__c);
        }*/
        for(AxtriaSalesIQTM__Country__c vjs : [select Name from AxtriaSalesIQTM__Country__c where SFE_Load__c = 'Delta Load' WITH SECURITY_ENFORCED])
        {
            DeltaCountry.add(vjs.Name);
        }

        return DeltaCountry;
    }
}