/*
@author - Prince Richard Augustin (A2661)
@description - Test class to test Position_Account_Direct_Load class.
*/

@isTest
private class Position_Account_Direct_Load_Test { 
    static testMethod void testMethod44() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        insert scen;
        AxtriaSalesIQTM__Team_Instance__c teamins = testDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        insert teamins;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        AxtriaSalesIQTM__Employee__c emp = TestDataFactory.createEmployee('firstName', 'lastName');
        insert emp;
        AxtriaSalesIQTM__Position__c pos1= TestDataFactory.createPosition(team,teamins);
        pos1.AxtriaSalesIQTM__Hierarchy_Level__c = '5';
        insert pos1;
        AxtriaSalesIQTM__User_Access_Permission__c u = TestDataFactory.createUserAccessPerm(pos1, teamins, UserInfo.getUserId());
        u.AxtriaSalesIQTM__Position__c=pos1.id;
        u.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        u.AxtriaSalesIQTM__User__c = loggedInUser.id;  
        u.AxtriaSalesIQTM__Is_Active__c = True;
        insert u;
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__Hierarchy_Level__c = '4';
        pos.AxtriaSalesIQTM__Parent_Position__c = pos1.id;
        pos.Call_Plan_Status__c ='test';
        pos.AxtriaSalesIQTM__Employee__c = emp.id;
        
        insert pos;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        insert positionAccountCallPlan;
        
        
        
        List<AxtriaSalesIQTM__Position__c> a = new List<AxtriaSalesIQTM__Position__c>();
        a.add(pos);
        AxtriaSalesIQTM__Affiliation_Network__c affnet = TestDataFactory.createAffliNet(countr);
        insert affnet;
        AxtriaSalesIQTM__Account_Affiliation__c accaff = TestDataFactory.createAcctAffli(acc,affnet);
        accaff.Account_Number__c = 'BH10461999';
        
        accaff.AxtriaSalesIQTM__Account__c = acc.Id;
        accaff.Affiliation_Status__c ='Active';
        accaff.AxtriaSalesIQTM__Is_Primary__c = true;
        accaff.AxtriaSalesIQTM__Active__c = true;
        insert accaff;
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            SalesIQUtility.setCookieString('CountryID', countr.id);
            ApexPages.currentPage().getParameters().put('teaminstance',teamins.Id);
            ApexPages.currentPage().getParameters().put('selectedCycle',teamins.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Name);
            ApexPages.currentPage().getParameters().put('countryID',countr.Id);
            Position_Account_Direct_Load obj=new Position_Account_Direct_Load();
            obj.scenarioChanged();
            obj.changeTerritory();
            obj.refreshData();
            obj.removemsgs();
            obj.selectedPosition = pos1.id;
            obj.territoryTableChange();
            obj.hidePopup();
            Set<String> accId = new Set<String>();
            accId.add(acc.id);
            
            obj.fetchchildAccountAffiliation(accId, 4);
            //obj.createpositionaccount();
            

        }
        Test.stopTest();
    }
    static testMethod void testMethod42() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        insert scen;
        AxtriaSalesIQTM__Team_Instance__c teamins = testDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        insert teamins;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        AxtriaSalesIQTM__Employee__c emp = TestDataFactory.createEmployee('firstName', 'lastName');
        insert emp;
        AxtriaSalesIQTM__Position__c pos1= TestDataFactory.createPosition(team,teamins);
        pos1.AxtriaSalesIQTM__Hierarchy_Level__c = '2';
        insert pos1;
        AxtriaSalesIQTM__User_Access_Permission__c u = TestDataFactory.createUserAccessPerm(pos1, teamins, UserInfo.getUserId());
        u.AxtriaSalesIQTM__Position__c=pos1.id;
        u.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        u.AxtriaSalesIQTM__User__c = loggedInUser.id;  
        u.AxtriaSalesIQTM__Is_Active__c = True;
        insert u;
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__Hierarchy_Level__c = '2';
        pos.AxtriaSalesIQTM__Parent_Position__c = pos1.id;
        pos.Call_Plan_Status__c ='test';
        pos.AxtriaSalesIQTM__Employee__c = emp.id;
        
        insert pos;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        insert positionAccountCallPlan;
        
        
        
        List<AxtriaSalesIQTM__Position__c> a = new List<AxtriaSalesIQTM__Position__c>();
        a.add(pos);
        AxtriaSalesIQTM__Affiliation_Network__c affnet = TestDataFactory.createAffliNet(countr);
        insert affnet;
        AxtriaSalesIQTM__Account_Affiliation__c accaff = TestDataFactory.createAcctAffli(acc,affnet);
        accaff.Account_Number__c = 'BH10461999';
        
        accaff.AxtriaSalesIQTM__Account__c = acc.Id;
        accaff.Affiliation_Status__c ='Active';
        accaff.AxtriaSalesIQTM__Is_Primary__c = true;
        accaff.AxtriaSalesIQTM__Active__c = true;
        insert accaff;
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            SalesIQUtility.setCookieString('CountryID', countr.id);
            ApexPages.currentPage().getParameters().put('teaminstance',teamins.Id);
            ApexPages.currentPage().getParameters().put('selectedCycle',teamins.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Name);
            ApexPages.currentPage().getParameters().put('countryID',countr.Id);
            Position_Account_Direct_Load obj=new Position_Account_Direct_Load();
            obj.scenarioChanged();
            obj.changeTerritory();
            obj.refreshData();
            obj.removemsgs();
            obj.selectedPosition = pos1.id;
            obj.territoryTableChange();
            obj.hidePopup();
            Set<String> accId = new Set<String>();
            accId.add(acc.id);
            
            obj.fetchchildAccountAffiliation(accId, 4);
            //obj.createpositionaccount();
            

        }
        Test.stopTest();
    }
    static testMethod void testMethod1() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        insert scen;
        AxtriaSalesIQTM__Team_Instance__c teamins = testDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        insert teamins;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        AxtriaSalesIQTM__Employee__c emp = TestDataFactory.createEmployee('princ','richie');
        insert emp;
        AxtriaSalesIQTM__Position__c pos1= TestDataFactory.createPosition(team,teamins);
        insert pos1;
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__Hierarchy_Level__c = '1';
        pos.AxtriaSalesIQTM__Parent_Position__c = pos1.id;
        insert pos;
        
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        insert positionAccountCallPlan;
        
        
        AxtriaSalesIQTM__User_Access_Permission__c u = TestDataFactory.createUserAccessPerm(pos, teamins, UserInfo.getUserId());
        u.AxtriaSalesIQTM__Position__c=pos.id;
        u.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        u.AxtriaSalesIQTM__User__c = loggedInUser.id;  
        u.AxtriaSalesIQTM__Is_Active__c = True;
        insert u;
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            SalesIQUtility.setCookieString('CountryID', countr.id);
            ApexPages.currentPage().getParameters().put('teaminstance',teamins.Id);
            ApexPages.currentPage().getParameters().put('selectedCycle',teamins.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Name);
            ApexPages.currentPage().getParameters().put('countryID',countr.Id);
            Position_Account_Direct_Load obj=new Position_Account_Direct_Load();
            obj.selectedPosition = pos.id;
            obj.selectedteamInstance = teamins.id;
            obj.teamInstanceChanged();
            obj.scenarioChanged();
        }
        Test.stopTest();
    }
    static testMethod void testMethod3() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        insert scen;
        AxtriaSalesIQTM__Team_Instance__c teamins = testDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        insert teamins;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        AxtriaSalesIQTM__Position__c pos1= TestDataFactory.createPosition(team,teamins);
        pos1.AxtriaSalesIQTM__Hierarchy_Level__c = '2';
        insert pos1;
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__Hierarchy_Level__c = '3';
        pos.AxtriaSalesIQTM__Parent_Position__c = pos1.id;
        insert pos;
        
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        insert positionAccountCallPlan;
        
        
        AxtriaSalesIQTM__User_Access_Permission__c u = TestDataFactory.createUserAccessPerm(pos, teamins, UserInfo.getUserId());
        u.AxtriaSalesIQTM__Position__c=pos.id;
        u.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        u.AxtriaSalesIQTM__User__c = loggedInUser.id;  
        u.AxtriaSalesIQTM__Is_Active__c = True;
        insert u;
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            SalesIQUtility.setCookieString('CountryID', countr.id);
            ApexPages.currentPage().getParameters().put('teaminstance',teamins.Id);
            ApexPages.currentPage().getParameters().put('selectedCycle',teamins.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Name);
            ApexPages.currentPage().getParameters().put('countryID',countr.Id);
            Position_Account_Direct_Load obj=new Position_Account_Direct_Load();
            obj.selectedPosition = pos.id;
            obj.selectedteamInstance = teamins.id;
            obj.teamInstanceChanged();
            obj.scenarioChanged();
        }
        Test.stopTest();
    }
   /*static testMethod void testMethod2() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        insert scen;
        AxtriaSalesIQTM__Team_Instance__c teamins = testDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        insert teamins;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        AxtriaSalesIQTM__Position__c pos1= TestDataFactory.createPosition(team,teamins);
        insert pos1;
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__Hierarchy_Level__c = '2';
        pos.AxtriaSalesIQTM__Parent_Position__c = pos1.id;
        insert pos;
        
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        insert positionAccountCallPlan;
        
        
        AxtriaSalesIQTM__User_Access_Permission__c u = TestDataFactory.createUserAccessPerm(pos, teamins, UserInfo.getUserId());
        u.AxtriaSalesIQTM__Position__c=pos.id;
        u.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        u.AxtriaSalesIQTM__User__c = loggedInUser.id;  
        u.AxtriaSalesIQTM__Is_Active__c = True;
        insert u;
        
        Test.startTest();
        System.runAs(loggedInUser){
ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            SalesIQUtility.setCookieString('CountryID', countr.id);
            ApexPages.currentPage().getParameters().put('teaminstance',teamins.Id);
            ApexPages.currentPage().getParameters().put('selectedCycle',teamins.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Name);
            ApexPages.currentPage().getParameters().put('countryID',countr.Id);
            Position_Account_Direct_Load obj=new Position_Account_Direct_Load();
            obj.selectedPosition = pos.id;
            obj.selectedteamInstance = teamins.id;
            obj.teamInstanceChanged();
            obj.scenarioChanged();
        }
        Test.stopTest();
    }*/

    static testMethod void testMethod4() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        insert scen;
        AxtriaSalesIQTM__Team_Instance__c teamins = testDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        insert teamins;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        AxtriaSalesIQTM__Position__c pos1= TestDataFactory.createPosition(team,teamins);
        pos1.AxtriaSalesIQTM__Hierarchy_Level__c = '3';
        insert pos1;
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__Hierarchy_Level__c = '4';
        pos.AxtriaSalesIQTM__Parent_Position__c = pos1.id;
        
        insert pos;
        
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        insert positionAccountCallPlan;
        
        
        AxtriaSalesIQTM__User_Access_Permission__c u = TestDataFactory.createUserAccessPerm(pos, teamins, UserInfo.getUserId());
        u.AxtriaSalesIQTM__Position__c=pos.id;
        u.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        u.AxtriaSalesIQTM__User__c = loggedInUser.id;  
        u.AxtriaSalesIQTM__Is_Active__c = True;
        insert u;
        List<AxtriaSalesIQTM__Position__c> a = new List<AxtriaSalesIQTM__Position__c>();
        a.add(pos);
        AxtriaSalesIQTM__Affiliation_Network__c affnet = TestDataFactory.createAffliNet(countr);
        insert affnet;
        AxtriaSalesIQTM__Account_Affiliation__c accaff = TestDataFactory.createAcctAffli(acc,affnet);
        accaff.Account_Number__c = 'BH10461999';
        
        accaff.AxtriaSalesIQTM__Account__c = acc.Id;
        accaff.Affiliation_Status__c ='Active';
        accaff.AxtriaSalesIQTM__Is_Primary__c = true;
        accaff.AxtriaSalesIQTM__Active__c = true;
        insert accaff;
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            SalesIQUtility.setCookieString('CountryID', countr.id);
            ApexPages.currentPage().getParameters().put('teaminstance',teamins.Id);
            ApexPages.currentPage().getParameters().put('selectedCycle',teamins.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Name);
            ApexPages.currentPage().getParameters().put('countryID',countr.Id);
            Position_Account_Direct_Load obj=new Position_Account_Direct_Load();
            obj.scenarioChanged();
            obj.changeTerritory();
            obj.refreshData();
            obj.removemsgs();
            obj.selectedPosition = pos1.id;
            obj.territoryTableChange();
            obj.hidePopup();
            Set<String> accId = new Set<String>();
            accId.add(acc.id);
            
            obj.fetchchildAccountAffiliation(accId, 4);
            //obj.createpositionaccount();
            

        }
        Test.stopTest();
    }
    /*static testMethod void testMethod5() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        insert scen;
        AxtriaSalesIQTM__Team_Instance__c teamins = testDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        insert teamins;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        AxtriaSalesIQTM__Position__c pos1= TestDataFactory.createPosition(team,teamins);
        insert pos1;
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__Hierarchy_Level__c = '5';
        pos.AxtriaSalesIQTM__Parent_Position__c = pos1.id;
        insert pos;
        
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        insert positionAccountCallPlan;
        
        
        AxtriaSalesIQTM__User_Access_Permission__c u = TestDataFactory.createUserAccessPerm(pos, teamins, UserInfo.getUserId());
        u.AxtriaSalesIQTM__Position__c=pos.id;
        u.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        u.AxtriaSalesIQTM__User__c = loggedInUser.id;  
        u.AxtriaSalesIQTM__Is_Active__c = True;
        insert u;
        
        Test.startTest();
        System.runAs(loggedInUser){
ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            SalesIQUtility.setCookieString('CountryID', countr.id);
            ApexPages.currentPage().getParameters().put('teaminstance',teamins.Id);
            ApexPages.currentPage().getParameters().put('selectedCycle',teamins.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Name);
            ApexPages.currentPage().getParameters().put('countryID',countr.Id);
            Position_Account_Direct_Load obj=new Position_Account_Direct_Load();
           
            obj.scenarioChanged();
            obj.changeTerritory();
            obj.refreshData();
            obj.removemsgs();
            obj.createpositionaccount();
            obj.hidePopup();
        }
        Test.stopTest();
    }*/
}