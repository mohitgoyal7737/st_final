/**********************************************************************************************
Author      : Himanshu Tariyal (A0994)
Date        : 30th July'2020
Description : Test class for MCCP_Utility
Revision(s) : v1.0
**********************************************************************************************/
@isTest
public class MCCP_Utility_Test
{
    static testMethod void sendMCCPRuleCallout1() 
    {
    	String className = 'MCCP_Utility_Test';

    	AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    	SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);

        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);

        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins,className);

        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        SnTDMLSecurityUtil.insertRecords(scen,className);

        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        pcc.Product_Code__c='PROD1';
        pcc.Veeva_External_ID__c='PROD1';
        SnTDMLSecurityUtil.insertRecords(pcc,className);

        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        mmc.State__c ='Executed';
        mmc.CallPlan_OutputS3FileUrl__c='https://google.com'; //Added by SM (A3059) for SMCCP-105 
        SnTDMLSecurityUtil.insertRecords(mmc,className);
        
        List<Measure_master__c> mmList= new List<Measure_master__c>(); //Added by SM (A3059) for SMCCP-105 
        mmList.add(mmc); //Added by SM (A3059) for SMCCP-105 

        AxtriaSalesIQTM__ETL_Config__c etlConfig = TestDataFactory.configureETL();
        etlConfig.Name = 'MCCP Rule Execution';
        SnTDMLSecurityUtil.insertRecords(etlConfig,className);

        System.Test.startTest();

        String nameSpace = MCCP_Utility.sntNamespace(className);

        List<String> MEASUREMASTER_READ_FIELD = new List<String>{nameSpace+'Team_Instance__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Measure_Master__c.SObjectType, MEASUREMASTER_READ_FIELD, false));

        MCCP_Utility.sendMCCPRuleCallout(mmc.Id,'MCCP Rule Execution');
        MCCP_Utility.sendMCCPRuleCallout('','MCCP Rule Execution');
        MCCP_Utility.sendMCCPRuleCallout(null,'MCCP Rule Execution');

        System.Test.stopTest();
    }

    static testMethod void sendMCCPRuleCallout2() 
    {
    	String className = 'MCCP_Utility_Test';

    	AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    	SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);

        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);

        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins,className);

        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        SnTDMLSecurityUtil.insertRecords(scen,className);

        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        pcc.Product_Code__c='PROD1';
        pcc.Veeva_External_ID__c='PROD1';
        SnTDMLSecurityUtil.insertRecords(pcc,className);

        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        mmc.State__c ='Executed';
        SnTDMLSecurityUtil.insertRecords(mmc,className);

        System.Test.startTest();

        String nameSpace = MCCP_Utility.sntNamespace(className);

        List<String> MEASUREMASTER_READ_FIELD = new List<String>{nameSpace+'Team_Instance__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Measure_Master__c.SObjectType, MEASUREMASTER_READ_FIELD, false));

        MCCP_Utility.sendMCCPRuleCallout(mmc.Id,'MCCP Rule Execution');

        System.Test.stopTest();
    }

    static testMethod void sendMCCPRuleCallout3() 
    {
        System.Test.startTest();

        String className = 'MCCP_Utility_Test';

        String nameSpace = MCCP_Utility.sntNamespace(className);

        List<String> MEASUREMASTER_READ_FIELD = new List<String>{nameSpace+'Team_Instance__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Measure_Master__c.SObjectType, MEASUREMASTER_READ_FIELD, false));

        MCCP_Utility.sendMCCPRuleCallout('','MCCP Rule Execution');
        
        System.Test.stopTest();
    }

    static testMethod void sendMCCPRuleCallout4() 
    {
        System.Test.startTest();

        String className = 'MCCP_Utility_Test';

        String nameSpace = MCCP_Utility.sntNamespace(className);

        List<String> MEASUREMASTER_READ_FIELD = new List<String>{nameSpace+'Team_Instance__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Measure_Master__c.SObjectType, MEASUREMASTER_READ_FIELD, false));

        MCCP_Utility.sendMCCPRuleCallout('ABCD','MCCP Rule Execution');
        
        System.Test.stopTest();
    }

    static testMethod void sendMCCPRuleCallout5() 
    {
        String className = 'MCCP_Utility_Test';

        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);

        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);

        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins,className);

        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        SnTDMLSecurityUtil.insertRecords(scen,className);

        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        pcc.Product_Code__c='PROD1';
        pcc.Veeva_External_ID__c='PROD1';
        SnTDMLSecurityUtil.insertRecords(pcc,className);

        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        mmc.State__c ='Executed';
        mmc.PDE_Method__c = 'Workload Balanced';
        mmc.PDE_Method_Type__c = 'Single Product Promotion Per Interaction'; 
        mmc.CallPlan_OutputS3FileUrl__c='https://google.com'; //Added by SM (A3059) for SMCCP-105 
        SnTDMLSecurityUtil.insertRecords(mmc,className);
        
        List<Measure_master__c> mmList= new List<Measure_master__c>(); //Added by SM (A3059) for SMCCP-105 
        mmList.add(mmc); //Added by SM (A3059) for SMCCP-105 

        MCCP_CNProd__c rec = new MCCP_CNProd__c();
        rec.Call_Plan__c = mmc.Id;
        rec.CE_Priority1__c = 20;
        rec.RecordTypeId = Schema.SObjectType.MCCP_CNProd__c.getRecordTypeInfosByName().get('Channel').getRecordTypeId();
        SnTDMLSecurityUtil.insertRecords(rec,className);

        AxtriaSalesIQTM__ETL_Config__c etlConfig = TestDataFactory.configureETL();
        etlConfig.Name = 'MCCP Rule Execution';
        SnTDMLSecurityUtil.insertRecords(etlConfig,className);

        AxtriaSalesIQTM__ETL_Config__c etlConfig2 = TestDataFactory.configureETL();
        etlConfig2.Name = 'MCCP Rule Comparison';
        SnTDMLSecurityUtil.insertRecords(etlConfig2,className);

        Team_Instance_Product_AZ__c rec1 = new Team_Instance_Product_AZ__c();
        rec1.Team_Instance__c = teamins.Id;
        SnTDMLSecurityUtil.insertRecords(rec1,className);

        System.Test.startTest();

        String nameSpace = MCCP_Utility.sntNamespace(className);

        List<String> MEASUREMASTER_READ_FIELD = new List<String>{nameSpace+'Team_Instance__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Measure_Master__c.SObjectType, MEASUREMASTER_READ_FIELD, false));

        MCCP_Utility.getMCCPRuleDiagnostics(mmc.Id,'MCCP Rule Execution');
        MCCP_Utility.getMCCPRuleDiagnostics('','MCCP Rule Execution');
        MCCP_Utility.getMCCPRuleDiagnostics(null,'MCCP Rule Execution');
        MCCP_Utility.getMCCPRuleComparison(mmc.Id,'MCCP Rule Comparison',pcc.Product_Code__c);

        MCCP_Utility.checkChEffValuesBR(mmc.Id);
        MCCP_Utility.updateEffectiveDaysInField(mmc.Id);

        System.Test.stopTest();
    }

    static testMethod void sendMCCPRuleCallout6() 
    {
        System.Test.startTest();

        String className = 'MCCP_Utility_Test';

        String nameSpace = MCCP_Utility.sntNamespace(className);

        List<String> MEASUREMASTER_READ_FIELD = new List<String>{nameSpace+'Team_Instance__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Measure_Master__c.SObjectType, MEASUREMASTER_READ_FIELD, false));

        MCCP_Utility.sntNamespace(className);
        MCCP_Utility.alignmentNamespace();
        MCCP_Utility.getDelimByUserLocale();
        MCCP_Utility.getNumDelimByUserLocale();
        MCCP_Utility.getCurrencyByUserLocale();
        MCCP_Utility.getDateFormatByUserLocale();
        MCCP_Utility.getDateTimeFormatByUserLocale();
        MCCP_Utility.getAllLocaleParamsByUserLocale();
        
        System.Test.stopTest();
    }
}