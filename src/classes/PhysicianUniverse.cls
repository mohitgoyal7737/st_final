/**
 * @author - Sahil Mahajan
 *
 * 
 */
 

public with sharing class PhysicianUniverse {

    Set<Id> pos{get;set;}                                   // All unique positions of Rep
    public List<select_wrap> displayList {get;set;}         // List to be displayed on Page
    List<AxtriaSalesIQTM__User_Access_Permission__c> positions {get;set;}    // All the positions of the Rep
    List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> ListUniverseAcc{get;set;}   // Temporary List for all Records
    string sortField {get;set;}                             
    string sortdir {get;set;}
    string soql {get;set;}                  

    List<String> posRead = new List<String>{'Name','AxtriaSalesIQTM__Client_Position_Code__c'};
    List<String> userAccessPermissionRead = new List<String>{'AxtriaSalesIQTM__Position__c','AxtriaSalesIQTM__Team_Instance__c', 'AxtriaSalesIQTM__user__c', 'AxtriaSalesIQTM__Is_Active__c'};
    List<String> accountRead = new List<String>{'ID','AxtriaSalesIQTM__FirstName__c','AxtriaSalesIQTM__LastName__c','AccountNumber','AxtriaSalesIQTM__AccountType__c','AxtriaSalesIQTM__Speciality__c','BillingCity','BillingState','BillingPostalCode','Name'};
    List<String> pacpRead = new List<String>{'AxtriaSalesIQTM__Calls_Approved__c','AxtriaSalesIQTM__Calls_Updated__c','AxtriaSalesIQTM__Calls_Original__c','AxtriaSalesIQTM__Call_Sequence_Approved__c','AxtriaSalesIQTM__Segment1__c','AxtriaSalesIQTM__Segment2__c','AxtriaSalesIQTM__ReasonAdd__c','AxtriaSalesIQTM__ReasonDrop__c','AxtriaSalesIQTM__Comments__c','AxtriaSalesIQTM__isModified__c','AxtriaSalesIQTM__Change_Status__c','AxtriaSalesIQTM__Position__c','AxtriaSalesIQTM__Team_Instance__c','AxtriaSalesIQTM__isIncludedCallPlan__c','AxtriaSalesIQTM__isAccountTarget__c', 'AxtriaSalesIQTM__Rank__c','AxtriaSalesIQTM__Account__c', 'AxtriaSalesIQTM__ReasonAdd__c'};
    List<String> pacpUpdate = new List<String>{'AxtriaSalesIQTM__Calls_Approved__c','AxtriaSalesIQTM__Calls_Updated__c','AxtriaSalesIQTM__Calls_Original__c','AxtriaSalesIQTM__Call_Sequence_Approved__c','AxtriaSalesIQTM__Segment1__c','AxtriaSalesIQTM__Segment2__c','AxtriaSalesIQTM__ReasonAdd__c','AxtriaSalesIQTM__ReasonDrop__c','AxtriaSalesIQTM__Comments__c','AxtriaSalesIQTM__isModified__c','AxtriaSalesIQTM__Change_Status__c','AxtriaSalesIQTM__Position__c','AxtriaSalesIQTM__Team_Instance__c','AxtriaSalesIQTM__isIncludedCallPlan__c','AxtriaSalesIQTM__isAccountTarget__c', 'AxtriaSalesIQTM__Rank__c','AxtriaSalesIQTM__Account__c', 'AxtriaSalesIQTM__ReasonAdd__c'};
    
    public PhysicianUniverse()
    {
        //securityReview();
        // Initialising Variables and Lists
        sortField = 'AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__FirstName__c';
        sortdir = 'asc';
        soql = 'select AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__FirstName__c,AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__LastName__c,AxtriaSalesIQTM__Account__r.Name,AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__AccountType__c,AxtriaSalesIQTM__Account__r.BillingCity,AxtriaSalesIQTM__Account__r.BillingState,AxtriaSalesIQTM__Account__r.BillingPostalCode,AxtriaSalesIQTM__ReasonDrop__c,AxtriaSalesIQTM__ReasonAdd__c,AxtriaSalesIQTM__Calls_Approved__c,AxtriaSalesIQTM__Calls_Updated__c,AxtriaSalesIQTM__Call_Sequence_Approved__c,AxtriaSalesIQTM__Comments__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c in :pos and AxtriaSalesIQTM__isIncludedCallPlan__c = False';
        pos = new Set<Id>();
        
        //commented by siva
       // if(SecurityUtil.checkRead(AxtriaSalesIQTM__User_Access_Permission__c.SObjectType, userAccessPermissionRead, false))
        {
             positions = [select AxtriaSalesIQTM__Position__c from AxtriaSalesIQTM__User_Access_Permission__c where AxtriaSalesIQTM__User__c = :Userinfo.getUserId()];                
        }
           
 
        for (AxtriaSalesIQTM__User_Access_Permission__c p1 :positions)
        {
            pos.add(p1.AxtriaSalesIQTM__Position__c);
        }
        
        runquery();         

    }
    
    
    // Wrapper Class for displaying Records 
    public class select_wrap
    {
        
       public boolean selected{get;set;}
       public  AxtriaSalesIQTM__Position_Account_Call_Plan__c rec{get;set;}
       
       public select_wrap(AxtriaSalesIQTM__Position_Account_Call_Plan__c rec)
       {
            selected = false;
            this.rec = rec;
            
       }
    }
    
    
    public pagereference redirectToReasonPage()
    {
        //PageReference
        PageReference pg = new PageReference('/apex/CallPlan3');
        //pg.setRedirect(true);
        return pg;  
    }
    
    List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacp;
    
    
    // Function executed when a physician is to be moved from Universe to Call Plan 
    public void IncludeInCallplan()
    {
        pacp = new List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c sw:reasonPacp)
        {   
            sw.AxtriaSalesIQTM__isIncludedCallPlan__c=True;
            
            if(sw.AxtriaSalesIQTM__isModified__c==True)  // If originally (after last Approved ) the record was in Call Plan
            {
                sw.AxtriaSalesIQTM__isModified__c = False;
                sw.AxtriaSalesIQTM__Change_Status__c='Original'; 
                sw.AxtriaSalesIQTM__ReasonAdd__c = 'None';           
                
            }
            else
            {
                sw.AxtriaSalesIQTM__isModified__c = True;
                sw.AxtriaSalesIQTM__Change_Status__c='Pending for Submission';
            }
            sw.AxtriaSalesIQTM__ReasonDrop__c = 'None';
            pacp.add(sw);
        }
        //commented by siva
       // if(SecurityUtil.checkUpdate(AxtriaSalesIQTM__Position_Account_Call_Plan__c.SObjectType, pacpUpdate, false))
        {
             update pacp;                
        }
        
    }
    
    List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> reasonPacp; 
    
    // Function to get Add Reason of Physician
    public List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> getphyAddDrop()
    {
        reasonPacp = new List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        for(select_wrap sw:displayList)
        {
            if(sw.selected)
            {
                reasonPacp.add(sw.rec);
            }
        }   
        return reasonPacp;
    }
    
    // Function Saves the Add Reason, Adds Physician to Call Plan and then redirect back to Phsyician Universe Page    
    public pagereference saveReason()
    {
        IncludeInCallplan();
        PageReference pg = new PageReference('/apex/CallMax');
        pg.setRedirect(true);
        return pg;
    }
    
    public pagereference cancelReason()
    {
        PageReference pg = new PageReference('/apex/PhysicianUniverse');
        pg.setRedirect(true);
        return pg;
    }
    
    // Function to run SOQL Queries
   public void runquery()
   {
        displayList = new List<select_wrap>();
        ListUniverseAcc = new List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
       //commented by siva
       // if(SecurityUtil.checkRead(AxtriaSalesIQTM__Position_Account_Call_Plan__c.SObjectType, pacpRead, false) 
        //    && SecurityUtil.checkRead(Account.SObjectType, accountRead, false))
        {
        	system.debug('***inrunquery method soql::'+soql);
            ListUniverseAcc = Database.query(soql+' order by '+ String.escapeSingleQuotes(sortField) + ' ' + String.escapeSingleQuotes(sortDir) + ' NULLS LAST');
        }
        system.debug('----Query executed----');
        
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c c1 : ListUniverseAcc)
        {
            displayList.add(new select_wrap(c1));
        }
        system.debug('---displayList size():---'+displayList.size());
        system.debug('---displaylist contains:;'+displayList);
   }
   
   public void toggleSort() 
   {
        sortDir = sortDir.equals('asc') ? 'desc' : 'asc';
        runquery();
   }
   
       

       // Executed when Search is Called
       public void  runSearch()
        {  
           displayList = new List<select_wrap>();
           
           //Fetching parameters from Page  
           String ids = Apexpages.currentPage().getParameters().get('id');
           String firstName = Apexpages.currentPage().getParameters().get('firstname');
           String city= Apexpages.currentPage().getParameters().get('city');       
           String state= Apexpages.currentPage().getParameters().get('state');
           String specialty= Apexpages.currentPage().getParameters().get('specialty');
			system.debug('888888Pos is:'+pos);
           soql = 'select AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__FirstName__c,AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__LastName__c,AxtriaSalesIQTM__Account__r.Name,AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__Speciality__c,AxtriaSalesIQTM__Account__r.BillingCity,AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__AccountType__c,AxtriaSalesIQTM__Account__r.BillingState,AxtriaSalesIQTM__Account__r.BillingPostalCode,AxtriaSalesIQTM__ReasonDrop__c,AxtriaSalesIQTM__ReasonAdd__c,AxtriaSalesIQTM__Calls_Approved__c,AxtriaSalesIQTM__Calls_Updated__c,AxtriaSalesIQTM__Call_Sequence_Approved__c,AxtriaSalesIQTM__Comments__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where  AxtriaSalesIQTM__isIncludedCallPlan__c = False';//AxtriaSalesIQTM__Position__c in :pos and
            
           if (ids != null && !ids.equals(''))
                 soql += ' and AxtriaSalesIQTM__account__r.AccountNumber LIKE \''+String.escapeSingleQuotes(ids)+'%\'  ';
                 
           if (firstname != null && !firstName.equals(''))
                 soql += ' and AxtriaSalesIQTM__account__r.AxtriaSalesIQTM__FirstName__c LIKE \''+String.escapeSingleQuotes(firstName)+'%\'  ';

            if (city!= null && !city.equals(''))
                 soql += ' and AxtriaSalesIQTM__account__r.BillingCity LIKE \''+String.escapeSingleQuotes(city)+'%\'  ';
                 
            if (state!= null && !state.equals(''))
                soql += ' and AxtriaSalesIQTM__account__r.BillingState LIKE \''+String.escapeSingleQuotes(state)+'%\'   ';   
            if (specialty!= null && !specialty.equals(''))
                soql += ' and AxtriaSalesIQTM__account__r.AxtriaSalesIQTM__Speciality__c LIKE \''+String.escapeSingleQuotes(specialty)+'%\'   ';     
			system.debug('**Soql is:'+soql);
           runQuery(); 
    }
        
    public String searchString{get; set;}
    public String city{get; set;}
    public String state{get; set;}
    public String specialty{get; set;}
    public String ids{get; set;}
    public void clearSearch()
    {
        searchString='';
        city='';
        ids='';
        state='';
        specialty='';
        sortField ='AxtriaSalesIQTM__account__r.name';
       runSearch();
    }
}