global class BatchCheckOrphanHCPFilesEmail implements Database.Batchable<sObject>, Database.Stateful  {
    public String query;
    public String results;
    public String results80k;
    public String results160k;
    public String results240k;
    public Integer count;

    public list<ErrorLogEmail__c> emailIds{get;set;}

    global BatchCheckOrphanHCPFilesEmail() {
        query = '';
        count=0;
        query= 'select Id,HCAs__c,HCPs__c,Position_Code__c,Team_Instance__c from Check_Missing_Affiliation__c';
        results = '';
        results80k = '';
        results160k = '';
        results240k = '';
        results = createCSVHeader();
        results80k = createCSVHeader();
        results160k = createCSVHeader();
        results240k = moreData();
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Check_Missing_Affiliation__c> scope) {

        System.debug('######### Query ##############' +scope.size());

        for(Check_Missing_Affiliation__c rec : scope)
        {

          if(count <= 80000)   
            results += createCSVRow(rec);

          else if(count > 80000 && count <= 160000)
            results80k += createCSVRow(rec);

          else if(count > 160000 && count <= 240000)
            results160k += createCSVRow(rec);

        }

        System.debug('########## results #########' +results);       
        
    }

    private String createCSVRow(Check_Missing_Affiliation__c rec){
        String headers = '"';
        headers += (String)rec.get('HCPs__c');
        System.debug('rec.HCPs__c ::::::::::' +rec.HCPs__c);
        headers +=  '","' +(String)rec.get('HCAs__c');
        headers +=  '","' +(String)rec.get('Position_Code__c');
        headers +=  '","' +(String)rec.get('Team_Instance__c');
        headers += '"\n';

        count++;
        return headers;
    }

    private string createCSVHeader(){

        String headers = '';
        headers += '"HCPs"';
        headers += ',"HCAs"';
        headers += ',"Position Code"';
        headers += ',"Team Instance"';
        headers += '\n';
        return headers;

    }

     private string moreData(){

        String message = '';
        message = '"More Data"';
        return message;

    }

    global void finish(Database.BatchableContext BC) {

        List<Messaging.Emailfileattachment> efaList = new List<Messaging.Emailfileattachment>();

        list<string>emaillist = new list<string>();

        System.debug('########## results in finish method #########' +results240k);
        System.debug('########## results size in finish method #########' +count);

        if(count <= 80000)
        { 

            Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
            efa.setFileName('MissingAffiliationFile.csv');
            blob excel = blob.valueOf(results);
            efa.setBody(excel);
            //efa.setContentType();
            efaList.add(efa);
        }
        if(count > 80000 && count <= 160000)
        { 

            Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
            efa.setFileName('MissingAffiliationFile1.csv');
            blob excel = blob.valueOf(results);
            efa.setBody(excel);
            //efa.setContentType();
            efaList.add(efa);

            Messaging.Emailfileattachment efa80k = new Messaging.Emailfileattachment();
            efa80k.setFileName('MissingAffiliationFile2.csv');
            blob excel1 = blob.valueOf(results80k);
            efa80k.setBody(excel1);
            //efa.setContentType();
            efaList.add(efa80k);
        }
        if(count > 160000 && count <= 240000)
        { 

            Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
            efa.setFileName('MissingAffiliationFile1.csv');
            blob excel = blob.valueOf(results);
            efa.setBody(excel);
            //efa.setContentType();
            efaList.add(efa);

            Messaging.Emailfileattachment efa80k = new Messaging.Emailfileattachment();
            efa80k.setFileName('MissingAffiliationFile2.csv');
            blob excel1 = blob.valueOf(results80k);
            efa80k.setBody(excel1);
            //efa.setContentType();
            efaList.add(efa80k);

            Messaging.Emailfileattachment efa160k = new Messaging.Emailfileattachment();
            efa160k.setFileName('MissingAffiliationFile3.csv');
            blob excel2 = blob.valueOf(results160k);
            efa160k.setBody(excel2);
            //efa.setContentType();
            efaList.add(efa160k);
        }

        if(count > 240000)
        { 

            Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
            efa.setFileName('MissingAffiliationFile1.csv');
            blob excel = blob.valueOf(results);
            efa.setBody(excel);
            //efa.setContentType();
            efaList.add(efa);

            Messaging.Emailfileattachment efa80k = new Messaging.Emailfileattachment();
            efa80k.setFileName('MissingAffiliationFile2.csv');
            blob excel1 = blob.valueOf(results80k);
            efa80k.setBody(excel1);
            //efa.setContentType();
            efaList.add(efa80k);

            Messaging.Emailfileattachment efa160k = new Messaging.Emailfileattachment();
            efa160k.setFileName('MissingAffiliationFile3.csv');
            blob excel2 = blob.valueOf(results160k);
            efa160k.setBody(excel2);
            //efa.setContentType();
            efaList.add(efa160k);

            Messaging.Emailfileattachment efa240k = new Messaging.Emailfileattachment();
            efa240k.setFileName('MissingAffiliationFile4.csv');
            blob excel3 = blob.valueOf(results240k);
            efa240k.setBody(excel3);
            //efa.setContentType();
            efaList.add(efa240k);
        }

        //Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
        //blob excel = blob.valueOf(results);
        //system.debug('excel blob Attachment'+excel);
        //attach.setBody(excel);

        //attach.setFileName('MissingAffiliationFile.csv');

        String subject = 'Missing/Orphan HCPs Affiliation Records';
        String body = 'This file is generated by System';

        emailIds = ErrorLogEmail__c.getall().values();
        if(emailIds!=null){
            for(ErrorLogEmail__c LEM:emailIds){
                emaillist.add(LEM.Name);
                system.debug('emaillist::::'+emaillist);
            }
        }

        //String[] address = new String[]{'Ayushi.Jain@Axtria.com'};
        //String []ccAdd=new String[]{'Sahil.Mahajan@Axtria.com'};

        Messaging.SingleEmailMessage emailwithattch = new Messaging.SingleEmailMessage();
        emailwithattch.setSubject(subject);
        emailwithattch.setToaddresses(emaillist);
        emailwithattch.setPlainTextBody(body);
        //emailwithattch.setCcAddresses(ccAdd);

        emailwithattch.setFileAttachments(efaList);

        // Sends the email
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {emailwithattch});
    }
}