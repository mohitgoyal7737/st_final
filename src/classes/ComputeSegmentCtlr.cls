public with sharing class ComputeSegmentCtlr extends BusinessRule implements IBusinessRule{
    public Boolean errorFlag{get;set;} // - Updated for SR-453
    public transient List<SLDSPageMessage> PageMessages{get;set;}
    public String ruleId {get;set;}
    public ComputeSegmentCtlr(){
        init();
        ruleId = ApexPages.currentPage().getParameters().get('rid');
        isAddNew = false;
        errorFlag = false;
        selectedMatrix = '';
        retUrl = '/apex/ComputeSegment?mode=' +mode+'&rid='+ruleId; 
        uiLocation = 'Compute Segment';
        if(!Test.isRunningTest())
        initStep();
    }

        public void saveFinal(String result){
            try{
            errorFlag = false;
            if(step != null && String.isBlank(result)){
                SnTDMLSecurityUtil.printDebugMessage('--selectedCompF2-- ' + selectedCompF2);
                SnTDMLSecurityUtil.printDebugMessage('--selectedCompF1-- ' + selectedCompF1);
                step.UI_Location__c = uiLocation;
                /*
                        Use Case - Error if we dont give the input to the Matrix used in the Business Rule
                        Action - Checking the input value of gridParam1,gridParam2 if it is NONE throwing error message on the VF Page
                        Developer -J Siva Gopi
                        Developer Employee ID -A1266
                        Date -21-03-2018
                        JIRA Bug Code (if Applicable)-
                */
                if(gridParam1=='None'){
                        SnTDMLSecurityUtil.printDebugMessage('__ENTERED NONE VALE');
                        errorFlag = true;
                       // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please Fill All Data and Click on Save AND Next Else Click Cancel'));

                    }
                else
                {
                    if(step.Step_Type__c == 'Matrix'){
                        step.Matrix__c = selectedMatrix;
                        step.Grid_Param_1__c = gridParam1;
                        if(gridMap.Grid_Type__c == '2D'){
                            if(gridParam2 == 'None'){
                                    errorFlag = true;
                            }
                            else{
                                step.Grid_Param_2__c = gridParam2;
                            }
                        }
                    }else if(step.Step_Type__c == 'Compute'){
                        computeObj.Field_1__c = selectedCompF1;
                        computeObj.Field_2_Type__c = field2Type;
                        if(String.isNotBlank(selectedCompF2))
                            computeObj.Field_2__c = selectedCompF2;
                        else{
                            computeObj.Field_2_val__c = selectedCompF3;
                        }
                        SnTDMLSecurityUtil.upsertRecords(computeObj,'ComputeSegmentCtlr');
                        step.Compute_Master__c = computeObj.Id;
                    }else if(step.Step_Type__c == 'Cases'){
                        computeObj.Expression__c = expression;
                        SnTDMLSecurityUtil.upsertRecords(computeObj,'ComputeSegmentCtlr');
                        step.Compute_Master__c = computeObj.Id;
                    }/* Shivansh - A1450 */
                    else if(step.Step_Type__c == 'Quantile'){
                        /*SnTDMLSecurityUtil.printDebugMessage('--selectedCompF2-- ' + selectedCompF2);*/
                        /*SnTDMLSecurityUtil.printDebugMessage('--selectedCompF1-- ' + selectedCompF1);*/
                        computeObj.Field_1__c = selectedCompF1;
                        computeObj.Field_2_val__c = selectedCompF3;
                        computeObj.Expression__c = expression;
                        Map<Id, String> parmeterIdToName = new Map<Id, String>();//Map to store old parameterid to name for editing the expression in Quantile step
                        /*for(Rule_Parameter__c rp: [SELECT Id, Parameter_Name__c FROM Rule_Parameter__c WHERE Measure_Master__c =: ruleObject.Id]){
                            parmeterIdToName.put(rp.Id,rp.Parameter_Name__c);
                        }*/
                        List<Rule_Parameter__c> rptemp;
                        try{
                            rptemp = [SELECT Id, Parameter_Name__c FROM Rule_Parameter__c WHERE Measure_Master__c =: ruleObject.Id WITH SECURITY_ENFORCED];
                        }
                            catch(Exception qe) 
                        {
                                PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                                SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
                            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
                        }

                        for(Rule_Parameter__c rp: rptemp){
                            parmeterIdToName.put(rp.Id,rp.Parameter_Name__c);
                        }

                        String exp = expression;
                        for(Id s : parmeterIdToName.keySet()){
                            exp = exp.replaceAll(s,parmeterIdToName.get(s));
                        }
                        computeObj.User_View_Expression__c = exp;
                        SnTDMLSecurityUtil.upsertRecords(computeObj,'ComputeSegmentCtlr');
                        step.Compute_Master__c = computeObj.Id;
                        step.isQuantileComputed__c = false;
                    }
                    /* Shivansh - A1450 till Here*/
                    SnTDMLSecurityUtil.upsertRecords(step,'ComputeSegmentCtlr');

                if(!isStepEditMode){
                    //Update Next steps if any
                    list<Step__c> allNextSteps ;
                    try{
                        allNextSteps = [SELECT Id, Sequence__c FROM Step__c WHERE Measure_Master__c =:ruleObject.Id AND Sequence__c >=: step.Sequence__c WITH SECURITY_ENFORCED ORDER BY Sequence__c];
                    }
                        catch(Exception qe) 
                    {
                            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                            SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
                        SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
                    }

                    Decimal duplicateSeq = 0;
                    map<Decimal, list<Step__c>> stepSeqMap = new map<Decimal,list<Step__c>>();
                    if(allNextSteps != null && allNextSteps.size() > 0){
                        // creating a map of id and sequence
                        
                        for(Step__c stepObj: allNextSteps){
                            if(stepSeqMap.get(stepObj.Sequence__c) == null)
                                stepSeqMap.put(stepObj.Sequence__c, new list<Step__c>{stepObj});
                            else{
                                list<Step__c> stepList = stepSeqMap.get(stepObj.Sequence__c);
                                stepList.add(stepObj);
                                stepSeqMap.put(stepObj.Sequence__c, stepList);
                            }
                        }

                        System.debug('stepSeqMap :'+stepSeqMap);

                        for(Step__c st: allNextSteps){
                            System.debug('st - '+st);
                            System.debug('duplicateSeq :'+duplicateSeq);
                            list<Step__c> tempStepList = stepSeqMap.get(st.Sequence__c);
                            if(tempStepList.size() > 1){
                                for(Step__c obj : tempStepList){
                                    if(st.Id != step.Id && step.Id != obj.Id){
                                        st.Sequence__c += 1;
                                        duplicateSeq = st.Sequence__c;
                                    }
                                }
                            }else{
                                
                                if(st.Sequence__c == duplicateSeq){
                                    st.Sequence__c += 1;
                                    duplicateSeq = st.Sequence__c;
                                }
                            }
                        }

                        System.debug('allNextSteps after update:'+allNextSteps);
                        /*for(Step__c st: allNextSteps){
                            System.debug('st - '+st);
                            if(st.Id != step.Id){
                                st.Sequence__c += 1;
                            }
                        }*/
                    }
                    SnTDMLSecurityUtil.updateRecords(allNextSteps,'ComputeSegmentCtlr');

                }

                Rule_Parameter__c rp = new Rule_Parameter__c();
                if(String.isNotBlank(step.Id)){
                    list<Rule_Parameter__c> rps ;
                    try{
                       rps = [select id from Rule_Parameter__c WHERE Step__c =: step.Id WITH SECURITY_ENFORCED];
                    }
                        catch(Exception qe) 
                    {
                            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                            SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
                        SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
                    }
                    if(rps != null && rps.size() > 0){
                        rp.Id = rps[0].Id;
                    }
                }
                rp.Measure_Master__c = ruleObject.Id;
                rp.Step__c = step.Id;
                rp.Type__c = step.Type__c;
                //upsert rp;
                SnTDMLSecurityUtil.upsertRecords(rp,'ComputeSegmentCtlr');
                updateRule(uiLocation, 'Compute Values');
            }
        }
            if(errorFlag)
            {
            PageMessages = SLDSPageMessage.add(PageMessages,Label.BR_DimensionError,'error');
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.BR_DimensionError));

                }
            }
            catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'ComputeSegmentCtlr',ruleId);
            } 
            
        }

        public void save(){
            try{
            String result = validateStep(uiLocation);
            SnTDMLSecurityUtil.printDebugMessage('===result in Save:'+result);
            if(String.isBlank(result)){
                saveFinal(result);
            }

            if(errorFlag){
            PageMessages = SLDSPageMessage.add(PageMessages,Label.BR_DimensionError,'error');
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.BR_DimensionError));
            }
            if(String.isNotBlank(result)){
		errorFlag = true; // -- Added by RT for SR-453 --
                PageMessages = SLDSPageMessage.add(PageMessages,result,'error');
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, result));
                SnTDMLSecurityUtil.printDebugMessage('No data found');
            }
        }
            catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'ComputeSegmentCtlr',ruleId);
                } 
        }

        /*Skip step added to compute segment ST-17*/
        public PageReference skipStep()
        {
            updateRule('Compute TCF', uiLocation);
            return nextPage('ComputeTCF');
        }
        public PageReference saveAndNext(){
            try{
            String result = validateStep(uiLocation);
             SnTDMLSecurityUtil.printDebugMessage('===result in saveAndNext:'+result);
            if(String.isBlank(result)){
                list<Step__c> steps ;
                try{
                    steps = [SELECT Id FROM Step__c WHERE Measure_Master__c =: ruleObject.Id AND UI_Location__c =:uiLocation WITH SECURITY_ENFORCED];
                }
                    catch(Exception qe) 
                {
                        PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                        SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
                    SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
                }

                if(step != null || (steps != null && steps.size() >0)){
                    saveFinal(result);
                    if(errorFlag){
                        PageMessages = SLDSPageMessage.add(PageMessages,Label.BR_DimensionError,'error');
                        //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.BR_DimensionError));
                    }
                    else{
                        updateRule('Compute TCF', uiLocation);
                        return nextPage('ComputeTCF');
                    }

                }else{
                    PageMessages = SLDSPageMessage.add(PageMessages,'Please add atleast one step','error');
                    //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please add atleast one step'));
                }
            }

            if(String.isNotBlank(result)){
                PageMessages = SLDSPageMessage.add(PageMessages,result,'error');
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, result));
                SnTDMLSecurityUtil.printDebugMessage('No data found');
                }
                return null;
            }
           catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'ComputeSegmentCtlr',ruleId);
            return null;
                }  
        }

        public void Openpopup(){
            SnTDMLSecurityUtil.printDebugMessage('============INSIDE OPEN POPUP FUNCTION');
            SnTDMLSecurityUtil.printDebugMessage('=========Select Matrix is::'+selectedMatrix);
            list<Step__c> Step  ;
            try{
                Step = [Select id,Name,Measure_Master__r.Name from Step__c where Matrix__c=:selectedMatrix WITH SECURITY_ENFORCED];
            }
            catch(Exception qe) 
            {
                PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
                SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
            }
            if(!step.isEmpty()){
                Measure_Master__c MM = [SELECT Id, Name FROM Measure_Master__c WHERE id =:ruleObject.Id WITH SECURITY_ENFORCED];
                //removed BTI in below query
                Grid_Master__c GM = [select id,name,Brand__c,Col__c,Country__c,Description__c,Dimension_1_Name__c,Dimension_2_Name__c,DM1_Output_Type__c,DM2_Output_Type__c,Grid_Type__c,Output_Name__c,Output_Type__c,Row__c from Grid_Master__c where id =:selectedMatrix WITH SECURITY_ENFORCED];
                    Grid_Master__c CloneGM = GM.Clone();
                    String name =GM.Name ;
                    CloneGM.Name = MM.Name+'_'+name;
                    CloneGM.Country__c = GM.Country__c;
                   // CloneGM.CurrencyIsoCode = 'EUR';
                    SnTDMLSecurityUtil.printDebugMessage('====CloneGM===:'+CloneGM);
                    try{
                    //insert CloneGM;
                    SnTDMLSecurityUtil.insertRecords(CloneGM, 'ComputeSegmentCtlr');
                    matrixList.add(new SelectOption(CloneGM.Id, CloneGM.Name));

                    list<Grid_Details__c> GD = new list<Grid_Details__c>();
                    list<Grid_Details__c> CloneGD = new list<Grid_Details__c>();
                    try{
                        GD = [select id,Name,Grid_Master__c,colvalue__c,Dimension_1_Value__c,Dimension_2_Value__c,Output_Value__c,Rowvalue__c  from Grid_Details__c where Grid_Master__c =:selectedMatrix WITH SECURITY_ENFORCED];
                    }
                    catch(Exception qe) 
                    {
                        PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                        SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
                        SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
                    }

                    
                    for(Grid_Details__c g : GD){
                        Grid_Details__c newGD = g.clone();
                            newGD.Grid_Master__c = CloneGM.id;
                            //newGD.CurrencyIsoCode = 'EUR';
                            newGD.Name = g.name;
                            CloneGD.add(newGD);
                        
                    }
                    //insert CloneGD;
                    SnTDMLSecurityUtil.insertRecords(CloneGD, 'ComputeSegmentCtlr');
                    SnTDMLSecurityUtil.printDebugMessage('========CloneGD====:'+CloneGD);
                    
                    selectedMatrix= CloneGM.id;
                }
                catch(Exception ex){
                   // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Matrix Name Should be Unique'));
                   PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                   SalesIQSnTLogger.createUnHandledErrorLogs(ex, SalesIQSnTLogger.BR_MODULE, 'ComputeSegmentCtlr',ruleId);
                }
            }
        }

    }