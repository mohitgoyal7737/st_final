public with sharing class PopulateDetailGroupOnProductHandler {

	public static void upsertDetailGroup(List<Product_Catalog__c> upsertProduct)
    {
    	List<ProductToDetailGroup__c> pro2DetailList = [select Id,Detail_Group__c,Veeva_External_ID__c from ProductToDetailGroup__c  WHERE Veeva_External_ID__c != NULL];
    	Map<String,String> mapProduct2DetailGroup= new Map<String,String>();
    	//List<Product_Catalog__c> prodList = new List<Product_Catalog__c>();
    	
    	if(pro2DetailList != null)
    	{
    		for(ProductToDetailGroup__c prd2DetailRec : pro2DetailList)
    		{
    			mapProduct2DetailGroup.put(prd2DetailRec.Veeva_External_ID__c,prd2DetailRec.Detail_Group__c);
    		}
    	}

    	for(Product_Catalog__c upsertRec : upsertProduct)
    	{
    		if(upsertRec.Populate_Detail_Group__c == true)
    		{
    			upsertRec.Detail_Group__c=mapProduct2DetailGroup.get(upsertRec.Veeva_External_ID__c);
    			//prodList.add(upsertRec);
    		}
    	}

    	//upsert prodList External_ID__c;
    }


    /*public static void updateDetailGroup(List<Product_Catalog__c> updateProduct)
    {
    }*/
    
}