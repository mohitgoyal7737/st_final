global class BatchUpdAccAfterTrigger implements Database.Batchable < sObject > , Database.Stateful, schedulable {
    public Integer recordsProcessed = 0;
    global Boolean batchFlag;
    public String batchID;
    global DateTime lastjobDate = null;
    global String query;
   public list < AxtriaSalesIQTM__Account_Affiliation__c  > afflist {get;set;}
    
    Set<id> accAffId;
    
    global BatchUpdAccAfterTrigger (Set<id> accIds) { //set<String> Accountid
        
        accAffId = new Set<id>(accIds);
        system.debug('trigger acc'+accAffId );
        batchFlag=true;
        query='Select Id,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Parent_Account__c from AxtriaSalesIQTM__Account_Affiliation__c ' +
              'where AxtriaSalesIQTM__Account__c IN:accAffId or AxtriaSalesIQTM__Parent_Account__c IN:accAffId ';

        System.debug('query' + query);
        //Create a new record for Scheduler Batch with values, Job_Type, Job_Status__c as Failed, Created_Date__c as Today’s Date.
    }
    
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
       
        return Database.getQueryLocator(query);
    }
    
    
    public void execute(System.SchedulableContext SC) {
    }
    
    
    global void execute(Database.BatchableContext bc, List < AxtriaSalesIQTM__Account_Affiliation__c  > records) {
        // process each batch of records
        System.debug('records in execute'+records.size());
        Set<id> AclList = new Set < id > ();
        for (AxtriaSalesIQTM__Account_Affiliation__c  SAA: records) {
             System.debug('SAA'+SAA);
             if(SAA.AxtriaSalesIQTM__Account__c!=null){
                AclList.add(SAA.AxtriaSalesIQTM__Account__c);
               }
             if(SAA.AxtriaSalesIQTM__Parent_Account__c!=null){
                AclList.add(SAA.AxtriaSalesIQTM__Parent_Account__c);
               }
            
           }
        System.debug('affiliated accounts level 1'+AclList.size());
       // List < Account > AccountList = [select id, Affiliation_Delta_Flag__c from Account where External_Account_Number__c IN: AclList];
       
        System.debug('AclList****'+AclList);
       /* list<AxtriaSalesIQTM__Account_Affiliation__c  >  accaffList1=[Select Id,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Parent_Account__c from AxtriaSalesIQTM__Account_Affiliation__c  where AxtriaSalesIQTM__Account__c IN:AclList or AxtriaSalesIQTM__Parent_Account__c IN:AclList];
        for(AxtriaSalesIQTM__Account_Affiliation__c aff:accaffList1){
        if(aff.AxtriaSalesIQTM__Parent_Account__c!=null){
        AclList.add(aff.AxtriaSalesIQTM__Parent_Account__c);
        }
        if(aff.AxtriaSalesIQTM__Account__c!=null){
        AclList.add(aff.AxtriaSalesIQTM__Account__c);
        }
        }
         System.debug('affiliated accounts level 2'+AclList.size());
       */
        System.debug('AclList****'+AclList);
        List < Account > updatelist = new List < Account > ();
        updatelist =[Select Id from Account where Id IN:AclList ];
        System.debug('updatelist '+updatelist );
        if(updatelist !=null && updatelist .size()>0){
            batchFlag=false;
            update updatelist ;
            }
        
    }
    
    
    global void finish(Database.BatchableContext bc) {
        // execute any post-processing operations
         batchFlag=false;
         System.debug('batch flag in batch'+batchFlag);
      
    }
}