/*
@author - Prince Richard Augustin (A2661)
@description - Test class to test PopulateDetailGroupOnProductHandler.
*/
@isTest
private class PopulateDetailGroupOnProductHandlerTest {
    static testMethod void firstTest() 
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        /*Create initial test data for all the objs reqd.*/
        AxtriaSalesIQTM__Organization_Master__c aom = new AxtriaSalesIQTM__Organization_Master__c(AxtriaSalesIQTM__Org_Level__c='Global',AxtriaSalesIQTM__Parent_Country_Level__c=true);
        insert aom;
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='USA',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active');
        insert country;
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = country.id;
        insert workspace;
        Account acc = new Account(Name='test acc',Marketing_Code__c='EU',AxtriaSalesIQTM__AccountType__c = 'HCP');
        insert acc;
        
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='Test Team');
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU',AxtriaSalesIQTM__Team__c=team.id,IsHCOSegmentationEnabled__c =true);
        insert ti;
        
        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c(Name='Pos',AxtriaSalesIQTM__Team_iD__c=team.id);
        insert pos;
        
        Product_Catalog__c pc = new Product_Catalog__c(Name='Test Product',Team_Instance__c=ti.id,Veeva_External_ID__c = 'ProdId',Product_Code__c= 'ProdId',IsActive__c=true,Country_Lookup__c=country.id);
        
        pc.Populate_Detail_Group__c = true;
        insert pc;
        
        /*Brand_Team_Instance__c bti = new Brand_Team_Instance__c(Brand__c=pc.id,Team_Instance__c=ti.id);
        insert bti;*/
        
        Measure_Master__c mm = new Measure_Master__c(Team_Instance__c=ti.id,Number_of_BU_Unique_Accounts__c = 1,is_push_to_alignment_allowed__c=true,is_publish_allowed__c = true,Brand_Lookup__c=pc.id,State__c='Executed',All_Segments__c = 'All',Measure_Type__c ='HCP');
        insert mm;
        
        
        AxtriaSalesIQTM__Position_Account__c pa = new AxtriaSalesIQTM__Position_Account__c(AxtriaSalesIQTM__Team_Instance__c=ti.id,AxtriaSalesIQTM__Account__c=acc.id);
        insert pa;
        BU_Response__c s = new BU_Response__c(Team_Instance__c =ti.id,Physician__c = acc.id);
        insert s;
        Account_Compute_Final__c t = TestDataFactory.createComputeFinal(mm, acc, pa, s, pos);
        insert t;
        AxtriaSalesIQTM__ETL_Config__c etl = TestDataFactory.configureETL();
        insert etl;
        Scheduler_Log__c sch = new Scheduler_Log__c();
        sch.Job_Type__c = 'Survey Data Load';
        sch.Job_Status__c = 'Success';
        insert sch;
        ProductToDetailGroup__c p = new ProductToDetailGroup__c();
        insert p;
        
        
        Test.startTest();
        System.runAs(loggedInUser){
                ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'BatchDeassignPositionAccountsTest'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> FIELD_LIST = new List<String>{nameSpace+'AccountNumber__c',nameSpace+'Account_Text__c'};
                    System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(temp_Obj__c.SObjectType, FIELD_LIST, false));
                PopulateDetailGroupOnProductHandler a = new PopulateDetailGroupOnProductHandler();
                List<Product_Catalog__c> upsertProduct = new List<Product_Catalog__c> ();
                upsertProduct.add(pc);
                PopulateDetailGroupOnProductHandler.upsertDetailGroup(upsertProduct);
                ProcecssStateViewCtrl obj = new ProcecssStateViewCtrl();
                
                obj.pathvalue ='test';
                
                pagereference p12 = obj.redirecttopage();
                List <sobject> sobjList = new List <sobject>();
                sobjList.add(pc);
                /*CIMPositionMatrixSummaryUpdateCtlrBatch c = new CIMPositionMatrixSummaryUpdateCtlrBatch(sobjList);
```````````````Database.executeBatch(c);*/
            }
        Test.stopTest();
        
    }
    static testMethod void nullPathTest() 
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'BatchDeassignPositionAccountsTest'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> FIELD_LIST = new List<String>{nameSpace+'AccountNumber__c',nameSpace+'Account_Text__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(temp_Obj__c.SObjectType, FIELD_LIST, false));

            ProcecssStateViewCtrl obj = new ProcecssStateViewCtrl();
            obj.pathvalue ='';
            //pagereference p = obj.redirecttopage();      
            PageReference pageRef = Page.ComputeValues;
            Test.setCurrentPage(pageRef);
            pageRef = obj.redirecttopage();  
        }
        Test.stopTest();
        
    }
    
}