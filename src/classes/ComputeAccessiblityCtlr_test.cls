@isTest
public class ComputeAccessiblityCtlr_test {
    @istest static void valtest() {

        User loggedInUser = new User(id = UserInfo.getUserId());
        String result = '';

        

        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;

       

        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;

        Grid_Master__c gMaster = TestDataFactory.gridMaster(countr);
        insert gMaster;

        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;

        Measure_Master__c mMaster = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        insert mMaster;

        Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
        insert pp;

        Compute_Master__c ccMaster1 = new Compute_Master__c();
        ccMaster1.Name = 'Test';
        ccMaster1.CurrencyIsoCode = 'USD';
        ccMaster1.Field_2_Type__c = 'Parameters';
        ccMaster1.Field_2_val__c = 1;
        insert ccMaster1;

        Step__c step = new Step__c();
        step.Name = 'abcd';
        step.CurrencyIsoCode = 'USD';
        step.UI_Location__c = 'Compute Segment';
        step.Step_Type__c = 'Cases';
        step.Compute_Master__c = ccMaster1.id;
        step.Matrix__c = gMaster.id;
        step.Measure_Master__c = mMaster.id;
        insert step;

        Rule_Parameter__c rps = TestDataFactory.ruleParameter(mMaster, pp, step);
        insert rps;

        Compute_Master__c ccMaster = TestDataFactory.createComputeMaster(rps);
        insert ccMaster;

        Step__c steps = TestDataFactory.step(ccMaster, gMaster, mMaster, rps);
        insert steps;
        
        Grid_Details__c gDetails = TestDataFactory.gridDetails(gMaster);
        insert gDetails;
        
        String gridParam1;
        Test.startTest();
        System.runAs(loggedinuser) {
            ComputeAccessiblityCtlr  obj = new ComputeAccessiblityCtlr();
            obj.gridParam1 = rps.id;
            obj.gridMap = gMaster;
            obj.gridParam2 = rps.id;
            obj.uiLocation = 'Compute Segment';
            obj.selectedBuisnessUnit = teamins.id;
            obj.selectedBrand = pcc.id;
            obj.selectedCycle = teamins.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c;
            obj.selectedMatrix = gMaster.id;
            obj.selectedCustType = '1';
            obj.ruleObject = mMaster;
            obj.selectedCompF1 = rps.id;
            obj.selectedCompF2 = rps.id;
            obj.field2Type = '1';
            obj.selectedCompF3 = 1 ;
            obj.computeObj = ccMaster;
            obj.expression = 'Test';
            obj.step = step;
            obj.saveFinal(result);
            obj.skipStep();
            obj.save();
            obj.saveAndNext();
            obj.Openpopup();
            //System.assert(ApexPages.hasMessages(ApexPages.SEVERITY.ERROR));
            System.assertNotEquals(gridParam1, rps.id,'');
            
        }

        Test.stopTest();
    }
    
    @istest static void valtest1() {

        User loggedInUser = new User(id = UserInfo.getUserId());
        String result = '';


        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;

       

        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;

        Grid_Master__c gMaster = TestDataFactory.gridMaster(countr);
        insert gMaster;

        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;

        Measure_Master__c mMaster = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        insert mMaster;

        Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
        insert pp;

        Compute_Master__c ccMaster1 = new Compute_Master__c();
        ccMaster1.Name = 'Test';
        ccMaster1.CurrencyIsoCode = 'USD';
        ccMaster1.Field_2_Type__c = 'Parameters';
        ccMaster1.Field_2_val__c = 1;
        insert ccMaster1;

        Step__c step = new Step__c();
        step.Name = 'abcd';
        step.CurrencyIsoCode = 'USD';
        step.UI_Location__c = 'Compute Segment';
        step.Step_Type__c = 'Matrix';
        step.Compute_Master__c = ccMaster1.id;
        step.Matrix__c = gMaster.id;
        step.Measure_Master__c = mMaster.id;
        insert step;

        Rule_Parameter__c rps = TestDataFactory.ruleParameter(mMaster, pp, step);
        insert rps;

        Compute_Master__c ccMaster = TestDataFactory.createComputeMaster(rps);
        insert ccMaster;

        Step__c steps = TestDataFactory.step(ccMaster, gMaster, mMaster, rps);
        insert steps;
        
        Grid_Details__c gDetails = TestDataFactory.gridDetails(gMaster);
        insert gDetails;
        
        String gridParam1;
        Test.startTest();
        System.runAs(loggedinuser) {
            ComputeAccessiblityCtlr  obj = new ComputeAccessiblityCtlr();
            obj.gridParam1 = rps.id;
            obj.gridMap = gMaster;
            obj.gridParam2 = rps.id;
            obj.uiLocation = 'Compute Segment';
            obj.selectedBuisnessUnit = teamins.id;
            obj.selectedBrand = pcc.id;
            obj.selectedCycle = teamins.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c;
            obj.selectedMatrix = gMaster.id;
            obj.selectedCustType = '1';
            obj.ruleObject = mMaster;
            obj.selectedCompF1 = rps.id;
            obj.selectedCompF2 = rps.id;
            obj.field2Type = '1';
            obj.selectedCompF3 = 1 ;
            obj.computeObj = ccMaster;
            obj.expression = 'Test';
            obj.step = step;
            obj.saveFinal(result);
            obj.skipStep();
            obj.save();
            obj.saveAndNext();
            obj.Openpopup();
            //System.assert(ApexPages.hasMessages(ApexPages.SEVERITY.ERROR));
            System.assertNotEquals(gridParam1, rps.id,'');
            
        }

        Test.stopTest();
    }
    
    @istest static void valtest2() {

        User loggedInUser = new User(id = UserInfo.getUserId());
        String result = '';

       

        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;

        

        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;

        Grid_Master__c gMaster = TestDataFactory.gridMaster(countr);
        insert gMaster;

        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;

        Measure_Master__c mMaster = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        insert mMaster;

        Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
        insert pp;

        Compute_Master__c ccMaster1 = new Compute_Master__c();
        ccMaster1.Name = 'Test';
        ccMaster1.CurrencyIsoCode = 'USD';
        ccMaster1.Field_2_Type__c = 'Parameters';
        ccMaster1.Field_2_val__c = 1;
        insert ccMaster1;

        Step__c step = new Step__c();
        step.Name = 'abcd';
        step.CurrencyIsoCode = 'USD';
        step.UI_Location__c = 'Compute Segment';
        step.Step_Type__c = 'Compute';
        step.Compute_Master__c = ccMaster1.id;
        step.Matrix__c = gMaster.id;
        step.Measure_Master__c = mMaster.id;
        insert step;

        Rule_Parameter__c rps = TestDataFactory.ruleParameter(mMaster, pp, step);
        insert rps;

        Compute_Master__c ccMaster = TestDataFactory.createComputeMaster(rps);
        insert ccMaster;

        Step__c steps = TestDataFactory.step(ccMaster, gMaster, mMaster, rps);
        insert steps;
        
        Grid_Details__c gDetails = TestDataFactory.gridDetails(gMaster);
        insert gDetails;
        
        String gridParam1;
        Test.startTest();
        System.runAs(loggedinuser) {
            ComputeAccessiblityCtlr  obj = new ComputeAccessiblityCtlr();
            obj.gridParam1 = rps.id;
            obj.gridMap = gMaster;
            obj.gridParam2 = rps.id;
            obj.uiLocation = 'Compute Segment';
            obj.selectedBuisnessUnit = teamins.id;
            obj.selectedBrand = pcc.id;
            obj.selectedCycle = teamins.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c;
            obj.selectedMatrix = gMaster.id;
            obj.selectedCustType = '1';
            obj.ruleObject = mMaster;
            obj.selectedCompF1 = rps.id;
            obj.selectedCompF2 = rps.id;
            obj.field2Type = '1';
            obj.selectedCompF3 = 1 ;
            obj.computeObj = ccMaster;
            obj.expression = 'Test';
            obj.step = step;
            obj.productPriorityEnabled = true;
            obj.saveFinal(result);
            obj.skipStep();
            obj.save();
            obj.saveAndNext();
            obj.Openpopup();
            //System.assert(ApexPages.hasMessages(ApexPages.SEVERITY.ERROR));
            System.assertNotEquals(gridParam1, rps.id,'');
            
        }

        Test.stopTest();
    }
    
    @istest static void valtest3() {

        User loggedInUser = new User(id = UserInfo.getUserId());
        String result = '';

       

        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;


        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;

        Grid_Master__c gMaster = TestDataFactory.gridMaster(countr);
        insert gMaster;

        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;

        Measure_Master__c mMaster = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        insert mMaster;

        Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
        insert pp;

        Compute_Master__c ccMaster1 = new Compute_Master__c();
        ccMaster1.Name = 'Test';
        ccMaster1.CurrencyIsoCode = 'USD';
        ccMaster1.Field_2_Type__c = 'Parameters';
        ccMaster1.Field_2_val__c = 1;
        insert ccMaster1;

        Step__c step = new Step__c();
        step.Name = 'abcd';
        step.CurrencyIsoCode = 'USD';
        step.UI_Location__c = 'Compute Segment';
        step.Step_Type__c = 'Compute';
        step.Compute_Master__c = ccMaster1.id;
        step.Matrix__c = gMaster.id;
        step.Measure_Master__c = mMaster.id;
        insert step;

        Rule_Parameter__c rps = TestDataFactory.ruleParameter(mMaster, pp, step);
        insert rps;

        Compute_Master__c ccMaster = TestDataFactory.createComputeMaster(rps);
        insert ccMaster;

        Step__c steps = TestDataFactory.step(ccMaster, gMaster, mMaster, rps);
        insert steps;
        
        Grid_Details__c gDetails = TestDataFactory.gridDetails(gMaster);
        insert gDetails;
        
        String gridParam1;
        Test.startTest();
        System.runAs(loggedinuser) {
            ComputeAccessiblityCtlr  obj = new ComputeAccessiblityCtlr();
            obj.gridParam1 = 'None';
            obj.gridMap = gMaster;
            obj.gridParam2 = rps.id;
            obj.uiLocation = 'Compute Segment';
            obj.selectedBuisnessUnit = teamins.id;
            obj.selectedBrand = pcc.id;
            obj.selectedCycle = teamins.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c;
            obj.selectedMatrix = gMaster.id;
            obj.selectedCustType = '1';
            obj.ruleObject = mMaster;
            obj.selectedCompF1 = rps.id;
            obj.selectedCompF2 = rps.id;
            obj.field2Type = '1';
            obj.selectedCompF3 = 1 ;
            obj.computeObj = ccMaster;
            obj.expression = 'Test';
            obj.step = step;
            obj.productPriorityEnabled = true;
            obj.saveFinal(result);
            obj.skipStep();
            obj.save();
            obj.saveAndNext();
            obj.Openpopup();
            //System.assert(ApexPages.hasMessages(ApexPages.SEVERITY.ERROR));
            System.assertNotEquals(gridParam1, rps.id,'');
            
        }

        Test.stopTest();
    }
}