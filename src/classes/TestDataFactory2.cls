/*Author : Ayush Rastogi*/

@isTest
public class TestDataFactory2 {

    User loggedInUser = new User(id=UserInfo.getUserId());
    
    public static AxtriaSalesIQTM__Team__c createTeam() {
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
        team.Name = 'HTN';
        team.CurrencyIsoCode = 'USD';
        //team.AxtriaSalesIQTM__Type__c = teamType; 
        //teamList.add(team);
        return team;
    }
    
    
    
    public static AxtriaSalesIQTM__Team_Instance__c createTeamInstance() {
    AxtriaSalesIQTM__Team_Instance__c teamins = new AxtriaSalesIQTM__Team_Instance__c();
        teamins.Name = 'abcde';
        teamins.Include_Secondary_Affiliations__c = true;
        teamins.Include_Territory__c = true;
        teamins.Product_Priority__c= true;
        teamins.AxtriaSalesIQTM__IC_EffEndDate__c = date.today();
        teamins.AxtriaSalesIQTM__IC_EffstartDate__c = date.today();
        return teamins;

    }
    
    
    public static AxtriaSalesIQTM__Organization_Master__c createOrganizationMaster() {
        AxtriaSalesIQTM__Organization_Master__c orgmas = new AxtriaSalesIQTM__Organization_Master__c();
        orgmas.Name = 'abcd';
        orgmas.AxtriaSalesIQTM__Org_Level__c = 'Global';
        orgmas.AxtriaSalesIQTM__Parent_Country_Level__c = true;
        return orgmas;

    }
    
    public static Measure_Master__c createMeasureMaster() {
        Measure_Master__c  mMaster = new Measure_Master__c();
        mMaster.is_complete__c = true;
        mMaster.is_promoted__c = true;
        mMaster.Name = 'abcd';
        mMaster.CurrencyIsoCode = 'USD';
        mMaster.State__c = 'Publishing Failed';
        mMaster.TcfOverride__c=true;
        
        mMaster.State__c='Complete';
        return mMaster;

    }
    
    public static Product_Priority__c productPriority() {
        Product_Priority__c pPriority = new Product_Priority__c();
        //pPriority.Name = 'abcd';
        pPriority.CurrencyIsoCode = 'USD';
        return pPriority;

    }
    
    public static Parameter__c parameter() {
        Parameter__c pp= new Parameter__c();
        pp.Name='Tst';
        pp.Name__c = 'testing';
        pp.CurrencyIsoCode = 'USD';
        return pp;
    }
    public static Rule_Parameter__c ruleParameter() {
        Rule_Parameter__c rps= new Rule_Parameter__c();
        
        rps.CurrencyIsoCode='USD';
        return rps;
    
    }
    
    public static Compute_Master__c createComputeMaster(){
        Compute_Master__c ccMaster = new Compute_Master__c ();
        ccMaster.Name = 'Test';
        ccMaster.CurrencyIsoCode = 'USD';
        return ccMaster;
    
    }
    
    public static Grid_Master__c gridMaster() {
        Grid_Master__c gMaster = new Grid_Master__c();
        gMaster.Name = 'abcd';
        gMaster.CurrencyIsoCode = 'USD';
        return gMaster;

    }
    
    
    public static Step__c step() {
        Step__c steps= new Step__c();
        steps.Name = 'abcd';
        steps.CurrencyIsoCode = 'USD';
        steps.UI_Location__c = 'Compute Segment';
        steps.Step_Type__c = 'Matrix';
        
        return steps;

    }
    
    
    public static Grid_Details__c gridDetails() {
        Grid_Details__c gDetails = new Grid_Details__c();
        gDetails.Name = 'abcd';
        gDetails.CurrencyIsoCode = 'USD';
        gDetails.Output_Value__c='test';
        gDetails.Dimension_1_Value__c='ND';
        gDetails.Dimension_2_Value__c='test';
        return gDetails;

    }

    public static Account createAccount() {
        Account acc = new Account();
        acc.Name = 'Chelsea Parson';
        acc.AccountNumber = 'adf23';
        acc.AxtriaSalesIQTM__External_Account_Number__c = '123';
        acc.BillingStreet = 'abc';
        acc.Marketing_Code__c = 'thgh';
        acc.Primary_Speciality_Code__c = 'id';
        return acc;
    }

    public static AxtriaSalesIQTM__Country__c createCountry() {
        AxtriaSalesIQTM__Country__c countr = new AxtriaSalesIQTM__Country__c();
        countr.Name = 'abcd';
        countr.AxtriaSalesIQTM__Country_Code__c = 'UK';
        countr.AxtriaSalesIQTM__Status__c = 'Active';
        return countr;
    }
    
    
    public static User createUser(String profileId) {
        // This code runs as the system user

        User tUser = new User(Alias = 'Rep', Email = 'repuser@testorg.com',
            EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US', ProfileId = profileId,
            TimeZoneSidKey = 'America/Los_Angeles', UserName = 'repuserQuset@testorg.com');
        return tUser;
    }
    public static AxtriaSalesIQTM__Workspace__c createWorkspace(String workspaceName, Date startDate, Date endDate) {
        AxtriaSalesIQTM__Workspace__c workspace = new AxtriaSalesIQTM__Workspace__c();
        workspace.Name = workspaceName;
        workspace.AxtriaSalesIQTM__Workspace_Description__c = 'Some Description';
        workspace.AxtriaSalesIQTM__Workspace_Start_Date__c = startDate;
        workspace.AxtriaSalesIQTM__Workspace_End_Date__c = endDate;

        return workspace;
    }
    
    public static AxtriaSalesIQTM__Position_Account__c createPositionAccount(Account acc,AxtriaSalesIQTM__Position__c pos,AxtriaSalesIQTM__Team_Instance__c teamins){
        AxtriaSalesIQTM__Position_Account__c posAccount = new AxtriaSalesIQTM__Position_Account__c ();
        posAccount.CurrencyIsoCode = 'USD';
        posAccount.AxtriaSalesIQTM__Account__c = acc.id;
        posAccount.AxtriaSalesIQTM__Position__c = pos.id;
        posAccount.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        return posAccount;
    
    
    }
    public static AxtriaSalesIQTM__Position_Account_Call_Plan__c createPositionAccountCallPlan() {
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccount= new AxtriaSalesIQTM__Position_Account_Call_Plan__c();
        positionAccount.Name = 'Test';
        
        positionAccount.Segment__c = 'Segment';
        positionAccount.CurrencyIsoCode = 'USD';
        positionAccount.P2_Parameter1__c = 'a';
        positionAccount.P2_Parameter2__c= 'b';
        positionAccount.P2_Parameter3__c= 'c';
        positionAccount.P2_Parameter4__c= 'd';
        positionAccount.P2_Parameter5__c= 'e';
        positionAccount.P2_Parameter6__c= 'f';
        positionAccount.P2_Parameter7__c= 'g';
        positionAccount.P2_Parameter8__c= 'h';
        positionAccount.P3_Parameter1__c = 'i';
        positionAccount.P3_Parameter2__c= 'j';
        positionAccount.P3_Parameter3__c= 'k';
        positionAccount.P3_Parameter4__c= 'l';
        positionAccount.P3_Parameter5__c= 'm';
        positionAccount.P3_Parameter6__c= 'n';
        positionAccount.P3_Parameter7__c= 'o';
        positionAccount.P3_Parameter8__c= 'p';
        positionAccount.P4_Parameter1__c = 'q';
        positionAccount.P4_Parameter2__c= 'r';
        positionAccount.P4_Parameter3__c= 's';
        positionAccount.P4_Parameter4__c= 't';
        positionAccount.P4_Parameter5__c= 'u';
        positionAccount.P4_Parameter6__c= 'v';
        positionAccount.P4_Parameter7__c= 'x';
        positionAccount.P4_Parameter8__c= 'y';
        positionAccount.Parameter1__c = 'a';
        positionAccount.Parameter2__c = 'b';
        positionAccount.Parameter3__c = 'c';
        positionAccount.Parameter4__c = 'd';
        positionAccount.Parameter5__c = 'e';
        positionAccount.Parameter6__c = 'f';
        positionAccount.Parameter7__c = 'g';
        positionAccount.Parameter8__c = 'h';
        positionAccount.P1__c = 'Test';
        positionAccount.Accessibility_Range__c = 'Test';

        return positionAccount;
    }

    public static AxtriaSalesIQTM__Scenario__c createScenario(Id workspaceId, Id scenarioTeamInstanceId, Id sourceTeamInstanceId, String stage, String processStage, String status, Date startDate, Date endDate) {
        AxtriaSalesIQTM__Scenario__c scenario = new AxtriaSalesIQTM__Scenario__c();
        if (scenarioTeamInstanceId != null)
            scenario.AxtriaSalesIQTM__Team_Instance__c = scenarioTeamInstanceId;
        if (sourceTeamInstanceId != null)
            scenario.AxtriaSalesIQTM__Source_Team_Instance__c = sourceTeamInstanceId;
        scenario.AxtriaSalesIQTM__Effective_Start_Date__c = startDate;
        scenario.AxtriaSalesIQTM__Effective_End_Date__c = endDate;
        scenario.AxtriaSalesIQTM__Scenario_Stage__c = stage;
        scenario.AxtriaSalesIQTM__Request_Process_Stage__c = processStage;
        scenario.AxtriaSalesIQTM__Scenario_Status__c = status;
        scenario.AxtriaSalesIQTM__Workspace__c = workspaceId;

        return scenario;
    }

    public static AxtriaSalesIQTM__Position__c createPosition() {

        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();
        pos.Name = 'Test';
        pos.AxtriaSalesIQTM__Client_Territory_Name__c = 'abc';
        //pos.AxtriaSalesIQTM__Parent_Position__c = parentPositionId;
        pos.AxtriaSalesIQTM__Client_Position_Code__c = '1NE30002';
        pos.AxtriaSalesIQTM__Client_Territory_Code__c = '1NE30002';
        pos.AxtriaSalesIQTM__Position_Type__c = 'Area';
        //pos.AxtriaSalesIQTM__Related_Position_Type__c = team.AxtriaSalesIQTM__Type__c;
        pos.AxtriaSalesIQTM__Inactive__c = false;
        //pos.AxtriaSalesIQTM__Effective_Start_Date__c = startDate;
        //pos.AxtriaSalesIQTM__Effective_End_Date__c = endDate;
        pos.AxtriaSalesIQTM__RGB__c = '41,210,117';
        pos.AxtriaSalesIQTM__X_Max__c = -72.6966429900;
        pos.AxtriaSalesIQTM__X_Min__c = -73.9625820000;
        pos.AxtriaSalesIQTM__Y_Max__c = 40.9666490000;
        pos.AxtriaSalesIQTM__Y_Min__c = 40.5821279800;
        pos.AxtriaSalesIQTM__Hierarchy_Level__c = '1';

        return pos;
    }

    public static AxtriaSalesIQTM__Team_Instance__c createTeamInstance(String teamId, String type, String scenarioId, Date startDate, Date endDate) {
        AxtriaSalesIQTM__Team_Instance__c objTeamInstance = new AxtriaSalesIQTM__Team_Instance__c();
        objTeamInstance.name = 'HTN_Q1_2016';
        if (scenarioId != null && scenarioId != '') {
            objTeamInstance.AxtriaSalesIQTM__Scenario__c = scenarioId;
            objTeamInstance.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Employee_Assignment__c = true;
        }

        objTeamInstance.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        objTeamInstance.AxtriaSalesIQTM__Team__c = teamId;
        objTeamInstance.AxtriaSalesIQTM__IC_EffstartDate__c = startDate;
        objTeamInstance.AxtriaSalesIQTM__IC_EffEndDate__c = endDate;
        objTeamInstance.AxtriaSalesIQTM__Alignment_Type__c = type;
        objTeamInstance.AxtriaSalesIQTM__isActiveCycle__c = 'Y';
        return objTeamInstance;
    }

    public static AxtriaSalesIQTM__Position_Team_Instance__c createPositionTeamInstance(Id positionId, Id parentPositionId, Id teamInstanceId) {
        AxtriaSalesIQTM__Position_Team_Instance__c positionTeamInstance = new AxtriaSalesIQTM__Position_Team_Instance__c();
        positionTeamInstance.AxtriaSalesIQTM__Position_ID__c = positionId;
        if (parentPositionId != null)
            positionTeamInstance.AxtriaSalesIQTM__Parent_Position_ID__c = parentPositionId;
        positionTeamInstance.AxtriaSalesIQTM__Effective_End_Date__c = Date.newInstance(2018, 1, 1);
        positionTeamInstance.AxtriaSalesIQTM__Effective_Start_Date__c = Date.newInstance(2015, 1, 1);
        positionTeamInstance.AxtriaSalesIQTM__Team_Instance_ID__c = teamInstanceId;
        positionTeamInstance.AxtriaSalesIQTM__X_Max__c = -72.6966429900;
        positionTeamInstance.AxtriaSalesIQTM__X_Min__c = -73.9625820000;
        positionTeamInstance.AxtriaSalesIQTM__Y_Max__c = 40.9666490000;
        positionTeamInstance.AxtriaSalesIQTM__Y_Min__c = 40.5821279800;

        return positionTeamInstance;
    }
    public static AxtriaSalesIQTM__User_Access_Permission__c createUserAccessPerm() {
        AxtriaSalesIQTM__User_Access_Permission__c objUserAccessPerm = new AxtriaSalesIQTM__User_Access_Permission__c();
        objUserAccessPerm.name = 'access';
        objUserAccessPerm.AxtriaSalesIQTM__Is_Active__c = true;
        return objUserAccessPerm;
    }

    public static AxtriaSalesIQTM__Employee__c createEmployee(String firstName, String lastName) {
        AxtriaSalesIQTM__Employee__c employee = new AxtriaSalesIQTM__Employee__c();
        employee.Name = firstName + ' ' + lastName;
        employee.AxtriaSalesIQTM__Employee_ID__c = '12345';
        employee.AxtriaSalesIQTM__Last_Name__c = lastName;
        employee.AxtriaSalesIQTM__FirstName__c = firstName;
        employee.AxtriaSalesIQTM__Gender__c = 'M';
        return employee;
    }

    public static AxtriaSalesIQTM__Position_Employee__c createPositionEmployee(String positionId, String employeeId, String assignmentType, Date startDate, Date endDate) {
        AxtriaSalesIQTM__Position_Employee__c positionEmployee = new AxtriaSalesIQTM__Position_Employee__c();
        positionEmployee.AxtriaSalesIQTM__Assignment_Type__c = assignmentType;
        positionEmployee.AxtriaSalesIQTM__Position__c = positionId;
        positionEmployee.AxtriaSalesIQTM__Employee__c = employeeId;
        positionEmployee.AxtriaSalesIQTM__Status__c = 'Approved';
        positionEmployee.AxtriaSalesIQTM__Effective_start_date__c = startDate;
        positionEmployee.AxtriaSalesIQTM__Effective_End_Date__c = endDate;

        return positionEmployee;
    }

    public static AxtriaSalesIQTM__Change_Request__c createChangeRequest(String userId, Id sourcePositionId, Id destinationPositionId, Id teamInstanceId, String status) {
        AxtriaSalesIQTM__Change_Request__c objChangeRequest = new AxtriaSalesIQTM__Change_Request__c();
        objChangeRequest.AxtriaSalesIQTM__Approver1__c = userId;
        objChangeRequest.AxtriaSalesIQTM__Destination_Position__c = destinationPositionId;

        if (sourcePositionId != null)
            objChangeRequest.AxtriaSalesIQTM__Source_Position__c = sourcePositionId;

        objChangeRequest.AxtriaSalesIQTM__Change_String_Value__c = '11788';
        objChangeRequest.AxtriaSalesIQTM__Status__c = status;
        objChangeRequest.OwnerID = userId;
        objChangeRequest.AxtriaSalesIQTM__Team_Instance_ID__c = teamInstanceId;
        objChangeRequest.AxtriaSalesIQTM__scenario_name__c = 'Scenario';
        objChangeRequest.AxtriaSalesIQTM__Execution_Date__c = date.today();

        return objChangeRequest;
    }
    
    
    public static Product2 newprod(){
        Product2 pp= new Product2();pp.name= 'SnT';
        pp.AxtriaSalesIQTM__Effective_End_Date__c = date.today();
        pp.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        return pp;
    }
    
    public static Product_Catalog__c productCatalog(){
        Product_Catalog__c pcc= new Product_Catalog__c();
        pcc.Name='Test';
        pcc.Product_Code__c='abc';
        pcc.Veeva_External_ID__c='abc';
        pcc.External_ID__c = 'test';
        
        return pcc;
    }
    
    public static Report_Page_Configuration__c reportPageConfiguration(){
        Report_Page_Configuration__c rpc = new Report_Page_Configuration__c();
        rpc.Name='Test';
        rpc.Active__c = true;
        rpc.CurrencyIsoCode = 'USD';
        rpc.Report_Label__c= 'Test';
        rpc.Report_Name__c = 'Test';
        rpc.Type__c= 'Alignment';
        rpc.Country__c = true;
        rpc.Position__c= true;
        rpc.Brand__c = true;
        rpc.TeamInstance__c = true;
        return rpc;
    
    }
    
    public static AxtriaSalesIQTM__Team_Instance_Product__c teamInstanceProduct(){
        AxtriaSalesIQTM__Team_Instance_Product__c teaminsprod = new AxtriaSalesIQTM__Team_Instance_Product__c();
        //teaminsprod.Name='Test';
        teaminsprod.CurrencyIsoCode ='USD';
        return teaminsprod ;
    
    }
    
    
    public static Team_Instance_Product_AZ__c teamInstanceProductAZ(){
        Team_Instance_Product_AZ__c tipaz = new Team_Instance_Product_AZ__c();
        tipaz.Name='Test';
        tipaz.CurrencyIsoCode = 'USD'; 
        //tipaz.Effective_End_Date__c = date.today();
        //tipaz.Effective_Start_Date__c = date.today();
        //tipaz.Business_Days_in_Cycle_Formula__c = 'Test';
        //tipaz.Call_Capacity_Formula__c = 'Test';
        tipaz.Calls_Day__c = 1;
        //tipaz.Effective_Days_in_Field_Formula__c = 1;
        tipaz.Holidays__c = 1;
        tipaz.Other_Days_Off__c = 1 ;
        tipaz.Vacation_Days__c = 1;
        return tipaz;
    
    }
    
    public static AxtriaSalesIQTM__Product__c createProduct(){
        AxtriaSalesIQTM__Product__c product = new AxtriaSalesIQTM__Product__c();
        product.CurrencyIsoCode = 'USD'; 
        product.Name= 'Test';
        product.AxtriaSalesIQTM__Product_Code__c = 'TestForclass';
        product.AxtriaSalesIQTM__Effective_End_Date__c = date.today();
        product.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        product.AxtriaSalesIQTM__External_ID__c = 'text';
        return product;
    }
    
    public static AxtriaSalesIQTM__Position_Product__c createPositionProduct(){
        AxtriaSalesIQTM__Position_Product__c positionproduct = new AxtriaSalesIQTM__Position_Product__c();
        positionproduct.CurrencyIsoCode = 'USD'; 
        positionproduct.External_ID__c = 'Test';
        positionproduct.AxtriaSalesIQTM__Product_Weight__c = 1;
        positionproduct.AxtriaSalesIQTM__isActive__c = true;
        positionproduct.Business_Days_in_Cycle__c = 1;
        positionproduct.Calls_Day__c = 1;
        positionproduct.Holidays__c = 1;
        positionproduct.Other_Days_Off__c = 1;
        positionproduct.Vacation_Days__c = 1;
        positionproduct.AxtriaSalesIQTM__Product_Weight__c = 1;
        positionproduct.AxtriaSalesIQTM__Effective_End_Date__c = date.today();
        positionproduct.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        return positionproduct;
    }
    
    public static BU_Response__c createBuResponse(AxtriaSalesIQTM__Position_Account__c posAccount,Product_Catalog__c pcc, AxtriaSalesIQTM__Team_Instance__c teamins,AxtriaSalesIQTM__Team__c team, Account acc) {
        BU_Response__c buResponse = new BU_Response__c ();
        buResponse.Position_Account__c = posAccount.id;
        buResponse.CurrencyIsoCode='USD';
        buResponse.Product__c = pcc.id;
        buResponse.Line__c = team.id;
        buResponse.Physician__c = acc.id;
        for(integer i=1;i<=25;i++){
         buResponse.put('Response'+i+'__c','Test'+i);
         }
        buResponse.Team__c = team.id;
        buResponse.Team_Instance__c = teamins.id;
        return buResponse;
    }
    
    public static MetaData_Definition__c createMetaDataDefinition(AxtriaSalesIQTM__Team_Instance__c teamins,Product_Catalog__c pcc,AxtriaSalesIQTM__Team__c team){
        MetaData_Definition__c metaData = new MetaData_Definition__c();
        metaData.CurrencyIsoCode = 'USD';
        metaData.Source_Field__c = 'abc';
        metaData.Display_Name__c = 'abc';
        metaData.Type__c = 'text';
        metaData.Sequence__c = 1;
        metaData.Source_Object__c = 'BU_Response__c';
        metaData.Team_Instance__c = teamins.id;
        metaData.Product_Catalog__c = pcc.id;
        metaData.Team__c = team.id;
        return metaData;
    
    
    }
    
    public static AxtriaSalesIQTM__Team_Instance_Object_Attribute__c createTeamInstanceObjectAttribute(AxtriaSalesIQTM__Team_Instance__c teamins,Product_Catalog__c pcc){
        AxtriaSalesIQTM__Team_Instance_Object_Attribute__c teamInstanceObjectAttribute = new AxtriaSalesIQTM__Team_Instance_Object_Attribute__c();
        teamInstanceObjectAttribute.CurrencyIsoCode = 'USD';
        teamInstanceObjectAttribute.AxtriaSalesIQTM__Attribute_API_Name__c = 'Test';
        teamInstanceObjectAttribute.AxtriaSalesIQTM__Attribute_Display_Name__c = 'Test';
        teamInstanceObjectAttribute.AxtriaSalesIQTM__WrapperFieldMap__c = 'Test';
        teamInstanceObjectAttribute.AxtriaSalesIQTM__isEnabled__c = true;
        teamInstanceObjectAttribute.AxtriaSalesIQTM__isRequired__c = true;
        teamInstanceObjectAttribute.AxtriaSalesIQTM__Interface_Name__c = 'Call Plan';
        teamInstanceObjectAttribute.AxtriaSalesIQTM__Object_Name__c = 'Test';
        teamInstanceObjectAttribute.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        teamInstanceObjectAttribute.Brand_Lookup__c = pcc.id;
        return teamInstanceObjectAttribute;
    
    }
    
    public static Account_Compute_Final__c createComputeFinal(Measure_Master__c  mMaster, Account acc){
        Account_Compute_Final__c compFinal = new Account_Compute_Final__c();
        compFinal.CurrencyIsoCode = 'USD';
        compFinal.Measure_Master__c = mMaster.id;
        for(integer i=1;i<=50;i++){
         compFinal.put('Output_Value_'+i+'__c','Test'+i);
         }
        for(integer i=1;i<=50;i++){
         compFinal.put('Output_Name_'+i+'__c','Test'+i);
         }
        compFinal.Physician_2__c = acc.id;
        compFinal.Final_Segment__c = 'Test'; 
        compFinal.Calculated_TCF__c = 1;
        compFinal.Proposed_TCF__c = 1;
        compFinal.Final_TCF__c = 1;
        return compFinal;
    
    }
    @future
    public static void assignPermissionSet(Id userId, String permissionSetName) {
        PermissionSet ps = [SELECT Id, Name From PermissionSet WHERE Name =: permissionSetName];

        List < PermissionSetAssignment > existingPS = [SELECT Id from PermissionSetAssignment WHERE AssigneeId =: userId and PermissionSetId =: ps.Id];
        if (existingPS.isEmpty())
            insert new PermissionSetAssignment(AssigneeId = userId, PermissionSetId = ps.Id);
    }

    @future
    public static void createUserAccessPermissionFuture(String posId, String userObjId, String teamIntanceId, String permissionSetName) {
        PermissionSet ps = [SELECT Id, Name From PermissionSet WHERE Name =: permissionSetName];

        List < PermissionSetAssignment > existingPS = [SELECT Id from PermissionSetAssignment WHERE AssigneeId =: userObjId and PermissionSetId =: ps.Id];
        if (existingPS.isEmpty())
            insert new PermissionSetAssignment(AssigneeId = userObjId, PermissionSetId = ps.Id);

        AxtriaSalesIQTM__User_Access_Permission__c objUserAccessPerm = createUserAccessPerm();
        insert objUserAccessPerm;
    }

}