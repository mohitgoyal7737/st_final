public with sharing class Direct_Load_Records_Status_Update
{
    public static void Direct_Load_Records_Status_Update(String crId, String className)
    {
        List<temp_Obj__c> listOfRecords = [SELECT Id, Status__c FROM temp_Obj__c WHERE Change_Request__c = :crId];
        List<temp_Obj__c> listToUpdate = new List<temp_Obj__c>();
        for (Integer i = 0, j = listOfRecords.size(); i < j; i++)
        {
        	if(listOfRecords[i].Status__c != 'Processed' && listOfRecords[i].Status__c != 'Rejected'){
        		listOfRecords[i].put('Status__c', 'Rejected');
                listOfRecords[i].put('SalesIQ_Error_Message__c', 'Mandatory Field Missing or Incorrect Team Instance');
                listOfRecords[i].put('isError__c', true);
                listToUpdate.add(listOfRecords[i]);
        	}
        }
        
        

        if(listToUpdate.size() > 0)
        {
        	SnTDMLSecurityUtil.updateRecords(listToUpdate, className);
        }
        AxtriaSalesIQTM__Change_Request__c changerequest = new AxtriaSalesIQTM__Change_Request__c();
        Integer recordsProcessed = 0;
        recordsProcessed= [Select count() from temp_Obj__c where  Change_Request__c = :crId and isError__c =false];
        changerequest.Records_Updated__c = recordsProcessed;
        changerequest.ID = crId;
        //update changerequest;
        SnTDMLSecurityUtil.updateRecords(changerequest, 'Direct_Load_Records_Status_Update');
    }
}