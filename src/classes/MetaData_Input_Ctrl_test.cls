@isTest
public class MetaData_Input_Ctrl_test {
    @istest static void metadatatest()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        MetaData_Input__c m=new MetaData_Input__c();
        m.name='test';
        insert m;
        
         AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
        team.Name = 'GR Sales IMM';
        insert team;
        
         AxtriaSalesIQTM__Team_Instance__c teamIns = new AxtriaSalesIQTM__Team_Instance__c();
        teamIns.AxtriaSalesIQTM__Team__c = team.id;
        insert teamIns;
        
            AxtriaSalesIQTM__Team_Instance_Object_Attribute__c teamAtt = new AxtriaSalesIQTM__Team_Instance_Object_Attribute__c();
        teamAtt.AxtriaSalesIQTM__Team_Instance__c = teamIns.id;
        teamAtt.AxtriaSalesIQTM__Interface_Name__c = 'Call Plan';
        teamAtt.AxtriaSalesIQTM__isRequired__c = true;
        teamAtt.AxtriaSalesIQTM__isEnabled__c = true;
        teamAtt.AxtriaSalesIQTM__Attribute_Display_Name__c='Test1';
        teamAtt.AxtriaSalesIQTM__Attribute_API_Name__c ='Position_Account_Call_Plan__r.AxtriaSalesIQTM__Account__r.Name';
        teamAtt.AxtriaSalesIQTM__Object_Name__c = 'Position_Account_Call_Plan_Product__c';
        insert teamatt;     
       
        Test.startTest();
         System.runAs(loggedinuser){
        MetaData_Input_Ctrl obj=new MetaData_Input_Ctrl();
         }
        
        Test.stopTest();

    }
}