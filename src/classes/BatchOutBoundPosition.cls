global class BatchOutBoundPosition implements Database.Batchable<sObject>,Database.stateful,Schedulable  {
    public String query;
    public Integer recordsProcessed=0;
    public String batchID;
    String UnAsgnPos='00000';
    String UnAsgnPos1='0';
    public List<String> posCodeList=new List<String>();
    global DateTime lastjobDate=null;
    global Date today=Date.today();
    public map<String,String>Countrymap {get;set;}
    public map<String,String>mapVeeva2Mktcode {get;set;}
    public set<String> Uniqueset {get;set;}                                
    public set<String> mkt {get;set;} 
    public list<Custom_Scheduler__c> mktList {get;set;}                                       
    public List<AxtriaSalesIQTM__Position__c> nationPosition {get;set;}  
    public list<Custom_Scheduler__c> lsCustomSchedulerUpdate {get;set;} 
    public String cycle {get;set;}
    global string Country_1;
    public list<String> CountryList;
    global boolean flag=true;
    public  List<String> DeltaCountry ;

        
    
             
    global BatchOutBoundPosition(String Country1){
        Country_1=Country1;
        CountryList=new list<String>();
        if(Country_1.contains(','))
        {
            CountryList=Country_1.split(',');
        }
        else
        {
            CountryList.add(Country_1);
        }
        System.debug('<<<<<<<<<--Country List-->>>>>>>>>>>>'+CountryList);
        flag=false;
        posCodeList.add('0');
        posCodeList.add('00000');
        posCodeList.add('Unassigned');
        posCodeList.add('Unassigned Territory');
        mkt = new set<String>();
        mktList=new list<Custom_Scheduler__c>();
        mktList=Custom_Scheduler__c.getall().values();
        mapVeeva2Mktcode=new map<String,String>();
        Countrymap=new map<String,String>();
        Uniqueset = new set<String>();
        nationPosition =new List<AxtriaSalesIQTM__Position__c>();
        
        for(AxtriaSalesIQTM__Country__c country: [select id,Name,AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c]){
            if(!Countrymap.containskey(country.name)){
                Countrymap.put(country.name,country.AxtriaSalesIQTM__Country_Code__c);
            }
        }

        ////Added by Ayushi 07-09-2018
        for(AxtriaARSnT__SIQ_MC_Country_Mapping__c countrymap: [select id,Name,AxtriaARSnT__SIQ_Veeva_Country_Code__c,AxtriaARSnT__SIQ_MC_Code__c from AxtriaARSnT__SIQ_MC_Country_Mapping__c]){
            if(!mapVeeva2Mktcode.containskey(countrymap.AxtriaARSnT__SIQ_Veeva_Country_Code__c)){
                mapVeeva2Mktcode.put(countrymap.AxtriaARSnT__SIQ_Veeva_Country_Code__c,countrymap.AxtriaARSnT__SIQ_MC_Code__c);
            }
        }

        query = 'Select Id,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.AxtriaARSnT__Marketing_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,AxtriaSalesIQTM__Team_Instance__c,CreatedDate,Event__c,Salesforce_Name__c,Name,POSITION_CODE__c,PARENT_POSITION_NAME__c,AxtriaSalesIQTM__Role__c,AxtriaSalesIQTM__Team_Instance__r.Name,LastModifiedDate,AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Position_Code__c, ' +
                'PARENT_POSITION_CODE__c,AxtriaSalesIQTM__Position_Type__c,AxtriaSalesIQTM__Hierarchy_Level__c,AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_iD__r.Name,AxtriaSalesIQTM__Parent_Position__r.Name,Partner_Flag__c,Sales_Team_Attribute__c,Position_Level__c,POSITION_ROLE__c,AxtriaSalesIQTM__Team_iD__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.name, ' +
                'SALES_TEAM_CODE__c,Team_Instance__c,Updated_Date__c,Channel_ID_AZ__c,Channel_AZ__c,DeveloperName__c,AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaARSnT__Sales_Team_Attribute_MS__c, ' +
                'AxtriaSalesIQTM__Effective_End_Date__c,Position_Description__c,Position_GEO1__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=\'current\' and (AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c=\'Live\' or AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c=\'Published\') and AxtriaSalesIQTM__Client_Position_Code__c not in: posCodeList and AxtriaSalesIQTM__Team_Instance__r.Country_Name__c IN :CountryList AND AxtriaSalesIQTM__Position_Type__c != null and AxtriaSalesIQTM__inactive__c = false ';
        
        System.debug('====================query'+ query);
        String query2='Select Id,Marketing_Code__c,Country_Code__c,CreatedDate,Event__c,Salesforce_Name__c,Name,POSITION_CODE__c,PARENT_POSITION_NAME__c,AxtriaSalesIQTM__Role__c,AxtriaSalesIQTM__Team_Instance__r.Name,LastModifiedDate,AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaARSnT__Sales_Team_Attribute_MS__c, ' +
                       'PARENT_POSITION_CODE__c,AxtriaSalesIQTM__Hierarchy_Level__c,AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Position_Type__c,AxtriaSalesIQTM__Team_iD__r.Name,AxtriaSalesIQTM__Parent_Position__r.Name,Partner_Flag__c,Sales_Team_Attribute__c,Position_Level__c,POSITION_ROLE__c,SALES_TEAM_CODE__c, ' +
                       'Team_Instance__c,Updated_Date__c,Channel_AZ__c,Channel_ID_AZ__c,DeveloperName__c,AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Team_Instance__r.Team_Instance_Category__c,AxtriaSalesIQTM__Team_iD__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__c, ' +
                       'AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.Marketing_Code__c,Position_Description__c,Position_GEO1__c from AxtriaSalesIQTM__Position__c ' +
                       'where (AxtriaSalesIQTM__Parent_Position__c=null) and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=\'Current\' and (AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c=\'Live\' or AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c=\'Published\') and AxtriaSalesIQTM__Client_Position_Code__c not in: posCodeList and AxtriaSalesIQTM__Team_Instance__r.Country_Name__c IN :CountryList AND AxtriaSalesIQTM__Position_Type__c != null ';
        
        System.debug('====================query2'+ query2);
        nationPosition=database.query(query2);
            
    }                                                                   
    
    global BatchOutBoundPosition() {
        posCodeList.add('0');
        posCodeList.add('00000');
        posCodeList.add('Unassigned');
        posCodeList.add('Unassigned Territory');
        mkt = new set<String>();
        mktList=new list<Custom_Scheduler__c>();
        mktList=Custom_Scheduler__c.getall().values();
        lsCustomSchedulerUpdate = new list<Custom_Scheduler__c>();
        
        for(Custom_Scheduler__c cs:mktList){
          if(cs.Status__c==true && cs.Schedule_date__c!=null){
            if(cs.Schedule_date__c>today.addDays(1)){
               mkt.add(cs.Marketing_Code__c);
            }else{
               //update Custom scheduler record
               cs.Status__c = False;
               lsCustomSchedulerUpdate.add(cs);
            }
          }
        }
        List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
        List<AxtriaSalesIQTM__Team_Instance__c> cycleList = new List<AxtriaSalesIQTM__Team_Instance__c>();
        cycleList=[Select Name,Cycle__r.Name from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current' and (AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c='Live' or AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c='Published')];
        if(cycleList!=null){
            for(AxtriaSalesIQTM__Team_Instance__c t1: cycleList)
            {
                if(t1.Cycle__r.Name !=null && t1.Cycle__r.Name !='')
                    cycle = t1.Cycle__r.Name;
            }
            
        }
           //cycle=cycleList.get(0).Name;
           //cycle=cycle.substring(cycle.length() - 3);
         
        Countrymap = new map<String,String>();
        mapVeeva2Mktcode = new map<String,String>();
        Uniqueset = new set<String>();                            
        nationPosition =new List<AxtriaSalesIQTM__Position__c>();
        schLogList=[Select Id,CreatedDate,Created_Date2__c from Scheduler_Log__c where Job_Name__c='OutBound Position Delta' and Job_Status__c='Successful' Order By Created_Date2__c desc];
        if(schLogList.size()>0){
            lastjobDate=schLogList[0].Created_Date2__c;  
        }
        else{
            lastjobDate=null;
        }
        System.debug('================last job'+lastjobDate);
        for(AxtriaSalesIQTM__Country__c country: [select id,Name,AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c]){
            if(!Countrymap.containskey(country.name)){
                Countrymap.put(country.name,country.AxtriaSalesIQTM__Country_Code__c);
            }
        }

        ////Added by Ayushi 07-09-2018
        for(AxtriaARSnT__SIQ_MC_Country_Mapping__c countrymap: [select id,Name,AxtriaARSnT__SIQ_Veeva_Country_Code__c,AxtriaARSnT__SIQ_MC_Code__c from AxtriaARSnT__SIQ_MC_Country_Mapping__c]){
            if(!mapVeeva2Mktcode.containskey(countrymap.AxtriaARSnT__SIQ_Veeva_Country_Code__c)){
                mapVeeva2Mktcode.put(countrymap.AxtriaARSnT__SIQ_Veeva_Country_Code__c,countrymap.AxtriaARSnT__SIQ_MC_Code__c);
            }
        }

        //Till here..
        
        Scheduler_Log__c sJob = new Scheduler_Log__c();
        
        sJob.Job_Name__c = 'OutBound Position Delta';
        sJob.Job_Status__c = 'Failed';
        sJob.Job_Type__c='Outbound';
         if(cycle!=null && cycle!='')
         sJob.Cycle__c=cycle;
        sJob.Created_Date2__c = DateTime.now();
        insert sJob;
        batchID = sJob.Id;
        recordsProcessed =0;

        DeltaCountry = new List<String>();
        DeltaCountry = StaticTeaminstanceList.getSFEDeltaCountries();

        System.debug('>>>>>>>>>>>>>>>>>>>>>>DeltaCountry>>>>>>>>>>>>>>>>>>>'+DeltaCountry);
        
        //this.query = query; 
        //SIQ_Country_Code__c,SIQ_Customer_Class__c,SIQ_Event__c,SIQ_Marketing_Code__c,SIQ_Salesforce_Name__c,SIQ_Segment_Type__c ----???????

        query = 'Select Id,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.AxtriaARSnT__Marketing_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,AxtriaSalesIQTM__Team_Instance__c,CreatedDate,Event__c,Salesforce_Name__c,Name,POSITION_CODE__c,PARENT_POSITION_NAME__c,AxtriaSalesIQTM__Role__c,AxtriaSalesIQTM__Team_Instance__r.Name,LastModifiedDate,AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Position_Code__c, ' +
                'PARENT_POSITION_CODE__c,AxtriaSalesIQTM__Position_Type__c,AxtriaSalesIQTM__Hierarchy_Level__c,AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_iD__r.Name,AxtriaSalesIQTM__Parent_Position__r.Name,Partner_Flag__c,Sales_Team_Attribute__c,Position_Level__c,POSITION_ROLE__c,AxtriaSalesIQTM__Team_iD__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.name, ' +
                'SALES_TEAM_CODE__c,Team_Instance__c,Updated_Date__c,Channel_ID_AZ__c,Channel_AZ__c,DeveloperName__c,AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaARSnT__Sales_Team_Attribute_MS__c, ' +
                'AxtriaSalesIQTM__Effective_End_Date__c,Position_Description__c,Position_GEO1__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=\'current\' and (AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c=\'Live\' or AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c=\'Published\') and AxtriaSalesIQTM__Client_Position_Code__c not in: posCodeList AND AxtriaSalesIQTM__Position_Type__c != null and AxtriaSalesIQTM__Team_Instance__r.Country_Name__c IN :DeltaCountry and AxtriaSalesIQTM__inactive__c = false ';
        if(lastjobDate!=null){
            query = query + ' and LastModifiedDate  >=:  lastjobDate '; 
        }
         if(mkt.size()>0){
            query=query + ' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c not in: mkt ' ;
        }
        System.debug('====================query'+ query);
        String query2='Select Id,Marketing_Code__c,Country_Code__c,CreatedDate,Event__c,Salesforce_Name__c,Name,POSITION_CODE__c,PARENT_POSITION_NAME__c,AxtriaSalesIQTM__Role__c,AxtriaSalesIQTM__Team_Instance__r.Name,LastModifiedDate,AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaARSnT__Sales_Team_Attribute_MS__c, ' +
                       'PARENT_POSITION_CODE__c,AxtriaSalesIQTM__Hierarchy_Level__c,AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Position_Type__c,AxtriaSalesIQTM__Team_iD__r.Name,AxtriaSalesIQTM__Parent_Position__r.Name,Partner_Flag__c,Sales_Team_Attribute__c,Position_Level__c,POSITION_ROLE__c,SALES_TEAM_CODE__c, ' +
                       'Team_Instance__c,Updated_Date__c,Channel_AZ__c,Channel_ID_AZ__c,DeveloperName__c,AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Team_Instance__r.Team_Instance_Category__c,AxtriaSalesIQTM__Team_iD__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__c, ' +
                       'AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.Marketing_Code__c,Position_Description__c,Position_GEO1__c from AxtriaSalesIQTM__Position__c ' +
                       'where (AxtriaSalesIQTM__Parent_Position__c=null) and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=\'Current\' and (AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c=\'Live\' or AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c=\'Published\') and AxtriaSalesIQTM__Client_Position_Code__c not in: posCodeList AND AxtriaSalesIQTM__Position_Type__c != null and AxtriaSalesIQTM__Team_Instance__r.Country_Name__c IN :DeltaCountry and AxtriaSalesIQTM__inactive__c = false  ';
        
        //AxtriaSalesIQTM__Position_Type__c=\'Nation\' or AxtriaSalesIQTM__Position_Type__c=\'NSM\'
        if(lastjobDate!=null){
            query2 = query2 + ' and LastModifiedDate  >=:  lastjobDate '; 
        }
        if(mkt.size()>0){
            query2=query2 + ' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c not in: mkt ' ;
        }                                                                                                                                                                                                                                                                                                             
                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
         System.debug('=====================Nation QUERY IS::::'+query2);                                                                                                                                                                                                                                                                                                                                                                                                                                                        
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
      
        nationPosition=database.query(query2);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
      
        //Update custom scheduler
        if(lsCustomSchedulerUpdate.size()!=0){
            update lsCustomSchedulerUpdate;
        }      
    }

    
   
   global void execute(System.SchedulableContext SC){
        database.executeBatch(new BatchOutBoundPosition());
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position__c> scope) {
        list<SIQ_Position_O__c>PositionList = new list<SIQ_Position_O__c>();
        Uniqueset = new set<String>();
        String code = ' ';
        for(AxtriaSalesIQTM__Position__c pos : scope)
        {
            //String key = pos.AxtriaSalesIQTM__Client_Position_Code__c+'_'+pos.Team_Instance__c;
            String Childcode=pos.AxtriaSalesIQTM__Client_Position_Code__c;
            string parentcode = '';
            if(pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Position_Code__c != null)
            {                               
                parentcode=pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
            }
            if(pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Position_Code__c==null)
            {
                parentcode=pos.AxtriaSalesIQTM__Team_iD__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.name;
            }
            String key = Childcode+'_'+parentcode+'_'+pos.AxtriaSalesIQTM__Team_Instance__r.Name;
            system.debug('==================Key is:::::'+key);
            if(!Uniqueset.contains(Key))
            {
                SIQ_Position_O__c obj = new SIQ_Position_O__c();                            
                //obj.SIQ_Marketing_Code__c=pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.AxtriaARSnT__Marketing_Code__c;
                obj.SIQ_Country_Code__c=pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                code = pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                //Added by Ayushi
                if(mapVeeva2Mktcode.get(code) != null){
                    obj.SIQ_Marketing_Code__c = mapVeeva2Mktcode.get(code);
                }
                else{
                    obj.SIQ_Marketing_Code__c = 'MC code does not exist';
                }
                //Till here..
                obj.SIQ_Event__c='Insert';
                //obj.SIQ_Event__c=pos.Event__c;
                obj.SIQ_Salesforce_Name__c=pos.AxtriaSalesIQTM__Team_iD__r.Name;
                //obj.Name=pos.Name;
                obj.SIQ_POSITION_CODE__c=pos.AxtriaSalesIQTM__Client_Position_Code__c;
                obj.SIQ_POSITION_NAME__c=pos.AxtriaSalesIQTM__Client_Position_Code__c ;  
                if(pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Position_Code__c != null)
                {                               
                    obj.SIQ_PARENT_POSITION_NAME__c=pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
                    obj.SIQ_PARENT_POSITION_CODE__c=pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
                }
                if(pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Position_Code__c==null)
                {
                    obj.SIQ_PARENT_POSITION_NAME__c=pos.AxtriaSalesIQTM__Team_iD__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.name;
                    obj.SIQ_PARENT_POSITION_CODE__c=pos.AxtriaSalesIQTM__Team_iD__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.name;
                }
                obj.SIQ_Partner_Flag__c='N';
                obj.SIQ_Channel__c=pos.Channel_AZ__c;      
                obj.SIQ_Sales_Team_Attribute__c=pos.AxtriaARSnT__Sales_Team_Attribute_MS__c;
                obj.SIQ_Position_Level__c=pos.AxtriaSalesIQTM__Position_Type__c;
                obj.SIQ_POSITION_ROLE__c=pos.AxtriaSalesIQTM__Role__c;
                obj.SIQ_SALES_TEAM_CODE__c=pos.AxtriaSalesIQTM__Team_iD__r.Name;
                obj.SIQ_Team_Instance__c=pos.AxtriaSalesIQTM__Team_Instance__r.Name;
                obj.SIQ_Position_Description__c =pos.Position_Description__c;
                obj.SIQ_position_GEO1__c=pos.Position_GEO1__c;
                obj.SIQ_Updated_Date__c=pos.LastModifiedDate;
                obj.SIQ_Channel_ID_AZ__c=pos.Channel_AZ__c;
                obj.SIQ_Created_Date__c=pos.CreatedDate;
                // obj.SIQ_DeveloperName__c=pos.DeveloperName__c;
                obj.SIQ_Effective_Start_Date__c=pos.AxtriaSalesIQTM__Effective_Start_Date__c;
                obj.SIQ_Effective_End_Date__c=pos.AxtriaSalesIQTM__Effective_End_Date__c;
                // obj.Unique_Id__c=pos.AxtriaSalesIQTM__Client_Position_Code__c+'_'+pos.AxtriaSalesIQTM__Team_Instance__c;
                obj.Unique_Id__c=key;
                PositionList.add(obj);
                recordsProcessed++;
                Uniqueset.add(Key);                    
            } 
        }
        Upsert PositionList Unique_Id__c;
        
    }

    global void finish(Database.BatchableContext BC) {
        System.debug(recordsProcessed + ' records processed. ');
        System.debug('*****nationPosition'+nationPosition);


        OutboundPositionProcessing opp=new OutboundPositionProcessing(nationPosition,recordsProcessed,batchID);
        Database.executeBatch(new BatchOutboundPosEventUpdate(), 2000);                                                

        if(flag)
        {
        Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID); 
        system.debug('schedulerObj++++before'+sJob);
        //Update the scheduler log with successful
        //OutboundPositionProcessing opp=new OutboundPositionProcessing(nationPosition,recordsProcessed,batchID);                                                
                                                                                                               
        sJob.No_Of_Records_Processed__c=recordsProcessed;
        sJob.Job_Status__c='Successful';
        system.debug('sJob++++++++'+sJob);
        update sJob;
       //Database.ExecuteBatch( new BatchOutBoundPositionProduct(),200);
        
        Set<String> updMkt = new set<String>();
         for(Custom_Scheduler__c cs:mktList){
           if(cs.Status__c==true && cs.Schedule_date__c!=null){
             if(cs.Schedule_date__c==today.addDays(2)){
                updMkt.add(cs.Marketing_Code__c);
             }
            }
          }
         if(updMkt.size()>0){
            List<AxtriaSalesIQTM__Position__c> posList=[Select Id FROM AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c in: updMkt]; 
            update posList;
        }
        }
    }
}