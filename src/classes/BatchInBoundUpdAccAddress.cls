global class BatchInBoundUpdAccAddress implements Database.Batchable<sObject>, Database.Stateful,schedulable{
    public Integer recordsProcessed=0;
    public String batchID;
    global DateTime lastjobDate=null;
    public static set<id> accountId=new set<id>();     
    global String query;
    public map<String, String> mapCountryCode ;
    public set<String> AclList {get;set;}
    public set<id> AddAclList {get;set;}
    public map<String,String>Accnoid {get;set;}
    Public String exceptionsAccountNumbers;
    public integer errorcount{get;set;}
    public list<AxtriaSalesIQTM__Country__c>countrylist {get;set;} 
    public String cycle {get;set;}
    public map<String, String> mapVeevaCode ;
    public map<String, String> mapMarketingCode ;
    public map<string,ClusterCountry__c>clustermap ;

     
    global BatchInBoundUpdAccAddress ()
    { //set<String> Accountid
      errorcount = 0;
      exceptionsAccountNumbers = '';
      AddAclList=new set<id>();
      mapCountryCode = new map<String, String>();
      mapVeevaCode=new map<String, String>();
      mapMarketingCode =new map<String, String>();
      clustermap = new map<string,ClusterCountry__c>();
      clustermap = ClusterCountry__c.getAll();


      countrylist=[Select Id,AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c];
      for(AxtriaSalesIQTM__Country__c c : countrylist)
      {
          if(!mapCountryCode.containskey(c.AxtriaSalesIQTM__Country_Code__c))
          {
              mapCountryCode.put(c.AxtriaSalesIQTM__Country_Code__c,c.id);
          }
      }
      
      list<SIQ_MC_Country_Mapping__c> countryMap=[Select Id,SIQ_MC_Code__c,SIQ_Veeva_Country_Code__c,SIQ_Country_Code__c from SIQ_MC_Country_Mapping__c];
      for(SIQ_MC_Country_Mapping__c c : countryMap)
      {
          if(!mapVeevaCode.containskey(c.SIQ_Country_Code__c))
          {
              mapVeevaCode.put(c.SIQ_Country_Code__c,c.SIQ_Veeva_Country_Code__c);
          }
          if(!mapMarketingCode.containskey(c.SIQ_Country_Code__c))
          {
              mapMarketingCode.put(c.SIQ_Country_Code__c,c.SIQ_MC_Code__c);
          }
      } 
      List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
      List<AxtriaSalesIQTM__Team_Instance__c> cycleList = new List<AxtriaSalesIQTM__Team_Instance__c>();
      cycleList=[Select Name, Cycle__r.Name from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current'];
      if(cycleList!=null)
      {
        for(AxtriaSalesIQTM__Team_Instance__c t1: cycleList)
        {
            if(t1.Cycle__r.Name !=null && t1.Cycle__r.Name !='')
            cycle = t1.Cycle__r.Name;
        }
       }
         
         //String cycle=cycleList.get(0).Name;
         //cycle=cycle.substring(cycle.length() - 3);
        schLogList=[Select Id,CreatedDate,Created_Date2__c from Scheduler_Log__c where Job_Name__c='Address Delta' and Job_Status__c='Successful' Order By Created_Date2__c desc];
        if(schLogList.size()>0)
        {
          lastjobDate=schLogList[0].Created_Date2__c;  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
        else
        {
          lastjobDate=null;       //else we set the lastjobDate to null
        }
        System.debug('last job'+lastjobDate);
        //Last Batch run ID
        Scheduler_Log__c sJob = new Scheduler_Log__c();

        sJob.Job_Name__c = 'Address Delta';
        sJob.Job_Status__c = 'Failed';
        sJob.Job_Type__c='Inbound';
        if(cycle!=null && cycle!='')
          sJob.Cycle__c=cycle;
        sJob.Created_Date2__c = DateTime.now();

        insert sJob;
        batchID = sJob.Id;

        recordsProcessed =0;
       query ='SELECT CreatedDate,CurrencyIsoCode, ' + 
        'Id,LastModifiedById,LastModifiedDate,Name,OwnerId,SIQ_Account_Number__c,SIQ_Address_Id__c, ' +
        'SIQ_Address_Type__c,SIQ_Billing_Address__c,SIQ_Billing_City__c,SIQ_Billing_Country__c, ' +
        'SIQ_Billing_State_Province__c,SIQ_External_ID__c,SIQ_Geography_Identifier__c, ' +
        'SIQ_Last_Modified_Date__c,SIQ_Latitude__c,SIQ_Longitude__c,SIQ_Mini_Brick__c, ' +
        'SIQ_Postal_Code_in_Customer_Address__c,SIQ_Primary_Address_Indicator__c, ' +
        'SIQ_Status__c,SystemModstamp ' +
        'FROM SIQ_Account_Address__c ' ;
        
        if(lastjobDate!=null){
          query = query + 'Where LastModifiedDate  >=:  lastjobDate order by SIQ_Last_Modified_Date__c DESC NULLS LAST '; 
        }
        System.debug('query'+ query);
    
          
    }
    
    
   global Database.QueryLocator  start(Database.BatchableContext bc) {
         return Database.getQueryLocator(query);
    }
     public void execute(System.SchedulableContext SC){
         database.executeBatch(new BatchInBoundUpdAccAddress());
       
    }
    global void execute(Database.BatchableContext bc, List<SIQ_Account_Address__c> records)
    {
        // process each batch of records
      AclList = new Set<String>();
      map<String,String>AccId = new Map<String,String>();
      set<string>uniqueid = new set<string>();
      set<string>clusteraccid = new set<string>();
      map<string,string>clusteraccmap = new map<string,string>();
      set<string>cuniq = new set<string>();

      for(SIQ_Account_Address__c SAA : records)
      {
          AclList.add(SAA.SIQ_Account_Number__c);
          if(clustermap.containskey(SAA.SIQ_Billing_Country__c))
          {
              String clustercode='';
              ClusterCountry__c cc = clustermap.get(SAA.SIQ_Billing_Country__c);
              clustercode = cc.Country_Code__c;
              String key=clustercode+'_'+SAA.SIQ_Account_Number__c;
              clusteraccid.add(key);
          }
         // AclList.add(SAA.SIQ_Parent_Account_Number__c);
      }
      System.debug('AclList1'+AclList); 
      system.debug('==============clusteraccid=='+clusteraccid);

      List<Account>AccountList = [select id,AccountNumber,External_Account_Id__c from Account where AccountNumber IN :AclList]; 
      for(Account Acc : AccountList)
      {
          if(!AccId.containsKey(Acc.AccountNumber))
          {
              AccId.put(Acc.AccountNumber,Acc.id);
          }
      } 
      System.debug('AclList2'+AccId);
      /*Cluster handling=================*/
      if(clusteraccid!=null)
      {
        List<Account> clusterAccList = [select Id,AccountNumber, External_Account_Id__c,Cluster_Unique__c from Account where Cluster_Unique__c in :clusteraccid];
        if(clusterAccList!=null && clusterAccList.size() > 0)
        {
          for(Account tempAcc : clusterAccList)
          {
            clusteraccmap.put(tempAcc.Cluster_Unique__c,tempAcc.Id);
          }
        }
      }




      List<AxtriaSalesIQTM__Account_Address__c> addresses = new List<AxtriaSalesIQTM__Account_Address__c>();
      for (SIQ_Account_Address__c acc : records)
      {
        //if(acc.SIQ_Parent_Account_Number__c!=''){
        if(AccId.containsKey(acc.SIQ_Account_Number__c) && !uniqueid.contains(acc.SIQ_External_ID__c))
        {
          AxtriaSalesIQTM__Account_Address__c address=new AxtriaSalesIQTM__Account_Address__c();
          address.AxtriaSalesIQTM__Account__c=AccId.get(acc.SIQ_Account_Number__c);
          address.Name =acc.Name;
          //address.Account_Number__c = acc.SIQ_Account_Number__c;                          
          address.CurrencyIsoCode=acc.CurrencyIsoCode;
          address.Address_Id__c=acc.SIQ_Address_Id__c;
          // address.AxtriaSalesIQTM__Address_Type__c=acc.SIQ_Address_Type__c;
          if(acc.SIQ_Primary_Address_Indicator__c!=null)
          {
            if(acc.SIQ_Primary_Address_Indicator__c.equals('Y'))
            {
             address.AxtriaSalesIQTM__Address_Type__c='Primary';
             address.AxtriaSalesIQTM__Is_Primary__c=true;
            }
            else if(acc.SIQ_Primary_Address_Indicator__c.equals('N'))
            {
            address.AxtriaSalesIQTM__Address_Type__c='Secondary';
            address.AxtriaSalesIQTM__Is_Primary__c=false;
            }
          }
          else
          {
            address.AxtriaSalesIQTM__Address_Type__c='Secondary';
            address.AxtriaSalesIQTM__Is_Primary__c=false;
          }
          address.AxtriaSalesIQTM__City__c=acc.SIQ_Billing_City__c;
          if(mapVeevaCode.get(acc.SIQ_Billing_Country__c) !=null)
          {
            address.AxtriaSalesIQTM__Country__c=mapCountryCode.get(mapVeevaCode.get(acc.SIQ_Billing_Country__c));
          }
          else
          {
            address.AxtriaSalesIQTM__Country__c=null;
          }
          //address.AxtriaSalesIQTM__Country__c=mapCountryCode.get(acc.SIQ_Billing_Country__c);
          address.AxtriaSalesIQTM__Pincode__c=acc.SIQ_Mini_Brick__c;
          address.AxtriaSalesIQTM__State__c=acc.SIQ_Billing_State_Province__c;
          address.AxtriaSalesIQTM__Street__c=acc.SIQ_Billing_Address__c;
          address.External_Id__c=acc.SIQ_External_ID__c;
          address.Geography_Identifier__c=acc.SIQ_Geography_Identifier__c;
          address.Last_Modified_Date__c=acc.SIQ_Last_Modified_Date__c;
          address.Postal_Code_in_Customer_Address__c = acc.SIQ_Postal_Code_in_Customer_Address__c;
          // if(acc.SIQ_Latitude__c!=null){
          //address.AxtriaSalesIQTM__Latitude__c=decimal.valueOf(acc.SIQ_Latitude__c);
          //}
          //else
          //{
          //address.AxtriaSalesIQTM__Latitude__c=0;
          //}
          //if(acc.SIQ_Latitude__c!=null){
          //address.AxtriaSalesIQTM__Longitude__c =decimal.valueOf(acc.SIQ_Longitude__c);
          //}
          //else
          //{
          //address.AxtriaSalesIQTM__Longitude__c=0;
          //}
          address.Mini_Brick__c =acc.SIQ_Mini_Brick__c;
          address.Primary_Address_Indicator__c =acc.SIQ_Primary_Address_Indicator__c;
          // address.Publish_Date__c =acc.SIQ_Publish_Date__c;
          // address.Publish_Event__c =acc.SIQ_Publish_Event__c;
          address.AxtriaSalesIQTM__Status__c  =acc.SIQ_Status__c;
          address.AxtriaARSnT__Status__c = acc.SIQ_Status__c;
          addresses.add(address);
          system.debug('recordsProcessed+'+recordsProcessed);
          recordsProcessed++;
          uniqueid.add(acc.SIQ_External_ID__c);
          //comments
        }
        else
        {
          exceptionsAccountNumbers +=acc.SIQ_Account_Number__c+';';
          errorcount ++;
        }


        if(clustermap.containskey(acc.SIQ_Billing_Country__c))
        {
          String ccode='';
          ClusterCountry__c cc = clustermap.get(acc.SIQ_Billing_Country__c);
          ccode = cc.Country_Code__c;
          String key=ccode+'_'+acc.SIQ_Account_Number__c;
          if(clusteraccmap.containskey(key) && !cuniq.contains(key))
          {
            AxtriaSalesIQTM__Account_Address__c address=new AxtriaSalesIQTM__Account_Address__c();
            address.AxtriaSalesIQTM__Account__c=clusteraccmap.get(key);
            address.Name =acc.Name;
            address.CurrencyIsoCode=acc.CurrencyIsoCode;
            address.Address_Id__c=acc.SIQ_Address_Id__c;
            if(acc.SIQ_Primary_Address_Indicator__c!=null)
            {
              if(acc.SIQ_Primary_Address_Indicator__c.equals('Y'))
              {
                address.AxtriaSalesIQTM__Address_Type__c='Primary';
                address.AxtriaSalesIQTM__Is_Primary__c=true;
              }
              else if(acc.SIQ_Primary_Address_Indicator__c.equals('N'))
              {
                address.AxtriaSalesIQTM__Address_Type__c='Secondary';
                address.AxtriaSalesIQTM__Is_Primary__c=false;
              }
            }
            else
            {
              address.AxtriaSalesIQTM__Address_Type__c='Secondary';
              address.AxtriaSalesIQTM__Is_Primary__c=false;
            }
            address.AxtriaSalesIQTM__City__c=acc.SIQ_Billing_City__c;
            system.debug('=============Cluster Country code is:::::'+ccode);
            if(mapVeevaCode.get(ccode) !=null)
            {
              address.AxtriaSalesIQTM__Country__c=mapCountryCode.get(mapVeevaCode.get(ccode));
            }
            else
            {
              address.AxtriaSalesIQTM__Country__c=null;
            }
            address.AxtriaSalesIQTM__Pincode__c=acc.SIQ_Mini_Brick__c;
            address.AxtriaSalesIQTM__State__c=acc.SIQ_Billing_State_Province__c;
            address.AxtriaSalesIQTM__Street__c=acc.SIQ_Billing_Address__c;
            address.External_Id__c=key;
            address.Geography_Identifier__c=acc.SIQ_Geography_Identifier__c;
            address.Last_Modified_Date__c=acc.SIQ_Last_Modified_Date__c;
            address.Postal_Code_in_Customer_Address__c = acc.SIQ_Postal_Code_in_Customer_Address__c;
            address.Mini_Brick__c =acc.SIQ_Mini_Brick__c;
            address.Primary_Address_Indicator__c =acc.SIQ_Primary_Address_Indicator__c;
            address.AxtriaSalesIQTM__Status__c  =acc.SIQ_Status__c;
            address.Status__c = acc.SIQ_Status__c;
            addresses.add(address);
            cuniq.add(address.External_Id__c);
          }
          
        }

      }
      upsert addresses External_Id__c;

      
      /*Schema.SObjectField f = AxtriaSalesIQTM__Account_Address__c.Fields.External_Id__c;
      database.upsert(addresses ,f,false);*/
      
      System.debug('addresses..'+addresses);
      /*for(AxtriaSalesIQTM__Account_Address__c add:addresses )
      {
      AddAclList.add(add.AxtriaSalesIQTM__Account__c);
      }*/
    }     
    global void finish(Database.BatchableContext bc){
      // execute any post-processing operations
      set<String> acIds=new set<String>();
      List<Account> updatelist = new list<Account>();
      List<Account> updateAccList=new list<Account>();
      System.debug('AddAclList'+AddAclList);                    
      String msg = 'Error Records:'+errorcount+'::::'+exceptionsAccountNumbers;                                     
      System.debug(recordsProcessed + ' records processed. ');
      Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID); 
      system.debug('schedulerObj++++before'+sJob);
      //Update the scheduler log with successful
      sjob.Object_Name__c = 'Account Address';
      //sjob.Changes__c                                       
           
      sJob.No_Of_Records_Processed__c=recordsProcessed;
      sJob.Job_Status__c='Successful';
      system.debug('error message'+msg);
      //sjob.Changes__c = msg;                    

      system.debug('sJob++++++++'+sJob);
      update sJob;
      //Database.executeBatch(new BatchUpdAccAfterTrigger(AddAclList));                            
        
    }   
}