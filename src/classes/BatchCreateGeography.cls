global class BatchCreateGeography  implements Database.Batchable<sObject>,Database.stateful,Schedulable {
    
   List<AxtriaSalesIQTM__Country__c> countryList = null;
    Public String accAddressQuery;
    public String batchID;
    global DateTime lastjobDate=null;
    public Integer recordsProcessed=0;
    
    global BatchCreateGeography(){
        
        List<Scheduler_Log__c> schLog = new List<Scheduler_log__c>();
        schLog = [Select Id,CreatedDate,Created_Date2__c from Scheduler_Log__c where Job_Name__c='Create Geography Delta' and Job_Status__c='Successful' Order By Created_Date2__c desc];
        if(schLog.size() > 0)
        {
          lastjobDate=schLog[0].Created_Date2__c;  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
        System.debug('last job'+lastjobDate);
        
        Scheduler_Log__c sJob = new Scheduler_Log__c();
        sJob.Job_Name__c = 'Create Geography Delta';
        sJob.Job_Status__c = 'Failed';
        sJob.Job_Type__c='Inbound';
        sJob.Created_Date2__c = DateTime.now();
        insert sJob;
        batchID = sJob.Id;
        
        accAddressQuery = 'SELECT Id, AxtriaSalesIQTM__Country__c, AxtriaSalesIQTM__Pincode__c FROM AxtriaSalesIQTM__Account_Address__c';
         
        if(lastjobDate!=null){
          accAddressQuery = accAddressQuery + ' Where CreatedDate  >=:  lastjobDate '; 
        }
        countryList = [SELECT Id, AxtriaSalesIQTM__Country_Code__c FROM AxtriaSalesIQTM__Country__c];

        system.debug('===============The query is:::'+accAddressQuery);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(accAddressQuery);

    }
    
    global void execute(Database.BatchableContext bc, List<sObject> scope){
        System.debug('Enter in execute method');
        
        List<AxtriaSalesIQTM__Account_Address__c> accountAddressList = new List<AxtriaSalesIQTM__Account_Address__c>();
        for(SObject obj: scope){
            accountAddressList.add((AxtriaSalesIQTM__Account_Address__c)obj);
        }
        
        System.debug('size of countryList ' + countryList.size());
        System.debug('size of accountAddressList ' + accountAddressList.size());
        
        //Create map of account_address having country wise set of pincodes 
        Map<String, Set<String>> pinCodeToInsert = new Map<String, Set<String>>();

        //Get set of country wise Geography types
         Map<String, Set<AxtriaSalesIQTM__Geography_Type__c>> countryGeoTypeMap = new Map<String, Set<AxtriaSalesIQTM__Geography_Type__c>>();

        for(AxtriaSalesIQTM__Country__c country: countryList){
            //Set of pincodes of Account_Address
            Set<String> accPincodeSet = new Set<String>();
            for(AxtriaSalesIQTM__Account_Address__c accAdd: accountAddressList){
                if(country.Id == accAdd.AxtriaSalesIQTM__Country__c){
                    accPincodeSet.add(accAdd.AxtriaSalesIQTM__Pincode__c);
                }   
            }
            if(!accPincodeSet.isEmpty()){
                List<AxtriaSalesIQTM__Geography__c> geoList = new List<AxtriaSalesIQTM__Geography__c>();
                geoList = [SELECT Id, Name FROM AxtriaSalesIQTM__Geography__c where Name in :accPincodeSet and Country_ID__c =:country.Id];
                Set<String> geoPincodeSet = new Set<String>();
                for(AxtriaSalesIQTM__Geography__c geography: geoList){
                    geoPincodeSet.add(geography.Name);
                }
                accPincodeSet.removeAll(geoPincodeSet);
                if(!accPincodeSet.isEmpty()){
                    pinCodeToInsert.put(country.Id, accPincodeSet);
                }
            }

            List<AxtriaSalesIQTM__Geography_Type__c> geoTypelst = [SELECT Id, Name FROM AxtriaSalesIQTM__Geography_Type__c where AxtriaSalesIQTM__Country__c =:country.Id];
            Set<AxtriaSalesIQTM__Geography_Type__c> geoTypeSet = new Set<AxtriaSalesIQTM__Geography_Type__c>();
            for(AxtriaSalesIQTM__Geography_Type__c geoType:geoTypelst){
                geoTypeSet.add(geoType);
            }
            if(!geoTypeSet.isEmpty()){
                countryGeoTypeMap.put(country.Id, geoTypeSet);
            }

        }
        System.debug('pinCodeToInsert size is ' + pinCodeToInsert.size());
        System.debug('countryGeoTypeMap size is ' + countryGeoTypeMap.size());

        //Insert entries in Geography corresponding to pincodes which are not present in Account_Address
        if(!pinCodeToInsert.isEmpty()){
            List<AxtriaSalesIQTM__Geography__c> geoLstToInsert = new List<AxtriaSalesIQTM__Geography__c>();
            for(String conKey: pinCodeToInsert.keySet()){
                for(String pin: pinCodeToInsert.get(conKey)){
                    
                    for(AxtriaSalesIQTM__Geography_Type__c geotype: countryGeoTypeMap.get(conKey)){
                        AxtriaSalesIQTM__Geography__c geoObj = new AxtriaSalesIQTM__Geography__c();
                        geoObj.Name = pin;
                        geoObj.AxtriaSalesIQTM__Geography_Type__c = geotype.Id;
                        geoObj.AxtriaSalesIQTM__External_Geo_Type__c = geotype.Id;
                        geoObj.AxtriaSalesIQTM__External_Country_Id__c = conKey;
                        geoObj.AxtriaSalesIQTM__Neighbor_Geography__c = '0';
                        geoObj.AxtriaSalesIQTM__Zip_Name__c = 'MiniBrick';
                        geoObj.AxtriaSalesIQTM__Zip_Type__c = 'MiniBrick';
                        geoObj.AxtriaARSnT__Geography_Type1__c = geotype.name;

                        for(AxtriaSalesIQTM__Country__c contry : countryList){
                            if(contry.Id == conKey){
                                geoObj.AxtriaARSnT__Country_Code__c = contry.AxtriaSalesIQTM__Country_Code__c;
                            }
                        }
                        geoLstToInsert.add(geoObj);
                    }
                }
            }
            System.debug('geoLstToInsert size is ' + geoLstToInsert.size());
            recordsProcessed +=geoLstToInsert.size();
            if(geoLstToInsert !=null && geoLstToInsert.size()>0){
                insert geoLstToInsert;
            }
        }
    }
    
    global void execute(SchedulableContext sc){
        Database.executeBatch(new BatchCreateGeography(), 200);
    }
    
    global void finish(Database.BatchableContext bc){
        Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID); 
        sJob.No_Of_Records_Processed__c=recordsProcessed;
        sJob.Job_Status__c='Successful';
        update sJob;
    }
}