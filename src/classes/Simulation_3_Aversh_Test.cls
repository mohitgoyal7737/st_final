@isTest
public with sharing class Simulation_3_Aversh_Test{
    static testMethod void Simulation3AvershTest(){
        Profile p = [select id from Profile where name = 'Rep1' or name = 'System Administrator' order by name limit 1];
        Profile p2 = [select id from Profile where name = 'DM1' or name = 'System Administrator' order by name limit 1];
        
       // User loggedInUser = new User(id=UserInfo.getUserId());
        
        User tUser = new User(Alias = 'Rep', Email='repusernew@astrazeneca.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = p.Id, 
                                TimeZoneSidKey='America/Los_Angeles', UserName='repusernewQuset@astrazeneca.com');
        insert tUser;
        
        User tUser1 = new User(Alias = 'Rep', Email='repuser1new@astrazeneca.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = p.Id, 
                                TimeZoneSidKey='America/Los_Angeles', UserName='repusernewQuset1@astrazeneca.com');
        insert tUser1;
        
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        User tUser2 = new User(Alias = 'Rep', Email='repuser1new@astrazeneca.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = p.Id, 
                                TimeZoneSidKey='America/Los_Angeles', UserName='repusernewQuset2@astrazeneca.com');
        
        insert tUser2; 
        
        User dmuser = new User(Alias = 'DM', Email='repuser5new@astrazeneca.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = p2.Id, 
                                TimeZoneSidKey='America/Los_Angeles', UserName='repusernewQuset6@astrazeneca.com');
        
        insert dmuser;
        
        Account testAccount = new Account();
             testAccount.Name='Test Account' ;
        testAccount.Marketing_Code__c='ES';
           insert testAccount;
        
        Test.startTest();
         //System.runAs(loggedinuser){
        Simulation_3_Aversh obj=new Simulation_3_Aversh();
        //obj.search();
        //obj.save1();
      //  obj.refreshtable();
       // obj.save();
       // obj.createBrandSegmentMap();
         ApexPages.StandardController sc = new ApexPages.StandardController(testAccount);
            
             Simulation_3_Aversh ext = new Simulation_3_Aversh(sc); 
             //ext.search();
        //ext.save1();
         
        
        Test.stopTest();
    }
}