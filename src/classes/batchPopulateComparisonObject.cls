global class batchPopulateComparisonObject implements Database.Batchable<sObject> {
    
    public String query;
    public String teamInstance;
    public String marketCode;

    global batchPopulateComparisonObject(String teamInstance) {
        this.teamInstance = teamInstance;

        marketCode = [select AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name from AxtriaSalesIQTM__Team_Instance__c where id = :teamInstance].AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name;
        this.query = 'select SIQ_AZ_Brand_Total__c,SIQ_Territory_Code__c,SIQ_AZ_Brand_Code__c, SIQ_Product_Class_Total__c from SIQ_Sales_Data__c where SIQ_Market_Code__c =: marketCode';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<SIQ_Sales_Data__c> scope) {
        Map<string,ID> terrcodeIdMap = new Map<string,ID>();
        set<string> terrcodeSet = new set<string>();
        List<Integer> allPotentials = new List<Integer>();
        List<Integer> allAdoptions = new List<Integer>();
        
        for(SIQ_Sales_Data__c salesData : scope)
        {
            allPotentials.add(Integer.valueof(salesData.SIQ_Product_Class_Total__c));
            allAdoptions.add(Integer.valueof(salesData.SIQ_AZ_Brand_Total__c));
            terrcodeSet.add(salesData.SIQ_Territory_Code__c) ;
        }
        
        for(AxtriaSalesIQTM__Position__c pos : [select id,AxtriaSalesIQTM__Client_Position_Code__c from AxtriaSalesIQTM__Position__c  where AxtriaSalesIQTM__Client_Position_Code__c IN:terrcodeSet]){
            terrcodeIdMap.put(pos.AxtriaSalesIQTM__Client_Position_Code__c ,pos.id);
        }

        List<AggregateResult> sumPotential = [select SUM(SIQ_AZ_Brand_Total__c) adoption, SUM(SIQ_Product_Class_Total__c) potential, SIQ_Territory_Code__c product from SIQ_Sales_Data__c where SIQ_Product_Class_Total__c in :allPotentials AND SIQ_AZ_Brand_Total__c IN:allAdoptions group by SIQ_Territory_Code__c];
        List<Adoption_Potential_Comparison__c> adpPotList = new List<Adoption_Potential_Comparison__c>();
        for(AggregateResult agg : sumPotential)
        {
            Adoption_Potential_Comparison__c adpPotComp = new Adoption_Potential_Comparison__c();
            adpPotComp.Team_Instance__c = teamInstance;
            adpPotComp.S_D_Potential__c = String.valueof(agg.get('potential'));
            adpPotComp.Product_Catalog__c = String.valueof(agg.get('product'));           
            adpPotComp.S_D_Adoption__c = String.valueof(agg.get('adoption'));
            string key = String.valueof(agg.get('SIQ_Territory_Code__c')) ;
            if(terrcodeIdMap.containskey(key)){
                adpPotComp.Territory__c =   terrcodeIdMap.get(key);          
            }
            adpPotComp.External_ID__c =  adpPotComp.Team_Instance__c +','+ adpPotComp.Territory__c;
            adpPotList.add(adpPotComp);
        }
        if(adpPotList.size() > 0){
            upsert adpPotList External_ID__c ;
        }
    }
    global void finish(Database.BatchableContext BC) {
    
    }
}