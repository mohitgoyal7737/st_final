@isTest
public class Delete_Staging_Survey_Data_Test {

    static testMethod void testMethod1() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        
        Account acc = TestDataFactory.createAccount();
        acc.Profile_Consent__c='yes';
        acc.AxtriaSalesIQTM__Speciality__c ='testspecs';
        insert acc;
        
        Account acc2 = TestDataFactory.createAccount();
        acc2.Profile_Consent__c='yes';
        acc2.AxtriaSalesIQTM__Speciality__c ='testspecs';
        acc2.name = 'testacc2';
        insert acc2;
              
        AxtriaSalesIQTM__Team_Instance__c teamInstance = TestDataFactory.createTeamInstance(team);
        teamInstance.AxtriaSalesIQTM__Team__c = team.id;
        teamInstance.Name = 'testteamins';
        teamInstance.Segmentation_Universe__c = 'Full S&T Input Customers';
        insert teamInstance;
        

        Product_Catalog__c pcc = TestDataFactory.productCatalog(team,teamInstance,countr);
        pcc.Country_Lookup__c = countr.id;
    
        insert pcc;
        
       
        
        Measure_Master__c mm1= TestDataFactory.createMeasureMaster(pcc,team,teamInstance);
        mm1.Team__c = team.id;
        mm1.Brand_Lookup__c = pcc.Id;
        mm1.Team_Instance__c = teamInstance.id;
        insert mm1;
        
        Measure_Master__c mm2= TestDataFactory.createMeasureMaster(pcc,team,teamInstance);
        mm2.Team__c = team.id;
        mm2.Brand_Lookup__c = pcc.Id;
        mm2.Team_Instance__c = teamInstance.id;
        insert mm2;
        
        insert  new MetaData_Definition__c (Display_Name__c='ACCESSIBILITY',Source_Field__c='Accessibility_Range__c',Source_Object__c='BU_Response__c',Team_Instance__c=teamInstance.id,Product_Catalog__c=pcc.Id);
 
        AxtriaSalesIQTM__Position__c pos = TestDataFactory.createPosition(team,teamInstance);
        insert pos;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamInstance);
        insert posAccount;
        
        Product_Priority__c pp = new Product_Priority__c();
        pp.CurrencyIsoCode = 'USD';
        pp.Product__c = pcc.id;
        pp.Speciality_ID__c='testSpecs';
        pp.priority__c='P2';
        insert pp;
        
        BU_Response__c bu = TestDataFactory.createBuResponse(posAccount,pcc,teamInstance,team,acc);
        bu.Team_Instance__c = teamInstance.id;
        bu.Product__c = pcc.id;
        //bu.Brand_c = pcc.id;
        insert bu;
        
        MetaData_Definition__c meta = TestDataFactory.createMetaDataDefinition(teamInstance, pcc, team);
        meta.Team_Instance__c = teamInstance.id;
        insert meta;
        
        Staging_Survey_Data__c sdata = TestDataFactory.createStagingSurveyData(teamInstance, pcc, team);
        sdata.Team_Instance__c = teamInstance.id;
        insert sdata;
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            Delete_Staging_Survey_Data obj=new Delete_Staging_Survey_Data(mm1.id,mm2.id,teamInstance.Name,pcc.Veeva_External_ID__c,'HCO',true);
            obj.query = 'select id from Staging_Survey_Data__c ';
            Database.executeBatch(obj);
        }
        Test.stopTest();
    }
}