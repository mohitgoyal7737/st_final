/**********************************************************************************************
Author      : Himanshu Tariyal (A0994)
Date        : 28th September'2020
Description : Test class for PositionEmployeeDirectLoad
Revision(s) : v1.0
**********************************************************************************************/
@isTest
private class PositionEmployeeDirectLoadTest 
{
    static testMethod void testMethod1() 
    {
    	String className = 'PositionEmployeeDirectLoadTest';

        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Load_Type__c = 'Full Load';
        SnTDMLSecurityUtil.insertRecords(countr,className);
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);

        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins,className);
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        SnTDMLSecurityUtil.insertRecords(scen,className);

        Account acc= TestDataFactory.createAccount();
        acc.AccountNumber ='BH10643156';
        SnTDMLSecurityUtil.insertRecords(acc,className);
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        SnTDMLSecurityUtil.insertRecords(pcc,className);
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        pos.AxtriaSalesIQTM__Effective_End_Date__c = date.today().addDays(10);
        pos.AxtriaSalesIQTM__IsMaster__c = true;
        pos.AxtriaSalesIQTM__Parent_Position__c = null;
        pos.AxtriaSalesIQTM__Client_Position_Code__c ='N003';
        SnTDMLSecurityUtil.insertRecords(pos,className);

        AxtriaSalesIQTM__Position__c pos1 = TestDataFactory.createPosition(team,teamins);
        pos1.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        pos1.AxtriaSalesIQTM__Effective_End_Date__c = date.today().addDays(10);
        pos1.AxtriaSalesIQTM__IsMaster__c=true;
        pos1.AxtriaSalesIQTM__Parent_Position__c = pos.Id;
        pos1.AxtriaSalesIQTM__Client_Position_Code__c ='T090';
        SnTDMLSecurityUtil.insertRecords(pos1,className);

        System.Test.startTest();

        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        Database.executeBatch(new PositionEmployeeDirectLoad(team.Id,teamins.Id));

        System.Test.stopTest();
    }

    static testMethod void testMethod2() 
    {
    	String className = 'PositionEmployeeDirectLoadTest';

    	User loggedInUser = new User(id=UserInfo.getUserId());
    	loggedInUser.FederationIdentifier = '1001';
    	SnTDMLSecurityUtil.updateRecords(loggedInUser,className);

        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        orgmas.Name = 'Nation';
        SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Load_Type__c = 'Full Load';
        SnTDMLSecurityUtil.insertRecords(countr,className);
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);

        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins,className);

        AxtriaSalesIQTM__Team_Instance__c teamins2 = TestDataFactory.createTeamInstance(team);
        teamins2.Name = 'New Team';
        SnTDMLSecurityUtil.insertRecords(teamins2,className);
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        SnTDMLSecurityUtil.insertRecords(scen,className);

        Account acc= TestDataFactory.createAccount();
        acc.AccountNumber ='BH10643156';
        SnTDMLSecurityUtil.insertRecords(acc,className);
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        SnTDMLSecurityUtil.insertRecords(pcc,className);
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        pos.AxtriaSalesIQTM__Effective_End_Date__c = date.today().addDays(10);
        pos.AxtriaSalesIQTM__IsMaster__c = true;
        pos.AxtriaSalesIQTM__Parent_Position__c = null;
        pos.AxtriaSalesIQTM__Client_Position_Code__c ='N003';
        SnTDMLSecurityUtil.insertRecords(pos,className);

        AxtriaSalesIQTM__Position__c pos1 = TestDataFactory.createPosition(team,teamins);
        pos1.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        pos1.AxtriaSalesIQTM__Effective_End_Date__c = date.today().addDays(10);
        pos1.AxtriaSalesIQTM__IsMaster__c=true;
        pos1.AxtriaSalesIQTM__Parent_Position__c = pos.Id;
        pos1.AxtriaSalesIQTM__Client_Position_Code__c ='T090';
        SnTDMLSecurityUtil.insertRecords(pos1,className);  

        AxtriaSalesIQTM__Position__c pos2 = TestDataFactory.createPosition(team,teamins);
        pos2.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        pos2.AxtriaSalesIQTM__Effective_End_Date__c = date.today().addDays(10);
        pos2.AxtriaSalesIQTM__IsMaster__c=true;
        pos2.AxtriaSalesIQTM__Parent_Position__c = pos.Id;
        pos2.AxtriaSalesIQTM__Client_Position_Code__c ='T092';
        SnTDMLSecurityUtil.insertRecords(pos2,className); 

        AxtriaSalesIQTM__Position__c pos3 = TestDataFactory.createPosition(team,teamins);
        pos3.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        pos3.AxtriaSalesIQTM__Effective_End_Date__c = date.today().addDays(10);
        pos3.AxtriaSalesIQTM__IsMaster__c=true;
        pos3.AxtriaSalesIQTM__Parent_Position__c = null;
        pos3.AxtriaSalesIQTM__Client_Position_Code__c ='Nation';
        SnTDMLSecurityUtil.insertRecords(pos3,className);    

        AxtriaSalesIQTM__Employee__c emp = new AxtriaSalesIQTM__Employee__c();
        emp.AxtriaSalesIQTM__Employee_ID__c = '1001';
        emp.AxtriaSalesIQTM__Country_Name__c = countr.Id;
        emp.Hire_Date__c = Date.today().addDays(-10);
        emp.AxtriaSalesIQTM__FirstName__c = 'John';
        emp.AxtriaSalesIQTM__Last_Name__c = 'Smith1';
        SnTDMLSecurityUtil.insertRecords(emp,className);

        AxtriaSalesIQTM__Employee__c emp2 = new AxtriaSalesIQTM__Employee__c();
        emp2.AxtriaSalesIQTM__Employee_ID__c = '1002';
        emp2.AxtriaSalesIQTM__Country_Name__c = countr.Id;
        emp2.Hire_Date__c = Date.today().addDays(-10);
        emp2.AxtriaSalesIQTM__FirstName__c = 'John';
        emp2.AxtriaSalesIQTM__Last_Name__c = 'Smith2';
        SnTDMLSecurityUtil.insertRecords(emp2,className);

        AxtriaSalesIQTM__Position_Employee__c posEmp = new AxtriaSalesIQTM__Position_Employee__c();
        posEmp.AxtriaSalesIQTM__Employee__c = emp.Id;
        posEmp.AxtriaSalesIQTM__Position__c = pos.Id;
        posEmp.AxtriaSalesIQTM__Effective_Start_Date__c = Date.today().addDays(-10);
        posEmp.AxtriaSalesIQTM__Effective_End_Date__c = Date.today().addDays(2);
        posEmp.AxtriaSalesIQTM__Assignment_Type__c = 'Primary';
        SnTDMLSecurityUtil.insertRecords(posEmp,className);

        AxtriaSalesIQTM__Position_Employee__c posEmp0 = new AxtriaSalesIQTM__Position_Employee__c();
        posEmp0.AxtriaSalesIQTM__Employee__c = emp.Id;
        posEmp0.AxtriaSalesIQTM__Position__c = pos2.Id;
        posEmp0.AxtriaSalesIQTM__Effective_Start_Date__c = Date.today().addDays(-10);
        posEmp0.AxtriaSalesIQTM__Effective_End_Date__c = Date.today().addDays(2);
        posEmp0.AxtriaSalesIQTM__Assignment_Type__c = 'Primary';
        SnTDMLSecurityUtil.insertRecords(posEmp0,className);

        AxtriaSalesIQTM__Position_Employee__c posEmp2 = new AxtriaSalesIQTM__Position_Employee__c();
        posEmp2.AxtriaSalesIQTM__Employee__c = emp2.Id;
        posEmp2.AxtriaSalesIQTM__Position__c = pos.Id;
        posEmp2.AxtriaSalesIQTM__Effective_Start_Date__c = Date.today().addDays(-10);
        posEmp2.AxtriaSalesIQTM__Effective_End_Date__c = Date.today().addDays(2);
        posEmp2.AxtriaSalesIQTM__Assignment_Type__c = 'Secondary';
        SnTDMLSecurityUtil.insertRecords(posEmp2,className);

        AxtriaSalesIQTM__Position_Employee__c posEmp2a = new AxtriaSalesIQTM__Position_Employee__c();
        posEmp2a.AxtriaSalesIQTM__Employee__c = emp2.Id;
        posEmp2a.AxtriaSalesIQTM__Position__c = pos.Id;
        posEmp2a.AxtriaSalesIQTM__Effective_Start_Date__c = Date.today().addDays(3);
        posEmp2a.AxtriaSalesIQTM__Effective_End_Date__c = Date.today().addDays(30);
        posEmp2a.AxtriaSalesIQTM__Assignment_Type__c = 'Secondary';
        SnTDMLSecurityUtil.insertRecords(posEmp2a,className);

        AxtriaSalesIQTM__Position_Employee__c posEmp3 = new AxtriaSalesIQTM__Position_Employee__c();
        posEmp3.AxtriaSalesIQTM__Employee__c = emp.Id;
        posEmp3.AxtriaSalesIQTM__Position__c = pos.Id;
        posEmp3.AxtriaSalesIQTM__Effective_Start_Date__c = Date.today().addDays(100);
        posEmp3.AxtriaSalesIQTM__Effective_End_Date__c = Date.today().addDays(200);
        posEmp3.AxtriaSalesIQTM__Assignment_Type__c = 'Primary';
        SnTDMLSecurityUtil.insertRecords(posEmp3,className);

        AxtriaSalesIQTM__Position_Employee__c posEmp4 = new AxtriaSalesIQTM__Position_Employee__c();
        posEmp4.AxtriaSalesIQTM__Employee__c = emp2.Id;
        posEmp4.AxtriaSalesIQTM__Position__c = pos.Id;
        posEmp4.AxtriaSalesIQTM__Effective_Start_Date__c = Date.today().addDays(100);
        posEmp4.AxtriaSalesIQTM__Effective_End_Date__c = Date.today().addDays(200);
        posEmp4.AxtriaSalesIQTM__Assignment_Type__c = 'Secondary';
        SnTDMLSecurityUtil.insertRecords(posEmp4,className);

        AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
        cr.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.Id;
        SnTDMLSecurityUtil.insertRecords(cr,className);  

        temp_Obj__c rec0 = new temp_Obj__c();
        rec0.Change_Request__c = cr.Id;
        rec0.Status__c = 'New';
        rec0.Team_Instance_Name__c = teamins.Name;
        rec0.Object__c = 'Position_Employee';
        rec0.Employee_PRID__c = '1001';
        rec0.Position_Code__c = 'T090';
        rec0.Assignment_Type__c = 'Primary';
        rec0.Effective_Start_Date__c = Date.today().addDays(-30);
        rec0.Effective_End_Date__c = Date.today().addDays(20);
        SnTDMLSecurityUtil.insertRecords(rec0,className);

        temp_Obj__c rec = new temp_Obj__c();
        rec.Change_Request__c = cr.Id;
        rec.Status__c = 'New';
        rec.Team_Instance_Name__c = teamins.Name;
        rec.Object__c = 'Position_Employee';
        rec.Employee_PRID__c = '1001';
        rec.Position_Code__c = 'T090';
        rec.Assignment_Type__c = 'Primary';
        rec.Effective_Start_Date__c = Date.today().addDays(3);
        rec.Effective_End_Date__c = Date.today().addDays(30);
        SnTDMLSecurityUtil.insertRecords(rec,className);

        temp_Obj__c rec1 = new temp_Obj__c();
        rec1.Change_Request__c = cr.Id;
        rec1.Status__c = 'New';
        rec1.Team_Instance_Name__c = teamins.Name;
        rec1.Object__c = 'Position_Employee';
        rec1.Employee_PRID__c = '1002';
        rec1.Position_Code__c = 'N003';
        rec1.Assignment_Type__c = 'Secondary';
        rec1.Effective_Start_Date__c = Date.today().addDays(3);
        rec1.Effective_End_Date__c = Date.today().addDays(30);
        SnTDMLSecurityUtil.insertRecords(rec1,className);

        temp_Obj__c rec2 = new temp_Obj__c();
        rec2.Change_Request__c = cr.Id;
        rec2.Status__c = 'New';
        rec2.Team_Instance_Name__c = teamins.Name;
        rec2.Object__c = 'Position_Employee';
        rec2.Employee_PRID__c = '1002';
        rec2.Position_Code__c = 'T090';
        rec2.Assignment_Type__c = 'Secondary';
        rec2.Effective_Start_Date__c = Date.today().addDays(3);
        rec2.Effective_End_Date__c = Date.today().addDays(30);
        SnTDMLSecurityUtil.insertRecords(rec2,className);

        temp_Obj__c rec2a = new temp_Obj__c();
        rec2a.Change_Request__c = cr.Id;
        rec2a.Status__c = 'New';
        rec2a.Team_Instance_Name__c = teamins.Name;
        rec2a.Object__c = 'Position_Employee';
        rec2a.Employee_PRID__c = '1002';
        rec2a.Position_Code__c = 'N003';
        rec2a.Assignment_Type__c = 'Primary';
        rec2a.Effective_Start_Date__c = Date.today().addDays(3);
        rec2a.Effective_End_Date__c = Date.today().addDays(30);
        SnTDMLSecurityUtil.insertRecords(rec2a,className);

        temp_Obj__c rec3 = new temp_Obj__c();
        rec3.Change_Request__c = cr.Id;
        rec3.Status__c = 'New';
        rec3.Object__c = 'Position_Employee';
        rec3.Employee_PRID__c = '1001';
        rec3.Position_Code__c = 'T090';
        rec3.Team_Instance_Name__c = teamins.Name;
        rec3.Assignment_Type__c = 'Primary';
        rec3.Effective_Start_Date__c = Date.today().addDays(-1);
        rec3.Effective_End_Date__c = Date.today().addDays(1);
        SnTDMLSecurityUtil.insertRecords(rec3,className);

        temp_Obj__c rec3z = new temp_Obj__c();
        rec3z.Change_Request__c = cr.Id;
        rec3z.Status__c = 'New';
        rec3z.Object__c = 'Position_Employee';
        rec3z.Employee_PRID__c = '1001';
        rec3z.Position_Code__c = 'T090';
        rec3z.Team_Instance_Name__c = teamins.Name;
        rec3z.Assignment_Type__c = 'Primary';
        rec3z.Effective_Start_Date__c = Date.today().addDays(-2);
        rec3z.Effective_End_Date__c = Date.today().addDays(-1);
        SnTDMLSecurityUtil.insertRecords(rec3z,className);

        temp_Obj__c rec3a = new temp_Obj__c();
        rec3a.Change_Request__c = cr.Id;
        rec3a.Status__c = 'New';
        rec3a.Object__c = 'Position_Employee';
        rec3a.Employee_PRID__c = '1002';
        rec3a.Position_Code__c = 'N003';
        rec3a.Team_Instance_Name__c = teamins.Name;
        rec3a.Assignment_Type__c = 'Primary';
        rec3a.Effective_Start_Date__c = Date.today().addDays(-1);
        rec3a.Effective_End_Date__c = Date.today().addDays(1);
        SnTDMLSecurityUtil.insertRecords(rec3a,className);

        temp_Obj__c rec3b = new temp_Obj__c();
        rec3b.Change_Request__c = cr.Id;
        rec3b.Status__c = 'New';
        rec3b.Object__c = 'Position_Employee';
        rec3b.Employee_PRID__c = '1002';
        rec3b.Position_Code__c = 'N003';
        rec3b.Team_Instance_Name__c = teamins.Name;
        rec3b.Assignment_Type__c = 'Primary';
        rec3b.Effective_Start_Date__c = Date.today().addDays(-2);
        rec3b.Effective_End_Date__c = Date.today().addDays(-1);
        SnTDMLSecurityUtil.insertRecords(rec3b,className);

        temp_Obj__c rec4 = new temp_Obj__c();
        rec4.Status__c = 'New';
        rec4.Team_Instance_Name__c = teamins.Name;
        rec4.Object__c = 'Position_Employee';
        rec4.Employee_PRID__c = '10021';
        rec4.Position_Code__c = 'T0901';
        SnTDMLSecurityUtil.insertRecords(rec4,className);

        temp_Obj__c rec5 = new temp_Obj__c();
        rec5.Change_Request__c = cr.Id;
        rec5.Status__c = 'New';
        rec5.Object__c = 'Position_Employee';
        rec5.Employee_PRID__c = '1002';
        rec5.Team_Instance_Name__c = teamins.Name;
        rec5.Position_Code__c = 'T0901';
        SnTDMLSecurityUtil.insertRecords(rec5,className);

        temp_Obj__c rec5b = new temp_Obj__c();
        rec5b.Change_Request__c = cr.Id;
        rec5b.Status__c = 'New';
        rec5b.Object__c = 'Position_Employee';
        rec5b.Employee_PRID__c = '1002';
        rec5b.Team_Instance_Name__c = teamins.Name;
        rec5b.Position_Code__c = 'Nation';
        SnTDMLSecurityUtil.insertRecords(rec5b,className);

        temp_Obj__c rec6 = new temp_Obj__c();
        rec6.Change_Request__c = cr.Id;
        rec6.Status__c = 'New';
        rec6.Object__c = 'Position_Employee';
        rec6.Employee_PRID__c = '1002';
        rec6.Team_Instance_Name__c = teamins2.Name;
        rec6.Position_Code__c = 'N003z';
        SnTDMLSecurityUtil.insertRecords(rec6,className);

        temp_Obj__c rec7 = new temp_Obj__c();
        rec7.Change_Request__c = cr.Id;
        rec7.Status__c = 'New';
        rec7.Team_Instance_Name__c = teamins2.Name;
        rec7.Object__c = 'Position_Employee';
        rec7.Employee_PRID__c = '1002';
        rec7.Position_Code__c = 'Nation';
        SnTDMLSecurityUtil.insertRecords(rec7,className);

        System.Test.startTest();

        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        Database.executeBatch(new PositionEmployeeDirectLoad(team.Id,teamins.Id,cr.Id));

        System.Test.stopTest();
    }
}