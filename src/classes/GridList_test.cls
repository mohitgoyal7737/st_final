@isTest
public class GridList_test {
    @istest static void gridtest()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        AxtriaSalesIQTM__Organization_Master__c om=new AxtriaSalesIQTM__Organization_Master__c();
        om.name='ES';
        om.CurrencyIsoCode='USD';
        om.AxtriaSalesIQTM__Parent_Country_Level__c=true;
        om.AxtriaSalesIQTM__Org_Level__c='Global';
        insert om;
        
        AxtriaSalesIQTM__Country__c c=new AxtriaSalesIQTM__Country__c();
        c.name='ES';
        c.CurrencyIsoCode='USD';
        c.AxtriaSalesIQTM__Parent_Organization__c=om.id;
        c.AxtriaSalesIQTM__Status__c='Active';
        insert c;
        
        Grid_Master__c g=new Grid_Master__c();
        g.name='test';
        g.Country__c=c.id;
        g.CurrencyIsoCode='USD';
        insert g;
        Step__c  s=new Step__c();
        s.Matrix__c =g.id;
        insert s;
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            GridList obj=new GridList();
            obj.editMatrix();
            obj.cloneMatrix();
            obj.viewMatrix();
        }
        
        Test.stopTest();

    }
}