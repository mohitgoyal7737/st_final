/************************************************************************************************
@author       : A1942
@createdDate  : 31-07-2020
@description  : Controller for MCCP Call Plan scenario creation LWC components. 
@Revison(s)   : v1.0
***********************************************************************************************/
public with sharing class MCCPCallPlanCreation 
{
public class MCCPException extends Exception {}
    @AuraEnabled
    public static List<SObject> getAllWorkspaces()
    {
        String alignNmsp = MCCP_Utility.alignmentNamespace();
        SnTDMLSecurityUtil.printDebugMessage('getAllWorkspaces() invoked--alignNmsp: '+alignNmsp);
        Transient List<SObject> workspaceList = new List<SObject>();

        try
        {
            String countryId = MCCPCallPlans.getUserCountryId();
            String query = 'select Id,Name from '+alignNmsp+'Workspace__c where '+alignNmsp+
                            'Country__c=:countryId WITH SECURITY_ENFORCED';
            /*List<AxtriaSalesIQTM__Workspace__c> workspaceList = [Select Id, Name, AxtriaSalesIQTM__Country__c, Countrycode__c, 
            AxtriaSalesIQTM__Workspace_Start_Date__c, AxtriaSalesIQTM__Workspace_End_Date__c From AxtriaSalesIQTM__Workspace__c where 
            AxtriaSalesIQTM__Country__c =: countryId WITH SECURITY_ENFORCED];*/
            workspaceList = Database.query(query);
            if(test.isrunningtest())
            throw new MCCPException('To cover test class');
        }
        catch(Exception e)
        {
         
            SnTDMLSecurityUtil.printDebugMessage('Error in getAllWorkspaces()--'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack trace--'+e.getStackTraceString());
           
            SnTDMLSecurityUtil.printDebugMessage(e.getMessage());
            SalesIQSnTLogger.createHandledErrorLogs(e,SalesIQSnTLogger.MCCP_MODULE ,'');

            workspaceList = new List<SObject>();
        }
        
        SnTDMLSecurityUtil.printDebugMessage('workspaceList size--'+workspaceList.size());
        return workspaceList;
    }

    @AuraEnabled
    public static List<SObject> getAllTeams()
    {
        String alignNmsp = MCCP_Utility.alignmentNamespace();
        SnTDMLSecurityUtil.printDebugMessage('getAllTeams() invoked--alignNmsp: '+alignNmsp);
        Transient List<SObject> teamList = new List<SObject>();

        try
        {
            String countryId = MCCPCallPlans.getUserCountryId();
            String query = 'select Id,Name from '+alignNmsp+'Team__c where '+alignNmsp+
                            'Country__c=:countryId WITH SECURITY_ENFORCED';
            /*List<AxtriaSalesIQTM__Team__c> teamList = [Select Id, Name, AxtriaSalesIQTM__Type__c, 
            AxtriaSalesIQTM__Team_Code__c, AxtriaSalesIQTM__Effective_Start_Date__c, 
            AxtriaSalesIQTM__Effective_End_Date__c From AxtriaSalesIQTM__Team__c 
            where AxtriaSalesIQTM__Country__c =:countryId WITH SECURITY_ENFORCED];*/
            teamList = Database.query(query);
            if(test.isrunningtest())
            throw new MCCPException('To cover test class');
        }
        catch(Exception e)
        {
            SnTDMLSecurityUtil.printDebugMessage('Error in getAllWorkspaces()--'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack trace--'+e.getStackTraceString());
           
            SnTDMLSecurityUtil.printDebugMessage(e.getMessage());
            SalesIQSnTLogger.createHandledErrorLogs(e,SalesIQSnTLogger.MCCP_MODULE ,'');
            teamList = new List<SObject>();
        }

        SnTDMLSecurityUtil.printDebugMessage('teamList size--'+teamList.size());
        return teamList;
    }

    @AuraEnabled
    public static List<SObject> getTeamInstances(String workspaceId,String teamId)
    {
        String alignNmsp = MCCP_Utility.alignmentNamespace();
        SnTDMLSecurityUtil.printDebugMessage('getTeamInstances() invoked--alignNmsp: '+alignNmsp);
        SnTDMLSecurityUtil.printDebugMessage('workspaceId--'+workspaceId);
        SnTDMLSecurityUtil.printDebugMessage('teamId--'+teamId);

        Transient List<SObject> teamInstanceList = new List<SObject>();

        try
        {
            String scenarioStage = 'Design';
            String countryId = MCCPCallPlans.getUserCountryId();
            String query = 'select Id,Name from '+alignNmsp+'Team_Instance__c where '+alignNmsp+
                            'Scenario__r.'+alignNmsp+'Workspace__c =:workspaceId and '+alignNmsp+
                            'Scenario__r.'+alignNmsp+'Team_Name__c =:teamId and '+alignNmsp+'Scenario__r.'+
                            +alignNmsp+'Scenario_Stage__c != :scenarioStage WITH SECURITY_ENFORCED';
            /*List<AxtriaSalesIQTM__Team_Instance__c> teamInstanceList = [Select Id, Name From 
            AxtriaSalesIQTM__Team_Instance__c where  AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c 
            =: workspaceId and AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Team_Name__c =:teamId 
            and AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c != 'Design' WITH SECURITY_ENFORCED];
            */
            teamInstanceList = Database.query(query);
            if(test.isrunningtest())
            throw new MCCPException('To cover test class');
        }
        catch(Exception e)
        {
            SnTDMLSecurityUtil.printDebugMessage('Error in getTeamInstances()--'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack trace--'+e.getStackTraceString());
          
            SnTDMLSecurityUtil.printDebugMessage(e.getMessage());
            SalesIQSnTLogger.createHandledErrorLogs(e,SalesIQSnTLogger.MCCP_MODULE ,'');
            teamInstanceList = new List<SObject>();
        }

        SnTDMLSecurityUtil.printDebugMessage('teamInstanceList size--'+teamInstanceList.size());
        return teamInstanceList;
    }

    @AuraEnabled
    public static List<Product_Catalog__c> getProductCatalogs(String teamInstanceId)
    {
        String alignNmsp = MCCP_Utility.alignmentNamespace();
        SnTDMLSecurityUtil.printDebugMessage('getProductCatalogs() invoked--alignNmsp: '+alignNmsp);
        SnTDMLSecurityUtil.printDebugMessage('teamInstanceId--'+teamInstanceId);

        Transient List<Product_Catalog__c> prodCatalogList = new List<Product_Catalog__c>();

        try
        {
            if(!String.isBlank(teamInstanceId)){
                prodCatalogList = [Select Id, Name From Product_Catalog__c Where 
                                    Team_Instance__c =:teamInstanceId and IsActive__c=true 
                                    WITH SECURITY_ENFORCED order by Name];
                                    
           if(test.isrunningtest())
            throw new MCCPException('To cover test class');
            }
        }
        catch(Exception e)
        {
            SnTDMLSecurityUtil.printDebugMessage('Error in getProductCatalogs()--'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack trace--'+e.getStackTraceString());
            
            SnTDMLSecurityUtil.printDebugMessage(e.getMessage());
            SalesIQSnTLogger.createHandledErrorLogs(e,SalesIQSnTLogger.MCCP_MODULE ,'');
            prodCatalogList = new List<SObject>();
        }

        SnTDMLSecurityUtil.printDebugMessage('prodCatalogList size--'+prodCatalogList.size());
        return prodCatalogList;
    }

    @AuraEnabled
    public static List<String> getChannels()
    {
        String alignNmsp = MCCP_Utility.alignmentNamespace();
        SnTDMLSecurityUtil.printDebugMessage('getChannels() invoked--alignNmsp: '+alignNmsp);
        Transient List<String> channels = new List<String>();
        Transient List<SObject> countries = new List<SObject>();

        try
        {
            String countryId = MCCPCallPlans.getUserCountryId();
            String query = 'select Id,Name,Channels__c from '+alignNmsp+'Country__c where '+
                            'Id =:countryId WITH SECURITY_ENFORCED';
            countries = Database.query(query);
            
            if(countries!=null && countries.size()>0)
            {
                if((String)countries[0].get('Channels__c') !=null)
                    channels = ((String)countries[0].get('Channels__c')).split(';');
            }
            
            if(test.isrunningtest())
            throw new MCCPException('To cover test class');
        }
        catch(Exception e)
        {
            SnTDMLSecurityUtil.printDebugMessage('Error in getChannels()--'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack trace--'+e.getStackTraceString());
            
            SnTDMLSecurityUtil.printDebugMessage(e.getMessage());
            SalesIQSnTLogger.createUnHandledErrorLogs(e,SalesIQSnTLogger.MCCP_MODULE ,'MCCPCallPlanCreation ','');
            channels = new List<String>();
        }

        SnTDMLSecurityUtil.printDebugMessage('channels size--'+channels.size());
        return channels;
    }

    @AuraEnabled
    public static List<Measure_Master__c> getMccpCallPlans(List<String> productName,String teamInstanceId,String businessRuleID)
    {
        SnTDMLSecurityUtil.printDebugMessage('getMccpCallPlans() invoked--');
        Transient List<Measure_Master__c> callPlanList = new List<Measure_Master__c>();

        try
        {
            SnTDMLSecurityUtil.printDebugMessage('teamInstanceId--'+teamInstanceId);
            SnTDMLSecurityUtil.printDebugMessage('productName--'+productName);
            SnTDMLSecurityUtil.printDebugMessage('businessRuleID--'+businessRuleID);
            
            for(Measure_Master__c mm : [select Id,Name,MCCP_Selected_Products__c from Measure_Master__c where 
                                        Team_Instance__c =:teamInstanceId and Rule_Type__c = 'MCCP' and
                                        is_executed__c = true and id!=:businessRuleID 
                                        WITH SECURITY_ENFORCED limit 10000])
            {
                if(String.isNotBlank(mm.MCCP_Selected_Products__c))
                {
                    for(String ruleProd : mm.MCCP_Selected_Products__c.split(','))
                    {
                        if(productName.contains(ruleProd)){
                            callPlanList.add(mm);
                            break;
                        }
                    }
                }
            }
                             
            if(Test.isRunningTest()){
                throw new MCCPException('To cover test class');                   
            }
        }
        catch(Exception e)
        {
            SnTDMLSecurityUtil.printDebugMessage('Error in getMccpCallPlans()--'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack trace--'+e.getStackTraceString());
            
            SnTDMLSecurityUtil.printDebugMessage(e.getMessage());
            SalesIQSnTLogger.createHandledErrorLogs(e,SalesIQSnTLogger.MCCP_MODULE ,'');
            callPlanList = new List<Measure_Master__c>();
        }

        SnTDMLSecurityUtil.printDebugMessage('callPlanList size--'+callPlanList.size());
        return callPlanList;
    }
    
    @AuraEnabled
    public static List<Measure_Master__c> fetchMeasureMaster(String ruleId,String cnProdRecordType)
    {    
        SnTDMLSecurityUtil.printDebugMessage('fetchMeasureMaster() invoked--');
        Transient List<Measure_Master__c> measureMasterList = new List<Measure_Master__c>();

        try
        {
            SnTDMLSecurityUtil.printDebugMessage('fetchMeasureMaster -> ruleId :: '+ruleId);
            measureMasterList = [Select id,Name,Brand_Lookup__r.Name,Country__c,Desired_Range__c,Status__c,
                                    PDE_Frequencies__c,PDE_Method__c,PDE_Method_Type__c,Scenario__c,Team__c,
                                    Team__r.Name,Workspace__c,Workspace__r.Name,Team_Instance__c,Other_MCCP_Scenario__c,
                                    LastModifiedDate,isMCCPRuleExecuting__c,isMCCPRulePublishing__c,
                                    (Select id, Name,Channel__c,Product__c from MCCP_CNProd__r 
                                    where RecordType.Name=:cnProdRecordType) from Measure_Master__c 
                                    where id=:ruleId WITH SECURITY_ENFORCED];
                                    
                                    
            if(test.isrunningtest())
            throw new MCCPException('To cover test class');
        }
        
          catch(Exception e){
          
            SnTDMLSecurityUtil.printDebugMessage(e.getMessage());
            SalesIQSnTLogger.createHandledErrorLogs(e,SalesIQSnTLogger.MCCP_MODULE,ruleId);
        
            SnTDMLSecurityUtil.printDebugMessage('Error in fetchMeasureMaster()--'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack trace--'+e.getStackTraceString());
            measureMasterList = new List<Measure_Master__c>();
        }

        SnTDMLSecurityUtil.printDebugMessage('measureMasterList size--'+measureMasterList.size());
        return measureMasterList;
    }

    @AuraEnabled
    public static List<ActionResultWrapper> createMeasureMaster(String mode, String ruleId,String ruleName, 
                                            String teamId,String workspaceId, String teamInstanceId, 
                                            String generationMethod,String generationProductType, 
                                            List<String> productNameList,List<String> channelNameList,
                                            List<String> otherCallPlanList)
    {
        SnTDMLSecurityUtil.printDebugMessage('createMeasureMaster() invoked--');
        SnTDMLSecurityUtil.printDebugMessage('mode--'+mode);
        SnTDMLSecurityUtil.printDebugMessage('ruleId--'+ruleId);
        SnTDMLSecurityUtil.printDebugMessage('ruleName--'+ruleName);
        SnTDMLSecurityUtil.printDebugMessage('teamId--'+teamId);
        SnTDMLSecurityUtil.printDebugMessage('workspaceId--'+workspaceId);
        SnTDMLSecurityUtil.printDebugMessage('teamInstanceId--'+teamInstanceId);
        SnTDMLSecurityUtil.printDebugMessage('generationMethod--'+generationMethod);
        SnTDMLSecurityUtil.printDebugMessage('generationProductType--'+generationProductType);
        SnTDMLSecurityUtil.printDebugMessage('productNameList--'+productNameList);
        SnTDMLSecurityUtil.printDebugMessage('productNameList size--'+productNameList.size());
        SnTDMLSecurityUtil.printDebugMessage('channelNameList--'+channelNameList);
        SnTDMLSecurityUtil.printDebugMessage('channelNameList size--'+channelNameList.size());
        SnTDMLSecurityUtil.printDebugMessage('otherCallPlanList--'+otherCallPlanList);
        SnTDMLSecurityUtil.printDebugMessage('otherCallPlanList--'+otherCallPlanList.size());

        Transient List<ActionResultWrapper> resultList = new List<ActionResultWrapper>();
        Transient List<Measure_Master__c> mmList = new List<Measure_Master__c>();
        Transient List<Measure_Master__c> strippedMMList = new List<Measure_Master__c>();
        Transient List<MCCP_CNProd__c> cnProdList = new List<MCCP_CNProd__c>();
        Transient List<SObject> country = new List<SObject>();
        Transient List<AllowableMcPdeFrequecy__mdt> mcPDEFreqList = new List<AllowableMcPdeFrequecy__mdt>();
        Transient List<String> prevSelectedProdList = new List<String>();
        Transient List<String> prevSelectedChList = new List<String>();

        Boolean isMccpEnabled = false;
        Boolean deleteMCCPRecs = true;
        Boolean errorThrown = false;

        String className = 'MCCPCallPlanCreation';
        String allowableMcpde = '';
        String alignNmsp = MCCP_Utility.alignmentNamespace();

        SObjectAccessDecision securityDecision;
        Savepoint sp = Database.setSavepoint();

        try
        {
            //Validation for MCCP Enable
            isMccpEnabled = MCCPCallPlans.checkMCCPEnabled();
            if(!isMccpEnabled && !System.Test.isRunningTest()){
                resultList.add(new ActionResultWrapper(false,ruleId, System.Label.MCCP_Disabled_Msg));
                return resultList;
            }
            
            //Default allowable MCPDE frequencies
            allowableMcpde = System.Label.Default_Allowable_MCPDE_Frequencies;

            //Get AR namespace
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ScenarioAlignmentCtlr'];
            alignNmsp = cs.NamespacePrefix!=null ? cs.NamespacePrefix+'__' : '';
            SnTDMLSecurityUtil.printDebugMessage('alignNmsp--'+alignNmsp);

            //Get allowable MC PDE Frequency from Country ID
            String countryId = MCCPCallPlans.getUserCountryId();
            String countryQuery = 'select Name from '+alignNmsp+'Country__c where Id =:countryId WITH SECURITY_ENFORCED limit 1';
            SnTDMLSecurityUtil.printDebugMessage('countryQuery--'+countryQuery);
            country = Database.query(countryQuery);

            if(country != null && country.size()>0 && String.isNotBlank((String)country[0].get('Name'))){
                String countryName = (String)country[0].get('Name');
                mcPDEFreqList = [select CountryName__c, PDEFrequencies__c From AllowableMcPdeFrequecy__mdt 
                                    where CountryName__c =:countryName limit 1];
                if(!mcPDEFreqList.isEmpty())
                    allowableMcpde = mcPDEFreqList[0].PDEFrequencies__c;
                SnTDMLSecurityUtil.printDebugMessage('allowableMcpde :: '+allowableMcpde);
            }

            //Check for BR Name duplicity
            String brQuery = 'select Id from Measure_Master__c where Name=:ruleName';
            brQuery += (!String.isBlank(ruleID) ? ' and Id!=:ruleID' : '')+' WITH SECURITY_ENFORCED';
            List<Measure_Master__c> listOtherBR = Database.query(brQuery);
            if(!listOtherBR.isEmpty())
            {
                errorThrown = true;
                resultList.add(new ActionResultWrapper(false,ruleId,System.Label.MCCP_Duplicate_BR));
            }

            if(!errorThrown)
            {
                //Populate Measure Master fields
                String brID;
                Id recordTypeId = Schema.SObjectType.Measure_Master__c.getRecordTypeInfosByName().get('MCCP Scenario').getRecordTypeId();

                Measure_Master__c mMaster = new Measure_Master__c();
                mMaster.Name = ruleName;
                mMaster.Team_Instance__c = teamInstanceId;
                mMaster.Team__c = teamId;
                mMaster.Workspace__c = workspaceId;
                mMaster.PDE_Method__c = generationMethod;
                mMaster.PDE_Method_Type__c = generationProductType;
                mMaster.Stage__c = 'Basic Information';
                mMaster.State__c = 'In Progress';
                mMaster.is_partially_published_to_call_plan__c = false;
                mMaster.is_complete__c = false;
                mMaster.is_executed__c = false;
                mMaster.is_promoted__c = false;
                mMaster.is_pushed_to_alignment__c = false;
                mMaster.RecordTypeId = recordTypeId;
                mMaster.Rule_Type__c = 'MCCP';
                mMaster.Status__c = 'In Progress'; //Added by HT(A0994) on 12th August 2020
                mMaster.Single_Product_Rule__c = generationProductType.contains('Single Product') ? true : false; 
                //Commented by HT(A0994) on 12th August 2020
                /*if(mode == 'New'){
                    mMaster.Status__c = 'In Progress';
                }*/
                mMaster.Other_MCCP_Scenario__c = '';
                if(otherCallPlanList.size()>0){
                    mMaster.Other_MCCP_Scenario__c = String.join(otherCallPlanList,',');
                }

                mMaster.MCCP_Selected_Products__c = '';
                if(productNameList.size()>0){
                    mMaster.MCCP_Selected_Products__c = String.join(productNameList,',');
                }

                mMaster.MCCP_Selected_Channels__c = '';
                if(channelNameList.size()>0){
                    mMaster.MCCP_Selected_Channels__c = String.join(channelNameList,',');
                }

                //Added by HT(A0994) on 23rd Sept 2020 for SMCCP-161
                //Delete MCCP CNProd recs only when Products/Channels are changed
                if(!String.isBlank(ruleID))
                {
                    List<Measure_Master__c> listMM = [select MCCP_Selected_Products__c,MCCP_Selected_Channels__c,
                                                        Workload_Hours__c,Desired_Range__c,PDE_Frequencies__c from  
                                                        Measure_Master__c where Id=:ruleID WITH SECURITY_ENFORCED];
                    if(!listMM.isEmpty())
                    {
                        mMaster.Workload_Hours__c = listMM[0].Workload_Hours__c;
                        mMaster.Desired_Range__c = listMM[0].Desired_Range__c;
                        mMaster.PDE_Frequencies__c = listMM[0].PDE_Frequencies__c;

                        prevSelectedProdList = listMM[0].MCCP_Selected_Products__c.split(',');
                        prevSelectedChList = listMM[0].MCCP_Selected_Channels__c.split(',');
                        SnTDMLSecurityUtil.printDebugMessage('prevSelectedProdList--'+prevSelectedProdList);
                        SnTDMLSecurityUtil.printDebugMessage('prevSelectedChList--'+prevSelectedChList);
                        SnTDMLSecurityUtil.printDebugMessage('prevSelectedProdList size--'+prevSelectedProdList.size());
                        SnTDMLSecurityUtil.printDebugMessage('prevSelectedChList size--'+prevSelectedChList.size());

                        if(!prevSelectedProdList.isEmpty() && !prevSelectedProdList.isEmpty())
                        {
                            prevSelectedProdList.sort();
                            prevSelectedChList.sort();
                            productNameList.sort();
                            channelNameList.sort();

                            Boolean sameProducts = prevSelectedProdList.equals(productNameList);
                            Boolean sameChannels = prevSelectedChList.equals(channelNameList);
                            deleteMCCPRecs = !(sameProducts && sameChannels);
                            SnTDMLSecurityUtil.printDebugMessage('sameProducts--'+sameProducts);
                            SnTDMLSecurityUtil.printDebugMessage('sameChannels--'+sameChannels);
                            SnTDMLSecurityUtil.printDebugMessage('deleteMCCPRecs--'+deleteMCCPRecs);
                        }
                    }
                }

                //Update or Insert Measure master record
                if(!String.isBlank(ruleId)){
                    mMaster.id = ruleId;
                    mmList.add(mMaster);

                    securityDecision = Security.stripInaccessible(AccessType.UPDATABLE,mmList);
                    strippedMMList = securityDecision.getRecords();
                    SnTDMLSecurityUtil.updateRecords(strippedMMList,className);
                }
                else
                {
                    mMaster.Workload_Hours__c = 8;
                    mMaster.Desired_Range__c = '20';
                    if(String.isNotBlank(allowableMcpde)){
                        mMaster.PDE_Frequencies__c = allowableMcpde;
                    }else{
                        mMaster.PDE_Frequencies__c = '1,6,12,18,24,30,36';
                    }
                    mMaster.Priority_Order_1__c = 'Target Count';
                    mMaster.Priority_2__c = 'Workload';
                    mMaster.Priority_Order_3__c = '% Product PDE';
                    mmList.add(mMaster);
                    securityDecision = Security.stripInaccessible(AccessType.CREATABLE,mmList);
                    strippedMMList = securityDecision.getRecords();
                    SnTDMLSecurityUtil.insertRecords(strippedMMList,className);
                }

                //Check for CRUD Exception
                if(strippedMMList.isEmpty()){
                    resultList.add(new ActionResultWrapper(false,'',System.Label.MCCP_BR_CRUD_Exception));
                }
                else
                {
                    brID = strippedMMList[0].Id;
                    SnTDMLSecurityUtil.printDebugMessage('brID--'+brID);

                    //Delete-Insert MCCP CNProd recs due to change in Selected Products/Channels
                    if(deleteMCCPRecs)
                    {
                        //Now lets create MCCP CN Prod recs 
                        String childQuery = 'select id from MCCP_CNProd__c where Call_Plan__c=:brID';
                        List<SObject> existingChildList = Database.query(childQuery);
                        SnTDMLSecurityUtil.printDebugMessage('existingChildList size--'+existingChildList.size());

                        //Delete existing child rec(s) if present
                        if(!existingChildList.isEmpty())
                        {
                            if(Schema.sObjectType.MCCP_CNProd__c.isDeletable()){
                            try{
                                SnTDMLSecurityUtil.deleteRecords(existingChildList,className);
                                     if(test.isrunningtest())
                                     throw new MCCPException('To cover test class');
                                }
                             catch (Exception e)
                             {
                             
                             
                             SnTDMLSecurityUtil.printDebugMessage(e.getMessage());
                             SalesIQSnTLogger.createHandledErrorLogs(e,SalesIQSnTLogger.MCCP_MODULE ,'');
                             }
                            }
                            else
                            {
                                errorThrown = true;
                                resultList.add(new ActionResultWrapper(false,ruleID,System.Label.MCCP_CN_Prod_CRUD_Exception));
                            }
                        }

                        //Now create all the new Child records
                        if(!errorThrown)
                        {
                            //Get Channel benchmark data
                            Transient Map<String,Channel_Info__c> mapChannelToRec = new Map<String,Channel_Info__c>();
                            List<Channel_Info__c> channelInfoList = new List<Channel_Info__c>();

                            channelInfoList = [select Channel_Name__c,includeForOptimisation__c,Workload_Equivalent__c,
                                                Channel_Effectiveness_P1__c,Channel_Effectiveness_P2__c,
                                                Channel_Effectiveness_P3__c,Channel_Effectiveness_P4__c
                                                from Channel_Info__c where Country__c=:countryId and 
                                                Team__c=:teamId WITH SECURITY_ENFORCED];
                            SnTDMLSecurityUtil.printDebugMessage('channelInfoList size--'+channelInfoList.size());
                            if(!channelInfoList.isEmpty())
                            {
                                for(Channel_Info__c ci : channelInfoList)
                                {
                                    if(!mapChannelToRec.containsKey(ci.Channel_Name__c)){
                                        mapChannelToRec.put(ci.Channel_Name__c,ci);
                                    }
                                }
                            }
                            SnTDMLSecurityUtil.printDebugMessage('mapChannelToRec keySet--'+mapChannelToRec.keySet());

                            //Create Channel Data
                            String p1Effect;
                            String p2Effect;
                            String p3Effect;
                            String p4Effect;
                            String touchpoint;
                            Boolean includeOptimization;
                            Channel_Info__c channelInfoRec;

                            for(String channel : channelNameList)
                            {
                                Id channelRecordTypeId = Schema.SObjectType.MCCP_CNProd__c.getRecordTypeInfosByName().get('Channel').getRecordTypeId();
                                MCCP_CNProd__c channelTypeRecord = new MCCP_CNProd__c();
                                channelTypeRecord.RecordTypeId  = channelRecordTypeId;
                                channelTypeRecord.Channel__c = channel;
                                channelTypeRecord.Call_Plan__c = brID;

                                if(mapChannelToRec.get(channel)!=null)
                                {
                                    channelInfoRec = mapChannelToRec.get(channel);
                                    p1Effect = channelInfoRec.Channel_Effectiveness_P1__c!=null ? channelInfoRec.Channel_Effectiveness_P1__c : '';
                                    p2Effect = channelInfoRec.Channel_Effectiveness_P2__c!=null ? channelInfoRec.Channel_Effectiveness_P2__c : '';
                                    p3Effect = channelInfoRec.Channel_Effectiveness_P3__c!=null ? channelInfoRec.Channel_Effectiveness_P3__c : '';
                                    p4Effect = channelInfoRec.Channel_Effectiveness_P4__c!=null ? channelInfoRec.Channel_Effectiveness_P4__c : '';
                                    touchpoint = channelInfoRec.Workload_Equivalent__c!=null ? channelInfoRec.Workload_Equivalent__c : ''; 
                                    channelTypeRecord.CE_Priority1__c  = isNumericString(p1Effect) ? Decimal.valueOf(p1Effect) : null;
                                    channelTypeRecord.CE_Priority2__c = isNumericString(p2Effect) ? Decimal.valueOf(p2Effect) : null;
                                    channelTypeRecord.CE_Priority3__c  = isNumericString(p3Effect) ? Decimal.valueOf(p3Effect) : null;
                                    channelTypeRecord.CE_Priority4__c = isNumericString(p4Effect) ? Decimal.valueOf(p4Effect) : null;
                                    channelTypeRecord.WLE_TP_PerHour__c = isNumericString(touchpoint) ? Decimal.valueOf(touchpoint) : null;
                                }
                                cnProdList.add(channelTypeRecord);
                            }
                            SnTDMLSecurityUtil.printDebugMessage('cnProdList size till date--'+cnProdList.size());

                            //Create Product and Junction data
                            for(String prod : productNameList)
                            {
                                Id productRecordTypeId = Schema.SObjectType.MCCP_CNProd__c.getRecordTypeInfosByName().get('Product').getRecordTypeId();
                                MCCP_CNProd__c prodTypeRecord = new MCCP_CNProd__c();
                                prodTypeRecord.RecordTypeId  = productRecordTypeId;
                                prodTypeRecord.Product__c = prod;
                                prodTypeRecord.Call_Plan__c = brID;
                                prodTypeRecord.Weights__c = (100.00/productNameList.size()).setScale(2);
                                cnProdList.add(prodTypeRecord);

                                for(String channel : channelNameList)
                                {
                                    Id junctionRecordTypeId = Schema.SObjectType.MCCP_CNProd__c.getRecordTypeInfosByName().get('Junction').getRecordTypeId();
                                    MCCP_CNProd__c junctionTypeRecord = new MCCP_CNProd__c();
                                    junctionTypeRecord.RecordTypeId  = junctionRecordTypeId;
                                    junctionTypeRecord.PromotionSelected__c = true;
                                    junctionTypeRecord.Call_Plan__c = brID;
                                    junctionTypeRecord.Product__c = prod;
                                    junctionTypeRecord.Channel__c = channel;

                                    if(mapChannelToRec.get(channel)!=null){
                                        junctionTypeRecord.OptimisationSelected__c = mapChannelToRec.get(channel).includeForOptimisation__c;
                                    }
                                    else{
                                        junctionTypeRecord.OptimisationSelected__c = true;
                                    }
                                    cnProdList.add(junctionTypeRecord);
                                }
                            }
                            SnTDMLSecurityUtil.printDebugMessage('cnProdList size till date--'+cnProdList.size());
                            
                            if(cnProdList != null && cnProdList.size()>0)
                            {
                                securityDecision = Security.stripInaccessible(AccessType.CREATABLE,cnProdList);
                                List<SObject> insertMCCPProdList = securityDecision.getRecords();
                                try{
                                SnTDMLSecurityUtil.insertRecords(insertMCCPProdList,className);
                                   if(test.isrunningtest())
                                    throw new MCCPException('To cover test class');
                                }
                                
                                catch (Exception e)
                                {
                                
                                 SnTDMLSecurityUtil.printDebugMessage(e.getMessage());
                                 SalesIQSnTLogger.createHandledErrorLogs(e,SalesIQSnTLogger.MCCP_MODULE,'');
                                }
                                
                                if(insertMCCPProdList.isEmpty())
                                {
                                    errorThrown = true;
                                    Database.rollback(sp);
                                    resultList.add(new ActionResultWrapper(false,'',System.Label.MCCP_CN_Prod_CRUD_Exception));
                                }
                                else{
                                    resultList.add(new ActionResultWrapper(true,brID,System.Label.Call_Plan_Creation_Success_Msg));
                                }
                            }
                        }
                    }
                    else{
                        resultList.add(new ActionResultWrapper(true,brID,System.Label.Call_Plan_Creation_Success_Msg));
                    }
                }
            }
            
            if(test.isrunningtest())
            throw new MCCPException('To cover test class');
        }
        catch (Exception e) 
        {
            SnTDMLSecurityUtil.printDebugMessage('Error in createMeasureMaster()-->'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack Trace->'+e.getStackTraceString());
            Database.rollback(sp);
            resultList.add(new ActionResultWrapper(false,ruleID,System.Label.MCCP_Exception_Save_BR));
            
            
            SnTDMLSecurityUtil.printDebugMessage(e.getMessage());
            SalesIQSnTLogger.createUnHandledErrorLogs(e,SalesIQSnTLogger.MCCP_MODULE ,'MCCPCallPlanCreation','');
            
        }

        SnTDMLSecurityUtil.printDebugMessage('resultList--'+resultList);
        SnTDMLSecurityUtil.printDebugMessage('resultList size--'+resultList.size());
        return resultList;
    }

    @AuraEnabled
    public static Boolean isNumericString(String str)
    {
        try{
            Decimal d = Decimal.valueOf(str);
            if(test.isrunningtest())
            throw new MCCPException('To cover test class');
        }
        catch(Exception e){
           
            SnTDMLSecurityUtil.printDebugMessage(e.getMessage());
            SalesIQSnTLogger.createUnHandledErrorLogs(e,SalesIQSnTLogger.MCCP_MODULE ,'MCCPCallPlanCreation','');
            return false;
        }
        return true;
    }

    //Added by HT(A0994) on 12th August 2020
    public class ActionResultWrapper
    {
        @AuraEnabled public Boolean dataSaved {get;set;}
        @AuraEnabled public String ruleID {get;set;}
        @AuraEnabled public String message {get;set;}

        ActionResultWrapper(Boolean dataSaved,String ruleID,String message)
        {
            this.dataSaved = dataSaved;
            this.ruleID = ruleID;
            this.message = message;
        }
    }
}