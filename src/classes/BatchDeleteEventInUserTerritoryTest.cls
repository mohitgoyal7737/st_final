@isTest
public class BatchDeleteEventInUserTerritoryTest {
    
    static testMethod void testMethod1() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        

        User_Territory__c u = new User_Territory__c();
        insert u;
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            BatchDeleteEventInUserTerritory obj=new BatchDeleteEventInUserTerritory();
            obj.query = 'select id,User__c,Territory__c,Event__c from User_Territory__c';
            Database.executeBatch(obj);
            
        }
        Test.stopTest();
    }
    
}