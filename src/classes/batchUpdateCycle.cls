global class batchUpdateCycle implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'select id,Cycle2__c from BU_Response__c';
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<BU_Response__c> scope) {
         for(BU_Response__c a : scope)
         {
             a.Cycle2__c = 'a1V0Y000000k3WH';            
         }
         update scope;
    }   
    
    global void finish(Database.BatchableContext BC) {
    }
}