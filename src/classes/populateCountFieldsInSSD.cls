global with sharing class populateCountFieldsInSSD implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts {
    
    public String query;
    public String workspace = '';
    public String teamInstanceName = '';
    public String productCode = ''; 
    public Integer distinctStagingDataCount = 0;
    public Integer distinctVeevaDataCount = 0;
    public Integer distinctStagingDataCount1 = 0;
    public Integer distinctVeevaDataCount1 = 0; 
    public String newSource = '';
    public Integer newprofiledCustCount = 0;
    public Integer newalignedCustCount = 0;
    public Integer newalignedButNotProfiledCount = 0;
    public Integer newprofiledCustButNotAlignCount = 0;
    public Boolean isBatchChained =false;
    public Boolean veevaDirectLoad = false;

    public String changeReqID;
    public Boolean check;
    public String status;

    public Map<String,Map<String,String>> finalMap = new Map<String,Map<String,String>>();


    global populateCountFieldsInSSD(String workspace, String teamInstanceName, String productCode){
        
        this.workspace = workspace;
        this.teamInstanceName = teamInstanceName;
        this.productCode = productCode;

        query = 'Select Id, Name, Product_Code__c, Team_Instance__c, Team_Instance__r.Name FROM Product_Catalog__c WHERE Product_Code__c ='+'\''+productCode+'\''+'AND Team_Instance__r.Name ='+'\''+teamInstanceName+'\''+'AND IsActive__c = true';

        this.query = query;
        
    }

    global populateCountFieldsInSSD(String teamIns, Boolean isBatchChained, String crId, Boolean chk, String stat){
        this.teamInstanceName = teamIns;
        this.isBatchChained = isBatchChained;
        this.changeReqID = crId;
        this.check = chk;
        this.status = stat;

        query = 'Select Id, Name, Product_Code__c, Team_Instance__c, Team_Instance__r.Name FROM Product_Catalog__c WHERE Team_Instance__r.Name ='+'\''+teamInstanceName+'\''+'AND IsActive__c = true';
        
        this.query = query;
    }
    global populateCountFieldsInSSD(String teamIns){

        this.teamInstanceName = teamIns;
        this.veevaDirectLoad = true;
        
        query = 'Select Id, Name, Product_Code__c, Team_Instance__c, Team_Instance__r.Name FROM Product_Catalog__c WHERE Team_Instance__r.Name ='+'\''+teamInstanceName+'\''+'AND IsActive__c = true';

        this.query = query;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Product_Catalog__c> scope) {

        String prodID = '';
        String teamInstanceID = '';
        Set<String> pos1 = new Set<String>();
        Map<String,String> newparamToCount;

        System.debug('Query is : '+query);
        try{
            //Get prod ID and TeamInstance ID
            for(Product_Catalog__c p:scope){
                
                prodID = p.Id;
                teamInstanceID = p.Team_Instance__c;
                teamInstanceName = p.Team_Instance__r.Name;
                productCode = p.Product_Code__c;

                Integer newParameterCount = 0;

                System.debug('prodID : '+prodID);
                System.debug('teamInstanceID : '+teamInstanceID);

                //Calculating Profiled Customers
                //All count of Staging Survey Data
                for(AggregateResult stagingSurveyData : [SELECT COUNT_DISTINCT(Account_Lookup__c) c1 FROM Staging_Survey_Data__c WHERE Product_Code__c =:productCode AND Team_Instance__c =:teamInstanceName /*AND Account_Lookup__c NOT IN (Select Account_Lookup__c FROM Staging_Veeva_Cust_Data__c WHERE BRAND_ID__c =:productCode AND Team_Instance__c =:teamInstanceName)*/]){

                    distinctStagingDataCount = Integer.valueOf(stagingSurveyData.get('c1'));
                }
                //Distinct count of Staging Veeva Cust Data
                for(AggregateResult stagingVeevaData : [SELECT COUNT_DISTINCT(Account_Lookup__c) c2 FROM Staging_Veeva_Cust_Data__c WHERE BRAND_ID__c =:productCode AND Team_Instance__c =:teamInstanceName AND Account_Lookup__c NOT IN (Select Account_Lookup__c FROM Staging_Survey_Data__c WHERE Product_Code__c =:productCode AND Team_Instance__c =:teamInstanceName)]){

                    distinctVeevaDataCount = Integer.valueOf(stagingVeevaData.get('c2'));
                }

                if(distinctStagingDataCount == 0 && distinctVeevaDataCount>0){
                    newSource = 'CRM';
                }else if(distinctStagingDataCount >0 && distinctVeevaDataCount == 0){
                    newSource = 'File';
                }else if(distinctStagingDataCount >0 && distinctVeevaDataCount>0){
                    newSource = 'File, CRM';
                }

                //Calculate Profiled Customers
                newprofiledCustCount = distinctStagingDataCount + distinctVeevaDataCount;

                List<AxtriaSalesIQTM__Position_Product__c> ppList = [Select AxtriaSalesIQTM__Position__c, Product_Catalog__r.Name, AxtriaSalesIQTM__Team_Instance__r.Name FROM AxtriaSalesIQTM__Position_Product__c WHERE Product_Catalog__r.Product_Code__c =: productCode  AND AxtriaSalesIQTM__Team_Instance__r.Name =: teamInstanceName AND AxtriaSalesIQTM__isActive__c = true];

                if(ppList.size()>0){
                    for(AxtriaSalesIQTM__Position_Product__c pp : ppList){
                        pos1.add(pp.AxtriaSalesIQTM__Position__c);
                    }            
                    System.debug('Pos Set : '+pos1);
                    System.debug('Pos Set Size: '+pos1.size());
                }

                //Calculate Aligned Customers
                for(AggregateResult posAccCount : [Select COUNT_DISTINCT(AxtriaSalesIQTM__Account__c) c3 FROM AxtriaSalesIQTM__Position_Account__c WHERE AxtriaSalesIQTM__Position__c IN:pos1 AND AxtriaSalesIQTM__Team_Instance__r.Name =:teamInstanceName AND (AxtriaSalesIQTM__Assignment_Status__c ='Active' OR AxtriaSalesIQTM__Assignment_Status__c ='Future Active')]){
                    newalignedCustCount = Integer.valueOf(posAccCount.get('c3'));
                }

                //Customers Aligned But Not Profiled
                for(AggregateResult posAccCount1 : [SELECT COUNT_DISTINCT(AxtriaSalesIQTM__Account__c) c4 FROM AxtriaSalesIQTM__Position_Account__c WHERE AxtriaSalesIQTM__Position__c IN:pos1 AND AxtriaSalesIQTM__Team_Instance__r.Name =:teamInstanceName AND (AxtriaSalesIQTM__Assignment_Status__c ='Active' OR AxtriaSalesIQTM__Assignment_Status__c ='Future Active') AND AxtriaSalesIQTM__Account__c NOT IN (Select Account_Lookup__c FROM Staging_Survey_Data__c WHERE Product_Code__c =:productCode AND Team_Instance__c =:teamInstanceName) AND AxtriaSalesIQTM__Account__c NOT IN (Select Account_Lookup__c FROM Staging_Veeva_Cust_Data__c WHERE BRAND_ID__c =:productCode AND Team_Instance__c =:teamInstanceName)]){
                    newalignedButNotProfiledCount = Integer.valueOf(posAccCount1.get('c4'));
                }
                
                //Customers Profiled But Not Aligned
                for(AggregateResult stagingSurveyData1 : [SELECT COUNT_DISTINCT(Account_Lookup__c) c5 FROM Staging_Survey_Data__c WHERE Product_Code__c =:productCode AND Team_Instance__c =:teamInstanceName /*AND Account_Lookup__c NOT IN (Select Account_Lookup__c FROM Staging_Veeva_Cust_Data__c WHERE BRAND_ID__c =:productCode AND Team_Instance__c =:teamInstanceName)*/ AND Account_Lookup__c NOT IN (SELECT AxtriaSalesIQTM__Account__c FROM AxtriaSalesIQTM__Position_Account__c WHERE AxtriaSalesIQTM__Position__c IN : pos1 AND AxtriaSalesIQTM__Team_Instance__r.Name =:teamInstanceName AND (AxtriaSalesIQTM__Assignment_Status__c ='Active' OR AxtriaSalesIQTM__Assignment_Status__c ='Future Active'))]){
                    distinctStagingDataCount1 = Integer.valueOf(stagingSurveyData1.get('c5'));
                }

                System.debug('distinctStagingDataCount1 : '+distinctStagingDataCount1);

                for(AggregateResult stagingVeevaData1 : [SELECT COUNT_DISTINCT(Account_Lookup__c) c6 FROM Staging_Veeva_Cust_Data__c WHERE BRAND_ID__c =:productCode AND Team_Instance__c =:teamInstanceName AND Account_Lookup__c NOT IN (Select Account_Lookup__c FROM Staging_Survey_Data__c WHERE Product_Code__c =:productCode AND Team_Instance__c =:teamInstanceName) AND Account_Lookup__c NOT IN (SELECT AxtriaSalesIQTM__Account__c FROM AxtriaSalesIQTM__Position_Account__c WHERE AxtriaSalesIQTM__Position__c IN : pos1 AND AxtriaSalesIQTM__Team_Instance__r.Name =:teamInstanceName AND (AxtriaSalesIQTM__Assignment_Status__c ='Active' OR AxtriaSalesIQTM__Assignment_Status__c ='Future Active'))]){
                    distinctVeevaDataCount1 = Integer.valueOf(stagingVeevaData1.get('c6'));
                }

                newprofiledCustButNotAlignCount = distinctStagingDataCount1 + distinctVeevaDataCount1;

                for(AggregateResult mdt : [SELECT COUNT_DISTINCT(Display_Name__c) c7 FROM MetaData_Definition__c WHERE Product_Catalog__r.Product_Code__c =:productCode AND Team_Instance__r.Name =:teamInstanceName]){
                    newParameterCount = Integer.valueOf(mdt.get('c7'));
                }
                
                newparamToCount = new Map<String,String>(); 
                newparamToCount.put('newprofiledCustCount',String.valueOf(newprofiledCustCount));
                newparamToCount.put('newalignedCustCount',String.valueOf(newalignedCustCount));
                newparamToCount.put('newprofiledCustButNotAlignCount',String.valueOf(newprofiledCustButNotAlignCount));
                newparamToCount.put('newalignedButNotProfiledCount',String.valueOf(newalignedButNotProfiledCount));     
                newparamToCount.put('newParameterCount',String.valueOf(newParameterCount));
                newparamToCount.put('newSource', newSource);

                finalMap.put(prodID+'-'+teamInstanceID, newparamToCount);
            }
        }catch(Exception e){
            SalesIQSnTLogger.createUnHandledErrorLogsforProfilingData(e,SalesIQSnTLogger.PROFILING_DATA_MODULE,'populateCountFieldsInSSD'); 
        }
    }

    global void finish(Database.BatchableContext BC){
        System.debug('finalMap : '+finalMap);
        try{
            Map<String,Set<String>> teamInsToProductMap = new Map<String,Set<String>>();
            Set<String> products = new Set<String>();

            List<Product_Catalog__c> prodToUpdate = new List<Product_Catalog__c>();
            List<Product_Catalog__c> prodList1;
            if(isBatchChained || veevaDirectLoad){
                prodList1 = [Select Id, Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Name, Country_Lookup__r.Name, Team_Instance__r.AxtriaSalesIQTM__Team__r.Name, Team_Instance__r.Name, Team_Instance__c, Name, Product_Code__c FROM Product_Catalog__c WHERE Team_Instance__r.Name =:teamInstanceName];
            }else{
                prodList1 = [Select Id, Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Name, Country_Lookup__r.Name, Team_Instance__r.AxtriaSalesIQTM__Team__r.Name, Team_Instance__r.Name, Team_Instance__c, Name, Product_Code__c FROM Product_Catalog__c WHERE Product_Code__c =:productCode AND Team_Instance__r.Name =: teamInstanceName];
            }
            //Getting Time acc to user Locale
            User objUser= [SELECT timezonesidkey FROM User WHERE Id =:UserInfo.getUserId()];
            Datetime dtDateTime= System.now();

            String strDateTime= dtDateTime.format('MM/dd/yyyy HH:mm:ss', objUser.timezonesidkey);

            for(String s : finalMap.keySet()){
                for(Product_Catalog__c pc : prodList1){
                    System.debug('Key is prod-teamins: '+s);
                    if(pc.Id == s.split('-')[0] && pc.Team_Instance__c == s.split('-')[1]){
                    //if(finalMap.containsKey(pc.Id+'-'+pc.Team_Instance__c)){
                        System.debug('Hi we are here.'+pc.Id);
                        pc.Customers_Profiled__c = Integer.valueOf(finalMap.get(s).get('newprofiledCustCount'));
                        pc.Customers_Aligned__c = Integer.valueOf(finalMap.get(s).get('newalignedCustCount'));
                        pc.Customers_Profiled_But_Not_Aligned__c = Integer.valueOf(finalMap.get(s).get('newprofiledCustButNotAlignCount'));
                        pc.Customers_Aligned_But_Not_Profiled__c = Integer.valueOf(finalMap.get(s).get('newalignedButNotProfiledCount'));
                        pc.Parameter_Count__c = Integer.valueOf(finalMap.get(s).get('newParameterCount'));
                        pc.Source__c = finalMap.get(s).get('newSource');
                        pc.LastUpdatedByBatch__c = strDateTime;
                        prodToUpdate.add(pc);
                        products.add(pc.Name);
                    }
                }
            }
            System.debug('prodToUpdate : '+prodToUpdate);
            
            teamInsToProductMap.put(teamInstanceName,products);
            
            if(prodToUpdate != null && prodToUpdate.size()>0){
                update prodToUpdate;
            }
            if(isBatchChained){
                BatchUpdateTempObjRecsCR batchCall2 = new BatchUpdateTempObjRecsCR(changeReqID,check,status,'Done');
                Database.executeBatch(batchCall2,2000);
            }

            //SR-455
            System.debug('teamInsToProductMap : '+teamInsToProductMap);

            String uniqueKey = prodList1[0].Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Name+'_'+prodList1[0].Country_Lookup__r.Name+'_'+prodList1[0].Team_Instance__r.AxtriaSalesIQTM__Team__r.Name+'_'+prodList1[0].Team_Instance__r.Name;

            System.debug('uniqueKey : '+uniqueKey);
            //Dev 12 Workspace_USA_Oncology_Oncology-Q4-2019A

            AxtriaSalesIQTM__ETL_Config__c etlConfig = [SELECT Id, AxtriaSalesIQTM__End_Point__c, AxtriaSalesIQTM__SF_UserName__c, AxtriaSalesIQTM__SF_Password__c, AxtriaSalesIQTM__S3_Security_Token__c FROM AxtriaSalesIQTM__ETL_Config__c WHERE Name = 'ProfilingDataDownload' WITH SECURITY_ENFORCED];
            
            if(etlConfig != null){
                String nameSpace = MCCP_Utility.sntNamespace('');
                String align_namespace = MCCP_Utility.alignmentNamespace();
                String endPointURL = etlConfig.AxtriaSalesIQTM__End_Point__c;
                endPointURL = endPointURL.replaceAll( '\\s+', '%20');
                endPointURL = endPointURL.replaceAll('\'', '%27');

                Map<String,Object> parameterMap = new Map<String,Object>();
                parameterMap.put('username',etlConfig.AxtriaSalesIQTM__SF_UserName__c);
                parameterMap.put('password',etlConfig.AxtriaSalesIQTM__SF_Password__c);
                parameterMap.put('security_token',etlConfig.AxtriaSalesIQTM__S3_Security_Token__c);
                parameterMap.put('team_name', prodList1[0].Team_Instance__r.AxtriaSalesIQTM__Team__r.Name);
                parameterMap.put('team_instance', prodList1[0].Team_Instance__r.Name);
                parameterMap.put('namespace',nameSpace);
                parameterMap.put('align_namespace',align_namespace);

                List<Map<String,String>> arrOfProdobj = new List<Map<String,String>>();
                Map<String,String> prodObj;

                for(Product_Catalog__c p : prodList1){
                    prodObj = new Map<String,String>();
                    prodObj.put('product_code',p.Product_Code__c);
                    prodObj.put('product_name',p.Name);
                    prodObj.put('unique_key',uniqueKey+'_'+p.Name);
                    prodObj.put('product_id',p.Id);
                    arrOfProdobj.add(prodObj);
                }
                parameterMap.put('product',arrOfProdobj);
                String postRequestBody = JSON.Serialize(parameterMap);
                SnTDMLSecurityUtil.printDebugMessage('postRequestBody ==== ' + postRequestBody);
                //Call Queueable
                ID jobID = System.enqueueJob(new AsyncCalloutProfilingDataDownload(endPointURL,postRequestBody));
            }
        }catch(Exception e){
            SalesIQSnTLogger.createUnHandledErrorLogsforProfilingData(e,SalesIQSnTLogger.PROFILING_DATA_MODULE,'populateCountFieldsInSSD'); 
        }
    }
}