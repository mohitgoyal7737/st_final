@isTest
public class Populate_RuleTypeTest {
    @isTest static void testMethod1(){
        
        User loggedInUser = new User(id=UserInfo.getUserId());
       
        AxtriaSalesIQTM__Organization_Master__c orgmas = new AxtriaSalesIQTM__Organization_Master__c();
        orgmas.Name = 'abcd';
        orgmas.AxtriaSalesIQTM__Org_Level__c = 'Global';
        orgmas.AxtriaSalesIQTM__Parent_Country_Level__c = true;
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c countr = new AxtriaSalesIQTM__Country__c();
        countr.Name = 'Spain';
        orgmas.AxtriaSalesIQTM__Org_Level__c = 'Global';
        countr.AxtriaSalesIQTM__Parent_Organization__c = orgmas.id;
        countr.AxtriaSalesIQTM__Status__c = 'Active';
        countr.MCCP_Enabled__c = true;
        insert  countr;
        
        Account a1= TestDataFactory.createAccount();
        a1.AccountNumber = 'BH10461999';
        a1.Status__c = 'Active';
        a1.Merge_Account_Number__c = 'BH10461999';
        insert a1;
        
        Account acc= TestDataFactory.createAccount();
        acc.AccountNumber = 'BH10455999';
        acc.Status__c = 'Active';
        acc.Merge_Account_Number__c =  'BH10455999';
        acc.AxtriaSalesIQTM__Active__c = 'Active';
        acc.Type = 'HCO';
        insert acc;

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
        insert team;

        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        insert scen;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Name = 'Test';
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        teamins.AxtriaSalesIQTM__IC_EffEndDate__c = date.today()+1;
        teamins.AxtriaSalesIQTM__IC_EffstartDate__c = date.today();
        insert teamins;
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__inactive__c = false;
        insert pos;
              
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        posAccount.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        posAccount.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        posAccount.AxtriaSalesIQTM__Account_Alignment_Type__c = 'Implicit';
        insert posAccount;
        
        temp_Obj__c tmp = new temp_Obj__c();
  	tmp.Status__c ='New';
      	tmp.Event__c = 'Insert';
  	tmp.Object__c ='Deassign_Postiton_Account__c';
        tmp.Account_Text__c = 'BH10455999';
        tmp.Position_Text__c = 'test';
        tmp.Team_Instance_Text__c = 'test';
  	insert tmp;        
        
        AxtriaSalesIQTM__Business_Rules__c b1 = new AxtriaSalesIQTM__Business_Rules__c();
        b1.AxtriaSalesIQTM__Scenario__c = scen.id;
        b1.AxtriaSalesIQTM__Rule_Type__c = 'Bottom Up';
        insert b1;
               
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD , false));
            Datetime dt = Datetime.now().addMinutes(1);
            Populate_RuleType obj = new Populate_RuleType(true,countr.id);
            //String query = 'Select id,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Metric1_Updated__c,AxtriaSalesIQTM__Metric2_Updated__c,AxtriaSalesIQTM__Metric3_Updated__c, AxtriaSalesIQTM__Metric4_Updated__c, AxtriaSalesIQTM__Metric5_Updated__c, AxtriaSalesIQTM__Metric6_Updated__c, Effective_Calls__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Team_Instance__c= :team_instance_id';
            //obj.query=query;
            Database.executeBatch(obj);
        }
        Test.stopTest();
    }
}