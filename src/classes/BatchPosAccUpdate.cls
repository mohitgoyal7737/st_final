global class BatchPosAccUpdate implements Database.Batchable<sObject> , Database.Stateful{
    
  public Set<String> inactiveID = new Set<String>();
  public String query {get;set;}
  public Map<String,AxtriaSalesIQTM__Position_Account__c> mapId2PosAcc = new Map<String,AxtriaSalesIQTM__Position_Account__c>();
  List<AxtriaSalesIQTM__Position_Account__c> paList = new List<AxtriaSalesIQTM__Position_Account__c>();

  global BatchPosAccUpdate(Map<String,AxtriaSalesIQTM__Position_Account__c> mapId2PA){
    inactiveID = mapId2PA.keyset();
    query = '';
    for(AxtriaSalesIQTM__Position_Account__c pa : mapId2PA.values())
      mapId2PosAcc.put(pa.id,pa);
    
  }
  
  global Database.QueryLocator start(Database.BatchableContext bc) {
  
      query = 'select id,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c,' +
              ' AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account_Alignment_Type__c,' +
              ' AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,'+
              ' AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c from AxtriaSalesIQTM__Position_Account__c'+
              ' where id In: inactiveID';
      system.debug('++++++++++QUERY IS:::::'+query);
      return Database.getQueryLocator(query);
  }
  
  global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Account__c> scope) {
      System.debug('Inactive Position Account');
      for(AxtriaSalesIQTM__Position_Account__c paRec : scope){
          paList.add(mapId2PosAcc.get(paRec.id));
      }
      System.debug('Position Account List update list' +paList);
      System.debug('Position Account List update list size' +paList.size());
      update paList;
  }

  global void finish(Database.BatchableContext BC) {
      
  }

}