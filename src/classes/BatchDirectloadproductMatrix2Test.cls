@isTest
public class BatchDirectloadproductMatrix2Test {
    static testMethod void testMethod1() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Name ='USA';
        insert countr;
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc.AxtriaSalesIQTM__Country__c = countr.id;
        acc.AccountNumber = 'BH10461999';
        acc.AxtriaSalesIQTM__AccountType__c = 'HCP';
        acc.Status__c = 'Active';
        acc.Type = 'HCP';
        insert acc;
        Account acc1= TestDataFactory.createAccount();
        acc1.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc1.AxtriaSalesIQTM__Country__c = countr.id;
        acc1.AccountNumber = 'BH10461969';
        acc1.AxtriaSalesIQTM__AccountType__c = 'HCA';
        acc1.Status__c = 'Active';
        acc1.Type = 'HCA';
        insert acc1;
        Account acc11= TestDataFactory.createAccount();
        acc11.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc11.AxtriaSalesIQTM__Country__c = countr.id;
        acc11.AccountNumber = 'BH10462969';
        acc11.AxtriaSalesIQTM__AccountType__c = 'HCA';
        acc11.Status__c = 'Active';
        acc11.Type = 'HCA';
        insert acc11;
        AxtriaSalesIQTM__Affiliation_Network__c affnet = TestDataFactory.createAffliNet(countr);
        insert affnet;
        AxtriaSalesIQTM__Account_Affiliation__c accaff = TestDataFactory.createAcctAffli(acc,affnet);
        accaff.Account_Number__c = 'BH10461999';
        accaff.AxtriaSalesIQTM__Root_Account__c = acc.id;
        accaff.AxtriaSalesIQTM__Parent_Account__c = acc.Id;
        accaff.AxtriaSalesIQTM__Account__c = acc.Id;
        accaff.Affiliation_Status__c ='Active';
        accaff.AxtriaSalesIQTM__Is_Primary__c = true;
        accaff.AxtriaSalesIQTM__Active__c = true;
        insert accaff;
        AxtriaSalesIQTM__Account_Affiliation__c accaff1 = TestDataFactory.createAcctAffli(acc1,affnet);
        accaff1.Account_Number__c = 'BH10461969';
        accaff1.AxtriaSalesIQTM__Root_Account__c = acc.id;
        accaff1.AxtriaSalesIQTM__Parent_Account__c = acc.Id;
        accaff1.AxtriaSalesIQTM__Account__c = acc1.Id;
        accaff1.Affiliation_Status__c ='Active';
        accaff1.AxtriaSalesIQTM__Is_Primary__c = true;
        accaff1.AxtriaSalesIQTM__Active__c = true;
        insert accaff1;
        AxtriaSalesIQTM__Account_Affiliation__c accaff11 = TestDataFactory.createAcctAffli(acc,affnet);
        accaff11.Account_Number__c = 'BH10462969';
        accaff11.AxtriaSalesIQTM__Root_Account__c = acc11.id;
        accaff11.AxtriaSalesIQTM__Parent_Account__c = acc11.Id;
        accaff11.AxtriaSalesIQTM__Account__c = acc.Id;
        accaff11.Affiliation_Status__c ='Active';
        accaff11.AxtriaSalesIQTM__Is_Primary__c = true;
        accaff11.AxtriaSalesIQTM__Active__c = true;
        insert accaff11;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        insert scen;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Name = 'test';
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        //teamins.AxtriaSalesIQTM__Team_Cycle_Name__c = 'Current';
        //teamins.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        insert teamins;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        AxtriaSalesIQTM__Product__c product = new AxtriaSalesIQTM__Product__c();
        product.Team_Instance__c = teamins.id;
        product.CurrencyIsoCode = 'USD'; 
        product.Name= 'Test';
        product.AxtriaSalesIQTM__Product_Code__c = 'testproduct'; 
        
        product.AxtriaSalesIQTM__Effective_End_Date__c = date.today();
        product.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        insert product;
        Master_List_AccProd__c m = new Master_List_AccProd__c();
        m.Status__c ='New';
        m.Team_Instance__c = teamins.id;
        m.Account__c = acc.id;
        m.POSITION__c = pos.id;
        m.Product__c = product.id;
        insert m;
        ProductToDetailGroup__c p = new ProductToDetailGroup__c();
        insert p;
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            BatchDirectloadproductMatrix2 obj=new BatchDirectloadproductMatrix2();
            //obj.query='select Id,AccountNumber__c,Position_Code__c,Team_Instance_Name__c,Status__c,Account_Type__c,Rule_Type__c from temp_Obj__c ';
            Database.executeBatch(obj);
            
        }
        Test.stopTest();
    }
    
    
}