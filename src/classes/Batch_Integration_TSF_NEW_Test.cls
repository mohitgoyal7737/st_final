@isTest
private class Batch_Integration_TSF_NEW_Test 
{
    static testMethod void testMethod1() 
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
            
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;
        Account acc= TestDataFactory.createAccount();
        acc.AccountNumber ='BH10643156';
        insert acc;
        
        AxtriaSalesIQTM__Affiliation_Network__c affnet = TestDataFactory.createAffliNet(countr);
        insert affnet;
        AxtriaSalesIQTM__Account_Affiliation__c accaff = TestDataFactory.createAcctAffli(acc,affnet);
        insert accaff;
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        Measure_Master__c mmc1 = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc1.Team_Instance__c = teamins.id;
        insert mmc1;
        Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
        pp.Name ='MARKET_ CRC';
        insert pp;
        Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
        rp.Parameter__c = pp.id;
        insert rp;
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;

        Source_to_Destination_Mapping__c sdm = new Source_to_Destination_Mapping__c();
        sdm.Load_Type__c = 'TSF';
        sdm.Destination_Object_Field__c = 'Metric10__c';
        sdm.Source_Object_Field__c = 'Metric10__c';
        sdm.Team_Instance__c = teamins.Id;
        insert sdm;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        positionAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c = true;
        insert positionAccountCallPlan;
        List<String> allTeamInstances = new List<String>();
        allTeamInstances.add(teamins.id);
        insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
        new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'UpdatePositionCode')};

        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            Batch_Integration_TSF_NEW o3=new Batch_Integration_TSF_NEW(allTeamInstances);
            o3.query = 'select id, AxtriaSalesIQTM__Segment10__c, P1__c,AxtriaSalesIQTM__Position__r.Employee1__c, AxtriaSalesIQTM__Position__r.Employee1__r.Name,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__r.Name, Segment__c, AxtriaSalesIQTM__Account__r.AZ_VeevaID__c , AxtriaSalesIQTM__Account__r.AccountNumber ,Adoption__c, Potential__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name, Country__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, Party_ID__r.Accessibility_Range__c, AxtriaSalesIQTM__Team_Instance__c,Accessibility_Range__c  from AxtriaSalesIQTM__Position_Account_Call_Plan__c';
            Database.executeBatch(o3);
        }
        Test.stopTest();
    }

    static testMethod void testMethod2() 
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;
        Account acc= TestDataFactory.createAccount();
        acc.AccountNumber ='BH10643156';
        insert acc;
        
        AxtriaSalesIQTM__Affiliation_Network__c affnet = TestDataFactory.createAffliNet(countr);
        insert affnet;
        AxtriaSalesIQTM__Account_Affiliation__c accaff = TestDataFactory.createAcctAffli(acc,affnet);
        insert accaff;
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        Measure_Master__c mmc1 = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc1.Team_Instance__c = teamins.id;
        insert mmc1;
        Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
        pp.Name ='MARKET_ CRC';
        insert pp;
        Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
        rp.Parameter__c = pp.id;
        insert rp;
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__IsMaster__c = true;
        pos.AxtriaSalesIQTM__Hierarchy_Level__c = '1';
        insert pos;
        
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;

        AxtriaSalesIQTM__Employee__c emp = new AxtriaSalesIQTM__Employee__c();
        emp.AxtriaSalesIQTM__FirstName__c = 'Test';
        emp.AxtriaSalesIQTM__Last_Name__c = 'User';
        emp.AxtriaSalesIQTM__Employee_ID__c = '1001';
        insert emp;

        AxtriaSalesIQTM__Position_Employee__c posEmp = new AxtriaSalesIQTM__Position_Employee__c();
        posEmp.AxtriaSalesIQTM__Position__c = pos.Id;
        posEmp.AxtriaSalesIQTM__Employee__c = emp.Id;
        posEmp.AxtriaSalesIQTM__Effective_Start_Date__c = Date.today();
        posEmp.AxtriaSalesIQTM__Effective_End_Date__c = Date.today().addDays(10);
        insert posEmp;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;

        Source_to_Destination_Mapping__c sdm = new Source_to_Destination_Mapping__c();
        sdm.Load_Type__c = 'TSF';
        sdm.Destination_Object_Field__c = 'Metric10__c';
        sdm.Source_Object_Field__c = 'Metric10__c';
        sdm.Team_Instance__c = teamins.Id;
        insert sdm;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        positionAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c = true;
        insert positionAccountCallPlan;
        List<String> allTeamInstances = new List<String>();
        allTeamInstances.add(teamins.id);
        AxtriaSalesIQTM__TriggerContol__c q = new AxtriaSalesIQTM__TriggerContol__c();
        q.AxtriaSalesIQTM__IsStopTrigger__c=false;
        q.name = 'UpdateVeevaPush';
        insert q;

        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            Batch_Integration_TSF_NEW o3=new Batch_Integration_TSF_NEW(allTeamInstances,false);
            o3.query = 'select id, AxtriaSalesIQTM__Segment10__c, P1__c,AxtriaSalesIQTM__Position__r.Employee1__c, AxtriaSalesIQTM__Position__r.Employee1__r.Name,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__r.Name, Segment__c, AxtriaSalesIQTM__Account__r.AZ_VeevaID__c , AxtriaSalesIQTM__Account__r.AccountNumber ,Adoption__c, Potential__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name, Country__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, Party_ID__r.Accessibility_Range__c, AxtriaSalesIQTM__Team_Instance__c,Accessibility_Range__c  from AxtriaSalesIQTM__Position_Account_Call_Plan__c';
            Database.executeBatch(o3);
        }
        Test.stopTest();
    }
}