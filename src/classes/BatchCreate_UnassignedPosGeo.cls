global class BatchCreate_UnassignedPosGeo implements Database.Batchable<sObject>,Database.stateful,Schedulable {
    
    global String geoDeltaQuery;
    public String batchID;
    global DateTime lastjobDate=null;
    public Integer recordsProcessed=0;
    global List<Zip_Type__mdt> zipMdt = null;
    Set<String> zipTypes = null;
    
    global BatchCreate_UnassignedPosGeo(){
        List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
        System.debug('Before scheduler');
        schLogList = [Select Id,CreatedDate,Created_Date2__c from Scheduler_Log__c where Job_Name__c='Unassigned PosGeo Delta' and Job_Status__c='Successful' Order By Created_Date2__c desc];
        System.debug('After scheduler'  + schLogList);
        if(schLogList.size() > 0)
        {
          lastjobDate=schLogList[0].Created_Date2__c;  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
        
        System.debug('last job'+lastjobDate);
        
        Scheduler_Log__c sJob = new Scheduler_Log__c();
        sJob.Job_Name__c = 'Unassigned PosGeo Delta';
        sJob.Job_Status__c = 'Failed';
        sJob.Job_Type__c='Inbound';
        sJob.Created_Date2__c = DateTime.now();
        insert sJob;
        batchID = sJob.Id;

        zipMdt = [Select Id, Label From Zip_Type__mdt];
        zipTypes = new Set<String>();
        for(Zip_Type__mdt zipType:zipMdt){
            zipTypes.add(zipType.Label);
        }

        geoDeltaQuery = 'SELECT Id, Name, AxtriaARSnT__Country_Code__c FROM AxtriaSalesIQTM__Geography__c where AxtriaSalesIQTM__Zip_Type__c IN: zipTypes';
        if(lastjobDate!=null){
          geoDeltaQuery = geoDeltaQuery + ' and CreatedDate  >=:  lastjobDate '; 
        }
        this.geoDeltaQuery = geoDeltaQuery;

        system.debug('======The query is:::'+geoDeltaQuery);
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(geoDeltaQuery);
    }
    
    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Geography__c> geoDeltaList) {
        
        System.debug('geoDeltaList size is ' + geoDeltaList.size());

        map<string,list<AxtriaSalesIQTM__Geography__c>> country2geomap = new map<string,list<AxtriaSalesIQTM__Geography__c>>();
        map<string,list<AxtriaSalesIQTM__Position_Team_Instance__c>> country2posteaminst = new map<string,list<AxtriaSalesIQTM__Position_Team_Instance__c>>();

        //filling country2goemap county wise from scope
        for(AxtriaSalesIQTM__Geography__c geo: geoDeltaList)
        {
            if(geo.Country_Code__c !=null)
            {
                if(!country2geomap.containsKey(geo.Country_Code__c))
                {

                    country2geomap.put(geo.Country_Code__c,new list<AxtriaSalesIQTM__Geography__c>{geo});
                }
                else
                {
                    country2geomap.get(geo.Country_Code__c).add(geo);   
                }
            }
        }
        system.debug('===============country2accmap.Keyset():::'+country2geomap.keySet());
        system.debug('===============country2accmap:::'+country2geomap);
        set<string>period= new set<string>();
        period.add('Current');
        period.add('Future');


        List<AxtriaSalesIQTM__Position_Team_Instance__c> posTeamInsLst = new List<AxtriaSalesIQTM__Position_Team_Instance__c>();
        posTeamInsLst = [select id,name,AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Position_ID__c,AxtriaSalesIQTM__Team_Instance_ID__c,AxtriaSalesIQTM__Team_Instance_ID__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Position_Team_Instance__c where AxtriaSalesIQTM__Team_Instance_ID__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c IN : country2geomap.keyset() and AxtriaSalesIQTM__Position_ID__r.name='Unassigned' and AxtriaSalesIQTM__Team_Instance_ID__r.AxtriaSalesIQTM__Alignment_Period__c IN : period and AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__Client_Position_Code__c = '00000' ];
        
        System.debug('posTeamInsLst size is ' + posTeamInsLst.size());
        for(AxtriaSalesIQTM__Position_Team_Instance__c post : posTeamInsLst){
            string key=post.AxtriaSalesIQTM__Team_Instance_ID__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
            if(!country2posteaminst.containsKey(key))
            {
                country2posteaminst.put(key,new list<AxtriaSalesIQTM__Position_Team_Instance__c>{post});
            }
            else
            {
                country2posteaminst.get(key).add(post);
            }

        }
        system.debug('===================country2posteaminst::::'+country2posteaminst.size());

        List<AxtriaSalesIQTM__Position_Geography__c> positionGeoListToInsert = new List<AxtriaSalesIQTM__Position_Geography__c>();
        for(String country : country2geomap.keySet()){

            list<AxtriaSalesIQTM__Geography__c>geolist = country2geomap.get(country);
            list<AxtriaSalesIQTM__Position_Team_Instance__c>posteamlst = country2posteaminst.get(country);
            if(geolist!=null && posteamlst!=null){
                for(AxtriaSalesIQTM__Geography__c geography: geolist){
                    for(AxtriaSalesIQTM__Position_Team_Instance__c posTeamIns : posteamlst){
                        AxtriaSalesIQTM__Position_Geography__c positionGeo = new AxtriaSalesIQTM__Position_Geography__c();
                        positionGeo.AxtriaSalesIQTM__Position__c =  posTeamIns.AxtriaSalesIQTM__Position_ID__c;
                        positionGeo.AxtriaSalesIQTM__Position_Team_Instance__c = posTeamIns.Id;
                        positionGeo.AxtriaSalesIQTM__Geography__c = geography.Id;
                        positionGeo.AxtriaSalesIQTM__Proposed_Position__c = posTeamIns.AxtriaSalesIQTM__Position_ID__c;
                        positionGeo.AxtriaSalesIQTM__Position_Id_External__c = posTeamIns.AxtriaSalesIQTM__Position_ID__c;
                        positionGeo.AxtriaSalesIQTM__Effective_End_Date__c = posTeamIns.AxtriaSalesIQTM__Effective_End_Date__c;
                        positionGeo.AxtriaSalesIQTM__Effective_Start_Date__c = posTeamIns.AxtriaSalesIQTM__Effective_Start_Date__c;
                        positionGeo.AxtriaSalesIQTM__Team_Instance__c = posTeamIns.AxtriaSalesIQTM__Team_Instance_ID__c;
                        positionGeo.Name = geography.Name;
                        positionGeoListToInsert.add(positionGeo);
                    }
                }
                System.debug('positionGeoListToInsert size is ' + positionGeoListToInsert.size());
                recordsProcessed += positionGeoListToInsert.size();
                System.debug('recordsProcessed is ' + recordsProcessed);
                if(positionGeoListToInsert.size()>0){
                    insert positionGeoListToInsert;
                }
            }
        }
    }
    
    global void execute(SchedulableContext sc){
        Database.executeBatch(new BatchCreate_UnassignedPosGeo(), 200);
    }
    global void finish(Database.BatchableContext BC) {
        Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID); 
        sJob.No_Of_Records_Processed__c=recordsProcessed;
        sJob.Job_Status__c='Successful';
        update sJob;
    }
    
}