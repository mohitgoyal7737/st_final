global class BatchCalculateDeltaLOA implements Database.Batchable<sObject>, Database.Stateful, schedulable{
    set<string> empIdsForCreatedEvents = new set<string>();
    global set<string> fieldEmpDepartmentSet = new set<string>();
    global map<string,list<string>> mapId2FieldEmpDetails = new map<string,list<string>>();
     
    global BatchCalculateDeltaLOA(){
        
        for(FieldEmployee__c fieldEmp : Database.Query('select id,Department__c,Job_Title__c,Operator__c from fieldemployee__c')){
            list<string> fieldEmpSet = new list<string>();
            fieldEmpSet.add(fieldEmp.Department__c);
            fieldEmpSet.add(fieldEmp.Operator__c); 
            fieldEmpSet.add(fieldEmp.Job_Title__c);
            mapId2FieldEmpDetails.put((string)fieldEmp.id,fieldEmpSet);
            fieldEmpDepartmentSet.add(fieldEmp.Department__c); 
            
        }
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){  
        boolean isRejectedValue = false;
        string query = 'SELECT Id, Name, AssociateOID__c, LOA_StartDate__c, LOA_EndDate__c, Paid_Time_Off_Policy__c, Total_Quantity__c '
                    +' FROM Current_Leave_Data_Feed__c where isRejected__c = :isRejectedValue order by LOA_StartDate__c desc ';
        system.debug('inside getQueryLocator :: query: '+query);
        return Database.getQueryLocator(query);
    }
    
       global void execute(SchedulableContext SC){
        BatchCalculateDeltaLOA execfile = new BatchCalculateDeltaLOA();
        database.executeBatch(execfile,100);
    }
    
    global void execute(Database.BatchableContext BC, list<Current_Leave_Data_Feed__c> currentLOAList){
        
        set<string> currDayEmpIds = new set<String>();
        map<string, list<Current_Leave_Data_Feed__c>> EmpIdToCurrDayListMapping = new map<string, list<Current_Leave_Data_Feed__c>>();
        map<string, list<Previous_Leave_Data_Feed__c>> EmpIdToPrevDayListMapping = new map<string, list<Previous_Leave_Data_Feed__c>>();
        list<CR_Employee_Feed__c> crFeedList = new list<CR_Employee_Feed__c>();
        map<string, AxtriaSalesIQTM__Employee__c> empAssIdToEmpList = new map<string, AxtriaSalesIQTM__Employee__c>();
        
        for(Current_Leave_Data_Feed__c curr : currentLOAList){
            currDayEmpIds.add(curr.AssociateOID__c);
            if(EmpIdToCurrDayListMapping.containsKey(curr.AssociateOID__c)){
                EmpIdToCurrDayListMapping.get(curr.AssociateOID__c).add(curr);
            }else{
                EmpIdToCurrDayListMapping.put(curr.AssociateOID__c, new list<Current_Leave_Data_Feed__c>{curr});
            }
        }
        
        String empQuery = 'select id,name, AxtriaSalesIQTM__HR_Termination_Date__c,AxtriaSalesIQTM__Original_Hire_Date__c,AxtriaSalesIQTM__Employee_ID__c,AxtriaSalesIQTM__HR_Status__c,'
                        +' AxtriaSalesIQTM__Rehire_Date__c, AxtriaSalesIQTM__Current_Territory__c,AssociateOID__c, AxtriaSalesIQTM__FirstName__c,'
                        +' AxtriaSalesIQTM__Last_Name__c,Department__c,JobCodeName__c '
                        +' from AxtriaSalesIQTM__Employee__c  where AxtriaSalesIQTM__Employee_ID__c IN: currDayEmpIds ';
                        
        for(AxtriaSalesIQTM__Employee__c emp : Database.query(empQuery)){
            empAssIdToEmpList.put(emp.AxtriaSalesIQTM__Employee_ID__c, emp);
        }
        
        String query = 'SELECT Id, Name, AssociateOID__c, LOA_StartDate__c, LOA_EndDate__c, Paid_Time_Off_Policy__c, Total_Quantity__c '
                    + ' FROM Previous_Leave_Data_Feed__c WHERE AssociateOID__c IN: currDayEmpIds  order by LOA_StartDate__c desc ';
        
        for(Previous_Leave_Data_Feed__c prev : Database.query(query)){
            if(EmpIdToPrevDayListMapping.containsKey(prev.AssociateOID__c)){
                EmpIdToPrevDayListMapping.get(prev.AssociateOID__c).add(prev);
            }else{
                EmpIdToPrevDayListMapping.put(prev.AssociateOID__c, new list<Previous_Leave_Data_Feed__c>{prev});
            }
        }
        
        system.debug('-------EmpIdToCurrDayListMapping.keySet()---'+EmpIdToCurrDayListMapping.keySet());
        system.debug('-------EmpIdToCurrDayListMapping---'+EmpIdToCurrDayListMapping);
        system.debug('-------empAssIdToEmpList---'+empAssIdToEmpList.keySet());
        
        for(String empId : EmpIdToCurrDayListMapping.keySet()){
            AxtriaSalesIQTM__Employee__c empRec = empAssIdToEmpList.get(empId);
            boolean eventCreatedForEmployee = false;
            system.debug('----empRec----'+empRec.Name);
            if(empRec != null && isValidEmployeeForLOAEventCreation(empRec)){
                if(EmpIdToPrevDayListMapping.containsKey(empId)){
                    system.debug('---isValidEmployeeForLOAEventCreation---'+empRec.Name);
                    for(Current_Leave_Data_Feed__c curr : EmpIdToCurrDayListMapping.get(empId)){
                        system.debug('----inside EmpIdToCurrDayListMapping loop empRec----'+empRec.Name);
                        if((isRehireEmployee(empRec) && curr.LOA_StartDate__c >= empRec.AxtriaSalesIQTM__Rehire_Date__c)
                            || ( isEmployeeNotTerminated(empRec) && curr.LOA_StartDate__c >= empRec.AxtriaSalesIQTM__Original_Hire_Date__c                          
                                && (empRec.AxtriaSalesIQTM__HR_Termination_Date__c == null || curr.LOA_EndDate__c <= empRec.AxtriaSalesIQTM__HR_Termination_Date__c))
                            ){
                                
                            string currKey = curr.AssociateOID__c + '_' + curr.Paid_Time_Off_Policy__c + '_' + curr.LOA_StartDate__c + '_' + curr.LOA_EndDate__c;
                            system.debug('====currKey===='+currKey);
                            
                            boolean createEvent = true;
                            for(Previous_Leave_Data_Feed__c prev : EmpIdToPrevDayListMapping.get(empId)){
                                string prevKey = prev.AssociateOID__c + '_' + prev.Paid_Time_Off_Policy__c + '_' + prev.LOA_StartDate__c + '_' + prev.LOA_EndDate__c;
                                system.debug('---prevKey---'+prevKey);
                                
                                if(currKey == prevKey){ //event already exist
                                    createEvent = false;
                                    break;
                                }   
                            }
                            
                            if(createEvent == true){
                                crFeedList.add(createEventFromCurrData(curr,empRec));
                                eventCreatedForEmployee = true; 
                                break;
                            }
                        }
                    }
                }
                else{   //create events related to the employee
                    system.debug('----else  empRec----'+empRec.Name);
                    for(Current_Leave_Data_Feed__c curr : EmpIdToCurrDayListMapping.get(empId)){
                        system.debug('----else  EmpIdToCurrDayListMapping loop ----'+empRec.Name);
                        if((isRehireEmployee(empRec) && curr.LOA_StartDate__c >= empRec.AxtriaSalesIQTM__Rehire_Date__c)
                            || ( isEmployeeNotTerminated(empRec) && curr.LOA_StartDate__c >= empRec.AxtriaSalesIQTM__Original_Hire_Date__c                          
                                && (empRec.AxtriaSalesIQTM__HR_Termination_Date__c == null || curr.LOA_EndDate__c <= empRec.AxtriaSalesIQTM__HR_Termination_Date__c))
                            ){
                                crFeedList.add(createEventFromCurrData(curr,empRec));                               
                                eventCreatedForEmployee = true;
                                break;
                            }
                    }
                }
            }
            /*if(eventCreatedForEmployee == true){
                break;
            }*/
        }
        
        system.debug('-----crFeedList----'+crFeedList); 
        if(crFeedList.size() > 0){
            insert crFeedList;
        }  
    }
    
    public CR_Employee_Feed__c createEventFromCurrData(Current_Leave_Data_Feed__c curr, AxtriaSalesIQTM__Employee__c empRec){
         CR_Employee_Feed__c crEmp = new CR_Employee_Feed__c();
         crEmp.Request_Date1__c = system.today();
         crEmp.Rep_Name__c = empRec.AxtriaSalesIQTM__FirstName__c + ' ' + empRec.AxtriaSalesIQTM__Last_Name__c  ;
         crEmp.Event_Name__c =   system.label.Leave_of_Absence;   
         crEmp.Status__c = 'Pending Action';
         crEmp.Leave_Start_Date__c   = curr.LOA_StartDate__c; 
         crEmp.Leave_End_Date__c     = curr.LOA_EndDate__c;
         crEmp.Employee__c  =  empRec.Id;
         crEmp.position__c  =  empRec.AxtriaSalesIQTM__Current_Territory__c;
         empIdsForCreatedEvents.add(curr.AssociateOID__c);
        
         return crEmp;
    }
    
    public boolean isValidEmployeeForLOAEventCreation(AxtriaSalesIQTM__Employee__c empRec){
        if(checkFieldEmployee(empRec.Department__c, empRec.JobCodeName__c) && ( isRehireEmployee(empRec) || isEmployeeNotTerminated(empRec))){
            return true;
        }
        system.debug('----!isValidEmployeeForLOAEventCreation---'+empRec.Name);
        return false;
    }
    
    public boolean isEmployeeNotTerminated(AxtriaSalesIQTM__Employee__c empRec){
        if( //(empRec.AxtriaSalesIQTM__HR_Status__c == system.label.Active || empRec.AxtriaSalesIQTM__HR_Status__c == system.label.LOA) &&
             (empRec.AxtriaSalesIQTM__Rehire_Date__c == null ) 
            && ( empRec.AxtriaSalesIQTM__HR_Termination_Date__c == null || empRec.AxtriaSalesIQTM__HR_Termination_Date__c > system.today())
        ){
            return true;
        }       
        
        system.debug('----!isEmployeeNotTerminated---'+empRec.Name);
        return false;
    }
    
    public boolean isRehireEmployee(AxtriaSalesIQTM__Employee__c empRec){
        if((empRec.AxtriaSalesIQTM__HR_Status__c == system.label.Active || empRec.AxtriaSalesIQTM__HR_Status__c == system.label.LOA)
            && empRec.AxtriaSalesIQTM__Rehire_Date__c != null 
            &&  empRec.AxtriaSalesIQTM__HR_Termination_Date__c < empRec.AxtriaSalesIQTM__Rehire_Date__c){
                return true;
        }
        system.debug('----!isRehireEmployee---'+empRec.Name);
        return false;
    }
    
   /* public boolean isFutureDatedTerminatedEmployee(AxtriaSalesIQTM__Employee__c empRec){
        if(empRec.AxtriaSalesIQTM__HR_Status__c == system.label.Active && empRec.AxtriaSalesIQTM__HR_Termination_Date__c > system.today()){ // active
            return true;
        }       
        return false;
    } */
    
    public Boolean checkFieldEmployee(string deptValue, string jobTitleValue){
        
        
        system.debug('====deptValue===='+deptValue);
        system.debug('====jobTitleValue===='+jobTitleValue);
        
        if(fieldEmpDepartmentSet.contains(deptValue)){
            for(string str : mapId2FieldEmpDetails.keySet()){
                if((mapId2FieldEmpDetails.get(str))[1]=='equals'){
                    if((mapId2FieldEmpDetails.get(str))[0]==deptValue && (mapId2FieldEmpDetails.get(str))[2]==jobTitleValue){
                        return true;
                    }
                }
                else if((mapId2FieldEmpDetails.get(str))[1]=='contains'){
                    if((mapId2FieldEmpDetails.get(str))[0]==deptValue && (jobTitleValue.contains(mapId2FieldEmpDetails.get(str)[2]))){
                        return true;
                    }
                }
            }
        }
        else{
            return false;
        }
        return false;
        
    }
    
     public void mailSender(){
        list<CR_Employee_Feed__c> crFeed = new list<CR_Employee_Feed__c>();
        map<string,string> eventNameCSVFieldMap = new map<string,string>();
        eventNameCSVFieldMap.put(system.label.Leave_of_Absence,'Leave_of_Absence_Mail_CSV__c');
        
        crFeed  = [select id,Event_Name__c from CR_Employee_Feed__c where createdDate = TODAY];
        if(crFeed.size()>0){
            map<string,list<CR_Employee_Feed__c>> eventNameCRListMap = new map<string,list<CR_Employee_Feed__c>>(); 
            for(CR_Employee_Feed__c crf : crFeed){
                if(!eventNameCRListMap.containsKey(crf.Event_Name__c)){
                    eventNameCRListMap.put(crf.Event_Name__c,new list<CR_Employee_Feed__c>());
                }
                eventNameCRListMap.get(crf.Event_Name__c).add(crf);
            }
            system.debug('---eventNameCRListMap--'+eventNameCRListMap);
            Contact con = new Contact();
                            con.FirstName = 'Test';
                            con.LastName = 'Contact';
                            con.Email = 'no-reply@axtria.com';
                            insert con;
            RMS_Email_Configurations__c RMC = RMS_Email_Configurations__c.getOrgDefaults();
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>(); // add rms email config?
            for(string key : eventNameCRListMap.keySet()){
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                if(eventNameCSVFieldMap.containsKey(key) && RMC != null){
                    string sendToCSV = string.valueOf(RMC.get(eventNameCSVFieldMap.get(key)));
                    list<string> sendTo;
                    if(sendTOCSV != null){
                        sendTo = sendToCSV.split(',');    
                    }
                    mail.setToAddresses(sendTo);
                    mail.setSubject('Workbench updated'); 
                    mail.setTargetObjectID(con.ID);
                    mail.setSenderDisplayName('Insmed SalesIQ');
                    //mail.setBccAddresses(new String[] {'Perumalla.Venkata@axtria.com','Ruchi.Jain@axtria.com'});
                    mail.setUseSignature(false);
                    mail.setSaveAsActivity(false); 
                    String body= '<html><body>'+
                                 '<p>This is an automated email.Please do not reply to this email.'+
                                 '<br/><br/>Date: '+Date.Today().format()+'<br/><br/>'+
                                 eventNameCRListMap.get(key).size()+' new '+ key + ' events have been created. Kindly login to view the events.'+
                                 '</p><br/>Thank you,<br/>Axtria SalesIQ Support</body></html>';
                    mail.setHtmlBody(body);
                    if(sendTo != null){  
                        mails.add(mail);
                    }
                }
            }
            try{
                if(!mails.isEmpty()){
                    Messaging.sendEmail(mails);
                }
            }
            catch(Exception e){}
            Delete con;
        }
    }
    
    global void finish(Database.BatchableContext BC){       
        system.debug('--finished---');
        system.debug('---empIdsForCreatedEvents--'+empIdsForCreatedEvents);
        Database.executeBatch(new BatchUpdateEmployeeStatusLOA(empIdsForCreatedEvents), 100); 
        mailSender();
    }
}