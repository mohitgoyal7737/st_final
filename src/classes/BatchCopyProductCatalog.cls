/**********************************************************************************************
@author       : SnT Team
@modifiedBy   : Himanshu Tariyal (A0994)
@modifiedDate : 2nd June'2020
@description  : Batch class for copying Product Catalog data from Source to Destination Team Instances
@Revison(s)   : v1.0
**********************************************************************************************/
global with sharing class BatchCopyProductCatalog implements Database.Batchable<sObject>,Database.Stateful
{
    public String query;
    public String batchName;
    public String sourceTeamInstance;
    public String destinationTeamInstance;
    public String cols;
    public String callPlanLoggerID;
    public String alignNmsp;

    public Boolean proceedNextBatch = true;

    global Map<String,String> mapProdCatSourceToProdCode;
    global Map<String,String> mapProdCodeToDestProdCatSFID;
    global Map<String,String> mapSourceToDestProdCatalog;

    List<AxtriaSalesIQTM__Team_Instance__c> teamInstList;
    Set<String> existingProdCatalogSet;

    global BatchCopyProductCatalog(String sourceTeamInst,String destTeamInst,String colsString,String loggerId) 
    {
        batchName = 'BatchCopyProductCatalog';

        SnTDMLSecurityUtil.printDebugMessage(batchName+' : constructor invoked-->');

        sourceTeamInstance = sourceTeamInst;
        destinationTeamInstance = destTeamInst;
        callPlanLoggerID = loggerId;

        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ScenarioAlignmentCtlr'];
        alignNmsp = cs.NamespacePrefix!=null && cs.NamespacePrefix!='' ? cs.NamespacePrefix+'__' : '';
        
        SnTDMLSecurityUtil.printDebugMessage('alignNmsp-->'+alignNmsp);
        SnTDMLSecurityUtil.printDebugMessage('sourceTeamInstance-->'+sourceTeamInstance);
        SnTDMLSecurityUtil.printDebugMessage('destinationTeamInstance-->'+destinationTeamInstance);
        SnTDMLSecurityUtil.printDebugMessage('callPlanLoggerID-->'+callPlanLoggerID);
        
        if(colsString!=null && colsString!=''){
            cols = colsString;       
        }
        else
        {
            cols = 'Id,Name,Product_Code__c,Product_Type__c,Detail_Group__c, Country_Lookup__c,'+
                    'Full_load__c,Veeva_External_Id__c,Team_Instance__c';
        }
        SnTDMLSecurityUtil.printDebugMessage('cols-->'+cols);

        existingProdCatalogSet = new Set<String>();
        mapProdCatSourceToProdCode = new Map<String,String>();

        teamInstList = [select Id,Name,AxtriaSalesIQTM__Country__c from AxtriaSalesIQTM__Team_Instance__c 
                            where id =: destinationTeamInstance WITH SECURITY_ENFORCED];
        SnTDMLSecurityUtil.printDebugMessage('teamInstList-->'+teamInstList);
        SnTDMLSecurityUtil.printDebugMessage('teamInstList size-->'+teamInstList.size());  

        query = 'SELECT '+cols+' FROM Product_Catalog__c where Team_Instance__c =:sourceTeamInstance '+
                'and IsActive__c = true WITH SECURITY_ENFORCED';
    }

    global BatchCopyProductCatalog(String sourceTeamInst,String destTeamInst,String colsString) 
    {
        batchName = 'BatchCopyProductCatalog';

        SnTDMLSecurityUtil.printDebugMessage(batchName+' : constructor invoked-->');

        sourceTeamInstance = sourceTeamInst;
        destinationTeamInstance = destTeamInst;
        
        SnTDMLSecurityUtil.printDebugMessage('sourceTeamInstance-->'+sourceTeamInstance);
        SnTDMLSecurityUtil.printDebugMessage('destinationTeamInstance-->'+destinationTeamInstance);
        
        if(colsString!=null && colsString!=''){
            cols = colsString;       
        }
        else
        {
            cols = 'Id,Name,Product_Code__c,Product_Type__c,Detail_Group__c, Country_Lookup__c,'+
                    'Full_load__c,Veeva_External_Id__c,Team_Instance__c';
        }
        SnTDMLSecurityUtil.printDebugMessage('cols-->'+cols);

        existingProdCatalogSet = new Set<String>();
        mapProdCatSourceToProdCode = new Map<String,String>();

        teamInstList = [select Id,Name,AxtriaSalesIQTM__Country__c from AxtriaSalesIQTM__Team_Instance__c 
                            where id =: destinationTeamInstance WITH SECURITY_ENFORCED];
        SnTDMLSecurityUtil.printDebugMessage('teamInstList-->'+teamInstList);
        SnTDMLSecurityUtil.printDebugMessage('teamInstList size-->'+teamInstList.size());  

        query = 'SELECT '+cols+' FROM Product_Catalog__c where Team_Instance__c =:sourceTeamInstance '+
                'and IsActive__c = true WITH SECURITY_ENFORCED';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : start() invoked--');
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : query--'+query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Product_Catalog__c> scope) 
    {
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : execute() invoked--');

        String existingProdCatalogQuery;
        Product_Catalog__c newProdCatalogRec;
        String mapKey;

        try
        {
            //Get existing Product Catalog recs
            existingProdCatalogQuery = 'select Product_Code__c from Product_Catalog__c where '+
                                        'Team_Instance__c =:destinationTeamInstance '+
                                        'and IsActive__c = true WITH SECURITY_ENFORCED'; 
            List<Product_Catalog__c> productCatalogList = Database.query(existingProdCatalogQuery);
            SnTDMLSecurityUtil.printDebugMessage('productCatalogList.size()--'+productCatalogList.size());

            if(!productCatalogList.isEmpty())
            {
                for(Product_Catalog__c pc : productCatalogList)
                {
                    if(pc.Product_Code__c!=null)
                        existingProdCatalogSet.add(pc.Product_Code__c); 
                }
            }
            SnTDMLSecurityUtil.printDebugMessage('existingProdCatalogSet--'+existingProdCatalogSet);
            SnTDMLSecurityUtil.printDebugMessage('existingProdCatalogSet size--'+existingProdCatalogSet.size());

            List<Product_Catalog__c> insertProductCatalogList = new List<Product_Catalog__c>();

            //Create new Recs
            for(Product_Catalog__c prodCatRec : scope)
            { 
                mapKey = prodCatRec.Product_Code__c;
                mapProdCatSourceToProdCode.put(prodCatRec.Id,mapKey);

                if(!existingProdCatalogSet.contains(mapKey))
                {                 
                    newProdCatalogRec = prodCatRec.clone(false);
                    newProdCatalogRec.Full_load__c = false;
                    newProdCatalogRec.Team_Instance__c = teamInstList[0].Id;
                    newProdCatalogRec.Country_Lookup__c = teamInstList[0].AxtriaSalesIQTM__Country__c;
                    insertProductCatalogList.add(newProdCatalogRec);
                } 
            }
            SnTDMLSecurityUtil.printDebugMessage('insertProductCatalogList--'+insertProductCatalogList);
            SnTDMLSecurityUtil.printDebugMessage('insertProductCatalogList size--'+insertProductCatalogList.size());

            if(!insertProductCatalogList.isEmpty()){
                SnTDMLSecurityUtil.insertRecords(insertProductCatalogList, batchName);
            }
        }
        catch(Exception e)
        {
            SnTDMLSecurityUtil.printDebugMessage('Error in '+batchName+' : execute()-->'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack Trace-->'+e.getStackTraceString());
            proceedNextBatch = false;

            SnTDMLSecurityUtil.printDebugMessage('callPlanLoggerID--'+callPlanLoggerID);

            if(callPlanLoggerID!=null && callPlanLoggerID!='')
            {
                SalesIQUtility.updateCallPlanLoggerRecord(callPlanLoggerID,'Failed at copying Product Catalog data','Error',alignNmsp);
            }
        }
    }

    global void finish(Database.BatchableContext BC) 
    {
        try
        {
            SnTDMLSecurityUtil.printDebugMessage(batchName+' : finish() invoked--');
            SnTDMLSecurityUtil.printDebugMessage('mapProdCatSourceToProdCode--'+mapProdCatSourceToProdCode);

            if(proceedNextBatch)
            {
                mapProdCodeToDestProdCatSFID = new Map<String,String>();
                mapSourceToDestProdCatalog = new Map<String,String>();

                //Get Destination Prod Catalogs
                String destProdCatalogQuery = 'select Id,Product_Code__c from Product_Catalog__c where '+
                                                'Team_Instance__c = :destinationTeamInstance and '+
                                                'Product_Code__c!=null WITH SECURITY_ENFORCED';
                List<sObject> destProdCatalogList = Database.query(destProdCatalogQuery);

                if(destProdCatalogList!=null && destProdCatalogList.size()>0)
                {
                    for(sObject destRec : destProdCatalogList){
                        mapProdCodeToDestProdCatSFID.put((String)destRec.get('Product_Code__c'),(String)destRec.get('Id')); 
                    }

                    String prodCode;
                    for(String sourceSFID : mapProdCatSourceToProdCode.keySet())
                    {
                        prodCode = mapProdCatSourceToProdCode.get(sourceSFID);
                        if(mapProdCodeToDestProdCatSFID.containsKey(prodCode)){
                            mapSourceToDestProdCatalog.put(sourceSFID,mapProdCodeToDestProdCatSFID.get(prodCode));  
                        }
                    }
                    SnTDMLSecurityUtil.printDebugMessage('mapSourceToDestProdCatalog--'+mapSourceToDestProdCatalog);
                }

                CopyCallPlanScenarioTriggerHandler copy = new CopyCallPlanScenarioTriggerHandler(sourceTeamInstance,destinationTeamInstance,callPlanLoggerID);
                copy.copySourceToDestMapping(mapSourceToDestProdCatalog);

                //Comment added by HT(A0994) on 12th June 2020
                /*if(mapSourceToDestProdCatalog!=null && mapSourceToDestProdCatalog.keySet().size()>0)
                {
                    CopyCallPlanScenarioTriggerHandler copy = new CopyCallPlanScenarioTriggerHandler(sourceTeamInstance,destinationTeamInstance,callPlanLoggerID);
                    copy.copySourceToDestMapping(mapSourceToDestProdCatalog);
                }*/
            }
        }
        catch(Exception e)
        {
            SnTDMLSecurityUtil.printDebugMessage('Error in '+batchName+' : finish()-->'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack Trace-->'+e.getStackTraceString());
            SnTDMLSecurityUtil.printDebugMessage('callPlanLoggerID--'+callPlanLoggerID);
            if(callPlanLoggerID!=null && callPlanLoggerID!='')
            {
                SalesIQUtility.updateCallPlanLoggerRecord(callPlanLoggerID,'Failed at copying Product Catalog data','Error',alignNmsp);
            }
        }
    }
}