@isTest()
public class RedirectchangeTest{
        
         
        static testMethod void testAutoRun1(){
        Profile p = [SELECT Id FROM Profile WHERE Name='SalesIQ Home Office' or name = 'System Administrator' order by name limit 1]; 
        
          User u = new User(Alias = 'staaandt', Email='standarduser@testorg.com', 
          EmailEncodingKey='UTF-8', LastName='Testiaaang', LanguageLocaleKey='en_US', 
          LocaleSidKey='en_US', ProfileId = p.Id, 
          TimeZoneSidKey='America/Los_Angeles', UserName='standaaaarduser@testorg.com');
          insert u;
          System.runAs(u){
        RedirectChange cr=new RedirectChange();
        cr.redirectToChange();
        }
      }
        static testMethod void testAutoRun2(){
        Profile p1 = [SELECT Id FROM Profile WHERE Name='SalesIQ RD' or name = 'System Administrator' order by name limit 1]; 
        
          User u1 = new User(Alias = 'staandt', Email='standarduser@testorg1.com.uat', 
          EmailEncodingKey='UTF-8', LastName='Testiaaanguat', LanguageLocaleKey='en_US', 
          LocaleSidKey='en_US', ProfileId = p1.Id, 
          TimeZoneSidKey='America/Los_Angeles', UserName='insmeduser@testorg.com.uat');
          insert u1;
          System.runAs(u1){
        RedirectChange cr=new RedirectChange();
        cr.redirectToChange();
        }
      }
      static testMethod void testAutoRun3(){
        Profile p2 = [SELECT Id FROM Profile WHERE Name='SalesIQ VP' or name = 'System Administrator' order by name limit 1]; 
        
          User u2 = new User(Alias = 'staandt', Email='insmedr@testorg1.com.uat', 
          EmailEncodingKey='UTF-8', LastName='insmeduat', LanguageLocaleKey='en_US', 
          LocaleSidKey='en_US', ProfileId = p2.Id, 
          TimeZoneSidKey='America/Los_Angeles', UserName='stanuser@testorg.com.uat');
          insert u2;
          System.runAs(u2){
        RedirectChange cr=new RedirectChange();
        cr.redirectToChange();
        }
      }
     static testmethod void redirectchangetest(){
        testAutoRun1();
        testAutoRun2();
        testAutoRun3();       
        }
}