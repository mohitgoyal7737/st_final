@isTest
	public class BatchPopulateEmployeeTest {
		static testMethod void testMethod1() {
			User loggedInUser = new User(id=UserInfo.getUserId());
			
			AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
			insert orgmas;
			
			AxtriaSalesIQTM__Country__c country = testDataFactory.createCountry(orgmas);
			insert country;
			
			AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(country);
			insert team;
			
			AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
			teamins.Name = 'All';
			insert teamins;
			
			AxtriaSalesIQTM__Position__c pos = TestDataFactory.createPosition(team,teamins);
			insert pos;
			
			AxtriaSalesIQTM__Change_Request__c changerequest = TestDataFactory.createChangeRequest(UserInfo.getUserId(),pos.id,pos.id,teamins.id,'test');
			insert changerequest;
			
			AxtriaSalesIQTM__CIM_Config__c cimcc = TestDataFactory.createCIMConfig(teamins);
			insert cimcc;
			
			temp_Obj__c newtempobj = TestDataFactory.tempobj();
			newtempobj.Display_Column_Order__c = null ;
			insert newtempobj;
			
			List<temp_Obj__c > tempobjlist = new List<temp_Obj__c >();
			tempobjlist.add(newtempobj);
			
			Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, country);
			 pcc.Product_Code__c='GIST';
			 pcc.Veeva_External_ID__c = 'GIST';
			 insert pcc;
			  
			 Source_to_Destination_Mapping__c mapping = TestDataFactory.createACF_PA_mapping(teamins,pcc);
			 mapping.Source_Object_Field__c='Metric1__c';
			 mapping.Load_Type__c = 'Employee';
			 insert mapping;
			
			Test.startTest();
			System.runAs(loggedInUser){
				ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'BatchPopulateEmployeeTest'];
				String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
				List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
				System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
				BatchPopulateEmployee batchpopulateemp = new BatchPopulateEmployee();
				Database.executeBatch(batchpopulateemp);
			}
			Test.stopTest();
		}
		static testMethod void testMethod2() {
			User loggedInUser = new User(id=UserInfo.getUserId());
			
			AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
			insert orgmas;
			
			AxtriaSalesIQTM__Country__c country = testDataFactory.createCountry(orgmas);
			insert country;
			
			AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(country);
			insert team;
			
			AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
			teamins.Name = 'All';
			insert teamins;
			
			AxtriaSalesIQTM__Position__c pos = TestDataFactory.createPosition(team,teamins);
			insert pos;
			
			AxtriaSalesIQTM__Change_Request__c changerequest = TestDataFactory.createChangeRequest(UserInfo.getUserId(),pos.id,pos.id,teamins.id,'test');
			insert changerequest;
			
			AxtriaSalesIQTM__CIM_Config__c cimcc = TestDataFactory.createCIMConfig(teamins);
			insert cimcc;
			
			temp_Obj__c newtempobj = TestDataFactory.tempobj();
			newtempobj.Display_Column_Order__c = null ;
			insert newtempobj;
			
			  
			List<temp_Obj__c > tempobjlist = new List<temp_Obj__c >();
			tempobjlist.add(newtempobj);
			
			AxtriaSalesIQTM__Change_Request__c a = new AxtriaSalesIQTM__Change_Request__c();
			a.AxtriaSalesIQTM__Status__c ='Approved' ;
			a.AxtriaSalesIQTM__Destination_Position__c = pos.id;
			a.AxtriaSalesIQTM__Team_Instance_ID__c= teamins.id;
			a.AxtriaSalesIQTM__Request_Type_Change__c='Call Plan';
			insert a;
			
			Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, country);
			 pcc.Product_Code__c='GIST';
			 pcc.Veeva_External_ID__c = 'GIST';
			 insert pcc;
			  
			 Source_to_Destination_Mapping__c mapping = TestDataFactory.createACF_PA_mapping(teamins,pcc);
			 mapping.Source_Object_Field__c='Metric1__c';
			 mapping.Load_Type__c = 'Employee';
			 insert mapping;
			
			Test.startTest();
			System.runAs(loggedInUser){
			 String schTime = '0 0 12 * * ?';
				ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
				String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
				List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
				System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
			   // List<String> Ids = new List<String>();
			   
				String Ids;
				AxtriaSalesIQTM__Change_Request__c CR = [Select id from AxtriaSalesIQTM__Change_Request__c limit 1];
				Ids = CR.Id;
				//Ids.add(CR.Id);
				
				Boolean crCheckFlag;
				BatchPopulateEmployee batchpopulateemp = new BatchPopulateEmployee(Ids,crCheckFlag);
				Database.executeBatch(batchpopulateemp);
				
				
				system.schedule('TestUpdateConAccJob', schTime, batchpopulateemp);
			   
				//Database.QueryLocator ql = batchpopulateemp.start(null);
				//batchpopulateemp.execute(null,tempobjlist);
				//batchpopulateemp.Finish(null);
			}
			Test.stopTest();
		}
		
		static testMethod void testMethod3() {
			String className = 'BatchPopulateEmployeeTest';
		
			User loggedInUser = new User(id=UserInfo.getUserId());
			
			AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
			insert orgmas;
			
			AxtriaSalesIQTM__Country__c country = testDataFactory.createCountry(orgmas);
			insert country;
			
			AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(country);
			insert team;
			
			AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
			teamins.Name = 'All';
			insert teamins;
			
			AxtriaSalesIQTM__Position__c pos = TestDataFactory.createPosition(team,teamins);
			insert pos;
			
			AxtriaSalesIQTM__Change_Request__c changerequest = TestDataFactory.createChangeRequest(UserInfo.getUserId(),pos.id,pos.id,teamins.id,'test');
			insert changerequest;
			
			AxtriaSalesIQTM__CIM_Config__c cimcc = TestDataFactory.createCIMConfig(teamins);
			insert cimcc;
			
			AxtriaSalesIQTM__Employee__c employee = new AxtriaSalesIQTM__Employee__c();
			  employee.Name = 'firstName' + ' ' + 'lastName';
			  employee.AxtriaSalesIQTM__Employee_ID__c = 'ABC01';
			  employee.AxtriaSalesIQTM__Last_Name__c = 'lastName';
			  employee.AxtriaSalesIQTM__FirstName__c = 'firstName';
			  employee.AxtriaSalesIQTM__Gender__c = 'M';
			  insert employee;
			  
			  List<AxtriaSalesIQTM__Employee__c> empPridList =new List<AxtriaSalesIQTM__Employee__c >();
			  empPridList.add(employee);
			  
		   /* temp_Obj__c newtempobj = TestDataFactory.tempobj();
			newtempobj.Display_Column_Order__c = null ;
			newtempobj.Country__c = 'USA';
			newtempobj.Country_Code__c='USA01';
			newtempobj.Marketing_Code__c='001';
			newtempobj.Employee_PRID__c='ABC01';
			newtempobj.First_Name__c='Jon';
			newtempobj.Last_Name__c='Hokins';
			insert newtempobj; */
			
			temp_Obj__c rec = new temp_Obj__c();
			rec.Status__c = 'New';
			rec.Product_Name__c = 'PROD1';
			rec.Position_Code__c = 'TERR1';
			rec.AccountNumber__c = 'ACC1';
			rec.Short_Question_Text1__c = 'Question1';
			rec.Metric10__c = 80;
			rec.Team_Instance_Text__c = teamins.Name;
			rec.Object__c = 'Employee';
			rec.Country__c = 'USA';
			rec.Country_Code__c='USA01';
			rec.Marketing_Code__c='001';
			rec.Employee_PRID__c='ABC01';
			rec.First_Name__c='Jon';
			rec.Last_Name__c='Hokins';
			rec.JobCodeName__c ='Rep';
			rec.AddressCity__c =('NY');
			rec.NickName__c ='Jo';
			rec.ReportingToWorkerName__c ='Kol';
			rec.ReportsToAssociateOID__c ='1a2';
			rec.WorkPhone__c ='123431234';
			rec.Worker_Category__c ='Employee';
			rec.Group_Name__c = 'MR';
			rec.PersonalCell__c = '1323245678';
			rec.company__c ='XYZ';
			rec.Work_Fax__c = 'AXE123';
			rec.Storage_Address__c = 'CA';
			rec.AddressLine1__c = 'Street 4';
			rec.AddressPostalCode__c = '20123';
			rec.Storage_Address__c = 'NY USA';
			rec.AddressStateCode__c = '20001';
			
			SnTDMLSecurityUtil.insertRecords(rec,className);

			temp_Obj__c rec2 = new temp_Obj__c();
			rec2.Status__c = 'New';
			rec2.Product_Name__c = 'PROD2';
			rec2.Position_Code__c = 'TERR2';
			rec2.AccountNumber__c = 'ACC2';
			rec2.Metric10__c = 80;
			rec2.Short_Question_Text1__c = 'Question2';
			rec2.Team_Instance_Text__c = teamins.Name;
			rec2.Object__c = 'Employee';
			rec2.Country__c = 'USA';
			rec2.Country_Code__c='USA01';
			rec2.Marketing_Code__c='001';
			rec2.Employee_PRID__c='ABC';
			rec2.First_Name__c='Jonh';
			rec2.Last_Name__c='Hosy';
			  
			SnTDMLSecurityUtil.insertRecords(rec2,className);
			
			List<temp_Obj__c > tempobjlist = new List<temp_Obj__c >();
			//tempobjlist.add(newtempobj);
			
			tempobjlist.add(rec);
			tempobjlist.add(rec2);
			
			
			  AxtriaSalesIQTM__Employee__c employee1 = new AxtriaSalesIQTM__Employee__c();
			  employee1.Name = 'firstName' + ' ' + 'lastName';
			  employee1.AxtriaSalesIQTM__Employee_ID__c = 'ABC01';
			  employee1.AxtriaSalesIQTM__Last_Name__c = 'lastName';
			  employee1.AxtriaSalesIQTM__FirstName__c = 'firstName2';
			  employee1.AxtriaSalesIQTM__Gender__c = 'M';
			  insert employee1;
			  
			 Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, country);
			 pcc.Product_Code__c='GIST';
			 pcc.Veeva_External_ID__c = 'GIST';
			 insert pcc;
			  
			 Source_to_Destination_Mapping__c mapping = TestDataFactory.createACF_PA_mapping(teamins,pcc);
			 mapping.Source_Object_Field__c='Metric1__c';
			 mapping.Load_Type__c = 'Employee';
			 insert mapping;
			  
			
			Test.startTest();
			System.runAs(loggedInUser){
			
			String schTime = '0 0 12 * * ?';
			
				ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'BatchPopulateEmployeeTest'];
				String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
				List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
				System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
				String Ids;
				//List<String> Ids = new List<String>();
				AxtriaSalesIQTM__Change_Request__c CR = [Select id from AxtriaSalesIQTM__Change_Request__c limit 1];
				//Ids.add(CR.Id);
				Ids = CR.Id;
			   // Boolean crCheckFlag;
				BatchPopulateEmployee batchpopulateemp = new BatchPopulateEmployee(Ids);
				Database.executeBatch(batchpopulateemp);
				
				 
				
				system.schedule('TestUpdateConAccJob', schTime, batchpopulateemp);
			   
			   // Database.QueryLocator ql = batchpopulateemp.start(null);
				//batchpopulateemp.execute(null,tempobjlist);
			   // batchpopulateemp.Finish(null);
				
			}
			Test.stopTest();
			
			
		}
	}