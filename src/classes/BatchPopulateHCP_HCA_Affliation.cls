global class BatchPopulateHCP_HCA_Affliation implements Database.Batchable<sObject> {
    public String query;

    global BatchPopulateHCP_HCA_Affliation() {
        this.query = query;
        query='Select id,Account__c,Type__c from Temp_Acc_Affliation__c where type__c=\'HCA\'';
        system.debug('==============Query::::'+query);
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Temp_Acc_Affliation__c> scope) {
        set<string>Hcaset =new set<string>();
        set<string>Hcpset = new set<string>();
        list<Temp_Acc_Affliation__c>tempacflist = new list<Temp_Acc_Affliation__c>();

        for(Temp_Acc_Affliation__c temp: scope){
            Hcaset.add(temp.Account__c);   
        }
        system.debug('=============HCASET.size():'+Hcaset.size());
        string query2='Select Id,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Parent_Account__c from AxtriaSalesIQTM__Account_Affiliation__c  where AxtriaSalesIQTM__Parent_Account__c IN:Hcaset ';
        system.debug('============Query22222:'+query2);

        list<AxtriaSalesIQTM__Account_Affiliation__c>affiliationlist = Database.query(query2);
        for(AxtriaSalesIQTM__Account_Affiliation__c  SAA: affiliationlist) {
            if(SAA.AxtriaSalesIQTM__Account__c!=null && SAA.AxtriaSalesIQTM__Parent_Account__c!=null){
                Hcpset.add(SAA.AxtriaSalesIQTM__Account__c);
            }
        }
        system.debug('==========Hcpset.size()::==='+Hcpset.size());
        for(string hcp: Hcpset){
            Temp_Acc_Affliation__c taf = new Temp_Acc_Affliation__c();
            taf.Account__c = hcp;
            taf.Type__c = 'HCP';
            tempacflist.add(taf);
        }

        system.debug('===========Size of tempacflist::::'+tempacflist.size());
        Schema.SObjectField unq = Temp_Acc_Affliation__c.Fields.Account__c;
        database.upsert(tempacflist,unq, false);
    }

    global void finish(Database.BatchableContext BC) {
        system.debug('==========system.debug=============finish method about to call Batchupdate_HCP ');
        Database.executeBatch(new Batchupdate_HCP(), 2000);

    }
}