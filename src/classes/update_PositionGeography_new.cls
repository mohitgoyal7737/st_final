global with sharing class update_PositionGeography_new implements Database.Batchable<sObject>, Database.Stateful
{
    /*Replaced temp_Zip_Terr__c with temp_Obj__c due to object purge*/

    global string query;
    global string teamID;
    String Ids;
    Boolean flag = true;
    public integer recordsProcessed;
    public Integer recordsCreated;
    global string teamInstance;
    public list<AxtriaSalesIQTM__Team_Instance__c> teminslst = new list<AxtriaSalesIQTM__Team_Instance__c>();
    public list<AxtriaSalesIQTM__Position_Team_Instance__c> posTI = new list<AxtriaSalesIQTM__Position_Team_Instance__c>();

    global list<temp_Obj__c> zipTerrlist = new list<temp_Obj__c>();
    public List<AxtriaSalesIQTM__Change_Request__c> cr = new List<AxtriaSalesIQTM__Change_Request__c>();

    //Added by HT(A0994) on 17th June 2020
    global String changeReqID;
    global Boolean flagValue;

    global update_PositionGeography_new(String Team, String TeamIns)
    {
         }

    global update_PositionGeography_new(String Team, String TeamIns, String Ids)
    {
          
    }

    //Added by HT(A0994) on 17th June 2020
    global update_PositionGeography_new(String Team, String TeamIns, String Ids,Boolean flag)
    {       
    }

    global Database.Querylocator start(Database.BatchableContext bc)
    {
        AxtriaSalesIQTM__TriggerContol__c obj = [select id, AxtriaSalesIQTM__IsStopTrigger__c from AxtriaSalesIQTM__TriggerContol__c where name = 'PositionGeographyTrigger'];
        obj.AxtriaSalesIQTM__IsStopTrigger__c = true;
        update obj;
        //SnTDMLSecurityUtil.updateRecords(obj, 'update_PositionGeography');
        return Database.getQueryLocator(query);
    }

    global void execute (Database.BatchableContext BC, List<temp_Obj__c>zipTerrlist)
    {
      
    }

    global void finish(Database.BatchableContext BC)
    {
       
    }
}