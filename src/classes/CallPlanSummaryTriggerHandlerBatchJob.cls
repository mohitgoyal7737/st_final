public with sharing class CallPlanSummaryTriggerHandlerBatchJob {
	
    public static void CallPlanSummaryTriggerHandlerBatchJob(list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> triggerNew, list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> triggerOld, map<Id,AxtriaSalesIQTM__Position_Account_Call_Plan__c> triggerOldMap, String type) {
        
        set<String> positionIds    = new set<String>();
        list<String> clonedPosition = new list<String>();
        list<String> pacps = new list<String>();
        list<String> clonedPacps = new list<String>();
        set<String> clonedlines = new set<String>();
        set<String> lines = new set<String>();
        set<String> teaminstance = new set<String>();
        
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp: triggerNew){
            positionIds.add(pacp.AxtriaSalesIQTM__Position__c);
            pacps.add(pacp.Id);
            //lines.add(pacp.Line__c);//Removed occurance of Line__c due to object purge activity A1422
            teaminstance.add(pacp.AxtriaSalesIQTM__Team_Instance__c);
        }


        if(!Test.isRunningTest())
        {

        	if(type == 'Terr')
        	{
	            CallPlanSummaryTriggerHandler.createCounters(pacps, positionIds);
	            CallPlanSummaryTriggerHandler.createCountersWOSpec(pacps, positionIds);

        	}
        	else if(type == 'Parent')
        	{
        	   	CallPlanSummaryTriggerHandler.createParentCounters(pacps, positionIds);
    			CallPlanSummaryTriggerHandler.createParentCountersWOSpec(pacps, positionIds);
        	}
        	else if(type == 'Parent Parent')
        	{
        		CallPlanSummaryTriggerHandler.createParentParentCounters(pacps, positionIds);
        		CallPlanSummaryTriggerHandler.createParentParentCountersWOSpec(pacps, positionIds);
        	}
            

            //Counter without speciality
            
            

        }
    }
}