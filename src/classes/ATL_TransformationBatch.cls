global class ATL_TransformationBatch implements Database.Batchable<sObject> {
    
    public String query;
    List<String> allTeamInstances;
    public Map<String, Set<String>> accsToTerrMap;
    public List<String> allAccs;
    public Map<String,String> accToVeevaID;
    public Map<String, String> accToCountryID;
    public Boolean chain = false;
    public List<String> allCountriesList;
    public boolean errorBatches=false;
    Set<String> activityLogIDSet;


    global ATL_TransformationBatch(List<String> allCountries) {

        allTeamInstances = new LIst<String>();
        allCountriesList = new LIst<String>(allCountries);  
        List<AxtriaSalesIQTM__Team_Instance__c> allTeams = [select id from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c in :allCountries and AxtriaSalesIQTM__Alignment_Period__c = 'Current' and (AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Live' OR AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Published') ];

        for(AxtriaSalesIQTM__Team_Instance__c ti : allTeams)
        {   
            allTeamInstances.add(ti.ID);
        }

        system.debug('+++++++++++ All TI '+ allTeamInstances);
        
        this.query = 'select AxtriaSalesIQTM__Account__c, AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__Country__c, AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Account__r.AZ_VeevaID__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Position__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances and AxtriaSalesIQTM__Assignment_Status__c != \'Inactive\'  and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c != \'00000\' order by AxtriaSalesIQTM__Account__c ';

    }

     global ATL_TransformationBatch(List<String> allCountries, Boolean chaining) {

        chain = chaining;
        allTeamInstances = new LIst<String>();
        allCountriesList = new LIst<String>(allCountries);
        List<AxtriaSalesIQTM__Team_Instance__c> allTeams = [select id from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c in :allCountries and AxtriaSalesIQTM__Alignment_Period__c = 'Current' and (AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Live' OR AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Published') ];

        for(AxtriaSalesIQTM__Team_Instance__c ti : allTeams)
        {   
            allTeamInstances.add(ti.ID);
        }
        
        system.debug('+++++++++++ All TI '+ allTeamInstances);

        this.query = 'select AxtriaSalesIQTM__Account__c, AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__Country__c, AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Account__r.AZ_VeevaID__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Position__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances and AxtriaSalesIQTM__Assignment_Status__c != \'Inactive\'  and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c != \'00000\' order by AxtriaSalesIQTM__Account__c ';

        
    }

    global ATL_TransformationBatch(List<String> allCountries, Boolean chaining, Set<String> activityLogSet) {

        chain = chaining;
        allTeamInstances = new LIst<String>();
        allCountriesList = new LIst<String>(allCountries);
        activityLogIDSet = new Set<String>();
        activityLogIDSet.addAll(activityLogSet);
        List<AxtriaSalesIQTM__Team_Instance__c> allTeams = [select id from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c in :allCountries and AxtriaSalesIQTM__Alignment_Period__c = 'Current' and (AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Live' OR AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Published') ];

        for(AxtriaSalesIQTM__Team_Instance__c ti : allTeams)
        {   
            allTeamInstances.add(ti.ID);
        }
        
        system.debug('+++++++++++ All TI '+ allTeamInstances);

        this.query = 'select AxtriaSalesIQTM__Account__c, AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__Country__c, AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Account__r.AZ_VeevaID__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Position__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances and AxtriaSalesIQTM__Assignment_Status__c != \'Inactive\'  and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c != \'00000\' order by AxtriaSalesIQTM__Account__c ';

        
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Account__c> scope) 
    {    
       /*try
       {*/
          accsToTerrMap= new Map<String, Set<String>>();
          accToVeevaID = new Map<String, String>();
          accToCountryID = new Map<String, String>();
          allAccs = new List<String>();
          Map<String,String> accIDtoAccNum = new Map<String,String>();

          for(AxtriaSalesIQTM__Position_Account__c posAcc : scope)
          {
              system.debug('++++++++++++++ posAcc.AxtriaSalesIQTM__Account__r.AccountNumber '+ posAcc.AxtriaSalesIQTM__Account__r.AccountNumber);
              allAccs.add(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber);
          }

          List<Staging_ATL__c> stagingRecs = [select id, Account__c, External_ID_AZ__c, Axtria_Account_ID__c,Country__c, Territory__c from Staging_ATL__c where Axtria_Account_ID__c in :allAccs and Status__c = 'Updated'];

          if(stagingRecs.size() > 0)
          {
              for(Staging_ATL__c sa : stagingRecs)
              {
                  accsToTerrMap.put(sa.External_ID_AZ__c, new Set<String>(sa.Territory__c.split(';')));
                  accToVeevaID.put(sa.External_ID_AZ__c, sa.Account__c);
                  accToCountryID.put(sa.External_ID_AZ__c, sa.Country__c);   
              }
          }

          Set<String> accsInTerr;

          for(AxtriaSalesIQTM__Position_Account__c posAcc : scope)
          {
              accToCountryID.put(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber, posAcc.AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__Country__c);
              if(accsToTerrMap.containsKey(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber))
              {
                  accsInTerr = new Set<String>(accsToTerrMap.get(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber));
                  accsInTerr.add(posAcc.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
              }
              else
              {
                  accToVeevaID.put(posAcc.AxtriaSalesIQTM__Account__c, posAcc.AxtriaSalesIQTM__Account__r.AZ_VeevaID__c);
                  accsInTerr = new Set<String>();
                  accsInTerr.add(posAcc.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
              }
              accsToTerrMap.put(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber, accsInTerr);
              //accIDtoAccNum.put(posAcc.AxtriaSalesIQTM__Account__, posAcc.AxtriaSalesIQTM__Account__r.AccountNumber);
          }

          List<Staging_ATL__c> allATLrecs = new List<Staging_ATL__c>();

          for(String accID : accsToTerrMap.keySet())
          {
             Staging_ATL__c atlRec = new Staging_ATL__c();
             atlRec.Account__c = accID;
             atlRec.Axtria_Account_ID__c = accID;
             atlRec.Territory__c = string.join(new List<String>(accsToTerrMap.get(accID)),';') ;

             
             if(atlRec.Territory__c.substring(0,1) != ';')
             {
                 atlRec.Territory__c = ';' + atlRec.Territory__c+ ';';
             }
             else
             {
                 atlRec.Territory__c = atlRec.Territory__c+ ';';
             }
             
             if(atlRec.Account__c != null)
             {
                 atlRec.External_ID__c = atlRec.Account__c;//atlRec.Account__c;
                 atlRec.Country__c = accToCountryID.get(accID);
                 atlRec.Country_Lookup__c = accToCountryID.get(accID);
                 atlRec.External_ID_AZ__c = accID;
                 atlRec.Status__c = 'Updated';
                 allATLrecs.add(atlRec);
                 
             }
          }

          upsert allATLrecs External_ID_AZ__c;
     /* }
      catch(Exception e)
           {
                errorBatches=true;
                system.debug('++inside catch');
                String Header='ATL Transformation  batch is failed, so delta has been haulted of this org for Today';
                tryCatchEmailAlert.veevaLoad(Header);
           }*/
    }

    global void finish(Database.BatchableContext BC) 
    {

        if(activityLogIDSet != null)
        {
            Database.executeBatch(new ATL_Transformation_null_Accounts(allCountriesList,chain,activityLogIDSet),2000);          
        }
        else
        {
            Database.executeBatch(new ATL_Transformation_null_Accounts(allCountriesList,chain),2000);
        }
      
    }
}