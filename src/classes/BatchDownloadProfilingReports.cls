global with sharing class BatchDownloadProfilingReports implements Database.Batchable<sObject>, Database.Stateful{
    public String query;
    /*public String teamInsID;
    public String prodID;
    public String type;
    public Set<String> pos1;*/
    global String results;
    /*public String localeSeperator = ',';
    public List<String> parameters;
    public Map<String,String> parameterAPIName;
    public String prodCode;
    public String teamInsName;
    public Integer countOfSurveyRec;
    public Integer countOfSurveyRec1;
    public Integer countOfVeeva;
    public Integer countOfVeeva1;

    //Wrapper Work
    public Boolean isVeevaData = false;
    public Boolean isDataFromBoth = false;
    public String team;
    public String teamIns;
    public String prCode;
    public String prName;

    public List<String> responseValVeeva;
    public Map<String,String> veevaQuesToRes = new Map<String,String>();
    public Map<Staging_Veeva_Cust_Data__c,List<String>> accToAllResponses = new Map<Staging_Veeva_Cust_Data__c,List<String>>();
    public List<String> headerVal;*/

    global BatchDownloadProfilingReports(String teamInsID, String prodID, String type) {

        results = '';
        /*this.teamInsID = teamInsID;
        this.prodID = prodID;
        this.type = type;

        if(type == 'AlignedCustomers'){

            isVeevaData = false;

            pos1 = new Set<String>();
            parameters = new List<String>{System.Label.Account_Name,System.Label.Account_Number,System.Label.Speciality,System.Label.Created_Date,System.Label.Last_Modified_Date,System.Label.Position_Name,System.Label.Parent_Position,System.Label.Position_Type,System.Label.EffectiveStartDate,System.Label.EffectiveEndDate,System.Label.Account_Alignment_Type,System.Label.Affiliation_Based_Alignment,System.Label.Team,System.Label.TeamInstance};

            parameterAPIName = new Map<String,String>{System.Label.Account_Name => 'AccountName__c', System.Label.Account_Number => 'Account_Number__c', System.Label.Speciality => 'Speciality__c', System.Label.Created_Date => 'CreatedDate', System.Label.Last_Modified_Date => 'LastModifiedDate', System.Label.Position_Name => 'AxtriaSalesIQTM__Position__r.Name', System.Label.Parent_Position => 'AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__r.Name', System.Label.Position_Type => 'AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c', System.Label.EffectiveStartDate => 'AxtriaSalesIQTM__Effective_Start_Date__c', System.Label.EffectiveEndDate => 'AxtriaSalesIQTM__Effective_End_Date__c', System.Label.Account_Alignment_Type => 'AxtriaSalesIQTM__Account_Alignment_Type__c', System.Label.Affiliation_Based_Alignment => 'AxtriaSalesIQTM__Affiliation_Based_Alignment__c', System.Label.Team => 'AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name', System.Label.TeamInstance => 'AxtriaSalesIQTM__Team_Instance__r.Name'};

            results  = createCSVHeader(parameters);
            results += '\n';

            List<AxtriaSalesIQTM__Position_Product__c> ppList = [Select AxtriaSalesIQTM__Position__c, Product_Catalog__r.Name, AxtriaSalesIQTM__Team_Instance__r.Name FROM AxtriaSalesIQTM__Position_Product__c WHERE Product_Catalog__c =:prodID AND AxtriaSalesIQTM__Team_Instance__c =:teamInsID AND AxtriaSalesIQTM__isActive__c = true];
            if(ppList.size()>0){
                for(AxtriaSalesIQTM__Position_Product__c pp : ppList){
                    pos1.add(pp.AxtriaSalesIQTM__Position__c);
                }
            }
            System.debug('Pos Set : '+pos1);
            System.debug('Pos Set Size: '+pos1.size());

            query = 'Select AccountName__c, Account_Number__c, Speciality__c, CreatedDate, LastModifiedDate, AxtriaSalesIQTM__Position__r.Name, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__r.Name, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c, AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Account_Alignment_Type__c, AxtriaSalesIQTM__Affiliation_Based_Alignment__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name, AxtriaSalesIQTM__Team_Instance__r.Name FROM AxtriaSalesIQTM__Position_Account__c WHERE AxtriaSalesIQTM__Position__c IN:pos1 AND (AxtriaSalesIQTM__Assignment_Status__c =\'Active\' OR AxtriaSalesIQTM__Assignment_Status__c =\'Future Active\')';

            this.query = query;

        }else if(type == 'parameters'){
            
            isVeevaData = false;

            parameters = new List<String>{System.Label.Team,System.Label.TeamInstance,System.Label.Product,System.Label.Parameter_Name};            
            parameterAPIName = new Map<String,String>{System.Label.Team => 'Team__r.Name', System.Label.TeamInstance => 'Team_Instance__r.Name', System.Label.Product => 'Product_Catalog__r.Name', System.Label.Parameter_Name => 'Display_Name__c'};

            results  = createCSVHeader(parameters);
            results += '\n';

            query = 'SELECT Display_Name__c, Team__r.Name, Team_Instance__r.Name, Product_Catalog__r.Name FROM MetaData_Definition__c WHERE Product_Catalog__c =:prodID AND Team_Instance__c =:teamInsID';
            
            this.query = query;

        }else if(type == 'AlignedCustomersButNotProfiled'){
            
            isVeevaData = false;

            pos1 = new Set<String>();
            parameters = new List<String>{System.Label.Account_Name,System.Label.Account_Number,System.Label.Speciality,System.Label.Created_Date,System.Label.Last_Modified_Date,System.Label.Position_Name,System.Label.Parent_Position,System.Label.Position_Type,System.Label.EffectiveStartDate,System.Label.EffectiveEndDate,System.Label.Account_Alignment_Type,System.Label.Affiliation_Based_Alignment,System.Label.Team,System.Label.TeamInstance};

            parameterAPIName = new Map<String,String>{System.Label.Account_Name => 'AccountName__c', System.Label.Account_Number => 'Account_Number__c', System.Label.Speciality => 'Speciality__c', System.Label.Created_Date => 'CreatedDate', System.Label.Last_Modified_Date => 'LastModifiedDate', System.Label.Position_Name => 'AxtriaSalesIQTM__Position__r.Name', System.Label.Parent_Position => 'AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__r.Name', System.Label.Position_Type => 'AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c', System.Label.EffectiveStartDate => 'AxtriaSalesIQTM__Effective_Start_Date__c', System.Label.EffectiveEndDate => 'AxtriaSalesIQTM__Effective_End_Date__c', System.Label.Account_Alignment_Type => 'AxtriaSalesIQTM__Account_Alignment_Type__c', System.Label.Affiliation_Based_Alignment => 'AxtriaSalesIQTM__Affiliation_Based_Alignment__c', System.Label.Team => 'AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name', System.Label.TeamInstance => 'AxtriaSalesIQTM__Team_Instance__r.Name'};

            results  = createCSVHeader(parameters);
            results += '\n';

            List<AxtriaSalesIQTM__Position_Product__c> ppList = [Select AxtriaSalesIQTM__Position__c, Product_Catalog__r.Product_Code__c, AxtriaSalesIQTM__Team_Instance__r.Name FROM AxtriaSalesIQTM__Position_Product__c WHERE Product_Catalog__c =:prodID AND AxtriaSalesIQTM__Team_Instance__c =:teamInsID AND AxtriaSalesIQTM__isActive__c = true];
            if(ppList.size()>0){
                for(AxtriaSalesIQTM__Position_Product__c pp : ppList){
                    pos1.add(pp.AxtriaSalesIQTM__Position__c);
                }
                prodCode = ppList[0].Product_Catalog__r.Product_Code__c;
                teamInsName = ppList[0].AxtriaSalesIQTM__Team_Instance__r.Name;
            }
            System.debug('Pos Set : '+pos1);
            System.debug('Pos Set Size: '+pos1.size());

            query = 'Select AccountName__c, Account_Number__c, Speciality__c, CreatedDate, LastModifiedDate, AxtriaSalesIQTM__Position__r.Name, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__r.Name, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c, AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Account_Alignment_Type__c, AxtriaSalesIQTM__Affiliation_Based_Alignment__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name, AxtriaSalesIQTM__Team_Instance__r.Name FROM AxtriaSalesIQTM__Position_Account__c WHERE AxtriaSalesIQTM__Position__c IN:pos1 AND (AxtriaSalesIQTM__Assignment_Status__c =\'Active\' OR AxtriaSalesIQTM__Assignment_Status__c =\'Future Active\') AND AxtriaSalesIQTM__Account__c NOT IN (Select Account_Lookup__c FROM Staging_Survey_Data__c WHERE Product_Code__c =:prodCode AND Team_Instance__c =:teamInsName) AND AxtriaSalesIQTM__Account__c NOT IN (Select Account_Lookup__c FROM Staging_Veeva_Cust_Data__c WHERE BRAND_ID__c =:prodCode AND Team_Instance__c =:teamInsName)';

            this.query = query;
        }
        else if(type == 'ProfiledCustomers'){

            System.debug('prodPreviewName : '+teamInsID);
            System.debug('teaminsPreviewName : '+prodID);

            List<Product_Catalog__c> prlist = [SELECT Name, Product_Code__c, Team_Instance__r.Name, Team_Instance__r.AxtriaSalesIQTM__Team__r.Name FROM Product_Catalog__c WHERE Name =:prodID AND Team_Instance__r.Name =:teamInsID];

            System.debug('prlist size : '+prlist);

            if(!prlist.isEmpty()){
                this.team = prlist[0].Team_Instance__r.AxtriaSalesIQTM__Team__r.Name;
                this.teamIns = prlist[0].Team_Instance__r.Name;
                this.prCode = prlist[0].Product_Code__c;
                this.prName = prlist[0].Name;
            }
            //All count of Staging Survey data
            for(AggregateResult stagingSurveyData : [SELECT COUNT_DISTINCT(Account_Lookup__c) c1 FROM Staging_Survey_Data__c WHERE Product_Code__c =:prCode AND Team_Instance__c =:teamInsID]){

                countOfSurveyRec = Integer.valueOf(stagingSurveyData.get('c1'));
            }
            //Distinct count of Staging Veeva Cust Data
            for(AggregateResult stagingVeevaData : [SELECT COUNT_DISTINCT(Account_Lookup__c) c2 FROM Staging_Veeva_Cust_Data__c WHERE BRAND_ID__c =:prCode AND Team_Instance__c =:teamInsID AND Account_Lookup__c NOT IN (Select Account_Lookup__c FROM Staging_Survey_Data__c WHERE Product_Code__c =:prCode AND Team_Instance__c =:teamInsID)]){

                countOfVeeva = Integer.valueOf(stagingVeevaData.get('c2'));
            }
            //Staging Case
            if((countOfSurveyRec != 0 && countOfSurveyRec > 0) || (countOfVeeva !=0 && countOfVeeva > 0)){

                isVeevaData = false;

                if(countOfVeeva !=0 && countOfVeeva > 0){
                    isDataFromBoth = true;
                }

                parameters = new List<String>{System.Label.Account_Number,System.Label.Survey_Name,System.Label.Team,System.Label.TeamInstance,System.Label.Type,System.Label.Product_Code,System.Label.ProductName};

                parameterAPIName = new Map<String,String>{System.Label.Account_Number => 'Account_Number__c', System.Label.Survey_Name => 'SURVEY_NAME__c', System.Label.Team => 'Team__c', System.Label.TeamInstance => 'Team_Instance__c', System.Label.Type => 'Type__c', System.Label.Product_Code => 'Product_Code__c', System.Label.ProductName => 'Product_Name__c'};

                //Query to Create Headers
                List<Staging_Survey_Data__c> stagData = [SELECT Short_Question_Text1__c, Short_Question_Text2__c, Short_Question_Text3__c, Short_Question_Text4__c, Short_Question_Text5__c, Short_Question_Text6__c, Short_Question_Text7__c, Short_Question_Text8__c, Short_Question_Text9__c, Short_Question_Text10__c, Short_Question_Text11__c, Short_Question_Text12__c, Short_Question_Text13__c, Short_Question_Text14__c, Short_Question_Text15__c, Short_Question_Text16__c, Short_Question_Text17__c, Short_Question_Text18__c, Short_Question_Text19__c, Short_Question_Text20__c, Short_Question_Text21__c, Short_Question_Text22__c, Short_Question_Text23__c, Short_Question_Text24__c, Short_Question_Text25__c, Response1__c, Response2__c, Response3__c, Response4__c, Response5__c, Response6__c, Response7__c, Response8__c, Response9__c, Response10__c, Response11__c, Response12__c, Response13__c, Response14__c, Response15__c, Response16__c, Response17__c, Response18__c, Response19__c, Response20__c, Response21__c, Response22__c, Response23__c, Response24__c, Response25__c FROM Staging_Survey_Data__c WHERE Product_Code__c =:prCode AND Team_Instance__c =:teamIns LIMIT 1];

                if(stagData!=null && stagData.size()>0){
                    for(Integer i=1;i<=25;i++){
                        //System.debug('stagData[0].get()'+stagData[0].get('Short_Question_Text'+i+'__c'));
                        if(String.valueOf(stagData[0].get('Short_Question_Text'+i+'__c')) != null && String.valueOf(stagData[0].get('Short_Question_Text'+i+'__c')) != ''){
                            parameters.add(String.valueOf(stagData[0].get('Short_Question_Text'+i+'__c')));
                            parameterAPIName.put(String.valueOf(stagData[0].get('Short_Question_Text'+i+'__c')), 'Response'+i+'__c');
                        }
                    }
                }
                parameters.add(System.Label.Created_Date);
                parameters.add(System.Label.Last_Modified_Date);
                parameterAPIName.put(System.Label.Created_Date,'CreatedDate');
                parameterAPIName.put(System.Label.Last_Modified_Date,'LastModifiedDate');

                results  = createCSVHeader(parameters);
                results += '\n';

                System.debug('Headers are : '+results);
                System.debug('parameterAPIName : '+parameterAPIName);

                query = 'Select Account_Number__c, SURVEY_NAME__c, Team__c, Team_Instance__c, Type__c, Product_Code__c, Product_Name__c, CreatedDate, LastModifiedDate, Short_Question_Text1__c, Short_Question_Text2__c, Short_Question_Text3__c, Short_Question_Text4__c, Short_Question_Text5__c, Short_Question_Text6__c, Short_Question_Text7__c, Short_Question_Text8__c, Short_Question_Text9__c, Short_Question_Text10__c, Short_Question_Text11__c, Short_Question_Text12__c, Short_Question_Text13__c, Short_Question_Text14__c, Short_Question_Text15__c, Short_Question_Text16__c, Short_Question_Text17__c, Short_Question_Text18__c, Short_Question_Text19__c, Short_Question_Text20__c, Short_Question_Text21__c, Short_Question_Text22__c, Short_Question_Text23__c, Short_Question_Text24__c, Short_Question_Text25__c, Response1__c, Response2__c, Response3__c, Response4__c, Response5__c, Response6__c, Response7__c, Response8__c, Response9__c, Response10__c, Response11__c, Response12__c, Response13__c, Response14__c, Response15__c, Response16__c, Response17__c, Response18__c, Response19__c, Response20__c, Response21__c, Response22__c, Response23__c, Response24__c, Response25__c FROM Staging_Survey_Data__c WHERE Product_Code__c =:prCode AND Team_Instance__c =:teamIns';

                this.query = query;

            }//Veeva Case
            else if(countOfSurveyRec == 0 && (countOfVeeva !=0 && countOfVeeva > 0)){

                isVeevaData = true;
                parameters = new List<String>{System.Label.Account_Number,System.Label.Survey_Name,System.Label.Team,System.Label.TeamInstance,System.Label.Type,System.Label.Product_Code,System.Label.ProductName};

                query = 'SELECT Id, QUESTION_SHORT_TEXT__c, RESPONSE__c, Account_Lookup__r.AccountNumber, Account_Lookup__r.Type, CreatedDate, LastModifiedDate, SURVEY_NAME__c FROM Staging_Veeva_Cust_Data__c WHERE BRAND_ID__c =:prCode AND Team_Instance__c =:teamInsID AND Account_Lookup__c NOT IN (Select Account_Lookup__c FROM Staging_Survey_Data__c WHERE Product_Code__c =:prCode AND Team_Instance__c =:teamInsID)';  
            }
        }else if(type == 'ProfiledCustomersButNotAligned'){

            pos1 = new Set<String>();

            List<AxtriaSalesIQTM__Position_Product__c> ppList = [Select AxtriaSalesIQTM__Position__c, Product_Catalog__r.Product_Code__c,Product_Catalog__r.Name, AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name FROM AxtriaSalesIQTM__Position_Product__c WHERE Product_Catalog__c =:prodID AND AxtriaSalesIQTM__Team_Instance__c =:teamInsID AND AxtriaSalesIQTM__isActive__c = true];
            System.debug('ppList.size() : '+ppList.size());
            System.debug('ppList.size() : '+ppList);

            if(ppList.size()>0){
                for(AxtriaSalesIQTM__Position_Product__c pp : ppList){
                    if(String.valueOf(pp.AxtriaSalesIQTM__Position__c) != null && String.valueOf(pp.AxtriaSalesIQTM__Position__c) != ''){
                        pos1.add(pp.AxtriaSalesIQTM__Position__c);
                    }
                }
                prodCode = ppList[0].Product_Catalog__r.Product_Code__c;
                teamInsName = ppList[0].AxtriaSalesIQTM__Team_Instance__r.Name;
                this.prCode = ppList[0].Product_Catalog__r.Product_Code__c;
                this.teamIns = ppList[0].AxtriaSalesIQTM__Team_Instance__r.Name;
                this.prName = ppList[0].Product_Catalog__r.Name;
                this.team = ppList[0].AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name;
            }

            //All count of Staging Survey data

            System.debug('prCode : '+prCode);
            System.debug('teamInsID : '+teamInsID);
            System.debug('teamInsName : '+teamInsName);            

            for(AggregateResult stagingSurveyData : [SELECT COUNT_DISTINCT(Account_Lookup__c) c1 FROM Staging_Survey_Data__c WHERE Product_Code__c =:prCode AND Team_Instance__c =:teamInsName AND Account_Lookup__c NOT IN (SELECT AxtriaSalesIQTM__Account__c FROM AxtriaSalesIQTM__Position_Account__c WHERE AxtriaSalesIQTM__Position__c IN : pos1 AND (AxtriaSalesIQTM__Assignment_Status__c ='Active' OR AxtriaSalesIQTM__Assignment_Status__c ='Future Active'))]){

                countOfSurveyRec1 = Integer.valueOf(stagingSurveyData.get('c1'));
            }
            //Distinct count of Staging Veeva Cust Data
            for(AggregateResult stagingVeevaData : [SELECT COUNT_DISTINCT(Account_Lookup__c) c2 FROM Staging_Veeva_Cust_Data__c WHERE BRAND_ID__c =:prCode AND Team_Instance__c =:teamInsName AND Account_Lookup__c NOT IN (Select Account_Lookup__c FROM Staging_Survey_Data__c WHERE Product_Code__c =:prCode AND Team_Instance__c =:teamInsName) AND Account_Lookup__c NOT IN (SELECT AxtriaSalesIQTM__Account__c FROM AxtriaSalesIQTM__Position_Account__c WHERE AxtriaSalesIQTM__Position__c IN : pos1 AND (AxtriaSalesIQTM__Assignment_Status__c ='Active' OR AxtriaSalesIQTM__Assignment_Status__c ='Future Active'))]){

                countOfVeeva1 = Integer.valueOf(stagingVeevaData.get('c2'));
            }

            System.debug('countOfSurveyRec1 : '+countOfSurveyRec1);
            System.debug('countOfVeeva1 : '+countOfVeeva1);

            //Staging Case
            if((countOfSurveyRec1 != 0 && countOfSurveyRec1 > 0) || (countOfVeeva1 !=0 && countOfVeeva1 > 0)){
            
            System.debug('Inside First IF... ');

                isVeevaData = false;

                if(countOfVeeva1 !=0 && countOfVeeva1 > 0){
                    isDataFromBoth = true;
                }

                parameters = new List<String>{System.Label.Account_Number,System.Label.Survey_Name,System.Label.Team,System.Label.TeamInstance,System.Label.Type,System.Label.Product_Code,System.Label.ProductName};

                parameterAPIName = new Map<String,String>{System.Label.Account_Number => 'Account_Number__c', System.Label.Survey_Name => 'SURVEY_NAME__c', System.Label.Team => 'Team__c', System.Label.TeamInstance => 'Team_Instance__c', System.Label.Type => 'Type__c', System.Label.Product_Code => 'Product_Code__c', System.Label.ProductName => 'Product_Name__c'};

                //Query to Create Headers
                List<Staging_Survey_Data__c> stagData = [SELECT Short_Question_Text1__c, Short_Question_Text2__c, Short_Question_Text3__c, Short_Question_Text4__c, Short_Question_Text5__c, Short_Question_Text6__c, Short_Question_Text7__c, Short_Question_Text8__c, Short_Question_Text9__c, Short_Question_Text10__c, Short_Question_Text11__c, Short_Question_Text12__c, Short_Question_Text13__c, Short_Question_Text14__c, Short_Question_Text15__c, Short_Question_Text16__c, Short_Question_Text17__c, Short_Question_Text18__c, Short_Question_Text19__c, Short_Question_Text20__c, Short_Question_Text21__c, Short_Question_Text22__c, Short_Question_Text23__c, Short_Question_Text24__c, Short_Question_Text25__c, Response1__c, Response2__c, Response3__c, Response4__c, Response5__c, Response6__c, Response7__c, Response8__c, Response9__c, Response10__c, Response11__c, Response12__c, Response13__c, Response14__c, Response15__c, Response16__c, Response17__c, Response18__c, Response19__c, Response20__c, Response21__c, Response22__c, Response23__c, Response24__c, Response25__c FROM Staging_Survey_Data__c WHERE Product_Code__c =:prCode AND Team_Instance__c =:teamInsName AND Account_Lookup__c NOT IN (SELECT AxtriaSalesIQTM__Account__c FROM AxtriaSalesIQTM__Position_Account__c WHERE AxtriaSalesIQTM__Position__c IN : pos1 AND (AxtriaSalesIQTM__Assignment_Status__c ='Active' OR AxtriaSalesIQTM__Assignment_Status__c ='Future Active')) LIMIT 1];

                if(stagData!=null && stagData.size()>0){
                    for(Integer i=1;i<=25;i++){
                        //System.debug('stagData[0].get()'+stagData[0].get('Short_Question_Text'+i+'__c'));
                        if(String.valueOf(stagData[0].get('Short_Question_Text'+i+'__c')) != null && String.valueOf(stagData[0].get('Short_Question_Text'+i+'__c')) != ''){
                            parameters.add(String.valueOf(stagData[0].get('Short_Question_Text'+i+'__c')));
                            parameterAPIName.put(String.valueOf(stagData[0].get('Short_Question_Text'+i+'__c')), 'Response'+i+'__c');
                        }
                    }
                }
                parameters.add(System.Label.Created_Date);
                parameters.add(System.Label.Last_Modified_Date);
                parameterAPIName.put(System.Label.Created_Date,'CreatedDate');
                parameterAPIName.put(System.Label.Last_Modified_Date,'LastModifiedDate');

                results  = createCSVHeader(parameters);
                results += '\n';

                System.debug('Headers are : '+results);
                System.debug('parameterAPIName : '+parameterAPIName);

                query = 'Select Account_Number__c, SURVEY_NAME__c, Team__c, Team_Instance__c, Type__c, Product_Code__c, Product_Name__c, CreatedDate, LastModifiedDate, Short_Question_Text1__c, Short_Question_Text2__c, Short_Question_Text3__c, Short_Question_Text4__c, Short_Question_Text5__c, Short_Question_Text6__c, Short_Question_Text7__c, Short_Question_Text8__c, Short_Question_Text9__c, Short_Question_Text10__c, Short_Question_Text11__c, Short_Question_Text12__c, Short_Question_Text13__c, Short_Question_Text14__c, Short_Question_Text15__c, Short_Question_Text16__c, Short_Question_Text17__c, Short_Question_Text18__c, Short_Question_Text19__c, Short_Question_Text20__c, Short_Question_Text21__c, Short_Question_Text22__c, Short_Question_Text23__c, Short_Question_Text24__c, Short_Question_Text25__c, Response1__c, Response2__c, Response3__c, Response4__c, Response5__c, Response6__c, Response7__c, Response8__c, Response9__c, Response10__c, Response11__c, Response12__c, Response13__c, Response14__c, Response15__c, Response16__c, Response17__c, Response18__c, Response19__c, Response20__c, Response21__c, Response22__c, Response23__c, Response24__c, Response25__c FROM Staging_Survey_Data__c WHERE Product_Code__c =:prCode AND Team_Instance__c =:teamInsName AND Account_Lookup__c NOT IN (SELECT AxtriaSalesIQTM__Account__c FROM AxtriaSalesIQTM__Position_Account__c WHERE AxtriaSalesIQTM__Position__c IN : pos1 AND (AxtriaSalesIQTM__Assignment_Status__c =\'Active\' OR AxtriaSalesIQTM__Assignment_Status__c =\'Future Active\'))';
                this.query = query;

            }//Veeva Case
            else if(countOfSurveyRec1 == 0 && (countOfVeeva1 !=0 && countOfVeeva1 > 0)){
            
            System.debug('Inside Second IF... ');

                isVeevaData = true;

                parameters = new List<String>{System.Label.Account_Number,System.Label.Survey_Name,System.Label.Team,System.Label.TeamInstance,System.Label.Type,System.Label.Product_Code,System.Label.ProductName};

                query = 'SELECT Id FROM Staging_Veeva_Cust_Data__c WHERE BRAND_ID__c =:prCode AND Team_Instance__c =:teamInsName AND Account_Lookup__c NOT IN (Select Account_Lookup__c FROM Staging_Survey_Data__c WHERE Product_Code__c =:prCode AND Team_Instance__c =:teamInsID)';

                this.query = query;
            }
        }*/
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        //System.debug('Query is : '+query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<sObject> scope) {
        /*System.debug('scope size : '+scope.size());
        System.debug('query : '+query);
        System.debug('type : '+type);

        if(isVeevaData){
            Map<String,Staging_Veeva_Cust_Data__c> accNoToVeevaMap = new Map<String,Staging_Veeva_Cust_Data__c>();
            Map<String,String> veevaQuesToRes = new Map<String,String>();

            if(type == 'ProfiledCustomersButNotAligned'){
                System.debug('pos1 size in execute : '+pos1.size());
                List<sObject> scope1 = [SELECT Id, QUESTION_SHORT_TEXT__c, RESPONSE__c, Account_Lookup__r.AccountNumber, Account_Lookup__r.Type, CreatedDate, LastModifiedDate, SURVEY_NAME__c FROM Staging_Veeva_Cust_Data__c WHERE Id IN:scope AND Account_Lookup__c NOT IN (SELECT AxtriaSalesIQTM__Account__c FROM AxtriaSalesIQTM__Position_Account__c WHERE AxtriaSalesIQTM__Position__c IN : pos1 AND (AxtriaSalesIQTM__Assignment_Status__c ='Active' OR AxtriaSalesIQTM__Assignment_Status__c ='Future Active'))];

                System.debug('scope1 size : '+scope1.size());
                System.debug('scope1 : '+scope1);

                headerVal = new List<String>();
                for(sObject data: scope1){
                    accNoToVeevaMap.put(String.valueOf(data.getSObject('Account_Lookup__r').get('AccountNumber')),(Staging_Veeva_Cust_Data__c)data);
                    veevaQuesToRes.put(data.getSObject('Account_Lookup__r').get('AccountNumber')+'|'+data.get('QUESTION_SHORT_TEXT__c'), String.valueOf(data.get('RESPONSE__c')));
                }
                for(String s : veevaQuesToRes.keySet()){
                    System.debug('s : '+s);
                    System.debug('s.split : '+s.split('\\|')[1]);
                    if(!headerVal.contains(s.split('\\|')[1])){
                        headerVal.add(s.split('\\|')[1]);
                    }
                }
                System.debug('headerVal : '+headerVal);
                System.debug('headerVal Size : '+headerVal.size());

                //Create Response from Veeva Records
                for(String s : accNoToVeevaMap.keySet()){
                    responseValVeeva = new List<String>();
                    for(Integer i=0; i<headerVal.size();i++){                    
                        if(veevaQuesToRes.containsKey(s+'|'+headerVal.get(i))){
                            responseValVeeva.add(veevaQuesToRes.get(s+'|'+headerVal.get(i)));
                        }
                        else{
                            responseValVeeva.add('-');
                        }
                    }
                    accToAllResponses.put(accNoToVeevaMap.get(s),responseValVeeva);
                }
            }else if(type == 'ProfiledCustomers'){
                headerVal = new List<String>();
                for(sObject data: scope){
                    accNoToVeevaMap.put(String.valueOf(data.getSObject('Account_Lookup__r').get('AccountNumber')),(Staging_Veeva_Cust_Data__c)data);
                    veevaQuesToRes.put(data.getSObject('Account_Lookup__r').get('AccountNumber')+'|'+data.get('QUESTION_SHORT_TEXT__c'), String.valueOf(data.get('RESPONSE__c')));
                }
                for(String s : veevaQuesToRes.keySet()){
                    System.debug('s : '+s);
                    System.debug('s.split : '+s.split('\\|')[1]);
                    if(!headerVal.contains(s.split('\\|')[1])){
                        headerVal.add(s.split('\\|')[1]);
                    }
                }
                System.debug('headerVal : '+headerVal);
                System.debug('headerVal Size : '+headerVal.size());

                //Create Response from Veeva Records
                for(String s : accNoToVeevaMap.keySet()){
                    responseValVeeva = new List<String>();
                    for(Integer i=0; i<headerVal.size();i++){                    
                        if(veevaQuesToRes.containsKey(s+'|'+headerVal.get(i))){
                            responseValVeeva.add(veevaQuesToRes.get(s+'|'+headerVal.get(i)));
                        }
                        else{
                            responseValVeeva.add('-');
                        }
                    }
                    accToAllResponses.put(accNoToVeevaMap.get(s),responseValVeeva);
                }
            }
        }else{
            for(sObject data: scope){
                results += createCSVRow(data);
                results += '\n';
                System.debug('Row wise data : '+results);
            }
        }*/
    }

    global void finish(Database.BatchableContext BC) {
    /*    
        if(isVeevaData){

            Datetime myDateTime1 = datetime.now();
            String fileTitle1 =  type+ '_' + myDateTime1.format();

            System.debug('accToAllResponses : '+accToAllResponses);
            System.debug('parameters before : '+parameters);
            System.debug('headerVal : '+headerVal);

            parameters.addAll(headerVal);
            parameters.add('Created Date');
            parameters.add('Last Modified Date');

            results = '';
            results  = createCSVHeader(parameters);
            results += '\n';

            System.debug('parameters after : '+parameters);
            for(Staging_Veeva_Cust_Data__c vd : accToAllResponses.keySet()){
                results += createCSVRow(vd);   
            }

            System.debug('results : '+ results);

            ContentVersion file = new ContentVersion(title = fileTitle1 + '.csv', versionData =  Blob.valueOf('\uFEFF'+ results ), pathOnClient = '/' + fileTitle1 + '.csv');
            insert file;

            SnTDMLSecurityUtil.printDebugMessage('File Id : '+file.ID);

            String body;
            String subject = 'Download Profiling Data';
            
            body = 'Hello,'+' \n \n '+'Attached the Profiled Customers Information you requested to send.'+' \n \n ';
            body += 'Name : '+System.UserInfo.getUserEmail() + ' \n ';
            body += 'Download Time : '+System.now() + ' \n ';
                
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            email.setSubject( subject );
            email.setToAddresses(new list<String>{ UserInfo.getUserEmail() });
            email.setPlainTextBody( body );
            List<Messaging.EmailFileAttachment> attachments = ContentDocumentAsAttachement(new Id[]{file.Id});
            email.setFileAttachments(attachments);
            Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});

        }else{
            if(isDataFromBoth){
                datetime myDateTime2 = datetime.now();
                String fileTitle3 =  type+ '_' + myDateTime2.format();
                //insert file for staging data
                ContentVersion file1 = new ContentVersion(title = fileTitle3 + '.csv', versionData =  Blob.valueOf('\uFEFF'+ results ), pathOnClient = '/' + fileTitle3 + '.csv');
                insert file1;

                SnTDMLSecurityUtil.printDebugMessage('File Id : '+file1.ID);

                Id fileId = file1.ID;
                //Chain Veeva batch
                BatchDownloadProfilingDataForVeevaObj batch = new BatchDownloadProfilingDataForVeevaObj(teamIns,prCode,type,fileId);
                Database.executeBatch(batch);

            }else{
                datetime myDateTime = datetime.now();
                String fileTitle =  type+ '_' + myDateTime.format();
                ContentVersion file = new ContentVersion(title = fileTitle + '.csv', versionData =  Blob.valueOf('\uFEFF'+ results ), pathOnClient = '/' + fileTitle + '.csv');
                insert file;
                
                SnTDMLSecurityUtil.printDebugMessage('File Id : '+file.ID);
                
                String body;
                String subject;
                if(type == 'AlignedCustomers' || type == 'AlignedCustomersButNotProfiled'){
                    subject = 'Download Alignment Data';
                    body = 'Hello,'+' \n \n '+'Attached the Aligned Customers Information you requested to send.'+' \n \n ';
                    body += 'Name : '+System.UserInfo.getUserEmail() + ' \n ';
                    body += 'Download Time : '+System.now() + ' \n ';
                }else if(type == 'ProfiledCustomers' || type == 'ProfiledCustomersButNotAligned'){
                    subject = 'Download Profiling Data';
                    body = 'Hello,'+' \n \n '+'Attached the Profiled Customers Information you requested to send.'+' \n \n ';
                    body += 'Name : '+System.UserInfo.getUserEmail() + ' \n ';
                    body += 'Download Time : '+System.now() + ' \n ';
                }else if(type == 'parameters'){
                    subject = 'Download Parameter\'s Data';
                    body = 'Hello,'+' \n \n '+'Attached the Parameter\'s Information you requested to send.'+' \n \n ';
                    body += 'Name : '+System.UserInfo.getUserEmail() + ' \n ';
                    body += 'Download Time : '+System.now() + ' \n ';
                }
                
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                email.setSubject( subject );
                email.setToAddresses(new list<String>{ UserInfo.getUserEmail() });
                email.setPlainTextBody( body );
                List<Messaging.EmailFileAttachment> attachments = ContentDocumentAsAttachement(new Id[]{file.Id});
                email.setFileAttachments(attachments);
                Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
            }    
        }*/        
    }
    //Method for creating CSV rows from veeva data
    /*private String createCSVRow(Staging_Veeva_Cust_Data__c vd){

        String row = '"';
        row += String.valueOf(vd.Account_Lookup__r.AccountNumber) + '","';
        row += String.valueOf(vd.SURVEY_NAME__c) + '","';
        row += team + '","';
        row +=  teamIns + '","';
        row +=  String.valueOf(vd.Account_Lookup__r.Type) + '","';
        row +=  prCode + '","';
        row +=  prName + '","';
        for(String s : accToAllResponses.get(vd)){
            row +=  s + '","';
        }
        row +=  Date.valueOf(vd.CreatedDate) + '","';
        row +=  Date.valueOf(vd.LastModifiedDate);
        row +=  '"\n';
        
        return row;
    }*/
    //Method for creating CSV Headers.
    /*private string createCSVHeader(list<String> parameters){
        String headers = '';
        
        for(String parameter: parameters){
            try
            {               
               headers += parameter;//String.valueof(output.value);
               headers += localeSeperator;
               
            }
            catch(Exception ex)
            {
               headers += parameter;
               headers += localeSeperator;
            }
            
        }
        return headers;
    }*/
    //Generic method for creating CSV rows
    /*private String createCSVRow(sObject posAcc){
        String row = '';
        for(String parameter: parameters){
            System.debug('parameter is : '+parameter);

            if(parameterAPIName.get(parameter).contains('.')){
                String paramName = String.valueof(parameterAPIName.get(parameter));
                SnTDMLSecurityUtil.printDebugMessage('++++++++++ Hey Parameter API Name with . is : ' + paramName);
                List<String> allRecs = new List<String>(paramName.split('\\.'));
                List<String>correctedRecs = new List<String>();
                System.debug('All Recs : '+allRecs);
                for(String str : allRecs)
                {
                    correctedRecs.add(str);
                }
                System.debug('correctedRecs : '+correctedRecs);
                SnTDMLSecurityUtil.printDebugMessage('+++++++++');
                System.debug(posAcc);
                if(correctedRecs.size() > 2){   
                    SnTDMLSecurityUtil.printDebugMessage('++++ row in if of correctedRecs.size() > 2  bef '+ row);
                    if(posAcc.getSObject(correctedRecs[0]).getSObject(correctedRecs[1]) !=null){
                        row += ''+'\"'+ (String.isNotBlank(String.valueof(posAcc.getSObject(correctedRecs[0]).getSObject(correctedRecs[1]).get(correctedRecs[2]))) ? String.valueof(posAcc.getSObject(correctedRecs[0]).getSObject(correctedRecs[1]).get(correctedRecs[2])):'-' )+'\"'+localeSeperator ;
                        SnTDMLSecurityUtil.printDebugMessage('++++ Wuhu '+ row);
                    }
                    else{
                        row+= '-'+localeSeperator; // Modified by RT for STIMPS-428
                        SnTDMLSecurityUtil.printDebugMessage('++++ row in if of correctedRecs.size() > 2  aft'+ row);
                    }
                }
                else{
                    row += ''+'\"'+(String.isNotBlank(String.valueof(posAcc.getSObject(correctedRecs[0]).get(correctedRecs[1]))) ? String.valueof(posAcc.getSObject(correctedRecs[0]).get(correctedRecs[1])):'-' )+'\"'+localeSeperator ;
                    SnTDMLSecurityUtil.printDebugMessage('++++ row in else of correctedRecs.size() > 2 '+ row);
                }
              
            }else{

                SnTDMLSecurityUtil.printDebugMessage('++++++++++ Hey Parameter API Name without . is : ' + String.valueof(parameterAPIName.get(parameter)));
                row += ''+'\"'+(String.isNotBlank((String.valueof(posAcc.get(parameterAPIName.get(parameter))))) ? (String.valueof(posAcc.get(parameterAPIName.get(parameter)))).replace(',',' '): '-') +'\"'+localeSeperator;
            }
        }
        return row;
    }*/

    /*public static List<Messaging.EmailFileAttachment> ContentDocumentAsAttachement(Id[] contentDocumentIds) {
        List<Messaging.EmailFileAttachment> attachments = new List<Messaging.EmailFileAttachment>{};
        List<ContentVersion> documents                  = new List<ContentVersion>{};
    
        documents.addAll([
          SELECT Id, Title, FileType, VersionData, isLatest, ContentDocumentId
          FROM ContentVersion
          WHERE Id IN :contentDocumentIds
        ]);
    
        for(ContentVersion document: documents) {
          
          Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
          attachment.setBody(document.VersionData);
          attachment.setFileName(document.Title);
          attachments.add(attachment);
        }
        return attachments;
    }*/
}