/**
Name        :   Position_Account_TSFAcc_Update_Utility
Author      :   Ritwik Shokeen
Date        :   05/10/2018
Description :   Position_Account_TSFAcc_Update_Utility
*/
global with sharing class Position_Account_TSFAcc_Update_Utility implements Database.Batchable<sObject>,Database.Stateful {
    public String query;
    public String selectedTeamInstance;
    public String selectedTeam;
    //public AxtriaSalesIQTM__Team_Instance__c notSelectedTeamInstance;
    Map<String,Id> teamInstanceNametoIDMap;
    Map<String,string> teamInstanceNametoTeamNameMap;
    //Map<String,string> teamInstanceNametoAZCycleMap;
    //Map<String,string> teamInstanceNametoAZCycleNameMap;
    Map<String,string> teamInstanceNametoTeamIDMap;
    Map<String,string> teamInstanceNametoBUMap;
    Map<String,string> brandIDteamInstanceNametoBrandTeamInstIDMap;
    //public Map<String,AxtriaSalesIQTM__Position_Account__c> questionToObjectMap;
    //public Map<String,Integer> questionToResponseFieldNoMap;
    public String accountfieldname;
    public String questionname;
    //public Integer availableRespField;
    public Map<String,Integer> productToavailableRespFieldMap;
    public Integer GlobalCounter = 1;
    Map<string,id> allTIPositionCodesToIDMap;
    Map<string,String> TSFAccNoTerrCodeToAccessibilityMap;
    
    global Position_Account_TSFAcc_Update_Utility(String teamInstance) {//,String accfieldname,String qname
        
        /*if(qname!=null && qname!=''){
            questionname=qname;
        }
        else{
            questionname='Accessibility';
        }*/
        questionname='Accessibility';
        //availableRespField=10;//Should start from 1 then ++ //Currently Manually at Response10__c
        
        allTIPositionCodesToIDMap = new Map<string,id>();
        List<AxtriaSalesIQTM__Position__c> posList = new List<AxtriaSalesIQTM__Position__c>([select id,AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_instance__r.name from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_instance__r.name = :teamInstance]);
        
        for (AxtriaSalesIQTM__Position__c p: posList){
            allTIPositionCodesToIDMap.put(p.AxtriaSalesIQTM__Client_Position_Code__c,p.id);
        }
        SnTDMLSecurityUtil.printDebugMessage('allTIPositionCodesToIDMap+++++'+allTIPositionCodesToIDMap);
        
        teamInstanceNametoIDMap = new Map<String,Id>();
        teamInstanceNametoTeamNameMap = new Map<String,String>();
        //teamInstanceNametoAZCycleMap = new Map<String,String>();
        //teamInstanceNametoAZCycleNameMap = new Map<String,String>();
        teamInstanceNametoTeamIDMap = new Map<String,String>();
        teamInstanceNametoBUMap = new Map<String,String>();
        brandIDteamInstanceNametoBrandTeamInstIDMap = new Map<String,String>();
        productToavailableRespFieldMap = new Map<String,Integer>();
        TSFAccNoTerrCodeToAccessibilityMap = new Map<string,String>();
        //questionToObjectMap = new Map<String,Staging_Cust_Survey_Profiling__c>();
        //questionToResponseFieldNoMap = new Map<String,Integer>();
        selectedTeamInstance = teamInstance;
        GlobalCounter = 1;
        
        for(AxtriaSalesIQTM__Team_Instance__c teamIns : [Select Id,Name,AxtriaSalesIQTM__Team__c,/*AxtriaSalesIQTM__Team__r.Business_Unit_Loopup__c,*/AxtriaSalesIQTM__Team__r.name/*,Cycle__c,Cycle__r.name*/ From AxtriaSalesIQTM__Team_Instance__c  WHERE Name != NULL]){
            teamInstanceNametoIDMap.put(teamIns.Name,teamIns.Id);
            teamInstanceNametoTeamNameMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__r.name);
            //teamInstanceNametoAZCycleMap.put(teamIns.Name,teamIns.Cycle__c);
           // teamInstanceNametoAZCycleNameMap.put(teamIns.Name,teamIns.Cycle__r.name);
            teamInstanceNametoTeamIDMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__c);
            //teamInstanceNametoBUMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__r.Business_Unit_Loopup__c);
        }
        //cpmmented as not below piece not used
        /*for(Brand_Team_Instance__c bti :[select id,Brand__c,Brand__r.name,Brand__r.External_ID__c,Team_Instance__c,Team_Instance__r.name from Brand_Team_Instance__c]){
            brandIDteamInstanceNametoBrandTeamInstIDMap.put(bti.Brand__r.External_ID__c+bti.Brand__r.name+bti.Team_Instance__r.name,bti.id);
        }*/
        
        selectedTeam = teamInstanceNametoTeamNameMap.get(selectedTeamInstance);
        
        
        //query = 'Select Id, Name, CurrencyIsoCode, SIQ_Veeva_Accessibility_AZ_EU__c, SIQ_Veeva_Account_vod__c, SIQ_Veeva_Territory_vod__c, SIQ_Veeva_External_ID__c FROM SIQ_Veeva_TSF_vod_Inbound__c where SIQ_Veeva_Territory_vod__c in :allTIPositionCodesToIDMap.keyset() ';//CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, LastActivityDate, 
        
        
        
        query = 'Select Id, Name, CurrencyIsoCode, AxtriaSalesIQTM__Account_Alignment_Type__c, AxtriaSalesIQTM__Account_Target_Type__c, AxtriaSalesIQTM__Account__c, AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Assignment_Status__c, AxtriaSalesIQTM__Change_Status__c, AxtriaSalesIQTM__Comments__c, AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Locked__c, AxtriaSalesIQTM__Metric10__c, AxtriaSalesIQTM__Metric1__c, AxtriaSalesIQTM__Metric2__c, AxtriaSalesIQTM__Metric3__c, AxtriaSalesIQTM__Metric4__c, AxtriaSalesIQTM__Metric5__c, AxtriaSalesIQTM__Metric6__c, AxtriaSalesIQTM__Metric7__c, AxtriaSalesIQTM__Metric8__c, AxtriaSalesIQTM__Metric9__c, AxtriaSalesIQTM__Metric_1__c, AxtriaSalesIQTM__Position_Team_Instance__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Segment_1__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__TempDestinationrgb__c, AxtriaSalesIQTM__isExcluded__c, AxtriaSalesIQTM__logged__c, Accessibility_Range__c, Speciality__c, Acc_Pos__c, AxtriaSalesIQTM__Account_Relationship_Type__c, Country__c, Event__c, AxtriaSalesIQTM__Affiliation_Based_Alignment__c, AxtriaSalesIQTM__IsShared__c, AxtriaSalesIQTM__SharedWith__c, AccountNumber__c, MC_code__c FROM AxtriaSalesIQTM__Position_Account__c   Where AxtriaSalesIQTM__Team_Instance__r.name = \'' + teamInstance + '\'';
        //Where Team_Instance__r.name = \'' + teamInstance + '\'';
        
        /*query = 'Select Id, Name, CurrencyIsoCode, Acc_Pos__c, BU__c, Brand_ID__c, Brand__c,Brand__r.Brand__c, Business_Unit__c, Cycle2__c, Cycle__c, External_ID__c, Is_Active__c, LineFormula__c, Line__c, Physician__c, Physician__r.AccountNumber, ';
        if(accfieldname!=null){
            query += 'Physician__r.'+accfieldname+' ,';
            accountfieldname=accfieldname;
        }
        else{
            query += 'Physician__r.AxtriaSalesIQTM__Speciality__c ,';
            accountfieldname='AxtriaSalesIQTM__Speciality__c';
        }
        query += ' Position_Account__c, Position__c, Profiling_Response__c, Response10__c, Response1__c, Response2__c, Response3__c, Response4__c, Response5__c, Response6__c, Response7__c, Response8__c, Response9__c, Survey_Master__c, Team_Instance__c, Team_Instance__r.name, Team__c, is_inserted__c, is_updated__c FROM AxtriaSalesIQTM__Position_Account__c  Where Team_Instance__r.name = \'' + teamInstance + '\'';
        */
        
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        SnTDMLSecurityUtil.printDebugMessage('query+++++++++++++   ' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Account__c> scopeRecs) {
        
        SnTDMLSecurityUtil.printDebugMessage('Hello++++++++++++++++++++++++++++');
        Set<String> allPosCodesScope = new Set<String>();

        for(AxtriaSalesIQTM__Position_Account__c posAcc : scopeRecs)
        {
            allPosCodesScope.add(posAcc.AxtriaSalesIQTM__Position__c);
        }


        List<SIQ_Veeva_TSF_vod_Inbound__c> TSFvodList = new List<SIQ_Veeva_TSF_vod_Inbound__c>([Select Id, Name, CurrencyIsoCode, SIQ_Veeva_Accessibility_AZ_EU__c, SIQ_Veeva_Account_vod__c, SIQ_Veeva_Territory_vod__c, SIQ_Veeva_External_ID__c FROM SIQ_Veeva_TSF_vod_Inbound__c where SIQ_Veeva_Territory_vod__c in :allPosCodesScope]);
        Set<String> TSFAccNoSet = new Set<String>();
        
        for (SIQ_Veeva_TSF_vod_Inbound__c TSFvodRec : TSFvodList){
            if(TSFvodRec.SIQ_Veeva_Account_vod__c!=null){
                TSFAccNoSet.add(TSFvodRec.SIQ_Veeva_Account_vod__c);
            }
        }
        
        List<Account> AccList = new List<Account>([Select Id,Name,AccountNumber, AZ_VeevaID__c from Account where AZ_VeevaID__c in :TSFAccNoSet]);
        Map<String,String> AccVeevaIDtoAccNoMap = new Map<String,String>();
        
        for (Account a : AccList){
            AccVeevaIDtoAccNoMap.put(a.AZ_VeevaID__c,a.AccountNumber);
        }
        
        
        
        SnTDMLSecurityUtil.printDebugMessage('TSFvodList+++++'+TSFvodList);
        for (SIQ_Veeva_TSF_vod_Inbound__c TSFvodRec : TSFvodList){
            if(TSFvodRec.SIQ_Veeva_Accessibility_AZ_EU__c!=null && TSFvodRec.SIQ_Veeva_Account_vod__c!=null && TSFvodRec.SIQ_Veeva_Territory_vod__c!=null ){//&& //AccVeevaIDtoAccNoMap.containsKey(TSFvodRec.SIQ_Veeva_Account_vod__c)
                TSFAccNoTerrCodeToAccessibilityMap.put(TSFvodRec.SIQ_Veeva_Account_vod__c+TSFvodRec.SIQ_Veeva_Territory_vod__c,TSFvodRec.SIQ_Veeva_Accessibility_AZ_EU__c); //AccVeevaIDtoAccNoMap.get(TSFvodRec.SIQ_Veeva_Account_vod__c)
            //}
        }
    }
        

        for(AxtriaSalesIQTM__Position_Account__c PosAccRec : scopeRecs){
            
            if(TSFAccNoTerrCodeToAccessibilityMap.containsKey(PosAccRec.AxtriaSalesIQTM__Account__r.AccountNumber+PosAccRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c)){
                PosAccRec.Accessibility_Range__c = TSFAccNoTerrCodeToAccessibilityMap.get(PosAccRec.AxtriaSalesIQTM__Account__r.AccountNumber+PosAccRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
            }
            else{
                PosAccRec.Accessibility_Range__c = '9999';
            }
            
            
        }
        
        //update scopeRecs;
        SnTDMLSecurityUtil.updateRecords(scopeRecs, 'Position_Account_TSFAcc_Update_Utility');

    }

    global void finish(Database.BatchableContext BC) {
        
    }
}