global with sharing class BatchUpdateSharedFlag implements Database.Batchable<sObject>,Database.stateful 
{
    //Removed occurance of Line__c due to object purge activity A1422
    public String query;
    public String line;
    public String brand;
    public String Ids;
    Boolean flag= true;
    Boolean thirdFlag = true;
    public String businessUnit ='';
    public String businessUnitcycle ='';
    public String businessUnitcyclee ='';
    public AxtriaSalesIQTM__TriggerContol__c customsetting ;
    public AxtriaSalesIQTM__TriggerContol__c customsetting2 ;
    public list<AxtriaSalesIQTM__TriggerContol__c>customsettinglist {get;set;}
    public string ruleidis {get;set;}
    public boolean directFlag=false;
    public string teaminstancename ;
    public boolean allcallflag ;
    public set<string>products {get;set;}

    //Added by HT(A0944) on 12th June 2020
    public String alignNmsp;
    public String batchName;
    public String callPlanLoggerID;
    public Boolean proceedNextBatch = true;
    

    global BatchUpdateSharedFlag(String ruleId){
        customsetting = new AxtriaSalesIQTM__TriggerContol__c();
        customsetting2 = new AxtriaSalesIQTM__TriggerContol__c();
        customsettinglist = new list<AxtriaSalesIQTM__TriggerContol__c>();
        ruleidis = '';
        ruleidis = ruleID;
        allcallflag =true;
        products = new set<string>();

        Measure_Master__c rule = [SELECT Id, Team__r.Name,Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c, Brand_Lookup__r.Name, Brand_Lookup__c,/*Line_2__c,*/ Team_Instance__c FROM Measure_Master__c WHERE Id =: ruleId LIMIT 1];
        //line = rule.Line_2__c;
        brand = rule.Brand_Lookup__r.Name;
        businessUnit = rule.Team_Instance__c;
        businessUnitcycle = rule.Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c;
        list<Product_Priority__c>priority = new list<Product_Priority__c>();

        try{
            priority = [select id,Product__c,Priority__c,TeamInstance__c from Product_Priority__c where Product__c=: rule.Brand_Lookup__c and TeamInstance__c=:businessUnit and Type__c = 'Specialty'];//Shivansh - A1450 -- Replacing Account_To_ProductType__c with Product_Priority__c -- Adding extra filter of Type__c
        }
        catch(Exception ex){
            SnTDMLSecurityUtil.printDebugMessage('============Exception caught in priority query-------');
        } 
        
        if(priority!=null && priority.size() >0 &&priority[0].Priority__c =='P2')
            allcallflag = false;

        SnTDMLSecurityUtil.printDebugMessage('=======allcallflag::::'+allcallflag);

        if(allcallflag)
        {
            SnTDMLSecurityUtil.printDebugMessage('========P1 Priority=======');
            this.query  = 'SELECT Id, Share__c, AxtriaSalesIQTM__Account__c, P1_Original__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Name,allcall__c,Final_TCF__c, AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__c ';
            this.query += 'FROM AxtriaSalesIQTM__Position_Account_Call_Plan__c '; 
            this.query += 'WHERE P1_Original__c = \'' + brand + '\' AND AxtriaSalesIQTM__Position__c != null AND AxtriaSalesIQTM__isIncludedCallPlan__c = true and AxtriaSalesIQTM__Team_Instance__c =:businessUnit';
        }
        else{
            SnTDMLSecurityUtil.printDebugMessage('========P2 Priority=======');
            this.query  = 'SELECT Id, Share__c, AxtriaSalesIQTM__Account__c, P1_Original__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Name,P2__c,allcall2__c,Final_TCF__c, TCF_P2__c,AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__c ';
            this.query += 'FROM AxtriaSalesIQTM__Position_Account_Call_Plan__c '; 
            this.query += 'WHERE P2__c = \'' + brand + '\' AND AxtriaSalesIQTM__Position__c != null AND AxtriaSalesIQTM__isIncludedCallPlan__c = true and AxtriaSalesIQTM__Team_Instance__c =:businessUnit';
        }
        
    }

    //Added by HT(A0944) on 12th June 2020
    global BatchUpdateSharedFlag(String teamIns,String directLoad,String loggerID,String alignNmspPrefix)
    {
        directFlag=true;
        customsetting = new AxtriaSalesIQTM__TriggerContol__c();
        customsetting2 = new AxtriaSalesIQTM__TriggerContol__c();
        customsettinglist = new list<AxtriaSalesIQTM__TriggerContol__c>();
        //ruleidis = '';
        teaminstancename ='';
        allcallflag =true;
        products = new set<string>();

        callPlanLoggerID = loggerID;
        alignNmsp = alignNmspPrefix;
        SnTDMLSecurityUtil.printDebugMessage('callPlanLoggerID--'+callPlanLoggerID);
        SnTDMLSecurityUtil.printDebugMessage('alignNmsp--'+alignNmsp);
      
        //ruleidis = ruleID;
        //Measure_Master__c rule = [SELECT Id, Team__r.Name, Brand_Lookup__r.Name, Line_2__c, Team_Instance__c, Cycle__c FROM Measure_Master__c WHERE Id =: ruleId LIMIT 1];
        //line = rule.Line_2__c;
        //brand = rule.Brand_Lookup__r.Name;
        list<AxtriaSalesIQTM__Team_Instance__c>tilist = [select id,name,AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c from AxtriaSalesIQTM__Team_Instance__c where name=:teamIns];
        businessUnitcyclee = tilist[0].AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c;
        teaminstancename = teamIns;
                SnTDMLSecurityUtil.printDebugMessage('==========tilist:::::'+tilist.size());
        if(tilist!=null && tilist.size()>0){
            businessUnit = tilist[0].id;
        }
        this.query  = 'SELECT Id, Share__c, AxtriaSalesIQTM__Account__c, P1_Original__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Name,allcall__c,Final_TCF__c, AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__c ';
        this.query += 'FROM AxtriaSalesIQTM__Position_Account_Call_Plan__c '; 
        this.query += 'WHERE AxtriaSalesIQTM__Position__c != null AND AxtriaSalesIQTM__isIncludedCallPlan__c = true and AxtriaSalesIQTM__Team_Instance__r.Name =:teaminstancename';
    }
    
    global BatchUpdateSharedFlag(String teamIns,String directLoad){
        directFlag=true;
        customsetting = new AxtriaSalesIQTM__TriggerContol__c();
        customsetting2 = new AxtriaSalesIQTM__TriggerContol__c();
        customsettinglist = new list<AxtriaSalesIQTM__TriggerContol__c>();
        //ruleidis = '';
        teaminstancename ='';
        allcallflag =true;
        products = new set<string>();
        //ruleidis = ruleID;
        //Measure_Master__c rule = [SELECT Id, Team__r.Name, Brand_Lookup__r.Name, Line_2__c, Team_Instance__c, Cycle__c FROM Measure_Master__c WHERE Id =: ruleId LIMIT 1];
        //line = rule.Line_2__c;
        //brand = rule.Brand_Lookup__r.Name;
        list<AxtriaSalesIQTM__Team_Instance__c>tilist = [select id,name,AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c from AxtriaSalesIQTM__Team_Instance__c where name=:teamIns];
        businessUnitcyclee = tilist[0].AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c;
        teaminstancename = teamIns;
                SnTDMLSecurityUtil.printDebugMessage('==========tilist:::::'+tilist.size());
        if(tilist!=null && tilist.size()>0){
            businessUnit = tilist[0].id;
        }
        this.query  = 'SELECT Id, Share__c, AxtriaSalesIQTM__Account__c, P1_Original__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Name,allcall__c,Final_TCF__c, AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__c ';
        this.query += 'FROM AxtriaSalesIQTM__Position_Account_Call_Plan__c '; 
        this.query += 'WHERE AxtriaSalesIQTM__Position__c != null AND AxtriaSalesIQTM__isIncludedCallPlan__c = true and AxtriaSalesIQTM__Team_Instance__r.Name =:teaminstancename';
    }
    
    global BatchUpdateSharedFlag(String teamIns,String directLoad,String Ids){
        directFlag=true;
        this.Ids = Ids;
        customsetting = new AxtriaSalesIQTM__TriggerContol__c();
        customsetting2 = new AxtriaSalesIQTM__TriggerContol__c();
        customsettinglist = new list<AxtriaSalesIQTM__TriggerContol__c>();
        //ruleidis = '';
        teaminstancename ='';
        allcallflag =true;
        products = new set<string>();
        //ruleidis = ruleID;
        //Measure_Master__c rule = [SELECT Id, Team__r.Name, Brand_Lookup__r.Name, Line_2__c, Team_Instance__c, Cycle__c FROM Measure_Master__c WHERE Id =: ruleId LIMIT 1];
        //line = rule.Line_2__c;
        //brand = rule.Brand_Lookup__r.Name;
        list<AxtriaSalesIQTM__Team_Instance__c>tilist = [select id,name,AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c from AxtriaSalesIQTM__Team_Instance__c where name=:teamIns];
        businessUnitcyclee = tilist[0].AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c;
        teaminstancename = teamIns;
                SnTDMLSecurityUtil.printDebugMessage('==========tilist:::::'+tilist.size());
        if(tilist!=null && tilist.size()>0){
            businessUnit = tilist[0].id;
        }
        this.query  = 'SELECT Id, Share__c, AxtriaSalesIQTM__Account__c, P1_Original__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Name,allcall__c,Final_TCF__c, AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__c ';
        this.query += 'FROM AxtriaSalesIQTM__Position_Account_Call_Plan__c '; 
        this.query += 'WHERE AxtriaSalesIQTM__Position__c != null AND AxtriaSalesIQTM__isIncludedCallPlan__c = true and AxtriaSalesIQTM__Team_Instance__r.Name =:teaminstancename';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, list<Sobject> scope) 
    {
       /* map<String, AxtriaSalesIQTM__Position_Account_Call_Plan__c> physician2callplan = new map<String, AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        set<String> callPlans = new set<String>();
        for(Sobject cp: scope){
            physician2callplan.put((String)cp.get('AxtriaSalesIQTM__Account__c'), (AxtriaSalesIQTM__Position_Account_Call_Plan__c)cp);
            callPlans.add((String)cp.get('Id'));
        }
        
        list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> callplan2Update = new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        set<String> checkDuplicate = new set<String>();
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c cp: [SELECT Id, AxtriaSalesIQTM__Account__c, Share__c FROM AxtriaSalesIQTM__Position_Account_Call_Plan__c WHERE AxtriaSalesIQTM__Account__c IN: physician2callplan.keySet() AND AxtriaSalesIQTM__Team_Instance__r.Name =: businessUnit AND AxtriaSalesIQTM__Position__c != null  AND AxtriaSalesIQTM__isIncludedCallPlan__c = true]){
            if(physician2callplan.containsKey(cp.AxtriaSalesIQTM__Account__c)){
                cp.Share__c = true;
                AxtriaSalesIQTM__Position_Account_Call_Plan__c cp2 = physician2callplan.get(cp.AxtriaSalesIQTM__Account__c);
                cp2.Share__c = true;
                callplan2Update.add(cp);
                if(!checkDuplicate.contains((String)cp2.get('Id'))){
                    callplan2Update.add(cp2);
                    checkDuplicate.add((String)cp2.get('Id'));
                }
            }
        }
        
        if(callplan2Update != null && callplan2Update.size() > 0){
            CallPlanSummaryTriggerHandler.execute_trigger = false; //Using variable to stop from executing trigger while running this batch.
            update callplan2Update;
            CallPlanSummaryTriggerHandler.execute_trigger = true; //Using variable to start from executing trigger after runnig the batch update.
        }*/

        try
        {
            customsetting = new AxtriaSalesIQTM__TriggerContol__c();
            customsetting2 = new AxtriaSalesIQTM__TriggerContol__c();
            customsettinglist = new list<AxtriaSalesIQTM__TriggerContol__c>();
            Set<String> allAccountNumbers = new Set<String>();
            
            Set<String> allProducts = new Set<String>();
            Set<String> uniqueKey = new Set<String>();
            string cyclename ='';
            List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> sharedPacp = new List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();


            SnTDMLSecurityUtil.printDebugMessage('======Execute method allcallflag::::::'+allcallflag);
            if(allcallflag){
                SnTDMLSecurityUtil.printDebugMessage('=========All Call Flag is true::::'+allcallflag);
                for(Sobject cp: scope){
                    allAccountNumbers.add((String)cp.get('AxtriaSalesIQTM__Account__c'));
                    allProducts.add((String)cp.get('P1_Original__c'));
                    String unKey = (String)cp.get('AxtriaSalesIQTM__Account__c') + '_' + (String)cp.get('P1_Original__c');
                    uniqueKey.add(unKey);
                    //cyclename=(String)cp.get('Cycle_Name__c');
                }
                products.addall(allProducts);
                List<Account> allAccounts = [select id, (select id, AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c, P1_Original__c, Share__c,Final_TCF__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.name,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position_Account_CallPlans__r where AxtriaSalesIQTM__Position__c != null and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c =:businessUnitcyclee AND AxtriaSalesIQTM__isIncludedCallPlan__c = true) from Account where id in :allAccountNumbers];
                SnTDMLSecurityUtil.printDebugMessage('===============allAccounts.size():::::'+allAccounts.size());
                List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacpRec;
                //List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> sharedPacp = new List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
                for(Account acc : allAccounts)
                {
                    SnTDMLSecurityUtil.printDebugMessage('===========ACC For sharedpacpmap::'+acc);
                    pacpRec = acc.AxtriaSalesIQTM__Position_Account_CallPlans__r;
                    SnTDMLSecurityUtil.printDebugMessage('==============pacpRec::::'+pacpRec);
                    /*if(pacpRec.size()>1)
                    {
                        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : pacpRec)
                        {
                            String unKey = pacp.AxtriaSalesIQTM__Account__c + '_' + pacp.P1_Original__c;

                            if(uniqueKey.contains(unKey))
                            {
                                pacp.Share__c = true;
                                sharedPacp.add(pacp);                        
                            }
                            else
                            {
                                pacp.Share__c = false;
                                sharedPacp.add(pacp);                           
                            }
                        }
                    }
                    else
                    {
                        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : pacpRec)
                        {
                            pacp.Share__c = false;
                            sharedPacp.add(pacp);
                        }
                    }*/

                    //Added by Ayushi
                    Set<String> duplicateKeySet = new Set<String>();
                    Set<String> uniqueKeySet = new Set<String>();

                    for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : pacpRec)
                    {
                        String uniqueKeyValue = pacp.AxtriaSalesIQTM__Account__c + '_' + pacp.P1_Original__c;
                        if(!uniqueKeySet.contains(uniqueKeyValue))
                        {
                            uniqueKeySet.add(uniqueKeyValue);
                            
                        }
                        else
                        {
                            duplicateKeySet.add(uniqueKeyValue);   
                        }
                        SnTDMLSecurityUtil.printDebugMessage('duplicateKeySet :::::::::::' +duplicateKeySet);
                        SnTDMLSecurityUtil.printDebugMessage('uniqueKeySet :::::::::::' +uniqueKeySet);
                    }

                    //Updating Shared Flag
		     
                    integer counter = 0;
                    for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : pacpRec)
                    {
                        String unKey = pacp.AxtriaSalesIQTM__Account__c + '_' + pacp.P1_Original__c;
                        SnTDMLSecurityUtil.printDebugMessage('unKey :::::::::::' +unKey);
			
                        if(duplicateKeySet.contains(unKey))
                        {
                            pacp.Share__c = true;
                            sharedPacp.add(pacp);                        
                        }
                        else
                        {
                            pacp.Share__c = false;
                            sharedPacp.add(pacp);                           
                        }
                    }

                    //till here...

                }
                SnTDMLSecurityUtil.printDebugMessage('=====1111111111=======sharedPacp::'+sharedPacp);
                /* Method added by siva to update the shared hcps All Call Field on 9-08-2018*/
                map<string,integer> mapAccToTeamCall=new map<string,integer>();
                for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacpRec2 : sharedPacp)
                {
                    if(pacpRec2.Share__c){
                        String key = pacpRec2.AxtriaSalesIQTM__Account__c+'_'+pacpRec2.P1_Original__c;
                        SnTDMLSecurityUtil.printDebugMessage('============Key inside shared hcp batch::'+key);
                        SnTDMLSecurityUtil.printDebugMessage('========1111111111mapAccToTeamCall:::'+mapAccToTeamCall);
                        if(!mapAccToTeamCall.containsKey(key)){
                            if(pacpRec2.Final_TCF__c!=null) 
                                mapAccToTeamCall.put(key,integer.valueof(pacpRec2.Final_TCF__c));
                        }
                        else
                        {
                            if(mapAccToTeamCall.containsKey(key)){
                                SnTDMLSecurityUtil.printDebugMessage('=========INSIDE contains block::');
                                integer temp;
                                if(pacpRec2.Final_TCF__c!=null){
                                    temp=mapAccToTeamCall.get(key)+integer.valueof(pacpRec2.Final_TCF__c);

                                    //SnTDMLSecurityUtil.printDebugMessage('temp++++++++++++'+temp);
                                    mapAccToTeamCall.put(key,temp);
                                }
                            }
                        }
                    }
                }

                SnTDMLSecurityUtil.printDebugMessage('========Final ---mapAccToTeamCall:::'+mapAccToTeamCall);
                SnTDMLSecurityUtil.printDebugMessage('=========sharedPacp SIZE()==='+sharedPacp.size());
                for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacpRec3 : sharedPacp){
                    if(pacpRec3.Share__c){
                        String key = pacpRec3.AxtriaSalesIQTM__Account__c+'_'+pacpRec3.P1_Original__c;
                        if(mapAccToTeamCall.containsKey(key)){
                            SnTDMLSecurityUtil.printDebugMessage('========Final ---mapAccToTeamCall.get(key):::'+mapAccToTeamCall.get(key));
                            pacpRec3.allcall__c = mapAccToTeamCall.get(key) !=null ? mapAccToTeamCall.get(key) :0;
                        }

                    }
                    else{
                        SnTDMLSecurityUtil.printDebugMessage('==============INSIDE NON SHARED BLOCK FILLING ALL CALL BY FINAL TCF');
                        pacpRec3.allcall__c = pacpRec3.Final_TCF__c;
                    }
                }
            
            }//end of if
            else
            {
                SnTDMLSecurityUtil.printDebugMessage('=========All Call Flag is false::::'+allcallflag);
                for(Sobject cp: scope)
                {
                    allAccountNumbers.add((String)cp.get('AxtriaSalesIQTM__Account__c'));
                    allProducts.add((String)cp.get('P2__c'));
                    String unKey = (String)cp.get('AxtriaSalesIQTM__Account__c') + '_' + (String)cp.get('P2__c');
                    uniqueKey.add(unKey);
                    //cyclename=(String)cp.get('Cycle_Name__c');
                }
            
                SnTDMLSecurityUtil.printDebugMessage('=========allAccountNumbers:::====='+allAccountNumbers);
                SnTDMLSecurityUtil.printDebugMessage('===========businessUnit:::::::'+businessUnit);
                List<Account> allAccounts = [select id, (select id, AxtriaSalesIQTM__Account__c, P1_Original__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c,P2__c, Allcall__c,Share__c,Allcall2__c,TCF_P2__c,Final_TCF__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.name,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position_Account_CallPlans__r where AxtriaSalesIQTM__Position__c != null and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c =:businessUnitcycle AND AxtriaSalesIQTM__isIncludedCallPlan__c = true) from Account where id in :allAccountNumbers];
                SnTDMLSecurityUtil.printDebugMessage('===============allAccounts.size():::::'+allAccounts.size());
                List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacpRec;
                
                
                for(Account acc : allAccounts)
                {
                    SnTDMLSecurityUtil.printDebugMessage('===========ACC For sharedpacpmap::'+acc);
                    pacpRec = acc.AxtriaSalesIQTM__Position_Account_CallPlans__r;
                    SnTDMLSecurityUtil.printDebugMessage('==============pacpRec::::'+pacpRec);
                    if(pacpRec.size()>1)
                    {
                        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : pacpRec)
                        {
                            String unKey = pacp.AxtriaSalesIQTM__Account__c + '_' + pacp.P2__c;

                            if(uniqueKey.contains(unKey))
                            {
                                pacp.Share__c = true;
                                sharedPacp.add(pacp);                        
                            }
                            // else
                            // {
                            //     pacp.Share__c = false;
                            //     sharedPacp.add(pacp);                           
                            // }
                        }
                    }
                    else
                    {
                        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : pacpRec)
                        {
                            pacp.Share__c = false;
                            sharedPacp.add(pacp);
                        }
                    }
                }
                SnTDMLSecurityUtil.printDebugMessage('=====1111111111=======sharedPacp::'+sharedPacp);
                /* Method added by siva to update the shared hcps All Call Field on 9-08-2018*/
                map<string,integer> mapAccToTeamCall=new map<string,integer>();
                for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacpRec2 : sharedPacp){
                    if(pacpRec2.Share__c){
                        String key = pacpRec2.AxtriaSalesIQTM__Account__c+'_'+pacpRec2.P2__c;
                        SnTDMLSecurityUtil.printDebugMessage('============Key inside shared hcp batch::'+key);
                        SnTDMLSecurityUtil.printDebugMessage('========1111111111mapAccToTeamCall:::'+mapAccToTeamCall);
                        if(!mapAccToTeamCall.containsKey(key)){
                            if(pacpRec2.TCF_P2__c!=null) 
                                mapAccToTeamCall.put(key,integer.valueof(pacpRec2.TCF_P2__c));
                        }
                        else
                        {
                            if(mapAccToTeamCall.containsKey(key)){
                                SnTDMLSecurityUtil.printDebugMessage('=========INSIDE contains block::');
                                integer temp;
                                if(pacpRec2.TCF_P2__c!=null){
                                    temp=mapAccToTeamCall.get(key)+integer.valueof(pacpRec2.TCF_P2__c);

                                    //SnTDMLSecurityUtil.printDebugMessage('temp++++++++++++'+temp);
                                    mapAccToTeamCall.put(key,temp);
                                }
                            }
                        }
                    }
                }

                SnTDMLSecurityUtil.printDebugMessage('========Final ---mapAccToTeamCall:::'+mapAccToTeamCall);
                SnTDMLSecurityUtil.printDebugMessage('=========sharedPacp SIZE()==='+sharedPacp.size());
                for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacpRec3 : sharedPacp)
                {   
                    Decimal allcall = pacpRec3.Allcall__c;
                    SnTDMLSecurityUtil.printDebugMessage('======All calls Allcall__c::'+allcall);
                    if(pacpRec3.Share__c){
                        String key = pacpRec3.AxtriaSalesIQTM__Account__c+'_'+pacpRec3.P2__c;
                        if(mapAccToTeamCall.containsKey(key)){
                            SnTDMLSecurityUtil.printDebugMessage('========Final ---mapAccToTeamCall.get(key):::'+mapAccToTeamCall.get(key));
                            pacpRec3.Allcall2__c = mapAccToTeamCall.get(key) !=null ? mapAccToTeamCall.get(key) :0;
                        }
                        pacpRec3.allcall__c = allcall;
                    }
                    else{
                        SnTDMLSecurityUtil.printDebugMessage('==============INSIDE NON SHARED BLOCK FILLING ALL CALL BY FINAL TCF');
                        pacpRec3.Allcall2__c = pacpRec3.TCF_P2__c;
                        pacpRec3.allcall__c = allcall;
                    }
                    SnTDMLSecurityUtil.printDebugMessage('=========pacpRec3:::::'+pacpRec3.allcall__c);
                }
            /*END by siva*/

            }
            if(sharedPacp != null && sharedPacp.size() > 0){
                CallPlanSummaryTriggerHandler.execute_trigger = false; //Using variable to stop from executing trigger while running this batch.
                customsetting = AxtriaSalesIQTM__TriggerContol__c.getValues('ParentPacp');
                customsetting2 = AxtriaSalesIQTM__TriggerContol__c.getValues('CallPlanSummaryTrigger');
                SnTDMLSecurityUtil.printDebugMessage('==========customsetting========'+customsetting);
                customsetting.AxtriaSalesIQTM__IsStopTrigger__c = true ;
                customsetting2.AxtriaSalesIQTM__IsStopTrigger__c = true ;
                //update customsetting ;
                customsettinglist.add(customsetting);
                customsettinglist.add(customsetting2);
                update customsettinglist;
                
                //update sharedPacp;
                SnTDMLSecurityUtil.updateRecords(sharedPacp, 'BatchUpdateSharedFlag');
                customsettinglist = new list<AxtriaSalesIQTM__TriggerContol__c>();
                customsetting.AxtriaSalesIQTM__IsStopTrigger__c = customsetting.AxtriaSalesIQTM__IsStopTrigger__c ? false : customsetting.AxtriaSalesIQTM__IsStopTrigger__c;
                customsetting2.AxtriaSalesIQTM__IsStopTrigger__c = customsetting2.AxtriaSalesIQTM__IsStopTrigger__c ? false : customsetting2.AxtriaSalesIQTM__IsStopTrigger__c;
                //update customsetting ;
                customsettinglist.add(customsetting);
                customsettinglist.add(customsetting2);
                update customsettinglist;
           
                CallPlanSummaryTriggerHandler.execute_trigger = true; //Using variable to start from executing trigger after runnig the batch update.
            }
        }
        catch(Exception e)
        {
            //Added by HT(A0994) on 12th June 2020
            SnTDMLSecurityUtil.printDebugMessage('Error in BatchUpdateSharedFlag : execute()-->'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack Trace-->'+e.getStackTraceString());
            proceedNextBatch = false;
            SnTDMLSecurityUtil.printDebugMessage('callPlanLoggerID--'+callPlanLoggerID);

            if(callPlanLoggerID!=null && callPlanLoggerID!=''){
                SalesIQUtility.updateCallPlanLoggerRecord(callPlanLoggerID,'Failed at creating Cycle Plan data','Error',alignNmsp);
            }
            //End
        } 
    }
    
    global void finish(Database.BatchableContext BC) 
    {
        try
        {
            SnTDMLSecurityUtil.printDebugMessage('===========directFlag::::'+directFlag);
            if(directFlag==true)
            {
                /*commented due to object purge*/
                //GenerateSntReport.Directloadcallplan(teaminstancename,products);// added by siva on 02-01-2018
                if(callPlanLoggerID!=null && callPlanLoggerID!='')
                {
                    if(proceedNextBatch)
                        Database.executeBatch(new BatchCreateParentPacp(teaminstancename,callPlanLoggerID,alignNmsp), 500);
                }
                else{
                    Database.executeBatch(new BatchCreateParentPacp(teaminstancename), 500);
                }
            }
            else{
                SnTDMLSecurityUtil.printDebugMessage('========= Rule Driven Call============');
                /*commented due to object purge*/
                //GenerateSntReport.Ruledrivencallplan(ruleidis);// added by siva on 02-01-2018
                Database.executeBatch(new BatchCreateParentPacp(businessUnit,ruleidis), 500);  
            }
        }
        catch(Exception e)
        {
            //Added by HT(A0994) on 12th June 2020
            SnTDMLSecurityUtil.printDebugMessage('Error in BatchUpdateSharedFlag : finish()-->'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack Trace-->'+e.getStackTraceString());
            SnTDMLSecurityUtil.printDebugMessage('callPlanLoggerID--'+callPlanLoggerID);

            if(callPlanLoggerID!=null && callPlanLoggerID!=''){
                SalesIQUtility.updateCallPlanLoggerRecord(callPlanLoggerID,'Failed at updating Shared Flag for '+
                                                            'Cycle Plan data','Error',alignNmsp);
            }
            //End
        }
    }
}