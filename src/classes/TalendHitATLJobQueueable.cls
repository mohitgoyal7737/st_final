public class TalendHitATLJobQueueable implements Queueable, Database.AllowsCallouts {

	String endPoint;
    String sfdcPath;
    String axtriaUsername;
    String axtriaPassword;
    String veevaUsername;
    String veevaPassword;

    public void execute(QueueableContext context)
    {
    	system.debug('#### Hit Talend job for ATL Delta called ####');

    	endPoint='';
        sfdcPath='';
        axtriaUsername='';
        axtriaPassword='';
        veevaUsername='';
        veevaPassword='';   
        List<AxtriaSalesIQTM__ETL_Config__c> etlConfig = [Select Id, Name, AxtriaSalesIQTM__Server_Type__c, AxtriaSalesIQTM__End_Point__c,AxtriaSalesIQTM__SFTP_Username__c,AxtriaSalesIQTM__SF_UserName__c,AxtriaSalesIQTM__SF_Password__c,AxtriaSalesIQTM__SFTP_Password__c from AxtriaSalesIQTM__ETL_Config__c where Name = 'ATL Delta Job'];
        endPoint=etlConfig[0].AxtriaSalesIQTM__End_Point__c;
        sfdcPath=etlConfig[0].AxtriaSalesIQTM__Server_Type__c;
        axtriaUsername=etlConfig[0].AxtriaSalesIQTM__SF_UserName__c;
        axtriaPassword=etlConfig[0].AxtriaSalesIQTM__SF_Password__c;
        veevaUsername=etlConfig[0].AxtriaSalesIQTM__SFTP_Username__c;
        veevaPassword=etlConfig[0].AxtriaSalesIQTM__SFTP_Password__c;

        if(endPoint != '' && endPoint != null)
        {
        	string TalendEndpoint=endPoint;
			
			try{

	        	TalendEndpoint += '&arg0=--context_param%20AZ_ARSNT_EUFULL_SIQ_Username='+axtriaUsername;
            	TalendEndpoint += '&arg1=--context_param%20AZ_ARSNT_EUFULL_SIQ_Password='+axtriaPassword;
            	TalendEndpoint += '&arg2=--context_param%20AZ_Veeva_Username='+veevaUsername;
            	TalendEndpoint += '&arg3=--context_param%20AZ_Veeva_Password='+veevaPassword;
            	TalendEndpoint += '&arg4=--context_param%20AZ_ARSNT_EUFULL_SIQ_URL='+sfdcPath;
            	TalendEndpoint += '&arg5=--context_param%20AZ_Veeva_URL='+sfdcPath;
	            
	            
	            system.debug('#### TalendEndpoint : '+TalendEndpoint);

	            Http h = new Http();
	            HttpRequest request = new HttpRequest();
	            TalendEndpoint = TalendEndpoint.replaceAll( '\\s+', '%20');
	            request.setEndPoint(TalendEndpoint);
	            request.setHeader('Content-type', 'application/json');
	            request.setMethod('GET');
	            request.setTimeout(100000);
	            system.debug('request '+request);
	            HttpResponse response = h.send(request);
	            system.debug('#### Talend Delete Response : '+response);
	        }
	        catch(exception ex)
	        {
	            system.debug('Error in Talend job for ATL Delta Job' +ex.getMessage());
	            String subject = 'Error in Talend job for ATL Delta Job';
	            String body = 'Error while execution Talend Job for ATL Delta Job:::::  ' +ex.getMessage();
	            String[] address = new String[]{'Ayushi.Jain@Axtria.com'};
	            //String []ccAdd=new String[]{'AZ_ROW_SalesIQ_MktDeployment@Axtria.com'};
	            Messaging.SingleEmailMessage emailwithattch = new Messaging.SingleEmailMessage();
	            emailwithattch.setSubject(subject);
	            emailwithattch.setToaddresses(address);
	            emailwithattch.setPlainTextBody(body);
	            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {emailwithattch});
	        }
	    }
	    else
	    {
	    	system.debug('Error in Talend job for ATL Delta Job due to End Point');
            String subject = 'Error in Talend job for ATL Delta Job due to End Point';
            String body = 'End Point Null for Full load';
            String[] address = new String[]{'Ayushi.Jain@Axtria.com'};
            //String []ccAdd=new String[]{'AZ_ROW_SalesIQ_MktDeployment@Axtria.com'};
            Messaging.SingleEmailMessage emailwithattch = new Messaging.SingleEmailMessage();
            emailwithattch.setSubject(subject);
            emailwithattch.setToaddresses(address);
            emailwithattch.setPlainTextBody(body);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {emailwithattch});
	    }
    }
    
}