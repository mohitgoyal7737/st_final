global with sharing class BatchPopulateSurveyDirectLoad implements Database.Batchable<sObject> 
{
    public String query;
    public String batchName;
    public String teamInstance;
    public String changeReqID;
    public String changeReqStatus;
    public String selectedLoadType;

    public Integer recsProcessed = 0;
    public Integer recsTotal = 0;
    public Integer recsFailed = 0;

    public Boolean flag = true;

    public List<AxtriaSalesIQTM__Change_Request__c> changeRequestCreation;
    public List<Staging_Survey_Data__c> surveyRecsDataList;
    public List<temp_Obj__c> processedTempObjList;

    //Added by HT(A0994) on 17th June 2020 for SNT-428
    global BatchPopulateSurveyDirectLoad(String selectedTeamIns,String loadType) 
    {
        batchName = 'BatchPopulateSurveyDirectLoad';
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : constructor invoked--');

        teamInstance = selectedTeamIns;
        selectedLoadType = loadType;
        SnTDMLSecurityUtil.printDebugMessage('teamInstance--'+teamInstance);
        SnTDMLSecurityUtil.printDebugMessage('selectedLoadType--'+selectedLoadType);

        this.query = 'select Id,Name,Sales_Cycle__c,SURVEY_ID__c,SURVEY_NAME__c,Team_Instance__c,'+
                     'Team__c,AccountNumber__c,Product_Code__c,Short_Question_Text1__c,Response1__c,'+
                     'Short_Question_Text2__c,Response2__c,Short_Question_Text3__c,Response3__c,'+
                     'Response4__c,Response5__c,Response6__c,Response7__c,'+
                     'Response9__c,Response10__c,Response8__c,Response11__c,Response12__c,'+
                     'Response13__c,Response14__c,Response15__c,Response19__c,Response16__c,'+
                     'Response17__c,Response18__c,Response20__c,Short_Question_Text4__c,'+
                     'Short_Question_Text5__c,Short_Question_Text21__c,Short_Question_Text22__c,'+
                     'Short_Question_Text23__c,Short_Question_Text24__c,Short_Question_Text25__c,'+
                     'Short_Question_Text6__c, Short_Question_Text10__c, Short_Question_Text9__c,'+
                     'Short_Question_Text8__c, Short_Question_Text7__c, Short_Question_Text11__c,'+
                     'Short_Question_Text12__c,Short_Question_Text13__c,Short_Question_Text14__c,'+
                     'Short_Question_Text15__c,Short_Question_Text16__c,Short_Question_Text17__c,'+
                     'Short_Question_Text18__c,Short_Question_Text19__c,Short_Question_Text20__c,'+
                     'Response21__c,Response22__c,Response23__c,Response24__c,Response25__c,' +
                     'Type__c, Product_Name__c, Position_Code__c FROM temp_Obj__c where Status__c = \'New\'  and Object__c = \'Survey\' and '+
                     'Team_Instance_Text__c = \'' + teamInstance + '\' WITH SECURITY_ENFORCED'; 
    }

    //Added by HT(A0994) on 15th June 2020 for SNT-428
    global BatchPopulateSurveyDirectLoad(String selectedTeamIns,String crID,String loadType) 
    {
        batchName = 'BatchPopulateSurveyDirectLoad';
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : constructor invoked--');

        changeReqID = crID;
        teamInstance = selectedTeamIns;
        selectedLoadType = loadType;
        SnTDMLSecurityUtil.printDebugMessage('changeReqID--'+changeReqID);
        SnTDMLSecurityUtil.printDebugMessage('teamInstance--'+teamInstance);
        SnTDMLSecurityUtil.printDebugMessage('selectedLoadType--'+selectedLoadType);

        changeRequestCreation = new List<AxtriaSalesIQTM__Change_Request__c>();
        changeRequestCreation = [select id, AxtriaSalesIQTM__Request_Type_Change__c,
                                    Records_Created__c from AxtriaSalesIQTM__Change_Request__c 
                                    where id = :changeReqID WITH SECURITY_ENFORCED];

        this.query = 'select Name,SURVEY_ID__c,SURVEY_NAME__c,Team_Instance_Text__c,Type__c,'+
                     'AccountNumber__c,Product_Code__c,Short_Question_Text1__c,Response1__c,'+
                     'Short_Question_Text2__c,Response2__c,Short_Question_Text3__c,Response3__c,'+
                     'Response4__c,Response5__c,Response6__c,Response7__c,'+
                     'Response9__c,Response10__c,Response8__c,Response11__c,Response12__c,'+
                     'Response13__c,Response14__c,Response15__c,Response19__c,Response16__c,'+
                     'Response17__c,Response18__c,Response20__c,Short_Question_Text4__c,'+
                     'Short_Question_Text5__c,Short_Question_Text21__c,Short_Question_Text22__c,'+
                     'Short_Question_Text23__c,Short_Question_Text24__c,Short_Question_Text25__c,'+
                     'Short_Question_Text6__c, Short_Question_Text10__c, Short_Question_Text9__c,'+
                     'Short_Question_Text8__c, Short_Question_Text7__c, Short_Question_Text11__c,'+
                     'Short_Question_Text12__c,Short_Question_Text13__c,Short_Question_Text14__c,'+
                     'Short_Question_Text15__c,Short_Question_Text16__c,Short_Question_Text17__c,'+
                     'Short_Question_Text18__c,Short_Question_Text19__c,Short_Question_Text20__c,'+
                     'Response21__c,Response22__c,Response23__c,Response24__c,Response25__c ' +
                     'FROM temp_Obj__c where Change_Request__c = :changeReqID WITH SECURITY_ENFORCED'; 
    }

    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : start() invoked--');
        SnTDMLSecurityUtil.printDebugMessage('query--'+query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<temp_Obj__c> scope) 
    {
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : execute() invoked--');

        surveyRecsDataList = new List<Staging_Survey_Data__c>();
        processedTempObjList = new List<temp_Obj__c>();

        recsTotal+=scope.size();

        try
        {
            Decimal questionID;
            String questionIDString;
            Boolean isQuestionIDNumeric;
            Boolean tempRecFail;
			Set<String> prodCodeSet = new Set<String>();
            Set<String> teaminstanceSet = new Set<String>();
            Set<String> accSet = new Set<String>();
			
			for(temp_Obj__c tempObj : scope)   // SR-496
            {
                if(String.isBlank(tempObj.AccountNumber__c))
                {   
                    tempObj.status__c = 'Rejected';
                    tempObj.isError__c = true;
                    tempObj.Error_Message__c = 'Account Number not present/missing in Data';
                    tempObj.SalesIQ_Error_Message__c = 'Account Number not present/missing in Data';
                    processedTempObjList.add(tempObj);
                }
                else if(String.isBlank(tempObj.Product_Code__c))
                {   
                    tempObj.status__c = 'Rejected';
                    tempObj.isError__c = true;
                    tempObj.Error_Message__c = 'Product Code not present/missing in Data';
                    tempObj.SalesIQ_Error_Message__c = 'Product Code not present/missing in Data';
                    processedTempObjList.add(tempObj);
                }
                else if(String.isBlank(tempObj.Team_Instance_Text__c))
                {   
                    tempObj.status__c = 'Rejected';
                    tempObj.isError__c = true;
                    tempObj.Error_Message__c = 'Team Instance not present/missing in Data';
                    tempObj.SalesIQ_Error_Message__c = 'Team Instance not present/missing in Data';
                    processedTempObjList.add(tempObj);
                }
                
                else
                {   
                    accSet.add(tempObj.AccountNumber__c );
                    prodCodeSet.add(tempObj.Product_Code__c);
                    teaminstanceSet.add(tempObj.Team_Instance_Text__c);
                }
                
            }

			// Team Instance  SR-496
           List<AxtriaSalesIQTM__Team_Instance__c>teaminst1 = [select id,Name from AxtriaSalesIQTM__Team_Instance__c where Name=: teaminstanceSet];
           teaminstanceSet.clear();
            if(!teaminst1.isEmpty())
            {
                for(AxtriaSalesIQTM__Team_Instance__c Ti : teaminst1){
                    teaminstanceSet.add(Ti.Name);
                    system.debug(' Team instance  ----->'+teaminstanceSet);
                }
            }
          // Account number  SR-496
            List<Account> listAccs = [select id,AccountNumber from Account where AccountNumber in :accSet];
            accSet.clear();
            if(!listAccs.isEmpty())
            {
                for(Account acc : listAccs){
                  accSet.add(acc.AccountNumber);
                  system.debug(' Account Number ----->'+accSet);
                }
            }
             // Product code SR-496
             List<Product_Catalog__c> listPos = [select Product_Code__c,Team_Instance__r.name from Product_Catalog__c where Team_Instance__r.name =:teamInstance ];
             system.debug(' Team instance  ----->'+teamInstance); 
             prodCodeSet.clear();
            if(!listPos.isEmpty())
            {
                for(Product_Catalog__c pos : listPos){
                    prodCodeSet.add(pos.Product_Code__c);
                    system.debug(' Product code  ----->'+prodCodeSet);
                }
            }

			
			
			
			for(temp_Obj__c tempObj : scope)
            {
               // if(String.isNotBlank(tempObj.AccountNumber__c) && String.isNotBlank(tempObj.Product_Code__c)  && String.isNotBlank(tempObj.Short_Question_Text1__c))
			   
			   if(teaminstanceSet.size()>0 && teaminstanceSet.contains(tempObj.Team_Instance_Text__c) &&  accSet.size()>0 
                && accSet.contains(tempObj.AccountNumber__c) && prodCodeSet.size()>0 && prodCodeSet.contains(tempObj.Product_Code__c)  
                && String.isNotBlank(tempObj.SURVEY_NAME__c))
                {  // SR-496
                    Staging_Survey_Data__c scp = new Staging_Survey_Data__c();
                    scp.Name  = tempObj.Name;
                    scp.SURVEY_ID__c  = tempObj.SURVEY_ID__c;
                    scp.SURVEY_NAME__c  = tempObj.SURVEY_NAME__c;

                    scp.Team_Instance__c  = tempObj.Team_Instance_Text__c;
                    scp.Account_Number__c  = tempObj.AccountNumber__c;
                    scp.Product_Code__c  = tempObj.Product_Code__c;
                    scp.Type__c  = tempObj.Type__c;

                    for(Integer i=1;i<=25;i++)
                    {
                        scp.put('Response'+i+'__c',tempObj.get('Response'+i+'__c'));
                        /*if(tempObj.get('Question_ID'+i+'__c')!=null)
                        {
                            questionIDString = (String)tempObj.get('Question_ID'+i+'__c');
                            if(questionIDString!=null && questionIDString!='')
                            {
                                if(isDecimal(questionIDString))
                                    scp.put('Question_ID'+i+'__c',Integer.valueOf(tempObj.get('Question_ID'+i+'__c')));
                            }
                        }*/
                        scp.put('Short_Question_Text'+i+'__c',tempObj.get('Short_Question_Text'+i+'__c'));
                    }
                    tempObj.status__c = 'Processed';
                    tempObj.isError__c = false;
                    processedTempObjList.add(tempObj);
                    surveyRecsDataList.add(scp);
                }
                else
                {
					if(!processedTempObjList.contains(tempObj)){
						tempObj.status__c = 'Rejected';
						tempObj.isError__c = true;
						tempObj.Error_Message__c = 'Mandatory field missing or Incorrect';
						tempObj.SalesIQ_Error_Message__c = 'Mandatory field missing or Incorrect';
						processedTempObjList.add(tempObj);
					}
                }
            }

            if(!surveyRecsDataList.isEmpty())
            {
                SnTDMLSecurityUtil.printDebugMessage('surveyRecsDataList--' + surveyRecsDataList);
                SnTDMLSecurityUtil.printDebugMessage('surveyRecsDataList size--' + surveyRecsDataList.size());

                Database.SaveResult[] ds = Database.insert(surveyRecsDataList, false);

                for(Database.SaveResult d : ds)
                {
                    if(d.isSuccess()){   
                        recsProcessed++;
                    }
                    else
                    {
                        flag = false;
                        for(Database.Error err : d.getErrors()) {
                            SnTDMLSecurityUtil.printDebugMessage('The following error has occurred.');                    
                            SnTDMLSecurityUtil.printDebugMessage(err.getStatusCode() + ': ' + err.getMessage());
                            SnTDMLSecurityUtil.printDebugMessage('Fields that affected this error: ' + err.getFields());
                        }
                    }
                }
            }

            if(!processedTempObjList.isEmpty())
            {
                SnTDMLSecurityUtil.printDebugMessage('processedTempObjList--'+processedTempObjList);
                SnTDMLSecurityUtil.printDebugMessage('processedTempObjList size--'+processedTempObjList.size());

                SObjectAccessDecision securityDecision = Security.stripInaccessible(AccessType.UPDATABLE,processedTempObjList);
                List<temp_Obj__c> tempList = securityDecision.getRecords();
                SnTDMLSecurityUtil.updateRecords(tempList,batchName);

                if(!securityDecision.getRemovedFields().isEmpty()){
                    throw new SnTException(securityDecision,'temp_Obj__c',SnTException.OperationType.U);
                }
            }
        }
        catch(Exception e)
        {
            SnTDMLSecurityUtil.printDebugMessage(batchName+' : Error in execute()--'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack trace--'+e.getStackTraceString());
        }
    }

    /*public Boolean isDecimal(String str)
    {
        try
        {
            Decimal d = Decimal.valueOf(str);
            return true;
        }
        catch(Exception e)
        {
            return false;
        }
    }*/

    global void finish(Database.BatchableContext BC) 
    {
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : constructor invoked--');

        Boolean noJobErrors;
        if(changeReqID != null && changeReqID!='')
        {
            AxtriaSalesIQTM__Change_Request__c changeRequest = new AxtriaSalesIQTM__Change_Request__c();
            changeRequest.Id = changeReqID;
            changeRequest.Records_Updated__c = recsProcessed;
            if(changeRequestCreation.size() > 0 && changeRequestCreation[0].AxtriaSalesIQTM__Request_Type_Change__c == 'Data Load Backend')
            {
                changeRequest.Records_Created__c = recsTotal;
            }
            SnTDMLSecurityUtil.updateRecords(changerequest,batchName);

            noJobErrors = ST_Utility.getJobStatus(BC.getJobId());
            changeReqStatus = flag && noJobErrors ? 'Done' : 'Error';
            //Direct_Load_Records_Status_Update.Direct_Load_Records_Status_Update(changeReqID,batchName);

            //Call Metadata definition batch
            if(flag && noJobErrors)
            {
                Delete_Metadata_Definition batchCall = new Delete_Metadata_Definition(teamInstance,selectedLoadType,changeReqID);
                Database.executeBatch(batchCall,500);
            }
            else
            {
                if(String.isNotBlank(changeReqID) && String.isNotBlank(changeReqStatus))
                {
                    BatchUpdateTempObjRecsCR batchCall = new BatchUpdateTempObjRecsCR(changeReqID,true,'Mandatory Field Missing or Incorrect Team Instance',changeReqStatus);
                    Database.executeBatch(batchCall,2000);
                }
            }
        }
        else
        {
            Delete_Metadata_Definition batchCall = new Delete_Metadata_Definition(teamInstance,selectedLoadType);
            Database.executeBatch(batchCall,500);
        }
    }
}