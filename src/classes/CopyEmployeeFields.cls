public class CopyEmployeeFields{
    public static AxtriaSalesIQTM__employee__c copyAllFields(CurrentPersonFeed__c ES , AxtriaSalesIQTM__employee__c insertEmployee){
                insertemployee.AddressCity__c = ES.AddressCity__c;
                insertemployee.AddressCountry__c = ES.AddressCountry__c;
                insertemployee.AddressLine1__c = ES.AddressLine1__c;
                insertemployee.AddressLine2__c = ES.AddressLine2__c;
                insertemployee.AddressPostalCode__c = ES.AddressPostalCode__c;
                insertemployee.AddressStateCode__c = ES.AddressStateCode__c;
                insertemployee.AssignmentStatusValue__c = ES.AssignmentStatusValue__c;
                insertemployee.Employee_Status__c = ES.AssignmentStatusValue__c;
                //insertemployee.AssociateOID__c = ES.AssociateOID__c;
                insertemployee.Department__c = ES.Department__c;
                insertemployee.Department_Code__c = ES.Department_Code__c;
                insertemployee.AxtriaSalesIQTM__Email__c = ES.Email__c;
                insertemployee.AxtriaSalesIQTM__Employee_ID__c = ES.PRID__c;
                insertemployee.FamilyName__c = ES.Last_Name__c;
                insertemployee.GivenName__c = ES.First_Name__c; 
                insertemployee.JobChangeReason__c = ES.JobChangeReason__c;
                insertemployee.JobCode__c = ES.JobCode__c;
                insertemployee.JobCodeName__c = ES.JobCodeName__c;
                insertemployee.LegalAddressCityName__c = ES.AddressCity__c;
                insertemployee.LegalAddressCountryCode__c = ES.AddressCountry__c;
                insertemployee.LegalAddressLine1__c = ES.AddressLine1__c;
                insertemployee.LegalAddressLine2__c = ES.AddressLine2__c;
                insertemployee.LegalAddressPostalCode__c = ES.AddressPostalCode__c;
                insertemployee.NickName__c = ES.NickName__c;
                insertemployee.AxtriaSalesIQTM__Original_Hire_Date__c = ES.OriginalHireDate__c;
                insertemployee.AxtriaSalesIQTM__Rehire_Date__c = ES.Rehire_Date__c;
                insertemployee.ReportingToWorkerName__c = ES.ReportingToWorkerName__c;
                insertemployee.ReportsToAssociateOID__c = ES.ReportsToAssociateOID__c;
                insertemployee.StateTerritory__c = ES.StateTerritory__c;
           //     insertemployee.TerminationDate__c = ES.TerminationDate__c;
                //insertemployee.WorkLocation__c = ES.WorkLocation__c;
                insertemployee.WorkPhone__c = ES.WorkPhone__c;
                insertemployee.AxtriaSalesIQTM__HR_Termination_Date__c= ES.TerminationDate__c;
                insertEmployee.AxtriaSalesIQTM__Last_Name__c=ES.Last_Name__c;
                insertEmployee.AxtriaSalesIQTM__FirstName__c=ES.First_Name__c;
                //insertEmployee.Dept_Change_Eff_Date__c=ES.DeptChangeEffDate__c;
                
                if(ES.OriginalHireDate__c != null && ES.TerminationDate__c != null && ES.TerminationDate__c < ES.OriginalHireDate__c){
                    insertemployee.AxtriaSalesIQTM__HR_Termination_Date__c = null;    
                }
                system.debug('====insertemployee==='+insertemployee);
                return insertemployee;      
    }
}