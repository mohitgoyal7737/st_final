global with sharing class BatchFillUniqueCountBUMM implements Database.Batchable<sObject>,Database.Stateful,Database.AllowsCallouts
{
    public String query;
    public String mmID;
    public String teamInstanceID;
    public Set<String> uniqueAcctsSet;
    public String mmStatus;
    public String ruleName;

    global BatchFillUniqueCountBUMM(String mmID,String teamInstanceID) {
        this.mmID = mmID;
        this.teamInstanceID = teamInstanceID;
        uniqueAcctsSet = new Set<String>();
        List<Measure_Master__c> mmList = [select Id,Name, Measure_Type__c from Measure_Master__c where id=:mmID WITH SECURITY_ENFORCED];
        if(mmList.size()>0 && mmList!=null){
            if(mmList[0].Measure_Type__c != 'None'){
                ruleName = mmList[0].Name;
                this.query = 'select id from Account where id in (select Physician__c '+
                'from BU_Response__c where Team_Instance__c =\''+teamInstanceID+'\') and Type = \'' + mmList[0].Measure_Type__c +'\' WITH SECURITY_ENFORCED';
            }
            else
            {
                ruleName = mmList[0].Name;
                this.query = 'select id from Account where id in (select Physician__c '+
                'from BU_Response__c where Team_Instance__c =\''+teamInstanceID+'\')';
            }
        }
    }

    global BatchFillUniqueCountBUMM(String mmID,String teamInstanceID,String brandID) {
        this.mmID = mmID;
        this.teamInstanceID = teamInstanceID;
        uniqueAcctsSet = new Set<String>();
        List<Measure_Master__c> mmList = [select Id,Name,Measure_Type__c from Measure_Master__c where id=:mmID WITH SECURITY_ENFORCED];
        if(mmList.size()>0 && mmList!=null){
            ruleName = mmList[0].Name;
            Measure_Master__c mm = new Measure_Master__c();
            mm.Id = mmList[0].id;
            mm.Download_Initiated__c = false;
            mm.IsContinueProcessing__c = false;
            SnTDMLSecurityUtil.updateRecords(mm, 'BatchFillUniqueCountBUMM');
            this.query = 'select id from Account where id in (select Physician__c '+
                      'from BU_Response__c where Team_Instance__c =\''+teamInstanceID+
                      '\' and Product__c = \''+brandID+'\') and Type  = \'' + mmList[0].Measure_Type__c +'\' WITH SECURITY_ENFORCED';
        }
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Account> accList) 
    {
        if(accList!=null && accList.size()>0)
        {
            for(Account acc : accList){
                uniqueAcctsSet.add(acc.id);
            }
        }
    }

    global void finish(Database.BatchableContext BC) 
    {
        List<Measure_Master__c> mmList = [select is_executed__c,State__c,Measure_Type__c,Brand_Lookup__c,Team_Instance__r.IsHCOSegmentationEnabled__c,Number_of_BU_Unique_Accounts__c,is_push_to_alignment_allowed__c,is_publish_allowed__c from Measure_Master__c 
                                        where id=:mmID WITH SECURITY_ENFORCED];

        if(mmList.size()>0 && mmList!=null)
        {
            Measure_Master__c mm = mmList[0];
            mm.Number_of_BU_Unique_Accounts__c = uniqueAcctsSet!=null ? uniqueAcctsSet.size() : 0;

            list<Measure_Master__c> oldRules;
            if(mm.Team_Instance__r.IsHCOSegmentationEnabled__c == true){
                //Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c
                oldRules = [SELECT Id FROM Measure_Master__c WHERE Team_Instance__c =: mm.Team_Instance__c  AND Brand_Lookup__c =: mm.Brand_Lookup__c AND is_complete__c = true AND State__c = 'Complete' AND Measure_Type__c =: mm.Measure_Type__c];//AND Line_2__c =: rule.Line_2__c
            }
            else{
                //Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c
                oldRules = [SELECT Id FROM Measure_Master__c WHERE Team_Instance__c =: mm.Team_Instance__c  AND Brand_Lookup__c =: mm.Brand_Lookup__c AND is_complete__c = true AND State__c = 'Complete'];//AND Line_2__c =: rule.Line_2__c
            }
            //mm.is_executed__c = true;
            if(oldRules != null && oldRules.size() > 0){
                mmStatus = 'Publish Not Allowed';
                //mm.State__c = 'Publish Not Allowed';
            }else{
                mmStatus = 'Executed';
                mm.is_push_to_alignment_allowed__c = true;//A1422
                mm.is_publish_allowed__c = true;//A1422
               // mm.State__c = 'Executed';
            }
            //update mm;
            mm.Download_Initiated__c = true;
            mm.IsContinueProcessing__c = false;
            SnTDMLSecurityUtil.updateRecords(mm, 'BatchFillUniqueCountBUMM');
            //createComparisonResultFile();
            System.enqueueJob(new AsyncCreateComparisonResultFile(mmStatus,ruleName,mmID));
        }

        

    }

    
}