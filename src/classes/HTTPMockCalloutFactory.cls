@isTest 
global class HTTPMockCalloutFactory implements HttpCalloutMock { 
    global HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse(); 
         System.assertEquals('http://13.66.200.148/ComparativeAnalysis/initParams?fileNameSource=F:/Bayer_SnT/ComparativeAnalysis/Data/Unmanaged%20QA//Data_For_a2P6g000000MjEoEAK.csv&fileNameDestination=F:/Bayer_SnT/ComparativeAnalysis/Data/Unmanaged%20QA/Data_For_a2P6g000000MjEnEAK.csv&sfdcUserName=sntproductdev@axtria.com.qa2&sfdcPassword=***********&sfdcSecurityToken=*************************&sourceRuleId=a2P6g000000MjEoEAK&destinationRuleId=a2P6g000000MjEnEAK&query=null&namespace=&objectName=Account_Compute_Final__c&action=Compare&segUniv=Survey%20Data%20Accounts', req.getEndpoint());
         //System.assertEquals('http://example.com/example/test', req.getEndpoint());

        System.assertEquals('GET', req.getMethod());

        res.setStatusCode(200); 
        res.setHeader('Content-type','application/json');
        res.setBody('{"example":"test"}'); 
        return res;
    }
}