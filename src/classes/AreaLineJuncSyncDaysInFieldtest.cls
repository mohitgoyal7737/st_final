@istest
public class AreaLineJuncSyncDaysInFieldtest 
{
    @istest static void AreaLineJuncSyncDaysInFieldtest()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        AxtriaSalesIQTM__TriggerContol__c ccd= new AxtriaSalesIQTM__TriggerContol__c();
        ccd.Name = 'UpdateCallcapacityCimconfig';
        ccd.AxtriaSalesIQTM__IsStopTrigger__c = false;
        insert ccd;
        AxtriaSalesIQTM__Team__c t=new AxtriaSalesIQTM__Team__c();
        t.Name='xx';
        insert t;
        AxtriaSalesIQTM__Organization_Master__c aom = new AxtriaSalesIQTM__Organization_Master__c(AxtriaSalesIQTM__Org_Level__c='Global',AxtriaSalesIQTM__Parent_Country_Level__c=true);
        insert aom;
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='Italy',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active');
        insert country;
        String teamId=t.id;
        AxtriaSalesIQTM__Team__c t1=new AxtriaSalesIQTM__Team__c();
        t1.Name='yy';
        insert t1;
        String parentteamId=t1.id;
        AxtriaSalesIQTM__Team_Instance__c ti=new AxtriaSalesIQTM__Team_Instance__c();
        ti.AxtriaSalesIQTM__Team__c=teamId;
        insert ti;
        String teamInstId=ti.id;
        AxtriaSalesIQTM__Position__c posNation = new AxtriaSalesIQTM__Position__c();
        posNation.AxtriaSalesIQTM__Position_Type__c = 'Nation';
        posNation.Name = 'Chico CA_SPEC';
        posNation.AxtriaSalesIQTM__Team_iD__c    = teamId;
        // posNation.AXTRIASALESIQTM__HIERARCHY_LEVEL__C='4';
        
        insert posNation;
        Product_Catalog__c pc = new Product_Catalog__c();
        pc.Name='BRILIQUE';
        pc.Team_Instance__c = teamInstId;
        pc.Veeva_External_ID__c ='ProdId';
        pc.Product_Code__c= 'ProdId';
        pc.IsActive__c=true;
        pc.Country_Lookup__c=country.id;
        insert pc;
        list<AxtriaSalesIQTM__Position_Product__c> poslist = new list<AxtriaSalesIQTM__Position_Product__c>();
        
        AxtriaSalesIQTM__Position_Product__c pos = new AxtriaSalesIQTM__Position_Product__c();
        pos.Business_Days_in_Cycle__c=5;
        pos.Calls_Day__c = 3;
        pos.AxtriaSalesIQTM__Position__c = posNation.id;
        pos.Product_Catalog__c = pc.id;
        
        pos.External_ID__c = posNation.id + '_' + pc.id;
        pos.AxtriaSalesIQTM__Effective_Start_Date__c = system.today();
        pos.AxtriaSalesIQTM__Effective_End_Date__c = system.today();
        pos.AxtriaSalesIQTM__Product_Weight__c = 3;
        insert pos;
        Area_Line_Junction__c area = new Area_Line_Junction__c();
        area.Area__c = posNation.id;
        area.Position_Product__c = pos.id;
        area.Team_Instance__c = teamInstId;
        insert area;
        
        poslist.add(pos);
        if(poslist!=null && poslist.size()>0){
            update poslist;
        }
        
        AxtriaSalesIQTM__Position_Team_Instance__c posteam = new AxtriaSalesIQTM__Position_Team_Instance__c();
        posteam.AxtriaSalesIQTM__Position_ID__c = pos.id;
        posteam.AxtriaSalesIQTM__Team_Instance_ID__c = ti.id;
        
        
        AxtriaSalesIQTM__CIM_Config__c cim = new AxtriaSalesIQTM__CIM_Config__c();
        cim.AxtriaSalesIQTM__Metric_Name__c = 'abdcg';
        cim.AxtriaSalesIQTM__Attribute_API_Name__c = 'iojk';
        cim.AxtriaSalesIQTM__Object_Name__c = 'thyunk';
        cim.IsCallCapacity__c = true;
        cim.AxtriaSalesIQTM__Team_Instance__c =ti.id;
        insert cim;
        
        AxtriaSalesIQTM__CIM_Position_Metric_Summary__c cpms = new AxtriaSalesIQTM__CIM_Position_Metric_Summary__c();
        cpms.AxtriaSalesIQTM__Team_Instance__c = ti.id;
        cpms.AxtriaSalesIQTM__CIM_Config__c = cim.id;
        cpms.AxtriaSalesIQTM__Position_Team_Instance__c = posteam.id;
        insert cpms;        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            
            AreaLineJuncSyncDaysInFieldTrgrHandler obj=new AreaLineJuncSyncDaysInFieldTrgrHandler();
            AreaLineJuncSyncDaysInFieldTrgrHandler.UpdateArealinejunction(poslist);
        }
        Test.stopTest();
    }
    
}