/*Author - Himanshu Tariyal(A0994)
Date : 16th January 2018*/
global with sharing class BatchDeleteRecsBeforereExecuteSS implements Database.Batchable<sObject> {
    public String query;
    public String ruleId;
    public String whereClause;

    global BatchDeleteRecsBeforereExecuteSS(String ruleId, String whereClause) 
    {       
        this.ruleId = ruleId;
        this.whereClause = whereClause;
        this.query = 'SELECT Id FROM Segment_Simulation__c WHERE Measure_Master__c = \''+ruleId+'\' WITH SECURITY_ENFORCED';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        try{
            return Database.getQueryLocator(query);
        }
        catch(System.QueryException qe){
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }
        return null;
    }

    global void execute(Database.BatchableContext BC, list<Sobject> scope) 
    {
        if(scope.size()>0 && scope!=null){
            if(Segment_Simulation__c.sObjectType.getDescribe().isDeletable()){
                Database.DeleteResult[] srList = Database.delete(scope, false);
            }
            else{
                SnTDMLSecurityUtil.printDebugMessage('You dont have permission to delete Segment_Simulation__c','BatchDeleteRecsBeforereExecuteSS');
            }
        }
    }

    global void finish(Database.BatchableContext BC) 
    {
        BatchDeleteRecsBeforereExecuteSSC batchExecute = new BatchDeleteRecsBeforereExecuteSSC(ruleId, WhereClause );
        Database.executeBatch(batchExecute, 2000);
        
    }
}