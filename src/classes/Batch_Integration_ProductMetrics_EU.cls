global with sharing class Batch_Integration_ProductMetrics_EU implements Database.Batchable<sObject>, Database.Stateful
{
    public String query;
    public List<SIQ_Product_Metrics_vod_O__c> allProductMetricsData;
    public Map<String, String> productToVeevaID;
    public List<Segment_Veeva_Mapping__c> segmentVeevaMapping;
    public Map<String, String> segmentVeevaMappingMap;
    public Map<String, String> AdoptionVeevaMappingMap;
    public Map<String, String> PotentialVeevaMappingMap;
    public Set<String> allStrs;
    public Map<String, String> detailGroupMap;

    public List<String> selectedTeamInstances;
    public Boolean chain = false;
    public String LOAD_TYPE = 'PM';
    public Map <String, List <String>> sourceToDestMapping = new Map <String, List <String>> ();

    global Batch_Integration_ProductMetrics_EU(List<String> selectedTeamInstances)
    {
        //this.query = 'select id, P1__c, AxtriaSalesIQTM__Segment10__c, Segment_Approved__c, AxtriaSalesIQTM__Account__r.AZ_VeevaID__c , AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__External_Account_Number__c, AxtriaSalesIQTM__Account__r.AccountNumber , Adoption__c, Potential__c, Country__c,AxtriaSalesIQTM__lastApprovedTarget__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,AxtriaSalesIQTM__Position__r.Country_Code_Formula__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Team_Instance__c in :selectedTeamInstances and P1__c != null  and AxtriaSalesIQTM__lastApprovedTarget__c = true';
        query = 'select id, P1__c, AxtriaSalesIQTM__Segment10__c, Segment_Approved__c, AxtriaSalesIQTM__Account__r.AZ_VeevaID__c , AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__External_Account_Number__c, AxtriaSalesIQTM__Account__r.AccountNumber , Adoption__c, Potential__c, Country__c,AxtriaSalesIQTM__lastApprovedTarget__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,AxtriaSalesIQTM__Position__r.Country_Code_Formula__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c,';

        Set<String> fields_in_query_set = new Set<String>();
        sourceToDestMapping = ST_utility.fetchSourceToDestinationMapping(LOAD_TYPE, selectedTeamInstances);
        String additionalFields = '';
        if(sourceToDestMapping.keySet().size() > 0)
        {
            fields_in_query_set = ST_utility.fetchFieldsUsedInSOQL(query);
        }
        additionalFields = ST_utility.fetchSourceFieldsInSourceToDestinationObject(sourceToDestMapping, fields_in_query_set);

        if(additionalFields != '')
        {
            query += additionalFields;
        }
        query = query.removeEnd(',');
        query += ' from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Team_Instance__c in :selectedTeamInstances and P1__c != null  and AxtriaSalesIQTM__lastApprovedTarget__c = true WITH SECURITY_ENFORCED';

        allStrs = new Set<String>();

        detailGroupMap = new Map<String, String>();
        this.selectedTeamInstances = new List<String>(selectedTeamInstances);
        allProductMetricsData = new List<SIQ_Product_Metrics_vod_O__c>();
        productToVeevaID = new Map<String, String>();

        segmentVeevaMapping = new List<Segment_Veeva_Mapping__c>();
        segmentVeevaMappingMap = new Map<String, String>();
        AdoptionVeevaMappingMap = new Map<String, String>();
        PotentialVeevaMappingMap = new Map<String, String>();


        segmentVeevaMapping = [select id, Segment_Axtria__c, Segment_Veeva__c, Adoption_Axtria__c, Adoption_Veeva__c, Potential_Axtria__c, Potential_Veeva__c, Team_Instance__c from Segment_Veeva_Mapping__c where Team_Instance__c in :selectedTeamInstances];

        for(Segment_Veeva_Mapping__c svm : segmentVeevaMapping)
        {
            segmentVeevaMappingMap.put(svm.Segment_Axtria__c + '_' + svm.Team_Instance__c, svm.Segment_Veeva__c);
            AdoptionVeevaMappingMap.put(svm.Adoption_Axtria__c + '_' + svm.Team_Instance__c, svm.Adoption_Veeva__c);
            PotentialVeevaMappingMap.put(svm.Potential_Axtria__c + '_' + svm.Team_Instance__c, svm.Potential_Veeva__c);
        }

        List<Product_Catalog__c> allProducts = [select id, Veeva_External_ID__c, External_ID__c, Country__c, Detail_Group__c,  Name from Product_Catalog__c where Team_Instance__c in :selectedTeamInstances and IsActive__c = true];

        for(Product_Catalog__c pc : allProducts)
        {
            productToVeevaID.put(pc.Name.toUpperCase() + '_' + pc.Country__c, pc.Veeva_External_ID__c);

            if(pc.Detail_Group__c != null)
                detailGroupMap.put(pc.Name + '_' + pc.Country__c, pc.Detail_Group__c);
        }
    }

    global Batch_Integration_ProductMetrics_EU(List<String> selectedTeamInstances, Boolean chaining)
    {
        chain = chaining;
        this.query = 'select id, P1__c, AxtriaSalesIQTM__Segment10__c, Segment_Approved__c, AxtriaSalesIQTM__Account__r.AZ_VeevaID__c , AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__External_Account_Number__c, AxtriaSalesIQTM__Account__r.AccountNumber , Adoption__c, Potential__c, Country__c,AxtriaSalesIQTM__lastApprovedTarget__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c,AxtriaSalesIQTM__Position__r.Country_Code_Formula__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,';

        Set<String> fields_in_query_set = new Set<String>();
        sourceToDestMapping = ST_utility.fetchSourceToDestinationMapping(LOAD_TYPE, selectedTeamInstances);
        String additionalFields = '';
        if(sourceToDestMapping.keySet().size() > 0)
        {
            fields_in_query_set = ST_utility.fetchFieldsUsedInSOQL(query);
        }
        additionalFields = ST_utility.fetchSourceFieldsInSourceToDestinationObject(sourceToDestMapping, fields_in_query_set);

        if(additionalFields != '')
        {
            query += additionalFields;
        }
        query = query.removeEnd(',');
        query += ' from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Team_Instance__c in :selectedTeamInstances and P1__c != null  and AxtriaSalesIQTM__lastApprovedTarget__c = true WITH SECURITY_ENFORCED';

        allStrs = new Set<String>();

        detailGroupMap = new Map<String, String>();
        this.selectedTeamInstances = new List<String>(selectedTeamInstances);
        allProductMetricsData = new List<SIQ_Product_Metrics_vod_O__c>();
        productToVeevaID = new Map<String, String>();

        segmentVeevaMapping = new List<Segment_Veeva_Mapping__c>();
        segmentVeevaMappingMap = new Map<String, String>();
        AdoptionVeevaMappingMap = new Map<String, String>();
        PotentialVeevaMappingMap = new Map<String, String>();

        segmentVeevaMapping = [select id, Segment_Axtria__c, Segment_Veeva__c, Adoption_Axtria__c, Adoption_Veeva__c, Potential_Axtria__c, Potential_Veeva__c, Team_Instance__c from Segment_Veeva_Mapping__c where Team_Instance__c in :selectedTeamInstances];

        for(Segment_Veeva_Mapping__c svm : segmentVeevaMapping)
        {
            segmentVeevaMappingMap.put(svm.Segment_Axtria__c + '_' + svm.Team_Instance__c, svm.Segment_Veeva__c);
            AdoptionVeevaMappingMap.put(svm.Adoption_Axtria__c + '_' + svm.Team_Instance__c, svm.Adoption_Veeva__c);
            PotentialVeevaMappingMap.put(svm.Potential_Axtria__c + '_' + svm.Team_Instance__c, svm.Potential_Veeva__c);
        }

        List<Product_Catalog__c> allProducts = [select id, Veeva_External_ID__c, External_ID__c, Country__c, Detail_Group__c,  Name from Product_Catalog__c where Team_Instance__c in :selectedTeamInstances and IsActive__c = true];

        for(Product_Catalog__c pc : allProducts)
        {
            productToVeevaID.put(pc.Name.toUpperCase() + '_' + pc.Country__c, pc.Veeva_External_ID__c);
            system.debug('++productToVeevaID' + productToVeevaID);

            if(pc.Detail_Group__c != null)
                detailGroupMap.put(pc.Name + '_' + pc.Country__c, pc.Detail_Group__c);
        }
    }

    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scope)
    {

        allProductMetricsData = new List<SIQ_Product_Metrics_vod_O__c>();
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : scope)
        {
            SIQ_Product_Metrics_vod_O__c spm = new SIQ_Product_Metrics_vod_O__c();
            spm.SIQ_Account_vod__c = pacp.AxtriaSalesIQTM__Account__r.AccountNumber;//pacp.AxtriaSalesIQTM__Account__r.AZ_VeevaID__c;
            spm.SIQ_Account_Number__c = pacp.AxtriaSalesIQTM__Account__r.AccountNumber;
            // spm.SIQ_Adoption_AZ__c = pacp.Adoption__c;
            // spm.SIQ_Potential_AZ__c = pacp.Potential__c;
            system.debug('+++++++++++++ pacp.P1__c ' + pacp.P1__c);
            system.debug('+++++++++++++ pacp.Country__c ' + pacp.Country__c);
            spm.SIQ_Products_vod__c = productToVeevaID.get(pacp.P1__c.toUpperCase() + '_' + pacp.Country__c);
            system.debug('============ ' + spm.SIQ_Products_vod__c);
            if(pacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c == 'No Cluster')
            {
                spm.SIQ_Country__c = pacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
            }
            else if(pacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c == 'SalesIQ Cluster')
            {
                spm.SIQ_Country__c = pacp.AxtriaSalesIQTM__Position__r.Country_Code_Formula__c;
            }
            else if (pacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c == 'Veeva Cluster')
            {
                spm.SIQ_Country__c = pacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c;
            }
            //spm.SIQ_Country__c = spm.SIQ_Products_vod__c.split('_')[2];
            spm.SIQ_Product_Name__c = pacp.P1__c;
            spm.Detail_Group__c = detailGroupMap.get(pacp.P1__c + '_' + pacp.Country__c);

            if(segmentVeevaMappingMap.containsKey(pacp.Segment_Approved__c + '_' + pacp.AxtriaSalesIQTM__Team_Instance__c))
            {
                spm.SIQ_Segment__c   = segmentVeevaMappingMap.get(pacp.Segment_Approved__c + '_' + pacp.AxtriaSalesIQTM__Team_Instance__c);
            }
            else
            {
                spm.SIQ_Segment__c   = pacp.Segment_Approved__c;
            }
            if(AdoptionVeevaMappingMap.containsKey(pacp.Adoption__c + '_' + pacp.AxtriaSalesIQTM__Team_Instance__c))
            {
                spm.SIQ_Adoption_AZ__c = AdoptionVeevaMappingMap.get(pacp.Adoption__c + '_' + pacp.AxtriaSalesIQTM__Team_Instance__c);
            }
            else
            {
                spm.SIQ_Adoption_AZ__c = pacp.Adoption__c;
            }

            if(PotentialVeevaMappingMap.containsKey(pacp.Potential__c + '_' + pacp.AxtriaSalesIQTM__Team_Instance__c))
            {
                spm.SIQ_Potential_AZ__c = PotentialVeevaMappingMap.get(pacp.Potential__c + '_' + pacp.AxtriaSalesIQTM__Team_Instance__c);
            }
            else
            {
                spm.SIQ_Potential_AZ__c = pacp.Potential__c;
            }

            //segment blank when looser
            //inactive->deleted
            if(pacp.AxtriaSalesIQTM__lastApprovedTarget__c == false)
            {
                if(pacp.AxtriaSalesIQTM__Segment10__c == 'Inactive')
                {
                    spm.SIQ_Status__c = 'Deleted';
                }
                if(pacp.AxtriaSalesIQTM__Segment10__c == 'Loser Account')
                {
                    spm.SIQ_Segment__c   = '';
                    spm.SIQ_Status__c = 'Updated';
                }
            }

            if(spm.Detail_Group__c != null)
                spm.SIQ_External_ID__c = spm.SIQ_Country__c + '_' + pacp.AxtriaSalesIQTM__Account__r.AccountNumber + '_' + spm.SIQ_Products_vod__c + '_' + spm.Detail_Group__c;
            else
                spm.SIQ_External_ID__c = spm.SIQ_Country__c + '_' + pacp.AxtriaSalesIQTM__Account__r.AccountNumber + '_' + spm.SIQ_Products_vod__c + '_';
            if(!allStrs.contains(spm.SIQ_External_ID__c) && spm.SIQ_Product_Name__c != null)
            {
                allProductMetricsData.add(spm);
                allStrs.add(spm.SIQ_External_ID__c);
            }
            /*
            if(detailGroupMap.containsKey(pacp.P1__c + '_' + pacp.Country__c))
            {
                spm.SIQ_Products_vod__c =  detailGroupMap.get(pacp.P1__c + '_' + pacp.Country__c);
                spm.SIQ_External_ID__c = spm.SIQ_Country__c +'_'+ spm.SIQ_Account_vod__c +'_'+ spm.SIQ_Products_vod__c;

                if(!allStrs.contains(spm.SIQ_External_ID__c))
                {
                    allProductMetricsData.add(spm);
                    allStrs.add(spm.SIQ_External_ID__c);
                }
            }*/
            try
            {
                for(String source_field : sourceToDestMapping.keySet())
                {
                    for(String dest_field : sourceToDestMapping.get(source_field))
                    {
                        String src_field = '', teamIns = '' ;
                        List<String> src_field_list = new List<String>();
                        if(String.isNotBlank(source_field))
                        {
                            src_field = source_field.split('-')[0];
                            teamIns = source_field.split('-')[1];
                            if(String.isNotBlank(src_field))
                            {
                                src_field_list = src_field.split('\\.');
                            }
                        }
                        if(teamIns == pacp.AxtriaSalesIQTM__Team_Instance__c)
                        {

                            if(src_field_list.size() == 1)
                                spm.put(dest_field, pacp.get(src_field));
                            else if(src_field_list.size() == 2)
                            {
                                spm.put(dest_field, pacp.getSObject(src_field_list[0]).get(src_field_list[1]));
                            }
                            else if(src_field_list.size() == 3)
                            {
                                spm.put(dest_field, pacp.getSObject(src_field_list[0]).getSObject(src_field_list[1]).get(src_field_list[2]));
                            }
                            else if(src_field_list.size() == 4)
                            {
                                spm.put(dest_field, pacp.getSObject(src_field_list[0]).getSObject(src_field_list[1]).getSObject(src_field_list[2]).get(src_field_list[3]));
                            }


                            SnTDMLSecurityUtil.printDebugMessage('dest field' + dest_field);
                            //SnTDMLSecurityUtil.printDebugMessage('source field value'+pacp.get(src_field));
                        }


                    }
                }
            }
            catch(Exception e)
            {
                SnTDMLSecurityUtil.printDebugMessage('Error in mapping ' + e.getMessage());

            }
        }

        upsert allProductMetricsData SIQ_External_ID__c;
    }


    global void finish(Database.BatchableContext BC)
    {
        if(chain)
        {
            list<string> teaminstancelistis = new list<string>();
            teaminstancelistis = StaticTeaminstanceList.getFullLoadTeamInstances();
            database.executeBatch(new Refresh_My_Setup_Products(teaminstancelistis, chain), 2000);
        }
    }
}