global class update_PositionGeography implements Database.Batchable<sObject>, Database.Stateful  {
    
    global string query;
    global string teamID;
    global string teamInstance;
    global list<AxtriaSalesIQTM__Team_Instance__c> teminslst=new list<AxtriaSalesIQTM__Team_Instance__c>();
    global list<AxtriaSalesIQTM__Position_Team_Instance__c> posTI=new list<AxtriaSalesIQTM__Position_Team_Instance__c>();
    global list<AxtriaSalesIQTM__Team_Instance_Geography__c> geoTI=new list<AxtriaSalesIQTM__Team_Instance_Geography__c>();
    global list<temp_Zip_Terr__c> zipTerrlist=new list<temp_Zip_Terr__c>();
    
    
    global update_PositionGeography(String Team,String TeamIns){
      /*  teamInstance=TeamIns;
        teamID=Team;
        teminslst=[select id,AxtriaSalesIQTM__IC_EffEndDate__c,AxtriaSalesIQTM__IC_EffstartDate__c from AxtriaSalesIQTM__Team_Instance__c where id=:teamInstance];
        query='SELECT id,Geography__c,Position__c,Territory_ID__c,Zip_Name__c,Team_Name__c, Position__r.AxtriaSalesIQTM__inactive__c FROM temp_Zip_Terr__c where Team__c=:teamID and Team_Instance__c=:teamInstance and Status__c = \'New\' and Event__c = \'Insert\' ';*/
       

    }
    
    global Database.Querylocator start(Database.BatchableContext bc){
       /* AxtriaSalesIQTM__TriggerContol__c obj = [select id,AxtriaSalesIQTM__IsStopTrigger__c from AxtriaSalesIQTM__TriggerContol__c where name = 'PositionGeographyTrigger'];
        obj.AxtriaSalesIQTM__IsStopTrigger__c = true;
        update obj;*/
        return Database.getQueryLocator(query);
        
    } 
    
    global void execute (Database.BatchableContext BC, List<temp_Zip_Terr__c>zipTerrlist){
       
      /*  list<String> list1 = new list<String>();
        list<String> list2 = new list<String>();
         set<string> setPosGeoTeamInstance  = new set<string>();
        list<temp_Zip_Terr__c> lsTempRecordToUpdate = new list<temp_Zip_Terr__c>();
        
        for(temp_Zip_Terr__c rec:zipterrlist){
            if(rec.Geography__c != null){
              list1.add(rec.Geography__c);
            }
            if(rec.Position__c != null){
              list2.add(rec.Position__c);
            }
        }
        
         //query existing position Geography with the key = Position+Geography+TeamInstanceName
        list<AxtriaSalesIQTM__Position_Geography__c> lsposGeo = [select id,name,AxtriaSalesIQTM__Assignment_Status__c,AxtriaSalesIQTM__Geography__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Position__c from AxtriaSalesIQTM__Position_Geography__c where AxtriaSalesIQTM__Position__c in:list2 and AxtriaSalesIQTM__Geography__c in:list1 and AxtriaSalesIQTM__Assignment_Status__c = 'Active' and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__inactive__c != True]; 
        
        for(AxtriaSalesIQTM__Position_Geography__c pg : lsposGeo){
            setPosGeoTeamInstance.add(string.valueof(pg.AxtriaSalesIQTM__Position__c)+string.valueof(pg.AxtriaSalesIQTM__Geography__c)+string.valueof(pg.AxtriaSalesIQTM__Team_Instance__c));
        }
        
        map<string,string> posmap=new map<string,string>();
        for(AxtriaSalesIQTM__Position_Team_Instance__c posTI:[select AxtriaSalesIQTM__Position_ID__c,id from AxtriaSalesIQTM__Position_Team_Instance__c where AxtriaSalesIQTM__Position_ID__c in: list2 and  AxtriaSalesIQTM__Team_Instance_ID__c=:teamInstance])
        {
            posmap.put(posTI.AxtriaSalesIQTM__Position_ID__c,posTI.id);
        }
        map<string,string> geomap=new map<string,string>();
        for(AxtriaSalesIQTM__Team_Instance_Geography__c geoTI:[select AxtriaSalesIQTM__Geography__c,id from AxtriaSalesIQTM__Team_Instance_Geography__c where AxtriaSalesIQTM__Geography__c in :list1 and AxtriaSalesIQTM__Team_Instance__c=:teamInstance])
        {
            geomap.put(geoTI.AxtriaSalesIQTM__Geography__c,geoTI.id);
        }
       
        
        list<AxtriaSalesIQTM__Position_Geography__c> posgeoListUpdate = new list<AxtriaSalesIQTM__Position_Geography__c>();
        
        for(temp_Zip_Terr__c rec:zipTerrlist){
            if(rec.Geography__c != null && rec.Position__c !=null){
              if(!setPosGeoTeamInstance.contains(string.valueof(rec.Position__c)+string.valueof(rec.Geography__c)+string.valueof(teamInstance))){
                 if(rec.Position__r.AxtriaSalesIQTM__inactive__c != True){
                  AxtriaSalesIQTM__Position_Geography__c obj=new AxtriaSalesIQTM__Position_Geography__c();
                
                obj.AxtriaSalesIQTM__Geography__c                = rec.Geography__c;
                obj.AxtriaSalesIQTM__Position__c                 = rec.Position__c;
                obj.AxtriaSalesIQTM__Team_Instance__c            = teamInstance;
                obj.AxtriaSalesIQTM__Position_Team_Instance__c   = posmap.get(rec.Position__c);
                obj.AxtriaSalesIQTM__Effective_End_Date__c       = teminslst[0].AxtriaSalesIQTM__IC_EffEndDate__c;
                obj.AxtriaSalesIQTM__Effective_Start_Date__c     = teminslst[0].AxtriaSalesIQTM__IC_EffstartDate__c;
                obj.AxtriaSalesIQTM__Position_Id_External__c     = rec.Position__c;
                obj.AxtriaSalesIQTM__Change_Status__c            = 'No Change';
                obj.AxtriaSalesIQTM__Proposed_Position__c        = rec.Position__c;
                
                posgeoListUpdate.add(obj);
                //Update Temp Record
                rec.status__c = 'Processed';
                lsTempRecordToUpdate.add(rec);
                  }else{
                      rec.status__c = 'Rejected';
                      rec.Reason_Code__c = 'Inactive Position in SalesIQ';
                      lsTempRecordToUpdate.add(rec);
                  }
            }else{
                rec.status__c = 'Rejected';
                rec.Reason_Code__c = 'Overlapping Assignment';
                lsTempRecordToUpdate.add(rec);

            }
            }else{
                rec.status__c = 'Rejected';
                rec.Reason_Code__c = 'Geography/Position/Team Instance does not exist in SalesIQ';
                lsTempRecordToUpdate.add(rec);
            
            }   
            
        }
        
        if(posgeoListUpdate.size()!=0){     
            insert posgeoListUpdate;
        }
        if(lsTempRecordToUpdate.size()!=0){
            update lsTempRecordToUpdate;
        }
        
        system.debug('******************************upsert done************************************');*/
          
    }
    
          global void finish(Database.BatchableContext BC){
              
             // AxtriaSalesIQTM__TriggerContol__c obj1 = [select id,AxtriaSalesIQTM__IsStopTrigger__c from AxtriaSalesIQTM__TriggerContol__c where name = 'PositionGeographyTrigger'];
             // obj1.AxtriaSalesIQTM__IsStopTrigger__c = false;
             // update obj1;
             /*string selectedObject = 'temp_Zip_Terr__c';
             Database.executeBatch(new batchdelete3(teamID,selectedObject),1000);*/
       }
}