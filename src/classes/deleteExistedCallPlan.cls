global with sharing class deleteExistedCallPlan implements Database.Batchable<sObject>
{
    public String query;
    public String productName;
    public String teamInsLookup;
    public String rule;
    public String accountType;
    public boolean hcoEnabled;
    public List<String> listProduct;

    global deleteExistedCallPlan(String ruleId)
    {
        query = '';
        rule = ruleId;
        Set<String> pr_name_list = new Set<String>();

        listProduct = new List<String>();
        String productList= '';
        Boolean isPortfolio = false;


        List<Measure_Master__c> measureMasterList = [Select Id, Brand_Lookup__c, Brand_Lookup__r.Name, Brand_Lookup__r.is_Catalog__c,Brand_Lookup__r.Product_List__c, Team_Instance__c, Team_Instance__r.IsHCOSegmentationEnabled__c, Measure_Type__c from Measure_Master__c where id = :rule WITH SECURITY_ENFORCED];
        SnTDMLSecurityUtil.printDebugMessage('measureMasterList:::::::::::: ' + measureMasterList);

        productName = measureMasterList[0].Brand_Lookup__r.Name;
        teamInsLookup = measureMasterList[0].Team_Instance__c;
        accountType = measureMasterList[0].Measure_Type__c;
        hcoEnabled = measureMasterList[0].Team_Instance__r.IsHCOSegmentationEnabled__c;


        isPortfolio = measureMasterList[0].Brand_Lookup__r.is_Catalog__c;
        productList = measureMasterList[0].Brand_Lookup__r.Product_List__c;
        if(isPortfolio && productList!=null){
            listProduct =productList.split(';');
            List<Product_Catalog__c> prod = [Select Name from Product_Catalog__c where Veeva_External_ID__c in :listProduct WITH SECURITY_ENFORCED];
            for(Product_Catalog__c pr : prod){
                pr_name_list.add(pr.name);
            }
        }
        
        pr_name_list.add(productName);
        listProduct = new List<String>();
        listProduct.addAll(pr_name_list);
        


    

        SnTDMLSecurityUtil.printDebugMessage('productName in constructor:::::::::::: ' + productName);
        SnTDMLSecurityUtil.printDebugMessage('teamInsLookup in constructor:::::::::::: ' + teamInsLookup);

        if(hcoEnabled)
        {
            query = 'Select id from AxtriaSalesIQTM__Position_Account_Call_Plan__c where Rule__c = :rule or ((P1__c in :listProduct or P2__c in :listProduct or P3__c in :listProduct) and AxtriaSalesIQTM__Team_Instance__c = :teamInsLookup and (AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__AccountType__c =:accountType or AxtriaSalesIQTM__Account__r.Type = :accountType)) WITH SECURITY_ENFORCED';
        }
        else
        {
            query = 'Select id from AxtriaSalesIQTM__Position_Account_Call_Plan__c where  Rule__c = :rule or ((P1__c in :listProduct or P2__c in :listProduct or P3__c in :listProduct) and AxtriaSalesIQTM__Team_Instance__c = :teamInsLookup) WITH SECURITY_ENFORCED';

        }

    }

    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scope)
    {

        SnTDMLSecurityUtil.printDebugMessage('Query Size +++++++++++' + scope.size());
        SnTDMLSecurityUtil.printDebugMessage('Query+++++++++++' + scope);

        SnTDMLSecurityUtil.printDebugMessage('productName:::::::::::: ' + productName);
        SnTDMLSecurityUtil.printDebugMessage('teamInsLookup:::::::::::: ' + teamInsLookup);

        //delete scope;
        if(scope.size() > 0 && scope != null)
        {
            if(AxtriaSalesIQTM__Position_Account_Call_Plan__c.sObjectType.getDescribe().isDeletable())
            {
                Database.DeleteResult[] srList = Database.delete(scope, false);
            }
            else
            {
                SnTDMLSecurityUtil.printDebugMessage('You dont have permission to delete AxtriaSalesIQTM__Position_Account_Call_Plan__c', 'deleteExistedCallPlan');
            }
        }

    }

    global void finish(Database.BatchableContext BC)
    {

        BatchPublishCallPlan batchExecute = new BatchPublishCallPlan(rule, ' Measure_Master__c =\'' + rule + '\' ' );
        Database.executeBatch(batchExecute, 1500);

    }
}