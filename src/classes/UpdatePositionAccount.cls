global class UpdatePositionAccount implements Database.Batchable<sObject>, Database.Stateful,schedulable{
	 public Integer recordsProcessed=0;
     public String batchID;
     global DateTime lastjobDate=null;
     global String query;
     
     global UpdatePositionAccount(){//set<String> Accountid
     	
        /*List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
        schLogList=[Select Id,CreatedDate,Created_Date__c from Scheduler_Log__c where Job_Name__c='Position Account' and Job_Status__c='Successful' Order By Created_Date__c desc];
        if(schLogList.size()>0){
            lastjobDate=schLogList[0].CreatedDate;  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
	    else{
	    	lastjobDate=null;       //else we set the lastjobDate to null
  		}
  		System.debug('last job'+lastjobDate);
        //Last Bacth run ID
		Scheduler_Log__c sJob = new Scheduler_Log__c();
		
		sJob.Job_Name__c = 'Position Account';
		sJob.Job_Status__c = 'Failed';
		sJob.Job_Type__c='Outbound';
		//sJob.CreatedDate = System.today();
	
		insert sJob;
	    batchID = sJob.Id;
	   
	    recordsProcessed =0;
	   query = 'Select Country__c,AxtriaSalesIQTM__Effective_Start_Date__c,Event__c,AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, ' +
	   'AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Account__r.External_Account_Id__c, ' +
	    'CreatedDate,LastModifiedById,LastModifiedDate,OwnerId,SystemModstamp ' +
	    'FROM AxtriaSalesIQTM__Position_Account__c ' ;
	  
        
        if(lastjobDate!=null){
        	query = query + 'Where LastModifiedDate  >=:  lastjobDate '; 
        }
                System.debug('query'+ query);
		*/
	        
    }
    
    
 	global Database.QueryLocator  start(Database.BatchableContext bc) {
         return Database.getQueryLocator(query);
    }
     public void execute(System.SchedulableContext SC){
       
    }
     global void execute(Database.BatchableContext bc, List<AxtriaSalesIQTM__Position_Account__c> records){
        // process each batch of records
       
    /*
        List<SIQ_Position_Account_O__c> positionAccounts = new List<SIQ_Position_Account_O__c>();
        for (AxtriaSalesIQTM__Position_Account__c position : records) {
            //if(acc.SIQ_Parent_Account_Number__c!=''){
               SIQ_Position_Account_O__c positionAccount=new SIQ_Position_Account_O__c();
           
		        positionAccount.SIQ_Marketing_Code__c=position.Country__c;
				positionAccount.SIQ_Country_Code__c=position.Country__c;
				positionAccount.SIQ_Event__c=position.Event__c;
				positionAccount.SIQ_Salesforce_Name__c=position.AxtriaSalesIQTM__Team_Instance__r.Name;
				positionAccount.SIQ_CUSTOMER_ID__c=position.AxtriaSalesIQTM__Account__r.External_Account_Id__c;
				//positionAccount.SIQ_Customer_Class__c=
				positionAccount.SIQ_Team_Instance__c=position.AxtriaSalesIQTM__Team_Instance__c;
				positionAccount.SIQ_Created_Date__c=position.CreatedDate;
				positionAccount.SIQ_Updated_Date__c=position.LastModifiedDate;
				positionAccount.SIQ_POSITION_CODE__c=position.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
				positionAccount.SIQ_EFFECTIVE_START_DATE__c=position.AxtriaSalesIQTM__Effective_Start_Date__c;
				
				positionAccount.SIQ_EFFECTIVE_END_DATE__c=position.AxtriaSalesIQTM__Effective_End_Date__c;
                positionAccounts.add(positionAccount);
                system.debug('recordsProcessed+'+recordsProcessed);
                recordsProcessed++;
                //comments
            }
     //   }
     
        upsert positionAccounts;
       */
      
        
    }    
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
         /*System.debug(recordsProcessed + ' records processed. ');
                Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID); 
                system.debug('schedulerObj++++before'+sJob);
                //Update the scheduler log with successful
                sJob.No_Of_Records_Processed__c=recordsProcessed;
                sJob.Job_Status__c='Successful';
                system.debug('sJob++++++++'+sJob);
                update sJob;
        */
    }   
}