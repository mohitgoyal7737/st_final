@isTest
public class ComponentData_test{
	@istest static void  testClass()
	{
		ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
		String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
		List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
		System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
		ComponentData controller = new ComponentData() ;
		controller.Mode = 'View';
		controller.output = 'View';
		controller.rowCount = 1;
		controller.colCount = 3;
	}
}