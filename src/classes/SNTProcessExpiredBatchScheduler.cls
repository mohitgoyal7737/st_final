global class SNTProcessExpiredBatchScheduler implements Schedulable {
    public static Boolean Scheduleflag = false;

    global SNTProcessExpiredBatchScheduler()
    {
        System.debug('Calling batch');
        //database.executeBatch(new AxtriaSalesIQTM.UpdateEmployeeBatch, 2000);
        //AxtriaSalesIQTM.UpdateEmployeeBatch empBatch=new AxtriaSalesIQTM.UpdateEmployeeBatch();
        //database.executebatch(empBatch,2000);
        //database.executeBatch(new ProcessExpiredPositionBatchSNT, 2000);
        ProcessExpiredPositionBatchSNT posBatch=new ProcessExpiredPositionBatchSNT();
        database.executebatch(posBatch,2000);

        System.debug('===Calling outbound jobs===='+Scheduleflag);

        if(Scheduleflag){
            System.debug('--inside Contry wise outbound run--');

            if(AxtriaSalesIQTM__TriggerContol__c.getValues('OutboundJobsControl') != null)
            {
                if(!AxtriaSalesIQTM__TriggerContol__c.getValues('OutboundJobsControl').AxtriaSalesIQTM__IsStopTrigger__c)
                {
                    CutoverOutboundScheduleJobs.CountryWiseOutboundJobs();
                }
            }
        }
    }

    global void execute(SchedulableContext sc) 
    {
        Scheduleflag = true; 
        SNTProcessExpiredBatchScheduler obj = new SNTProcessExpiredBatchScheduler();
    }
    
}