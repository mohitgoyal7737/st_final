/*Deprecated due to Object purge activity on Jan 13th, 2020*/
@deprecated
global class UpdateAccountAddress implements Database.Batchable<sObject>, Database.Stateful,schedulable{
	 public Integer recordsProcessed=0;
     public String batchID;
     global DateTime lastjobDate=null;
     global String query;
     public map<String, String> mapCountryCode ;
     public set<String> AclList {get;set;}
     public map<String,String>Accnoid {get;set;}
     
     global UpdateAccountAddress(){//set<String> Accountid
     	
        /*List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
        schLogList=[Select Id,CreatedDate,Created_Date__c from Scheduler_Log__c where Job_Name__c='Address Delta' and Job_Status__c='Successful' Order By Created_Date__c desc];
        if(schLogList.size()>0){
            lastjobDate=schLogList[0].CreatedDate;  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
	    else{
	    	lastjobDate=null;       //else we set the lastjobDate to null
  		}
  		System.debug('last job'+lastjobDate);
        //Last Bacth run ID
		Scheduler_Log__c sJob = new Scheduler_Log__c();
		
		sJob.Job_Name__c = 'Address Delta';
		sJob.Job_Status__c = 'Failed';
		sJob.Job_Type__c='Inbound';
		//sJob.CreatedDate = System.today();
	
		insert sJob;
	    batchID = sJob.Id;
	    
	    recordsProcessed =0;
	   query ='SELECT ConnectionReceivedId,ConnectionSentId,CreatedById,CreatedDate,CurrencyIsoCode, ' + 
	    'Id,IsDeleted,LastModifiedById,LastModifiedDate,Name,OwnerId,SIQ_Account_Number__c,SIQ_Address_Id__c, ' +
	    'SIQ_Address_Type__c,SIQ_Billing_Address__c,SIQ_Billing_City__c,SIQ_Billing_Country__c, ' +
	    'SIQ_Billing_State_Province__c,SIQ_External_ID__c,SIQ_Geography_Identifier__c, ' +
	    'SIQ_Last_Modified_Date__c,SIQ_Latitude__c,SIQ_Longitude__c,SIQ_Mini_Brick__c, ' +
	    'SIQ_Postal_Code_in_Customer_Address__c,SIQ_Primary_Address_Indicator__c,SIQ_Publish_Date__c, ' +
	    'SIQ_Publish_Event__c,SIQ_Status__c,SystemModstamp ' +
	    'FROM SIQ_Account_Address__c ' ;
	  
        
        if(lastjobDate!=null){
        	query = query + 'Where LastModifiedDate  >=:  lastjobDate '; 
        }
                System.debug('query'+ query);
		
	      */  
    }
    
    
 	global Database.QueryLocator  start(Database.BatchableContext bc) {
         return Database.getQueryLocator(query);
    }
     public void execute(System.SchedulableContext SC){
       
    }
     global void execute(Database.BatchableContext bc, List<SIQ_Account_Address__c> records){
        // process each batch of records
     /*   AclList = new Set<String>();
        map<String,String>AccId = new Map<String,String>();
        for(SIQ_Account_Address__c SAA : records){
            AclList.add(SAA.SIQ_Account_Number__c);
           // AclList.add(SAA.SIQ_Parent_Account_Number__c);

        }
        List<Account>AccountList = [select id,External_Account_Number__c from Account where External_Account_Number__c IN :AclList]; 
        for(Account Acc : AccountList){
            if(!AccId.containsKey(Acc.External_Account_Number__c)){
                AccId.put(Acc.External_Account_Number__c,Acc.id);
            }
        }

        List<AxtriaSalesIQTM__Account_Address__c> addresses = new List<AxtriaSalesIQTM__Account_Address__c>();
        for (SIQ_Account_Address__c acc : records) {
            //if(acc.SIQ_Parent_Account_Number__c!=''){
               AxtriaSalesIQTM__Account_Address__c address=new AxtriaSalesIQTM__Account_Address__c();
           
             if(AccId.containsKey(acc.SIQ_Account_Number__c)){
                address.AxtriaSalesIQTM__Account__c=AccId.get(acc.SIQ_Account_Number__c);
                }
               address.Name =acc.Name;
               address.CurrencyIsoCode=acc.CurrencyIsoCode;
               address.Address_Id__c=acc.SIQ_Address_Id__c;
               address.AxtriaSalesIQTM__Address_Type__c=acc.SIQ_Address_Type__c;
               address.AxtriaSalesIQTM__City__c=acc.SIQ_Billing_City__c;
               address.AxtriaSalesIQTM__Country__c=acc.SIQ_Billing_Country__c;
               address.AxtriaSalesIQTM__Pincode__c=acc.SIQ_Postal_Code_in_Customer_Address__c;
               address.AxtriaSalesIQTM__State__c=acc.SIQ_Billing_State_Province__c;
               address.AxtriaSalesIQTM__Street__c=acc.SIQ_Billing_Address__c;
               address.External_Id__c=acc.SIQ_External_ID__c;
               address.Geography_Identifier__c=acc.SIQ_Geography_Identifier__c;
               address.Last_Modified_Date__c=acc.SIQ_Last_Modified_Date__c;
             //  address.AxtriaSalesIQTM__Latitude__c=decimal.valueOf(acc.SIQ_Latitude__c);
              // address.AxtriaSalesIQTM__Longitude__c =decimal.valueOf(acc.SIQ_Longitude__c);
               address.Mini_Brick__c =acc.SIQ_Mini_Brick__c;
               address.Primary_Address_Indicator__c =acc.SIQ_Primary_Address_Indicator__c;
               address.Publish_Date__c =acc.SIQ_Publish_Date__c;
               address.Publish_Event__c =acc.SIQ_Publish_Event__c;
               address.Status__c  =acc.SIQ_Status__c;
               addresses.add(address);
                system.debug('recordsProcessed+'+recordsProcessed);
                recordsProcessed++;
                //comments
            }
     //   }
     
        upsert addresses;
     */  
      
        
    }    
    global void finish(Database.BatchableContext bc){
     /*   // execute any post-processing operations
         System.debug(recordsProcessed + ' records processed. ');
                Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID); 
                system.debug('schedulerObj++++before'+sJob);
                //Update the scheduler log with successful
                sJob.No_Of_Records_Processed__c=recordsProcessed;
                sJob.Job_Status__c='Successful';
                system.debug('sJob++++++++'+sJob);
                update sJob;
     */   
    }   
}