@isTest
private class changeMCChannelStatusTest {


    static testMethod void testMethod3() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        insert scen;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        
        insert teamins;
        
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        
        
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__Client_Position_Code__c ='N053';
        insert pos;
        
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        
        positionAccountCallPlan.Segment10__c = 'Deleted';
        
        insert positionAccountCallPlan;
        
        Parent_PACP__c p = TestDataFactory.createParentPACP(teamins,acc,pos);
        
        insert p;
        SIQ_MC_Cycle_Plan_Target_vod_O__c calsum = TestDataFactory.createSIQ_MC_Cycle_Plan_Target_vod(teamins);
        calsum.Rec_Status__c ='Updated';
        calsum.SIQ_Target_vod__c = acc.AccountNumber;
        insert calsum;
        Veeva_Market_Specific__c v = TestDataFactory.createVeevaMarketSpecific();
        insert v;
        List<String> allChannelsTemp = new List<String> ();
        allChannelsTemp.add('F2F');
        allChannelsTemp.add('Email');
        allChannelsTemp.add('Meeting');
        List<String> teamInstanceSelectedTemp = new List<String> ();
        teamInstanceSelectedTemp.add(teamins.id);
        Set<String> activityLogSet = new Set<String>();
        activityLogSet.add('test');
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            changeMCChannelStatus obj=new changeMCChannelStatus(teamins.id,allChannelsTemp);  
            Database.executeBatch(obj);
            
        }
        Test.stopTest();
    }

}