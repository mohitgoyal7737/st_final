@isTest
private class test_VeevaOT2A {
public static String CRON_EXP = '0 0 0 28 2 ? 2022';
    static testMethod void validate_VeevaOT2A(){
    
    AxtriaSalesIQTM__SalesIQ_Logger__c DS = New AxtriaSalesIQTM__SalesIQ_Logger__c();
    DS.AxtriaSalesIQTM__Status__c = 'In Progress';
    DS.AxtriaSalesIQTM__Module__c = 'VeevaOT2A';
    DS.AxtriaSalesIQTM__Type__c = 'VeevaOT2A';
    Insert DS;
    
   
    AxtriaSalesIQTM__BRMS_Config__mdt BRMS_CS = new AxtriaSalesIQTM__BRMS_Config__mdt();
    BRMS_CS.MasterLabel = 'VeevaOT2A';
    BRMS_CS.NamespacePrefix = 'lll';
    BRMS_CS.AxtriaSalesIQTM__BRMS_Value__c = 'lllll';
    
    AxtriaSalesIQTM__ETL_Config__c  ETL = New AxtriaSalesIQTM__ETL_Config__c();
    ETL.Name = 'VeevaOT2A';
    ETL.AxtriaSalesIQTM__SF_UserName__c = 'Salesiq@123.com';
    ETL.AxtriaSalesIQTM__SF_Password__c = 'Salesiq@123.com';
    ETL.AxtriaSalesIQTM__S3_Security_Token__c= 'Salesiq@123.com';
    ETL.AxtriaSalesIQTM__BDT_DataSet_Id__c = 'EUfull';
    ETL.AxtriaSalesIQTM__BR_PG_Database__c= 'EU_IB_OB';
    ETL.AxtriaSalesIQTM__BR_PG_Host__c= 'localhost';
    Insert ETL;    
    
        
   VeevaOT2A.getBRMSConfigValues('VeevaOT2A');
   VeevaOT2A.getETLConfigByName('VeevaOT2A');
   VeevaOT2A.isSandboxOrg(); 
   Test.StartTest();

    String jobId = System.schedule('test_VeevaOT2A',CRON_EXP,new VeevaOT2A());

Test.StopTest(); 
    }
}