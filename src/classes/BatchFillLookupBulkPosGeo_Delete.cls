global class BatchFillLookupBulkPosGeo_Delete implements Database.Batchable<sObject>, Database.Stateful  {
     String query;
     String CountryName;
     String key;
     Map<String,String> KeyIdMap;
     Set<String> processedrecordSet;

    
     global BatchFillLookupBulkPosGeo_Delete(String Country){
         /* CountryName= Country;
   query = 'Select Id,Country__c,Event__c,Team_Instance_Name__c,Team_Name__c,Status__c,Territory_ID__c,Zip_Name__c from temp_Zip_Terr__c where Country__c =: CountryName and Event__c = \'Delete\' and Status__c = \'New\' ';*/
     }
    
     global Database.Querylocator start(Database.BatchableContext bc){
          return Database.getQueryLocator(query);
        
     } 
    
     global void execute (Database.BatchableContext BC, List<temp_Zip_Terr__c>scope){

         /* System.debug('Execute method');

          KeyIdMap = new Map<String,String>();
          processedrecordSet = new Set<String>();
          List<AxtriaSalesIQTM__Position_Geography__c> posGeoList = new  List<AxtriaSalesIQTM__Position_Geography__c>();
          List<AxtriaSalesIQTM__Position_Geography__c> futurePosGeoList = new  List<AxtriaSalesIQTM__Position_Geography__c>();


          for(temp_Zip_Terr__c posgeo : scope)
          {
               
               String key = posgeo.Territory_ID__c.toUpperCase()+'_'+posgeo.Zip_Name__c.toUpperCase()+'_'+posgeo.Team_Instance_Name__c.toUpperCase();
               KeyIdMap.put(key,posgeo.Id);
               System.debug('inside first loop :::::'+KeyIdMap);
          }

          for(AxtriaSalesIQTM__Position_Geography__c posGeoCore : [Select Id,Unique_key__c,AxtriaSalesIQTM__Assignment_Status__c,AxtriaSalesIQTM__Geography__r.Name,AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Effective_End_Date__c from AxtriaSalesIQTM__Position_Geography__c where Unique_key__c In: KeyIdMap.keySet() and AxtriaSalesIQTM__Assignment_Status__c != 'Inactive'])
          {
               processedrecordSet.add(posGeoCore.Unique_key__c);

               if(posGeoCore.AxtriaSalesIQTM__Assignment_Status__c == 'Active'){

                    posGeoCore.AxtriaSalesIQTM__Effective_End_Date__c =System.Today();
                    posGeoList.add(posGeoCore);

                    System.debug('inside second loop :::::'+posGeoCore.AxtriaSalesIQTM__Effective_End_Date__c );
               }

               else if (posGeoCore.AxtriaSalesIQTM__Assignment_Status__c == 'Future Active')
               {
                    System.debug('inside delete loop :::::'+posGeoCore);

                    futurePosGeoList.add(posGeoCore);
               }

          }

          if(!posGeoList.isEmpty()){
               update posGeoList ;
          }

          if(!futurePosGeoList.isEmpty()){
               delete futurePosGeoList ; 
          } 

          for(temp_Zip_Terr__c posgeo : scope)
          {
               String key = posgeo.Territory_ID__c.toUpperCase()+'_'+posgeo.Zip_Name__c.toUpperCase()+'_'+posgeo.Team_Instance_Name__c.toUpperCase();
               if(processedrecordSet.Contains(key))
               {
                    System.debug('inside processed loop :::::' );

                    posgeo.Status__c = 'Processed';
               }
               else
               {
                    System.debug('inside Rejected loop :::::' );
                    posgeo.Status__c = 'Rejected';
               }
          }

          update scope;   */  
          
     }
    
     global void finish(Database.BatchableContext BC){
     }
}