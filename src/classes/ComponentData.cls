public with sharing class ComponentData{
	public map<String,Grid_Details__c>griddata{get;set;}
	public map<String,Grid_Details_Copy__c>griddataTemp{get;set;}
	public list<String>cheaders{get;set;}
	public list<String>rheaders{get;set;}
	public String Mode{get;set;}
	public list<integer>row{get;set;}
	public list<integer>column{get;set;}
	public String output{get;set;}
	
	public Integer rowCount {get;set;}
	public Integer colCount {get;set;}
	public ComponentData(){
	
	}
}