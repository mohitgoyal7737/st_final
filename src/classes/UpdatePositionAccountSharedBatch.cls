/**
* Description   : Batch class to update isShared and SharedWith on Position Account 
* Author        : Gaurav 
* Constructor 1 Parameters : Team Instance ID (String)
* Constructor 2 Parameters : Team Instance ID (String) and List<string> Account Ids
* Date        : 3-May-2018
*/

global with sharing class UpdatePositionAccountSharedBatch implements Database.Batchable<sObject> ,Database.Stateful{
        
    // This variable will remain stateful throughout batch execution to maintain Account Ids with their list of Position Accounts.
    public map<ID,list<AxtriaSalesIQTM__Position_Account__c>>  mapOfAccountIdAndPositionAccount = new map<ID,list<AxtriaSalesIQTM__Position_Account__c>>();
    public String query;
    string teamInstanceId;
    list<string> accIDs;
    private map<ID,AxtriaSalesIQTM__Position_Account__c> toBeUpdatedPosAccMap;
    set<string> sharedPositions ;
    set<Id> accountIds;
     
    public UpdatePositionAccountSharedBatch (string teamInstance){
        teamInstanceId = teamInstance;
        accIDs = new list<string>();
        sharedPositions = new set<string>();
        accountIds = new set<Id>();
        toBeUpdatedPosAccMap = new map<ID,AxtriaSalesIQTM__Position_Account__c>();
        query = ''; 
        string namespace = SalesIQGlobalConstants.getOrgNameSpace();
        list<AggregateResult> Results = [select AxtriaSalesIQTM__Account__c, count(ID) from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Team_Instance__c =: teamInstanceId group by AxtriaSalesIQTM__Account__c having count(ID)>1 limit 2000];
        SnTDMLSecurityUtil.printDebugMessage('######### Aggregate Result ' + Results);
        for(AggregateResult sharedAccounts : Results){
            SnTDMLSecurityUtil.printDebugMessage('########## account id ' + sharedAccounts.get(namespace+'Account__c'));
            if(sharedAccounts.get(namespace+'Account__c') != null){
                accIDs.add(string.valueOf(sharedAccounts.get(namespace+'Account__c')));
            }
        }
        SnTDMLSecurityUtil.printDebugMessage('########## accIDs ' + accIDs);
        query = 'select id,AxtriaSalesIQTM__SharedWith__c,AxtriaSalesIQTM__IsShared__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Position__r.Name from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Team_Instance__c =: teamInstanceId and AxtriaSalesIQTM__Account__c in: accIDs and AxtriaSalesIQTM__Assignment_Status__c != \'Inactive\'';
    }
     
    public UpdatePositionAccountSharedBatch (string teamInstance,list<String> accounts){
        teamInstanceId = teamInstance;
        accIDs = new list<string>();
        toBeUpdatedPosAccMap = new map<ID,AxtriaSalesIQTM__Position_Account__c>();
        accountIds = new set<Id>();
        sharedPositions = new set<string>();
        query = '';         
        if(accounts != null && accounts.size() >0){
            accIDs.addAll(accounts);
            query = 'select id,AxtriaSalesIQTM__SharedWith__c,AxtriaSalesIQTM__IsShared__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Position__r.Name from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Team_Instance__c =: teamInstanceId and AxtriaSalesIQTM__Account__c in: accIDs and AxtriaSalesIQTM__Assignment_Status__c != \'Inactive\'';
        }
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc){
        SnTDMLSecurityUtil.printDebugMessage('query ' + query);
        return Database.getQueryLocator(query);
    }
     
    public void Execute (Database.BatchableContext bc , list<AxtriaSalesIQTM__Position_Account__c> scope){
        SnTDMLSecurityUtil.printDebugMessage('######## scope ' + scope);
        /*
            Modified By : Raghvendra Rathoreset<Id> accountIds = new set<Id>();
            Modified On : 9th May 2018
            Description : Modified to fix issue SIQ-1777. Clear inactive position assignments from SharedWith__c
        */
        //set<string> sharedPositions = new set<string>();
        //
        for(AxtriaSalesIQTM__Position_Account__c posAcc : scope){
            if(!string.isBlank(posAcc.AxtriaSalesIQTM__SharedWith__c)){
                sharedPositions.addAll(posAcc.AxtriaSalesIQTM__SharedWith__c.split(';'));
            }
            sharedPositions.add(posAcc.AxtriaSalesIQTM__Position__r.Name);
            accountIds.add(posAcc.AxtriaSalesIQTM__Account__c);
        }
        //SnTDMLSecurityUtil.printDebugMessage('#### sharedPositions : '+sharedPositions);
        //SnTDMLSecurityUtil.printDebugMessage('#### accountIds: '+accountIds);
        SnTDMLSecurityUtil.printDebugMessage('#### sharedPositions.size() : '+sharedPositions.size());
        SnTDMLSecurityUtil.printDebugMessage('#### sharedPositions : '+sharedPositions);
        SnTDMLSecurityUtil.printDebugMessage('#### accountIdssize():  '+accountIds.size());
        SnTDMLSecurityUtil.printDebugMessage('#### accountIds: '+accountIds);
        
        //RR : Find active shared assignments for selected accounts
        map<string,list<AxtriaSalesIQTM__Position_Account__c>> sharedAssignmentMap = new map<string,list<AxtriaSalesIQTM__Position_Account__c>>();
        for(AxtriaSalesIQTM__Position_Account__c pa : [SELECT Id, AxtriaSalesIQTM__Position__r.Name, AxtriaSalesIQTM__Account__c FROM AxtriaSalesIQTM__Position_Account__c WHERE AxtriaSalesIQTM__Position__r.Name IN : sharedPositions AND 
                                      AxtriaSalesIQTM__Account__c IN : accountIds AND AxtriaSalesIQTM__Assignment_Status__c !=: 'Inactive' AND 
                                      AxtriaSalesIQTM__Team_Instance__c =: teamInstanceId]){
            list<AxtriaSalesIQTM__Position_Account__c> temp = sharedAssignmentMap.get(pa.AxtriaSalesIQTM__Position__r.Name);
            if(temp == null){
                temp = new list<AxtriaSalesIQTM__Position_Account__c>();
            }
            temp.add(pa);
            sharedAssignmentMap.put(pa.AxtriaSalesIQTM__Position__r.Name,temp);
        }
        SnTDMLSecurityUtil.printDebugMessage('#### sharedAssignmentMap.size() = : '+sharedAssignmentMap.size());
        SnTDMLSecurityUtil.printDebugMessage('#### sharedAssignmentMap : '+sharedAssignmentMap);

        /*In this loop we are updating current record as well as records that we have maintained inside the stateful map.*/
        for(AxtriaSalesIQTM__Position_Account__c posAcc : scope){
            SnTDMLSecurityUtil.printDebugMessage('#### posAcc : '+posAcc);
            SnTDMLSecurityUtil.printDebugMessage('#### mapOfAccountIdAndPositionAccount : '+mapOfAccountIdAndPositionAccount);
            if(mapOfAccountIdAndPositionAccount.containsKey(posAcc.AxtriaSalesIQTM__Account__c)){
                list<AxtriaSalesIQTM__Position_Account__c> lsPosAcc = mapOfAccountIdAndPositionAccount.get(posAcc.AxtriaSalesIQTM__Account__c);               
                for(AxtriaSalesIQTM__Position_Account__c pAcc : lsPosAcc){
                    SnTDMLSecurityUtil.printDebugMessage('#### pAcc : '+pAcc);
                    //RR : Remove position from pAcc.SharedWith__c if no active assignment found
                    string sharedWithStr = '';
                    if(!string.isBlank(pAcc.AxtriaSalesIQTM__SharedWith__c)){
                        for(string pos : pAcc.AxtriaSalesIQTM__SharedWith__c.split(';')){
                            SnTDMLSecurityUtil.printDebugMessage('#### shared pos 1 '+ pos);
                            if(sharedAssignmentMap.containsKey(pos)){
                                for(AxtriaSalesIQTM__Position_Account__c pa : sharedAssignmentMap.get(pos)){
                                    SnTDMLSecurityUtil.printDebugMessage('#### shared pos : '+pos);
                                    if(pa.AxtriaSalesIQTM__Account__c == pAcc.AxtriaSalesIQTM__Account__c){
                                        sharedWithStr += pos+';';
                                    }
                                }
                            }
                        }
                        sharedWithStr = sharedWithStr.removeEnd(';');
                        pAcc.AxtriaSalesIQTM__SharedWith__c = sharedWithStr;
                        SnTDMLSecurityUtil.printDebugMessage('#### pAcc.SharedWith__c : '+pAcc.AxtriaSalesIQTM__SharedWith__c);
                        SnTDMLSecurityUtil.printDebugMessage('#### sharedWithStr : '+sharedWithStr);
                    }
                    
                    //Update pAcc.SharedWith__c
                    if(!string.isBlank(pAcc.AxtriaSalesIQTM__SharedWith__c) && !pAcc.AxtriaSalesIQTM__SharedWith__c.contains(posAcc.AxtriaSalesIQTM__Position__r.Name)){                  
                        pAcc.AxtriaSalesIQTM__SharedWith__c += ';'+posAcc.AxtriaSalesIQTM__Position__r.Name;
                    }else{
                        pAcc.AxtriaSalesIQTM__SharedWith__c = posAcc.AxtriaSalesIQTM__Position__r.Name;
                    }
                    pAcc.AxtriaSalesIQTM__IsShared__c = true;
                    toBeUpdatedPosAccMap.put(pAcc.ID,pAcc);
                    
                    //RR : Remove position from posAcc.SharedWith__c if no active assignment found
                    sharedWithStr = '';
                    if(!string.isBlank(posAcc.AxtriaSalesIQTM__SharedWith__c)){
                        for(string pos : posAcc.AxtriaSalesIQTM__SharedWith__c.split(';')){
                            SnTDMLSecurityUtil.printDebugMessage('#### shared pos : '+pos);
                            if(sharedAssignmentMap.containsKey(pos)){
                                for(AxtriaSalesIQTM__Position_Account__c pa : sharedAssignmentMap.get(pos)){
                                    SnTDMLSecurityUtil.printDebugMessage('#### shared assignment : '+ pa);
                                    if(pa.AxtriaSalesIQTM__Account__c == posAcc.AxtriaSalesIQTM__Account__c){
                                        sharedWithStr += pos+';';
                                    }
                                }
                            }
                        }
                        sharedWithStr = sharedWithStr.removeEnd(';');
                        posAcc.AxtriaSalesIQTM__SharedWith__c = sharedWithStr;
                        SnTDMLSecurityUtil.printDebugMessage('#### posAcc.SharedWith__c : '+posAcc.AxtriaSalesIQTM__SharedWith__c);
                        SnTDMLSecurityUtil.printDebugMessage('#### sharedWithStr : '+sharedWithStr);
                    }

                    if(!string.isBlank(posAcc.AxtriaSalesIQTM__SharedWith__c) && !posAcc.AxtriaSalesIQTM__SharedWith__c.contains(pAcc.AxtriaSalesIQTM__Position__r.Name)){
                        posAcc.AxtriaSalesIQTM__SharedWith__c += ';'+pAcc.AxtriaSalesIQTM__Position__r.Name;
                    }else{
                        posAcc.AxtriaSalesIQTM__SharedWith__c = pAcc.AxtriaSalesIQTM__Position__r.Name;
                    }
                    posAcc.AxtriaSalesIQTM__IsShared__c = true;
                    //A2412
                    //first error: STRING_TOO_LONG, Comments: data value too large: Filled in UpdatePositionAccountSharedBatch (max length=40): [AxtriaSalesIQTM__Comments__c]
                    if(!System.Test.isRunningTest())
                        {
                    posAcc.AxtriaSalesIQTM__Comments__c = 'Filled in UpdatePositionAccountSharedBatch';
                    }
                    toBeUpdatedPosAccMap.put(posAcc.ID,posAcc);
                }
                SnTDMLSecurityUtil.printDebugMessage('############ toBeUpdatedPosAccMap.size() =  ' + toBeUpdatedPosAccMap.size());
                SnTDMLSecurityUtil.printDebugMessage('############ toBeUpdatedPosAccMap ' + toBeUpdatedPosAccMap);
                mapOfAccountIdAndPositionAccount.get(posAcc.AxtriaSalesIQTM__Account__c).add(posAcc);
            }else{
                mapOfAccountIdAndPositionAccount.put(posAcc.AxtriaSalesIQTM__Account__c,new list<AxtriaSalesIQTM__Position_Account__c>{posAcc});
            }
            SnTDMLSecurityUtil.printDebugMessage('############ mapOfAccountIdAndPositionAccount ' + mapOfAccountIdAndPositionAccount);
        }
        
        /*Raghav - Remove the sharing flag from the assignments not shared anymore*/
        for(Id accId : mapOfAccountIdAndPositionAccount.keyset()){
            if(mapOfAccountIdAndPositionAccount.get(accId).size() == 1){
                list<AxtriaSalesIQTM__Position_Account__c> posAcc = mapOfAccountIdAndPositionAccount.get(accId);
                if(posAcc[0].AxtriaSalesIQTM__IsShared__c || posAcc[0].AxtriaSalesIQTM__SharedWith__c != ''){
                    posAcc[0].AxtriaSalesIQTM__IsShared__c = false;
                    posAcc[0].AxtriaSalesIQTM__SharedWith__c = '';
                    toBeUpdatedPosAccMap.put(posAcc[0].Id,posAcc[0]);
                }
            }
        }
        
        //update toBeUpdatedPosAccMap.values();
        SnTDMLSecurityUtil.updateRecords(toBeUpdatedPosAccMap.values(), 'UpdatePositionAccountSharedBatch');

    }
     
    public void Finish(Database.BatchableContext bc){
        SnTDMLSecurityUtil.printDebugMessage('######### Batch Execution finished ');
    }
}