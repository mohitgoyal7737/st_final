@isTest
private class batchOutBoundUpdPosGeographyTest {
     @testSetup 
    static void setup() {
 
        AxtriaSalesIQTM__Team__c t=new AxtriaSalesIQTM__Team__c();
        t.Name='xx';
        insert t;
        String teamId=t.id;
        AxtriaSalesIQTM__Team__c t1=new AxtriaSalesIQTM__Team__c();
        t1.Name='yy';
        insert t1;
        String parentteamId=t1.id;
        AxtriaSalesIQTM__Team_Instance__c ti=new AxtriaSalesIQTM__Team_Instance__c();
        ti.AxtriaSalesIQTM__Team__c=teamId;
        ti.AxtriaSalesIQTM__Alignment_Period__c='current';
        insert ti;
        String teamInstId=ti.id;
        AxtriaSalesIQTM__Geography__c geo=new AxtriaSalesIQTM__Geography__c();
        geo.Team_Name__c='yy';
        geo.Team_Instance__c='y';
        insert geo;
        String geoId=geo.id;
         AxtriaSalesIQTM__Position__c pos1=new AxtriaSalesIQTM__Position__c();
        pos1.AxtriaSalesIQTM__Team_iD__c=parentteamId;
        insert pos1;
        String parentposId=pos1.id;
        AxtriaSalesIQTM__Position__c pos=new AxtriaSalesIQTM__Position__c();
        pos.AxtriaSalesIQTM__Team_iD__c=teamId;
        pos.AxtriaSalesIQTM__Parent_Position__c=parentposId;
        insert pos;
        String posId=pos.id;
        AxtriaSalesIQTM__Position_Geography__c pg=new AxtriaSalesIQTM__Position_Geography__c();
        pg.AxtriaSalesIQTM__Geography__c=geoId;
        pg.AxtriaSalesIQTM__Position__c=posId;
        pg.AxtriaSalesIQTM__Effective_Start_Date__c=Date.valueOf('2018-01-01');
        pg.AxtriaSalesIQTM__Effective_End_Date__c=date.valueOf('2018-05-05');
        pg.AxtriaSalesIQTM__Team_Instance__c=teamInstId;
        
       insert pg;
    }
    static testmethod void test() {        
        Test.startTest();
         AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
        team.Name = 'Specialty';
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamIns = new AxtriaSalesIQTM__Team_Instance__c();
        teamIns.AxtriaSalesIQTM__Team__c = team.id;
        teamIns.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        teamIns.Name = 'abc';
        insert teamIns;
        /*batchOutBoundUpdPosGeography usa = new batchOutBoundUpdPosGeography();
        Id batchId = Database.executeBatch(usa);*/
      //  BatchOutBoundPositionProduct obj = new BatchOutBoundPositionProduct();
        
        Test.stopTest();
        // after the testing stops, assert records were updated 
    }
}