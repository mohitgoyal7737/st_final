/*Author - Himanshu Tariyal(A0994)
Date : 7th January 2018*/
@isTest
private class SimulationModellingCtlrTest
{
    private static testMethod void firstTest()
    {
        System.test.startTest();

        /*Create initial test data for all the objs reqd.*/
        AxtriaSalesIQTM__Organization_Master__c aom = new AxtriaSalesIQTM__Organization_Master__c(AxtriaSalesIQTM__Org_Level__c = 'Global', AxtriaSalesIQTM__Parent_Country_Level__c = true);
        insert aom;

        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name = 'Italy', AxtriaSalesIQTM__Parent_Organization__c = aom.id, AxtriaSalesIQTM__Status__c = 'Active');
        insert country;

        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = country.id;
        insert workspace;

        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name = 'Test Team');
        team.AxtriaSalesIQTM__Country__c = country.id;
        insert team;

        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name = 'Test BU', AxtriaSalesIQTM__Team__c = team.id/*,Cycle__c=cycle.id*/);
        insert ti;
		AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(ti, team, workspace);
        insert scen;
        Product_Catalog__c pc = new Product_Catalog__c(Name = 'Test Product', Team_Instance__c = ti.id, Veeva_External_ID__c = 'ProdId', Product_Code__c = 'ProdId', IsActive__c = true, Country_Lookup__c = country.id);
        insert pc;

        Measure_Master__c mm = new Measure_Master__c(Team_Instance__c = ti.id, Brand_Lookup__c = pc.id, State__c = 'Executed');
        insert mm;

        Compute_Master__c cm1 = new Compute_Master__c();
        cm1.Expression__c = '{"elif":[{"andOr":[],"input":"Avg Din BYD","condition":"less than","match":"70","returVal":"Average"},' +
                            '{"andOr":[],"input":"Avg Din BYD","condition":"less than or equal to","match":"90","returVal":"Medium"}],' +
                            '"ifCase":{"andOr":[],"input":"Avg Din BYD","condition":"less than","match":"50","returVal":"Low"},' +
                            '"elseCase":{"andOr":[],"returVal":"High"}}';
        insert cm1;

        Compute_Master__c cm2 = new Compute_Master__c();
        cm2.Expression__c = '{"elif":[{"andOr":[],"input":"Avg Din BYD","condition":"greater than or equal to","match":"70","returVal":"Medium"},' +
                            '{"andOr":[],"input":"Avg Din BYD","condition":"greater than","match":"50","returVal":"Average"}],' +
                            '"ifCase":{"andOr":[],"input":"Avg Din BYD","condition":"greater than","match":"90","returVal":"High"},' +
                            '"elseCase":{"andOr":[],"returVal":"Low"}}';
        insert cm2;
		
        Step__c step = new Step__c(Modelling_Type__c = 'Adoption', Name = 'Test Step 1', Measure_Master__c = mm.id, Step_Type__c = 'Cases', Compute_Master__c = cm1.id,UI_Location__c = 'Compute Values');
        insert step;
        

        Step__c step2 = new Step__c(Modelling_Type__c = 'Potential', Name = 'Test Step 2', Measure_Master__c = mm.id, Step_Type__c = 'Cases', Compute_Master__c = cm2.id,UI_Location__c = 'Compute Values');
        insert step2;

        Modelling_Simulation__c ms1 = new Modelling_Simulation__c(HCP_Count__c = 5, Value__c = '45',Parameter_Value__c=45, Measure_Master__c = mm.id, Parameter_Name__c = 'Test Step 1',CustomerSegment__c = 'null');
        insert ms1;

        Modelling_Simulation__c ms11 = new Modelling_Simulation__c(HCP_Count__c = 15, Value__c = '115',Parameter_Value__c=115, Measure_Master__c = mm.id, Parameter_Name__c = 'Test Step 1',CustomerSegment__c = 'null');
        insert ms11;

        Modelling_Simulation__c ms2 = new Modelling_Simulation__c(HCP_Count__c = 9, Value__c = '72', Parameter_Value__c = 72,Measure_Master__c = mm.id, Parameter_Name__c = 'Test Step 2',CustomerSegment__c = 'null');
        insert ms2;

        Modelling_Simulation__c ms21 = new Modelling_Simulation__c(HCP_Count__c = 19, Value__c = '25',Parameter_Value__c=25, Measure_Master__c = mm.id, Parameter_Name__c = 'Test Step 2',CustomerSegment__c = 'null');
        insert ms21;

        Segment_Simulation__c s = new Segment_Simulation__c();
        s.Measure_Master__c = mm.id;
        s.CustomerSegment__c = 'A';
        insert s;
        
        Account acc = TestDataFactory.createAccount();
        acc.Profile_Consent__c='No';
        acc.AxtriaSalesIQTM__Speciality__c ='testspecs';
        insert acc;
    
        Account_Compute_Final__c acf = new Account_Compute_Final__c();
        acf.Measure_Master__c = mm.Id;
        acf.Physician_2__c=acc.id;
        acf.CustonerSegVal__c = '';
        Integer i;
        for(i=1;i<10;i++){
            acf.put('Output_Name_'+i+'__c',i+'');
        }
        acf.put('Output_Name_'+i+'__c','Test Step 1');
        acf.put('Output_Name_'+(++i)+'__c','Test Step 2');
        
   		insert acf;
        /*Test Data ends here*/

        /*Create an instance of the class*/
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String> {nameSpace + 'Parameter__c'};
        System.assertEquals(true, SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        ApexPages.currentPage().getParameters().put('rid',mm.id);
        ApexPages.currentPage().getParameters().put('cycleId',workspace.id);
        ApexPages.currentPage().getParameters().put('teamInstanceID',ti.id);
        ApexPages.currentPage().getParameters().put('brandID',pc.id);
        SimulationModellingCtlr sm = new SimulationModellingCtlr();
        sm.cycleSelected = workspace.id;
        sm.selectedBu = ti.id;
        sm.selectedBrand = pc.id;
        sm.selectedRule = mm.id;
        sm.selectedCustomerSegment = 'null';
        

        sm.redirectToPage();
        sm.selectedSimulationType = 'Segment Simulation';
        sm.redirectToPage();
        sm.selectedSimulationType = 'Workload Simulation';
        sm.redirectToPage();
        sm.selectedSimulationType = 'Quantile Analysis';
        sm.redirectToPage();
        sm.selectedSimulationType = 'Comparative Analysis';
        sm.redirectToPage();
        sm.selectedSimulationType = 'Modelling';
        
        sm.redirectToPage();
        
        //sm.fillCycleOptions();
        //sm.fillBusinessUnitOptions();
        //sm.fillBrandOptions();
        //sm.fillRuleOptions();
        sm.selectedProfileParam = 'Test Step 1';
        sm.populateProfileParam();
        sm.selectedProfileParam = 'Test Step 2';
        sm.changeProfileParam();
       

        //sm.checkValidations();
        
        sm.inputDataList = new List<SimulationModellingCtlr.profileParamWrapper>();
        /*ppw.bucketSequence,ppw.bucketName,ppw.bucketCount,ppw.param1_name,ppw.param1_operator,ppw.param1_val,ppw.param2_name,ppw.param2_operator,ppw.param2_val,ppw.separator,ppw.percentCount*/
        sm.inputDataList.add(new SimulationModellingCtlr.profileParamWrapper('1','abcd', '9', 'AB', 'greater than','10','','','','','20'));
        sm.inputDataList.add(new SimulationModellingCtlr.profileParamWrapper('1','abcd', '20', 'AB', 'greater than','2','','','','','80'));
        sm.checkValidations();
        
		sm.createConcCurve();
        sm.chartType = '2';
        sm.createConcCurve();
        sm.pushToBusinessRules();
        mm.State__c = 'Publishing';
        update mm;
        sm.switchToBusinessRules();
        // sm.pushToBusinessRules();
        /*All the methods have been tested on the Ctlr*/
        System.test.stopTest();
    }
    private static testMethod void secondTest()
    {
        System.test.startTest();

        /*Create initial test data for all the objs reqd.*/
        AxtriaSalesIQTM__Organization_Master__c aom = new AxtriaSalesIQTM__Organization_Master__c(AxtriaSalesIQTM__Org_Level__c = 'Global', AxtriaSalesIQTM__Parent_Country_Level__c = true);
        insert aom;

        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name = 'Italy', AxtriaSalesIQTM__Parent_Organization__c = aom.id, AxtriaSalesIQTM__Status__c = 'Active');
        insert country;

        /* Cycle__c cycle = new Cycle__c(Name='Test Cycle',Country__c = country.id);
        insert cycle;*/

        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name = 'Test Team');
        team.AxtriaSalesIQTM__Country__c = country.id;
        insert team;

        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name = 'Test BU', AxtriaSalesIQTM__Team__c = team.id/*,Cycle__c=cycle.id*/);
        insert ti;

        Product_Catalog__c pc = new Product_Catalog__c(Name = 'Test Product', Team_Instance__c = ti.id, Veeva_External_ID__c = 'ProdId', Product_Code__c = 'ProdId', IsActive__c = true, Country_Lookup__c = country.id);
        insert pc;

        Measure_Master__c mm = new Measure_Master__c(Team_Instance__c = ti.id, Brand_Lookup__c = pc.id, State__c = 'Executed');
        insert mm;

        Compute_Master__c cm1 = new Compute_Master__c();
        cm1.Expression__c = '{"elif":[{"andOr":[],"input":"Avg Din BYD","condition":"less than","match":"70","returVal":"Average"},' +
                            '{"andOr":[],"input":"Avg Din BYD","condition":"less than or equal to","match":"90","returVal":"Medium"}],' +
                            '"ifCase":{"andOr":[{"input":"Avg Din BYD","condition":"greater than","match":"90","returVal":"High","type":"AND"}],"input":"Avg Din BYD","condition":"less than","match":"50","returVal":"Low"},' +
                            '"elseCase":{"andOr":[],"returVal":"High"}}';
        cm1.Simulated_Expression__c = '{"elif":[{"andOr":[],"input":"Avg Din BYD","condition":"less than","match":"70","returVal":"Average"},' +
                            '{"andOr":[],"input":"Avg Din BYD","condition":"less than or equal to","match":"90","returVal":"Medium"}],' +
                            '"ifCase":{"andOr":[],"input":"Avg Din BYD","condition":"less than","match":"50","returVal":"Low"},' +
                            '"elseCase":{"andOr":[],"returVal":"High"}}';
        insert cm1;

        Compute_Master__c cm2 = new Compute_Master__c();
        cm2.Expression__c = '{"elif":[{"andOr":[],"input":"Avg Din BYD","condition":"greater than or equal to","match":"70","returVal":"Medium"},' +
                            '{"andOr":[],"input":"Avg Din BYD","condition":"greater than","match":"50","returVal":"Average"}],' +
            '"ifCase":{"andOr":[{"input":"Avg Din BYD","condition":"greater than","match":"90","returVal":"High","type":"AND"}],"input":"Avg Din BYD","condition":"greater than","match":"90","returVal":"High"},' +
                            '"elseCase":{"andOr":[],"returVal":"Low"}}';
        insert cm2;
		Parameter__c pp = TestDataFactory.parameter(pc, team, ti);
        insert pp;
        Step__c step = new Step__c(Modelling_Type__c = 'Adoption', Name = 'Test Step 1', Measure_Master__c = mm.id, Step_Type__c = 'Cases', Compute_Master__c = cm1.id,UI_Location__c = 'Compute Values');
        insert step;
        Rule_Parameter__c rps = TestDataFactory.ruleParameter(mm, pp, step);
        rps.CustomerSegmentedField__c = true;
        insert rps;

        Step__c step2 = new Step__c(Modelling_Type__c = 'Potential', Name = 'Test Step 2', Measure_Master__c = mm.id, Step_Type__c = 'Cases', Compute_Master__c = cm2.id,UI_Location__c = 'Compute Values');
        insert step2;

        Modelling_Simulation__c ms1 = new Modelling_Simulation__c(HCP_Count__c = 5, Value__c = '45',Parameter_Value__c=45, Measure_Master__c = mm.id, Parameter_Name__c = 'Test Step 1',CustomerSegment__c = 'null');
        insert ms1;

        Modelling_Simulation__c ms11 = new Modelling_Simulation__c(HCP_Count__c = 15, Value__c = '115',Parameter_Value__c=115, Measure_Master__c = mm.id, Parameter_Name__c = 'Test Step 1',CustomerSegment__c = 'null');
        insert ms11;

        Modelling_Simulation__c ms2 = new Modelling_Simulation__c(HCP_Count__c = 9, Value__c = '72', Parameter_Value__c = 72,Measure_Master__c = mm.id, Parameter_Name__c = 'Test Step 2',CustomerSegment__c = 'null');
        insert ms2;

        Modelling_Simulation__c ms21 = new Modelling_Simulation__c(HCP_Count__c = 19, Value__c = '25',Parameter_Value__c=25, Measure_Master__c = mm.id, Parameter_Name__c = 'Test Step 2',CustomerSegment__c = 'null');
        insert ms21;

        Segment_Simulation__c s = new Segment_Simulation__c();
        s.Measure_Master__c = mm.id;
        s.CustomerSegment__c = 'A';
        insert s;
        
        Account acc = TestDataFactory.createAccount();
        acc.Profile_Consent__c='No';
        acc.AxtriaSalesIQTM__Speciality__c ='testspecs';
        insert acc;
    
        Account_Compute_Final__c acf = new Account_Compute_Final__c();
        acf.Measure_Master__c = mm.Id;
        acf.Physician_2__c=acc.id;
        acf.CustonerSegVal__c = '';
        Integer i;
        for(i=1;i<10;i++){
            acf.put('Output_Name_'+i+'__c',i+'');
        }
        acf.put('Output_Name_'+i+'__c','Test Step 1');
        acf.put('Output_Name_'+(++i)+'__c','Test Step 2');
        
   		insert acf;
        /*Test Data ends here*/

        /*Create an instance of the class*/
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String> {nameSpace + 'Parameter__c'};
        System.assertEquals(true, SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        SimulationModellingCtlr sm = new SimulationModellingCtlr();
        // sm.cycleSelected = cycle.id;
        sm.selectedBu = ti.id;
        sm.selectedBrand = pc.id;
        sm.selectedRule = mm.id;
        sm.selectedCustomerSegment = 'null';
        

        sm.redirectToPage();
        sm.selectedSimulationType = 'Segment Simulation';
        sm.redirectToPage();
        sm.selectedSimulationType = 'Workload Simulation';
        sm.redirectToPage();
        sm.selectedSimulationType = 'Quantile Analysis';
        sm.redirectToPage();
        sm.selectedSimulationType = 'Modelling';
        sm.redirectToPage();
        
        sm.fillCycleOptions();
        sm.fillBusinessUnitOptions();
        sm.fillBrandOptions();
        sm.fillRuleOptions();
        sm.selectedProfileParam = 'Test Step 1';
        sm.populateProfileParam();
        sm.selectedProfileParam = 'Test Step 2';
        sm.changeProfileParam();
       

        //sm.checkValidations();
        
        sm.inputDataList = new List<SimulationModellingCtlr.profileParamWrapper>();
        /*ppw.bucketSequence,ppw.bucketName,ppw.bucketCount,ppw.param1_name,ppw.param1_operator,ppw.param1_val,ppw.param2_name,ppw.param2_operator,ppw.param2_val,ppw.separator,ppw.percentCount*/
        sm.inputDataList.add(new SimulationModellingCtlr.profileParamWrapper('1','abcd', '9', 'AB', 'greater than','10','AND','BC','equal','TRUE','20'));
        sm.inputDataList.add(new SimulationModellingCtlr.profileParamWrapper('1','abcd', '20', 'AB', 'greater than','2','','','','','80'));
        sm.checkValidations();
        
		sm.createConcCurve();
        sm.chartType = '2';
        sm.createConcCurve();
        sm.pushToBusinessRules();
        mm.State__c = 'Publishing';
        update mm;
        // sm.pushToBusinessRules();
        /*All the methods have been tested on the Ctlr*/
        System.test.stopTest();
    }

    
}