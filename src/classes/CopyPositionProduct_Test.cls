@isTest
public class CopyPositionProduct_Test {
    @istest static void BatchCallPlanDownload_Test()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__AccountType__c ='HCP';
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'ONCO';
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        insert scen;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        insert teamins;
        AxtriaSalesIQTM__Team_Instance__c teamins2 = TestDataFactory.createTeamInstance(team);
        teamins2.AxtriaSalesIQTM__Scenario__c = scen.id;
        insert teamins2;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        Product_Catalog__c pcc1 = TestDataFactory.productCatalog(team, teamins2, countr);
        insert pcc1;
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        AxtriaSalesIQTM__Position_Product__c posproduct = TestDataFactory.createPositionProduct(teamins,pos,pcc);
        insert posproduct;
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            /*CopyPositionProduct obj = new CopyPositionProduct(teamins.id,teamins2.id);
            obj.query='SELECT Calls_Day__c,Holidays__c,isHolidayOverridden__c,Other_Days_Off__c,Product_Catalog__c,Product_Catalog__r.name,Vacation_Days__c,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__isActive__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Product_Weight__c,AxtriaSalesIQTM__Team_Instance__c,Id,Name,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c FROM AxtriaSalesIQTM__Position_Product__c';
            Database.executeBatch(obj);*/
        }
        Test.stopTest();
    }
}