global class BatchOutBoundEmployee implements Database.Batchable<sObject>,Database.stateful,Schedulable {
    public String query;
    public Integer recordsProcessed=0;
    public String batchID;
    global DateTime lastjobDate=null;
    public map<String,String>mapVeeva2Mktcode {get;set;}
    public map<String,String>Countrymap {get;set;}
    public set<String> Uniqueset {get;set;}    
    public String cycle {get;set;}
    global boolean flag=true;
    Global String Country_1;
    public list<String> CountryList;
    public  List<String> DeltaCountry ;

        


    global BatchOutBoundEmployee(String Country1) {}

    global BatchOutBoundEmployee() {}

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
    public void execute(System.SchedulableContext SC){
       database.executeBatch(new BatchOutBoundEmployee());
    }
    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Employee__c> scope) {}

    global void finish(Database.BatchableContext BC) {}
}