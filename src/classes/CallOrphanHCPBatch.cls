global class CallOrphanHCPBatch implements Database.Batchable<sObject> ,Database.Stateful, Schedulable  {
    public String query;
    public Set<String> allCountriesList;
    public Set<String> teamInsSet;

    global CallOrphanHCPBatch() {
        query = '';
        allCountriesList = new Set<String>();
        teamInsSet = new Set<String>();
        query = 'select id, Country__c from Veeva_Job_Scheduling__c where Load_Type__c = \'Delta\' or Load_Type__c = \'Full Load\'';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Veeva_Job_Scheduling__c> scope) {

        System.debug('<<<<<<<<<<---- Query --->>>>>>>>>>' +scope);

        for(Veeva_Job_Scheduling__c vcs : scope)
        {
            allCountriesList.add(vcs.Country__c);
        }

        System.debug('allCountriesList::::::::' +allCountriesList);

        List<AxtriaSalesIQTM__Team_Instance__c> teamInsList = [select Id from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c = 'Current' and AxtriaSalesIQTM__Scenario__c != null and AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Live'];

        for(AxtriaSalesIQTM__Team_Instance__c tiRec : teamInsList)
        {
            teamInsSet.add(tiRec.Id);
        }
        System.debug('teamInsSet:::::: ' +teamInsSet);
        
    }

    public void execute(System.SchedulableContext SC){
       database.executeBatch(new CallOrphanHCPBatch());
    }

    global void finish(Database.BatchableContext BC) {

        for(String teamIns : teamInsSet)
        {
            //DeleteDataCheckMissingAffiliation deleteBatch = new DeleteDataCheckMissingAffiliation(teamIns);
            //Database.executeBatch(deleteBatch,2000);
        }
    }
}