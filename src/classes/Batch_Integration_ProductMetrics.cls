global class Batch_Integration_ProductMetrics implements Database.Batchable<sObject>, Database.Stateful  {
    public String query;
    public List<SIQ_Product_Metrics_vod_O__c> allProductMetricsData;
    public Map<String,String> productToVeevaID;
    public List<Segment_Veeva_Mapping__c> segmentVeevaMapping;
    public Map<String,String> segmentVeevaMappingMap;
    public Set<String> allStrs;
    public Map<String,String> detailGroupMap;
    
    public List<String> selectedTeamInstances;

    global Batch_Integration_ProductMetrics(List<String> selectedTeamInstances) 
    {
        this.query = 'select id, P1__c, AxtriaSalesIQTM__Segment10__c, Segment__c, AxtriaSalesIQTM__Account__r.AZ_VeevaID__c , AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__External_Account_Number__c, AxtriaSalesIQTM__Account__r.AccountNumber , Adoption__c, Potential__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c, Country__c,AxtriaSalesIQTM__lastApprovedTarget__c,AxtriaARSnT__Segment_Approved__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Team_Instance__c in :selectedTeamInstances';
        allStrs = new Set<String>();

        detailGroupMap = new Map<String,String>();
        this.selectedTeamInstances = new List<String>(selectedTeamInstances);
        allProductMetricsData = new List<SIQ_Product_Metrics_vod_O__c>();
        productToVeevaID = new Map<String, String>();

        segmentVeevaMapping = new List<Segment_Veeva_Mapping__c>();
        segmentVeevaMappingMap = new Map<String, String>();

        segmentVeevaMapping = [select id, Segment_Axtria__c, Segment_Veeva__c, Team_Instance__c from Segment_Veeva_Mapping__c where Team_Instance__c in :selectedTeamInstances];

        for(Segment_Veeva_Mapping__c svm : segmentVeevaMapping)
        {
            segmentVeevaMappingMap.put(svm.Segment_Axtria__c+ '_' + svm.Team_Instance__c, svm.Segment_Veeva__c);
        }

        List<Product_Catalog__c> allProducts = [select id, Veeva_External_ID__c, Country__c,Detail_Group__c,  Name from Product_Catalog__c where Team_Instance__c in :selectedTeamInstances and AxtriaARSnT__IsActive__c=true];

        for(Product_Catalog__c pc : allProducts)
        {
            productToVeevaID.put(pc.Name + '_' + pc.Country__c , pc.Veeva_External_ID__c);

            if(pc.Detail_Group__c != null)
            detailGroupMap.put(pc.Name + '_' + pc.Country__c , pc.Detail_Group__c);
        }
    }

    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scope) 
    {
        
         allProductMetricsData = new List<SIQ_Product_Metrics_vod_O__c>();
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : scope)
        {
            SIQ_Product_Metrics_vod_O__c spm = new SIQ_Product_Metrics_vod_O__c();
            spm.SIQ_Account_vod__c = pacp.AxtriaSalesIQTM__Account__r.AccountNumber;//pacp.AxtriaSalesIQTM__Account__r.AZ_VeevaID__c;
            spm.SIQ_Account_Number__c = pacp.AxtriaSalesIQTM__Account__r.AccountNumber;
            spm.SIQ_Adoption_AZ__c = pacp.Adoption__c;
            spm.SIQ_Potential_AZ__c = pacp.Potential__c;
            spm.SIQ_Country__c = pacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
            spm.SIQ_Product_Name__c = pacp.P1__c;
            spm.Detail_Group__c = detailGroupMap.get(pacp.P1__c + '_' + pacp.Country__c);

            if(segmentVeevaMappingMap.containsKey(pacp.AxtriaARSnT__Segment_Approved__c +'_'+pacp.AxtriaSalesIQTM__Team_Instance__c))
            {
                spm.SIQ_Segment__c   = segmentVeevaMappingMap.get(pacp.AxtriaARSnT__Segment_Approved__c +'_'+pacp.AxtriaSalesIQTM__Team_Instance__c);
            }
            else
            {
                spm.SIQ_Segment__c   = pacp.AxtriaARSnT__Segment_Approved__c;    
            }
            
            //segment blank when looser
            //inactive->deleted
            if(pacp.AxtriaSalesIQTM__lastApprovedTarget__c == false)
            {
                if(pacp.AxtriaSalesIQTM__Segment10__c == 'Inactive')
                {
                    spm.SIQ_Status__c = 'Deleted';
                }
                if(pacp.AxtriaSalesIQTM__Segment10__c == 'Loser Account')
                {
                   spm.SIQ_Segment__c   = '';
                   spm.SIQ_Status__c = 'Updated';
                } 
            }

            spm.SIQ_Products_vod__c = productToVeevaID.get(pacp.P1__c + '_' + pacp.Country__c);
            if(spm.Detail_Group__c!= null)
            spm.SIQ_External_ID__c = spm.SIQ_Country__c +'_'+ pacp.AxtriaSalesIQTM__Account__r.AccountNumber/*spm.SIQ_Account_vod__c*/ +'_'+ spm.SIQ_Products_vod__c + '_' + spm.Detail_Group__c;
            else
                spm.SIQ_External_ID__c = spm.SIQ_Country__c +'_'+ pacp.AxtriaSalesIQTM__Account__r.AccountNumber/*spm.SIQ_Account_vod__c*/ +'_'+ spm.SIQ_Products_vod__c + '_';
            
            if(!allStrs.contains(spm.SIQ_External_ID__c) && spm.SIQ_Product_Name__c != null)
            {
                allProductMetricsData.add(spm);
                allStrs.add(spm.SIQ_External_ID__c);
            }
            /*
            if(detailGroupMap.containsKey(pacp.P1__c + '_' + pacp.Country__c))
            {
                spm.SIQ_Products_vod__c =  detailGroupMap.get(pacp.P1__c + '_' + pacp.Country__c);
                spm.SIQ_External_ID__c = spm.SIQ_Country__c +'_'+ spm.SIQ_Account_vod__c +'_'+ spm.SIQ_Products_vod__c;

                if(!allStrs.contains(spm.SIQ_External_ID__c))
                {
                    allProductMetricsData.add(spm);
                    allStrs.add(spm.SIQ_External_ID__c);
                }
            }*/
        }

        upsert allProductMetricsData SIQ_External_ID__c;
    }


    global void finish(Database.BatchableContext BC) 
    {

    }
}