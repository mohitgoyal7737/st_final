global with sharing class BatchPopulateChannelPreferenceData implements Database.Batchable<sObject>, Database.Stateful {
    public String query;
    public String nameSpace;
    public String loadType;
    public boolean flag;
    public String Ids;
    Integer recordsUpdated;
    Integer totalRecords;

    //Added by HT(A0994) on 17th June 2020
    global String changeReqID;
    global Boolean flagValue = false;

    global BatchPopulateChannelPreferenceData(String CR_ID) {
    	this.loadType = 'Channel Preference';
        this.nameSpace = ST_Utility.fetchNamespace('BatchPopulateChannelPreferenceData');
        this.flag = true;
        this.IDs= CR_ID;
        recordsUpdated = 0;
        totalRecords = 0;
        this.query = 'Select Channel_1__c, Channel_2__c, Channel_3__c, Channel_4__c, Channel_5__c, Objective_1__c, Objective_2__c, Objective_3__c, Objective_4__c, Objective_5__c, AccountNumber__c, Product_Name__c, Object__c, Change_Request__c, isError__c, SalesIQ_Error_Message__c from temp_Obj__c where Object__c = :loadType and Status__c = \'New\'';
    }

    //Added by HT(A0994) on 17th June 2020
    global BatchPopulateChannelPreferenceData(String CR_ID,Boolean flag) 
    {
        this.loadType = 'Channel Preference';
        this.nameSpace = ST_Utility.fetchNamespace('BatchPopulateChannelPreferenceData');
        this.flag = true;
        this.IDs= CR_ID;
        flagValue = flag;
        changeReqID = CR_ID;
        recordsUpdated = 0;
        totalRecords = 0;
        this.query = 'Select Channel_1__c, Channel_2__c, Channel_3__c, Channel_4__c, Channel_5__c, '+
                    'Objective_1__c, Objective_2__c, Objective_3__c, Objective_4__c, Objective_5__c, '+
                    'AccountNumber__c, Product_Name__c, Object__c, Change_Request__c, isError__c, '+
                    'SalesIQ_Error_Message__c from temp_Obj__c where Object__c = :loadType and '+
                    'Status__c = \'New\' and Change_Request__c =:changeReqID WITH SECURITY_ENFORCED';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Sobject> scope) {
       	totalRecords += scope.size();
       	Product_Priority__c pp = new Product_Priority__c();
       	List<Product_Priority__c> ppList = new List<Product_Priority__c>();
       	String accountNumber = '';
       	Set<String> setAccNumber = new Set<String>();
       	Map<String,String> mapAccount = new Map<String,String>();
       	for(Integer i=0,j=scope.size();i<j;i++){
       		accountNumber = String.valueOf(scope[i].get(nameSpace+'AccountNumber__c'));
			if(String.isNotBlank(accountNumber)){
       			setAccNumber.add(accountNumber);
       		}
       	}
       	for(Account acc : [Select Id,AccountNumber from Account where AccountNumber in:setAccNumber WITH SECURITY_ENFORCED]){
       		mapAccount.put(acc.AccountNumber,acc.ID);
       	}
       	
       	for(Integer i=0,j=scope.size();i<j;i++){
			accountNumber = String.valueOf(scope[i].get(nameSpace+'AccountNumber__c'));
			if(String.isBlank(accountNumber)){
				scope[i].put('Status__c', 'Rejected');
                scope[i].put('SalesIQ_Error_Message__c', 'Account Number cannot be blank');
                scope[i].put('isError__c', true);
                 
			}
			else if(!mapAccount.containsKey(accountNumber)){
				scope[i].put('Status__c', 'Rejected');
                scope[i].put('SalesIQ_Error_Message__c', 'Account Number not present in system');
                scope[i].put('isError__c', true);
			}
			else{
				pp = new Product_Priority__c();
				pp.Channel_1_Name__c = String.valueOf(scope[i].get(nameSpace+'Channel_1__c'));
				pp.Channel_2_Name__c = String.valueOf(scope[i].get(nameSpace+'Channel_2__c'));
				pp.Channel_3_Name__c = String.valueOf(scope[i].get(nameSpace+'Channel_3__c'));
				pp.Channel_4_Name__c = String.valueOf(scope[i].get(nameSpace+'Channel_4__c'));
				pp.Channel_5_Name__c = String.valueOf(scope[i].get(nameSpace+'Channel_5__c'));
				pp.Channel_1_Preference__c = String.valueOf(scope[i].get(nameSpace+'Objective_1__c'));
				pp.Channel_2_Preference__c = String.valueOf(scope[i].get(nameSpace+'Objective_2__c'));
				pp.Channel_3_Preference__c = String.valueOf(scope[i].get(nameSpace+'Objective_3__c'));
				pp.Channel_4_Preference__c = String.valueOf(scope[i].get(nameSpace+'Objective_4__c'));
				pp.Channel_5_Preference__c = String.valueOf(scope[i].get(nameSpace+'Objective_5__c'));
				pp.Account_Number__c = String.valueOf(scope[i].get(nameSpace+'AccountNumber__c'));
				pp.Product_Name__c = String.valueOf(scope[i].get(nameSpace+'Product_Name__c'));
				pp.Type__c = loadType;
				pp.External_ID__c = pp.Account_Number__c + '_'+pp.Product_Name__c;
				pp.Account__c = mapAccount.get(accountNumber);
				ppList.add(pp);	

				scope[i].put('Status__c', 'Processed');
                scope[i].put('SalesIQ_Error_Message__c', null);
                scope[i].put('isError__c', false);
			}
			scope[i].put('Change_Request__c', Ids);


			
		}
		if(scope.size()>0){
			SnTDMLSecurityUtil.updateRecords(scope, 'BatchPopulateChannelPreferenceData');
		}

		if(!ppList.IsEmpty())
        {
            SnTDMLSecurityUtil.printDebugMessage('channelpreferenceUpsertList :: ' + ppList);
            Schema.SObjectField externalID = Product_Priority__c.Fields.External_ID__c;
            Database.UpsertResult[] ds = Database.Upsert(ppList,externalID, false);
	        
	            //End of security review change
            for(Database.UpsertResult d : ds)
            {
                if(d.isSuccess())
                {   
                    recordsUpdated++;
                }
                else
                {
                    flag = false;
                    for(Database.Error err : d.getErrors()) {
                        SnTDMLSecurityUtil.printDebugMessage('The following error has occurred.');                    
                        SnTDMLSecurityUtil.printDebugMessage(err.getStatusCode() + ': ' + err.getMessage());
                        SnTDMLSecurityUtil.printDebugMessage('Fields that affected this error: ' + err.getFields());
                    }
                }

            }
        } 
    }

    global void finish(Database.BatchableContext BC) 
    {
        Boolean noJobErrors;
        String changeReqStatus;

    	if(Ids != null)
        {	
            AxtriaSalesIQTM__Change_Request__c changerequest = new AxtriaSalesIQTM__Change_Request__c();
            changerequest.id = Ids;
            noJobErrors = ST_Utility.getJobStatus(BC.getJobId());
            changeReqStatus = flag && noJobErrors ? 'Done' : 'Error';

            /*if(flag && ST_Utility.getJobStatus(BC.getJobId()))
            {
                changerequest.Job_Status__c = 'Done';
            }
            else
            {
                changerequest.Job_Status__c = 'Error';
            }*/
            changerequest.Records_Updated__c = recordsUpdated;
            List<AxtriaSalesIQTM__Change_Request__c> changeRequestCreation = [Select id, AxtriaSalesIQTM__Request_Type_Change__c, Records_Created__c from AxtriaSalesIQTM__Change_Request__c where id = : Ids ];
            if(changeRequestCreation.size() > 0 && changeRequestCreation[0].AxtriaSalesIQTM__Request_Type_Change__c == 'Data Load Backend')
            {
                changerequest.Records_Created__c = totalRecords;
            }

            //update changerequest;
            SnTDMLSecurityUtil.updateRecords(changerequest, 'BatchPopulateChannelPreferenceData');

            BatchUpdateTempObjRecsCR batchCall = new BatchUpdateTempObjRecsCR(Ids,true,'Mandatory Field Missing or Incorrect Team Instance',changeReqStatus);
            Database.executeBatch(batchCall,2000);
            //Direct_Load_Records_Status_Update.Direct_Load_Records_Status_Update(Ids, 'BatchPopulateChannelPreferenceData');
        }
    }
}