global class Batch_Integration_TSF_Extended implements Database.Batchable<sObject> {
    
    public String query;
    public List<String> allProcessedIds;
    public List<String> allTeamInstances;

    public List<SIQ_TSF_vod_O__c> allStagingTSF;


    global Batch_Integration_TSF_Extended(List<String> allProcessedIds) {
        this.allProcessedIds = allProcessedIds;
        this.query = 'select id, Status__c,SIQ_My_Target_vod__c from SIQ_TSF_vod_O__c where SIQ_External_ID_AZ__c NOT IN :allProcessedIds';

    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);

    }

    global void execute(Database.BatchableContext BC, list<SIQ_TSF_vod_O__c> scope) {
        
        for(SIQ_TSF_vod_O__c tsf : scope)
         {
            tsf.SIQ_My_Target_vod__c = false;
            tsf.Status__c = 'Deleted';
         }

         update scope;
    }

    global void finish(Database.BatchableContext BC) {

    }
}