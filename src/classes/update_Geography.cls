global with sharing class update_Geography implements Database.Batchable<sObject>, Database.Stateful{
    
    global string query;
    global string teamID;
    global string countrycode {get;set;}
    global map<String, String> mapCountryCode ;
    global list<AxtriaSalesIQTM__Country__c>countrylist {get;set;}
    global list<AxtriaSalesIQTM__Team__c> teamList= new list<AxtriaSalesIQTM__Team__c>() ;
    global map<string,string>geotypetoidmap {get;set;}
    global update_Geography(String country){
        countrycode = '';
        countrycode = country;
        mapCountryCode = new map<String, String>();
        geotypetoidmap = new map<string,string>();

        countrylist=[Select Id,AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c  WHERE AxtriaSalesIQTM__Country_Code__c != NULL WITH SECURITY_ENFORCED] ;
        for(AxtriaSalesIQTM__Country__c c : countrylist){
            if(!mapCountryCode.containskey(c.AxtriaSalesIQTM__Country_Code__c)){
                mapCountryCode.put(c.AxtriaSalesIQTM__Country_Code__c,c.id);
            }
        }
        list<AxtriaSalesIQTM__Geography_Type__c>geotype = [select id,AxtriaSalesIQTM__Country__c,AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,name from  AxtriaSalesIQTM__Geography_Type__c where AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c =: countrycode WITH SECURITY_ENFORCED];
        if(geotype!=null && geotype.size() >0){
            for(AxtriaSalesIQTM__Geography_Type__c type :geotype){
                geotypetoidmap.put(type.name,type.id);
            }
        }
        query='select id,name,AxtriaSalesIQTM__Parent_Zip__c,AxtriaSalesIQTM__Parent_Zip_Code__c,Geography_Type1__c,Country_Code__c ,AxtriaSalesIQTM__External_Geo_Type__c ,AxtriaSalesIQTM__Zip_Type__c  from AxtriaSalesIQTM__Geography__c where Country_Code__c =:countrycode WITH SECURITY_ENFORCED';
       SnTDMLSecurityUtil.printDebugMessage('========Query is :::'+query); 
    }
    
    global Database.Querylocator start(Database.BatchableContext bc){
        SnTDMLSecurityUtil.printDebugMessage('============'+Database.getQueryLocator(query)+'===='+teamID);
        return Database.getQueryLocator(query);
        
    }
    
    global void execute (Database.BatchableContext BC, List<AxtriaSalesIQTM__geography__c>scope){
        
       SnTDMLSecurityUtil.printDebugMessage('==========geotypetoidmap:::'+geotypetoidmap);
       SnTDMLSecurityUtil.printDebugMessage('==========mapCountryCode::::'+mapCountryCode);
       //AxtriaSalesIQTM__Parent_Zip__c
       set<string>parentzipsset = new set<string>();
       for(AxtriaSalesIQTM__geography__c geo : scope)
       {
            parentzipsset.add(geo.AxtriaSalesIQTM__Parent_Zip__c);
       }
        SnTDMLSecurityUtil.printDebugMessage('==========geotypetoidmap::::'+geotypetoidmap.keyset());
       /*Updating the scope with below 3 fields */
       for(AxtriaSalesIQTM__geography__c geo : scope)
       {
          
         geo.AxtriaSalesIQTM__Geography_Type__c =  geotypetoidmap.get(geo.Geography_Type1__c);
         geo.AxtriaSalesIQTM__External_Geo_Type__c = (string)geotypetoidmap.get(geo.Geography_Type1__c);
         geo.AxtriaSalesIQTM__External_Country_Id__c = (string)mapCountryCode.get(countrycode);
       }
       //update scope;
       SnTDMLSecurityUtil.updateRecords(scope, 'update_Geography');

       SnTDMLSecurityUtil.printDebugMessage('======parentzips::::'+parentzipsset);
       map<string,string>parentzipmap = new map<string,string>();
       list<AxtriaSalesIQTM__geography__c>parentzips = [select id,name,AxtriaSalesIQTM__Parent_Zip__c,Geography_Type1__c from AxtriaSalesIQTM__geography__c where name in :parentzipsset and Country_Code__c=:countrycode WITH SECURITY_ENFORCED];
       if(parentzips!=null && parentzips.size()>0)
       {
        for(AxtriaSalesIQTM__geography__c gg : parentzips)
        {
            string key = gg.name+'_'+gg.Geography_Type1__c;
            parentzipmap.put(key,gg.id);
        }
       }
       SnTDMLSecurityUtil.printDebugMessage('================parentzipmap.keyset()::::'+parentzipmap);
       /* Filling hte parent ZIP lookup field as parent zip should be picked with respect to geography type*/
       list<AxtriaSalesIQTM__geography__c>updategeography = new list<AxtriaSalesIQTM__geography__c>();
       for(AxtriaSalesIQTM__geography__c g : scope)
       {
            string key = g.AxtriaSalesIQTM__Parent_Zip__c+'_'+g.Geography_Type1__c;
            
            SnTDMLSecurityUtil.printDebugMessage('============keys is:::::'+key);
            if(parentzipmap.containskey(key))
            {   
                String val = parentzipmap.get(key);
                SnTDMLSecurityUtil.printDebugMessage('============Geography rec id is::'+g.id);
                SnTDMLSecurityUtil.printDebugMessage('============Key value is========'+val);
                
                if(g.id !=val)
                {
                    g.AxtriaSalesIQTM__Parent_Zip_Code__c = parentzipmap.get(key);
                    updategeography.add(g);  
                }
                
                
            }
            //SnTDMLSecurityUtil.printDebugMessage('==========Parent ZIP Code is:::::'+g.AxtriaSalesIQTM__Parent_Zip_Code__c);
       }
       //Database.update(scope,false);
       //update scope;
       //update updategeography;
        SnTDMLSecurityUtil.updateRecords(updategeography, 'update_Geography');

    }
    
    global void finish(Database.BatchableContext BC)
    {
    }
}