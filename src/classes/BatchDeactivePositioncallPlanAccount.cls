global with sharing class BatchDeactivePositioncallPlanAccount implements Database.Batchable<sObject>, Database.Stateful, Schedulable{
    
     Public String Country=null;
       Set<String> callPlanIDs = new Set<String>();

        global BatchDeactivePositioncallPlanAccount(){   
        
        }
    
     global BatchDeactivePositioncallPlanAccount(String Country){   
        this.Country = Country;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query;
        if(Country!= null){

            query = 'SELECT Id, Name, AxtriaSalesIQTM__Account__c,Party_ID__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__lastApprovedTarget__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where (AxtriaSalesIQTM__lastApprovedTarget__c = true or AxtriaSalesIQTM__isincludedCallPlan__c = true) and AxtriaSalesIQTM__Account__c != null and AxtriaSalesIQTM__Position__c != null and AxtriaSalesIQTM__Team_Instance__c != null and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name =: Country and (Party_ID__c = null or Party_ID__r.AxtriaSalesIQTM__Assignment_Status__c = \'InActive\') ';
        }   

        else{

            query = 'SELECT Id, Name, AxtriaSalesIQTM__Account__c,Party_ID__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__lastApprovedTarget__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where (AxtriaSalesIQTM__lastApprovedTarget__c = true or AxtriaSalesIQTM__isincludedCallPlan__c = true) and AxtriaSalesIQTM__Account__c != null and AxtriaSalesIQTM__Position__c != null and AxtriaSalesIQTM__Team_Instance__c != null and (Party_ID__c = null or Party_ID__r.AxtriaSalesIQTM__Assignment_Status__c = \'InActive\') ';
        }
        
        system.debug('Position Account Call Plan Records:::::::::::::' + query);
        
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scope) 
    {
        System.debug(Scope);
       Set<String> positionCallplan = new Set<String>(); 
       Set<String> teaminstanceCallplan  = new Set<String>();
       Set<String> accountCallplan  = new Set<String>();
        Set<String> keyCallPlanSet  = new Set<String>();
        Map<String,Set<String>> mapCallPlanKey2IDSet= new Map<String,Set<String>>(); 
        
        
        system.debug('keyCallPlanSet:::::::::' + keyCallPlanSet);
        system.debug('mapCallPlanKey2IDSet:::::::::' + mapCallPlanKey2IDSet);
        
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacpRec : scope)
       {
            if(pacpRec.Party_ID__c != null)
            {
               positionCallplan.add(pacpRec.AxtriaSalesIQTM__Position__c);
               accountCallplan.add(pacpRec.AxtriaSalesIQTM__Account__c);
               teaminstanceCallplan.add(pacpRec.AxtriaSalesIQTM__Team_Instance__c);
                String keycombination = pacpRec.AxtriaSalesIQTM__Position__c +'_' + pacpRec.AxtriaSalesIQTM__Account__c + '_' + pacpRec.AxtriaSalesIQTM__Team_Instance__c;
                
                keyCallPlanSet.add(keycombination); 
                system.debug('keycombination:::::::::' + keycombination);
               
                system.debug('keyCallPlanSet:::::::::' + keyCallPlanSet);
                
                if(!mapCallPlanKey2IDSet.containsKey(keycombination))
                {
                    Set<String> temp= new Set<String>();
                    temp.add(pacpRec.id);
                    mapCallPlanKey2IDSet.put(keycombination,temp);
                }
                else
                {
                    mapCallPlanKey2IDSet.get(keycombination).add(pacpRec.id);
                } 
            }
        }
         set<string>removedrec = new set<string>();
        List <AxtriaSalesIQTM__Position_Account__c> posAccList = [Select id,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Assignment_Status__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position_Account__c where (AxtriaSalesIQTM__Assignment_Status__c = 'Active' or AxtriaSalesIQTM__Assignment_Status__c = 'Future Active') and AxtriaSalesIQTM__Account__c in :accountCallplan and AxtriaSalesIQTM__Position__c in :positionCallplan and AxtriaSalesIQTM__Team_Instance__c in: teaminstanceCallplan and AxtriaSalesIQTM__Account__c != null and AxtriaSalesIQTM__Position__c != null and AxtriaSalesIQTM__Team_Instance__c != null];
        System.debug('Position Account relted to Call Plan::::::::::::::' +posAccList );
                  
        for(AxtriaSalesIQTM__Position_Account__c posAccRec : posAccList)
        {
           String positionaccountkey = posAccRec.AxtriaSalesIQTM__Position__c +'_'+ posAccRec.AxtriaSalesIQTM__Account__c +'_'+ posAccRec.AxtriaSalesIQTM__Team_Instance__c;

           if(keyCallPlanSet.contains(positionaccountkey))
           {
                keyCallPlanSet.remove(positionaccountkey);
                removedrec.add(positionaccountkey);
               System.debug('keyCallPlanSetremove:::::::' + keyCallPlanSet);
           }
               
              
        }
        system.debug('===========Removed Rec are :'+removedrec); 
        //System.debug('Final Set of Call plan key where Position Account does not exist::::::::::::::' +keyCallPlanSet);
        for(String callPlanKeyRec : keyCallPlanSet)
        {
           // Verify the removed records 
            if(removedrec.contains(callPlanKeyRec))
            {
                system.debug('=============Remove is not happening properly check it======');
            }

            callPlanIDs.addAll(mapCallPlanKey2IDSet.get(callPlanKeyRec));
            System.debug('callPlanIDsvvvv::::::::::::::' +callPlanIDs);
        }
        System.debug(callPlanIDs);
        System.debug('Final Set of Call plan IDs where Position Account does not exist::::::::::::::' +callPlanIDs);
       
        System.debug(':::::::::::::::::::::::::::Call Plan Handling::::::::::::::::::::::::::::::');
         List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> mailcallplanids = [Select id,AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__r.name from AxtriaSalesIQTM__Position_Account_Call_Plan__c where id in: callPlanIDs];
       
       for(AxtriaSalesIQTM__Position_Account_Call_Plan__c callPlannIDs : mailcallplanids){
       
           CallPlannIDs.AxtriaSalesIQTM__lastApprovedTarget__c = false;
           CallPlannIDs.AxtriaSalesIQTM__isincludedCallPlan__c = false;
         }         
         update Mailcallplanids; 
    }  
    global void execute(SchedulableContext sc){
        /*List<Veeva_Job_Scheduling__c> fetchAllCountries = [Select Country__r.Name from Veeva_Job_Scheduling__c where Load_Type__c != 'No Load'];
            system.debug(fetchAllCountries + 'Countr ');
            
        for(Veeva_Job_Scheduling__c Coun : fetchAllCountries){
            system.debug(fetchAllCountries + 'Coun');
            database.executeBatch(new BatchDeactivePositioncallPlanAccount(Coun.Country__r.Name),10);     
        } */
         /*Replaced Veeva_Job_Scheduling__c with field at country*/
        List<AxtriaSalesIQTM__Country__c> fetchAllCountries = [Select Name from AxtriaSalesIQTM__Country__c where Load_Type__c != 'No Load'];
            system.debug(fetchAllCountries + 'Countr ');
            
        for(AxtriaSalesIQTM__Country__c Coun : fetchAllCountries){
            system.debug(fetchAllCountries + 'Coun');
            database.executeBatch(new BatchDeactivePositioncallPlanAccount(Coun.Name),10);     
        }
    }  
  
        
    
    global void finish(Database.BatchableContext BC) {
            
    }
}