/**********************************************************************************************
@author       : Himanshu Tariyal (A0994)
@createdDate  : 15th August 2020
@description  : Test class for BatchPopulateProductSegment
@Revision(s)  : v1.0
**********************************************************************************************/
@isTest
private class BatchPopulateProductSegmentTest 
{
	//Data added via UI based test
    public static TestMethod void testMethod1() 
    {
    	String className = 'BatchPopulateProductSegmentTest';

    	//Create Org Master rec
    	AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    	SnTDMLSecurityUtil.insertRecords(orgmas,className);

    	//Create Country Master
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);

        //Create Team
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);

        //Create Workspace
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today().addDays(90));
        workspace.Name = 'WS';
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);

        //Create Team Instance
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins,className);

        //Create Scenario
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        SnTDMLSecurityUtil.insertRecords(scen,className);

        //Create Product Catalog
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        pcc.Product_Code__c='PROD1';
        pcc.Veeva_External_ID__c='PROD1';
        pcc.Team_Instance__c=null;
        SnTDMLSecurityUtil.insertRecords(pcc,className);

        //Create Account
        Account acc = new Account();
		acc.Name = 'Acc1';
		acc.AccountNumber = 'Acc1';
		acc.Marketing_Code__c = 'US';
		SnTDMLSecurityUtil.insertRecords(acc,className);

		//Create Change Request
		AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
		cr.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.Id;
		SnTDMLSecurityUtil.insertRecords(cr,className);

		//Create Temp Obj recs
		List<temp_Obj__c> tempObjList = new List<temp_Obj__c>();

		temp_Obj__c rec1 = new temp_Obj__c();
		rec1.Segment__c = '';
		rec1.Change_Request__c = cr.Id;
		tempObjList.add(rec1);

		temp_Obj__c rec2 = new temp_Obj__c();
		rec2.Segment__c = 'B';
		rec2.AccountNumber__c = '';
		rec2.Change_Request__c = cr.Id;
		tempObjList.add(rec2);

		temp_Obj__c rec3 = new temp_Obj__c();
		rec3.Segment__c = 'A';
		rec3.AccountNumber__c = 'Acc1';
		rec3.Product_Code__c = '';
		rec3.Change_Request__c = cr.Id;
		tempObjList.add(rec3);

		temp_Obj__c rec4 = new temp_Obj__c();
		rec4.Segment__c = 'A';
		rec4.Product_Code__c = 'PROD1';
		rec4.AccountNumber__c = 'Acc1';
		rec4.Workspace__c = '';
		rec4.Change_Request__c = cr.Id;
		tempObjList.add(rec4);

		temp_Obj__c rec5 = new temp_Obj__c();
		rec5.Segment__c = 'A';
		rec5.Workspace__c = 'WS';
		rec5.Product_Code__c = 'PROD1';
		rec5.AccountNumber__c = 'Acc2';
		rec5.Change_Request__c = cr.Id;
		tempObjList.add(rec5);

		temp_Obj__c rec6 = new temp_Obj__c();
		rec6.Segment__c = 'A';
		rec6.Workspace__c = 'WS';
		rec6.AccountNumber__c = 'Acc1';
		rec6.Product_Code__c = 'PROD2';
		rec6.Change_Request__c = cr.Id;
		tempObjList.add(rec6);

		temp_Obj__c rec7 = new temp_Obj__c();
		rec7.Segment__c = 'A';
		rec7.Workspace__c = 'WS';
		rec7.AccountNumber__c = 'Acc1';
		rec7.Product_Code__c = 'PROD1';
		rec7.Change_Request__c = cr.Id;
		tempObjList.add(rec7);

		temp_Obj__c rec8 = new temp_Obj__c();
		rec8.Segment__c = 'A';
		rec8.Workspace__c = 'WS';
		rec8.AccountNumber__c = 'Acc1';
		rec8.Product_Code__c = 'PROD1';
		rec8.Change_Request__c = cr.Id;
		tempObjList.add(rec8);
		SnTDMLSecurityUtil.insertRecords(tempObjList,className); 

        System.Test.startTest();

        //Check FLS of HCP__c field in MCCP DataLoad object
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = :className];
        String namespace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';

        List<String> MCCP_DATALOAD_READFIELD = new List<String>{namespace+'HCP__c'};
    	System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(MCCP_DataLoad__c.SObjectType,MCCP_DATALOAD_READFIELD,false));

        //Test BatchPopulateProductSegment
        Database.executeBatch(new BatchPopulateProductSegment(cr.Id,countr.Id,workspace.Id),2000);

        System.Test.stopTest();
    }

    //Data added directly in Temp obj based test
    public static TestMethod void testMethod2() 
    {
    	String className = 'BatchPopulateProductSegmentTest';

    	//Create Org Master rec
    	AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    	SnTDMLSecurityUtil.insertRecords(orgmas,className);

    	//Create Country Master
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);

        //Create Team
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);

        //Create Workspace
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today().addDays(90));
        workspace.Name = 'WS';
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);

        //Create Team Instance
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins,className);

        //Create Scenario
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        SnTDMLSecurityUtil.insertRecords(scen,className);

        //Create Product Catalog
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        pcc.Product_Code__c='PROD1';
        pcc.Veeva_External_ID__c='PROD1';
        pcc.Team_Instance__c=null;
        SnTDMLSecurityUtil.insertRecords(pcc,className);

        //Create Account
        Account acc = new Account();
		acc.Name = 'Acc1';
		acc.AccountNumber = 'Acc1';
		acc.Marketing_Code__c = 'US';
		SnTDMLSecurityUtil.insertRecords(acc,className);

		//Create Account
        Account acc2 = new Account();
		acc2.Name = 'Acc2';
		acc2.AccountNumber = 'Acc2';
		acc2.Marketing_Code__c = 'US';
		SnTDMLSecurityUtil.insertRecords(acc2,className);

		//Create Change Request
		AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
		cr.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.Id;
		SnTDMLSecurityUtil.insertRecords(cr,className);

		//Create Temp Obj recs
		List<temp_Obj__c> tempObjList = new List<temp_Obj__c>();

		temp_Obj__c rec1 = new temp_Obj__c();
		rec1.Segment__c = '';
		rec1.Change_Request__c = cr.Id;
		rec1.Status__c = 'New';
		rec1.Object_Name__c = 'MCCP Product Segment';
		tempObjList.add(rec1);

		temp_Obj__c rec2 = new temp_Obj__c();
		rec2.Segment__c = 'B';
		rec2.AccountNumber__c = '';
		rec2.Change_Request__c = cr.Id;
		rec2.Status__c = 'New';
		rec2.Object_Name__c = 'MCCP Product Segment';
		tempObjList.add(rec2);

		temp_Obj__c rec3 = new temp_Obj__c();
		rec3.Segment__c = 'A';
		rec3.AccountNumber__c = 'Acc1';
		rec3.Product_Code__c = '';
		rec3.Change_Request__c = cr.Id;
		rec3.Status__c = 'New';
		rec3.Object_Name__c = 'MCCP Product Segment';
		tempObjList.add(rec3);

		temp_Obj__c rec4 = new temp_Obj__c();
		rec4.Segment__c = 'A';
		rec4.Product_Code__c = 'PROD1';
		rec4.AccountNumber__c = 'Acc1';
		rec4.Workspace__c = '';
		rec4.Change_Request__c = cr.Id;
		rec4.Status__c = 'New';
		rec4.Object_Name__c = 'MCCP Product Segment';
		tempObjList.add(rec4);

		temp_Obj__c rec5 = new temp_Obj__c();
		rec5.Segment__c = 'A';
		rec5.Workspace__c = 'WS';
		rec5.Product_Code__c = 'PROD1';
		rec5.AccountNumber__c = 'Acc3';
		rec5.Change_Request__c = cr.Id;
		rec5.Status__c = 'New';
		rec5.Object_Name__c = 'MCCP Product Segment';
		tempObjList.add(rec5);

		temp_Obj__c rec6 = new temp_Obj__c();
		rec6.Segment__c = 'A';
		rec6.Workspace__c = 'WS';
		rec6.AccountNumber__c = 'Acc1';
		rec6.Product_Code__c = 'PROD2';
		rec6.Change_Request__c = cr.Id;
		rec6.Status__c = 'New';
		rec6.Object_Name__c = 'MCCP Product Segment';
		tempObjList.add(rec6);

		temp_Obj__c rec7 = new temp_Obj__c();
		rec7.Segment__c = 'A';
		rec7.Workspace__c = 'WS';
		rec7.AccountNumber__c = 'Acc1';
		rec7.Product_Code__c = 'PROD1';
		rec7.Change_Request__c = cr.Id;
		rec7.Status__c = 'New';
		rec7.Object_Name__c = 'MCCP Product Segment';
		tempObjList.add(rec7);

		temp_Obj__c rec8 = new temp_Obj__c();
		rec8.Segment__c = 'A';
		rec8.Workspace__c = 'WS';
		rec8.AccountNumber__c = 'Acc1';
		rec8.Product_Code__c = 'PROD1';
		rec8.Change_Request__c = cr.Id;
		rec8.Status__c = 'New';
		rec8.Object_Name__c = 'MCCP Product Segment';
		tempObjList.add(rec8);

		temp_Obj__c rec9 = new temp_Obj__c();
		rec9.Segment__c = 'B';
		rec9.Workspace__c = 'WS';
		rec9.AccountNumber__c = 'Acc2';
		rec9.Product_Code__c = 'PROD1';
		rec9.Change_Request__c = cr.Id;
		rec9.Status__c = 'New';
		rec9.Object_Name__c = 'MCCP Product Segment';
		tempObjList.add(rec9);
		SnTDMLSecurityUtil.insertRecords(tempObjList,className); 

		//Create MCCP Unique Segment data
		MCCP_DataLoad__c data1 = new MCCP_DataLoad__c();
		data1.RecordTypeID = Schema.SObjectType.MCCP_DataLoad__c.getRecordTypeInfosByName().get('Unique Segment').getRecordTypeId();
		data1.Product__c = pcc.Id;
		data1.Country__c = countr.Id;
		data1.Workspace__c = workspace.Id;
		data1.ExternalID__c = data1.RecordTypeID+'_'+countr.Id+'_'+workspace.Id+'_'+pcc.Id;
		SnTDMLSecurityUtil.insertRecords(data1,className);

        System.Test.startTest();

        //Check FLS of HCP__c field in MCCP DataLoad object
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = :className];
        String namespace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';

        List<String> MCCP_DATALOAD_READFIELD = new List<String>{namespace+'HCP__c'};
    	System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(MCCP_DataLoad__c.SObjectType,MCCP_DATALOAD_READFIELD,false));

        //Test BatchPopulateProductSegment
        Database.executeBatch(new BatchPopulateProductSegment(cr.Id,'MCCP Product Segment',countr.Id,workspace.Id),2000);

        System.Test.stopTest();
    }
}