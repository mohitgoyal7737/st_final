@isTest
private class FillExternalIDonPositionProduct_Test{

    static testMethod void testMethod1(){

    	String className = 'FillExternalIDonPositionProduct_Test';

        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c country = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(country,className);

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(country);
        SnTDMLSecurityUtil.insertRecords(team,className);

        AxtriaSalesIQTM__Team_Instance__c teamInst = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamInst,className);

        AxtriaSalesIQTM__Position__c position = TestDataFactory.createPosition(team, teamInst);
        SnTDMLSecurityUtil.insertRecords(position,className);

        Product_Catalog__c prodCatalog = TestDataFactory.productCatalog(team, teamInst, country);
        SnTDMLSecurityUtil.insertRecords(prodCatalog,className);
        
        AxtriaSalesIQTM__TriggerContol__c triggerActive = new AxtriaSalesIQTM__TriggerContol__c();
        triggerActive.Name = 'FillExternalIDonPosProduct';
        triggerActive.AxtriaSalesIQTM__IsStopTrigger__c  = false;
        SnTDMLSecurityUtil.insertRecords(triggerActive,className);

        AxtriaSalesIQTM__Position_Product__c posProd = TestDataFactory.createPositionProduct(teamInst, position, prodCatalog);
        SnTDMLSecurityUtil.insertRecords(posProd,className);
        
        posProd.External_ID__c= 'Test2222';
        update posProd;
        SnTDMLSecurityUtil.updateRecords(posProd,className);

        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
    }
}