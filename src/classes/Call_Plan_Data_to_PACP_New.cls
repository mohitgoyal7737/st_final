global with sharing class Call_Plan_Data_to_PACP_New implements Database.Batchable<sObject>,Database.Stateful   
{
    //Error_Object__c replaced with AxtriaSalesIQTM__SalesIQ_Logger__c due to object purge activity
    global string queri;
    global string teamID;
    public string Ids;
    Boolean flag= true;
    public integer recordsProcessed;
    public String selectedTeamInstance;
     public boolean affiliationFlag; // check whether call plan is HCP level or HCp-HCO level                        
    //global string teamInstance;
   
    
    public AxtriaSalesIQTM__TriggerContol__c customsetting ;
    public AxtriaSalesIQTM__TriggerContol__c customsetting2 ;
   // public boolean affiliationFlag;
    public list<AxtriaSalesIQTM__TriggerContol__c>customsettinglist {get;set;}

    //multichannel Code by Riya
    List<AxtriaSalesIQTM__Team_Instance__c> teamMultiChannel= new List<AxtriaSalesIQTM__Team_Instance__c>();
    public Boolean multiChannel;
    List<Veeva_Channels__mdt> channels;
    List<String> allchannels;
    Map<String,String> channelsmapping;

    //Added by HT(A0944) on 12th June 2020
    public String alignNmsp;
    public String batchName;
    public String callPlanLoggerID;
    public Boolean proceedNextBatch = true;
    
    //Added by HT(A0994) on 17th June 2020  
    global String changeReqID;  
    global Boolean flagValue = false;

    //Added by HT(A0944) on 12th June 2020
    global Call_Plan_Data_to_PACP_New(String teamInstance,String loggerID,String alignNmspPrefix)
    {
       } 
    
    global Call_Plan_Data_to_PACP_New(String teamInstance)
    {
        } //Corresponding_HCP__c,Gain_Loss_Position__c,
    
    global Call_Plan_Data_to_PACP_New(String teamInstance,string Ids)
    {
         }
    
    //Added by HT(A0944) on 17th June 2020
    global Call_Plan_Data_to_PACP_new(String teamInstance,string Ids,Boolean flag)
    {
       
    }

    global Database.Querylocator start(Database.BatchableContext bc)
    {
        SnTDMLSecurityUtil.printDebugMessage('*************************Checkpoint3');
        try{
        return Database.getQueryLocator(queri);
        }
        catch(Exception e){
        SnTDMLSecurityUtil.printDebugMessage(e.getMessage()); 
        return Database.getQueryLocator(queri);
        }
         
    } 
    
    global void execute (Database.BatchableContext BC, List<temp_Obj__c>callplnlist)
    {
                  
      }
    
           global void finish(Database.BatchableContext BC){
           }
}