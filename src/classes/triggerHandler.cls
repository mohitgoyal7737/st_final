public class triggerHandler{

    public static List<AxtriaSalesIQTM__Team_Instance__c> updateTeamInstanceCode(List<AxtriaSalesIQTM__Team_Instance__c> newRecords){
        Integer maxCount = getTeamCodeMaxCount('TeamInstanceCodeSequence');
        for(AxtriaSalesIQTM__Team_Instance__c teamIns :newRecords){
            teamIns.AxtriaSalesIQTM__Team_Instance_Code__c = 'TI-'+String.valueOf(maxCount).leftpad(5).replaceAll(' ', '0');
            maxCount+=1;
        }
        updateAlignmentGlobalSettingValue('TeamInstanceCodeSequence',String.valueOf(maxCount));
        return newRecords;
    }
    
    public static integer getTeamCodeMaxCount(String recordName){
        if(AxtriaSalesIQTM__Alignment_Global_Settings__c.getValues(recordName)!=null){
            return Integer.valueOf(AxtriaSalesIQTM__Alignment_Global_Settings__c.getValues(recordName).AxtriaSalesIQTM__Tree_Hierarchy_Sort_Field__c) + 1;
        }
        else if(recordName == 'TeamCodeSequence'){
            if(AxtriaSalesIQTM__Alignment_Global_Settings__c.getValues('TeamCodeMax')!=null){
                upsert new AxtriaSalesIQTM__Alignment_Global_Settings__c(Name=recordName, AxtriaSalesIQTM__Tree_Hierarchy_Sort_Field__c = AxtriaSalesIQTM__Alignment_Global_Settings__c.getValues('TeamCodeMax').AxtriaSalesIQTM__Tree_Hierarchy_Sort_Field__c);
                delete AxtriaSalesIQTM__Alignment_Global_Settings__c.getValues('TeamCodeMax');
                return Integer.valueOf(AxtriaSalesIQTM__Alignment_Global_Settings__c.getValues(recordName).AxtriaSalesIQTM__Tree_Hierarchy_Sort_Field__c) + 1;
            }
            else{              
                upsert new AxtriaSalesIQTM__Alignment_Global_Settings__c( Name = recordName, AxtriaSalesIQTM__Tree_Hierarchy_Sort_Field__c = '1');
                return 1;
            }
        }
        else if(recordName == 'TeamInstanceCodeSequence'){
           if(AxtriaSalesIQTM__Alignment_Global_Settings__c.getValues('TeamInstanceCodeMax')!=null){
                upsert new AxtriaSalesIQTM__Alignment_Global_Settings__c(Name=recordName, AxtriaSalesIQTM__Tree_Hierarchy_Sort_Field__c = AxtriaSalesIQTM__Alignment_Global_Settings__c.getValues('TeamInstanceCodeMax').AxtriaSalesIQTM__Tree_Hierarchy_Sort_Field__c);
                delete AxtriaSalesIQTM__Alignment_Global_Settings__c.getValues('TeamInstanceCodeMax');
                return Integer.valueOf(AxtriaSalesIQTM__Alignment_Global_Settings__c.getValues(recordName).AxtriaSalesIQTM__Tree_Hierarchy_Sort_Field__c) + 1;
            }
            else{              
                upsert new AxtriaSalesIQTM__Alignment_Global_Settings__c( Name = recordName, AxtriaSalesIQTM__Tree_Hierarchy_Sort_Field__c = '1');
                return 1;
            }
        }else{
           return 0; 
        }
    }
    
    public static void updateAlignmentGlobalSettingValue(String recordName, String customValue){
        if( AxtriaSalesIQTM__Alignment_Global_Settings__c.getValues(recordName) == null){
            upsert new AxtriaSalesIQTM__Alignment_Global_Settings__c(
                Name = recordName,
                AxtriaSalesIQTM__Tree_Hierarchy_Sort_Field__c = customValue
            );
        }
        else{
            AxtriaSalesIQTM__Alignment_Global_Settings__c inst = AxtriaSalesIQTM__Alignment_Global_Settings__c.getValues(recordName);
            inst.AxtriaSalesIQTM__Tree_Hierarchy_Sort_Field__c = customValue;
            update inst;
        }
    }



}