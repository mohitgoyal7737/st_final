global with sharing class BatchDeleteSurveyData implements Database.Batchable<sObject> {
    public String query;

    global BatchDeleteSurveyData() {
        query = '';
        query = 'SELECT Id FROM Staging_Veeva_Cust_Data__c WITH SECURITY_ENFORCED';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Staging_Veeva_Cust_Data__c> scope) {
        
        System.debug('scope size::::::: ' +scope.size());
        //delete scope;
         SnTDMLSecurityUtil.deleteRecords(scope, 'BatchDeleteSurveyData');
    }

    global void finish(Database.BatchableContext BC) {

        ID jobID = System.enqueueJob(new TalendSurveyLoadHitQueueable());
        System.debug('====jobID :::::::::' +jobID);
        
    }
}