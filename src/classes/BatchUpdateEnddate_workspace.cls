public class BatchUpdateEnddate_workspace implements database.batchable<sobject> {
                   
    public string query ;
    public string workspaceID = '';
    public Date newdatetoupdate;
    public String objName = '';
    public String fieldtoupdate ;
    public String condition = '';
    List<workspacerelatedobject__c> relatedobj;
    
    //fetch custom setting in constructor
    //field object where clause
     public BatchUpdateEnddate_workspace(String Workspace, Date Newdate ){
        workspaceID = Workspace;
        newdatetoupdate = Newdate;
        //objName = ObjectName;
        //activefield = Fieldname;
        system.debug('workspaceID+++++++ '+workspaceID);
        //query = 'SELECT Id FROM '+objName +' where '+activefield +' = \'Active\'';
        
        relatedobj = workspacerelatedobject__c.getAll().values();
        fieldtoupdate = relatedobj[0].fieldname__c;
        query = ' Select id,' +relatedobj[0].fieldname__c+' FROM '+relatedobj[0].Objectname__c+' where '+relatedobj[0].whereclause__c+'=\''+Workspace+'\'';
        system.debug('query1 +++++++' + query);
                     
    }
    
    public BatchUpdateEnddate_workspace(String Workspace, Date Newdate,String ObjectName,String Fieldname,String Cond ){
        workspaceID = Workspace;
        newdatetoupdate = Newdate;
        objName = ObjectName;
        fieldtoupdate = Fieldname;
        condition = Cond;
        relatedobj = new List<workspacerelatedobject__c>();
        system.debug('workspaceID+++++++ '+workspaceID);
        //query = 'SELECT Id FROM '+objName +' where '+activefield +' = \'Active\'';
        system.debug('query +++++++' + query);

        query = ' Select id,' +fieldtoupdate+' FROM '+objName+' where '+condition+'=\''+Workspace+'\'';

        system.debug('query2 +++++++' + query);

        //query = 'Selct id,' +lstCust[0]field+;
                     
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC){         
        return Database.getQueryLocator(query);
    }      
    
    public void execute(Database.BatchableContext BC, List<sObject> scope)
    {
      
        for(sobject s : scope )
        {
            s.put(fieldtoupdate,newdatetoupdate);
            
        }

        update scope;
    }
    
    public void finish(Database.BatchableContext BC){
        
        //List<workspacerelatedobject__c> relatedobj = workspacerelatedobject__c.getAll().values();
        
        for(integer k=1; k<relatedobj.size();K++){
            BatchUpdateEnddate_workspace obj = new BatchUpdateEnddate_workspace(workspaceID, newdatetoupdate, relatedobj[k].Objectname__c,relatedobj[k].fieldname__c,relatedobj[k].whereclause__c);
            Database.executeBatch(obj,1000);
        }
    

        
    } 

}