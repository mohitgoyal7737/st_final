/**********************************************************************************************
Author      : Himanshu Tariyal (A0994)
Date        : 10th August'2020
Description : Test class for MCCPInputDataCtlr
Revision(s) : v1.0
**********************************************************************************************/
@isTest
public with sharing class MCCPInputDataCtlrTest 
{
	public static testMethod void testMethod1() 
    {
        String className = 'MCCPInputDataCtlrTest';

    	AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    	SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);

        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today().addDays(90));
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);

        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins,className);

        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        SnTDMLSecurityUtil.insertRecords(scen,className);

        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        pcc.Name = 'PROD1';
        pcc.Product_Code__c='PROD1';
        pcc.Veeva_External_ID__c='PROD1';
        SnTDMLSecurityUtil.insertRecords(pcc,className);

        Product_Catalog__c pcc2 = TestDataFactory.productCatalog(team, teamins, countr);
        pcc2.Name='PROD2';
        pcc2.Product_Code__c='PROD2';
        pcc2.Veeva_External_ID__c='PROD2';
        SnTDMLSecurityUtil.insertRecords(pcc2,className);

        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        mmc.State__c ='In Progress';
        mmc.Target_Count_Maximum__c = 100;
        mmc.Target_Count_Minimun__c = 20;
        mmc.Workload_Hours__c = 8;
        mmc.Desired_Range__c = '-20';
        mmc.Single_Product_Rule__c = true;
        mmc.Rule_Type__c = 'MCCP';
        mmc.MCCP_Selected_Products__c = 'PROD1,PROD2';
        mmc.MCCP_Selected_Channels__c = 'Email';
        SnTDMLSecurityUtil.insertRecords(mmc,className);

        Measure_Master__c mmc2 = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc2.Team_Instance__c = teamins.id;
        mmc2.State__c ='In Progress';
        mmc2.Brand_Lookup__c = pcc2.Id;
        mmc2.Target_Count_Minimun__c = 20;
        mmc2.Workload_Hours__c = 8;
        mmc2.Desired_Range__c = '-20';
        mmc2.Rule_Type__c = 'MCCP';
        mmc2.Single_Product_Rule__c = true;
        mmc2.MCCP_Selected_Products__c = 'PROD1,PROD2';
        mmc2.MCCP_Selected_Channels__c = 'Email';
        SnTDMLSecurityUtil.insertRecords(mmc2,className);

        //Mock Product Data
        MCCP_CNProd__c rec = new MCCP_CNProd__c();
        rec.Call_Plan__c = mmc.Id;
        rec.Product__c = 'PROD1';
        rec.Weights__c = 50;
        rec.RecordTypeId = Schema.SObjectType.MCCP_CNProd__c.getRecordTypeInfosByName().get('Product').getRecordTypeId();
        SnTDMLSecurityUtil.insertRecords(rec,className);

        //Mock Channel Data
        MCCP_CNProd__c rec2 = new MCCP_CNProd__c();
        rec2.Call_Plan__c = mmc.Id;
        rec2.CE_Priority1__c = 1;
        rec2.CE_Priority2__c = 1;
        rec2.WLE_TP_PerHour__c = 8;
        rec2.Channel__c = 'Email';
        rec2.RecordTypeId = Schema.SObjectType.MCCP_CNProd__c.getRecordTypeInfosByName().get('Channel').getRecordTypeId();
        SnTDMLSecurityUtil.insertRecords(rec2,className);

        //Mock Junction Data
        MCCP_CNProd__c rec3 = new MCCP_CNProd__c();
        rec3.Call_Plan__c = mmc.Id;
        rec3.Product__c = 'PROD1';
        rec3.Channel__c = 'Email';
        rec3.OptimisationSelected__c = true;
        rec3.PromotionSelected__c = true;
        rec3.RecordTypeId = Schema.SObjectType.MCCP_CNProd__c.getRecordTypeInfosByName().get('Junction').getRecordTypeId();
        SnTDMLSecurityUtil.insertRecords(rec3,className); //Insert MCCP CN Prod Data

        //Mock Segment Data
        MCCP_CNProd__c rec4 = new MCCP_CNProd__c();
        rec4.Call_Plan__c = mmc.Id;
        rec4.Product__c = 'PROD1';
        rec4.Channel__c = 'Email';
        rec4.Data_Source__c = 'Direct Load';
        rec4.Segment__c = 'A';
        rec4.RecordTypeId = Schema.SObjectType.MCCP_CNProd__c.getRecordTypeInfosByName().get('Segment').getRecordTypeId();
        SnTDMLSecurityUtil.insertRecords(rec4,className); //Insert MCCP CN Prod Data

        MCCP_CNProd__c rec5 = new MCCP_CNProd__c();
        rec5.Call_Plan__c = mmc.Id;
        rec5.Product__c = 'PROD1';
        rec5.Channel__c = 'Email';
        rec5.Segment__c = 'A';
        rec5.Data_Source__c = 'SalesIQ S&T Output';
        rec5.Rule_Name__c = mmc2.Id;
        rec5.RecordTypeId = Schema.SObjectType.MCCP_CNProd__c.getRecordTypeInfosByName().get('Segment').getRecordTypeId();
        SnTDMLSecurityUtil.insertRecords(rec5,className); //Insert MCCP CN Prod Data

        Channel_Info__c ci = new Channel_Info__c();
        ci.Channel_Name__c = 'Email';
        ci.Channel_Effectiveness_P1__c = '10';
        ci.Channel_Effectiveness_P2__c = '5';
        ci.Country__c = countr.Id;
        ci.Team_Instance__c = mmc.Team_Instance__c;
        SnTDMLSecurityUtil.insertRecords(ci,className); //Insert MCCP CN Prod Data

        //Create Unique Segment data
        List<MCCP_DataLoad__c> dataLoadList = new List<MCCP_DataLoad__c>();
        MCCP_DataLoad__c uniqueSeg = new MCCP_DataLoad__c();
        uniqueSeg.Product__c = pcc.Id;
        uniqueSeg.Unique_Segments__c = 'A';
        uniqueSeg.ExternalID__c = 'Ext1';
        uniqueSeg.Country__c = countr.Id;
        uniqueSeg.RecordTypeId = Schema.SObjectType.MCCP_DataLoad__c.getRecordTypeInfosByName().get('Unique Segment').getRecordTypeId();
        dataLoadList.add(uniqueSeg);

        MCCP_DataLoad__c uniqueChCon = new MCCP_DataLoad__c();
        uniqueChCon.Product__c = pcc.Id;
        uniqueChCon.Channel_Name__c = 'Email';
        uniqueChCon.ExternalID__c = 'Ext2';
        uniqueChCon.Country__c = countr.Id;
        uniqueChCon.RecordTypeId = Schema.SObjectType.MCCP_DataLoad__c.getRecordTypeInfosByName().get('Unique Channel Preference').getRecordTypeId();
        dataLoadList.add(uniqueChCon);

        MCCP_DataLoad__c uniqueChPref = new MCCP_DataLoad__c();
        uniqueChPref.Product__c = pcc.Id;
        uniqueChPref.Channel_Name__c = 'Email';
        uniqueChPref.ExternalID__c = 'Ext3';
        uniqueChPref.Country__c = countr.Id;
        uniqueChPref.RecordTypeId = Schema.SObjectType.MCCP_DataLoad__c.getRecordTypeInfosByName().get('Unique Channel Consent').getRecordTypeId();
        dataLoadList.add(uniqueChPref);
        SnTDMLSecurityUtil.insertRecords(dataLoadList,className);

        System.Test.startTest();

        String nameSpace = MCCP_Utility.sntNamespace(className);
        List<String> PROD_PRIORITY_READ_FIELD = new List<String>{nameSpace+'MCCP_Selected_Products__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Measure_Master__c.SObjectType, PROD_PRIORITY_READ_FIELD, false));

        //Test getting data for the selected Business Rule
        MCCPInputDataCtlr.getBusinessRuleData(null);
        MCCPInputDataCtlr.getBusinessRuleData('abcd');
        MCCPInputDataCtlr.getBusinessRuleData(mmc.Id);

        List<MCCPInputDataCtlr.ProductSegmentWrapper> prodList = new List<MCCPInputDataCtlr.ProductSegmentWrapper>();
        List<MCCPInputDataCtlr.ChannelConsentWrapper> chConList = new List<MCCPInputDataCtlr.ChannelConsentWrapper>();
        List<MCCPInputDataCtlr.ChannelPrefWrapper> chPrefList = new List<MCCPInputDataCtlr.ChannelPrefWrapper>();

        MCCPInputDataCtlr.ProductSegmentWrapper psw = new MCCPInputDataCtlr.ProductSegmentWrapper();
        psw.productName = 'PROD1';
        psw.dataSource = 'Direct Load';
        psw.ruleId = '';
        psw.ruleStatus = true;
        psw.dataSourceList = new List<String>{'Direct Load','SNT Output'};
        psw.ruleIDList = new List<Measure_Master__c>();
        prodList.add(psw);

        MCCPInputDataCtlr.ProductSegmentWrapper psw2 = new MCCPInputDataCtlr.ProductSegmentWrapper();
        psw2.productName = 'PROD2';
        psw2.dataSource = 'SNT Output';
        psw2.ruleId = mmc2.Id;
        psw2.ruleStatus = true;
        psw2.dataSourceList = new List<String>{'Direct Load','SNT Output'};
        psw2.ruleIDList = new List<Measure_Master__c>{mmc2};
        prodList.add(psw2);

        MCCPInputDataCtlr.ChannelConsentWrapper ccw = new MCCPInputDataCtlr.ChannelConsentWrapper();
        ccw.productName = 'PROD1';
        ccw.isDataLoaded = true;
        ccw.chConDataPresent = true;
        chConList.add(ccw);

        MCCPInputDataCtlr.ChannelConsentWrapper ccw2 = new MCCPInputDataCtlr.ChannelConsentWrapper();
        ccw2.productName = 'PROD2';
        ccw2.isDataLoaded = true;
        ccw2.chConDataPresent = true;
        chConList.add(ccw2);

        MCCPInputDataCtlr.ChannelPrefWrapper cpw = new MCCPInputDataCtlr.ChannelPrefWrapper();
        cpw.productName = 'PROD1';
        cpw.isDataLoaded = true;
        cpw.chPrefDataPresent = true;
        chPrefList.add(cpw);

        MCCPInputDataCtlr.ChannelPrefWrapper cpw2 = new MCCPInputDataCtlr.ChannelPrefWrapper();
        cpw2.productName = 'PROD2';
        cpw2.isDataLoaded = true;
        cpw2.chPrefDataPresent = true;
        chPrefList.add(cpw2);

        MCCPInputDataCtlr.BusinessRuleDataWrapper brdw = new MCCPInputDataCtlr.BusinessRuleDataWrapper();
        brdw.prodList = new List<MCCPInputDataCtlr.ProductSegmentWrapper>();
        brdw.prodList.addAll(prodList);

        brdw.chPrefList = new List<MCCPInputDataCtlr.ChannelPrefWrapper>();
        brdw.chPrefList.addAll(chPrefList);

        brdw.chConsentList = new List<MCCPInputDataCtlr.ChannelConsentWrapper>();
        brdw.chConsentList.addAll(chConList);
        brdw.isProdLevelSelected = true;

        brdw.mmList = new List<Measure_Master__c>();
        brdw.mmList.add(mmc);
        
        

        MCCPInputDataCtlr.getProdSegmentData(mmc.Id,prodList);
        MCCPInputDataCtlr.getChPrefData(mmc.Id,chPrefList);
        MCCPInputDataCtlr.getChConsentData(mmc.Id,chConList);

        MCCPInputDataCtlr.getCurrentDirectLoadStatus('PROD1',mmc.Id);

        MCCPInputDataCtlr.downloadReportData(mmc.Id,'PROD1','ProductSegment','Direct Load','');
        MCCPInputDataCtlr.previewReportData(mmc.Id,'PROD1','ProductSegment','Direct Load','');

        MCCPInputDataCtlr.downloadReportData(mmc.Id,'PROD1','ProductSegment','Direct Load2',mmc2.Id);
        MCCPInputDataCtlr.previewReportData(mmc.Id,'PROD1','ProductSegment','Direct Load2',mmc2.Id);

        MCCPInputDataCtlr.downloadReportData(mmc.Id,'PROD1','ChannelPref','Direct Load','');
        MCCPInputDataCtlr.previewReportData(mmc.Id,'PROD1','ChannelPref','Direct Load','');

        MCCPInputDataCtlr.downloadReportData(mmc.Id,'PROD1','ChannelCon','Direct Load','');
        MCCPInputDataCtlr.previewReportData(mmc.Id,'PROD1','ChannelCon','Direct Load','');

        MCCPInputDataCtlr.getDataSourceOptions();
        MCCPInputDataCtlr.getSntRules(teamins.Id,'PROD1');
        MCCPInputDataCtlr.getAllProdSNTRules(teamins.Id);

        String testString = JSON.serialize(brdw);

        //Test saving data for the selected Business Rule
        MCCPInputDataCtlr.saveBusinessRuleData('');
        MCCPInputDataCtlr.saveBusinessRuleData('abcd');
        MCCPInputDataCtlr.saveBusinessRuleData(testString);
        //MCCPInputDataCtlr.createProductSegment(mmc.Id,prodList);

        //Create mock data
        /*
        MCCPProductChannelWeightsCtlr.BusinessRuleDataWrapper brw = new MCCPProductChannelWeightsCtlr.BusinessRuleDataWrapper();
        brw.ruleData = mmc;
        brw.productData = new List<MCCP_CNProd__c>();
        brw.channelData = new List<MCCP_CNProd__c>();
        brw.productData.add(rec);
        brw.channelData.add(rec2);

        MCCPProductChannelWeightsCtlr.ProductChannelWrapper pcw = new MCCPProductChannelWeightsCtlr.ProductChannelWrapper();
        brw.junctionData = new List<MCCPProductChannelWeightsCtlr.ProductChannelWrapper>();
        pcw.setProducts = new Set<String>{'Prod1'};
        pcw.setChannels = new Set<String>{'Email'};
        pcw.channelProductList = new Map<String,List<MCCP_CNProd__c>>();

        Map<String,List<MCCP_CNProd__c>> tempMap = new Map<String,List<MCCP_CNProd__c>>();
        tempMap.put('Email',new List<MCCP_CNProd__c>{rec3});
        pcw.channelProductList.putAll(tempMap);
        brw.junctionData.add(pcw);

        String testString = JSON.serialize(brw);

        //Test saving data for the selected Business Rule
        MCCPProductChannelWeightsCtlr.saveBusinessRuleData('');
        MCCPProductChannelWeightsCtlr.saveBusinessRuleData('abcd');
        MCCPProductChannelWeightsCtlr.saveBusinessRuleData(testString);
        */
        System.Test.stopTest();
    }
}