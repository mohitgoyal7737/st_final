/*
@author       : A1942
@createdDate  : 07-08-2020
@description  : Controller for LWC components Input Parameter. 
@Revison(s)   : v1.0
*/
public with sharing class MCCPInputParameter 
{
    public class MCCPException extends Exception {}
    public MCCPInputParameter() {}

    @AuraEnabled
    public static List<InputParamInputWrapper> fetchInputParameters(String ruleId)
    {
        List<InputParamInputWrapper> inputParamList = new List<InputParamInputWrapper>();
        List<MCCP_CNProd__c> cNProdList = new List<MCCP_CNProd__c>();
        List<MCCP_CNProd__c> cnProdSegmentTypeList = new List<MCCP_CNProd__c>();
        List<Segmentation_Scheme__c> segmentSchemeList = new List<Segmentation_Scheme__c>();
        Map<String, List<SegmentRankWrapper>> schemeSegmentRankWrapMap = new Map<String, List<SegmentRankWrapper>>();
        Map<String, List<ChannelWrapper>> productChannelMap = new Map<String, List<ChannelWrapper>>();
        Map<String, Map<String, List<MCCP_CNProd__c>>> productSegmentMap = new Map<String, Map<String, List<MCCP_CNProd__c>>>();
        Map<String, String> productSchemeMap = new Map<String, String>();

        try
        {
            Id junctionRecordTypeId = Schema.SObjectType.MCCP_CNProd__c.getRecordTypeInfosByName().get('Junction').getRecordTypeId();
            Id segmentRecordTypeId = Schema.SObjectType.MCCP_CNProd__c.getRecordTypeInfosByName().get('Segment').getRecordTypeId();
            Set<Id> recordIdSet = new Set<Id>();
            recordIdSet.add(junctionRecordTypeId);
            recordIdSet.add(segmentRecordTypeId);

            cNProdList = [Select Id, Name, Rule_Name__c, Channel__c, Product__c, Data_Source__c, SegmentScheme__c, Call_Plan__r.Team_Instance__r.Name, Call_Plan__r.PDE_Method__c,Call_Plan__r.Priority_Order_1__c, Call_Plan__r.Priority_2__c, Call_Plan__r.Priority_Order_3__c, MaxInteractionVal__c, Segment__c, MC_PDE__c, Channel_Distribution__c, OptimisationSelected__c, RecordTypeId, Call_Plan__r.PDE_Frequencies__c,Call_Plan__r.Country__c,Call_Plan__r.isMCCPRuleExecuting__c,Call_Plan__r.isMCCPRulePublishing__c From MCCP_CNProd__c where Call_Plan__c=:ruleId and RecordTypeId IN:recordIdSet WITH SECURITY_ENFORCED order by Product__c,Channel__c,Segment__c];
            SnTDMLSecurityUtil.printDebugMessage('CNProdList size :: '+cNProdList.size());

            String rulePdeMethod = '', priority1, priority2, priority3, allowablePde = '', countryId='';
            Boolean isMCCPRuleExecuting = false,isMCCPRulePublishing = false;
            if(cNProdList !=null && cNProdList.size()>0){
                rulePdeMethod = cNProdList[0].Call_Plan__r.PDE_Method__c;
                priority1 = cNProdList[0].Call_Plan__r.Priority_Order_1__c;
                priority2 = cNProdList[0].Call_Plan__r.Priority_2__c;
                priority3 = cNProdList[0].Call_Plan__r.Priority_Order_3__c;
                allowablePde = cNProdList[0].Call_Plan__r.PDE_Frequencies__c;
                countryId = cNProdList[0].Call_Plan__r.Country__c;
                isMCCPRuleExecuting = cNProdList[0].Call_Plan__r.isMCCPRuleExecuting__c;
                isMCCPRulePublishing = cNProdList[0].Call_Plan__r.isMCCPRulePublishing__c;
            }

            SnTDMLSecurityUtil.printDebugMessage('RulePdeMethod :: '+rulePdeMethod);

            for(MCCP_CNProd__c cnProd:cNProdList){
                if(cnProd.RecordTypeId == segmentRecordTypeId){
                    cnProdSegmentTypeList.add(cnProd);
                }else if(cnProd.RecordTypeId == junctionRecordTypeId){
                    List<ChannelWrapper> channels = new List<ChannelWrapper>();
                    ChannelWrapper channel = new ChannelWrapper();
                    channel.channelName = cnProd.Channel__c;
                    channel.optimisationSelected = cnProd.OptimisationSelected__c;
                    channels.add(channel);
                    if(productChannelMap.containsKey(cnProd.Product__c) ){
                        channels.addALL(productChannelMap.get(cnProd.Product__c));
                    }
                    productChannelMap.put(cnProd.Product__c, channels);
                }
            }
            SnTDMLSecurityUtil.printDebugMessage('CNProdSegmentTypeList :: '+cnProdSegmentTypeList);
            SnTDMLSecurityUtil.printDebugMessage('ProductChannelMap :: '+productChannelMap);

            for(MCCP_CNProd__c cnProd:cnProdSegmentTypeList){
                Map<String, List<MCCP_CNProd__c>> segmentChannelMap = new Map<String, List<MCCP_CNProd__c>>();
                List<MCCP_CNProd__c> channels = new List<MCCP_CNProd__c>();
                if(productSegmentMap.containsKey(cnProd.Product__c)){
                    segmentChannelMap =productSegmentMap.get(cnProd.Product__c);
                    if(segmentChannelMap.containsKey(cnProd.Segment__c)){
                        channels.addAll(segmentChannelMap.get(cnProd.Segment__c));
                    }
                }
                channels.add(cnProd);
                segmentChannelMap.put(cnProd.Segment__c, channels);
                productSegmentMap.put(cnProd.Product__c, segmentChannelMap);
                productSchemeMap.put(cnProd.Product__c,cnProd.SegmentScheme__c);
            }
            SnTDMLSecurityUtil.printDebugMessage('ProductSegmentMap :: '+productSegmentMap);

            List<String> schemeNameList = productSchemeMap.values();
            segmentSchemeList = [Select Id, Scheme_Name__c, Segment_Name__c, Rank__c from Segmentation_Scheme__c where Scheme_Name__c IN:schemeNameList and Country__c=:countryId WITH SECURITY_ENFORCED order by Rank__c desc,Segment_Name__c];
            for(Segmentation_Scheme__c obj:segmentSchemeList){
                if(!String.isBlank(obj.Segment_Name__c) && obj.Rank__c !=null){
                    List<SegmentRankWrapper> tempList = new List<SegmentRankWrapper>();
                    tempList.add(new SegmentRankWrapper(obj.Segment_Name__c, String.valueOf(obj.Rank__c)));
                    if(schemeSegmentRankWrapMap.containsKey(obj.Scheme_Name__c)){
                        tempList.addAll(schemeSegmentRankWrapMap.get(obj.Scheme_Name__c));
                    }
                    schemeSegmentRankWrapMap.put(obj.Scheme_Name__c, tempList);
                }
            }

            SnTDMLSecurityUtil.printDebugMessage('schemeSegmentRankWrapMap :: '+schemeSegmentRankWrapMap);

            for(String prod:productSegmentMap.keySet()){
                InputParamInputWrapper inputParam = new InputParamInputWrapper();
                inputParam.product = prod;
                inputParam.pdeMethod = rulePdeMethod;
                inputParam.priority1 = priority1;
                inputParam.priority2 = priority2;
                inputParam.priority3 = priority3;
                inputParam.allowablePde = allowablePde;
                inputParam.segmentScheme = productSchemeMap.get(prod);
                inputParam.channelList = new List<channelWrapper>();
                inputParam.channelList.addAll(productChannelMap.get(prod));
                inputParam.isChannelDistribution = false;
                inputParam.isChannelInteraction = false;
                inputParam.isMCCPRuleExecuting = isMCCPRuleExecuting;
                inputParam.isMCCPRulePublishing = isMCCPRulePublishing;
                if(!String.isBlank(rulePdeMethod)){
                    if(rulePdeMethod == 'PDE Optimised & Workload Balanced'){
                        for(channelWrapper channelwrap:inputParam.channelList){
                            if(!channelwrap.optimisationSelected){
                                inputParam.isChannelInteraction = true;
                            }else{
                                inputParam.isChannelDistribution = true;
                            }
                        }
                    }else if(rulePdeMethod == 'Workload Balanced'){
                        inputParam.isChannelInteraction = true;
                    }
                }

                inputParam.segmentList = new List<SegmentOutputWrapper>();
                Map<String, List<MCCP_CNProd__c>> segmentChannelMap = new Map<String, List<MCCP_CNProd__c>>();
                segmentChannelMap = productSegmentMap.get(prod);
                List<SegmentOutputWrapper> segmentList = new List<SegmentOutputWrapper>();
                for(String segment:segmentChannelMap.keySet()){
                    SegmentOutputWrapper segmentWrap = new SegmentOutputWrapper();
                    segmentWrap.Name = segment;

                    if(schemeSegmentRankWrapMap.containsKey(productSchemeMap.get(prod))){
                        for(SegmentRankWrapper segWrap : schemeSegmentRankWrapMap.get(productSchemeMap.get(prod))){
                            if(segWrap.segmentName == segment){
                                segmentWrap.segmentRank = segWrap.segmentRank;
                                break;
                            }
                        }
                    }
                    if(String.isBlank(segmentWrap.segmentRank)){
                        segmentWrap.segmentRank = '';
                    }

                    segmentWrap.Channels = new List<InteractionWrapper>();
                    List<InteractionWrapper> channelsWrap = new List<InteractionWrapper>();
                    for(MCCP_CNProd__c obj:segmentChannelMap.get(segment)){
                        segmentWrap.mcPde = obj.MC_PDE__c != null ? String.valueOf(obj.MC_PDE__c) : '' ;

                        InteractionWrapper channelInteract = new InteractionWrapper();
                        channelInteract.Name = obj.Channel__c;
                        if(!String.isBlank(obj.Channel_Distribution__c)){
                           channelInteract.interactionVal = obj.Channel_Distribution__c;
                        }else
                            channelInteract.interactionVal = '';
                        if(!String.isBlank(obj.MaxInteractionVal__c)){
                            channelInteract.maxInteractionVal = obj.MaxInteractionVal__c;
                        }else
                            channelInteract.maxInteractionVal = '';
                        for(channelWrapper chwrap :inputParam.channelList){
                            if(chwrap.channelName == channelInteract.Name){
                               channelInteract.isOptimised = chwrap.optimisationSelected;
                            }
                        }
                        channelsWrap.add(channelInteract);
                    }
                    segmentWrap.Channels.addAll(channelsWrap);
                    segmentList.add(segmentWrap);
                }
                inputParam.segmentList.addAll(segmentList);
                inputParamList.add(inputParam);
            }
            SnTDMLSecurityUtil.printDebugMessage('inputParamList :: '+inputParamList);
            if(Test.isRunningTest()){
                throw new MCCPException('To cover test class');
            }
        }
        catch(Exception e){
            SnTDMLSecurityUtil.printDebugMessage('Error in fetchInputParameters()--'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack trace--'+e.getStackTraceString());
            
            SnTDMLSecurityUtil.printDebugMessage(e.getMessage());
            SalesIQSnTLogger.createUnHandledErrorLogs(e,SalesIQSnTLogger.MCCP_MODULE ,'MCCPInputParameter ',ruleId);
        }  
        return inputParamList;
        
    }
    
    @AuraEnabled
    public static List<SegmentSchemeWrapper> fetchSegmentScheme()
    {
        String countryId = '';
        Map<String, List<SegmentRankWrapper>> schemeMap = new Map<String, List<SegmentRankWrapper>>();
        List<Segmentation_Scheme__c> schemeList = new List<Segmentation_Scheme__c>();
        List<SegmentSchemeWrapper> resultList = new List<SegmentSchemeWrapper>();
        try
        {
            countryId = MCCPCallPlans.getUserCountryId();
            if(String.isNotBlank(countryId))
            {
                schemeList = [Select Id, Scheme_Name__c, Segment_Name__c, Rank__c From Segmentation_Scheme__c where  Country__c =: countryId WITH SECURITY_ENFORCED  order by Scheme_Name__c, Rank__c desc, Segment_Name__c];
                for(Segmentation_Scheme__c obj:schemeList){
                    List<SegmentRankWrapper> tempList = new List<SegmentRankWrapper>();
                    tempList.add(new SegmentRankWrapper(obj.Segment_Name__c, String.valueOf(obj.Rank__c)));
                    if(schemeMap.containsKey(obj.Scheme_Name__c)){
                        tempList.addAll(schemeMap.get(obj.Scheme_Name__c));
                    }
                    schemeMap.put(obj.Scheme_Name__c, tempList);
                }
                for(String key:schemeMap.keySet()){
                    SegmentSchemeWrapper tempObj = new SegmentSchemeWrapper();
                    tempObj.name = key;
                    tempObj.segmentList = new List<SegmentRankWrapper>();
                    tempObj.segmentList.addAll(schemeMap.get(key));
                    resultList.add(tempObj);
                }
            }
            if(Test.isRunningTest()){
                throw new MCCPException('To cover test class');
            }
        }
        catch(Exception e){
            SnTDMLSecurityUtil.printDebugMessage('Exception in fetchSegmentScheme()-- '+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack trace-- '+ e.getStackTraceString());
           
            SnTDMLSecurityUtil.printDebugMessage(e.getMessage());
            SalesIQSnTLogger.createHandledErrorLogs(e,SalesIQSnTLogger.MCCP_MODULE ,'');
        }
        return resultList;
    }

    @AuraEnabled
    public static List<ActionResultWrapper> createInputParameter(String ruleId, String inputParamValues, String priority1, String priority2, String priority3, String allowableMcPdes){

        SnTDMLSecurityUtil.printDebugMessage('ruleId :: '+ruleId+', inputParamValues :: '+inputParamValues+', priority1 :: '+priority1+', priority2 :: '+priority2+', priority3 :: '+priority3);

        Transient List<ActionResultWrapper> resultList = new List<ActionResultWrapper>();
        Measure_Master__c updateRule = new Measure_Master__c();
        List<MCCP_CNProd__c> cnProdListToUpdate = new List<MCCP_CNProd__c>();
        List<MCCP_CNProd__c> existingCNProdList = new List<MCCP_CNProd__c>();
        Map<String, String> existingkeyIdMap = new Map<String, String>();
        Boolean isMccpEnabled = false;

        try{

            //Validation for MCCP Enable
            isMccpEnabled = MCCPCallPlans.checkMCCPEnabled();
            if(!isMccpEnabled && !System.Test.isRunningTest()){
                resultList.add(new ActionResultWrapper(false,'', System.Label.MCCP_Disabled_Msg));
                return resultList;
            }

            List<InputParamOutputWrapper> inputParamList = (List<InputParamOutputWrapper>)JSON.deserialize(inputParamValues,List<InputParamOutputWrapper>.Class);

            Id segmentRecordTypeId = Schema.SObjectType.MCCP_CNProd__c.getRecordTypeInfosByName().get('Segment').getRecordTypeId();
            
            existingCNProdList = [Select Id, Segment__c, Product__c, Channel__c From MCCP_CNProd__c where Call_Plan__c =:ruleId and recordTypeId =:segmentRecordTypeId WITH SECURITY_ENFORCED];
            for(MCCP_CNProd__c cnProd: existingCNProdList){
                if(!String.isBlank(cnProd.Product__c) && !String.isBlank(cnProd.Segment__c) && !String.isBlank(cnProd.Channel__c)){
                    existingkeyIdMap.put(cnProd.Product__c +'_'+cnProd.Segment__c +'_'+ cnProd.Channel__c, cnProd.Id);
                }
            }

            for(InputParamOutputWrapper obj:inputParamList){
                String productName = obj.Product;
                String schemeName = obj.schemeName;
                for(SegmentOutputWrapper segmentsWrap:obj.Segments){
                    String segmentsName = segmentsWrap.Name;
                    Integer mcPde;
                    if(!String.isBlank(segmentsWrap.mcPde)){
                        mcPde = Integer.valueOf(segmentsWrap.mcPde);
                    }
                    for(InteractionWrapper channelsWrap:segmentsWrap.Channels){
                        MCCP_CNProd__c cnProd = new MCCP_CNProd__c();
                        String key = productName +'_'+ segmentsName +'_'+ channelsWrap.Name;
                        if(existingkeyIdMap.containsKey(key)){
                             cnProd.Id = existingkeyIdMap.get(key);
                        }else{
                            cnProd.Call_Plan__c = ruleId;
                        }
                        cnProd.RecordTypeId = segmentRecordTypeId;
                        cnProd.Product__c = productName;
                        cnProd.Segment__c = segmentsName;
                        cnProd.SegmentScheme__c = schemeName;
                        if(mcPde != null){
                            cnProd.MC_PDE__c = mcPde;
                        }
                        cnProd.Channel__c = channelsWrap.Name;
                        cnProd.Channel_Distribution__c = channelsWrap.interactionVal;
                        cnProd.MaxInteractionVal__c = channelsWrap.maxInteractionVal;
                        cnProdListToUpdate.add(cnProd);
                    }
                }
            }
            if(cnProdListToUpdate !=null && cnProdListToUpdate.size()>0){
                //SnTDMLSecurityUtil.upsertRecords(cnProdList,'MCCPInputParameter');
                upsert cnProdListToUpdate;

                updateRule.id = ruleId;
                updateRule.Priority_Order_1__c = priority1;
                updateRule.Priority_2__c = priority2;
                updateRule.Priority_Order_3__c = priority3;
                updateRule.PDE_Frequencies__c = allowableMcPdes;
                SnTDMLSecurityUtil.updateRecords(updateRule, 'MCCPInputParameter');
            }
            
             if(test.isrunningtest())
             throw new MCCPException('To cover test class');
        }catch(Exception e){
            SnTDMLSecurityUtil.printDebugMessage('Error in createInputParameter()--'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack trace--'+e.getStackTraceString());
             
            SnTDMLSecurityUtil.printDebugMessage(e.getMessage());
            SalesIQSnTLogger.createHandledErrorLogs(e,SalesIQSnTLogger.MCCP_MODULE ,ruleId);
            resultList.add(new ActionResultWrapper(false,ruleId,System.Label.Input_Parameter_Page_Exception));
            return resultList;
        }
        resultList.add(new ActionResultWrapper(true,ruleId,System.Label.Input_Parameter_Save_Msg));
        return resultList;
    }

    public class ActionResultWrapper {
        @AuraEnabled public Boolean dataSaved {get;set;}
        @AuraEnabled public String ruleID {get;set;}
        @AuraEnabled public String message {get;set;}

        ActionResultWrapper(Boolean dataSaved,String ruleID,String message) {
            this.dataSaved = dataSaved;
            this.ruleID = ruleID;
            this.message = message;
        }
    }

    public class SegmentSchemeWrapper{
        @AuraEnabled public String name;
        @AuraEnabled public List<SegmentRankWrapper> segmentList;
    }

    public class SegmentRankWrapper{
        @AuraEnabled public String segmentName;
        @AuraEnabled public String segmentRank;

        SegmentRankWrapper(String segmentName, String segmentRank){
            this.segmentName = segmentName;
            this.segmentRank = segmentRank;
        }

    }
    
    public class ChannelWrapper{
        @AuraEnabled public String channelName;
        @AuraEnabled public Boolean optimisationSelected;
    }

    public class InteractionWrapper{
        @AuraEnabled public String Name;
        @AuraEnabled public String interactionVal;
        @AuraEnabled public String maxInteractionVal;
        @AuraEnabled public Boolean isOptimised;
    }

    public class InputParamInputWrapper{
        @AuraEnabled public String product;
        @AuraEnabled public String pdeMethod;
        @AuraEnabled public String priority1;
        @AuraEnabled public String priority2;
        @AuraEnabled public String priority3;
        @AuraEnabled public String allowablePde;
        @AuraEnabled public String segmentScheme;
        @AuraEnabled public Boolean isChannelDistribution;
        @AuraEnabled public Boolean isChannelInteraction;
        @AuraEnabled public Boolean isMCCPRuleExecuting;
        @AuraEnabled public Boolean isMCCPRulePublishing;
        @AuraEnabled public List<ChannelWrapper> channelList;
        @AuraEnabled public List<SegmentOutputWrapper> segmentList;
    }

    public class SegmentOutputWrapper{
        @AuraEnabled public String Name;
        @AuraEnabled public String segmentRank;
        @AuraEnabled public String mcPde;
        @AuraEnabled public List<InteractionWrapper> channels;
    }

    public class InputParamOutputWrapper{
        @AuraEnabled public String Product;
        @AuraEnabled public String schemeName;
        @AuraEnabled public Boolean isChannelDistribution;
        @AuraEnabled public List<SegmentOutputWrapper> Segments;
    }
}