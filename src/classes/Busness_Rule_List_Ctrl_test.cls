@isTest
public class Busness_Rule_List_Ctrl_test {
    @istest static void gridtest()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        String classname = 'Busness_Rule_List_Ctrl_test';
        
        Account acc = TestDataFactory.createAccount();
        SnTDMLSecurityUtil.insertRecords(acc,className);
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
       SnTDMLSecurityUtil.insertRecords(countr,className);

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins,className);
        
        AxtriaSalesIQTM__Position__c pos = TestDataFactory.createPosition(team,teamins);
        SnTDMLSecurityUtil.insertRecords(pos,className);
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
         SnTDMLSecurityUtil.insertRecords(posAccount,className);
        
        Grid_Master__c gMaster = TestDataFactory.gridMaster(countr);
        SnTDMLSecurityUtil.insertRecords(gMaster,className);

        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        SnTDMLSecurityUtil.insertRecords(pcc,className);

        Measure_Master__c mMaster = TestDataFactory.createMeasureMaster(pcc, team, teamins);
       SnTDMLSecurityUtil.insertRecords(mMaster,className);
        
        BU_Response__c buResponse = TestDataFactory.createBuResponse(posAccount,pcc,teamins,team,acc);
        SnTDMLSecurityUtil.insertRecords(buResponse,className);
        
        Account_Compute_Final__c compFinal = TestDataFactory.createComputeFinal(mMaster, acc,posAccount,buResponse,pos);
        SnTDMLSecurityUtil.insertRecords(compFinal,className);
        
        Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
        SnTDMLSecurityUtil.insertRecords(pp,className);

        Compute_Master__c ccMaster1 = new Compute_Master__c();
        ccMaster1.Name = 'Test';
        ccMaster1.CurrencyIsoCode = 'USD';
        ccMaster1.Field_2_Type__c = 'Parameters';
        ccMaster1.Field_2_val__c = 1;
        SnTDMLSecurityUtil.insertRecords(ccMaster1,className);

        Step__c step = new Step__c();
        step.Name = 'abcd2';
        step.CurrencyIsoCode = 'USD';
        step.UI_Location__c = 'Compute Segment';
        step.Step_Type__c = 'Cases';
        step.Compute_Master__c = ccMaster1.id;
        step.Matrix__c = gMaster.id;
        step.Measure_Master__c = mMaster.id;
        SnTDMLSecurityUtil.insertRecords(step,className);

        //Rule_Parameter__c rps = TestDataFactory.ruleParameter(mMaster, pp, step);
        //insert rps;
        
        Rule_Parameter__c rps= new Rule_Parameter__c();
        rps.Measure_Master__c = mMaster.id;
        rps.Summary_Potential__c = true;
        rps.Summary_Adoption__c = true;
        rps.CustomerSegmentedField__c  = true;
        rps.Parameter__c = pp.id;
        rps.Step__c = step.id;
        rps.CurrencyIsoCode='USD';
        SnTDMLSecurityUtil.insertRecords(rps,className);

        Compute_Master__c ccMaster = TestDataFactory.createComputeMaster(rps);
        SnTDMLSecurityUtil.insertRecords(ccMaster,className);

        Step__c steps = TestDataFactory.step(ccMaster, gMaster, mMaster, rps);
        SnTDMLSecurityUtil.insertRecords(steps,className);
        
        Grid_Details__c gDetails = TestDataFactory.gridDetails(gMaster);
         SnTDMLSecurityUtil.insertRecords(gDetails,className);
        
        String UIloc = 'Compute Segment';
        //String query
        Test.startTest();
        
        System.runAs(loggedinuser){
        Busness_Rule_List_Ctrl obj=new Busness_Rule_List_Ctrl();
        //obj.query = compFinal.id;
        obj.ruleId = mMaster.id;
        obj.populateRuleList();
        obj.newrule();
        obj.editrule();
        obj.previewrule();
        obj.viewRule();
        obj.checkForMissingStep(UIloc);
        obj.ExecuteRule();
        obj.cloneRule();
        obj.initiatePublishPopup();
        obj.hidePopup();
        obj.deactivateRule();
        //obj.Publish();
        obj.initiateSimulationPopup();
        System.assertEquals(UIloc,'Compute Segment');
         }
        
        Test.stopTest();

    }
    
    @istest static void gridtest1()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        String classname = 'Busness_Rule_List_Ctrl_test';
        
        Account acc = TestDataFactory.createAccount();
        SnTDMLSecurityUtil.insertRecords(acc,className);
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
         SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
       SnTDMLSecurityUtil.insertRecords(team,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins,className);
        
        AxtriaSalesIQTM__Position__c pos = TestDataFactory.createPosition(team,teamins);
        SnTDMLSecurityUtil.insertRecords(pos,className);
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        SnTDMLSecurityUtil.insertRecords(posAccount,className);
        
        Grid_Master__c gMaster = TestDataFactory.gridMaster(countr);
        SnTDMLSecurityUtil.insertRecords(gMaster,className);

        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        SnTDMLSecurityUtil.insertRecords(pcc,className);

        Measure_Master__c mMaster = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        SnTDMLSecurityUtil.insertRecords(mMaster,className);
        
        BU_Response__c buResponse = TestDataFactory.createBuResponse(posAccount,pcc,teamins,team,acc);
        SnTDMLSecurityUtil.insertRecords(buResponse,className);
        
        Account_Compute_Final__c compFinal = TestDataFactory.createComputeFinal(mMaster, acc,posAccount,buResponse,pos);
        SnTDMLSecurityUtil.insertRecords(compFinal,className);
        
        Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
        SnTDMLSecurityUtil.insertRecords(pp,className);

        Compute_Master__c ccMaster1 = new Compute_Master__c();
        ccMaster1.Name = 'Test';
        ccMaster1.CurrencyIsoCode = 'USD';
        ccMaster1.Field_2_Type__c = 'Parameters';
        ccMaster1.Field_2_val__c = 1;
         SnTDMLSecurityUtil.insertRecords(ccMaster1,className);

        Step__c step = new Step__c();
        step.Name = 'abcd2';
        step.CurrencyIsoCode = 'USD';
        step.UI_Location__c = 'Compute Segment';
        step.Step_Type__c = 'Quantile';
        step.Compute_Master__c = ccMaster1.id;
        step.Matrix__c = gMaster.id;
        step.Measure_Master__c = mMaster.id;
        SnTDMLSecurityUtil.insertRecords(step,className);

        //Rule_Parameter__c rps = TestDataFactory.ruleParameter(mMaster, pp, step);
        //insert rps;
        
        Rule_Parameter__c rps= new Rule_Parameter__c();
        rps.Measure_Master__c = mMaster.id;
        rps.Summary_Potential__c = true;
        rps.Summary_Adoption__c = true;
        rps.CustomerSegmentedField__c  = true;
        rps.Parameter__c = pp.id;
        rps.Step__c = step.id;
        rps.CurrencyIsoCode='USD';
        SnTDMLSecurityUtil.insertRecords(rps,className);

        Compute_Master__c ccMaster = TestDataFactory.createComputeMaster(rps);
        SnTDMLSecurityUtil.insertRecords(ccMaster,className);

        Step__c steps = TestDataFactory.step(ccMaster, gMaster, mMaster, rps);
         SnTDMLSecurityUtil.insertRecords(steps,className);
        
        Grid_Details__c gDetails = TestDataFactory.gridDetails(gMaster);
        SnTDMLSecurityUtil.insertRecords(gDetails,className);
        
        String UIloc = 'Compute Segment';
        //String query
        Test.startTest();
        
        System.runAs(loggedinuser){
        Busness_Rule_List_Ctrl obj=new Busness_Rule_List_Ctrl();
        //obj.query = compFinal.id;
        obj.ruleId = mMaster.id;
        obj.populateRuleList();
        obj.newrule();
        obj.editrule();
        obj.previewrule();
        obj.viewRule();
        obj.checkForMissingStep(UIloc);
        obj.ExecuteRule();
        obj.cloneRule();
        obj.initiatePublishPopup();
        obj.hidePopup();
        obj.deactivateRule();
        //obj.Publish();
        obj.initiateSimulationPopup();
        System.assertEquals(UIloc,'Compute Segment');
         }
        
        Test.stopTest();

    }
}