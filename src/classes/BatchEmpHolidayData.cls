global with sharing class BatchEmpHolidayData implements Database.Batchable<sObject> , Database.Stateful {
    public String query;
    public AxtriaSalesIQTM__Team_Instance__c teamInstance;
    public Integer count = 0;
    
    Map<String,String> positionEmpData = new Map<String,String>();
    Map<String,Integer> holidayEmpData = new Map<String,Integer>();
    
    set<String> posPRID = new set<String>();
    set<String> holidayPRID = new set<String>();

    List<Emp_holiday_code__c> allInsert = new List<Emp_holiday_code__c>();

    Date startDate;
    Date endDate;
    String selectedteaminstance;

    global BatchEmpHolidayData(String teamInstanceID)
    {
        count = 0;
        selectedteaminstance=teamInstanceID;

        teamInstance = [select id, AxtriaSalesIQTM__IC_EffstartDate__c, AxtriaSalesIQTM__IC_EffEndDate__c from AxtriaSalesIQTM__Team_Instance__c where id = :teamInstanceID];
        startDate  = teamInstance.AxtriaSalesIQTM__IC_EffstartDate__c;
        endDate  = teamInstance.AxtriaSalesIQTM__IC_EffEndDate__c;
       // delete [select id from Emp_holiday_code__c  WHERE Name != NULL];
        //Replaced for Security Review
        List<Emp_holiday_code__c> emp = new List<Emp_holiday_code__c>();
        try{
            emp=  [select id from Emp_holiday_code__c  WHERE Name != NULL WITH SECURITY_ENFORCED];
        }
        catch(System.QueryException qe) 
        {
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }
            //delete emp;
        SnTDMLSecurityUtil.deleteRecords(emp, 'BatchEmpHolidayData');
        
        query = 'SELECT PRID__c , Holiday_Date__c from Emp_Holiday_Data__c where Holiday_Date__c >= :startDate and Holiday_Date__c <= :endDate Order By PRID__c';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, list<Emp_Holiday_Data__c> scope) {
        SnTDMLSecurityUtil.printDebugMessage('startDate++++'+startDate);
        SnTDMLSecurityUtil.printDebugMessage('endDate++++'+endDate);
        SnTDMLSecurityUtil.printDebugMessage('****  Query  ******'+ scope);
        List<AxtriaSalesIQTM__Position_Employee__c> posDetails = new List<AxtriaSalesIQTM__Position_Employee__c>();
        String teamInstID = teamInstance.id;
        posDetails = [select AxtriaSalesIQTM__Employee__r.Employee_PRID__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c from AxtriaSalesIQTM__Position_Employee__c where AxtriaSalesIQTM__Assignment_Type__c = 'Primary' and Team_Instance__c = :teamInstID];
        
        if(posDetails != null){
           for(AxtriaSalesIQTM__Position_Employee__c posCode : posDetails){
            positionEmpData.put(posCode.AxtriaSalesIQTM__Employee__r.Employee_PRID__c.toUpperCase(), posCode.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
        }
    }

    posPRID = positionEmpData.keySet();
    SnTDMLSecurityUtil.printDebugMessage('====positionEmpData===' +positionEmpData);
    SnTDMLSecurityUtil.printDebugMessage('PRID of Position Employee' + posPRID);

    for(Emp_Holiday_Data__c insertData : scope){
        if(!holidayPRID.contains(insertData.PRID__c.toUpperCase())){
            count = 1;
        }
        else{
            count = holidayEmpData.get(insertData.PRID__c.toUpperCase());
            count = count + 1;
        }

        holidayPRID.add(insertData.PRID__c.toUpperCase());
        holidayEmpData.put(insertData.PRID__c.toUpperCase(), count);
        SnTDMLSecurityUtil.printDebugMessage('Map for holidays count' + holidayEmpData);

    }
    allInsert = new List<Emp_holiday_code__c>(); 
    Set<String> processedPRID = new Set<String>();

    SnTDMLSecurityUtil.printDebugMessage('++++++++++++++ posPRID' + posPRID);

    for(Emp_Holiday_Data__c newinsert : scope)
    {
        SnTDMLSecurityUtil.printDebugMessage('+++++++++++++++ newinsert' + newinsert.PRID__c.toUpperCase());

        Emp_holiday_code__c finalEmpData = new Emp_holiday_code__c();
        if(!processedPRID.contains(newinsert.PRID__c.toUpperCase()))
        {
         SnTDMLSecurityUtil.printDebugMessage('+++++++ Inside Processed IDs');
         if(posPRID.contains(newinsert.PRID__c.toUpperCase()))
         {
            finalEmpData.PRID__c = newinsert.PRID__c.toUpperCase();
            SnTDMLSecurityUtil.printDebugMessage('inserted PRID in Final Holiday Position Code Table' + finalEmpData.PRID__c.toUpperCase());
            finalEmpData.Holiday__c = String.valueOf(holidayEmpData.get(newinsert.PRID__c.toUpperCase()));
            finalEmpData.Position_code__c = positionEmpData.get(newinsert.PRID__c.toUpperCase());
            finalEmpData.Team_Instance__c = teamInstance.id;
            finalEmpData.Team_Instance_LookUp__c = teamInstance.id;
            SnTDMLSecurityUtil.printDebugMessage('final Employee data with position Code' + finalEmpData);
            processedPRID.add(finalEmpData.PRID__c);
            allInsert.add(finalEmpData);
        }
        else
        {
          SnTDMLSecurityUtil.printDebugMessage('+++++++++ PRID not found' + newinsert.PRID__c.toUpperCase());
      }
  }
}
SnTDMLSecurityUtil.printDebugMessage('*****Holidays and Position Code Insert' + allInsert);
if(!allInsert.isEmpty()){   
 //insert allInsert;
 SnTDMLSecurityUtil.insertRecords(allInsert, 'BatchEmpHolidayData');
}
}

global void finish(Database.BatchableContext BC) 
{
 PopulatePositionProductHoliday populate= new PopulatePositionProductHoliday(selectedteaminstance);
 Database.executeBatch(populate,2000);
}
}