@isTest
private class BatchPopulateCimConfig_Test {
    static testMethod void testMethod1() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c country = testDataFactory.createCountry(orgmas);
        insert country;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(country);
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.Name = 'All';
        insert teamins;
        
        AxtriaSalesIQTM__Position__c pos = TestDataFactory.createPosition(team,teamins);
        insert pos;
        
        AxtriaSalesIQTM__Change_Request__c changerequest = TestDataFactory.createChangeRequest(UserInfo.getUserId(),pos.id,pos.id,teamins.id,'test');
        insert changerequest;
        
        AxtriaSalesIQTM__CIM_Config__c cimcc = TestDataFactory.createCIMConfig(teamins);
        insert cimcc;
        
        temp_Obj__c newtempobj = TestDataFactory.tempobj();
        newtempobj.Display_Column_Order__c = null ;
        insert newtempobj;
        
        List<temp_Obj__c > tempobjlist = new List<temp_Obj__c >();
        tempobjlist.add(newtempobj);
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            BatchPopulateCimConfig batchpopulatecimconfig = new BatchPopulateCimConfig('All');
            Database.executeBatch(batchpopulatecimconfig);
        }
        Test.stopTest();
    }
    static testMethod void testMethod2() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c country = testDataFactory.createCountry(orgmas);
        insert country;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(country);
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.Name = 'All';
        insert teamins;
        
        AxtriaSalesIQTM__Position__c pos = TestDataFactory.createPosition(team,teamins);
        insert pos;
        
        AxtriaSalesIQTM__Change_Request__c changerequest = TestDataFactory.createChangeRequest(UserInfo.getUserId(),pos.id,pos.id,teamins.id,'test');
        insert changerequest;
        
        AxtriaSalesIQTM__CIM_Config__c cimcc = TestDataFactory.createCIMConfig(teamins);
        insert cimcc;
        
        temp_Obj__c newtempobj = TestDataFactory.tempobj();
        newtempobj.Display_Column_Order__c = null ;
        insert newtempobj;
        
        List<temp_Obj__c > tempobjlist = new List<temp_Obj__c >();
        tempobjlist.add(newtempobj);
         String Ids;
         Boolean flag;
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            BatchPopulateCimConfig batchpopulatecimconfig = new BatchPopulateCimConfig('All' ,Ids,flag);
            Database.executeBatch(batchpopulatecimconfig);
        }
        Test.stopTest();
    }
}