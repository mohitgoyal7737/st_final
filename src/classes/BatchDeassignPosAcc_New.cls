global with sharing class BatchDeassignPosAcc_New implements Database.Batchable<sObject> { //, Database.Stateful
    public String query;
   
    global BatchDeassignPosAcc_New() {
        }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<temp_Obj__c> scope) {
    }

    global void finish(Database.BatchableContext BC) {

    }
}