@isTest
private class teamInstance_Test {
    public static testMethod void testocc(){
        User loggedInUser = new User(id=UserInfo.getUserId());
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            Account acc= TestDataFactory.createAccount();
            insert acc;
            AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
            insert orgmas;
            AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
            insert countr;
            AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
            insert team;
            
            AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
            workspace.AxtriaSalesIQTM__Country__c = countr.id;
            insert workspace;
            
            AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
            insert teamins;
            
            
            AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
            insert scen;
            
            //AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
            teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
            update scen; 

            /*if(teamins.AxtriaSalesIQTM__Alignment_Period__c =='Future' &&
               teamIns.AxtriaSalesIQTM__Alignment_Period__c =='Current'){
                   scen.AxtriaSalesIQTM__Last_Sync_Success_Date__c = null;
                   scen.AxtriaSalesIQTM__Last_Promote_Success_Date__c = null;
                   update scen;
                   
               }*/
               
           }
           Test.stopTest();
       }
   }