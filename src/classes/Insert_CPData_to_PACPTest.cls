@isTest
public class Insert_CPData_to_PACPTest {
    
    //For Single channel data flow
    static testMethod void testMethod1() {

        String className = 'Insert_CPData_to_PACPTest';
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        acc.AccountNumber = 'ES206265';
        acc.Type = 'HCO';
        SnTDMLSecurityUtil.insertRecords(acc,className);

        Account acc1= TestDataFactory.createAccount();
        acc1.AccountNumber = 'ES206211';
        acc1.Type = 'HCO';
        SnTDMLSecurityUtil.insertRecords(acc1,className);

        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Name = 'USA';
        SnTDMLSecurityUtil.insertRecords(countr,className);
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);

        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        teamins1.Name = teamins1.Name + '1';
        SnTDMLSecurityUtil.insertRecords(teamins1,className);
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
        SnTDMLSecurityUtil.insertRecords(scen,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.MultiChannel__c = false;
        //teamins.AxtriaSalesIQTM__Country__c = countr.id;
        teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        //teamins.Country_Name__c = 'USA';
        SnTDMLSecurityUtil.insertRecords(teamins,className);

        AxtriaSalesIQTM__Team_Instance__c teamins2 = TestDataFactory.createTeamInstance(team);
        teamins2.Name = teamins2.Name + '2';
        teamins2.AxtriaSalesIQTM__Scenario__c = scen.id;
        //teamins.AxtriaSalesIQTM__Country__c = countr.id;
        teamins2.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        //teamins.Country_Name__c = 'USA';
        SnTDMLSecurityUtil.insertRecords(teamins2,className);
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        SnTDMLSecurityUtil.insertRecords(pcc,className);
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        SnTDMLSecurityUtil.insertRecords(mmc,className);
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        SnTDMLSecurityUtil.insertRecords(pos,className);

        AxtriaSalesIQTM__Position__c pos1= TestDataFactory.createPosition(team,teamins2);
        SnTDMLSecurityUtil.insertRecords(pos1,className);

        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        SnTDMLSecurityUtil.insertRecords(posAccount,className);

        AxtriaSalesIQTM__Position_Account__c posAccount1 = TestDataFactory.createPositionAccount(acc1,pos1,teamins2);
        SnTDMLSecurityUtil.insertRecords(posAccount1,className);

        Product_Priority__c pPriority = TestDataFactory.productPriority();
        SnTDMLSecurityUtil.insertRecords(pPriority,className);
        
        AxtriaSalesIQTM__Position_Team_Instance__c posTeam = TestDataFactory.createPositionTeamInstance(pos.Id,pos1.id,teamins.Id);
        SnTDMLSecurityUtil.insertRecords(posTeam,className);

        AxtriaSalesIQTM__TriggerContol__c trigger3 = new AxtriaSalesIQTM__TriggerContol__c();
        trigger3.Name = 'TestAddTarget';
        trigger3.AxtriaSalesIQTM__IsStopTrigger__c = true;
        SnTDMLSecurityUtil.insertRecords(trigger3,className);

        AxtriaSalesIQTM__TriggerContol__c trigger4 = new AxtriaSalesIQTM__TriggerContol__c();
        trigger4.Name = 'TestPacp';
        trigger4.AxtriaSalesIQTM__IsStopTrigger__c = true;
        SnTDMLSecurityUtil.insertRecords(trigger4,className);
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        positionAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c = true;
        positionAccountCallPlan.Final_TCF_Approved__c = 3.0;
        positionAccountCallPlan.Final_TCF__c = 3.0;
        positionAccountCallPlan.P1__c = 'GIST';
        SnTDMLSecurityUtil.insertRecords(positionAccountCallPlan,className);
        
        temp_Obj__c newpa= new temp_Obj__c();
        newpa.Account__c = acc.Id;
        newpa.AccountNumber__c = acc.AccountNumber;
        newpa.Territory_ID__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        newpa.Team_Name__c = teamins.AxtriaSalesIQTM__Team__r.name;
        newpa.Team_Instance__c = teamins.Id;
        newPa.Team_Instance_Name__c = teamins.Name;
        newpa.Account_Type__c = acc.type;
        newpa.Object__c = 'Call_Plan__c';
        newpa.Status__c = 'New';
        newPa.Objective__c = '10';
        newPa.Product_Name__c = pcc.Name;
        newPa.Product_Code__c = pcc.Product_Code__c;
        newPa.Segment__c = 'A';
        newPa.Target__c = true;
        newPa.position__c = pos.Id;
        newPa.Position_Team_Instance__c = posTeam.Id;
        newPa.Position_Account__c = posAccount.Id;
        SnTDMLSecurityUtil.insertRecords(newPa,className);
        
        AxtriaSalesIQTM__TriggerContol__c trigger1 = new AxtriaSalesIQTM__TriggerContol__c();
        trigger1.Name = 'ParentPacp';
        trigger1.AxtriaSalesIQTM__IsStopTrigger__c = true;
        SnTDMLSecurityUtil.insertRecords(trigger1,className);

        AxtriaSalesIQTM__TriggerContol__c trigger2 = new AxtriaSalesIQTM__TriggerContol__c();
        trigger2.Name = 'CallPlanSummaryTrigger';
        trigger2.AxtriaSalesIQTM__IsStopTrigger__c = true;
        SnTDMLSecurityUtil.insertRecords(trigger2,className);

        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            
            Insert_CPData_to_PACP obj1 = new Insert_CPData_to_PACP(teamins.Name,null,'');
            Insert_CPData_to_PACP obj2 = new Insert_CPData_to_PACP(teamins.Name, null);
            Insert_CPData_to_PACP obj3 = new Insert_CPData_to_PACP(teamins.Name,null,false);
            Insert_CPData_to_PACP obj = new Insert_CPData_to_PACP(teamins.Name);
            Database.executeBatch(obj);
        }
        Test.stopTest();
    }
    
    //For Multichannel Data flow
    static testMethod void testMethod2() {

        String className = 'Insert_CPData_to_PACPTest';
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        acc.AccountNumber = 'ES206265';
        acc.Type = 'HCO';
        SnTDMLSecurityUtil.insertRecords(acc,className);

        Account acc1= TestDataFactory.createAccount();
        acc1.AccountNumber = 'ES206211';
        acc1.Type = 'HCO';
        SnTDMLSecurityUtil.insertRecords(acc1,className);

        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Name = 'USA';
        SnTDMLSecurityUtil.insertRecords(countr,className);
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);

        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        teamins1.Name = teamins1.Name + '1';
        SnTDMLSecurityUtil.insertRecords(teamins1,className);
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
        SnTDMLSecurityUtil.insertRecords(scen,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.MultiChannel__c = true;
        //teamins.AxtriaSalesIQTM__Country__c = countr.id;
        teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        //teamins.Country_Name__c = 'USA';
        SnTDMLSecurityUtil.insertRecords(teamins,className);

        AxtriaSalesIQTM__Team_Instance__c teamins2 = TestDataFactory.createTeamInstance(team);
        teamins2.Name = teamins2.Name + '2';
        teamins2.AxtriaSalesIQTM__Scenario__c = scen.id;
        //teamins.AxtriaSalesIQTM__Country__c = countr.id;
        teamins2.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        //teamins.Country_Name__c = 'USA';
        SnTDMLSecurityUtil.insertRecords(teamins2,className);
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        SnTDMLSecurityUtil.insertRecords(pcc,className);
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        SnTDMLSecurityUtil.insertRecords(mmc,className);
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        SnTDMLSecurityUtil.insertRecords(pos,className);

        AxtriaSalesIQTM__Position__c pos1= TestDataFactory.createPosition(team,teamins2);
        SnTDMLSecurityUtil.insertRecords(pos1,className);

        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        //posAccount.AxtriaSalesIQTM__Account__r.AccountNumber = 'ES206265';
        SnTDMLSecurityUtil.insertRecords(posAccount,className);

        AxtriaSalesIQTM__Position_Account__c posAccount1 = TestDataFactory.createPositionAccount(acc1,pos1,teamins2);
        //posAccount.AxtriaSalesIQTM__Account__r.AccountNumber = 'ES206265';
        SnTDMLSecurityUtil.insertRecords(posAccount1,className);

        Product_Priority__c pPriority = TestDataFactory.productPriority();
        SnTDMLSecurityUtil.insertRecords(pPriority,className);
        
        AxtriaSalesIQTM__Position_Team_Instance__c posTeam = TestDataFactory.createPositionTeamInstance(pos.Id,pos1.id,teamins.Id);
        SnTDMLSecurityUtil.insertRecords(posTeam,className);

        AxtriaSalesIQTM__TriggerContol__c trigger3 = new AxtriaSalesIQTM__TriggerContol__c();
        trigger3.Name = 'TestAddTarget';
        trigger3.AxtriaSalesIQTM__IsStopTrigger__c = true;
        SnTDMLSecurityUtil.insertRecords(trigger3,className);

        AxtriaSalesIQTM__TriggerContol__c trigger4 = new AxtriaSalesIQTM__TriggerContol__c();
        trigger4.Name = 'TestPacp';
        trigger4.AxtriaSalesIQTM__IsStopTrigger__c = true;
        SnTDMLSecurityUtil.insertRecords(trigger4,className);
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        positionAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c = true;
        positionAccountCallPlan.Final_TCF_Approved__c = 3.0;
        positionAccountCallPlan.Final_TCF__c = 3.0;
        positionAccountCallPlan.P1__c = 'GIST';
        //positionAccountCallPlan.AxtriaSalesIQTM__Account__r.AccountNumber = 'ES206265';
        SnTDMLSecurityUtil.insertRecords(positionAccountCallPlan,className);
       
        temp_Obj__c newpa= new temp_Obj__c();
        newpa.Account__c = acc.Id;
        newpa.AccountNumber__c = acc.AccountNumber;
        newpa.Territory_ID__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        newpa.Team_Name__c = teamins.AxtriaSalesIQTM__Team__r.name;
        newpa.Team_Instance__c = teamins.Id;
        newPa.Team_Instance_Name__c = teamins.Name;
        newpa.Account_Type__c = acc.type;
        newpa.Object__c = 'Call_Plan__c';
        newpa.Status__c = 'New';
        newPa.Objective__c = '10';
        newPa.Product_Name__c = pcc.Name;
        newPa.Product_Code__c = pcc.Product_Code__c;
        newPa.Segment__c = 'A';
        newPa.Target__c = true;
        newPa.position__c = pos.Id;
        newPa.Position_Team_Instance__c = posTeam.Id;
        newPa.Position_Account__c = posAccount.Id;
        newPa.Objective_1__c = '5';
        newPa.Objective_2__c = '5';
        newPa.Objective_3__c = '5';
        newPa.Objective_4__c = '5';
        newPa.Objective_5__c = '5';
        newPa.Channel_1__c = ' Email';
        newPa.Channel_2__c = 'F2F';
        newPa.Channel_3__c = 'Skype';
        newPa.Channel_4__c = 'Telephone';
        newPa.Channel_5__c = 'Virtual Webinar';
        SnTDMLSecurityUtil.insertRecords(newPa,className);
        
        
        AxtriaSalesIQTM__TriggerContol__c trigger1 = new AxtriaSalesIQTM__TriggerContol__c();
        trigger1.Name = 'ParentPacp';
        trigger1.AxtriaSalesIQTM__IsStopTrigger__c = true;
        SnTDMLSecurityUtil.insertRecords(trigger1,className);
        
        AxtriaSalesIQTM__TriggerContol__c trigger2 = new AxtriaSalesIQTM__TriggerContol__c();
        trigger2.Name = 'CallPlanSummaryTrigger';
        trigger2.AxtriaSalesIQTM__IsStopTrigger__c = true;
        SnTDMLSecurityUtil.insertRecords(trigger2,className);

        Test.startTest();
        System.runAs(loggedInUser){
            
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            
            Insert_CPData_to_PACP obj1 = new Insert_CPData_to_PACP(teamins.Name,null,'');
            Insert_CPData_to_PACP obj2 = new Insert_CPData_to_PACP(teamins.Name, null);
            Insert_CPData_to_PACP obj3 = new Insert_CPData_to_PACP(teamins.Name,null,false);
            Insert_CPData_to_PACP obj = new Insert_CPData_to_PACP(teamins.Name);
            Database.executeBatch(obj);
        }
        Test.stopTest();
    }
}