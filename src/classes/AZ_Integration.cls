global class AZ_Integration {
    
    string teamInstanceSelected;
    
    global AZ_Integration(string teamInstance)
    {
        teamInstanceSelected = teamInstance;
        
       create_MC_Cycle();
    }
        
    global AZ_Integration(string teamInstance, Integer count)
    {
        teamInstanceSelected = teamInstance;
        
       create_MC_Cycle_Plan_Target_vod();
    }
   
    
    global void create_MC_Cycle()
    {
        List<AxtriaSalesIQTM__Team_Instance__c> allTeamInstanceRecs = new List<AxtriaSalesIQTM__Team_Instance__c>();
        
        allTeamInstanceRecs = [select Name,AxtriaSalesIQTM__Team__r.Name, AxtriaSalesIQTM__IC_EffstartDate__c, Increment_Field__c, AxtriaSalesIQTM__IC_EffendDate__c, AxtriaSalesIQTM__Team_Cycle_Name__c, AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Base_Team_Name__c, AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Team_Instance__c where id = :teamInstanceSelected];
        
        List<SIQ_MC_Cycle_vod_O__c> mcRecs = new List<SIQ_MC_Cycle_vod_O__c>();
         
        for(AxtriaSalesIQTM__Team_Instance__c teamInstanceRecs : allTeamInstanceRecs)
        {
            SIQ_MC_Cycle_vod_O__c mc = new SIQ_MC_Cycle_vod_O__c();
            mc.Name                          =   teamInstanceRecs.Name;             // Do Modifications in Name (Add Year to Name with Underscores)
            mc.SIQ_Status_vod__c                 =   'Planned_vod';
            
            DateTime dtStart                 =   teamInstanceRecs.AxtriaSalesIQTM__IC_EffstartDate__c;
            Date myDateStart                 =   date.newinstance(dtStart.year(), dtStart.month(), dtStart.day());

            DateTime dtEnd                   =   teamInstanceRecs.AxtriaSalesIQTM__IC_EffendDate__c;
            Date myDateEnd                   =   date.newinstance(dtEnd.year(), dtEnd.month(), dtEnd.day());

            String dateWithoutTimeStamp      =   dtStart.year() + '-0' + dtStart.month() + '-' + dtStart.day();
            
            mc.SIQ_Start_Date_vod__c             =   myDateStart;
            mc.SIQ_End_Date_vod__c               =   myDateEnd;
            ///mc.AZ_Business_Unit__c          =   teamInstanceRecs.Team_Cycle_Name__c; // CNS or Immunolgy : Take Business Unit of AZ
            //mc.AZ_Country_Code__c           =   teamInstanceRecs.Team__r.Base_Team_Name__c;        // Take from Orbit tables_For Callmax File
            //mc.Calculate_Pull_Through_vod__c =   true;  
            mc.SIQ_Description_vod_del__c            =   'Cycle for MCCP';
            //mc.AZ_External_ID__c            =   mc.AZ_Country_Code__c + '_' + 'CY' + '_' + dateWithoutTimeStamp + '_' + String.valueof(teamInstanceRecs.Increment_Field__c);
            mc.SIQ_Country_Code_AZ__c            =   teamInstanceRecs.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;  
            mc.SIQ_Calculate_Pull_Through_vod__c      = true;
            mc.SIQ_External_Id_vod__c            =   mc.SIQ_Country_Code_AZ__c  + 'CY' + '_' + dateWithoutTimeStamp + '_' + String.valueof(teamInstanceRecs.Increment_Field__c);
            mcRecs.add(mc);
        }
        
        upsert mcRecs SIQ_External_Id_vod__c;
        
        createMC_Cycle_Plan();
    }
    
    global void createMC_Cycle_Plan()
    {
        List<AxtriaSalesIQTM__Position_Team_Instance__c> posTeamInstanceRecs = [select id,AxtriaSalesIQTM__Team_Instance_ID__r.Name , AxtriaSalesIQTM__Team_Instance_ID__r.AxtriaSalesIQTM__Team__r.Name, AxtriaSalesIQTM__Team_Instance_ID__r.Increment_Field__c, AxtriaSalesIQTM__Team_Instance_ID__r.AxtriaSalesIQTM__IC_EffstartDate__c, AxtriaSalesIQTM__Team_Instance_ID__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c, AxtriaSalesIQTM__Position_ID__c, AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__Client_Position_Code__c from AxtriaSalesIQTM__Position_Team_Instance__c where AxtriaSalesIQTM__Team_Instance_ID__c = :teamInstanceSelected];
        
        List<SIQ_MC_Cycle_Plan_vod_O__c> mcCyclePlan = new List<SIQ_MC_Cycle_Plan_vod_O__c>();
        
        List<AxtriaSalesIQTM__User_Access_Permission__c> uap = [select AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__User__r.FederationIdentifier from AxtriaSalesIQTM__User_Access_Permission__c];
        
        Map<String,String> posTeamToUser = new Map<String,String>();
        
        for(AxtriaSalesIQTM__User_Access_Permission__c u: uap)
        {
            string teamInstancePos = u.AxtriaSalesIQTM__Team_Instance__r.Name +'_'+ u.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
            posTeamToUser.put(teamInstancePos ,u.AxtriaSalesIQTM__User__r.FederationIdentifier);
        }
        
        system.debug('++++++++ '+posTeamToUser);
        
        for(AxtriaSalesIQTM__Position_Team_Instance__c pti : posTeamInstanceRecs)
        {
            SIQ_MC_Cycle_Plan_vod_O__c mcp = new SIQ_MC_Cycle_Plan_vod_O__c();
            string teamInstancePos = pti.AxtriaSalesIQTM__Team_Instance_ID__r.Name +'_' + pti.AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__Client_Position_Code__c; // Use Client Position Code for Territory ID
            string userID = posTeamToUser.get(teamInstancePos);
            DateTime dtStart                 =   pti.AxtriaSalesIQTM__Team_Instance_ID__r.AxtriaSalesIQTM__IC_EffstartDate__c;
            Date myDateStart                 =   date.newinstance(dtStart.year(), dtStart.month(), dtStart.day());

            String dateWithoutTimeStamp      =   dtStart.year() + '-0' + dtStart.month() + '-' + dtStart.day();
            
            mcp.Name                    =   teamInstancePos +'_'+userID;
            mcp.SIQ_Country_Code_AZ__c = pti.AxtriaSalesIQTM__Team_Instance_ID__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
            //mcp.AZ_Country_Code__c     =   pti.Team_Instance_ID__r.Team__r.Base_Team_Name__c; // Same as Earlier in MC Cycle
            mcp.SIQ_Cycle_vod__c            =   mcp.SIQ_Country_Code_AZ__c + '_' + 'CY' + '_' + dateWithoutTimeStamp + '_' + String.valueof(pti.AxtriaSalesIQTM__Team_Instance_ID__r.Increment_Field__c);
            mcp.SIQ_Owner_Id__c             =   userID;                                 // Federation ID
            mcp.SIQ_Description_vod__c      =   pti.AxtriaSalesIQTM__Team_Instance_ID__r.Name ;       // Give Cycle Name Data
            mcp.SIQ_Territory_vod__c        =   pti.AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__Client_Position_Code__c;   // Add Client Position Code instead of Salesforce ID
           // mcp.AZ_External_ID_vod__c  =   mcp.Name +'_'+mcp.Territory_vod__c;                          // Create this new field
            mcp.SIQ_External_Id_vod__c      =   mcp.Name +'_'+mcp.SIQ_Territory_vod__c;
            mcp.SIQ_Lock_vod__c             =   'false';                              // Add this new field
            mcp.SIQ_Channel_Interaction_Status_vod__c    = 'On_Schedule_vod';
            mcp.SIQ_Product_Interaction_Status_vod__c     = 'On_Schedule_vod'; 
           mcp.SIQ_RecordType__c             = '012d0000001103d';
            
            
            
            mcCyclePlan.add(mcp);
        }
        
        upsert mcCyclePlan SIQ_External_Id_vod__c;
        
        createMC_Cycle_Channel_vod();
    }
    
    global void createMC_Cycle_Channel_vod()
    {
        List<AxtriaSalesIQTM__Team_Instance__c> teamInstanceRecs = [select id, Name, AxtriaSalesIQTM__IC_EffstartDate__c, Increment_Field__c, AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Base_Team_Name__c from AxtriaSalesIQTM__Team_Instance__c where id = :teamInstanceSelected];
        //List<Channel_Weights__c> channelAndWeights = [select Name, Weights__c, Channel_Criteria_vod__c from Channel_Weights__c];

        String selectedMarket = [select AxtriaSalesIQTM__Team__r.Country_Name__c from AxtriaSalesIQTM__Team_Instance__c where id = :teamInstanceSelected].AxtriaSalesIQTM__Team__r.Country_Name__c;
        
        List<Veeva_Market_Specific__c>  veevaCriteria = [select Channel_Criteria__c, Market__c from Veeva_Market_Specific__c where Market__c = :selectedMarket];
        
        String selectedCriteria;

        if(veevaCriteria.size()>0)
        {
            selectedCriteria = veevaCriteria[0].Channel_Criteria__c;
        }
        else
        {
            selectedCriteria = 'Invalid Criteria';
        }
        Set<String> uniqueCheck = new Set<String>();

        List<SIQ_MC_Cycle_Channel_vod_O__c> mcCycleChannel = new List<SIQ_MC_Cycle_Channel_vod_O__c>();
        
        for(AxtriaSalesIQTM__Team_Instance__c teamInstance : teamInstanceRecs)
        {
            //for(Channel_Weights__c cw : channelAndWeights)
            //{
                SIQ_MC_Cycle_Channel_vod_O__c mcc = new SIQ_MC_Cycle_Channel_vod_O__c();
                
                DateTime dtStart                 =   teamInstance.AxtriaSalesIQTM__IC_EffstartDate__c;
                Date myDateStart                 =   date.newinstance(dtStart.year(), dtStart.month(), dtStart.day());

                String dateWithoutTimeStamp      =   dtStart.year() + '-' + dtStart.month() + '-' + dtStart.day(); 
                
                //mcc.CHANNEL_LABEL_VOD__C    =   'f2f';
               /* 
                if(cw.Name.contains('Email'))
                {
                    mcc.SIQ_Channel_Object_vod__c   =   'Sent_Email_vod__c';
                }
                else*/
                mcc.SIQ_Channel_Object_vod__c   =   'Call2_vod__c';
                mcc.SIQ_Country_Code_AZ__c      =    teamInstance.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                
                mcc.SIQ_Channel_Criteria_vod__c =   selectedCriteria;//the Channels, Add this field in Channel Weights object
                mcc.SIQ_Channel_Weight_vod__c   =   1;
                mcc.SIQ_Cycle_vod__c            =   mcc.SIQ_Country_Code_AZ__c  + 'CY' + '_' + dateWithoutTimeStamp + '_' + String.valueof(teamInstance.Increment_Field__c); // Use this external ID
                mcc.SIQ_External_Id_vod__c      =   teamInstance.Name + '-f2f-' + mcc.SIQ_Channel_Object_vod__c;
                //mcc.AZ_External_Id__c      =   teamInstance.Name + '-' + cw.Name + '-' + mcc.SIQ_Channel_Object_vod__c; // Not Required as of now. May be used in future;
                mcc.SIQ_RecordTypeId__c         =   '012d0000001103V';        // Add this static Field
                mcc.External_ID_Axtria__c = mcc.SIQ_External_Id_vod__c;

                if(!uniqueCheck.contains(mcc.SIQ_External_Id_vod__c))
                {
                    mcCycleChannel.add(mcc);
                    uniqueCheck.add(mcc.SIQ_External_Id_vod__c);
                }
            //}
        }
        
        upsert mcCycleChannel External_ID_Axtria__c; 
        createMC_Cycle_Product_vod();
        
    }
    
    List<String> allChannels = new List<String>{'F2F'};
    
    global void createMC_Cycle_Product_vod()
    {
        String teamInstanceName = [select Name from AxtriaSalesIQTM__Team_Instance__c where id = :teamInstanceSelected LIMIT 1].Name;

        List<Product_Catalog__c> allProductsAndWeights = [select Name, Weights__c, External_ID__c from  Product_Catalog__c where Team_Instance__c = :teamInstanceSelected];

        AxtriaSalesIQTM__Team_Instance__c teamInstance = [select id,AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Base_Team_Name__c, Name from AxtriaSalesIQTM__Team_Instance__c where id= :teamInstanceSelected];
        
        List<SIQ_MC_Cycle_Product_vod_O__c> mcCycleProduct = new List<SIQ_MC_Cycle_Product_vod_O__c>();
        
        
        
        for(Product_Catalog__c pro : allProductsAndWeights)         // What all products we have for this team instance //Get all product code from Vardhan File (Master List)
        {
            
            
            string object1;
            
            for(String channel : allChannels)
            {
                SIQ_MC_Cycle_Product_vod_O__c mcp = new SIQ_MC_Cycle_Product_vod_O__c();
                
                if(channel == 'Email')
                object1 = 'Sent_Email_vod__c';
                else 
                object1 = 'Call2_vod__c';
                    
                
                //mcp.External_Id_vod__c = teamInstance.Name + '-' + channel + '-' + object1;
                //mcp.Channel_Label_vod__c = channel;
                mcp.SIQ_Cycle_Channel_vod__c = teamInstance.Name + '-' + channel + '-' + object1;  
                
                mcp.SIQ_Product_vod__c = pro.External_ID__c;       // Product Code is unique here
                mcp.SIQ_Product_Weight_vod__c = 1;        // Keep it Static, 1 for all
                //mcp.AZ_External_Id__c = teamInstance.Name + '__' + channel + '__' +pro.External_ID__c + '_'+pro.Name;
                mcp.SIQ_External_Id_vod__c = teamInstance.Name + '__' + channel + '__' +pro.External_ID__c + '_'+pro.Name;
                mcp.External_ID_Axtria__c = mcp.SIQ_External_Id_vod__c;
                
                mcCycleProduct.add(mcp);
            }
            
        }
        
        system.debug('+++++++++++'+ mcCycleProduct);
        upsert mcCycleProduct External_ID_Axtria__c;
        
        create_MC_Cycle_Plan_Target_vod();
    }       
    
    global void create_MC_Cycle_Plan_Target_vod()
    {
        AZ_Integration_MC_Target_MP u1 = new AZ_Integration_MC_Target_MP(teamInstanceSelected, allChannels);
        
        Database.executeBatch(u1, 2000);

        create_MC_Cycle_Plan_Channel_vod();
    }
    
    global void create_MC_Cycle_Plan_Channel_vod() 
    {
        AZ_Integration_New_Utility_MP u1 = new AZ_Integration_New_Utility_MP(teamInstanceSelected, allChannels);
        
        Database.executeBatch(u1, 2000);
        
        create_MC_Cycle_Plan_Product_vod();
    }
    
    global  void create_MC_Cycle_Plan_Product_vod()
    {
        AZ_IntegrationUtility u1 = new AZ_IntegrationUtility(teamInstanceSelected, allChannels);
        
        Database.executeBatch(u1, 2000);
        
    }
}