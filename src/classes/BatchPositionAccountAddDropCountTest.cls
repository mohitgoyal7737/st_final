@isTest
public class BatchPositionAccountAddDropCountTest {
    static testMethod void testMethod1() {
        
        String className = 'BatchPositionAccountAddDropCountTest';
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        acc.AccountNumber = 'ES206265';
        acc.Type = 'HCO';
        SnTDMLSecurityUtil.insertRecords(acc,className);
        
        Account acc1= TestDataFactory.createAccount();
        acc1.AccountNumber = 'ES206211';
        acc1.Type = 'HCO';
        SnTDMLSecurityUtil.insertRecords(acc1,className);
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Name = 'USA';
        countr.Email_ID__c = 'test123@axtria.com';
        SnTDMLSecurityUtil.insertRecords(countr,className);
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins1,className);
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
        SnTDMLSecurityUtil.insertRecords(scen,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        //teamins.AxtriaSalesIQTM__Country__c = countr.id;
        teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        //teamins.Country_Name__c = 'USA';
        SnTDMLSecurityUtil.insertRecords(teamins,className);
        AxtriaSalesIQTM__Team_Instance__c teamins2 = TestDataFactory.createTeamInstance(team);
        teamins2.AxtriaSalesIQTM__Scenario__c = scen.id;
        //teamins.AxtriaSalesIQTM__Country__c = countr.id;
        teamins2.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        //teamins.Country_Name__c = 'USA';
        SnTDMLSecurityUtil.insertRecords(teamins2,className);
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        SnTDMLSecurityUtil.insertRecords(pcc,className);
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        SnTDMLSecurityUtil.insertRecords(mmc,className);
        
        List<AxtriaSalesIQTM__Position__c> posList = new List<AxtriaSalesIQTM__Position__c>();
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.One_day_Previous_Count__c = 5;
        pos.Current_Count__c = 3;
        posList.add(pos);
        SnTDMLSecurityUtil.insertRecords(posList,className);
        
        AxtriaSalesIQTM__Position__c pos1= TestDataFactory.createPosition(team,teamins2);
        SnTDMLSecurityUtil.insertRecords(pos1,className);
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        //posAccount.AxtriaSalesIQTM__Account__r.AccountNumber = 'ES206265';
        SnTDMLSecurityUtil.insertRecords(posAccount,className);
        
        AxtriaSalesIQTM__Position_Account__c posAccount1 = TestDataFactory.createPositionAccount(acc1,pos1,teamins2);
        //posAccount.AxtriaSalesIQTM__Account__r.AccountNumber = 'ES206265';
        SnTDMLSecurityUtil.insertRecords(posAccount1,className);
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        SnTDMLSecurityUtil.insertRecords(pPriority,className);
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        positionAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c = true;
        positionAccountCallPlan.Final_TCF_Approved__c = 3.0;
        positionAccountCallPlan.Final_TCF__c = 3.0;
        positionAccountCallPlan.P1__c = 'GIST';
        //positionAccountCallPlan.AxtriaSalesIQTM__Account__r.AccountNumber = 'ES206265';
        SnTDMLSecurityUtil.insertRecords(positionAccountCallPlan,className);
       
        temp_Obj__c newpa= new temp_Obj__c();
        newpa.AccountNumber__c = acc.AccountNumber;
        newpa.Territory_ID__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        newpa.Team_Name__c = teamins.AxtriaSalesIQTM__Team__r.name;
        newpa.Account_Type__c = acc.type;
        SnTDMLSecurityUtil.insertRecords(newpa,className);

        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            BatchPositionAccountAddDropCount obj=new BatchPositionAccountAddDropCount('USA'); 
            obj.query = 'SELECT Id, Name,PA_Active__c,PA_Inactive__c, One_day_Previous_Count__c, Current_Count__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Client_Territory_Name__c FROM AxtriaSalesIQTM__Position__c where  AxtriaSalesIQTM__Hierarchy_Level__c = \'1\' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = \'Current\' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = \'Live\'';
            Database.executeBatch(obj);
        }
        Test.stopTest();
    }
}