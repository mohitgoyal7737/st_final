global class BatchDeltaMCChannelStatus implements Database.Batchable<sObject>
{
    public String query;
   public Datetime Lmd;
    List<SIQ_MC_Cycle_Plan_Channel_vod_O__c> mCyclePlanChannel;
    List<Parent_PACP__c> pacpRecs;
    List<String> allTeamInstancespro;
    List<String> allTeamInstances;
    List<String> allChannels;
    String teamInstanceSelected;
    public Boolean chaining = false;
    global BatchDeltaMCChannelStatus(Date lastModifiedDate,string teamInstanceSelectedTemp, List<String> allChannelsTemp)  
    {
        /*Lmd= lastModifiedDate;
         teamInstanceSelected = teamInstanceSelectedTemp;
         query = 'select id, Team_Instance__c,Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c, (select id, AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Base_Team_Name__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c,AxtriaSalesIQTM__Team_Instance__r.Increment_Field__c  ,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__c, Final_TCF__c,Segment10__c,AxtriaSalesIQTM__lastApprovedTarget__c from Call_Plan_Summary__r  where AxtriaSalesIQTM__Position__c !=null and P1__c != null ) from Parent_PACP__c where lastModifiedDate= Last_N_Days:1 and Team_Instance__c = :teamInstanceSelected';
          allChannels = allChannelsTemp;*/
    }

     global BatchDeltaMCChannelStatus(Date lastModifiedDate,List<String> teamInstanceSelectedTemp, List<String> allChannelsTemp)  
    {
        /*Lmd= lastModifiedDate;
         query = 'select id, Team_Instance__c,Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c, (select id, AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Base_Team_Name__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c,AxtriaSalesIQTM__Team_Instance__r.Increment_Field__c  ,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__c, Final_TCF__c,Segment10__c,AxtriaSalesIQTM__lastApprovedTarget__c from Call_Plan_Summary__r  where AxtriaSalesIQTM__Position__c !=null and P1__c != null ) from Parent_PACP__c where lastModifiedDate= Last_N_Days:1 and Team_Instance__c in :allTeamInstances';
         allTeamInstances = new List<String>(teamInstanceSelectedTemp);
          allChannels = allChannelsTemp;*/
    }
    global BatchDeltaMCChannelStatus(Datetime lastjobDate,string teamInstanceSelectedTemp, List<String> allChannelsTemp)  
    {
        Lmd= lastjobDate;
        teamInstanceSelected = teamInstanceSelectedTemp;
        query = 'select id, Team_Instance__c,Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c, (select id, AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Base_Team_Name__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c,AxtriaSalesIQTM__Team_Instance__r.Increment_Field__c  ,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaARSnT__Final_TCF_Approved__c, Final_TCF__c,Segment10__c,AxtriaSalesIQTM__lastApprovedTarget__c from Call_Plan_Summary__r  where AxtriaSalesIQTM__Position__c !=null and P1__c != null ),Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaARSnT__Cluster_Information__c,Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaARSnT__Country_Veeva_Code__c  from Parent_PACP__c where LastModifiedDate >: Lmd  and Team_Instance__c = :teamInstanceSelected';
        allChannels = allChannelsTemp;
    }

     global BatchDeltaMCChannelStatus(Datetime lastjobDate,List<String> teamInstanceSelectedTemp, List<String> allChannelsTemp)  
    {
        Lmd= lastjobDate;
        query = 'select id, Team_Instance__c,Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c, (select id, AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Base_Team_Name__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c,AxtriaSalesIQTM__Team_Instance__r.Increment_Field__c  ,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaARSnT__Final_TCF_Approved__c,Final_TCF__c,Segment10__c,AxtriaSalesIQTM__lastApprovedTarget__c from Call_Plan_Summary__r  where AxtriaSalesIQTM__Position__c !=null and P1__c != null ),Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaARSnT__Cluster_Information__c,Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaARSnT__Country_Veeva_Code__c  from Parent_PACP__c where LastModifiedDate >: Lmd  and Team_Instance__c in :allTeamInstances';
        allTeamInstances = new List<String>(teamInstanceSelectedTemp);
        allChannels = allChannelsTemp;
    }
    
    global BatchDeltaMCChannelStatus(Datetime lastjobDate,List<String> teamInstanceSelectedTemp, List<String> allChannelsTemp, Boolean chain)  
    {
       /* Chaining = chain;
        Lmd= lastjobDate;
         query = 'select id, Team_Instance__c,Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c, (select id, AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Base_Team_Name__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c,AxtriaSalesIQTM__Team_Instance__r.Increment_Field__c  ,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, Final_TCF__c,Segment10__c,AxtriaSalesIQTM__lastApprovedTarget__c from Call_Plan_Summary__r  where AxtriaSalesIQTM__Position__c !=null and P1__c != null ) from Parent_PACP__c where LastModifiedDate >: Lmd  and Team_Instance__c in :allTeamInstances';
         allTeamInstances = new List<String>(teamInstanceSelectedTemp);
          allChannels = allChannelsTemp;*/
    }

    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator(query);
    }
     public void create_MC_Cycle_Plan_Channel_vod(List<Parent_PACP__c> scopePacpProRecs)
    {
        system.debug('++query'+query);
        allTeamInstancespro= new List<String>();
        for(Parent_PACP__c pacprecords: scopePacpProRecs)
        {
            allTeamInstancespro.add(pacprecords.Team_Instance__c);
        }
         system.debug('++query'+query);
        //pacpRecs = scopePacpProRecs;
        mCyclePlanChannel = new List<SIQ_MC_Cycle_Plan_Channel_vod_O__c>();
        Set<String> allRecs = new Set<String>();

        String selectedMarket = [select AxtriaSalesIQTM__Team__r.Country_Name__c from AxtriaSalesIQTM__Team_Instance__c where id = :allTeamInstancespro[0]].AxtriaSalesIQTM__Team__r.Country_Name__c;

        List<Veeva_Market_Specific__c>  veevaCriteria = [select MCCP_Target_logic__c, Channel_Criteria__c,Market__c,MC_Cycle_Threshold_Max__c,MC_Cycle_Threshold_Min__c,MC_Cycle_Channel_Record_Type__c,MC_Cycle_Plan_Channel_Record_Type__c,MC_Cycle_Plan_Product_Record_Type__c,MC_Cycle_Plan_Record_Type__c,MC_Cycle_Plan_Target_Record_Type__c,MC_Cycle_Product_Record_Type__c,MC_Cycle_Record_Type__c from Veeva_Market_Specific__c where Market__c = :selectedMarket];
        
        Decimal max = 0;
        Decimal sum = 0;

        for(Parent_PACP__c ppacp : scopePacpProRecs)
        {
            system.debug('++ppacp'+ppacp);
            max = 0;
            sum = 0;
            Integer f2fCalls = 0;
            
            Boolean flag = false;
            Boolean deltacheck=false;
            SIQ_MC_Cycle_Plan_Channel_vod_O__c mcpc = new SIQ_MC_Cycle_Plan_Channel_vod_O__c();

            String channels = 'F2F';
            for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : ppacp.Call_Plan_Summary__r)
            {
                if(pacp.AxtriaSalesIQTM__lastApprovedTarget__c==true)
                {
                    system.debug('++hey');
                    deltacheck= true;
                    if(pacp.AxtriaARSnT__Final_TCF_Approved__c == null)
                        pacp.AxtriaARSnT__Final_TCF_Approved__c = 0;
                    if(max < pacp.AxtriaARSnT__Final_TCF_Approved__c)
                    {
                        max = Integer.valueOf(pacp.AxtriaARSnT__Final_TCF_Approved__c);
                    }

                    flag = true;
                    sum = sum + pacp.AxtriaARSnT__Final_TCF_Approved__c;
                
                    string teamPos = pacp.AxtriaSalesIQTM__Team_Instance__r.Name +'_' + pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
                    mcpc.SIQ_Cycle_Channel_vod__c       =   pacp.AxtriaSalesIQTM__Team_Instance__r.Name + '-' + channels + '-Call2_vod__c';
                    
                    DateTime dtStart                 =   pacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c;
                    Date myDateStart                 =   date.newinstance(dtStart.year(), dtStart.month(), dtStart.day());
                    String dateWithoutTimeStamp      =   dtStart.year() + '-0' + dtStart.month() + '-' + dtStart.day();
                    mcpc.SIQ_Cycle_Plan_Target_vod__c   =   teamPos + '_' + pacp.AxtriaSalesIQTM__Account__r.AccountNumber;
                    mcpc.SIQ_External_Id_vod__c =teamPos + '-' +channels + '-'+ pacp.AxtriaSalesIQTM__Account__r.AccountNumber;
                }    
                if(deltacheck==true)
                {    
                    system.debug('++if');
                    mcpc.Rec_Status__c = 'Updated';
                    mcpc.External_ID_Axtria__c = mcpc.SIQ_External_Id_vod__c;
                    mcpc.Team_Instance__c = ppacp.Team_Instance__c;
                    if(ppacp.Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaARSnT__Cluster_Information__c=='No Cluster')
                    {
                        mcpc.CountryID__c = ppacp.Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                    }
                    else if(ppacp.Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaARSnT__Cluster_Information__c=='SalesIQ Cluster')
                    {
                        mcpc.CountryID__c = ppacp.Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                    }
                    else if(ppacp.Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaARSnT__Cluster_Information__c=='Veeva Cluster')
                    {
                        mcpc.CountryID__c = ppacp.Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaARSnT__Country_Veeva_Code__c;
                    }
                    //mcpc.CountryID__c = ppacp.Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                    
                    if(flag && !allRecs.contains(mcpc.SIQ_External_Id_vod__c))
                    {
                        mcpc.SIQ_RecordTypeId__c  = veevaCriteria[0].MC_Cycle_Plan_Channel_Record_Type__c;

                        if(veevaCriteria[0].MCCP_Target_logic__c == 'SUM' || veevaCriteria[0].MCCP_Target_logic__c == 'Sum' || veevaCriteria[0].MCCP_Target_logic__c == 'sum')
                        {
                            mcpc.SIQ_Channel_Activity_Goal_vod__c = sum;
                            mcpc.SIQ_Channel_Activity_Max_vod__c = sum;
                        }
                        else
                        {
                            mcpc.SIQ_Channel_Activity_Goal_vod__c = max;
                            mcpc.SIQ_Channel_Activity_Max_vod__c = max;  
                        }
                        mCyclePlanChannel.add(mcpc);
                        allRecs.add(mcpc.SIQ_External_Id_vod__c);
                    }
                }
                else
                {
                    system.debug('++else');
                    mcpc.Rec_Status__c = 'Deleted';
                    flag = true;
                    string teamPos = pacp.AxtriaSalesIQTM__Team_Instance__r.Name +'_' + pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
                    mcpc.Team_Instance__c = ppacp.Team_Instance__c;
                    if(ppacp.Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaARSnT__Cluster_Information__c=='No Cluster')
                    {
                        mcpc.CountryID__c = ppacp.Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                    }
                    else if(ppacp.Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaARSnT__Cluster_Information__c=='SalesIQ Cluster')
                    {
                        mcpc.CountryID__c = ppacp.Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                    }
                    else if(ppacp.Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaARSnT__Cluster_Information__c=='Veeva Cluster')
                    {
                        mcpc.CountryID__c = ppacp.Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaARSnT__Country_Veeva_Code__c;
                    }
                    //mcpc.CountryID__c = ppacp.Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                    mcpc.External_ID_Axtria__c=teamPos + '-' +channels + '-'+ pacp.AxtriaSalesIQTM__Account__r.AccountNumber;
                    system.debug('+++External_ID_Axtria__c'+mcpc.External_ID_Axtria__c);
                    if(flag && !allRecs.contains(mcpc.External_ID_Axtria__c))
                    {
                        system.debug('in else+++ ');
                        mCyclePlanChannel.add(mcpc); 
                        allRecs.add(mcpc.External_ID_Axtria__c);    
                    } 
                }
            }
        }
        upsert mCyclePlanChannel External_ID_Axtria__c;
    }
    

    global void execute(Database.BatchableContext BC, List<Sobject> scopePacpProRecs) 
    {
        create_MC_Cycle_Plan_Channel_vod(scopePacpProRecs);
        
    }

    global void finish(Database.BatchableContext BC) 
    {
        List<Scheduler_Log__c> sc = [select id from Scheduler_Log__c where Job_Name__c = 'MCCP Delta' and Job_Status__c = 'Failed' order by Created_Date2__c desc LIMIT 1];
        String batchID = null;
        if(sc.size() > 0)
        {
            batchID = sc[0].id;
        }
        
        changeMCproductTest u1 = new changeMCproductTest(lmd,allTeamInstances, allChannels,batchID);        
        Database.executeBatch(u1,2000);
    }
}