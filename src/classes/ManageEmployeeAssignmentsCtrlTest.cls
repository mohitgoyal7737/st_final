@IsTest
public class ManageEmployeeAssignmentsCtrlTest{
 /* static List<CR_Employee_Feed__c> empFeed = new List<CR_Employee_Feed__c>();
    static List<AxtriaSalesIQTM__Employee__c> employees ;
    static list<AxtriaSalesIQTM__Position__c> positions ;
    public static void createData(){
      
        List<AxtriaSalesIQTM__Team__c> teams = TestDataFactory.createTeam();
        List<AxtriaSalesIQTM__Team_Instance__c> teaminstances =  TestDataFactory.createTeamInstance(teams);
        
        positions = TestDataFactory.createPositions('TestPos',6,'KAOM',teaminstances);
        for(AxtriaSalesIQTM__Position__c pos : positions){
          pos.AxtriaSalesIQTM__Effective_Start_Date__c = date.newinstance(2017, 4, 15);  
            pos.AxtriaSalesIQTM__Effective_End_Date__c = date.newinstance(2017, 8, 31);
            pos.AxtriaSalesIQTM__Position_Type__c = 'Territory';
        }
        positions[2].AxtriaSalesIQTM__Effective_Start_Date__c = date.newinstance(2017, 6, 15);
        insert positions ;
        
        employees = TestDataFactory.createEmployees('Test','Emp','YES','Promotion','1001',date.today(), 6);
        for(AxtriaSalesIQTM__Employee__c emp : employees){
            emp.Hire_Date__c = date.newinstance(2017, 2, 1);
        }
        employees[2].AxtriaSalesIQTM__HR_Termination_Date__c = date.newinstance(2023, 5, 31) ;
        insert employees ;
        
        List<AxtriaSalesIQTM__Position_Employee__c> posEmps = TestDataFactory.createPositionEmployees(employees ,positions ) ;
        for(AxtriaSalesIQTM__Position_Employee__c posemp : posEmps){
            posemp.AxtriaSalesIQTM__Assignment_Type__c = 'Primary';            
            posemp.AxtriaSalesIQTM__Effective_Start_Date__c = date.newinstance(2017, 5, 15);
            posemp.AxtriaSalesIQTM__Effective_End_Date__c = date.newinstance(2017, 6, 31);
        }
        posemps[1].AxtriaSalesIQTM__Employee__c = employees[0].id;
        Insert posEmps ;
        
        empFeed = TestDataFactory.createEmpFeed('testEmpFeed','Leave of Absence', employees,positions, 3) ;
        empFeed[0].Event_Name__c = 'Separation';
        empFeed[1].Event_Name__c = 'Promotion';
        empFeed[2].Event_Name__c = 'New Hire';
        insert empFeed ;
        
        List<AxtriaSalesIQTM__Change_Request_Type__c> changetypeList = TestDataFactory.createChangeRequesttype('Employee Assignment',1);
        insert changetypeList ;
        
        AxtriaSalesIQTM__TotalApproval__c setting = new AxtriaSalesIQTM__TotalApproval__c();
        setting.AxtriaSalesIQTM__No_Of_Approval__c = 1;
        setting.Name = 'Approvals';
        insert setting;
    }
    
    public testMethod static void validateController(){
        createData();
        ApexPages.currentPage().getParameters().put('Employee', employees[0].AxtriaSalesIQTM__Employee_ID__c);
        ApexPages.currentPage().getParameters().put('EventName','Promotion');
    ApexPages.currentPage().getParameters().put('Id',empFeed[0].id);

        ManageEmployeeAssignmentsCtrl meaCntrl = new ManageEmployeeAssignmentsCtrl();
        meaCntrl.posChosen = positions[0].id;
       
        meaCntrl.selectedID = '0' ;
        meaCntrl.addAssignment();
        meaCntrl.makeFieldsEditable();
        meaCntrl.saveAssignments();
        meaCntrl.deleteAssignment();
        meaCntrl.DisplayPositionAssignments();
        meaCntrl.currentAssignments();       
        meaCntrl.cancelChanges();
        meaCntrl.backToWorkbench();
        meaCntrl.DisplayEmployeeAssignments();
        meaCntrl.positionAssignmentDetails();
        meaCntrl.showPopup();
        meaCntrl.closePopup();
        meaCntrl.runSearch();
        meaCntrl.backToAlignment();
        meaCntrl.closeCRMsg();
        ManageEmployeeAssignmentsCtrl.fillPositionTable(meaCntrl.query);
        List<selectoption> namelist = meaCntrl.reasonCodeList ;
        List<selectoption> namelist2 = meaCntrl.assignmenttypeList;
        //meaCntrl.makeFieldsEditable();
    }
    public testMethod static void validateController2(){
        createData();
        ApexPages.currentPage().getParameters().put('Employee', employees[2].AxtriaSalesIQTM__Employee_ID__c);
        ApexPages.currentPage().getParameters().put('EventName','Promotion');
    ApexPages.currentPage().getParameters().put('Id',empFeed[2].id);

        ManageEmployeeAssignmentsCtrl meaCntrl = new ManageEmployeeAssignmentsCtrl();
        meaCntrl.posChosen = positions[2].id;
       
        meaCntrl.selectedID = '0' ;
        meaCntrl.makeFieldsEditable();
        meaCntrl.saveAssignments();
        meaCntrl.deleteAssignment();
        meaCntrl.DisplayPositionAssignments();
        meaCntrl.currentAssignments();        
        meaCntrl.addAssignment();
        meaCntrl.cancelChanges();
        meaCntrl.backToWorkbench();
        meaCntrl.DisplayEmployeeAssignments();
        meaCntrl.positionAssignmentDetails();
        meaCntrl.showPopup();
        meaCntrl.closePopup();
        meaCntrl.runSearch();
        meaCntrl.backToAlignment();
        meaCntrl.closeCRMsg();
        ManageEmployeeAssignmentsCtrl.fillPositionTable(meaCntrl.query);
        List<selectoption> namelist = meaCntrl.reasonCodeList ;
        List<selectoption> namelist2 = meaCntrl.assignmenttypeList;
        //meaCntrl.makeFieldsEditable();
    } */
}