@istest 
public class ExpiredSNTAssignmentBatchTest {
    @istest static void ExpiredSNTAssignmentBatchTest()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__AccountType__c ='HCP';
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'ONCO';
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__IC_EffstartDate__c = date.today()-2;
        teamins.AxtriaSalesIQTM__IC_EffEndDate__c = date.today()-1;
        insert teamins;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__Client_Position_Code__c = 'N003';
        pos.AxtriaSalesIQTM__Effective_End_Date__c = date.today()-5;
        pos.AxtriaSalesIQTM__Effective_Start_Date__c= date.today()-6;
        pos.AxtriaSalesIQTM__Inactive__c = true;
        pos.AxtriaSalesIQTM__Team_iD__c = team.Id;
        
        insert pos;
        AxtriaSalesIQTM__User_Access_Permission__c u = TestDataFactory.createUserAccessPerm(pos, teamins, UserInfo.getUserId());
        u.AxtriaSalesIQTM__Position__c=pos.id;
        u.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        u.AxtriaSalesIQTM__User__c = loggedInUser.id;        
        insert u;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        positionAccountCallPlan.AxtriaSalesIQTM__isIncludedCallPlan__c = true;
        positionAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c = true;
        positionAccountCallPlan.AxtriaSalesIQTM__Effective_End_Date__c = date.today()-1;
        positionAccountCallPlan.AxtriaSalesIQTM__Effective_Start_Date__c= date.today()-2;
        positionAccountCallPlan.AxtriaSalesIQTM__Position__c = pos.id;
        insert positionAccountCallPlan;
        
        AxtriaSalesIQTM__Product__c product = new AxtriaSalesIQTM__Product__c();
        product.Team_Instance__c = teamins.id;
        product.CurrencyIsoCode = 'USD'; 
        product.Name= 'Test';
        product.AxtriaSalesIQTM__Product_Code__c = 'testproduct'; 
        
        product.AxtriaSalesIQTM__Effective_End_Date__c = date.today();
        product.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        insert product;


        AxtriaSalesIQTM__Position_Product__c positionproduct = new AxtriaSalesIQTM__Position_Product__c();
        positionproduct.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        positionproduct.AxtriaSalesIQTM__Product_Master__c = product.id;
        //positionproduct.AxtriaSalesIQTM__Product__c = product.id;
        positionproduct.AxtriaSalesIQTM__Position__c = pos.id;
        positionproduct.Product_Catalog__c = pcc.id;
        positionproduct.CurrencyIsoCode = 'USD'; 
        positionproduct.AxtriaSalesIQTM__Product_Weight__c = 1;
        positionproduct.AxtriaSalesIQTM__isActive__c = true;
        positionproduct.Business_Days_in_Cycle__c = 1;
        positionproduct.Calls_Day__c = 1;
        positionproduct.Holidays__c = 1;
        positionproduct.Other_Days_Off__c = 1;
        positionproduct.Vacation_Days__c = 1;
        positionproduct.AxtriaSalesIQTM__Product_Weight__c = 1;
        positionproduct.AxtriaSalesIQTM__Effective_End_Date__c = date.today()-1;
        positionproduct.AxtriaSalesIQTM__Effective_Start_Date__c = date.today()-10;
        positionproduct.Product_Catalog__c = pcc.id;

        insert positionproduct;
        
        set<Id> positionId = new set<Id>();
        positionId.add(pos.Id);
        Set<String> t = new Set<String>();
        t.add(pos.AxtriaSalesIQTM__Client_Position_Code__c + '_' + pos.AxtriaSalesIQTM__Team_iD__c);
        System.debug(t);
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            ExpiredSNTAssignmentBatch obj = new ExpiredSNTAssignmentBatch(positionId,'Position_Account_Call_Plan__c');
            obj.clientPosTeamKey = t;
            obj.query = 'Select Id, AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__c,AxtriaSalesIQTM__isincludedCallPlan__c, AxtriaSalesIQTM__lastApprovedTarget__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c,AxtriaSalesIQTM__Effective_End_Date__c From AxtriaSalesIQTM__Position_Account_Call_Plan__c';
            Database.executeBatch(obj);
        }
        Test.stopTest();
    }
}