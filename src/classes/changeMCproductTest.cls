global class changeMCproductTest implements Database.Batchable<sObject> 
{
    public String query;
    public Boolean flag = true;
    //public Date lmd;
     List<String> allChannels;
    String teamInstanceSelected;
    List<String> allTeamInstances;
    public Datetime Lmd;
     public id schedularid;
	public Boolean chaining = false;
   global changeMCproductTest(string teamInstanceSelectedTemp, List<String> allChannelsTemp)
    { 
        
/*       query = 'select id, Rec_Status__c from SIQ_MC_Cycle_Plan_Product_vod_O__c where Team_Instance__c in :teamInstanceSelected';
        teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;*/
    }
     global changeMCproductTest(List<string> teamInstanceSelectedTemp, List<String> allChannelsTemp)
    { 
        /*allTeamInstances = new List<String>(teamInstanceSelectedTemp);
        system.debug('++++++ Hey All TO are '+ allTeamInstances);
       query = 'select id, Rec_Status__c from SIQ_MC_Cycle_Plan_Product_vod_O__c where Team_Instance__c in :allTeamInstances';
       
        allChannels = allChannelsTemp;*/
    }
    global changeMCproductTest(Datetime lastjobDate,string teamInstanceSelectedTemp, List<String> allChannelsTemp,string batchID)
    { 
        Lmd=lastjobDate;
        schedularid= batchID;
       query = 'select id, Rec_Status__c from SIQ_MC_Cycle_Plan_Product_vod_O__c where Team_Instance__c in :teamInstanceSelected';
        teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;
    }
     global changeMCproductTest(Datetime lastjobDate,List<string> teamInstanceSelectedTemp, List<String> allChannelsTemp,string batchID)
    { 
        Lmd=lastjobDate;
         schedularid= batchID;
        allTeamInstances = new List<String>(teamInstanceSelectedTemp);
        //system.debug('++++++ Hey All TO are '+ allTeamInstances);
       query = 'select id, Rec_Status__c from SIQ_MC_Cycle_Plan_Product_vod_O__c where Team_Instance__c in :allTeamInstances';
       
        allChannels = allChannelsTemp;
    }
    
    global changeMCproductTest(Datetime lastjobDate,List<string> teamInstanceSelectedTemp, List<String> allChannelsTemp,string batchID, Boolean chain)
    { 
       /* chaining = chain;
        Lmd=lastjobDate;
         schedularid= batchID;
        allTeamInstances = new List<String>(teamInstanceSelectedTemp);
        //system.debug('++++++ Hey All TO are '+ allTeamInstances);
       query = 'select id, Rec_Status__c from SIQ_MC_Cycle_Plan_Product_vod_O__c where Team_Instance__c in :allTeamInstances';
       
        allChannels = allChannelsTemp;*/
    }



    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        return Database.getQueryLocator(query);
    }

     global void execute(Database.BatchableContext BC, List<SIQ_MC_Cycle_Plan_Product_vod_O__c> scopePacpProRecs)
    {
        system.debug('++query'+query);
        for(SIQ_MC_Cycle_Plan_Product_vod_O__c mcTarget : scopePacpProRecs)
        {
            mcTarget.Rec_Status__c = '';
        }
        update scopePacpProRecs;
    }

    global void finish(Database.BatchableContext BC)
    {
        //lmd=Date.Today();
        system.debug('++lmd'+lmd);
        BatchDeltaMCProduct u2 = new BatchDeltaMCProduct(lmd,allTeamInstances,allChannels,schedularid);
        database.executeBatch(u2,2000);
    }
}