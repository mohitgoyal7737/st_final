global with sharing class ExpiredCallPlanAssignmentBatch implements Database.Batchable<sObject>, Database.Stateful {
    public String query;
    public Set<Id> inactivePosId;
    public Date processDate;
    public String type;
    public Set<String> clientPosSet;
    public Set<String> teamSet;
    public Set<String> clientPosTeamKey;

    global ExpiredCallPlanAssignmentBatch(set<Id> positionId) {
        this.query = query;
        inactivePosId = new Set<Id>();
        teamSet = new Set<String>();
        clientPosSet = new Set<String>();
        clientPosTeamKey = new Set<String>();

        inactivePosId.addAll(positionId);

        double processDay;
        
        processDate = System.today();

        SnTDMLSecurityUtil.printDebugMessage('processDate - '+processDate);
        
        List<AxtriaSalesIQTM__Position__c> allPosList = [Select Id, AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Team_iD__c,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position__c where Id in :inactivePosId];

        SnTDMLSecurityUtil.printDebugMessage('allPosList - '+allPosList);

        for(AxtriaSalesIQTM__Position__c posRec : allPosList)
        {
            clientPosSet.add(posRec.AxtriaSalesIQTM__Client_Position_Code__c);
            teamSet.add(posRec.AxtriaSalesIQTM__Team_iD__c);
            clientPosTeamKey.add(posRec.AxtriaSalesIQTM__Client_Position_Code__c + '_' + posRec.AxtriaSalesIQTM__Team_iD__c);
        }

        SnTDMLSecurityUtil.printDebugMessage('teamSet - '+teamSet);
        SnTDMLSecurityUtil.printDebugMessage('clientPosSet - '+allPosList);

        query = 'Select Id, AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__c,AxtriaSalesIQTM__isincludedCallPlan__c, AxtriaSalesIQTM__lastApprovedTarget__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c,AxtriaSalesIQTM__Effective_End_Date__c From AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c <: processDate and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c in : clientPosSet and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__c in :teamSet and AxtriaSalesIQTM__Team_Instance__c != null and AxtriaSalesIQTM__Account__c != null and AxtriaSalesIQTM__isincludedCallPlan__c = true and AxtriaSalesIQTM__lastApprovedTarget__c = true order by LastModifiedDate desc';

        
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scope) {

        SnTDMLSecurityUtil.printDebugMessage('---- updateCallPlan----');

        list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> callPlanList=new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();

        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c callPlanRec :scope)
        {
            if(clientPosTeamKey.contains(callPlanRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' + callPlanRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__c))
            {
                if(callPlanRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c <= System.today())
                {
                    callPlanRec.AxtriaSalesIQTM__isincludedCallPlan__c = false;
                    callPlanRec.AxtriaSalesIQTM__lastApprovedTarget__c = false;
                }

                if(callPlanRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c < callPlanRec.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c)
                    callPlanRec.AxtriaSalesIQTM__Effective_End_Date__c = callPlanRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c;
                
                callPlanList.add(callPlanRec);  
                SnTDMLSecurityUtil.printDebugMessage('---- callPlanRec----' +callPlanRec);
            }
        }

        SnTDMLSecurityUtil.printDebugMessage('---- callPlanList----' +callPlanList);
        SnTDMLSecurityUtil.printDebugMessage('---- callPlanList.size()----' +callPlanList.size());

        if(callPlanList.size() > 0){
            //update callPlanList;
            SnTDMLSecurityUtil.updateRecords(callPlanList, 'ExpiredCallPlanAssignmentBatch');
        }
        
    }

    global void finish(Database.BatchableContext BC) {

        ExpiredPositionProductAssignmentBatch b5 = new ExpiredPositionProductAssignmentBatch(inactivePosId);
        Database.executeBatch(b5,200);
    }
}