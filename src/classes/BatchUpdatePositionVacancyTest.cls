@isTest
private class BatchUpdatePositionVacancyTest {
	 @testSetup 
    static void setup() {
       List<AxtriaSalesIQTM__Team__c> teamList= TestDataFactoryAZ.createTeam();
       
       List<AxtriaSalesIQTM__Team_Instance__c> teamIns=TestDataFactoryAZ.createTeamInstance(teamList) ;
      for(Integer i=0;i<teamIns.size();i++)
      {
      	System.debug(teamIns.get(i).AxtriaSalesIQTM__Team__c);
      	System.debug(teamIns.get(i).id);
      }
       List<AxtriaSalesIQTM__Position__c> posList=TestDataFactoryAZ.createPositions('a',5,'CAEOS',teamIns);
       insert posList;
    }
    static testmethod void test() { 
    	Scheduler_Log__c sJob = new Scheduler_Log__c();
		
		sJob.Job_Name__c = 'Employee Feed';
		sJob.Job_Status__c = 'Failed';
		sJob.Job_Type__c='Inbound';
		//sJob.CreatedDate = System.today();
	
		insert sJob;
	   
	    String batchID = sJob.Id;        
        Test.startTest();
        BatchUpdatePositionVacancy usa = new BatchUpdatePositionVacancy(batchID);
        Id batchId1 = Database.executeBatch(usa);
        Test.stopTest();
        // after the testing stops, assert records were updated 
    }
}