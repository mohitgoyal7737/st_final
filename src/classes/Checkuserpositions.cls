global with sharing class Checkuserpositions
{
    public string query ;
    public string query2;
    public list<AxtriaSalesIQTM__User_Access_Permission__c>userposlist;
    public list<AxtriaSalesIQTM__CIM_Config__c>cimconfiglist ;
    public string teaminstancename ;
    public list<CallPlanCallCapacity__c>callcapacitylist ;
    public map<string, CallPlanCallCapacity__c>capacitymap ;
    public Checkuserpositions(string selectedteaminstance)
    {
        teaminstancename = '';
        query = '';
        query2 = '';
        teaminstancename = selectedteaminstance;
        userposlist = new list<AxtriaSalesIQTM__User_Access_Permission__c>();
        cimconfiglist = new list<AxtriaSalesIQTM__CIM_Config__c>();
        capacitymap = new map<string, CallPlanCallCapacity__c>();
        callcapacitylist = new list<CallPlanCallCapacity__c>();

        fetchdata();
    }
    global void fetchdata()
    {
        SnTDMLSecurityUtil.printDebugMessage('==========teaminstancename:::' + teaminstancename);
        query = 'select id,isCallPlanEnabled__c from AxtriaSalesIQTM__User_Access_Permission__c where AxtriaSalesIQTM__Team_Instance__r.Name =:teaminstancename WITH SECURITY_ENFORCED';
        SnTDMLSecurityUtil.printDebugMessage('=========Query:::::' + query);
        userposlist = database.query(query);
        if(userposlist != null && userposlist.size() > 0)
        {
            for(AxtriaSalesIQTM__User_Access_Permission__c uap : userposlist)
            {
                uap.isCallPlanEnabled__c = true;
            }
            SnTDMLSecurityUtil.printDebugMessage('============USERPOSITIONLIST::' + userposlist);
            //update userposlist;
            SnTDMLSecurityUtil.updateRecords(userposlist, 'Checkuserpositions');
        }

        capacitymap = CallPlanCallCapacity__c.getAll();
        set<string>teaminstancenamelist = new set<string>();
        if(capacitymap != null)
            teaminstancenamelist.addall(capacitymap.keyset());

        if(!teaminstancenamelist.contains(teaminstancename))
        {
            SnTDMLSecurityUtil.printDebugMessage('============CREATING CallPlanCallCapacity__c=============');
            CallPlanCallCapacity__c newcpc = new CallPlanCallCapacity__c();
            newcpc.name = teaminstancename;
            newcpc.Aggregate_Function__c = 'MAX';
            //insert newcpc;
            SnTDMLSecurityUtil.insertRecords(newcpc, 'Checkuserpositions');
            SnTDMLSecurityUtil.printDebugMessage('===========CallPlanCallCapacity__c=======' + newcpc);
        }
        /*string query2 = 'select id,AxtriaSalesIQTM__Acceptable_Max__c,AxtriaSalesIQTM__Acceptable_Min__c,AxtriaSalesIQTM__Attribute_API_Name__c,AxtriaSalesIQTM__Change_Request_Type__c,AxtriaSalesIQTM__CR_Type_Name__c,AxtriaSalesIQTM__Display_Name__c,AxtriaSalesIQTM__Team_Instance__r.name,Name';
        query2 += ' from AxtriaSalesIQTM__CIM_Config__c where AxtriaSalesIQTM__Team_Instance__r.name=:teaminstancename  and AxtriaSalesIQTM__CR_Type_Name__c = \'Call_Plan_Change\' WITH SECURITY_ENFORCED '; //
        SnTDMLSecurityUtil.printDebugMessage('===========query2:::' + query2);
        cimconfiglist = database.query(query2);
        if(cimconfiglist != null && cimconfiglist.size() > 0)
        {
            SnTDMLSecurityUtil.printDebugMessage('==================UPDATING CIM CONFIGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG++++++++++++++++');
            for(AxtriaSalesIQTM__CIM_Config__c cim : cimconfiglist)
            {
                SnTDMLSecurityUtil.printDebugMessage('=======NAME is:' + cim.name + '=====ATTRIBUTE API NAME:::' + cim.AxtriaSalesIQTM__Attribute_API_Name__c);
                if(cim.AxtriaSalesIQTM__CR_Type_Name__c == 'Call_Plan_Change')
                {
                    if(cim.name == 'WarningGardrialCallplan' && cim.AxtriaSalesIQTM__Attribute_API_Name__c == 'WarningGardrial')
                    {
                        SnTDMLSecurityUtil.printDebugMessage('===== inside loop1=====');
                        cim.AxtriaSalesIQTM__Acceptable_Max__c = '60';
                        cim.AxtriaSalesIQTM__Acceptable_Min__c = '70';
                    }
                    else if(cim.name == 'ErrorGardial2Callplan' && cim.AxtriaSalesIQTM__Attribute_API_Name__c == 'ErrorGardial')
                    {
                        SnTDMLSecurityUtil.printDebugMessage('===== inside loop2=====');
                        cim.AxtriaSalesIQTM__Acceptable_Max__c = '250';
                        cim.AxtriaSalesIQTM__Acceptable_Min__c = '200';
                    }
                    else if(cim.name == 'WarningGardrial2Callplan' && cim.AxtriaSalesIQTM__Attribute_API_Name__c == 'WarningGardrial')
                    {
                        SnTDMLSecurityUtil.printDebugMessage('===== inside loop3=====');
                        cim.AxtriaSalesIQTM__Acceptable_Max__c = '60';
                        cim.AxtriaSalesIQTM__Acceptable_Min__c = '70';
                    }
                    else if(cim.name == 'NoErrorGardrialCallplan' && cim.AxtriaSalesIQTM__Attribute_API_Name__c == 'NoErrorGardrial')
                    {
                        SnTDMLSecurityUtil.printDebugMessage('===== inside loop4=====');
                        cim.AxtriaSalesIQTM__Acceptable_Max__c = '200';
                        cim.AxtriaSalesIQTM__Acceptable_Min__c = '50';
                    }
                    else if(cim.name == 'ErrorGardialCallplan' && cim.AxtriaSalesIQTM__Attribute_API_Name__c == 'ErrorGardial')
                    {
                        SnTDMLSecurityUtil.printDebugMessage('===== inside loop5=====');
                        cim.AxtriaSalesIQTM__Acceptable_Max__c = '50';
                        cim.AxtriaSalesIQTM__Acceptable_Min__c = '0';
                    }
                }
            }
            SnTDMLSecurityUtil.printDebugMessage('=======cimconfiglist========' + cimconfiglist);
            //update cimconfiglist;
            SnTDMLSecurityUtil.updateRecords(cimconfiglist, 'Checkuserpositions');

            List<HierarchyWiseNonEditable__c> allSeg = [select id from HierarchyWiseNonEditable__c where Team_Instance__c = :teaminstancename WITH SECURITY_ENFORCED];
            SnTDMLSecurityUtil.printDebugMessage('allSeg--' + allSeg + '--teaminstancename--' + teaminstancename);
            Boolean hierarchyWiseCheckFlag = [Select Id, createHierarchyWiseCustomSetting__c From AxtriaSalesIQTM__Team_Instance__c where Name = : teaminstancename WITH SECURITY_ENFORCED].createHierarchyWiseCustomSetting__c;
            SnTDMLSecurityUtil.printDebugMessage('hierarchyWiseCheckFlag--' + hierarchyWiseCheckFlag);

            if(hierarchyWiseCheckFlag == false)
            {
                SnTDMLSecurityUtil.printDebugMessage('No custom setting created');
                //delete allSeg;
                SnTDMLSecurityUtil.deleteRecords(allSeg, 'Checkuserpositions');
                SnTDMLSecurityUtil.printDebugMessage('deleted custom setting for selected TI');
            }
            else
            {
                List<HierarchyWiseNonEditable__c> hier = new List<HierarchyWiseNonEditable__c>();

                HierarchyWiseNonEditable__c hi = new HierarchyWiseNonEditable__c();
                hi.Name = teaminstancename;
                hi.Hierarchy_Level__c = '1';
                hi.Non_Editable_API_Name__c = 'Segment__c';
                hi.Team_Instance__c = teaminstancename ;

                hier.add(hi);



                hi = new HierarchyWiseNonEditable__c();
                hi.Name = teaminstancename + '_Level2';
                hi.Hierarchy_Level__c = '2';
                hi.Non_Editable_API_Name__c = 'Segment__c';
                hi.Team_Instance__c = teaminstancename;

                hier.add(hi);
                //Database.insert(hier, false);
                SnTDMLSecurityUtil.insertRecords(hier, 'Checkuserpositions');
            }


        }*/
    }
}