global class Batch_Notifymissingusers implements Database.Batchable<sObject>,Database.stateful,Schedulable {
    public String query;
    public set<string>nonunserprids;
    public list<ErrorLogEmail__c> emailIds{get;set;}
   // public set<string>nonunserprids = new set<string>();

    global Batch_Notifymissingusers() {
        nonunserprids = new set<string>();
        query= 'select id ,AxtriaSalesIQTM__Employee_ID__c from AxtriaSalesIQTM__Employee__c';
        this.query = query;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Employee__c> scope) {
        set<string>empid = new set<string>();
        set<string>userprid = new set<string>();
        
        for( AxtriaSalesIQTM__Employee__c e : scope)
        {
            empid.add(e.AxtriaSalesIQTM__Employee_ID__c);
        }
        list<user>users = [select id ,FederationIdentifier  from user where FederationIdentifier IN : empid];
        if(users !=null && users.size() >0)
        {
            for(user u: users)
            {
                userprid.add(u.FederationIdentifier);
            }
        }
        else
        {
            nonunserprids.addall(empid);
        }

        //idendify missing PRIDS
        for(string emp : empid)
        {
            if(!userprid.contains(emp))
                nonunserprids.add(emp);

        }
        system.debug('============nonunserprids.size():::'+nonunserprids.size());
        
    }

    global void execute(SchedulableContext sc)
    {
        Database.executebatch(new Batch_Notifymissingusers(),2000);
    }

    global void finish(Database.BatchableContext BC) 
    {
        if(nonunserprids !=null && nonunserprids.size()>0)
        {

            list<AxtriaSalesIQTM__Employee__c>emplist = [select id,AxtriaSalesIQTM__Employee_ID__c,AxtriaSalesIQTM__Email__c,AxtriaSalesIQTM__Country__c from AxtriaSalesIQTM__Employee__c where AxtriaSalesIQTM__Employee_ID__c IN : nonunserprids];
            String datastring='ID,PRID,EMAIL,Country \n';
            for(AxtriaSalesIQTM__Employee__c em : emplist)
            {
                datastring+=em.id+','+em.AxtriaSalesIQTM__Employee_ID__c+','+em.AxtriaSalesIQTM__Email__c+em.AxtriaSalesIQTM__Country__c+'\n';
            }

            Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
            blob excel = blob.valueOf(datastring);
            system.debug('=========excel blob Attachment'+excel);

            attach.setBody(excel);
            attach.setFileName('Missing Users.xlsx');

            String subject = 'Missing Users in the system';
            String body = 'These employees are present in Employee but users are not created';
            //String[] address = new String[]{'jeedi.gopi@axtria.com'};
            //String []ccAdd=new String[]{'AZ_ROW_SalesIQ_MktDeployment@Axtria.com'};
            list<string>emaillist = new list<string>();
            emailIds = ErrorLogEmail__c.getall().values();
            if(emailIds!=null){
                for(ErrorLogEmail__c LEM:emailIds){
                    emaillist.add(LEM.Name);
                    system.debug('emaillist::::'+emaillist);
                }
            }

            Messaging.SingleEmailMessage emailwithattch = new Messaging.SingleEmailMessage();
            emailwithattch.setSubject(subject);
            emailwithattch.setToaddresses(emaillist);
            emailwithattch.setPlainTextBody(body);
            //emailwithattch.setCcAddresses(ccAdd);

            emailwithattch.setFileAttachments(new Messaging.EmailFileAttachment[]{attach});

            // Sends the email
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {emailwithattch});



        }
        

    }
}