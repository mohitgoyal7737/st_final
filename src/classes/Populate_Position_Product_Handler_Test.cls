@isTest
    public class Populate_Position_Product_Handler_Test {
        
        static testMethod void testMethod1() {
            User loggedInUser = new User(id=UserInfo.getUserId());
            
            AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
            insert orgmas;
            AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
            insert countr;
            
            AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
            workspace.AxtriaSalesIQTM__Country__c = countr.id;
            insert workspace;
            AxtriaSalesIQTM__Geography_Type__c type = new AxtriaSalesIQTM__Geography_Type__c();
            type.AxtriaSalesIQTM__Country__c = countr.Id;
            insert type;
            AxtriaSalesIQTM__Geography__c zt0 = new AxtriaSalesIQTM__Geography__c();
            zt0.name = 'Y';
            zt0.AxtriaSalesIQTM__Parent_Zip__c = '07059';
            zt0.AxtriaSalesIQTM__Geography_Type__c = type.Id;
            insert zt0;
            
            AxtriaSalesIQTM__Geography__c geo = new AxtriaSalesIQTM__Geography__c();
            geo.name = 'X';
            geo.AxtriaSalesIQTM__Parent_Zip__c = '07060';
            geo.AxtriaSalesIQTM__Parent_Zip_Code__c=zt0.id;
            geo.AxtriaSalesIQTM__Geography_Type__c = type.Id;
            insert geo;
            AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
            insert team;
            AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
            insert teamins;
            Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
            pcc.Product_Type__c = 'Product';
            insert pcc;
            
            Team_Instance_Product_AZ__c azteam = TestDataFactory.teamInstanceProductAZ(pcc,teamins);
            azteam.Full_Load__c = true;
            azteam.Product_Catalogue__c = pcc.id;
            azteam.Team_Instance__c = teamins.id;
            insert azteam;
            List<Team_Instance_Product_AZ__c> allPosProducts= new List<Team_Instance_Product_AZ__c>();
            allPosProducts.add(azteam);
            List<Product_Catalog__c> allProducts= new List<Product_Catalog__c>();
            allProducts.add(pcc);
            AxtriaSalesIQTM__TriggerContol__c ccd= new AxtriaSalesIQTM__TriggerContol__c();
            ccd.Name = 'FillExternalIDonPosProduct';
            ccd.AxtriaSalesIQTM__IsStopTrigger__c = false;
            insert ccd;
            
            AxtriaSalesIQTM__TriggerContol__c ccd1= new AxtriaSalesIQTM__TriggerContol__c();
            ccd1.Name = 'TeamInstanceProductTrigger';
            ccd1.AxtriaSalesIQTM__IsStopTrigger__c = True;
        //ccd.Value__c = 'FillExternalIDonPosProduct';
            insert ccd1;
            AxtriaSalesIQTM__Position__c pos = TestDataFactory.createPosition(team,teamins);
            pos.AxtriaSalesIQTM__Team_iD__c = team.Id;
            insert pos;
/*
        AxtriaSalesIQTM__Product__c prod = TestDataFactory.createProduct(team,teamins);
        prod.Team__c = team.id;
        prod.Team_Instance__c = teamins.id; 
        insert prod;
        AxtriaSalesIQTM__Position_Product__c posprod = TestDataFactory.createPositionProduct(teamins,pos,pcc);
        posprod.AxtriaSalesIQTM__Product_Master__c = prod.id;
        insert posprod;*/

        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            Populate_Position_Product_Handler obj=new Populate_Position_Product_Handler();
            Populate_Position_Product_Handler.populatePosition(allPosProducts);
            Populate_Position_Product_Handler.deletePosition(allPosProducts);
            Populate_Position_Product_Handler.updatePosition(allPosProducts);
        }
        Test.stopTest();
    }
    static testMethod void testMethod2() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        AxtriaSalesIQTM__Geography_Type__c type = new AxtriaSalesIQTM__Geography_Type__c();
        type.AxtriaSalesIQTM__Country__c = countr.Id;
        insert type;
        AxtriaSalesIQTM__Geography__c zt0 = new AxtriaSalesIQTM__Geography__c();
        zt0.name = 'Y';
        zt0.AxtriaSalesIQTM__Parent_Zip__c = '07059';
        zt0.AxtriaSalesIQTM__Geography_Type__c = type.Id;
        insert zt0;
        
        AxtriaSalesIQTM__Geography__c geo = new AxtriaSalesIQTM__Geography__c();
        geo.name = 'X';
        geo.AxtriaSalesIQTM__Parent_Zip__c = '07060';
        geo.AxtriaSalesIQTM__Parent_Zip_Code__c=zt0.id;
        geo.AxtriaSalesIQTM__Geography_Type__c = type.Id;
        insert geo;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        pcc.Product_Type__c = 'Product';
        insert pcc;
        
        Team_Instance_Product_AZ__c azteam = TestDataFactory.teamInstanceProductAZ(pcc,teamins);
        azteam.Full_Load__c = true;
        insert azteam;
        List<Team_Instance_Product_AZ__c> allPosProducts= new List<Team_Instance_Product_AZ__c>();
        allPosProducts.add(azteam);
        List<Product_Catalog__c> allProducts= new List<Product_Catalog__c>();
        allProducts.add(pcc);
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            Populate_Position_Product_Handler_PC obj1=new Populate_Position_Product_Handler_PC();
            Populate_Position_Product_Handler_PC.populatePosition(allProducts);
            Populate_Position_Product_Handler_PC.deletePosition(allProducts);  
        }
        Test.stopTest();
    }
    
    /*static testMethod void testMethod1update() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        AxtriaSalesIQTM__Geography_Type__c type = new AxtriaSalesIQTM__Geography_Type__c();
        type.AxtriaSalesIQTM__Country__c = countr.Id;
        insert type;
        AxtriaSalesIQTM__Geography__c zt0 = new AxtriaSalesIQTM__Geography__c();
        zt0.name = 'Y';
        zt0.AxtriaSalesIQTM__Parent_Zip__c = '07059';
        zt0.AxtriaSalesIQTM__Geography_Type__c = type.Id;
        insert zt0;
        
        AxtriaSalesIQTM__Geography__c geo = new AxtriaSalesIQTM__Geography__c();
        geo.name = 'X';
        geo.AxtriaSalesIQTM__Parent_Zip__c = '07060';
        geo.AxtriaSalesIQTM__Parent_Zip_Code__c=zt0.id;
        geo.AxtriaSalesIQTM__Geography_Type__c = type.Id;
        insert geo;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        pcc.Product_Type__c = 'Product';
        insert pcc;
        Product_Catalog__c pcc1 = TestDataFactory.productCatalog(team, teamins, countr);
        pcc1.Product_Type__c = 'Product';
        insert pcc1;
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        
        AxtriaSalesIQTM__Product__c product = new AxtriaSalesIQTM__Product__c();
        product.Team_Instance__c = teamins.id;
        product.CurrencyIsoCode = 'USD'; 
        product.Name= 'Test';
        product.AxtriaSalesIQTM__Product_Code__c = 'testproduct'; 
        
        product.AxtriaSalesIQTM__Effective_End_Date__c = date.today();
        product.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        insert product;
        
        
        AxtriaSalesIQTM__Position_Product__c positionproduct = new AxtriaSalesIQTM__Position_Product__c();
        positionproduct.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        positionproduct.AxtriaSalesIQTM__Product_Master__c = product.id;
        //positionproduct.AxtriaSalesIQTM__Product__c = product.id;
        positionproduct.AxtriaSalesIQTM__Position__c = pos.id;
        positionproduct.Product_Catalog__c = pcc.id;
        positionproduct.CurrencyIsoCode = 'USD'; 
        positionproduct.AxtriaSalesIQTM__Product_Weight__c = 1;
        positionproduct.AxtriaSalesIQTM__isActive__c = true;
        positionproduct.Business_Days_in_Cycle__c = 1;
        positionproduct.Calls_Day__c = 1;
        positionproduct.Holidays__c = 1;
        positionproduct.Other_Days_Off__c = 1;
        positionproduct.Vacation_Days__c = 1;
        positionproduct.AxtriaSalesIQTM__Product_Weight__c = 1;
        positionproduct.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        positionproduct.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        
        insert positionproduct;
        Team_Instance_Product_AZ__c azteam = TestDataFactory.teamInstanceProductAZ(pcc,teamins);
        azteam.Full_Load__c = true;
        azteam.Team_Instance__c = teamins.id;
        azteam.Product_Catalogue__c = pcc.id;
        insert azteam;
        List<Team_Instance_Product_AZ__c> allPosProducts= new List<Team_Instance_Product_AZ__c>();
        allPosProducts.add(azteam);
        List<Product_Catalog__c> allProducts= new List<Product_Catalog__c>();
        allProducts.add(pcc);
        Test.startTest();
        System.runAs(loggedInUser){
ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            
            
            Populate_Position_Product_Handler obj=new Populate_Position_Product_Handler();
            //Populate_Position_Product_Handler.populatePosition(allPosProducts);
            //Populate_Position_Product_Handler.updatePosition(allPosProducts);
        }
        Test.stopTest();
    }*/
    
}