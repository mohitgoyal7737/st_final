@isTest

public class batchUpdatePosGeoAZTest {
    
    static testmethod void testbatch (){
        Map<String, Set<String>> mapPosGeo = new Map<String, Set<String>>();
        mapPosGeo.put('abcd',new Set<String>{'abcd1','abcd2','abcd3'});
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
        team.Name='HTN';
        insert team;
        
        List<AxtriaSalesIQTM__Position__c> poss = new List<AxtriaSalesIQTM__Position__c>();
        for(Integer i=0;i<10;i++){
        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();    
            pos.name = 'abc'+i;
        pos.AxtriaSalesIQTM__Team_iD__c = team.id;
       // pos.CurrencyIsoCode = 'USD';
            poss.add(pos);
          }
        
        insert poss;
        
        
        AxtriaSalesIQTM__Team_Instance__c objTeamInstance = new AxtriaSalesIQTM__Team_Instance__c();
        objTeamInstance.Name = 'abcd';
       // objTeamInstance.CurrencyIsoCode = 'USD';
        objTeamInstance.AxtriaSalesIQTM__Team__c = team.id;
        insert objTeamInstance;
        
        
        
        
        AxtriaSalesIQTM__Geography__c   geo = new AxtriaSalesIQTM__Geography__c();
        geo.Name = 'abcd';
       // geo.CurrencyIsoCode = 'USD';
        insert geo;
        
        
        
        list<AxtriaSalesIQTM__Position_Geography__c> posgeos = new list<AxtriaSalesIQTM__Position_Geography__c>();
        for(Integer i=0; i<=10;i++){
            AxtriaSalesIQTM__Position_Geography__c posgeo = new AxtriaSalesIQTM__Position_Geography__c();
                 posgeo.Name = 'def';
        posgeo.AxtriaSalesIQTM__Effective_Start_Date__c =date.today()-10;
        posgeo.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+100;
        posgeo.AxtriaSalesIQTM__Team_Instance__c = objTeamInstance.id;
        posgeo.AxtriaSalesIQTM__Geography__c = geo.id;
        //posgeo.CurrencyIsoCode ='EUR';
            //posgeo.AxtriaSalesIQTM__IsShared__c =true;
            posgeos.add(posgeo);
        }
   insert posgeos;
       
        
        Test.startTest();
        Id teamintsanceId=objTeamInstance.id;
        string temp=json.serialize(mapPosGeo);
        batchUpdatePosGeoAZ bpos = new batchUpdatePosGeoAZ(temp,teamintsanceId );
        database.executeBatch(bpos, 100);
        Test.StopTest();
        
    }
}