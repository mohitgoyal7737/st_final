global with sharing class BatchActiveUSerGroupEvent implements Database.Batchable<sObject> {
    public String query;
    public Set<String> countrySet;

    public Set<String> uniqueRec;
    global BatchActiveUSerGroupEvent(Set<String> country) 
    {
        query = '';
        countrySet=new Set<String>();
        countrySet.addAll(country);
        SnTDMLSecurityUtil.printDebugMessage('countrySet:::::::::' +countrySet);
        query='select id,AxtriaSalesIQTM__Employee__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.Sales_Team_Attribute_MS__c,AxtriaSalesIQTM__Position__r.Employee1__c,AxtriaSalesIQTM__Assignment_Status__c from AxtriaSalesIQTM__Position_Employee__c where AxtriaSalesIQTM__Employee__c!=null and AxtriaSalesIQTM__Position__c!=null and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__r.AxtriaSalesIQTM__Country__c in :countrySet  and AxtriaSalesIQTM__Assignment_Status__c=\'Active\' and AxtriaSalesIQTM__Position__r.Sales_Team_Attribute_MS__c != null and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__inactive__c=false and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__IsMaster__c=true and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__r.AxtriaSalesIQTM__Country__c in :countrySet WITH SECURITY_ENFORCED';     //Added Master checks   
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Employee__c> scope) 
    {
        /*try
        {*/
            uniqueRec = new Set<String>();
            SnTDMLSecurityUtil.printDebugMessage('======Query::::::' +scope);
            SnTDMLSecurityUtil.printDebugMessage('====List of countries:::::' +countrySet);
            List<GroupAZ__c> insertGroup = new List<GroupAZ__c>();
            Set<String> setGPName = new Set<String>();
            Set<String> setExistGroup = new Set<String>();
            Set<String> gpInsert = new Set<String>();
            Map<String,String> mapGrpName2Id= new Map<String,String>();

            for(AxtriaSalesIQTM__Position_Employee__c  peRec: scope)
            {
                for(String str : peRec.AxtriaSalesIQTM__Position__r.Sales_Team_Attribute_MS__c.split(';'))
                {
                    setGPName.add(str);
                }
            }

            SnTDMLSecurityUtil.printDebugMessage('======setGPName::::::' +setGPName);

            List <GroupAZ__c> newGPList = [select Name,Id from GroupAZ__c where Group_type__c = 'Sales Team Attribute' and Name in :setGPName WITH SECURITY_ENFORCED];
            SnTDMLSecurityUtil.printDebugMessage('======newGPList::::::' +newGPList);

            if(newGPList !=null && newGPList.size() > 0)
            {
                for(GroupAZ__c gp : newGPList)
                {
                    setExistGroup.add(gp.Name);
                }
                for(AxtriaSalesIQTM__Position_Employee__c  peRec: scope)
                {
                    for(String str : peRec.AxtriaSalesIQTM__Position__r.Sales_Team_Attribute_MS__c.split(';'))
                    {
                        if(!setExistGroup.contains(str))
                        {
                            if(!gpInsert.contains(str))
                            {
                               GroupAZ__c newGrp = new GroupAZ__c();
                               newGrp.Name = str;
                               newGrp.Group_type__c='Sales Team Attribute';
                               insertGroup.add(newGrp);
                               gpInsert.add(str);
                           }
                        }
                    }
                }             
            }
            else
            {
                   for(String gpname : setGPName)
                   {
                        if(!gpInsert.contains(gpname))
                        {
                           GroupAZ__c newGrp = new GroupAZ__c();
                           newGrp.Name = gpname;
                           newGrp.Group_type__c='Sales Team Attribute';
                           insertGroup.add(newGrp);
                           gpInsert.add(gpname);
                       }
                   }
            }
            SnTDMLSecurityUtil.printDebugMessage('======insertGroup::::::' +insertGroup);
            SnTDMLSecurityUtil.printDebugMessage('======insertGroup.size()::::::' +insertGroup.size());

            if(insertGroup.size() > 0){
                //insert insertGroup;
                SnTDMLSecurityUtil.insertRecords(insertGroup, 'BatchActiveUSerGroupEvent');
            }

            List<GroupAZ__c> gpList=[select Name,Id from GroupAZ__c where Group_type__c = 'Sales Team Attribute' and Name in :setGPName];
            
            for(GroupAZ__c gpRec : gpList)
            {
              mapGrpName2Id.put(gpRec.Name,gpRec.Id);
            } 
            SnTDMLSecurityUtil.printDebugMessage('======mapGrpName2Id::::::' +mapGrpName2Id);
            

            List<UserGroups__c> usergrpsUpsert=new List<UserGroups__c>();
            for(AxtriaSalesIQTM__Position_Employee__c  peRec: scope)
            {
                for(String str : peRec.AxtriaSalesIQTM__Position__r.Sales_Team_Attribute_MS__c.split(';'))
                {
                    String key=str+peRec.AxtriaSalesIQTM__Employee__c;
                    SnTDMLSecurityUtil.printDebugMessage('Upsert UserGroup');
                    UserGroups__c newUsr = new UserGroups__c();
                    newUsr.Employee__c = peRec.AxtriaSalesIQTM__Employee__c;
                    newUsr.Group_Type__c = 'Sales Team Attribute';
                    newUsr.Event__c      = 'Add';
                    newUsr.Name = str;
                    newUsr.Position__c = peRec.AxtriaSalesIQTM__Position__c;
                    newUsr.ExternalUserID__c=key;
                    newUsr.Group__c = mapGrpName2Id.get(str);
                    newUsr.Start_date__c = system.today();

                    if(!uniqueRec.contains(newUsr.ExternalUserID__c))
                    {
                        usergrpsUpsert.add(newUsr);
                        uniqueRec.add(newUsr.ExternalUserID__c);
                    }
                }
                
            }
            SnTDMLSecurityUtil.printDebugMessage('======usergrpsUpsert::::::' +usergrpsUpsert);
            SnTDMLSecurityUtil.printDebugMessage('======usergrpsUpsert.size()::::::' +usergrpsUpsert.size());

            if(usergrpsUpsert.size() > 0)
                upsert usergrpsUpsert ExternalUserID__c;
        /*} catch(Exception e)
           {
               
                SnTDMLSecurityUtil.printDebugMessage('++inside catch');
                String Header='User Group Batch is failed, so delta has been haulted of this org for Today';
                tryCatchEmailAlert.veevaLoad(Header);
           }*/
    }


    global void finish(Database.BatchableContext BC) 
    {

    }
}