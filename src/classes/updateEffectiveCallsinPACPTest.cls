/**********************************************************************************************
@author       : SnT Team
@modifiedBy   : Rashmi Tyagi 
@modifiedDate : 01 Feb'2021
@description  : Test class for covering updateEffectiveCallsinPACP class scenarios,modified to increase the coverage and put assert statements
@Revison(s)   : v1.1 
**********************************************************************************************/
@isTest
public class updateEffectiveCallsinPACPTest {
    static  testmethod void calltest1(){
        
        String classname = 'updateEffectiveCallsinPACPTest';
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Name ='USA';
        countr.MCCP_Enabled__c = true;
        SnTDMLSecurityUtil.insertRecords(countr,className);
        System.assertEquals('USA',countr.Name);
        
        Account a1= TestDataFactory.createAccount();
            a1.AxtriaSalesIQTM__Country__c = countr.id;
            a1.AccountNumber = 'BH10477999';
            a1.Type = 'HCO';
            a1.Status__c ='Inactive';
           SnTDMLSecurityUtil.insertRecords(a1,className);
           List<Account> acclist = new List<Account>();
           acclist.add(a1);
           
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Country__c = countr.id;
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
        SnTDMLSecurityUtil.insertRecords(team,className);
        List<AxtriaSalesIQTM__Team__c> teamList = [Select id,AxtriaSalesIQTM__Country__c,AxtriaSalesIQTM__Country__r.name from AxtriaSalesIQTM__Team__c where id =:team.id];
        System.debug('team>>'+team);
        System.debug('team country>>'+team.AxtriaSalesIQTM__Country__c);
        System.debug('team country name>>'+teamList[0].AxtriaSalesIQTM__Country__r.name);
        System.assert(teamList.size()==1);
        
        //System.assertEquals('USA',team.AxtriaSalesIQTM__Country__r.name);
        
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins1,className);
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        SnTDMLSecurityUtil.insertRecords(scen,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Name = 'test';
        teamins.AxtriaSalesIQTM__Team__c = team.id;
        teamins.Metric_Mapping__c = 'Final_TCF_Approved__c,AxtriaSalesIQTM__Metric2_Approved__c,AxtriaSalesIQTM__Metric1_Approved__c';
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        teamins.Multichannel__c = true;
        teamins.MCCP_Enabled__c = true;
        teamins.Channel_Weights__c ='1,1,1';
        //teamins.AxtriaSalesIQTM__Team_Cycle_Name__c = 'Current';
        //teamins.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        SnTDMLSecurityUtil.insertRecords(teamins,className);
        List<AxtriaSalesIQTM__Team_Instance__c> teamInsList = [Select id,Country_Name__c from AxtriaSalesIQTM__Team_Instance__c where id =:teamins.id];
        System.debug('teamins>>'+teamInsList[0].id);
        System.debug('teamins country name>>'+teamInsList[0].Country_Name__c);
        System.assertEquals('USA',teamInsList[0].Country_Name__c);
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        SnTDMLSecurityUtil.insertRecords(pcc,className);
        
        Veeva_Market_Specific__c vms = new Veeva_Market_Specific__c();
        vms.Name = teamInsList[0].Country_Name__c;
        vms.Metric_Name__c = 'AxtriaSalesIQTM__Metric1_Approved__c,AxtriaSalesIQTM__Metric2_Approved__c,AxtriaSalesIQTM__Metric3_Approved__c';
        vms.Channel_Name__c = 'F2F,Email,Meeting';
        vms.Weight__c = '1,1,1';
        vms.MCCP_Target_logic__c = 'MAX';
        SnTDMLSecurityUtil.insertRecords(vms,className);
        System.debug('vms.Name >>'+vms.Name);
        //System.assertEquals('USA',vms.Name);
        
        
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
       SnTDMLSecurityUtil.insertRecords(mmc,className);
          AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        SnTDMLSecurityUtil.insertRecords(pos,className);
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(a1,pos,teamins);
        SnTDMLSecurityUtil.insertRecords(posAccount,className);
          Product_Priority__c pPriority = TestDataFactory.productPriority();
       SnTDMLSecurityUtil.insertRecords(pPriority,className);
        AxtriaSalesIQTM__Position_Product__c posprod = TestDataFactory.createPositionProduct(teamins, pos, pcc);
        posprod.Calls_Day__c = 1;
        // posprod.Effective_Days_in_Field_Formula__c = 1;
         SnTDMLSecurityUtil.insertRecords(posprod,className);
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,a1,teamins,posAccount,pPriority,pos);
        positionAccountCallPlan.Final_TCF__c = 10;
        positionAccountCallPlan.P1__c = pcc.name;
        positionAccountCallPlan.p1_original__c = pcc.name;
        positionAccountCallPlan.Final_TCF_Original__c = 10;
        positionAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c = true;
         SnTDMLSecurityUtil.insertRecords(positionAccountCallPlan,className);
         List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacplist = new List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
         pacplist.add(positionAccountCallPlan);
         Test.startTest();
         //Veeva_Channels__mdt mccpchannels = [Select id,Channels__c,Metric_Mapping__c,Team_Instance_Name__c from Veeva_Channels__mdt where Team_Instance_Name__c =:teamins.name];
         updateEffectiveCallsinPACP obj = new updateEffectiveCallsinPACP();
         obj.updateEffectiveCallsinPACP(pacplist);
         Test.stopTest();
         
        
    }

}