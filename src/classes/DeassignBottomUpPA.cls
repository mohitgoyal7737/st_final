global with sharing class DeassignBottomUpPA implements Database.Batchable<sObject> {
    public String query;
    public Boolean flag{get;set;}
    public Set<String> deassignID;
    public Set<String> deassignHCAPosSet; 
    public Set<String> deassignHCPPosSet; 
    public Set<String> deassignHCATeamInsSet;
    public Set<String> deassignHCAKey;
    public Set<String> deassignHCPTeamInsSet;
    public Set<String> deassignHCPKey;
    public Set<String> inputHCAset;
    public Set<String> inputHCPset;
    public String countryName;

    global DeassignBottomUpPA(Set<String> setDeassignID,Boolean flagg, String country) {
        query = '';
        deassignID=new Set<String>();
        flag=flagg;
        countryName=country;
        deassignID.addAll(setDeassignID);
        SnTDMLSecurityUtil.printDebugMessage('deassignID:::' +deassignID);
        SnTDMLSecurityUtil.printDebugMessage('countryName:::' +countryName);
        //query='select Id,Account__c,Position__c,Team_Instance__c,Status__c,Account_Type__c,Rule_Type__c from Deassign_Postiton_Account__c where Status__c=\'New\' and Id in :deassignID and  = :countryName';
        // chenge to temp_Obj__c by Mayank Pathak on 19/03/2020
        query='select Id,Account_Text__c,Position_Text__c,Team_Instance_Text__c,Status__c,Account_Type__c,Rule_Type__c from temp_Obj__c where Status__c=\'New\' and Id in :deassignID and Country__c = :countryName  and Object__c = \'Deassign_Postiton_Account__c\' ';
        SnTDMLSecurityUtil.printDebugMessage('query:::' +query);
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<temp_Obj__c> scope) {

        deassignHCAPosSet=new Set<String>();
        //deassignAccSet=new Set<String>();
        deassignHCATeamInsSet=new Set<String>();
        deassignHCAKey=new Set<String>();
        deassignHCPKey=new Set<String>();
        inputHCAset=new Set<String>();
        inputHCPset=new Set<String>();
        deassignHCPPosSet=new Set<String>();
        deassignHCPTeamInsSet=new Set<String>();
        Map<String,Set<String>> mapinputHCA2key=new Map<String,Set<String>>();
        Map<String,Set<String>> mapinputHCP2key=new Map<String,Set<String>>();
        List<AxtriaSalesIQTM__Position_Account__c> deassignPosAccList = new List<AxtriaSalesIQTM__Position_Account__c>();
        List<temp_Obj__c> deassignList = new List<temp_Obj__c>();

        List<String> hcoList = new List<String>{'HCA','HCO','CMOP'};
        List<String> hcpList = new List<String>{'HCP','Physician'};
        
        SnTDMLSecurityUtil.printDebugMessage('=====Query::::::::::' +scope);
        for(temp_Obj__c deassignRec : scope)
        {
            //if(deassignRec.Rule_Type__c == 'Bottom Up')
            //{
                SnTDMLSecurityUtil.printDebugMessage('Rule Type is Bottom Up');
                if(hcoList.contains(String.valueof(deassignRec.get('Account_Type__c'))))
                {
                    SnTDMLSecurityUtil.printDebugMessage('Account is HCA and Rule is Bottom Up');
                    inputHCAset.add(deassignRec.Account_Text__c);
                    deassignHCAPosSet.add(deassignRec.Position_Text__c);
                    //deassignAccSet.add(deassignRec.Account_Text__c);
                    deassignHCATeamInsSet.add(deassignRec.Team_Instance_Text__c);
                    deassignHCAKey.add(deassignRec.Account_Text__c + '_' + deassignRec.Position_Text__c + '_' + deassignRec.Team_Instance_Text__c);
                    deassignRec.Status__c='Please provide the respective HCPs';
                }
                else if(hcpList.contains(String.valueof(deassignRec.get('Account_Type__c'))))
                {
                    SnTDMLSecurityUtil.printDebugMessage('Account is HCP and Rule is Bottom Up'); 
                    inputHCPset.add(deassignRec.Account_Text__c);
                    deassignHCPPosSet.add(deassignRec.Position_Text__c);
                    deassignHCPTeamInsSet.add(deassignRec.Team_Instance_Text__c);
                    deassignHCPKey.add(deassignRec.Account_Text__c + '_' + deassignRec.Position_Text__c + '_' + deassignRec.Team_Instance_Text__c);
                    deassignRec.Status__c='Processed';
                }
                deassignList.add(deassignRec);
            //}
        }
        SnTDMLSecurityUtil.printDebugMessage('======inputHCAset::::' +inputHCAset);
        SnTDMLSecurityUtil.printDebugMessage('======inputHCPset::::' +inputHCPset);

        SnTDMLSecurityUtil.printDebugMessage('=========Handling Bottom Up------ Input is HCP::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::');

        SnTDMLSecurityUtil.printDebugMessage('===========Querying Position Account for Deassign Account-Position Set For HCP================================================');
        if(inputHCPset != null)
        {
            List<AxtriaSalesIQTM__Position_Account__c> deassignHCPPosAccList = [select Id,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.AccountNumber,IsGasAssignment__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__c,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c from AxtriaSalesIQTM__Position_Account__c where (AxtriaSalesIQTM__Assignment_Status__c = 'Active' or AxtriaSalesIQTM__Assignment_Status__c = 'Future Active') and AxtriaSalesIQTM__Position__c!=null and AxtriaSalesIQTM__Account__c!=null and AxtriaSalesIQTM__Team_Instance__c!=null and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c in :deassignHCPPosSet and AxtriaSalesIQTM__Account__r.AccountNumber in :inputHCPset and AxtriaSalesIQTM__Team_Instance__r.Name in :deassignHCPTeamInsSet];
          
            SnTDMLSecurityUtil.printDebugMessage('=====deassign HCP Position Account=====' +deassignHCPPosAccList.size());
            if(deassignHCPPosAccList != null)
            {
                for(AxtriaSalesIQTM__Position_Account__c deassignPA : deassignHCPPosAccList)
                {
                    String key = deassignPA.AxtriaSalesIQTM__Account__r.AccountNumber + '_' +deassignPA.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' + deassignPA.AxtriaSalesIQTM__Team_Instance__r.Name;
                    if(deassignHCPKey.contains(key))
                    {
                        if(!mapinputHCP2key.containsKey(deassignPA.AxtriaSalesIQTM__Account__r.AccountNumber))
                        {
                            String tempKey = deassignPA.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' +deassignPA.AxtriaSalesIQTM__Team_Instance__r.Name;
                            Set<String> tempKeySet= new Set<String>();
                            tempKeySet.add(tempKey);
                            mapinputHCP2key.put(deassignPA.AxtriaSalesIQTM__Account__r.AccountNumber,tempKeySet);
                        }
                        else
                        {
                            String tempKey = deassignPA.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' +deassignPA.AxtriaSalesIQTM__Team_Instance__r.Name;
                            mapinputHCP2key.get(deassignPA.AxtriaSalesIQTM__Account__r.AccountNumber).add(tempKey);
                        }
                        
                        deassignPA.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addDays(-1);

                        if(flag == true)
                        {
                            deassignPosAccList.add(deassignPA);
                        }
                        else if(flag== false)
                        {
                            if(deassignPA.IsGasAssignment__c == false)
                            {
                                deassignPosAccList.add(deassignPA);
                            }
                        }


                    }
                }
              }
              SnTDMLSecurityUtil.printDebugMessage('=====mapinputHCPkey::::::' +mapinputHCP2key);

              SnTDMLSecurityUtil.printDebugMessage('==========Affiliation Handling for HCPs========================================================================================');
              List<AxtriaSalesIQTM__Account_Affiliation__c> inputHCP2affHCAList = [select Id, AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Account__r.Status__c,Account_Number__c,Parent_Account_Number__c,AxtriaSalesIQTM__Affiliation_Network__c,Country__c,IsDeleted,AxtriaSalesIQTM__Parent_Account__c,AxtriaSalesIQTM__Parent_Account__r.AccountNumber,AxtriaSalesIQTM__Root_Account__c from   AxtriaSalesIQTM__Account_Affiliation__c where (Affiliation_Status__c='Active' or Affiliation_Status__c='Future Active') and AxtriaSalesIQTM__Account__r.AccountNumber in :inputHCPset and AxtriaSalesIQTM__Active__c=true];

             SnTDMLSecurityUtil.printDebugMessage('=====deassign HCP and HCA Affiliation=====' +inputHCP2affHCAList);

             Map<String,Set<String>> mapinputHCP2affHCAset=new Map<String,Set<String>>();

             if(inputHCP2affHCAList != null)
             {
                for(AxtriaSalesIQTM__Account_Affiliation__c inputHCP2affHCARec : inputHCP2affHCAList)
                {
                    if(mapinputHCP2affHCAset.containsKey(inputHCP2affHCARec.AxtriaSalesIQTM__Account__r.AccountNumber))
                    {
                        mapinputHCP2affHCAset.get(inputHCP2affHCARec.AxtriaSalesIQTM__Account__r.AccountNumber).add(inputHCP2affHCARec.AxtriaSalesIQTM__Parent_Account__r.AccountNumber);
                    }
                    else
                    {
                        Set<String> tempKey = new Set<String>();
                        tempkey.add(inputHCP2affHCARec.AxtriaSalesIQTM__Parent_Account__r.AccountNumber);
                        mapinputHCP2affHCAset.put(inputHCP2affHCARec.AxtriaSalesIQTM__Account__r.AccountNumber,tempkey);
                    }
                }
             }
             SnTDMLSecurityUtil.printDebugMessage('=====mapinputHCP2affHCAset:::::::::' +mapinputHCP2affHCAset);

             Set<String> affHCAset = new Set<String>();

             if(mapinputHCP2affHCAset != null)
             {
                 for(String hcp : mapinputHCP2affHCAset.keySet())
                 {
                    if(mapinputHCP2affHCAset.get(hcp) != null)
                    {
                        for(String hca : mapinputHCP2affHCAset.get(hcp))
                        {
                            affHCAset.add(hca);
                        }
                    }
                 }
             }
             SnTDMLSecurityUtil.printDebugMessage('=====affHCAset=====' +affHCAset);

             SnTDMLSecurityUtil.printDebugMessage('===========Querying Position Account for affiliated HCA (HCP-->HCA) First level===========');
             List<AxtriaSalesIQTM__Position_Account__c> affHCAPosAccList = [select Id,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__c,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c from AxtriaSalesIQTM__Position_Account__c where (AxtriaSalesIQTM__Assignment_Status__c = 'Active' or AxtriaSalesIQTM__Assignment_Status__c = 'Future Active') and AxtriaSalesIQTM__Position__c!=null and AxtriaSalesIQTM__Account__c!=null and AxtriaSalesIQTM__Team_Instance__c!=null and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c in :deassignHCPPosSet and AxtriaSalesIQTM__Account__r.AccountNumber in :affHCAset and AxtriaSalesIQTM__Team_Instance__r.Name in :deassignHCPTeamInsSet];

             SnTDMLSecurityUtil.printDebugMessage('=====HCA Position Account=====' +affHCAPosAccList.size());
             Map<String,Set<String>> mapaffHCA2key=new Map<String,Set<String>>();

             if(affHCAPosAccList != null)
             {
                for(AxtriaSalesIQTM__Position_Account__c affHCARec : affHCAPosAccList)
                {
                    if(!mapaffHCA2key.containsKey(affHCARec.AxtriaSalesIQTM__Account__r.AccountNumber))
                    {
                        String tempKey = affHCARec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' +affHCARec.AxtriaSalesIQTM__Team_Instance__r.Name;
                        Set<String> tempKeySet= new Set<String>();
                        tempKeySet.add(tempKey);
                        mapaffHCA2key.put(affHCARec.AxtriaSalesIQTM__Account__r.AccountNumber,tempKeySet);
                    }
                    else
                    {
                        String tempKey = affHCARec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' +affHCARec.AxtriaSalesIQTM__Team_Instance__r.Name;
                        mapaffHCA2key.get(affHCARec.AxtriaSalesIQTM__Account__r.AccountNumber).add(tempKey);
                    }
                }
             }
             SnTDMLSecurityUtil.printDebugMessage('mapaffHCA2key::::::'+mapaffHCA2key);

             SnTDMLSecurityUtil.printDebugMessage('===========Check for common Positions in HCP ----> HCA=======================================================================');
             Map<String,Set<String>> mapfurthercheckHCA=new Map<String,Set<String>>();

             if(mapinputHCP2key != null)
             {
                for(String hcp : mapinputHCP2key.keySet())
                {
                    SnTDMLSecurityUtil.printDebugMessage('hcp::::::'+hcp);
                    Set<String> hcpPosTIKey = mapinputHCP2key.get(hcp);
                    SnTDMLSecurityUtil.printDebugMessage('hcpPosTIKey::::::'+hcpPosTIKey);
                    Set<String> hcaSet = mapinputHCP2affHCAset.get(hcp);
                    if(hcaSet != null)
                    {
                        for(String affhca : hcaSet)
                        {
                            SnTDMLSecurityUtil.printDebugMessage('affhca::::::'+affhca);
                            if(mapaffHCA2key.get(affhca) != null)
                            {
                                for(String hcaPosTIKey : mapaffHCA2key.get(affhca))
                                {
                                    SnTDMLSecurityUtil.printDebugMessage('hcaPosTIKey::::::'+hcaPosTIKey);
                                    if(hcpPosTIKey.contains(hcaPosTIKey))
                                    {
                                        if(!mapfurthercheckHCA.containsKey(affhca))
                                        {
                                            SnTDMLSecurityUtil.printDebugMessage('Common Pos');
                                            SnTDMLSecurityUtil.printDebugMessage('hcaPosTIKey common::::::'+hcaPosTIKey);
                                            Set<String> commonKeySET = new Set<String>();
                                            commonKeySET.add(hcaPosTIKey);
                                            mapfurthercheckHCA.put(affhca,commonKeySET);
                                            SnTDMLSecurityUtil.printDebugMessage('mapfurthercheckHCA.put::::::'+mapfurthercheckHCA);
                                        }
                                        else
                                        {
                                            mapfurthercheckHCA.get(affhca).add(hcaPosTIKey);
                                            SnTDMLSecurityUtil.printDebugMessage('mapfurthercheckHCA.add::::::'+mapfurthercheckHCA.get(affhca));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
             }
             SnTDMLSecurityUtil.printDebugMessage('=====mapfurthercheckHCA:::::::::' +mapfurthercheckHCA);

             SnTDMLSecurityUtil.printDebugMessage('==========Further Affiliation Handling for HCAs==================================================================================');
             List<AxtriaSalesIQTM__Account_Affiliation__c> furtherAffHCA2HCPList = [select Id, AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Account__r.Status__c,Account_Number__c,Parent_Account_Number__c,AxtriaSalesIQTM__Affiliation_Network__c,Country__c,IsDeleted,AxtriaSalesIQTM__Parent_Account__c,AxtriaSalesIQTM__Parent_Account__r.AccountNumber,AxtriaSalesIQTM__Root_Account__c from   AxtriaSalesIQTM__Account_Affiliation__c where Affiliation_Status__c='Active' and Parent_Account_Number__c in :mapfurthercheckHCA.keySet() and AxtriaSalesIQTM__Active__c=true];

             SnTDMLSecurityUtil.printDebugMessage('=====Affiliated HCAs to further HCPs list size=====' +furtherAffHCA2HCPList.size());
             SnTDMLSecurityUtil.printDebugMessage('=====Affiliated HCAs to further HCPs=====' +furtherAffHCA2HCPList);

             Map<String,Set<String>> mapFurtherHCA2affHCP=new Map<String,Set<String>>();

            if(furtherAffHCA2HCPList != null)
            {
                for(AxtriaSalesIQTM__Account_Affiliation__c hca2hcpRec : furtherAffHCA2HCPList)
                {
                    if(mapFurtherHCA2affHCP.containsKey(hca2hcpRec.AxtriaSalesIQTM__Parent_Account__r.AccountNumber))
                    {
                        mapFurtherHCA2affHCP.get(hca2hcpRec.AxtriaSalesIQTM__Parent_Account__r.AccountNumber).add(hca2hcpRec.AxtriaSalesIQTM__Account__r.AccountNumber);
                    }
                    else
                    {
                        Set<String> hcp = new Set<String>();
                        hcp.add(hca2hcpRec.AxtriaSalesIQTM__Account__r.AccountNumber);
                        mapFurtherHCA2affHCP.put(hca2hcpRec.AxtriaSalesIQTM__Parent_Account__r.AccountNumber,hcp);  ////Doubt................
                    }
                    
                }
            }

            SnTDMLSecurityUtil.printDebugMessage('=====mapFurtherHCA2affHCP=====' +mapFurtherHCA2affHCP);
             
            SnTDMLSecurityUtil.printDebugMessage('=====Maintain further affiliated HCPs to check the Position Account=================================================');
            Set<String> furtheraffHCPs = new Set<String>();

            if(mapFurtherHCA2affHCP != null)
            {
                for(String hca : mapFurtherHCA2affHCP.keySet())
                {
                    if(mapFurtherHCA2affHCP.get(hca) != null)
                    {
                        furtheraffHCPs.addAll(mapFurtherHCA2affHCP.get(hca));
                    }
                }
            }
            SnTDMLSecurityUtil.printDebugMessage('=====furtheraffHCPs=====' +furtheraffHCPs);

            SnTDMLSecurityUtil.printDebugMessage('=====Querying Position Account for further affiliated HCPs=================================================================');
            List<AxtriaSalesIQTM__Position_Account__c> furtherAffHCPsPosAccList = [select Id,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.AccountNumber,IsGasAssignment__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__c,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c from AxtriaSalesIQTM__Position_Account__c where (AxtriaSalesIQTM__Assignment_Status__c = 'Active' or AxtriaSalesIQTM__Assignment_Status__c = 'Future Active') and AxtriaSalesIQTM__Position__c!=null and AxtriaSalesIQTM__Account__c!=null and AxtriaSalesIQTM__Team_Instance__c!=null and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c in :deassignHCPPosSet and AxtriaSalesIQTM__Account__r.AccountNumber in :furtheraffHCPs and AxtriaSalesIQTM__Team_Instance__r.Name in :deassignHCPTeamInsSet];

            SnTDMLSecurityUtil.printDebugMessage('=====further affiliated HCPs Position Account========' +furtherAffHCPsPosAccList.size());
            Map<String,Set<String>> mapFurtheraffHCP2key=new Map<String,Set<String>>();

            if(furtherAffHCPsPosAccList != null)
            {
                for(AxtriaSalesIQTM__Position_Account__c furtherHCP2key : furtherAffHCPsPosAccList)
                {
                    if(!mapFurtheraffHCP2key.containsKey(furtherHCP2key.AxtriaSalesIQTM__Account__r.AccountNumber))
                    {
                        String tempKey = furtherHCP2key.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' +furtherHCP2key.AxtriaSalesIQTM__Team_Instance__r.Name;
                        Set<String> tempKeySet= new Set<String>();
                        tempKeySet.add(tempKey);
                        mapFurtheraffHCP2key.put(furtherHCP2key.AxtriaSalesIQTM__Account__r.AccountNumber,tempKeySet);
                    }
                    else
                    {
                        String tempKey = furtherHCP2key.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' +furtherHCP2key.AxtriaSalesIQTM__Team_Instance__r.Name;
                        mapFurtheraffHCP2key.get(furtherHCP2key.AxtriaSalesIQTM__Account__r.AccountNumber).add(tempKey);
                    }
                }
            }
            SnTDMLSecurityUtil.printDebugMessage('=====mapFurtheraffHCP2key:::::::::' +mapFurtheraffHCP2key);

            SnTDMLSecurityUtil.printDebugMessage('=====check for commom Position Check::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::');
            Map<String,Set<String>> mapHCAkey2HCPkey = new Map<String,Set<String>>();

            if(mapfurthercheckHCA != null)
            {
                for(String hca : mapfurthercheckHCA.keySet())
                {
                    SnTDMLSecurityUtil.printDebugMessage('=====hca:::::::::' +hca);
                    Set<String> furtherPosTI_HCA = mapfurthercheckHCA.get(hca);
                    SnTDMLSecurityUtil.printDebugMessage('=====furtherPosTI_HCA:::::::::' +furtherPosTI_HCA);
                    if(furtherPosTI_HCA != null)
                    {
                        for(String hcakey : furtherPosTI_HCA)
                        {
                            SnTDMLSecurityUtil.printDebugMessage('=====hcakey:::::::::' +hcakey);
                            String hcakeyCombination = hca+'_'+hcakey;
                            SnTDMLSecurityUtil.printDebugMessage('=====hcakeyCombination:::::::::' +hcakeyCombination);
                            if(mapFurtherHCA2affHCP.get(hca) != null)
                            {
                                for(String hcp : mapFurtherHCA2affHCP.get(hca))
                                {
                                    SnTDMLSecurityUtil.printDebugMessage('=====hcp:::::::::' +hcp);
                                    if(mapFurtheraffHCP2key.get(hcp) != null)
                                    {
                                        for(String hcpkeycheck : mapFurtheraffHCP2key.get(hcp))
                                        {
                                            SnTDMLSecurityUtil.printDebugMessage('=====hcpkeycheck:::::::::' +hcpkeycheck);
                                            if(furtherPosTI_HCA.contains(hcpkeycheck))
                                            {
                                                SnTDMLSecurityUtil.printDebugMessage('=====hcpkeycheck:::::::::' +hcpkeycheck);
                                                String hcpkeyCombination = hcp+'_'+hcpkeycheck;
                                                SnTDMLSecurityUtil.printDebugMessage('=====hcpkeyCombination:::::::::' +hcpkeyCombination);
                                                if(!mapHCAkey2HCPkey.containsKey(hcakeyCombination))
                                                {
                                                    Set<String> setkeyHCP = new Set<String>();
                                                    setkeyHCP.add(hcpkeyCombination);
                                                    mapHCAkey2HCPkey.put(hcakeyCombination,setkeyHCP);
                                                    SnTDMLSecurityUtil.printDebugMessage('=====mapHCAkey2HCPkey inside if:::::::::' +mapHCAkey2HCPkey);
                                                }
                                                else
                                                {
                                                    mapHCAkey2HCPkey.get(hcakeyCombination).add(hcpkeyCombination);
                                                    SnTDMLSecurityUtil.printDebugMessage('=====mapHCAkey2HCPkey inside else:::::::::' +mapHCAkey2HCPkey.get(hcakeyCombination));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }  
            SnTDMLSecurityUtil.printDebugMessage('=====mapHCAkey2HCPkey:::::::::' +mapHCAkey2HCPkey);

            SnTDMLSecurityUtil.printDebugMessage('=====Remove the input HCP from further affiliated HCP================================================================================');
            if(mapHCAkey2HCPkey != null)
            {
                for(String hcakey : mapHCAkey2HCPkey.keySet())
                {
                    SnTDMLSecurityUtil.printDebugMessage('=====hcakey:::::::::' +hcakey);
                    for(String hcpkey : mapHCAkey2HCPkey.get(hcakey))
                    {
                        SnTDMLSecurityUtil.printDebugMessage('=====hcpkey:::::::::' +hcpkey);
                        if(deassignHCPKey.contains(hcpkey))
                        {
                            mapHCAkey2HCPkey.get(hcakey).remove(hcpkey);
                        }
                    }
                }
            }
            SnTDMLSecurityUtil.printDebugMessage('=====mapHCAkey2HCPkey after removing input HCPs key:::::::::' +mapHCAkey2HCPkey);

            Set<String> inactiveHCAkey = new Set<String>();

            if(mapHCAkey2HCPkey != null)
            {
                for(String key_HCA : mapHCAkey2HCPkey.keySet())
                {
                    SnTDMLSecurityUtil.printDebugMessage('=====key_HCA:::::::::' +key_HCA);
                    SnTDMLSecurityUtil.printDebugMessage('=====mapHCAkey2HCPkey.get(key_HCA).size():::::::::' +mapHCAkey2HCPkey.get(key_HCA).size());
                    if(mapHCAkey2HCPkey.get(key_HCA).size() == 0)
                    {
                        inactiveHCAkey.add(key_HCA);
                    }
                }
            }
            SnTDMLSecurityUtil.printDebugMessage('=====inactiveHCAkey:::::::::' +inactiveHCAkey);

            SnTDMLSecurityUtil.printDebugMessage('=====Remove the extra affiliated HCAs if included through cross join==================================================================');
            Set<String> furtherHCAkeyset = new Set<String>();

            if(mapfurthercheckHCA != null)
            {
                for(String hca : mapfurthercheckHCA.keySet())
                {
                    for(String posTI : mapfurthercheckHCA.get(hca))
                    {
                        furtherHCAkeyset.add(hca+'_'+posTI);
                    }
                }
            }
            SnTDMLSecurityUtil.printDebugMessage('=====furtherHCAkeyset:::::::::' +furtherHCAkeyset);

            if(inactiveHCAkey != null)
            {
                for(String inactivekey : inactiveHCAkey)
                {
                    SnTDMLSecurityUtil.printDebugMessage('=====inactivekey:::::::::' +inactivekey);
                    if(!furtherHCAkeyset.contains(inactivekey))
                    {
                        SnTDMLSecurityUtil.printDebugMessage('=====furtherHCAkeyset does not contain inactivekey:::::::::' +inactivekey);
                        inactiveHCAkey.remove(inactivekey);
                    }
                }
            }
            SnTDMLSecurityUtil.printDebugMessage('=====inactiveHCAkey after removing:::::::::' +inactiveHCAkey);

            SnTDMLSecurityUtil.printDebugMessage('====================Handling Position Accounts of affiliated inactive HCP==========================================================');
            List<AxtriaSalesIQTM__Position_Account__c> inactiveaffhcaPosAccList = new List<AxtriaSalesIQTM__Position_Account__c>();

            if(affHCAPosAccList != null)
            {
                for(AxtriaSalesIQTM__Position_Account__c inactiveAffHCARec : affHCAPosAccList)
                {
                    String tempKey = inactiveAffHCARec.AxtriaSalesIQTM__Account__r.AccountNumber + '_' + inactiveAffHCARec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' +inactiveAffHCARec.AxtriaSalesIQTM__Team_Instance__r.Name;
                    if(inactiveHCAkey.contains(tempKey))
                    {
                        inactiveAffHCARec.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addDays(-1);

                        if(flag == true)
                        {
                            inactiveaffhcaPosAccList.add(inactiveAffHCARec);
                        }
                        else if(flag== false)
                        {
                            if(inactiveAffHCARec.IsGasAssignment__c == false)
                            {
                                inactiveaffhcaPosAccList.add(inactiveAffHCARec);
                            }
                        }

                        SnTDMLSecurityUtil.printDebugMessage('========inactiveaffhcaPosAccList::::::::::' +inactiveaffhcaPosAccList);
                    }
                }
            }

            SnTDMLSecurityUtil.printDebugMessage('=====inactiveaffhcaPosAccList.size():::::::::' +inactiveaffhcaPosAccList.size());
            SnTDMLSecurityUtil.printDebugMessage('=====inactiveaffhcaPosAccList:::::::::' +inactiveaffhcaPosAccList);

            if(inactiveaffhcaPosAccList.size() > 0){
                //update inactiveaffhcaPosAccList;
                SnTDMLSecurityUtil.updateRecords(inactiveaffhcaPosAccList, 'DeassignBottomUpPA');
            }
        //end of if
         }

         SnTDMLSecurityUtil.printDebugMessage('deassignList:::::' +deassignList);
         SnTDMLSecurityUtil.printDebugMessage('deassignList.size():::::::' +deassignList.size());

         if(deassignList.size() > 0){
             //update deassignList;
             SnTDMLSecurityUtil.updateRecords(deassignList, 'DeassignBottomUpPA');
         }

         SnTDMLSecurityUtil.printDebugMessage('deassignPosAccList:::::' +deassignPosAccList);
         SnTDMLSecurityUtil.printDebugMessage('deassignPosAccList.size():::::::' +deassignPosAccList.size());

         if(deassignPosAccList.size() > 0){
             //update deassignPosAccList;
             SnTDMLSecurityUtil.updateRecords(deassignPosAccList, 'DeassignBottomUpPA');
         }
        
    }

    global void finish(Database.BatchableContext BC) {

    }
}