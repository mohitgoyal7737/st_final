global class Batch_Populate_PosAcc_HCA_Affiliation implements Database.Batchable<sObject>,database.stateful{

   public string AccountType;
   public string countryName;
   public string AlignmentPeriod;
   public set<string>HCASet= new set<string>();
   public map<string,set<string>> HCP_PosAccMap= new map<string,set<string>>();
   public map<string,set<string>> HCA_PositionMap= new map<string,set<string>>();
   public map<string,string> HCP_HCA= new map<string,string>();
   public list<temp_Acc_Terr__c > tempAccList= new list<temp_Acc_Terr__c >();
   //public list<HCP_without_HCA__c> errorRecordList = new list<HCP_without_HCA__c>();//commented due to object purge activity A1450
   
   global Batch_Populate_PosAcc_HCA_Affiliation ( string accType, string country, string alignPeriod){
    this.AccountType= accType;
    this.countryName=country;
    this.AlignmentPeriod=alignPeriod;
      
   }

   global Database.QueryLocator start(Database.BatchableContext BC){
      
      string query= 'SELECT Id,Name,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Account__r.type,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.name, AxtriaSalesIQTM__Team_Instance__r.Country_Name__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c, AxtriaSalesIQTM__Account__c, AxtriaSalesIQTM__Assignment_Status__c, AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Position_Team_Instance__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Team_Instance__c, Accessibility_Range__c, Speciality__c, Acc_Pos__c, Country__c, AccountNumber__c FROM AxtriaSalesIQTM__Position_Account__c  where AxtriaSalesIQTM__Account__r.type=:AccountType and AxtriaSalesIQTM__Team_Instance__r.Country_Name__c =: countryName and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c =:AlignmentPeriod';  // 
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<AxtriaSalesIQTM__Position_Account__c > scope){
     set<string>uniquehcp = new set<string>();
     for(AxtriaSalesIQTM__Position_Account__c  posAcc: scope)
     {
         if (HCP_PosAccMap.containsKey(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber))
         {
               HCP_PosAccMap.get(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber). add(posAcc.AxtriaSalesIQTM__Position__c);     
         }
         else
         {
              HCP_PosAccMap.put(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber,new set <string> {posAcc.AxtriaSalesIQTM__Position__c});
         }
         
      }
      
      for (AxtriaSalesIQTM__Account_Affiliation__c  accAff : [select id,AxtriaSalesIQTM__Account__r.AccountNUmber,AxtriaSalesIQTM__Parent_Account__r.AccountNumber from AxtriaSalesIQTM__Account_Affiliation__c
      where AxtriaSalesIQTM__Account__r.AccountNUmber IN :HCP_PosAccMap.keyset() and Primary_Affiliation_Indicator__c='Y' and AxtriaSalesIQTM__Active__c=true and AxtriaSalesIQTM__Is_Primary__c=true] )
      {
         
          HCP_HCA.put(accAff.AxtriaSalesIQTM__Account__r.AccountNUmber,accAff.AxtriaSalesIQTM__Parent_Account__r.AccountNumber);
          HCASet.add(accAff.AxtriaSalesIQTM__Parent_Account__r.AccountNumber);
          system.debug('HCASet========='+HCASet);
      }     
      
      for(AxtriaSalesIQTM__Position_Account__c posAcc :[select id,AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__r.name from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Account__r.AccountNumber IN:HCASet])
      {
          if (HCA_PositionMap.containsKey(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber))
         {
               HCA_PositionMap.get(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber). add(posAcc.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);     
         }
         else
         {
              HCA_PositionMap.put(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber,new set <string> {posAcc.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c});
         }
      
      }
      
      for(AxtriaSalesIQTM__Position_Account__c  posAcc: scope)
      {
      
        for(string position : HCP_PosAccMap.get(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber))
        {
          set <string> HCA_Positions = new set<string>();
                
          if(HCP_HCA.get(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber)!=null)
          {
              HCA_Positions =HCA_PositionMap.get(HCP_HCA.get(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber));
 
          }
          else
          { 
              if(!uniquehcp.contains(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber)){
                 //HCP_without_HCA__c errorRecord = new HCP_without_HCA__c();//commented due to object purge activity A1450
                 //errorRecord.Account_Number__c=posAcc.AxtriaSalesIQTM__Account__r.AccountNumber;//commented due to object purge activity A1450
                 //errorRecord.Error__c= 'HCA not found';//commented due to object purge activity A1450
                 //errorRecordList.add(errorRecord);//commented due to object purge activity A1450
                 uniquehcp.add(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber);
              }
             
          
          } 
              //system.debug('HCA_Positions.size()======'+HCA_Positions.size());
              //system.debug('HCP_HCA.size()===='+HCP_HCA.size());
              if(HCA_Positions!=null && HCP_HCA!=null)
              {
                  if((!HCA_Positions.contains(position) || HCA_Positions==null) && HCP_HCA.get(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber)!=null )
                  {
                      // create record;
                      
                      temp_Acc_Terr__c sobj = new temp_Acc_Terr__c(
                      Status__c = 'New',      // Status
                      //Reason_Code__c = '',  // Reason Code
                      TeamName__c = posAcc.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.name,        // TeamName
                      AccountNumber__c =HCP_HCA.get(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber),  // AccountNumber
                      Territory_ID__c = posAcc.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,  // Territory ID
                      Account_Type__c = 'HCA'
                     // Account__c = posAcc.AxtriaSalesIQTM__Account__c  
                      );
                      tempAccList.add(sobj);
                  }
               }
          
      }
     }
        system.debug('==================='+tempAccList.size());
        database.insert (tempAccList,false);
        //database.insert (errorRecordList,false);//commented due to object purge activity A1450
        tempAccList.clear();
        HCP_PosAccMap.clear();
        HCA_PositionMap.clear();
        HCP_HCA.clear();
        tempAccList.clear();
        //errorRecordList.clear();//commented due to object purge activity A1450
        HCASet.clear();
   }

   global void finish(Database.BatchableContext BC){
   
   
   }
}