/*
@author - Prince Richard Augustin (A2661)
@description - Test class to test the CompareAnalysisCtrl Controller.
*/

@isTest
private class CompareAnalysisCtrl_Test {
    static testMethod void testMethod1() {
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        mmc.State__c ='Executed';
        insert mmc;
        Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
        insert pp;
        Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
        insert rp;
        AxtriaSalesIQTM__ETL_Config__c etl = TestDataFactory.configureETL();
        insert etl;
        
        Test.startTest();
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        ApexPages.currentPage().getParameters().put('cycleId',teamins.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c);
        ApexPages.currentPage().getParameters().put('teamInstanceId',teamins.id);
        ApexPages.currentPage().getParameters().put('brandID',mmc.Brand_Lookup__c);
        ApexPages.currentPage().getParameters().put('ruleId',mmc.id); 
        CompareAnalysisCtrl c = new CompareAnalysisCtrl();
        c.countryID = countr.id;
        c.selectedSourceCycle = teamins.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c;
        c.fillSourceBusinessUnitOptions();
        c.selectedDestinationCycle = teamins.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c;
        c.filldestinationBusinessUnitOptions();
        c.selectedDestinationBusinessUnit = teamins.id;
        c.fillDestinationProductOptions();
        c.selectedSourceRule =mmc.id;
        c.selectedDestinationBusinessUnit = teamins.id;
        c.selectedDestinationProduct = mmc.Brand_Lookup__c;
        c.fillDestinationRuleOptions();
        c.selectedDestinationRule = mmc.Id;
        c.selectedSourceRule=mmc.Id;
        
        Test.stopTest();
    }
    static testMethod void testMethod2() {
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        mmc.State__c ='Executed';
        insert mmc;
        Measure_Master__c mmc1 = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc1.Team_Instance__c = teamins.id;
        mmc1.State__c ='Executed';
        insert mmc1;
        Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
        insert pp;
        Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
        insert rp;
        AxtriaSalesIQTM__ETL_Config__c etlConfig = TestDataFactory.configureETL();
        insert etlConfig;
        
        Test.startTest();
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        ApexPages.currentPage().getParameters().put('cycleId',teamins.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c);
        ApexPages.currentPage().getParameters().put('teamInstanceId',teamins.id);
        ApexPages.currentPage().getParameters().put('brandID',mmc.Brand_Lookup__c);
        ApexPages.currentPage().getParameters().put('ruleId',mmc.id);
        CompareAnalysisCtrl c = new CompareAnalysisCtrl(); 
        c.countryID = countr.id;
        c.selectedSourceCycle = teamins.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c;
        c.fillSourceBusinessUnitOptions();
        c.selectedDestinationCycle = teamins.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c;
        c.filldestinationBusinessUnitOptions();
        c.selectedDestinationBusinessUnit = teamins.id;
        c.fillDestinationProductOptions();
        c.selectedDestinationBusinessUnit = teamins.id;
        c.selectedDestinationProduct = mmc.Brand_Lookup__c;
        c.fillDestinationRuleOptions();
        
        c.selectedSimulation='Segment Simulation';
        c.selectedSourceRule=mmc.Id;
        c.countryID = countr.id;
        c.redirectToPage();
        c.selectedSimulation='Workload Simulation';
        c.selectedSourceRule=mmc.Id;
        c.countryID = countr.id;
        c.redirectToPage();
        c.selectedSimulation='Modelling';
        c.selectedSourceRule=mmc.Id;
        c.countryID = countr.id;
        c.redirectToPage();
        c.selectedSimulation='Quantile Analysis';
        c.selectedSourceRule=mmc.Id;
        c.countryID = countr.id;
        c.redirectToPage();
        c.selectedSimulation='Comparative Analysis';
        c.selectedSourceRule=mmc.Id;
        c.countryID = countr.id;
        c.redirectToPage();
        c.selectedSimulation='other';
        c.selectedSourceRule=mmc.Id;
        c.countryID = countr.id;
        c.redirectToPage();
        Test.stopTest();
    }
    
    static testMethod void testMethod3() {
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        mmc.State__c ='Executed';
        mmc.is_Deactivated__c = false;
        insert mmc;
        Measure_Master__c mmc1 = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc1.Team_Instance__c = teamins.id;
        mmc1.State__c ='Executed';
        insert mmc1;
        Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
        insert pp;
        Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
        insert rp;
        AxtriaSalesIQTM__ETL_Config__c etlConfig = TestDataFactory.configureETL();
        insert etlConfig;
        
        Test.startTest();
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        ApexPages.currentPage().getParameters().put('cycleId',teamins.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c);
        ApexPages.currentPage().getParameters().put('teamInstanceId',teamins.id);
        ApexPages.currentPage().getParameters().put('brandID',mmc.Brand_Lookup__c);
        ApexPages.currentPage().getParameters().put('ruleId',mmc.id);
        
        CompareAnalysisCtrl c = new CompareAnalysisCtrl(); 
        Http h = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndPoint('http://example.com/example/test');
        request.setHeader('Content-type','application/json');
        request.setMethod('Post');
        request.setTimeout(120000);
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerate());
        c.selectedSourceRule = mmc.Id;
        c.selectedDestinationRule = mmc.Id;
        c.compareFunction();
        c.countryID = countr.id;
        c.selectedSourceCycle = teamins.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c;
        c.fillSourceBusinessUnitOptions();
        c.selectedDestinationCycle = teamins.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c;
        c.filldestinationBusinessUnitOptions();
        c.selectedDestinationBusinessUnit = teamins.id;
        c.fillDestinationProductOptions();
        c.selectedDestinationBusinessUnit = teamins.id;
        c.selectedDestinationProduct = mmc.Brand_Lookup__c;
        c.fillDestinationRuleOptions();
        Test.stopTest();
    }
    
}

/*Http h = new Http();
HttpRequest request = new HttpRequest();
//request.setEndPoint('http://13.66.200.148/ComparativeAnalysis/initParams?fileNameSource=F:/Bayer_SnT/ComparativeAnalysis/Data/Unmanaged%20QA//Data_For_a2P6g000000MjEoEAK.csv&fileNameDestination=F:/Bayer_SnT/ComparativeAnalysis/Data/Unmanaged%20QA/Data_For_a2P6g000000MjEnEAK.csv&sfdcUserName=sntproductdev@axtria.com.qa2&sfdcPassword=***********&sfdcSecurityToken=*************************&sourceRuleId=a2P6g000000MjEoEAK&destinationRuleId=a2P6g000000MjEnEAK&query=null&namespace=&objectName=Account_Compute_Final__c&action=Compare&segUniv=Survey%20Data%20Accounts');

request.setEndPoint('http://example.com/example/test');
request.setHeader('Content-type','application/json');
request.setMethod('GET');
request.setTimeout(120000);
Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratortest());
HttpResponse res ;

res = new Http().send(request);
//MockHttpResponseGeneratortest.respond(request);
c.compareFunction();*/
/*
Http h = new Http();
HttpRequest request = new HttpRequest();
//request.setEndPoint('http://13.66.200.148/ComparativeAnalysis/initParams?fileNameSource=F:/Bayer_SnT/ComparativeAnalysis/Data/Unmanaged%20QA//Data_For_a2P6g000000MjEoEAK.csv&fileNameDestination=F:/Bayer_SnT/ComparativeAnalysis/Data/Unmanaged%20QA/Data_For_a2P6g000000MjEnEAK.csv&sfdcUserName=sntproductdev@axtria.com.qa2&sfdcPassword=***********&sfdcSecurityToken=*************************&sourceRuleId=a2P6g000000MjEoEAK&destinationRuleId=a2P6g000000MjEnEAK&query=null&namespace=&objectName=Account_Compute_Final__c&action=Compare&segUniv=Survey%20Data%20Accounts');

request.setEndPoint('http://example.com/example/test');
request.setHeader('Content-type','application/json');
request.setMethod('GET');
request.setTimeout(120000);
Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratortest());
//HttpResponse res =CompareAnalysisCtrl.compareFunction();
/*
String contentType = res.getHeader('Content-Type');
System.assert(contentType == 'application/json');
String actualValue = res.getBody();
String expectedValue = '{"example":"test"}';
System.assertEquals(actualValue, expectedValue);
System.assertEquals(200, res.getStatusCode());
*/
//c.compareFunction();