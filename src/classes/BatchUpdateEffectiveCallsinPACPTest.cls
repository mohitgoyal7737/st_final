@isTest
public class BatchUpdateEffectiveCallsinPACPTest {
    static testMethod void testMethod1() {
         User loggedInUser = new User(id=UserInfo.getUserId());
       
        AxtriaSalesIQTM__Organization_Master__c orgmas = new AxtriaSalesIQTM__Organization_Master__c();
        orgmas.Name = 'abcd';
        orgmas.AxtriaSalesIQTM__Org_Level__c = 'Global';
        orgmas.AxtriaSalesIQTM__Parent_Country_Level__c = true;
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c countr = new AxtriaSalesIQTM__Country__c();
        countr.Name = 'Spain';
        orgmas.AxtriaSalesIQTM__Org_Level__c = 'Global';
        countr.AxtriaSalesIQTM__Parent_Organization__c = orgmas.id;
        countr.AxtriaSalesIQTM__Status__c = 'Active';
        countr.MCCP_Enabled__c = true;
        insert  countr;
        
        Account a1= TestDataFactory.createAccount();
        a1.AccountNumber = 'BH10461999';
        a1.Status__c = 'Active';
        a1.Merge_Account_Number__c = 'BH10461999';
        insert a1;
        
        Account acc= TestDataFactory.createAccount();
        acc.AccountNumber = 'BH10455999';
        acc.Status__c = 'Active';
        acc.Merge_Account_Number__c =  'BH10455999';
        insert acc;

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
        insert team;

        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        insert scen;
        
        //AxtriaSalesIQTM__Team_Instance__c ti = [Select name from AxtriaSalesIQTM__Team_Instance__c where Channel_Weights__c != Null and Metric_Mapping__c !=Null and MCCP_Enabled__c = true LIMIT 1];
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Name = 'Oncology-Q4-2019';
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        teamins.AxtriaSalesIQTM__IC_EffEndDate__c = date.today()+1;
        teamins.AxtriaSalesIQTM__IC_EffstartDate__c = date.today();
        teamins.Channel_Weights__c  = '1,1,1';
        teamins.Metric_Mapping__c = 'Final_TCF_Approved__c,AxtriaSalesIQTM__Metric2_Approved__c,AxtriaSalesIQTM__Metric1_Approved__c';
        teamins.MCCP_Enabled__c = true;
        insert teamins;
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__inactive__c = false;
        insert pos;
        AxtriaSalesIQTM__Position__c pos1= TestDataFactory.createPosition(team,teamins);
        pos1.AxtriaSalesIQTM__inactive__c = false;
        insert pos1;
      
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        posAccount.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        posAccount.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        posAccount.AxtriaSalesIQTM__Account_Alignment_Type__c = 'Implicit';
        insert posAccount;
        AxtriaSalesIQTM__Position_Account__c posAccount1 = TestDataFactory.createPositionAccount(a1,pos1,teamins);
        posAccount1.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        posAccount1.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        posAccount1.AxtriaSalesIQTM__Account_Alignment_Type__c = 'Implicit';
        insert posAccount1;
    
        AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp = new AxtriaSalesIQTM__Position_Account_Call_Plan__c();
        
        pacp.AxtriaSalesIQTM__Account__c = acc.id;
        pacp.Name = 'test';
        pacp.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        pacp.AxtriaSalesIQTM__Position__c = pos.id;
        pacp.AxtriaSalesIQTM__lastApprovedTarget__c = true;
        pacp.AxtriaSalesIQTM__Metric1_Updated__c = 0;
        pacp.AxtriaSalesIQTM__Metric2_Updated__c = 0;
        pacp.AxtriaSalesIQTM__Metric3_Updated__c = 0;
        pacp.AxtriaSalesIQTM__Metric4_Updated__c = 0;
        pacp.AxtriaSalesIQTM__Metric5_Updated__c = 0;
        pacp.AxtriaSalesIQTM__Metric6_Updated__c = 0;
        pacp.Effective_Calls__c = 0;       
        insert pacp;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp1 = new AxtriaSalesIQTM__Position_Account_Call_Plan__c();
        
        pacp1.AxtriaSalesIQTM__Account__c = acc.id;
        pacp1.Name = 'test';
        pacp1.AxtriaSalesIQTM__Team_Instance__c = teamins1.id;
        pacp1.AxtriaSalesIQTM__Position__c = pos.id;
        pacp1.AxtriaSalesIQTM__lastApprovedTarget__c = true;
        pacp1.AxtriaSalesIQTM__Metric1_Updated__c = 0;
        pacp1.AxtriaSalesIQTM__Metric2_Updated__c = 0;
        pacp1.AxtriaSalesIQTM__Metric3_Updated__c = 0;
        pacp1.AxtriaSalesIQTM__Metric4_Updated__c = 0;
        pacp1.AxtriaSalesIQTM__Metric5_Updated__c = 0;
        pacp1.AxtriaSalesIQTM__Metric6_Updated__c = 0;
        pacp1.Effective_Calls__c = 0;       
        insert pacp1;
        
      /*List<Veeva_Channels__mdt> VeevaMetadata = new List<Veeva_Channels__mdt>();
                VeevaMetadata = [Select Channels__c, Team_Instance_Name__c, Metric_Mapping__c, Workload_Equivalent__c FROM Veeva_Channels__mdt where Team_Instance_Name__c = 'X2020' and Channels__c = 'F2F' ]; */
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD , false));
            Datetime dt = Datetime.now().addMinutes(1);
           /* BatchUpdateEffectiveCallsinPACP obj=new BatchUpdateEffectiveCallsinPACP(teamins.id);
            String query = 'Select id,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Metric1_Updated__c,AxtriaSalesIQTM__Metric2_Updated__c,AxtriaSalesIQTM__Metric3_Updated__c, AxtriaSalesIQTM__Metric4_Updated__c, AxtriaSalesIQTM__Metric5_Updated__c, AxtriaSalesIQTM__Metric6_Updated__c, Effective_Calls__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Team_Instance__c= :team_instance_id';
            obj.query=query;
            Database.executeBatch(obj);*/
        }
        Test.stopTest();
    }
    static testMethod void testMethod2() {
         User loggedInUser = new User(id=UserInfo.getUserId());
       
        AxtriaSalesIQTM__Organization_Master__c orgmas = new AxtriaSalesIQTM__Organization_Master__c();
        orgmas.Name = 'abcd';
        orgmas.AxtriaSalesIQTM__Org_Level__c = 'Global';
        orgmas.AxtriaSalesIQTM__Parent_Country_Level__c = true;
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c countr = new AxtriaSalesIQTM__Country__c();
        countr.Name = 'Spain';
        orgmas.AxtriaSalesIQTM__Org_Level__c = 'Global';
        countr.AxtriaSalesIQTM__Parent_Organization__c = orgmas.id;
        countr.AxtriaSalesIQTM__Status__c = 'Active';
        countr.MCCP_Enabled__c = true;
        insert  countr;
        
        Account a1= TestDataFactory.createAccount();
        a1.AccountNumber = 'BH10461999';
        a1.Status__c = 'Active';
        a1.Merge_Account_Number__c = 'BH10461999';
        insert a1;
        
        Account acc= TestDataFactory.createAccount();
        acc.AccountNumber = 'BH10455999';
        acc.Status__c = 'Active';
        acc.Merge_Account_Number__c =  'BH10455999';
        insert acc;

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
        insert team;

        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        insert scen;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Name = 'Oncology-Q4-2019';
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        teamins.AxtriaSalesIQTM__IC_EffEndDate__c = date.today()+1;
        teamins.AxtriaSalesIQTM__IC_EffstartDate__c = date.today();
        teamins.Channel_Weights__c  = null;
        teamins.Metric_Mapping__c = 'Final_TCF_Approved__c,AxtriaSalesIQTM__Metric2_Approved__c,AxtriaSalesIQTM__Metric1_Approved__c';
        teamins.MCCP_Enabled__c = true;
        insert teamins;
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__inactive__c = false;
        insert pos;
        AxtriaSalesIQTM__Position__c pos1= TestDataFactory.createPosition(team,teamins);
        pos1.AxtriaSalesIQTM__inactive__c = false;
        insert pos1;
      
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        posAccount.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        posAccount.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        posAccount.AxtriaSalesIQTM__Account_Alignment_Type__c = 'Implicit';
        insert posAccount;
        AxtriaSalesIQTM__Position_Account__c posAccount1 = TestDataFactory.createPositionAccount(a1,pos1,teamins);
        posAccount1.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        posAccount1.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        posAccount1.AxtriaSalesIQTM__Account_Alignment_Type__c = 'Implicit';
        insert posAccount1;
    
        AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp = new AxtriaSalesIQTM__Position_Account_Call_Plan__c();
        
        pacp.AxtriaSalesIQTM__Account__c = acc.id;
        pacp.Name = 'test';
        pacp.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        pacp.AxtriaSalesIQTM__Position__c = pos.id;
        pacp.AxtriaSalesIQTM__lastApprovedTarget__c = true;
        pacp.AxtriaSalesIQTM__Metric1_Updated__c = 0;
        pacp.AxtriaSalesIQTM__Metric2_Updated__c = 0;
        pacp.AxtriaSalesIQTM__Metric3_Updated__c = 0;
        pacp.AxtriaSalesIQTM__Metric4_Updated__c = 0;
        pacp.AxtriaSalesIQTM__Metric5_Updated__c = 0;
        pacp.AxtriaSalesIQTM__Metric6_Updated__c = 0;
        pacp.Effective_Calls__c = 0;       
        insert pacp;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp1 = new AxtriaSalesIQTM__Position_Account_Call_Plan__c();
        
        pacp1.AxtriaSalesIQTM__Account__c = acc.id;
        pacp1.Name = 'test';
        pacp1.AxtriaSalesIQTM__Team_Instance__c = teamins1.id;
        pacp1.AxtriaSalesIQTM__Position__c = pos.id;
        pacp1.AxtriaSalesIQTM__lastApprovedTarget__c = true;
        pacp1.AxtriaSalesIQTM__Metric1_Updated__c = 0;
        pacp1.AxtriaSalesIQTM__Metric2_Updated__c = 0;
        pacp1.AxtriaSalesIQTM__Metric3_Updated__c = 0;
        pacp1.AxtriaSalesIQTM__Metric4_Updated__c = 0;
        pacp1.AxtriaSalesIQTM__Metric5_Updated__c = 0;
        pacp1.AxtriaSalesIQTM__Metric6_Updated__c = 0;
        pacp1.Effective_Calls__c = 0;       
        insert pacp1;
        
      /*  List<Veeva_Channels__mdt> VeevaMetadata = new List<Veeva_Channels__mdt>();
        VeevaMetadata = [Select Channels__c, Team_Instance_Name__c, Metric_Mapping__c, Workload_Equivalent__c FROM Veeva_Channels__mdt where Team_Instance_Name__c = 'X2020' and Channels__c = 'F2F' ]; */
        
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD , false));
            Datetime dt = Datetime.now().addMinutes(1);
           /* BatchUpdateEffectiveCallsinPACP obj=new BatchUpdateEffectiveCallsinPACP(teamins.id);
            String query = 'Select id,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Metric1_Updated__c,AxtriaSalesIQTM__Metric2_Updated__c,AxtriaSalesIQTM__Metric3_Updated__c, AxtriaSalesIQTM__Metric4_Updated__c, AxtriaSalesIQTM__Metric5_Updated__c, AxtriaSalesIQTM__Metric6_Updated__c, Effective_Calls__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Team_Instance__c= :team_instance_id';
            obj.query=query;
            Database.executeBatch(obj);*/
        }
        Test.stopTest();
    }
}