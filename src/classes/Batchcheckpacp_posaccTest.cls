@isTest
private class Batchcheckpacp_posaccTest {
    static testMethod void testMethod1() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        acc.AccountNumber = 'ES206265';
        acc.Type = 'HCO';
        insert acc;
        Account acc1= TestDataFactory.createAccount();
        acc1.AccountNumber = 'ES206211';
        acc1.Type = 'HCO';
        insert acc1;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Name = 'USA';
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
        insert scen;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        //teamins.AxtriaSalesIQTM__Country__c = countr.id;
        teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        //teamins.Country_Name__c = 'USA';
        insert teamins;
        AxtriaSalesIQTM__Team_Instance__c teamins2 = TestDataFactory.createTeamInstance(team);
        teamins2.AxtriaSalesIQTM__Scenario__c = scen.id;
        //teamins.AxtriaSalesIQTM__Country__c = countr.id;
        teamins2.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        //teamins.Country_Name__c = 'USA';
        insert teamins2;
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        AxtriaSalesIQTM__Position__c pos1= TestDataFactory.createPosition(team,teamins2);
        insert pos1;
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        //posAccount.AxtriaSalesIQTM__Account__r.AccountNumber = 'ES206265';
        insert posAccount;
        AxtriaSalesIQTM__Position_Account__c posAccount1 = TestDataFactory.createPositionAccount(acc1,pos1,teamins2);
        //posAccount.AxtriaSalesIQTM__Account__r.AccountNumber = 'ES206265';
        insert posAccount1;
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        positionAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c = true;
        positionAccountCallPlan.Final_TCF_Approved__c = 3.0;
        positionAccountCallPlan.Final_TCF__c = 3.0;
        positionAccountCallPlan.P1__c = 'GIST';
        //positionAccountCallPlan.AxtriaSalesIQTM__Account__r.AccountNumber = 'ES206265';
        insert positionAccountCallPlan;
       /* AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan1 = TestDataFactory.createPositionAccountCallPlan(mmc,acc1,teamins2,posAccount1,pPriority,pos1);
        positionAccountCallPlan1.AxtriaSalesIQTM__lastApprovedTarget__c = true;
        positionAccountCallPlan1.Final_TCF_Approved__c = 3.0;
        positionAccountCallPlan1.Final_TCF__c = 3.0;
        positionAccountCallPlan1.P1__c = 'GIST';
        //positionAccountCallPlan.AxtriaSalesIQTM__Account__r.AccountNumber = 'ES206265';
        insert positionAccountCallPlan1;*/
        temp_Obj__c newpa= new temp_Obj__c();
        newpa.AccountNumber__c = acc.AccountNumber;
        newpa.Territory_ID__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        newpa.Team_Name__c = teamins.AxtriaSalesIQTM__Team__r.name;
        newpa.Account_Type__c = acc.type;
        insert newpa;

        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            Batchcheckpacp_posacc obj=new Batchcheckpacp_posacc('USA'); 
            obj.query = 'select id,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c ';
            Database.executeBatch(obj);
        }
        Test.stopTest();
    }
}