public with sharing class RedirectToARReport {
  
    public string loggedInPosition                                            {get;set;}
    public string loggedInPositionclientcode                                  {get;set;}
    public String gProfileName                                                {get;set;}
    public String reportname                                                  {get;set;}
    public string buSelected                                                  {get;set;}
    public string buSelectedName                                              {get;set;}
    public List<SelectOption> allbu                                           {get;set;}
    public string lineSelected                                                {get;set;}
    public string lineSelectedName                                            {get;set;}
    public List<SelectOption> allline                                         {get;set;}
    public List<SelectOption> allreports                                      {get;set;}
    public List<AxtriaSalesIQTM__User_Access_Permission__c> loggedInUserData  {get;set;}
    
    public string brandSelected                                               {get;set;}
    public string brandSelectedName                                           {get;set;}
    public List<SelectOption> allbrand                                        {get;set;}
    
    public String countryID                                                   {get;set;}
    Map<string,string> successErrorMap;
    Public AxtriaSalesIQTM__Country__c Country                                {get;set;}

    public RedirectToARReport()
    {
        countryID = SalesIQUtility.getCookie(SalesIQUtility.getCountryCookieName());
        system.debug('##### countryID ' + countryID);
        successErrorMap = SalesIQUtility.checkCountryAccess(countryID);
        system.debug('############ successErrorMap ' + successErrorMap);
        if(successErrorMap.containsKey('Success')){
           countryID = successErrorMap.get('Success');               
           system.debug('########## countryID from Map ' + countryID);
           //As soon as we get Country ID set it in the cookie and it will applicable for whole application.
           SalesIQUtility.setCookieString('CountryID',countryID);
            Country = new AxtriaSalesIQTM__Country__c();
            Country = [select AxtriaSalesIQTM__Country_Flag__c,Name from AxtriaSalesIQTM__Country__c where id =:countryID limit 1];
        }

        enableFlag = false;
        system.debug('inside constructor');
        gProfileName = [Select Id,Name from Profile where Id=:UserInfo.getProfileId()].Name;
        loggedInUserData     = new List<AxtriaSalesIQTM__User_Access_Permission__c>();
        allbu      = new List<SelectOption>();
        allreports      = new List<SelectOption>();
        allline      = new List<SelectOption>();
        allbrand      = new List<SelectOption>();
        
        loggedInUserData = SalesIQUtility.getUserAccessPermistion(Userinfo.getUserId(), countryID);
        for(AxtriaSalesIQTM__User_Access_Permission__c userInfo : loggedInUserData)
        {   
            if(!allbu.contains(new SelectOption(userInfo.AxtriaSalesIQTM__Team_Instance__c, userInfo.AxtriaSalesIQTM__Team_Instance__r.Name))){
            allbu.add(new SelectOption(userInfo.AxtriaSalesIQTM__Team_Instance__c, userInfo.AxtriaSalesIQTM__Team_Instance__r.Name));
            }
        
        }
        
        if(buSelected==null && loggedInUserData.size()>0){
        buSelected = loggedInUserData[0].AxtriaSalesIQTM__Team_Instance__c;
        buSelectedName = loggedInUserData[0].AxtriaSalesIQTM__Team_Instance__r.Name;
        }
        
        
        system.debug('end of constructor +++buSelected'+buSelected+'buSelectedName'+buSelectedName);
        buChanged(); 
        
        
    }
    public boolean enableFlag {get;set;}
    
    public void disableLine()
    {
        enableFlag = false;
        
         if(reportname == 'Rep_Call_Plan_Submission_Report' || reportname == 'Stato_Call_Plan'  || reportname == 'Adoption_per_HCPVisite' || reportname == 'Potential_per_HCPVisite' ) //|| reportname == 'CenterHospitalViewPage'  || reportname == 'TierByHCPPage'   
        {
            enableFlag = true;
        }
         
        if(reportname == 'TierByHCPPage' || reportname == 'CenterHospitalViewPage')    
        {
            
             allbu      = new List<SelectOption>();
            for(AxtriaSalesIQTM__User_Access_Permission__c userInfo : loggedInUserData)
            {
                if( userInfo.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name=='ONCOLOGY')
                {
                     buSelected=userInfo.AxtriaSalesIQTM__Team_Instance__c;
                     allbu.add(new SelectOption(userInfo.AxtriaSalesIQTM__Team_Instance__c, userInfo.AxtriaSalesIQTM__Team_Instance__r.Name));
                }
                
            }
            buChanged();
        }
        else
        {
             allbu      = new List<SelectOption>();
            
            for(AxtriaSalesIQTM__User_Access_Permission__c userInfo : loggedInUserData)
            {
                
                     allbu.add(new SelectOption(userInfo.AxtriaSalesIQTM__Team_Instance__c, userInfo.AxtriaSalesIQTM__Team_Instance__r.Name));
               
            }
            
            buSelected = loggedInUserData[0].AxtriaSalesIQTM__Team_Instance__c;
            buSelectedName = loggedInUserData[0].AxtriaSalesIQTM__Team_Instance__r.Name;
            buChanged();
            
        }
        system.debug('++++++++++++++++++++'+enableFlag);
    }
    
    
    public void buChanged()
    {
        system.debug('in buChanged +++buSelected'+buSelected+'buSelectedName'+buSelectedName);
        allline=new list<selectOption>();
        allline.add(new SelectOption('none','--Choose Team--'));
        for(AxtriaSalesIQTM__User_Access_Permission__c userInfo : loggedInUserData)
        {
            if(userInfo.AxtriaSalesIQTM__Team_Instance__c==buSelected)
            {
                buSelectedName = userInfo.AxtriaSalesIQTM__Team_Instance__r.Name;
                integer x=0;
  
            }
        }
        

    lineChanged();
        
    }
    

    public void lineChanged()
    {
        //system.debug('in buChanged +++buSelected'+lineSelected+'buSelectedName'+lineSelectedName);
        allreports=new list<selectOption>();
        allreports.add(new SelectOption('none','--Choose Report--'));
        for(AxtriaSalesIQTM__User_Access_Permission__c userInfo : loggedInUserData)
        {
            if(userInfo.AxtriaSalesIQTM__Team_Instance__c==buSelected)
            {
            if(gProfileName=='System Administrator' || gProfileName =='Amministratore del sistema')
            {
                
                    allreports.add(new SelectOption('ZipToTerr','ZipToTerr'));
                    allreports.add(new SelectOption('AccountToTerr','AccountToTerr'));
                    allreports.add(new SelectOption('Hierarchy','Hierarchy'));
                    allreports.add(new SelectOption('ChangeRequestReport','ChangeRequestReport'));
                  
              
            }
            if(gProfileName=='HO1' || gProfileName=='HO')
            {
                
                    allreports.add(new SelectOption('ZipToTerr','ZipToTerr'));
                    allreports.add(new SelectOption('AccountToTerr','AccountToTerr'));
                    allreports.add(new SelectOption('Hierarchy','Hierarchy'));
                    allreports.add(new SelectOption('ChangeRequestReport', 'ChangeRequestReport'));
                   
              
            }

       
        }

        }
        system.debug('end of buChanged +++buSelected'+buSelected+'buSelectedName'+buSelectedName);
    }
    
    public PageReference openReport()
    {
        
        AxtriaSalesIQTM__User_Access_Permission__c uap = [select id,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Position__r.Name from AxtriaSalesIQTM__User_Access_Permission__c where AxtriaSalesIQTM__User__c = :Userinfo.getUserId() and AxtriaSalesIQTM__Team_Instance__c =:buSelected LIMIT 1];  
        loggedInPosition = uap.AxtriaSalesIQTM__Position__r.Name;
        loggedInPositionclientcode = uap.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
       // lineSelectedName = [select Name from Line__c where id = :lineSelected].Name;
        system.debug('@@@@@@' + buSelected);
        //system.debug('@@@@@@' + lineSelectedName);
      
        
        if(reportname=='ZipToTerr')
        {
            List<Report> rp= [SELECT Name FROM Report where Name='ZipToTerr'];
            
            if(rp.size()>0) 
            {
                PageReference pg = new PageReference ('/one/one.app#/sObject/'+ rp[0].Id +'/view'+'?fv0='+buSelectedName); //'&fv1='+lineSelectedName+
                pg.setRedirect(true);
                return pg;
            }
        }
        
        else if(reportname=='AccountToTerr')
        {
            List<Report> rp = [select id from Report where Name = 'AccountToTerr'];
            PageReference pg = new PageReference ('/one/one.app#/sObject/'+ rp[0].Id +'/view'+'?fv0='+buSelectedName);//+'&fv1='+lineSelectedName
             pg.setRedirect(true);
            return pg;
        }
        
        else if(reportname=='Hierarchy')
        {
            List<Report> rp = [select id from Report where Name = 'Hierarchy'];
           PageReference pg = new PageReference ('/one/one.app#/sObject/'+ rp[0].Id +'/view'+'?fv0='+buSelectedName); //'&fv1='+lineSelectedName+
            pg.setRedirect(true);
            return pg;
        }
        
        else if(reportname=='ChangeRequestReport')
        {
            List<Report> rp = [select id from Report where Name = 'ChangeRequestReport'];
           PageReference pg = new PageReference ('/one/one.app#/sObject/'+ rp[0].Id +'/view'+'?fv0='+buSelectedName); //'&fv1='+lineSelectedName+
            pg.setRedirect(true);
            return pg;
        }

        
                
 
        
        return null;
    }
}