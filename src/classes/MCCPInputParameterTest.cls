/**
@author       : A1942
@createdDate  : 07-08-2020
@description  : Test class for MCCPInputParameter.cls 
@Revison(s)   : v1.0
 */
@isTest
public with sharing class MCCPInputParameterTest {
    public MCCPInputParameterTest() {

    }

    public static testmethod void testInputParameter(){
        
		String className = 'MCCPInputParameterTest';
        //Basic Data preperation
        AxtriaSalesIQTM__Organization_Master__c orgMaster = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgMaster,className);
        AxtriaSalesIQTM__Country__c country = TestDataFactory.createCountry(orgMaster);
        country.Channels__c = 'F2F;Email';
        country.MCCP_Enabled__c = true;
        SnTDMLSecurityUtil.insertRecords(country,className);
        Segmentation_Scheme__c segmentScheme = TestDataFactory.createSegmentationScheme(country.Id);
        SnTDMLSecurityUtil.insertRecords(segmentScheme,className);
        String workspaceName = 'TestWorkspace';
        Date startDate = Date.newInstance(2020, 2, 17);
        Date endDate = Date.newInstance(2020, 8, 17);
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace(workspaceName, startDate, endDate);
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(country);
        SnTDMLSecurityUtil.insertRecords(team,className);
        AxtriaSalesIQTM__Team_Instance__c teamInstance = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamInstance,className);
        AxtriaSalesIQTM__Position__c position = TestDataFactory.createPosition(team, teamInstance);
        SnTDMLSecurityUtil.insertRecords(position,className);
        String stage = 'Design';
        String processStage = 'Ready';
        String status = 'Active';
        AxtriaSalesIQTM__User_Access_Permission__c userPosition = TestDataFactory.createUserAccessPerm(position, teamInstance, UserInfo.getUserId());
        SnTDMLSecurityUtil.insertRecords(userPosition,className);
        AxtriaSalesIQTM__Scenario__c scenario = TestDataFactory.createScenario(workspace.id, teamInstance.id, null, stage, processStage, status, startDate, endDate); 
        SnTDMLSecurityUtil.insertRecords(scenario,className);
        Product_Catalog__c prodCatalog = TestDataFactory.productCatalog(team, teamInstance, country);
        prodCatalog.Veeva_External_ID__c = prodCatalog.Product_Code__c;//Veeva_External_Id must be equal to Product_Code;
        SnTDMLSecurityUtil.insertRecords(prodCatalog,className);
        AxtriaSalesIQTM__TriggerContol__c ccd= new AxtriaSalesIQTM__TriggerContol__c();
        ccd.Name = 'FillSegmentScheme';
        ccd.AxtriaSalesIQTM__IsStopTrigger__c = true;
        SnTDMLSecurityUtil.insertRecords(ccd,className);
        Measure_Master__c measureMaster = TestDataFactory.createMeasureMaster(prodCatalog, team, teamInstance); 
        measureMaster.PDE_Method__c = 'PDE Optimised & Workload Balanced';
        measureMaster.Rule_Type__c = 'MCCP';
        SnTDMLSecurityUtil.insertRecords(measureMaster,className);
        Id junctionRecordTypeId = Schema.SObjectType.MCCP_CNProd__c.getRecordTypeInfosByName().get('Junction').getRecordTypeId();
        Mccp_CNProd__c cnProd = TestDataFactory.createMccpCNProd(measureMaster.Id, junctionRecordTypeId,prodCatalog);
        SnTDMLSecurityUtil.insertRecords(cnProd,className);
        Id segmentRecordTypeId = Schema.SObjectType.MCCP_CNProd__c.getRecordTypeInfosByName().get('Segment').getRecordTypeId();
        Mccp_CNProd__c cnProd1 = TestDataFactory.createMccpCNProd(measureMaster.Id, segmentRecordTypeId,prodCatalog);
         SnTDMLSecurityUtil.insertRecords(cnProd1,className);
        

        String wrapField =   '[{\"Product\":\"DTC\",\"isChannelDistribution\" : true,\"Segments\": [{\"Name\":\"A\",\"mcPde\":5,\"Channels\":[{\"Name\":\"F2F\",\"interactionVal\":5,\"isOptimised\":true}]}]}]';
        String priority1 = 'Target Count';
        String priority2 = 'Workload';
        String priority3 = '% Product PDE';
        String allowableMcPdes = '1,4,8,12,16,20,24';

        System.Test.startTest();
        
        String nameSpace = MCCP_Utility.sntNamespace(className);
        List<String> PROD_PRIORITY_READ_FIELD = new List<String>{nameSpace+'MCCP_Selected_Products__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Measure_Master__c.SObjectType, PROD_PRIORITY_READ_FIELD, false));

        MCCPInputParameter.fetchInputParameters(measureMaster.id);
        MCCPInputParameter.fetchSegmentScheme();
        MCCPInputParameter.createInputParameter(measureMaster.id, wrapField, priority1, priority2, priority3, allowableMcPdes);
        System.Test.stopTest();
    }
}