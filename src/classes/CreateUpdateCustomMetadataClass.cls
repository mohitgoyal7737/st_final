/**********************************************************************************************
@author     : Himanshu Tariyal (A0994)
@date       : 29th May'2020
@description: Class for creating/updating Custom Metadata records
Revison(s)  : v1.0
**********************************************************************************************/
global with sharing class CreateUpdateCustomMetadataClass implements Metadata.DeployCallback 
{     
    //Inteface method 
    public void handleResult(Metadata.DeployResult result, Metadata.DeployCallbackContext context) 
    {
        if (result.status == Metadata.DeployStatus.Succeeded) {
            SnTDMLSecurityUtil.printDebugMessage('Success Result-' + result);
        } 
        else {
            SnTDMLSecurityUtil.printDebugMessage('Failed Result-' + result);
        }
    }

    //create Custom Metadata record
    @future
    global static void createCustomMetadata(String metdataName,String cols,String sourceTeamInstName,
                                            String destTeamInstName,String recordDevName,String nmsp)
    {
        SnTDMLSecurityUtil.printDebugMessage('metdataName--'+metdataName);
        SnTDMLSecurityUtil.printDebugMessage('cols--'+cols);
        SnTDMLSecurityUtil.printDebugMessage('sourceTeamInstName--'+sourceTeamInstName);
        SnTDMLSecurityUtil.printDebugMessage('destTeamInstName--'+destTeamInstName);

        /*Pattern nonAlphanumeric = Pattern.compile('[^a-zA-Z0-9]');
        Matcher matcher = nonAlphanumeric.matcher(destTeamInstName.replaceAll(' ', '_'));
        String recordDevName = matcher.replaceAll('');*/
        SnTDMLSecurityUtil.printDebugMessage('recordDevName--'+recordDevName);

        Metadata.CustomMetadata cMetadata = new Metadata.CustomMetadata();
        cMetadata.fullName = nmsp+metdataName.replaceAll('__mdt','') + '.' + recordDevName;
        cMetadata.label = destTeamInstName;
        SnTDMLSecurityUtil.printDebugMessage('cMetadata--'+cMetadata);

        String veevaChannelQuery = 'select '+String.escapeSingleQuotes(cols)+' from '+
                                    String.escapeSingleQuotes(metdataName)+' where Team_Instance_Name__c = '+
                                    '\''+String.escapeSingleQuotes(sourceTeamInstName)+
                                    '\' WITH SECURITY_ENFORCED';
        List<sObject> sourceVeevaChannelList = Database.query(veevaChannelQuery);
        SnTDMLSecurityUtil.printDebugMessage('sourceVeevaChannelList--'+sourceVeevaChannelList);
        
        if(sourceVeevaChannelList!=null && sourceVeevaChannelList.size()>0)
        {
            for(sObject sourceVeevaChannelRec : sourceVeevaChannelList)
            {
                for(String colName : cols.split(','))
                {
                    Metadata.CustomMetadataValue cMetadataValue = new Metadata.CustomMetadataValue();
                    cMetadataValue.Field = nmsp+colName;
                    if(colName=='Team_Instance_Name__c')
                        cMetadataValue.Value = destTeamInstName; 
                    else    
                        cMetadataValue.Value = sourceVeevaChannelRec.get(colName); 
                    cMetadata.values.add(cMetadataValue);
                }
                SnTDMLSecurityUtil.printDebugMessage('cMetadata.values--'+cMetadata.values);

                Metadata.DeployContainer mdContainer = new Metadata.DeployContainer();
                mdContainer.addMetadata(cMetadata);
                CreateUpdateCustomMetadataClass callback = new CreateUpdateCustomMetadataClass();
				if (!Test.isRunningTest()) { // -- Added to rectify test class error on 27th Jan,2021- System.AsyncException: Metadata cannot be deployed from within a test
					Id jobId = Metadata.Operations.enqueueDeployment(mdContainer, callback);
				}
            }
        }
    }

    @future
    global static void createCustomMetadata(String metdataName,String cols,String sourceTeamInstName,
                                            String destTeamInstName,String recordDevName)
    {
        SnTDMLSecurityUtil.printDebugMessage('metdataName--'+metdataName);
        SnTDMLSecurityUtil.printDebugMessage('cols--'+cols);
        SnTDMLSecurityUtil.printDebugMessage('sourceTeamInstName--'+sourceTeamInstName);
        SnTDMLSecurityUtil.printDebugMessage('destTeamInstName--'+destTeamInstName);

        /*Pattern nonAlphanumeric = Pattern.compile('[^a-zA-Z0-9]');
        Matcher matcher = nonAlphanumeric.matcher(destTeamInstName.replaceAll(' ', '_'));
        String recordDevName = matcher.replaceAll('');*/
        SnTDMLSecurityUtil.printDebugMessage('recordDevName--'+recordDevName);

        Metadata.CustomMetadata cMetadata = new Metadata.CustomMetadata();
        cMetadata.fullName = metdataName.replaceAll('__mdt','') + '.' + recordDevName;
        cMetadata.label = destTeamInstName;
        SnTDMLSecurityUtil.printDebugMessage('cMetadata--'+cMetadata);

        String veevaChannelQuery = 'select '+String.escapeSingleQuotes(cols)+' from '+
                                    String.escapeSingleQuotes(metdataName)+' where Team_Instance_Name__c = '+
                                    '\''+String.escapeSingleQuotes(sourceTeamInstName)+
                                    '\' WITH SECURITY_ENFORCED';
        List<sObject> sourceVeevaChannelList = Database.query(veevaChannelQuery);
        SnTDMLSecurityUtil.printDebugMessage('sourceVeevaChannelList--'+sourceVeevaChannelList);
        
        if(sourceVeevaChannelList!=null && sourceVeevaChannelList.size()>0)
        {
            for(sObject sourceVeevaChannelRec : sourceVeevaChannelList)
            {
                for(String colName : cols.split(','))
                {
                    Metadata.CustomMetadataValue cMetadataValue = new Metadata.CustomMetadataValue();
                    cMetadataValue.Field = colName;
                    if(colName=='Team_Instance_Name__c')
                        cMetadataValue.Value = destTeamInstName; 
                    else    
                        cMetadataValue.Value = sourceVeevaChannelRec.get(colName); 
                    cMetadata.values.add(cMetadataValue);
                }
                SnTDMLSecurityUtil.printDebugMessage('cMetadata.values--'+cMetadata.values);

                Metadata.DeployContainer mdContainer = new Metadata.DeployContainer();
                mdContainer.addMetadata(cMetadata);
                CreateUpdateCustomMetadataClass callback = new CreateUpdateCustomMetadataClass();
                Id jobId = Metadata.Operations.enqueueDeployment(mdContainer, callback);
            }
        }
    }

    //update Custom Metadata record
    @future
    global static void updateCustomMetadata(String metdataName,String cols,String sourceTeamInstName,
                                            String destTeamInstName,String recordDevName,String nmsp)
    {
        SnTDMLSecurityUtil.printDebugMessage('metdataName--'+metdataName);
        SnTDMLSecurityUtil.printDebugMessage('cols--'+cols);
        SnTDMLSecurityUtil.printDebugMessage('sourceTeamInstName--'+sourceTeamInstName);
        SnTDMLSecurityUtil.printDebugMessage('destTeamInstName--'+destTeamInstName);
        SnTDMLSecurityUtil.printDebugMessage('recordDevName--'+recordDevName);

        /*Pattern nonAlphanumeric = Pattern.compile('[^a-zA-Z0-9]');
        Matcher matcher = nonAlphanumeric.matcher(destTeamInstName.replaceAll(' ', '_'));
        String recordDevName = matcher.replaceAll('');
        SnTDMLSecurityUtil.printDebugMessage('recordDevName--'+recordDevName);*/

        Metadata.CustomMetadata cMetadata = new Metadata.CustomMetadata();
        cMetadata.fullName = nmsp+metdataName.replaceAll('__mdt','') + '.' + recordDevName;
        cMetadata.label = destTeamInstName;
        SnTDMLSecurityUtil.printDebugMessage('cMetadata--'+cMetadata);

        String veevaChannelQuery = 'select '+String.escapeSingleQuotes(cols)+' from '+
                                    String.escapeSingleQuotes(metdataName)+' where Team_Instance_Name__c = '+
                                    '\''+String.escapeSingleQuotes(sourceTeamInstName)+
                                    '\' WITH SECURITY_ENFORCED';
        List<sObject> sourceVeevaChannelList = Database.query(veevaChannelQuery);
        SnTDMLSecurityUtil.printDebugMessage('sourceVeevaChannelList--'+sourceVeevaChannelList);
        
        if(sourceVeevaChannelList!=null && sourceVeevaChannelList.size()>0)
        {
            for(sObject sourceVeevaChannelRec : sourceVeevaChannelList)
            {
                for(String colName : cols.split(','))
                {
                    Metadata.CustomMetadataValue cMetadataValue = new Metadata.CustomMetadataValue();
                    cMetadataValue.Field = nmsp+colName;
                    if(colName=='Team_Instance_Name__c')
                        cMetadataValue.Value = destTeamInstName; 
                    else    
                        cMetadataValue.Value = sourceVeevaChannelRec.get(colName); 
                    cMetadata.values.add(cMetadataValue);
                }
                SnTDMLSecurityUtil.printDebugMessage('cMetadata.values--'+cMetadata.values);

                Metadata.DeployContainer mdContainer = new Metadata.DeployContainer();
                mdContainer.addMetadata(cMetadata);
                CreateUpdateCustomMetadataClass callback = new CreateUpdateCustomMetadataClass();
                Id jobId = Metadata.Operations.enqueueDeployment(mdContainer, callback);
            }
        }
    }

    //update Custom Metadata record
    @future
    global static void updateCustomMetadata(String metdataName,String cols,String sourceTeamInstName,
                                            String destTeamInstName,String recordDevName)
    {
        SnTDMLSecurityUtil.printDebugMessage('metdataName--'+metdataName);
        SnTDMLSecurityUtil.printDebugMessage('cols--'+cols);
        SnTDMLSecurityUtil.printDebugMessage('sourceTeamInstName--'+sourceTeamInstName);
        SnTDMLSecurityUtil.printDebugMessage('destTeamInstName--'+destTeamInstName);
        SnTDMLSecurityUtil.printDebugMessage('recordDevName--'+recordDevName);

        /*Pattern nonAlphanumeric = Pattern.compile('[^a-zA-Z0-9]');
        Matcher matcher = nonAlphanumeric.matcher(destTeamInstName.replaceAll(' ', '_'));
        String recordDevName = matcher.replaceAll('');
        SnTDMLSecurityUtil.printDebugMessage('recordDevName--'+recordDevName);*/

        Metadata.CustomMetadata cMetadata = new Metadata.CustomMetadata();
        cMetadata.fullName = metdataName.replaceAll('__mdt','') + '.' + recordDevName;
        cMetadata.label = destTeamInstName;
        SnTDMLSecurityUtil.printDebugMessage('cMetadata--'+cMetadata);

        String veevaChannelQuery = 'select '+String.escapeSingleQuotes(cols)+' from '+
                                    String.escapeSingleQuotes(metdataName)+' where Team_Instance_Name__c = '+
                                    '\''+String.escapeSingleQuotes(sourceTeamInstName)+'\' WITH SECURITY_ENFORCED';
        List<sObject> sourceVeevaChannelList = Database.query(veevaChannelQuery);
        SnTDMLSecurityUtil.printDebugMessage('sourceVeevaChannelList--'+sourceVeevaChannelList);
        
        if(sourceVeevaChannelList!=null && sourceVeevaChannelList.size()>0)
        {
            for(sObject sourceVeevaChannelRec : sourceVeevaChannelList)
            {
                for(String colName : cols.split(','))
                {
                    Metadata.CustomMetadataValue cMetadataValue = new Metadata.CustomMetadataValue();
                    cMetadataValue.Field = colName;
                    if(colName=='Team_Instance_Name__c')
                        cMetadataValue.Value = destTeamInstName; 
                    else    
                        cMetadataValue.Value = sourceVeevaChannelRec.get(colName); 
                    cMetadata.values.add(cMetadataValue);
                }
                SnTDMLSecurityUtil.printDebugMessage('cMetadata.values--'+cMetadata.values);

                Metadata.DeployContainer mdContainer = new Metadata.DeployContainer();
                mdContainer.addMetadata(cMetadata);
                CreateUpdateCustomMetadataClass callback = new CreateUpdateCustomMetadataClass();
                Id jobId = Metadata.Operations.enqueueDeployment(mdContainer, callback);
            }
        }
    }
}