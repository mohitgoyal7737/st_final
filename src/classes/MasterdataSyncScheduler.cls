global class MasterdataSyncScheduler implements Schedulable {
    
    //MasterdataSyncScheduler.syncDataObjectIdsWithPython('brPullMasterDataSources');
    global void execute(SchedulableContext sc) {
        syncDataObjectIdsWithTalendFuture('brPullMasterDataSources');
    }

    @future (callout=true)
    public static void syncDataObjectIdsWithTalendFuture(String serviceName){
        syncDataObjectIdsWithPython('brPullMasterDataSources');
    }
    
    public static void syncDataObjectIdsWithPython(String serviceName){
        List<AxtriaSalesIQTM__Data_Set__c> dataSetList = [SELECT AxtriaSalesIQTM__Data_Set_Object_Name__c,AxtriaSalesIQTM__destination__c,AxtriaSalesIQTM__is_internal__c,AxtriaSalesIQTM__Is_Master__c,AxtriaSalesIQTM__SalesIQ_Internal__c, (select Id from AxtriaSalesIQTM__Data_Objects__r LIMIT 1) FROM AxtriaSalesIQTM__Data_Set__c WHERE AxtriaSalesIQTM__Is_Master__c = true AND AxtriaSalesIQTM__is_internal__c = false AND AxtriaSalesIQTM__SalesIQ_Internal__c = true AND AxtriaSalesIQTM__Data_Set_Object_Name__c != null];

        List<String> dataObjectIds = new List<String>();
        for(AxtriaSalesIQTM__Data_Set__c dataSet : dataSetList){
            if(!dataSet.AxtriaSalesIQTM__Data_Objects__r.isEmpty())
                dataObjectIds.add(dataSet.AxtriaSalesIQTM__Data_Objects__r[0].Id);
        }

        String dataObjectIdsString = String.join(dataObjectIds, ',');
        system.debug('DO Ids: ' + dataObjectIdsString);
        
        List<AxtriaSalesIQTM__BRMS_Config__mdt> typesList = [Select AxtriaSalesIQTM__BRMS_Type__c,AxtriaSalesIQTM__BRMS_Value__c from AxtriaSalesIQTM__BRMS_Config__mdt where AxtriaSalesIQTM__BRMS_Type__c= 'BRDataSync'];
        string pythonService ;
        if(typesList.size()>0){
            pythonService =typesList[0].AxtriaSalesIQTM__BRMS_Value__c; 
        }  
        

        AxtriaSalesIQTM__ETL_Config__c etlObj = [SELECT AxtriaSalesIQTM__SF_UserName__c, AxtriaSalesIQTM__SF_Password__c, AxtriaSalesIQTM__S3_Security_Token__c FROM AxtriaSalesIQTM__ETL_Config__c where name = 'BRMS' limit 1];
        String sfUserName = etlObj.AxtriaSalesIQTM__SF_UserName__c;
        String sfPassword = etlObj.AxtriaSalesIQTM__SF_Password__c;
        String sfToken = etlObj.AxtriaSalesIQTM__S3_Security_Token__c;
        
        try{
            
            String url = pythonService+'?user_name='+sfUserName+'&password='+sfPassword+'&security_token='+sfToken+'&scenario_id=&dataObjectIds='+dataObjectIdsString+'&namespace=AxtriaSalesIQTM__';
            system.debug('data sync url --  ' + url);
            Http h = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndPoint(url);
            request.setHeader('Content-type', 'application/json');
            request.setMethod('GET');
            request.setTimeout(120000);
            system.debug('request '+request);
            HttpResponse response = h.send(request);
            system.debug('#### Python Response : '+response);
        }
        catch(exception ex){
            system.debug('Error in Python JobCallOut : '+ex.getMessage());
        }
    }
}