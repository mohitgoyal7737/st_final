global class BatchDeltaTSF implements Database.Batchable<sObject> 
{
    public String query;
    public Date Lmd;
    public List<SIQ_TSF_vod_O__c> tsflist;
    public Set<String> allProcessedIds;
    public List<SIQ_TSF_vod_O__c> allStagingTSF;
    public Set<String> teamInstanceSelected;
    public Map<String,String> pacpToSegment10Map;
    public Integer recordsProcessed=0;
    public String batchID;
    global DateTime lastjobDate=null;
    public List<String> allTeamInstances;
    public String cycle{get;set;}
    public Set<String> teamInsSet;
    public integer errorcount{get;set;}
    public String nonProcessedAcs {get;set;}
	 public Boolean chaining = false;
    
    global BatchDeltaTSF(Date lastModifiedDate)
    {

        /*lmd= lastModifiedDate;
        allProcessedIds = new List<String>();
        teamInstanceSelected= new Set<String>();
        //tsflist=new List<SIQ_TSF_vod_O__c>();
        system.debug('+++lmd++'+lmd);
         system.debug('+++lastModifiedDate++'+lastModifiedDate);

        query='Select id, P1__c, Segment__c, AxtriaSalesIQTM__Account__r.AZ_VeevaID__c , AxtriaSalesIQTM__Account__r.AccountNumber ,Adoption__c, Potential__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c,Country__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c,Accessibility_Range__c, Party_ID__r.Accessibility_Range__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__lastApprovedTarget__c,AxtriaSalesIQTM__Segment10__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c WHERE LastModifiedDate= Last_N_Days:1 and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = \'Current\' and (AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = \'Live\' OR AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = \'Published\')';*/
        /*for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp :scope)
        {
          teamInstanceSelected.add(pacp.AxtriaSalesIQTM__Team_Instance__c);
        }*/

    }
     global BatchDeltaTSF(Date lastModifiedDate,List<String> allTeamInstances)
    {
          
        lmd= lastModifiedDate;
        allProcessedIds = new Set<String>();
        teamInstanceSelected= new Set<String>();
        this.allTeamInstances  = allTeamInstances;
        system.debug('+++lmd++'+lmd);
        system.debug('+++lastModifiedDate++'+lastModifiedDate);
        List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
        List<AxtriaSalesIQTM__Team_Instance__c> cycleList = new List<AxtriaSalesIQTM__Team_Instance__c>();
        cycleList=[Select Id,Name, Cycle__r.Name from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current' and Id in :teamInsSet];
        if(cycleList!=null)
        {
            for(AxtriaSalesIQTM__Team_Instance__c t1: cycleList)
            {
                if(t1.Cycle__r.Name !=null && t1.Cycle__r.Name !='')
                cycle = t1.Cycle__r.Name;
            }
        }
            System.debug(cycle);
        schLogList=[Select Id,CreatedDate,Created_Date2__c from Scheduler_Log__c where Job_Name__c='TSF Delta' and Job_Status__c='Successful'  Order By CreatedDate desc LIMIT 1];
        if(schLogList.size()>0)
        {
            lastjobDate=schLogList[0].Created_Date2__c.addDays(-1);  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
         else
        {
            lastjobDate=null;       //else we set the lastjobDate to null
        }
        System.debug('last job'+lastjobDate);
        Scheduler_Log__c sJob = new Scheduler_Log__c();        
            sJob.Job_Name__c = 'TSF Delta';
            sJob.Job_Status__c = 'Failed';
            sJob.Job_Type__c='Outbound';
            if(cycle!=null && cycle!='')
               sJob.Cycle__c=cycle;
               sJob.Created_Date2__c = datetime.now();
        system.debug(datetime.now());
        system.debug(sJob);
            insert sJob;
            batchID = sJob.Id;
            recordsProcessed =0;

        if(lastjobDate == null )
        {
            query='Select id, P1__c, Segment__c, AxtriaSalesIQTM__Account__r.AZ_VeevaID__c , AxtriaSalesIQTM__Account__r.AccountNumber ,Adoption__c, Potential__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c,Country__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c,Accessibility_Range__c, Party_ID__r.Accessibility_Range__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__lastApprovedTarget__c,AxtriaSalesIQTM__Segment10__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__r.Name from AxtriaSalesIQTM__Position_Account_Call_Plan__c WHERE  AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances ' ;             
        }
        else
        {
            query='Select id, P1__c, Segment__c, AxtriaSalesIQTM__Account__r.AZ_VeevaID__c , AxtriaSalesIQTM__Account__r.AccountNumber ,Adoption__c, Potential__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c,Country__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c,Accessibility_Range__c, Party_ID__r.Accessibility_Range__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__lastApprovedTarget__c,AxtriaSalesIQTM__Segment10__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__r.Name from AxtriaSalesIQTM__Position_Account_Call_Plan__c WHERE  AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances and lastModifiedDate >:lastjobDate' ;   
        }
    }

    global BatchDeltaTSF(Date lastModifiedDate,List<String> allTeamInstances, Boolean chain)
    {
        chaining = chain;
        lmd= lastModifiedDate;
        allProcessedIds = new Set<String>();
        teamInstanceSelected= new Set<String>();
        this.allTeamInstances  = allTeamInstances;
        system.debug('+++lmd++'+lmd);
        system.debug('+++lastModifiedDate++'+lastModifiedDate);
        List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
        List<AxtriaSalesIQTM__Team_Instance__c> cycleList = new List<AxtriaSalesIQTM__Team_Instance__c>();
        cycleList=[Select Id,Name, Cycle__r.Name from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current' and Id in :teamInsSet];
        if(cycleList!=null)
        {
            for(AxtriaSalesIQTM__Team_Instance__c t1: cycleList)
            {
                if(t1.Cycle__r.Name !=null && t1.Cycle__r.Name !='')
                cycle = t1.Cycle__r.Name;
            }
        }
        System.debug(cycle);
    schLogList=[Select Id,CreatedDate,Created_Date2__c from Scheduler_Log__c where Job_Name__c='TSF Delta' and Job_Status__c='Successful'  Order By CreatedDate desc LIMIT 1];
        if(schLogList.size()>0)
        {
            lastjobDate=schLogList[0].Created_Date2__c.addDays(-1);  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
         else
        {
            lastjobDate=null;       //else we set the lastjobDate to null
        }
        System.debug('last job'+lastjobDate);
        Scheduler_Log__c sJob = new Scheduler_Log__c();        
        sJob.Job_Name__c = 'TSF Delta';
        sJob.Job_Status__c = 'Failed';
        sJob.Job_Type__c='Outbound';
        if(cycle!=null && cycle!='')
        sJob.Cycle__c=cycle;
        sJob.Created_Date2__c = datetime.now();
        system.debug(datetime.now());
        system.debug(sJob);
            insert sJob;
            batchID = sJob.Id;
            recordsProcessed =0;
        if(lastjobDate == null )
            {
                query='Select id, P1__c, Segment__c, AxtriaSalesIQTM__Account__r.AZ_VeevaID__c , AxtriaSalesIQTM__Account__r.AccountNumber ,Adoption__c, Potential__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c,Country__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c,Accessibility_Range__c, Party_ID__r.Accessibility_Range__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__lastApprovedTarget__c,AxtriaSalesIQTM__Segment10__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__r.Name from AxtriaSalesIQTM__Position_Account_Call_Plan__c WHERE  AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances' ;             
            }
        else
            {
                query='Select id, P1__c, Segment__c, AxtriaSalesIQTM__Account__r.AZ_VeevaID__c , AxtriaSalesIQTM__Account__r.AccountNumber ,Adoption__c, Potential__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c,Country__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c,Accessibility_Range__c, Party_ID__r.Accessibility_Range__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__lastApprovedTarget__c,AxtriaSalesIQTM__Segment10__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__r.Name from AxtriaSalesIQTM__Position_Account_Call_Plan__c WHERE  AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances and lastModifiedDate >:lastjobDate' ;   
            }
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        system.debug('++query++'+query);   
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scope) 
    {
      
         system.debug('++query++'+query);
         pacpToSegment10Map = new Map<String,String>();
         allStagingTSF= new List<SIQ_TSF_vod_O__c>(); 

        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c posAccountCallPlan : scope)           
        { 
            SIQ_TSF_vod_O__c stsf = new SIQ_TSF_vod_O__c();
               
            if(posAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c==true)
            { 
                system.debug('++if++');
               
                if(posAccountCallPlan.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__c != null)

                  stsf.Name = posAccountCallPlan.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__r.Name;
                else

                  stsf.Name = 'Vacant';
                
                stsf.SIQ_My_Target_vod__c = true;
                stsf.SIQ_Account_Number__c = posAccountCallPlan.AxtriaSalesIQTM__Account__r.AccountNumber;
                stsf.SIQ_Account_vod__c = posAccountCallPlan.AxtriaSalesIQTM__Account__r.AccountNumber;
                stsf.SIQ_Territory_vod__c = posAccountCallPlan.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;

                if(posAccountCallPlan.Party_ID__r.Accessibility_Range__c !=null)
                {
                    stsf.SIQ_Accessibility_AZ_EU__c = posAccountCallPlan.AxtriaARSnT__Accessibility_Range__c;
                }
                 else
                {
                    stsf.SIQ_Accessibility_AZ_EU__c = posAccountCallPlan.Party_ID__r.Accessibility_Range__c;
                } 
                    
                stsf.Team_Instance__c = posAccountCallPlan.AxtriaSalesIQTM__Team_Instance__c;
                stsf.Team_Instance_Lookup__c = posAccountCallPlan.AxtriaSalesIQTM__Team_Instance__c;
                stsf.Position_Account_Call_Plan__c = posAccountCallPlan.ID;
                stsf.SIQ_External_ID_AZ__c = stsf.SIQ_Account_Number__c +'__'+ stsf.SIQ_Territory_vod__c;
                stsf.Status__c = 'Updated';
                system.debug('stsf.SIQ_External_ID_AZ__c'+stsf.SIQ_External_ID_AZ__c);
            }
            else
            {
                system.debug('++else++');

                stsf.SIQ_My_Target_vod__c = false;

                if(posAccountCallPlan.AxtriaSalesIQTM__Segment10__c== 'Inactive')
                {
                    stsf.Status__c = 'Deleted';
                }
                else if(posAccountCallPlan.AxtriaSalesIQTM__Segment10__c == 'Loser Account')
                {
                    stsf.Status__c = 'Deleted';
                }
                else
                { 
                    stsf.Status__c = 'Updated';
                }

                stsf.SIQ_External_ID_AZ__c = posAccountCallPlan.AxtriaSalesIQTM__Account__r.AccountNumber +'__'+  posAccountCallPlan.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
                system.debug('+++ stsf.SIQ_External_ID_AZ__c'+ stsf.SIQ_External_ID_AZ__c);
            }
                system.debug('====SG==External id is:::::'+stsf.SIQ_External_ID_AZ__c +'Record id is::::::::'+posAccountCallPlan.id); 
            if(!allProcessedIds.contains(stsf.SIQ_External_ID_AZ__c))
                {
                    system.debug('++external'+stsf.SIQ_External_ID_AZ__c);
                    allStagingTSF.add(stsf);
                    allProcessedIds.add(stsf.SIQ_External_ID_AZ__c);
                    system.debug('recordsProcessed+'+recordsProcessed);
                    recordsProcessed++;
                }   
            system.debug('+++allStagingTSF++'+allStagingTSF);
            system.debug('+++stsf.SIQ_External_ID_AZ__c++'+stsf.SIQ_External_ID_AZ__c);
        }
        upsert allStagingTSF SIQ_External_ID_AZ__c;       
    }

    global void finish(Database.BatchableContext BC) 
    {
        Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID); 
        String ErrorMsg ='ERROR COUNT:-'+errorcount+'--'+nonProcessedAcs; 
        system.debug('schedulerObj++++before'+sJob);      
        //Update the scheduler log with successful
        sJob.No_Of_Records_Processed__c=recordsProcessed;
        sjob.Object_Name__c = 'TSF Delta';     
        sJob.Job_Status__c='Successful';
        sjob.Changes__c = ErrorMsg;               
        system.debug('sJob++++++++'+sJob);
        update sJob;
 
        if(chaining)
        {
            Database.executeBatch(new changeProductMetricsDeltaStatus(allTeamInstances, true),2000);
        }
    }
}