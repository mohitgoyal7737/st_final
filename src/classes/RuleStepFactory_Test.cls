@isTest
public class RuleStepFactory_Test {

    @istest static void RuleStepFactory_Test3()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        Test.startTest();
        Step__c step = new Step__c();
        step.Step_Type__c ='Matrix';
        
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            RuleStepFactory obj = new RuleStepFactory();
            obj.getRuleStep(step);
        }
        Test.stopTest();
    }
    @istest static void RuleStepFactory_Test4()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        Test.startTest();
        Step__c step = new Step__c();
        step.Step_Type__c ='Other';
        
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            RuleStepFactory obj = new RuleStepFactory();
            obj.getRuleStep(step);
        }
        Test.stopTest();
    }
}