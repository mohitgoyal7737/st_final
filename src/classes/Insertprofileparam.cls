/* 
  @Author :Siva
  Date:13-10-2018
  Use case:To create the profile parametrs team instance object attribute and corresponding detail values if the product priority value is enabled.Gold version of SnT(Italy)
*/
public with sharing class Insertprofileparam {
  //public map<string,set<string>>qa2parammap;
    public static void Insertprofileparamvalues(string teaminstance,map<string,set<string>>qa2parm,string prod) {
      SnTDMLSecurityUtil.printDebugMessage('============qa2respmap:::'+qa2parm);
      SnTDMLSecurityUtil.printDebugMessage('=================teaminstance==:'+teaminstance);
      SnTDMLSecurityUtil.printDebugMessage('================prod:'+prod);
      string teaminstid=teaminstance;
      map<string,set<string>>qa2parammap;
      qa2parammap = new map<string,set<string>>();
      string product=prod;
      boolean p2 = false;
      set<string>occupiedapis = new set<string>();
      if(qa2parm!=null){
        for(string key : qa2parm.keyset()){
          qa2parammap.put(key,qa2parm.get(key));
        }
      }
      SnTDMLSecurityUtil.printDebugMessage('==========qa2parammap IN new map:'+qa2parammap);
       boolean enablepriority =false;
      AxtriaSalesIQTM__Team_Instance__c tilist = [select id,name,Product_Priority__c from AxtriaSalesIQTM__Team_Instance__c where id=:teaminstid limit 1];
      enablepriority = tilist.Product_Priority__c;
      SnTDMLSecurityUtil.printDebugMessage('===========enablepriority:'+enablepriority);
      /* If the product priority is enabled then the below if condition will be executed and the corresponding team instance object attribute and details will be created.*/
      if(enablepriority){
        list<Temp_Obj_Attribute__c>templist = new list<Temp_Obj_Attribute__c>();
        map<string,string>tempmap = new map<string,string>();
        if(qa2parammap !=null){
          for(string key: qa2parammap.keyset()){
            SnTDMLSecurityUtil.printDebugMessage('=========key::::::'+key);
            SnTDMLSecurityUtil.printDebugMessage('==========values :::'+qa2parammap.get(key));
            string value='';
            for(String valu : qa2parammap.get(key)){
              String temp=valu+';';
              value+=temp;
            }
            SnTDMLSecurityUtil.printDebugMessage('==========value string is :'+value);
            value=value.removeEnd(';');
            tempmap.put(key,value);
          }
          SnTDMLSecurityUtil.printDebugMessage('============tempmap========'+tempmap);
        }
        // filling temp object
        set<string>nonparam = new set<string>();
        nonparam.add('SPECIALTY');
        nonparam.add('SPECIALITY');
        nonparam.add('ACCESSIBILITY');
        nonparam.add('ACCESSIBILITA');
        nonparam.add('ACCESSIBILITÀ');
        //nonparam.add('H2-ACCESSIBILITÀ');
        //nonparam.add('H2-SPECIALITY');
        
        for(string key :tempmap.keyset()){
          Temp_Obj_Attribute__c newtobj = new Temp_Obj_Attribute__c();
          if(!nonparam.contains(key.toUpperCase())){
            newtobj.Team_Instance__c=tilist.id;
            newtobj.Display_Name__c = key;
            newtobj.Attribute_Values2__c = tempmap.get(key);
            newtobj.Unique_ID__c = tilist.id+'_'+key;
            templist.add(newtobj);
          }
        }
        SnTDMLSecurityUtil.printDebugMessage('=====templist==:'+templist);
        if(templist!=null && templist.size()>0){
          upsert templist Unique_ID__c;      
        }
        list<AxtriaSalesIQTM__Team_Instance_Object_Attribute__c>tiobjlist = [select id,AxtriaSalesIQTM__Attribute_API_Name__c,AxtriaSalesIQTM__Attribute_Display_Name__c,NoCustomLabel__c,AxtriaSalesIQTM__Display_Column_Order__c,AxtriaSalesIQTM__Interface_Name__c from AxtriaSalesIQTM__Team_Instance_Object_Attribute__c where AxtriaSalesIQTM__Team_Instance__c=:tilist.id and AxtriaSalesIQTM__Interface_Name__c='Call Plan' order by AxtriaSalesIQTM__Display_Column_Order__c DESC];

          decimal topvalue=tiobjlist[0].AxtriaSalesIQTM__Display_Column_Order__c;
          if(topvalue==null || topvalue==0){
            topvalue=20;
          }
          SnTDMLSecurityUtil.printDebugMessage('===============topvalue:'+topvalue);
          set<string>presentvalues= new set<string>();
          map<string,string>queston2field = new map<string,string>();
          /* Fetching the existing team instance object attributes and creating map to check not to load the same values again*/

          for(AxtriaSalesIQTM__Team_Instance_Object_Attribute__c obj : tiobjlist){
            if(obj.NoCustomLabel__c){
              presentvalues.add((obj.AxtriaSalesIQTM__Attribute_Display_Name__c).toUppercase());
              queston2field.put((obj.AxtriaSalesIQTM__Attribute_Display_Name__c).toUppercase(),obj.AxtriaSalesIQTM__Attribute_API_Name__c);
              occupiedapis.add(obj.AxtriaSalesIQTM__Attribute_API_Name__c);
              
            }
          }
          SnTDMLSecurityUtil.printDebugMessage('==============P2 IS::::'+p2);
          SnTDMLSecurityUtil.printDebugMessage('========presentvalues==:'+presentvalues);
          SnTDMLSecurityUtil.printDebugMessage('=============occupiedapis::::'+occupiedapis);
          list<Field_Api_mapping__c>fieldmapslist = [select id,Field_Api__c,wrapper__c from Field_Api_mapping__c  WHERE Field_Api__c != NULL];
          map<string,string>fieldapimap = new map<string,string>();
          if(fieldmapslist!=null && fieldmapslist.size()>0){
            for(Field_Api_mapping__c api: fieldmapslist){
              fieldapimap.put(api.Field_Api__c,api.wrapper__c);  
            }
            
          }
          SnTDMLSecurityUtil.printDebugMessage('=================fieldapimap::'+fieldapimap);
          /* Checking the product priority based on the product priority we are creating the team instance object attribute */
          //commented due to object purge activity A1422 
          Product_Priority__c pp = [select id,/*Team_Instance__c,*/Priority__c,Product__c,TeamInstance__c from Product_Priority__c where Product__c=:product and TeamInstance__c=:tilist.id and Type__c = 'Specialty' limit 1];//Shivansh - A1450 -- Replacing Account_To_ProductType__c with Product_Priority__c -- Adding extra filter of Type__c
          string priority =pp.Priority__c;
          SnTDMLSecurityUtil.printDebugMessage('============priority=='+priority);
          /* fetching the brandteam instnace for lookup in the team instance object attribute */
          /*Brand_Team_Instance__c br = [select id from Brand_Team_Instance__c where Brand__c=:product and Team_Instance__c=:tilist.id limit 1];*/
          List<Product_Catalog__c> br = new List<Product_Catalog__c>();
          br = [select id from Product_Catalog__c where Id=:product and Team_Instance__c=:tilist.id limit 1];
          list<AxtriaSalesIQTM__Team_Instance_Object_Attribute__c>inssertlist = new list<AxtriaSalesIQTM__Team_Instance_Object_Attribute__c>();
          if(priority == 'P1'){
            integer i=1;
            for(Temp_Obj_Attribute__c tiobj : templist){
              string key =((string)tiobj.Display_Name__c).toUppercase();
              key=key.remove('\'');
              SnTDMLSecurityUtil.printDebugMessage('========KEY:::'+key);
              if(!presentvalues.contains(key)){
                
                AxtriaSalesIQTM__Team_Instance_Object_Attribute__c cp = new AxtriaSalesIQTM__Team_Instance_Object_Attribute__c();
                String param='Parameter'+i+'__c';
                String param2='P2_Parameter'+i+'__c';
                String param3='P3_Parameter'+i+'__c';
                if(fieldapimap.containskey(param) && !occupiedapis.contains(param)){
                  cp.AxtriaSalesIQTM__Attribute_API_Name__c=param;
                  cp.AxtriaSalesIQTM__Attribute_Display_Name__c=key;
                  cp.AxtriaSalesIQTM__Data_Type__c='Picklist';
                  cp.AxtriaSalesIQTM__Display_Column_Order__c=topvalue+1;
                  cp.AxtriaSalesIQTM__DisplayOn__c=true;
                  cp.AxtriaSalesIQTM__Interface_Name__c='Call Plan';
                  cp.AxtriaSalesIQTM__isEditable__c=true;
                  cp.AxtriaSalesIQTM__isEnabled__c=true;
                  cp.AxtriaSalesIQTM__isRequired__c=true;
                  cp.NoCustomLabel__c =true;
                  //cp.Brand_Team_Instance__c = br.id;
                  if(br!= null && br.size()>0){
                    cp.Brand_Lookup__c = br[0].id;  
                  }
                  
                  cp.Java_Script_Function__c='filldata';
                  cp.AxtriaSalesIQTM__Object_Name__c='AxtriaSalesIQTM__Position_Account_Call_Plan__c';
                  cp.AxtriaSalesIQTM__Team_Instance__c=tilist.id;
                  cp.AxtriaSalesIQTM__WrapperFieldMap__c=fieldapimap.get(param);
                  i++;
                  topvalue++;
                  inssertlist.add(cp);
                  presentvalues.add(key);
                }
                else if(fieldapimap.containskey(param2) && !occupiedapis.contains(param2)){
                  cp.AxtriaSalesIQTM__Attribute_API_Name__c=param2;
                  cp.AxtriaSalesIQTM__Attribute_Display_Name__c=key;
                  cp.AxtriaSalesIQTM__Data_Type__c='Picklist';
                  cp.AxtriaSalesIQTM__Display_Column_Order__c=topvalue+1;
                  cp.AxtriaSalesIQTM__DisplayOn__c=true;
                  cp.AxtriaSalesIQTM__Interface_Name__c='Call Plan';
                  cp.AxtriaSalesIQTM__isEditable__c=true;
                  cp.AxtriaSalesIQTM__isEnabled__c=true;
                  cp.AxtriaSalesIQTM__isRequired__c=true;
                  cp.NoCustomLabel__c =true;
                  //cp.Brand_Team_Instance__c = br.id;
                  if(br!= null && br.size()>0){
                    cp.Brand_Lookup__c = br[0].id;  
                  }
                  cp.Java_Script_Function__c='filldata';
                  cp.AxtriaSalesIQTM__Object_Name__c='AxtriaSalesIQTM__Position_Account_Call_Plan__c';
                  cp.AxtriaSalesIQTM__Team_Instance__c=tilist.id;
                  cp.AxtriaSalesIQTM__WrapperFieldMap__c=fieldapimap.get(param2);
                  i++;
                  topvalue++;
                  inssertlist.add(cp);
                  presentvalues.add(key);
                }
                else if(fieldapimap.containskey(param3) && !occupiedapis.contains(param3)){
                  cp.AxtriaSalesIQTM__Attribute_API_Name__c=param3;
                  cp.AxtriaSalesIQTM__Attribute_Display_Name__c=key;
                  cp.AxtriaSalesIQTM__Data_Type__c='Picklist';
                  cp.AxtriaSalesIQTM__Display_Column_Order__c=topvalue+1;
                  cp.AxtriaSalesIQTM__DisplayOn__c=true;
                  cp.AxtriaSalesIQTM__Interface_Name__c='Call Plan';
                  cp.AxtriaSalesIQTM__isEditable__c=true;
                  cp.AxtriaSalesIQTM__isEnabled__c=true;
                  cp.AxtriaSalesIQTM__isRequired__c=true;
                  cp.NoCustomLabel__c =true;
                  //cp.Brand_Team_Instance__c = br.id;
                  if(br!= null && br.size()>0){
                    cp.Brand_Lookup__c = br[0].id;  
                  }
                  cp.Java_Script_Function__c='filldata';
                  cp.AxtriaSalesIQTM__Object_Name__c='AxtriaSalesIQTM__Position_Account_Call_Plan__c';
                  cp.AxtriaSalesIQTM__Team_Instance__c=tilist.id;
                  cp.AxtriaSalesIQTM__WrapperFieldMap__c=fieldapimap.get(param3);
                  i++;
                  topvalue++;
                  inssertlist.add(cp);
                  presentvalues.add(key);
                }
                 }
            }
          }
          //end of p1 priority
          if(priority == 'P2'){
            integer i=1;
            for(Temp_Obj_Attribute__c tiobj : templist){
              string key =((string)tiobj.Display_Name__c).toUppercase();
              if(!presentvalues.contains(key)){
                
                AxtriaSalesIQTM__Team_Instance_Object_Attribute__c cp = new AxtriaSalesIQTM__Team_Instance_Object_Attribute__c();
                String param='P2_Parameter'+i+'__c';
                String param3='P3_Parameter'+i+'__c';
                String param4='P4_Parameter'+i+'__c';
                
                if(fieldapimap.containskey(param)&&!occupiedapis.contains(param)){
                  cp.AxtriaSalesIQTM__Attribute_API_Name__c=param;
                  cp.AxtriaSalesIQTM__Attribute_Display_Name__c=key;
                  cp.AxtriaSalesIQTM__Data_Type__c='Picklist';
                  cp.AxtriaSalesIQTM__Display_Column_Order__c=topvalue+1;
                  cp.AxtriaSalesIQTM__DisplayOn__c=true;
                  cp.AxtriaSalesIQTM__Interface_Name__c='Call Plan';
                  cp.AxtriaSalesIQTM__isEditable__c=true;
                  cp.AxtriaSalesIQTM__isEnabled__c=true;
                  cp.AxtriaSalesIQTM__isRequired__c=true;
                  cp.NoCustomLabel__c =true;
                  //cp.Brand_Team_Instance__c = br.id;
                  if(br!= null && br.size()>0){
                    cp.Brand_Lookup__c = br[0].id;  
                  }
                  cp.Java_Script_Function__c='filldata';
                  cp.AxtriaSalesIQTM__Object_Name__c='AxtriaSalesIQTM__Position_Account_Call_Plan__c';
                  cp.AxtriaSalesIQTM__Team_Instance__c=tilist.id;
                  cp.AxtriaSalesIQTM__WrapperFieldMap__c=fieldapimap.get(param);
                  i++;
                  topvalue++;
                  inssertlist.add(cp);
                  presentvalues.add(key);
                }
                else if(fieldapimap.containskey(param3)&& !occupiedapis.contains(param3)){
                  cp.AxtriaSalesIQTM__Attribute_API_Name__c=param3;
                  cp.AxtriaSalesIQTM__Attribute_Display_Name__c=key;
                  cp.AxtriaSalesIQTM__Data_Type__c='Picklist';
                  cp.AxtriaSalesIQTM__Display_Column_Order__c=topvalue+1;
                  cp.AxtriaSalesIQTM__DisplayOn__c=true;
                  cp.AxtriaSalesIQTM__Interface_Name__c='Call Plan';
                  cp.AxtriaSalesIQTM__isEditable__c=true;
                  cp.AxtriaSalesIQTM__isEnabled__c=true;
                  cp.AxtriaSalesIQTM__isRequired__c=true;
                  cp.NoCustomLabel__c =true;
                  //cp.Brand_Team_Instance__c = br.id;
                  if(br!= null && br.size()>0){
                    cp.Brand_Lookup__c = br[0].id;  
                  }
                  cp.Java_Script_Function__c='filldata';
                  cp.AxtriaSalesIQTM__Object_Name__c='AxtriaSalesIQTM__Position_Account_Call_Plan__c';
                  cp.AxtriaSalesIQTM__Team_Instance__c=tilist.id;
                  cp.AxtriaSalesIQTM__WrapperFieldMap__c=fieldapimap.get(param3);
                  i++;
                  topvalue++;
                  inssertlist.add(cp);
                  presentvalues.add(key);
                }
                else if(fieldapimap.containskey(param4)&&!occupiedapis.contains(param4)){
                  cp.AxtriaSalesIQTM__Attribute_API_Name__c=param4;
                  cp.AxtriaSalesIQTM__Attribute_Display_Name__c=key;
                  cp.AxtriaSalesIQTM__Data_Type__c='Picklist';
                  cp.AxtriaSalesIQTM__Display_Column_Order__c=topvalue+1;
                  cp.AxtriaSalesIQTM__DisplayOn__c=true;
                  cp.AxtriaSalesIQTM__Interface_Name__c='Call Plan';
                  cp.AxtriaSalesIQTM__isEditable__c=true;
                  cp.AxtriaSalesIQTM__isEnabled__c=true;
                  cp.AxtriaSalesIQTM__isRequired__c=true;
                  cp.NoCustomLabel__c =true;
                  //cp.Brand_Team_Instance__c = br.id;
                  if(br!= null && br.size()>0){
                    cp.Brand_Lookup__c = br[0].id;  
                  }
                  cp.Java_Script_Function__c='filldata';
                  cp.AxtriaSalesIQTM__Object_Name__c='AxtriaSalesIQTM__Position_Account_Call_Plan__c';
                  cp.AxtriaSalesIQTM__Team_Instance__c=tilist.id;
                  cp.AxtriaSalesIQTM__WrapperFieldMap__c=fieldapimap.get(param4);
                  i++;
                  topvalue++;
                  inssertlist.add(cp);
                  presentvalues.add(key);
                }
                 }
            }
          }
          // end of p2 priority
          if(priority == 'P3'){
            integer i=1;
            for(Temp_Obj_Attribute__c tiobj : templist){
              string key =((string)tiobj.Display_Name__c).toUppercase();
              if(!presentvalues.contains(key)){
                
                AxtriaSalesIQTM__Team_Instance_Object_Attribute__c cp = new AxtriaSalesIQTM__Team_Instance_Object_Attribute__c();
                String param='P3_Parameter'+i+'__c';
                String param4='P4_Parameter'+i+'__c';
                String param5='P5_Parameter'+i+'__c';
                
                
                if(fieldapimap.containskey(param) && !occupiedapis.contains(param)){
                  cp.AxtriaSalesIQTM__Attribute_API_Name__c=param;
                  cp.AxtriaSalesIQTM__Attribute_Display_Name__c=key;
                  cp.AxtriaSalesIQTM__Data_Type__c='Picklist';
                  cp.AxtriaSalesIQTM__Display_Column_Order__c=topvalue+1;
                  cp.AxtriaSalesIQTM__DisplayOn__c=true;
                  cp.AxtriaSalesIQTM__Interface_Name__c='Call Plan';
                  cp.AxtriaSalesIQTM__isEditable__c=true;
                  cp.AxtriaSalesIQTM__isEnabled__c=true;
                  cp.AxtriaSalesIQTM__isRequired__c=true;
                  cp.NoCustomLabel__c =true;
                  //cp.Brand_Team_Instance__c = br.id;
                  if(br!= null && br.size()>0){
                    cp.Brand_Lookup__c = br[0].id;  
                  }
                  cp.Java_Script_Function__c='filldata';
                  cp.AxtriaSalesIQTM__Object_Name__c='AxtriaSalesIQTM__Position_Account_Call_Plan__c';
                  cp.AxtriaSalesIQTM__Team_Instance__c=tilist.id;
                  cp.AxtriaSalesIQTM__WrapperFieldMap__c=fieldapimap.get(param);
                  i++;
                  topvalue++;
                  inssertlist.add(cp);
                  presentvalues.add(key);
                }
                else if(fieldapimap.containskey(param4) &&!occupiedapis.contains(param4)){
                  cp.AxtriaSalesIQTM__Attribute_API_Name__c=param4;
                  cp.AxtriaSalesIQTM__Attribute_Display_Name__c=key;
                  cp.AxtriaSalesIQTM__Data_Type__c='Picklist';
                  cp.AxtriaSalesIQTM__Display_Column_Order__c=topvalue+1;
                  cp.AxtriaSalesIQTM__DisplayOn__c=true;
                  cp.AxtriaSalesIQTM__Interface_Name__c='Call Plan';
                  cp.AxtriaSalesIQTM__isEditable__c=true;
                  cp.AxtriaSalesIQTM__isEnabled__c=true;
                  cp.AxtriaSalesIQTM__isRequired__c=true;
                  cp.NoCustomLabel__c =true;
                  //cp.Brand_Team_Instance__c = br.id;
                  if(br!= null && br.size()>0){
                    cp.Brand_Lookup__c = br[0].id;  
                  }
                  cp.Java_Script_Function__c='filldata';
                  cp.AxtriaSalesIQTM__Object_Name__c='AxtriaSalesIQTM__Position_Account_Call_Plan__c';
                  cp.AxtriaSalesIQTM__Team_Instance__c=tilist.id;
                  cp.AxtriaSalesIQTM__WrapperFieldMap__c=fieldapimap.get(param4);
                  i++;
                  topvalue++;
                  inssertlist.add(cp);
                  presentvalues.add(key);
                }
                else if(fieldapimap.containskey(param5) &&!occupiedapis.contains(param5)){
                  cp.AxtriaSalesIQTM__Attribute_API_Name__c=param5;
                  cp.AxtriaSalesIQTM__Attribute_Display_Name__c=key;
                  cp.AxtriaSalesIQTM__Data_Type__c='Picklist';
                  cp.AxtriaSalesIQTM__Display_Column_Order__c=topvalue+1;
                  cp.AxtriaSalesIQTM__DisplayOn__c=true;
                  cp.AxtriaSalesIQTM__Interface_Name__c='Call Plan';
                  cp.AxtriaSalesIQTM__isEditable__c=true;
                  cp.AxtriaSalesIQTM__isEnabled__c=true;
                  cp.AxtriaSalesIQTM__isRequired__c=true;
                  cp.NoCustomLabel__c =true;
                  //cp.Brand_Team_Instance__c = br.id;
                  if(br!= null && br.size()>0){
                    cp.Brand_Lookup__c = br[0].id;  
                  }
                  cp.Java_Script_Function__c='filldata';
                  cp.AxtriaSalesIQTM__Object_Name__c='AxtriaSalesIQTM__Position_Account_Call_Plan__c';
                  cp.AxtriaSalesIQTM__Team_Instance__c=tilist.id;
                  cp.AxtriaSalesIQTM__WrapperFieldMap__c=fieldapimap.get(param5);
                  i++;
                  topvalue++;
                  inssertlist.add(cp);
                  presentvalues.add(key);
                }
                 }
            }
          }
          // end of p3 priority
          //P2 teaminstance object attribute not exist so creating P2__c
          
          SnTDMLSecurityUtil.printDebugMessage('============inssertlist:::'+inssertlist);
          map<string,string>attribute2idmap = new map<string,string>();
          if(inssertlist !=null && inssertlist.size()>0){
            //insert inssertlist;
            SnTDMLSecurityUtil.insertRecords(inssertlist, 'Insertprofileparam');
            /*To update the segment picklist filed non editable*/
            //list<AxtriaSalesIQTM__Team_Instance_Object_Attribute__c>updatetiobj = new list<AxtriaSalesIQTM__Team_Instance_Object_Attribute__c>();
            set<string>apinameset = new set<string>();
            apinameset.add('Segment__c');
            apinameset.add('Final_TCF__c');
            apinameset.add('P1__c');
            apinameset.add('AxtriaSalesIQTM__Picklist3_Updated__c');
            

            list<AxtriaSalesIQTM__Team_Instance_Object_Attribute__c>updateseg = [select id,Java_Script_Function__c,AxtriaSalesIQTM__Attribute_API_Name__c,AxtriaSalesIQTM__isEditable__c,AxtriaSalesIQTM__isRequired__c from AxtriaSalesIQTM__Team_Instance_Object_Attribute__c where AxtriaSalesIQTM__Attribute_API_Name__c IN :apinameset and AxtriaSalesIQTM__Team_Instance__c=:tilist.id and AxtriaSalesIQTM__Interface_Name__c='Call Plan'];
            for(AxtriaSalesIQTM__Team_Instance_Object_Attribute__c tobj:updateseg){
                if(tobj.AxtriaSalesIQTM__Attribute_API_Name__c == 'Segment__c')
                  tobj.AxtriaSalesIQTM__isEditable__c=false;
                else if(tobj.AxtriaSalesIQTM__Attribute_API_Name__c == 'Final_TCF__c')
                  tobj.Java_Script_Function__c='selectphysicians';
                else if(tobj.AxtriaSalesIQTM__Attribute_API_Name__c == 'P1__c')
                    tobj.AxtriaSalesIQTM__isEditable__c=true;
                else if(tobj.AxtriaSalesIQTM__Attribute_API_Name__c == 'AxtriaSalesIQTM__Picklist3_Updated__c')
                    tobj.AxtriaSalesIQTM__isRequired__c=true;
               }
            //update updateseg;
            SnTDMLSecurityUtil.updateRecords(updateseg, 'Insertprofileparam');

            list<AxtriaSalesIQTM__Team_Instance_Object_Attribute__c>p2list = [select id,AxtriaSalesIQTM__Attribute_API_Name__c from AxtriaSalesIQTM__Team_Instance_Object_Attribute__c where  AxtriaSalesIQTM__Attribute_API_Name__c='P2__c' and AxtriaSalesIQTM__Team_Instance__c=:tilist.id and AxtriaSalesIQTM__Interface_Name__c='Call Plan'];
            list<AxtriaSalesIQTM__Team_Instance_Object_Attribute__c>insertp2 = new list<AxtriaSalesIQTM__Team_Instance_Object_Attribute__c>();
            if(p2list==null || p2list.size()==0){
              SnTDMLSecurityUtil.printDebugMessage('==============P2 Does not exist================');
              AxtriaSalesIQTM__Team_Instance_Object_Attribute__c cp = new AxtriaSalesIQTM__Team_Instance_Object_Attribute__c();
              cp.AxtriaSalesIQTM__Attribute_API_Name__c='P2__c';
            cp.AxtriaSalesIQTM__Attribute_Display_Name__c='Product2';
            cp.AxtriaSalesIQTM__Data_Type__c='Picklist';
            cp.AxtriaSalesIQTM__Display_Column_Order__c=topvalue+1;
            cp.AxtriaSalesIQTM__DisplayOn__c=false;
            cp.AxtriaSalesIQTM__Interface_Name__c='Call Plan';
            cp.AxtriaSalesIQTM__isEditable__c=false;
            cp.AxtriaSalesIQTM__isEnabled__c=false;
            cp.AxtriaSalesIQTM__isRequired__c=true;
            cp.NoCustomLabel__c =true;
            //cp.Brand_Team_Instance__c = br.id;
            //cp.Java_Script_Function__c='filldata';
            cp.AxtriaSalesIQTM__Object_Name__c='AxtriaSalesIQTM__Position_Account_Call_Plan__c';
            cp.AxtriaSalesIQTM__Team_Instance__c=tilist.id;
            cp.AxtriaSalesIQTM__WrapperFieldMap__c='Priority2';
            //i++;
            //topvalue++;
            insertp2.add(cp);
            //insert insertp2;
            SnTDMLSecurityUtil.insertRecords(insertp2, 'Insertprofileparam');

            }

            for(AxtriaSalesIQTM__Team_Instance_Object_Attribute__c tio: inssertlist){
              if(tio.AxtriaSalesIQTM__Attribute_API_Name__c != 'P2__c')
                attribute2idmap.put((tio.AxtriaSalesIQTM__Attribute_Display_Name__c).toUppercase(),tio.id);
            }
            SnTDMLSecurityUtil.printDebugMessage('=========attribute2idmap:'+attribute2idmap);
            /*  Creating attribute detail for the team instance object attribute created  in above section*/
            list<AxtriaSalesIQTM__Team_Instance_Object_Attribute_Detail__c>detaillist = [select id,Type__c,AxtriaSalesIQTM__isActive__c,AxtriaSalesIQTM__Object_Attibute_Team_Instance__c from AxtriaSalesIQTM__Team_Instance_Object_Attribute_Detail__c where AxtriaSalesIQTM__Team_Instance__c=:tilist.id and AxtriaSalesIQTM__Object_Attibute_Team_Instance__c  IN :attribute2idmap.values() ];
            if(detaillist.size()==0){
              SnTDMLSecurityUtil.printDebugMessage('===========ENTERED Attribute detail function:');
              list<AxtriaSalesIQTM__Team_Instance_Object_Attribute_Detail__c>insertdetail = new list<AxtriaSalesIQTM__Team_Instance_Object_Attribute_Detail__c>();
              for(string attribute: attribute2idmap.keyset()){
                decimal j=1;
                string values =tempmap.get(attribute);
                SnTDMLSecurityUtil.printDebugMessage('========Values String:::::'+values);
                list<string>valuelist = values.split(';');
                SnTDMLSecurityUtil.printDebugMessage('=================valuelist::'+valuelist);
                set<string>unqval = new set<string>();
                for(string s :valuelist){
                  s=s.remove('\'');
                  unqval.add(s);
                }
                valuelist.clear();
                valuelist.addall(unqval);
                
                for(string detail : valuelist){
                  detail=detail.remove('\'');
                  SnTDMLSecurityUtil.printDebugMessage('========DETAIL is::'+detail);
                  SnTDMLSecurityUtil.printDebugMessage('=====unqval is:'+unqval);
                  
                  AxtriaSalesIQTM__Team_Instance_Object_Attribute_Detail__c dt= new AxtriaSalesIQTM__Team_Instance_Object_Attribute_Detail__c();
                  dt.AxtriaSalesIQTM__isActive__c=true;
                  dt.AxtriaSalesIQTM__Object_Attibute_Team_Instance__c = attribute2idmap.get(attribute);
                  dt.AxtriaSalesIQTM__Object_Value_Name__c = detail;
                  dt.AxtriaSalesIQTM__Team_Instance__c=tilist.id;
                  dt.Type__c = 'Change TCF';
                  dt.AxtriaSalesIQTM__Object_Value_Seq__c=string.valueOf(j);
                  j++;
                  insertdetail.add(dt);
                }

              }
              SnTDMLSecurityUtil.printDebugMessage('============detaillist:::::'+detaillist);
              if(insertdetail!=null && insertdetail.size() >0){
                //insert insertdetail;
                SnTDMLSecurityUtil.insertRecords(insertdetail, 'Insertprofileparam');
              }
            }
          }


      }//end of if block of enablepriority
    }//end of static method
    
}