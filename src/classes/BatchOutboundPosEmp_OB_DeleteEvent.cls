global class BatchOutboundPosEmp_OB_DeleteEvent implements Database.Batchable<sObject> {
   
    global Database.QueryLocator start(Database.BatchableContext BC) {
        //String query = 'SELECT Id,Name,Account_ID__c,Scenario_Name__c,AxtriaARSnT__POSITION__c,AxtriaARSnT__Objective__c,AxtriaARSnT__Target__c,Product_Name__c,Workspace__c,Adoption__c,Potential__c,Product_ID__c,Segment__c FROM AxtriaARSnT__Direct_Load_Product_Matrix__c where Status__c = \'New\'';
        //return Database.getQueryLocator(query);
        
        String query = 'SELECT CreatedDate,Id,LastModifiedDate,Name,SIQ_ASSIGNMENT_END_DATE__c,SIQ_ASSIGNMENT_START_DATE__c,SIQ_ASSIGNMENT_TYPE__c,SIQ_Country_Code__c,SIQ_Created_Date__c,SIQ_EMP_ID__c,SIQ_Event__c,SIQ_isActive__c,SIQ_Marketing_Code__c,SIQ_Permanent_Position_Flag__c,SIQ_POSITION_CODE__c,SIQ_Position_Type__c,SIQ_Salesforce_Name__c,SIQ_Team_Instance__c,SIQ_Updated_Date__c,SystemModstamp,Unique_Id__c FROM SIQ_Position_Employee_O__c where SIQ_isActive__c = \'Active\' and SIQ_Position_Type__c !=\'Global Admins\' ';
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<SIQ_Position_Employee_O__c> scope) {


        System.debug('<><><>scope<><><>'+scope);


         List<SIQ_Position_Employee_O__c> poSEmpUpdOutbound = new List<SIQ_Position_Employee_O__c>();
         Map<String,String> poSEmpActiveMap = new Map<String,String>();
         Map<String,Date> poSEmpInactiveMap = new Map<String,Date>();


         for(AxtriaSalesIQTM__Position_Employee__c posEmp : [Select AxtriaSalesIQTM__Assignment_Status__c,AxtriaSalesIQTM__Assignment_Type__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,Employee_PRID__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__r.Name,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.Name from AxtriaSalesIQTM__Position_Employee__c where AxtriaSalesIQTM__Assignment_Status__c = 'Active' ])
         {
            String key= posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c+'_'+posEmp.Employee_PRID__c+'_'+posEmp.AxtriaSalesIQTM__Assignment_Type__c+'_'+posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__r.Name;
            System.debug('<>>><<<>><>key<><><<>><><'+key);

            if(!poSEmpActiveMap.containsKey(key))
            {
                poSEmpActiveMap.put(key,posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.Name);
            }
         }

         for(AxtriaSalesIQTM__Position_Employee__c posEmp : [Select AxtriaSalesIQTM__Assignment_Status__c,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__Assignment_Type__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,Employee_PRID__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__r.Name,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.Name from AxtriaSalesIQTM__Position_Employee__c where AxtriaSalesIQTM__Assignment_Status__c = 'Inactive' ])
         {
            String key= posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c+'_'+posEmp.Employee_PRID__c+'_'+posEmp.AxtriaSalesIQTM__Assignment_Type__c+'_'+posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__r.Name;
            System.debug('<>>><<<>><>key<><><<>><><'+key);

            if(!poSEmpInactiveMap.containsKey(key))
            {
                poSEmpInactiveMap.put(key,posEmp.AxtriaSalesIQTM__Effective_End_Date__c);
            }
         }

         Date teamInstDate = [Select AxtriaSalesIQTM__IC_EffstartDate__c from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c = 'Current' LIMIT 1].AxtriaSalesIQTM__IC_EffstartDate__c;
         teamInstDate = teamInstDate.addDays(-1);

         System.debug('<><>><<>><><teamInstDate><<>><><><<<'+teamInstDate);

         Map<String,SIQ_Position_Employee_O__c>  posEmpActiveOutBoundMap= new Map<String,SIQ_Position_Employee_O__c>();
         for(SIQ_Position_Employee_O__c posEmp_O : scope)
         {
            if(!poSEmpActiveMap.containsKey(posEmp_O.Unique_Id__c))
            {
                

                if(poSEmpInactiveMap.containsKey(posEmp_O.Unique_Id__c))
                {
                    posEmp_O.SIQ_ASSIGNMENT_END_DATE__c = poSEmpInactiveMap.get(posEmp_O.Unique_Id__c);
                    //posEmp_O.SIQ_Created_Date__c = System.now();
                    posEmp_O.SIQ_Updated_Date__c = System.now();
                    posEmp_O.SIQ_isActive__c = 'Inactive';
                }
                else
                {
                    posEmp_O.SIQ_ASSIGNMENT_END_DATE__c = posEmp_O.SIQ_ASSIGNMENT_START_DATE__c;
                    //posEmp_O.SIQ_Created_Date__c = System.now();
                    posEmp_O.SIQ_Updated_Date__c = System.now();
                    posEmp_O.SIQ_isActive__c = 'Inactive';
                }
                poSEmpUpdOutbound.add(posEmp_O);
            }
            //posEmpActiveOutBound.put(posEmp_O.Unique_Id__c,posEmp_O);
         }

         update poSEmpUpdOutbound;


        
        
    }  
    
    global void finish(Database.BatchableContext BC) {
     
    }
}