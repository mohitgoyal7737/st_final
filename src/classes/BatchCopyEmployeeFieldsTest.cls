@isTest
private class BatchCopyEmployeeFieldsTest {
   @testSetup 
    static void setup() {
        List<CurrentPersonFeed__c> personList = new List<CurrentPersonFeed__c>();
        
        list<string> empIDList = new list<string>();
        Map<string,AxtriaSalesIQTM__Employee__c> empMap       = new Map<string,AxtriaSalesIQTM__Employee__c>();
        List<AxtriaSalesIQTM__Employee__c> empList=TestDataFactoryAZ.createEmployees('x','x','x','x','x',System.today(),5);
        insert empList;
        list<AxtriaSalesIQTM__Employee_Status__c> lsEmpStatus= new list<AxtriaSalesIQTM__Employee_Status__c>();
         for(AxtriaSalesIQTM__Employee__c emp : Database.query('select id, AxtriaSalesIQTM__Employee_ID__c from AxtriaSalesIQTM__Employee__c')){
                empIDList.add(emp.AxtriaSalesIQTM__Employee_ID__c); //Filling empIDList with IDs 
                empMap.put(emp.AxtriaSalesIQTM__Employee_ID__c,emp); //Filling empMap 
                lsEmpStatus.add(new AxtriaSalesIQTM__Employee_Status__c(AxtriaSalesIQTM__Employee__c=emp.id,AxtriaSalesIQTM__effective_start_date__c=system.today(),AxtriaSalesIQTM__effective_end_date__c=Date.newinstance(4000,12,31),AxtriaSalesIQTM__Employee_Status__c='Active'));
            }
        insert lsEmpStatus;    
             // insert 5 accounts
        for (Integer i=0;i<5;i++) {
            personList.add(new CurrentPersonFeed__c(First_Name__c='A '+i,isRejected__c=false,PRID__c='x'+i,Hire_Date__c=system.today(),HR_Status__c = 'Active'));
        
        } 
        
        insert personList;
       /* list<string> feedIds = new list<string>();
       for(CurrentPersonFeed__c feed: [select id from CurrentPersonFeed__c]){
        feedIds.add(feed.id);
      }
      if(feedIds.size()!=0){
        Database.delete(feedIds);
      } */
    }
    static testmethod void test() { 
      Scheduler_Log__c sJob = new Scheduler_Log__c();
    
    sJob.Job_Name__c = 'Employee Feed';
    sJob.Job_Status__c = 'Failed';
    sJob.Job_Type__c='Inbound';
    //sJob.CreatedDate = System.today();
  
    insert sJob;
     map<string,date> mapEvent2Date = new map<string,date>();
     mapEvent2Date.put('Employee New Hire',system.today());
        
      String batchID = sJob.Id;        
        Test.startTest();
        BatchCopyEmployeeFields usa = new BatchCopyEmployeeFields(batchID,mapEvent2Date);
        Id batchId1 = Database.executeBatch(usa);
        Test.stopTest();
        // after the testing stops, assert records were updated 
    }
}