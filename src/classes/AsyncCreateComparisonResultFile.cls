public with sharing class AsyncCreateComparisonResultFile implements Queueable, Database.AllowsCallouts
{
    String mmStatus;
    String ruleName;
    String mmID;
    public AsyncCreateComparisonResultFile(String mmStatus, String ruleName, String mmID)
    {
        try{
        SnTDMLSecurityUtil.printDebugMessage('in constructor mmStatus -- ' + mmStatus);
        SnTDMLSecurityUtil.printDebugMessage('ruleName -- ' + ruleName);
        SnTDMLSecurityUtil.printDebugMessage('mmID -- ' + mmID);
        this.mmStatus = mmStatus;
        this.ruleName = ruleName;
        this.mmID = mmID;
        }
        catch(Exception qe) {
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'AsyncCreateComparisonResultFile',mmID);
            } 
    }
    public void execute(QueueableContext context)
    {
        // Your processing logic here

        // Chain this job to next job by submitting the next job
        //System.enqueueJob(new SecondJob());

        SnTDMLSecurityUtil.printDebugMessage(' in execute mmStatus -- ' + mmStatus);
        SnTDMLSecurityUtil.printDebugMessage('ruleName -- ' + ruleName);
        SnTDMLSecurityUtil.printDebugMessage('mmID -- ' + mmID);
        createComparisonResultFile();

    }
    public void createComparisonResultFile()
    {


        try{
        SnTDMLSecurityUtil.printDebugMessage('Inside Callout ---> ');
        List<AxtriaSalesIQTM__ETL_Config__c> etlConfigList = [Select Id, AxtriaSalesIQTM__End_Point__c, AxtriaSalesIQTM__SF_UserName__c,
                                             AxtriaSalesIQTM__SF_Password__c, AxtriaSalesIQTM__S3_Security_Token__c,
                                             AxtriaSalesIQTM__S3_Bucket__c, AxtriaSalesIQTM__S3_Filename__c, AxtriaSalesIQTM__Org_Type__c
                                             from AxtriaSalesIQTM__ETL_Config__c where Name = 'ComparativeAnalysis' WITH SECURITY_ENFORCED];
        if(etlConfigList.size() > 0)
        {
            AxtriaSalesIQTM__ETL_Config__c etlConfig = etlConfigList[0];
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'BatchFillUniqueCountBUMM' WITH SECURITY_ENFORCED];
            String nameSpacePrefix = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';

            String objectName = nameSpacePrefix + 'Account_Compute_Final__c';
            /*SObjectType objType = Schema.getGlobalDescribe().get(objectName);
            Map<String,Schema.SObjectField> mfields = objType.getDescribe().fields.getMap();*/
            String queryCols = '';
            /*for(String s : mfields.keySet()){
                queryCols += mfields.get(s) + ', ';
            }
            queryCols = queryCols.removeEnd(', ');*/
            //queryCols = 'Id, '+nameSpacePrefix+'Account_Number__c, '+nameSpacePrefix+'Account_Ext_ID__c, '+nameSpacePrefix+'Final_Segment__c, '+nameSpacePrefix+'Final_TCF__c ';

            queryCols = 'Id, ' + nameSpacePrefix + 'Account_Number__c, ' + nameSpacePrefix + 'Account_Ext_ID__c, ' + nameSpacePrefix + 'Final_Segment__c, ';
            queryCols += nameSpacePrefix + 'Final_TCF__c, ' + nameSpacePrefix + 'Account_Name_Formula__c, ' + nameSpacePrefix + 'Measure_Master_Name_Formula__c, ';
            queryCols += nameSpacePrefix + 'Position__c, ' + nameSpacePrefix + 'Physician__r.AxtriaSalesIQTM__SharedWith__c, ' + nameSpacePrefix + 'is_Shared_Formula__c, ';
            queryCols += nameSpacePrefix + 'Assigned_Rep__c, ' + nameSpacePrefix + 'CustonerSegVal__c, ';
            queryCols += nameSpacePrefix + 'OUTPUT_NAME_1__c, ' + nameSpacePrefix + 'OUTPUT_NAME_2__c, ' + nameSpacePrefix + 'OUTPUT_NAME_3__c, ' + nameSpacePrefix + 'OUTPUT_NAME_4__c, ';
            queryCols += nameSpacePrefix + 'OUTPUT_NAME_5__c, ' + nameSpacePrefix + 'OUTPUT_NAME_6__c, ' + nameSpacePrefix + 'OUTPUT_NAME_7__c, ' + nameSpacePrefix + 'OUTPUT_NAME_8__c, ';
            queryCols += nameSpacePrefix + 'OUTPUT_NAME_9__c, ' + nameSpacePrefix + 'OUTPUT_NAME_10__c, ' + nameSpacePrefix + 'OUTPUT_NAME_11__c, ' + nameSpacePrefix + 'OUTPUT_NAME_12__c, ';
            queryCols += nameSpacePrefix + 'OUTPUT_NAME_13__c, ' + nameSpacePrefix + 'OUTPUT_NAME_14__c, ' + nameSpacePrefix + 'OUTPUT_NAME_15__c, ' + nameSpacePrefix + 'OUTPUT_NAME_16__c, ';
            queryCols += nameSpacePrefix + 'OUTPUT_NAME_17__c, ' + nameSpacePrefix + 'OUTPUT_NAME_18__c, ' + nameSpacePrefix + 'OUTPUT_NAME_19__c, ' + nameSpacePrefix + 'OUTPUT_NAME_20__c, ';
            queryCols += nameSpacePrefix + 'OUTPUT_NAME_21__c, ' + nameSpacePrefix + 'OUTPUT_NAME_22__c, ' + nameSpacePrefix + 'OUTPUT_NAME_23__c, ' + nameSpacePrefix + 'OUTPUT_NAME_24__c, ';
            queryCols += nameSpacePrefix + 'OUTPUT_NAME_25__c, ' + nameSpacePrefix + 'OUTPUT_NAME_26__c, ' + nameSpacePrefix + 'OUTPUT_NAME_27__c, ' + nameSpacePrefix + 'OUTPUT_NAME_28__c, ';
            queryCols += nameSpacePrefix + 'OUTPUT_NAME_29__c, ' + nameSpacePrefix + 'OUTPUT_NAME_30__c, ' + nameSpacePrefix + 'OUTPUT_NAME_31__c, ' + nameSpacePrefix + 'OUTPUT_NAME_32__c, ';
            queryCols += nameSpacePrefix + 'OUTPUT_NAME_33__c, ' + nameSpacePrefix + 'OUTPUT_NAME_34__c, ' + nameSpacePrefix + 'OUTPUT_NAME_35__c, ' + nameSpacePrefix + 'OUTPUT_NAME_36__c, ';
            queryCols += nameSpacePrefix + 'OUTPUT_NAME_37__c, ' + nameSpacePrefix + 'OUTPUT_NAME_38__c, ' + nameSpacePrefix + 'OUTPUT_NAME_39__c, ' + nameSpacePrefix + 'OUTPUT_NAME_40__c, ';
            queryCols += nameSpacePrefix + 'OUTPUT_NAME_41__c, ' + nameSpacePrefix + 'OUTPUT_NAME_42__c, ' + nameSpacePrefix + 'OUTPUT_NAME_43__c, ' + nameSpacePrefix + 'OUTPUT_NAME_44__c, ';
            queryCols += nameSpacePrefix + 'OUTPUT_NAME_45__c, ' + nameSpacePrefix + 'OUTPUT_NAME_46__c, ' + nameSpacePrefix + 'OUTPUT_NAME_47__c, ' + nameSpacePrefix + 'OUTPUT_NAME_48__c, ';
            queryCols += nameSpacePrefix + 'OUTPUT_NAME_51__c, ' + nameSpacePrefix + 'OUTPUT_NAME_52__c, ' + nameSpacePrefix + 'OUTPUT_NAME_53__c, ' + nameSpacePrefix + 'OUTPUT_NAME_54__c, '+ + nameSpacePrefix + 'OUTPUT_NAME_55__c, ';
            queryCols += nameSpacePrefix + 'OUTPUT_NAME_49__c, ' + nameSpacePrefix + 'OUTPUT_NAME_50__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_1__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_2__c, ';
            queryCols += nameSpacePrefix + 'OUTPUT_VALUE_3__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_4__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_5__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_6__c, ';
            queryCols += nameSpacePrefix + 'OUTPUT_VALUE_7__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_8__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_9__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_10__c, ';
            queryCols += nameSpacePrefix + 'OUTPUT_VALUE_11__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_12__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_13__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_14__c, ';
            queryCols += nameSpacePrefix + 'OUTPUT_VALUE_15__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_16__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_17__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_18__c, ';
            queryCols += nameSpacePrefix + 'OUTPUT_VALUE_19__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_20__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_21__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_22__c, ';
            queryCols += nameSpacePrefix + 'OUTPUT_VALUE_23__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_24__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_25__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_26__c, ';
            queryCols += nameSpacePrefix + 'OUTPUT_VALUE_27__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_28__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_29__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_30__c, ';
            queryCols += nameSpacePrefix + 'OUTPUT_VALUE_31__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_32__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_33__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_34__c, ';
            queryCols += nameSpacePrefix + 'OUTPUT_VALUE_35__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_36__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_37__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_38__c, ';
            queryCols += nameSpacePrefix + 'OUTPUT_VALUE_39__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_40__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_41__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_42__c, ';
            queryCols += nameSpacePrefix + 'OUTPUT_VALUE_43__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_44__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_45__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_46__c, ';
            queryCols += nameSpacePrefix + 'OUTPUT_VALUE_47__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_48__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_49__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_50__c ,';
             queryCols += nameSpacePrefix + 'OUTPUT_VALUE_51__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_52__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_53__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_54__c, ' + nameSpacePrefix + 'OUTPUT_VALUE_55__c ';
           



            String queryFinal = 'SELECT ' + queryCols + ' FROM ' + objectName + ' WHERE ' + nameSpacePrefix + 'Measure_Master__c = \'' + mmID + '\' AND isDeleted = false';
            String fileName = etlConfig.AxtriaSalesIQTM__S3_Filename__c;
            fileName = fileName.replace('measureId', mmID);
            String endPointURL = etlConfig.AxtriaSalesIQTM__End_Point__c;

            Map<String,String> parameterMap = new Map<String,String>();
            parameterMap.put('fileNameSource',fileName);
            parameterMap.put('fileNameDestination',fileName);
            parameterMap.put('sfdcUserName',etlConfig.AxtriaSalesIQTM__SF_UserName__c);
            parameterMap.put('sfdcPassword',etlConfig.AxtriaSalesIQTM__SF_Password__c);
            parameterMap.put('rule_status',mmStatus);
            parameterMap.put('userId',UserInfo.getUserId());
            parameterMap.put('ruleName',ruleName);
            parameterMap.put('execution_state',mmStatus);
            parameterMap.put('isAttachmentUpload','Upload');
            parameterMap.put('sfdcSecurityToken',etlConfig.AxtriaSalesIQTM__S3_Security_Token__c);
            parameterMap.put('orgType',etlConfig.AxtriaSalesIQTM__Org_Type__c);
            parameterMap.put('sourceRuleId',mmID);
            parameterMap.put('destinationRuleId','null');
            parameterMap.put('query',queryFinal);
            parameterMap.put('namespace',nameSpacePrefix);
            parameterMap.put('objectName',objectName);
            parameterMap.put('action','Fetch');
            parameterMap.put('segUniv','Survey Data Accounts');

            //String postRequestBody  = 'fileNameSource=' + fileName + '&fileNameDestination=' + fileName;
            //postRequestBody += '&sfdcUserName=' + etlConfig.AxtriaSalesIQTM__SF_UserName__c + '&sfdcPassword=' + etlConfig.AxtriaSalesIQTM__SF_Password__c;
            //postRequestBody += '&rule_status=' + mmStatus + '&userId=' + UserInfo.getUserId() + '&ruleName=' + ruleName + '&execution_state=' + mmStatus + '&isAttachmentUpload=Upload';
            //postRequestBody += '&sfdcSecurityToken=' + etlConfig.AxtriaSalesIQTM__S3_Security_Token__c + '&orgType=' + etlConfig.AxtriaSalesIQTM__Org_Type__c + '&sourceRuleId=' + mmID + '&destinationRuleId=null' + '&query=' + queryFinal;
            //postRequestBody += '&namespace=' + nameSpacePrefix + '&objectName=' + objectName + '&action=Fetch';
            SnTDMLSecurityUtil.printDebugMessage('endPointURL1 ==== ' + endPointURL);
            String postRequestBody = JSON.Serialize(parameterMap);
            SnTDMLSecurityUtil.printDebugMessage('postRequestBody ==== ' + postRequestBody);
            endPointURL = endPointURL.replaceAll( '\\s+', '%20');
            endPointURL = endPointURL.replaceAll('\'', '%27');
            SnTDMLSecurityUtil.printDebugMessage('endPointURL 222==== ' + endPointURL);

            Http h = new Http();
            HttpRequest request = new HttpRequest();
            HttpResponse response = new HttpResponse();
            String errorMessage = '';
            try
            {

                request.setEndPoint(endPointURL);
                request.setHeader('Content-type', 'application/json');
                request.setMethod('POST');
                request.setbody(postRequestBody);
                request.setTimeout(120000);
                response = h.send(request);
                SnTDMLSecurityUtil.printDebugMessage('response body ---> ' + response.getBody());
                if(response.getBody() != 'OK')
                {
                    errorMessage = response.getBody();
                }
            }
            catch(Exception e)
            {
                SnTDMLSecurityUtil.printDebugMessage('Error --> ' + e.getStackTraceString());
                SnTDMLSecurityUtil.printDebugMessage('getCause --> ' + e.getCause());
                SnTDMLSecurityUtil.printDebugMessage('getLineNumber --> ' + e.getLineNumber());
                SnTDMLSecurityUtil.printDebugMessage('getMessage --> ' + e.getMessage());
                SnTDMLSecurityUtil.printDebugMessage('getTypeName --> ' + e.getTypeName());

                if(e.getTypeName() == 'System.CalloutException')
                {
                    if(e.getMessage() != 'Read timed out')
                    {
                        errorMessage = e.getMessage();
                    }
                }


                else
                {
                    errorMessage = e.getStackTraceString();
                }
                     SalesIQSnTLogger.createUnHandledErrorLogs(e, SalesIQSnTLogger.BR_MODULE, 'AsyncCreateComparisonResultFile',mmID);
            }
            if(errorMessage.length() > 130000)
            {
                errorMessage = errorMessage.substring(0, 129998);
            }
            SnTDMLSecurityUtil.printDebugMessage('response code ---> ' + response.getStatusCode());
            if(String.isNotBlank(errorMessage))
            {

                ST_utility.updateRuleStatus( mmID, SalesIQGlobalConstants.RULE_EXECUTION_FAILED, errorMessage, System.Label.Python_Service_Error, false);
            }


        }
        //SNT-516
        else
        {

            ST_utility.updateRuleStatus( mmID, SalesIQGlobalConstants.RULE_EXECUTION_FAILED, System.Label.ETL_Config_Comparative_Analysis_Missing, '', false);
            }
        }
         catch(Exception qe) {
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'AsyncCreateComparisonResultFile',mmID);
            } 

    }
}