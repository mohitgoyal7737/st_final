/*Author - Himanshu Tariyal(A0994)
Date : 7th January 2018*/
@isTest
private class SegmentSimulationCtlrTest 
{
    private static testMethod void firstTest()
    {
        System.test.startTest();
        /*Create initial test data for all the objs reqd.*/
        AxtriaSalesIQTM__Organization_Master__c aom = new AxtriaSalesIQTM__Organization_Master__c(AxtriaSalesIQTM__Org_Level__c='Global',AxtriaSalesIQTM__Parent_Country_Level__c=true);
        insert aom;
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='Italy',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active');
        insert country;
        
        /*Cycle__c cycle = new Cycle__c(Name='Test Cycle',Country__c = country.id);
        insert cycle;*/
        
        Account acc = new Account(Name='test acc',Marketing_Code__c='EU');
        insert acc;
        
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='Test Team');
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU',AxtriaSalesIQTM__Team__c=team.id/*,Cycle__c=cycle.id*/);
        insert ti;
        
        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c(Name='Pos',AxtriaSalesIQTM__Team_iD__c=team.id);
        insert pos;

        Product_Catalog__c pc = new Product_Catalog__c(Name='Test Product',Team_Instance__c=ti.id,Veeva_External_ID__c = 'ProdId',Product_Code__c= 'ProdId',IsActive__c=true,Country_Lookup__c=country.id);
        insert pc;
        
        /*Brand_Team_Instance__c bti = new Brand_Team_Instance__c(Brand__c=pc.id,Team_Instance__c=ti.id);
        insert bti;*/
        
        Measure_Master__c mm = new Measure_Master__c(Team_Instance__c=ti.id,Brand_Lookup__c=pc.id,State__c='Executed'/*,Cycle__c=cycle.id*/);
        mm.Number_of_Unique_Accounts__c = 215;
        insert mm;
        
        AxtriaSalesIQTM__Position_Account__c pa = new AxtriaSalesIQTM__Position_Account__c(AxtriaSalesIQTM__Team_Instance__c=ti.id,AxtriaSalesIQTM__Account__c=acc.id);
        insert pa;
        
        BU_Response__c bur = new BU_Response__c(Team_Instance__c=ti.id,Position_Account__c=pa.id,Product__c=pc.id);
        insert bur;
        
        Grid_Master__c matrix = new Grid_Master__c(Name='Test Matrix',Grid_Type__c='2D',Dimension_1_Name__c='Dim1',Dimension_2_Name__c='Dim2',Country__c=country.id);
        insert matrix;
        
        Step__c testStep = new Step__c(Name='Dim1');
        insert testStep;
        
        Rule_Parameter__c rp1 = new Rule_Parameter__c(Step__c=testStep.id,Measure_Master__c=mm.id,CustomerSegmentedField__c=true);
        insert rp1;
        
        Rule_Parameter__c rp2 = new Rule_Parameter__c(Step__c=testStep.id,Measure_Master__c=mm.id,CustomerSegmentedField__c=true);
        insert rp2;
        
        Grid_Details__c gd1 = new Grid_Details__c(Name='2_2', Dimension_1_Value__c='Dim11 Value', Dimension_2_Value__c='Dim21 Value', Output_Value__c='A', Grid_Master__c=matrix.id);
        insert gd1;
        
        Grid_Details__c gd2 = new Grid_Details__c(Name='3_3', Dimension_1_Value__c='Dim1 Value', Dimension_2_Value__c='Dim2 Value', Output_Value__c='B', Grid_Master__c=matrix.id);
        insert gd2;
        
        Grid_Details__c gd3 = new Grid_Details__c(Name='4_4', Dimension_1_Value__c='Dim111 Value', Dimension_2_Value__c='Dim211 Value', Output_Value__c='C', Grid_Master__c=matrix.id);
        insert gd3;
        
        Grid_Details__c gd4 = new Grid_Details__c(Name='5_5', Dimension_1_Value__c='Dim122 Value', Dimension_2_Value__c='Dim222 Value', Output_Value__c='D', Grid_Master__c=matrix.id);
        insert gd4;
        
        Step__c matrixStep = new Step__c(UI_Location__c = 'Compute Segment',Name='Dim1',Matrix__c=matrix.id,Measure_Master__c=mm.id,Step_Type__c='Matrix',Grid_Param_1__c=rp1.id,Grid_Param_2__c=rp2.id);
        insert matrixStep; 
        
        /*Account_Compute_Final_Copy__c acfc = new Account_Compute_Final_Copy__c(Measure_Master__c=mm.id,Output_Name_1__c='Dim1');
        insert acfc;*/
        
        Segment_Simulation__c ssc = new Segment_Simulation__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim1 Value',Dimension2__c='Dim2 Value',
            Measure_Master__c=mm.id,Name='ss',Segment__c='A',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
            Sum_Proposed_TCF__c=20,Sum_TCF__c=20);
        
        Segment_Simulation__c ssc2 = new Segment_Simulation__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim11 Value',Dimension2__c='Dim21 Value',
            Measure_Master__c=mm.id,Name='ss',Segment__c='B',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
            Sum_Proposed_TCF__c=20,Sum_TCF__c=20);
        
        Segment_Simulation__c ssc3 = new Segment_Simulation__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim111 Value',Dimension2__c='Dim211 Value',
            Measure_Master__c=mm.id,Name='ss',Segment__c='C',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
            Sum_Proposed_TCF__c=20,Sum_TCF__c=20);
        
        Segment_Simulation__c ssc4 = new Segment_Simulation__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim122 Value',Dimension2__c='Dim222 Value',
            Measure_Master__c=mm.id,Name='ss',Segment__c='D',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
            Sum_Proposed_TCF__c=20,Sum_TCF__c=20);
        insert ssc;
        insert ssc2;
        insert ssc3;
        insert ssc4;
        /*Test Data ends here*/
        
       // /*Create an instance of the class*/
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        PageReference myVfPage = Page.SegmentSimulation;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('rid', mm.id);
        SegmentSimulationCtlr ss = new SegmentSimulationCtlr();
        ss.countryID = country.id;
       // ss.cycleSelected = cycle.id;
        ss.selectedBu = ti.id;
        ss.selectedBrand = pc.id;
        ss.selectedRule = mm.id;
        ss.fillCycleOptions();
        ss.fillBusinessUnitOptions();
        ss.fillBrandOptions();
        ss.fillRuleOptions();
        
        ss.populateMatrixData();
        ss.dummyFn();
        
        ss.selectedTerr = pos.Name;
        ss.createChartData();
        
        ss.selectedDimension = 'D';
        ss.chnagedGridValue = '2_2';
        ss.changeMatrixData();
    //  ss.changeIntermediateMatrixData();
        
        ss.pushToBusinessRules();
        
        ss.redirectToPage();
        ss.selectedSimulation = 'Segment Simulation';
        ss.redirectToPage();
        ss.selectedSimulation = 'Workload Simulation';
        ss.redirectToPage();
        ss.selectedSimulation = 'Modelling';
        ss.redirectToPage();
        ss.selectedSimulation = 'Quantile Analysis';
        ss.redirectToPage();
        ss.selectedSimulation = 'Comparative Analysis';
        ss.redirectToPage();
        //ss.populateMatrixDataCustSeg();
        ss.copyMatrix(matrix.id);
       // ss.copySegSimulation(mm.id);
       // ss.copySSegSimulationCopy(mm.id);
       // ss.fillIntermediateData();
        /*All the methods have been tested on the Ctlr*/
        System.test.stopTest();
    }
    
    private static testMethod void secondTest()
    {
        System.test.startTest();
        /*Create initial test data for all the objs reqd.*/
        AxtriaSalesIQTM__Organization_Master__c aom = new AxtriaSalesIQTM__Organization_Master__c(AxtriaSalesIQTM__Org_Level__c='Global',AxtriaSalesIQTM__Parent_Country_Level__c=true);
        insert aom;
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='Italy',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active');
        insert country;
        
        /*Cycle__c cycle = new Cycle__c(Name='Test Cycle',Country__c = country.id);
        insert cycle;*/
        
        Account acc = new Account(Name='test acc',Marketing_Code__c='EU');
        insert acc;
        
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='Test Team');
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU',AxtriaSalesIQTM__Team__c=team.id/*,Cycle__c=cycle.id*/);
        insert ti;
        
        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c(Name='Pos',AxtriaSalesIQTM__Team_iD__c=team.id);
        insert pos;

        Product_Catalog__c pc = new Product_Catalog__c(Name='Test Product',Team_Instance__c=ti.id,Veeva_External_ID__c = 'ProdId',Product_Code__c= 'ProdId',IsActive__c=true,Country_Lookup__c=country.id);
        insert pc;
        
        /*Brand_Team_Instance__c bti = new Brand_Team_Instance__c(Brand__c=pc.id,Team_Instance__c=ti.id);
        insert bti;*/
        
        Measure_Master__c mm = new Measure_Master__c(Team_Instance__c=ti.id,Brand_Lookup__c=pc.id,State__c='Executed'/*,Cycle__c=cycle.id*/);
        insert mm;
        
        AxtriaSalesIQTM__Position_Account__c pa = new AxtriaSalesIQTM__Position_Account__c(AxtriaSalesIQTM__Team_Instance__c=ti.id,AxtriaSalesIQTM__Account__c=acc.id);
        insert pa;
        
        BU_Response__c bur = new BU_Response__c(Team_Instance__c=ti.id,Position_Account__c=pa.id,Product__c=pc.id);
        insert bur;
        
        Grid_Master__c matrix = new Grid_Master__c(Name='Test Matrix',Grid_Type__c='2D',Dimension_1_Name__c='Dim1',Dimension_2_Name__c='Dim2',Country__c=country.id);
        insert matrix;
        
        Step__c testStep = new Step__c(Name='Dim1');
        insert testStep;
        
        Rule_Parameter__c rp1 = new Rule_Parameter__c(Step__c=testStep.id,Measure_Master__c=mm.id);
        insert rp1;
        
        Rule_Parameter__c rp2 = new Rule_Parameter__c(Step__c=testStep.id,Measure_Master__c=mm.id);
        insert rp2;
        
        Grid_Details__c gd1 = new Grid_Details__c(Name='2_2', Dimension_1_Value__c='Dim11 Value', Dimension_2_Value__c='Dim21 Value', Output_Value__c='A', Grid_Master__c=matrix.id);
        insert gd1;
        
        Grid_Details__c gd2 = new Grid_Details__c(Name='3_3', Dimension_1_Value__c='Dim1 Value', Dimension_2_Value__c='Dim2 Value', Output_Value__c='B', Grid_Master__c=matrix.id);
        insert gd2;
        
        Grid_Details__c gd3 = new Grid_Details__c(Name='4_4', Dimension_1_Value__c='Dim111 Value', Dimension_2_Value__c='Dim211 Value', Output_Value__c='C', Grid_Master__c=matrix.id);
        insert gd3;
        
        Grid_Details__c gd4 = new Grid_Details__c(Name='5_5', Dimension_1_Value__c='Dim122 Value', Dimension_2_Value__c='Dim222 Value', Output_Value__c='D', Grid_Master__c=matrix.id);
        insert gd4;
        
        Step__c matrixStep = new Step__c(UI_Location__c = 'Compute Segment',Name='Dim1',Matrix__c=matrix.id,Measure_Master__c=mm.id,Step_Type__c='Matrix',Grid_Param_1__c=rp1.id,Grid_Param_2__c=rp2.id);
        insert matrixStep; 
        
        /*Account_Compute_Final_Copy__c acfc = new Account_Compute_Final_Copy__c(Measure_Master__c=mm.id);
        insert acfc;*/
        
        Segment_Simulation__c ssc = new Segment_Simulation__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim1 Value',Dimension2__c='Dim2 Value',
            Measure_Master__c=mm.id,Name='ss',Segment__c='A',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
            Sum_Proposed_TCF__c=20,Sum_TCF__c=20);
        
        Segment_Simulation__c ssc2 = new Segment_Simulation__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim11 Value',Dimension2__c='Dim21 Value',
            Measure_Master__c=mm.id,Name='ss',Segment__c='B',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
            Sum_Proposed_TCF__c=20,Sum_TCF__c=20);
        
        Segment_Simulation__c ssc3 = new Segment_Simulation__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim111 Value',Dimension2__c='Dim211 Value',
            Measure_Master__c=mm.id,Name='ss',Segment__c='C',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
            Sum_Proposed_TCF__c=20,Sum_TCF__c=20);
        
        Segment_Simulation__c ssc4 = new Segment_Simulation__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim122 Value',Dimension2__c='Dim222 Value',
            Measure_Master__c=mm.id,Name='ss',Segment__c='D',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
            Sum_Proposed_TCF__c=20,Sum_TCF__c=20);
        insert ssc;
        insert ssc2;
        insert ssc3;
        insert ssc4;
        /*Test Data ends here*/
        
       // /*Create an instance of the class*/
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        PageReference myVfPage = Page.SegmentSimulation;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('rid', mm.id);
        SegmentSimulationCtlr ss = new SegmentSimulationCtlr();
        ss.countryID = country.id;
      //ss.cycleSelected = cycle.id;
        ss.selectedBu = ti.id;
        ss.selectedBrand = pc.id;
       // ss.selectedRule = mm.id;
        ss.fillCycleOptions();
        ss.fillBusinessUnitOptions();
        ss.fillBrandOptions();
        ss.fillRuleOptions();
        ss.actionMethod();
        ss.fillRuleCustSegOptions();
        ss.populateMatrixData();
        ss.populateMatrixDataCustSeg();
        ss.copyMatrix(matrix.id);
        ss.copySegSimulation(mm.id);
        ss.copySSegSimulationCopy(mm.id);
        ss.switchToBusinessRules();
        ss.dummyFn();
       // ss.fillIntermediateData();
        /*All the methods have been tested on the Ctlr*/
        System.test.stopTest();
    }
    private static testMethod void thirdTest()
    {
        System.test.startTest();
        /*Create initial test data for all the objs reqd.*/
        AxtriaSalesIQTM__Organization_Master__c aom = new AxtriaSalesIQTM__Organization_Master__c(AxtriaSalesIQTM__Org_Level__c='Global',AxtriaSalesIQTM__Parent_Country_Level__c=true);
        insert aom;
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='Italy',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active');
        insert country;
        
        /*Cycle__c cycle = new Cycle__c(Name='Test Cycle',Country__c = country.id);
        insert cycle;*/
        
        Account acc = new Account(Name='test acc',Marketing_Code__c='EU');
        insert acc;
        
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='Test Team');
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU',AxtriaSalesIQTM__Team__c=team.id/*,Cycle__c=cycle.id*/);
        insert ti;
        
        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c(Name='Pos',AxtriaSalesIQTM__Team_iD__c=team.id);
        insert pos;

        Product_Catalog__c pc = new Product_Catalog__c(Name='Test Product',Team_Instance__c=ti.id,Veeva_External_ID__c = 'ProdId',Product_Code__c= 'ProdId',IsActive__c=true,Country_Lookup__c=country.id);
        insert pc;
        
        /*Brand_Team_Instance__c bti = new Brand_Team_Instance__c(Brand__c=pc.id,Team_Instance__c=ti.id);
        insert bti;*/
        
        Measure_Master__c mm = new Measure_Master__c(Team_Instance__c=ti.id,Brand_Lookup__c=pc.id,State__c='Executed'/*,Cycle__c=cycle.id*/);
        insert mm;
        
        AxtriaSalesIQTM__Position_Account__c pa = new AxtriaSalesIQTM__Position_Account__c(AxtriaSalesIQTM__Team_Instance__c=ti.id,AxtriaSalesIQTM__Account__c=acc.id);
        insert pa;
        
        BU_Response__c bur = new BU_Response__c(Team_Instance__c=ti.id,Position_Account__c=pa.id,Product__c=pc.id);
        insert bur;
        
        Grid_Master__c matrix = new Grid_Master__c(Name='Test Matrix',Grid_Type__c='2D',Dimension_1_Name__c='Dim1',Dimension_2_Name__c='Dim2',Country__c=country.id);
        insert matrix;
        
        Step__c testStep = new Step__c(Name='Dim1');
        insert testStep;
        
        Rule_Parameter__c rp1 = new Rule_Parameter__c(Step__c=testStep.id,Measure_Master__c=mm.id);
        insert rp1;
        
        Rule_Parameter__c rp2 = new Rule_Parameter__c(Step__c=testStep.id,Measure_Master__c=mm.id);
        insert rp2;
        
        Grid_Details__c gd1 = new Grid_Details__c(Name='2_2', Dimension_1_Value__c='Dim11 Value', Dimension_2_Value__c='Dim21 Value', Output_Value__c='A', Grid_Master__c=matrix.id);
        insert gd1;
        
        Grid_Details__c gd2 = new Grid_Details__c(Name='3_3', Dimension_1_Value__c='Dim1 Value', Dimension_2_Value__c='Dim2 Value', Output_Value__c='B', Grid_Master__c=matrix.id);
        insert gd2;
        
        Grid_Details__c gd3 = new Grid_Details__c(Name='4_4', Dimension_1_Value__c='Dim111 Value', Dimension_2_Value__c='Dim211 Value', Output_Value__c='C', Grid_Master__c=matrix.id);
        insert gd3;
        
        Grid_Details__c gd4 = new Grid_Details__c(Name='5_5', Dimension_1_Value__c='Dim122 Value', Dimension_2_Value__c='Dim222 Value', Output_Value__c='D', Grid_Master__c=matrix.id);
        insert gd4;
        
        Step__c matrixStep = new Step__c(UI_Location__c = 'Compute Segment',Name='Dim1',Matrix__c=matrix.id,Measure_Master__c=mm.id,Step_Type__c='Matrix',Grid_Param_1__c=rp1.id,Grid_Param_2__c=rp2.id);
        insert matrixStep; 
        
        /*Account_Compute_Final_Copy__c acfc = new Account_Compute_Final_Copy__c(Measure_Master__c=mm.id);
        insert acfc;*/
        
        Segment_Simulation__c ssc = new Segment_Simulation__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim1 Value',Dimension2__c='Dim2 Value',
            Measure_Master__c=mm.id,Name='ss',Segment__c='A',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
            Sum_Proposed_TCF__c=20,Sum_TCF__c=20);
        
        Segment_Simulation__c ssc2 = new Segment_Simulation__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim11 Value',Dimension2__c='Dim21 Value',
            Measure_Master__c=mm.id,Name='ss',Segment__c='B',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
            Sum_Proposed_TCF__c=20,Sum_TCF__c=20);
        
        Segment_Simulation__c ssc3 = new Segment_Simulation__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim111 Value',Dimension2__c='Dim211 Value',
            Measure_Master__c=mm.id,Name='ss',Segment__c='C',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
            Sum_Proposed_TCF__c=20,Sum_TCF__c=20);
        
        Segment_Simulation__c ssc4 = new Segment_Simulation__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim122 Value',Dimension2__c='Dim222 Value',
            Measure_Master__c=mm.id,Name='ss',Segment__c='D',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
            Sum_Proposed_TCF__c=20,Sum_TCF__c=20);
        insert ssc;
        insert ssc2;
        insert ssc3;
        insert ssc4;
        /*Test Data ends here*/
        
       // /*Create an instance of the class*/
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        /*PageReference myVfPage = Page.SegmentSimulation;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('rid', mm.id);*/
        SegmentSimulationCtlr ss = new SegmentSimulationCtlr();
        ss.countryID = country.id;
      //ss.cycleSelected = cycle.id;
        ss.selectedBu = ti.id;
        ss.selectedBrand = pc.id;
       ss.selectedRule = mm.id;
        ss.fillCycleOptions();
        ss.fillBusinessUnitOptions();
        ss.fillBrandOptions();
        ss.fillRuleOptions();
        ss.actionMethod();
        ss.fillRuleCustSegOptions();
        ss.populateMatrixData();
        //ss.populateMatrixDataCustSeg();
        ss.copyMatrix(matrix.id);
        ss.copySegSimulation(mm.id);
        ss.copySSegSimulationCopy(mm.id);
        ss.switchToBusinessRules();
        //ss.fillIntermediateData();
        ss.dummyFn();
        /*All the methods have been tested on the Ctlr*/
        System.test.stopTest();
    }
}