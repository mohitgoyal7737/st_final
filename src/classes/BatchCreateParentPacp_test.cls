/*
@author - Prince Richard Augustin (A2661)
@description - Test class to test the BatchCreateParentPacp.
*/

@isTest
private class BatchCreateParentPacp_test {
    
    static testMethod void testMethod2() {
        
        String className = 'BatchCreateParentPacp_test';
        
        User loggedInUser = new User(id=UserInfo.getUserId());
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);
        
        Account acc = TestDataFactory.createAccount();
        acc.Profile_Consent__c='yes';
        acc.AxtriaSalesIQTM__Speciality__c ='testspecs';
        SnTDMLSecurityUtil.insertRecords(acc,className);
        
        Account acc2 = TestDataFactory.createAccount();
        acc2.Profile_Consent__c='yes';
        acc2.AxtriaSalesIQTM__Speciality__c ='testspecs';
        acc2.name = 'testacc2';
        SnTDMLSecurityUtil.insertRecords(acc2,className);
        
        SnTDMLSecurityUtil.insertRecords(new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
        new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'CallPlanSummaryTrigger')},className);
        
        AxtriaSalesIQTM__Team_Instance__c teamInstance = TestDataFactory.createTeamInstance(team);
        teamInstance.AxtriaSalesIQTM__Team__c = team.id;
        SnTDMLSecurityUtil.insertRecords(teamInstance,className);
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team,teamInstance,countr);
        pcc.Country_Lookup__c = countr.id;
        pcc.team_instance__c = teaminstance.id;
        SnTDMLSecurityUtil.insertRecords(pcc,className);
        
        Measure_Master__c measureMaster= TestDataFactory.createMeasureMaster(pcc,team,teamInstance);
        measureMaster.Team__c = team.id;
        measuremaster.Brand_Lookup__c = pcc.Id;
        measureMaster.Team_Instance__c = teamInstance.id;
        SnTDMLSecurityUtil.insertRecords(measureMaster,className);
        
        SnTDMLSecurityUtil.insertRecords(new MetaData_Definition__c (Display_Name__c='ACCESSIBILITY',Source_Field__c='Accessibility_Range__c',Source_Object__c='BU_Response__c',Team_Instance__c=teamInstance.id,Product_Catalog__c=pcc.Id),className);
        
        Account_Compute_Final__c acf = new Account_Compute_Final__c();
        acf.Measure_Master__c = measureMaster.Id;
        acf.Physician_2__c=acc2.id;
        for(integer i=1;i<=50;i++){
            acf.put('Output_Name_'+i+'__c',i+'');
        }
        SnTDMLSecurityUtil.insertRecords(acf,className);
        
        Grid_Master__c gridMaster = TestDataFactory.gridMaster(countr);
        gridMaster.Country__c = countr.id;
        gridMaster.Grid_Type__c = '2D';
        SnTDMLSecurityUtil.insertRecords(gridMaster,className);
        
        Grid_Master__c gridMaster2 = TestDataFactory.gridMaster(countr);
        gridMaster2.Country__c = countr.id;
        gridMaster2.name = 'test222';
        gridMaster2.Grid_Type__c = '1D';
        SnTDMLSecurityUtil.insertRecords(gridMaster2,className);
        
        //Grid_Details__c gridDetail = TestDataFactory.gridDetails();
        //gridDetail.Grid_Master__c = gridMaster.id;
        //insert gridDetail;
        
        List<Grid_Details__c> NDValueDetails = new List<Grid_Details__c>();
        Grid_Details__c gDetails = new Grid_Details__c();
        gDetails.Name = 'abcd';
        gDetails.CurrencyIsoCode = 'USD';
        gDetails.Output_Value__c='test';
        gDetails.Dimension_1_Value__c='ND';
        gDetails.Dimension_2_Value__c='test';
        gDetails.Grid_Master__c = gridMaster.id;
        NDValueDetails.add(gDetails);
        SnTDMLSecurityUtil.insertRecords(NDValueDetails,className);
        
        Parameter__c parameters = TestDataFactory.parameter(pcc,team,teamInstance);
        parameters.Team__c = team.id; 
        parameters.Name__c= '1';
        parameters.Team_Instance__c = teamInstance.id;
        SnTDMLSecurityUtil.insertRecords(parameters,className);
        
        Parameter__c parameter = TestDataFactory.parameter(pcc,team,teamInstance);
        parameter.Team__c = team.id; 
        parameter.Name__c= '2';
        parameter.Team_Instance__c = teamInstance.id;
        SnTDMLSecurityUtil.insertRecords(parameter,className);        
        
        Step__c ss1 = new Step__c();
        ss1.Matrix__c=gridMaster.id;
        //ss.Grid_Param_1__c = rps.id;
        //ss.Grid_Param_2__c = rpss.id;
        ss1.Step_Type__c ='Matrix';
        ss1.UI_Location__c = 'Compute Segment';
        ss1.Measure_Master__c=measureMaster.Id;
        SnTDMLSecurityUtil.insertRecords(ss1,className);
        
        Rule_Parameter__c rpss = testDataFactory.ruleParameter(measureMaster,parameters,ss1);
        rpss.parameter__c = parameters.id;
        SnTDMLSecurityUtil.insertRecords(rpss,className);
        
        Rule_Parameter__c rps = testDataFactory.ruleParameter(measureMaster,parameters,ss1);
        rps.parameter__c = parameter.id;
        SnTDMLSecurityUtil.insertRecords(rps,className);
        
        
        Compute_Master__c ccMaster = TestDataFactory.createComputeMaster(rps);
        SnTDMLSecurityUtil.insertRecords(ccMaster,className);
        
        Step__c ss = TestDataFactory.step(ccMaster,gridMaster,measureMaster,rps);
        ss.Matrix__c=gridMaster.id;
        ss.Grid_Param_1__c = rps.id;
        ss.Grid_Param_2__c = rpss.id;
        ss.Step_Type__c ='Matrix';
        ss.UI_Location__c = 'Compute Segment';
        ss.Measure_Master__c=measureMaster.Id;
        SnTDMLSecurityUtil.insertRecords(ss,className);
        
        Step__c ss2 = TestDataFactory.step(ccMaster,gridMaster2,measureMaster,rps);
        ss2.Matrix__c=gridMaster2.id;
        ss2.Grid_Param_1__c = rps.id;
        ss2.Grid_Param_2__c = rpss.id;
        ss2.Step_Type__c ='Matrix';
        ss2.UI_Location__c = 'Compute TCF';
        ss2.Measure_Master__c=measureMaster.Id;
        SnTDMLSecurityUtil.insertRecords(ss2,className);
        
        AxtriaSalesIQTM__Position__c pos = TestDataFactory.createPosition(team,teamInstance);
        SnTDMLSecurityUtil.insertRecords(pos,className);
        
        AxtriaSalesIQTM__Position_Product__c positionproduct = TestDataFactory.createPositionProduct(teamInstance,pos,pcc);
        SnTDMLSecurityUtil.insertRecords(positionproduct,className);
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamInstance);
        SnTDMLSecurityUtil.insertRecords(posAccount,className);
        
        Product_Priority__c pp = new Product_Priority__c();
        pp.CurrencyIsoCode = 'USD';
        pp.Product__c = pcc.id;
        pp.Speciality_ID__c='testSpecs';
        pp.priority__c='P2';
        SnTDMLSecurityUtil.insertRecords(pp,className);
        
        List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> listpositionaccountcallplan = new List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionacc = TestDataFactory.createPositionAccountCallPlan(measureMaster,acc,teamInstance,posAccount,pp,pos);
        positionacc.AxtriaSalesIQTM__Account__c= acc.id;
        positionacc.AxtriaSalesIQTM__Team_Instance__c =teamInstance.id;
        positionacc.p1__c='test';
        listpositionaccountcallplan.add(positionacc);
        SnTDMLSecurityUtil.insertRecords(listpositionaccountcallplan,className);
        
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            BatchCreateParentPacp obj=new BatchCreateParentPacp(teaminstance.id);
            
            obj.query = 'Select id,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c!=null and Parent_PACP__c =null ';
            Database.executeBatch(obj);
            
            //To test constructor functions
            BatchCreateParentPacp obj1=new BatchCreateParentPacp(teaminstance.id,'', nameSpace);
            BatchCreateParentPacp obj2=new BatchCreateParentPacp(teaminstance.id,'', false);
            BatchCreateParentPacp obj3=new BatchCreateParentPacp();
        }
        
        Test.stopTest();
        
    }
    
    static testMethod void testMethod3() {
        String className = 'BatchCreateParentPacp_test';
        User loggedInUser = new User(id=UserInfo.getUserId());
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);
        
        Account acc = TestDataFactory.createAccount();
        acc.Profile_Consent__c='yes';
        acc.AxtriaSalesIQTM__Speciality__c ='testspecs';
        SnTDMLSecurityUtil.insertRecords(acc,className);
        
        Account acc2 = TestDataFactory.createAccount();
        acc2.Profile_Consent__c='yes';
        acc2.AxtriaSalesIQTM__Speciality__c ='testspecs';
        acc2.name = 'testacc2';
        SnTDMLSecurityUtil.insertRecords(acc2,className);
        
        SnTDMLSecurityUtil.insertRecords(new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
        new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'CallPlanSummaryTrigger')}, className);
        
        AxtriaSalesIQTM__Team_Instance__c teamInstance = TestDataFactory.createTeamInstance(team);
        teamInstance.AxtriaSalesIQTM__Team__c = team.id;
        SnTDMLSecurityUtil.insertRecords(teamInstance,className);
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team,teamInstance,countr);
        pcc.Country_Lookup__c = countr.id;
        pcc.team_instance__c = teaminstance.id;
        SnTDMLSecurityUtil.insertRecords(pcc,className);
        
        Measure_Master__c measureMaster= TestDataFactory.createMeasureMaster(pcc,team,teamInstance);
        measureMaster.Team__c = team.id;
        measuremaster.Brand_Lookup__c = pcc.Id;
        measureMaster.Team_Instance__c = teamInstance.id;
        SnTDMLSecurityUtil.insertRecords(measureMaster,className);
        
        SnTDMLSecurityUtil.insertRecords(new MetaData_Definition__c (Display_Name__c='ACCESSIBILITY',Source_Field__c='Accessibility_Range__c',Source_Object__c='BU_Response__c',Team_Instance__c=teamInstance.id,Product_Catalog__c=pcc.Id),className);
        
        Account_Compute_Final__c acf = new Account_Compute_Final__c();
        acf.Measure_Master__c = measureMaster.Id;
        acf.Physician_2__c=acc2.id;
        for(integer i=1;i<=50;i++){
            acf.put('Output_Name_'+i+'__c',i+'');
        }
        SnTDMLSecurityUtil.insertRecords(acf,className);
        
        Grid_Master__c gridMaster = TestDataFactory.gridMaster(countr);
        gridMaster.Country__c = countr.id;
        gridMaster.Grid_Type__c = '2D';
        SnTDMLSecurityUtil.insertRecords(gridMaster,className);
        
        Grid_Master__c gridMaster2 = TestDataFactory.gridMaster(countr);
        gridMaster2.Country__c = countr.id;
        gridMaster2.name = 'test222';
        gridMaster2.Grid_Type__c = '1D';
        SnTDMLSecurityUtil.insertRecords(gridMaster2,className);
        
        
        //Grid_Details__c gridDetail = TestDataFactory.gridDetails();
        //gridDetail.Grid_Master__c = gridMaster.id;
        //insert gridDetail;
        
        List<Grid_Details__c> NDValueDetails = new List<Grid_Details__c>();
        Grid_Details__c gDetails = new Grid_Details__c();
        gDetails.Name = 'abcd';
        gDetails.CurrencyIsoCode = 'USD';
        gDetails.Output_Value__c='test';
        gDetails.Dimension_1_Value__c='ND';
        gDetails.Dimension_2_Value__c='test';
        gDetails.Grid_Master__c = gridMaster.id;
        NDValueDetails.add(gDetails);
        SnTDMLSecurityUtil.insertRecords(NDValueDetails,className);
        
        Parameter__c parameters = TestDataFactory.parameter(pcc,team,teamInstance);
        parameters.Team__c = team.id; 
        parameters.Name__c= '1';
        parameters.Team_Instance__c = teamInstance.id;
        SnTDMLSecurityUtil.insertRecords(parameters,className);
        
        Parameter__c parameter = TestDataFactory.parameter(pcc,team,teamInstance);
        parameter.Team__c = team.id; 
        parameter.Name__c= '2';
        parameter.Team_Instance__c = teamInstance.id;
        SnTDMLSecurityUtil.insertRecords(parameter,className);        
        
        Step__c ss1 = new Step__c();
        ss1.Matrix__c=gridMaster.id;
        //ss.Grid_Param_1__c = rps.id;
        //ss.Grid_Param_2__c = rpss.id;
        ss1.Step_Type__c ='Matrix';
        ss1.UI_Location__c = 'Compute Segment';
        ss1.Measure_Master__c=measureMaster.Id;
        SnTDMLSecurityUtil.insertRecords(ss1,className);
        
        Rule_Parameter__c rpss = testDataFactory.ruleParameter(measureMaster,parameters,ss1);
        rpss.parameter__c = parameters.id;
        SnTDMLSecurityUtil.insertRecords(rpss,className);
        
        Rule_Parameter__c rps = testDataFactory.ruleParameter(measureMaster,parameters,ss1);
        rps.parameter__c = parameter.id;
        SnTDMLSecurityUtil.insertRecords(rps,className);
        
        Compute_Master__c ccMaster = TestDataFactory.createComputeMaster(rps);
        SnTDMLSecurityUtil.insertRecords(ccMaster,className);
        
        Step__c ss = TestDataFactory.step(ccMaster,gridMaster,measureMaster,rps);
        ss.Matrix__c=gridMaster.id;
        ss.Grid_Param_1__c = rps.id;
        ss.Grid_Param_2__c = rpss.id;
        ss.Step_Type__c ='Matrix';
        ss.UI_Location__c = 'Compute Segment';
        ss.Measure_Master__c=measureMaster.Id;
        SnTDMLSecurityUtil.insertRecords(ss,className);
        
        Step__c ss2 = TestDataFactory.step(ccMaster,gridMaster2,measureMaster,rps);
        ss2.Matrix__c=gridMaster2.id;
        ss2.Grid_Param_1__c = rps.id;
        ss2.Grid_Param_2__c = rpss.id;
        ss2.Step_Type__c ='Matrix';
        ss2.UI_Location__c = 'Compute TCF';
        ss2.Measure_Master__c=measureMaster.Id;
        SnTDMLSecurityUtil.insertRecords(ss2,className);
        
        AxtriaSalesIQTM__Position__c pos = TestDataFactory.createPosition(team,teamInstance);
        SnTDMLSecurityUtil.insertRecords(pos,className);
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamInstance);
        SnTDMLSecurityUtil.insertRecords(posAccount,className);
        
        Product_Priority__c pp = new Product_Priority__c();
        pp.CurrencyIsoCode = 'USD';
        pp.Product__c = pcc.id;
        pp.Speciality_ID__c='testSpecs';
        pp.priority__c='P2';
        SnTDMLSecurityUtil.insertRecords(pp,className);
        
        List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> listpositionaccountcallplan = new List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionacc = TestDataFactory.createPositionAccountCallPlan(measureMaster,acc,teamInstance,posAccount,pp,pos);
        positionacc.AxtriaSalesIQTM__Account__c= acc.id;
        positionacc.AxtriaSalesIQTM__Team_Instance__c =teamInstance.id;
        positionacc.p1__c='test';
        listpositionaccountcallplan.add(positionacc);
        SnTDMLSecurityUtil.insertRecords(listpositionaccountcallplan,className);
        
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            BatchCreateParentPacp obj=new BatchCreateParentPacp(teaminstance.id,measureMaster.id);
            obj.query = 'Select id,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c!=null and Parent_PACP__c =null ';
            Database.executeBatch(obj);
        }
        
        Test.stopTest();
        
    }
}