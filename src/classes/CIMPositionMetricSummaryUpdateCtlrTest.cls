/*
@author - Prince Richard Augustin (A2661)
@description - Test class to test CIMPositionMetricSummaryUpdateCtlr Controller.
*/

@isTest
private class CIMPositionMetricSummaryUpdateCtlrTest { 
    static testMethod void testMethod1() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamIns = TestDataFactory.createTeamInstance(team);
        insert teamIns;
        AxtriaSalesIQTM__Position__c posNation = new AxtriaSalesIQTM__Position__c();
        posNation.AxtriaSalesIQTM__Position_Type__c = 'Nation';
        posNation.Name = 'Chico CA_SPEC';
        posNation.AxtriaSalesIQTM__Team_iD__c    = team.id;
        // posNation.AXTRIASALESIQTM__HIERARCHY_LEVEL__C='4';
        //posNation.Line__c = line.id;
        insert posNation;
        AxtriaSalesIQTM__Position__c pos2 = new AxtriaSalesIQTM__Position__c();
        pos2.AxtriaSalesIQTM__Position_Type__c = 'Region';
        pos2.Name = 'Chicago';
        pos2.AxtriaSalesIQTM__Team_iD__c = team.id;
        pos2.AxtriaSalesIQTM__Parent_Position__c = posNation.id;
        //pos2.Line__c= line.id;
        // pos2.AXTRIASALESIQTM__HIERARCHY_LEVEL__C='3';
        insert pos2;
        
        AxtriaSalesIQTM__Position__c posdm = new AxtriaSalesIQTM__Position__c();
        posdm.AxtriaSalesIQTM__Position_Type__c = 'District';
        posdm.Name = 'Chico CA_SPEC';
        posdm.AxtriaSalesIQTM__Team_iD__c    = team.id;
        posdm.AxtriaSalesIQTM__Parent_Position__c = pos2.id;
        //posdm.Line__c = line.id;
        //posdm.AXTRIASALESIQTM__HIERARCHY_LEVEL__C='2';
        insert posdm;
        
        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();
        pos.AxtriaSalesIQTM__Position_Type__c = 'Territory';
        pos.Name = 'Chico CA_SPEC';
        pos.AxtriaSalesIQTM__Team_iD__c  = team.id;
        pos.AxtriaSalesIQTM__Parent_Position__c = posdm.id;
        //pos.Line__c = line.id;
        //pos.AXTRIASALESIQTM__HIERARCHY_LEVEL__C='1';
        insert pos;
        AxtriaSalesIQTM__Position__c pos22 = new AxtriaSalesIQTM__Position__c();
        pos22.AxtriaSalesIQTM__Position_Type__c = 'Territory';
        pos22.Name = 'Chico CA_SPEC2';
        pos22.AxtriaSalesIQTM__Team_iD__c  = team.id;
        pos22.AxtriaSalesIQTM__Parent_Position__c = posdm.id;
        //pos22.Line__c = line.id;
        //pos.AXTRIASALESIQTM__HIERARCHY_LEVEL__C='1';
        insert pos22;
        
        
        AxtriaSalesIQTM__Position_Team_Instance__c posTeam = new AxtriaSalesIQTM__Position_Team_Instance__c();
        posTeam.AxtriaSalesIQTM__Position_ID__c = pos.id;
        posTeam.AxtriaSalesIQTM__Parent_Position_ID__c = posdm.id;
        posTeam.AxtriaSalesIQTM__Team_Instance_ID__c = teamIns.id;
        posTeam.AxtriaSalesIQTM__Effective_End_Date__c = Date.newInstance(2022,11,09);
        posTeam.AxtriaSalesIQTM__Effective_Start_Date__c = Date.newInstance(2016,02,29);
        //posTeam.isDMRSubmitted__c = true;
        insert posTeam;
        /*
* public static AxtriaSalesIQTM__Position_Team_Instance__c createPositionTeamInstance(Id positionId, Id parentPositionId, Id teamInstanceId) {
AxtriaSalesIQTM__Position_Team_Instance__c positionTeamInstance = new AxtriaSalesIQTM__Position_Team_Instance__c();
positionTeamInstance.AxtriaSalesIQTM__Position_ID__c = positionId;
if (parentPositionId != null)
positionTeamInstance.AxtriaSalesIQTM__Parent_Position_ID__c = parentPositionId;
positionTeamInstance.AxtriaSalesIQTM__Effective_End_Date__c = Date.newInstance(2018, 1, 1);
positionTeamInstance.AxtriaSalesIQTM__Effective_Start_Date__c = Date.newInstance(2015, 1, 1);
positionTeamInstance.AxtriaSalesIQTM__Team_Instance_ID__c = teamInstanceId;
positionTeamInstance.AxtriaSalesIQTM__X_Max__c = -72.6966429900;
positionTeamInstance.AxtriaSalesIQTM__X_Min__c = -73.9625820000;
positionTeamInstance.AxtriaSalesIQTM__Y_Max__c = 40.9666490000;
positionTeamInstance.AxtriaSalesIQTM__Y_Min__c = 40.5821279800;

return positionTeamInstance;
}
*/

Account acc = new Account();
acc.Name = 'Chelsea Parson';
acc.AxtriaSalesIQTM__External_Account_Number__c='123';
acc.BillingStreet='abc';
acc.Marketing_Code__c='ES';
insert acc;

AxtriaSalesIQTM__Position_Account_Call_Plan__c plan=new AxtriaSalesIQTM__Position_Account_Call_Plan__c();
plan.AxtriaSalesIQTM__Team_Instance__c = teamIns.id;
plan.AxtriaSalesIQTM__Account__c = acc.id;
insert plan;

AxtriaSalesIQTM__Change_Request_Type__c crType = new AxtriaSalesIQTM__Change_Request_Type__c();
crType.AxtriaSalesIQTM__Change_Request_Code__c = 'Request  Code';
crType.AxtriaSalesIQTM__CR_Type_Name__c = 'Call_Plan_Change';
insert crType;

AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
cr.AxtriaSalesIQTM__RecordTypeID__c = crType.id;
cr.AxtriaSalesIQTM__Approver1__c = loggedInUser.id;
        // cr.AxtriaSalesIQTM__Is_Auto_Approved__c = True;
cr.AxtriaSalesIQTM__Approver2__c=loggedInUser.id;
cr.AxtriaSalesIQTM__Approver3__c = loggedInUser.id;
cr.AxtriaSalesIQTM__Source_Position__c = pos.id;
cr.AxtriaSalesIQTM__Destination_Position__c = pos22.id;
cr.AxtriaSalesIQTM__Status__c = 'Pending';
cr.AxtriaSalesIQTM__Team_Instance_ID__c=teamIns.id;
cr.AxtriaSalesIQTM__Account_Moved_Id__c = '123';
insert cr;
        //      select Name, AxtriaSalesIQTM__Attribute_API_Name__c,AxtriaSalesIQTM__Aggregation_Object_Name__c
        //      ,AxtriaSalesIQTM__Object_Name__c, AxtriaSalesIQTM__Aggregation_Type__c, AxtriaSalesIQTM__Aggregation_Condition_Attribute_Value__c, 
        //      AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c, AxtriaSalesIQTM__Threshold_Max__c, AxtriaSalesIQTM__Threshold_Min__c, 
        //      AxtriaSalesIQTM__Threshold_Warning_Max__c, AxtriaSalesIQTM__Threshold_Warning_Min__c, AxtriaSalesIQTM__MetricCalculationType__c, 
        //      AxtriaSalesIQTM__Enforcable__c from AxtriaSalesIQTM__CIM_Config__c 
        //      Where AxtriaSalesIQTM__Team_Instance__c =: teamInstanceId And 
        //      AxtriaSalesIQTM__Change_Request_Type__r.AxtriaSalesIQTM__CR_Type_Name__c =: crType and 
        //      AxtriaSalesIQTM__Enable__c=true];

AxtriaSalesIQTM__CIM_Config__c cimcc =TestDataFactory.createCIMConfig(teamins);
cimcc.AxtriaSalesIQTM__Change_Request_Type__c = crType.id;
cimcc.AxtriaSalesIQTM__Threshold_Max__c = '0';
cimcc.AxtriaSalesIQTM__Threshold_Min__c= '0';
cimcc.AxtriaSalesIQTM__Threshold_Warning_Max__c= '0';
cimcc.AxtriaSalesIQTM__Threshold_Warning_Min__c= '0';
cimcc.AxtriaSalesIQTM__MetricCalculationType__c= 'Percentage';
cimcc.AxtriaSalesIQTM__Enforcable__c = true;
cimcc.AxtriaSalesIQTM__Aggregation_Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
cimcc.AxtriaSalesIQTM__Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
        //cimcc.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c = 'AxtriaSalesIQTM__isincludedCallPlan__c=true';
insert cimcc;
AxtriaSalesIQTM__CIM_Position_Metric_Summary__c cimposumm = new AxtriaSalesIQTM__CIM_Position_Metric_Summary__c();
cimposumm.name = 'Total Targets Test';
cimposumm.AxtriaSalesIQTM__CIM_Config__c = cimcc.Id;
cimposumm.AxtriaSalesIQTM__Position_Team_Instance__c = posTeam.Id;
cimposumm.AxtriaSalesIQTM__Team_Instance__c = teamins.Id;
insert cimposumm;

        /*
*   public static AxtriaSalesIQTM__CIM_Config__c createCIMConfig(AxtriaSalesIQTM__Team_Instance__c teamins){
AxtriaSalesIQTM__CIM_Config__c cimcc = new AxtriaSalesIQTM__CIM_Config__c();
cimcc.name = 'Total Targets Test';
cimcc.AxtriaSalesIQTM__Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
cimcc.AxtriaSalesIQTM__Attribute_API_Name__c = 'Account_HCP_HCO_Number__c';
cimcc.AxtriaSalesIQTM__Aggregation_Type__c = 'COUNT_DISTINCT';
cimcc.AxtriaSalesIQTM__Aggregation_Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
cimcc.AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c = 'AxtriaSalesIQTM__Position_Team_Instance__c';
//cimcc.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c = 'AxtriaSalesIQTM__isincludedCallPlan__c=true';
//cimcc.AxtriaSalesIQTM__Aggregation_Condition_Attribute_Value__c = ' ';
cimcc.AxtriaSalesIQTM__team_instance__c = teamins.id;
cimcc.AxtriaSalesIQTM__Enable__c = true ;
return cimcc;
}
*/
list<AxtriaSalesIQTM__Change_Request__c>crlist = new list<AxtriaSalesIQTM__Change_Request__c>();
crlist.add(cr);
AxtriaSalesIQTM__CR_Call_Plan__c ccc=new AxtriaSalesIQTM__CR_Call_Plan__c();
ccc.AxtriaSalesIQTM__Account__c=acc.id;
ccc.AxtriaSalesIQTM__Change_Request_ID__c = cr.id;
insert ccc;


Test.startTest();
System.runAs(loggedInUser){
    ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
    String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
    List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
    System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
    
    CIMPositionMetricSummaryUpdateCtlr c =new CIMPositionMetricSummaryUpdateCtlr();
    CIMPositionMetricSummaryUpdateCtlr.updatePosMetricSummary(teamins.Id,pos.Id,'Call_Plan_Change');
    
}
Test.stopTest();
}
static testMethod void testMethod2() {
    User loggedInUser = new User(id=UserInfo.getUserId());
    AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    insert orgmas;
    AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
    insert countr;
    AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
    insert team;
    AxtriaSalesIQTM__Team_Instance__c teamIns = TestDataFactory.createTeamInstance(team);
    insert teamIns;
    AxtriaSalesIQTM__Position__c posNation = new AxtriaSalesIQTM__Position__c();
    posNation.AxtriaSalesIQTM__Position_Type__c = 'Nation';
    posNation.Name = 'Chico CA_SPEC';
    posNation.AxtriaSalesIQTM__Team_iD__c    = team.id;
        // posNation.AXTRIASALESIQTM__HIERARCHY_LEVEL__C='4';
        //posNation.Line__c = line.id;
    insert posNation;
    AxtriaSalesIQTM__Position__c pos2 = new AxtriaSalesIQTM__Position__c();
    pos2.AxtriaSalesIQTM__Position_Type__c = 'Region';
    pos2.Name = 'Chicago';
    pos2.AxtriaSalesIQTM__Team_iD__c = team.id;
    pos2.AxtriaSalesIQTM__Parent_Position__c = posNation.id;
        //pos2.Line__c= line.id;
        // pos2.AXTRIASALESIQTM__HIERARCHY_LEVEL__C='3';
    insert pos2;
    
    AxtriaSalesIQTM__Position__c posdm = new AxtriaSalesIQTM__Position__c();
    posdm.AxtriaSalesIQTM__Position_Type__c = 'District';
    posdm.Name = 'Chico CA_SPEC';
    posdm.AxtriaSalesIQTM__Team_iD__c    = team.id;
    posdm.AxtriaSalesIQTM__Parent_Position__c = pos2.id;
        //posdm.Line__c = line.id;
        //posdm.AXTRIASALESIQTM__HIERARCHY_LEVEL__C='2';
    insert posdm;
    
    AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();
    pos.AxtriaSalesIQTM__Position_Type__c = 'Territory';
    pos.Name = 'Chico CA_SPEC';
    pos.AxtriaSalesIQTM__Team_iD__c  = team.id;
    pos.AxtriaSalesIQTM__Parent_Position__c = posdm.id;
        //pos.Line__c = line.id;
        //pos.AXTRIASALESIQTM__HIERARCHY_LEVEL__C='1';
    insert pos;
    AxtriaSalesIQTM__Position__c pos22 = new AxtriaSalesIQTM__Position__c();
    pos22.AxtriaSalesIQTM__Position_Type__c = 'Territory';
    pos22.Name = 'Chico CA_SPEC2';
    pos22.AxtriaSalesIQTM__Team_iD__c  = team.id;
    pos22.AxtriaSalesIQTM__Parent_Position__c = posdm.id;
        //pos22.Line__c = line.id;
        //pos.AXTRIASALESIQTM__HIERARCHY_LEVEL__C='1';
    insert pos22;
    
    
    AxtriaSalesIQTM__Position_Team_Instance__c posTeam = new AxtriaSalesIQTM__Position_Team_Instance__c();
    posTeam.AxtriaSalesIQTM__Position_ID__c = pos.id;
    posTeam.AxtriaSalesIQTM__Parent_Position_ID__c = posdm.id;
    posTeam.AxtriaSalesIQTM__Team_Instance_ID__c = teamIns.id;
    posTeam.AxtriaSalesIQTM__Effective_End_Date__c = Date.newInstance(2022,11,09);
    posTeam.AxtriaSalesIQTM__Effective_Start_Date__c = Date.newInstance(2016,02,29);
        //posTeam.isDMRSubmitted__c = true;
    insert posTeam;
        /*
* public static AxtriaSalesIQTM__Position_Team_Instance__c createPositionTeamInstance(Id positionId, Id parentPositionId, Id teamInstanceId) {
AxtriaSalesIQTM__Position_Team_Instance__c positionTeamInstance = new AxtriaSalesIQTM__Position_Team_Instance__c();
positionTeamInstance.AxtriaSalesIQTM__Position_ID__c = positionId;
if (parentPositionId != null)
positionTeamInstance.AxtriaSalesIQTM__Parent_Position_ID__c = parentPositionId;
positionTeamInstance.AxtriaSalesIQTM__Effective_End_Date__c = Date.newInstance(2018, 1, 1);
positionTeamInstance.AxtriaSalesIQTM__Effective_Start_Date__c = Date.newInstance(2015, 1, 1);
positionTeamInstance.AxtriaSalesIQTM__Team_Instance_ID__c = teamInstanceId;
positionTeamInstance.AxtriaSalesIQTM__X_Max__c = -72.6966429900;
positionTeamInstance.AxtriaSalesIQTM__X_Min__c = -73.9625820000;
positionTeamInstance.AxtriaSalesIQTM__Y_Max__c = 40.9666490000;
positionTeamInstance.AxtriaSalesIQTM__Y_Min__c = 40.5821279800;

return positionTeamInstance;
}
*/

Account acc = new Account();
acc.Name = 'Chelsea Parson';
acc.AxtriaSalesIQTM__External_Account_Number__c='123';
acc.BillingStreet='abc';
acc.Marketing_Code__c='ES';
insert acc;

AxtriaSalesIQTM__Position_Account_Call_Plan__c plan=new AxtriaSalesIQTM__Position_Account_Call_Plan__c();
plan.AxtriaSalesIQTM__Team_Instance__c = teamIns.id;
plan.AxtriaSalesIQTM__Account__c = acc.id;
insert plan;

AxtriaSalesIQTM__Change_Request_Type__c crType = new AxtriaSalesIQTM__Change_Request_Type__c();
crType.AxtriaSalesIQTM__Change_Request_Code__c = 'Request  Code';
crType.AxtriaSalesIQTM__CR_Type_Name__c = 'Call_Plan_Change';
insert crType;

AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
cr.AxtriaSalesIQTM__RecordTypeID__c = crType.id;
cr.AxtriaSalesIQTM__Approver1__c = loggedInUser.id;
        // cr.AxtriaSalesIQTM__Is_Auto_Approved__c = True;
cr.AxtriaSalesIQTM__Approver2__c=loggedInUser.id;
cr.AxtriaSalesIQTM__Approver3__c = loggedInUser.id;
cr.AxtriaSalesIQTM__Source_Position__c = pos.id;
cr.AxtriaSalesIQTM__Destination_Position__c = pos22.id;
cr.AxtriaSalesIQTM__Status__c = 'Pending';
cr.AxtriaSalesIQTM__Team_Instance_ID__c=teamIns.id;
cr.AxtriaSalesIQTM__Account_Moved_Id__c = '123';
insert cr;
        //      select Name, AxtriaSalesIQTM__Attribute_API_Name__c,AxtriaSalesIQTM__Aggregation_Object_Name__c
        //      ,AxtriaSalesIQTM__Object_Name__c, AxtriaSalesIQTM__Aggregation_Type__c, AxtriaSalesIQTM__Aggregation_Condition_Attribute_Value__c, 
        //      AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c, AxtriaSalesIQTM__Threshold_Max__c, AxtriaSalesIQTM__Threshold_Min__c, 
        //      AxtriaSalesIQTM__Threshold_Warning_Max__c, AxtriaSalesIQTM__Threshold_Warning_Min__c, AxtriaSalesIQTM__MetricCalculationType__c, 
        //      AxtriaSalesIQTM__Enforcable__c from AxtriaSalesIQTM__CIM_Config__c 
        //      Where AxtriaSalesIQTM__Team_Instance__c =: teamInstanceId And 
        //      AxtriaSalesIQTM__Change_Request_Type__r.AxtriaSalesIQTM__CR_Type_Name__c =: crType and 
        //      AxtriaSalesIQTM__Enable__c=true];

AxtriaSalesIQTM__CIM_Config__c cimcc =TestDataFactory.createCIMConfig(teamins);
cimcc.AxtriaSalesIQTM__Change_Request_Type__c = crType.id;
cimcc.AxtriaSalesIQTM__Threshold_Max__c = '0';
cimcc.AxtriaSalesIQTM__Threshold_Min__c= '0';
cimcc.AxtriaSalesIQTM__Threshold_Warning_Max__c= '0';
cimcc.AxtriaSalesIQTM__Threshold_Warning_Min__c= '0';
cimcc.AxtriaSalesIQTM__MetricCalculationType__c= 'Percentage';
cimcc.AxtriaSalesIQTM__Enforcable__c = true;
cimcc.AxtriaSalesIQTM__Aggregation_Object_Name__c = null;
cimcc.AxtriaSalesIQTM__Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
        //cimcc.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c = 'AxtriaSalesIQTM__isincludedCallPlan__c=true';
insert cimcc;
AxtriaSalesIQTM__CIM_Position_Metric_Summary__c cimposumm = new AxtriaSalesIQTM__CIM_Position_Metric_Summary__c();
cimposumm.name = 'Total Targets Test';
cimposumm.AxtriaSalesIQTM__CIM_Config__c = cimcc.Id;
cimposumm.AxtriaSalesIQTM__Position_Team_Instance__c = posTeam.Id;
cimposumm.AxtriaSalesIQTM__Team_Instance__c = teamins.Id;
insert cimposumm;

        /*
*   public static AxtriaSalesIQTM__CIM_Config__c createCIMConfig(AxtriaSalesIQTM__Team_Instance__c teamins){
AxtriaSalesIQTM__CIM_Config__c cimcc = new AxtriaSalesIQTM__CIM_Config__c();
cimcc.name = 'Total Targets Test';
cimcc.AxtriaSalesIQTM__Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
cimcc.AxtriaSalesIQTM__Attribute_API_Name__c = 'Account_HCP_HCO_Number__c';
cimcc.AxtriaSalesIQTM__Aggregation_Type__c = 'COUNT_DISTINCT';
cimcc.AxtriaSalesIQTM__Aggregation_Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
cimcc.AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c = 'AxtriaSalesIQTM__Position_Team_Instance__c';
//cimcc.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c = 'AxtriaSalesIQTM__isincludedCallPlan__c=true';
//cimcc.AxtriaSalesIQTM__Aggregation_Condition_Attribute_Value__c = ' ';
cimcc.AxtriaSalesIQTM__team_instance__c = teamins.id;
cimcc.AxtriaSalesIQTM__Enable__c = true ;
return cimcc;
}
*/
list<AxtriaSalesIQTM__Change_Request__c>crlist = new list<AxtriaSalesIQTM__Change_Request__c>();
crlist.add(cr);
AxtriaSalesIQTM__CR_Call_Plan__c ccc=new AxtriaSalesIQTM__CR_Call_Plan__c();
ccc.AxtriaSalesIQTM__Account__c=acc.id;
ccc.AxtriaSalesIQTM__Change_Request_ID__c = cr.id;
insert ccc;


Test.startTest();
System.runAs(loggedInUser){
    ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
    String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
    List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
    System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
    
    CIMPositionMetricSummaryUpdateCtlr c =new CIMPositionMetricSummaryUpdateCtlr();
    CIMPositionMetricSummaryUpdateCtlr.updatePosMetricSummary(teamins.Id,pos.Id,'Call_Plan_Change');
    
}
Test.stopTest();
}
}