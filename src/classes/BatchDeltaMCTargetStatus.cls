global class BatchDeltaMCTargetStatus implements Database.Batchable<sObject> 
{
    public String query;
    public Datetime Lmd;
    List<String> allChannels; 
    String teamInstanceSelected;
    List<String> allTeamInstances;
     List<Parent_PACP__c> pacpRecs;
	public Boolean chaining = false;
    
    global BatchDeltaMCTargetStatus(Date lastModifiedDate,string teamInstanceSelectedTemp, List<String> allChannelsTemp) 
    {
        /*teamInstanceSelected = teamInstanceSelectedTemp;
        Lmd= lastModifiedDate;
           query = 'select ID, Sum_TCF__c, Team_Instance__c,(Select id, AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Account__r.AccountNumber , AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__lastApprovedTarget__c,Final_TCF__c,Segment10__c  from Call_Plan_Summary__r where AxtriaSalesIQTM__Position__c !=null and P1__c != null),Position__r.Original_Country_Code__c,Position__r.AxtriaSalesIQTM__Client_Position_Code__c from Parent_PACP__c where LastModifiedDate = Last_N_Days:1 and Team_Instance__c =: teamInstanceSelected';*/
    }
    global BatchDeltaMCTargetStatus(Datetime lastjobDate,string teamInstanceSelectedTemp, List<String> allChannelsTemp) 
    {
        teamInstanceSelected = teamInstanceSelectedTemp;
        Lmd= lastjobDate;
        query = 'select ID, Sum_TCF__c, Team_Instance__c,(Select id, AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Account__r.AccountNumber , AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__lastApprovedTarget__c,Final_TCF__c,Segment10__c,AxtriaARSnT__Final_TCF_Approved__c  from Call_Plan_Summary__r where AxtriaSalesIQTM__Position__c !=null and P1__c != null),Position__r.Original_Country_Code__c,Position__r.AxtriaSalesIQTM__Client_Position_Code__c,Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaARSnT__Country_Veeva_Code__c,Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaARSnT__Cluster_Information__c  from Parent_PACP__c where LastModifiedDate >: Lmd and Team_Instance__c =: teamInstanceSelected';
    }

    global BatchDeltaMCTargetStatus(Date lastModifiedDate,List<String> teamInstanceSelectedTemp, List<String> allChannelsTemp) 
    {
        /*allTeamInstances = new List<String>(teamInstanceSelectedTemp);
        Lmd= lastModifiedDate;
           query = 'select ID, Sum_TCF__c, Team_Instance__c, (select id, AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Account__r.AccountNumber , AxtriaSalesIQTM__lastApprovedTarget__c,Final_TCF__c,AxtriaSalesIQTM__Team_Instance__c,Segment10__c  from Call_Plan_Summary__r where AxtriaSalesIQTM__Position__c !=null and P1__c != null),Position__r.Original_Country_Code__c,Position__r.AxtriaSalesIQTM__Client_Position_Code__c from Parent_PACP__c where LastModifiedDate = Last_N_Days:1 and Team_Instance__c in :allTeamInstances';*/
    }
    global BatchDeltaMCTargetStatus(Datetime lastjobDate,List<String> teamInstanceSelectedTemp, List<String> allChannelsTemp) 
    {
        allTeamInstances = new List<String>(teamInstanceSelectedTemp);
        Lmd= lastjobDate;
        query = 'select ID, Sum_TCF__c, Team_Instance__c, (select id, AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Account__r.AccountNumber , AxtriaSalesIQTM__lastApprovedTarget__c,Final_TCF__c,AxtriaARSnT__Final_TCF_Approved__c,AxtriaSalesIQTM__Team_Instance__c,Segment10__c  from Call_Plan_Summary__r where AxtriaSalesIQTM__Position__c !=null and P1__c != null),Position__r.Original_Country_Code__c,Position__r.AxtriaSalesIQTM__Client_Position_Code__c,Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaARSnT__Country_Veeva_Code__c,Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaARSnT__Cluster_Information__c  from Parent_PACP__c where LastModifiedDate >: Lmd and Team_Instance__c in :allTeamInstances';
    }
    
     global BatchDeltaMCTargetStatus(Datetime lastjobDate,List<String> teamInstanceSelectedTemp, List<String> allChannelsTemp, Boolean chain) 
    {
       /* chaining = chain;
        allTeamInstances = new List<String>(teamInstanceSelectedTemp);
        Lmd= lastjobDate;
           query = 'select ID, Sum_TCF__c, Team_Instance__c, (select id, AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Account__r.AccountNumber , AxtriaSalesIQTM__lastApprovedTarget__c,Final_TCF__c,AxtriaSalesIQTM__Team_Instance__c,Segment10__c  from Call_Plan_Summary__r where AxtriaSalesIQTM__Position__c !=null and P1__c != null),Position__r.Original_Country_Code__c,Position__r.AxtriaSalesIQTM__Client_Position_Code__c from Parent_PACP__c where LastModifiedDate >: Lmd and Team_Instance__c in :allTeamInstances';*/
    }

    global Database.QueryLocator start(Database.BatchableContext bc) 

    {
        return Database.getQueryLocator(query);
    }
    
    public void create_MC_Cycle_Plan_Target_vod(List<Parent_PACP__c> scopePacpProRecs)
    {
         
         allTeamInstances= new List<String>();
        for(Parent_PACP__c pacprecords: scopePacpProRecs)
        {
            allTeamInstances.add(pacprecords.Team_Instance__c);
        }
        //pacpRecs = scopePacpProRecs;
        system.debug('++allTeamInstances++'+allTeamInstances);
        List<SIQ_MC_Cycle_Plan_Target_vod_O__c> mcPlanTarget = new List<SIQ_MC_Cycle_Plan_Target_vod_O__c>();

         /*List<AxtriaSalesIQTM__Position_Employee__c> uap = [select AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,Team_Instance_Name__c, AxtriaSalesIQTM__Employee__r.Employee_PRID__c from AxtriaSalesIQTM__Position_Employee__c where Team_Instance__c in :allTeamInstances and AxtriaSalesIQTM__Assignment_Type__c  = 'Primary' and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Hierarchy_Level__c = '1'];
          system.debug('++uap++'+uap);
        
        Map<String,String> posTeamToUser = new Map<String,String>();
         
        for(AxtriaSalesIQTM__Position_Employee__c u: uap)
        {
            string teamInstancePos = u.Team_Instance_Name__c +'_'+ u.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
            posTeamToUser.put(teamInstancePos ,u.AxtriaSalesIQTM__Employee__r.Employee_PRID__c);
        }
        system.debug('++posTeamToUser++'+posTeamToUser);
        */
        mcPlanTarget = new List<SIQ_MC_Cycle_Plan_Target_vod_O__c>();
        Map<String, Map<String,Integer>> targetCallsMap = new Map<String,  Map<String,Integer>>();        
        Set<String> uniquePos = new Set<String>();

        String selectedMarket = [select AxtriaSalesIQTM__Team__r.Country_Name__c from AxtriaSalesIQTM__Team_Instance__c where id = :allTeamInstances[0]].AxtriaSalesIQTM__Team__r.Country_Name__c;
        
        List<Veeva_Market_Specific__c> veevaCriteria = [select MCCP_Target_logic__c, Channel_Criteria__c,Market__c,MC_Cycle_Threshold_Max__c,MC_Cycle_Threshold_Min__c,MC_Cycle_Channel_Record_Type__c,MC_Cycle_Plan_Channel_Record_Type__c,MC_Cycle_Plan_Product_Record_Type__c,MC_Cycle_Plan_Record_Type__c,MC_Cycle_Plan_Target_Record_Type__c,MC_Cycle_Product_Record_Type__c,MC_Cycle_Record_Type__c from Veeva_Market_Specific__c where Market__c = :selectedMarket];
        
        Decimal max;
        Decimal sum;
        for(Parent_PACP__c ppacp : scopePacpProRecs)
        {
            max = 0;
            sum = 0;
            SIQ_MC_Cycle_Plan_Target_vod_O__c mcp = new SIQ_MC_Cycle_Plan_Target_vod_O__c();
            mcp.SIQ_Channel_Interactions_Goal_vod__c= 0;
            Boolean flag = false;
            Boolean deltaFlag = false;
            system.debug('ppacp+++ '+ ppacp);
            system.debug('deltaFlag+++ '+ deltaFlag);
            system.debug('ppacp.Call_Plan_Summary__r+++ '+ ppacp.Call_Plan_Summary__r);
            

            for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : ppacp.Call_Plan_Summary__r)
            {
               system.debug('pacp+++ '+ pacp);
               if(pacp.AxtriaSalesIQTM__lastApprovedTarget__c==true)
                {
                    system.debug('hey+++ ');
                    deltaFlag = true;
                    if(pacp.AxtriaARSnT__Final_TCF_Approved__c == null)
                    pacp.AxtriaARSnT__Final_TCF_Approved__c = 0;
                    
                    system.debug('++++++++++ Hey Inside recs are '+ ppacp.Call_Plan_Summary__r);
                    flag = true;
                    String teamPos = pacp.AxtriaSalesIQTM__Team_Instance__r.Name + '_' + pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
                    //String userID = posTeamToUser.get(teamPos);
                   //userID = 'U1234';
                    mcp.SIQ_Cycle_Plan_vod__c = teamPos; //:- Get from Cycle Plan vod External ID 
                    mcp.SIQ_Target_vod__c  = pacp.AxtriaSalesIQTM__Account__r.AccountNumber;
                    mcp.SIQ_Status_vod__c = 'Active_vod';
                    if(max < pacp.AxtriaARSnT__Final_TCF_Approved__c)
                    {
                        max = Integer.valueOf(pacp.AxtriaARSnT__Final_TCF_Approved__c);
                    }
                    sum = sum + pacp.AxtriaARSnT__Final_TCF_Approved__c;
                    
                    mcp.SIQ_External_Id_vod__c  =   teamPos + '_' + pacp.AxtriaSalesIQTM__Account__r.AccountNumber;
                }         
                if(deltaFlag==true)
                {
                    system.debug('hey+++ ');

                    if(ppacp.Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaARSnT__Cluster_Information__c=='No Cluster')
                    {
                        mcp.AxtriaARSnT__CountryID__c = ppacp.Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                    }
                    else if(ppacp.Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaARSnT__Cluster_Information__c=='SalesIQ Cluster')
                    {
                        mcp.AxtriaARSnT__CountryID__c = ppacp.AxtriaARSnT__Position__r.AxtriaARSnT__Original_Country_Code__c;
                    }
                    else if(ppacp.Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaARSnT__Cluster_Information__c=='Veeva Cluster')
                    {
                        mcp.AxtriaARSnT__CountryID__c = ppacp.Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaARSnT__Country_Veeva_Code__c;
                    }
                    //mcp.CountryID__c = ppacp.Position__r.Original_Country_Code__c;
                    mcp.External_ID_Axtria__c = mcp.SIQ_External_Id_vod__c;
                    mcp.Rec_Status__c = 'Updated';
                    mcp.RecordTypeId__c = veevaCriteria[0].MC_Cycle_Plan_Target_Record_Type__c;

                    if(veevaCriteria[0].MCCP_Target_logic__c == 'SUM' || veevaCriteria[0].MCCP_Target_logic__c == 'Sum' || veevaCriteria[0].MCCP_Target_logic__c == 'sum')
                    {
                    mcp.SIQ_Channel_Interactions_Goal_vod__c  = sum;
                    }
                    else
                    {
                    mcp.SIQ_Channel_Interactions_Goal_vod__c  = max;  
                    }

                    mcp.SIQ_Product_Interactions_Goal_vod__c  = sum;
                    //mcp.Channel Interaction Max = Same as Goal
                    mcp.Team_Instance__c = ppacp.Team_Instance__c;
                    mcp.Territory__c = ppacp.Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
                    if(flag && !uniquePos.contains(mcp.External_ID_Axtria__c))
                    {
                        system.debug('hey+++ ');
                        mcPlanTarget.add(mcp); 
                        uniquePos.add(mcp.External_ID_Axtria__c);    
                    }
                    system.debug('uniquePos+++ '+uniquePos);
                    system.debug('mcPlanTarget++'+mcPlanTarget);
                }
                else
                {
                    system.debug('hey+++ ');
                    Map<string,string> posaccseg = new Map<string,string>();
                    String teamPos = pacp.AxtriaSalesIQTM__Team_Instance__r.Name + '_' + pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
                    flag = true;
                    string accpos= pacp.AxtriaSalesIQTM__Account__r.AccountNumber+'_'+pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_'+ pacp.AxtriaSalesIQTM__Team_Instance__c;
                    posaccseg.put(accpos,pacp.Segment10__c);
                    system.debug('posaccseg++'+posaccseg);
                    string acc=pacp.AxtriaSalesIQTM__Account__r.AccountNumber;
                    //List<String> tempList = (pacp.AxtriaSalesIQTM__Team_Instance__r.Name + '_' + pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c).split('_');
                    String pos = pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
                    String accPosition = acc + '_' + pos + '_' + pacp.AxtriaSalesIQTM__Team_Instance__c;
                    system.debug('accPosition++'+accPosition);
                         
                    if(posaccseg.containsKey(accPosition))
                    {
                        if(posaccseg.get(accPosition) =='Inactive')
                        {
                          mcp.SIQ_Status_vod__c = 'Inactive';
                          mcp.Rec_Status__c ='Updated';
                        }
                        else if(posaccseg.get(accPosition) =='Merged')
                        {
                          system.debug('merged');
                          mcp.SIQ_Status_vod__c = 'Merged';
                          mcp.Rec_Status__c ='Updated'; 
                        }
                        else
                        {
                          mcp.SIQ_Status_vod__c = 'Deleted';
                          mcp.Rec_Status__c ='Deleted';
                        }  
                    } 
                    else
                    {
                        mcp.SIQ_Status_vod__c = 'Deleted';
                        mcp.Rec_Status__c ='Deleted';
                    } 

                    mcp.External_ID_Axtria__c =teamPos + '_' + pacp.AxtriaSalesIQTM__Account__r.AccountNumber;
                    system.debug('+++External_ID_Axtria__c'+mcp.External_ID_Axtria__c);
                    system.debug('+++External_ID_Axtria__c'+mcp.SIQ_External_Id_vod__c);
                    system.debug('+++flag'+flag);
                    /* pacp.AxtriaSalesIQTM__Team_Instance__r.Name + '_' + pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' + pacp.AxtriaSalesIQTM__Account__r.AccountNumber;*/
                    /* mcPlanTarget.add(mcp); 
                    system.debug('++mcPlanTarget'+mcPlanTarget);*/
                    if(flag && !uniquePos.contains(mcp.External_ID_Axtria__c))
                    {
                    system.debug('hey+++ ');
                    mcPlanTarget.add(mcp); 
                    uniquePos.add(mcp.External_ID_Axtria__c);    
                    } 
                }               
            }    
             
        }
        upsert mcPlanTarget External_ID_Axtria__c;               
    }

    global void execute(Database.BatchableContext BC, List<Parent_PACP__c> scopePacpProRecs) 
    {     
      create_MC_Cycle_Plan_Target_vod(scopePacpProRecs);
    }

    global void finish(Database.BatchableContext BC) 
    {     
        changeMCChannelDelta u1 = new changeMCChannelDelta(lmd,allTeamInstances, allChannels);        
        Database.executeBatch(u1,2000);  
    }
}