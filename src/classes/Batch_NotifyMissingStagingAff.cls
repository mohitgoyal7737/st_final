global class Batch_NotifyMissingStagingAff implements Database.Batchable<sObject>,database.stateful {
    public String query;
    global string data='ID ,STATUS \n';
    global boolean flag =false;

    global Batch_NotifyMissingStagingAff() {
        data='';
        query='select id,AxtriaARSnT__SIQ_Account_Number__c,AxtriaARSnT__SIQ_Parent_Account_Number__c,AxtriaARSnT__SIQ_Affiliation_Status__c,AxtriaARSnT__Unique_Field__c from AxtriaARSnT__SIQ_Account_Affiliation__c ';
        this.query = query;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaARSnT__SIQ_Account_Affiliation__c> scope) 
    {
        set<string>unq = new set<string>();
        list<AxtriaSalesIQTM__Account_Affiliation__c>mainaff = new list<AxtriaSalesIQTM__Account_Affiliation__c>();
        string affquery='';
        map<string,string>statuscheck = new map<string,string>();
        for(AxtriaARSnT__SIQ_Account_Affiliation__c af : scope)
        {
            String key=af.AxtriaARSnT__SIQ_Account_Number__c+'_'+af.AxtriaARSnT__SIQ_Parent_Account_Number__c;
            unq.add(key);
        }
        affquery= 'Select id,AxtriaARSnT__Affiliation_Status__c,AxtriaARSnT__Unique_Id__c from AxtriaSalesIQTM__Account_Affiliation__c where AxtriaARSnT__Unique_Id__c IN : unq';
        System.debug('===========AFF Query is:::'+affquery);
        mainaff =Database.query(affquery);
        for(AxtriaSalesIQTM__Account_Affiliation__c aff: mainaff)
        {
            statuscheck.put(aff.AxtriaARSnT__Unique_Id__c,aff.AxtriaARSnT__Affiliation_Status__c);
        }
        system.debug('===========statuscheck::::'+statuscheck);
        for(AxtriaARSnT__SIQ_Account_Affiliation__c af : scope)
        {
            string key=af.AxtriaARSnT__SIQ_Account_Number__c+'_'+af.AxtriaARSnT__SIQ_Parent_Account_Number__c;
            if(!statuscheck.containskey(key)){
                data+=af.id +','+'NOT Present'+'\n';
                flag=true;
            }
            else{
                if(af.AxtriaARSnT__SIQ_Affiliation_Status__c !=statuscheck.get(key))
                {
                    data+=af.id+','+'Status MisMatch'+'\n';
                    flag=true;
                }
            }
        }

        
    }

    global void finish(Database.BatchableContext BC) {
        if(flag)
        {
            Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
            blob excel = blob.valueOf(data);
            system.debug('excel blob Attachment'+excel);

            attach.setBody(excel);
            attach.setFileName('Missing Affiliation data.CSV');
            String subject = 'Missing data from affliation staging table';
            String body = '';
            String[] address = new String[]{'Ayushi.Jain@axtria.com'};
            String[] ccAdd = new String[]{'Ayushi.Jain@axtria.com'};
                
            Messaging.SingleEmailMessage emailwithattch = new Messaging.SingleEmailMessage();
            emailwithattch.setSubject(subject);
            emailwithattch.setToaddresses(address);
            emailwithattch.setPlainTextBody(body);
            emailwithattch.setCcAddresses(ccAdd);

            emailwithattch.setFileAttachments(new Messaging.EmailFileAttachment[]{attach});

            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {emailwithattch});
       
        }

    }
}