@isTest
private class WorkbenchlightningCtrlTest {
	 @isTest static void testgetProfileNameMap() {
       map<String,list<String>> profileNameMap = new map<String,list<String>>();
       map<String,list<String>> profileNameMap1 = new map<String,list<String>>();
        profileNameMap.put('HO',SalesIQGlobalConstants.HO_PROFILE);
        profileNameMap.put('HR',SalesIQGlobalConstants.HR_PROFILE); 
        profileNameMap.put('DM',SalesIQGlobalConstants.DM_PROFILE);
        profileNameMap.put('RM',SalesIQGlobalConstants.RM_PROFILE);
        profileNameMap.put('Rep',SalesIQGlobalConstants.REP_PROFILE);
        profileNameMap.put('System Administrator',new list<String>{'System Administrator'});
        profileNameMap1=WorkbenchlightningCtrl.getProfileNameMap();
        System.assertEquals(profileNameMap,profileNameMap1);
    }
    
}