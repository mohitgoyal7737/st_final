global class AZ_Integration_Pack_For_Multichannel implements Schedulable{
    
    string teamInstanceSelected;
    String status;
    List<Veeva_Market_Specific__c>  veevaCriteria;
    List<String> allTeamInstances;
    list<string>teaminstancelistis;
    List<String> allChannels;
    
    global AZ_Integration_Pack_For_Multichannel(string teamInstance)
    {
        teamInstanceSelected = teamInstance;
        
       create_MC_Cycle();
    }
     
    global AZ_Integration_Pack_For_Multichannel(List<String> teamInstance)
    {
        allTeamInstances = new List<String>(teamInstance);
        //teamInstanceSelected = teamInstance;
        
       create_MC_Cycle();
    }

    global AZ_Integration_Pack_For_Multichannel(string teamInstance, Integer count)
    {
        teamInstanceSelected = teamInstance;
        
       create_MC_Cycle_Plan_Target_vod();
    }
   
    
    global void create_MC_Cycle()
    {
        List<AxtriaSalesIQTM__Team_Instance__c> allTeamInstanceRecs = new List<AxtriaSalesIQTM__Team_Instance__c>();
        veevaCriteria= new List<Veeva_Market_Specific__c>();
        
        allTeamInstanceRecs = [select Name,AxtriaSalesIQTM__Team__r.Name, AxtriaSalesIQTM__IC_EffstartDate__c, Increment_Field__c, AxtriaSalesIQTM__IC_EffendDate__c, AxtriaSalesIQTM__Team_Cycle_Name__c, AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Base_Team_Name__c, AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c, AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c from AxtriaSalesIQTM__Team_Instance__c where id in :allTeamInstances];

        String selectedMarket = [select AxtriaSalesIQTM__Team__r.Country_Name__c from AxtriaSalesIQTM__Team_Instance__c where id = :allTeamInstances[0]].AxtriaSalesIQTM__Team__r.Country_Name__c;
        
        veevaCriteria = [select Channel_Name__c,Channel_Criteria__c,Market__c,MC_Cycle_Threshold_Max__c,MC_Cycle_Threshold_Min__c,MC_Cycle_Channel_Record_Type__c,MC_Cycle_Plan_Channel_Record_Type__c,MC_Cycle_Plan_Product_Record_Type__c,MC_Cycle_Plan_Record_Type__c,MC_Cycle_Plan_Target_Record_Type__c,MC_Cycle_Product_Record_Type__c,MC_Cycle_Record_Type__c from Veeva_Market_Specific__c where Market__c = :selectedMarket];
        
        List<SIQ_MC_Cycle_vod_O__c> mcRecs = new List<SIQ_MC_Cycle_vod_O__c>();
         
        for(AxtriaSalesIQTM__Team_Instance__c teamInstanceRecs : allTeamInstanceRecs)
        {
            SIQ_MC_Cycle_vod_O__c mc = new SIQ_MC_Cycle_vod_O__c();
            mc.Name                          =   teamInstanceRecs.Name;             // Do Modifications in Name (Add Year to Name with Underscores)
            
            
            DateTime dtStart                 =   teamInstanceRecs.AxtriaSalesIQTM__IC_EffstartDate__c;
            Date myDateStart                 =   Date.valueof(dtStart);

            DateTime dtEnd                   =   teamInstanceRecs.AxtriaSalesIQTM__IC_EffendDate__c;
            Date myDateEnd                   =   Date.valueof(dtEnd);

            String dateWithoutTimeStamp      =   dtStart.year() + '-0' + dtStart.month() + '-' + dtStart.day();
            
            mc.SIQ_Start_Date_vod__c             =   myDateStart;
            mc.SIQ_End_Date_vod__c               =   myDateEnd;
            ///mc.AZ_Business_Unit__c          =   teamInstanceRecs.Team_Cycle_Name__c; // CNS or Immunolgy : Take Business Unit of AZ
            //mc.AZ_Country_Code__c           =   teamInstanceRecs.Team__r.Base_Team_Name__c;        // Take from Orbit tables_For Callmax File
            //mc.Calculate_Pull_Through_vod__c =   true;  
            mc.SIQ_Description_vod_del__c            =   'Cycle for MCCP';
            //mc.AZ_External_ID__c            =   mc.AZ_Country_Code__c + '_' + 'CY' + '_' + dateWithoutTimeStamp + '_' + String.valueof(teamInstanceRecs.Increment_Field__c);
            if(teamInstanceRecs.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c=='No Cluster')
            {
             mc.SIQ_Country_Code_AZ__c           =   teamInstanceRecs.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;      
            }
            else if(teamInstanceRecs.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c=='SalesIQ Cluster')
            {
              mc.SIQ_Country_Code_AZ__c          =   teamInstanceRecs.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
            }
            else if(teamInstanceRecs.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c=='Veeva Cluster')
            {
                mc.SIQ_Country_Code_AZ__c        =   teamInstanceRecs.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c;
            }
            //mc.SIQ_Country_Code_AZ__c            =   teamInstanceRecs.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
            mc.SIQ_Record_Type__c                =   veevaCriteria[0].MC_Cycle_Record_Type__c;
            mc.Over_Reached_Threshold_vod__c     =   veevaCriteria[0].MC_Cycle_Threshold_Max__c;
            mc.Under_Reached_Threshold_vod__c    =   veevaCriteria[0].MC_Cycle_Threshold_Min__c;
                
            if(myDateEnd.daysBetween(Date.today()) > 0)
            {
                mc.SIQ_Status_vod__c                 =   'Planned_vod';
                mc.SIQ_Calculate_Pull_Through_vod__c      = true;
                
            }
            else
            {
                mc.SIQ_Status_vod__c                 =   'In_Progress_vod';
                mc.SIQ_Calculate_Pull_Through_vod__c      = false;

            }
            system.debug('++mc.SIQ_Status_vod__c in MC Cycle'+mc.SIQ_Status_vod__c);
            status =  mc.SIQ_Status_vod__c ;
            system.debug('++status in MC Cycle'+status);
            mc.SIQ_External_Id_vod__c            =   mc.SIQ_Country_Code_AZ__c  + '_'+ 'CY' + '_' + dateWithoutTimeStamp + '_' + String.valueof(teamInstanceRecs.Increment_Field__c);
            mcRecs.add(mc);
        }
        
        upsert mcRecs SIQ_External_Id_vod__c;
        
        createMC_Cycle_Plan();
    }
    
    global void createMC_Cycle_Plan() 
    {
        List<AxtriaSalesIQTM__Position__c> posTeamInstanceRecs = [select id, Original_Country_Code__c, AxtriaSalesIQTM__Team_Instance__r.Name , AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name, AxtriaSalesIQTM__Team_Instance__r.Increment_Field__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c , AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c  from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances and AxtriaSalesIQTM__Hierarchy_Level__c ='1' and (AxtriaSalesIQTM__Client_Position_Code__c != '000000' and AxtriaSalesIQTM__Client_Position_Code__c != '0' and AxtriaSalesIQTM__Client_Position_Code__c != '00000') and AxtriaSalesIQTM__Inactive__c = false];
        
        List<SIQ_MC_Cycle_Plan_vod_O__c> mcCyclePlan = new List<SIQ_MC_Cycle_Plan_vod_O__c>();
        Set<String> allPositions = new Set<String>();


        for(AxtriaSalesIQTM__Position__c pti : posTeamInstanceRecs)
        {
            allPositions.add(pti.AxtriaSalesIQTM__Client_Position_Code__c);
        }

        List<AxtriaSalesIQTM__Position_Employee__c> uap = [select AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, Team_Instance_Name__c, AxtriaSalesIQTM__Employee__r.Employee_PRID__c from AxtriaSalesIQTM__Position_Employee__c where AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c in :allPositions and AxtriaSalesIQTM__Assignment_Type__c  = 'Primary' and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Hierarchy_Level__c = '1' and (AxtriaSalesIQTM__Assignment_Status__c = 'Active' ) and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__IsMaster__c = true];
        
        Map<String,String> posTeamToUser = new Map<String,String>();
         
        for(AxtriaSalesIQTM__Position_Employee__c u: uap)
        {
            string teamInstancePos =  u.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
            posTeamToUser.put(teamInstancePos ,u.AxtriaSalesIQTM__Employee__r.Employee_PRID__c);
        }
        
        system.debug('++++++++ '+posTeamToUser);
        Set<String> allTeams = new Set<String>();
        Set<String> uniqueRec = new Set<String>();
         system.debug('++status in MC Cycle Plan'+status);

        for(AxtriaSalesIQTM__Position__c pti : posTeamInstanceRecs)
        {
            SIQ_MC_Cycle_Plan_vod_O__c mcp = new SIQ_MC_Cycle_Plan_vod_O__c();
            string teamInstancePos = pti.AxtriaSalesIQTM__Client_Position_Code__c; // Use Client Position Code for Territory ID
            string userID = posTeamToUser.get(teamInstancePos);
            system.debug('++status in MC Cycle Plan'+status);

            //userID = 'U1234';
            DateTime dtStart                 =   pti.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c;
            Date myDateStart                 =   date.newinstance(dtStart.year(), dtStart.month(), dtStart.day());

            String dateWithoutTimeStamp      =   dtStart.year() + '-0' + dtStart.month() + '-' + dtStart.day();
            
            mcp.Name                    =   teamInstancePos;
            if(pti.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c=='No Cluster')
            {
               mcp.SIQ_Country_Code_AZ__c = pti.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c ;
            }
            else if(pti.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c=='SalesIQ Cluster')
            {
               mcp.SIQ_Country_Code_AZ__c = pti.Original_Country_Code__c;
            }
            else if (pti.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c=='Veeva Cluster')
            {
                mcp.SIQ_Country_Code_AZ__c = pti.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c;
            }
            //mcp.SIQ_Country_Code_AZ__c = pti.Original_Country_Code__c;
            
            //mcp.AZ_Country_Code__c     =   pti.Team_Instance_ID__r.Team__r.Base_Team_Name__c; // Same as Earlier in MC Cycle
            mcp.SIQ_Cycle_vod__c            =  mcp.SIQ_Country_Code_AZ__c + '_' + 'CY' + '_' + dateWithoutTimeStamp + '_' + String.valueof(pti.AxtriaSalesIQTM__Team_Instance__r.Increment_Field__c);
            if(userID == null)
            {
                userID = 'Sadmin1';
            }
            mcp.SIQ_Owner_Id__c             =   userID;                                 // Federation ID
            mcp.SIQ_Description_vod__c      =   pti.AxtriaSalesIQTM__Team_Instance__r.Name ;       // Give Cycle Name Data
            mcp.SIQ_Territory_vod__c        =   pti.AxtriaSalesIQTM__Client_Position_Code__c;   // Add Client Position Code instead of Salesforce ID
           // mcp.AZ_External_ID_vod__c  = x  mcp.Name +'_'+mcp.Territory_vod__c;                          // Create this new field
            mcp.SIQ_External_Id_vod__c      =   pti.AxtriaSalesIQTM__Team_Instance__r.Name +'_' + teamInstancePos;
            mcp.SIQ_Lock_vod__c             =   'false';                              // Add this new field
            mcp.SIQ_Channel_Interaction_Status_vod__c    = 'On_Schedule_vod';
            mcp.SIQ_Product_Interaction_Status_vod__c     = 'On_Schedule_vod'; 
            mcp.SIQ_RecordType__c             = veevaCriteria[0].MC_Cycle_Plan_Record_Type__c;
            mcp.SIQ_Status_vod__c             = status;
            system.debug('++SIQ_Status_vod__c'+mcp.SIQ_Status_vod__c);
            system.debug('++status in MC Cycle Plan'+status);

            
            if(!uniqueRec.contains(mcp.SIQ_External_Id_vod__c))
            {
                uniqueRec.add(mcp.SIQ_External_Id_vod__c);
                mcCyclePlan.add(mcp);    
            }                         
        }
        
        upsert mcCyclePlan SIQ_External_Id_vod__c;
        
        createMC_Cycle_Channel_vod();
    }
    
    global void createMC_Cycle_Channel_vod()
    {
        List<AxtriaSalesIQTM__Team_Instance__c> teamInstanceRecs = [select id, Name, AxtriaSalesIQTM__IC_EffstartDate__c, Increment_Field__c, AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Base_Team_Name__c, AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c, AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c from AxtriaSalesIQTM__Team_Instance__c where id in :allTeamInstances];
        //List<Channel_Weights__c> channelAndWeights = [select Name, Weights__c, Channel_Criteria_vod__c from Channel_Weights__c];

        //String selectedMarket = [select AxtriaSalesIQTM__Team__r.Country_Name__c from AxtriaSalesIQTM__Team_Instance__c where id = :teamInstanceSelected].AxtriaSalesIQTM__Team__r.Country_Name__c;
        
        //List<Veeva_Market_Specific__c>  veevaCriteria = [select Channel_Criteria__c,Market__c from Veeva_Market_Specific__c where Market__c = :selectedMarket];
         Map<string,string> channelAndWeights = new Map<string,string>();
         system.debug('veevaCriteria==='+veevaCriteria[0].Channel_Name__c);
        String selectedCriteria;
        allChannels = new List<String>();
        for(integer i=0; i < veevaCriteria[0].Channel_Name__c.split(',').size();i++){
            
            channelAndWeights.put(veevaCriteria[0].Channel_Name__c.split(',')[i],'1');
            allChannels.add(veevaCriteria[0].Channel_Name__c.split(',')[i]); 
           
           
        }

        if(veevaCriteria.size()>0)
        {
            selectedCriteria = veevaCriteria[0].Channel_Criteria__c;
        }
        else
        {
            selectedCriteria = 'Not Found';
        }
        Set<String> uniqueCheck = new Set<String>();

        List<SIQ_MC_Cycle_Channel_vod_O__c> mcCycleChannel = new List<SIQ_MC_Cycle_Channel_vod_O__c>();
        
        for(AxtriaSalesIQTM__Team_Instance__c teamInstance : teamInstanceRecs)
        {
            //for(Channel_Weights__c cw : channelAndWeights)
            //{
            /*********** adding weight for multichannel*****************************/
            for(String channelName : channelAndWeights.keySet())
            {
                SIQ_MC_Cycle_Channel_vod_O__c mcc = new SIQ_MC_Cycle_Channel_vod_O__c();
                
                DateTime dtStart                 =   teamInstance.AxtriaSalesIQTM__IC_EffstartDate__c;
                Date myDateStart                 =   date.newinstance(dtStart.year(), dtStart.month(), dtStart.day());

                String dateWithoutTimeStamp      =   dtStart.year() + '-0' + dtStart.month() + '-' + dtStart.day(); 
                
                //mcc.CHANNEL_LABEL_VOD__C    =   'f2f';
               /* 
                if(cw.Name.contains('Email'))
                {
                    mcc.SIQ_Channel_Object_vod__c   =   'Sent_Email_vod__c';
                }
                else*/
                /******************** different for email and event**************/
                
                if(channelName.contains('Email'))
                {
                    mcc.SIQ_Channel_Object_vod__c   =   'Sent_Email_vod__c';
                }
                else if(channelName.contains('Events')){
                    mcc.SIQ_Channel_Object_vod__c   =   'Multichannel_Activity_vod__c';
                }
                else{
                    mcc.SIQ_Channel_Object_vod__c   =   'Call2_vod__c';
                }
                /******************* end*****************/
                
               // mcc.SIQ_Channel_Object_vod__c   =   'Call2_vod__c';
                if(teamInstance.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c=='No Cluster')
                {
                    mcc.SIQ_Country_Code_AZ__c        =   teamInstance.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;      
                }
                else if(teamInstance.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c=='SalesIQ Cluster')
                {
                    mcc.SIQ_Country_Code_AZ__c        =   teamInstance.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                }
                else if(teamInstance.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c=='Veeva Cluster')
                {
                    mcc.SIQ_Country_Code_AZ__c        =   teamInstance.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c;
                }
                //mcc.SIQ_Country_Code_AZ__c      =    teamInstance.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                String country = teamInstance.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;  
                mcc.SIQ_Channel_Criteria_vod__c =   selectedCriteria;//the Channels, Add this field in Channel Weights object
                mcc.SIQ_Channel_Weight_vod__c   =   1;
                mcc.SIQ_Cycle_vod__c            =   mcc.SIQ_Country_Code_AZ__c +'_' + 'CY' + '_' + dateWithoutTimeStamp + '_' + String.valueof(teamInstance.Increment_Field__c);
                mcc.SIQ_External_Id_vod__c      =   teamInstance.Name + '-'+ channelName +'-' + mcc.SIQ_Channel_Object_vod__c;//teamInstance.Name + '-f2f-' + mcc.SIQ_Channel_Object_vod__c;
                //mcc.AZ_External_Id__c      =   teamInstance.Name + '-' + cw.Name + '-' + mcc.SIQ_Channel_Object_vod__c; // Not Required as of now. May be used in future;
                mcc.SIQ_RecordTypeId__c         =   veevaCriteria[0].MC_Cycle_Channel_Record_Type__c;        // Add this static Field
                mcc.External_ID_Axtria__c = mcc.SIQ_External_Id_vod__c;

                if(!uniqueCheck.contains(mcc.SIQ_External_Id_vod__c))
                {
                    mcCycleChannel.add(mcc);
                    uniqueCheck.add(mcc.SIQ_External_Id_vod__c);
                }
              }
            //}
            
        }
        
        upsert mcCycleChannel External_ID_Axtria__c; 
        createMC_Cycle_Product_vod();
        
    }
    
  
    
    global void createMC_Cycle_Product_vod()
    {
        //String teamInstanceName = [select Name from AxtriaSalesIQTM__Team_Instance__c where id = :teamInstanceSelected LIMIT 1].Name;

        List<Product_Catalog__c> allProductsAndWeights = [select Name, Weights__c,Detail_Group__c, Veeva_External_ID__c, Team_Instance__c from  Product_Catalog__c where Team_Instance__c in :allTeamInstances and IsActive__c=true];

        Map<String, List<Product_Catalog__c>> teamInstanceWisePC = new Map<String, List<Product_Catalog__c>>();

        for(Product_Catalog__c pc : allProductsAndWeights)
        {
            List<Product_Catalog__c> tempPC = new List<Product_Catalog__c>();

            if(teamInstanceWisePC.containsKey(pc.Team_Instance__c))
            {
                tempPC = teamInstanceWisePC.get(pc.Team_Instance__c);
            }
            tempPC.add(pc);
            teamInstanceWisePC.put(pc.Team_Instance__c, tempPC);
        }


        List<AxtriaSalesIQTM__Team_Instance__c> teamInstance = [select id,AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Base_Team_Name__c, AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,Name,AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c, AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c from AxtriaSalesIQTM__Team_Instance__c where id in :allTeamInstances];

        List<SIQ_MC_Cycle_Product_vod_O__c> mcCycleProduct = new List<SIQ_MC_Cycle_Product_vod_O__c>();
        
        Set<String> allUnique = new Set<String>();


        for(AxtriaSalesIQTM__Team_Instance__c teamInstanceRec : teamInstance)
        {
            system.debug('+++++++++++ Team Instance '+ teamInstanceRec);
            system.debug('++++++++ teamInstanceWisePC' + teamInstanceWisePC);
            if(teamInstanceWisePC.containsKey(teamInstanceRec.id))
            {
                 for(Product_Catalog__c pro : teamInstanceWisePC.get(teamInstanceRec.id))         // What all products we have for this team instance //Get all product code from  File (Master List)
                {                  
                    string object1;
                    
                    for(String channel : allChannels)
                    {
                        SIQ_MC_Cycle_Product_vod_O__c mcp = new SIQ_MC_Cycle_Product_vod_O__c();
                        
                        if(channel == 'Email')
                        object1 = 'Sent_Email_vod__c';
                        else 
                        object1 = 'Call2_vod__c';
                            
                        
                        //mcp.External_Id_vod__c = teamInstance.Name + '-' + channel + '-' + object1;
                        //mcp.Channel_Label_vod__c = channel;
                        mcp.SIQ_Cycle_Channel_vod__c = teamInstanceRec.Name + '-' + channel + '-' + object1;  
                        mcp.SIQ_Detail_Group__c = pro.Detail_Group__c;
                        mcp.SIQ_Product_vod__c = pro.Veeva_External_ID__c;       // Product Code is unique here
                        mcp.SIQ_Product_Weight_vod__c = 1;        // Keep it Static, 1 for all
                        //mcp.AZ_External_Id__c = teamInstance.Name + '__' + channel + '__' +pro.External_ID__c + '_'+pro.Name;
                        mcp.SIQ_External_Id_vod__c = teamInstanceRec.Name + '__' + channel + '__' +pro.Veeva_External_ID__c ;
                        mcp.External_ID_Axtria__c = teamInstanceRec.Name + '__' + channel + '__' +pro.Veeva_External_ID__c;
                        mcp.SIQ_Record_Type__c = veevaCriteria[0].MC_Cycle_Product_Record_Type__c;
                         if(teamInstanceRec.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c=='No Cluster')
                        {
                            mcp.Country_ID__c = teamInstanceRec.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                        }
                        else if(teamInstanceRec.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c=='SalesIQ Cluster')
                        {
                            mcp.Country_ID__c = teamInstanceRec.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                        }
                        else if(teamInstanceRec.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c=='Veeva Cluster')
                        {
                            mcp.Country_ID__c = teamInstanceRec.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c;
                        }

                        //mcp.Country_ID__c = teamInstanceRec.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;

                        if(!allUnique.contains(mcp.External_ID_Axtria__c))
                        {
                            allUnique.add(mcp.External_ID_Axtria__c);
                            mcCycleProduct.add(mcp);
                        }
                        
                    }
                    
                }
            }
           
            
            system.debug('+++++++++++'+ mcCycleProduct);
                       
        }

        upsert mcCycleProduct External_ID_Axtria__c; 
        create_MC_Cycle_Plan_Target_vod();
    }       
    
     global void create_MC_Cycle_Plan_Target_vod()
    {
        for(String ti : allTeamInstances)
        {
            List<String> oneTeamInstance = new List<String>();
            oneTeamInstance.add(ti);
            changeMCTargetStatusForMultichannel u1 = new changeMCTargetStatusForMultichannel(oneTeamInstance, allChannels);
        
            Database.executeBatch(u1, 2000);    
        }
        

        create_MC_Cycle_Plan_Channel_vod();
    }
    
    global void create_MC_Cycle_Plan_Channel_vod() 
    {
        
        changeMCChannelStatusMultiChannel u1 = new changeMCChannelStatusMultiChannel(allTeamInstances, allChannels);
        
        Database.executeBatch(u1, 2000);
        
        create_MC_Cycle_Plan_Product_vod();
    }
    
    global  void create_MC_Cycle_Plan_Product_vod()
    {
        changeMCProductMultiChannel u1 = new changeMCProductMultiChannel(allTeamInstances, allChannels);
        
        Database.executeBatch(u1, 2000);
        
    }
    global void execute(System.SchedulableContext SC){
        teaminstancelistis = new list<string>();
        teaminstancelistis = StaticTeaminstanceList.filldataCallPlan();
        //database.executeBatch(new changeTSFStatus(teaminstancelistis));
        AZ_Integration_Pack_For_Multichannel aip = new AZ_Integration_Pack_For_Multichannel(teaminstancelistis);
    }
}