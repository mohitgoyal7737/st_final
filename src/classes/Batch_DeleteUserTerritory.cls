global  with sharing class Batch_DeleteUserTerritory implements Database.Batchable<sObject> {
    list<User_Territory__c> userTerr;
    string jobType = util.getJobType('UserTerritory');
    set<string> allCountries = jobType == 'Full Load'?util.getFulloadCountries():util.getDeltaLoadCountries();
   // map<string, boolean> mapProdStatus;
    string query;
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        if(jobType == 'Full Load'){
            query = 'Select Id, Event__c, Record_Status__c, Country__c from User_Territory__c WITH SECURITY_ENFORCED';
        }
        else{    
            query = 'Select Id, Event__c, Record_Status__c from User_Territory__c Where Record_Status__c = \'Updated\' WITH SECURITY_ENFORCED';
        }
        return database.getQueryLocator(query);
    }
            
    global void execute(Database.BatchableContext bc, List<User_Territory__c> scope){
        userTerr = new list<User_Territory__c>();
        if(jobType == 'Full Load'){
            for(User_Territory__c pe : scope){ 
                if(allCountries.contains(pe.Country__c)){
                    pe.Event__c = 'Deleted';
                    pe.Record_Status__c = 'Updated';
                    userTerr.add(pe);
                }
                else{
                    pe.Record_Status__c = 'No Change';
                    userTerr.add(pe);    
                }
            }
        }
        else{
            for(User_Territory__c pe : scope){ 
                pe.Record_Status__c = 'No Change';
                userTerr.add(pe);
            }
        }
        if(UserTerr.size()>0){
            //upsert UserTerr;
            SnTDMLSecurityUtil.upsertRecords(UserTerr, 'Batch_DeleteUserTerritory');
        }
    }
    
    global void finish(Database.BatchableContext bc){
        if(!System.Test.isRunningTest())
            database.executeBatch(new Batch_UserTerritoryMapping(), 2000);
    }
}