global class BatchupdateTeamInstanceId implements Database.Batchable<sObject>{

    //global BatchupdateTeamInstanceId(){
        
    //}
     global Database.QueryLocator start(Database.BatchableContext BC) {
        //String query = 'SELECT Id,Name,Account_ID__c,Scenario_Name__c,AxtriaARSnT__POSITION__c,AxtriaARSnT__Objective__c,AxtriaARSnT__Target__c,Product_Name__c,Workspace__c,Adoption__c,Potential__c,Product_ID__c,Segment__c FROM Master_List_AccProd__c where Status__c = \'New\'';


        String query = 'SELECT Id,Name,CreatedDate,Account__c,Account__r.AccountNumber,Status__c,Account_Name__c,AccountProductKey__c,Adoption__c,Objective__c,POSITION__c,Potential__c,Previous_Cycle_Calls__c,Product__c,Product_ID__c,Scenario_Name__c,Segment__c ,Target__c,Team_Instance__c,Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,Workspace__c FROM Master_List_AccProd__c where Status__c = \'New\' AND ( Account__c = null OR Team_Instance__c = null)';


        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Master_List_AccProd__c> scope) {

        List<String> teamInstName= new List<String>();
    List<String> accountName= new List<String>();
    
    for(Master_List_AccProd__c Accprod : scope)
    {
        teamInstName.add(Accprod.Scenario_Name__c);
        accountName.add(Accprod.Account_Name__c);
    }
    
    Map<String,String> teamInstMap = new Map<String,String>();
    Map<String,String> AccountMap = new Map<String,String>();
    
    //teamInstList = [Select Team_Instance__c,Scenario_Name__c from Master_List_AccProd__c];
    
    
    for(Account acc:  [Select Id,AccountNumber from Account where AccountNumber IN: accountName])
    {
        AccountMap.put(acc.AccountNumber,acc.id);
    }

    for(AxtriaSalesIQTM__Team_Instance__c teamInst :  [Select Id,Name from AxtriaSalesIQTM__Team_Instance__c where Name IN: teamInstName])
    {
        teamInstMap.put(teamInst.name,teamInst.id);
    }
    

    List<Master_List_AccProd__c> updateAccProdList = new List<Master_List_AccProd__c>();
    for(Master_List_AccProd__c Accprod : scope)
    {
        if(Accprod.Team_Instance__c == null)
        {
            Accprod.Team_Instance__c = teamInstMap.get(Accprod.Scenario_Name__c);
            Accprod.Account__c = AccountMap.get(Accprod.Account_Name__c);
            updateAccProdList.add(Accprod);
        }
    }

    update updateAccProdList;
        

  }  
    
    global void finish(Database.BatchableContext BC) {
    }
}