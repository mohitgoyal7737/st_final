global with sharing class BatchVeevaSurveyCreation implements Database.Batchable<sObject> ,Database.Stateful
{
    public String query;
    List<String> teaminstancelist;
     public String teamInstName;
   
   

    global BatchVeevaSurveyCreation() 
    {
        teaminstancelist= new List<String>();
        List<Measure_Master__c> mm= [Select id,State__c,Team_Instance__c from Measure_Master__c where State__c='Complete' WITH SECURITY_ENFORCED];
        if(mm!=null && mm.size()>0)
        {
           for(Measure_Master__c measure:mm)
            {
                teaminstancelist.add(measure.Team_Instance__c);
            } 
        }
       
        SnTDMLSecurityUtil.printDebugMessage('++yo'+teaminstancelist);
        query='select id,Name from AxtriaSalesIQTM__Team_Instance__c where isVeevaSurveyScheduled__c = true and id not in :teaminstancelist WITH SECURITY_ENFORCED';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Team_Instance__c> scope) 
    {
        //List<ScheduledVeevaTeamInstances__c> scheduledTIList  = [select id from ScheduledVeevaTeamInstances__c]; 
        List<ScheduledTeamInstances__c> scheduledTIList  = [select id from ScheduledTeamInstances__c where Type__c = 'Veeva']; 
        
        SnTDMLSecurityUtil.printDebugMessage('scheduledTIList size()-->'+scheduledTIList.size());
        if(scheduledTIList!=null && scheduledTIList.size()>0)
        {
            //delete scheduledTIList;
            SnTDMLSecurityUtil.deleteRecords(scheduledTIList, 'BatchVeevaSurveyCreation');
        }
        //scheduledTIList  = new List<ScheduledVeevaTeamInstances__c>();
        scheduledTIList  = new List<ScheduledTeamInstances__c>();
        for(AxtriaSalesIQTM__Team_Instance__c ti: scope)
        {
            //ScheduledVeevaTeamInstances__c sti = new ScheduledVeevaTeamInstances__c(Name=ti.Name);
            ScheduledTeamInstances__c sti = new ScheduledTeamInstances__c(Name=ti.Name);
            sti.Team_Instance_Name__c = ti.id;
            sti.Type__c = 'Veeva';
            scheduledTIList.add(sti);
        }
        SnTDMLSecurityUtil.printDebugMessage('++scheduledTIList'+scheduledTIList);
        if(scheduledTIList!=null && scheduledTIList.size()>0)
        {
            SnTDMLSecurityUtil.printDebugMessage('scheduledTIList size to be inserted-->'+scheduledTIList.size());
            //insert scheduledTIList;  
            SnTDMLSecurityUtil.insertRecords(scheduledTIList, 'BatchVeevaSurveyCreation');             
            teamInstName = scheduledTIList[0].Name;
            SnTDMLSecurityUtil.printDebugMessage('+++teamInstName'+teamInstName);
            //delete scheduledTIList[0]; 
            SnTDMLSecurityUtil.deleteRecords(scheduledTIList[0], 'BatchVeevaSurveyCreation');                    
        }
    }

    global void finish(Database.BatchableContext BC) 
    {
        if(!System.Test.isRunningTest())
            {
                SnTDMLSecurityUtil.printDebugMessage('+++teamInstName'+teamInstName);
                Survey_Def_Prof_Para_Meta_Insert_Utility svsd = new Survey_Def_Prof_Para_Meta_Insert_Utility(teamInstName,true,'cust2');
                Database.executeBatch(svsd,500);
            }    
    }
}