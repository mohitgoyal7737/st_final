public with sharing class Measure_Master_Trigger_Handler
{

    //Removed occurance of Line__c due to object purge activity A1422
    //Map<string,string> successErrorMap;
    //Public AxtriaSalesIQTM__Country__c Country {get;set;}

    //removed occurance of business unit due to object purge A1422

    public Measure_Master_Trigger_Handler()
    {

        // Country

    }

    public static void businessRule(List<Measure_Master__c> allRecords)
    {

        Integer executeIndex = -1, publishIndex = -1, publishAlignmentIndex = -1;
        Integer batchSize = 500;
        List<Batch_Size_Configuration__mdt> batch_size_config = new List<Batch_Size_Configuration__mdt>();

        try
        {
            batch_size_config = [SELECT Batch_Class_Name__c, Batch_Size__c FROM Batch_Size_Configuration__mdt WHERE Batch_Class_Name__c = 'BatchExecuteRuleEngine'];
        }
        catch(System.QueryException qe)
        {
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }
        if(batch_size_config.size() > 0)
        {
            batchSize = (Integer)batch_size_config[0].Batch_Size__c;
        }
        SnTDMLSecurityUtil.printDebugMessage('---batchSize----' + batchSize);
        //To Get First Record having State In Queue
        for(Integer i = 0; i < allRecords.size(); i++)
        {
            if(allRecords[i].State__c == 'In Queue' || allRecords[i].State__c == 'Executed' || allRecords[i].State__c == 'Execution Failed' || allRecords[i].State__c == 'Executing')
            {
                executeIndex = i;
                break;
            }
        }
        //To Get First Record having State In Publish Queue
        for(Integer j = 0; j < allRecords.size(); j++)
        {
            if(allRecords[j].State__c == 'In Publish Queue' || allRecords[j].State__c == 'Complete' || allRecords[j].State__c == 'Publishing Failed')
            {
                publishIndex = j;
                break;
            }
        }

        for(Integer k = 0; k < allRecords.size(); k++)
        {
            if(allRecords[k].State__c == 'In Publish To Alignment Queue' || allRecords[k].State__c == 'Published to Alignment' || allRecords[k].State__c == 'Error: Publish to Alignment')
            {
                publishAlignmentIndex = k;
                break;
            }
        }

        //Process queue mechanism for Execute state
        if(executeIndex != -1)
        {

            List<Measure_Master__c>  ruleExecute, executingRules;
            try
            {
                ruleExecute = [SELECT Id, Name, Team_Instance__c, Team_Instance__r.Name, Brand_Lookup__c, State__c, Team__r.Name, Country__c, IsContinueProcessing__c, Measure_Type__c FROM   Measure_Master__c WHERE Id = : allRecords[executeIndex].ID WITH SECURITY_ENFORCED];

                executingRules = [SELECT Id, Name, Country__c, Team_Instance__c, Team_Instance__r.Name, Brand_Lookup__c, State__c, Team__r.Name, IsContinueProcessing__c FROM  Measure_Master__c WHERE Country__c = :ruleExecute[0].Country__c and State__c = 'Executing' and Rule_Type__c != 'MCCP' WITH SECURITY_ENFORCED]; //Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c
            }
            catch(System.QueryException qe)
            {
                SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
            }

            //Shivansh - A1450 -- If the same batch is already executing
            if(executingRules.size() > 0 && ruleExecute.size() > 0)
            {
                Boolean flag = false;
                for(Measure_Master__c mm : executingRules)
                {
                    if(mm.Id == ruleExecute[0].Id)
                    {
                        flag = true;
                    }
                }

                if(flag)
                {
                    if(ruleExecute[0].IsContinueProcessing__c == true)
                    {
                        String WhereClause = ' Product__c =\'' + ruleExecute[0].Brand_Lookup__c + '\' AND Team_Instance__c =\'' + ruleExecute[0].Team_Instance__c + '\' ';
                        WhereClause += ' AND Type__c =\'' + ruleExecute[0].Measure_Type__c + '\' '; //Shivansh - A1450 - Adding type filter for BU Response in Where Clause
                        BatchExecuteRuleEngine batchExecute = new BatchExecuteRuleEngine(ruleExecute[0].Id, WhereClause);
                        Database.executeBatch(batchExecute, batchSize);
                    }
                }
            }
            else if(executingRules == NULL || executingRules.size() == 0)
            {

                List<Enqueue_Business_Rule_Job__c> enqueue;
                try
                {
                    enqueue = [SELECT Id, Market__c, Jobs_Id__c FROM Enqueue_Business_Rule_Job__c WHERE Market__c = :ruleExecute[0].Country__c AND State_Type__c = 'Execute' WITH SECURITY_ENFORCED];
                }
                catch(System.QueryException qe)
                {
                    SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
                }
                Measure_Master__c ruleBusiness;

                SnTDMLSecurityUtil.printDebugMessage('++++++++++ Hey Enqueue is ' + enqueue);
                if(enqueue != NULL && enqueue.size() > 0 && enqueue[0].Jobs_Id__c != null && enqueue[0].Jobs_Id__c.length() > 0)
                {
                    List<String> enqueueJobs = enqueue[0].Jobs_Id__c.split(',');

                    SnTDMLSecurityUtil.printDebugMessage('++++++++++ Hey Enqueue Jobs are ' + enqueueJobs);

                    if(enqueueJobs != null && enqueueJobs.size() > 0)
                    {

                        try
                        {
                            ruleBusiness = [SELECT Id, Name, Team_Instance__c, Team_Instance__r.Name, Brand_Lookup__c, State__c, Team__r.Name, Measure_Type__c, IsContinueProcessing__c FROM Measure_Master__c WHERE Id = : enqueueJobs[0] WITH SECURITY_ENFORCED];
                        }
                        catch(System.QueryException qe)
                        {
                            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
                        }

                        List<BU_Response__c> BUResponseCount;
                        try
                        {
                            BUResponseCount = Database.query('SELECT id FROM BU_Response__c WHERE Product__c =\'' + ruleBusiness.Brand_Lookup__c + '\' AND Team_Instance__c =\'' + ruleBusiness.Team_Instance__c + '\' WITH SECURITY_ENFORCED LIMIT 1');
                        }
                        catch(System.QueryException qe)
                        {
                            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
                        }

                        if(BUResponseCount.size() == 1)
                        {
                            String WhereClause = ' Product__c =\'' + ruleBusiness.Brand_Lookup__c + '\' AND Team_Instance__c =\'' + ruleBusiness.Team_Instance__c + '\' ';
                            WhereClause += ' AND Type__c =\'' + ruleBusiness.Measure_Type__c + '\' '; //Shivansh - A1450 - Adding type filter for BU Response in Where Clause
                            ruleBusiness.State__c = 'Executing';

                            //update ruleBusiness;
                            SnTDMLSecurityUtil.updateRecords(ruleBusiness, 'Measure_Master_Trigger_Handler');
                            /* Shivansh - A1450
                               To reset all the steps used for quantiling
                            */
                            List<Step__c> steps;
                            try
                            {
                                steps = [SELECT Id, isQuantileComputed__c FROM Step__c WHERE Measure_Master__c = :enqueueJobs[0] and Step_Type__c = 'Quantile' WITH SECURITY_ENFORCED];
                            }
                            catch(System.QueryException qe)
                            {
                                SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
                            }

                            for(Step__c step : steps)
                            {
                                step.isQuantileComputed__c = false;
                            }
                            if(steps.size() > 0)
                            {
                                //update steps;
                                SnTDMLSecurityUtil.updateRecords(steps, 'Measure_Master_Trigger_Handler');
                            }

                            /* Shivansh - A1450 till here */

                            if(ruleBusiness.IsContinueProcessing__c == true)
                            {
                                BatchExecuteRuleEngine batchExecute = new BatchExecuteRuleEngine(ruleBusiness.id, WhereClause);
                                Database.executeBatch(batchExecute, batchSize);
                            }
                            else
                            {
                                BatchDeleteRecsBeforereExecute batchExecute = new BatchDeleteRecsBeforereExecute(ruleBusiness.id, WhereClause );
                                Database.executeBatch(batchExecute, 2000);
                            }
                        }
                        else
                        {
                            ruleBusiness.State__c = 'No Data Found';
                            //update ruleBusiness;
                            SnTDMLSecurityUtil.updateRecords(ruleBusiness, 'Measure_Master_Trigger_Handler');
                        }
                        SnTDMLSecurityUtil.printDebugMessage('++++++++++ Hey Running ' + ruleBusiness);
                        String leftJobs = '';

                        List<String> enqueuedJobsss = enqueue[0].Jobs_Id__c.split(',');

                        for(Integer i = 1; i < enqueuedJobsss.size() ; i++)
                        {
                            leftJobs += enqueuedJobsss[i];
                            if(i < enqueuedJobsss.size() - 1)
                            {
                                leftJobs += ',';
                            }
                        }

                        enqueue[0].Jobs_Id__c = leftJobs;
                        //update enqueue;
                        SnTDMLSecurityUtil.updateRecords(enqueue, 'Measure_Master_Trigger_Handler');
                    }

                }
            }

        }
        else if(publishIndex != -1)
        {
            //Process queue mechanism for Publish State
            //fetch Rule
            List<Measure_Master__c>  rulePublish = [SELECT Id, Name, Team_Instance__c, Team_Instance__r.Name, Brand_Lookup__c, State__c, Team__r.Name, Country__c FROM   Measure_Master__c WHERE Id = : allRecords[publishIndex].ID WITH SECURITY_ENFORCED];

            List<Measure_Master__c> publishingRules = [SELECT Id, Name, Country__c, Team_Instance__c, Team_Instance__r.Name, Brand_Lookup__c, /*Business_Unit_Loopup__c,*/ /*Line_2__c, Cycle__c, */State__c, Team__r.Name FROM  Measure_Master__c WHERE Country__c = :rulePublish[0].Country__c and State__c = 'Publishing' WITH SECURITY_ENFORCED]; //Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c

            if(publishingRules == NULL || publishingRules.size() == 0)
            {

                List<Enqueue_Business_Rule_Job__c> enqueue = [SELECT Id, Market__c, Jobs_Id__c FROM Enqueue_Business_Rule_Job__c WHERE Market__c = :rulePublish[0].Country__c AND State_Type__c = 'Publish' WITH SECURITY_ENFORCED];
                Measure_Master__c ruleBusiness;

                SnTDMLSecurityUtil.printDebugMessage('++++++++++ Hey Enqueue is ' + enqueue);
                if(enqueue != NULL && enqueue.size() > 0 && enqueue[0].Jobs_Id__c != null && enqueue[0].Jobs_Id__c.length() > 0)
                {
                    List<String> enqueueJobs = enqueue[0].Jobs_Id__c.split(',');

                    SnTDMLSecurityUtil.printDebugMessage('++++++++++ Hey Enqueue Jobs are ' + enqueueJobs);

                    if(enqueueJobs != null && enqueueJobs.size() > 0)
                    {

                        //Change state of First Job in queue FROM 'In Publish Queue' to 'Publishing'
                        ruleBusiness = [SELECT Id, Name, Team_Instance__c, Team_Instance__r.Name, Brand_Lookup__c, State__c, Team__r.Name FROM Measure_Master__c WHERE Id = : enqueueJobs[0] WITH SECURITY_ENFORCED]; //Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c


                        //Edited by Ayushi

                        //BatchPublishCallPlan batchExecute = new BatchPublishCallPlan(ruleBusiness.Id, ' Measure_Master__c =\''+ruleBusiness.Id+'\' ' );
                        Measure_Master__c rule = [SELECT Id, State__c FROM Measure_Master__c WHERE Id = :ruleBusiness.Id WITH SECURITY_ENFORCED];
                        rule.State__c = 'Publishing';
                        //update rule;
                        SnTDMLSecurityUtil.updateRecords(rule, 'Measure_Master_Trigger_Handler');

                        SnTDMLSecurityUtil.printDebugMessage('rule::::::::::::::::::' + rule);
                        //Database.executeBatch(batchExecute, 1500);

                        deleteExistedCallPlan deleteBatch = new deleteExistedCallPlan(ruleBusiness.Id);
                        Database.executeBatch(deleteBatch, 1500);

                        //Till here.......


                        //Removal of first jobId FROM enqueue
                        SnTDMLSecurityUtil.printDebugMessage('++++++++++ Hey Running ' + ruleBusiness);
                        String leftJobs = '';

                        List<String> enqueuedJobsss = enqueue[0].Jobs_Id__c.split(',');

                        for(Integer i = 1; i < enqueuedJobsss.size() ; i++)
                        {
                            leftJobs += enqueuedJobsss[i] + ',';
                        }

                        enqueue[0].Jobs_Id__c = leftJobs;
                        //update enqueue;
                        SnTDMLSecurityUtil.updateRecords(enqueue, 'Measure_Master_Trigger_Handler');
                    }
                }
            }//else Nothing to do Because there is already a rule with state 'Publishing'
        }//else Nothing to do because State of records is neither 'In Queue' nor 'In Publish Queue'
        else if(publishAlignmentIndex != -1)  //Process queue mechanism for Publish Alignment State
        {

            //fetch Rule
            List<Measure_Master__c>  rulePublishAlignment = [SELECT Id, Name, Team_Instance__c, Team_Instance__r.Name, Brand_Lookup__c, State__c, Team__r.Name, Country__c FROM   Measure_Master__c WHERE Id = : allRecords[publishAlignmentIndex].ID WITH SECURITY_ENFORCED];

            List<Measure_Master__c> publishingAlignmentRules = [SELECT Id, Name, Country__c, Team_Instance__c, Team_Instance__r.Name, Brand_Lookup__c, State__c, Team__r.Name FROM  Measure_Master__c WHERE Country__c = :rulePublishAlignment[0].Country__c and State__c = 'Publishing to Alignment' WITH SECURITY_ENFORCED];
            if(publishingAlignmentRules == NULL || publishingAlignmentRules.size() == 0)
            {

                List<Enqueue_Business_Rule_Job__c> enqueue = [SELECT Id, Market__c, Jobs_Id__c FROM Enqueue_Business_Rule_Job__c WHERE Market__c = :rulePublishAlignment[0].Country__c AND State_Type__c = 'Publish' WITH SECURITY_ENFORCED];
                Measure_Master__c ruleBusiness;

                SnTDMLSecurityUtil.printDebugMessage('++++++++++ Hey Enqueue is ' + enqueue);
                if(enqueue != NULL && enqueue.size() > 0 && enqueue[0].Jobs_Id__c != null && enqueue[0].Jobs_Id__c.length() > 0)
                {
                    List<String> enqueueJobs = enqueue[0].Jobs_Id__c.split(',');

                    SnTDMLSecurityUtil.printDebugMessage('++++++++++ Hey Enqueue Jobs are ' + enqueueJobs);

                    if(enqueueJobs != null && enqueueJobs.size() > 0)
                    {

                        //Change state of First Job in queue FROM 'In Publish Queue' to 'Publishing'
                        ruleBusiness = [SELECT Id, Name, Team_Instance__c, Team_Instance__r.Name, Brand_Lookup__c, State__c, Team__r.Name FROM Measure_Master__c WHERE Id = : enqueueJobs[0] WITH SECURITY_ENFORCED];

                        //Edited by Ayushi

                        //BatchPublishCallPlan batchExecute = new BatchPublishCallPlan(ruleBusiness.Id, ' Measure_Master__c =\''+ruleBusiness.Id+'\' ' );
                        Measure_Master__c rule = [SELECT Id, State__c FROM Measure_Master__c WHERE Id = :ruleBusiness.Id WITH SECURITY_ENFORCED];
                        rule.State__c = 'Publishing to Alignment';
                        //update rule;
                        SnTDMLSecurityUtil.updateRecords(rule, 'Measure_Master_Trigger_Handler');


                        SnTDMLSecurityUtil.printDebugMessage('rule::::::::::::::::::' + rule);

                        Database.executeBatch(new BatchEmptyDataInMappedFieldsInPosAcc(ruleBusiness.Team_Instance__c, ruleBusiness.ID, ruleBusiness.Brand_Lookup__c), 2000);

                        //Removal of first jobId FROM enqueue
                        SnTDMLSecurityUtil.printDebugMessage('++++++++++ Hey Running ' + ruleBusiness);
                        String leftJobs = '';

                        List<String> enqueuedJobsss = enqueue[0].Jobs_Id__c.split(',');

                        for(Integer i = 1; i < enqueuedJobsss.size() ; i++)
                        {
                            leftJobs += enqueuedJobsss[i] + ',';
                        }

                        enqueue[0].Jobs_Id__c = leftJobs;
                        //update enqueue;
                        SnTDMLSecurityUtil.updateRecords(enqueue, 'Measure_Master_Trigger_Handler');
                    }
                }
            }//else Nothing to do Because there is already a rule with state 'Publishing'
        }
    }

    public static void mccpBusinessRule(List<Measure_Master__c> oldRuleList , List<Measure_Master__c> updatedRuleList){

        SnTDMLSecurityUtil.printDebugMessage('oldRuleList :: '+oldRuleList);
        SnTDMLSecurityUtil.printDebugMessage('updatedRuleList :: '+updatedRuleList);

        try{
            for(Integer i=0;i<updatedRuleList.size();i++){
                for(Integer j=0;j<oldRuleList.size();j++){
                    if(updatedRuleList[i].Id == oldRuleList[j].Id && updatedRuleList[i].is_promoted__c && !oldRuleList[j].is_promoted__c){
                        deleteExistedCallPlan deleteBatch = new deleteExistedCallPlan(updatedRuleList[i].Id);
                        Database.executeBatch(deleteBatch, 1500);
                    }
                }
            }
        }catch(Exception e){
            SnTDMLSecurityUtil.printDebugMessage('Exception in mccpBusinessRule :: '+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('StackTraceing :: '+e.getStackTraceString());
        }
    }
}