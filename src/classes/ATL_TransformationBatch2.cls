global class ATL_TransformationBatch2 implements Database.Batchable<sObject> {
    
    public String query;
    List<String> allTeamInstances;
    public Map<String, Set<String>> accsToTerrMap;
    public List<String> allAccs;
    public Map<String,String> accToVeevaID;
    public Map<String, String> accToCountryID;

    global ATL_TransformationBatch2(List<String> allCountries) {

       /* allTeamInstances = new LIst<String>();
        
        List<AxtriaSalesIQTM__Team_Instance__c> allTeams = [select id from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c in :allCountries and (AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Live' or AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Published')];

        for(AxtriaSalesIQTM__Team_Instance__c ti : allTeams)
        {   
            allTeamInstances.add(ti.ID);
        }
        
        this.query = 'select AxtriaSalesIQTM__Account__c, AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__Country__c, AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Account__r.AZ_VeevaID__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Position__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances and AxtriaSalesIQTM__Assignment_Status__c = \'Active\' OR AxtriaSalesIQTM__Assignment_Status__c = \'Future Active\'order by AxtriaSalesIQTM__Account__c';

        accsToTerrMap= new Map<String, Set<String>>();
        accToVeevaID = new Map<String, String>();
        accToCountryID = new Map<String, String>();
        allAccs = new List<String>();*/
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Account__c> scope) 
    {    
        /*for(AxtriaSalesIQTM__Position_Account__c posAcc : scope)
        {
            allAccs.add(posAcc.AxtriaSalesIQTM__Account__c);
        }

        List<Staging_ATL__c> stagingRecs = [select id, Account__c, Axtria_Account_ID__c,Country__c, Territory__c from Staging_ATL__c where Account__c in :allAccs and Status__c = 'Updated'];

        if(stagingRecs.size() > 0)
        {
            for(Staging_ATL__c sa : stagingRecs)
            {
                accsToTerrMap.put(sa.Axtria_Account_ID__c, new Set<String>(sa.Territory__c.split(';')));
                accToVeevaID.put(sa.Axtria_Account_ID__c, sa.Account__c);
                accToCountryID.put(sa.Axtria_Account_ID__c, sa.Country__c);
            }
        }

        Set<String> accsInTerr;

        for(AxtriaSalesIQTM__Position_Account__c posAcc : scope)
        {

            if(accsToTerrMap.containsKey(posAcc.AxtriaSalesIQTM__Account__c))
            {
                accsInTerr = new Set<String>(accsToTerrMap.get(posAcc.AxtriaSalesIQTM__Account__c));
                accsInTerr.add(posAcc.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
            }
            else
            {
                accToVeevaID.put(posAcc.AxtriaSalesIQTM__Account__c, posAcc.AxtriaSalesIQTM__Account__r.AZ_VeevaID__c);
                accsInTerr = new Set<String>();
                accsInTerr.add(posAcc.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
            }
            accsToTerrMap.put(posAcc.AxtriaSalesIQTM__Account__c, accsInTerr);
        }

        List<Staging_ATL__c> allATLrecs = new List<Staging_ATL__c>();

        for(String accID : accsToTerrMap.keySet())
        {
           Staging_ATL__c atlRec = new Staging_ATL__c();
           atlRec.Account__c = accToVeevaID.get(accID);
           atlRec.Axtria_Account_ID__c = accID;
           atlRec.Territory__c = string.join(new List<String>(accsToTerrMap.get(accID)),';');
           
           if(atlRec.Account__c != null)
           {
               atlRec.External_ID__c = atlRec.Account__c;
               atlRec.Country__c = accToCountryID.get(accID);
               atlRec.Status__c = 'Updated';
               allATLrecs.add(atlRec);
               
           }
        }

        upsert allATLrecs External_ID__c;*/
    }

    global void finish(Database.BatchableContext BC) {
        //Database.executeBatch(new ATL_Transformation_null_Accounts());
    }
}