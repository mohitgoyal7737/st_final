@isTest
public class LoggerNotification_Test {
    public static testMethod Void method1(){
        Logger_Email__c LE =new Logger_Email__c(name='sivagopi.j@gmail.com');
        insert LE;
        Scheduler_Log__c sl1 = new Scheduler_Log__c();
        sl1.Changes__c= 'NO';
        sl1.Cycle__c = '2018SC3';
        sl1.Job_Type__c = 'Inbound';
        sl1.Job_Name__c = 'AccountDelta';
        sl1.Job_Status__c = 'Success';
        sl1.Object_Name__c = 'Account';
        sl1.No_of_Records_Processed__c = 10;
        sl1.Total_Errors__c = 8;
        insert Sl1;
        Scheduler_Log__c sl2 = new Scheduler_Log__c();
        sl2.Changes__c= 'NO';
        sl2.Cycle__c = '2018SC3';
        sl2.Job_Type__c = 'Outbound';
        sl2.Job_Name__c = 'AccountDelta';
        sl2.Job_Status__c = 'Success';
        sl2.Object_Name__c = 'Account';
        sl2.No_of_Records_Processed__c = 10;
        sl2.Total_Errors__c = 8;
        insert Sl2;
        test.startTest();
        LoggerNotification logno = new LoggerNotification();
        logno.sendEmailsInbound();
    }

}