global with sharing class UpdatePosGeography implements Database.Batchable<sObject>, Database.Stateful
{
    /*Replaced temp_Zip_Terr__c with temp_Obj__c due to object purge*/

    global string query;
    global string teamID;
    String Ids;
    Boolean flag = true;
    public integer recordsProcessed;
    public Integer recordsCreated;
    global string teamInstance;
    public list<AxtriaSalesIQTM__Team_Instance__c> teminslst = new list<AxtriaSalesIQTM__Team_Instance__c>();
    public list<AxtriaSalesIQTM__Position_Team_Instance__c> posTI = new list<AxtriaSalesIQTM__Position_Team_Instance__c>();

    global list<temp_Obj__c> zipTerrlist = new list<temp_Obj__c>();
    public List<AxtriaSalesIQTM__Change_Request__c> cr = new List<AxtriaSalesIQTM__Change_Request__c>();

    //Added by HT(A0994) on 17th June 2020
    global String changeReqID;
    global Boolean flagValue;

    global UpdatePosGeography(String Team, String TeamIns)
    {
        teamInstance = TeamIns;
        teamID = Team;
        recordsProcessed = 0;
        recordsCreated = 0;
        teminslst = [select id, AxtriaSalesIQTM__IC_EffEndDate__c, AxtriaSalesIQTM__IC_EffstartDate__c from AxtriaSalesIQTM__Team_Instance__c where id = :teamInstance];
        query = 'SELECT id,Geography__c,Position__c,Territory_ID__c,Zip_Name__c,Team_Name__c, Position__r.AxtriaSalesIQTM__inactive__c,Change_Request__c, isError__c, SalesIQ_Error_Message__c, Error_message__c FROM temp_Obj__c where Team__c=:teamID and Team_Instance__c=:teamInstance and Status__c = \'New\' and Event__c = \'Insert\' and Object__c =\'Zip_Terr\'';
    }

    global UpdatePosGeography(String Team, String TeamIns, String Ids)
    {
        teamInstance = TeamIns;
        teamID = Team;
        changeReqID = Ids;
        recordsProcessed = 0;
        recordsCreated = 0;
        this.Ids = Ids;
        teminslst = [select id, AxtriaSalesIQTM__IC_EffEndDate__c, AxtriaSalesIQTM__IC_EffstartDate__c 
                        from AxtriaSalesIQTM__Team_Instance__c where id = :teamInstance WITH SECURITY_ENFORCED];
        cr = [Select Id, AxtriaSalesIQTM__Request_Type_Change__c,Records_Created__c from 
                AxtriaSalesIQTM__Change_Request__c where id = :IDs WITH SECURITY_ENFORCED];

        query = 'SELECT id,Geography__c,Position__c,Territory_ID__c,Zip_Name__c,Team_Name__c,'+
                'Position__r.AxtriaSalesIQTM__inactive__c,Change_Request__c,isError__c,'+
                'SalesIQ_Error_Message__c, Error_message__c,Event__c FROM temp_Obj__c '+
                'where Team__c=:teamID and Team_Instance__c=:teamInstance and '+
                'Status__c = \'New\' and Event__c = \'Insert\' and Object__c =\'Zip_Terr\'';       
    }

    //Added by HT(A0994) on 17th June 2020
    global UpdatePosGeography(String Team, String TeamIns, String Ids,Boolean flag)
    {
        teamInstance = TeamIns;
        teamID = Team;
        changeReqID = Ids;
        flagValue = flag;
        recordsProcessed = 0;
        recordsCreated = 0;
        this.Ids = Ids;
        teminslst = [select id, AxtriaSalesIQTM__IC_EffEndDate__c, AxtriaSalesIQTM__IC_EffstartDate__c 
                        from AxtriaSalesIQTM__Team_Instance__c where id = :teamInstance WITH SECURITY_ENFORCED];
        cr = [Select Id, AxtriaSalesIQTM__Request_Type_Change__c,Records_Created__c from 
                AxtriaSalesIQTM__Change_Request__c where id = :IDs WITH SECURITY_ENFORCED];

        //Changed by HT(A0994) on 17th June 2020         
        /*query = 'SELECT id,Geography__c,Position__c,Territory_ID__c,Zip_Name__c,Team_Name__c,'+
                'Position__r.AxtriaSalesIQTM__inactive__c,Change_Request__c,isError__c,'+
                'SalesIQ_Error_Message__c, Error_message__c,Event__c FROM temp_Obj__c '+
                'where Team__c=:teamID and Team_Instance__c=:teamInstance and '+
                'Status__c = \'New\' and Event__c = \'Insert\' and Object__c =\'Zip_Terr\'';*/
        query = 'SELECT id,Geography__c,Position__c,Territory_ID__c,Zip_Name__c,Team_Name__c,'+
                'Position__r.AxtriaSalesIQTM__inactive__c,Change_Request__c,isError__c,'+
                'SalesIQ_Error_Message__c, Error_message__c,Event__c FROM temp_Obj__c '+
                'where Team__c=:teamID and Team_Instance__c=:teamInstance and '+
                'Status__c = \'New\' and Event__c = \'Insert\' and Object__c =\'Zip_Terr\' and '+
                'Change_Request__c =: changeReqID WITH SECURITY_ENFORCED';        
    }

    global Database.Querylocator start(Database.BatchableContext bc)
    {
        AxtriaSalesIQTM__TriggerContol__c obj = [select id, AxtriaSalesIQTM__IsStopTrigger__c from AxtriaSalesIQTM__TriggerContol__c where name = 'PositionGeographyTrigger'];
        obj.AxtriaSalesIQTM__IsStopTrigger__c = true;
        update obj;
        //SnTDMLSecurityUtil.updateRecords(obj, 'UpdatePosGeography');
        return Database.getQueryLocator(query);
    }

    global void execute (Database.BatchableContext BC, List<temp_Obj__c>zipTerrlist)
    {
        recordsCreated+=zipTerrlist.size();
        list<String> list1 = new list<String>();
        list<String> list2 = new list<String>();
        set<string> setPosGeoTeamInstance  = new set<string>();
        list<temp_Obj__c> lsTempRecordToUpdate = new list<temp_Obj__c>();

        for(temp_Obj__c rec : zipterrlist)
        {
            if(rec.Geography__c != null)
            {
                list1.add(rec.Geography__c);
            }
            if(rec.Position__c != null)
            {
                list2.add(rec.Position__c);
            }
        }

        //query existing position Geography with the key = Position+Geography+TeamInstanceName
        list<AxtriaSalesIQTM__Position_Geography__c> lsposGeo = [select id, name, AxtriaSalesIQTM__Assignment_Status__c, AxtriaSalesIQTM__Geography__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Position__c from AxtriaSalesIQTM__Position_Geography__c where AxtriaSalesIQTM__Position__c in:list2 and AxtriaSalesIQTM__Geography__c in:list1 and AxtriaSalesIQTM__Assignment_Status__c = 'Active' and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__inactive__c != True];

        for(AxtriaSalesIQTM__Position_Geography__c pg : lsposGeo)
        {
            setPosGeoTeamInstance.add(string.valueof(pg.AxtriaSalesIQTM__Position__c) + string.valueof(pg.AxtriaSalesIQTM__Geography__c) + string.valueof(pg.AxtriaSalesIQTM__Team_Instance__c));
        }

        map<string, string> posmap = new map<string, string>();
        for(AxtriaSalesIQTM__Position_Team_Instance__c posTI : [select AxtriaSalesIQTM__Position_ID__c, id from AxtriaSalesIQTM__Position_Team_Instance__c where AxtriaSalesIQTM__Position_ID__c in: list2 and  AxtriaSalesIQTM__Team_Instance_ID__c = :teamInstance])
        {
            posmap.put(posTI.AxtriaSalesIQTM__Position_ID__c, posTI.id);
        }
        /*map<string,string> geomap=new map<string,string>();
        for(AxtriaSalesIQTM__Team_Instance_Geography__c geoTI:[select AxtriaSalesIQTM__Geography__c,id from AxtriaSalesIQTM__Team_Instance_Geography__c where AxtriaSalesIQTM__Geography__c in :list1 and AxtriaSalesIQTM__Team_Instance__c=:teamInstance])
        {
            geomap.put(geoTI.AxtriaSalesIQTM__Geography__c,geoTI.id);
        }*/

        list<AxtriaSalesIQTM__Position_Geography__c> posgeoListUpdate = new list<AxtriaSalesIQTM__Position_Geography__c>();

        for(temp_Obj__c rec : zipTerrlist)
        {
            if(String.isNotBlank(rec.Territory_ID__c) && String.isNotBlank(rec.Zip_Name__c) && String.isNotBlank(rec.Team_Name__c) && String.isNotBlank(rec.Event__c)){
                if(rec.Geography__c != null && rec.Position__c != null)
                {
                    if(!setPosGeoTeamInstance.contains(string.valueof(rec.Position__c) + string.valueof(rec.Geography__c) + string.valueof(teamInstance)))
                    {
                        if(rec.Position__r.AxtriaSalesIQTM__inactive__c != True)
                        {
                            AxtriaSalesIQTM__Position_Geography__c obj = new AxtriaSalesIQTM__Position_Geography__c();

                            obj.AxtriaSalesIQTM__Geography__c                = rec.Geography__c;
                            obj.AxtriaSalesIQTM__Position__c                 = rec.Position__c;
                            obj.AxtriaSalesIQTM__Team_Instance__c            = teamInstance;
                            obj.AxtriaSalesIQTM__Position_Team_Instance__c   = posmap.get(rec.Position__c);
                            obj.AxtriaSalesIQTM__Effective_End_Date__c       = teminslst[0].AxtriaSalesIQTM__IC_EffEndDate__c;
                            obj.AxtriaSalesIQTM__Effective_Start_Date__c     = teminslst[0].AxtriaSalesIQTM__IC_EffstartDate__c;
                            obj.AxtriaSalesIQTM__Position_Id_External__c     = rec.Position__c;
                            obj.AxtriaSalesIQTM__Change_Status__c            = 'No Change';
                            obj.AxtriaSalesIQTM__Proposed_Position__c        = rec.Position__c;

                            posgeoListUpdate.add(obj);
                            //Update Temp Record
                            rec.status__c = 'Processed';
                            rec.Change_Request__c = Ids; //A1422
                            rec.isError__c = false;//A1422
                            lsTempRecordToUpdate.add(rec);
                        }
                        else
                        {
                            rec.status__c = 'Rejected';
                            rec.Reason_Code__c = 'Inactive Position in SalesIQ';
                            rec.SalesIQ_Error_Message__c = 'Inactive Position in SalesIQ';
                            rec.Change_Request__c = Ids;//A1422
                            rec.isError__c = true;//A1422
                            lsTempRecordToUpdate.add(rec);
                        }
                    }
                    else
                    {
                        rec.status__c = 'Rejected';
                        rec.Reason_Code__c = 'Overlapping Assignment';
                        rec.SalesIQ_Error_Message__c = 'Overlapping Assignment';
                        rec.Change_Request__c = Ids;//A1422
                        rec.isError__c = true;//A1422
                        lsTempRecordToUpdate.add(rec);

                    }
                }
                else
                {
                    rec.status__c = 'Rejected';
                    rec.Reason_Code__c = 'Geography/Position/Team Instance does not exist in SalesIQ';
                    rec.SalesIQ_Error_Message__c = 'Geography/Position/Team Instance does not exist in SalesIQ';
                    rec.Change_Request__c = Ids;//A1422
                    rec.isError__c = true;//A1422
                    lsTempRecordToUpdate.add(rec);
                }    
            }
            else{
                rec.status__c = 'Rejected';
                rec.Reason_Code__c = 'Mandatory Field Missing or Incorrect Team Instance';
                rec.SalesIQ_Error_Message__c = 'Mandatory Field Missing or Incorrect Team Instance';
                rec.Change_Request__c = Ids;//A1422
                rec.isError__c = true;//A1422
                lsTempRecordToUpdate.add(rec);
            }
        }

        if(posgeoListUpdate.size() != 0)
        {
            //database.saveresult[] ds = Database.insert(posgeoListUpdate, false);
            //Changed for security review
            SObjectAccessDecision securityDecision = Security.stripInaccessible(AccessType.CREATABLE,posgeoListUpdate);
            //list<AxtriaSalesIQTM__Position_Geography__c> temp = securityDecision.getRecords();
            database.saveresult[] ds = Database.insert(securityDecision.getRecords(), false);
            //throw exception if permission is missing 
            if(!securityDecision.getRemovedFields().isEmpty() ){
               throw new SnTException(securityDecision, 'AxtriaSalesIQTM__Position_Geography__c', SnTException.OperationType.C ); 
            }
            //End of security review change
            for(database.SaveResult d : ds)
            {
                if(d.issuccess())
                {
                    recordsProcessed++;
                }
                else
                {
                    flag = false;
                }
            }
        }
        if(lsTempRecordToUpdate.size() != 0)
        {
            //update lsTempRecordToUpdate;
            SnTDMLSecurityUtil.updateRecords(lsTempRecordToUpdate, 'UpdatePosGeography');
        }
        system.debug('******************************upsert done************************************');
    }

    global void finish(Database.BatchableContext BC)
    {
        Boolean noJobErrors;
        String changeReqStatus;

        AxtriaSalesIQTM__TriggerContol__c obj1 = [select id, AxtriaSalesIQTM__IsStopTrigger__c from AxtriaSalesIQTM__TriggerContol__c where name = 'PositionGeographyTrigger'];
        obj1.AxtriaSalesIQTM__IsStopTrigger__c = false;
        update obj1;
        //SnTDMLSecurityUtil.updateRecords(obj1, 'UpdatePosGeography');
        
        if(Ids != null)
        {
            AxtriaSalesIQTM__Change_Request__c changerequest = new AxtriaSalesIQTM__Change_Request__c();

            noJobErrors = ST_Utility.getJobStatus(BC.getJobId());
            changeReqStatus = flag && noJobErrors ? 'Done' : 'Error';
             
                // Changes On Sep relase 2020
                // AR-1693 and SAL 714 and Changed By A1930
                
               if(changeReqStatus == 'Done'){
                    Database.executeBatch(new updatePosGeoAZ( teamInstance ));
               }

            /*if(flag && ST_Utility.getJobStatus(BC.getJobId()))
            {
                changerequest.Job_Status__c = 'Done';
            }
            else
            {
                changerequest.Job_Status__c = 'Error';
            }*/
            changerequest.Records_Updated__c = recordsProcessed;
            changerequest.Id = IDs;
            if(cr.size()>0 && cr[0].AxtriaSalesIQTM__Request_Type_Change__c == 'Data Load Backend'){
                changerequest.Records_Created__c = recordsCreated;
            }
            //update changerequest;
            SnTDMLSecurityUtil.updateRecords(changerequest, 'UpdatePosGeography');

            BatchUpdateTempObjRecsCR batchCall = new BatchUpdateTempObjRecsCR(Ids,true,'Mandatory Field Missing or Incorrect Team Instance',changeReqStatus);
            Database.executeBatch(batchCall,2000);
            //Direct_Load_Records_Status_Update.Direct_Load_Records_Status_Update(Ids,'UpdatePosGeography'); 
        }

        /*string selectedObject = 'temp_Obj__c';
        Database.executeBatch(new batchdelete3(teamID,selectedObject),1000);*/
    }
}