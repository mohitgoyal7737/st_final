global with sharing class Batch_Integration_TSF_NEW implements Database.Batchable<sObject>, Database.Stateful
{
    public String query;
    public List<String> allProcessedIds;
    public List<String> allTeamInstances;

    public List<SIQ_TSF_vod_O__c> allStagingTSF;
    public Boolean chain = false;
    public String LOAD_TYPE = 'TSF';
    public Map <String, List <String>> sourceToDestMapping = new Map <String, List <String>> ();


    global Batch_Integration_TSF_NEW(List<String> allTeamInstances)
    {
        this.allTeamInstances  = allTeamInstances;
        allProcessedIds = new List<String>();

        system.debug('+++++++++++ Hey All Team Instances are ' + allTeamInstances);
        //this.query = 'select id, AxtriaSalesIQTM__Segment10__c, P1__c,AxtriaSalesIQTM__Position__r.Employee1__c, AxtriaSalesIQTM__Position__r.Employee1__r.Name,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__r.Name, Segment__c, AxtriaSalesIQTM__Account__r.AZ_VeevaID__c , AxtriaSalesIQTM__Account__r.AccountNumber ,Adoption__c, Potential__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name, Country__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, Party_ID__r.Accessibility_Range__c, AxtriaSalesIQTM__Team_Instance__c,Accessibility_Range__c  from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c != null and AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances and AxtriaSalesIQTM__lastApprovedTarget__c = true';
        query = 'select id, AxtriaSalesIQTM__Segment10__c, P1__c,AxtriaSalesIQTM__Position__r.Employee1__c, AxtriaSalesIQTM__Position__r.Employee1__r.Name,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__r.Name, Segment__c, AxtriaSalesIQTM__Account__r.AZ_VeevaID__c , AxtriaSalesIQTM__Account__r.AccountNumber ,Adoption__c, Potential__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name, Country__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, Party_ID__r.Accessibility_Range__c, AxtriaSalesIQTM__Team_Instance__c,Accessibility_Range__c,';

        Set<String> fields_in_query_set = new Set<String>();
        sourceToDestMapping = ST_utility.fetchSourceToDestinationMapping(LOAD_TYPE, allTeamInstances);
        String additionalFields = '';
        if(sourceToDestMapping.keySet().size() > 0)
        {
            fields_in_query_set = ST_utility.fetchFieldsUsedInSOQL(query);
        }
        additionalFields = ST_utility.fetchSourceFieldsInSourceToDestinationObject(sourceToDestMapping, fields_in_query_set);

        if(additionalFields != '')
        {
            query += additionalFields;
        }
        query = query.removeEnd(',');
        query += ' from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c != null and AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances and AxtriaSalesIQTM__lastApprovedTarget__c = true';


        // Account_vod__c  Territory   AZ External Id  My Target   Accessibility';
    }

    global Batch_Integration_TSF_NEW(List<String> allTeamInstances, Boolean chaining)
    {

        chain = chaining;

        this.allTeamInstances  = allTeamInstances;
        allProcessedIds = new List<String>();

        system.debug('+++++++++++ Hey All Team Instances are ' + allTeamInstances);
        //this.query = 'select id, AxtriaSalesIQTM__Segment10__c, P1__c,AxtriaSalesIQTM__Position__r.Employee1__c,AxtriaSalesIQTM__Position__r.Employee1__r.Name,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__r.Name, Segment__c, AxtriaSalesIQTM__Account__r.AZ_VeevaID__c , AxtriaSalesIQTM__Account__r.AccountNumber ,Adoption__c, Potential__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name, Country__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, Party_ID__r.Accessibility_Range__c, AxtriaSalesIQTM__Team_Instance__c,Accessibility_Range__c  from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c != null and AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances and AxtriaSalesIQTM__lastApprovedTarget__c = true';
        query = 'select id, AxtriaSalesIQTM__Segment10__c, P1__c,AxtriaSalesIQTM__Position__r.Employee1__c,AxtriaSalesIQTM__Position__r.Employee1__r.Name,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__r.Name, Segment__c, AxtriaSalesIQTM__Account__r.AZ_VeevaID__c , AxtriaSalesIQTM__Account__r.AccountNumber ,Adoption__c, Potential__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name, Country__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, Party_ID__r.Accessibility_Range__c, AxtriaSalesIQTM__Team_Instance__c,Accessibility_Range__c,';

        Set<String> fields_in_query_set = new Set<String>();
        sourceToDestMapping = ST_utility.fetchSourceToDestinationMapping(LOAD_TYPE, allTeamInstances);
        String additionalFields = '';
        if(sourceToDestMapping.keySet().size() > 0)
        {
            fields_in_query_set = ST_utility.fetchFieldsUsedInSOQL(query);
        }
        additionalFields = ST_utility.fetchSourceFieldsInSourceToDestinationObject(sourceToDestMapping, fields_in_query_set);

        if(additionalFields != '')
        {
            query += additionalFields;
        }
        query = query.removeEnd(',');
        query += ' from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c != null and AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances and AxtriaSalesIQTM__lastApprovedTarget__c = true';

        // Account_vod__c  Territory   AZ External Id  My Target   Accessibility';
    }
    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        system.debug('+++++++++++ Hey All Team Instances are ' + allTeamInstances);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scope)
    {

        allStagingTSF = new List<SIQ_TSF_vod_O__c>();
        allProcessedIds = new List<String>();
        /* List<AxtriaSalesIQTM__Position__c> posTeamInstanceRecs = [select id, Original_Country_Code__c, AxtriaSalesIQTM__Team_Instance__r.Name ,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name, AxtriaSalesIQTM__Team_Instance__r.Increment_Field__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c , AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c  from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances and AxtriaSalesIQTM__Hierarchy_Level__c ='1' and (AxtriaSalesIQTM__Client_Position_Code__c != '000000' and AxtriaSalesIQTM__Client_Position_Code__c != '0' and AxtriaSalesIQTM__Client_Position_Code__c != '00000') and AxtriaSalesIQTM__Inactive__c = false]; */

        Set<String> allPositions = new Set<String>();

        System.debug('scope::::::' + scope);

        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pti : scope)
        {
            allPositions.add(pti.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
        }
        System.debug('allPositions::::::' + allPositions);


        List<AxtriaSalesIQTM__Position_Employee__c> uap = [select AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, Team_Instance_Name__c, AxtriaSalesIQTM__Employee__r.Employee_PRID__c, AxtriaSalesIQTM__Position__r.Employee1__r.Employee_PRID__c, AxtriaSalesIQTM__Position__r.Employee1__r.Employee_Name__c from AxtriaSalesIQTM__Position_Employee__c where AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c in :allPositions /*and  AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Hierarchy_Level__c = '1'*/ and (AxtriaSalesIQTM__Assignment_Status__c = 'Active' ) and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__IsMaster__c = true]; // AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Hierarchy_Level__c = '1' comment for SR-92

        Map<String, String> posTeamToUser = new Map<String, String>();

        for(AxtriaSalesIQTM__Position_Employee__c u : uap)
        {
            string teamInstancePos =  u.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
            posTeamToUser.put(teamInstancePos, u.AxtriaSalesIQTM__Position__r.Employee1__r.Employee_Name__c);
        }
        system.debug('++posTeamToUser' + posTeamToUser);

        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : scope)
        {
            SIQ_TSF_vod_O__c stc = new SIQ_TSF_vod_O__c();
            system.debug('pacp++' + pacp);
            string teamInstancePos = pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c; // Use Client Position Code for Territory ID
            //string userID = posTeamToUser.get(teamInstancePos);
            //if(pacp.AxtriaSalesIQTM__Position__r.Employee1__c != null)
            if(posTeamToUser.get(teamInstancePos) != null)
                stc.Name = posTeamToUser.get(teamInstancePos);
            else
                stc.Name = 'Vacant';//pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
            stc.Sync_Offline__c = false;
            //stc.Route__c=null;

            stc.SIQ_My_Target_vod__c = true;
            stc.SIQ_Account_Number__c = pacp.AxtriaSalesIQTM__Account__r.AccountNumber;
            stc.SIQ_Account_vod__c = pacp.AxtriaSalesIQTM__Account__r.AccountNumber;//pacp.AxtriaSalesIQTM__Account__r.AZ_VeevaID__c;
            stc.SIQ_Territory_vod__c = pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;

            if(pacp.Accessibility_Range__c != null)
            {
                stc.SIQ_Accessibility_AZ_EU__c = pacp.Accessibility_Range__c;
            }
            else
            {
                stc.SIQ_Accessibility_AZ_EU__c = pacp.Party_ID__r.Accessibility_Range__c;
            }
            /*if(pacp.Party_ID__r.Accessibility_Range__c !=null)
            stc.SIQ_Accessibility_AZ_EU__c = pacp.Party_ID__r.Accessibility_Range__c;*/
            stc.Team_Instance__c = pacp.AxtriaSalesIQTM__Team_Instance__c;
            stc.Team_Instance_Lookup__c = pacp.AxtriaSalesIQTM__Team_Instance__c;
            stc.Position_Account_Call_Plan__c = pacp.ID;
            stc.SIQ_External_ID_AZ__c = stc.SIQ_Account_Number__c + '__' + stc.SIQ_Territory_vod__c;
            stc.Status__c = 'Updated';

            try
            {
                for(String source_field : sourceToDestMapping.keySet())
                {
                    for(String dest_field : sourceToDestMapping.get(source_field))
                    {
                        String src_field = '', teamIns = '' ;
                        List<String> src_field_list = new List<String>();
                        if(String.isNotBlank(source_field))
                        {
                            src_field = source_field.split('-')[0];
                            teamIns = source_field.split('-')[1];
                            if(String.isNotBlank(src_field))
                            {
                                src_field_list = src_field.split('\\.');
                            }
                        }
                        if(teamIns == pacp.AxtriaSalesIQTM__Team_Instance__c)
                        {

                            if(src_field_list.size() == 1)
                                stc.put(dest_field, pacp.get(src_field));
                            else if(src_field_list.size() == 2)
                            {
                                stc.put(dest_field, pacp.getSObject(src_field_list[0]).get(src_field_list[1]));
                            }
                            else if(src_field_list.size() == 3)
                            {
                                stc.put(dest_field, pacp.getSObject(src_field_list[0]).getSObject(src_field_list[1]).get(src_field_list[2]));
                            }
                            else if(src_field_list.size() == 4)
                            {
                                stc.put(dest_field, pacp.getSObject(src_field_list[0]).getSObject(src_field_list[1]).getSObject(src_field_list[2]).get(src_field_list[3]));
                            }


                            SnTDMLSecurityUtil.printDebugMessage('dest field' + dest_field);
                            //SnTDMLSecurityUtil.printDebugMessage('source field value'+pacp.get(src_field));
                        }


                    }
                }
            }
            catch(Exception e)
            {
                SnTDMLSecurityUtil.printDebugMessage('Error in mapping ' + e.getMessage());

            }

            if(!allProcessedIds.contains(stc.SIQ_External_ID_AZ__c))
            {
                allStagingTSF.add(stc);
                allProcessedIds.add(stc.SIQ_External_ID_AZ__c);
            }
        }

        upsert allStagingTSF SIQ_External_ID_AZ__c;

    }

    global void finish(Database.BatchableContext BC)
    {
        database.executeBatch(new Batch_Integration_TSF_Extension_NEW(allTeamInstances, chain), 2000);
    }
}