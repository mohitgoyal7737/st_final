/**********************************************************************************************
@author     : Himanshu Tariyal (A0994)
@date       : 2nd July'2020
@description: Class for creating Rep Roster data in Staging_Rep_Roster__c object from data in
			  Position Employee and Employee object
Revison(s)  : v1.0
**********************************************************************************************/
global with sharing class BatchCreateRepRosterData implements Database.Batchable<sObject>,Database.Stateful
{
	global String batchName;
	global String jobType;
	global String query;
	global String posEmpQuery;
	global String alignNmsp;
	global String securityQuery = 'WITH SECURITY_ENFORCED';

	global Date todayDate = Date.today();

	global List<Staging_Rep_Roster__c> repRosterInsertList;
	global List<Staging_Rep_Roster__c> repRosterUpdateList;
	global List<SObject> currentPosEmpList;

	global Set<String> teamInstanceList;
	global Set<String> countryList;
	Set<String> empPRIDSet;

	global Map<String,String> mapEmpIdToPosID;
	global Map<String, Staging_Rep_Roster__c> mapEmpIdToRec;

    global BatchCreateRepRosterData()
    {
    	batchName = 'BatchCreateRepRosterData';
    	SnTDMLSecurityUtil.printDebugMessage(batchName+' : constructor invoked');

    	ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ScenarioAlignmentCtlr'];
        alignNmsp = cs.NamespacePrefix!=null && cs.NamespacePrefix!='' ? cs.NamespacePrefix+'__' : '';

        //Added
        //jobType = 'Full Load';
    	//jobType = SalesIQUtility.getIntegrationJobType('Rep_Roster');
    	/*teamInstanceList = (jobType=='Full Load') ? util.getFullLoadTeamInstancesCallPlan() 
    						: util.getDeltaLoadTeamInstancesCallPlan();*/

    	countryList = Util.getFullDeltaLoadCountries();
    	teamInstanceList = Util.getFullDeltaTeamInstances();

		SnTDMLSecurityUtil.printDebugMessage('alignNmsp--'+alignNmsp);
		SnTDMLSecurityUtil.printDebugMessage('todayDate--'+todayDate);
		//SnTDMLSecurityUtil.printDebugMessage('jobType--'+jobType);
        //SnTDMLSecurityUtil.printDebugMessage('teamInstanceList--'+teamInstanceList);
		SnTDMLSecurityUtil.printDebugMessage('teamInstanceList size--'+teamInstanceList.size());
		SnTDMLSecurityUtil.printDebugMessage('countryList size--'+countryList.size());

		query = 'SELECT AddressCity__c,AddressCountry__c,AddressLine1__c,AddressLine2__c,'+
				'AddressPostalCode__c,AddressStateCode__c,AxtriaSalesIQTM__Cellphone_Number__c,'+
				'AxtriaSalesIQTM__Country_Name__c,AxtriaSalesIQTM__Email__c,Employee_ID__c,'+
				'AxtriaSalesIQTM__Employee_ID__c,AxtriaSalesIQTM__Fax_Number__c,'+
				'Employee_Name__c,Employee_PRID__c,Employee_Status__c,WorkLocation__c,'+
				'HomePhone__c,Id,Job_Title__c,Name,PersonalCell__c,Territory_Code__c,'+
				'TimeZone__c,WorkPhone__c,Work_Fax__c FROM AxtriaSalesIQTM__Employee__c '+
				'where Employee_PRID__c!=null and '+alignNmsp+'Country_Name__c in :countryList '+
				securityQuery+' order by Employee_PRID__c';
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
    	SnTDMLSecurityUtil.printDebugMessage(batchName+' : start() invoked--');
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : query--'+query);
        return Database.getQueryLocator(query);
    }
    
    global void initiateVariables()
    {
        repRosterInsertList = new List<Staging_Rep_Roster__c>();
        repRosterUpdateList = new List<Staging_Rep_Roster__c>();
        currentPosEmpList = new List<SObject>();
        empPRIDSet = new Set<String>();
        mapEmpIdToPosID = new Map<String,String>();
        mapEmpIdToRec = new Map<String, Staging_Rep_Roster__c>();
    }
    
    global void createRepRosterMap(Set<String> externalIDSet)
    {
        for(Staging_Rep_Roster__c repRoster : [SELECT Address_Line_1__c,Address_Line_2__c,City__c,
                                                Country__c,Employee__c,Fax_Number__c,Home_Phone__c,
                                                Id,Mobile_Number__c,Name,Position__c,External_ID__c,
                                                State__c,Username_External_Id__c,Zip__c from Staging_Rep_Roster__c 
                                                where External_ID__c in :externalIDSet WITH SECURITY_ENFORCED])
		{
            mapEmpIdToRec.put(repRoster.External_ID__c,repRoster);
        }
    }
        
    global void execute(Database.BatchableContext bc, List<SObject> empList)
    {
        try
        {
            SnTDMLSecurityUtil.printDebugMessage(batchName+' : execute() invoked--');

            String empSFID;
            String empPRID;
            String posSFID;
            String assignType;

            Staging_Rep_Roster__c repRosterRec;
            initiateVariables();

            for(SObject rec : empList){
            	empPRIDSet.add((String)rec.get('Employee_PRID__c'));
            }
            /*SnTDMLSecurityUtil.printDebugMessage('teamInstanceList--'+teamInstanceList);
            SnTDMLSecurityUtil.printDebugMessage('empPRIDSet--'+empPRIDSet);*/
            SnTDMLSecurityUtil.printDebugMessage('empPRIDSet size--'+empPRIDSet.size());

            createRepRosterMap(empPRIDSet);

            posEmpQuery = 'select '+alignNmsp+'Position__c,'+alignNmsp+'Employee__r.Employee_PRID__c,'+alignNmsp+
        					'Assignment_Type__c from '+alignNmsp+'Position_Employee__c where '+
                            'Employee_ID__c in :empPRIDSet and '+alignNmsp+'Effective_Start_Date__c <= :todayDate '+
        					'and '+alignNmsp+'Effective_End_Date__c>=:todayDate and '+
                            'Team_Instance__c in :teamInstanceList and '+alignNmsp+'Position__c!=null and '+
                            alignNmsp+'Position__r.'+alignNmsp+'IsMaster__c = true and '+alignNmsp+
                            'Position__r.'+alignNmsp+'inactive__c = false '+securityQuery;
            currentPosEmpList = Database.query(posEmpQuery);

            SnTDMLSecurityUtil.printDebugMessage('posEmpQuery--'+posEmpQuery);
            SnTDMLSecurityUtil.printDebugMessage('currentPosEmpList size--'+currentPosEmpList.size());
            //SnTDMLSecurityUtil.printDebugMessage('currentPosEmpList--'+currentPosEmpList);

            //Get current Position for all the employees.
            for(Sobject rec1 : currentPosEmpList)
            {
            	posSFID = rec1.get(alignNmsp+'Position__c')!=null ? (String)rec1.get(alignNmsp+'Position__c') : null;
            	empPRID = (String)rec1.getSObject(alignNmsp+'Employee__r').get('Employee_PRID__c');
            	mapEmpIdToPosID.put(empPRID,posSFID);
            }
            //SnTDMLSecurityUtil.printDebugMessage('mapEmpIdToPosID--'+mapEmpIdToPosID);
            currentPosEmpList.clear();

            //Now create the final recs
            for(SObject emp : empList)
            {
            	empPRID = (String)emp.get('Employee_PRID__c');

            	repRosterRec = new Staging_Rep_Roster__c();
            	repRosterRec.External_ID__c = empPRID;
            	repRosterRec.Address_Line_1__c = (String)emp.get('AddressLine1__c');
            	repRosterRec.Address_Line_2__c = (String)emp.get('AddressLine2__c');
            	repRosterRec.City__c = (String)emp.get('AddressCity__c');
            	repRosterRec.Country__c = (String)emp.get(alignNmsp+'Country_Name__c');
            	repRosterRec.Employee__c = (String)emp.get('Id');
            	repRosterRec.Fax_Number__c = (String)emp.get(alignNmsp+'Fax_Number__c');
            	repRosterRec.Home_Phone__c = (String)emp.get('HomePhone__c');
            	repRosterRec.Mobile_Number__c = (String)emp.get('PersonalCell__c');
            	repRosterRec.Name = (String)emp.get('Name');
                repRosterRec.Position__c = null;

            	if(mapEmpIdToPosID.get(empPRID)!=null)
            		repRosterRec.Position__c = mapEmpIdToPosID.get(empPRID);
            	
            	repRosterRec.State__c = (String)emp.get('AddressStateCode__c');
            	repRosterRec.Username_External_Id__c = (String)emp.get(alignNmsp+'Email__c');
            	repRosterRec.Zip__c = (String)emp.get('AddressPostalCode__c');

            	if(mapEmpIdToRec.containsKey(empPRID))
            	{
            		repRosterRec.Id = mapEmpIdToRec.get(empPRID).Id;
            		repRosterUpdateList.add(repRosterRec);
            	}
            	else{
            		repRosterInsertList.add(repRosterRec);
            	}
            }

            SnTDMLSecurityUtil.printDebugMessage('repRosterUpdateList size--'+repRosterUpdateList.size());
            SnTDMLSecurityUtil.printDebugMessage('repRosterInsertList size--'+repRosterInsertList.size());

            if(!repRosterUpdateList.isEmpty()){
               SnTDMLSecurityUtil.updateRecords(repRosterUpdateList,batchName);
            }

            if(!repRosterInsertList.isEmpty()){
               SnTDMLSecurityUtil.insertRecords(repRosterInsertList,batchName);
            }

        }
        catch(Exception e)
        {
            SnTDMLSecurityUtil.printDebugMessage('Error in '+batchName+' : execute()-->'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack Trace-->'+e.getStackTraceString());
        }
    }
    
    global void finish(Database.BatchableContext bc)
    {
    	SnTDMLSecurityUtil.printDebugMessage(batchName+' : finish() invoked--');
    }
}