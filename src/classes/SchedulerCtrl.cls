public class SchedulerCtrl{
    
    public List<selectOption> classNameList {get;set;}
    public List<selectOption> timeList {get;set;}
    Map<string,Scheduled_Classes__c> allscheduleClasses;
    Map<string,CronTrigger> scheduledJobdetail;
    public Date datename {get; set;}
    public String msg {get; set;}
    public String selectedRecurrence{get;set;}
    public String selectedClass{get;set;}
    public String selectedClassName{get;set;}
    public String selectedTime{get;set;}
    public List<String> selectedDay{get;set;}
    public List<wrapClass> wrapList{get;set;}
    public boolean showPopUp{get;set;}
    public boolean showAlertPopUp{get;set;}
    public boolean showConfirmPopUp{get;set;}
    public boolean showsavePopUp {get;set;}
    public boolean activeValue{get;set;}
    public String activeStrValue{get;set;}
    public String selectedLap{get;set;}
    
    public SchedulerCtrl(){
        //List<CronTrigger> jobList=[SELECT CronJobDetail.Name FROM CronTrigger ];
       /* set<String> jobNameList=new set<String>();
        for(CronTrigger ct:[SELECT CronJobDetail.Name FROM CronTrigger ]){
        jobNameList.add(ct.CronJobDetail.Name);
        }
        
        List<Scheduled_Classes__c> scheduleClassesList=Scheduled_Classes__c.getAll().values();
        List<Scheduled_Classes__c> updList=new List<Scheduled_Classes__c>();
        System.debug('**job list'+jobNameList);
        //String jobName=jobList.get(0).CronJobDetail.Name;
        
       for(Scheduled_Classes__c sc:scheduleClassesList){
          
                 if(jobNameList.contains(sc.Original_Class_Name__c)){
                     sc.Schedule_Status__c=true;
                     updList.add(sc);
                 }
            
        }
       update updList;*/
        showAlertPopUp=false;
        showConfirmPopUp=false;
        showPopUp = false;
        selectedClass = '';
        selectedRecurrence ='Hourly';
        selectedDay = new List<String> ();
        classNameList = new List<selectOption> ();
        timeList = new List<selectOption> ();
        wrapList = new List<wrapClass>();
        scheduledJobdetail=new map<String,CronTrigger>();
        activeStrValue = 'Inactive';
        activeValue = false;
        List<CronTrigger> jobList=[SELECT CronJobDetail.Name,State,NextFireTime,PreviousFireTime FROM CronTrigger ];
        for(CronTrigger ct:jobList) {
            String jobName=ct.CronJobDetail.Name;
            scheduledJobdetail.put(ct.CronJobDetail.Name,ct);
            System.debug('jobdetail'+ct);
        }      
        allscheduleClasses = Scheduled_Classes__c.getAll();
        if(allscheduleClasses != Null){
            for(string classLabel : allscheduleClasses.keyset()){
                classNameList.add(new selectOption(allscheduleClasses.get(ClassLabel).Original_Class_Name__c,classLabel));
                if(scheduledJobdetail.get(allscheduleClasses.get(ClassLabel).Original_Class_Name__c)!=null){
                    CronTrigger ct=scheduledJobdetail.get(allscheduleClasses.get(ClassLabel).Original_Class_Name__c);
                    wrapList.add(new wrapClass(allscheduleClasses.get(ClassLabel).Original_Class_Name__c,classLabel,allscheduleClasses.get(ClassLabel).Schedule_Status__c,string.valueOfGmt(ct.PreviousFireTime),'',string.valueOfGmt(ct.NextFireTime) ));
                }
                else
                     wrapList.add(new wrapClass(allscheduleClasses.get(ClassLabel).Original_Class_Name__c,classLabel,allscheduleClasses.get(ClassLabel).Schedule_Status__c,'','','') );
            }
        } 
        
        integer counter = 1 ;
        while(counter < 3 ){
        
            string clockCounter ;
            if(counter == 1)
                clockCounter = ':00 AM';
            else
                clockCounter = ':00 PM' ;
                
            for(integer i=1; i<=12; i++){
                if(clockCounter == ':00 AM')
                timeList.add(new selectOption(String.valueof(i),String.valueof(i) + ''+ clockCounter));
                
                else
                timeList.add(new selectOption(String.valueof(i+12),String.valueof(i) + ''+ clockCounter));
            } 
            counter ++;  
        }
    }
    
    public void doSchedule(){
        
        string cronExpression ;
        
        if(selectedRecurrence == 'Daily' && selectedTime != Null){
            if(selectedTime == '24'){
                selectedTime = '0';
            }
            //  cronExpression = '0 0 '+selectedTime+' * * ? *' ;
            system.debug('========selectedLap:::'+selectedLap);
            if(selectedLap == Null || selectedLap=='' || selectedLap ==null){
                selectedLap ='1';
            }
            cronExpression = '0 0 '+selectedTime+' 1/'+selectedLap+' * ? *' ;
        }
        else if(selectedRecurrence == 'Weekly' && selectedDay.size() > 0 && selectedTime != Null){
            string selectedDaysExp ;
            for(string dayVar :selectedDay){
                selectedDaysExp = selectedDaysExp != Null ? selectedDaysExp + ',' +dayVar : dayVar ;
            }
            if(selectedLap == Null){
                selectedLap ='1';
            }
            cronExpression = '0 0 '+selectedTime+' ? * '+ selectedDaysExp;
        }
        else if(selectedRecurrence == 'Monthly'){
            cronExpression = '0 0 0 1 */1 ?' ;
        }
        else if(selectedRecurrence == 'Hourly' && selectedLap != Null){
            if(selectedLap == Null){
                selectedLap ='1';
            }            
            cronExpression = '0 0 0/'+selectedLap+' 1/1 * ? *' ;
        }
        
        
        system.debug('========selectedDay===='+selectedDay);
        //selectedClass = 'BatchOutboundUpdteamInstance';
        system.debug('========selectedClass===='+selectedClass);

        

        system.debug('========' + cronExpression + '=====');
        
        String jobID = system.schedule(selectedClass, cronExpression, (Schedulable)Type.forName(selectedClass).newInstance());
        showPopUp = false;
        showsavePopUp =false;
       
        msg='This Process has already been scheduled';
        
        /*List<CronTrigger> jobList=[SELECT CronJobDetail.Name FROM CronTrigger where id=:jobID];
        System.debug('**job name'+jobList.get(0).CronJobDetail.Name);
        String jobName=jobList.get(0).CronJobDetail.Name;
       List<Scheduled_Classes__c> scheduleClassesList=Scheduled_Classes__c.getAll().values();
        for(Scheduled_Classes__c sc:scheduleClassesList){
        if(sc.Original_Class_Name__c==jobName){
        sc.Schedule_Status__c=true;
        update sc;
        break;
        }
        }*/
    }
    
    public void checkRec(){
        showPopUp = true;
        system.debug('-----' + selectedDay);
    }
     public void checkRec4(){
        showConfirmPopUp = true;
       // system.debug('-----' + selectedDay);
    }
    public void checkRec2(){
        //showPopUp = true;
        system.debug('-----' + selectedDay);
        system.debug('========selectedClass====in checkrec2'+selectedClass);
        List<CronTrigger> jobList=[SELECT CronJobDetail.Name FROM CronTrigger where CronJobDetail.Name=:selectedClass];
        if(jobList.size()>0){
        System.debug(jobList.get(0));
        showAlertPopUp=true;
        showPopUp = false;
        }
        else{
        showPopUp = true;
        showAlertPopUp=false;
        }
        
    }

    public void checkRec3(){
        showPopUp = false;
        showAlertPopUp=true;
        showConfirmPopUp = false;
        system.debug('-----' + showAlertPopUp);
        system.debug('========showConfirmPopUp ===='+showConfirmPopUp );
        datetime dt= system.now();
        string acron = dt.second() +' '+(dt.minute()+1)+' '+dt.hour()+' '+dt.day() +' '+dt.month()+' ? '+dt.year();
        System.debug(acron);
        
        //String jobID = system.schedule('x', acron, (Schedulable)Type.forName(selectedClass).newInstance()); 
        Type typ = Type.forName(selectedClass);
         Object instance = null;
         instance = typ.newInstance();
         if(selectedClass!='PositionProductOutBound' || selectedClass!='MergeUnmergeDelta'){
          Database.executeBatch((Database.Batchable<sObject>)instance, 200); 
          }
      
        msg='The batch is executing';
      
       
         }
    public void cancelrec(){
        showPopUp = false;
        system.debug('==============Inside cancel func ');
    }
    public void closeAlert(){
        System.debug('====showalertPopUp'+showalertPopUp);
        System.debug('====showConfirmPopUp '+showConfirmPopUp );
       
        showalertPopUp = false;
        showConfirmPopUp = false;
        system.debug('==============Inside close func ');
    }
    
     public void save(){
        showsavePopUp = true;
        system.debug('==============Inside cancel func ');
    }
    
    public void toggleActive(){
        activeStrValue = activeValue ? 'Active' : 'Inactive';
        
    }
    
    public class wrapClass{
        public string classLabel{get;set;}
        public string classOrgName{get;set;}
        public boolean scheduleStatus{get;set;}
        public string classLastRun{get;set;} 
        public string classLastRunStatus{get;set;} 
        public string classNextRun{get;set;} 
        
        public wrapClass(string name, string label,boolean status,string lastRun,string lastRunstatus,string nextRun){
            classLabel = label;
            classOrgName = name;
            scheduleStatus=status;
            classLastRun=lastRun;
            classLastRunStatus=lastRunStatus;
            classNextRun=nextRun;
        }
    }
    
    
}