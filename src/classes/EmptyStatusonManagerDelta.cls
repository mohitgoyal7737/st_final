global with sharing class EmptyStatusonManagerDelta implements Database.Batchable<sObject>,Database.Stateful {
    public String query;
    public Set<String> countrySet;
    public List<AxtriaSalesIQTM__Country__c> countryList = new List<AxtriaSalesIQTM__Country__c>();

    global EmptyStatusonManagerDelta() {
    }

    global EmptyStatusonManagerDelta(Set<String> country) {
        query = '';
        countrySet=new Set<String>();
        countryList = [Select Id, Name from AxtriaSalesIQTM__Country__c where id in :country];
        for(AxtriaSalesIQTM__Country__c rec : countryList)
        {
            countrySet.add(rec.Name);
        }
        SnTDMLSecurityUtil.printDebugMessage('countrySet::::::::' +countrySet);
        query='select Id,Client_Territory_Code__c,Employee__c,Employee_PRID__c,Manager__c,Manager_PRID__c,Status__c from SIQ_User_Manager_Role_Outbound__c where Status__c = \'Updated\' and Country__c in :countrySet';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<SIQ_User_Manager_Role_Outbound__c> scope) {

        SnTDMLSecurityUtil.printDebugMessage('====Query::::::::' +scope);
        List<SIQ_User_Manager_Role_Outbound__c> statusUpdList=new List<SIQ_User_Manager_Role_Outbound__c>();

        for(SIQ_User_Manager_Role_Outbound__c statusoutbountEmpRec : scope)
        {
            statusoutbountEmpRec.Status__c='';
            statusUpdList.add(statusoutbountEmpRec);
        }
        

        SnTDMLSecurityUtil.printDebugMessage('====statusUpdList.size():::::' +statusUpdList.size());
        if(statusUpdList.size() > 0){
            //update statusUpdList;
            SnTDMLSecurityUtil.updateRecords(statusUpdList, 'EmptyStatusonManagerDelta');
        }
        
    }

    global void finish(Database.BatchableContext BC) {

        BatchSIQUserManagerUpdateOutbound obj1 = new BatchSIQUserManagerUpdateOutbound(countrySet);
        Database.executeBatch(obj1,2000);

    }
}