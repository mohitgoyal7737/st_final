/**
Name        :   BU_Response_Insert_Utility
Author      :   Ritwik Shokeen
Date        :   05/10/2018
Description :   BU_Response_Insert_Utility
*/
global class BU_Response_Insert_Utility implements Database.Batchable<sObject>,Database.Stateful {
    public String query;
    public String selectedTeamInstance;
    public String selectedTeam;
    //public AxtriaSalesIQTM__Team_Instance__C notSelectedTeamInstance;
    Map<String,Id> teamInstanceNametoIDMap;
    Map<String,string> teamInstanceNametoTeamNameMap;
    Map<String,string> teamInstanceNametoAZCycleMap;
    Map<String,string> teamInstanceNametoAZCycleNameMap;
    Map<String,string> teamInstanceNametoTeamIDMap;
    Map<String,string> teamInstanceNametoBUMap;
    Map<String,string> brandIDteamInstanceNametoBrandTeamInstIDMap;
    public Map<String,Staging_Cust_Survey_Profiling__c> questionToObjectMap;
    public Map<String,Integer> questionToResponseFieldNoMap;
    
    global BU_Response_Insert_Utility(String teamInstance,Map<String,Integer> localquestionToResponseFieldNoMap) {
        teamInstanceNametoIDMap = new Map<String,Id>();
        teamInstanceNametoTeamNameMap = new Map<String,String>();
        teamInstanceNametoAZCycleMap = new Map<String,String>();
        teamInstanceNametoAZCycleNameMap = new Map<String,String>();
        teamInstanceNametoTeamIDMap = new Map<String,String>();
        teamInstanceNametoBUMap = new Map<String,String>();
        brandIDteamInstanceNametoBrandTeamInstIDMap = new Map<String,String>();
        selectedTeamInstance = teamInstance;
        questionToResponseFieldNoMap = new Map<String,Integer>(localquestionToResponseFieldNoMap);
        
        for(AxtriaSalesIQTM__Team_Instance__C teamIns : [Select Id,Name,AxtriaSalesIQTM__Team__c,AxtriaSalesIQTM__Team__r.Business_Unit_Loopup__c,AxtriaSalesIQTM__Team__r.name,Cycle__c,Cycle__r.name From AxtriaSalesIQTM__Team_Instance__C]){
            teamInstanceNametoIDMap.put(teamIns.Name,teamIns.Id);
            teamInstanceNametoTeamNameMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__r.name);
            teamInstanceNametoAZCycleMap.put(teamIns.Name,teamIns.Cycle__c);
            teamInstanceNametoAZCycleNameMap.put(teamIns.Name,teamIns.Cycle__r.name);
            teamInstanceNametoTeamIDMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__c);
            teamInstanceNametoBUMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__r.Business_Unit_Loopup__c);
        }
        
        for(Brand_Team_Instance__c bti :[select id,Brand__c,Brand__r.External_ID__c,Team_Instance__c,Team_Instance__r.name from Brand_Team_Instance__c]){
            brandIDteamInstanceNametoBrandTeamInstIDMap.put(bti.Brand__r.External_ID__c+bti.Team_Instance__r.name,bti.id);
        }
        
        selectedTeam = teamInstanceNametoTeamNameMap.get(selectedTeamInstance);
        //notSelectedTeamInstance = [Select Id, Name from AxtriaSalesIQTM__Team_Instance__C where Name LIKE : 'NS%' and Name != :teamInstance];
        
        query = 'Select Id, Name, CurrencyIsoCode, SURVEY_ID__c, SURVEY_NAME__c, BRAND_ID__c, BRAND_NAME__c, PARTY_ID__c, QUESTION_SHORT_TEXT__c, RESPONSE__c, Team_Instance__c, Last_Updated__c, QUESTION_ID__c FROM Staging_Cust_Survey_Profiling__c  Where Team_Instance__c = \'' + teamInstance + '\'';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        system.debug('query+++++++++++++   ' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Staging_Cust_Survey_Profiling__c> scopeRecs) {
      system.debug('Hello++++++++++++++++++++++++++++');
        //Set<string> posCodeSet = new Set<string>();
        Set<string> accountNumberSet = new Set<string>();
        Set<string> brandIDSet = new Set<string>();
        
        //Map<string,id> posCodeIdMap = new Map<string,id>();
        //Map<string,id> inactivePosCodeIdMap = new Map<string,id>();
        Map<string,id> accountNumberIdMap = new Map<string,id>();
        Map<string,id> brandIdMap = new Map<string,id>();
        //Map<string,id> posTeamInstMap = new Map<string,id>();
        
        
        List<Error_Object__c> errorList = new List<Error_object__c>();
        Error_object__c tempErrorObj;

        for(Staging_Cust_Survey_Profiling__c scsp : scopeRecs){      //Positions Set
            /*if(!string.IsBlank(scsp.Territory_ID__c)){
                posCodeSet.add(scsp.Territory_ID__c);
            }*/
            if(!string.IsBlank(scsp.PARTY_ID__c)){            //Accounts Set
                accountNumberSet.add(scsp.PARTY_ID__c);
            }
            /*if(!string.IsBlank(scsp.BRAND_ID__c)){            //Product_Catalog__c Set
                brandIDSet.add(scsp.BRAND_ID__c);
            }*/
                   
        }
        String key;
        
        
        //-------- Account Map
         for(Account acc : [Select id,AccountNumber from Account where AccountNumber IN:accountNumberSet ]){ //,Team_Name__c   and Team_Name__c=: selectedTeam
            accountNumberIdMap.put(acc.AccountNumber,acc.id);                  //Map of accounts and their lookups
            
        }
        for(Product_Catalog__c pc : [Select id,External_ID__c from Product_Catalog__c where AxtriaARSnT__IsActive__c=true])
        {
            brandIdMap.put(pc.External_ID__c,pc.id);                  //Map of brands and their lookups
        }
        
        
        Staging_BU_Response__c tempStagingBUResponseRec = new Staging_BU_Response__c();
        List<Staging_BU_Response__c> StagingBUResponseList = new List<Staging_BU_Response__c>();
        
        for(Staging_Cust_Survey_Profiling__c scsp : scopeRecs){
            
            
            /*tempStagingBUResponseRec.is_inserted__c = false; //??
            tempStagingBUResponseRec.is_updated__c = false; //??
            tempStagingBUResponseRec.Is_Active__c = true; //??
            tempStagingBUResponseRec.Position_Account__c = ''; //??
            tempStagingBUResponseRec.Survey_Master__c = ''; //??*/
            //tempStagingBUResponseRec.CurrencyIsoCode = 'EUR';
            
            
            if(scsp.QUESTION_SHORT_TEXT__c != null){
                
                /*if(questionToResponseFieldNoMap.get(scsp.QUESTION_SHORT_TEXT__c) == 0){
                    tempStagingBUResponseRec.Response1__c = scsp.RESPONSE__c;
                }
                else */
                if(questionToResponseFieldNoMap.get(scsp.QUESTION_SHORT_TEXT__c) == 1){
                    tempStagingBUResponseRec.Response1__c = scsp.RESPONSE__c;
                }
                else if(questionToResponseFieldNoMap.get(scsp.QUESTION_SHORT_TEXT__c) == 2){
                    tempStagingBUResponseRec.Response2__c = scsp.RESPONSE__c;
                }
                else if(questionToResponseFieldNoMap.get(scsp.QUESTION_SHORT_TEXT__c) == 3){
                    tempStagingBUResponseRec.Response3__c = scsp.RESPONSE__c;
                }
                else if(questionToResponseFieldNoMap.get(scsp.QUESTION_SHORT_TEXT__c) == 4){
                    tempStagingBUResponseRec.Response4__c = scsp.RESPONSE__c;
                }
                else if(questionToResponseFieldNoMap.get(scsp.QUESTION_SHORT_TEXT__c) == 5){
                    tempStagingBUResponseRec.Response5__c = scsp.RESPONSE__c;
                }
                else if(questionToResponseFieldNoMap.get(scsp.QUESTION_SHORT_TEXT__c) == 6){
                    tempStagingBUResponseRec.Response6__c = scsp.RESPONSE__c;
                }
                else if(questionToResponseFieldNoMap.get(scsp.QUESTION_SHORT_TEXT__c) == 7){
                    tempStagingBUResponseRec.Response7__c = scsp.RESPONSE__c;
                }
                else if(questionToResponseFieldNoMap.get(scsp.QUESTION_SHORT_TEXT__c) == 8){
                    tempStagingBUResponseRec.Response8__c = scsp.RESPONSE__c;
                }
                else if(questionToResponseFieldNoMap.get(scsp.QUESTION_SHORT_TEXT__c) == 9){
                    tempStagingBUResponseRec.Response9__c = scsp.RESPONSE__c;
                }
                else if(questionToResponseFieldNoMap.get(scsp.QUESTION_SHORT_TEXT__c) == 10){
                    tempStagingBUResponseRec.Response10__c = scsp.RESPONSE__c;
                }
                /*else if(questionToResponseFieldNoMap.get(scsp.QUESTION_SHORT_TEXT__c) == 11){
                    tempStagingBUResponseRec.Response11__c = scsp.RESPONSE__c;
                }
                else if(questionToResponseFieldNoMap.get(scsp.QUESTION_SHORT_TEXT__c) == 12){
                    tempStagingBUResponseRec.Response12__c = scsp.RESPONSE__c;
                }
                else if(questionToResponseFieldNoMap.get(scsp.QUESTION_SHORT_TEXT__c) == 13){
                    tempStagingBUResponseRec.Response13__c = scsp.RESPONSE__c;
                }
                else if(questionToResponseFieldNoMap.get(scsp.QUESTION_SHORT_TEXT__c) == 14){
                    tempStagingBUResponseRec.Response14__c = scsp.RESPONSE__c;
                }
                else if(questionToResponseFieldNoMap.get(scsp.QUESTION_SHORT_TEXT__c) == 15){
                    tempStagingBUResponseRec.Response15__c = scsp.RESPONSE__c;
                }
                else{
                }*/
                
            }
            
            //---------- Team Instance lookups
            if(!string.IsBlank(scsp.Team_Instance__c)){
                if(teamInstanceNametoIDMap.containsKey(scsp.Team_Instance__c)){
                    tempStagingBUResponseRec.Team_Instance__c = teamInstanceNametoIDMap.get(scsp.Team_Instance__c);
                    tempStagingBUResponseRec.Team__c = teamInstanceNametoTeamIDMap.get(scsp.Team_Instance__c);
                    tempStagingBUResponseRec.Line__c = teamInstanceNametoTeamIDMap.get(scsp.Team_Instance__c);
                    tempStagingBUResponseRec.Cycle__c = teamInstanceNametoIDMap.get(scsp.Team_Instance__c);
                    tempStagingBUResponseRec.Cycle2__c = teamInstanceNametoAZCycleMap.get(scsp.Team_Instance__c);
                    tempStagingBUResponseRec.Business_Unit__c = teamInstanceNametoBUMap.get(scsp.Team_Instance__c);
                    
                }
                else{
                    //tempSurveyResponseRec.isError__c = true;
                    tempErrorObj = new Error_object__c();
                    tempErrorObj.Record_Id__c = scsp.Id;
                    tempErrorObj.Object_Name__c = 'Staging_Cust_Survey_Profiling__c';
                    tempErrorObj.Field_Name__c = 'Team_Instance__c';
                    tempErrorObj.Comments__c = 'Problem in Name of Team Instance: Team Instance name not found in map: Lookup not filled in Staging_BU_Response__c.';
                    tempErrorObj.Team_Instance_Name__c = scsp.Team_Instance__c;
                    errorList.add(tempErrorObj);
                }
            }
            else{
              
                //tempSurveyResponseRec.isError__c = true;
                tempErrorObj = new Error_object__c();
                tempErrorObj.Record_Id__c = scsp.Id;
                tempErrorObj.Object_Name__c = 'Staging_Cust_Survey_Profiling__c';
                tempErrorObj.Field_Name__c = 'Team_Instance__c';
                tempErrorObj.Comments__c = 'Team Instance Name is Blank: Lookup not filled in Staging_BU_Response__c.';
                tempErrorObj.Team_Instance_Name__c = scsp.Team_Instance__c;
                errorList.add(tempErrorObj);
            }
            //---------- Account lookups
            if(!string.isBlank(scsp.PARTY_ID__c)){
              if(accountNumberIdMap.containsKey(scsp.PARTY_ID__c)){
                tempStagingBUResponseRec.Physician__c = accountNumberIdMap.get(scsp.PARTY_ID__c);
              }
              else{
                //tempSurveyResponseRec.isError__c = true;
                    tempErrorObj = new Error_object__c();
                    tempErrorObj.Record_Id__c = scsp.Id;
                    tempErrorObj.Object_Name__c = 'Staging_Cust_Survey_Profiling__c';
                    tempErrorObj.Field_Name__c = 'PARTY_ID__c';
                    tempErrorObj.Comments__c = 'Account Number not found in map: Lookup not filled in Staging_BU_Response__c.';
                    tempErrorObj.Team_Instance_Name__c = scsp.Team_Instance__c;
                    errorList.add(tempErrorObj);
              }
            }
            else{
              //tempSurveyResponseRec.isError__c = true;
                tempErrorObj = new Error_object__c();
                tempErrorObj.Record_Id__c = scsp.Id;
                tempErrorObj.Object_Name__c = 'Staging_Cust_Survey_Profiling__c';
                tempErrorObj.Field_Name__c = 'PARTY_ID__c';
                tempErrorObj.Comments__c = 'Account Number is Blank: Lookup not filled in Staging_BU_Response__c.';
                tempErrorObj.Team_Instance_Name__c = scsp.Team_Instance__c;
                errorList.add(tempErrorObj);
              
            }
            
            //brands
            if(!string.isBlank(scsp.BRAND_ID__c)){
              if(brandIdMap.containsKey(scsp.BRAND_ID__c)){
                
                tempStagingBUResponseRec.Brand__c = brandIDteamInstanceNametoBrandTeamInstIDMap.get(scsp.BRAND_ID__c + scsp.Team_Instance__c);
                tempStagingBUResponseRec.Brand_ID__c = scsp.BRAND_NAME__c;//BRAND_ID__c
                
              }
              else{
                //tempSurveyResponseRec.isError__c = true;
                    tempErrorObj = new Error_object__c();
                    tempErrorObj.Record_Id__c = scsp.Id;
                    tempErrorObj.Object_Name__c = 'Staging_Cust_Survey_Profiling__c';
                    tempErrorObj.Field_Name__c = 'BRAND_ID__c';
                    tempErrorObj.Comments__c = 'Account Number not found in map: Lookup not filled in Staging_BU_Response__c.';
                    tempErrorObj.Team_Instance_Name__c = scsp.Team_Instance__c;
                    errorList.add(tempErrorObj);
              }
            }
            else{
              //tempSurveyResponseRec.isError__c = true;
                tempErrorObj = new Error_object__c();
                tempErrorObj.Record_Id__c = scsp.Id;
                tempErrorObj.Object_Name__c = 'Staging_Cust_Survey_Profiling__c';
                tempErrorObj.Field_Name__c = 'BRAND_ID__c';
                tempErrorObj.Comments__c = 'Account Number is Blank: Lookup not filled in Staging_BU_Response__c.';
                tempErrorObj.Team_Instance_Name__c = scsp.Team_Instance__c;
                errorList.add(tempErrorObj);
              
            }
            
            if(!string.isBlank(scsp.BRAND_ID__c) && !string.isBlank(scsp.Team_Instance__c) && !string.isBlank(scsp.PARTY_ID__c)){
                tempStagingBUResponseRec.External_ID__c = scsp.BRAND_ID__c + scsp.Team_Instance__c + scsp.PARTY_ID__c;
            }
            
            StagingBUResponseList.add(tempStagingBUResponseRec);
            
        }
        
        if(errorList.size() > 0){
            insert errorList;
        }
        
        upsert StagingBUResponseList External_ID__c;

    }

    global void finish(Database.BatchableContext BC) {
        //Database.executeBatch(new All_BU_Response_Insert_Utility(selectedTeamInstance),2000);  //to duplicate for position account (based on account)
    }
}