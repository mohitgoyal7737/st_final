/*
@author - Prince Richard Augustin (A2661)
@description - Test class to test Quantile_Analysis_Ctrl Controller.
*/

@isTest
private class Quantile_Analysis_Ctrl_Test { 

   static testMethod void testMethod1() {
    AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    insert orgmas;
    AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
    insert countr;
    AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
    insert team;
    AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
    workspace.AxtriaSalesIQTM__Country__c = countr.id;
    insert workspace;
    AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
    insert teamins;
    AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
    insert scen;
    Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
    insert pcc;
    Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
    mmc.Team_Instance__c = teamins.id;
    mmc.State__c ='Executed';
    insert mmc;
    Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
    insert pp;
    Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
    insert rp;
    Compute_Master__c ccMaster = TestDataFactory.createComputeMaster(rp);
    insert ccMaster;
    Grid_Master__c gMaster = TestDataFactory.gridMaster(countr);
    insert gMaster;
    Step__c steps = TestDataFactory.step(ccMaster, gMaster, mmc, rp);
    steps.Step_Type__c = 'Quantile';
    insert steps;
    Segment_Simulation_Copy__c ssc = TestDataFactory.createSegSimulation(mmc,steps);
    insert ssc;
    
    AxtriaSalesIQTM__ETL_Config__c etl = TestDataFactory.configureETL();
    insert etl;
    ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
    String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
    List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
    System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
    Quantile_Analysis_Ctrl c = new Quantile_Analysis_Ctrl();
    c.countryID = countr.id;
    c.fillCycleOptions();
    c.countryID = countr.id;
    c.selectedCycle = teamins.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c;
    c.fillBusinessUnitOptions();
    c.selectedBusinessUnit = teamins.id;
    c.fillMeasureOptions();
    c.selectedMeasureId = mmc.id;
    c.fillStepOptions();
    c.loadParameterTable();
    
    
    c.selectedSimulation='Segment Simulation';
    c.countryID = countr.id;
    c.redirectToPage();
    
} 
static testMethod void testMethod2() {
    AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    insert orgmas;
    AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
    insert countr;
    AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
    insert team;
    AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
    workspace.AxtriaSalesIQTM__Country__c = countr.id;
    insert workspace;
    AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
    insert teamins;
    AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
    insert scen;
    Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
    insert pcc;
    Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
    mmc.Team_Instance__c = teamins.id;
    mmc.State__c ='Executed';
    insert mmc;
    Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
    insert pp;
    Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
    insert rp;
    Compute_Master__c ccMaster = TestDataFactory.createComputeMaster(rp);
    insert ccMaster;
    Grid_Master__c gMaster = TestDataFactory.gridMaster(countr);
    insert gMaster;
    Step__c steps = TestDataFactory.step(ccMaster, gMaster, mmc, rp);
    steps.Step_Type__c = 'Quantile';
    insert steps;
    Segment_Simulation_Copy__c ssc = TestDataFactory.createSegSimulation(mmc,steps);
    insert ssc;
    
    AxtriaSalesIQTM__ETL_Config__c etl = TestDataFactory.configureETL();
    insert etl;
    ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
    String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
    List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
    System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
    Quantile_Analysis_Ctrl c = new Quantile_Analysis_Ctrl();
    c.countryID = countr.id;
    c.fillCycleOptions();
    c.countryID = countr.id;
    c.selectedCycle = teamins.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c;
    c.fillBusinessUnitOptions();
    c.selectedBusinessUnit = teamins.id;
    c.fillMeasureOptions();
    c.selectedMeasureId = mmc.id;
    c.fillStepOptions();
    c.loadParameterTable();
    
    
    c.selectedSimulation='Workload Simulation';
    c.countryID = countr.id;
    c.redirectToPage();
    
}
static testMethod void testMethod3() {
    AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    insert orgmas;
    AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
    insert countr;
    AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
    insert team;
    AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
    workspace.AxtriaSalesIQTM__Country__c = countr.id;
    insert workspace;
    AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
    insert teamins;
    AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
    insert scen;
    Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
    insert pcc;
    Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
    mmc.Team_Instance__c = teamins.id;
    mmc.State__c ='Executed';
    insert mmc;
    Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
    insert pp;
    Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
    insert rp;
    Compute_Master__c ccMaster = TestDataFactory.createComputeMaster(rp);
    insert ccMaster;
    Grid_Master__c gMaster = TestDataFactory.gridMaster(countr);
    insert gMaster;
    Step__c steps = TestDataFactory.step(ccMaster, gMaster, mmc, rp);
    steps.Step_Type__c = 'Quantile';
    insert steps;
    Segment_Simulation_Copy__c ssc = TestDataFactory.createSegSimulation(mmc,steps);
    insert ssc;
    
    AxtriaSalesIQTM__ETL_Config__c etl = TestDataFactory.configureETL();
    insert etl;
    ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
    String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
    List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
    System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
    Quantile_Analysis_Ctrl c = new Quantile_Analysis_Ctrl();
    c.countryID = countr.id;
    c.fillCycleOptions();
    c.countryID = countr.id;
    c.selectedCycle = teamins.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c;
    c.fillBusinessUnitOptions();
    c.selectedBusinessUnit = teamins.id;
    c.fillMeasureOptions();
    c.selectedMeasureId = mmc.id;
    c.fillStepOptions();
    c.loadParameterTable();
    
    
    c.selectedSimulation='Modelling';
    c.countryID = countr.id;
    c.redirectToPage();
    
}
static testMethod void testMethod4() {
    AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    insert orgmas;
    AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
    insert countr;
    AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
    insert team;
    AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
    workspace.AxtriaSalesIQTM__Country__c = countr.id;
    insert workspace;
    AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
    insert teamins;
    AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
    insert scen;
    Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
    insert pcc;
    Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
    mmc.Team_Instance__c = teamins.id;
    mmc.State__c ='Executed';
    insert mmc;
    Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
    insert pp;
    Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
    insert rp;
    Compute_Master__c ccMaster = TestDataFactory.createComputeMaster(rp);
    insert ccMaster;
    Grid_Master__c gMaster = TestDataFactory.gridMaster(countr);
    insert gMaster;
    Step__c steps = TestDataFactory.step(ccMaster, gMaster, mmc, rp);
    steps.Step_Type__c = 'Quantile';
    insert steps;
    Segment_Simulation_Copy__c ssc = TestDataFactory.createSegSimulation(mmc,steps);
    insert ssc;
    
    AxtriaSalesIQTM__ETL_Config__c etl = TestDataFactory.configureETL();
    insert etl;
    ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
    String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
    List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
    System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
    Quantile_Analysis_Ctrl c = new Quantile_Analysis_Ctrl();
    c.countryID = countr.id;
    c.fillCycleOptions();
    c.countryID = countr.id;
    c.selectedCycle = teamins.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c;
    c.fillBusinessUnitOptions();
    c.selectedBusinessUnit = teamins.id;
    c.fillMeasureOptions();
    c.selectedMeasureId = mmc.id;
    c.fillStepOptions();
    c.loadParameterTable();
    
    
    c.selectedSimulation='Quantile Analysis';
    c.countryID = countr.id;
    c.redirectToPage();
    
}
static testMethod void testMethod5() {
    AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    insert orgmas;
    AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
    insert countr;
    AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
    insert team;
    AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
    workspace.AxtriaSalesIQTM__Country__c = countr.id;
    insert workspace;
    AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
    insert teamins;
    AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
    insert scen;
    Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
    insert pcc;
    Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
    mmc.Team_Instance__c = teamins.id;
    mmc.State__c ='Executed';
    insert mmc;
    Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
    insert pp;
    Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
    insert rp;
    Compute_Master__c ccMaster = TestDataFactory.createComputeMaster(rp);
    insert ccMaster;
    Grid_Master__c gMaster = TestDataFactory.gridMaster(countr);
    insert gMaster;
    Step__c steps = TestDataFactory.step(ccMaster, gMaster, mmc, rp);
    steps.Step_Type__c = 'Quantile';
    insert steps;
    Segment_Simulation_Copy__c ssc = TestDataFactory.createSegSimulation(mmc,steps);
    insert ssc;
    
    AxtriaSalesIQTM__ETL_Config__c etl = TestDataFactory.configureETL();
    insert etl;
    ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
    String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
    List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
    System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
    Quantile_Analysis_Ctrl c = new Quantile_Analysis_Ctrl();
    c.countryID = countr.id;
    c.fillCycleOptions();
    c.countryID = countr.id;
    c.selectedCycle = teamins.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c;
    c.fillBusinessUnitOptions();
    c.selectedBusinessUnit = teamins.id;
    c.fillMeasureOptions();
    c.selectedMeasureId = mmc.id;
    c.fillStepOptions();
    c.loadParameterTable();
    
    
    c.selectedSimulation='Comparative Analysis';
    c.countryID = countr.id;
    c.redirectToPage();
    
}
}