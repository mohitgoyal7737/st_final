global with sharing class BatchPopulateAccRuleTypeforMerge implements Database.Batchable<sObject>, Database.Stateful {
    public String query;
    public Set<String> setTDDeassignID;
    public Set<String> setBUDeassignID;
    public Set<String> looserSet;
    
    global BatchPopulateAccRuleTypeforMerge(Set<String> looserSetRec) {
        query = '';
        setTDDeassignID=new Set<String>();
        setBUDeassignID=new Set<String>();
        looserSet=new Set<String>();
        looserSet.addAll(looserSetRec);
        query='select id,Account__c,Position__c,Team_Instance__c,Status__c,Account_Type__c,Rule_Type__c,Country_Name__c from Deassign_Postiton_Account__c where Status__c=\'New Merge\' and Account__c in :looserSet';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Deassign_Postiton_Account__c> scope) {
        
        SnTDMLSecurityUtil.printDebugMessage('====Query:::::::' +scope);

        Set<String> setAccount=new Set<String>();
        Set<String> setTeamIns=new Set<String>();
        Set<String> setScenario=new Set<String>();
        Map<String,String> mapAccNum2AccType=new Map<String,String>();
        Map<String,String> mapTeamIns2Scenario=new Map<String,String>();
        Map<String,String> mapScenario2RuleType=new Map<String,String>();
        Map<String,String> mapTeamIns2RuleType=new Map<String,String>();
        Map<String,String> mapTeamIns2Country=new Map<String,String>();
        List<Deassign_Postiton_Account__c> deassignList=new List<Deassign_Postiton_Account__c>();

        for(Deassign_Postiton_Account__c deassignRec : scope)
        {
            setTeamIns.add(deassignRec.Team_Instance__c);
            setAccount.add(deassignRec.Account__c);
        }

        SnTDMLSecurityUtil.printDebugMessage('====setTeamIns:::::::' +setTeamIns);
        SnTDMLSecurityUtil.printDebugMessage('====setAccount:::::::' +setAccount);

        List<Account> accList = [select Id,Merge_Account_Number__c,Type from Account where Merge_Account_Number__c in :setAccount and Merge_Account_Number__c != null and AxtriaSalesIQTM__Active__c='Active' and Type!=null];

        for(Account acc : accList)
        {
            mapAccNum2AccType.put(acc.Merge_Account_Number__c,acc.Type);
        }

        SnTDMLSecurityUtil.printDebugMessage('====mapAccNum2AccType:::::::' +mapAccNum2AccType);

        List<AxtriaSalesIQTM__Team_Instance__c> teamInsList = [select Id,Name,AxtriaSalesIQTM__Scenario__c,Country_Name__c from AxtriaSalesIQTM__Team_Instance__c where Name in :setTeamIns and AxtriaSalesIQTM__Scenario__c != null];

        for(AxtriaSalesIQTM__Team_Instance__c tiRec : teamInsList)
        {
            mapTeamIns2Scenario.put(tiRec.Name,tiRec.AxtriaSalesIQTM__Scenario__c);
            mapTeamIns2Country.put(tiRec.Name,tiRec.Country_Name__c);
            setScenario.add(tiRec.AxtriaSalesIQTM__Scenario__c);
        }

        SnTDMLSecurityUtil.printDebugMessage('====mapTeamIns2Scenario:::::::' +mapTeamIns2Scenario);
        SnTDMLSecurityUtil.printDebugMessage('====setScenario:::::::' +setScenario);

        List<AxtriaSalesIQTM__Business_Rules__c> bussRuleList = [select Id, AxtriaSalesIQTM__Rule_Type__c,AxtriaSalesIQTM__Scenario__c from AxtriaSalesIQTM__Business_Rules__c where AxtriaSalesIQTM__Scenario__c in :setScenario and AxtriaSalesIQTM__Rule_Type__c!=null];

         for(AxtriaSalesIQTM__Business_Rules__c busRule : bussRuleList)
         {
            mapScenario2RuleType.put(busRule.AxtriaSalesIQTM__Scenario__c,busRule.AxtriaSalesIQTM__Rule_Type__c);
         }

         SnTDMLSecurityUtil.printDebugMessage('====mapScenario2RuleType:::::::' +mapScenario2RuleType);

         for(String teamIns : mapTeamIns2Scenario.keySet())
         {
            if(mapTeamIns2Scenario.get(teamIns) != null)
            {
                String scenario = mapTeamIns2Scenario.get(teamIns);
                SnTDMLSecurityUtil.printDebugMessage('====scenario:::::::' +scenario);
                String ruleType=mapScenario2RuleType.get(scenario);
                SnTDMLSecurityUtil.printDebugMessage('====ruleType:::::::' +ruleType);
                mapTeamIns2RuleType.put(teamIns,ruleType);
            }
         }
         SnTDMLSecurityUtil.printDebugMessage('====mapTeamIns2RuleType:::::::' +mapTeamIns2RuleType);

         for(Deassign_Postiton_Account__c deassignRec : scope)
         {
            deassignRec.Account_Type__c=mapAccNum2AccType.get(deassignRec.Account__c);
            deassignRec.Rule_Type__c=mapTeamIns2RuleType.get(deassignRec.Team_Instance__c);
            deassignRec.Country_Name__c=mapTeamIns2Country.get(deassignRec.Team_Instance__c);
            //setDeassignID.add(deassignRec.Id);
            deassignList.add(deassignRec);
            SnTDMLSecurityUtil.printDebugMessage('====deassignRec:::::::' +deassignRec);
         }

         SnTDMLSecurityUtil.printDebugMessage('====deassignList.size():::::::' +deassignList.size());

         if(deassignList.size() > 0){
            //update deassignList;
            SnTDMLSecurityUtil.updateRecords(deassignList, 'BatchPopulateAccRuleTypeforMerge');
        }

        for(Deassign_Postiton_Account__c deassignRec : deassignList)
        {
            if(deassignRec.Rule_Type__c == 'Top Down')
            {
                setTDDeassignID.add(deassignRec.Id);
            }
            if(deassignRec.Rule_Type__c == 'Bottom Up')
            {
                setBUDeassignID.add(deassignRec.Id);
            }
        }
        SnTDMLSecurityUtil.printDebugMessage('====setTDDeassignID:::::::' +setTDDeassignID);
        SnTDMLSecurityUtil.printDebugMessage('====setBUDeassignID:::::::' +setBUDeassignID);
    }

    global void finish(Database.BatchableContext BC) {

        
        if(setTDDeassignID != null && setBUDeassignID != null)
        {
            SnTDMLSecurityUtil.printDebugMessage('=========Calling Deassignment Position Account Batch for Top Down Rule Type:::::::::::::::::::::::::::');
            BatchMergeTopDownHandling posAcc = new BatchMergeTopDownHandling(setTDDeassignID,true,looserSet,setBUDeassignID);
            Database.executeBatch(posAcc,1);
        }

        else if(setTDDeassignID == null && setBUDeassignID != null)
        {
            SnTDMLSecurityUtil.printDebugMessage('=========Calling Deassignment Position Account Batch for Bottom Up Rule Type:::::::::::::::::::::::::::');
            BatchMergeBottomUpHandling buPosAcc = new BatchMergeBottomUpHandling(setBUDeassignID,true,looserSet);
            Database.executeBatch(buPosAcc,1);
        }
        else if(setTDDeassignID != null && setBUDeassignID == null)
        {
            SnTDMLSecurityUtil.printDebugMessage('=========Calling Deassignment Position Account Batch for Top Down Rule Type:::::::::::::::::::::::::::');
            BatchMergeTopDownHandling posAcc = new BatchMergeTopDownHandling(setTDDeassignID,true,looserSet);
            Database.executeBatch(posAcc,1);
        }
        else if(setTDDeassignID == null && setBUDeassignID == null)
        {
            SnTDMLSecurityUtil.printDebugMessage('=========Calling Inactive Account and Affiliation Batch:::::::::::::::::::::::::::');
            BatchMergeInactiveAffAcc acc = new BatchMergeInactiveAffAcc(looserSet);
            Database.executeBatch(acc,2);
        }

        /*if(setBUDeassignID != null)
        {
            SnTDMLSecurityUtil.printDebugMessage('=========Calling Deassignment Position Account Batch for Bottom Up Rule Type:::::::::::::::::::::::::::');
            BatchMergeBottomUpHandling buPosAcc = new BatchMergeBottomUpHandling(setBUDeassignID,true,looserSet);
            Database.executeBatch(buPosAcc,1);
        }*/
    }

}