global class CreateDeltaUser implements Database.Batchable<sObject> {
    public String query;

    global CreateDeltaUser() {
        query='Select id from Account';
        this.query = query;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Account> scope) {
        for(Account a : scope){
            a.name='Axtria';
        }
        
    }

    global void finish(Database.BatchableContext BC) {

    }
}