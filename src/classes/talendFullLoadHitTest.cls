/**********************************************************************************************
Author      : Himanshu Tariyal (A0994)
Date        : 6th October'2020
Description : Test class for talendFullLoadHit
Revision(s) : v1.0
**********************************************************************************************/
@isTest
private class talendFullLoadHitTest 
{
    static testMethod void testMethod1() 
    {
    	System.Test.startTest();

    	String className = 'talendFullLoadHitTest';
    	String nameSpace = MCCP_Utility.sntNamespace(className);
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'State__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Measure_Master__c.SObjectType, RULEPARAMETER_READ_FIELD, false));

    	talendFullLoadHit con = new talendFullLoadHit();
    	talendFullLoadHit.ETLWebServiceCallOutForFullLoad();

    	System.Test.stopTest();
        
    }
}