/**********************************************************************************************
Author      : Himanshu Tariyal (A0994)
Date        : 24th September'2020
Description : Test class for BatchPopulateSurveyDirectLoad
Revision(s) : v1.0
**********************************************************************************************/
@isTest
public with sharing class BatchPopulateSurveyDirectLoadTest 
{
    public static testMethod void testMethod1() 
    {
    	String className = 'BatchPopulateSurveyDirectLoadTest';

    	AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    	SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);

        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);

        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.Name = 'Team_ins';
        SnTDMLSecurityUtil.insertRecords(teamins,className);

        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        SnTDMLSecurityUtil.insertRecords(scen,className);

        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        pcc.Product_Code__c='PROD1';
        pcc.Veeva_External_ID__c='PROD1';
        SnTDMLSecurityUtil.insertRecords(pcc,className);

        Account acc = new Account();
        acc.Name = 'ACC_NAME';
        acc.AccountNumber = 'ACC_NO';
        acc.Marketing_Code__c = 'IT';
        SnTDMLSecurityUtil.insertRecords(acc,className);

        temp_Obj__c rec = new temp_Obj__c();
        rec.Status__c = 'New';
        rec.Product_Name__c = 'PROD1';
        rec.Position_Code__c = 'TERR1';
        rec.AccountNumber__c = 'ACC1';
        rec.Short_Question_Text1__c = 'Question1';
        rec.Metric10__c = 80;
        rec.Team_Instance_Text__c = teamins.Name;
        SnTDMLSecurityUtil.insertRecords(rec,className);

        temp_Obj__c rec2 = new temp_Obj__c();
        rec2.Status__c = 'New';
        rec2.Product_Name__c = 'PROD2';
        rec2.Position_Code__c = 'TERR2';
        rec2.AccountNumber__c = 'ACC2';
        rec2.Metric10__c = 80;
        rec2.Short_Question_Text1__c = 'Question2';
        rec2.Team_Instance_Text__c = teamins.Name;
        SnTDMLSecurityUtil.insertRecords(rec2,className);

        temp_Obj__c rec3 = new temp_Obj__c();
        rec3.Status__c = 'New';
        rec3.AccountNumber__c = 'ACC2';
        rec3.Metric10__c = 80;
        rec3.Team_Instance_Text__c = teamins.Name;
        SnTDMLSecurityUtil.insertRecords(rec3,className);

        System.Test.startTest();

        String nameSpace = MCCP_Utility.sntNamespace(className);

        List<String> PROD_PRIORITY_READ_FIELD = new List<String>{nameSpace+'Status__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(temp_Obj__c.SObjectType, PROD_PRIORITY_READ_FIELD, false));

        Database.executeBatch(new BatchPopulateSurveyDirectLoad(teamins.Name,'File'));

        System.Test.stopTest();
    }

    public static testMethod void testMethod2() 
    {
    	String className = 'BatchPopulateSurveyDirectLoadTest';

    	AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    	SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);

        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);

        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.Name = 'Team_ins';
        SnTDMLSecurityUtil.insertRecords(teamins,className);

        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        SnTDMLSecurityUtil.insertRecords(scen,className);

        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        pcc.Product_Code__c='PROD1';
        pcc.Veeva_External_ID__c='PROD1';
        SnTDMLSecurityUtil.insertRecords(pcc,className);

        Account acc = new Account();
        acc.Name = 'ACC_NAME';
        acc.AccountNumber = 'ACC_NO';
        acc.Marketing_Code__c = 'IT';
        SnTDMLSecurityUtil.insertRecords(acc,className);

        AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
        cr.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.Id;
        SnTDMLSecurityUtil.insertRecords(cr,className);

        temp_Obj__c rec = new temp_Obj__c();
        rec.Status__c = 'New';
        rec.Change_Request__c = cr.Id;
        rec.Product_Code__c = 'PROD1';
        rec.Position_Code__c = 'TERR1';
        rec.AccountNumber__c = 'ACC1';
        rec.Short_Question_Text1__c = 'Question1';
        rec.Metric10__c = 80;
        rec.Team_Instance_Text__c = teamins.Name;
        SnTDMLSecurityUtil.insertRecords(rec,className);

        temp_Obj__c rec2 = new temp_Obj__c();
        rec2.Status__c = 'New';
        rec2.Change_Request__c = cr.Id;
        rec2.Product_Code__c = 'PROD2';
        rec2.Position_Code__c = 'TERR2';
        rec2.AccountNumber__c = 'ACC2';
        rec2.Metric10__c = 80;
        rec2.Short_Question_Text1__c = 'Question2';
        rec2.Team_Instance_Text__c = teamins.Name;
        SnTDMLSecurityUtil.insertRecords(rec2,className);

        temp_Obj__c rec3 = new temp_Obj__c();
        rec3.Status__c = 'New';
        rec3.Change_Request__c = cr.Id;
        rec3.AccountNumber__c = 'ACC2';
        rec3.Metric10__c = 80;
        rec3.Team_Instance_Text__c = teamins.Name;
        SnTDMLSecurityUtil.insertRecords(rec3,className);

        System.Test.startTest();

        String nameSpace = MCCP_Utility.sntNamespace(className);

        List<String> PROD_PRIORITY_READ_FIELD = new List<String>{nameSpace+'Status__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(temp_Obj__c.SObjectType, PROD_PRIORITY_READ_FIELD, false));

        Database.executeBatch(new BatchPopulateSurveyDirectLoad(teamins.Name,cr.Id,'File'));

        System.Test.stopTest();
    }
}