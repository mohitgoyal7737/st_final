@isTest
private class BatchUserTerritoryFlow_Test {
    static testMethod void testMethod1() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        
        AxtriaSalesIQTM__Organization_Master__c orgmas1 = TestDataFactory.createOrganizationMaster();
        orgmas1.AxtriaSalesIQTM__Parent_Organization_Name__c = orgmas.id;
        insert orgmas1;
        
        Group grp = new Group ();
        grp.Name = 'abcd';
        insert grp;
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            BatchUserTerritoryFlow batchuserterriotryflow = new BatchUserTerritoryFlow();
            Database.executeBatch(batchuserterriotryflow);
        }
        Test.stopTest();
    }
}