public with sharing class InsertAzCycleTriggerhandler 
{
    public static boolean firstrun =true;

    public static void Insertcycle(list<AxtriaSalesIQTM__Team_Instance__c>triggernew ){ //, Map<id,AxtriaSalesIQTM__Team_Instance__c>triggeroldmap
        if(RecursivetriggerCommonUtility.check){
            RecursivetriggerCommonUtility.check = false;
            Set<String> cyclename = new set<String>();
            list<Cycle__c>newcycles = new list<Cycle__c>();
            map<String,String>Cycleidmap = new map<String,String>();
            list<Cycle__c> cycleList = [select id,Name,Start_Date__c,End_Date__c,isActive__c from Cycle__c];
            for(Cycle__c c: cycleList){
                cyclename.add(c.Name);
                Cycleidmap.put(c.Name,c.id);
            }

            list<AxtriaSalesIQTM__Team_Instance__c>Teaminstance = [select id,Cycle__c,AxtriaSalesIQTM__Country__c,AxtriaSalesIQTM__IC_EffstartDate__c,AxtriaSalesIQTM__IC_EffEndDate__c,AxtriaSalesIQTM__Team__r.Sales_Cycle_Name__c,AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Name from AxtriaSalesIQTM__Team_Instance__c where ID in:triggernew];

            for(AxtriaSalesIQTM__Team_Instance__c team : Teaminstance){

                System.debug('=====team.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Name=========='+team.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Name);
                if(!cyclename.contains(team.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Name) && team.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Name !=null){
                    Cycle__c newcycl = new Cycle__c();
                    newcycl.Name = team.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Name;
                    newcycl.Start_Date__c=team.AxtriaSalesIQTM__IC_EffstartDate__c ;
                    newcycl.End_Date__c =team.AxtriaSalesIQTM__IC_EffEndDate__c ;
                    newcycl.Country__c = team.AxtriaSalesIQTM__Country__c;
                    newcycles.add(newcycl);
                }
            }
            system.debug('==========newcycles======'+newcycles);
            insert newcycles;
            for(Cycle__c  c : newcycles){
                Cycleidmap.put(c.Name,c.id);
            }
            for(AxtriaSalesIQTM__Team_Instance__c TI : Teaminstance){
                if(TI.Cycle__c ==null){
                    TI.Cycle__c = Cycleidmap.get(TI.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Name);
                }
            }
            update Teaminstance;

        }
    }
}