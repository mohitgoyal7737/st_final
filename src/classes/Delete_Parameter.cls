/*Author - Himanshu Tariyal(A0994)
Date : 16th January 2018*/
global class Delete_Parameter implements Database.Batchable<sObject>
{
    public String query;
    public String teamInstance;
    public String type;
    global Delete_Parameter(String teamInstance,String type)
    {   
        this.type=type;
        this.teamInstance = teamInstance;
        query='select id from Parameter__c where Team_Instance__r.Name = \''+teamInstance+'\'';
    }
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        System.debug('query-->'+query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Parameter__c> scope)
    {
        System.debug('scope size-->'+scope.size());
        if(scope!=null && scope.size()>0){
            delete scope;
            Database.emptyRecycleBin(scope);  
        }
    }

    global void finish(Database.BatchableContext BC)
    {
        Delete_Metadata_Definition batch = new Delete_Metadata_Definition(teamInstance,type);
        Database.executeBatch(batch,500);
    }
}