global class AZ_Integration_MC_Target_MP implements Database.Batchable<sObject> {
    

    List<String> allChannels;
    String teamInstanceSelected;
    String queryString;
    
    List<Parent_PACP__c> pacpRecs;
    
    global AZ_Integration_MC_Target_MP(string teamInstanceSelectedTemp, List<String> allChannelsTemp)
    { 

       queryString = 'select ID, Sum_TCF__c, (select id, AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Account__r.AccountNumber , Final_TCF__c  from Call_Plan_Summary__r where AxtriaSalesIQTM__isIncludedCallPlan__c = true) from Parent_PACP__c where Team_Instance__c = :teamInstanceSelected';

        teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;
    }
    
    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        return Database.getQueryLocator(queryString);
    }
    
    public void create_MC_Cycle_Plan_Target_vod(List<Parent_PACP__c> scopePacpProRecs)
    {
        pacpRecs = scopePacpProRecs;
        List<SIQ_MC_Cycle_Plan_Target_vod_O__c> mcPlanTarget = new List<SIQ_MC_Cycle_Plan_Target_vod_O__c>();

         List<AxtriaSalesIQTM__User_Access_Permission__c> uap = [select AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__User__r.FederationIdentifier from AxtriaSalesIQTM__User_Access_Permission__c];
        
        Map<String,String> posTeamToUser = new Map<String,String>();
        
        for(AxtriaSalesIQTM__User_Access_Permission__c u: uap)
        {
            string teamInstancePos = u.AxtriaSalesIQTM__Team_Instance__r.Name +'_'+ u.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
            posTeamToUser.put(teamInstancePos ,u.AxtriaSalesIQTM__User__r.FederationIdentifier);
        }
        
        mcPlanTarget = new List<SIQ_MC_Cycle_Plan_Target_vod_O__c>();
        
        
        Map<String, Map<String,Integer>> targetCallsMap = new Map<String,  Map<String,Integer>>();
        
        Set<String> uniquePos = new Set<String>();

        for(Parent_PACP__c ppacp : scopePacpProRecs)
        {
            system.debug('++++++++++ Hey Inside recs are '+ scopePacpProRecs);

            SIQ_MC_Cycle_Plan_Target_vod_O__c mcp = new SIQ_MC_Cycle_Plan_Target_vod_O__c();
            mcp.SIQ_Channel_Interactions_Goal_vod__c= 0;
            Boolean flag = false;
            for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : ppacp.Call_Plan_Summary__r)
            {
                flag = true;
                String teamPos = pacp.AxtriaSalesIQTM__Team_Instance__r.Name + '_' + pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
                String userID = posTeamToUser.get(teamPos);
                

                mcp.SIQ_Cycle_Plan_vod__c = teamPos +'_'+userID+'_'+pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c ; //:- Get from Cycle Plan vod External ID 
                
                

                //mcp.AZ_External_Id__c  = teamPos + '_' + pacp.Account__r.AccountNumber;
                mcp.SIQ_Target_vod__c  = pacp.AxtriaSalesIQTM__Account__r.AccountNumber;
                
                mcp.SIQ_Status_vod__c = 'Active_vod';
                mcp.SIQ_Channel_Interactions_Goal_vod__c = mcp.SIQ_Channel_Interactions_Goal_vod__c + pacp.Final_TCF__c;
                
                mcp.SIQ_External_Id_vod__c  =   teamPos + '_' + pacp.AxtriaSalesIQTM__Account__r.AccountNumber;

            }

            mcp.External_ID_Axtria__c = mcp.SIQ_External_Id_vod__c;
            mcp.Rec_Status__c = 'Updated';
            mcp.SIQ_Product_Interactions_Goal_vod__c    = mcp.SIQ_Channel_Interactions_Goal_vod__c;

            if(flag && !uniquePos.contains(mcp.External_ID_Axtria__c))
            {
                mcPlanTarget.add(mcp); 
                uniquePos.add(mcp.External_ID_Axtria__c);    
            }
            
        }
        upsert mcPlanTarget External_ID_Axtria__c;
    }
    
    global void execute(Database.BatchableContext BC, List<Parent_PACP__c> scopePacpProRecs)
    {
        create_MC_Cycle_Plan_Target_vod(scopePacpProRecs);

    }

    global void finish(Database.BatchableContext BC)
    {
        Database.executeBatch(new MarkMCCPtargetDeleted());
    }
}