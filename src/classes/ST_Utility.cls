public with sharing class ST_Utility
{

    public static Boolean getJobStatus(String jobID, String ruleId)
    {
        AsyncApexJob aaj = [SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors, ExtendedStatus FROM AsyncApexJob WHERE ID = :jobID ];
        SnTDMLSecurityUtil.printDebugMessage('Apex job ===' + aaj);
        if(aaj.ExtendedStatus == null && aaj.NumberOfErrors == 0)
        {
            return true;
        }
        else
        {
            updateRuleStatus(ruleId, 'Execution Failed', aaj.ExtendedStatus, '', false);
            return false;
        }


    }
    public static Boolean getJobStatus(String jobID)
    {
        AsyncApexJob aaj = [SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors, ExtendedStatus FROM AsyncApexJob WHERE ID = :jobID ];
        SnTDMLSecurityUtil.printDebugMessage('Apex job ===' + aaj);
        if(aaj.ExtendedStatus == null && aaj.NumberOfErrors == 0)
        {
            return true;
        }
        else
            return false;
    }
    public static void updateRuleStatus(String ruleId, String status, String errorMessage, String errorMessageEndUser, Boolean continueProcessingFlag)
    {
        Measure_Master__c mm = new Measure_Master__c();
        mm.Id = ruleId;
        mm.ErrorDetail__c = errorMessage;
        if(errorMessage != ''){
            mm.EndUserErrorMsg__c = errorMessageEndUser == '' ? 'Please Contact System Admin' : errorMessageEndUser;
            mm.Download_Initiated__c = FALSE;
            mm.Quantile_Initiated__c = FALSE;
        }
        mm.IsContinueProcessing__c = continueProcessingFlag;
        if(status != ''){
            mm.State__c = status;
            mm.Status__c = status;
        }
        mm.isMCCPRuleExecuting__c = false;
        mm.isMCCPRulePublishing__c = false;
        //update mm;
        SnTDMLSecurityUtil.updateRecords(mm, 'ST_Utility');
    }
    public static Boolean isNumericString(String str)
    {
        try
        {
            SnTDMLSecurityUtil.printDebugMessage('str--' + str);
            Decimal d = Decimal.valueOf(str);
        }
        catch(Exception e)
        {
            return false;
        }
        return true;
    }
    public static String fetchNamespace(String className)
    {
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = :className];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        return nameSpace;
    }
    public static void enabledisableTrigger(Map<String, Boolean> triggerNameMap)
    {
        AxtriaSalesIQTM__TriggerContol__c customsetting = new AxtriaSalesIQTM__TriggerContol__c();
        List<AxtriaSalesIQTM__TriggerContol__c> customsettinglist = new List<AxtriaSalesIQTM__TriggerContol__c>();
        for(String key : triggerNameMap.keySet())
        {
            customsetting = AxtriaSalesIQTM__TriggerContol__c.getValues(key);
            if(customsetting != null)
            {
                customsetting.AxtriaSalesIQTM__IsStopTrigger__c = triggerNameMap.get(key);
                customsettinglist.add(customsetting);
            }
            
        }
        if(customsettinglist.size() > 0)
        {
            update customsettinglist;
        }
    }

    public static List<WrapperValidation> fetchRestrictedAccounts(String objectAPI, String condition, String teamIns, String posCode, Map<String, Set<String>> mapAccProd,List<id> recIds)
    {
        Integer count = 0;
        List<SObject> listObject = new List<SObject>();
        String acc_api_name = '', prod_api_name = '';
        Set<String> accNumSet = new Set<String>();
        Set<String> prodNameSet = new Set<String>();
        accNumSet = mapAccProd.keySet();
        Set<String> setAccProd = new Set<String>();
        for(String acc : accNumSet)
        {
            prodNameSet.addAll(mapAccProd.get(acc));
        }
        if(String.isNotBlank(condition))
        {
            if(condition.containsIgnoreCase('in :accNumSet'))
            {
                acc_api_name = getAPIName('accNumSet', condition);
            }
            if(condition.containsIgnoreCase('in :prodNameSet'))
            {
                prod_api_name = getAPIName('prodNameSet', condition);
            }

        }
        String query = 'SELECT ID,';
        if(String.isNotBlank(acc_api_name))
            query += acc_api_name + ',';
        if(String.isNotBlank(prod_api_name))
            query += prod_api_name + ',';
        query = query.removeEnd(',');
        query += ' FROM ' + objectAPI + ' ';

        if(String.isNotBlank(condition))
        {
            query += ' WHERE ';
            query += condition;
        }
        if(!recIds.isEmpty()){
            query += ' and Id IN : recIds';
        }
        
        List<String> list_acc_api = new List<String>();
        List<String> list_prod_api = new List<String>();
        if(String.isNotBlank(acc_api_name))
        {
            list_acc_api = acc_api_name.split('\\.');
        }
        if(String.isNotBlank(prod_api_name))
        {
            list_prod_api = prod_api_name.split('\\.');
        }
        SnTDMLSecurityUtil.printDebugMessage('query ::' + query);
        try
        {
            listObject = Database.Query(query);
            if(listObject.size() > 0)
            {
                SnTDMLSecurityUtil.printDebugMessage('count ::' + listObject.size());

                Boolean flag_non_blank = false;
                String accNumber = '', prodName = '';
                List<WrapperValidation> wrapperValidationList = new List<WrapperValidation>();
                for(Integer i = 0; i < listObject.size(); i++)
                {


                    flag_non_blank = false;
                    accNumber = '';
                    prodName = '';
                    if(String.isNotBlank(acc_api_name))
                    {
                        if(list_acc_api.size() == 1)
                            accNumber = String.valueOf(listObject[i].get(acc_api_name));
                        else if(list_acc_api.size() == 2)
                        {
                            accNumber = String.valueOf(listObject[i].getSObject(list_acc_api[0]).get(list_acc_api[1]));
                        }
                        else if(list_acc_api.size() == 3)
                        {
                            accNumber = String.valueOf(listObject[i].getSObject(list_acc_api[0]).getSObject(list_acc_api[1]).get(list_acc_api[2]));
                        }
                        else if(list_acc_api.size() == 4)
                        {
                            accNumber = String.valueOf(listObject[i].getSObject(list_acc_api[0]).getSObject(list_acc_api[1]).getSObject(list_acc_api[2]).get(list_acc_api[3]));
                        }
                        flag_non_blank = true;
                    }
                    if(String.isNotBlank(prod_api_name))
                    {
                        prodName = String.valueOf(listObject[i].get(prod_api_name));
                        flag_non_blank = true;
                    }
                    if(String.isNotBlank(prod_api_name))
                    {
                        if(list_prod_api.size() == 1)
                            prodName = String.valueOf(listObject[i].get(prod_api_name));
                        else if(list_prod_api.size() == 2)
                        {
                            prodName = String.valueOf(listObject[i].getSObject(list_prod_api[0]).get(list_prod_api[1]));
                        }
                        else if(list_prod_api.size() == 3)
                        {
                            prodName = String.valueOf(listObject[i].getSObject(list_prod_api[0]).getSObject(list_prod_api[1]).get(list_prod_api[2]));
                        }
                        else if(list_prod_api.size() == 4)
                        {
                            prodName = String.valueOf(listObject[i].getSObject(list_prod_api[0]).getSObject(list_prod_api[1]).getSObject(list_prod_api[2]).get(list_prod_api[3]));
                        }
                        flag_non_blank = true;
                    }

                    if(flag_non_blank)
                    {
                        /*If
                        Condition is independent of account (Data has only product )
                        OR
                        Condition is independent of product (Data has only account )
                        OR
                        Condition is combination of account and product

                        Assumption - Data should be consistent*/
                        if(String.isBlank(accNumber) || String.isBlank(prodName) || (mapAccProd.containsKey(accNumber) && mapAccProd.get(accNumber).contains(prodName)))
                        {   //STIMPS-235 check if same combination of account prod already exists
                            if(!setAccProd.contains(accNumber + prodName))
                            {
                                setAccProd.add(accNumber + prodName);
                                wrapperValidationList.add(new WrapperValidation(accNumber, prodName));
                            }

                        }

                    }
                }
                return wrapperValidationList;
            }
            else
            {
                return (new List<WrapperValidation>());
            }
        }
        catch(Exception ex)
        {
            SnTDMLSecurityUtil.printDebugMessage('Exception ::' + ex);
            //return false;
            return (new List<WrapperValidation>());
        }
    }

    public static String getAPIName(String setName, String condition)
    {
        String api_name = '';
        condition = condition.toUpperCase();
        setName = setName.toUpperCase();
        if(condition.contains('IN :' + setName))
        {
            api_name = condition.split('IN :' + setName)[0].trim();
            api_name = api_name.substring(api_name.lastIndexOf(' ') + 1);
        }
        return api_name;
    }
    public static List<Validation_Framework__c> fetchValidationFrameworkRecord(String team_instance, String action)
    {
        List<Validation_Framework__c> validationFrameworkList = new List<Validation_Framework__c>();
        validationFrameworkList = [Select Action__c, Action_Type__c, Condition__c, Error_Message__c, Execution_Order__c, Object__c, Product__c, Target_Field__c, Target_Value__c, Team_Instance__c, Target_Field_Data_Type__c  from Validation_Framework__c where Team_Instance__c = :team_instance AND Action__c = :action  AND Enable__c = true ORDER BY Action__c, Action_Type__c, Execution_Order__c];
        return validationFrameworkList;
    }
    public class WrapperValidation
    {

        public String accountNumber;
        public String productName;
        public WrapperValidation(String accNum, String prodName)
        {
            this.accountNumber = accNum;
            this.productName = prodName;
        }
    }

    public static Map<String, List<String>> fetchSourceToDestinationMapping(String loadType, List<String> teamInstanceList)
    {
        Map <String, List <String>> sourceToDestMapping = new Map <String, List <String>> ();
        List<Source_to_Destination_Mapping__c> mappingList = [SELECT Id, Destination_Object_Field__c, Source_Object_Field__c, Team_Instance__c FROM Source_to_Destination_Mapping__c WHERE  Load_Type__c = :loadType and Team_Instance__c in :teamInstanceList WITH SECURITY_ENFORCED];

        String source_field = '', dest_field = '';
        List<String> tempList;
        for(Integer i = 0, j = mappingList.size(); i < j; i++)
        {
            source_field = mappingList[i].Source_Object_Field__c.toUppercase() + '-' + mappingList[i].Team_Instance__c;
            dest_field = mappingList[i].Destination_Object_Field__c.toUppercase();
            if(!sourceToDestMapping.containsKey(source_field))
            {
                tempList = new List<String>();
            }
            else
            {
                tempList = sourceToDestMapping.get(source_field);
            }
            tempList.add(dest_field);
            sourceToDestMapping.put(source_field, tempList);
        }

        return sourceToDestMapping;

    }
    public static String fetchSourceFieldsInSourceToDestinationObject(Map<String, List<String>> sourceToDestMapping, Set<String> fields_in_query_set)
    {
        String new_fields = '';
        Set<String> new_fields_set = new Set<String>();
        String source_field = '';
        for(String field : sourceToDestMapping.keySet())
        {
            SnTDMLSecurityUtil.printDebugMessage('field ==' + field);
            SnTDMLSecurityUtil.printDebugMessage('new_fields_set' + new_fields_set);
            source_field = field.substring(0, field.indexOf('-'));
            SnTDMLSecurityUtil.printDebugMessage('source_field ==' + source_field);
            if(!fields_in_query_set.contains(source_field) && !new_fields_set.contains(source_field))
            {
                new_fields += source_field + ',';
                new_fields_set.add(source_field);
                SnTDMLSecurityUtil.printDebugMessage('inside if new_fields_set' + new_fields_set);
            }
        }
        new_fields = new_fields.removeEnd(',');
        return new_fields;
    }
    public static Set<String> fetchFieldsUsedInSOQL(String query)
    {
        List<String> fields_in_query = new List<String>();
        Set<String> fields_in_query_set = new Set<String>();
        Set<String> trimmed_set = new Set<String>();


        fields_in_query = query.substring(query.indexOf(' ')).split(',');
        for(String field : fields_in_query)
        {
            trimmed_set.add(field.trim().toUppercase());
        }
        fields_in_query_set = trimmed_set;
        SnTDMLSecurityUtil.printDebugMessage('inside fetchFieldsUsedInSOQL fields_in_query_set' + fields_in_query_set);
        return fields_in_query_set;
    }


}