global with sharing class BatchPositionAccountAddDropCount implements Database.Batchable<sObject>, Database.Stateful{
    
    map<String,Integer> Active_Map;
    map<String,Integer> InActive_Map;  
    public String Country;
    
    Public String query;
    public String batchID;
    global DateTime lastjobDate=null;
    public Integer recordsProcessed=0;
    
    List<PercentChangeView> perCngViewLst;
    public Boolean isAlert;
    String[] recipientsMail;
  
    
    global BatchPositionAccountAddDropCount (String Country){
        isAlert = false;
        perCngViewLst = new List<PercentChangeView>();
        Active_Map = new map<String,Integer>();
        InActive_Map = new map<String,Integer>();
        this.country = country;
        List<AxtriaSalesIQTM__Country__c> countries = [Select Name, Email_ID__c From AxtriaSalesIQTM__Country__c where Name =:Country];
        String allEmailIds = '';
        recipientsMail = new String[] {};
        iF(countries.get(0).Email_ID__c != null){
          allEmailIds = countries.get(0).Email_ID__c;
          SnTDMLSecurityUtil.printDebugMessage('All Email IDs : '+allEmailIds);
          recipientsMail = allEmailIds.split(';');
        }
        SnTDMLSecurityUtil.printDebugMessage('RecipientsMail is '+ recipientsMail);
        
        List<Scheduler_Log__c> schLog = new List<Scheduler_Log__c>();
        schLog = [Select Id,Created_Date__c,Created_Date2__c from Scheduler_Log__c where Job_Name__c= 'Create Position Account Records Count' and Job_Status__c='Successful'  Order By Created_Date2__c desc];
        if(schLog.size() > 0)
        {
          lastjobDate=schLog[0].Created_Date2__c;  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
        SnTDMLSecurityUtil.printDebugMessage('last job'+lastjobDate);
        
        Scheduler_Log__c sJob = new Scheduler_Log__c();
        sJob.Job_Name__c = 'Create Position Account Records Count';
        sJob.Job_Status__c = 'Failed';
        sJob.Job_Type__c='Inbound';
        sJob.Created_Date2__c = DateTime.now();
        //insert sJob;
        SnTDMLSecurityUtil.insertRecords(sJob, 'BatchPositionAccountAddDropCount');
        batchID = sJob.Id;
        

        query = 'SELECT Id, Name,PA_Active__c,PA_Inactive__c, One_day_Previous_Count__c, Current_Count__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Client_Territory_Name__c FROM AxtriaSalesIQTM__Position__c where  AxtriaSalesIQTM__Hierarchy_Level__c = \'1\' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = \'Current\' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = \'Live\'and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name =: Country' ;        
         
    }
       global Database.QueryLocator start(Database.BatchableContext BC) {
        SnTDMLSecurityUtil.printDebugMessage('Position Account Records:::::::::::::' + query);        
        return Database.getQueryLocator(query);
    }
    
     global void execute(Database.BatchableContext BC, List<AxtriaSalesIQTM__Position__c> posList){  

          Set<String> posIdSet = new Set<String>();
          Set<String> posTeamInsIdSet = new Set<String>();
          for(AxtriaSalesIQTM__Position__c pos:posList){
            posIdSet.add(pos.Id);
            posTeamInsIdSet.add(pos.AxtriaSalesIQTM__Team_Instance__c);
          }
          SnTDMLSecurityUtil.printDebugMessage('posIdSet size is ' + posIdSet.size());
          List<AggregateResult> activePosAcc = new List<AggregateResult>();
          List<AggregateResult> inActivePosAcc = new List<AggregateResult>();
          SnTDMLSecurityUtil.printDebugMessage('lastjobDate is ' + lastjobDate);
          if(lastjobDate != null){
            activePosAcc = [SELECT AxtriaSalesIQTM__Position__c, count(Id) con FROM AxtriaSalesIQTM__Position_Account__c WHERE AxtriaSalesIQTM__Position__c IN : posIdSet and AxtriaSalesIQTM__Assignment_Status__c = 'Active' and LastModifiedDate >:lastjobDate group by AxtriaSalesIQTM__Position__c];
            inActivePosAcc = [SELECT AxtriaSalesIQTM__Position__c, count(Id) con FROM AxtriaSalesIQTM__Position_Account__c WHERE AxtriaSalesIQTM__Position__c IN : posIdSet and AxtriaSalesIQTM__Assignment_Status__c = 'InActive' and LastModifiedDate >:lastjobDate group by AxtriaSalesIQTM__Position__c];
          }else{
            activePosAcc = [SELECT AxtriaSalesIQTM__Position__c, count(Id) con FROM AxtriaSalesIQTM__Position_Account__c WHERE AxtriaSalesIQTM__Position__c IN : posIdSet and AxtriaSalesIQTM__Assignment_Status__c = 'Active' and LastModifiedDate = LAST_N_Days:1 group by AxtriaSalesIQTM__Position__c];
            inActivePosAcc = [SELECT AxtriaSalesIQTM__Position__c, count(Id) con FROM AxtriaSalesIQTM__Position_Account__c WHERE AxtriaSalesIQTM__Position__c IN : posIdSet and AxtriaSalesIQTM__Assignment_Status__c = 'InActive' and LastModifiedDate = LAST_N_Days:1 group by AxtriaSalesIQTM__Position__c];
          }

          for(AggregateResult posIdcount : activePosAcc){
            Active_Map.put((String)posIdcount.get('AxtriaSalesIQTM__Position__c'), (Integer)posIdcount.get('con'));
          }
          for(AggregateResult posIdcount : inActivePosAcc){
            InActive_Map.put((String)posIdcount.get('AxtriaSalesIQTM__Position__c'), (Integer)posIdcount.get('con'));
          }

          for(AxtriaSalesIQTM__Position__c pos:posList){
            pos.PA_Active__c = Active_Map.get(pos.Id); 
            pos.PA_Inactive__c  = InActive_Map.get(pos.Id);
          }

          //update PosList;  
          SnTDMLSecurityUtil.updateRecords(PosList, 'BatchPositionAccountAddDropCount');
            
          recordsProcessed +=PosList.size();
          
          List<AxtriaSalesIQTM__Team_Instance__c> teamInstLst = [Select Id, percent_Change_lmt__c From AxtriaSalesIQTM__Team_Instance__c where Id IN:posTeamInsIdSet];
          Map<String,Integer> teamIdLmtMap = new Map<String,Integer>();
          for(AxtriaSalesIQTM__Team_Instance__c teamInst: teamInstLst){
            Integer prtChngLmt = (Integer)teamInst.percent_Change_lmt__c;
            if(prtChngLmt != NULL && prtChngLmt >= 0){
                teamIdLmtMap.put(teamInst.Id, prtChngLmt);
            }else {
                teamIdLmtMap.put(teamInst.Id, 10);
            }
            SnTDMLSecurityUtil.printDebugMessage('teamId : '+teamInst.Id + ' , PCL : '+ (Integer)teamInst.percent_Change_lmt__c);
          }
          for(AxtriaSalesIQTM__Position__c pos : PosList){

            if(pos.AxtriaSalesIQTM__Team_Instance__c != null && pos.One_day_Previous_Count__c != null && pos.Current_Count__c != null){
              Integer changePrct = (Integer)(((pos.One_day_Previous_Count__c - pos.Current_Count__c) / pos.One_day_Previous_Count__c)*100);
              Integer teamPrtLmt = teamIdLmtMap.get(pos.AxtriaSalesIQTM__Team_Instance__c);
              SnTDMLSecurityUtil.printDebugMessage('changePrct is ' + changePrct + ', teamPrtLmt is '+ teamPrtLmt);
              if(teamPrtLmt!=null && changePrct >= teamPrtLmt && recipientsMail != null){
                isAlert = true;
                  PercentChangeView perCngView = new PercentChangeView();
                  perCngView.territoryName = pos.AxtriaSalesIQTM__Client_Territory_Name__c;
                  perCngView.prevCount = String.valueOf(pos.One_day_Previous_Count__c);
                  perCngView.currentCount = String.valueOf(pos.Current_Count__c);
                  perCngView.pertChng = String.valueOf(changePrct);
                  perCngView.breachLmt = String.valueOf(teamPrtLmt);
                  perCngViewLst.add(perCngView);
              }
            }
         }
    }
    
    global void finish(Database.BatchableContext BC) {
        Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID); 
        sJob.No_Of_Records_Processed__c=recordsProcessed;
        sJob.Job_Status__c='Successful';
        //update sJob;
        SnTDMLSecurityUtil.updateRecords(sJob, 'BatchPositionAccountAddDropCount');
        
        SnTDMLSecurityUtil.printDebugMessage('isAlert is ' + isAlert);
        SnTDMLSecurityUtil.printDebugMessage('perCngViewLst size is '+perCngViewLst);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String bodyMessage = '';
        if(isAlert && perCngViewLst.size() > 0){
                  mail.setSubject('Country :: ' + Country + ' Limit Breached');
            mail.setToAddresses(recipientsMail);
            mail.setSenderDisplayName('Country : ' + Country + ' Limit Breached');
            bodyMessage  = 'Limit Breached info file is attached. This is System Generated File.';
            String csvRow = '"TerritoryName","PreviousCount","CurrentCount","%Change","Limit"\n';
            for(PercentChangeView perCngView:perCngViewLst){
                      csvRow += '"' + perCngView.territoryName + '","' + perCngView.prevCount + '","' + perCngView.currentCount + '","'
                    + perCngView.pertChng + '","' + perCngView.breachLmt + '"\n';
            }
            Messaging.Emailfileattachment attachment = new Messaging.Emailfileattachment();
            attachment.setFileName(Country +'_PositionLimitBreached.csv');
            blob excelData = blob.valueOf(csvRow);
            attachment.setBody(excelData);
            SnTDMLSecurityUtil.printDebugMessage('bodyMessage is ' + bodyMessage);
            mail.setHtmlBody(bodyMessage);
            List<Messaging.Emailfileattachment> attachmentList = new List<Messaging.Emailfileattachment>();
            attachmentList.add(attachment);
            mail.setFileAttachments(attachmentList);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }
    
    global class PercentChangeView {
        public String territoryName {get;set;}
        public String prevCount {get;set;}
        public String currentCount {get;set;}
        public String pertChng {get;set;}
        public String breachLmt {get;set;}
    }
}