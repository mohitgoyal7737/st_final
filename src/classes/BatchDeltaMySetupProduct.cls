global class BatchDeltaMySetupProduct implements Database.Batchable<sObject> ,Database.stateful
{
    public String query;
    public Date Lmd;
    
    public List<String>allTeamsInstancesinit;
    public String cycle{get;set;}
    public Set<String> teamInsSet;
    public integer errorcount{get;set;}
    public String nonProcessedAcs {get;set;}
    public String batchID;
    global DateTime lastjobDate=null;
    public Integer recordsProcessed=0;
    public Boolean chaining = false;
    Set<String> positionSet= new Set<String>();
    public Set<String> allCountries;
    public List<String> allTeamsInstances;

    Map<String,Id> teamInstanceNametoIDMap;
    Map<String,string> teamInstanceNametoTeamNameMap;
    Map<String,string> teamInstanceNametoAZCycleMap;
    Map<String,string> teamInstanceNametoAZCycleNameMap;
    Map<String,string> teamInstanceNametoTeamIDMap;
    Map<String,string> teamInstanceNametoBUMap;
    Map<String,string> brandIDteamInstanceNametoBrandTeamInstIDMap;


    global BatchDeltaMySetupProduct(Date passDate)
    {
       /* allTeamsInstances = new Set<String>();
        lmd= passDate;
        //allTeamsInstances = teamInstance;
        teamInstanceNametoIDMap = new Map<String,Id>();
        teamInstanceNametoTeamNameMap = new Map<String,String>();
        teamInstanceNametoAZCycleMap = new Map<String,String>();
        teamInstanceNametoAZCycleNameMap = new Map<String,String>();
        teamInstanceNametoTeamIDMap = new Map<String,String>();
        teamInstanceNametoBUMap = new Map<String,String>();
        brandIDteamInstanceNametoBrandTeamInstIDMap = new Map<String,String>();       
        
       query= 'Select Id, Name, CurrencyIsoCode, AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Metric10__c, AxtriaSalesIQTM__Metric1__c, AxtriaSalesIQTM__Metric2__c, AxtriaSalesIQTM__Metric3__c, AxtriaSalesIQTM__Metric4__c, AxtriaSalesIQTM__Metric5__c, AxtriaSalesIQTM__Metric6__c, AxtriaSalesIQTM__Metric7__c, AxtriaSalesIQTM__Metric8__c, AxtriaSalesIQTM__Metric9__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Position__r.Employee1__r.Employee_PRID__c, AxtriaSalesIQTM__Position__r.Country_Code_Formula__c, AxtriaSalesIQTM__Position__r.Country_Code__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Product_Master__c, AxtriaSalesIQTM__Product_Weight__c, AxtriaSalesIQTM__Product__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__isActive__c, Business_Days_in_Cycle_Formula__c, Business_Days_in_Cycle__c, Call_Capacity_Formula__c, Calls_Day__c, Effective_Days_in_Field_Formula__c, Holidays__c, Other_Days_Off__c, Product_Catalog__c, Product_Catalog__r.Detail_Group__c, Product_Catalog__r.Veeva_External_ID__c, Vacation_Days__c FROM AxtriaSalesIQTM__Position_Product__c  Where LastModifiedDate = Last_N_Days:1 and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = \'Current\' and (AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = \'Live\' OR AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = \'Published\')';       */
        
    }
    
      global BatchDeltaMySetupProduct(Date passDate,List<String> teamInstance)
    {
        allTeamsInstances = new List<String>(teamInstance);
        
        allCountries = new Set<String>();
        lmd= passDate;
        allTeamsInstances = teamInstance;
        teamInstanceNametoIDMap = new Map<String,Id>();
        teamInstanceNametoTeamNameMap = new Map<String,String>();
        teamInstanceNametoAZCycleMap = new Map<String,String>();
        teamInstanceNametoAZCycleNameMap = new Map<String,String>();
        teamInstanceNametoTeamIDMap = new Map<String,String>();
        teamInstanceNametoBUMap = new Map<String,String>();
        brandIDteamInstanceNametoBrandTeamInstIDMap = new Map<String,String>();
        //selectedTeamInstance = teamInstance;
        
        for(AxtriaSalesIQTM__Team_Instance__c teamIns : [Select Id,Name,AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c, AxtriaSalesIQTM__Team__c,AxtriaSalesIQTM__Team__r.Business_Unit_Loopup__c,AxtriaSalesIQTM__Team__r.name,Cycle__c,Cycle__r.name From AxtriaSalesIQTM__Team_Instance__c where id in :allTeamsInstances])
        {
            teamInstanceNametoIDMap.put(teamIns.Name,teamIns.Id);
            teamInstanceNametoTeamNameMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__r.name);
            teamInstanceNametoAZCycleMap.put(teamIns.Name,teamIns.Cycle__c);
            teamInstanceNametoAZCycleNameMap.put(teamIns.Name,teamIns.Cycle__r.name);
            teamInstanceNametoTeamIDMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__c);
            //teamInstanceNametoBUMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__r.Business_Unit_Loopup__c);
            allCountries.add(teamIns.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c);
        }
        
        for(Brand_Team_Instance__c bti :[select id,Brand__c,Brand__r.Veeva_External_ID__c,Team_Instance__c,Team_Instance__r.name from Brand_Team_Instance__c where Team_Instance__c in :allTeamsInstances])
        {
            brandIDteamInstanceNametoBrandTeamInstIDMap.put(bti.Brand__r.Veeva_External_ID__c+bti.Team_Instance__r.name,bti.id);
        } 
        List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
        List<AxtriaSalesIQTM__Team_Instance__c> cycleList = new List<AxtriaSalesIQTM__Team_Instance__c>();
        cycleList=[Select Id,Name, Cycle__r.Name from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current' and Id in :teamInsSet];
          if(cycleList!=null)
          {
            for(AxtriaSalesIQTM__Team_Instance__c t1: cycleList)
            {
                if(t1.Cycle__r.Name !=null && t1.Cycle__r.Name !='')
                cycle = t1.Cycle__r.Name;
            }
          }       
        System.debug(cycle);
        schLogList=[Select Id,CreatedDate,Created_Date2__c from Scheduler_Log__c where Job_Name__c='MySetupProduct Delta' and Job_Status__c='Successful'  Order By CreatedDate desc LIMIT 1];
        if(schLogList.size()>0)
            {
              lastjobDate=schLogList[0].Created_Date2__c.addDays(-1);  //set the lastjobDate to the last successfull batch job run if there exists an entry
            }
            else
            {
              lastjobDate=null;       //else we set the lastjobDate to null
            }
        System.debug('last job'+lastjobDate);
        Scheduler_Log__c sJob = new Scheduler_Log__c();        
            sJob.Job_Name__c = 'MySetupProduct Delta';
            sJob.Job_Status__c = 'Failed';
            sJob.Job_Type__c='Outbound';
        if(cycle!=null && cycle!='')
           sJob.Cycle__c=cycle;
            sJob.Created_Date2__c = datetime.now();
            system.debug(datetime.now());
            system.debug(sJob);
            insert sJob;
            batchID = sJob.Id;
            recordsProcessed =0; 

         if(lastjobDate == null )
         {
            query= 'Select Id, Name, CurrencyIsoCode, AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Metric10__c, AxtriaSalesIQTM__Metric1__c, AxtriaSalesIQTM__Metric2__c, AxtriaSalesIQTM__Metric3__c, AxtriaSalesIQTM__Metric4__c, AxtriaSalesIQTM__Metric5__c, AxtriaSalesIQTM__Metric6__c, AxtriaSalesIQTM__Metric7__c, AxtriaSalesIQTM__Metric8__c, AxtriaSalesIQTM__Metric9__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Position__r.Employee1__r.Employee_PRID__c, AxtriaSalesIQTM__Position__r.Country_Code__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Product_Master__c, AxtriaSalesIQTM__Product_Weight__c, AxtriaSalesIQTM__Product__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__isActive__c, Business_Days_in_Cycle_Formula__c, Business_Days_in_Cycle__c, Call_Capacity_Formula__c, Calls_Day__c, Effective_Days_in_Field_Formula__c, Holidays__c, Other_Days_Off__c, Product_Catalog__c, Product_Catalog__r.Detail_Group__c, Product_Catalog__r.Veeva_External_ID__c, Vacation_Days__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,AxtriaSalesIQTM__Position__r.Country_Code_Formula__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c FROM AxtriaSalesIQTM__Position_Product__c  Where  AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c in :allCountries order by AxtriaSalesIQTM__isActive__c';  

         }
         else
         {
            query= 'Select Id, Name, CurrencyIsoCode, AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Metric10__c, AxtriaSalesIQTM__Metric1__c, AxtriaSalesIQTM__Metric2__c, AxtriaSalesIQTM__Metric3__c, AxtriaSalesIQTM__Metric4__c, AxtriaSalesIQTM__Metric5__c, AxtriaSalesIQTM__Metric6__c, AxtriaSalesIQTM__Metric7__c, AxtriaSalesIQTM__Metric8__c, AxtriaSalesIQTM__Metric9__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Position__r.Employee1__r.Employee_PRID__c, AxtriaSalesIQTM__Position__r.Country_Code__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Product_Master__c, AxtriaSalesIQTM__Product_Weight__c, AxtriaSalesIQTM__Product__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__isActive__c, Business_Days_in_Cycle_Formula__c, Business_Days_in_Cycle__c, Call_Capacity_Formula__c, Calls_Day__c, Effective_Days_in_Field_Formula__c, Holidays__c, Other_Days_Off__c, Product_Catalog__c, Product_Catalog__r.Detail_Group__c, Product_Catalog__r.Veeva_External_ID__c, Vacation_Days__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,AxtriaSalesIQTM__Position__r.Country_Code_Formula__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c FROM AxtriaSalesIQTM__Position_Product__c  Where lastModifiedDate >:lastjobDate and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c in :allCountries order by AxtriaSalesIQTM__isActive__c';  
         }
            
                          
    }

        global BatchDeltaMySetupProduct(Date passDate,List<String> teamInstance, Boolean chain)

    {
        allTeamsInstances = new List<String>(teamInstance);
        chaining = chain;
        allCountries = new Set<String>();
        allTeamsInstances = teamInstance;
        teamInstanceNametoIDMap = new Map<String,Id>();
        teamInstanceNametoTeamNameMap = new Map<String,String>();
        teamInstanceNametoAZCycleMap = new Map<String,String>();
        teamInstanceNametoAZCycleNameMap = new Map<String,String>();
        teamInstanceNametoTeamIDMap = new Map<String,String>();
        teamInstanceNametoBUMap = new Map<String,String>();
        brandIDteamInstanceNametoBrandTeamInstIDMap = new Map<String,String>();
        lmd= passDate;
        //allTeamsInstancesinit = teamInstance; 
        for(AxtriaSalesIQTM__Team_Instance__c teamIns : [Select Id,Name,AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c, AxtriaSalesIQTM__Team__c,AxtriaSalesIQTM__Team__r.Business_Unit_Loopup__c,AxtriaSalesIQTM__Team__r.name,Cycle__c,Cycle__r.name From AxtriaSalesIQTM__Team_Instance__c where id in :allTeamsInstances])
        {
            teamInstanceNametoIDMap.put(teamIns.Name,teamIns.Id);
            teamInstanceNametoTeamNameMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__r.name);
            teamInstanceNametoAZCycleMap.put(teamIns.Name,teamIns.Cycle__c);
            teamInstanceNametoAZCycleNameMap.put(teamIns.Name,teamIns.Cycle__r.name);
            teamInstanceNametoTeamIDMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__c);
            //teamInstanceNametoBUMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__r.Business_Unit_Loopup__c);
            allCountries.add(teamIns.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c);
        }
        system.debug('++allCountries'+allCountries);
        
        for(Brand_Team_Instance__c bti :[select id,Brand__c,Brand__r.Veeva_External_ID__c,Team_Instance__c,Team_Instance__r.name from Brand_Team_Instance__c where Team_Instance__c in :allTeamsInstances])
        {
            brandIDteamInstanceNametoBrandTeamInstIDMap.put(bti.Brand__r.Veeva_External_ID__c+bti.Team_Instance__r.name,bti.id);
        } 
        List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
        List<AxtriaSalesIQTM__Team_Instance__c> cycleList = new List<AxtriaSalesIQTM__Team_Instance__c>();
        cycleList=[Select Id,Name, Cycle__r.Name from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current' and Id in :teamInsSet];
        if(cycleList!=null)
        {
            for(AxtriaSalesIQTM__Team_Instance__c t1: cycleList)
            {
                if(t1.Cycle__r.Name !=null && t1.Cycle__r.Name !='')
                cycle = t1.Cycle__r.Name;
            }
        }
         
        System.debug(cycle);
        schLogList=[Select Id,CreatedDate,Created_Date2__c from Scheduler_Log__c where Job_Name__c='MySetupProduct Delta' and Job_Status__c='Successful'  Order By CreatedDate desc LIMIT 1];
        if(schLogList.size()>0)
        {
            lastjobDate=schLogList[0].Created_Date2__c.addDays(-1);  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
        else
        {
            lastjobDate=null;       //else we set the lastjobDate to null
        }
        System.debug('last job'+lastjobDate);
        Scheduler_Log__c sJob = new Scheduler_Log__c();        
        sJob.Job_Name__c = 'MySetupProduct Delta';
        sJob.Job_Status__c = 'Failed';
        sJob.Job_Type__c='Outbound';
        if(cycle!=null && cycle!='')
           sJob.Cycle__c=cycle;
        sJob.Created_Date2__c = datetime.now();
        system.debug(datetime.now());
        system.debug(sJob);
        insert sJob;
        batchID = sJob.Id;
        recordsProcessed =0;
        if(lastjobDate == null )
        {
            query= 'Select Id, Name, CurrencyIsoCode, AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Metric10__c, AxtriaSalesIQTM__Metric1__c, AxtriaSalesIQTM__Metric2__c, AxtriaSalesIQTM__Metric3__c, AxtriaSalesIQTM__Metric4__c, AxtriaSalesIQTM__Metric5__c, AxtriaSalesIQTM__Metric6__c, AxtriaSalesIQTM__Metric7__c, AxtriaSalesIQTM__Metric8__c, AxtriaSalesIQTM__Metric9__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Position__r.Employee1__r.Employee_PRID__c, AxtriaSalesIQTM__Position__r.Country_Code__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Product_Master__c, AxtriaSalesIQTM__Product_Weight__c, AxtriaSalesIQTM__Product__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__isActive__c, Business_Days_in_Cycle_Formula__c, Business_Days_in_Cycle__c, Call_Capacity_Formula__c, Calls_Day__c, Effective_Days_in_Field_Formula__c, Holidays__c, Other_Days_Off__c, Product_Catalog__c, Product_Catalog__r.Detail_Group__c, Product_Catalog__r.Veeva_External_ID__c, Vacation_Days__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,AxtriaSalesIQTM__Position__r.Country_Code_Formula__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c FROM AxtriaSalesIQTM__Position_Product__c  Where  AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c in :allCountries order by AxtriaSalesIQTM__isActive__c';  

        }
        else
        {
            query= 'Select Id, Name, CurrencyIsoCode, AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Metric10__c, AxtriaSalesIQTM__Metric1__c, AxtriaSalesIQTM__Metric2__c, AxtriaSalesIQTM__Metric3__c, AxtriaSalesIQTM__Metric4__c, AxtriaSalesIQTM__Metric5__c, AxtriaSalesIQTM__Metric6__c, AxtriaSalesIQTM__Metric7__c, AxtriaSalesIQTM__Metric8__c, AxtriaSalesIQTM__Metric9__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Position__r.Employee1__r.Employee_PRID__c, AxtriaSalesIQTM__Position__r.Country_Code__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Product_Master__c, AxtriaSalesIQTM__Product_Weight__c, AxtriaSalesIQTM__Product__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__isActive__c, Business_Days_in_Cycle_Formula__c, Business_Days_in_Cycle__c, Call_Capacity_Formula__c, Calls_Day__c, Effective_Days_in_Field_Formula__c, Holidays__c, Other_Days_Off__c, Product_Catalog__c, Product_Catalog__r.Detail_Group__c, Product_Catalog__r.Veeva_External_ID__c, Vacation_Days__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,AxtriaSalesIQTM__Position__r.Country_Code_Formula__c ,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c FROM AxtriaSalesIQTM__Position_Product__c  Where lastModifiedDate >:lastjobDate and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c in :allCountries order by AxtriaSalesIQTM__isActive__c';  
        }        
    }

    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<AxtriaSalesIQTM__Position_Product__c> scope)
    {
        system.debug('++query++'+query);
        for(AxtriaSalesIQTM__Position_Product__c positionProd:scope)
        {
            allTeamsInstances.add(positionProd.AxtriaSalesIQTM__Team_Instance__c);
        }
        system.debug('+++allTeamsInstances++'+allTeamsInstances);

        SIQ_My_Setup_Products_vod_O__c tempMySetupProdRec = new SIQ_My_Setup_Products_vod_O__c();
        SIQ_My_Setup_Products_vod_O__c tempMySetupProdRec2 = new SIQ_My_Setup_Products_vod_O__c();
        Map<String,SIQ_My_Setup_Products_vod_O__c> MySetupProdMap = new Map<String,SIQ_My_Setup_Products_vod_O__c>();
        Set<String> detailGroup = new Set<String>();
        Map<String,List<Product_Catalog__c>> pCatalogMap= new Map<String,List<Product_Catalog__c>>();
        Map<String,Set<String>> positionEmployeeMap = new  Map<String,Set<String>>();

        for(AxtriaSalesIQTM__Position_Product__c prodC : scope)
        {
            detailGroup.add(prodC.Product_Catalog__r.Detail_Group__c);
            positionSet.add(prodC.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
        }
        system.debug('++detailGroup++'+detailGroup);
        List<Product_Catalog__c> prodCatalog = [select Id,Detail_Group__c, Veeva_External_ID__c, Product_Type__c,Product_code__c FROM Product_Catalog__c where Detail_Group__c in :detailGroup and  Product_Type__c= :'Detail Topic' and AxtriaARSnT__IsActive__c=true];

        for(Product_Catalog__c pCatalog : prodCatalog)
        {
            if(pCatalogMap.containsKey(pCatalog.Detail_Group__c))
            {
                pCatalogMap.get(pCatalog.Detail_Group__c).add(pCatalog);
            }
            else
            {
                List<Product_Catalog__c> pcTemp = new List<Product_Catalog__c>();
                pcTemp.add(pCatalog);
                pCatalogMap.put(pCatalog.Detail_Group__c , pcTemp);
            }
        }

        List<AxtriaSalesIQTM__Position_Employee__c> posEmp= [Select id ,AxtriaSalesIQTM__Position__c, AxtriaARSnT__Employee_PRID__c,  AxtriaSalesIQTM__Assignment_Status__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c from AxtriaSalesIQTM__Position_Employee__c where AxtriaSalesIQTM__Assignment_Status__c=:'Active' and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c in: positionSet];

        system.debug('+++posEmp'+posEmp);
        for(AxtriaSalesIQTM__Position_Employee__c positionEmp: posEmp)
        {
            if(positionEmployeeMap.containsKey(positionEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c))
            {
                Set<String> emp=positionEmployeeMap.get(positionEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
                emp.add(positionEmp.AxtriaARSnT__Employee_PRID__c);
                system.debug('+++emp'+emp);
                positionEmployeeMap.put(positionEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,emp);
            }
            
            else
            {
                positionEmployeeMap.put(positionEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,new set<string>{positionEmp.AxtriaARSnT__Employee_PRID__c});     
            }
            system.debug('+++positionEmployeeMap'+positionEmployeeMap);

        }

        for(AxtriaSalesIQTM__Position_Product__c scsp : scope)
        {
            Set<String> prid= positionEmployeeMap.get(scsp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c); 
            system.debug('+++prid'+prid);
            if(prid != null)
            for(string employeePrid:prid)
           {     
                tempMySetupProdRec = new SIQ_My_Setup_Products_vod_O__c();
                if(scsp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c=='No Cluster')
                    {
                        tempMySetupProdRec.SIQ_Country__c = scsp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                    }
                else if(scsp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c=='SalesIQ Cluster')
                    {
                        tempMySetupProdRec.SIQ_Country__c = scsp.AxtriaSalesIQTM__Position__r.Country_Code_Formula__c;
                    }
                else if (scsp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c=='Veeva Cluster')
                    {
                        tempMySetupProdRec.SIQ_Country__c = scsp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c;
                    }
                if(scsp.AxtriaSalesIQTM__isActive__c==true) 
                {
                    system.debug('+++inside');
                    tempMySetupProdRec.CurrencyIsoCode = scsp.CurrencyIsoCode;
                    tempMySetupProdRec.SIQ_Favorite_vod__c = 'true';
                    tempMySetupProdRec.SIQ_Product_vod__c = scsp.Product_Catalog__r.Veeva_External_ID__c;
                    tempMySetupProdRec.SIQ_Employee_PRID__c = employeePrid;//scsp.AxtriaSalesIQTM__Position__r.Employee1__r.Employee_PRID__c;
                    //tempMySetupProdRec.SIQ_Country__c = scsp.AxtriaSalesIQTM__Position__r.Country_Code_Formula__c;
                    tempMySetupProdRec.External_ID__c = tempMySetupProdRec.SIQ_Country__c + '_' + employeePrid + '_' + scsp.Product_Catalog__r.Veeva_External_ID__c;
                    //system.debug('++++++ Get '+ tempMySetupProdRec.SIQ_Product_vod__c.split('_')[2]);
                    system.debug('++++++  tempMySetupProdRec.External_ID__c++++ '+ tempMySetupProdRec.External_ID__c);

                    tempMySetupProdRec.Type__c = 'Product';
                    tempMySetupProdRec.Status__c = 'Updated';

                    if(!MySetupProdMap.containsKey(tempMySetupProdRec.External_ID__c) && tempMySetupProdRec.SIQ_Employee_PRID__c != null && scsp.Product_Catalog__r.Veeva_External_ID__c != null)
                    {
                        MySetupProdMap.put(tempMySetupProdRec.External_ID__c,tempMySetupProdRec);

                        if(scsp.Product_Catalog__r.Detail_Group__c != null && scsp.Product_Catalog__r.Detail_Group__c != '')
                        {
                            tempMySetupProdRec2 = new SIQ_My_Setup_Products_vod_O__c();
                            tempMySetupProdRec2 = tempMySetupProdRec.clone();
                            tempMySetupProdRec2.SIQ_Product_vod__c = scsp.Product_Catalog__r.Detail_Group__c;
                            tempMySetupProdRec2.External_ID__c = tempMySetupProdRec2.SIQ_Country__c + '_' + employeePrid + '_' + scsp.Product_Catalog__r.Detail_Group__c;
                            tempMySetupProdRec2.Type__c = 'Detail Group'; 
                            tempMySetupProdRec.SIQ_Favorite_vod__c = 'false';
                            tempMySetupProdRec.Status__c = 'Updated';
                            if(!MySetupProdMap.containsKey(tempMySetupProdRec2.External_ID__c) && employeePrid != null && scsp.Product_Catalog__r.Detail_Group__c != null)
                            {
                                MySetupProdMap.put(tempMySetupProdRec2.External_ID__c,tempMySetupProdRec2);
                                if(pCatalogMap.containsKey(scsp.Product_Catalog__r.Detail_Group__c))
                                {
                                    for(Product_Catalog__c pc : pCatalogMap.get(scsp.Product_Catalog__r.Detail_Group__c))
                                    {
                                        tempMySetupProdRec2 = new SIQ_My_Setup_Products_vod_O__c();
                                        tempMySetupProdRec2 = tempMySetupProdRec.clone();
                                        tempMySetupProdRec2.SIQ_Product_vod__c = pc.Veeva_External_ID__c;
                                        tempMySetupProdRec.SIQ_Favorite_vod__c = 'false';
                                        tempMySetupProdRec2.External_ID__c = tempMySetupProdRec2.SIQ_Country__c + '_' + employeePrid+ '_' + pc.Veeva_External_ID__c; 
                                        tempMySetupProdRec2.Type__c = 'Detail Topic';
                                        tempMySetupProdRec2.Status__c = 'Updated';
                                        MySetupProdMap.put(tempMySetupProdRec2.External_ID__c,tempMySetupProdRec2);
                                    }                            
                                }
                            }
                        }
                    }
                }        
                
              else
                {
                    system.debug('+++else');
                    system.debug('+++scsp'+scsp);
                    tempMySetupProdRec.SIQ_Product_vod__c = scsp.Product_Catalog__r.Veeva_External_ID__c;
                    system.debug('scsp.Product_Catalog__r.Veeva_External_ID__c'+scsp.Product_Catalog__r.Veeva_External_ID__c);
                    tempMySetupProdRec.SIQ_Employee_PRID__c = employeePrid;
                    
                    system.debug('tempMySetupProdRec.SIQ_Employee_PRID__c'+tempMySetupProdRec.SIQ_Employee_PRID__c);
                    tempMySetupProdRec.External_ID__c = tempMySetupProdRec.SIQ_Country__c + '_' + employeePrid + '_' + scsp.Product_Catalog__r.Veeva_External_ID__c;
                    tempMySetupProdRec.Status__c = 'Deleted';
                    MySetupProdMap.put(tempMySetupProdRec.External_ID__c,tempMySetupProdRec);
                }
                system.debug('recordsProcessed+'+recordsProcessed);
                recordsProcessed++;
            }
        }     
        upsert MySetupProdMap.values() External_ID__c;
    }

    global void finish(Database.BatchableContext BC)
    {
        Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID); 
        String ErrorMsg ='ERROR COUNT:-'+errorcount+'--'+nonProcessedAcs; 
        system.debug('schedulerObj++++before'+sJob);
         
        //Update the scheduler log with successful
        sJob.No_Of_Records_Processed__c=recordsProcessed;
        sjob.Object_Name__c = 'MySetupProduct Delta';       
        sJob.Job_Status__c='Successful';
        sjob.Changes__c = ErrorMsg;               
        system.debug('sJob++++++++'+sJob);
        update sJob;
        list<string> Teaminstancelistis = new list<string>();
        teaminstancelistis = StaticTeaminstanceList.getDeltaTeamInstancesCallPlan();
        if(chaining)
        {
         AZ_Integration_Delta_Pack a1 = new AZ_Integration_Delta_Pack(teaminstancelistis,true);
        }
    }
}