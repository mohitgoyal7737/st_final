public with sharing class Profileconsenthandler {
    public static void fetchaccounts(list<Account>triggernew, Map<Id, Account> triggerOldMap){
        List<Account> acclist = new list<Account>();
        list<Account> acclist2 = new list<account>();
        System.debug('----Called fetch Accounts---');
        for(Account a : triggernew){
            Account oldAccount = triggerOldMap.get(a.Id);
            system.debug('=================COUNTRY CODE::'+oldAccount.Country_Code__c);
        	if(oldAccount.Country_Code__c=='IT'){
	            if((String.isBlank(a.Profile_Consent__c) || a.Profile_Consent__c=='NO') && oldAccount.Profile_Consent__c == 'YES'){
	                acclist.add(a);
	            }
	            if((String.isBlank(oldAccount.Profile_Consent__c) || oldAccount.Profile_Consent__c=='NO') && a.Profile_Consent__c == 'YES'){
	                acclist2.add(a);
	            }
	            system.debug('---Profile Consent is:'+a.Profile_Consent__c);
	        }
        }
        system.debug('---Account in trigger are:'+acclist);
        if(acclist.size()>0){
            updatecallplan(acclist);
        }
        if(acclist2.size()>0){
            system.debug('=====================CONSENT IS FROM NO TO YES +++++++++++++++++++++++++');
            rerunruleforconsent(acclist2);
        }
        
    }
    public static void updatecallplan(list<Account>updatedaccounts){
        system.debug('---Updatecallplan called--');
       list<id> accounts = new list<id>(); 
       list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>pacp = new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
       list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>updatepacp = new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
       for(Account ac : updatedaccounts){
           accounts.add(ac.id);
       }
       system.debug('---Account id are:'+accounts);
       pacp=[select id,segment__c,Final_TCF__c,Final_TCF_Approved__c,isFinalTCFApproved__c,Final_TCF_Original__c,Proposed_TCF__c,Proposed_TCF2__c,Calculated_TCF__c,Calculated_TCF2__c,Segment2__c,Parameter1__c,Parameter2__c,Parameter3__c,Parameter4__c,Parameter5__c,Parameter6__c,Parameter7__c,Parameter8__c,P2_Parameter1__c,P2_Parameter2__c,P2_Parameter3__c,P2_Parameter4__c,P2_Parameter5__c,P2_Parameter6__c,P2_Parameter7__c,P2_Parameter8__c,P3_Parameter1__c,P3_Parameter2__c,P3_Parameter3__c,P3_Parameter4__c,P3_Parameter5__c,P3_Parameter6__c,P3_Parameter7__c,P3_Parameter8__c,P4_Parameter1__c,P4_Parameter2__c,P4_Parameter3__c,P4_Parameter4__c,P4_Parameter5__c,P4_Parameter6__c,P4_Parameter7__c,P4_Parameter8__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Account__c in:accounts];
       if(pacp.size()>0){
           for(AxtriaSalesIQTM__Position_Account_Call_Plan__c p : pacp){
                AxtriaSalesIQTM__Position_Account_Call_Plan__c pac = new AxtriaSalesIQTM__Position_Account_Call_Plan__c();
                    pac.id= p.id;
                
                    pac.P2_Parameter1__c = ''; 
                    pac.P2_Parameter2__c = '';
                    pac.P2_Parameter3__c = '';
                    pac.P2_Parameter4__c = '';
                    pac.P2_Parameter5__c = '';
                    pac.P2_Parameter6__c = '';
                    pac.P2_Parameter7__c = '';
                    pac.P2_Parameter8__c = '';
                   // pac.P3__c = '';
                    pac.P3_Parameter1__c = '';
                    pac.P3_Parameter2__c = '';
                    pac.P3_Parameter3__c = '';
                    pac.P3_Parameter4__c = '';
                    pac.P3_Parameter5__c = '';
                    pac.P3_Parameter6__c = '';
                    pac.P3_Parameter7__c = '';
                    pac.P3_Parameter8__c = '';
                    
                    //pac.P4__c = '';
                    pac.P4_Parameter1__c = '';
                    pac.P4_Parameter2__c = '';
                    pac.P4_Parameter3__c  = '';
                    pac.P4_Parameter4__c = '';
                    pac.P4_Parameter6__c = '';
                    pac.P4_Parameter7__c = '';
                    pac.P4_Parameter8__c = '';
                    
                    pac.Parameter1__c = '';
                    pac.Parameter2__c = '';
                    pac.Parameter3__c = '';
                    pac.Parameter4__c = '';
                    pac.Parameter5__c = '';
                    pac.Parameter6__c = '';
                    pac.Parameter7__c = '';
                    pac.Parameter8__c= '';
                    pac.segment__c = '';
                    pac.Final_TCF__c=null;
                    pac.Final_TCF_Approved__c = null;
                    pac.Final_TCF_Original__c = null;
                    pac.Proposed_TCF__c = null;
                    pac.Proposed_TCF2__c = null;
                    pac.Calculated_TCF__c = null;
                    pac.Calculated_TCF2__c = null;
                    pac.isFinalTCFApproved__c =false;
                    
                    pac.segment__c = 'ND';
                    pac.Segment2__c = '';
                    updatepacp.add(pac);
               }
       }
       list<Survey_Response__c> sr = [select id,Cycle__c,Physician__c,Response__c,Cycle_ID__c from Survey_Response__c where Physician__c in :Accounts ];
       for(Survey_Response__c s : sr){
           s.Response__c = '';
       }
       update sr;
       
       list<Rating_Response__c> rr = [select id,Physician__c,Response__c from Rating_Response__c where Physician__c in: Accounts];
       for(Rating_Response__c r : rr){
           r.Response__c = '';
       }
       update rr;
       
       try{
           CallPlanSummaryTriggerHandler.execute_trigger = false;
           update updatepacp;
           CallPlanSummaryTriggerHandler.execute_trigger = true;
           System.debug('----updatepacp  is:'+updatepacp);
           list<String> updateCallPlanSegment = new list<String>();
           if(updatepacp != null && updatepacp.size() > 0){
                for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pa: updatepacp){
                    updateCallPlanSegment.add(pa.Id);
                }
            }
            if(updateCallPlanSegment != null && updateCallPlanSegment.size() > 0){
                system.debug('----Calling Execute helper---');
                ruleExecuteHelper.updateSegment(updateCallPlanSegment, true);
                
                system.debug('----Called Execute helper---');
                
            }
           
       }
       catch(Exception e){
       System.debug('---Exception Caught in updating the profile perametrs---');    
        System.debug('--- error: ' + e.getMessage());
        System.debug('--- error: ' + e.getStackTraceString());
       }
        
    }
    
    public static void rerunruleforconsent(list<Account>newaccounts){
        system.debug('---rerunruleforconsent called--');
       list<id> accounts2 = new list<id>(); 
       list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>pacp2 = new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
       list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>updatepacp2 = new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
       for(Account ac : newaccounts){
           accounts2.add(ac.id);
       }
       system.debug('---Account id are:'+accounts2);
       pacp2=[select id,segment__c,Final_TCF__c,Final_TCF_Approved__c,isFinalTCFApproved__c,Final_TCF_Original__c,Proposed_TCF__c,Proposed_TCF2__c,Calculated_TCF__c,Calculated_TCF2__c,Segment2__c,Parameter1__c,Parameter2__c,Parameter3__c,Parameter4__c,Parameter5__c,Parameter6__c,Parameter7__c,Parameter8__c,P2_Parameter1__c,P2_Parameter2__c,P2_Parameter3__c,P2_Parameter4__c,P2_Parameter5__c,P2_Parameter6__c,P2_Parameter7__c,P2_Parameter8__c,P3_Parameter1__c,P3_Parameter2__c,P3_Parameter3__c,P3_Parameter4__c,P3_Parameter5__c,P3_Parameter6__c,P3_Parameter7__c,P3_Parameter8__c,P4_Parameter1__c,P4_Parameter2__c,P4_Parameter3__c,P4_Parameter4__c,P4_Parameter5__c,P4_Parameter6__c,P4_Parameter7__c,P4_Parameter8__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Account__c in:accounts2];
       if(pacp2.size()>0){
           for(AxtriaSalesIQTM__Position_Account_Call_Plan__c p : pacp2){
                AxtriaSalesIQTM__Position_Account_Call_Plan__c pac = new AxtriaSalesIQTM__Position_Account_Call_Plan__c();
                    pac.id= p.id;
                    system.debug('=======isFinalTCFApproved__c====:'+pac.isFinalTCFApproved__c);
                        pac.isFinalTCFApproved__c =false;
                        updatepacp2.add(pac);
                }
       }
       
        try{
           CallPlanSummaryTriggerHandler.execute_trigger = false;
           if(updatepacp2!=null && updatepacp2.size()>0)
                update updatepacp2;
           CallPlanSummaryTriggerHandler.execute_trigger = true;
           System.debug('----updatepacp2  is:'+updatepacp2);
           list<String> updateCallPlanSegment2 = new list<String>();
           if(updatepacp2 != null && updatepacp2.size() > 0){
                for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pa: updatepacp2){
                    updateCallPlanSegment2.add(pa.Id);
                }
            }
            if(updateCallPlanSegment2 != null && updateCallPlanSegment2.size() > 0){
                system.debug('----Calling Execute helper---');
                ruleExecuteHelper.updateSegment(updateCallPlanSegment2, true);
                
                system.debug('----Called Execute helper---');
                
            }
           
       }
       catch(Exception e){
       System.debug('---Exception Caught in updating the profile perametrs---');    
        System.debug('--- error: ' + e.getMessage());
        System.debug('--- error: ' + e.getStackTraceString());
       }
        
    }

}