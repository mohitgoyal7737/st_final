global class BatchDownloadProfilingDataForVeevaObj implements Database.Batchable<sObject>, Database.Stateful{
    public String query;
    /*public String team;
    public String teamIns;
    public String prCode;
    public String prName;
    public Boolean isVeevaData = false;
    public String type;
    public String localeSeperator = ',';
    public String teamInsID;
    public String prodID;
    public Id fileId;*/

    global String results;

   /* public List<String> parameters;
    public List<String> responseValVeeva;
    public Map<String,String> veevaQuesToRes = new Map<String,String>();
    public Map<Staging_Veeva_Cust_Data__c,List<String>> accToAllResponses = new Map<Staging_Veeva_Cust_Data__c,List<String>>();
    public List<String> headerVal;
    public Set<String> pos1;*/


    global BatchDownloadProfilingDataForVeevaObj(String tName, String pCode, String type, Id fileId) {

        results = '';
       /* this.type = type;
        this.prCode = pCode;
        this.fileId = fileId;
        this.teamIns = tName;

        System.debug('Product Code : '+prCode);
        System.debug('teamins Preview Name : '+teamIns);

        List<Product_Catalog__c> prlist = [SELECT Id, Team_Instance__c, Name, Product_Code__c, Team_Instance__r.Name, Team_Instance__r.AxtriaSalesIQTM__Team__r.Name FROM Product_Catalog__c WHERE Product_Code__c =:prCode AND Team_Instance__r.Name =:teamIns AND IsActive__c = true WITH SECURITY_ENFORCED];

        System.debug('prlist size : '+prlist);

        if(!prlist.isEmpty()){
            this.team = prlist[0].Team_Instance__r.AxtriaSalesIQTM__Team__r.Name;
            this.teamIns = prlist[0].Team_Instance__r.Name;
            this.prCode = prlist[0].Product_Code__c;
            this.prName = prlist[0].Name;  
            this.teamInsID = prlist[0].Team_Instance__c;
            this.prodID = prlist[0].Id;
        }
        
        isVeevaData = true;

        
        parameters = new List<String>{System.Label.Account_Number,System.Label.Survey_Name,System.Label.Team,System.Label.TeamInstance,System.Label.Type,System.Label.Product_Code,System.Label.ProductName};

        if(type == 'ProfiledCustomers'){
            query = 'SELECT Id, QUESTION_SHORT_TEXT__c, RESPONSE__c, Account_Lookup__r.AccountNumber, Account_Lookup__r.Type, CreatedDate, LastModifiedDate, SURVEY_NAME__c FROM Staging_Veeva_Cust_Data__c WHERE BRAND_ID__c =:prCode AND Team_Instance__c =:teamIns AND Account_Lookup__c NOT IN (Select Account_Lookup__c FROM Staging_Survey_Data__c WHERE Product_Code__c =:prCode AND Team_Instance__c =:teamIns)';
            this.query = query;

        }else if(type == 'ProfiledCustomersButNotAligned'){

            pos1 = new Set<String>();

            List<AxtriaSalesIQTM__Position_Product__c> ppList = [Select AxtriaSalesIQTM__Position__c, Product_Catalog__r.Name, AxtriaSalesIQTM__Team_Instance__r.Name FROM AxtriaSalesIQTM__Position_Product__c WHERE Product_Catalog__c =:prodID AND AxtriaSalesIQTM__Team_Instance__c =:teamInsID AND AxtriaSalesIQTM__isActive__c = true];
            if(ppList.size()>0){
                for(AxtriaSalesIQTM__Position_Product__c pp : ppList){
                    pos1.add(pp.AxtriaSalesIQTM__Position__c);
                }
            }
            System.debug('Pos Set : '+pos1);
            System.debug('Pos Set Size: '+pos1.size());

            //query = 'SELECT Id, QUESTION_SHORT_TEXT__c, RESPONSE__c, Account_Lookup__r.AccountNumber, Account_Lookup__r.Type, CreatedDate, LastModifiedDate, SURVEY_NAME__c FROM Staging_Veeva_Cust_Data__c WHERE BRAND_ID__c =:prCode AND Team_Instance__c =:teamInsID AND Account_Lookup__c NOT IN (Select Account_Lookup__c FROM Staging_Survey_Data__c WHERE Product_Code__c =:prCode AND Team_Instance__c =:teamIns AND Account_Lookup__c NOT IN (SELECT AxtriaSalesIQTM__Account__c FROM AxtriaSalesIQTM__Position_Account__c WHERE AxtriaSalesIQTM__Position__c IN : pos1 AND (AxtriaSalesIQTM__Assignment_Status__c =\'Active\' OR AxtriaSalesIQTM__Assignment_Status__c =\'Future Active\')))';

            query = 'SELECT Id FROM Staging_Veeva_Cust_Data__c WHERE BRAND_ID__c =:prCode AND Team_Instance__c =:teamIns AND Account_Lookup__c NOT IN (Select Account_Lookup__c FROM Staging_Survey_Data__c WHERE Product_Code__c =:prCode AND Team_Instance__c =:teamIns)';

            this.query = query;
        }*/
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Sobject> scope) {
        /*System.debug('scope size : '+scope.size());
        System.debug('scope : '+scope);
        System.debug('query : '+query);
        System.debug('type : '+type);
        
        Map<String,Staging_Veeva_Cust_Data__c> accNoToVeevaMap = new Map<String,Staging_Veeva_Cust_Data__c>();
        Map<String,String> veevaQuesToRes = new Map<String,String>();

        if(type == 'ProfiledCustomersButNotAligned'){
            System.debug('pos1 size in execute : '+pos1.size());

            List<sObject> scope1 = [SELECT Id, QUESTION_SHORT_TEXT__c, RESPONSE__c, Account_Lookup__r.AccountNumber, Account_Lookup__r.Type, CreatedDate, LastModifiedDate, SURVEY_NAME__c FROM Staging_Veeva_Cust_Data__c WHERE Id IN:scope AND Account_Lookup__c NOT IN (SELECT AxtriaSalesIQTM__Account__c FROM AxtriaSalesIQTM__Position_Account__c WHERE AxtriaSalesIQTM__Position__c IN : pos1 AND (AxtriaSalesIQTM__Assignment_Status__c ='Active' OR AxtriaSalesIQTM__Assignment_Status__c ='Future Active'))];

            System.debug('scope1 size : '+scope1.size());
            System.debug('scope1 : '+scope1);

            if(isVeevaData){
                           
                headerVal = new List<String>();

                for(sObject data: scope1){
                    accNoToVeevaMap.put(String.valueOf(data.getSObject('Account_Lookup__r').get('AccountNumber')),(Staging_Veeva_Cust_Data__c)data);
                    veevaQuesToRes.put(data.getSObject('Account_Lookup__r').get('AccountNumber')+'|'+data.get('QUESTION_SHORT_TEXT__c'), String.valueOf(data.get('RESPONSE__c')));
                }
                for(String s : veevaQuesToRes.keySet()){
                    System.debug('s : '+s);
                    System.debug('s.split : '+s.split('\\|')[1]);
                    if(!headerVal.contains(s.split('\\|')[1])){
                        headerVal.add(s.split('\\|')[1]);
                    }
                }
                System.debug('headerVal : '+headerVal);
                System.debug('headerVal Size : '+headerVal.size());

                //Create Response from Veeva Records
                for(String s : accNoToVeevaMap.keySet()){
                    responseValVeeva = new List<String>();
                    for(Integer i=0; i<headerVal.size();i++){
                        if(veevaQuesToRes.containsKey(s+'|'+headerVal.get(i))){
                            responseValVeeva.add(veevaQuesToRes.get(s+'|'+headerVal.get(i)));
                        }
                        else{
                            responseValVeeva.add('-');
                        }
                    }
                    accToAllResponses.put(accNoToVeevaMap.get(s),responseValVeeva);
                }

            }
        }else if(type == 'ProfiledCustomers'){

            if(isVeevaData){
                // Map<String,Staging_Veeva_Cust_Data__c> accNoToVeevaMap = new Map<String,Staging_Veeva_Cust_Data__c>();
                // Map<String,String> veevaQuesToRes = new Map<String,String>();
                
                headerVal = new List<String>();

                for(sObject data: scope){
                    accNoToVeevaMap.put(String.valueOf(data.getSObject('Account_Lookup__r').get('AccountNumber')),(Staging_Veeva_Cust_Data__c)data);
                    veevaQuesToRes.put(data.getSObject('Account_Lookup__r').get('AccountNumber')+'|'+data.get('QUESTION_SHORT_TEXT__c'), String.valueOf(data.get('RESPONSE__c')));
                }
                for(String s : veevaQuesToRes.keySet()){
                    System.debug('s : '+s);
                    System.debug('s.split : '+s.split('\\|')[1]);
                    if(!headerVal.contains(s.split('\\|')[1])){
                        headerVal.add(s.split('\\|')[1]);
                    }
                }
                System.debug('headerVal : '+headerVal);
                System.debug('headerVal Size : '+headerVal.size());

                //Create Response from Veeva Records
                for(String s : accNoToVeevaMap.keySet()){
                    responseValVeeva = new List<String>();
                    for(Integer i=0; i<headerVal.size();i++){
                        if(veevaQuesToRes.containsKey(s+'|'+headerVal.get(i))){
                            responseValVeeva.add(veevaQuesToRes.get(s+'|'+headerVal.get(i)));
                        }
                        else{
                            responseValVeeva.add('-');
                        }
                    }
                    accToAllResponses.put(accNoToVeevaMap.get(s),responseValVeeva);
                }

            }
        }*/    
    }

    global void finish(Database.BatchableContext BC) {
        /*if(isVeevaData){

            Datetime myDateTime1 = datetime.now();
            String fileTitle1 =  type+ '_' + myDateTime1.format();

            System.debug('accToAllResponses : '+accToAllResponses);
            System.debug('parameters before : '+parameters);
            System.debug('headerVal : '+headerVal);

            parameters.addAll(headerVal);
            parameters.add('Created Date');
            parameters.add('Last Modified Date');

            results  = createCSVHeader(parameters);
            results += '\n';

            System.debug('parameters after : '+parameters);
            for(Staging_Veeva_Cust_Data__c vd : accToAllResponses.keySet()){
                results += createCSVRow(vd);   
            }

            System.debug('results : '+ results);

            ContentVersion file = new ContentVersion(title = fileTitle1 + '.csv', versionData =  Blob.valueOf('\uFEFF'+ results ), pathOnClient = '/' + fileTitle1 + '.csv');
            insert file;

            SnTDMLSecurityUtil.printDebugMessage('File Id : '+file.ID);

            String body;
            String subject;
            subject = 'Download Profiling Data';
            body = 'Hello,'+' \n \n '+'Attached the Aligned Customers Information you requested to send.'+' \n \n ';
            body += 'Name : '+System.UserInfo.getUserEmail() + ' \n ';
            body += 'Download Time : '+System.now() + ' \n ';

            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            email.setSubject( subject );
            email.setToAddresses(new list<String>{ UserInfo.getUserEmail() });
            email.setPlainTextBody( body );
            List<Messaging.EmailFileAttachment> attachments = ContentDocumentAsAttachement(new Id[]{file.Id,fileId});
            email.setFileAttachments(attachments);
            Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
        }*/
    }
    /*private string createCSVHeader(list<String> parameters){
        String headers = '';
        
        for(String parameter: parameters){
            try
            {              
               headers += parameter;//String.valueof(output.value);
               headers += localeSeperator;
               
            }
            catch(Exception ex)
            {
               headers += parameter;
               headers += localeSeperator ;
            }
            
        }
        return headers;
    }*/
    /*private String createCSVRow(Staging_Veeva_Cust_Data__c vd){

        String row = '"';
        System.debug('row in start : '+ row);

        row += String.valueOf(vd.Account_Lookup__r.AccountNumber) + '","';
        System.debug('row after first : '+ row);

        row += String.valueOf(vd.SURVEY_NAME__c) + '","';
        System.debug('row after second : '+ row);

        row += team + '","';
        System.debug('row after third : '+ row);

        row +=  teamIns + '","';
        System.debug('row after fourth : '+ row);

        row +=  String.valueOf(vd.Account_Lookup__r.Type) + '","';
        System.debug('row after fifth : '+ row);

        row +=  prCode + '","';
        System.debug('row after Sixth : '+ row);

        row +=  prName + '","';
        System.debug('row after seventh : '+ row);

        for(String s : accToAllResponses.get(vd)){
            row +=  s + '","';
            System.debug('row in loop : '+ row);
        }
        row +=  Date.valueOf(vd.CreatedDate) + '","';
        System.debug('row after seventh 1: '+ row);

        row +=  Date.valueOf(vd.LastModifiedDate);
        System.debug('row after seventh 2: '+ row);

        row +=  '"\n';
        System.debug('Row before return : '+row);
        return row;
    }

    public static List<Messaging.EmailFileAttachment> ContentDocumentAsAttachement(Id[] contentDocumentIds) {
        List<Messaging.EmailFileAttachment> attachments = new List<Messaging.EmailFileAttachment>{};
        List<ContentVersion> documents                  = new List<ContentVersion>{};
    
        documents.addAll([
          SELECT Id, Title, FileType, VersionData, isLatest, ContentDocumentId
          FROM ContentVersion
          WHERE Id IN :contentDocumentIds
        ]);
    
        for(ContentVersion document: documents) {
          
          Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
          attachment.setBody(document.VersionData);
          attachment.setFileName(document.Title);
          attachments.add(attachment);
        }
        return attachments;
    }*/
}