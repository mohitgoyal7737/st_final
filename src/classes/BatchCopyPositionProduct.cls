/**********************************************************************************************
@author       : SnT Team
@modifiedBy   : Himanshu Tariyal (A0994)
@modifiedDate : 2nd June'2020
@description  : Batch class for copying Position Product data from Source to Destination Team Instances
@Revison(s)   : v1.0
**********************************************************************************************/
global with sharing class BatchCopyPositionProduct implements Database.Batchable<sObject>,Database.Stateful
{
    public String query;
    public String batchName;
    public String sourceTeamInstance;
    public String destTeamInstance;
    public String cols;
    public String callPlanLoggerID;
    public String alignNmsp;

    public Boolean proceedNextBatch = true;

    public List<AxtriaSalesIQTM__Team_Instance__c> teamInst;
    public List<AxtriaSalesIQTM__Position_Product__c> positionProductList;
    public Set<String> existingPosProductSet;

    public Map<String,Id> mapPositionNameToID;
    public Map<String,Id> mapProductNameToID;

    global BatchCopyPositionProduct(String sourceTeamInst,String destTeamInst,String colsString,String loggerID) 
    {
        batchName = 'BatchCopyPositionProduct';
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : constructor invoked-->');

        sourceTeamInstance = sourceTeamInst;
        destTeamInstance = destTeamInst;
        callPlanLoggerID = loggerId;

        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ScenarioAlignmentCtlr'];
        alignNmsp = cs.NamespacePrefix!=null && cs.NamespacePrefix!='' ? cs.NamespacePrefix+'__' : '';
        
        SnTDMLSecurityUtil.printDebugMessage('alignNmsp-->'+alignNmsp);
        SnTDMLSecurityUtil.printDebugMessage('callPlanLoggerID-->'+callPlanLoggerID);
        SnTDMLSecurityUtil.printDebugMessage('sourceTeamInstance-->'+sourceTeamInstance);
        SnTDMLSecurityUtil.printDebugMessage('destTeamInstance-->'+destTeamInstance);

        if(colsString!=null && colsString!=''){
            cols = colsString;       
        }
        else
        {
            cols = 'Id,Calls_Day__c,Holidays__c,isHolidayOverridden__c,Other_Days_Off__c,'+
                    'AxtriaSalesIQTM__Effective_Start_Date__c,Product_Catalog__r.Name,Vacation_Days__c,'+
                    'AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Position__c,'+
                    'Product_Catalog__c,AxtriaSalesIQTM__isActive__c,AxtriaSalesIQTM__Product_Weight__c,'+
                    'AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c';
        }
        SnTDMLSecurityUtil.printDebugMessage('cols-->'+cols);

        teamInst = [select Id,Name, AxtriaSalesIQTM__IC_EffEndDate__c,AxtriaSalesIQTM__IC_EffstartDate__c 
                        from AxtriaSalesIQTM__Team_Instance__c where id =: destTeamInstance WITH SECURITY_ENFORCED];

        String existingPosProdQuery = 'select id from AxtriaSalesIQTM__Position_Product__c where '+
                                        'AxtriaSalesIQTM__Team_Instance__c=:destTeamInstance';
        List<AxtriaSalesIQTM__Position_Product__c> existingPosProductList = Database.query(existingPosProdQuery);
        SnTDMLSecurityUtil.printDebugMessage('existingPosProductList--'+existingPosProductList);

        if(existingPosProductList!=null && existingPosProductList.size()>0){
            SnTDMLSecurityUtil.deleteRecords(existingPosProductList,batchName);
        }

        query = 'SELECT '+cols+' from AxtriaSalesIQTM__Position_Product__c where AxtriaSalesIQTM__Team_Instance__c =: '+
                'sourceTeamInstance and AxtriaSalesIQTM__isActive__c = true '+
                'and Product_Catalog__r.IsActive__c = true WITH SECURITY_ENFORCED';
    }

    global BatchCopyPositionProduct(String sourceTeamInst,String destTeamInst,String colsString) 
    {
        batchName = 'BatchCopyPositionProduct';
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : constructor invoked-->');

        sourceTeamInstance = sourceTeamInst;
        destTeamInstance = destTeamInst;
        SnTDMLSecurityUtil.printDebugMessage('sourceTeamInstance-->'+sourceTeamInstance);
        SnTDMLSecurityUtil.printDebugMessage('destTeamInstance-->'+destTeamInstance);

        if(colsString!=null && colsString!=''){
            cols = colsString;       
        }
        else
        {
            cols = 'Id,Calls_Day__c,Holidays__c,isHolidayOverridden__c,Other_Days_Off__c,'+
                    'AxtriaSalesIQTM__Effective_Start_Date__c,Product_Catalog__r.Name,Vacation_Days__c,'+
                    'AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Position__c,'+
                    'Product_Catalog__c,AxtriaSalesIQTM__isActive__c,AxtriaSalesIQTM__Product_Weight__c,'+
                    'AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c';
        }
        SnTDMLSecurityUtil.printDebugMessage('cols-->'+cols);

        teamInst = [select Id,Name, AxtriaSalesIQTM__IC_EffEndDate__c,AxtriaSalesIQTM__IC_EffstartDate__c 
                        from AxtriaSalesIQTM__Team_Instance__c where id =: destTeamInstance WITH SECURITY_ENFORCED];

        String existingPosProdQuery = 'select id from AxtriaSalesIQTM__Position_Product__c where '+
                                        'AxtriaSalesIQTM__Team_Instance__c=:destTeamInstance';
        List<AxtriaSalesIQTM__Position_Product__c> existingPosProductList = Database.query(existingPosProdQuery);
        SnTDMLSecurityUtil.printDebugMessage('existingPosProductList--'+existingPosProductList);

        if(existingPosProductList!=null && existingPosProductList.size()>0){
            SnTDMLSecurityUtil.deleteRecords(existingPosProductList,batchName);
        }

        query = 'SELECT '+cols+' from AxtriaSalesIQTM__Position_Product__c where AxtriaSalesIQTM__Team_Instance__c =: '+
                'sourceTeamInstance and AxtriaSalesIQTM__isActive__c = true '+
                'and Product_Catalog__r.IsActive__c = true WITH SECURITY_ENFORCED';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : start() invoked--');
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : query--'+query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<AxtriaSalesIQTM__Position_Product__c> scope) 
    {
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : execute() invoked--');

        try
        {
            mapPositionNameToID = new Map<String,Id>();
            mapProductNameToID = new Map<String,Id>();
            positionProductList = new List<AxtriaSalesIQTM__Position_Product__c>();

            //position map
            List<AxtriaSalesIQTM__Position__c> position;
            position = [Select Id,Name,AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__c 
                        from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_Instance__c =: destTeamInstance 
                        WITH SECURITY_ENFORCED];

            for(AxtriaSalesIQTM__Position__c posP : position){
                 mapPositionNameToID.put(posP.AxtriaSalesIQTM__Client_Position_Code__c,posP.Id);
            }
            SnTDMLSecurityUtil.printDebugMessage('mapPositionNameToID--'+mapPositionNameToID);

             // product map
            List<Product_Catalog__c> prodCatalogList = [select Id, Name, Product_Code__c,Team_Instance__c from 
                                                            Product_Catalog__c where IsActive__c=true and 
                                                            Team_Instance__c =: destTeamInstance 
                                                            WITH SECURITY_ENFORCED];

            SnTDMLSecurityUtil.printDebugMessage('prodCatalogList--'+prodCatalogList);
            SnTDMLSecurityUtil.printDebugMessage('prodCatalogList size--'+prodCatalogList.size());

            for(Product_Catalog__c product : prodCatalogList){
                mapProductNameToID.put(product.Name,product.Id);
            }
            SnTDMLSecurityUtil.printDebugMessage('mapProductNameToID--'+mapProductNameToID);

            //copy of position product
            AxtriaSalesIQTM__Position_Product__c posProdDest;
            for(AxtriaSalesIQTM__Position_Product__c posProduct : scope)
            {
                posProdDest = posProduct.clone();
                posProdDest.AxtriaSalesIQTM__Team_Instance__c = destTeamInstance;
                posProdDest.AxtriaSalesIQTM__Position__c = mapPositionNameToID.get(posProduct.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
                posProdDest.Product_Catalog__c= mapProductNameToID.get(posProduct.Product_Catalog__r.Name);
                posProdDest.AxtriaSalesIQTM__Effective_Start_Date__c=teamInst[0].AxtriaSalesIQTM__IC_EffstartDate__c;
                posProdDest.AxtriaSalesIQTM__Effective_End_Date__c=teamInst[0].AxtriaSalesIQTM__IC_EffEndDate__c;
                positionProductList.add(posProdDest);
            }

            SnTDMLSecurityUtil.printDebugMessage('positionProductList--'+positionProductList);
            SnTDMLSecurityUtil.printDebugMessage('positionProductList size--'+positionProductList.size());

            if(!positionProductList.isEmpty()){
               SnTDMLSecurityUtil.insertRecords(positionProductList, 'CopyPositionProduct');
            }

        }
        catch(Exception e)
        {
            SnTDMLSecurityUtil.printDebugMessage('Error in '+batchName+' : execute()-->'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack Trace-->'+e.getStackTraceString());
            proceedNextBatch = false;

            SnTDMLSecurityUtil.printDebugMessage('callPlanLoggerID--'+callPlanLoggerID);

            if(callPlanLoggerID!=null && callPlanLoggerID!=''){
                SalesIQUtility.updateCallPlanLoggerRecord(callPlanLoggerID,'Failed at copying Position Product data','Error',alignNmsp);
            }
        }
    }

    global void finish(Database.BatchableContext BC) 
    {
        try
        {
            SnTDMLSecurityUtil.printDebugMessage(batchName+' : finish() invoked--');
            SnTDMLSecurityUtil.printDebugMessage('proceedNextBatch--'+proceedNextBatch);

            if(proceedNextBatch)
            {
                CopyCallPlanScenarioTriggerHandler call = new CopyCallPlanScenarioTriggerHandler(sourceTeamInstance,destTeamInstance,callPlanLoggerID);
                call.copyCIMConfigs();
            }
        }
        catch(Exception e)
        {
            SnTDMLSecurityUtil.printDebugMessage('Error in '+batchName+' : finish()-->'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack Trace-->'+e.getStackTraceString());
            proceedNextBatch = false;

            SnTDMLSecurityUtil.printDebugMessage('callPlanLoggerID--'+callPlanLoggerID);

            if(callPlanLoggerID!=null && callPlanLoggerID!='')
            {
                SalesIQUtility.updateCallPlanLoggerRecord(callPlanLoggerID,'Failed at copying Product Catalog data','Error',alignNmsp);
            }
        }
    }
}