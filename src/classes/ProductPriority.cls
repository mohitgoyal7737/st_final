public with sharing class ProductPriority extends BusinessRule implements IBusinessRule
{
    public List<SelectOption> adoptPotList{get;set;}
    public List<Step__c> listSteps;
    public List<SelectOption> adoptPotConcCurveList{get;set;}
    public list<Decimal> sumList {get;set;}
    
    public String selectedAdopt{get;set;}
    public String selectedPot{get;set;}
    public String graphString{get;set;} 
    public String individualGraphString{get;set;}
    public String adoptOrPot{get;set;}
    public Boolean errorFlag{get;set;} // - Added for SR-453 on 11th Jan

    public Boolean showCurveData{get;set;}

    public List<SelectOption> ProductPriorityList {get;set;}
    public String selectedP1 {get;set;}
    public String selectedP2 {get;set;}
    public String selectedP3 {get;set;}
    public List<SelectOption> ProductPotentialList {get;set;}
    public String selectedPP1 {get;set;}
    public String selectedPP2 {get;set;}
    public String selectedPP3 {get;set;}
    public String ruleId {get;set;}
    public transient List<SLDSPageMessage> PageMessages{get;set;}
    public ProductPriority()
    {
        init();
        ruleId = ApexPages.currentPage().getParameters().get('rid');
        adoptPotList = new List<SelectOption>();
        adoptPotConcCurveList = new List<SelectOption>();
        isAddNew = false;
        showCurveData = false;
        uiLocation = 'Product Priority';
        adoptOrPot = 'Adoption';
        selectedMatrix = '';
        selectedAdopt = '';
        selectedPot = '';
        graphString = '';
        individualGraphString = '';
        retUrl = '/apex/ProductPriority?mode=' +mode+'&rid='+ruleId; 
        selectedP1 = '';
        selectedP2 = '';
        selectedP3 = '';
        initStep();
        //createAdoptPotList();
        createProductPriorityList();
        createProductPotentialList();
        //createConcCurve();
    }

    public void save()
    {
        SnTDMLSecurityUtil.printDebugMessage('in saveAndNext fn:-->');
        try{
		errorFlag = false;// -- Added by RT for SR-453 -
        string result = validateStep(uiLocation);
        SnTDMLSecurityUtil.printDebugMessage('result-->'+result);
        SnTDMLSecurityUtil.printDebugMessage('step-->'+step);
		
        if(String.isNotBlank(result)){
			System.debug('In Save error block');
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, result));
			errorFlag = true;// -- Added by RT for SR-453 -
			PageMessages = SLDSPageMessage.add(PageMessages,result,'error'); // -- Added by RT for SR-453 --
		}
		else{
			if(step != null && String.isBlank(result))
			{
				step.UI_Location__c = uiLocation;
				/*SnTDMLSecurityUtil.printDebugMessage('---step.Step_Type__c--- ' + step.Step_Type__c);*/
				if(step.Step_Type__c == 'Matrix'){
					step.Matrix__c = selectedMatrix;
					step.Grid_Param_1__c = gridParam1;
					if(gridMap.Grid_Type__c == '2D'){
						step.Grid_Param_2__c = gridParam2;
					}
				}
				else if(step.Step_Type__c == 'Compute'){
					/*SnTDMLSecurityUtil.printDebugMessage('--selectedCompF2-- ' + selectedCompF2);*/
					/*SnTDMLSecurityUtil.printDebugMessage('--selectedCompF1-- ' + selectedCompF1);*/
					computeObj.Field_1__c = selectedCompF1;
					computeObj.Field_2_Type__c = field2Type;
					if(String.isNotBlank(selectedCompF2))
						computeObj.Field_2__c = selectedCompF2;
					else{
						computeObj.Field_2_val__c = selectedCompF3;
					}
					//upsert computeObj;
					SnTDMLSecurityUtil.upsertRecords(computeObj, 'ProductPriority');
					step.Compute_Master__c = computeObj.Id;
				}
				/* Shivansh - A1450 */
				else if(step.Step_Type__c == 'Quantile'){
					/*SnTDMLSecurityUtil.printDebugMessage('--selectedCompF2-- ' + selectedCompF2);*/
					/*SnTDMLSecurityUtil.printDebugMessage('--selectedCompF1-- ' + selectedCompF1);*/
					computeObj.Field_1__c = selectedCompF1;
					computeObj.Field_2_val__c = selectedCompF3;
					computeObj.Expression__c = expression;
					//upsert computeObj;
					SnTDMLSecurityUtil.upsertRecords(computeObj, 'ProductPriority');
					step.Compute_Master__c = computeObj.Id;
					step.isQuantileComputed__c = false;
				}
				/* Shivansh - A1450 till Here*/
				else if(step.Step_Type__c == 'Cases'){
					computeObj.Expression__c = expression;
					//upsert computeObj;
					SnTDMLSecurityUtil.upsertRecords(computeObj, 'ProductPriority');
					step.Compute_Master__c = computeObj.Id;
				}
				/*SnTDMLSecurityUtil.printDebugMessage('---saving step -- ' + step);*/
				//upsert step;
				SnTDMLSecurityUtil.upsertRecords(step, 'ProductPriority');

				if(!isStepEditMode){
					//Update Next steps if any
					//list<Step__c> allNextSteps = [SELECT Id, Sequence__c FROM Step__c WHERE Measure_Master__c =:ruleObject.Id AND Sequence__c >=: step.Sequence__c ORDER BY Sequence__c];
					list<Step__c> allNextSteps ;
					try{
						allNextSteps = [SELECT Id, Sequence__c FROM Step__c WHERE Measure_Master__c =:ruleObject.Id AND Sequence__c >=: step.Sequence__c WITH SECURITY_ENFORCED ORDER BY Sequence__c];
					}
						catch(Exception qe) 
					{
							PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
							SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
						SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
					}
					Decimal duplicateSeq = 0;
						map<Decimal, list<Step__c>> stepSeqMap = new map<Decimal,list<Step__c>>();
						if(allNextSteps != null && allNextSteps.size() > 0){
							// creating a map of id and sequence
							
							for(Step__c stepObj: allNextSteps){
								if(stepSeqMap.get(stepObj.Sequence__c) == null)
									stepSeqMap.put(stepObj.Sequence__c, new list<Step__c>{stepObj});
								else{
									list<Step__c> stepList = stepSeqMap.get(stepObj.Sequence__c);
									stepList.add(stepObj);
									stepSeqMap.put(stepObj.Sequence__c, stepList);
								}
							}

						System.debug('stepSeqMap :'+stepSeqMap);

						for(Step__c st: allNextSteps){
							System.debug('st - '+st);
							System.debug('duplicateSeq :'+duplicateSeq);
							list<Step__c> tempStepList = stepSeqMap.get(st.Sequence__c);
							if(tempStepList.size() > 1){
								for(Step__c obj : tempStepList){
									if(st.Id != step.Id && step.Id != obj.Id){
										st.Sequence__c += 1;
										duplicateSeq = st.Sequence__c;
									}
								}
							}else{
								
								if(st.Sequence__c == duplicateSeq){
									st.Sequence__c += 1;
									duplicateSeq = st.Sequence__c;
								}
							}
						}

						System.debug('allNextSteps after update:'+allNextSteps);
						/*for(Step__c st: allNextSteps){
							System.debug('st - '+st);
							if(st.Id != step.Id){
								st.Sequence__c += 1;
							}
						}*/
					}
					//Update allNextSteps;
					SnTDMLSecurityUtil.updateRecords(allNextSteps, 'ProductPriority');
				}
					Rule_Parameter__c rp = new Rule_Parameter__c();
					if(String.isNotBlank(step.Id)){
						//list<Rule_Parameter__c> rps = [select id from Rule_Parameter__c WHERE Step__c =: step.Id];
						list<Rule_Parameter__c> rps ;
						try{
							rps = [select id from Rule_Parameter__c WHERE Step__c =: step.Id WITH SECURITY_ENFORCED];
						}
							catch(Exception qe) 
						{
								PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
								SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
							SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
						}
						if(rps != null && rps.size() > 0){
							rp.Id = rps[0].Id;
						}
					}
					rp.Measure_Master__c = ruleObject.Id;
					rp.Step__c = step.Id;
					rp.Type__c = step.Type__c;
					//upsert rp;           
					SnTDMLSecurityUtil.upsertRecords(rp,'ProductPriority');
					//updateRule(uiLocation, 'Profiling Parameter');
					//createAdoptPotList();
					updateRule(uiLocation, 'Compute Accessibility');
					createProductPriorityList();
					createProductPotentialList();
				}

			}
        }
        catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'ProductPriority',ruleId);
            } 
    }
    
    public void createProductPriorityList()
    {
        /*SnTDMLSecurityUtil.printDebugMessage('in createAdoptPotMap fn-->');
        adoptPotConcCurveList = new List<SelectOption>();
        adoptPotConcCurveList.add(new SelectOption('Adoption','Adoption'));
        adoptPotConcCurveList.add(new SelectOption('Potential','Potential'));*/
        
        try{
        ProductPriorityList = new List<SelectOption>();
        /*listSteps = [SELECT Id,name,Product_Priority__c FROM Step__c WHERE Measure_Master__c =:ruleObject.Id and Step_Type__c = 'Cases' 
                        and UI_Location__c = 'Product Priority']; */
        try{
            listSteps = [SELECT Id,name,Product_Priority__c FROM Step__c WHERE Measure_Master__c =:ruleObject.Id and Step_Type__c = 'Cases' 
                        and UI_Location__c = 'Product Priority' WITH SECURITY_ENFORCED];
        }
            catch(Exception qe) 
        {
                PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }
        
        SnTDMLSecurityUtil.printDebugMessage('listSteps-->'+listSteps);
        ProductPriorityList.add(new SelectOption('--NONE--','--NONE--'));
        if(listSteps!=null && listSteps.size()>0)
        {
            for(Step__c step : listSteps)
            {
                ProductPriorityList.add(new SelectOption(step.id,step.name));
                if(step.Product_Priority__c=='1'){
                    selectedP1 = step.id;   
                }
                if(step.Product_Priority__c=='2'){
                    selectedP2 = step.id; 
                }
                if(step.Product_Priority__c=='3'){
                    selectedP3 = step.id; 
                }
            }
            SnTDMLSecurityUtil.printDebugMessage('selectedP1-->'+selectedP1+'*****selectedP2-->'+selectedP2);
        }
    }
        catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'ProductPriority',ruleId);
            } 
    }
    public void createProductPotentialList()
    {
        /*SnTDMLSecurityUtil.printDebugMessage('in createAdoptPotMap fn-->');
        adoptPotConcCurveList = new List<SelectOption>();
        adoptPotConcCurveList.add(new SelectOption('Adoption','Adoption'));
        adoptPotConcCurveList.add(new SelectOption('Potential','Potential'));*/
        try{
        ProductPotentialList = new List<SelectOption>();
        List<Rule_Parameter__c> listParameters = new List<Rule_Parameter__c>();
        try{
            listParameters = [SELECT Id,name,Parameter_Name__c,Product_Potential_Type__c FROM Rule_Parameter__c WHERE Measure_Master__c =:ruleObject.Id and name != null WITH SECURITY_ENFORCED];
        }
            catch(Exception qe) 
        {
                PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }
        SnTDMLSecurityUtil.printDebugMessage('listParameters-->'+listParameters);
        ProductPotentialList.add(new SelectOption('--NONE--','--NONE--'));
        
            for(Rule_Parameter__c param : listParameters)
            {
                ProductPotentialList.add(new SelectOption(param.id,param.Parameter_Name__c));
                //if(param.Product_Potential_Type__c=='1'){
                if(param.Product_Potential_Type__c!=null && param.Product_Potential_Type__c.contains('1')){
                    selectedPP1 = param.id;   
                }
                if(param.Product_Potential_Type__c!=null && param.Product_Potential_Type__c.contains('2')){
                    selectedPP2 = param.id; 
                }
                if(param.Product_Potential_Type__c!=null && param.Product_Potential_Type__c.contains('3')){
                    selectedPP3 = param.id; 
                }
            }
            SnTDMLSecurityUtil.printDebugMessage('selectedPP1-->'+selectedPP1+'*****selectedPP2-->'+selectedPP2);
        }
        catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'ProductPriority',ruleId);
            } 
    }
    /*public void createAdoptPotList()
    {
        SnTDMLSecurityUtil.printDebugMessage('in createAdoptPotMap fn-->');
        adoptPotConcCurveList = new List<SelectOption>();
        adoptPotConcCurveList.add(new SelectOption('Adoption','Adoption'));
        adoptPotConcCurveList.add(new SelectOption('Potential','Potential'));
        
        adoptPotList = new List<SelectOption>();
        listSteps = [SELECT Id,name,Modelling_Type__c FROM Step__c WHERE Measure_Master__c =:ruleObject.Id and Step_Type__c = 'Cases' 
                        and UI_Location__c = 'Compute Values']; 
        SnTDMLSecurityUtil.printDebugMessage('listSteps-->'+listSteps);
        adoptPotList.add(new SelectOption('--NONE--','--NONE--'));
        if(listSteps!=null && listSteps.size()>0)
        {
            for(Step__c step : listSteps)
            {
                adoptPotList.add(new SelectOption(step.id,step.name));
                if(step.Modelling_Type__c=='Potential'){
                    selectedPot = step.id;   
                }
                if(step.Modelling_Type__c=='Adoption'){
                    selectedAdopt = step.id; 
                }
            }
            SnTDMLSecurityUtil.printDebugMessage('selectedAdopt-->'+selectedAdopt+'*****selectedPot-->'+selectedPot);
        }
    }*/
    
    /*public void createConcCurve()
    {
        SnTDMLSecurityUtil.printDebugMessage('in createConcCurve() fn-->');
        showCurveData = false;
        graphString = '';
        individualGraphString = '';
        
        Decimal totalCount = 0;
        Decimal cumulativeSum=0;
        Integer i=1;
        
        List<BU_Response__c> buList = new List<BU_Response__c>();
        set<String> uniqueAccs = new set<String>();
        sumList = new list<Decimal>();
        
        map<Integer,Decimal> mapCountToIndividualVal = new map<Integer,Decimal>();
        String productSelected;
        String teaminstanceSelected;
        List<Measure_Master__c> mmList = [select id,Team_Instance__c,Brand_Lookup__c FROM Measure_Master__c WHERE ID=:ruleObject.id];
        if(mmList!=null && mmList.size()>0){
            productSelected = mmList[0].Brand_Lookup__c;
            teaminstanceSelected = mmList[0].Team_Instance__c;
        }
        
        SnTDMLSecurityUtil.printDebugMessage('productSelected-->'+productSelected+'*****teaminstanceSelected-->'+teaminstanceSelected+'*****adoptOrPot-->'+adoptOrPot);
        List<Brand_Team_Instance__c> btiList = [select id from Brand_Team_Instance__c where Brand__c=:productSelected and Team_Instance__c=:teaminstanceSelected];
        SnTDMLSecurityUtil.printDebugMessage('btiList size-->'+btiList.size());
        if(btiList!=null && btiList.size()>0)
        {
            String query = 'select Response1__c,Response2__c,Physician__c from BU_Response__c where Brand__c= \''+btiList[0].id+'\'';
            String addQuery = ' order by '+(adoptOrPot=='Adoption' ? 'Response1__c' : 'Response2__c') + ' desc limit 40000';
            buList = Database.query(query+addQuery);
            if(buList!=null && buList.size()>0)
            {
                AddressWrapper[] addressList = new AddressWrapper[0];
                for(BU_Response__c BU : buList)
                {
                    if(!uniqueAccs.contains(bu.Physician__c))
                    {
                        String variable = (adoptOrPot=='Adoption' ? (String)bu.get('Response1__c') : (String)bu.get('Response2__c'));
                        Decimal val = variable!=null ? Decimal.valueOf(variable) : 0;
                        addressList.add(new AddressWrapper(bu.Physician__c,Integer.valueOf(val)));
                        totalCount+=val;
                        i++;
                    }
                }
                addressList.sort(); //Please don't remove this function
                if(totalCount > 0)
                {
                    for(i=0;i<addressList.size();i++)
                    {
                        cumulativeSum +=addressList[addressList.size()-i-1].responseVal;
                        sumList.add(cumulativeSum/totalCount); 
                        mapCountToIndividualVal.put(i,addressList[addressList.size()-i-1].responseVal);
                        graphString += String.valueOf(sumList[i])+';';
                    }
                    if(graphString!=null)
                        graphString = graphString.removeEnd(';');    
                }
                if(mapCountToIndividualVal!=null){
                    individualGraphString = JSON.serialize(mapCountToIndividualVal);
                    showCurveData = true;
                }
                SnTDMLSecurityUtil.printDebugMessage('individualGraphString-->'+individualGraphString);
                SnTDMLSecurityUtil.printDebugMessage('graphString-->'+graphString);
            }
        }
    }*/
    
    /*public void createConcCurve()
    {
        try
        {
            SnTDMLSecurityUtil.printDebugMessage('in createConcCurve() fn-->');
            showCurveData = false;
            graphString = '';
            individualGraphString = '';
            
            Decimal totalCount = 0;
            Decimal cumulativeSum=0;
            Decimal respVal = 0;
            Integer i=1;
            
            List<BU_Response__c> buList = new List<BU_Response__c>();
            set<String> uniqueAccs = new set<String>();
            sumList = new list<Decimal>();
            
            map<Integer,Decimal> mapCountToIndividualVal = new map<Integer,Decimal>();
            String productSelected;
            String teaminstanceSelected;
            List<Measure_Master__c> mmList = [select id,Team_Instance__c,Brand_Lookup__c FROM Measure_Master__c WHERE ID=:ruleObject.id];
            if(mmList!=null && mmList.size()>0){
                productSelected = mmList[0].Brand_Lookup__c;
                teaminstanceSelected = mmList[0].Team_Instance__c;
            }
            
            SnTDMLSecurityUtil.printDebugMessage('productSelected-->'+productSelected+'*****teaminstanceSelected-->'+teaminstanceSelected+'*****adoptOrPot-->'+adoptOrPot);
            List<Brand_Team_Instance__c> btiList = [select id from Brand_Team_Instance__c where Brand__c=:productSelected and Team_Instance__c=:teaminstanceSelected];
            SnTDMLSecurityUtil.printDebugMessage('btiList size-->'+btiList.size());
            if(btiList!=null && btiList.size()>0)
            {
                String field = adoptOrPot=='Adoption' ? 'AdoptionVal__c' : 'PotentialVal__c';
                String query = 'select '+field+',Physician__c from BU_Response__c where Brand__c= \''+btiList[0].id+'\' order by '+field+ ' desc limit 30000';
                buList = Database.query(query);
                if(buList!=null && buList.size()>0)
                {
                    for(BU_Response__c BU : buList)
                    {
                        if(!uniqueAccs.contains(bu.Physician__c))
                        {
                            respVal = 0;
                            try{
                                respVal = bu.get(field)!=null ? (Decimal)bu.get(field) : 0;
                            }
                            catch(TypeException e){
                            }
                            totalCount+=respVal;
                            sumList.add(totalCount);
                            mapCountToIndividualVal.put(i,respVal);
                            i++;
                        }
                    }
                    buList.clear();
                    if(totalCount > 0)
                    {
                        for(i=0;i<sumList.size();i++)
                        {
                            //sumList[i] = (sumList[i]/totalCount).setScale(3);
                            graphString += String.valueOf((sumList[i]/totalCount).setScale(3))+';';
                        }
                        if(graphString!=null)
                            graphString = graphString.removeEnd(';');    
                    }
                    if(mapCountToIndividualVal!=null){
                        individualGraphString = JSON.serialize(mapCountToIndividualVal);
                        if(graphString!='')
                            showCurveData = true;
                    }
                    sumList.clear();
                    mapCountToIndividualVal.clear();
                    SnTDMLSecurityUtil.printDebugMessage('individualGraphString-->'+individualGraphString);
                    SnTDMLSecurityUtil.printDebugMessage('graphString-->'+graphString);
                }
            }
            
        }
        catch(Exception e){
            showCurveData = false;
            SnTDMLSecurityUtil.printDebugMessage('Error message-->'+e.getMessage());
        }
    }*/
    
    public static Boolean numberPresent(String val)
    {
        Boolean isValidDecimal = true;
        try{
            Decimal.valueOf(val);
        }
        catch(TypeException e){
            isValidDecimal = false; 
        }
        return isValidDecimal;
    }
    
    public class AddressWrapper implements Comparable 
    {
        public String phyName;
        public Integer responseVal;
        public AddressWrapper(String phyName, Integer responseVal) {
            this.phyName = phyName;
            this.responseVal = responseVal;
        }
        public Integer compareTo(Object other) {
            return responseVal-((AddressWrapper)other).responseVal;
        }
    }
    
    public PageReference saveAdoptPotValAndExit()
    {
        try{
        SnTDMLSecurityUtil.printDebugMessage('saveAdoptPotValAndExit function-->selectedAdopt-->'+selectedAdopt+'*********selectedPot-->'+selectedPot);
        /*listSteps = [select id,Product_Priority__c from Step__c where Measure_Master__c =:ruleObject.Id and Step_Type__c = 'Cases'
                        and UI_Location__c = 'Product Priority'];*/
        try{
            listSteps = [select id,Product_Priority__c from Step__c where Measure_Master__c =:ruleObject.Id and Step_Type__c = 'Cases'
                        and UI_Location__c = 'Product Priority' WITH SECURITY_ENFORCED];
        }
            catch(Exception qe) 
        {
                PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }
        List<Rule_Parameter__c> listRuleParam = new List<Rule_Parameter__c>();
        if(listSteps!=null && listSteps.size()>0)
        {
            for(Step__c step : listSteps){
                step.Product_Priority__c = null;
                if(selectedP1!=null && selectedP1!='--NONE--'){
                    if(step.id==selectedP1)
                        step.Product_Priority__c = '1';
                }
                if(selectedP2!=null && selectedP2!='--NONE--'){
                    if(step.id==selectedP2)
                        step.Product_Priority__c = '2';
                }
                if(selectedP3!=null && selectedP3!='--NONE--'){
                    if(step.id==selectedP3)
                        step.Product_Priority__c = '3';
                }
            }
            list<Rule_Parameter__c> rptemp ;
            try{
                rptemp = [Select id, Product_Potential_Type__c from Rule_Parameter__c where  Measure_Master__c =:ruleObject.Id WITH SECURITY_ENFORCED];
            }
                catch(Exception qe) 
            {
                     PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                     SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
                SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
            }
            for(Rule_Parameter__c rp: rptemp){
                rp.Product_Potential_Type__c = null;
                if(selectedPP1!=null && selectedPP1!='--NONE--'){
                    if(rp.id==selectedPP1)
                        rp.Product_Potential_Type__c = rp.Product_Potential_Type__c == null ? '1': rp.Product_Potential_Type__c + ',1';
                }
                if(selectedPP2!=null && selectedPP2!='--NONE--'){
                    if(rp.id==selectedPP2)
                        //rp.Product_Potential_Type__c = '2';
                    rp.Product_Potential_Type__c = rp.Product_Potential_Type__c == null ? '2': rp.Product_Potential_Type__c + ',2';
                }
                if(selectedPP3!=null && selectedPP3!='--NONE--'){
                    if(rp.id==selectedPP3)
                        //rp.Product_Potential_Type__c = '3';
                        rp.Product_Potential_Type__c = rp.Product_Potential_Type__c == null ? '3': rp.Product_Potential_Type__c + ',3';
                }
                listRuleParam.add(rp);
            }
            updateRule('Summary', uiLocation);
            //update listSteps;
            SnTDMLSecurityUtil.updateRecords(listSteps, 'ProductPriority');
            if(listRuleParam.size()>0){
                //update listRuleParam;
                SnTDMLSecurityUtil.updateRecords(listRuleParam, 'ProductPriority');
            }
        }
        return nextPage('SummaryPage');
    }
        catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'ProductPriority',ruleId);
            return null;
            } 
    }

    public PageReference saveAndNext(){
        try{
        SnTDMLSecurityUtil.printDebugMessage('in saveAndNext fn:-->');
        save();
        //Commented by Himanshu Tariyal on 2nd Jan 2019 for including the Modal popup for selecting the Adoption/Potential step
        //updateRule('Compute Segment', uiLocation);
        return null;
        //Commented by Himanshu Tariyal on 2nd Jan 2019 for including the Modal popup for selecting the Adoption/Potential step
        //return nextPage('ComputeSegment');
    }
        catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'ProductPriority',ruleId);
            return null;
            } 
    }

    public PageReference skipStep()
    {
        /*updateRule('Compute Segment', uiLocation);
        return nextPage('ComputeSegment');*/
        try{
        updateRule('Summary', uiLocation);
        return nextPage('SummaryPage');
        }
        catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'ProductPriority',ruleId);
            return null;
            } 
        
    }

    public void Openpopup(){
        SnTDMLSecurityUtil.printDebugMessage('============INSIDE OPEN POPUP FUNCTION');
        SnTDMLSecurityUtil.printDebugMessage('=========Select Matrix is::'+selectedMatrix);
        //list<Step__c> Step = [Select id,Name,Measure_Master__r.Name from Step__c where Matrix__c=:selectedMatrix];
        list<Step__c> Step ;
        try{
            Step = [Select id,Name,Measure_Master__r.Name from Step__c where Matrix__c=:selectedMatrix WITH SECURITY_ENFORCED];
        }
        catch(Exception qe) 
        {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }
        // if(!step.isEmpty()){
        Measure_Master__c MM = [SELECT Id, Name FROM Measure_Master__c WHERE id =:ruleObject.Id  WITH SECURITY_ENFORCED];
        Grid_Master__c GM = [select id,name,Brand__c,Col__c,Country__c,Description__c,Dimension_1_Name__c,Dimension_2_Name__c,
                                DM1_Output_Type__c,DM2_Output_Type__c,Grid_Type__c,Output_Name__c,Output_Type__c,Row__c from Grid_Master__c 
                                where id =:selectedMatrix  WITH SECURITY_ENFORCED limit 1];
        Grid_Master__c CloneGM = GM.Clone();
        String name = GM.Name;

        String nameMatrix = GM.name;
        String tempName = nameMatrix;
        Integer i = 1;
        //Added Where Clause foe Security Review
        //List<Grid_Master__c> allGridMasters = [select Name from Grid_Master__c ];
        List<Grid_Master__c> allGridMasters = [select Name from Grid_Master__c where Country__c =: countryID WITH SECURITY_ENFORCED];
        SnTDMLSecurityUtil.printDebugMessage('allGridMasters-->'+allGridMasters);
        List<String> allgms = new List<String>();
        SnTDMLSecurityUtil.printDebugMessage('tempName-->'+tempName);
        for(Grid_Master__c g : allGridMasters){
            allgms.add(g.Name);
        }
        
        Boolean flag = true;
        while(flag)
        {
            SnTDMLSecurityUtil.printDebugMessage('+++++++++++ Hey Matrix is '+ tempName);
            if(allgms.contains(tempName))
            {
                tempName = nameMatrix + '_' + String.valueof(i);
                i++;
            }
            else
            {
                allgms.add(tempName);
                flag = false;
            }
        }
        SnTDMLSecurityUtil.printDebugMessage('tempName now-->'+tempName);
        CloneGM.Name = tempName;
        CloneGM.Country__c = GM.Country__c;
        CloneGM.CurrencyIsoCode = 'EUR';
        SnTDMLSecurityUtil.printDebugMessage('====CloneGM===:'+CloneGM);
        try
        {
            //insert CloneGM;
            SnTDMLSecurityUtil.insertRecords(CloneGM, 'ProductPriority');
            SnTDMLSecurityUtil.printDebugMessage('CloneGM.id-->'+CloneGM.id);
            SnTDMLSecurityUtil.printDebugMessage('CloneGM.id-->'+CloneGM.Name);
            matrixList.add(new SelectOption(CloneGM.Id, CloneGM.Name));
            list<Grid_Details__c> GD = new list<Grid_Details__c>();
            list<Grid_Details__c> CloneGD = new list<Grid_Details__c>();

            GD = [select id,Name,Grid_Master__c,colvalue__c,Dimension_1_Value__c,Dimension_2_Value__c,Output_Value__c,Rowvalue__c from Grid_Details__c 
                    where Grid_Master__c =:selectedMatrix WITH SECURITY_ENFORCED];
            for(Grid_Details__c g : GD){
                Grid_Details__c newGD = g.clone();
                newGD.Grid_Master__c = CloneGM.id;
                //newGD.CurrencyIsoCode = 'EUR';
                newGD.Name = g.name;
                CloneGD.add(newGD);
            }
            //insert CloneGD;
            SnTDMLSecurityUtil.insertRecords(CloneGD, 'ProductPriority');
            SnTDMLSecurityUtil.printDebugMessage('========CloneGD====:'+CloneGD);
            selectedMatrix= CloneGM.id;
        }
        catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Matrix Name Should be Unique'));
        }
       // }
    }
}