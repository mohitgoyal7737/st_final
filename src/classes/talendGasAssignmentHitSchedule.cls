global with sharing class talendGasAssignmentHitSchedule implements Schedulable, Database.AllowsCallouts {
    
    string endPoint;
    String sfdcPath;
    String axtriaUsername;
    String axtriaPassword;
    String veevaUsername;
    String veevaPassword;
    
        
    @future(callout=true)
    public static void ETLWebServiceCallOutForGasAssignment(String Endpointss, String sfdc, string axtriaUser, string veevaUser,String axtriaPass, String veevaPass)
    {
        system.debug('#### Hit Talend job for talendGasAssignment called ####');
        string TalendEndpoint=Endpointss;
        
        try{
            TalendEndpoint += '&arg0=--context_param%20AZ_ARSNT_EUFULL_SIQ_Username='+axtriaUser;
            TalendEndpoint += '&arg1=--context_param%20AZ_ARSNT_EUFULL_SIQ_Password='+axtriaPass;
            TalendEndpoint += '&arg2=--context_param%20AZ_Veeva_Username='+veevaUser;
            TalendEndpoint += '&arg3=--context_param%20AZ_Veeva_Password='+veevaPass;
            TalendEndpoint += '&arg4=--context_param%20AZ_ARSNT_EUFULL_SIQ_URL='+sfdc;
            TalendEndpoint += '&arg5=--context_param%20AZ_Veeva_URL='+sfdc;
            
            system.debug('#### TalendEndpoint : '+TalendEndpoint);
            
            Http h = new Http();
            HttpRequest request = new HttpRequest();
            TalendEndpoint = TalendEndpoint.replaceAll( '\\s+', '%20');
            request.setEndPoint(TalendEndpoint);
            request.setHeader('Content-type', 'application/json');
            request.setMethod('GET');
            request.setTimeout(100000);
            system.debug('request '+request);
            HttpResponse response = h.send(request);
            system.debug('#### Talend Delete Response : '+response);
        }
        catch(exception ex){
            system.debug('Error in Talend job for talendGasAssignment' +ex.getMessage());
            String subject = 'Error in Talend job for talendGasAssignment';
            String body = 'Error while execution Talend Job for talendGasAssignment:::::  ' +ex.getMessage();
            String[] address = new String[]{'Ayushi.Jain@Axtria.com'};
            //String []ccAdd=new String[]{'AZ_ROW_SalesIQ_MktDeployment@Axtria.com'};
            Messaging.SingleEmailMessage emailwithattch = new Messaging.SingleEmailMessage();
            emailwithattch.setSubject(subject);
            emailwithattch.setToaddresses(address);
            emailwithattch.setPlainTextBody(body);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {emailwithattch});
        }

    }
    
    global void execute(SchedulableContext sc) {
        endPoint='';
        sfdcPath='';
        axtriaUsername='';
        axtriaPassword='';
        veevaUsername='';
        veevaPassword='';   
        List<AxtriaSalesIQTM__ETL_Config__c> etlConfig = [Select Id, Name, AxtriaSalesIQTM__Server_Type__c, AxtriaSalesIQTM__End_Point__c,AxtriaSalesIQTM__SFTP_Username__c,AxtriaSalesIQTM__SF_UserName__c,AxtriaSalesIQTM__SF_Password__c,AxtriaSalesIQTM__SFTP_Password__c from AxtriaSalesIQTM__ETL_Config__c where Name = 'Gas Assignment'];
        endPoint=etlConfig[0].AxtriaSalesIQTM__End_Point__c;
        sfdcPath=etlConfig[0].AxtriaSalesIQTM__Server_Type__c;
        axtriaUsername=etlConfig[0].AxtriaSalesIQTM__SF_UserName__c;
        axtriaPassword=etlConfig[0].AxtriaSalesIQTM__SF_Password__c;
        veevaUsername=etlConfig[0].AxtriaSalesIQTM__SFTP_Username__c;
        veevaPassword=etlConfig[0].AxtriaSalesIQTM__SFTP_Password__c;
        ETLWebServiceCallOutForGasAssignment(endPoint,sfdcPath,axtriaUsername,veevaUsername,axtriaPassword,veevaPassword);
    }

}