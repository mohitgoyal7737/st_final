/**********************************************************************************************
@author       : SnT Team
@modifiedBy   : Himanshu Tariyal (A0994)
@modifiedDate : 3rd June'2020
@description  : Batch class for copying Call Plan data from PACP to Temp Object 
@Revison(s)   : v1.0
**********************************************************************************************/
global with sharing class BatchCopyCyclePlan implements Database.Batchable<sObject>,Database.Stateful
{
    public String query;
    public String sourceTeamInst;
    public String destTeamInst;
    public String cols;
    public String teamInstanceName;
    public String batchName;
    public String alignNmsp;
    public String callPlanLoggerID;

    public Boolean proceedNextBatch = true;

    List<temp_Obj__c> stagingList = new List<temp_Obj__c>();
    List<AxtriaSalesIQTM__Team_Instance__c> teamInst;
    List<Product_Catalog__c> prod;
    
    Set<String> prodName;

    global BatchCopyCyclePlan(String sourceTeamInstance,String destTeamInstance,String colsString,String loggerID) 
    {
        batchName = 'BatchCopyCyclePlan';
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : constructor invoked-->');

        sourceTeamInst = sourceTeamInstance;
        destTeamInst = destTeamInstance;
        callPlanLoggerID = loggerID;

        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ScenarioAlignmentCtlr'];
        alignNmsp = cs.NamespacePrefix!=null && cs.NamespacePrefix!='' ? cs.NamespacePrefix+'__' : '';

        SnTDMLSecurityUtil.printDebugMessage('alignNmsp-->'+alignNmsp);
        SnTDMLSecurityUtil.printDebugMessage('callPlanLoggerID-->'+callPlanLoggerID);
        SnTDMLSecurityUtil.printDebugMessage('sourceTeamInst-->'+sourceTeamInst);
        SnTDMLSecurityUtil.printDebugMessage('destTeamInst-->'+destTeamInst);

        teamInst = [Select Id,Name from AxtriaSalesIQTM__Team_Instance__c where id =: destTeamInst];

        if(colsString!=null && colsString!=''){
            cols = colsString;
        }
        else
        {
            cols = 'Parent_Account__c,Party_ID__c,CurrencyIsoCode,Previous_Calls__c,Previous_Segment__c,P1__c,Adoption__c,'+
                    'Potential__c,Segment_Approved__c,Final_TCF_Approved__c,Segment1__c,Segment2__c,'+
                    'Segment3__c,Segment4__c,Segment5__c,Segment6__c,Segment7__c,Segment8__c,Segment9__c,Segment10__c,'+
                    'AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position_Team_Instance__c,AxtriaSalesIQTM__Account__c,'+
                    'AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__lastApprovedTarget__c,'+
                    'AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,'+
                    'AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Segment10__c,'+
                    'AxtriaSalesIQTM__Segment1_Approved__c,AxtriaSalesIQTM__Segment2_Approved__c,'+
                    'AxtriaSalesIQTM__Segment3_Approved__c,AxtriaSalesIQTM__Segment4_Approved__c,'+
                    'AxtriaSalesIQTM__Segment5_Approved__c,AxtriaSalesIQTM__CheckBox5_Approved__c,'+
                    'AxtriaSalesIQTM__CheckBox6_Approved__c,AxtriaSalesIQTM__CheckBox7_Approved__c,'+
                    'AxtriaSalesIQTM__CheckBox8_Approved__c,AxtriaSalesIQTM__CheckBox9_Approved__c,'+
                    'AxtriaSalesIQTM__CheckBox10_Approved__c,AxtriaSalesIQTM__Metric6_Approved__c,'+
                    'AxtriaSalesIQTM__Metric7_Approved__c,AxtriaSalesIQTM__Metric8_Approved__c,'+
                    'AxtriaSalesIQTM__Metric9_Approved__c,AxtriaSalesIQTM__Metric10_Approved__c';
        }
        SnTDMLSecurityUtil.printDebugMessage('cols-->'+cols);

        query = 'select '+cols+' FROM AxtriaSalesIQTM__Position_Account_Call_Plan__c where '+
                'AxtriaSalesIQTM__Team_Instance__c = :sourceTeamInst and AxtriaSalesIQTM__lastApprovedTarget__c=true';
    }

    global BatchCopyCyclePlan(String sourceTeamInstance,String destTeamInstance,String colsString) 
    {
        batchName = 'BatchCopyCyclePlan';
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : constructor invoked-->');

        sourceTeamInst = sourceTeamInstance;
        destTeamInst = destTeamInstance;
        SnTDMLSecurityUtil.printDebugMessage('sourceTeamInst-->'+sourceTeamInst);
        SnTDMLSecurityUtil.printDebugMessage('destTeamInst-->'+destTeamInst);

        teamInst = [Select Id,Name from AxtriaSalesIQTM__Team_Instance__c where id =: destTeamInst];

        if(colsString!=null && colsString!=''){
            cols = colsString;
        }
        else
        {
            cols = 'Parent_Account__c,Party_ID__c,CurrencyIsoCode,Previous_Calls__c,Previous_Segment__c,P1__c,Adoption__c,'+
                    'Potential__c,Segment_Approved__c,Final_TCF_Approved__c,Segment1__c,Segment2__c,'+
                    'Segment3__c,Segment4__c,Segment5__c,Segment6__c,Segment7__c,Segment8__c,Segment9__c,Segment10__c,'+
                    'AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position_Team_Instance__c,AxtriaSalesIQTM__Account__c,'+
                    'AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__lastApprovedTarget__c,'+
                    'AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,'+
                    'AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Segment10__c,'+
                    'AxtriaSalesIQTM__Segment1_Approved__c,AxtriaSalesIQTM__Segment2_Approved__c,'+
                    'AxtriaSalesIQTM__Segment3_Approved__c,AxtriaSalesIQTM__Segment4_Approved__c,'+
                    'AxtriaSalesIQTM__Segment5_Approved__c,AxtriaSalesIQTM__CheckBox5_Approved__c,'+
                    'AxtriaSalesIQTM__CheckBox6_Approved__c,AxtriaSalesIQTM__CheckBox7_Approved__c,'+
                    'AxtriaSalesIQTM__CheckBox8_Approved__c,AxtriaSalesIQTM__CheckBox9_Approved__c,'+
                    'AxtriaSalesIQTM__CheckBox10_Approved__c,AxtriaSalesIQTM__Metric6_Approved__c,'+
                    'AxtriaSalesIQTM__Metric7_Approved__c,AxtriaSalesIQTM__Metric8_Approved__c,'+
                    'AxtriaSalesIQTM__Metric9_Approved__c,AxtriaSalesIQTM__Metric10_Approved__c';
        }
        SnTDMLSecurityUtil.printDebugMessage('cols-->'+cols);

        query = 'select '+cols+' FROM AxtriaSalesIQTM__Position_Account_Call_Plan__c where '+
                'AxtriaSalesIQTM__Team_Instance__c = :sourceTeamInst and AxtriaSalesIQTM__lastApprovedTarget__c=true';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : start() invoked--');
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : query--'+query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scope) 
    {
        try
        {
            SnTDMLSecurityUtil.printDebugMessage(batchName+' : execute() invoked--');

            stagingList = new List<temp_Obj__c>();
            //destinationTeamInstance=  destTeamInstance;
            prodName = new Set<String>();
            for(AxtriaSalesIQTM__Position_Account_Call_Plan__c product : scope){
                prodName.add(product.P1__c);
            }
            SnTDMLSecurityUtil.printDebugMessage('prodName--'+prodName);
            SnTDMLSecurityUtil.printDebugMessage('prodName size--'+prodName.size());

            prod = [Select Name, Veeva_External_ID__c, Product_Code__c,Id,Team_Instance__c FROM Product_Catalog__c 
                        where Name in : prodName and Team_Instance__c =: sourceTeamInst and IsActive__c=true];
            SnTDMLSecurityUtil.printDebugMessage('prod--'+prod);
            SnTDMLSecurityUtil.printDebugMessage('prod size--'+prod.size());            

            Map<String,String> allProdMap = new Map<String,String>();

            for(Product_Catalog__c pc : prod){
                allProdMap.put(pc.Name.toUpperCase(), pc.Veeva_External_ID__c);
            }

            teamInstanceName = teamInst[0].Name;
            SnTDMLSecurityUtil.printDebugMessage('allProdMap--'+ allProdMap);
            SnTDMLSecurityUtil.printDebugMessage('teamInstanceName--'+teamInstanceName);
            for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp: scope) 
            { 
                temp_Obj__c cp = new temp_Obj__c();
                cp.Parent_Account__c = pacp.Parent_Account__c;
                cp.Account__c = pacp.AxtriaSalesIQTM__Account__c;
                cp.Position__c = pacp.AxtriaSalesIQTM__Position__c;
                cp.Position_Team_Instance__c = pacp.AxtriaSalesIQTM__Position_Team_Instance__c;
                cp.Team_Instance__c = pacp.AxtriaSalesIQTM__Team_Instance__c;
                cp.Position_Account__c = pacp.Party_ID__c;

                //cp.Target__c = true;
                cp.Target__c = pacp.AxtriaSalesIQTM__lastApprovedTarget__c;
                cp.CurrencyIsoCode = pacp.CurrencyIsoCode;
                cp.Previous_Cycle_Calls__c = pacp.Previous_Calls__c;
                cp.Previous_Cycle_Segment__c = pacp.Previous_Segment__c;
                cp.Product_Name__c = pacp.P1__c;
                cp.Adoption__c = pacp.Adoption__c;
                cp.Potential__c = pacp.Potential__c;

                cp.AccountNumber__c = pacp.AxtriaSalesIQTM__Account__r.AccountNumber;
                cp.Territory_ID__c =pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
                cp.Team_ID__c= teamInst[0].name;
                cp.Team_Instance_Name__c = teamInst[0].name;
                cp.Cycle__c = teamInst[0].name;
                cp.Object__c = 'Call_Plan__c';

                if(pacp.Segment_Approved__c == null){
                    pacp.Segment_Approved__c= 'Non Target';
                }
                cp.Segment__c = pacp.Segment_Approved__c;

                if(pacp.Final_TCF_Approved__c == null ){
                    pacp.Final_TCF_Approved__c = 0;
                }
                cp.Previous_Cycle_Calls__c = pacp.AxtriaSalesIQTM__Segment10__c;
                cp.Objective__c = String.valueof(pacp.Final_TCF_Approved__c);
                cp.Objective_1__c = String.valueOf(pacp.Final_TCF_Approved__c);

                SnTDMLSecurityUtil.printDebugMessage('+++++ P1__c '+ pacp.P1__c);
                cp.Product_Code__c = allProdMap.get(pacp.P1__c.toUpperCase());

                cp.Segment1__c = pacp.AxtriaSalesIQTM__Segment1_Approved__c;
                cp.Segment2__c = pacp.AxtriaSalesIQTM__Segment2_Approved__c;
                cp.Segment3__c = pacp.AxtriaSalesIQTM__Segment3_Approved__c;
                cp.Segment4__c = pacp.AxtriaSalesIQTM__Segment4_Approved__c;
                cp.Segment5__c = pacp.AxtriaSalesIQTM__Segment5_Approved__c;

                cp.Segment_1__c = pacp.Segment1__c;
                cp.Segment_2__c = pacp.Segment2__c;
                cp.Segment_3__c = pacp.Segment3__c;
                cp.Segment_4__c = pacp.Segment4__c;
                cp.Segment_5__c = pacp.Segment5__c;
                cp.Segment_6__c = pacp.Segment6__c;
                cp.Segment_7__c = pacp.Segment7__c;
                cp.Segment_8__c = pacp.Segment8__c;
                cp.Segment_9__c = pacp.Segment9__c;
                cp.Segment_10__c = pacp.Segment10__c;

                cp.Checkbox5__c = pacp.AxtriaSalesIQTM__CheckBox5_Approved__c;
                cp.Checkbox6__c = pacp.AxtriaSalesIQTM__CheckBox6_Approved__c;
                cp.Checkbox7__c = pacp.AxtriaSalesIQTM__CheckBox7_Approved__c;
                cp.Checkbox8__c = pacp.AxtriaSalesIQTM__CheckBox8_Approved__c;
                cp.Checkbox9__c = pacp.AxtriaSalesIQTM__CheckBox9_Approved__c;
                cp.Checkbox10__c = pacp.AxtriaSalesIQTM__CheckBox10_Approved__c;
				cp.Metric1__c = pacp.AxtriaSalesIQTM__Metric1_Approved__c;
                cp.Metric2__c = pacp.AxtriaSalesIQTM__Metric2_Approved__c;
                cp.Metric3__c = pacp.AxtriaSalesIQTM__Metric3_Approved__c;
                cp.Metric4__c = pacp.AxtriaSalesIQTM__Metric4_Approved__c;
                cp.Metric5__c = pacp.AxtriaSalesIQTM__Metric5_Approved__c;
                cp.Metric6__c = pacp.AxtriaSalesIQTM__Metric6_Approved__c;
                cp.Metric7__c = pacp.AxtriaSalesIQTM__Metric7_Approved__c;
                cp.Metric8__c = pacp.AxtriaSalesIQTM__Metric8_Approved__c;
                cp.Metric9__c = pacp.AxtriaSalesIQTM__Metric9_Approved__c;
                cp.Metric10__c = pacp.AxtriaSalesIQTM__Metric10_Approved__c;

                stagingList.add(cp);
            } 
            
            SnTDMLSecurityUtil.printDebugMessage('++++stagingList+++'+stagingList);

            if(!stagingList.isEmpty()){
               SnTDMLSecurityUtil.insertRecords(stagingList,batchName);
            }

        }
        catch(Exception e)
        {
            SnTDMLSecurityUtil.printDebugMessage('Error in '+batchName+' : execute()-->'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack Trace-->'+e.getStackTraceString());
            proceedNextBatch = false;

            SnTDMLSecurityUtil.printDebugMessage('callPlanLoggerID--'+callPlanLoggerID);

            if(callPlanLoggerID!=null && callPlanLoggerID!='')
            {
                SalesIQUtility.updateCallPlanLoggerRecord(callPlanLoggerID,'Failed at copying Cycle Plan data','Error',alignNmsp);
            }
        }
        
    }

    global void finish(Database.BatchableContext BC) 
    {
        try
        {
            SnTDMLSecurityUtil.printDebugMessage(batchName+' : finish() invoked--');

            //Changes made by HT(A0994) on 12th June 2020
            /*if(!test.isRunningTest()){
               PACP_ErrorFilter_Utility pacUtil = new PACP_ErrorFilter_Utility(teamInstanceName);
               Database.executeBatch(pacUtil,2000);
            }*/

            if(!Test.isRunningTest())
            {
                if(callPlanLoggerID!=null && callPlanLoggerID!='')
                {
                    if(proceedNextBatch)
                    {
                        PACP_ErrorFilter_Utility pacUtil = new PACP_ErrorFilter_Utility(teamInstanceName,callPlanLoggerID,alignNmsp);
                        Database.executeBatch(pacUtil,2000);
                    }
                }
                else
                {
                    PACP_ErrorFilter_Utility pacUtil = new PACP_ErrorFilter_Utility(teamInstanceName);
                    Database.executeBatch(pacUtil,2000);
                }
            }
        }
        catch(Exception e)
        {
            SnTDMLSecurityUtil.printDebugMessage('Error in '+batchName+' : finish()-->'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack Trace-->'+e.getStackTraceString());
            SnTDMLSecurityUtil.printDebugMessage('callPlanLoggerID--'+callPlanLoggerID);

            if(callPlanLoggerID!=null && callPlanLoggerID!=''){
                SalesIQUtility.updateCallPlanLoggerRecord(callPlanLoggerID,'Failed at copying Cycle Plan data','Error',alignNmsp);
            }
        }
    }
}