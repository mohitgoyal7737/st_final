public with sharing class RuleStepFactory {
    public Step getRuleStep(Step__c step){
        String type = step.Step_Type__c;
        Step currentStep;
        if(type == 'Cases'){
            currentStep = new StepCases(step);
        }else if(type == 'Matrix'){
            currentStep = new StepMatrix(step);
        }else if(type == 'Compute'){
            currentStep = new StepCompute(step);
        }else{
            currentStep = null;
        }
        return currentStep;
    }
}