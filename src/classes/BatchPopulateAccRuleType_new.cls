global with sharing class BatchPopulateAccRuleType_new implements Database.Batchable<sObject>, Database.Stateful {
    public String query;
  
    global BatchPopulateAccRuleType_new(Boolean flagg, String country) {
        }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<sObject> scope) {
    
    }

    global void finish(Database.BatchableContext BC) {

    }

}