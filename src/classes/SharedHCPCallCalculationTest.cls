@isTest
public class SharedHCPCallCalculationTest {
    
    static testmethod void sharedplan(){
        
        list<String> pacprecid = new list<string>();
        
        AxtriaSalesIQTM__Workspace__c work = new AxtriaSalesIQTM__Workspace__c();
        work.name = 'khj';        
        work.AxtriaSalesIQTM__Workspace_Start_Date__c = system.Today();
        work.AxtriaSalesIQTM__Workspace_Description__c = 'hjghj';
        work.AxtriaSalesIQTM__Workspace_End_Date__c = system.Today()+1;
        insert work;
        
        AxtriaSalesIQTM__Scenario__c sce = new AxtriaSalesIQTM__Scenario__c();
        sce.AxtriaSalesIQTM__Workspace__c = work.id;
        
        insert sce;
        
        
        Account acc = new Account();
        acc.Name = 'Chelsea Parson';
        acc.AccountNumber = 'adf23';
        acc.AxtriaSalesIQTM__External_Account_Number__c='123';
        acc.BillingStreet='abc';
        acc.Marketing_Code__c = 'thgh';
        insert acc;
        system.debug(acc + '--------Acc---');
        
        /* Cycle__c cycle = new Cycle__c();
cycle.Name = 'SP 2081SC1';
cycle.CurrencyIsoCode = 'EUR';
insert cycle;
system.debug(cycle + '--------cycle---');*/

AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
team.Name = 'xyz';              
team.CurrencyIsoCode = 'USD';
insert team;

system.debug(team + '--------team---');

AxtriaSalesIQTM__Team_Instance__c teamIns = new AxtriaSalesIQTM__Team_Instance__c();
        //teamIns.Cycle__c = cycle.id; 
teamIns.AxtriaSalesIQTM__Team__c = team.id;
insert teamIns;        
system.debug(teamIns + '--------teamIns---');

AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();
pos.Name = 'abc';
pos.CurrencyIsoCode ='USD';
pos.AxtriaSalesIQTM__Team_iD__c = team.id;
insert pos;
AxtriaSalesIQTM__Position__c pos1 = new AxtriaSalesIQTM__Position__c();
pos1.Name = 'abcd';
pos1.CurrencyIsoCode = 'USD';
pos1.AxtriaSalesIQTM__Team_iD__c = team.id;
insert pos1;
AxtriaSalesIQTM__Position__c pos2 = new AxtriaSalesIQTM__Position__c();
pos2.Name = 'abce';
pos2.CurrencyIsoCode = 'USD';
pos2.AxtriaSalesIQTM__Team_iD__c = team.id;
insert pos2;

AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp = new AxtriaSalesIQTM__Position_Account_Call_Plan__c();

pacp.Name = 'Test1';
pacp.AxtriaSalesIQTM__Account__c = acc.id;
pacp.AxtriaSalesIQTM__Position__c = pos.id;
pacp.AxtriaSalesIQTM__Team_Instance__c = teamIns.id;
pacp.Final_TCF__c=5;
pacp.Share__c = true;
insert pacp;
pacprecid.add(pacp.id);

AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp1 = new AxtriaSalesIQTM__Position_Account_Call_Plan__c();
pacp1.Name = 'Test2';
pacp1.AxtriaSalesIQTM__Account__c = acc.id;
pacp1.AxtriaSalesIQTM__Position__c = pos.id;
pacp1.AxtriaSalesIQTM__Team_Instance__c = teamIns.id;
pacp1.Share__c = true;
pacp1.Final_TCF__c=5;
insert pacp1;
AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp2 = new AxtriaSalesIQTM__Position_Account_Call_Plan__c();
pacp2.Name = 'Test3';
pacp2.AxtriaSalesIQTM__Account__c = acc.id;
pacp2.AxtriaSalesIQTM__Position__c = pos.id;
pacp2.AxtriaSalesIQTM__Team_Instance__c = teamIns.id;
pacp2.Share__c = true;
pacp2.Final_TCF__c=5;
insert pacp2;
AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp3 = new AxtriaSalesIQTM__Position_Account_Call_Plan__c();
pacp3.Name = 'Test3';
pacp3.AxtriaSalesIQTM__Account__c = acc.id;
pacp3.AxtriaSalesIQTM__Position__c = pos.id;
pacp3.AxtriaSalesIQTM__Team_Instance__c = teamIns.id;
pacp3.Share__c = false;
pacp3.Final_TCF__c=5;
insert pacp3;

Test.startTest();
ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
SharedHCPCallCalculation sh = new SharedHCPCallCalculation();
SharedHCPCallCalculation.calcualtesum(pacprecid);
Test.stopTest();

}
}