public with sharing class Updatepacpsequenceexisting {
     List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacpexisting1 = new List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
     public void Updatepacpsequenceexisting(List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacpList){
       ErrorMessageCallCapacity__c customcount = new ErrorMessageCallCapacity__c();
        ErrorMessageCallCapacity__c customcount1 = new ErrorMessageCallCapacity__c();
        customcount = ErrorMessageCallCapacity__c.getValues('Sequence');
         integer count1;
	boolean insertcheck = false;
        integer counter = 0;
        if(customcount == null)
        {
            pacpexisting1 = [Select id, Sequence__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where Sequence__c!=null ORDER BY Sequence__c DESC LIMIT 1];
            
        }
        else
        {
            count1 = Integer.valueOf(customcount.ErrorMessage__c);
        }  
         for(integer i = 0;i<pacpList.size();i++)
            {
                if(pacpList[i].Sequence__c == null)
                {
                    counter++;
                    if(customcount!= null)
                    {
                        pacpList[i].Sequence__c = count1 + counter;
                        customcount.ErrorMessage__c = String.valueOf(pacpList[i].Sequence__c);
                    }
                    else
                    {
                        customcount1 = new ErrorMessageCallCapacity__c();
                        customcount1.Name = 'Sequence';
			insertcheck = true;
                        if(pacpexisting1.size()>0)
                        {
                            if(pacpexisting1[0].Sequence__c != null)
                            pacpList[i].Sequence__c = pacpexisting1[0].Sequence__c + counter;
                            
                            customcount1.ErrorMessage__c = String.valueOf(pacpList[i].Sequence__c);
                        }
                        else
                        {
                            pacpList[i].Sequence__c = counter;
                            customcount1.ErrorMessage__c = String.valueOf(pacpList[i].Sequence__c);
                        }
                    }
                }
            } 
            if(customcount != null)
            {
                update customcount;
            }
            else
            {
                system.debug('check custom setting'+customcount1);
		 if(insertcheck == true)
                {
                insert customcount1;
		}
            }
     }
}