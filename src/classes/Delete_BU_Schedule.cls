global with sharing class Delete_BU_Schedule implements Database.Batchable<sObject>
{
    public String query;
    Set<String> teamInstanceManualSet;
    Set<String> teamInstanceVeevaSet;
    List<String> teaminstanceManualList;
    List<String> teaminstanceVeevaList;
    public Set<string> teaminstancelist;

    global Delete_BU_Schedule()
    {

        teaminstancelist = new Set<String>();
        teaminstancelist = StaticTeaminstanceList.getCompleteRuleTeamInstances();
        this.query = 'SELECT Id FROM BU_Response__c WHERE Team_Instance__c NOT IN:teaminstancelist WITH SECURITY_ENFORCED';

    }

    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Sobject> scope)
    {
        SnTDMLSecurityUtil.printDebugMessage('scope size-->' + scope.size());
        if(scope.size() > 0 && scope != null)
        {
            if(BU_Response__c.sObjectType.getDescribe().isDeletable())
            {
                Database.DeleteResult[] srList = Database.delete(scope, false);
            }
            else
            {
                SnTDMLSecurityUtil.printDebugMessage('You dont have permission to delete BU_Response__c', 'Delete_BU_Schedule');
            }
        }
    }

    global void finish(Database.BatchableContext BC)
    {
        Survey_Data_Transformation_Utility svsd = new Survey_Data_Transformation_Utility(true);
        Database.executeBatch(svsd, 500);
    }
}