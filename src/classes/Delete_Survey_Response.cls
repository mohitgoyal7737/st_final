global class Delete_Survey_Response implements Database.Batchable<sObject>
{
    public String query;
    public String type;
    public String teamInstance;
    public String teamInstanceID;
    public Boolean scheduledJobCall=false; 
    public Set<string> teaminstancelist;
    public list<String> teamInst_ProdList; //Added by dhiren
    public String teamInstance_Prod;//Added by dhiren
    public String product;//Added by dhiren
    public boolean ondemand = false;//Added by dhiren

    global Delete_Survey_Response(String teamInstance,String type)
    {   
        this.type=type;
        this.teamInstance = teamInstance;
        scheduledJobCall=false;
        AxtriaSalesIQTM__Team_Instance__c teamIns = [select Id From AxtriaSalesIQTM__Team_Instance__c where Name=:teamInstance];
        teamInstanceID = teamIns!=null ? teamIns.id : '';    
        this.query = 'select id from Survey_Response__c where Team_Instance__c = \''+teamInstanceID+'\'';
    }
     global Delete_Survey_Response()
    {
        system.debug('++Inside whole survey response');
      scheduledJobCall=true;
      teaminstancelist= new Set<String>();
      teaminstancelist= StaticTeaminstanceList.getCompleteRuleTeamInstances();
      system.debug('+++teaminstancelist'+teaminstancelist);
      this.query= 'select id from Survey_Response__c where Team_Instance__c not in:teaminstancelist';
    }

        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Added  by dhiren<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    global Delete_Survey_Response(String teamInstanceProd)
    {
        system.debug('++Inside whole survey response');

        teamInstance_Prod = teamInstanceProd;
        System.debug('<><><><><><><><>'+teamInstance_Prod);
        teamInst_ProdList=new list<String>();
        if(teamInstance_Prod.contains(';'))
        {
            teamInst_ProdList=teamInstance_Prod.split(';');
        }
        
        system.debug('<><>teamInst_ProdList<><<>'+teamInst_ProdList);
        teamInstance = teamInst_ProdList[0];
        product = teamInst_ProdList[1];
        scheduledJobCall=false;
        ondemand = true;

        system.debug('<>>>>>>><>teamInstance<><><><><><><> '+teamInstance+'  <>>>>>>>>product>>>>>>>>>> '+product);
        query='select id from Survey_Response__c where Team_Instance__r.name =: teamInstance and Product__r.Veeva_External_ID__c =: product ';

    }
    //<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>End of code by dhiren<><><>>>>>>><><><><><><><><><><><>><><><<
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        System.debug('query-->'+query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        System.debug('scope size-->'+scope.size());
        if(scope!=null && scope.size()>0)
        {
            delete scope;
            //Database.emptyRecycleBin(scope);  
        }
    }

    global void finish(Database.BatchableContext BC)
    {
        if(scheduledJobCall)
        {
            Delete_Metadata_Definition batch = new Delete_Metadata_Definition();
            database.executebatch(batch,500);
        } 
        else
        {
            if(ondemand)
            {
                Delete_Metadata_Definition batch = new Delete_Metadata_Definition(teamInstance_Prod);
                Database.executeBatch(batch,500);
            }

            else
            {
                Delete_Metadata_Definition batch = new Delete_Metadata_Definition(teamInstance,type);
                Database.executeBatch(batch,500);
            }
        }
    }
}