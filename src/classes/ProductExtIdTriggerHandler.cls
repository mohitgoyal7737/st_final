public with sharing class ProductExtIdTriggerHandler {

    public static void productExternalId(List<Product_Catalog__c> pcatalog )
    {
      
         for(Product_Catalog__c pc :pcatalog)
          {
            if(pc.Detail_Group__c!=null && pc.Detail_Group__c!='')
            {
                
            pc.External_ID__c= pc.Team_Instance__c+'_'+pc.Detail_Group__c+'_'+pc.Veeva_External_ID__c;
            }
            
            else
            {
                pc.External_ID__c= pc.Team_Instance__c+'_null'+'_'+pc.Veeva_External_ID__c;
            }

            if(pc.Veeva_External_ID__c != pc.Product_Code__c){
              pc.addError('Product Code should be same as Veeva External ID');
            }
          }
    }
    /* ***********
    @method name:createrulereport
    @Purpose:  create the record in Rule_Report__c to identify whether the callPlan is based on SnT rule or direct load.
    @Author :   SIVA 
    @Date: 24-12-2018
    */
    //commented due to object purge activity A1422
    /*public static void createrulereport(List<Product_Catalog__c> pcatalog){
      set<string>uniqidset = new set<string>();
      list<Rule_Report__c> insertreportlist = new list<Rule_Report__c>();
      map<string,Product_Catalog__c>pcmap = new map<string,Product_Catalog__c>();
      for(Product_Catalog__c pc :pcatalog)
      {
        system.debug('=====PC:::'+pc);
        if((string)pc.Team_Instance__c !=null && (string)pc.Team_Instance__c !=''){
          system.debug('entered---------');
          string key = (string)pc.Team_Instance__c+'_'+(string)pc.Id;
          uniqidset.add(key);
          pcmap.put(key,pc);
        }
      }
      system.debug('======uniqidset:'+uniqidset);
      if(uniqidset!=null && uniqidset.size()>0)
      {

        list<Rule_Report__c>reportlist = [select id,Uniq__c from Rule_Report__c where uniq__c in :uniqidset];
        system.debug('========reportlist.size():'+reportlist.size());
        if(reportlist ==null || reportlist.size() ==0){
          system.debug('==========Creating the rule report data:::');
          for(Product_Catalog__c prod :pcatalog){
            Rule_Report__c rp = new Rule_Report__c();
            rp.Team_Instance__c = prod.Team_Instance_Name__c;
            rp.Product_Name__c = prod.name;
            rp.Country_Name__c = prod.Country_Name__c;
            rp.Type__c = '';
            rp.Uniq__c = prod.Team_Instance__c+'_'+prod.id;
            insertreportlist.add(rp);
          }
        }
        else{
            system.debug('========records already exists-----------');
            for(Rule_Report__c exrp :reportlist ){
                if(pcmap.containskey(exrp.Uniq__c)){
                  Product_Catalog__c pcrec = pcmap.get(exrp.Uniq__c);
                  exrp.Product_Name__c = pcrec.name;
                  exrp.Team_Instance__c = pcrec.Team_Instance_Name__c;
                  insertreportlist.add(exrp);
                }
            }
        }
        system.debug('========insertreportlist.size():'+insertreportlist.size());
        if(insertreportlist!=null && insertreportlist.size() >0){
            try{
                Schema.SObjectField unq = Rule_Report__c.Fields.Uniq__c;
                database.upsert(insertreportlist,unq, false);
            }
            catch(Exception ex){
              system.debug('======Excep caught in upserting the Rule_Report__c'+ex);
            }
        }

      } //end of if 

    }*///end of create rule report method
}