/**********************************************************************************************
Author      : Himanshu Tariyal (A0994)
Date        : 8th October'2020
Description : Test class for Util class
Revision(s) : v1.0
**********************************************************************************************/
@isTest
public class UtilTest 
{
    @istest static void testMethod1()
    {
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Load_Type__c = 'Full Load';
        insert countr;

        AxtriaSalesIQTM__Country__c countr2 = TestDataFactory.createCountry(orgmas);
        countr2.Load_Type__c = 'Delta';
        countr2.Name = 'New Country';
        insert countr2;

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'ONCO';
        insert team;

        AxtriaSalesIQTM__Team__c team2 = TestDataFactory.createTeam(countr2);
        team2.Name = 'ONCO2';
        team2.hasCallPlan__c = true;
        insert team2;

        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;

        AxtriaSalesIQTM__Team_Instance__c teamins1b = TestDataFactory.createTeamInstance(team2);
        insert teamins1b;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        insert scen;

        AxtriaSalesIQTM__Workspace__c workspace2 = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace2.AxtriaSalesIQTM__Country__c = countr2.id;
        insert workspace2;
        
        AxtriaSalesIQTM__Scenario__c scen2 = TestDataFactory.newcreateScenario(teamins1b, team2, workspace2);
        scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
        insert scen2;

        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        insert teamins;

        AxtriaSalesIQTM__Team_Instance__c teamins2 = TestDataFactory.createTeamInstance(team2);
        teamins2.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        teamins2.AxtriaSalesIQTM__Scenario__c = scen2.id;
        insert teamins2;

        Scheduler_Log__c sc = new Scheduler_Log__c();
        sc.Cycle__c = 'Cycle';
        sc.Job_Name__c = 'JobName';
        sc.Created_Date2__c = Datetime.now();
        insert sc;

        System.Test.startTest();
        
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        
        Util.getFullDeltaLoadCountries();
        Util.getFulloadCountries();
        Util.getFullDeltaTeamInstances();
        Util.getFulloadCountryCode();
        Util.getFullLoadTeamInstancesCallPlan();
        Util.getDeltaloadCountries();
        Util.getDeltaLoadCountryCode();
        Util.getDeltaLoadTeamInstancesCallPlan();
        Util.getJobType(sc.Job_Name__c);
        Util.getSyncTime(sc.Job_Name__c);
        Util.updateSyncTime(sc.Job_Name__c,Datetime.now());

        System.Test.stopTest();
    }
}