global with sharing class BatchPositionEmpDirectLoad_new implements Database.Batchable<sObject>, Database.Stateful
{
    /*Replaced Temp_Position_Employee__c with Temp_Obj__c due to object purge */
    global string query = '';
    global string teamID = '';
    global string teamInstanceG = '';
    global string teamG;
    public string Ids;
    Boolean flag = true;
    public integer recordsProcessed;
    public Integer recordsCreated;
    global string country;
    global list<AxtriaSalesIQTM__Team_Instance__c> teminslst = new list<AxtriaSalesIQTM__Team_Instance__c>();
    global list<AxtriaSalesIQTM__Team_Instance__c> teamins = new list<AxtriaSalesIQTM__Team_Instance__c>();
    global list<AxtriaSalesIQTM__Position_Team_Instance__c> posTI = new list<AxtriaSalesIQTM__Position_Team_Instance__c>();
    global list<AxtriaSalesIQTM__Team_Instance__c> countrylst = new list<AxtriaSalesIQTM__Team_Instance__c>();

    global set<AxtriaSalesIQTM__Position_employee__c> setPEInsert = new set<AxtriaSalesIQTM__Position_employee__c>();
    global set<AxtriaSalesIQTM__Position_employee__c> setPEUpdate = new set<AxtriaSalesIQTM__Position_employee__c>();
    global list<AxtriaSalesIQTM__Position_employee__c> lsPEInsert = new list<AxtriaSalesIQTM__Position_employee__c>();
    global list<AxtriaSalesIQTM__Position_employee__c> lsPEUpdate = new list<AxtriaSalesIQTM__Position_employee__c>();
    global list<temp_Obj__c> lsPEToBeUpdated = new list<temp_Obj__c>();
    global list<GroupMember> lsInsertGrpMember = new list<GroupMember>();
    global Set<String> primaryPRID = new Set<String>();
    global Set<String> primaryPosition = new Set<String>();
    global Set<String> uniquePosEmpKey = new Set<String>();
    public List<AxtriaSalesIQTM__Change_Request__c> cr = new List<AxtriaSalesIQTM__Change_Request__c>();
    global BatchPositionEmpDirectLoad_new(string team, string teamInstance)
    {
      
    }

    global BatchPositionEmpDirectLoad_new(string team, string teamInstance, String Ids)
    {  
    }



    global Database.Querylocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator(query);
    }

    global void execute (Database.BatchableContext BC, List<temp_Obj__c> posEmpList)
    {
       
    }

    global void finish(Database.BatchableContext BC)
    {
       
    }

}