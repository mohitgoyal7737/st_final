public with sharing class updateEffectiveCallsinPACP {

	public void updateEffectiveCallsinPACP(List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacpList){
		
		try{
			System.debug('inside updateEffectiveCallsinPACP ----');
			Boolean isMCCP_Enabled = false;
			Boolean MCCPEnabledCheck = false;
			String team_instance_id = '';
			AxtriaSalesIQTM__Team_Instance__c team_instance = new AxtriaSalesIQTM__Team_Instance__c();
			Veeva_Market_Specific__c MCCP_country_setting = new Veeva_Market_Specific__c();
			String delimiter = ',';
			String channel_call ='0';
			Integer weight=1, channel_call_integer = 0;
			List<String> channels = new List<String>();
			List<String> weights = new List<String>();
			if(pacpList.size() > 0){
				team_instance_id = pacpList[0].AxtriaSalesIQTM__Team_Instance__c;
			}
			try{
				team_instance = [Select ID, Name, Country_Name__c,Channel_Weights__c,Metric_Mapping__c,MultiChannel__c,MCCP_Enabled__c,AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.MCCP_Enabled__c from AxtriaSalesIQTM__Team_Instance__c where id = :team_instance_id WITH SECURITY_ENFORCED limit 1 ];
			}
			catch(System.QueryException qe) 
			{
	        	SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
	    	}
			if(team_instance!=null)
			 {
			        isMCCP_Enabled = team_instance.MultiChannel__c;
			        if(team_instance.MCCP_Enabled__c && team_instance.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.MCCP_Enabled__c)
			        MCCPEnabledCheck = true;
			 }
				
			
			if(isMCCP_Enabled){
				try{
					MCCP_country_setting = [Select Metric_Name__c,Weight__c from Veeva_Market_Specific__c where Name = :team_instance.Country_Name__c and Metric_Name__c != null  WITH SECURITY_ENFORCED limit 1];
				}
				catch(System.QueryException qe) 
				{
		        	SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
		    	}
				if(MCCP_country_setting != null)
					channels = MCCP_country_setting.Metric_Name__c.split(delimiter);
				if(MCCP_country_setting!= null && MCCP_country_setting.Weight__c!= null){
					weights = MCCP_country_setting.Weight__c.split(delimiter);				
				}
				System.debug('country level channels---'+channels);
				System.debug('country level weights---'+weights);
				if(team_instance!= null && team_instance.Metric_Mapping__c != null){
					channels =  new List<String>();
					channels = team_instance.Metric_Mapping__c.split(delimiter);
					weights =  new List<String>();
					if(team_instance.Channel_Weights__c != null){
						weights = team_instance.Channel_Weights__c.split(delimiter);
					}
				System.debug('team instance level channels---'+channels);
				System.debug('team instance level weights---'+weights);
					
				}
				if(weights.size()==0 || (weights.size()!=0 && channels.size()==weights.size())){
					System.debug('inside updation ---');
					/*for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp: pacpList){
						System.debug('pacp==='+pacp);
						pacp.Effective_Calls__c = 0;
						System.debug('pacp.AxtriaSalesIQTM__Account__c=='+pacp.AxtriaSalesIQTM__Account__c);
						for(Integer i=0; i<channels.size();i++){
							
							channel_call = String.valueOf(pacp.get(channels[i].trim()));
							channel_call_integer = convertToInteger(channel_call,0);
							System.debug('hannel_call_integer=='+channel_call_integer +'--- weight==='+weight);
							if(weights.size()>0){
								
								weight = convertToInteger(weights[i].trim(),1);
								pacp.Effective_Calls__c += channel_call_integer*weight;	
							}
							else{
								pacp.Effective_Calls__c += channel_call_integer;	
							}					
						System.debug('pacp.Effective_Calls__c ==='+pacp.Effective_Calls__c);
						}
					}*/
					Integer calls = 0;
					for(Integer x =0,y=pacpList.size();x<y;x++){
						//System.debug('pacp==='+pacp);
						pacpList[x].put('Effective_Calls__c',0);
						//System.debug('pacp.AxtriaSalesIQTM__Account__c=='+pacp.AxtriaSalesIQTM__Account__c);
						for(Integer i=0; i<channels.size();i++){
							
							channel_call = String.valueOf(pacpList[x].get(channels[i].trim()));
							channel_call_integer = convertToInteger(channel_call,0);
							System.debug('hannel_call_integer=='+channel_call_integer +'--- weight==='+weight);
							calls = Integer.valueOf(pacpList[x].get('Effective_Calls__c'));
							if(weights.size()>0){
								
								weight = convertToInteger(weights[i].trim(),1);
								//pacp.Effective_Calls__c += channel_call_integer*weight;

								pacpList[x].put('Effective_Calls__c',calls+channel_call_integer*weight);	
							}
							else{
								//pacp.Effective_Calls__c += channel_call_integer;
								pacpList[x].put('Effective_Calls__c',calls+channel_call_integer);		
							}					
						//System.debug('pacp.Effective_Calls__c ==='+pacp.Effective_Calls__c);
						}
					}	
				}
				

			}
			if(MCCPEnabledCheck == true)
			{
          system.debug('check in mccp condition');
          Veeva_Channels__mdt mccpchannels = [Select id,Channels__c,Metric_Mapping__c,Team_Instance_Name__c from Veeva_Channels__mdt where Team_Instance_Name__c =:team_instance.Name WITH SECURITY_ENFORCED limit 1];
          channels = mccpchannels.Metric_Mapping__c.split(delimiter);
          weights = team_instance.Channel_Weights__c.split(delimiter);
          system.debug('check channels'+channels);
          system.debug('check weights'+weights);
          decimal calls = 0;
          decimal channel_call1 = 0;
          for(Integer x =0,y=pacpList.size();x<y;x++){
               pacpList[x].put('Effective_Calls__c',0);
               system.debug('pacp list'+pacpList[x]);
               for(Integer i=0; i<channels.size();i++)
               {
                    channel_call = String.valueOf((pacpList[x].get(channels[i].trim())));
					if(channel_call == null)
					{
						channel_call = '0';
					}
                    //system.debug('channel_call----->'+channel_call);
                    // channel_call1 = convertToInteger(channel_call,0);
                    channel_call1 = decimal.valueOf(channel_call);
                    //system.debug('channel_call1---->'+channel_call1);
                    calls = decimal.valueOf(String.valueOf(pacpList[x].get('Effective_Calls__c')));
                    //system.debug('calls---->'+calls);
                  if(weights.size()>0){
                    
                    decimal weight1 = decimal.valueOf(weights[i].trim());
                    pacpList[x].put('Effective_Calls__c',(calls+(channel_call1/weight1)).setScale(2));  
                    //system.debug('pacp list after update'+pacpList[x]); 
                  }
                  else{
                    pacpList[x].put('Effective_Calls__c',calls+channel_call1);    
                  }          
               }
          }
          
      }
				
		}
		catch(Exception ex){
			System.debug('ex.getStackTraceString()---'+ex.getStackTraceString());
		}
		
			
	}
	public static Integer convertToInteger(String num, Integer replacement){
		try{
			return Integer.valueOf(num);
		}
		catch(Exception e){
			return replacement;
		}
	}
	
    
}