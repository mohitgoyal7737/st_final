global with sharing class BatchMergeInactiveAffAcc implements Database.Batchable<sObject>, Database.Stateful {
    public String query;
    public Set<String> allLooserSet;
    List<Account> updateAclist ; // --STIMPS-482

    global BatchMergeInactiveAffAcc(Set<String> looserSetRec) {
        query = '';
        allLooserSet = new Set<String>();
        updateAclist = new List<Account>();
        allLooserSet.addAll(looserSetRec);
        query = 'Select Id,Merge_Account_Number__c from Account where Merge_Account_Number__c in :allLooserSet';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Account> accList) {

      //  updateAclist = new List<Account>();
        Set<String> lossingaccounts = new Set<String>();
		list<AxtriaSalesIQTM__Position_Account__c>updateposacc = new list<AxtriaSalesIQTM__Position_Account__c>();
		List<String> periodList=new List<String>();
        periodList.add('Current');
        periodList.add('Future');

        Set<String> loosersSet = new Set<String>();
        for(Account acc : accList)
        {
            loosersSet.add(acc.Merge_Account_Number__c);
        }

        SnTDMLSecurityUtil.printDebugMessage('Inactive affiliation Record for Looser==================================================================================');

        String query2 = 'SELECT id, AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.Merge_Account_Number__c, AxtriaSalesIQTM__Account__r.Status__c,Account_Number__c,Parent_Account_Number__c,AxtriaSalesIQTM__Affiliation_Network__c,Country__c,IsDeleted,AxtriaSalesIQTM__Parent_Account__c,AxtriaSalesIQTM__Root_Account__c,Affiliation_Status__c,Affiliation_End_Date__c,AxtriaSalesIQTM__Active__c FROM   AxtriaSalesIQTM__Account_Affiliation__c where (AxtriaSalesIQTM__Account__r.Merge_Account_Number__c IN:loosersSet or AxtriaSalesIQTM__Parent_Account__r.Merge_Account_Number__c IN:loosersSet) and (AxtriaSalesIQTM__Active__c = true or Affiliation_Status__c = \'Active\')';

        List<AxtriaSalesIQTM__Account_Affiliation__c> losAccAffList2 =  Database.query(query2);
        //String query3 = 'select id,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c, AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account_Alignment_Type__c,AxtriaSalesIQTM__Account__r.Merge_Account_Number__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Account__r.Merge_Account_Number__c IN:allLooserSet and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c IN : periodList   and (AxtriaSalesIQTM__Assignment_Status__c = \'Active\' or AxtriaSalesIQTM__Assignment_Status__c = \'Future Active\') and AxtriaSalesIQTM__Account_Alignment_Type__c!=null and AxtriaSalesIQTM__Position__c!=null and AxtriaSalesIQTM__Account__c!=null and AxtriaSalesIQTM__Team_Instance__c != null'; // Query commented for SR-357
        String query3 = 'select id,AxtriaSalesIQTM__Assignment_Status__c ,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account_Alignment_Type__c,AxtriaSalesIQTM__Account__r.Merge_Account_Number__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Account__r.Merge_Account_Number__c IN:allLooserSet and (AxtriaSalesIQTM__Assignment_Status__c = \'Active\' or AxtriaSalesIQTM__Assignment_Status__c = \'Future Active\') and AxtriaSalesIQTM__Account_Alignment_Type__c!=null and AxtriaSalesIQTM__Position__c!=null and AxtriaSalesIQTM__Account__c!=null and AxtriaSalesIQTM__Team_Instance__c != null'; // -----Above query modified,AxtriaSalesIQTM__Assignment_Status__c field added for SR-357
        
        List<AxtriaSalesIQTM__Position_Account__c> LosPositionAccount = Database.query(query3);

        if(losAccAffList2!=null && losAccAffList2.size() >0)
        {
            for(AxtriaSalesIQTM__Account_Affiliation__c af: losAccAffList2) 
            {
                af.Affiliation_Status__c = 'Inactive';
                af.Affiliation_End_Date__c = Date.today().addDays(-1);
                af.AxtriaSalesIQTM__Active__c = false;
            }
            //update losAccAffList2;  
            SnTDMLSecurityUtil.updateRecords(losAccAffList2, 'BatchMergeInactiveAffAcc');  
        }

        if(accList != null)
        {
            for(Account acc : accList)
            {
                lossingaccounts.add(acc.Id);   
            }
        }


        if(lossingaccounts!=null &&  lossingaccounts.size()>0)
        {
          for(string sfdcid : lossingaccounts)
          {
            if(sfdcid!=null)
            {
                    Account acc = new Account(id=sfdcid);
                    SnTDMLSecurityUtil.printDebugMessage('==========DEACTIVATING ACCOUNTS::'+acc);
                    acc.Status__c = 'Inactive';
                    acc.AxtriaSalesIQTM__Active__c = 'Inactive';
                    acc.Active__c = 'Inactive';
                    updateAclist.add(acc);
                    SnTDMLSecurityUtil.printDebugMessage('acc:::::' +acc);
              }
          }
        }
         // -- SR-357 Starts--
        Set<String> futurePosAccIds = new Set<String>();
        List<AxtriaSalesIQTM__Position_Account__c> posAcctTodeleteList = new list<AxtriaSalesIQTM__Position_Account__c>();
        List<Account_Compute_Final__c> acfToDeleteList = new List<Account_Compute_Final__c>();
        List<BU_Response__c> buResToDeleteList = new List<BU_Response__c>();
        List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacpToDeleteList = new List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        //  SR-357 changes ends--
        
        if(LosPositionAccount.size() > 0)
        {
            for(AxtriaSalesIQTM__Position_Account__c p : LosPositionAccount)
            {
                //--- Below code commented by RT for SR-357 on 5th Nov,2020----
                 /*if(p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c =='Current' ){
                    p.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addDays(-1);
                  }
                  if(p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c == 'Future'){
                    p.AxtriaSalesIQTM__Effective_End_Date__c=p.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c.addDays(-1);
                 } */
                 
                 //------ Added by RT for SR-357 on 5th Nov,2020  ---
                if(p.AxtriaSalesIQTM__Assignment_Status__c =='Active' ){
                    p.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addDays(-1);
                    updateposacc.add(p);
                }
                if(p.AxtriaSalesIQTM__Assignment_Status__c == 'Future Active'){
                    futurePosAccIds.add(p.Id);
                }
                 // --- SR-357 changes ends---
            }
        }
        
        if(updateposacc.size()>0)
        {
            SnTDMLSecurityUtil.updateRecords(updateposacc, 'BatchMergeInactiveAffAcc');  
        }
         //------ Added by RT for SR-357 on 5th Nov,2020  ---
        if(!futurePosAccIds.isEmpty()){
          acfToDeleteList = [Select id from Account_Compute_Final__c where Physician__c in:futurePosAccIds];
          buResToDeleteList = [Select id from BU_Response__c where Position_Account__c in:futurePosAccIds];
          pacpToDeleteList = [Select id from AxtriaSalesIQTM__Position_Account_Call_Plan__c where Party_ID__c in:futurePosAccIds];
          posAcctTodeleteList = [Select id from AxtriaSalesIQTM__Position_Account__c where id in:futurePosAccIds];
          
          try{
            if(!buResToDeleteList.isEmpty()){
              SnTDMLSecurityUtil.deleteRecords(buResToDeleteList,'UpdateProfileParameterAccounts');
            }
            if(!acfToDeleteList.isEmpty()){
              SnTDMLSecurityUtil.deleteRecords(acfToDeleteList,'UpdateProfileParameterAccounts');
            }
            if(!pacpToDeleteList.isEmpty()){
              SnTDMLSecurityUtil.deleteRecords(pacpToDeleteList,'UpdateProfileParameterAccounts');
            }
            if(!posAcctTodeleteList.isEmpty()){
               SnTDMLSecurityUtil.deleteRecords(posAcctTodeleteList,'UpdateProfileParameterAccounts'); 
            }
          }
          catch(DMLException ex){
            System.debug('Exception in Position Account and related records deletion in BatchMergeInactiveAffAcc:: '+ex.getMessage()+'Line Number::'+ex.getLineNumber());
          }
        }
         //------ SR-357 changes ends here  ---

        if(updateAclist!=null && updateAclist.size()>0){
             //Update updateAclist;
             SnTDMLSecurityUtil.updateRecords(updateAclist, 'BatchMergeInactiveAffAcc');  
         }
        
    }

    global void finish(Database.BatchableContext BC) {
        
        // --- Added by RT on 14th Sep,2020 for STIMPS-482 to resolve async batch error and update summary(KPI) cards---
        if(!updateAclist.isEmpty()){
            Set<String> inactiveAccIdSet = new Set<String>();
            Set<String> teamInstIdSet = new Set<String>();
            List<AxtriaSalesIQTM__CIM_Config__c> cimConfigList= new List<AxtriaSalesIQTM__CIM_Config__c>();
            System.debug('In BatchMergeInactiveAffAcc finish updateAclist'+updateAclist);
            //UpdateProfileParameterAccounts.processInactiveAccountsFromBatch(updateAclist);
            for(Account acc : updateAclist){
                inactiveAccIdSet.add(acc.id);
            }
            // Fetch teaminstance ids for corresponding PACP
            for(AxtriaSalesIQTM__Position_Account_Call_Plan__c callPlan: [select id,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Account__c in:inactiveAccIdSet WITH SECURITY_ENFORCED]){
                teamInstIdSet.add(callPlan.AxtriaSalesIQTM__Team_Instance__c);
            }
            // call batch to reflect the in-active PACP corresponding to inactive Accounts in KPI cards on Call Plan
            if(!teamInstIdSet.isEmpty()){
                cimConfigList = [select id,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__CIM_Config__c where AxtriaSalesIQTM__CR_Type_Name__c = :SalesIQGlobalConstants.CR_TYPE_CALL_PLAN and AxtriaSalesIQTM__Enable__c =true and
                                     AxtriaSalesIQTM__Team_Instance__c in:teamInstIdSet WITH SECURITY_ENFORCED];
                System.debug(' RT cimConfigList>>>' + cimConfigList);
                 if(!cimConfigList.isEmpty()){
                    Database.executeBatch(new BatchCIMPositionMatrixSummaryUpdate(cimConfigList),2000);
                 }
            }   
       
        }
         // -- Ends here ---
    }
}