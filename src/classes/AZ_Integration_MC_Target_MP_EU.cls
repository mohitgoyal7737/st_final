global with sharing class AZ_Integration_MC_Target_MP_EU implements Database.Batchable<sObject> 
{
    

    List<String> allChannels; 
    String teamInstanceSelected;
    String queryString;
    List<String> allTeamInstances;
    
    List<Parent_PACP__c> pacpRecs;
    Set<String> activityLogIDSet;
    List<String> teamProdAccConcat;
    public Map<String,Decimal> teamSellValues;
    public Map<String,Decimal> teamSellValues1;
    public boolean errorBatches=true;
    
    global AZ_Integration_MC_Target_MP_EU(string teamInstanceSelectedTemp, List<String> allChannelsTemp)
    { 

       queryString = 'select ID, Sum_TCF__c, Team_Instance__r.Team_Goals__c,Team_Instance__r.Team_Selling__c,Team_Instance__c,  Team_Instance__r.multichannel__c,(select id, AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Account__r.AccountNumber , Final_TCF__c,Final_TCF_Approved__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c,P1__c,AxtriaSalesIQTM__Metric2_Approved__c,AxtriaSalesIQTM__Metric3_Approved__c,AxtriaSalesIQTM__Metric4_Approved__c, AxtriaSalesIQTM__Metric5_Approved__c,AxtriaSalesIQTM__Metric1_Approved__c,AxtriaSalesIQTM__Team_Instance__r.multichannel__c  from Call_Plan_Summary__r where AxtriaSalesIQTM__lastApprovedTarget__c = true and AxtriaSalesIQTM__Position__c !=null and P1__c != null),Position__r.Original_Country_Code__c,Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c,Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c from Parent_PACP__c where Team_Instance__c = :teamInstanceSelected';

        teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;
    }

   global AZ_Integration_MC_Target_MP_EU(List<String> teamInstanceSelectedTemp, List<String> allChannelsTemp)
    { 
        
        allTeamInstances = new List<String>(teamInstanceSelectedTemp);
        queryString = 'select ID, Sum_TCF__c, Team_Instance__c,Team_Instance__r.Team_Goals__c,Team_Instance__r.Team_Selling__c,Team_Instance__r.multichannel__c,Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c, Position__r.AxtriaSalesIQTM__Client_Position_Code__c,(select id, Total_Objective__c, AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Account__r.AccountNumber ,P1__c, Final_TCF__c,Final_TCF_Approved__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c,AxtriaSalesIQTM__Metric2_Approved__c,AxtriaSalesIQTM__Metric3_Approved__c,AxtriaSalesIQTM__Metric4_Approved__c, AxtriaSalesIQTM__Metric5_Approved__c,AxtriaSalesIQTM__Metric1_Approved__c,AxtriaSalesIQTM__Team_Instance__r.multichannel__c  from Call_Plan_Summary__r where AxtriaSalesIQTM__lastApprovedTarget__c = true and AxtriaSalesIQTM__Position__c !=null and P1__c != null),Position__r.Original_Country_Code__c,Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c,Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c from Parent_PACP__c where Team_Instance__c in :allTeamInstances';    
        //teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;
    }   


    global AZ_Integration_MC_Target_MP_EU(List<String> teamInstanceSelectedTemp, List<String> allChannelsTemp, Set<String> activityLogSet)
    { 
        
        allTeamInstances = new List<String>(teamInstanceSelectedTemp);
        queryString = 'select ID, Sum_TCF__c, Team_Instance__c,Team_Instance__r.Team_Goals__c,Team_Instance__r.Team_Selling__c,Team_Instance__r.multichannel__c,Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c, Position__r.AxtriaSalesIQTM__Client_Position_Code__c,(select id, Total_Objective__c, AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Account__r.AccountNumber ,P1__c, Final_TCF__c,Final_TCF_Approved__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c,AxtriaSalesIQTM__Metric2_Approved__c,AxtriaSalesIQTM__Metric3_Approved__c,AxtriaSalesIQTM__Metric4_Approved__c, AxtriaSalesIQTM__Metric5_Approved__c,AxtriaSalesIQTM__Metric1_Approved__c,AxtriaSalesIQTM__Team_Instance__r.multichannel__c  from Call_Plan_Summary__r where AxtriaSalesIQTM__lastApprovedTarget__c = true and AxtriaSalesIQTM__Position__c !=null and P1__c != null),Position__r.Original_Country_Code__c,Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c,Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c from Parent_PACP__c where Team_Instance__c in :allTeamInstances';    
        //teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;
        activityLogIDSet = new Set<String>();
        activityLogIDSet.addAll(activityLogSet);
    }   
    
    
    
    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        return Database.getQueryLocator(queryString);
    }
    
    public void create_MC_Cycle_Plan_Target_vod(List<Parent_PACP__c> scopePacpProRecs)
    {
        /*try
        {*/
            pacpRecs = scopePacpProRecs;
            List<SIQ_MC_Cycle_Plan_Target_vod_O__c> mcPlanTarget = new List<SIQ_MC_Cycle_Plan_Target_vod_O__c>();
            teamProdAccConcat = new List<String>();
            teamSellValues= new Map<String,Decimal>();
            teamSellValues1= new Map<String,Decimal>();

            mcPlanTarget = new List<SIQ_MC_Cycle_Plan_Target_vod_O__c>();
            for(Parent_PACP__c ppacp : scopePacpProRecs)
            {
                system.debug('++ppacp'+ppacp);
                system.debug('++++== ppacp.Call_Plan_Summary__r ' + ppacp.Call_Plan_Summary__r);
                for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacpTeam : ppacp.Call_Plan_Summary__r)
                {
                    teamProdAccConcat.add(pacpTeam.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c +'&'+pacpTeam.AxtriaSalesIQTM__Account__r.AccountNumber+'&'+pacpTeam.P1__c);
                    system.debug('++teamProdAccConcat'+teamProdAccConcat);
                }
            }  
            if(scopePacpProRecs[0].Team_Instance__r.Team_Selling__c==true) 
            {
                if(scopePacpProRecs[0].Team_Instance__r.Team_Goals__c=='Sum Team')
                {
                    teamSellValues= Team_Selling.updateSumTeamGoals(teamProdAccConcat);
                }
                else if(scopePacpProRecs[0].Team_Instance__r.Team_Goals__c=='Max Team')
                {
                    teamSellValues= Team_Selling.updateMaxTeamGoals(teamProdAccConcat);
                }
                else if(scopePacpProRecs[0].Team_Instance__r.Team_Goals__c=='Sum Team+Individual')
                {
                    teamSellValues= Team_Selling.updateSumTeamGoals(teamProdAccConcat);
                }
                else if(scopePacpProRecs[0].Team_Instance__r.Team_Goals__c=='Max Team+Individual')
                {
                    teamSellValues= Team_Selling.updateMaxTeamGoals(teamProdAccConcat);
                }
                system.debug('++teamSellValues'+teamSellValues);
            }
            else
            {
                system.debug('inside Individual');
                teamSellValues= Team_Selling.updateIndividualGoals(teamProdAccConcat);
                system.debug('++teamSellValues'+teamSellValues);
            }
            
                    
            Map<String, Map<String,Integer>> targetCallsMap = new Map<String,  Map<String,Integer>>();
            
            Set<String> uniquePos = new Set<String>();

            String selectedMarket = [select AxtriaSalesIQTM__Team__r.Country_Name__c from AxtriaSalesIQTM__Team_Instance__c where id = :allTeamInstances[0]].AxtriaSalesIQTM__Team__r.Country_Name__c;
            
            List<Veeva_Market_Specific__c> veevaCriteria = [select MCCP_Target_logic__c, Channel_Criteria__c,Market__c,MC_Cycle_Threshold_Max__c,MC_Cycle_Threshold_Min__c,MC_Cycle_Channel_Record_Type__c,MC_Cycle_Plan_Channel_Record_Type__c,MC_Cycle_Plan_Product_Record_Type__c,MC_Cycle_Plan_Record_Type__c,MC_Cycle_Plan_Target_Record_Type__c,MC_Cycle_Product_Record_Type__c,MC_Cycle_Record_Type__c,Channel_Name__c,Metric_Name__c,Weight__c from Veeva_Market_Specific__c where Market__c = :selectedMarket];
           
            Decimal maxInd;
            Decimal sumInd;
            Decimal maxTeam;
            Decimal sumTeam;
            Decimal maxSumTeam;
            Decimal sumMaxTeam;
            Decimal maxMetric1;
            Decimal maxMetric2;
            Decimal maxMetric3;
            Decimal maxMetric4;
            Decimal sumOtherChannels;
            Decimal totalObjective;//SNT-378

            Decimal value;
            for(Parent_PACP__c ppacp : scopePacpProRecs)
            {
                maxInd = 0;
                sumInd = 0;
                maxTeam = 0;
                sumTeam = 0;
                maxSumTeam=0;
                sumMaxTeam=0;
                value = 0;

                maxMetric1=0;
                maxMetric2=0;
                maxMetric3=0;
                maxMetric4=0;
                sumOtherChannels=0;
                totalObjective = 0;//SNT-378
                
                SIQ_MC_Cycle_Plan_Target_vod_O__c mcp = new SIQ_MC_Cycle_Plan_Target_vod_O__c();
                mcp.SIQ_Channel_Interactions_Goal_vod__c= 0;
                Boolean flag = false;
                Map<string,string> metricAndWeights = new Map<string,string>();
                system.debug('ppacp '+ ppacp);
                if(veevaCriteria[0].Channel_Name__c!=null)
                {
                  for(integer i=0; i < veevaCriteria[0].Channel_Name__c.split(',').size();i++)
                  {
                      if(veevaCriteria[0].Metric_Name__c!=null)
                    {
                        metricAndWeights.put(veevaCriteria[0].Metric_Name__c.split(',')[i],veevaCriteria[0].Weight__c.split(',')[i]);
                    }
                      
                    allChannels.add(veevaCriteria[0].Channel_Name__c.split(',')[i]); 
                  }
                }
                
                  system.debug('+++metricAndWeights'+metricAndWeights);
                  system.debug('+++allChannels'+allChannels);


                for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : ppacp.Call_Plan_Summary__r)
                {

                    system.debug('++++++++++ Hey Inside recs are '+ ppacp.Call_Plan_Summary__r);
                    if(pacp.Final_TCF_Approved__c == null)
                        pacp.Final_TCF_Approved__c = 0;   
                
                    for(string metric:metricAndWeights.keyset())
                    {
                        system.debug('++metric'+metric);
                        if(pacp.get(metric)!=null)
                        {
                             //decimal sum=metricAndcalls.get(metric);
                            system.debug('++metric'+metric);
                            decimal weight=Decimal.valueOf(metricAndWeights.get(metric));
                            decimal metricValue=(decimal)(pacp.get(metric));
                            sumOtherChannels= sumOtherChannels+(metricValue*weight);
                            system.debug('+++metricValue'+metricValue);
                        }
                        system.debug('+++sumOtherChannels'+sumOtherChannels);
                    }   
                        
                    flag = true;
                    String teamPos = pacp.AxtriaSalesIQTM__Team_Instance__r.Name + '_' + pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
                    //String userID = posTeamToUser.get(teamPos);
                    
                    //userID = 'U1234';
                    mcp.SIQ_Cycle_Plan_vod__c = teamPos; //:- Get from Cycle Plan vod External ID               

                    //mcp.AZ_External_Id__c  = teamPos + '_' + pacp.Account__r.AccountNumber;
                    mcp.SIQ_Target_vod__c  = pacp.AxtriaSalesIQTM__Account__r.AccountNumber;
                    
                    mcp.SIQ_Status_vod__c = 'Active_vod';
                    
                    if(maxInd < pacp.Final_TCF_Approved__c)
                    {
                        maxInd = Integer.valueOf(pacp.Final_TCF_Approved__c);
                    }
                    if(maxMetric1 < pacp.AxtriaSalesIQTM__Metric1_Approved__c)
                    {
                        maxMetric1= Integer.valueOf(pacp.AxtriaSalesIQTM__Metric1_Approved__c);
                    }
                    if(maxMetric2< pacp.AxtriaSalesIQTM__Metric2_Approved__c)
                    {
                        maxMetric2=Integer.valueOf(pacp.AxtriaSalesIQTM__Metric2_Approved__c);
                    }
                    if(maxMetric3< pacp.AxtriaSalesIQTM__Metric3_Approved__c)
                    {
                        maxMetric3=Integer.valueOf(pacp.AxtriaSalesIQTM__Metric3_Approved__c);
                    }
                    if(maxMetric4< pacp.AxtriaSalesIQTM__Metric4_Approved__c)
                    {
                        maxMetric4=Integer.valueOf(pacp.AxtriaSalesIQTM__Metric4_Approved__c);
                    }

                    system.debug('+++maxInd'+maxInd);
                    system.debug('+++maxMetric1'+maxMetric1);
                    system.debug('+++maxMetric2'+maxMetric2);



                    sumInd = sumInd + pacp.Final_TCF_Approved__c;
                    system.debug('+++sumInd'+sumInd);
                
                    mcp.SIQ_External_Id_vod__c  =   teamPos + '_' + pacp.AxtriaSalesIQTM__Account__r.AccountNumber;

                    if(teamSellValues.containsKey(pacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c+'&'+pacp.AxtriaSalesIQTM__Account__r.AccountNumber+'&'+pacp.P1__c))
                    {
                        value=teamSellValues.get(pacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c+'&'+pacp.AxtriaSalesIQTM__Account__r.AccountNumber+'&'+pacp.P1__c);
                        sumTeam=sumTeam+value;
                        sumMaxTeam=sumMaxTeam+value;
                        if(maxSumTeam<value)
                        {
                            maxSumTeam=value;
                        }  
                    }
                    system.debug('++sum'+sumTeam);
                    system.debug('++maxSumteam'+maxSumTeam);
                    //START SNT-378
                    if(pacp.Total_Objective__c!=null){
                        totalObjective = pacp.Total_Objective__c;
                    }else{
                        totalObjective = decimal.valueOf('0');
                    }//END SNT-378
                }
                if(ppacp.Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c=='No Cluster')
                {
                    mcp.CountryID__c = ppacp.Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                }
                else if(ppacp.Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c=='SalesIQ Cluster')
                {
                    mcp.CountryID__c = ppacp.Position__r.Original_Country_Code__c;
                }
                else if(ppacp.Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c=='Veeva Cluster')
                {
                    mcp.CountryID__c = ppacp.Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c;
                }   

            //mcp.CountryID__c = ppacp.Position__r.Original_Country_Code__c;
                mcp.External_ID_Axtria__c = mcp.SIQ_External_Id_vod__c;
                mcp.Rec_Status__c = 'Updated';
                mcp.RecordTypeId__c = veevaCriteria[0].MC_Cycle_Plan_Target_Record_Type__c;
                mcp.Team_Instance__c = ppacp.Team_Instance__c;
                mcp.Territory__c = ppacp.Position__r.AxtriaSalesIQTM__Client_Position_Code__c;

                if(ppacp.Team_Instance__r.Multichannel__c==true)
                {
                    system.debug('++multichannel');
                    if(ppacp.Team_Instance__r.Team_Goals__c=='Individual')
                    {
                       if(veevaCriteria[0].MCCP_Target_logic__c == 'SUM' || veevaCriteria[0].MCCP_Target_logic__c == 'Sum' || veevaCriteria[0].MCCP_Target_logic__c == 'sum')
                        {
                          mcp.SIQ_Channel_Interactions_Goal_vod__c =maxInd+maxMetric1+maxMetric2+maxMetric3+maxMetric4;
                          mcp.SIQ_Product_Interactions_Goal_vod__c = sumOtherChannels;
                          mcp.SIQ_Channel_Interactions_Max_vod__c=maxInd+maxMetric1+maxMetric2+maxMetric3+maxMetric4;
                          system.debug('++sumInd+sumOtherChannels'+sumOtherChannels);
                          system.debug('++mcp.SIQ_Channel_Interactions_Goal_vod__c'+ mcp.SIQ_Channel_Interactions_Goal_vod__c);
                        }

                       else if(veevaCriteria[0].MCCP_Target_logic__c == 'MAX' || veevaCriteria[0].MCCP_Target_logic__c == 'Max' || veevaCriteria[0].MCCP_Target_logic__c == 'max')
                        {
                            mcp.SIQ_Channel_Interactions_Goal_vod__c =maxInd+maxMetric1+maxMetric2+maxMetric3+maxMetric4;
                            mcp.SIQ_Product_Interactions_Goal_vod__c =sumOtherChannels;
                            mcp.SIQ_Channel_Interactions_Max_vod__c=maxInd+maxMetric1+maxMetric2+maxMetric3+maxMetric4;
                            system.debug('++sumInd+sumOtherChannels'+sumOtherChannels);
                            system.debug('++mcp.SIQ_Channel_Interactions_Goal_vod__c'+ mcp.SIQ_Channel_Interactions_Goal_vod__c);
                        }
                    }
                    else if(ppacp.Team_Instance__r.Team_Goals__c=='Sum Team')
                    {
                        if(veevaCriteria[0].MCCP_Target_logic__c == 'SUM' || veevaCriteria[0].MCCP_Target_logic__c == 'Sum' || veevaCriteria[0].MCCP_Target_logic__c == 'sum')
                        {
                            mcp.SIQ_Team_Channel_Interactions_Goal_vod__c=sumTeam;
                            mcp.SIQ_Team_Channel_Interactions_Max_vod__c=sumTeam;
                            mcp.SIQ_Team_Product_Interactions_Goal_vod__c=sumTeam;
                        }
                        else if(veevaCriteria[0].MCCP_Target_logic__c == 'MAX' || veevaCriteria[0].MCCP_Target_logic__c == 'Max' || veevaCriteria[0].MCCP_Target_logic__c == 'max')
                        {
                            // mcp.SIQ_Team_Channel_Interactions_Goal_vod__c=t;
                            mcp.SIQ_Team_Channel_Interactions_Max_vod__c=maxSumTeam;
                            mcp.SIQ_Team_Product_Interactions_Goal_vod__c=maxSumTeam;
                            mcp.SIQ_Team_Channel_Interactions_Goal_vod__c=maxSumTeam;
                        }
                        
                        mcp.SIQ_Channel_Interactions_Goal_vod__c =0;
                        mcp.SIQ_Product_Interactions_Goal_vod__c = 0;
                        mcp.SIQ_Channel_Interactions_Max_vod__c=0;
                    }
                    else if(ppacp.Team_Instance__r.Team_Goals__c=='Sum Team+Individual')
                    {
                        if(veevaCriteria[0].MCCP_Target_logic__c == 'SUM' || veevaCriteria[0].MCCP_Target_logic__c == 'Sum' || veevaCriteria[0].MCCP_Target_logic__c == 'sum')
                        {
                            mcp.SIQ_Team_Channel_Interactions_Goal_vod__c=sumTeam;
                            mcp.SIQ_Team_Channel_Interactions_Max_vod__c=sumTeam;
                            mcp.SIQ_Team_Product_Interactions_Goal_vod__c=sumTeam;
                        }
                        else if(veevaCriteria[0].MCCP_Target_logic__c == 'MAX' || veevaCriteria[0].MCCP_Target_logic__c == 'Max' || veevaCriteria[0].MCCP_Target_logic__c == 'max')
                        {
                            // mcp.SIQ_Team_Channel_Interactions_Goal_vod__c=t;
                            mcp.SIQ_Team_Channel_Interactions_Max_vod__c=maxSumTeam;
                            mcp.SIQ_Team_Product_Interactions_Goal_vod__c=maxSumTeam;
                            mcp.SIQ_Team_Channel_Interactions_Goal_vod__c=maxSumTeam;
                        }
                        mcp.SIQ_Channel_Interactions_Goal_vod__c =sumInd;
                        mcp.SIQ_Product_Interactions_Goal_vod__c = sumInd;
                        mcp.SIQ_Channel_Interactions_Max_vod__c=sumInd;
                    }
    
                    else if(ppacp.Team_Instance__r.Team_Goals__c=='Max Team')
                    {
                        if(veevaCriteria[0].MCCP_Target_logic__c == 'SUM' || veevaCriteria[0].MCCP_Target_logic__c == 'Sum' || veevaCriteria[0].MCCP_Target_logic__c == 'sum')
                        {
                            mcp.SIQ_Team_Channel_Interactions_Goal_vod__c=sumMaxTeam;
                            mcp.SIQ_Team_Channel_Interactions_Max_vod__c=sumMaxTeam;
                            mcp.SIQ_Team_Product_Interactions_Goal_vod__c=sumMaxTeam;
                        }

                       else if(veevaCriteria[0].MCCP_Target_logic__c == 'MAX' || veevaCriteria[0].MCCP_Target_logic__c == 'Max' || veevaCriteria[0].MCCP_Target_logic__c == 'max')
                        {
                            mcp.SIQ_Team_Channel_Interactions_Goal_vod__c=maxSumTeam;
                            mcp.SIQ_Team_Channel_Interactions_Max_vod__c=maxSumTeam;
                            mcp.SIQ_Team_Product_Interactions_Goal_vod__c=maxSumTeam;
                        }
                        
                        mcp.SIQ_Channel_Interactions_Goal_vod__c =0;
                        mcp.SIQ_Product_Interactions_Goal_vod__c = 0;
                        mcp.SIQ_Channel_Interactions_Max_vod__c=0;
                    }
                    else if(ppacp.Team_Instance__r.Team_Goals__c=='Max Team+Individual')
                    {
                        if(veevaCriteria[0].MCCP_Target_logic__c == 'SUM' || veevaCriteria[0].MCCP_Target_logic__c == 'Sum' || veevaCriteria[0].MCCP_Target_logic__c == 'sum')
                        {
                          mcp.SIQ_Team_Channel_Interactions_Goal_vod__c=sumMaxTeam;
                          mcp.SIQ_Team_Channel_Interactions_Max_vod__c=sumMaxTeam;
                          mcp.SIQ_Team_Product_Interactions_Goal_vod__c=sumMaxTeam;
                        }
                       else if(veevaCriteria[0].MCCP_Target_logic__c == 'MAX' || veevaCriteria[0].MCCP_Target_logic__c == 'Max' || veevaCriteria[0].MCCP_Target_logic__c == 'max')
                        {
                            mcp.SIQ_Team_Channel_Interactions_Goal_vod__c=maxSumTeam;
                            mcp.SIQ_Team_Channel_Interactions_Max_vod__c=maxSumTeam;
                            mcp.SIQ_Team_Product_Interactions_Goal_vod__c=maxSumTeam;
                        }
                        mcp.SIQ_Channel_Interactions_Goal_vod__c =maxInd;
                        mcp.SIQ_Product_Interactions_Goal_vod__c = maxInd;
                        mcp.SIQ_Channel_Interactions_Max_vod__c=maxInd;
                    }
                    //START SNT-378
                    else if(ppacp.Team_Instance__r.Team_Goals__c=='Total Objective' || ppacp.Team_Instance__r.Team_Goals__c=='Total Objective+Max Calls'){
                        mcp.SIQ_Product_Interactions_Goal_vod__c = 0;
                        mcp.SIQ_Channel_Interactions_Goal_vod__c  = totalObjective;
                        mcp.SIQ_Channel_Interactions_Max_vod__c =  totalObjective;
                    }
                    //END SNT-378
                } 
                else
                {
                    system.debug('+++inside not MC');
                    if(ppacp.Team_Instance__r.Team_Goals__c=='Individual')
                    {
                        system.debug('++inside pacp Individual');
                       if(veevaCriteria[0].MCCP_Target_logic__c == 'SUM' || veevaCriteria[0].MCCP_Target_logic__c == 'Sum' || veevaCriteria[0].MCCP_Target_logic__c == 'sum')
                       {
                          mcp.SIQ_Channel_Interactions_Goal_vod__c =sumInd;
                          mcp.SIQ_Product_Interactions_Goal_vod__c = sumInd;
                          mcp.SIQ_Channel_Interactions_Max_vod__c=sumInd;
                       }
                       else if(veevaCriteria[0].MCCP_Target_logic__c == 'MAX' || veevaCriteria[0].MCCP_Target_logic__c == 'Max' || veevaCriteria[0].MCCP_Target_logic__c == 'max')
                       {
                            mcp.SIQ_Channel_Interactions_Goal_vod__c =maxInd;
                            mcp.SIQ_Product_Interactions_Goal_vod__c =sumInd;
                            mcp.SIQ_Channel_Interactions_Max_vod__c=maxInd;
                       }
                    }
                    else if(ppacp.Team_Instance__r.Team_Goals__c=='Sum Team')
                    {
                        if(veevaCriteria[0].MCCP_Target_logic__c == 'SUM' || veevaCriteria[0].MCCP_Target_logic__c == 'Sum' || veevaCriteria[0].MCCP_Target_logic__c == 'sum')
                        {
                            mcp.SIQ_Team_Channel_Interactions_Goal_vod__c=sumTeam;
                            mcp.SIQ_Team_Channel_Interactions_Max_vod__c=sumTeam;
                            mcp.SIQ_Team_Product_Interactions_Goal_vod__c=sumTeam;
                        }
                        else if(veevaCriteria[0].MCCP_Target_logic__c == 'MAX' || veevaCriteria[0].MCCP_Target_logic__c == 'Max' || veevaCriteria[0].MCCP_Target_logic__c == 'max')
                        {
                            // mcp.SIQ_Team_Channel_Interactions_Goal_vod__c=t;
                            mcp.SIQ_Team_Channel_Interactions_Max_vod__c=maxSumTeam;
                            mcp.SIQ_Team_Product_Interactions_Goal_vod__c=maxSumTeam;
                            mcp.SIQ_Team_Channel_Interactions_Goal_vod__c=maxSumTeam;
                        }
                        
                        mcp.SIQ_Channel_Interactions_Goal_vod__c =0;
                        mcp.SIQ_Product_Interactions_Goal_vod__c = 0;
                        mcp.SIQ_Channel_Interactions_Max_vod__c=0;
                    }
                    else if(ppacp.Team_Instance__r.Team_Goals__c=='Sum Team+Individual')
                    {
                        if(veevaCriteria[0].MCCP_Target_logic__c == 'SUM' || veevaCriteria[0].MCCP_Target_logic__c == 'Sum' || veevaCriteria[0].MCCP_Target_logic__c == 'sum')
                        {
                            mcp.SIQ_Team_Channel_Interactions_Goal_vod__c=sumTeam;
                            mcp.SIQ_Team_Channel_Interactions_Max_vod__c=sumTeam;
                            mcp.SIQ_Team_Product_Interactions_Goal_vod__c=sumTeam;
                            mcp.SIQ_Channel_Interactions_Goal_vod__c =sumInd;
                            mcp.SIQ_Product_Interactions_Goal_vod__c = sumInd;
                            mcp.SIQ_Channel_Interactions_Max_vod__c=sumInd;
                        }
                        else if(veevaCriteria[0].MCCP_Target_logic__c == 'MAX' || veevaCriteria[0].MCCP_Target_logic__c == 'Max' || veevaCriteria[0].MCCP_Target_logic__c == 'max')
                        {
                            // mcp.SIQ_Team_Channel_Interactions_Goal_vod__c=t;
                            mcp.SIQ_Team_Channel_Interactions_Max_vod__c=maxSumTeam;
                            mcp.SIQ_Team_Product_Interactions_Goal_vod__c=sumTeam;
                            mcp.SIQ_Team_Channel_Interactions_Goal_vod__c=maxSumTeam;
                            mcp.SIQ_Channel_Interactions_Goal_vod__c =maxInd;
                            mcp.SIQ_Product_Interactions_Goal_vod__c = sumInd;
                            mcp.SIQ_Channel_Interactions_Max_vod__c=maxInd;
                        }
                        system.debug('++mcp.SIQ_Team_Product_Interactions_Goal_vod__c'+mcp.SIQ_Team_Product_Interactions_Goal_vod__c);
                        system.debug('++mcp.SIQ_Product_Interactions_Goal_vod__c'+mcp.SIQ_Product_Interactions_Goal_vod__c);
                    }
        
                    else if(ppacp.Team_Instance__r.Team_Goals__c=='Max Team')
                    {
                        if(veevaCriteria[0].MCCP_Target_logic__c == 'SUM' || veevaCriteria[0].MCCP_Target_logic__c == 'Sum' || veevaCriteria[0].MCCP_Target_logic__c == 'sum')
                       {
                            mcp.SIQ_Team_Channel_Interactions_Goal_vod__c=sumMaxTeam;
                            mcp.SIQ_Team_Channel_Interactions_Max_vod__c=sumMaxTeam;
                              mcp.SIQ_Team_Product_Interactions_Goal_vod__c=sumMaxTeam;
                       }
                       else if(veevaCriteria[0].MCCP_Target_logic__c == 'MAX' || veevaCriteria[0].MCCP_Target_logic__c == 'Max' || veevaCriteria[0].MCCP_Target_logic__c == 'max')
                       {
                            mcp.SIQ_Team_Channel_Interactions_Goal_vod__c=maxSumTeam;
                            mcp.SIQ_Team_Channel_Interactions_Max_vod__c=maxSumTeam;
                            mcp.SIQ_Team_Product_Interactions_Goal_vod__c=maxSumTeam;
                       }
                        
                        mcp.SIQ_Channel_Interactions_Goal_vod__c =0;
                        mcp.SIQ_Product_Interactions_Goal_vod__c = 0;
                        mcp.SIQ_Channel_Interactions_Max_vod__c=0;
                    }
                    else if(ppacp.Team_Instance__r.Team_Goals__c=='Max Team+Individual')
                    {
                        if(veevaCriteria[0].MCCP_Target_logic__c == 'SUM' || veevaCriteria[0].MCCP_Target_logic__c == 'Sum' || veevaCriteria[0].MCCP_Target_logic__c == 'sum')
                       {
                          mcp.SIQ_Team_Channel_Interactions_Goal_vod__c=sumMaxTeam;
                          mcp.SIQ_Team_Channel_Interactions_Max_vod__c=sumMaxTeam;
                          mcp.SIQ_Team_Product_Interactions_Goal_vod__c=sumMaxTeam;
                          mcp.SIQ_Channel_Interactions_Goal_vod__c =sumInd;
                          mcp.SIQ_Product_Interactions_Goal_vod__c = sumMaxTeam;
                          mcp.SIQ_Channel_Interactions_Max_vod__c=sumInd;
                       }
                       else if(veevaCriteria[0].MCCP_Target_logic__c == 'MAX' || veevaCriteria[0].MCCP_Target_logic__c == 'Max' || veevaCriteria[0].MCCP_Target_logic__c == 'max')
                       {
                            mcp.SIQ_Team_Channel_Interactions_Goal_vod__c=maxSumTeam;
                            mcp.SIQ_Team_Channel_Interactions_Max_vod__c=maxSumTeam;
                            mcp.SIQ_Team_Product_Interactions_Goal_vod__c=sumMaxTeam;
                            mcp.SIQ_Channel_Interactions_Goal_vod__c =maxInd;
                            mcp.SIQ_Product_Interactions_Goal_vod__c = sumMaxTeam;
                            mcp.SIQ_Channel_Interactions_Max_vod__c=maxInd;
                       }
                    }
                }  
                system.debug('+++mcp.SIQ_Team_Channel_Interactions_Goal_vod__c'+mcp.SIQ_Team_Channel_Interactions_Goal_vod__c);
                system.debug('+++mcp.SIQ_Team_Channel_Interactions_Goal_vod__c'+ mcp.SIQ_Team_Product_Interactions_Goal_vod__c);
                system.debug('+++mcp.SIQ_Channel_Interactions_Max_vod__c'+ mcp.SIQ_Channel_Interactions_Max_vod__c);
                system.debug('+++mcp.SIQ_Product_Interactions_Goal_vod__c'+ mcp.SIQ_Product_Interactions_Goal_vod__c);
                if(flag && !uniquePos.contains(mcp.External_ID_Axtria__c))
                {
                    mcPlanTarget.add(mcp); 
                    uniquePos.add(mcp.External_ID_Axtria__c);    
                }
            }
            upsert mcPlanTarget External_ID_Axtria__c;
       /* }catch(Exception e)
           {
                errorBatches=false;
                system.debug('++inside catch');
                system.debug('+++errors'+e.getMessage());
                String Header='Cycle Plan Target batch is failed, so delta has been haulted of this org for Today'+e.getMessage();
                tryCatchEmailAlert.veevaLoad(Header);
           }*/
    }
    
    global void execute(Database.BatchableContext BC, List<Parent_PACP__c> scopePacpProRecs)
    {
        create_MC_Cycle_Plan_Target_vod(scopePacpProRecs);

    }

    global void finish(Database.BatchableContext BC)
    {
        if(activityLogIDSet != null)
            Database.executeBatch(new MarkMCCPtargetDeleted_EU(allTeamInstances, allChannels, activityLogIDSet),2000);

        else
            Database.executeBatch(new MarkMCCPtargetDeleted_EU(allTeamInstances, allChannels),2000);
    }
}