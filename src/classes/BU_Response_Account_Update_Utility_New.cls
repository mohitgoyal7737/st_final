global with sharing class BU_Response_Account_Update_Utility_New implements Database.Batchable<sObject>,Database.Stateful {
    public String query;
    public String selectedTeamInstance;
    public String selectedTeam;
    //public AxtriaSalesIQTM__Team_Instance__c notSelectedTeamInstance;
    Map<String,Id> teamInstanceNametoIDMap;
    Map<String,string> teamInstanceNametoTeamNameMap;
    //Map<String,string> teamInstanceNametoAZCycleMap;
    //Map<String,string> teamInstanceNametoAZCycleNameMap;
    Map<String,string> teamInstanceNametoTeamIDMap;
    Map<String,string> teamInstanceNametoBUMap;
    Map<String,string> brandIDteamInstanceNametoBrandTeamInstIDMap;
    //public Map<String,BU_Response__c> questionToObjectMap;
    //public Map<String,Integer> questionToResponseFieldNoMap;
    public String accountfieldname;
    public String questionname;
    //public Integer availableRespField;
    public Map<String,Integer> productToavailableRespFieldMap;
    public Integer GlobalCounter = 1;
    List<Parameter__c> parameterList;
    Set<String> tempSet;

    
    global BU_Response_Account_Update_Utility_New(String teamInstance,String accfieldname,String qname) {
        //availableRespField=1;
        tempSet = new Set<String>(); 
        teamInstanceNametoIDMap = new Map<String,Id>();
        teamInstanceNametoTeamNameMap = new Map<String,String>();
        //teamInstanceNametoAZCycleMap = new Map<String,String>();
        //teamInstanceNametoAZCycleNameMap = new Map<String,String>();
        teamInstanceNametoTeamIDMap = new Map<String,String>();
        teamInstanceNametoBUMap = new Map<String,String>();
        brandIDteamInstanceNametoBrandTeamInstIDMap = new Map<String,String>();
        productToavailableRespFieldMap = new Map<String,Integer>();
        //questionToObjectMap = new Map<String,Staging_Cust_Survey_Profiling__c>();
        //questionToResponseFieldNoMap = new Map<String,Integer>();
        selectedTeamInstance = teamInstance;
        GlobalCounter = 1;
        
        for(AxtriaSalesIQTM__Team_Instance__c teamIns : [Select Id,Name,AxtriaSalesIQTM__Team__c,/*AxtriaSalesIQTM__Team__r.Business_Unit_Loopup__c,*/AxtriaSalesIQTM__Team__r.name/*,Cycle__c,Cycle__r.name*/ From AxtriaSalesIQTM__Team_Instance__c WHERE name = :selectedTeamInstance])
        {
            teamInstanceNametoIDMap.put(teamIns.Name,teamIns.Id);
            teamInstanceNametoTeamNameMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__r.name);
            //teamInstanceNametoAZCycleMap.put(teamIns.Name,teamIns.Cycle__c);
            //teamInstanceNametoAZCycleNameMap.put(teamIns.Name,teamIns.Cycle__r.name);
            teamInstanceNametoTeamIDMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__c);
            //teamInstanceNametoBUMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__r.Business_Unit_Loopup__c);
        }
        
        /*for(Brand_Team_Instance__c bti :[select id,Brand__c,Brand__r.name,Brand__r.Veeva_External_ID__c,Team_Instance__c,Team_Instance__r.name from Brand_Team_Instance__c])
        {
            brandIDteamInstanceNametoBrandTeamInstIDMap.put(bti.Brand__r.Veeva_External_ID__c+bti.Brand__r.name+bti.Team_Instance__r.name,bti.id);
        }*/
        for(Product_Catalog__c bti :[select id,name,Veeva_External_ID__c,Team_Instance__c,Team_Instance__r.name from Product_Catalog__c WHERE name != NULL and Team_Instance__r.name = :selectedTeamInstance])
        {
            brandIDteamInstanceNametoBrandTeamInstIDMap.put(bti.Veeva_External_ID__c+bti.name+bti.Team_Instance__r.name,bti.id);
        }
        
        selectedTeam = teamInstanceNametoTeamNameMap.get(selectedTeamInstance);
        
        
        if(qname!=null && qname!='')
        {
            questionname=qname;
        }
        else{
            questionname='Speciality';
        }
        
        List<Product_Catalog__c> pcList = new List<Product_Catalog__c>();
        pcList = [select id,name,Veeva_External_ID__c from Product_Catalog__c where Team_Instance__c = :teamInstanceNametoIDMap.get(teamInstance) and IsActive__c=true];//and IsActive__c = true
        
        for(Product_Catalog__c pcRec1 : pcList)
        {
            productToavailableRespFieldMap.put(pcRec1.Veeva_External_ID__c+pcRec1.name,1);
        }
        
        
        parameterList = new List<Parameter__c>();
        List<MetaData_Definition__c> metaDataDefinitionList = new List<MetaData_Definition__c>();
        //List<Survey_Definition__c> surveyDefinitionList = new List<Survey_Definition__c>();
        Parameter__c tempParameterRec = new Parameter__c();
        MetaData_Definition__c tempMetaDataDefinitionRec = new MetaData_Definition__c();
       /* Survey_Definition__c tempsurveyDefinitionRec = new Survey_Definition__c();*/
        
        Map<string,Parameter__c> parameterAlreadyExistList = new Map<string,Parameter__c>();
        for(Parameter__c p : [select id,Name__c,Product_Catalog__r.Veeva_External_ID__c,Product_Catalog__r.name, Team_Instance__c from Parameter__c WHERE Name__c != NULL and Team_Instance__c = :teamInstanceNametoIDMap.get(teamInstance)])
        {
            parameterAlreadyExistList.put(p.Name__c+p.Product_Catalog__r.Veeva_External_ID__c+p.Product_Catalog__r.name+p.Team_Instance__c,p);
        }
        
        Map<string,MetaData_Definition__c> metaDataDefinitionAlreadyExistList = new Map<string,MetaData_Definition__c>();
        for(MetaData_Definition__c mdd : [select id,Display_Name__c,Team_Instance__c,Team_Instance__r.name,Product_Catalog__r.Veeva_External_ID__c,Product_Catalog__r.name from MetaData_Definition__c where Sequence__c != 25 and  Team_Instance__c = :teamInstanceNametoIDMap.get(teamInstance) ])
        {//and  Sequence__c != 9
            metaDataDefinitionAlreadyExistList.put(mdd.Display_Name__c+mdd.Product_Catalog__r.Veeva_External_ID__c+mdd.Product_Catalog__r.name+mdd.Team_Instance__c,mdd);
            if(mdd.Team_Instance__r.name == teamInstance){
                //availableRespField++;
                if(!productToavailableRespFieldMap.containsKey(mdd.Product_Catalog__r.Veeva_External_ID__c+mdd.Product_Catalog__r.name)){
                    productToavailableRespFieldMap.put(mdd.Product_Catalog__r.Veeva_External_ID__c+mdd.Product_Catalog__r.name,1);
                }
                else{
                    productToavailableRespFieldMap.put(mdd.Product_Catalog__r.Veeva_External_ID__c+mdd.Product_Catalog__r.name,productToavailableRespFieldMap.get(mdd.Product_Catalog__r.Veeva_External_ID__c+mdd.Product_Catalog__r.name)+1);
                }
            }
        }
        
        /*Map<string,Survey_Definition__c> surveyDefinitionAlreadyExistList = new Map<string,Survey_Definition__c>();
        for(Survey_Definition__c sd : [select id,name,Brand_ID__c,Brand_Name__c,Question_Short_Text__c,Team_Instance__c,Cycle__c from Survey_Definition__c]){
            surveyDefinitionAlreadyExistList.put(sd.Brand_ID__c+sd.Brand_Name__c+sd.Question_Short_Text__c+sd.Team_Instance__c,sd);
        }*/
        //SnTDMLSecurityUtil.printDebugMessage('questionToObjectMap+++++'+questionToObjectMap);
        //SnTDMLSecurityUtil.printDebugMessage('questionToResponseFieldNoMap+++++'+questionToResponseFieldNoMap);
        
        //Integer counter1 = 1;
        //Integer counter2 = 1;
        /*if(!parameterAlreadyExistList.containsKey(questionname + teamInstanceNametoIDMap.get(teamInstance))){
            tempParameterRec = new Parameter__c();
            
            tempParameterRec.Name = questionname;
            tempParameterRec.isActive__c = true;
            tempParameterRec.Team_Instance__c = teamInstanceNametoIDMap.get(teamInstance);
            tempParameterRec.Team__c = teamInstanceNametoTeamIDMap.get(teamInstance);
            tempParameterRec.Business_Unit__c = teamInstanceNametoBUMap.get(teamInstance);
            tempParameterRec.Type__c = 'Text'; // ??
            tempParameterRec.CurrencyIsoCode = 'EUR';
            
            parameterList.add(tempParameterRec);
        }*/
        
        
        for(Product_Catalog__c pcRec : pcList){
            
            if(!parameterAlreadyExistList.containsKey(questionname + pcRec.Veeva_External_ID__c + pcRec.name + teamInstanceNametoIDMap.get(teamInstance))){
                tempParameterRec = new Parameter__c();
                
                tempParameterRec.Name__c = questionname;
                tempParameterRec.isActive__c = true;
                tempParameterRec.Team_Instance__c = teamInstanceNametoIDMap.get(teamInstance);
                tempParameterRec.Team__c = teamInstanceNametoTeamIDMap.get(teamInstance);
                //tempParameterRec.Business_Unit__c = teamInstanceNametoBUMap.get(teamInstance);
                tempParameterRec.Type__c = 'Text'; // ??
                //tempParameterRec.CurrencyIsoCode = 'EUR';
                tempParameterRec.Product_Catalog__c = brandIDteamInstanceNametoBrandTeamInstIDMap.get(pcRec.Veeva_External_ID__c + pcRec.name + teamInstance);  //??
                tempParameterRec.Parameter_Type__c = 'None';//SnT-170
                tempParameterRec.External_ID__c = questionname +'_'+ teamInstanceNametoIDMap.get(selectedTeamInstance) +'_'+ pcRec.Veeva_External_ID__c+'_'+ 'None';
                parameterList.add(tempParameterRec);
                /******SnT-170  begins****/
                Parameter__c tempParameterRec1 = new Parameter__c();
                tempParameterRec1 = tempParameterRec.clone();
                tempParameterRec1.Parameter_Type__c = 'HCO';
                tempParameterRec.External_ID__c = questionname +'_'+ teamInstanceNametoIDMap.get(selectedTeamInstance) +'_'+ pcRec.Veeva_External_ID__c+'_'+ 'HCO';
                parameterList.add(tempParameterRec1);
                tempParameterRec1 = new Parameter__c();
                tempParameterRec1 = tempParameterRec.clone();
                tempParameterRec1.Parameter_Type__c = 'HCP';
                tempParameterRec.External_ID__c = questionname +'_'+ teamInstanceNametoIDMap.get(selectedTeamInstance) +'_'+ pcRec.Veeva_External_ID__c+'_'+ 'HCP';
                parameterList.add(tempParameterRec1);

                /******SnT-170  ends****/
            }
        
            if(!metaDataDefinitionAlreadyExistList.containsKey(questionname + pcRec.Veeva_External_ID__c + pcRec.name + teamInstanceNametoIDMap.get(teamInstance))){
                tempMetaDataDefinitionRec = new MetaData_Definition__c();
                
                tempMetaDataDefinitionRec.Source_Object__c = 'BU_Response__c';
                tempMetaDataDefinitionRec.Type__c = 'Text'; // ??
                tempMetaDataDefinitionRec.Sequence__c = productToavailableRespFieldMap.get(pcRec.Veeva_External_ID__c+pcRec.name);//availableRespField;//Integer.ValueOf(questionToObjectMap.get(qtext).QUESTION_ID__c)+1; //counter1;
                tempMetaDataDefinitionRec.Source_Field__c = 'Response'+String.ValueOf(productToavailableRespFieldMap.get(pcRec.Veeva_External_ID__c+pcRec.name))+'__c';//String.ValueOf(Integer.ValueOf(questionToObjectMap.get(qtext).QUESTION_ID__c)+1)//String.valueOf(counter1)
                tempMetaDataDefinitionRec.Display_Name__c = questionname;
                tempMetaDataDefinitionRec.Team_Instance__c = teamInstanceNametoIDMap.get(teamInstance);
                tempMetaDataDefinitionRec.Team__c = teamInstanceNametoTeamIDMap.get(teamInstance);
                //tempMetaDataDefinitionRec.CurrencyIsoCode = 'EUR';
                tempMetaDataDefinitionRec.Product_Catalog__c = brandIDteamInstanceNametoBrandTeamInstIDMap.get(pcRec.Veeva_External_ID__c + pcRec.name + teamInstance);  //??
                //counter1++;
                
                SnTDMLSecurityUtil.printDebugMessage('pcRec.Veeva_External_ID__c + teamInstance  +++++'+pcRec.Veeva_External_ID__c+ pcRec.name + teamInstance);
                metaDataDefinitionList.add(tempMetaDataDefinitionRec);
                 /******SnT-170  begins****/
                MetaData_Definition__c tempMetaDataDefinitionRec1 = new MetaData_Definition__c();
                tempMetaDataDefinitionRec1 = tempMetaDataDefinitionRec.clone();
                tempMetaDataDefinitionRec1.Parameter_Type__c = 'HCO';
                metaDataDefinitionList.add(tempMetaDataDefinitionRec1);
                tempMetaDataDefinitionRec1 = new MetaData_Definition__c();
                tempMetaDataDefinitionRec1 = tempMetaDataDefinitionRec.clone();
                tempMetaDataDefinitionRec1.Parameter_Type__c = 'HCP';
                metaDataDefinitionList.add(tempMetaDataDefinitionRec1);

                /******SnT-170  ends****/
            }
            
            /*if(!surveyDefinitionAlreadyExistList.containsKey(pcRec.Veeva_External_ID__c+ pcRec.name + questionname + teamInstanceNametoIDMap.get(teamInstance))){
                tempsurveyDefinitionRec = new Survey_Definition__c();
                
                tempsurveyDefinitionRec.Brand_ID__c = pcRec.Veeva_External_ID__c;
                tempsurveyDefinitionRec.Brand_Name__c = pcRec.name;
                tempsurveyDefinitionRec.Cycle__c = teamInstanceNametoAZCycleMap.get(teamInstance);
                tempsurveyDefinitionRec.Cycle_ID__c = teamInstanceNametoAZCycleNameMap.get(teamInstance);
                tempsurveyDefinitionRec.Cycle_Name__c = teamInstanceNametoAZCycleNameMap.get(teamInstance);
                
                tempsurveyDefinitionRec.Question_ID__c = String.valueOf(productToavailableRespFieldMap.get(pcRec.Veeva_External_ID__c+pcRec.name));//String.valueOf(Integer.ValueOf(questionToObjectMap.get(qtext).QUESTION_ID__c)+1);//String.valueOf(counter2);//
                tempsurveyDefinitionRec.Question_Short_Text__c = questionname;
                tempsurveyDefinitionRec.Question_Text__c = questionname;
                
                tempsurveyDefinitionRec.Survey_ID__c = 'Axtria Internal';//questionToObjectMap.get(qtext).Survey_ID__c;
                tempsurveyDefinitionRec.Survey_Name__c = 'Axtria Internal';//questionToObjectMap.get(qtext).Survey_Name__c;
                tempsurveyDefinitionRec.Team_Instance__c = teamInstanceNametoIDMap.get(teamInstance); //??
                //tempsurveyDefinitionRec.CurrencyIsoCode = 'EUR';
                //counter2++;
                
                surveyDefinitionList.add(tempsurveyDefinitionRec);
            }*/
        }
        
        SnTDMLSecurityUtil.printDebugMessage('parameterList+++++++++++'+parameterList);
        SnTDMLSecurityUtil.printDebugMessage('metaDataDefinitionList+++++++++++'+metaDataDefinitionList);
        
        //insert parameterList;
        SnTDMLSecurityUtil.insertRecords(parameterList, 'BU_Response_Account_Update_Utility_New');
        //insert metaDataDefinitionList;
        SnTDMLSecurityUtil.insertRecords(metaDataDefinitionList, 'BU_Response_Account_Update_Utility_New');
        //insert surveyDefinitionList;
        
        
        //removed BU__c
        query = 'Select Id, Name, CurrencyIsoCode, Acc_Pos__c,  Brand_ID__c, Product__c,  Product__r.Veeva_External_ID__c,Product__r.name, External_ID__c, Is_Active__c,  Physician__c, Physician__r.AccountNumber, ';
        if(accfieldname!=null && accfieldname!='')
        {
            query += 'Physician__r.'+accfieldname+' ,';
            accountfieldname=accfieldname;
        }
        else{
            query += 'Physician__r.Primary_Speciality_Name__c ,';
            accountfieldname='Primary_Speciality_Name__c';
        }
        //removed Survey_Master__c 
        query += ' Position_Account__c, Position__c, Response10__c, Response1__c, Response2__c, Response3__c, Response4__c, Response5__c, Response6__c, Response7__c, Response8__c, Response9__c, Team_Instance__c, Team_Instance__r.name, Team__c, is_inserted__c, is_updated__c,Response11__c,Response12__c,Response13__c,Response14__c,Response15__c,Response16__c,Response17__c,Response18__c,Response19__c,Response20__c,Response21__c,Response22__c,Response23__c,Response24__c,Response25__c FROM BU_Response__c  Where Team_Instance__r.name = \'' + teamInstance + '\'';//Profiling_Response__c, 
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        SnTDMLSecurityUtil.printDebugMessage('query+++++++++++++   ' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<BU_Response__c> scopeRecs) 
    {
        
        SnTDMLSecurityUtil.printDebugMessage('Hello++++++++++++++++++++++++++++');
        SnTDMLSecurityUtil.printDebugMessage('query++++++++++++++++++++++++++++'+query);
        
        //List<Survey_Response__c> surveyResponseList = new List<Survey_Response__c>();//commented due to object purge activity A1450
        //Survey_Response__c tempSurveyResponseRec = new Survey_Response__c();//commented due to object purge activity A1450
        
        for(BU_Response__c BUresp : scopeRecs)
           {
                //tempSurveyResponseRec = new Survey_Response__c();//commented due to object purge activity A1450
                //commented due to object purge activity A1450
                //tempSurveyResponseRec.Question_ID__c = productToavailableRespFieldMap.get(BUresp.Product__r.Veeva_External_ID__c+BUresp.Product__r.name);
                //tempSurveyResponseRec.Question_Short_Text__c = questionname;
                //SnTDMLSecurityUtil.printDebugMessage('BUresp.get(Physician__r.+accountfieldname)++++++++++++++++++++++++++++'+'Physician__r.'+accountfieldname);
                //SnTDMLSecurityUtil.printDebugMessage('productToavailableRespFieldMap.get(BUresp.Product__r.External_ID__c)++++++++++++++++++++++++++++'+productToavailableRespFieldMap.get(BUresp.Product__r.Veeva_External_ID__c+BUresp.Product__r.name));
                //SnTDMLSecurityUtil.printDebugMessage('Response value++----'+String.valueOf(BUresp.getSobject('Physician__r').get(accountfieldname)));
                //tempSurveyResponseRec.Response__c = String.valueOf(BUresp.getSobject('Physician__r').get(accountfieldname));
                //tempSurveyResponseRec.name = 'Axtria Internal';
                //tempSurveyResponseRec.Team_Instance__c = BUresp.Team_Instance__c;
                //tempSurveyResponseRec.Product__c = BUresp.Brand__r.Brand__c;
                //tempSurveyResponseRec.Physician__c = BUresp.Physician__c;
                

                //tempSurveyResponseRec.MetaData_Input__c//commented earlier 
                
                //commented due to object purge activity A1450
                //tempSurveyResponseRec.Cycle__c=teamInstanceNametoAZCycleMap.get(BUresp.Team_Instance__r.name);
                //tempSurveyResponseRec.Cycle_ID__c = teamInstanceNametoAZCycleNameMap.get(BUresp.Team_Instance__r.name);
                
                
                
                if(productToavailableRespFieldMap.get(BUresp.Product__r.Veeva_External_ID__c+BUresp.Product__r.name) == 1)
                {
                    if(BUresp.getSobject('Physician__r').get(accountfieldname) == null || BUresp.getSobject('Physician__r').get(accountfieldname) == '' ){
                        BUresp.Response1__c = 'ND';
                    }
                    else{
                        BUresp.Response1__c = String.valueOf(BUresp.getSobject('Physician__r').get(accountfieldname));
                    }
                }
                else if(productToavailableRespFieldMap.get(BUresp.Product__r.Veeva_External_ID__c+BUresp.Product__r.name) == 2){
                    if(BUresp.getSobject('Physician__r').get(accountfieldname) == null || BUresp.getSobject('Physician__r').get(accountfieldname) == '' ){
                        BUresp.Response2__c = 'ND';
                    }
                    else{
                        BUresp.Response2__c = String.valueOf(BUresp.getSobject('Physician__r').get(accountfieldname));
                    }
                }
                else if(productToavailableRespFieldMap.get(BUresp.Product__r.Veeva_External_ID__c+BUresp.Product__r.name) == 3){
                    if(BUresp.getSobject('Physician__r').get(accountfieldname) == null || BUresp.getSobject('Physician__r').get(accountfieldname) == '' ){
                        BUresp.Response3__c = 'ND';
                    }
                    else{
                        BUresp.Response3__c = String.valueOf(BUresp.getSobject('Physician__r').get(accountfieldname));
                    }
                }
                else if(productToavailableRespFieldMap.get(BUresp.Product__r.Veeva_External_ID__c+BUresp.Product__r.name) == 4){
                    if(BUresp.getSobject('Physician__r').get(accountfieldname) == null || BUresp.getSobject('Physician__r').get(accountfieldname) == '' ){
                        BUresp.Response4__c = 'ND';
                    }
                    else{
                        BUresp.Response4__c = String.valueOf(BUresp.getSobject('Physician__r').get(accountfieldname));
                    }
                }
                else if(productToavailableRespFieldMap.get(BUresp.Product__r.Veeva_External_ID__c+BUresp.Product__r.name) == 5){
                    if(BUresp.getSobject('Physician__r').get(accountfieldname) == null || BUresp.getSobject('Physician__r').get(accountfieldname) == '' ){
                        BUresp.Response5__c = 'ND';
                    }
                    else{
                        BUresp.Response5__c = String.valueOf(BUresp.getSobject('Physician__r').get(accountfieldname));
                    }
                }
                else if(productToavailableRespFieldMap.get(BUresp.Product__r.Veeva_External_ID__c+BUresp.Product__r.name) == 6){
                    if(BUresp.getSobject('Physician__r').get(accountfieldname) == null || BUresp.getSobject('Physician__r').get(accountfieldname) == '' ){
                        BUresp.Response6__c = 'ND';
                    }
                    else{
                        BUresp.Response6__c = String.valueOf(BUresp.getSobject('Physician__r').get(accountfieldname));
                    }
                }
                else if(productToavailableRespFieldMap.get(BUresp.Product__r.Veeva_External_ID__c+BUresp.Product__r.name) == 7){
                    if(BUresp.getSobject('Physician__r').get(accountfieldname) == null || BUresp.getSobject('Physician__r').get(accountfieldname) == '' ){
                        BUresp.Response7__c = 'ND';
                    }
                    else{
                        BUresp.Response7__c = String.valueOf(BUresp.getSobject('Physician__r').get(accountfieldname));
                    }
                }
                else if(productToavailableRespFieldMap.get(BUresp.Product__r.Veeva_External_ID__c+BUresp.Product__r.name) == 8){
                    if(BUresp.getSobject('Physician__r').get(accountfieldname) == null || BUresp.getSobject('Physician__r').get(accountfieldname) == '' ){
                        BUresp.Response8__c = 'ND';
                    }
                    else
                        BUresp.Response8__c = String.valueOf(BUresp.getSobject('Physician__r').get(accountfieldname));
                }
                else if(productToavailableRespFieldMap.get(BUresp.Product__r.Veeva_External_ID__c+BUresp.Product__r.name) == 9){
                    if(BUresp.getSobject('Physician__r').get(accountfieldname) == null || BUresp.getSobject('Physician__r').get(accountfieldname) == '' ){
                        BUresp.Response9__c = 'ND';
                    }
                    else{
                        BUresp.Response9__c = String.valueOf(BUresp.getSobject('Physician__r').get(accountfieldname));
                    }
                }
                else if(productToavailableRespFieldMap.get(BUresp.Product__r.Veeva_External_ID__c+BUresp.Product__r.name) == 10){
                    if(BUresp.getSobject('Physician__r').get(accountfieldname) == null || BUresp.getSobject('Physician__r').get(accountfieldname) == '' )
                    {
                        BUresp.Response10__c = 'ND';
                    }
                    else
                    {
                        BUresp.Response10__c = String.valueOf(BUresp.getSobject('Physician__r').get(accountfieldname));
                    }
                }
                
                else if(productToavailableRespFieldMap.get(BUresp.Product__r.Veeva_External_ID__c+BUresp.Product__r.name) == 11)
                {
                    if(BUresp.getSobject('Physician__r').get(accountfieldname) == null || BUresp.getSobject('Physician__r').get(accountfieldname) == '' )
                    {
                        BUresp.Response11__c = 'ND';
                    }
                    else{
                        BUresp.Response11__c = String.valueOf(BUresp.getSobject('Physician__r').get(accountfieldname));
                    }
                }
                else if(productToavailableRespFieldMap.get(BUresp.Product__r.Veeva_External_ID__c+BUresp.Product__r.name) == 12){
                    if(BUresp.getSobject('Physician__r').get(accountfieldname) == null || BUresp.getSobject('Physician__r').get(accountfieldname) == '' ){
                        BUresp.Response12__c = 'ND';
                    }
                    else{
                        BUresp.Response12__c = String.valueOf(BUresp.getSobject('Physician__r').get(accountfieldname));
                    }
                }
                else if(productToavailableRespFieldMap.get(BUresp.Product__r.Veeva_External_ID__c+BUresp.Product__r.name) == 13){
                    if(BUresp.getSobject('Physician__r').get(accountfieldname) == null || BUresp.getSobject('Physician__r').get(accountfieldname) == '' ){
                        BUresp.Response13__c = 'ND';
                    }
                    else{
                        BUresp.Response13__c = String.valueOf(BUresp.getSobject('Physician__r').get(accountfieldname));
                    }
                }
                else if(productToavailableRespFieldMap.get(BUresp.Product__r.Veeva_External_ID__c+BUresp.Product__r.name) == 14){
                    if(BUresp.getSobject('Physician__r').get(accountfieldname) == null || BUresp.getSobject('Physician__r').get(accountfieldname) == '' ){
                        BUresp.Response14__c = 'ND';
                    }
                    else{
                        BUresp.Response14__c = String.valueOf(BUresp.getSobject('Physician__r').get(accountfieldname));
                    }
                }
                else if(productToavailableRespFieldMap.get(BUresp.Product__r.Veeva_External_ID__c+BUresp.Product__r.name) == 15)
                {
                    if(BUresp.getSobject('Physician__r').get(accountfieldname) == null || BUresp.getSobject('Physician__r').get(accountfieldname) == '' )
                    {
                        BUresp.Response15__c = 'ND';
                    }
                    else
                    {
                        BUresp.Response15__c = String.valueOf(BUresp.getSobject('Physician__r').get(accountfieldname));
                    }
                }
                 else if(productToavailableRespFieldMap.get(BUresp.Product__r.Veeva_External_ID__c+BUresp.Product__r.name) == 16)
                {
                    if(BUresp.getSobject('Physician__r').get(accountfieldname) == null || BUresp.getSobject('Physician__r').get(accountfieldname) == '' )
                    {
                        BUresp.Response16__c = 'ND';
                    }
                    else
                    {
                        BUresp.Response16__c = String.valueOf(BUresp.getSobject('Physician__r').get(accountfieldname));
                    }
                }
                 else if(productToavailableRespFieldMap.get(BUresp.Product__r.Veeva_External_ID__c+BUresp.Product__r.name) == 17)
                {
                    if(BUresp.getSobject('Physician__r').get(accountfieldname) == null || BUresp.getSobject('Physician__r').get(accountfieldname) == '' )
                    {
                        BUresp.Response17__c = 'ND';
                    }
                    else
                    {
                        BUresp.Response17__c = String.valueOf(BUresp.getSobject('Physician__r').get(accountfieldname));
                    }
                }
                 else if(productToavailableRespFieldMap.get(BUresp.Product__r.Veeva_External_ID__c+BUresp.Product__r.name) == 18)
                {
                    if(BUresp.getSobject('Physician__r').get(accountfieldname) == null || BUresp.getSobject('Physician__r').get(accountfieldname) == '' )
                    {
                        BUresp.Response18__c = 'ND';
                    }
                    else
                    {
                        BUresp.Response18__c = String.valueOf(BUresp.getSobject('Physician__r').get(accountfieldname));
                    }
                }
                 else if(productToavailableRespFieldMap.get(BUresp.Product__r.Veeva_External_ID__c+BUresp.Product__r.name) == 19)
                {
                    if(BUresp.getSobject('Physician__r').get(accountfieldname) == null || BUresp.getSobject('Physician__r').get(accountfieldname) == '' )
                    {
                        BUresp.Response19__c = 'ND';
                    }
                    else
                    {
                        BUresp.Response19__c = String.valueOf(BUresp.getSobject('Physician__r').get(accountfieldname));
                    }
                }
                
                else if(productToavailableRespFieldMap.get(BUresp.Product__r.Veeva_External_ID__c+BUresp.Product__r.name) == 20)
                {
                    if(BUresp.getSobject('Physician__r').get(accountfieldname) == null || BUresp.getSobject('Physician__r').get(accountfieldname) == '' )
                    {
                        BUresp.Response20__c = 'ND';
                    }
                    else
                    {
                        BUresp.Response20__c = String.valueOf(BUresp.getSobject('Physician__r').get(accountfieldname));
                    }
                }
                else if(productToavailableRespFieldMap.get(BUresp.Product__r.Veeva_External_ID__c+BUresp.Product__r.name) == 21)
                {
                    if(BUresp.getSobject('Physician__r').get(accountfieldname) == null || BUresp.getSobject('Physician__r').get(accountfieldname) == '' )
                    {
                        BUresp.Response21__c = 'ND';
                    }
                    else
                    {
                        BUresp.Response21__c = String.valueOf(BUresp.getSobject('Physician__r').get(accountfieldname));
                    }
                }
                else if(productToavailableRespFieldMap.get(BUresp.Product__r.Veeva_External_ID__c+BUresp.Product__r.name) == 22)
                {
                    if(BUresp.getSobject('Physician__r').get(accountfieldname) == null || BUresp.getSobject('Physician__r').get(accountfieldname) == '' )
                    {
                        BUresp.Response22__c = 'ND';
                    }
                    else
                    {
                        BUresp.Response22__c = String.valueOf(BUresp.getSobject('Physician__r').get(accountfieldname));
                    }
                }
                 else if(productToavailableRespFieldMap.get(BUresp.Product__r.Veeva_External_ID__c+BUresp.Product__r.name) == 23)
                {
                    if(BUresp.getSobject('Physician__r').get(accountfieldname) == null || BUresp.getSobject('Physician__r').get(accountfieldname) == '' )
                    {
                        BUresp.Response23__c = 'ND';
                    }
                    else
                    {
                        BUresp.Response23__c = String.valueOf(BUresp.getSobject('Physician__r').get(accountfieldname));
                    }
                }
                 else if(productToavailableRespFieldMap.get(BUresp.Product__r.Veeva_External_ID__c+BUresp.Product__r.name) == 24)
                {   
                    SnTDMLSecurityUtil.printDebugMessage('===accountfieldname=='+accountfieldname);
                    SnTDMLSecurityUtil.printDebugMessage('=BUresp.getSobject='+BUresp.getSobject('Physician__r').get(accountfieldname));
                    if(BUresp.getSobject('Physician__r').get(accountfieldname) == null || BUresp.getSobject('Physician__r').get(accountfieldname) == '' )
                    {
                        BUresp.Response24__c = 'ND';
                    }
                    else
                    {
                        BUresp.Response24__c = String.valueOf(BUresp.getSobject('Physician__r').get(accountfieldname));
                    }
                }
                 else if(productToavailableRespFieldMap.get(BUresp.Product__r.Veeva_External_ID__c+BUresp.Product__r.name) == 25)
                {
                    if(BUresp.getSobject('Physician__r').get(accountfieldname) == null || BUresp.getSobject('Physician__r').get(accountfieldname) == '' )
                    {
                        BUresp.Response25__c = 'ND';
                    }
                    else
                    {
                        BUresp.Response25__c = String.valueOf(BUresp.getSobject('Physician__r').get(accountfieldname));
                    }
                }
            if(String.isNotBlank(String.valueOf(BUresp.getSobject('Physician__r').get(accountfieldname)))){
                tempSet.add(String.valueOf(BUresp.getSobject('Physician__r').get(accountfieldname)));
                SnTDMLSecurityUtil.printDebugMessage('tempset element=== '+String.valueOf(BUresp.getSobject('Physician__r').get(accountfieldname)));
            }
            
        }
        
        //update scopeRecs;
        SnTDMLSecurityUtil.updateRecords(scopeRecs, 'BU_Response_Account_Update_Utility_New');
        

    }

    global void finish(Database.BatchableContext BC) {
        
        List<String> tempList = new List<String>();
        List<Decimal> tempListNumeric = new List<Decimal>();
        tempList.addAll(tempSet);
        Boolean flag_non_numeric = true;
        SnTDMLSecurityUtil.printDebugMessage('tempList---'+tempList);
        tempList.sort();
        if(tempList.size()>50){
           tempList = new List<String>(); 
        }

        if(tempList.size()>0 && ST_Utility.isNumericString(tempList[0])){
            flag_non_numeric = false;
            for(String temp : tempList){
                if(!ST_Utility.isNumericString(temp)){
                    flag_non_numeric = true;
                    break;
                }
                else{
                    tempListNumeric.add(Decimal.valueOf(temp));     
                }
                 
            }
            
        }

        Parameter__c param = new Parameter__c();
        List<Parameter__c> paramListUpdate = new List<Parameter__c>();
        SnTDMLSecurityUtil.printDebugMessage('questionname--'+questionname);
        SnTDMLSecurityUtil.printDebugMessage('selectedTeamInstance--'+selectedTeamInstance);
        for(Parameter__c p : [Select ID from Parameter__c where Team_Instance__r.Name =:selectedTeamInstance and Name__c =:questionname and isActive__c = true]){
            param = new Parameter__c();
            param.ID= p.ID;
            if(flag_non_numeric){
                SnTDMLSecurityUtil.printDebugMessage('non-numeric');
                param.Values__c = String.join(tempList,';');  
            }
            else{
                SnTDMLSecurityUtil.printDebugMessage('numeric');
                tempListNumeric.sort();
                param.Values__c = String.join(tempListNumeric,';');     
            }
           paramListUpdate.add(param); 
        }
        if(paramListUpdate.size()>0){
            SnTDMLSecurityUtil.updateRecords(paramListUpdate, 'BU_Response_Account_Update_Utility_New');  
        }

    }

}