global with sharing class GenericBatch implements Database.Batchable<sObject> {
    global string Objname;
    global string fieldvalue;
    global string Query;
    global string fieldapi {get;set;}

    global GenericBatch(string ObjApiName , string fieldapiname,string value) {
        Query='';
        Objname='';
        fieldapi='';
        fieldvalue = '';

        Query='Select id from ';
        if(ObjApiName!=null && ObjApiName!=''){
            Objname = ObjApiName;
            SnTDMLSecurityUtil.printDebugMessage('===========obj Name::'+Objname);
            Query += Objname ;
        }
        if(fieldapiname!=null && fieldapiname!=''){
            fieldapi=fieldapiname;
            fieldvalue = value;
            SnTDMLSecurityUtil.printDebugMessage('=======fieldapi='+fieldapi);
            SnTDMLSecurityUtil.printDebugMessage('=======fieldvalue='+fieldvalue);
            Query+= ' Where '+fieldapi +'=:fieldvalue WITH SECURITY_ENFORCED';

        }
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        SnTDMLSecurityUtil.printDebugMessage('=========Query is::'+query);

        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Sobject> scope) {
        Database.DeleteResult[] DR_Dels = Database.delete(scope , false);
        Database.EmptyRecycleBinResult[] emptyRecycleBinResults = DataBase.emptyRecycleBin(scope); 
        //--Security code change----
        /*if(!Objname.isEmpty()){
            // Get the sObject token from the first ID
            // (the List contains IDs of sObjects of the same type).
            Schema.SObjectType token = Objname[0].getSObjectType();
            // Using the token, do a describe
            // and construct a query dynamically.
            Schema.DescribeSObjectResult objectDescribe = token.getDescribe();

            if (objectDescribe.isDeletable()) {
                Database.DeleteResult[] DR_Dels = Database.delete(scope , false);
                Database.EmptyRecycleBinResult[] emptyRecycleBinResults = DataBase.emptyRecycleBin(scope); 
            }
            else{
                printDebugMessage('Failed in '+clsName+', '+ System.Label.Security_Error_Object_Not_Deletable + objectDescribe);
            }
            */
        
        //Security Review change for CRUD DELETE
        /*sObject obj = sObject(Objname);
        if(scope.size()>0 && scope!=null){
            if(obj.sObjectType.getDescribe().isDeletable()){
                Database.DeleteResult[] DR_Dels = Database.delete(scope , false);
                Database.EmptyRecycleBinResult[] emptyRecycleBinResults = DataBase.emptyRecycleBin(scope); 
            }
            else{
                SnTDMLSecurityUtil.printDebugMessage('You dont have permission to delete Account_Compute_Final__c','BatchDeleteRecsBeforereExecute');
            }
        }*/


    }

    global void finish(Database.BatchableContext BC) {
        //Dtabase.ExcuteBatch(newbatch,200);
    }
}