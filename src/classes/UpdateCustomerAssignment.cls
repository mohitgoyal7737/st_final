public with sharing class UpdateCustomerAssignment {
public list<Account>AccountList {get;set;}
	public map<String,String>InactAcct{get;set;}
	public map<String,String>ActAcct{get;set;}
	public list<AxtriaSalesIQTM__Position_Account__c>posacc {get;set;}
	public String query {get;set;}
    public DateTime lastjobDate=null;
	public UpdateCustomerAssignment(){
		AccountList = new list<Account>();
		InactAcct = new map<String,String>();
		ActAcct = new map<String,String>();
		posacc = new list<AxtriaSalesIQTM__Position_Account__c>();
		query = '';
	}
    
    public void updatePosAcc(){
        List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
        schLogList=[Select Id,CreatedDate,Created_Date__c from Scheduler_Log__c where Job_Name__c='Accounts Delta' and Job_Status__c='Successful' Order By Created_Date__c desc];
        if(schLogList.size()>0){
            lastjobDate=schLogList[0].CreatedDate;  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
        else{
        lastjobDate=null;       //else we set the lastjobDate to null
        }
        query ='SELECT AccountNumber,Id,isAddressChanged__c,isSpecialtyChanged__c,isTypeChanged__c,LastModifiedDate, ' +
               'Mini_Brick__c FROM Account Where isAddressChanged__c=true ';
    	if(lastjobDate!=null){
        query = query + 'and LastModifiedDate  >=:  lastjobDate '; 
        }
        System.debug('query'+ query);
                
        list<Account>acctList  = Database.query(query);      
        list<AxtriaSalesIQTM__Position_Account__c>posacct = new list<AxtriaSalesIQTM__Position_Account__c>();
        posacct=[select id from AxtriaSalesIQTM__Position_Account__c where LastModifiedDate  >=:  lastjobDate and AxtriaSalesIQTM__Account__c in:acctList ];
                

    }
    }