public with sharing class ComputeTCFCtlr extends BusinessRule implements IBusinessRule{
    public Boolean errorFlag{get;set;} // - Updated for SR-453
    public integer row1 {get;set;}
    public integer col1{get;set;}
    public String gmid{get;set;}
    public String dm1 {get;set;}
    public String outputname {get;set;}
    public String outputType {get;set;}
    public String dm2 {get;set;}
    public String matrixname {get;set;}
    public list<Integer> cols{get;set;}
    public list<Integer> rows{get;set;}
     public String modeis {get;set;}
      public string interactiontype {get;set;}
    public String dm2outputtype{get;set;}
    public string dm1outputtype{get;set;}
    public boolean teammccp {get;set;}
    public list<SelectOption> options1{get;set;}
    public boolean countrymccp {get;set;}
     public boolean unique {get;set;}
    public list<string> colnames{get;set;}
    public list<string> colnamescheck{get;set;}
    public String JSONdata{get;set;}
    public String columnnames{get;set;}
    public list<String> dmlist;
    public String passedParam1;
    //public transient List<SLDSPageMessage> PageMessages{get;set;}
    public boolean frmsaveandnext;
    public list<String>displaydata {get;set;}
    public list<String>rowheader {get;set;}
    public map<String,Grid_Details__c>map2ddata {get;set;}
    public list<Grid_Details__c>viewgriddetails {get;set;}
    public String viewDM1{get;set;}
    public String viewDM2{get;set;}
    public set<String>rowheaders {get;set;}
    public set<String>colheaders {get;set;}
    public list<Grid_Master__c>viewgridmasterlist {get;set;}
    public list<Integer> colsize{get;set;}
    public list<Integer> Rowsize{get;set;}
   // public boolean isStepViewMode{get;set;}
    public boolean displaynewtable{get;set;}
    public String countryID {get;set;}
    public string businessRuleId;
    public ComputeTCFCtlr(){
        try{
        init();
        isAddNew = false;
        unique=false;
        errorFlag = false;
        dm1 = 'Segment';
        dm2 = 'Channel';
        passedParam1 = '';
        frmsaveandnext = false;
        col1 = 0;
        outputType ='';
        displaynewtable = false;
        dm2outputtype = '';
        dm1outputtype = '';
       // isStepViewMode = false;
        colnames = new List<String>();
        colnamescheck = new List<String>();
      /*  countryID = SalesIQUtility.getCookie(SalesIQUtility.getCountryCookieName());
        Map<String, String> successErrorMap = new Map<String, String>();
        successErrorMap = SalesIQUtility.checkCountryAccess(countryID);
        SnTDMLSecurityUtil.printDebugMessage('############ successErrorMap ' + successErrorMap);
        if(successErrorMap.containsKey('Success'))
        {
            countryID = successErrorMap.get('Success');
            SnTDMLSecurityUtil.printDebugMessage('########## countryID from Map ' + countryID);
            //As soon as we get Country ID set it in the cookie and it will applicable for whole application.
            SalesIQUtility.setCookieString('CountryID', countryID);
        }*/
        
        countryID = MCCP_Utility.getKeyValueFromPlatformCache('SIQCountryID');
       
        if(!Test.isRunningTest())
        {
            Measure_Master__c MM = [SELECT Id, Name,Channels__c,Team_Instance__c,Team_Instance__r.MCCP_Enabled__c,Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.MCCP_Enabled__c FROM Measure_Master__c WHERE id =:ruleObject.Id WITH SECURITY_ENFORCED];
            teammccp = MM.Team_Instance__r.MCCP_Enabled__c;
                businessRuleId= mm.id;
            countrymccp = MM.Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.MCCP_Enabled__c;
            /* if(MM.Channels__c != '' && MM.Channels__c != null)
            {
                
              //  if(interactiontype == 'Interaction')
              //  {
                    colnames = MM.Channels__c.split(';');
                    col1 = colnames.size();
                    columnnames = MM.Channels__c;
               // }
              /*  if(interactiontype == 'PDE')
                {
                    colnamescheck = MM.Channels__c.split(';');
                    colnames.add('Total PDE');
                    colnames.addAll(colnamescheck);
                    col1 = colnames.size();
                    columnnames = 'Total PDE';
                    columnnames = columnnames + MM.Channels__c;
                }
            }*/
        }
        else
        {
            teammccp = true;
            countrymccp = true;
            //colnames.add('F2F');
            col1 = 1;
            columnnames = 'F2F';
        }
        selectedMatrix = '';
        outputname = '';
        matrixname = '';
        colsize = new List<Integer>();
        rowsize = new List<Integer>();
        options1 = new list<SelectOption>();
        options1.add(new SelectOption('None', 'None'));
        options1.add(new SelectOption('Text', 'Text'));
        options1.add(new SelectOption('Number', 'Number'));
        retUrl = '/apex/ComputeTCF?mode=' +mode+'&rid='+ruleId; 
        uiLocation = 'Compute TCF';
        cols = new list<Integer>();
        rows = new list<Integer>();
        if(!Test.isRunningTest())
        initStep();
    }
        catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'ComputeTCFCtlr', businessRuleId);
            } 
    }

    public pagereference selectOnClick() 
    {
        try{
    if(!Test.isRunningTest())
        {
        Measure_Master__c MM = [SELECT Id, Name,Channels__c,Team_Instance__c,Team_Instance__r.MCCP_Enabled__c,Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.MCCP_Enabled__c FROM Measure_Master__c WHERE id =:ruleObject.Id WITH SECURITY_ENFORCED];
        if(MM.Channels__c != '' && MM.Channels__c != null)
            {
                
                colnamescheck.clear();
                colnames.clear();
                col1 = 0;
                columnnames ='';
                if(interactiontype == 'Interaction')
                {
                    colnames = MM.Channels__c.split(';');
                    col1 = colnames.size();
                    columnnames = MM.Channels__c;
                }
                if(interactiontype == 'PDE')
                {
                    colnamescheck = MM.Channels__c.split(';');
                    for(integer i=0;i<colnamescheck.size();i++)
                    {
                        colnamescheck[i] = colnamescheck[i]+' (%)';
                    }
                    colnames.add('Total PDE');
                    colnames.addAll(colnamescheck);
                    col1 = colnames.size();
                    columnnames = 'Total PDE';
                    columnnames = columnnames + MM.Channels__c;
                }
            }
            row1 = null;
            system.debug('check row1 in action'+row1);
        }
        else
        {
            col1 = 1;
            columnnames = 'F2F';
        }
        return null;  
        }catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'ComputeTCFCtlr', businessRuleId);
            return null;
            } 
    }

    public void saveFinal(String result){
        try{
        errorFlag = false;
        if(step != null && String.isBlank(result)){
     if(interactiontype == 'PDE')
            {
                step.Is_PDE__c = true;
            }
            else
            {
                step.Is_PDE__c = false;
            }
            step.UI_Location__c = uiLocation;
            /*
                    Use Case - Error if we dont give the input to the Matrix used in the Business Rule
                    Action - Checking the input value of gridParam1,gridParam2 if it is NONE throwing error message on the VF Page
                    Developer -J Siva Gopi
                    Developer Employee ID -A1266
                    Date -22-03-2018
                    JIRA Bug Code (if Applicable)-
            */
            if(gridParam1=='None'){
                    errorFlag = true;
                   // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please Fill All Data and Click on Save AND Next Else Click Cancel'));

                }
            else
            {
                if(step.Step_Type__c == 'Matrix'){
                    step.Matrix__c = selectedMatrix;
                    step.Grid_Param_1__c = gridParam1;
                    if(gridMap.Grid_Type__c == '2D'){
                        if(gridParam2 == 'None'){
                                errorFlag = true;
                        }
                        else{
                            step.Grid_Param_2__c = gridParam2;
                        }
                    }
                }else if(step.Step_Type__c == 'Compute'){
                    computeObj.Field_1__c = selectedCompF1;
                    computeObj.Field_2_Type__c = field2Type;
                    if(String.isNotBlank(selectedCompF2))
                        computeObj.Field_2__c = selectedCompF2;
                    else{
                        computeObj.Field_2_val__c = selectedCompF3;
                    }
                    //upsert computeObj;
                    SnTDMLSecurityUtil.upsertRecords(computeObj, 'ComputeTCFCtlr');
                    step.Compute_Master__c = computeObj.Id;
                }else if(step.Step_Type__c == 'Cases'){
                    computeObj.Expression__c = expression;
                    //upsert computeObj;
                    SnTDMLSecurityUtil.upsertRecords(computeObj, 'ComputeTCFCtlr');
                    step.Compute_Master__c = computeObj.Id;
         }
                else if(step.Step_Type__c == 'Multi Channel TCF Interactions'){
         String countryID =SalesIQUtility.getCookie(SalesIQUtility.getCountryCookieName());
                        Map<String, String> successErrorMap = new Map<String, String>();
                        successErrorMap = SalesIQUtility.checkCountryAccess(countryID);
                        SnTDMLSecurityUtil.printDebugMessage('############ successErrorMap ' + successErrorMap);
                        if(successErrorMap.containsKey('Success'))
                        {
                            countryID = successErrorMap.get('Success');
                            SnTDMLSecurityUtil.printDebugMessage('########## countryID from Map ' + countryID);
                            //As soon as we get Country ID set it in the cookie and it will applicable for whole application.
                            SalesIQUtility.setCookieString('CountryID', countryID);
                        }
                    if(JSONdata!=null)
                    {
                   /* List<Step__c> s = [Select id,Matrix__c from Step__c where Step_Type__c = 'Multi Channel TCF Interactions' and Measure_Master__c =:ruleObject.Id WITH SECURITY_ENFORCED];
                    if(s.size()>0)
                    {
                        system.debug('inside step'+s);
                        List<Grid_Master__c> existinggrid = [Select id from Grid_Master__c where id =:s[0].Matrix__c];
                        if(existinggrid.size()>0)
                        {
                            system.debug('check grid size'+existinggrid);
                            delete existinggrid;
                        }
                    } */
                    if(frmsaveandnext == false)
                    {
                    passedParam1 = Apexpages.currentPage().getParameters().get('JSONdata');
                    passedParam1=JSONdata;
                    }
                    List<Grid_Master__c> testGm;
                    list<Grid_Master__c> gm = new list<Grid_Master__c>();
                    Grid_Master__c obj1=new Grid_Master__c();
                    obj1.Name = matrixname;
                    obj1.Grid_Type__c = '2D';
                    obj1.Dimension_1_Name__c = dm1;
                    obj1.DM1_Output_Type__c = dm1outputtype;
                    obj1.Dimension_2_Name__c = dm2;
                    system.debug('check dm2 value'+dm2);
                    obj1.DM2_Output_Type__c = dm2outputtype;
                    obj1.Output_Type__c = Outputtype;
                    obj1.Output_Name__c = OutputName;
                    obj1.Row__c=row1;
                    obj1.Col__c=col1;
                    //obj1.Country__c = SalesIQUtility.getCookie(SalesIQUtility.getCountryCookieName());
              try
                    {
                        obj1.Country__c = countryID;
                    }
                    catch(Exception e)
                    {
                        SnTDMLSecurityUtil.printDebugMessage('########## country not found error ');
                    }
                     gm.add(obj1);
                     SObjectAccessDecision securityDecision = Security.stripInaccessible(AccessType.CREATABLE,gm);
                     list<Grid_Master__c> gmtemp = securityDecision.getRecords();
                Database.SaveResult[] sr=Database.insert(gmtemp,false); 
                 if(!securityDecision.getRemovedFields().isEmpty() ){
                   throw new SnTException(securityDecision, 'Grid_Matrix', SnTException.OperationType.C ); 
                }
                for (Database.SaveResult sr1 : sr) {
                    if (sr1.isSuccess()) {
                        // Operation was successful, so get the ID of the record that was processed
                        gmid=sr1.getid();
                        unique=true;
                    }
                else {
                    // Operation failed, so get all errors                
                    unique=false;
                    gmid='';
                    break;
                }
        }
        if(unique)
        {
            list<Map<String,String>> gg = (list<Map<String,String>>)JSON.deserialize(passedParam1, list<Map<String,String>>.Class);
            SnTDMLSecurityUtil.printDebugMessage('DATA IS:'+gg);
            list<Grid_Details__c>insertgm=new list<Grid_Details__c>();

            for(Map<String,String> g : gg){
                Grid_Details__c gd = new Grid_Details__c();
                gd.Dimension_1_Value__c = g.get('Dimension_1_Value__c');
        if((g.get('Dimension_2_Value__c')).contains('(%)'))
                {
                    gd.Dimension_2_Value__c = (g.get('Dimension_2_Value__c')).substringbefore(' (%)');
                }
                else
                {
                    gd.Dimension_2_Value__c = g.get('Dimension_2_Value__c');
                }

                
                gd.Output_Value__c = g.get('Output_Value__c');
                gd.Grid_Master__c = gmid;
                gd.Name = g.get('Name');
                insertgm.add(gd);
                SnTDMLSecurityUtil.printDebugMessage('NEW gridDetails Data is::'+gd);

            }
            SnTDMLSecurityUtil.printDebugMessage('INSert gridlist size is:'+insertgm.size());
            try{
                SObjectAccessDecision securityDecision1 = Security.stripInaccessible(AccessType.CREATABLE,insertgm);
                SnTDMLSecurityUtil.printDebugMessage('Grid_Matrix',' in insertRecords '); 
                list<Grid_Details__c> gmtemp1 = securityDecision1.getRecords();
                Database.SaveResult[] griddetail=Database.insert(insertgm,false);   
                SnTDMLSecurityUtil.printDebugMessage('Grid_Matrix',' in getRemovedFields::: '+securityDecision1.getRemovedFields());
                if(!securityDecision1.getRemovedFields().isEmpty() ){
                   throw new SnTException(securityDecision1, 'Grid_Matrix', SnTException.OperationType.C ); 
                }
                string returnurl=Apexpages.currentPage().getParameters().get('retUrl');
                SnTDMLSecurityUtil.printDebugMessage('--returnurl---' + returnurl);
                string rid=Apexpages.currentPage().getParameters().get('rid');
                SnTDMLSecurityUtil.printDebugMessage('--rid---' + rid);
                PageReference pr;
                if(returnurl==null || returnurl==''){
                    pr= new PageReference('/apex/List_View');
                    pr.setRedirect(true);
                }
                else{
                    String encoded = returnurl+'&rid='+rid;
                    pr= new PageReference(encoded);
                    pr.setRedirect(true);
                }
                SnTDMLSecurityUtil.printDebugMessage('---pr contains:'+pr);
               // return pr;
        step.Matrix__c = gmid;
            }
            Catch(Exception e){
               // ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please Fill All Data and Click on Save or Else Click Cancel');
               // ApexPages.addMessage(myMsg);
                   PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                   SalesIQSnTLogger.createUnHandledErrorLogs(e, SalesIQSnTLogger.BR_MODULE, 'ComputeTCFCtlr',businessRuleId);
                SnTDMLSecurityUtil.printDebugMessage('Exception Caught::'+e);
            }
        }
        else
        {
            PageMessages = SLDSPageMessage.add(PageMessages,Label.Uniquenames,'error');
      //  ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Kindly Use Unique Names');
              //  ApexPages.addMessage(myMsg);
        }
                   
                         frmsaveandnext = false;
                    }
                }
                //upsert step;
                SnTDMLSecurityUtil.upsertRecords(step, 'ComputeTCFCtlr');

                if(!isStepEditMode){
                    //Update Next steps if any
                    list<Step__c> allNextSteps ;
                    try{
                        allNextSteps = [SELECT Id, Sequence__c FROM Step__c WHERE Measure_Master__c =:ruleObject.Id AND Sequence__c >=: step.Sequence__c WITH SECURITY_ENFORCED ORDER BY Sequence__c];
                    }
                        catch(Exception qe) 
                    {
                            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                            SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,businessRuleId);
                        SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
                    }
                    Decimal duplicateSeq = 0;
                    map<Decimal, list<Step__c>> stepSeqMap = new map<Decimal,list<Step__c>>();
                    if(allNextSteps != null && allNextSteps.size() > 0){
                        // creating a map of id and sequence
                        
                        for(Step__c stepObj: allNextSteps){
                            if(stepSeqMap.get(stepObj.Sequence__c) == null)
                                stepSeqMap.put(stepObj.Sequence__c, new list<Step__c>{stepObj});
                            else{
                                list<Step__c> stepList = stepSeqMap.get(stepObj.Sequence__c);
                                stepList.add(stepObj);
                                stepSeqMap.put(stepObj.Sequence__c, stepList);
                            }
                        }

                        System.debug('stepSeqMap :'+stepSeqMap);

                        for(Step__c st: allNextSteps){
                            System.debug('st - '+st);
                            System.debug('duplicateSeq :'+duplicateSeq);
                            list<Step__c> tempStepList = stepSeqMap.get(st.Sequence__c);
                            if(tempStepList.size() > 1){
                                for(Step__c obj : tempStepList){
                                    if(st.Id != step.Id && step.Id != obj.Id){
                                        st.Sequence__c += 1;
                                        duplicateSeq = st.Sequence__c;
                                    }
                                }
                            }else{
                                
                                if(st.Sequence__c == duplicateSeq){
                                    st.Sequence__c += 1;
                                    duplicateSeq = st.Sequence__c;
                                }
                            }
                        }

                        System.debug('allNextSteps after update:'+allNextSteps);
                        /*for(Step__c st: allNextSteps){
                            System.debug('st - '+st);
                            if(st.Id != step.Id){
                                st.Sequence__c += 1;
                            }
                        }*/
                    }
                    //Update allNextSteps;
                    SnTDMLSecurityUtil.updateRecords(allNextSteps, 'ComputeTCFCtlr');
                }

                Rule_Parameter__c rp = new Rule_Parameter__c();
                if(String.isNotBlank(step.Id)){
                    list<Rule_Parameter__c> rps ;
                    try{
                        rps = [select id from Rule_Parameter__c WHERE Step__c =: step.Id WITH SECURITY_ENFORCED];
                    }
                        catch(Exception qe) 
                    {
                            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                            SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,businessRuleId);
                        SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
                    }

                    if(rps != null && rps.size() > 0){
                        rp.Id = rps[0].Id;
                    }
                }
                rp.Measure_Master__c = ruleObject.Id;
                rp.Step__c = step.Id;
                rp.Type__c = step.Type__c;
                //upsert rp;
                SnTDMLSecurityUtil.upsertRecords(rp,'ComputeTCFCtlr');
                updateRule(uiLocation, 'Compute Segment');
            }
        
        if(errorFlag){
            PageMessages = SLDSPageMessage.add(PageMessages,Label.BR_DimensionError,'error');
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.BR_DimensionError));
            }
        }
       }
       catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'ComputeTCFCtlr',businessRuleId);
            } 
    }
     public PageReference updatematrix(){
    try{
    SnTDMLSecurityUtil.printDebugMessage('*** entered updategrid method');
    SnTDMLSecurityUtil.printDebugMessage('JSONdata from Page::'+JSONdata);
    
    
    /* Code for updating Grid Details_data*/
    list<Map<String,String>> gg = (list<Map<String,String>>)JSON.deserialize(JSONdata, list<Map<String,String>>.Class);
    SnTDMLSecurityUtil.printDebugMessage('DATA for updating:'+gg);
    list<Grid_Details__c>updategriddetails=new list<Grid_Details__c>();
        for(Map<String,String> g : gg){
      Grid_Details__c gd = new Grid_Details__c();
      gd.Dimension_1_Value__c = g.get('Dimension_1_Value__c');

      //SnTDMLSecurityUtil.printDebugMessage('---GridType::'+gridMaster.Grid_Type__c);
      //if(gridMaster.Grid_Type__c!='1D')
      gd.Dimension_2_Value__c = g.get('Dimension_2_Value__c');
      gd.Output_Value__c = g.get('Output_Value__c');
      gd.id = g.get('Id');
      gd.Name = g.get('Name');
      updategriddetails.add(gd);
      SnTDMLSecurityUtil.printDebugMessage('NEW gridDetails Data is::'+gd);
    }
    SnTDMLSecurityUtil.printDebugMessage('updategriddetails size is:'+updategriddetails.size());
    try{
      //Database.SaveResult[] griddetail=Database.update(updategriddetails,false);
      //Changed for security review
      SObjectAccessDecision securityDecision4 = Security.stripInaccessible(AccessType.UPDATABLE,updategriddetails);
          //insert securityDecision.getRecords();
          list<Grid_Details__c> gmtemp4 = securityDecision4.getRecords();
          DataBase.SaveResult[] griddetail=Database.update(gmtemp4,false);  
          SnTDMLSecurityUtil.printDebugMessage('Grid_Details__c',' in getRemovedFields::: '+securityDecision4.getRemovedFields());           
          //throw exception if permission is missing 
          if(!securityDecision4.getRemovedFields().isEmpty() ){
             throw new SnTException(securityDecision4, 'Grid_Details__c', SnTException.OperationType.U ); 
          }
      //End of security review change
      SnTDMLSecurityUtil.printDebugMessage('------SaveResult Size is:'+griddetail.size());
    }
    Catch(Exception e){
      PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
      SalesIQSnTLogger.createUnHandledErrorLogs(e, SalesIQSnTLogger.BR_MODULE, 'ComputeTCFCtlr',businessRuleId);
      SnTDMLSecurityUtil.printDebugMessage('Exception Caught::'+e);
    }
    
    //PageReference p = new pageReference('/apex/')
    //PageReference pr= new PageReference('/apex/List_View');
    //pr.setRedirect(true);
    return null;
    }
    catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'ComputeTCFCtlr', businessRuleId);
            return null;
            } 
   }
    public void viewblock()
    {
    try{
    isStepViewMode = true;
    viewgridmasterlist=new list<Grid_Master__c>();
    viewgriddetails=new list<Grid_Details__c>();
    rowheaders = new set<String>();
    colheaders = new set<String>();
    displaydata = new list<String>();
    rowheader = new list<String>();
    map2ddata = new map<String,Grid_Details__c>();
    VDM1='';
    VDM2='';
    Step__c s = [Select id,Matrix__c,Step_Type__c,Is_PDE__c from Step__c where id = :stepEditId];
    try{
          viewgridmasterlist=[SELECT id,Name,Description__c, isGlobal__c, Brand__r.Name,Dimension_1_Name__c,Dimension_2_Name__c,Grid_Type__c,Output_Name__c,Output_Type__c,Brand__r.Team_Instance__r.Name,Row__c,Col__c,DM2_Output_Type__c,DM1_Output_Type__c from Grid_Master__c where id=:s.Matrix__c WITH SECURITY_ENFORCED];
      }
          catch(Exception qe) 
      {
              PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
              SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,businessRuleId);
          SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
      }
    try{
          viewgriddetails=[select id,Name,Dimension_1_Value__c,Dimension_2_Value__c,Output_Value__c,Rowvalue__c,colvalue__c from Grid_Details__c where Grid_Master__c=:s.Matrix__c WITH SECURITY_ENFORCED order by Rowvalue__c, colvalue__c];
      }
          catch(Exception qe) 
      {
              PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
              SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,businessRuleId);
          SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
      }
 
    for(Grid_Details__c g : viewgriddetails){
      rowheaders.add(g.Dimension_1_Value__c);
    if(s.Is_PDE__c)
      {
        if(g.Dimension_2_Value__c != 'Total PDE')
        {
        g.Dimension_2_Value__c = g.Dimension_2_Value__c + ' (%)';
        }
        colheaders.add(g.Dimension_2_Value__c);
      }
      else
      {
         colheaders.add(g.Dimension_2_Value__c); 
      }
    
    
      
    
    
    
    
    
      map2ddata.put(g.Name,g);
    }
    
    displaydata.addAll(colheaders);
    rowheader.addAll(rowheaders);
    comp.griddata=map2ddata;
    comp.cheaders=displaydata;
    comp.rheaders=rowheader;
    comp.Mode='View';
    viewDM1=viewgridmasterlist[0].Dimension_1_Name__c;
      viewDM2=viewgridmasterlist[0].Dimension_2_Name__c;
      if(viewgridmasterlist[0].Col__c != null){
        for(integer i=1;i<=viewgridmasterlist[0].Col__c;i++){
          colsize.add(i);
        }
      }
      if(viewgridmasterlist[0].Row__c != null){
        for(integer i=1;i<=viewgridmasterlist[0].Row__c;i++){
          Rowsize.add(i);
        }
      }
      }
      catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            system.debug('==========PageMessages========'+PageMessages);
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,'ComputeTCFCtlr', businessRuleId);
            } 
    }
    /*Skip Added to skip Compute TCF ST-17*/
    public PageReference skipStep()
    {
        try{
        updateRule('Compute Accessibility', uiLocation);
        return nextPage('ComputeAccessiblity');
        }
        catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'ComputeTCFCtlr', businessRuleId);
            return null;
            } 
    }
    public void save(){
        try{
        String result = validateStep(uiLocation);
        saveFinal(result);
        if(errorFlag){
            PageMessages = SLDSPageMessage.add(PageMessages,Label.BR_DimensionError,'error');
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.BR_DimensionError));
        }

        if(String.isNotBlank(result)){
        errorFlag = true; // -- Added by RT for SR-453 --
            PageMessages = SLDSPageMessage.add(PageMessages,result,'error');
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, result));
            SnTDMLSecurityUtil.printDebugMessage('No data found');
            }
        }
        catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'ComputeTCFCtlr',businessRuleId);
            } 
    }

    public PageReference saveAndNext(){
        try{
        String result = validateStep(uiLocation);
        if(String.isBlank(result)){
            list<Step__c> steps ;
            try{
                steps = [SELECT Id,Step_Type__c FROM Step__c WHERE Measure_Master__c =: ruleObject.Id AND UI_Location__c =:uiLocation WITH SECURITY_ENFORCED];
            }
                catch(Exception qe) 
            {
                    PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                    SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,businessRuleId);
                SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
            }
            if(step != null || (steps != null && steps.size() >0)){
         // updated for IQS - AR-1864
        if(step != null)
        {
             if(step.Step_Type__c != null) 
                {
            if(step.Step_Type__c == 'Multi Channel TCF Interactions')
        {
        if(JSONdata!=null)
                    {
                        passedParam1 = Apexpages.currentPage().getParameters().get('JSONdata');
                        passedParam1=JSONdata;
                        system.debug('check JSON'+passedParam1);
                        frmsaveandnext = true;
            }
                }
        }
    }
    else
        {
            if(steps[0].Step_Type__c != null)
                {
                    if(JSONdata!=null)
                    {
                        if(steps[0].Step_Type__c == 'Multi Channel TCF Interactions')
                        {
                            if(JSONdata!=null)
                            {
                                passedParam1 = Apexpages.currentPage().getParameters().get('JSONdata');
                                passedParam1=JSONdata;
                                system.debug('check JSON'+passedParam1);
                                frmsaveandnext = true;
                            } 
                        }
                    }
                }
            }
            // Chnages for IQS - AR-1864 ends here
                saveFinal(result);
                if(errorFlag){
                    PageMessages = SLDSPageMessage.add(PageMessages,Label.BR_DimensionError,'error');
                    //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.BR_DimensionError));
                }
                else{
                    updateRule('Compute Accessibility', uiLocation);
                    return nextPage('ComputeAccessiblity');
                }
            }else{
                PageMessages = SLDSPageMessage.add(PageMessages,'Please add atleast one step','error');
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please add atleast one step'));
            }
        }

        if(String.isNotBlank(result)){
            PageMessages = SLDSPageMessage.add(PageMessages,result,'error');
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, result));
            SnTDMLSecurityUtil.printDebugMessage('No data found');
            }
            return null;
        }
        catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'ComputeTCFCtlr',businessRuleId);
            return null;
            } 
    }
     public void createtable()
    {
       try{
       dmlist = new List<String>();
       Measure_Master__c MM = [SELECT Id, Name,Channels__c FROM Measure_Master__c WHERE id =:ruleObject.Id WITH SECURITY_ENFORCED];
       
      /*  if(!Test.isRunningTest())
        {
            colnames = MM.Channels__c.split(';');
        }
        else
        {
             colnames = new List<String>{'F2F'};
        } */
       col1 = colnames.size();
       
      
       dm1='Segment';
       dmlist.add(dm1);
       for(Integer i = 0; i< colnames.size();i++){
                  cols.add(i);
              }
        dm2='Channels';
        dmlist.add(dm2);
         for(Integer i = 0; i< row1;i++){
                  rows.add(i);
              }
    }
          catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'ComputeTCFCtlr',businessRuleId);
            
            } 
    }

    public void Openpopup(){
        SnTDMLSecurityUtil.printDebugMessage('============INSIDE OPEN POPUP FUNCTION');
        SnTDMLSecurityUtil.printDebugMessage('=========Select Matrix is::'+selectedMatrix);
        list<Step__c> Step ;
        try{
            Step = [Select id,Name,Measure_Master__r.Name from Step__c where Matrix__c=:selectedMatrix WITH SECURITY_ENFORCED];
        }
        catch(Exception qe) 
        {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,businessRuleId);
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }
        if(!step.isEmpty()){
            //removed BTI in below query
            Measure_Master__c MM = [SELECT Id, Name FROM Measure_Master__c WHERE id =:ruleObject.Id WITH SECURITY_ENFORCED];
            Grid_Master__c GM = [select id,name,Brand__c,Col__c,Country__c,Description__c,Dimension_1_Name__c,Dimension_2_Name__c,DM1_Output_Type__c,DM2_Output_Type__c,Grid_Type__c,Output_Name__c,Output_Type__c,Row__c from Grid_Master__c where id =:selectedMatrix WITH SECURITY_ENFORCED];
                Grid_Master__c CloneGM = GM.Clone();
                String name =GM.Name ;
                CloneGM.Name = MM.Name+'_'+name;
                CloneGM.Country__c = GM.Country__c;
               // CloneGM.CurrencyIsoCode = 'EUR';
                SnTDMLSecurityUtil.printDebugMessage('====CloneGM===:'+CloneGM);
                try{
                //insert CloneGM;
                SnTDMLSecurityUtil.insertRecords(CloneGM, 'ComputeTCFCtlr');
                matrixList.add(new SelectOption(CloneGM.Id, CloneGM.Name));

                list<Grid_Details__c> GD = new list<Grid_Details__c>();
                list<Grid_Details__c> CloneGD = new list<Grid_Details__c>();

                GD = [select id,Name,Grid_Master__c,colvalue__c,Dimension_1_Value__c,Dimension_2_Value__c,Output_Value__c,Rowvalue__c  from Grid_Details__c where Grid_Master__c =:selectedMatrix WITH SECURITY_ENFORCED];
                for(Grid_Details__c g : GD){
                    Grid_Details__c newGD = g.clone();
                        newGD.Grid_Master__c = CloneGM.id;
                        //newGD.CurrencyIsoCode = 'EUR';
                        newGD.Name = g.name;
                        CloneGD.add(newGD);
                    
                }
                //insert CloneGD;
                SnTDMLSecurityUtil.insertRecords(CloneGD, 'ComputeTCFCtlr');
                SnTDMLSecurityUtil.printDebugMessage('========CloneGD====:'+CloneGD);
                
                selectedMatrix= CloneGM.id;
            }
            catch(Exception ex){
               // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Matrix Name Should be Unique'));
                PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                SalesIQSnTLogger.createUnHandledErrorLogs(ex, SalesIQSnTLogger.BR_MODULE, 'ComputeTCFCtlr',businessRuleId);
            }
        }
    }
}