public with sharing class CreatePositionDirectLoad {

    String level='';
    String teamS='';
    String teamInsS='';
    Set<String> posSet = new Set<String>();
    Set<String> setPRID = new Set<String>();
    Set<String> teamID = new Set<String>();
    Set<String> teamName = new Set<String>();
    Set<String> teamInsID = new Set<String>();
    Set<String> teamInsName = new Set<String>();
    Map<String,String> mapTeamName2ID = new Map<String,String>();
    Map<String,AxtriaSalesIQTM__Position__c> mapPosCode2Rec = new Map<String,AxtriaSalesIQTM__Position__c>();
    Map<String,String> mapTeamInsName2Id = new Map<String,String>();
    Map<String,String> mapPRID2Name = new Map<String,String>();
    Map<String,Temp_Position_Employee__c> mapPos2TempRec = new Map<String,Temp_Position_Employee__c>();
    Map<String,List<AxtriaSalesIQTM__CR_Team_Instance_Config__c>> mapTI2Config = new Map<String,List<AxtriaSalesIQTM__CR_Team_Instance_Config__c>>();
    List<AxtriaSalesIQTM__Position__c> insertPosList = new List<AxtriaSalesIQTM__Position__c>();

    public CreatePositionDirectLoad(string team, string teamInstance){

        teamS = team;
        teamInsS = teamInstance;
        System.debug('++++++++++Korea Team++++++++++');
        List<AxtriaSalesIQTM__Team__c> teamList = [select Id, Name from AxtriaSalesIQTM__Team__c where Country_Name__c = 'Korea'];
        
        for(AxtriaSalesIQTM__Team__c teamRec : teamList){

            teamID.add(teamRec.Id);
            System.debug('team ID set' +teamID);
            teamName.add(teamRec.Name);
            System.debug('team Name set' +teamName);
            mapTeamName2ID.put(teamRec.Name,teamRec.Id);
        }

        System.debug('++++++++++Korea Team Instances++++++++++');
        System.debug('selected Team instance===' +teamS);
        if(teamName.contains(teamS) || teamID.contains(teamS)){
            List<AxtriaSalesIQTM__Team_Instance__c> teamInsList = [select Id, Name from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Team__c in :teamID and Country_Name__c = 'Korea' and (AxtriaSalesIQTM__Alignment_Period__c = 'Current' or AxtriaSalesIQTM__Alignment_Period__c = 'Future')];
            
            for(AxtriaSalesIQTM__Team_Instance__c teamInsRec : teamInsList){
                teamInsID.add(teamInsRec.Id);
                teamInsName.add(teamInsRec.Name);
                mapTeamInsName2Id.put(teamInsRec.Name,teamInsRec.Id);
            }

            System.debug('++++++++++Korea Positions++++++++++');
            List<AxtriaSalesIQTM__Position__c> posList = [select Id,AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Team_iD__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__inactive__c = false and AxtriaSalesIQTM__Team_iD__c in :teamID and AxtriaSalesIQTM__Team_Instance__c in :teamInsID];
            
            for(AxtriaSalesIQTM__Position__c posRec : posList){
                if(!posSet.contains(posrec.AxtriaSalesIQTM__Client_Position_Code__c)){
                    posSet.add(posrec.AxtriaSalesIQTM__Client_Position_Code__c);
                    mapPosCode2Rec.put(posrec.AxtriaSalesIQTM__Client_Position_Code__c,posrec);
                    System.debug('mapPosCode2Rec===' +mapPosCode2Rec);
                    System.debug('posSet===' +posSet);
                }
            }

            System.debug('++++++++++Korea new Position Employee Handling++++++++++');
            List<Temp_Position_Employee__c> tempPosEmpList = [select Id, Employee_PRID__c, Position_Code__c, Parent_Position__c, Role_Type__c, Team__c,Team_Instance__c from Temp_Position_Employee__c where Status__c = 'New' and Team__c in :teamName and Team_Instance__c in :teamInsName];
            for(Temp_Position_Employee__c peRec : tempPosEmpList){
                if(!posSet.contains(peRec.Position_Code__c)){
                    mapPos2TempRec.put(peRec.Position_Code__c,peRec);
                    setPRID.add(peRec.Employee_PRID__c);
                    System.debug('mapPos2TempRec===' +mapPos2TempRec);
                    System.debug('setPRID===' +setPRID);
                }
            }

            System.debug('++++++PRID and Employee Name Map+++++++');
            for(AxtriaSalesIQTM__Employee__c empRec : [select Id,Name, Employee_PRID__c from AxtriaSalesIQTM__Employee__c where Employee_PRID__c in :setPRID]){
                mapPRID2Name.put(empRec.Employee_PRID__c,empRec.Name);
                System.debug('mapPRID2Name===' +mapPRID2Name);
            }

            System.debug('+++++++++Role Type and Hierarchy Level++++++++++++');
            for(AxtriaSalesIQTM__CR_Team_Instance_Config__c config : [select Id,AxtriaSalesIQTM__Configuration_Value__c, AxtriaSalesIQTM__Configuration_Name__c, AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__CR_Team_Instance_Config__c where AxtriaSalesIQTM__Configuration_Type__c = 'Hierarchy Configuration' and AxtriaSalesIQTM__Team_Instance__c in :teamInsID]){
                //mapTI2Config.put(config.AxtriaSalesIQTM__Team_Instance__c,config);
                if(mapTI2Config.containsKey(config.AxtriaSalesIQTM__Team_Instance__c))
                    {

                      mapTI2Config.get(config.AxtriaSalesIQTM__Team_Instance__c).add(config);
                    }
                    else
                    {
                      List<AxtriaSalesIQTM__CR_Team_Instance_Config__c> crList = new List<AxtriaSalesIQTM__CR_Team_Instance_Config__c>();
                      crList.add(config);
                      mapTI2Config.put(config.AxtriaSalesIQTM__Team_Instance__c,crList);
                    }
            }
            System.debug('mapTI2Config===' +mapTI2Config);

            System.debug('+++++++++Create new Position++++++++++++');
            if(mapPos2TempRec.size() != null || mapPos2TempRec.size() > 0){
                for(String temp : mapPos2TempRec.keySet()){
                    System.debug('Loop Position' +temp);
                    AxtriaSalesIQTM__Position__c newPos = new AxtriaSalesIQTM__Position__c();
                    newPos.AxtriaSalesIQTM__Client_Position_Code__c = temp;
                    newPos.AxtriaSalesIQTM__RGB__c = '111,111,111';
                    newPos.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';

                    //Fetch Employee name
                    Temp_Position_Employee__c posEmp = mapPos2TempRec.get(temp);
                    newPos.Name = mapPRID2Name.get(posEmp.Employee_PRID__c);
                    
                    //Client Territory Code
                    newPos.AxtriaSalesIQTM__Client_Territory_Code__c = newPos.Name + '(' + temp + ')';

                    Temp_Position_Employee__c pos = mapPos2TempRec.get(temp);
                    System.debug('pos====' +pos);
                    newPos.AxtriaSalesIQTM__Team_Instance__c = mapTeamInsName2Id.get(pos.Team_Instance__c);
                    newPos.AxtriaSalesIQTM__Team_iD__c = mapTeamName2ID.get(pos.Team__c);

                    //Start Date and End Date
                    newPos.AxtriaSalesIQTM__Effective_Start_Date__c = newPos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c;
                    newPos.AxtriaSalesIQTM__Effective_End_Date__c = newPos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c;

                    //Hierarchy Level and Position Type
                    List<AxtriaSalesIQTM__CR_Team_Instance_Config__c> crConfigList = mapTI2Config.get(newPos.AxtriaSalesIQTM__Team_Instance__c);
                    Temp_Position_Employee__c tempRec = mapPos2TempRec.get(temp);
                    for(AxtriaSalesIQTM__CR_Team_Instance_Config__c tiConf : crConfigList){
                        if(tempRec.Role_Type__c != null){
                            if(tempRec.Role_Type__c == tiConf.AxtriaSalesIQTM__Configuration_Value__c){
                                String level = tiConf.AxtriaSalesIQTM__Configuration_Name__c;
                            }
                        }
                    }
                    newPos.AxtriaSalesIQTM__Position_Type__c = tempRec.Role_Type__c;
                    newPos.AxtriaSalesIQTM__Hierarchy_Level__c = level;
                    
                    newPos.AxtriaSalesIQTM__Parent_Position_Code__c = tempRec.Parent_Position__c;
                    if(tempRec.Parent_Position__c != null){
                        AxtriaSalesIQTM__Position__c posParent = mapPosCode2Rec.get(tempRec.Parent_Position__c);
                        newPos.AxtriaSalesIQTM__Parent_Position__c = posParent.Id;
                    }

                    insertPosList.add(newPos);
                }
            }
            if(insertPosList.size() > 0)
                insert insertPosList;
        }

        //BatchPositionEmpDirectLoad load = new BatchPositionEmpDirectLoad(teamS,teamInsS);
        //Database.executeBatch(load, 2000);
    }
    
}