global with sharing class Batch_DeleteProductMaster implements Database.Batchable<sObject> {
    list<Staging_Product_Master__c> listProd;
    string jobType = util.getJobType('Product');
    set<string> allCountries = jobType == 'Full Load'?util.getFulloadCountries():util.getDeltaLoadCountries();
   // map<string, boolean> mapProdStatus;
    string query;
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        SnTDMLSecurityUtil.printDebugMessage('JOBTYPE--'+jobType);
        SnTDMLSecurityUtil.printDebugMessage('COUNTRIES--'+allCountries);
        if(jobType == 'Full Load'){
            query = 'Select Id, IsActive__c, Record_Status__c, Country__c from Staging_Product_Master__c WITH SECURITY_ENFORCED';
        }
        else{    
            query = 'Select Id, IsActive__c, Record_Status__c, Country__c from Staging_Product_Master__c Where Record_Status__c = \'Updated\' WITH SECURITY_ENFORCED';
        }
        return database.getQueryLocator(query);
    }
            
    global void execute(Database.BatchableContext bc, List<Staging_Product_Master__c> scope){
        
        listProd = new list<Staging_Product_Master__c>();
        if(jobType == 'Full Load'){
            for(Staging_Product_Master__c pe : scope){ 
                if(allCountries.contains(pe.Country__c)){
                    system.debug('INSIDE COUNTRY--'+pe.country__c);
                    pe.IsActive__c = false;
                    pe.Record_Status__c = 'Updated';
                    listProd.add(pe);
                }
                else{
                    system.debug('COUNTRY LIST--'+allCountries);
                    system.debug('INSIDE OTHER COUNTRY--'+pe.country__c);
                    pe.Record_Status__c = 'No Change';
                    listProd.add(pe);    
                }
            }
        }
        else{
            for(Staging_Product_Master__c pe : scope){ 
                pe.Record_Status__c = 'No Change';
                listProd.add(pe);
            }    
        }
        if(listProd.size()>0){
            //upsert listProd;
            SnTDMLSecurityUtil.upsertRecords(listProd,'Batch_DeleteProductMaster');
        }
    }
    
    global void finish(Database.BatchableContext bc){
        if(!System.Test.isRunningTest())
            database.executeBatch(new Batch_ProductMasterMapping(), 2000);
    }
}