/*Author - Himanshu Tariyal(A0994)
Date : 21st January 2018*/
@isTest
private class DeleteStagingCustSurveyProfilingTest 
{
    private static testMethod void firstTest() 
    {
        /*Create initial test data for all the objs reqd.*/
        AxtriaSalesIQTM__Organization_Master__c aom = new AxtriaSalesIQTM__Organization_Master__c(AxtriaSalesIQTM__Org_Level__c='Global',AxtriaSalesIQTM__Parent_Country_Level__c=true);
        insert aom;
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='USA',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active');
        insert country;
        
        /*Global_Question_ID_AZ__c gq = new Global_Question_ID_AZ__c(Name='1');
        insert gq;*/
        
        /*Cycle__c cycle = new Cycle__c(Name='Test Cycle',Country__c = country.id);
        insert cycle;*/
        
        Account acc = new Account(Name='test acc',Marketing_Code__c='EU',AccountNumber='123456');
        insert acc;
        
        /*Business_Unit__c bu = new Business_Unit__c(Name='Test BU Loopup');
        insert bu;*/
        
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='Test Team'/*,Business_Unit_Loopup__c=bu.id*/);
        team.AxtriaSalesIQTM__Country__c = country.id;
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU',AxtriaSalesIQTM__Team__c=team.id/*,Cycle__c=cycle.id*/);
        insert ti;

        Product_Catalog__c pc = new Product_Catalog__c(Name='Test Product',Team_Instance__c=ti.id,Veeva_External_ID__c = 'ProdId',Product_Code__c= 'ProdId',IsActive__c=true,Country_Lookup__c=country.id);
        insert pc;
        
        /*Brand_Team_Instance__c bti = new Brand_Team_Instance__c(Brand__c=pc.id,Team_Instance__c=ti.id);
        insert bti;*/
        
        /*Prepare Staging_Survey_Data__c data*/
        Staging_Survey_Data__c ssd = new Staging_Survey_Data__c();
        ssd.CurrencyIsoCode = 'USD';
        ssd.Sales_Cycle__c = '2018CICLO2';
        ssd.SURVEY_ID__c = '1';
        ssd.SURVEY_NAME__c = 'Methanol_pcp';
        ssd.Team_Instance__c = '';
        ssd.Team__c = 'RIA_CRESTOR';
        ssd.Account_Number__c = 'IT100501';
        ssd.Product_Code__c = '172_002000017000_IT';
        ssd.Product_Name__c = 'Methanol';
        ssd.Position_Code__c = '';
        ssd.Question_ID1__c = 1;
        ssd.Question_ID2__c = 2;
        ssd.Question_ID3__c = 3;
        ssd.Question_ID4__c = 4;
        ssd.Question_ID5__c = 5;
        ssd.Question_ID6__c = 6;
        ssd.Question_ID7__c = 7;
        ssd.Question_ID8__c = 8;
        ssd.Question_ID9__c = 9;
        ssd.Question_ID10__c = 10;
        ssd.Response1__c = '1';
        ssd.Response2__c = '2';
        ssd.Response3__c = '3';
        ssd.Response4__c = '4';
        ssd.Response5__c = '5';
        ssd.Response6__c = '6';
        ssd.Response7__c = '7';
        ssd.Response8__c = '8';
        ssd.Response9__c = '9';
        ssd.Response10__c = '10';
        ssd.Short_Question_Text1__c = 'a';
        ssd.Short_Question_Text2__c = 'b';
        ssd.Short_Question_Text3__c = 'c';
        ssd.Short_Question_Text4__c = 'd';
        ssd.Short_Question_Text5__c = 'e';
        ssd.Short_Question_Text6__c = 'f';
        ssd.Short_Question_Text7__c = 'g';
        ssd.Short_Question_Text8__c = 'h';
        ssd.Short_Question_Text9__c = 'i';
        ssd.Short_Question_Text10__c = 'j';
        insert ssd;
        
        /*Prepare Staging_Cust_Survey_Profiling__c data*/
        Staging_Cust_Survey_Profiling__c scsp = new Staging_Cust_Survey_Profiling__c();
        scsp.CurrencyIsoCode = 'USD';
        scsp.SURVEY_ID__c = '1';
        scsp.SURVEY_NAME__c = 'Methanol_pcp';
        scsp.BRAND_ID__c = 'ProdId';
        scsp.BRAND_NAME__c = 'Test Product';
        scsp.PARTY_ID__c = '123456';
        scsp.QUESTION_SHORT_TEXT__c = 'Question data';
        scsp.RESPONSE__c = '4';
        scsp.Team_Instance__c = ti.Name;
        scsp.QUESTION_ID__c = 1;
        insert scsp;
        
        Staging_Cust_Survey_Profiling__c scsp2 = new Staging_Cust_Survey_Profiling__c();
        scsp2.CurrencyIsoCode = 'USD';
        scsp2.SURVEY_ID__c = '2';
        scsp2.SURVEY_NAME__c = 'Methanol_pcp';
        scsp2.BRAND_ID__c = 'ProdId';
        scsp2.BRAND_NAME__c = 'Test Product';
        scsp2.PARTY_ID__c = '123456';
        scsp2.QUESTION_SHORT_TEXT__c = 'Question data 2';
        scsp2.RESPONSE__c = '5';
        scsp2.Team_Instance__c = ti.Name;
        scsp2.QUESTION_ID__c = 2;
        insert scsp2;
        
        /*Prepare Survey_Response__c data*/

        //commented due to object purge activity A1450
        // Survey_Response__c sr = new Survey_Response__c();
        // sr.Name = 'Test SR';
        // sr.Team_Instance__c = ti.id;
        // insert sr;
        
        /*Prepare Survey_Response__c data*/
        Parameter__c param = new Parameter__c();
        param.Name = 'Test param';
        param.Team_Instance__c = ti.id;
        insert param; 
        
        /*Prepare MetaData_Definition__c data*/
        MetaData_Definition__c md = new MetaData_Definition__c();
        md.Team_Instance__c = ti.id;
        insert md;
        
        /*Prepare Survey_Definition__c data*/
       /* Survey_Definition__c sd = new Survey_Definition__c();
        sd.Team_Instance__c = ti.id;
        insert sd; */
        
        /*Prepare Staging_BU_Response__c data*/
        Staging_BU_Response__c sbur = new Staging_BU_Response__c();
        sbur.Team_Instance__c = ti.id;
        insert sbur; 
        
        /*Prepare BU_Response__c data*/
        BU_Response__c bur = new BU_Response__c();
        bur.Team_Instance__c = ti.id;
        insert bur;

        System.test.startTest();
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        Delete_Staging_Cust_Survey_Profiling batch = new Delete_Staging_Cust_Survey_Profiling(ti.Name,'File');
        Database.executeBatch(batch,2000);
        System.test.stopTest();
    }
    
    private static testMethod void secondTest()
    {
        /*Create initial test data for all the objs reqd.*/
        AxtriaSalesIQTM__Organization_Master__c aom = new AxtriaSalesIQTM__Organization_Master__c(AxtriaSalesIQTM__Org_Level__c='Global',AxtriaSalesIQTM__Parent_Country_Level__c=true);
        insert aom;
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='Italy',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active');
        insert country;
        
       /* Global_Question_ID_AZ__c gq = new Global_Question_ID_AZ__c(Name='1');
       insert gq;*/
       
        /*Cycle__c cycle = new Cycle__c(Name='Test Cycle',Country__c = country.id);
        insert cycle;*/
        
        Account acc = new Account(Name='test acc',Marketing_Code__c='EU',AccountNumber='123456');
        insert acc;
        
        /*Business_Unit__c bu = new Business_Unit__c(Name='Test BU Loopup');
        insert bu;*/
        
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='Test Team'/*,Business_Unit_Loopup__c=bu.id*/);
        team.AxtriaSalesIQTM__Country__c = country.id;
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU',AxtriaSalesIQTM__Team__c=team.id/*,Cycle__c=cycle.id*/);
        insert ti;

        Product_Catalog__c pc = new Product_Catalog__c(Name='Test Product',Team_Instance__c=ti.id,Veeva_External_ID__c = 'ProdId',Product_Code__c= 'ProdId',IsActive__c=true,Country_Lookup__c=country.id);
        insert pc;
        
        /*Brand_Team_Instance__c bti = new Brand_Team_Instance__c(Brand__c=pc.id,Team_Instance__c=ti.id);
        insert bti;*/
        
        /*Prepare Staging_Veeva_Cust_Data__c data*/
        Staging_Veeva_Cust_Data__c svcd = new Staging_Veeva_Cust_Data__c();
        svcd.BRAND_ID__c = '172_002000017000_IT';
        svcd.BRAND_NAME__c = 'Methanol';
        svcd.PARTY_ID__c = 'IT100501';
        svcd.QUESTION_ID__c = 1;
        svcd.QUESTION_SHORT_TEXT__c = 'Test1';
        svcd.RESPONSE__c = '5';
        svcd.SURVEY_ID__c = '1';
        svcd.SURVEY_NAME__c = 'Methanol_pcp';
        insert svcd;
        
        Staging_Veeva_Cust_Data__c svcd2 = new Staging_Veeva_Cust_Data__c();
        svcd2.BRAND_ID__c = '172_002000017000_IT';
        svcd2.BRAND_NAME__c = 'Methanol';
        svcd2.PARTY_ID__c = 'IT100501';
        svcd2.QUESTION_ID__c = 2;
        svcd2.QUESTION_SHORT_TEXT__c = 'Test2';
        svcd2.RESPONSE__c = '5';
        svcd2.SURVEY_ID__c = '1';
        svcd2.SURVEY_NAME__c = 'Methanol_pcp';
        insert svcd2;

        System.test.startTest();
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        Delete_Staging_Cust_Survey_Profiling batch = new Delete_Staging_Cust_Survey_Profiling(ti.Name,'');
        Database.executeBatch(batch,2000);
        System.test.stopTest();
    }
    private static testMethod void thirdTest()
    {
        /*Create initial test data for all the objs reqd.*/
        AxtriaSalesIQTM__Organization_Master__c aom = new AxtriaSalesIQTM__Organization_Master__c(AxtriaSalesIQTM__Org_Level__c='Global',AxtriaSalesIQTM__Parent_Country_Level__c=true);
        insert aom;
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='Italy',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active');
        insert country;
        
        /*Global_Question_ID_AZ__c gq = new Global_Question_ID_AZ__c(Name='1');
        insert gq;
        */
        /*Cycle__c cycle = new Cycle__c(Name='Test Cycle',Country__c = country.id);
        insert cycle;*/
        
        Account acc = new Account(Name='test acc',Marketing_Code__c='EU',AccountNumber='123456');
        insert acc;
        
        /*Business_Unit__c bu = new Business_Unit__c(Name='Test BU Loopup');
        insert bu;*/
        
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='Test Team'/*,Business_Unit_Loopup__c=bu.id*/);
        team.AxtriaSalesIQTM__Country__c = country.id;
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU',AxtriaSalesIQTM__Team__c=team.id/*,Cycle__c=cycle.id*/);
        insert ti;

        Product_Catalog__c pc = new Product_Catalog__c(Name='Test Product',Team_Instance__c=ti.id,Veeva_External_ID__c = 'ProdId',Product_Code__c= 'ProdId',IsActive__c=true,Country_Lookup__c=country.id);
        insert pc;
        
        /*Brand_Team_Instance__c bti = new Brand_Team_Instance__c(Brand__c=pc.id,Team_Instance__c=ti.id);
        insert bti;*/
        
        /*Prepare Staging_Veeva_Cust_Data__c data*/
        Staging_Veeva_Cust_Data__c svcd = new Staging_Veeva_Cust_Data__c();
        svcd.BRAND_ID__c = '172_002000017000_IT';
        svcd.BRAND_NAME__c = 'Methanol';
        svcd.PARTY_ID__c = 'IT100501';
        svcd.QUESTION_ID__c = 1;
        svcd.QUESTION_SHORT_TEXT__c = 'Test1';
        svcd.RESPONSE__c = '5';
        svcd.SURVEY_ID__c = '1';
        svcd.SURVEY_NAME__c = 'Methanol_pcp';
        insert svcd;
        
        Staging_Veeva_Cust_Data__c svcd2 = new Staging_Veeva_Cust_Data__c();
        svcd2.BRAND_ID__c = '172_002000017000_IT';
        svcd2.BRAND_NAME__c = 'Methanol';
        svcd2.PARTY_ID__c = 'IT100501';
        svcd2.QUESTION_ID__c = 2;
        svcd2.QUESTION_SHORT_TEXT__c = 'Test2';
        svcd2.RESPONSE__c = '5';
        svcd2.SURVEY_ID__c = '1';
        svcd2.SURVEY_NAME__c = 'Methanol_pcp';
        insert svcd2;

        System.test.startTest();
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        Delete_Staging_Cust_Survey_Profiling batch = new Delete_Staging_Cust_Survey_Profiling();
        Database.executeBatch(batch,2000);
        System.test.stopTest();
    }
}