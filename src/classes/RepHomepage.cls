public with sharing class RepHomepage {
    public integer phyAdded {get;set;}
    public integer phyDropped {get;set;}
    public string colorNexavarCalls {get;set;}
    public string colorTotalHCPs {get;set;}
    public string colorStivargaCalls {get;set;}
    public string colortotalCallsHCP {get;set;}
    public boolean lock {get;set;}  
    public boolean savelock {get;set;}
    public string selectedPosition {get;set;} 
    public string selectedTeamInstance {get;set;}
    public Integer totalCallsHCP {get;set;}
    public Integer totalHCPs {get;set;}
    public Integer nexavarCalls {get;set;}
    public Integer stivargaCalls {get;set;} 
    public string genericCssForBar {get;set;}
    public string soql {get;set;}
    public String allFields{get;set;}
    public string soqlObject;
    public Map<String,String> segmentToValueMap{get;set;}
    public string columnsDataTable{get;set;} 
    public string columnDefsDataTable{get;set;}
    public Map<String,Integer> columnDefsOrderMap{get;set;}
    public string jsonMapString{get;set;}
    public string jsonStringfield{get;set;}
    public string teamInstanceName{get;set;}
    public List<pieChartWrap> pieChartWrapper   {get;set;}
    public RepHomepage(){
        List<AxtriaSalesIQTM__User_Access_Permission__c> loggedInUserData = SalesIQUtility.getUserAccessPermistion(Userinfo.getUserId());
        genericCssForBar = 'left,#FF0000 ' + 12.50 + '%,#ffbf00 ' + 25.00 + '%,#00FF00 ' + 75.00 + '%,#ffbf00 ' + 87.50 + '%,#FF0000 100%';
        system.debug('+++++++++ Logged in User '+loggedInUserData);
        if(loggedInUserData!=null && loggedInUserData.size()>0){
            selectedTeamInstance = loggedInUserData[0].AxtriaSalesIQTM__Team_Instance__c;
            selectedPosition     = loggedInUserData[0].AxtriaSalesIQTM__Position__c;
            teamInstanceName     = loggedInUserData[0].AxtriaSalesIQTM__Team_Instance__r.Name;
        }
        initialiseDataTable();
        cimConfig();
    }

    public void initialiseDataTable(){
        List<AxtriaSalesIQTM__Team_Instance_Object_Attribute__c> fetchAllRequiredColumns;
        fetchAllRequiredColumns = [select AxtriaSalesIQTM__Attribute_API_Name__c, AxtriaSalesIQTM__Attribute_Display_Name__c, AxtriaSalesIQTM__Data_Type__c, AxtriaSalesIQTM__Display_Column_Order__c, AxtriaSalesIQTM__Object_Name__c  from AxtriaSalesIQTM__Team_Instance_Object_Attribute__c where AxtriaSalesIQTM__isRequired__c = true and AxtriaSalesIQTM__Team_Instance__c =:selectedTeamInstance and AxtriaSalesIQTM__Interface_Name__c = 'Homepage CallPlan'];
        if(fetchAllRequiredColumns != null && fetchAllRequiredColumns.size() > 0){
            Set<ID> allPicklistColumnsRecs = new Set<ID>();
            Map<String,Map<String,String>> columnAndCorrespondingInfo = new Map<String,Map<String,String>>();
            Map<String,String> indtermediateMap;
            soql = 'select ';
            allFields ='';
            for(AxtriaSalesIQTM__Team_Instance_Object_Attribute__c teamInstanceObj : fetchAllRequiredColumns){
                indtermediateMap = new Map<String,String>();
                if(teamInstanceObj.AxtriaSalesIQTM__Data_Type__c == 'Picklist'){
                    allPicklistColumnsRecs.add(teamInstanceObj.ID);
                }

                indtermediateMap.put('Display Name', teamInstanceObj.AxtriaSalesIQTM__Attribute_Display_Name__c);
                indtermediateMap.put('Data Type', teamInstanceObj.AxtriaSalesIQTM__Data_Type__c);
                indtermediateMap.put('Order', String.valueof(teamInstanceObj.AxtriaSalesIQTM__Display_Column_Order__c));
                indtermediateMap.put('ID', teamInstanceObj.ID);

                columnAndCorrespondingInfo.put(teamInstanceObj.AxtriaSalesIQTM__Attribute_API_Name__c, indtermediateMap);
                
                soql = soql + teamInstanceObj.AxtriaSalesIQTM__Attribute_API_Name__c + ', ';
                allFields = allFields + teamInstanceObj.AxtriaSalesIQTM__Attribute_API_Name__c + ',';
            }

            system.debug('++ Hey before removing '+soql);
            soql = soql.removeEnd(', ');
            allFields = allFields.removeEnd(',');
            system.debug('++ Hey after removing '+soql);

            soql = soql + ' from '+ fetchAllRequiredColumns[0].AxtriaSalesIQTM__Object_Name__c + ' where AxtriaSalesIQTM__Position__c = :selectedPosition and AxtriaSalesIQTM__Team_Instance__c = :selectedTeamInstance '; //added 
            soqlObject = fetchAllRequiredColumns[0].AxtriaSalesIQTM__Object_Name__c;

            List<AxtriaSalesIQTM__Team_Instance_Object_Attribute_Detail__c> teamInstanceDetailRecs;
            teamInstanceDetailRecs = [select AxtriaSalesIQTM__Object_Value_Name__c, AxtriaSalesIQTM__Object_Value_Seq__c, AxtriaSalesIQTM__Object_Attibute_Team_Instance__r.AxtriaSalesIQTM__Attribute_API_Name__c from AxtriaSalesIQTM__Team_Instance_Object_Attribute_Detail__c where AxtriaSalesIQTM__Object_Attibute_Team_Instance__c in :allPicklistColumnsRecs];
            Map<String,List<AxtriaSalesIQTM__Team_Instance_Object_Attribute_Detail__c>> idToValuesMap = new Map<String,List<AxtriaSalesIQTM__Team_Instance_Object_Attribute_Detail__c>>();
            for(AxtriaSalesIQTM__Team_Instance_Object_Attribute_Detail__c attributeDetailRec : teamInstanceDetailRecs){
                if(idToValuesMap.containsKey(attributeDetailRec.AxtriaSalesIQTM__Object_Attibute_Team_Instance__r.AxtriaSalesIQTM__Attribute_API_Name__c)){
                    idToValuesMap.get(attributeDetailRec.AxtriaSalesIQTM__Object_Attibute_Team_Instance__r.AxtriaSalesIQTM__Attribute_API_Name__c).add(attributeDetailRec);
                }else{
                    idToValuesMap.put(attributeDetailRec.AxtriaSalesIQTM__Object_Attibute_Team_Instance__r.AxtriaSalesIQTM__Attribute_API_Name__c, new List<AxtriaSalesIQTM__Team_Instance_Object_Attribute_Detail__c>{attributeDetailRec});
                }
            }

            List<AxtriaSalesIQTM__Team_Instance_Object_Attribute_Detail__c> tempList;
            Map<Integer,String> indexToValMap;
            String val;
            segmentToValueMap = new Map<String, String>();
            for(String apiName : idToValuesMap.keySet()){
                tempList = new List<AxtriaSalesIQTM__Team_Instance_Object_Attribute_Detail__c>(idToValuesMap.get(apiName));
                indexToValMap = new Map<Integer, String>();
                val = '';
                for(AxtriaSalesIQTM__Team_Instance_Object_Attribute_Detail__c detailRec : tempList){
                    indexToValMap.put(Integer.valueof(detailRec.AxtriaSalesIQTM__Object_Value_Seq__c),detailRec.AxtriaSalesIQTM__Object_Value_Name__c);
                }
                integer counter = 0;
                for(Integer seq : indexToValMap.keySet()){
                    counter = counter + 1;
                    val = val + (indexToValMap.get(counter)) + ',';
                }
                val = val.removeEnd(',');
                segmentToValueMap.put(apiName, val);
            }
            jsonMapString = JSON.serialize(segmentToValueMap);
            system.debug('++++++++ segmentToValueMap '+segmentToValueMap);

            List<AxtriaSalesIQTM__Team_Instance_Object_Attribute__c> fetchAllVisibleColumns;
            fetchAllVisibleColumns = [select AxtriaSalesIQTM__Attribute_API_Name__c, AxtriaSalesIQTM__Attribute_Display_Name__c, AxtriaSalesIQTM__Data_Type__c, AxtriaSalesIQTM__Display_Column_Order__c, AxtriaSalesIQTM__Object_Name__c, AxtriaSalesIQTM__WrapperFieldMap__c  from AxtriaSalesIQTM__Team_Instance_Object_Attribute__c where AxtriaSalesIQTM__isEnabled__c = true and AxtriaSalesIQTM__Team_Instance__c =:selectedTeamInstance and AxtriaSalesIQTM__Interface_Name__c = 'Homepage CallPlan' order by AxtriaSalesIQTM__Display_Column_Order__c];
            createColumnStructure(fetchAllVisibleColumns);
        }
    }

    /*
�� �* Method to created Columns parameter of datatable
    * @param List<Team_Instance_Object_Attribute__c>  all Visible Columns
    */ 

    public void createColumnStructure(List<AxtriaSalesIQTM__Team_Instance_Object_Attribute__c> fetchAllVisibleColumns){
        columnsDataTable = '[';
        columnDefsOrderMap = new Map<String,Integer>();
        integer counter = 0;
        for(AxtriaSalesIQTM__Team_Instance_Object_Attribute__c objAttr : fetchAllVisibleColumns){
            columnsDataTable = columnsDataTable + '{ data: \'' + objAttr.AxtriaSalesIQTM__WrapperFieldMap__c +'\' , title: \''+objAttr.AxtriaSalesIQTM__Attribute_Display_Name__c+'\'},';
            columnDefsOrderMap.put(objAttr.AxtriaSalesIQTM__Attribute_API_Name__c,counter);
            counter++;
        }
        columnsDataTable = columnsDataTable.removeEnd(',');
        columnsDataTable = columnsDataTable + ']'; 
        columnsDataTable = JSON.serialize(columnsDataTable);
        columnsDataTable = columnsDataTable.substring(1, columnsDataTable.length()-1);
        system.debug('+++++++ columnsDataTable '+columnsDataTable);
        runQuery( soql,  selectedPosition,  selectedTeamInstance,  segmentToValueMap,  allFields);
    }

    public Integer maxTotalHCPs {get;set;}
    public Integer maxnexavarCalls {get;set;}
    public Integer maxstivargaCalls {get;set;}
    public Integer maxtotalCallsHCP {get;set;}
    public Integer percentageTotalHcps {get;set;}
    public Integer percentagenexavarCalls {get;set;}
    public Integer percentagestivargaCalls {get;set;}
    public Integer percentagetotalCallsHCP {get;set;}
    public boolean warningMetrics {get;set;}
    public Integer originaltotalCallsHCP {get;set;}
    public Integer originaltotalHCPs {get;set;}
    public Integer originalnexavarCalls {get;set;}
    public Integer originalstivargaCalls {get;set;} 
    public Integer originalF2fCalls  {get;set;}
    public Integer originalPhoneCalls  {get;set;}
    public Integer originalEmailCalls  {get;set;}
    public Integer originalRemoteCalls  {get;set;}
    public Integer originalWebinarCalls  {get;set;}

    public Integer updatedF2fCalls  {get;set;}
    public Integer updatedPhoneCalls  {get;set;}
    public Integer updatedEmailCalls  {get;set;}
    public Integer updatedRemoteCalls  {get;set;}
    public Integer updatedWebinarCalls  {get;set;}

    public Integer percentageF2fCalls  {get;set;}
    public Integer percentagePhoneCalls  {get;set;}
    public Integer percentageEmailCalls  {get;set;}
    public Integer percentageRemoteCalls  {get;set;}
    public Integer percentageWebinarCalls  {get;set;}
    public boolean mcCallPlan {get;set;}
    public string colorF2F{get;set;}
    public string colorPhone{get;set;}
    public string colorEmail{get;set;}
    public string colorRemote{get;set;}
    public string colorWebinar{get;set;} 
    public string colorTotalCalls{get;set;}

    /**
    * This method implements CIM Config i.e. populating metrics dynamically
    */
    public void cimConfig(){
        pieChartWrapper = new List<pieChartWrap>();
        if(teamInstanceName != 'S2_KAM'){
            warningMetrics = false;
            system.debug('Hey user position is '+selectedPosition); 
            system.debug('--selectedTeamInstance '+selectedTeamInstance);
            maxTotalHCPs = 0;
            maxtotalCallsHCP = 0;
            maxnexavarCalls = 0; 
            maxstivargaCalls = 0;
            List<AggregateResult> cpaggUpdated = [select count(id) countRecs from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c = :selectedPosition and AxtriaSalesIQTM__Team_Instance__c = :selectedTeamInstance and AxtriaSalesIQTM__isIncludedCallPlan__c = true];
            List<AggregateResult> cpaggOriginal = [select count(id) countRecs from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c = :selectedPosition and AxtriaSalesIQTM__Team_Instance__c = :selectedTeamInstance and AxtriaSalesIQTM__isAccountTarget__c = true];
            totalHCPs =  Integer.valueof(cpaggUpdated[0].get('countRecs'));
            originaltotalHCPs =  Integer.valueof(cpaggOriginal[0].get('countRecs'));
            Decimal orginalHCps = originaltotalHCPs;
            Decimal updateHCps = totalHCPs;
            if(originaltotalHCPs != 0){
                percentageTotalHcps = Integer.valueOf((updateHCps*100/orginalHCps));
            }else{
                percentageTotalHcps = 0;
            }

            if(percentageTotalHcps > 110 || percentageTotalHcps < 90){
                savelock = true;
                colorTotalHCPs = 'red';
            }else if( (percentageTotalHcps >= 90 && percentageTotalHcps < 95) || (percentageTotalHcps > 105 && percentageTotalHcps <= 110)){
                colorTotalHCPs = 'yellow';
                warningMetrics = true;
            }else if(percentageTotalHcps >= 95 && percentageTotalHcps <= 105){
                colorTotalHCPs = 'green';
            }
            AggregateResult originalDirectionCalls = [select sum(Calculated_TCF__c) calls from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__isAccountTarget__c = true and AxtriaSalesIQTM__Position__c = :selectedPosition and AxtriaSalesIQTM__Team_Instance__c = :selectedTeamInstance];
            AggregateResult originalDirectionCallsUpdated = [select sum(Proposed_TCF__c) calls from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__isIncludedCallPlan__c = true and AxtriaSalesIQTM__Position__c = :selectedPosition and AxtriaSalesIQTM__Team_Instance__c = :selectedTeamInstance];
            if(Integer.valueOf(originalDirectionCallsUpdated.get('calls'))!=null){
                totalCallsHCP = Integer.valueOf(originalDirectionCallsUpdated.get('calls'));
            }else{
                totalCallsHCP = 0;
            }

            if(Integer.valueOf(originalDirectionCalls.get('calls'))!=null){
                originaltotalCallsHCP = Integer.valueOf(originalDirectionCalls.get('calls'));
            }else{
                originaltotalCallsHCP = 0;
            }
            Decimal updateCallsHcps = totalCallsHCP;
            Decimal orginalCallscps = originaltotalCallsHCP;

            if(originaltotalCallsHCP != 0 && originaltotalCallsHCP != null){
                percentagetotalCallsHCP = Integer.valueOf((updateCallsHcps *100 /orginalCallscps));
            }else{
                percentagetotalCallsHCP = 0;
            }

            maxtotalCallsHCP = Integer.valueOf(orginalCallscps) * 2;
            if(percentagetotalCallsHCP > 110 || percentagetotalCallsHCP < 90){
                colortotalCallsHCP = 'red';
                savelock = true;
            }else if((percentagetotalCallsHCP >= 90 && percentagetotalCallsHCP < 95) || (percentagetotalCallsHCP > 105 && percentagetotalCallsHCP <= 110)){
                colortotalCallsHCP = 'yellow';  
                warningMetrics = true;
            }else if(percentagetotalCallsHCP >= 95 && percentagetotalCallsHCP <= 105){
                colortotalCallsHCP = 'green';
            }
            system.debug('+++++++++++++ In cim selected Position and team instance is '+selectedPosition+' '+selectedTeamInstance);
            AggregateResult aggAdded = [select count(id) countRecs from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__isAccountTarget__c = false and AxtriaSalesIQTM__isIncludedCallPlan__c = true and AxtriaSalesIQTM__Position__c = :selectedPosition and AxtriaSalesIQTM__Team_Instance__c = :selectedTeamInstance];
            phyAdded = Integer.valueof(aggAdded.get('countRecs'));
            AggregateResult aggDropped = [select count(id) countRecs from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__isAccountTarget__c = true and AxtriaSalesIQTM__isIncludedCallPlan__c = false and AxtriaSalesIQTM__Position__c = :selectedPosition and AxtriaSalesIQTM__Team_Instance__c = :selectedTeamInstance];
            phyDropped = Integer.valueof(aggDropped.get('countRecs'));
        }else{
            mcCallPlan = true;
            maxTotalHCPs = 0;
            maxtotalCallsHCP = 0;
            maxnexavarCalls = 0; 
            maxstivargaCalls = 0;
            AggregateResult aggTargetsOriginal = [select count(id) sumTargets, sum(Calculated_TCF__c) f2fCalls ,sum(AxtriaSalesIQTM__Picklist4_Metric__c) phoneCalls, sum(AxtriaSalesIQTM__Picklist5_Metric__c) emailCalls, sum(AxtriaSalesIQTM__Picklist5_Metric__c) remoteCalls, sum(AxtriaSalesIQTM__Picklist5_Metric__c) webinarCalls from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c = :selectedPosition and AxtriaSalesIQTM__Team_Instance__c = :selectedTeamInstance and AxtriaSalesIQTM__isAccountTarget__c = true];
            AggregateResult aggTargetsUpdated = [select count(id) sumTargets, sum(Proposed_TCF__c) f2fCalls ,sum(AxtriaSalesIQTM__Picklist4_Metric_Approved__c) phoneCalls, sum(AxtriaSalesIQTM__Picklist4_Metric_Approved__c) emailCalls, sum(AxtriaSalesIQTM__Picklist4_Metric_Approved__c) remoteCalls, sum(AxtriaSalesIQTM__Picklist4_Metric_Approved__c) webinarCalls from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c = :selectedPosition and AxtriaSalesIQTM__Team_Instance__c = :selectedTeamInstance and AxtriaSalesIQTM__isIncludedCallPlan__c = true];
            totalHCPs = Integer.valueOf(aggTargetsUpdated.get('sumTargets'));
            originaltotalHCPs = Integer.valueOf(aggTargetsOriginal.get('sumTargets'));
            Decimal ttotalHcps = totalHCPs;
            Decimal ooriginaltotalHCPs = originaltotalHCPs;

            if(ooriginaltotalHCPs != 0 && ooriginaltotalHCPs != null) {
                percentageTotalHcps =   Integer.valueOf((ttotalHcps/ooriginaltotalHCPs)*100);
            }else{
                percentageTotalHcps = 200;  
            }

            if(percentageTotalHcps > 110 || percentageTotalHcps < 90){
                savelock = true;
                colorTotalHCPs = 'red';
            }else if( (percentageTotalHcps >= 90 && percentageTotalHcps < 95) || (percentageTotalHcps > 105 && percentageTotalHcps <= 110)){
                colorTotalHCPs = 'yellow';  
                warningMetrics = true;
            }else if(percentageTotalHcps >= 95 && percentageTotalHcps <= 105){
                colorTotalHCPs = 'green';
            }
            totalCallsHCP = Integer.valueOf(aggTargetsUpdated.get('f2fCalls'))+Integer.valueOf(aggTargetsUpdated.get('phoneCalls'))+Integer.valueOf(aggTargetsUpdated.get('emailCalls')) +Integer.valueOf(aggTargetsUpdated.get('remoteCalls')) ;
            originaltotalCallsHCP = Integer.valueOf(aggTargetsOriginal.get('f2fCalls'))+Integer.valueOf(aggTargetsOriginal.get('phoneCalls'))+Integer.valueOf(aggTargetsOriginal.get('emailCalls')) +Integer.valueOf(aggTargetsOriginal.get('remoteCalls'));
            Decimal ttotalCallsHCP = totalCallsHCP;
            Decimal ooriginaltotalCallsHCP = originaltotalCallsHCP;
            if(ooriginaltotalCallsHCP != 0 && ooriginaltotalCallsHCP != null){
                percentagetotalCallsHCP = Integer.valueOf((ttotalCallsHCP/ooriginaltotalCallsHCP)*100);
            }else{
               percentagetotalCallsHCP = 200;  
            }

            if(percentagetotalCallsHCP > 110 || percentagetotalCallsHCP < 90){
                colortotalCallsHCP = 'red';
                savelock = true;
            }else if((percentagetotalCallsHCP >= 90 && percentagetotalCallsHCP < 95) || (percentagetotalCallsHCP > 105 && percentagetotalCallsHCP <= 110)){
                colortotalCallsHCP = 'yellow';  
                warningMetrics = true;
            }else if(percentagetotalCallsHCP >= 95 && percentagetotalCallsHCP <= 105){
                colortotalCallsHCP = 'green';
            }

            originalF2fCalls = Integer.valueOf(aggTargetsOriginal.get('f2fCalls'));
            originalPhoneCalls = Integer.valueOf(aggTargetsOriginal.get('phoneCalls'));
            originalEmailCalls = Integer.valueOf(aggTargetsOriginal.get('emailCalls'));
            originalRemoteCalls = Integer.valueOf(aggTargetsOriginal.get('remoteCalls'));
            originalWebinarCalls = Integer.valueOf(aggTargetsOriginal.get('webinarCalls'));
            updatedF2fCalls = Integer.valueOf(aggTargetsUpdated.get('f2fCalls'));
            updatedPhoneCalls = Integer.valueOf(aggTargetsUpdated.get('phoneCalls'));
            updatedEmailCalls = Integer.valueOf(aggTargetsUpdated.get('emailCalls'));
            updatedRemoteCalls = Integer.valueOf(aggTargetsUpdated.get('remoteCalls'));
            updatedWebinarCalls = Integer.valueOf(aggTargetsUpdated.get('webinarCalls'));
            Decimal originalCall = originalF2fCalls;
            Decimal updatedCall = updatedF2fCalls;

            if( originalCall != 0 ){
                percentageF2fCalls = Integer.valueof((updatedCall * 100)/originalCall);
            }else{
                percentageF2fCalls = 0;
            }

            if(percentageF2fCalls > 110 || percentageF2fCalls < 90){
                colorF2F = 'red';
                savelock = true;
            }else if((percentageF2fCalls >= 90 && percentageF2fCalls < 95) || (percentageF2fCalls > 105 && percentageF2fCalls <= 110)){
                colorF2F = 'yellow';  
                warningMetrics = true;
            }else if(percentageF2fCalls >= 95 && percentagetotalCallsHCP <= 105){
                colorF2F = 'green';
            }

            originalCall = originalPhoneCalls;
            updatedCall = updatedPhoneCalls;
            if( originalCall != 0 ){
                percentagePhoneCalls = Integer.valueof((updatedCall * 100)/originalCall);
            }else{
                percentagePhoneCalls = 0;
            }

            if(percentagePhoneCalls > 110 || percentagePhoneCalls < 90){
                colorPhone = 'red';
                savelock = true;
            }else if((percentagePhoneCalls >= 90 && percentagePhoneCalls < 95) || (percentagePhoneCalls > 105 && percentagePhoneCalls <= 110)){
                colorPhone = 'yellow';  
                warningMetrics = true;
            }else if(percentagePhoneCalls >= 95 && percentagePhoneCalls <= 105){
                colorPhone = 'green';
            }

            originalCall = originalEmailCalls;
            updatedCall = updatedEmailCalls;

            if( originalCall != 0 ){
                percentageEmailCalls = Integer.valueof((updatedCall * 100)/originalCall);
            }else{
                percentageEmailCalls = 0;
            }

            if(percentageEmailCalls > 110 || percentageEmailCalls < 90){
                colorEmail = 'red';
                savelock = true;
            }else if((percentageEmailCalls >= 90 && percentageEmailCalls < 95) || (percentageEmailCalls > 105 && percentageEmailCalls <= 110)){
                colorEmail = 'yellow';  
                warningMetrics = true;
            }else if(percentageEmailCalls >= 95 && percentageEmailCalls <= 105){
                colorEmail = 'green';
            }

            originalCall = originalRemoteCalls;
            updatedCall = updatedRemoteCalls;

            if( originalCall != 0 ){
                percentageRemoteCalls = Integer.valueof((updatedCall * 100)/originalCall);
            }else{
                percentageRemoteCalls = 0;
            }

            if(percentageRemoteCalls > 110 || percentageRemoteCalls < 90){
                colorRemote = 'red';
                savelock = true;
            }else if((percentageRemoteCalls >= 90 && percentageRemoteCalls < 95) || (percentageRemoteCalls > 105 && percentageRemoteCalls <= 110)){
                colorRemote = 'yellow';  
                warningMetrics = true;
            }else if(percentageRemoteCalls >= 95 && percentageRemoteCalls <= 105){
                colorRemote = 'green';
            }

            originalCall = originalWebinarCalls;
            updatedCall = updatedWebinarCalls;

            if( originalCall != 0 ){
                percentageWebinarCalls = Integer.valueof((updatedCall * 100)/originalCall);
            }else
            {
                percentageWebinarCalls = 0;
            }
            
            if(percentageWebinarCalls > 110 || percentageWebinarCalls < 90){
                colorWebinar = 'red';
                savelock = true;
            }else if((percentageWebinarCalls >= 90 && percentageWebinarCalls < 95) || (percentageWebinarCalls > 105 && percentageWebinarCalls <= 110)){
                colorWebinar = 'yellow';  
                warningMetrics = true;
            }else if(percentageWebinarCalls >= 95 && percentageWebinarCalls <= 105){
                colorWebinar = 'green';
            }

            AggregateResult aggAdded = [select count(id) countRecs from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__isAccountTarget__c = false and AxtriaSalesIQTM__isIncludedCallPlan__c = true and AxtriaSalesIQTM__Position__c = :selectedPosition and AxtriaSalesIQTM__Team_Instance__c = :selectedTeamInstance];
            phyAdded = Integer.valueof(aggAdded.get('countRecs'));

            AggregateResult aggDropped = [select count(id) countRecs from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__isAccountTarget__c = true and AxtriaSalesIQTM__isIncludedCallPlan__c = false and AxtriaSalesIQTM__Position__c = :selectedPosition and AxtriaSalesIQTM__Team_Instance__c = :selectedTeamInstance];
            phyDropped = Integer.valueof(aggDropped.get('countRecs'));

            pieChartWrapper.add(new pieChartWrap('F2F', updatedF2FCalls));
            pieChartWrapper.add(new pieChartWrap('Phone', updatedPhoneCalls));
            pieChartWrapper.add(new pieChartWrap('Email', updatedEmailCalls));
            pieChartWrapper.add(new pieChartWrap('Remote', updatedRemoteCalls));
       }
   }

    public void runQuery(string soql, string selectedPosition, string selectedTeamInstance, Map<String,String> segmentToValueMap, String allFieldsString){
        List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> phyDisplayList = new List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        List<phyWrapper> phyDisplayWrapper = new List<phyWrapper>();
        system.debug('++++++++++ sahil '+segmentToValueMap);
        soql = soql + ' and ((AxtriaSalesIQTM__isIncludedCallPlan__c = true and AxtriaSalesIQTM__isAccountTarget__c = false ) or (AxtriaSalesIQTM__isIncludedCallPlan__c = false and AxtriaSalesIQTM__isAccountTarget__c = true ))';
        system.debug('========================='+soql);
        phyDisplayList = Database.query(soql ); //fetches data of all physicians
        system.debug('++++++++++ mahajan  '+phyDisplayList);
        Set<String> allFields = new Set<String>(allFieldsString.split(','));
        if(phyDisplayList!=null && phyDisplayList.size()>0){
            system.debug('+++++++++++++++++++++ Size is '+phyDisplayList.size());
            for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : phyDisplayList ){
                phyDisplayWrapper.add(new phyWrapper(pacp,segmentToValueMap,allFields));
            }
        }
        phyDisplayList.clear();
        jsonStringField = JSON.serialize(phyDisplayWrapper);
        //return(phyDisplayWrapper);
    }

    public class phyWrapper{
        public string id                                {get;set;}
        public string accountID                         {get;set;}
        public string firstName                         {get;set;}
        public string lastName                          {get;set;}
        public string name                              {get;set;}
        public string status                            {get;set;}
        public string speciality                        {get;set;}
        public string accNum                            {get;set;}
        public string picklist2_Segment                 {get;set;}
        public string picklist1_Segment                 {get;set;}
        public string picklist2_Updated                 {get;set;}
        public string picklist1_Updated                 {get;set;}
        public string picklist3_Updated                 {get;set;}
       public string picklist2_Approved                {get;set;}
        public string picklist1_Approved                {get;set;}
        
        public Map<String,String> picklist1_UpdatedMap  {get;set;}
        public Map<String,String> picklist2_UpdatedMap  {get;set;}
        public Map<String,String> picklist3_UpdatedMap  {get;set;}
        public string isTarget                         {get;set;}
        public string city                              {get;set;}
        public string state                             {get;set;}
        public string zip                               {get;set;}
        public string segment6                          {get;set;}
        
        public string picklist4_Updated                 {get;set;}
        public string picklist5_Updated                 {get;set;}
        public string picklist6_Updated                 {get;set;}
        public string picklist7_Updated                 {get;set;}
        
        public string tempUpdatedSalesDirection         {get;set;}
        
        public string modifiedBy                        {get;set;}
        public string date1                             {get;set;}
        
        public phyWrapper(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp , Map<String,String> segmentToValueMap , Set<String> allFields)
        { 
            
            picklist1_UpdatedMap = new Map<String,String>();
            picklist2_UpdatedMap = new Map<String,String>();
            picklist3_UpdatedMap = new Map<String,String>();
            
            id                          =       pacp.ID;
               
            if(allFields.contains('AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__FirstName__c'))
            {
                if(pacp.AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__FirstName__c == null)
                {
                    firstName                   =       'Not Available';
                }
                else
                {
                    firstName                    =       pacp.AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__FirstName__c;
 //                   firstName                   =       pacp.Account__r.Name.split(' ')[0];
                }
            }
            
            if(allFields.contains('AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__LastName__c'))
            {           
                if(pacp.AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__LastName__c == null)
                {
                    lastName                    =       'Not Available';    
                }
                else
                {
                //    lastName                  = pacp.Account__r.Name.split(' ')[1];
                    lastName                    =       pacp.AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__LastName__c;
                }
            }           
            
            if(allFields.contains('AxtriaSalesIQTM__Account__c'))
            {
                accountID                   =       pacp.AxtriaSalesIQTM__Account__c;
            }
            
            if(allFields.contains('AxtriaSalesIQTM__Account__r.Name'))
            {
                name                        =       pacp.AxtriaSalesIQTM__Account__r.Name;
            }
            
            if(allFields.contains('AxtriaSalesIQTM__Change_Status__c'))
            { 
                status                      =       pacp.AxtriaSalesIQTM__Change_Status__c;
            }
            else
            {
                status='No Change';
            }
            
            if(allFields.contains('AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__Speciality__c') )
            {
                system.debug('+++++++++++++++ Speciality is '+ pacp.AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__Speciality__c);
                if(pacp.AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__Speciality__c != null && pacp.AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__Speciality__c != '')
                {
                    speciality                  =       pacp.AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__Speciality__c;
                }
                else
                {
                    speciality = 'Hospital';
                }
                
               
                
            }  

            accNum                      =       pacp.AxtriaSalesIQTM__Account__r.AccountNumber ;

             
            if(allFields.contains('AxtriaSalesIQTM__Picklist2_Segment__c'))
            {
                picklist2_Segment           =       pacp.AxtriaSalesIQTM__Picklist2_Segment__c;
                picklist2_Updated           =       pacp.AxtriaSalesIQTM__Picklist2_Updated__c;
             picklist2_Approved = ' ';
                tempUpdatedSalesDirection   =       picklist2_Updated;
                if(segmentToValueMap.containsKey('AxtriaSalesIQTM__Picklist2_Updated__c'))
                {
                    List<String> allVals = (segmentToValueMap.get('AxtriaSalesIQTM__Picklist2_Updated__c')).split(',');
            //       system.debug('+++++++++ Hey all Vals picklist2 is '+allVals);
                    
                    for(String str : allVals)
                    {
                        picklist2_UpdatedMap.put(str,str);
                    }
                }
            }
            
            if(allFields.contains('AxtriaSalesIQTM__Picklist1_Segment__c'))
            {
                picklist1_Segment           =       pacp.AxtriaSalesIQTM__Picklist1_Segment__c;
                picklist1_Updated           =       pacp.AxtriaSalesIQTM__Picklist1_Updated__c;
                picklist1_Approved          =       pacp.AxtriaSalesIQTM__Picklist1_Segment_Approved__c; 
                
                if(segmentToValueMap.containsKey('AxtriaSalesIQTM__Picklist1_Updated__c'))
                {
                    List<String> allVals = (segmentToValueMap.get('AxtriaSalesIQTM__Picklist1_Updated__c')).split(',');
                    
               //     system.debug('+++++++++ Hey all Vals picklist1 is '+allVals);
                    for(String str : allVals)
                    {
                        picklist1_UpdatedMap.put(str,str);
                    }
                }
            }
                        
            if(allFields.contains('AxtriaSalesIQTM__Picklist3_Updated__c'))
            {                       
                picklist3_Updated           =       pacp.AxtriaSalesIQTM__Picklist3_Updated__c; 
                if(segmentToValueMap.containsKey('AxtriaSalesIQTM__Picklist3_Updated__c'))
                {
                    List<String> allVals = (segmentToValueMap.get('AxtriaSalesIQTM__Picklist3_Updated__c')).split(',');
                    system.debug('+++++++++ Hey all Vals picklist3 is '+allVals);
                    
                    for(String str : allVals)
                    {
                        picklist3_UpdatedMap.put(str,str);
                    }
                }
            }
            segment6 = ' ';
            
            if(allFields.contains('AxtriaSalesIQTM__Picklist4_Metric_Updated__c'))
            {                       
                picklist4_Updated           =       String.valueof(pacp.AxtriaSalesIQTM__Picklist4_Metric_Updated__c); 
               
            }
            
            if(allFields.contains('AxtriaSalesIQTM__Picklist5_Metric_Approved__c'))
            {                       
                picklist5_Updated           =       String.valueof(pacp.AxtriaSalesIQTM__Picklist5_Metric_Approved__c); 
            }
            
            if(allFields.contains('AxtriaSalesIQTM__Picklist5_Metric_Approved__c'))
            {                       
                picklist6_Updated           =       String.valueof(pacp.AxtriaSalesIQTM__Picklist5_Metric_Approved__c); 
            }
            if(allFields.contains('AxtriaSalesIQTM__Picklist5_Metric_Approved__c'))
            {                       
                picklist7_Updated           =       String.valueof(pacp.AxtriaSalesIQTM__Picklist5_Metric_Approved__c); 
            }

            if(allFields.contains('AxtriaSalesIQTM__isIncludedCallPlan__c'))
            {
                if(pacp.AxtriaSalesIQTM__isIncludedCallPlan__c == true)
                isTarget                    =       'Added';
                else
                isTarget                    =       'Dropped';
            }
            
            if(allFields.contains('AxtriaSalesIQTM__Account__r.BillingCity'))
            {
                city                        =       pacp.AxtriaSalesIQTM__Account__r.BillingCity;
            }
            
            if(allFields.contains('AxtriaSalesIQTM__Account__r.BillingState'))
            {
                state                       =       pacp.AxtriaSalesIQTM__Account__r.BillingState;
            }
            
            if(allFields.contains('AxtriaSalesIQTM__Account__r.BillingPostalCode'))
            {
                zip                         =       pacp.AxtriaSalesIQTM__Account__r.BillingPostalCode;
            }           
            
            if(allFields.contains('LastModifiedBy.Name'))
            {
                system.debug('++++++++++ Last Modified By '+ pacp.LastModifiedBy.Name);
                if(pacp.LastModifiedBy.Name == null)
                {
                    modifiedBy = ' ';
                }
                else
                    modifiedBy = pacp.LastModifiedBy.Name;
            }
            
            if(allFields.contains('LastModifiedDate'))
            {
                system.debug('++++++++++ pacp.LastModifiedDate '+ pacp.LastModifiedDate);
                if(pacp.LastModifiedDate == null)
                {
                    date1= ' ';
                }
                else
                date1 = String.valueof(pacp.LastModifiedDate);
            }
 
            
        }
    } 
    
    public class pieChartWrap
    {
        string name {get;set;}
        string data1 {get;set;}
        
        public pieChartWrap(string name, Integer data1)
        {
            this.name = name;
            this.data1 = String.valueof(data1);
        }
    }
    
    
}