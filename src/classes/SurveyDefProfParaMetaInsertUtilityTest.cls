/*Author - Himanshu Tariyal(A0994)
Date : 16th January 2018*/
@isTest
private class SurveyDefProfParaMetaInsertUtilityTest 
{
    private static testMethod void firstTest() 
    {
        /*Create initial test data for all the objs reqd.*/
        AxtriaSalesIQTM__Organization_Master__c aom = new AxtriaSalesIQTM__Organization_Master__c(AxtriaSalesIQTM__Org_Level__c='Global',AxtriaSalesIQTM__Parent_Country_Level__c=true);
        insert aom;
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='USA',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active');
        insert country;
        
        /*Global_Question_ID_AZ__c gq = new Global_Question_ID_AZ__c(Name='1');
        insert gq;*/
        
       /* Cycle__c cycle = new Cycle__c(Name='Test Cycle',Country__c = country.id);
       insert cycle;*/
       
       Account acc = new Account(Name='test acc',Marketing_Code__c='EU',AccountNumber='123456');
       insert acc;
       
        /*Business_Unit__c bu = new Business_Unit__c(Name='Test BU Loopup');
        insert bu;*/
        
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='Test Team'/*,Business_Unit_Loopup__c=bu.id*/);
        team.AxtriaSalesIQTM__Country__c = country.id;
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU',AxtriaSalesIQTM__Team__c=team.id/*,Cycle__c=cycle.id*/);
        insert ti;

        Product_Catalog__c pc = new Product_Catalog__c(Name='Test Product',Team_Instance__c=ti.id,Veeva_External_ID__c = 'ProdId',Product_Code__c= 'ProdId',IsActive__c=true,Country_Lookup__c=country.id);
        insert pc;
        
        /*Brand_Team_Instance__c bti = new Brand_Team_Instance__c(Brand__c=pc.id,Team_Instance__c=ti.id);
        insert bti;*/
        
        /*Prepare Staging_Cust_Survey_Profiling__c data*/
        Staging_Cust_Survey_Profiling__c scsp = new Staging_Cust_Survey_Profiling__c();
        scsp.CurrencyIsoCode = 'USD';
        scsp.SURVEY_ID__c = '1';
        scsp.SURVEY_NAME__c = 'Methanol_pcp';
        scsp.BRAND_ID__c = 'ProdId';
        scsp.BRAND_NAME__c = 'Test Product';
        scsp.PARTY_ID__c = '123456';
        scsp.QUESTION_SHORT_TEXT__c = 'Question data';
        scsp.RESPONSE__c = '4';
        scsp.Team_Instance__c = ti.Name;
        scsp.QUESTION_ID__c = 1;
        insert scsp;
        
        Staging_Cust_Survey_Profiling__c scsp2 = new Staging_Cust_Survey_Profiling__c();
        scsp2.CurrencyIsoCode = 'USD';
        scsp2.SURVEY_ID__c = '2';
        scsp2.SURVEY_NAME__c = 'Methanol_pcp';
        scsp2.BRAND_ID__c = 'ProdId';
        scsp2.BRAND_NAME__c = 'Test Product';
        scsp2.PARTY_ID__c = '123456';
        scsp2.QUESTION_SHORT_TEXT__c = 'Question data 2';
        scsp2.RESPONSE__c = '5';
        scsp2.Team_Instance__c = ti.Name;
        scsp2.QUESTION_ID__c = 2;
        insert scsp2;

        System.test.startTest();
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        Survey_Def_Prof_Para_Meta_Insert_Utility batch = new Survey_Def_Prof_Para_Meta_Insert_Utility(ti.Name);
        Database.executeBatch(batch,2000);
        System.test.stopTest();
    }
    
    private static testMethod void secondTest()
    {
        String className = 'SurveyDefProfParaMetaInsertUtilityTest';
        /*Create initial test data for all the objs reqd.*/
        AxtriaSalesIQTM__Organization_Master__c aom = new AxtriaSalesIQTM__Organization_Master__c(AxtriaSalesIQTM__Org_Level__c='Global',AxtriaSalesIQTM__Parent_Country_Level__c=true);
        insert aom;
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='USD',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active');
        insert country;
        
       /* Cycle__c cycle = new Cycle__c(Name='Test Cycle',Country__c = country.id);
        insert cycle;
        */
        Account acc = new Account(Name='test acc',Marketing_Code__c='EU',AccountNumber='123456');
        insert acc;
        
        /*Business_Unit__c bu = new Business_Unit__c(Name='Test BU Loopup');
        insert bu;
        */
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='Test Team'/*,Business_Unit_Loopup__c=bu.id*/);
        team.AxtriaSalesIQTM__Country__c = country.id;
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU',AxtriaSalesIQTM__Team__c=team.id/*,Cycle__c=cycle.id*/);
        insert ti;

        Product_Catalog__c pc = new Product_Catalog__c(Name='Test Product',Team_Instance__c=ti.id,Veeva_External_ID__c = 'ProdId',Product_Code__c= 'ProdId',IsActive__c=true,Country_Lookup__c=country.id);
        insert pc;
        
        /*Brand_Team_Instance__c bti = new Brand_Team_Instance__c(Brand__c=pc.id,Team_Instance__c=ti.id);
        insert bti;
        */
        AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
        cr.AxtriaSalesIQTM__Team_Instance_ID__c = ti.Id;
        SnTDMLSecurityUtil.insertRecords(cr,className);
    
        /*Prepare Staging_Cust_Survey_Profiling__c data*/
        Staging_Cust_Survey_Profiling__c scsp = new Staging_Cust_Survey_Profiling__c();
        scsp.CurrencyIsoCode = 'USD';
        scsp.SURVEY_ID__c = '1';
        scsp.SURVEY_NAME__c = 'Methanol_pcp';
        scsp.BRAND_NAME__c = 'Test Product';
        scsp.QUESTION_SHORT_TEXT__c = 'Question data';
        scsp.RESPONSE__c = '4';
        scsp.Team_Instance__c = ti.Name;
        scsp.QUESTION_ID__c = 1;
        insert scsp;
        
        Staging_Cust_Survey_Profiling__c scsp2 = new Staging_Cust_Survey_Profiling__c();
        scsp2.CurrencyIsoCode = 'USD';
        scsp2.SURVEY_ID__c = '2';
        scsp2.SURVEY_NAME__c = 'Methanol_pcp';
        scsp2.BRAND_NAME__c = 'Test Product';
        scsp2.QUESTION_SHORT_TEXT__c = 'Question data 2';
        scsp2.RESPONSE__c = '5';
        scsp2.Team_Instance__c = ti.Name;
        scsp2.QUESTION_ID__c = 2;
        insert scsp2;

        System.test.startTest();
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        Survey_Def_Prof_Para_Meta_Insert_Utility batch = new Survey_Def_Prof_Para_Meta_Insert_Utility(ti.Name);
        Database.executeBatch(batch,2000);
        System.test.stopTest();
    }
    
    private static testMethod void thirdTest()
    {
        String className = 'SurveyDefProfParaMetaInsertUtilityTest';
        /*Create initial test data for all the objs reqd.*/
        AxtriaSalesIQTM__Organization_Master__c aom = new AxtriaSalesIQTM__Organization_Master__c(AxtriaSalesIQTM__Org_Level__c='Global',AxtriaSalesIQTM__Parent_Country_Level__c=true);
        insert aom;
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='USD',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active');
        insert country;
        
       /* Cycle__c cycle = new Cycle__c(Name='Test Cycle',Country__c = country.id);
        insert cycle;
        */
        Account acc = new Account(Name='test acc',Marketing_Code__c='EU',AccountNumber='123456');
        insert acc;
        
        /*Business_Unit__c bu = new Business_Unit__c(Name='Test BU Loopup');
        insert bu;
        */
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='Test Team'/*,Business_Unit_Loopup__c=bu.id*/);
        team.AxtriaSalesIQTM__Country__c = country.id;
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU',AxtriaSalesIQTM__Team__c=team.id/*,Cycle__c=cycle.id*/);
        insert ti;

        Product_Catalog__c pc = new Product_Catalog__c(Name='Test Product',Team_Instance__c=ti.id,Veeva_External_ID__c = 'ProdId',Product_Code__c= 'ProdId',IsActive__c=true,Country_Lookup__c=country.id);
        insert pc;
        
        /*Brand_Team_Instance__c bti = new Brand_Team_Instance__c(Brand__c=pc.id,Team_Instance__c=ti.id);
        insert bti;
        */
        AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
        cr.AxtriaSalesIQTM__Team_Instance_ID__c = ti.Id;
        SnTDMLSecurityUtil.insertRecords(cr,className);
    
        /*Prepare Staging_Cust_Survey_Profiling__c data*/
        Staging_Cust_Survey_Profiling__c scsp = new Staging_Cust_Survey_Profiling__c();
        scsp.CurrencyIsoCode = 'USD';
        scsp.SURVEY_ID__c = '1';
        scsp.SURVEY_NAME__c = 'Methanol_pcp';
        scsp.BRAND_NAME__c = 'Test Product';
        scsp.QUESTION_SHORT_TEXT__c = 'Question data';
        scsp.RESPONSE__c = '4';
        scsp.Team_Instance__c = ti.Name;
        scsp.QUESTION_ID__c = 1;
        insert scsp;
        
        Staging_Cust_Survey_Profiling__c scsp2 = new Staging_Cust_Survey_Profiling__c();
        scsp2.CurrencyIsoCode = 'USD';
        scsp2.SURVEY_ID__c = '2';
        scsp2.SURVEY_NAME__c = 'Methanol_pcp';
        scsp2.BRAND_NAME__c = 'Test Product';
        scsp2.QUESTION_SHORT_TEXT__c = 'Question data 2';
        scsp2.RESPONSE__c = '5';
        scsp2.Team_Instance__c = ti.Name;
        scsp2.QUESTION_ID__c = 2;
        insert scsp2;

        System.test.startTest();
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        Survey_Def_Prof_Para_Meta_Insert_Utility batch = new Survey_Def_Prof_Para_Meta_Insert_Utility(ti.Name,cr.Id);
        Database.executeBatch(batch,2000);
        System.test.stopTest();
    }
    
    private static testMethod void FourthTest()
    {
        String className = 'SurveyDefProfParaMetaInsertUtilityTest';
        /*Create initial test data for all the objs reqd.*/
        AxtriaSalesIQTM__Organization_Master__c aom = new AxtriaSalesIQTM__Organization_Master__c(AxtriaSalesIQTM__Org_Level__c='Global',AxtriaSalesIQTM__Parent_Country_Level__c=true);
        insert aom;
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='USD',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active');
        insert country;
        
       /* Cycle__c cycle = new Cycle__c(Name='Test Cycle',Country__c = country.id);
        insert cycle;
        */
        Account acc = new Account(Name='test acc',Marketing_Code__c='EU',AccountNumber='123456');
        insert acc;
        
        /*Business_Unit__c bu = new Business_Unit__c(Name='Test BU Loopup');
        insert bu;
        */
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='Test Team'/*,Business_Unit_Loopup__c=bu.id*/);
        team.AxtriaSalesIQTM__Country__c = country.id;
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU',AxtriaSalesIQTM__Team__c=team.id/*,Cycle__c=cycle.id*/);
        insert ti;

        Product_Catalog__c pc = new Product_Catalog__c(Name='Test Product',Team_Instance__c=ti.id,Veeva_External_ID__c = 'ProdId',Product_Code__c= 'ProdId',IsActive__c=true,Country_Lookup__c=country.id);
        insert pc;
        
        /*Brand_Team_Instance__c bti = new Brand_Team_Instance__c(Brand__c=pc.id,Team_Instance__c=ti.id);
        insert bti;
        */
        AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
        cr.AxtriaSalesIQTM__Team_Instance_ID__c = ti.Id;
        SnTDMLSecurityUtil.insertRecords(cr,className);
    
        /*Prepare Staging_Cust_Survey_Profiling__c data*/
        Staging_Cust_Survey_Profiling__c scsp = new Staging_Cust_Survey_Profiling__c();
        scsp.CurrencyIsoCode = 'USD';
        scsp.SURVEY_ID__c = '1';
        scsp.SURVEY_NAME__c = 'Methanol_pcp';
        scsp.BRAND_NAME__c = 'Test Product';
        scsp.QUESTION_SHORT_TEXT__c = 'Question data';
        scsp.RESPONSE__c = '4';
        scsp.Team_Instance__c = ti.Name;
        scsp.QUESTION_ID__c = 1;
        insert scsp;
        
        Staging_Cust_Survey_Profiling__c scsp2 = new Staging_Cust_Survey_Profiling__c();
        scsp2.CurrencyIsoCode = 'USD';
        scsp2.SURVEY_ID__c = '2';
        scsp2.SURVEY_NAME__c = 'Methanol_pcp';
        scsp2.BRAND_NAME__c = 'Test Product';
        scsp2.QUESTION_SHORT_TEXT__c = 'Question data 2';
        scsp2.RESPONSE__c = '5';
        scsp2.Team_Instance__c = ti.Name;
        scsp2.QUESTION_ID__c = 2;
        insert scsp2;

        System.test.startTest();
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        Survey_Def_Prof_Para_Meta_Insert_Utility batch = new Survey_Def_Prof_Para_Meta_Insert_Utility(ti.Name,'HCO',true);
        Database.executeBatch(batch,2000);
        System.test.stopTest();
    }
    
    private static testMethod void fifthTest()
    {
        String className = 'SurveyDefProfParaMetaInsertUtilityTest';
        /*Create initial test data for all the objs reqd.*/
        AxtriaSalesIQTM__Organization_Master__c aom = new AxtriaSalesIQTM__Organization_Master__c(AxtriaSalesIQTM__Org_Level__c='Global',AxtriaSalesIQTM__Parent_Country_Level__c=true);
        insert aom;
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='USD',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active');
        insert country;
        
       /* Cycle__c cycle = new Cycle__c(Name='Test Cycle',Country__c = country.id);
        insert cycle;
        */
        Account acc = new Account(Name='test acc',Marketing_Code__c='EU',AccountNumber='123456');
        insert acc;
        
        /*Business_Unit__c bu = new Business_Unit__c(Name='Test BU Loopup');
        insert bu;
        */
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='Test Team'/*,Business_Unit_Loopup__c=bu.id*/);
        team.AxtriaSalesIQTM__Country__c = country.id;
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU',AxtriaSalesIQTM__Team__c=team.id/*,Cycle__c=cycle.id*/);
        insert ti;

        Product_Catalog__c pc = new Product_Catalog__c(Name='Test Product',Team_Instance__c=ti.id,Veeva_External_ID__c = 'ProdId',Product_Code__c= 'ProdId',IsActive__c=true,Country_Lookup__c=country.id);
        insert pc;
        
        /*Brand_Team_Instance__c bti = new Brand_Team_Instance__c(Brand__c=pc.id,Team_Instance__c=ti.id);
        insert bti;
        */
        AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
        cr.AxtriaSalesIQTM__Team_Instance_ID__c = ti.Id;
        SnTDMLSecurityUtil.insertRecords(cr,className);
    
        /*Prepare Staging_Cust_Survey_Profiling__c data*/
        Staging_Cust_Survey_Profiling__c scsp = new Staging_Cust_Survey_Profiling__c();
        scsp.CurrencyIsoCode = 'USD';
        scsp.SURVEY_ID__c = '1';
        scsp.SURVEY_NAME__c = 'Methanol_pcp';
        scsp.BRAND_NAME__c = 'Test Product';
        scsp.QUESTION_SHORT_TEXT__c = 'Question data';
        scsp.RESPONSE__c = '4';
        scsp.Team_Instance__c = ti.Name;
        scsp.QUESTION_ID__c = 1;
        insert scsp;
        
        Staging_Cust_Survey_Profiling__c scsp2 = new Staging_Cust_Survey_Profiling__c();
        scsp2.CurrencyIsoCode = 'USD';
        scsp2.SURVEY_ID__c = '2';
        scsp2.SURVEY_NAME__c = 'Methanol_pcp';
        scsp2.BRAND_NAME__c = 'Test Product';
        scsp2.QUESTION_SHORT_TEXT__c = 'Question data 2';
        scsp2.RESPONSE__c = '5';
        scsp2.Team_Instance__c = ti.Name;
        scsp2.QUESTION_ID__c = 2;
        insert scsp2;

        System.test.startTest();
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        Survey_Def_Prof_Para_Meta_Insert_Utility batch = new Survey_Def_Prof_Para_Meta_Insert_Utility(ti.Name,true);
        Database.executeBatch(batch,2000);
        System.test.stopTest();
    }
    
    private static testMethod void sixthTest()
    {
        String className = 'SurveyDefProfParaMetaInsertUtilityTest';
        /*Create initial test data for all the objs reqd.*/
        AxtriaSalesIQTM__Organization_Master__c aom = new AxtriaSalesIQTM__Organization_Master__c(AxtriaSalesIQTM__Org_Level__c='Global',AxtriaSalesIQTM__Parent_Country_Level__c=true);
        insert aom;
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='USD',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active');
        insert country;
        
       /* Cycle__c cycle = new Cycle__c(Name='Test Cycle',Country__c = country.id);
        insert cycle;
        */
        Account acc = new Account(Name='test acc',Marketing_Code__c='EU',AccountNumber='123456');
        insert acc;
        
        /*Business_Unit__c bu = new Business_Unit__c(Name='Test BU Loopup');
        insert bu;
        */
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='Test Team'/*,Business_Unit_Loopup__c=bu.id*/);
        team.AxtriaSalesIQTM__Country__c = country.id;
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU',AxtriaSalesIQTM__Team__c=team.id/*,Cycle__c=cycle.id*/);
        insert ti;

        Product_Catalog__c pc = new Product_Catalog__c(Name='Test Product',Team_Instance__c=ti.id,Veeva_External_ID__c = 'ProdId',Product_Code__c= 'ProdId',IsActive__c=true,Country_Lookup__c=country.id);
        insert pc;
        
        /*Brand_Team_Instance__c bti = new Brand_Team_Instance__c(Brand__c=pc.id,Team_Instance__c=ti.id);
        insert bti;
        */
        AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
        cr.AxtriaSalesIQTM__Team_Instance_ID__c = ti.Id;
        SnTDMLSecurityUtil.insertRecords(cr,className);
    
        /*Prepare Staging_Cust_Survey_Profiling__c data*/
        Staging_Cust_Survey_Profiling__c scsp = new Staging_Cust_Survey_Profiling__c();
        scsp.CurrencyIsoCode = 'USD';
        scsp.SURVEY_ID__c = '1';
        scsp.SURVEY_NAME__c = 'Methanol_pcp';
        scsp.BRAND_NAME__c = 'Test Product';
        scsp.QUESTION_SHORT_TEXT__c = 'Question data';
        scsp.RESPONSE__c = '4';
        scsp.Team_Instance__c = ti.Name;
        scsp.QUESTION_ID__c = 1;
        insert scsp;
        
        Staging_Cust_Survey_Profiling__c scsp2 = new Staging_Cust_Survey_Profiling__c();
        scsp2.CurrencyIsoCode = 'USD';
        scsp2.SURVEY_ID__c = '2';
        scsp2.SURVEY_NAME__c = 'Methanol_pcp';
        scsp2.BRAND_NAME__c = 'Test Product';
        scsp2.QUESTION_SHORT_TEXT__c = 'Question data 2';
        scsp2.RESPONSE__c = '5';
        scsp2.Team_Instance__c = ti.Name;
        scsp2.QUESTION_ID__c = 2;
        insert scsp2;

        System.test.startTest();
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        Survey_Def_Prof_Para_Meta_Insert_Utility batch = new Survey_Def_Prof_Para_Meta_Insert_Utility(ti.Name,true,'test');
        Database.executeBatch(batch,2000);
        System.test.stopTest();
    }
    
    private static testMethod void sevenTest()
    {
        String className = 'SurveyDefProfParaMetaInsertUtilityTest';
        /*Create initial test data for all the objs reqd.*/
        AxtriaSalesIQTM__Organization_Master__c aom = new AxtriaSalesIQTM__Organization_Master__c(AxtriaSalesIQTM__Org_Level__c='Global',AxtriaSalesIQTM__Parent_Country_Level__c=true);
        insert aom;
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='USD',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active');
        insert country;
        
       /* Cycle__c cycle = new Cycle__c(Name='Test Cycle',Country__c = country.id);
        insert cycle;
        */
        Account acc = new Account(Name='test acc',Marketing_Code__c='EU',AccountNumber='123456');
        insert acc;
        
        /*Business_Unit__c bu = new Business_Unit__c(Name='Test BU Loopup');
        insert bu;
        */
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='Test Team'/*,Business_Unit_Loopup__c=bu.id*/);
        team.AxtriaSalesIQTM__Country__c = country.id;
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU',AxtriaSalesIQTM__Team__c=team.id/*,Cycle__c=cycle.id*/);
        insert ti;

        Product_Catalog__c pc = new Product_Catalog__c(Name='Test Product',Team_Instance__c=ti.id,Veeva_External_ID__c = 'ProdId',Product_Code__c= 'ProdId',IsActive__c=true,Country_Lookup__c=country.id);
        insert pc;
        
        /*Brand_Team_Instance__c bti = new Brand_Team_Instance__c(Brand__c=pc.id,Team_Instance__c=ti.id);
        insert bti;
        */
        AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
        cr.AxtriaSalesIQTM__Team_Instance_ID__c = ti.Id;
        SnTDMLSecurityUtil.insertRecords(cr,className);
    
        /*Prepare Staging_Cust_Survey_Profiling__c data*/
        Staging_Cust_Survey_Profiling__c scsp = new Staging_Cust_Survey_Profiling__c();
        scsp.CurrencyIsoCode = 'USD';
        scsp.SURVEY_ID__c = '1';
        scsp.SURVEY_NAME__c = 'Methanol_pcp';
        scsp.BRAND_NAME__c = 'Test Product';
        scsp.QUESTION_SHORT_TEXT__c = 'Question data';
        scsp.RESPONSE__c = '4';
        scsp.Team_Instance__c = ti.Name;
        scsp.QUESTION_ID__c = 1;
        insert scsp;
        
        Staging_Cust_Survey_Profiling__c scsp2 = new Staging_Cust_Survey_Profiling__c();
        scsp2.CurrencyIsoCode = 'USD';
        scsp2.SURVEY_ID__c = '2';
        scsp2.SURVEY_NAME__c = 'Methanol_pcp';
        scsp2.BRAND_NAME__c = 'Test Product';
        scsp2.QUESTION_SHORT_TEXT__c = 'Question data 2';
        scsp2.RESPONSE__c = '5';
        scsp2.Team_Instance__c = ti.Name;
        scsp2.QUESTION_ID__c = 2;
        insert scsp2;

        System.test.startTest();
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        Survey_Def_Prof_Para_Meta_Insert_Utility batch = new Survey_Def_Prof_Para_Meta_Insert_Utility(ti.Name+';'+pc.Name,'test');
        Database.executeBatch(batch,2000);
        System.test.stopTest();
    }
}