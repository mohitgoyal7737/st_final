/*
@author - Prince Richard Augustin (A2661)
@description - Test class to test ATL_Delete_Recs.
*/
@isTest
private class ATL_Delete_RecsTest { 
    static testMethod void testMethod1() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
            new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ATLDeltaJob')};
                Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__Speciality__c='Oncology';
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Name = 'USA';
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
        insert scen;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        insert teamins;
        
        Staging_ATL__c d = new Staging_ATL__c();
        d.Status__c = 'Updated';
        d.Territory_Old__c ='test';
        d.Territory__c = 'test';
        d.Country_Lookup__c = countr.id;
        insert d;
        
        List<String> allCountries1 = new List<String> ();
        allCountries1.add(countr.Id);
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ATL_Delete_RecsTest'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> SCHEDULERLOG_READ_FIELD = new List<String>{nameSpace+'Job_Type__c'};
                System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Scheduler_Log__c.SObjectType, SCHEDULERLOG_READ_FIELD, false));
            ATL_Delete_Recs obj=new ATL_Delete_Recs(allCountries1,1);
            obj.query = 'select id,Status__c,Account__c, External_ID_AZ__c, Axtria_Account_ID__c,Country__c, Territory__c,Territory_Old__c from Staging_ATL__c';
            Database.executeBatch(obj);
        }
        Test.stopTest();
    }
    static testMethod void testMethod2() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        //User loggedInUser = [select id,userrole.name from User where id=:UserInfo.getUserId()];
        
        
        insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
            new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=true,name = 'ATLDeltaJob')};
                Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__Speciality__c='Oncology';
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Name = 'USA';
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
        insert scen;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        insert teamins;
        Staging_ATL__c d = new Staging_ATL__c();
        d.Status__c = 'Updated';
        d.Territory_Old__c ='test';
        d.Territory__c = 'test';
        d.Country_Lookup__c = countr.id;
        insert d;
        
        List<String> allCountries1 = new List<String> ();
        allCountries1.add(countr.Id);
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ATL_Delete_RecsTest'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> SCHEDULERLOG_READ_FIELD = new List<String>{nameSpace+'Job_Type__c'};
                System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Scheduler_Log__c.SObjectType, SCHEDULERLOG_READ_FIELD, false));
            ATL_Delete_Recs obj=new ATL_Delete_Recs(allCountries1);
            obj.query = 'select id,Status__c,Account__c, External_ID_AZ__c, Axtria_Account_ID__c,Country__c, Territory__c,Territory_Old__c from Staging_ATL__c';
            Database.executeBatch(obj);
            
            
        }
        Test.stopTest();
    }
    
    static testMethod void testMethod3() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
            new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=true,name = 'ATLDeltaJob')};
                Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__Speciality__c='Oncology';
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Name = 'USA';
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
        insert scen;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        insert teamins;
        
        Staging_ATL__c d = new Staging_ATL__c();
        d.Status__c = 'Updated';
        d.Territory_Old__c ='test';
        d.Territory__c = 'test';
        d.Country_Lookup__c = countr.id;
        insert d;
        List<String> allCountries1 = new List<String> ();
        allCountries1.add(countr.Id);
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ATL_Delete_RecsTest'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> SCHEDULERLOG_READ_FIELD = new List<String>{nameSpace+'Job_Type__c'};
                System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Scheduler_Log__c.SObjectType, SCHEDULERLOG_READ_FIELD, false));
            ATL_Delete_Recs obj=new ATL_Delete_Recs(allCountries1,true);
            obj.query = 'select id,Status__c,Account__c, External_ID_AZ__c, Axtria_Account_ID__c,Country__c, Territory__c,Territory_Old__c from Staging_ATL__c';
            Database.executeBatch(obj);
            
            
        }
        Test.stopTest();
    }
    static testMethod void testMethod4() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
            new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=true,name = 'ATLDeltaJob')};
                Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__Speciality__c='Oncology';
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Name = 'USA';
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
        insert scen;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        insert teamins;
        
        Staging_ATL__c d = new Staging_ATL__c();
        d.Status__c = 'Updated';
        d.Territory_Old__c ='test';
        d.Territory__c = 'test';
        d.Country_Lookup__c = countr.id;
        insert d;
        Scheduler_Log__c activityLogrec = new Scheduler_Log__c();
        activityLogrec.Job_Status__c = 'Failed';
        //activityLogrec.Job_Type__c = 'ATL Delta Job';
        activityLogrec.Country__c = countr.Id;
        activityLogrec.Team_Instance__c = teamins.id;
        activityLogrec.Job_Name__c = 'ATL Delta Job';
        insert activityLogrec;
        List<String> allCountries1 = new List<String> ();
        allCountries1.add(countr.Id);
        Set<String> activitylogSet = new Set<String>();
        activitylogSet.add(activityLogrec.id);
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ATL_Delete_RecsTest'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> SCHEDULERLOG_READ_FIELD = new List<String>{nameSpace+'Job_Type__c'};
                System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Scheduler_Log__c.SObjectType, SCHEDULERLOG_READ_FIELD, false));
            ATL_Delete_Recs obj=new ATL_Delete_Recs(allCountries1,false,activitylogSet);
            obj.query = 'select id,Status__c,Account__c, External_ID_AZ__c, Axtria_Account_ID__c,Country__c, Territory__c,Territory_Old__c from Staging_ATL__c';
            Database.executeBatch(obj);
        }
        Test.stopTest();
    }
}