public with sharing class SummaryCtlr extends BusinessRule implements IBusinessRule{
    public List<Rule_Parameter__c> ruleParameters {get;set;}
    public List<Rule_Parameter__c> ruleParameters1;
    public Boolean displayPopup {get;set;}
    public String selectedParam{get;set;}
    public String selectedParam1{get;set;}
    public String selectedParameters{get;set;}
    public List<selectOption> selectValues{get;set;}
    public Map<String, List<Step__c>> ruleSteps {get;set;}
    public list<String> summaryPath{get;set;}
    public transient List<SLDSPageMessage> PageMessages{get;set;}
    public String ruleId {get;set;}

    public String pageName {get;set;}
    
    public SummaryCtlr() {
        try{
        init();
            ruleId = ApexPages.currentPage().getParameters().get('rid');
        selectedParameters = '';
        displayPopup = false;
        selectValues = new List<selectOption>();
        ruleParameters1 = new list<Rule_Parameter__c>();
        retUrl = '/apex/ComputeValues?mode=' +mode+'&rid='+ruleId; 
        /*Product Priority Placeholder ST-25*/
        
        if(productPriorityEnabled){
            summaryPath = new list<String>{'Compute Values', 'Compute Segment', 'Compute TCF', 'Compute Accessibility','Product Priority'};
        }
        else{
           summaryPath = new list<String>{'Compute Values', 'Compute Segment', 'Compute TCF', 'Compute Accessibility'}; 
        }
        
        //ruleParameters = [SELECT Id, Name, Parameter_Name__c FROM Rule_Parameter__c WHERE Measure_Master__c =:ruleObject.Id ORDER BY Name];
        try{
            ruleParameters = [SELECT Id, Name, Parameter_Name__c FROM Rule_Parameter__c WHERE Measure_Master__c =:ruleObject.Id WITH SECURITY_ENFORCED ORDER BY Name];
        }
            catch(Exception qe) 
        {
                PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }
        selectValues.add(new SelectOption('None','None'));
        list<Rule_Parameter__c> rptemp ;
        try{
            rptemp = [Select Id,Parameter__c,Parameter_Name__c, CustomerSegmentedField__c from Rule_Parameter__c where Measure_Master__c = :ruleObject.Id WITH SECURITY_ENFORCED];
        }
            catch(Exception qe) 
        {
                PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }
        for(Rule_Parameter__c Rp: rptemp){
            selectValues.add(new SelectOption(Rp.Parameter_Name__c,Rp.Parameter_Name__c));
        }
        ruleSteps = new Map<String, List<Step__c>>();
        for(String path : pathList){
            ruleSteps.put(path, new List<Step__c>());
        }
        list<Step__c> steptemp ;
        try{
            steptemp = [SELECT Id, Name, Sequence__c, UI_Location__c FROM Step__c WHERE Measure_Master__c =:ruleObject.Id WITH SECURITY_ENFORCED ORDER BY Sequence__c ];
        }
            catch(Exception qe) 
        {
                PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }
        for(Step__c step: steptemp){
            List<Step__c> steps = ruleSteps.get(step.UI_Location__c);
            steps.add(step);
            ruleSteps.put(step.UI_Location__c, steps);
            }
        }
        catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'ComputeValues',ruleId);
        } 
    }   

    public void save(){

    }
    
    public void hidePopup()
    {
        displayPopup = false;
    }

    public PageReference redirectToPageRefer()
    {
        try{
        SnTDMLSecurityUtil.printDebugMessage('+++++++++ Hey Page Name is '+ pageName);
        PageReference pref = new PageReference('/apex/'+pageName);
        pref.setRedirect(true);
        return pref;
        }
        catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'SummaryCtlr',ruleId);
            return null;
            } 
    }

    public PageReference saveandnext(){
    if(mode == 'Edit' || mode == 'New'){
        displayPopup = true;
        }
        else{
        displayPopup = false;
        }
  
       
  //      PageReference pref = new PageReference('/apex/Business_Rule_list');
  //      ruleObject.State__c = 'Ready';
  //      update ruleObject;
  //      return pref;
          return null;
    }
    
  /*  public PageReference SaveParamSaveAndNext(){
        save1();
        displayPopup = false;
        
    }*/
   
      public void saveFinalCopy(){
        try{
        list<Rule_Parameter__c> avalRuleParam;
        try{
            avalRuleParam = [SELECT Id, Parameter__c FROM Rule_Parameter__c WHERE Measure_Master__c =:ruleObject.Id WITH SECURITY_ENFORCED ORDER BY Name];
        }
            catch(Exception qe) 
        {
                PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }
        set<String> paramSet = new set<String>();
        for(Rule_Parameter__c rp: avalRuleParam){
            paramSet.add(rp.Parameter__c);
        }
        ruleParameters1 = new list<Rule_Parameter__c>();
        list<String> parameters = (list<String>)JSON.deserialize(selectedParameters, list<String>.class);
        SnTDMLSecurityUtil.printDebugMessage('--selectedParameters-- ' + parameters);
        for(String param: parameters){
            if(String.isNotBlank(param) && !paramSet.contains(param)){
                Rule_Parameter__c rp = new Rule_Parameter__c();
                rp.Measure_Master__c = ruleObject.Id;
                rp.Parameter__c = param;
                ruleParameters1.add(rp);
            }
        }
        //insert ruleParameters1;
        SnTDMLSecurityUtil.insertRecords(ruleParameters1, 'SummaryCtlr');
        SnTDMLSecurityUtil.printDebugMessage('selectValues---' + selectValues);
    } 
        catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'SummaryCtlr',ruleId);
            } 
    } 
   
   
   
    
    public PageReference save1(){
    SnTDMLSecurityUtil.printDebugMessage('---Inside Save---'  );
   // saveFinalCopy();

      try{
      List<Rule_Parameter__c> allRuleParamAll;
      try{
          allRuleParamAll = [Select Id,Parameter__c,Summary_Potential__c,Summary_Adoption__c,Parameter_Name__c,Parameter__r.name__c, CustomerSegmentedField__c from Rule_Parameter__c where Measure_Master__c = :ruleObject.Id WITH SECURITY_ENFORCED];
      }
          catch(Exception qe) 
      {
              PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
              SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
          SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
      }
      SnTDMLSecurityUtil.printDebugMessage('---Inside All Param---'  );
      for(Rule_Parameter__c Rp : allRuleParamAll)
      {
          rp.Summary_Adoption__c = false;
          rp.Summary_Potential__c = false;
      }

      //update allRuleParamAll;
      SnTDMLSecurityUtil.updateRecords(allRuleParamAll, 'SummaryCtlr');
      
      //List<Rule_Parameter__c> allRuleParam = [Select Id,Parameter__c,Summary_Potential__c,Summary_Adoption__c,Parameter_Name__c,Parameter__r.name__c, CustomerSegmentedField__c from Rule_Parameter__c where Measure_Master__c = :ruleObject.Id and (Parameter_Name__c=:selectedParam OR Parameter_Name__c=:selectedParam1) ];
      List<Rule_Parameter__c> allRuleParam;
      try{
          allRuleParam = [Select Id,Parameter__c,Summary_Potential__c,Summary_Adoption__c,Parameter_Name__c,Parameter__r.name__c, CustomerSegmentedField__c from Rule_Parameter__c where Measure_Master__c = :ruleObject.Id and (Parameter_Name__c=:selectedParam OR Parameter_Name__c=:selectedParam1) WITH SECURITY_ENFORCED];
      }
          catch(Exception qe) 
      {
              PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
              SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
          SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
      }
      Boolean flag = false;
    
      if(allRuleParam != null && allRuleParam.size() > 0)
      {
           for(Rule_Parameter__c Rp : allRuleParam)
            {
                //SnTDMLSecurityUtil.printDebugMessage('Parameter_Name ::' + rp.Parameter_Name__c == selectedParam);
                //SnTDMLSecurityUtil.printDebugMessage('Parameter_Name :: '+ rp.Parameter_Name__c == selectedParam1);
                if(rp.Parameter_Name__c == selectedParam)
                {
                       
                    rp.Summary_Adoption__c = true;
                        
                } 
                else if(rp.Parameter_Name__c == selectedParam1)
                {
                   
                    rp.Summary_Potential__c = true;
                }

            }
            //update allRuleParam;
            SnTDMLSecurityUtil.updateRecords(allRuleParam, 'SummaryCtlr');    
        
       } 
       else{
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please choose the required parameters'));
            PageMessages = SLDSPageMessage.add(PageMessages,'Please choose the required parameters','error');
      } 
        
  

     PageReference pref = new PageReference('/apex/Business_Rule_list');
     displayPopup = false;
     ruleObject.State__c = 'Ready';
     //update ruleObject;
      SnTDMLSecurityUtil.updateRecords(ruleObject, 'SummaryCtlr'); 
     return pref;

    }   
    
    catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'SummaryCtlr',ruleId);
            return null;
            }    
    }
}