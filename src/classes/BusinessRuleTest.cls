@isTest
public class BusinessRuleTest {
    static testMethod void testMethod1() {
        User loggedInUser = new User(id=UserInfo.getUserId());  
        
        Account acc= TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.MCCP_Enabled__c = true;
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.MCCP_Enabled__c = true;
        insert teamins;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        mmc.Channels__c = 'email';
        insert mmc;
        
        Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
        pp.name = 'testing';
        pp.Values__c = 'test,test';
        pp.Parameter_Type__c = '1';
        insert pp;
        
        Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
        insert rp;
        
        Compute_Master__c ccMaster = TestDataFactory.createComputeMaster(rp);
        ccMaster.Operator__c = 'ADD';
        ccMaster.Field_1__c = rp.Id;
        insert ccMaster;
        
        Grid_Master__c gMaster = TestDataFactory.gridMaster(countr);
        insert gMaster;
                
        Step__c s = TestDataFactory.step(ccMaster, gMaster, mmc, rp);
        s.Measure_Master__c = mmc.id;
        s.Step_Type__c = 'Multi Channel TCF Interactions';
        insert s;
        
        Grid_Details__c g = TestDataFactory.gridDetails(gMaster);
        insert g;
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        
        AxtriaSalesIQTM__User_Access_Permission__c u = TestDataFactory.createUserAccessPerm(pos, teamins, UserInfo.getUserId());
        u.AxtriaSalesIQTM__Position__c=pos.id;
        u.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        u.AxtriaSalesIQTM__User__c = loggedInUser.id;        
        insert u;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        insert positionAccountCallPlan;
        
        BU_Response__c bu = TestDataFactory.createBuResponse(posAccount,pcc,teamins,team,acc);
        insert bu;
        
        HCO_Parameters__c hcoParam = TestDataFactory.createHCOParameter(rp,mmc);
        hcoParam.Source_Rule__c = mmc.id;
        hcoParam.Destination_Rule__c = mmc.id;
        insert hcoParam;
        
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'BusinessRuleConstructCtlr_test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        Test.startTest();
        System.runAs(loggedInUser){
            List<String> MEASUREMASTER_READ_FIELD = new List<String>{nameSpace+'Country__c', 'Name'};
            SnT_FLS_SecurityUtil.checkRead(Measure_Master__c.SObjectType, MEASUREMASTER_READ_FIELD, false) ;
            List<String> WORKSPACE_READ_FIELD = new List<String>{'AxtriaSalesIQTM__Country__c', 'Name'};
            SnT_FLS_SecurityUtil.checkRead(AxtriaSalesIQTM__Workspace__c.SObjectType, WORKSPACE_READ_FIELD, false) ;
            List<String> TEAMINSTANCE_READ_FIELD = new List<String>{'AxtriaSalesIQTM__Country__c', 'Name', nameSpace+'IsHCOSegmentationEnabled__c'};
            SnT_FLS_SecurityUtil.checkRead(AxtriaSalesIQTM__Team_Instance__c.SObjectType, TEAMINSTANCE_READ_FIELD, false) ;
            List<String> PRODUCTCATALOG_READ_FIELD = new List<String>{nameSpace+'Team_Instance__c', 'Name',nameSpace+'isActive__c'};
            SnT_FLS_SecurityUtil.checkRead(Product_Catalog__c.SObjectType, PRODUCTCATALOG_READ_FIELD, false) ;

            BusinessRule obj=new BusinessRule();
            obj.selectedBuisnessUnit = teamins.id;
            obj.selStepType = 'Compute';
            //Test.setCurrentPageReference(new PageReference('Page.myPage'));
            System.currentPageReference().getParameters().put('mode', 'edit');
            System.currentPageReference().getParameters().put('rid',mmc.Id);
            obj.init();
            System.assert(obj.countryID != null,'Constructor Not Excecuted successfully');
            //System.assert(!obj.Buset.isEmpty(),'Assertion Error');
            //System.assert(!obj.cycles.isEmpty(),'Assertion Error');
            //System.assert(!obj.businessUnits.isEmpty(),'Assertion Error');
            //System.assert(!obj.brands.isEmpty(),'Assertion Error');
            //obj.save();
            //obj.saveAndNext();
            //obj.isRuleValid();
            //obj.fillLineOptions();
            obj.nextPage('Compute Values');
            obj.addNewStep();
            obj.fetchRuleParametersToDisplay();
            obj.updateRule('test','test1');
        }
        Test.stopTest();
    }
    
    static testMethod void testMethod2() {
        User loggedInUser = new User(id=UserInfo.getUserId());  
        
        Account acc= TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.MCCP_Enabled__c=true;    
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.MCCP_Enabled__c = true;
        insert teamins;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        pcc.Product_List__c = 'Test1;Test2';
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        mmc.Channels__c = 'test;test';
        mmc.Brand_Lookup__c = pcc.id;
        mmc.Stage__c = 'Basic Information';    
        insert mmc;
        
        Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
        pp.name = 'testing';
        pp.Values__c = 'test;test';
        pp.Parameter_Type__c = '1';
        insert pp;
        
        Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
        insert rp;
        
        Compute_Master__c ccMaster = TestDataFactory.createComputeMaster(rp);
        ccMaster.Operator__c = 'ADD';
        ccMaster.Field_1__c = rp.Id;
        insert ccMaster;
        
        Grid_Master__c gMaster = TestDataFactory.gridMaster(countr);
        insert gMaster;
                
        Step__c s = TestDataFactory.step(ccMaster, gMaster, mmc, rp);
        s.Step_Type__c = 'Matrix';
        s.UI_Location__c = 'Compute Values';    
        insert s;
        
        Grid_Details__c g = TestDataFactory.gridDetails(gMaster);
        insert g;
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        
        AxtriaSalesIQTM__User_Access_Permission__c u = TestDataFactory.createUserAccessPerm(pos, teamins, UserInfo.getUserId());
        u.AxtriaSalesIQTM__Position__c=pos.id;
        u.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        u.AxtriaSalesIQTM__User__c = loggedInUser.id;        
        insert u;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        insert positionAccountCallPlan;
        
        BU_Response__c bu = TestDataFactory.createBuResponse(posAccount,pcc,teamins,team,acc);
        insert bu;
        
        HCO_Parameters__c hcoParam = TestDataFactory.createHCOParameter(rp,mmc);
        hcoParam.Source_Rule__c = mmc.id;
        hcoParam.Destination_Rule__c = mmc.id;
        insert hcoParam;
        
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'BusinessRuleConstructCtlr_test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        
        System.runAs(loggedInUser){
            List<String> MEASUREMASTER_READ_FIELD = new List<String>{nameSpace+'Country__c', 'Name'};
            SnT_FLS_SecurityUtil.checkRead(Measure_Master__c.SObjectType, MEASUREMASTER_READ_FIELD, false) ;
            List<String> WORKSPACE_READ_FIELD = new List<String>{'AxtriaSalesIQTM__Country__c', 'Name'};
            SnT_FLS_SecurityUtil.checkRead(AxtriaSalesIQTM__Workspace__c.SObjectType, WORKSPACE_READ_FIELD, false) ;
            List<String> TEAMINSTANCE_READ_FIELD = new List<String>{'AxtriaSalesIQTM__Country__c', 'Name', nameSpace+'IsHCOSegmentationEnabled__c'};
            SnT_FLS_SecurityUtil.checkRead(AxtriaSalesIQTM__Team_Instance__c.SObjectType, TEAMINSTANCE_READ_FIELD, false) ;
            List<String> PRODUCTCATALOG_READ_FIELD = new List<String>{nameSpace+'Team_Instance__c', 'Name',nameSpace+'isActive__c'};
            SnT_FLS_SecurityUtil.checkRead(Product_Catalog__c.SObjectType, PRODUCTCATALOG_READ_FIELD, false) ;
            
            Test.startTest();
            BusinessRule obj=new BusinessRule();
            obj.selectedBuisnessUnit = teamins.id;
            obj.selStepType = 'Matrix';
            System.currentPageReference().getParameters().put('mode', 'new');
            System.currentPageReference().getParameters().put('rid',mmc.Id);
            obj.init();
            System.assert(obj.countryID != null,'Constructor Not Excecuted successfully');
            obj.stepEditId = s.Id;
            obj.selStepType = s.Step_Type__c;
            //System.assert(!obj.Buset.isEmpty(),'Assertion Error');
            //System.assert(!obj.cycles.isEmpty(),'Assertion Error');
            //System.assert(!obj.businessUnits.isEmpty(),'Assertion Error');
            //System.assert(!obj.brands.isEmpty(),'Assertion Error');
            //obj.save();
            //obj.saveAndNext();
            //obj.isRuleValid();
            //obj.fillLineOptions();
            obj.editStep();
            obj.addNewStep();
            obj.fetchRuleParametersToDisplay();
            obj.updateRule('Profiling Parameter','Basic Information');
        }
        Test.stopTest();
    }
    static testMethod void testMethod3() {
        User loggedInUser = new User(id=UserInfo.getUserId());  
        
        Account acc= TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.MCCP_Enabled__c=true;    
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.MCCP_Enabled__c = true;
        insert teamins;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        pcc.Product_List__c = 'Test1;Test2';
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        mmc.Channels__c = 'test;test';
        mmc.Brand_Lookup__c = pcc.id;
        mmc.Stage__c = 'Basic Information';    
        insert mmc;
        
        Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
        pp.name = 'testing';
        pp.Values__c = 'test;test';
        pp.Parameter_Type__c = '1';
        insert pp;
        
        Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
        insert rp;
        
        Compute_Master__c ccMaster = TestDataFactory.createComputeMaster(rp);
        ccMaster.Operator__c = 'ADD';
        ccMaster.Field_1__c = rp.Id;
        insert ccMaster;
        
        Grid_Master__c gMaster = TestDataFactory.gridMaster(countr);
        insert gMaster;
                
        Step__c s = TestDataFactory.step(ccMaster, gMaster, mmc, rp);
        s.Step_Type__c = 'Compute';
        s.UI_Location__c = 'Compute Values';    
        insert s;
        
        Grid_Details__c g = TestDataFactory.gridDetails(gMaster);
        insert g;
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        
        AxtriaSalesIQTM__User_Access_Permission__c u = TestDataFactory.createUserAccessPerm(pos, teamins, UserInfo.getUserId());
        u.AxtriaSalesIQTM__Position__c=pos.id;
        u.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        u.AxtriaSalesIQTM__User__c = loggedInUser.id;        
        insert u;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        insert positionAccountCallPlan;
        
        BU_Response__c bu = TestDataFactory.createBuResponse(posAccount,pcc,teamins,team,acc);
        insert bu;
        
        HCO_Parameters__c hcoParam = TestDataFactory.createHCOParameter(rp,mmc);
        hcoParam.Source_Rule__c = mmc.id;
        hcoParam.Destination_Rule__c = mmc.id;
        insert hcoParam;
        
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'BusinessRuleConstructCtlr_test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        
        System.runAs(loggedInUser){
            List<String> MEASUREMASTER_READ_FIELD = new List<String>{nameSpace+'Country__c', 'Name'};
            SnT_FLS_SecurityUtil.checkRead(Measure_Master__c.SObjectType, MEASUREMASTER_READ_FIELD, false) ;
            List<String> WORKSPACE_READ_FIELD = new List<String>{'AxtriaSalesIQTM__Country__c', 'Name'};
            SnT_FLS_SecurityUtil.checkRead(AxtriaSalesIQTM__Workspace__c.SObjectType, WORKSPACE_READ_FIELD, false) ;
            List<String> TEAMINSTANCE_READ_FIELD = new List<String>{'AxtriaSalesIQTM__Country__c', 'Name', nameSpace+'IsHCOSegmentationEnabled__c'};
            SnT_FLS_SecurityUtil.checkRead(AxtriaSalesIQTM__Team_Instance__c.SObjectType, TEAMINSTANCE_READ_FIELD, false) ;
            List<String> PRODUCTCATALOG_READ_FIELD = new List<String>{nameSpace+'Team_Instance__c', 'Name',nameSpace+'isActive__c'};
            SnT_FLS_SecurityUtil.checkRead(Product_Catalog__c.SObjectType, PRODUCTCATALOG_READ_FIELD, false) ;
            
            Test.startTest();
            BusinessRule obj=new BusinessRule();
            obj.selectedBuisnessUnit = teamins.id;
            obj.selStepType = 'Matrix';
            System.currentPageReference().getParameters().put('mode', 'new');
            System.currentPageReference().getParameters().put('rid',mmc.Id);
            obj.init();
            System.assert(obj.countryID != null,'Constructor Not Excecuted successfully');
            obj.stepEditId = s.Id;
            obj.selStepType = s.Step_Type__c;
            //System.assert(!obj.Buset.isEmpty(),'Assertion Error');
            //System.assert(!obj.cycles.isEmpty(),'Assertion Error');
            //System.assert(!obj.businessUnits.isEmpty(),'Assertion Error');
            //System.assert(!obj.brands.isEmpty(),'Assertion Error');
            //obj.save();
            //obj.saveAndNext();
            //obj.isRuleValid();
            //obj.fillLineOptions();
            obj.editStep();
            obj.addNewStep();
            obj.fetchRuleParametersToDisplay();
            obj.updateRule('Profiling Parameter','Basic Information');
        }
        Test.stopTest();
    }
    static testMethod void testMethod4() {
        User loggedInUser = new User(id=UserInfo.getUserId());  
        
        Account acc= TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.MCCP_Enabled__c=true;    
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.MCCP_Enabled__c = true;
        insert teamins;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        pcc.Product_List__c = 'Test1;Test2';
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        mmc.Channels__c = 'test;test';
        mmc.Brand_Lookup__c = pcc.id;
        mmc.Stage__c = 'Basic Information';    
        insert mmc;
        
        Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
        pp.name = 'testing';
        pp.Values__c = 'test;test';
        pp.Parameter_Type__c = '1';
        insert pp;
        
        Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
        insert rp;
        
        Compute_Master__c ccMaster = TestDataFactory.createComputeMaster(rp);
        ccMaster.Operator__c = 'ADD';
        ccMaster.Field_1__c = rp.Id;
        insert ccMaster;
        
        Grid_Master__c gMaster = TestDataFactory.gridMaster(countr);
        insert gMaster;
                
        Step__c s = TestDataFactory.step(ccMaster, gMaster, mmc, rp);
        s.Step_Type__c = 'Multi Channel TCF Interactions';
        s.UI_Location__c = 'Compute Values';    
        insert s;
        
        Grid_Details__c g = TestDataFactory.gridDetails(gMaster);
        insert g;
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        
        AxtriaSalesIQTM__User_Access_Permission__c u = TestDataFactory.createUserAccessPerm(pos, teamins, UserInfo.getUserId());
        u.AxtriaSalesIQTM__Position__c=pos.id;
        u.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        u.AxtriaSalesIQTM__User__c = loggedInUser.id;        
        insert u;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        insert positionAccountCallPlan;
        
        BU_Response__c bu = TestDataFactory.createBuResponse(posAccount,pcc,teamins,team,acc);
        insert bu;
        
        HCO_Parameters__c hcoParam = TestDataFactory.createHCOParameter(rp,mmc);
        hcoParam.Source_Rule__c = mmc.id;
        hcoParam.Destination_Rule__c = mmc.id;
        insert hcoParam;
        
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'BusinessRuleConstructCtlr_test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        
        System.runAs(loggedInUser){
            List<String> MEASUREMASTER_READ_FIELD = new List<String>{nameSpace+'Country__c', 'Name'};
            SnT_FLS_SecurityUtil.checkRead(Measure_Master__c.SObjectType, MEASUREMASTER_READ_FIELD, false) ;
            List<String> WORKSPACE_READ_FIELD = new List<String>{'AxtriaSalesIQTM__Country__c', 'Name'};
            SnT_FLS_SecurityUtil.checkRead(AxtriaSalesIQTM__Workspace__c.SObjectType, WORKSPACE_READ_FIELD, false) ;
            List<String> TEAMINSTANCE_READ_FIELD = new List<String>{'AxtriaSalesIQTM__Country__c', 'Name', nameSpace+'IsHCOSegmentationEnabled__c'};
            SnT_FLS_SecurityUtil.checkRead(AxtriaSalesIQTM__Team_Instance__c.SObjectType, TEAMINSTANCE_READ_FIELD, false) ;
            List<String> PRODUCTCATALOG_READ_FIELD = new List<String>{nameSpace+'Team_Instance__c', 'Name',nameSpace+'isActive__c'};
            SnT_FLS_SecurityUtil.checkRead(Product_Catalog__c.SObjectType, PRODUCTCATALOG_READ_FIELD, false) ;
            
            Test.startTest();
            BusinessRule obj=new BusinessRule();
            obj.selectedBuisnessUnit = teamins.id;
            obj.selStepType = 'Matrix';
            System.currentPageReference().getParameters().put('mode', 'new');
            System.currentPageReference().getParameters().put('rid',mmc.Id);
            obj.init();
            System.assert(obj.countryID != null,'Constructor Not Excecuted successfully');
            obj.stepEditId = s.Id;
            obj.selStepType = s.Step_Type__c;
            //System.assert(!obj.Buset.isEmpty(),'Assertion Error');
            //System.assert(!obj.cycles.isEmpty(),'Assertion Error');
            //System.assert(!obj.businessUnits.isEmpty(),'Assertion Error');
            //System.assert(!obj.brands.isEmpty(),'Assertion Error');
            //obj.save();
            //obj.saveAndNext();
            //obj.isRuleValid();
            //obj.fillLineOptions();
            obj.editStep();
            obj.addNewStep();
            obj.fetchRuleParametersToDisplay();
            obj.updateRule('Profiling Parameter','Basic Information');
        }
        Test.stopTest();
    }
    static testMethod void testMethod5() {
        User loggedInUser = new User(id=UserInfo.getUserId());  
        
        Account acc= TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.MCCP_Enabled__c=true;    
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.MCCP_Enabled__c = true;
        insert teamins;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        pcc.Product_List__c = 'Test1;Test2';
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        mmc.Channels__c = 'test;test';
        mmc.Brand_Lookup__c = pcc.id;
        mmc.Stage__c = 'Basic Information';    
        insert mmc;
        
        Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
        pp.name = 'testing';
        pp.Values__c = 'test;test';
        pp.Parameter_Type__c = '1';
        insert pp;
        
        Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
        insert rp;
        
        Compute_Master__c ccMaster = TestDataFactory.createComputeMaster(rp);
        ccMaster.Operator__c = 'ADD';
        ccMaster.Field_1__c = rp.Id;
        insert ccMaster;
        
        Grid_Master__c gMaster = TestDataFactory.gridMaster(countr);
        insert gMaster;
                
        Step__c s = TestDataFactory.step(ccMaster, gMaster, mmc, rp);
        s.Step_Type__c = 'Quantile';
        s.UI_Location__c = 'Compute Values'; 
        s.Sequence__c = 20;
        s.Modelling_Type__c = 'test';
        insert s;
        
        rp.Step__c = s.id;
        update rp;
        
        Grid_Details__c g = TestDataFactory.gridDetails(gMaster);
        insert g;
        
        Step__c s1 = TestDataFactory.step(ccMaster, gMaster, mmc, rp);
        s1.Step_Type__c = 'Quantile';
        s1.UI_Location__c = 'Compute Values'; 
        s1.Sequence__c = 25;
        s1.Modelling_Type__c = 'test';
        insert s1;
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        
        AxtriaSalesIQTM__User_Access_Permission__c u = TestDataFactory.createUserAccessPerm(pos, teamins, UserInfo.getUserId());
        u.AxtriaSalesIQTM__Position__c=pos.id;
        u.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        u.AxtriaSalesIQTM__User__c = loggedInUser.id;        
        insert u;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        insert positionAccountCallPlan;
        
        BU_Response__c bu = TestDataFactory.createBuResponse(posAccount,pcc,teamins,team,acc);
        insert bu;
        
        HCO_Parameters__c hcoParam = TestDataFactory.createHCOParameter(rp,mmc);
        hcoParam.Source_Rule__c = mmc.id;
        hcoParam.Destination_Rule__c = mmc.id;
        insert hcoParam;
        
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'BusinessRuleConstructCtlr_test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        
        System.runAs(loggedInUser){
            List<String> MEASUREMASTER_READ_FIELD = new List<String>{nameSpace+'Country__c', 'Name'};
            SnT_FLS_SecurityUtil.checkRead(Measure_Master__c.SObjectType, MEASUREMASTER_READ_FIELD, false) ;
            List<String> WORKSPACE_READ_FIELD = new List<String>{'AxtriaSalesIQTM__Country__c', 'Name'};
            SnT_FLS_SecurityUtil.checkRead(AxtriaSalesIQTM__Workspace__c.SObjectType, WORKSPACE_READ_FIELD, false) ;
            List<String> TEAMINSTANCE_READ_FIELD = new List<String>{'AxtriaSalesIQTM__Country__c', 'Name', nameSpace+'IsHCOSegmentationEnabled__c'};
            SnT_FLS_SecurityUtil.checkRead(AxtriaSalesIQTM__Team_Instance__c.SObjectType, TEAMINSTANCE_READ_FIELD, false) ;
            List<String> PRODUCTCATALOG_READ_FIELD = new List<String>{nameSpace+'Team_Instance__c', 'Name',nameSpace+'isActive__c'};
            SnT_FLS_SecurityUtil.checkRead(Product_Catalog__c.SObjectType, PRODUCTCATALOG_READ_FIELD, false) ;
            
            Test.startTest();
            BusinessRule obj=new BusinessRule();
            obj.selectedBuisnessUnit = teamins.id;
            obj.selStepType = 'Matrix';
            System.currentPageReference().getParameters().put('mode', 'new');
            System.currentPageReference().getParameters().put('rid',mmc.Id);
            obj.init();
            System.assert(obj.countryID != null,'Constructor Not Excecuted successfully');
            obj.stepEditId = s.Id;
            obj.selStepType = s.Step_Type__c;
            //System.assert(!obj.Buset.isEmpty(),'Assertion Error');
            //System.assert(!obj.cycles.isEmpty(),'Assertion Error');
            //System.assert(!obj.businessUnits.isEmpty(),'Assertion Error');
            //System.assert(!obj.brands.isEmpty(),'Assertion Error');
            //obj.save();
            //obj.saveAndNext();
            //obj.isRuleValid();
            //obj.fillLineOptions();
            obj.editStep();
            obj.addNewStep();
            obj.fetchRuleParametersToDisplay();
            obj.updateRule('Profiling Parameter','Basic Information');
            obj.clearComputeValues();
            obj.clearComputeValues_2();
            obj.showStepHelpPopup();
            obj.hideStepHelpPopup();
            
            BusinessRule.StepClassInnerWrapper wr1=new BusinessRule.StepClassInnerWrapper(s.Name,integer.valueOf(s.Sequence__c));
            List<BusinessRule.StepClassInnerWrapper> innerWrapperList = new List<BusinessRule.StepClassInnerWrapper>();
            innerWrapperList.add(new BusinessRule.StepClassInnerWrapper(s.Name,integer.valueOf(s.Sequence__c)));
            map<String,List<BusinessRule.StepClassInnerWrapper>> testMap = new map<String,List<BusinessRule.StepClassInnerWrapper>>();
            testMap.put('Test',innerWrapperList);
            BusinessRule.StepClassWrapperNew wr2 = new BusinessRule.StepClassWrapperNew(true,testMap);
            BusinessRule.checkIsStepDeletableNew(s.id);
            BusinessRule.checkStepDeleted(s.id);
            obj.currentStepID = s.id;
            obj.deleteStep();
            //obj.validateExpression();
            //obj.validateExpressionBeforeSave();
        }
        Test.stopTest();
    }
    
    static testMethod void testMethod6() {
        User loggedInUser = new User(id=UserInfo.getUserId());  
        
        Account acc= TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        
        Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
        pp.name = 'testing';
        pp.Values__c = 'test,test';
        pp.Parameter_Type__c = '1';
        insert pp;
        
        Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
        insert rp;
        
        Compute_Master__c ccMaster = TestDataFactory.createComputeMaster(rp);
        ccMaster.Operator__c = 'ADD';
        ccMaster.Field_1__c = rp.Id;
        insert ccMaster;
        
        Grid_Master__c gMaster = TestDataFactory.gridMaster(countr);
        insert gMaster;
                
        Step__c s = TestDataFactory.step(ccMaster, gMaster, mmc, rp);
        s.Step_Type__c = 'Quantile';
        s.Measure_Master__c = mmc.id;
        s.Sequence__c = 22;
        s.UI_Location__c = 'Compute Segment';
        insert s;
        
        Step__c s1 = TestDataFactory.step(ccMaster, gMaster, mmc, rp);
        s1.Step_Type__c = 'Quantile';
        s1.Measure_Master__c = mmc.id;
        s1.Sequence__c = 15;
        s1.UI_Location__c = 'Compute Segment';
        insert s1;
        
        Grid_Details__c g = TestDataFactory.gridDetails(gMaster);
        insert g;
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        
        AxtriaSalesIQTM__User_Access_Permission__c u = TestDataFactory.createUserAccessPerm(pos, teamins, UserInfo.getUserId());
        u.AxtriaSalesIQTM__Position__c=pos.id;
        u.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        u.AxtriaSalesIQTM__User__c = loggedInUser.id;        
        insert u;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        insert positionAccountCallPlan;
        
        BU_Response__c bu = TestDataFactory.createBuResponse(posAccount,pcc,teamins,team,acc);
        insert bu;
        
        HCO_Parameters__c hcoParam = TestDataFactory.createHCOParameter(rp,mmc);
        hcoParam.Source_Rule__c = mmc.id;
        hcoParam.Destination_Rule__c = mmc.id;
        insert hcoParam;
        
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'BusinessRuleConstructCtlr_test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        Test.startTest();
        System.runAs(loggedInUser){
            List<String> MEASUREMASTER_READ_FIELD = new List<String>{nameSpace+'Country__c', 'Name'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Measure_Master__c.SObjectType, MEASUREMASTER_READ_FIELD, false));
            List<String> WORKSPACE_READ_FIELD = new List<String>{'AxtriaSalesIQTM__Country__c', 'Name'};
            SnT_FLS_SecurityUtil.checkRead(AxtriaSalesIQTM__Workspace__c.SObjectType, WORKSPACE_READ_FIELD, false) ;
            List<String> TEAMINSTANCE_READ_FIELD = new List<String>{'AxtriaSalesIQTM__Country__c', 'Name', nameSpace+'IsHCOSegmentationEnabled__c'};
            SnT_FLS_SecurityUtil.checkRead(AxtriaSalesIQTM__Team_Instance__c.SObjectType, TEAMINSTANCE_READ_FIELD, false) ;
            List<String> PRODUCTCATALOG_READ_FIELD = new List<String>{nameSpace+'Team_Instance__c', 'Name',nameSpace+'isActive__c'};
            SnT_FLS_SecurityUtil.checkRead(Product_Catalog__c.SObjectType, PRODUCTCATALOG_READ_FIELD, false) ;

            BusinessRule obj=new BusinessRule();
            obj.step = s;
            obj.ruleObject = mmc;
            
            //System.assert(obj.countryID != null,'Constructor Not Excecuted successfully');
            //System.assert(!obj.Buset.isEmpty(),'Assertion Error');
            //System.assert(!obj.cycles.isEmpty(),'Assertion Error');
            //System.assert(!obj.businessUnits.isEmpty(),'Assertion Error');
            //System.assert(!obj.brands.isEmpty(),'Assertion Error');
            //obj.save();
            //obj.saveAndNext();
            //obj.isRuleValid();
            //obj.fillLineOptions();
            obj.validateStep('Compute Segment');
        }
        Test.stopTest();
    }
}