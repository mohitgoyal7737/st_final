@isTest
public class GenerateCRReport_Test 
{
    @istest static void GenerateCRReport_Test()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__AccountType__c ='HCP';
        insert acc;

        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'ONCO';
        insert team;

        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        insert scen;

        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        insert teamins;

        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;

        AxtriaSalesIQTM__Position__c pos1 = TestDataFactory.createPosition(team,teamins);
        pos1.AxtriaSalesIQTM__Client_Position_Code__c = 'N003';
        pos1.AxtriaSalesIQTM__Inactive__c = false;
        pos1.AxtriaSalesIQTM__Team_iD__c = team.Id;
        pos1.AxtriaSalesIQTM__Team_Instance__c = teamins.Id;
        pos1.AXTRIASALESIQTM__HIERARCHY_LEVEL__C = '2';
        pos1.AxtriaSalesIQTM__Position_Type__c = 'Nation';
        insert pos1;

        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__Client_Position_Code__c = 'T003';
        pos.AxtriaSalesIQTM__Inactive__c = false;
        pos.AxtriaSalesIQTM__Team_iD__c = team.Id;
        pos.AxtriaSalesIQTM__Parent_Position__c = pos1.Id;
        pos.AxtriaSalesIQTM__Team_Instance__c = teamins.Id;
        pos.AXTRIASALESIQTM__HIERARCHY_LEVEL__C = '1';
        pos.AxtriaSalesIQTM__Position_Type__c = 'Territory';
        insert pos;

        AxtriaSalesIQTM__User_Access_Permission__c u = TestDataFactory.createUserAccessPerm(pos, teamins, UserInfo.getUserId());
        u.AxtriaSalesIQTM__Position__c=pos1.id;
        u.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        u.AxtriaSalesIQTM__User__c = loggedInUser.id;        
        insert u;

        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        BU_Response__c bu = TestDataFactory.createBuResponse(posAccount,pcc,teamins,team,acc);
        insert bu;
        List<AxtriaSalesIQTM__Position__c> allPositionsListtemp =  new List<AxtriaSalesIQTM__Position__c> ();
        allPositionsListtemp.add(pos);

        AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
        cr.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.Id;
        cr.AxtriaSalesIQTM__Destination_Position__c = pos.Id;
        cr.AxtriaSalesIQTM__Request_Type_Change__c = 'Call Plan Change';
        cr.AxtriaSalesIQTM__Status__c = 'Pending';
        insert cr;

        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            ApexPages.currentPage().getParameters().put('buSelectedName',teamins.Name);
            GenerateCRReport obj = new GenerateCRReport();
        }
        Test.stopTest();
    }
}