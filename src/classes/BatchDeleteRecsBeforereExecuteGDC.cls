/*Author - Himanshu Tariyal(A0994)
Date : 16th January 2018*/
global with sharing class BatchDeleteRecsBeforereExecuteGDC implements Database.Batchable<sObject>, Database.Stateful {
    public String query;
    public String ruleId;
    public String whereClause;
    public Set<String> allMatrices;
    public Set<String> allMatricesGMC;


    global BatchDeleteRecsBeforereExecuteGDC(String ruleId, String whereClause) 
    {   

        allMatrices = new Set<String>();
        allMatricesGMC = new Set<String>();

        this.ruleId = ruleId;
        this.whereClause = whereClause;
        List <Step__c> allRequiredSteps;
        try
        {
            allRequiredSteps = [SELECT Id, Matrix__c, Matrix__r.Name, Matrix__r.Brand__c FROM Step__c WHERE Measure_Master__c =:ruleId 
                                                AND  Step_Type__c = 'Matrix' WITH SECURITY_ENFORCED];
        }
        catch(Exception e)
        {
            SnTDMLSecurityUtil.printDebugMessage(e.getMessage());
        }

        String gridMasterId = '';
        if(allRequiredSteps!=null && allRequiredSteps.size()>0)
        {
            for(Step__c st : allRequiredSteps)
            {
                allMatrices.add(st.Matrix__c);
            }
        }
        system.debug('+++++++++++ All Matrices '+ allMatrices);
        //this.query = 'select id, GridMaster_Copy__c from Grid_Details_Copy__c where Grid_Master__c in :allMatrices';
        this.query = 'SELECT Id, Grid_Master__c FROM Grid_Details_Copy__c WHERE Grid_Master__c in :allMatrices WITH SECURITY_ENFORCED';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        try{
            return Database.getQueryLocator(query);
        }
        catch(System.QueryException qe){
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }
        return null;
    }

    global void execute(Database.BatchableContext BC, List <SObject> scope) 
    {
        for (SObject gdc : scope)
        {
            if(gdc.get('Grid_Master__c') != null)
            {
                allMatricesGMC.add(String.valueof(gdc.get('Grid_Master__c')));
            }
        }

        if(scope.size()>0 && scope!=null){
            if(Grid_Details_Copy__c.sObjectType.getDescribe().isDeletable()){
                Database.DeleteResult[] srList = Database.delete(scope, false);
            }
            else{
                SnTDMLSecurityUtil.printDebugMessage('You dont have permission to delete Grid_Details_Copy__c','BatchDeleteRecsBeforereExecuteGDC');
            }
        }
    }

    global void finish(Database.BatchableContext BC) 
    {
        Integer batchSize = 2000;
        List<Batch_Size_Configuration__mdt> batch_size_config = new List<Batch_Size_Configuration__mdt>();
        try
        {
            batch_size_config = [Select Batch_Class_Name__c,Batch_Size__c from Batch_Size_Configuration__mdt where Batch_Class_Name__c = 'BatchExecuteRuleEngine'];
        }
        catch(System.QueryException qe)
        {
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }

        if(batch_size_config.size()>0){
            batchSize = (Integer)batch_size_config[0].Batch_Size__c;
        }
        System.debug('---batchSize----'+batchSize);

        BatchExecuteRuleEngine batchExecute = new BatchExecuteRuleEngine(ruleId, WhereClause);
        Database.executeBatch(batchExecute,batchSize);
    }
}