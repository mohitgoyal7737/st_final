@isTest
public class BatchdeleteRecordsMCCPTest {

    static testMethod void testMethod2() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        

        SIQ_MC_Cycle_vod_O__c u = new SIQ_MC_Cycle_vod_O__c();
        u.SIQ_Country_Code_AZ__c = 'US';
        insert u;
        List<String> countryCode = new List<String> ();
        countryCode.add('US');
        String objName = 'SIQ_MC_Cycle_vod_O__c';
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            BatchdeleteRecordsMCCP obj=new BatchdeleteRecordsMCCP(countryCode);
            //BatchdeleteRecordsMCCP.query = 'SELECT Id FROM '+objName;
            Database.executeBatch(obj);
            
        }
        Test.stopTest();
    }
    
}