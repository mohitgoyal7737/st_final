global with sharing class UpdateAccTerr implements Database.Batchable<sObject>, Database.Stateful
{
    /*Replaced temp_acc_Terr__c with temp_obj__c due to object purge*/
    global string query ;
    global string teamID;
    global string teamInstance;
    global string teamName;
    String Ids;
    Boolean flag = true;
    global list<AxtriaSalesIQTM__Team__c> temlst = new list<AxtriaSalesIQTM__Team__c>();
    global list<temp_Obj__c> accTerrlist = new list<temp_Obj__c>();
    Global Id BC {get; set;}
    global string country;
    global list<AxtriaSalesIQTM__Team_Instance__c> countrylst = new list<AxtriaSalesIQTM__Team_Instance__c>();
    public List<AxtriaSalesIQTM__Change_Request__c> cr = new List<AxtriaSalesIQTM__Change_Request__c>();
    public Integer recordsCreated;

    //Added by HT(A0994) on 17th June 2020
    global String changeReqID;
    global Boolean flagValue = false;

    global UpdateAccTerr(String Team, String TeamIns)
    {
        teamInstance = TeamIns;
        teamID = Team;
        temlst = [select name from AxtriaSalesIQTM__Team__c where id = :teamID];
        countrylst = [select AxtriaSalesIQTM__Country__c from AxtriaSalesIQTM__Team_Instance__c where id = :teamInstance];
        country = countrylst[0].AxtriaSalesIQTM__Country__c;
        teamName = temlst[0].name;
        recordsCreated =0;
        SnTDMLSecurityUtil.printDebugMessage('======team =====' + teamName);
        SnTDMLSecurityUtil.printDebugMessage('======team ID =====' + teamID);
        SnTDMLSecurityUtil.printDebugMessage('======teamInstance =====' + teamInstance);
        query = 'SELECT id,Territory_ID__c,AccountNumber__c,Team_Name__c,Account_Type__c FROM temp_Obj__c where Team_Name__c=:teamName and status__c = \'New\' and Object__c = \'Acc_Terr\' ';
    }

    global UpdateAccTerr(String Team, String TeamIns, String Ids)
    {
        teamInstance = TeamIns;
        teamID = Team;
        this.Ids = Ids;
        recordsCreated = 0;
        temlst = [select name from AxtriaSalesIQTM__Team__c where id = :teamID];
        countrylst = [select AxtriaSalesIQTM__Country__c from AxtriaSalesIQTM__Team_Instance__c where id = :teamInstance];
        country = countrylst[0].AxtriaSalesIQTM__Country__c;
        teamName = temlst[0].name;
        SnTDMLSecurityUtil.printDebugMessage('======team =====' + teamName);
        SnTDMLSecurityUtil.printDebugMessage('======team ID =====' + teamID);
        SnTDMLSecurityUtil.printDebugMessage('======teamInstance =====' + teamInstance);
        cr = [Select Id, AxtriaSalesIQTM__Request_Type_Change__c,Records_Created__c from AxtriaSalesIQTM__Change_Request__c where id = :IDs];
        query = 'SELECT id,Territory_ID__c,AccountNumber__c,Team_Name__c,Account_Type__c FROM temp_Obj__c where Team_Name__c=:teamName and status__c = \'New\' and Object__c = \'Acc_Terr\'';
    }

    //Added by HT(A0994) on 17th June 2020
    global UpdateAccTerr(String Team, String TeamIns, String Ids, Boolean flag)
    {
        teamInstance = TeamIns;
        teamID = Team;
        this.Ids = Ids;
        flagValue = flag;
        changeReqID = Ids;
        recordsCreated = 0;
        temlst = [select name from AxtriaSalesIQTM__Team__c where id = :teamID WITH SECURITY_ENFORCED];
        countrylst = [select AxtriaSalesIQTM__Country__c from AxtriaSalesIQTM__Team_Instance__c 
                        where id = :teamInstance WITH SECURITY_ENFORCED];
        country = countrylst[0].AxtriaSalesIQTM__Country__c;
        teamName = temlst[0].name;
        SnTDMLSecurityUtil.printDebugMessage('======team =====' + teamName);
        SnTDMLSecurityUtil.printDebugMessage('======team ID =====' + teamID);
        SnTDMLSecurityUtil.printDebugMessage('======teamInstance =====' + teamInstance);
        cr = [Select Id, AxtriaSalesIQTM__Request_Type_Change__c,Records_Created__c 
                from AxtriaSalesIQTM__Change_Request__c where id = :IDs WITH SECURITY_ENFORCED];
        query = 'SELECT id,Territory_ID__c,AccountNumber__c,Team_Name__c,Account_Type__c '+
                'FROM temp_Obj__c where Team_Name__c=:teamName and status__c = \'New\' and '+
                'Object__c = \'Acc_Terr\' and Change_Request__c =:changeReqID WITH SECURITY_ENFORCED';
    }

    global Database.Querylocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator(query);
    }

    global void execute (Database.BatchableContext BC, List<temp_Obj__c>accTerrlist)
    {
        recordsCreated+=accTerrlist.size();
        SnTDMLSecurityUtil.printDebugMessage('======team =====' + teamName);
        SnTDMLSecurityUtil.printDebugMessage('======team ID =====' + teamID);
        SnTDMLSecurityUtil.printDebugMessage('======teamInstance =====' + teamInstance);

        list<String> accNolst = new List<String>();
        for(temp_Obj__c a : accTerrlist)
        {   String acc = a.AccountNumber__c;
            if(acc!=null && acc !='')
                accNolst.add(a.AccountNumber__c);
        }

        map<string, string> accmap = new map<string, string>();
        for(Account a : [select AccountNumber, id from Account where AccountNumber in : accNolst and Country_ID__c = :country])
        {
            accmap.put(a.AccountNumber, a.id);
        }

        map<string, string> posmap = new map<string, string>();
        for(AxtriaSalesIQTM__Position__c p : [select AxtriaSalesIQTM__Client_Position_Code__c, id from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_iD__c = :teamID and AxtriaSalesIQTM__Team_Instance__c = :teamInstance])
        {
            posmap.put(p.AxtriaSalesIQTM__Client_Position_Code__c, p.id);
        }

        list<temp_Obj__c> accTerListUpdate = new list<temp_Obj__c>();
        
        for(temp_Obj__c rec : accTerrlist)
        {
            if(String.isNotBlank(rec.Territory_ID__c) &&  String.isNotBlank(rec.AccountNumber__c) && String.isNotBlank(rec.Team_Name__c) && String.isNotBlank(rec.Account_Type__c)){
                rec.Team__c = teamID;
                if(accmap.containsKey(rec.AccountNumber__c))
                    rec.Account__c = accmap.get(rec.AccountNumber__c);
                if(posmap.containsKey(rec.Territory_ID__c))
                    rec.Territory__c = posmap.get(rec.Territory_ID__c);  
            }
            else{
                SnTDMLSecurityUtil.printDebugMessage('rec.Territory_ID__c'+rec.Territory_ID__c +'rec.AccountNumber__c'+rec.AccountNumber__c+'rec.Team_Name__c'+rec.Team_Name__c+'rec.Account_Type__c'+rec.Account_Type__c);
                rec.status__c = 'Rejected';
                rec.Reason_Code__c = 'Mandatory Field Missing or Incorrect Team Instance';
                rec.SalesIQ_Error_Message__c = 'Mandatory Field Missing or Incorrect Team Instance';
                rec.Change_Request__c = Ids;//A1422
                rec.isError__c = true;//A1422
            }
            accTerListUpdate.add(rec);
        }
        //update accTerListUpdate;
        SnTDMLSecurityUtil.updateRecords(accTerListUpdate, 'UpdateAccTerr');
        SnTDMLSecurityUtil.printDebugMessage('******************************update done************************************');
    }

    global void finish(Database.BatchableContext BC)
    {   
        AxtriaSalesIQTM__Change_Request__c changerequest = new  AxtriaSalesIQTM__Change_Request__c();
        if(ST_Utility.getJobStatus(BC.getJobId())){
            if(Ids != null)
            {
                //batch to update position_account
                if(flagValue)
                    Database.executeBatch(new UpdatePosAcc(teamID,teamInstance,Ids,flagValue), 500);
                else
                    Database.executeBatch(new UpdatePosAcc(teamID,teamInstance,Ids), 500);
            }
            else
            {
                Database.executeBatch(new UpdatePosAcc(teamID, teamInstance), 500);   //batch to update position_account
            }
            
            changerequest.Id = Ids;
            if(cr.size()>0 && cr[0].AxtriaSalesIQTM__Request_Type_Change__c == 'Data Load Backend'){
                changerequest.Records_Created__c = recordsCreated;
            }
            //update changerequest; 
            SnTDMLSecurityUtil.updateRecords(changerequest, 'UpdateAccTerr');   
        }
        else if(Ids != null){

            changerequest.Id = Ids;
            changerequest.Job_Status__c = 'Error';
            if(cr.size()>0 && cr[0].AxtriaSalesIQTM__Request_Type_Change__c == 'Data Load Backend'){
                changerequest.Records_Created__c = recordsCreated;
            }
            //update changerequest;
            SnTDMLSecurityUtil.updateRecords(changerequest, 'UpdateAccTerr');
        }      
    }
}