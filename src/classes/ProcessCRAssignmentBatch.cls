/*****************************************************************************************************
    Name        :   ProcessCRAssignmentBatch.cls
    Description :   Batch Class to Update Related Objects of the Expired Position of the Change Request. 
                    This class updates the Position Accounts, Position Geographies of an Expired Position 
                    to that of an Unassigned Territory and End Date of the Position Employee.
    Params      :   Change Request Id , CR Assignment Type
*****************************************************************************************************/
global class ProcessCRAssignmentBatch implements Database.Batchable<sObject>, Database.AllowsCallouts 
{
    global String query;
    //global string crID;
    global String type;
    //global Id unassignedTerritory;
    //global Id unassignedPosTeamInsId;
    global Date endDate;
    List<Id> objList;
    String positionId;
    String teamInstance;
    //List<CR_Position__c> crPosList;

    global map<id,AxtriaSalesIQTM__Team_Instance__c> DateTeamInstanceMap;
    global map<id,id> UnassignedPosMap;
    global set<Id> unassignedTerrIds;
    global map<id,id> UnassignedPosTeamInsMap;
    global Date processDate;
    global set<Id> inactivePosId;

    //global set<Id> processedRecIds;
    
    global ProcessCRAssignmentBatch(AxtriaSalesIQTM__Change_Request__c changeReq,String assignmentType)
    {
        /*type = assignmentType;
        crID = changeReq.Id;
        objList = new List<Id>();
        positionId = changeReq.Source_Position__c;
        teamInstance = changeReq.Team_Instance_ID__c;
        unassignedTerritory = changeReq.Destination_Position__c;
        Position_Team_Instance__c unassignedPosTeamIns = [Select Id from Position_Team_Instance__c where Position_ID__c =: unassignedTerritory and Team_Instance_ID__c =: teamInstance limit 1];
        unassignedPosTeamInsId = unassignedPosTeamIns.Id;

        processedRecIds = new set<Id>();

        List<CR_Position__c> crPosList = [Select id,Effective_End_Date__c,Position__c from CR_Position__c where Position__c =: positionId limit 1];
        if(crPosList.size()>0)
        {
            endDate = crPosList[0].Effective_End_Date__c;
        }

        if(assignmentType == 'CR_Account')
        {
            List<CR_Account__c> scope = [Select id,Name,Account__c,Change_Request__c from CR_Account__c where Change_Request__c =: changeReq.Id];
            for(sObject tmp: scope)
            {
                CR_Account__c tmp1 = (CR_Account__c)tmp;
                objList.add(tmp1.Account__c);
            }
            query = 'Select id,Name,Account__c,Team_Instance__c,Position__c from Position_Account__c where Account__c in: objList and Position__c =: positionId and Team_Instance__c =: teamInstance and Assignment_Status__c != \''+SalesIQGlobalConstants.POSITION_ACCOUNT_INACTIVE+ '\'';

        }
        else if(assignmentType == 'CR_Geography')
        {
            List<CR_Geography__c> scope = [Select id,Name,Geography__c,Change_Request__c from CR_Geography__c where Change_Request__c =: changeReq.Id];
            for(sObject tmp: scope)
            {
                CR_Geography__c tmp1 = (CR_Geography__c)tmp;
                objList.add(tmp1.Geography__c);
            }
            query = 'Select id,Name,Geography__c,Team_Instance__c,Position__c from Position_Geography__c where Geography__c in: objList and Position__c =: positionId and Team_Instance__c =: teamInstance and Assignment_Status__c != \''+SalesIQGlobalConstants.POSITION_GEOGRAPHY_INACTIVE+'\'';
        }
        else if(assignmentType == 'CR_Employee')
        {
            List<CR_Employee_Assignment__c> scope = [Select id,Name,Employee_ID__c,Change_Request_ID__c from CR_Employee_Assignment__c where Change_Request_ID__c =: changeReq.Id];
            for(sObject tmp: scope)
            {
                CR_Employee_Assignment__c tmp1 = (CR_Employee_Assignment__c)tmp;
                objList.add(tmp1.Employee_ID__c);
            }
            query = 'Select id,Name,Effective_End_Date__c,Position__c,Employee__c from Position_Employee__c where Employee__c in: objList and Position__c =: positionId and Assignment_Status__c != \''+SalesIQGlobalConstants.EMPLOYEE_STATUS_INACTIVE+'\'';
        }*/
    }

    global ProcessCRAssignmentBatch(set<Id> positionId,String assignmentType)
    {
        type = assignmentType;
        DateTeamInstanceMap = new map<id,AxtriaSalesIQTM__Team_Instance__c>();
        UnassignedPosMap = new map<id,id>();
        UnassignedPosTeamInsMap = new map<id,id>();
        unassignedTerrIds = new set<Id>();
        inactivePosId = new set<Id>();
        inactivePosId.addAll(positionId);

        double processDay;
        if(AxtriaSalesIQTM__TotalApproval__c.getValues('ProcessDay') != null){
            processDay = AxtriaSalesIQTM__TotalApproval__c.getValues('ProcessDay').AxtriaSalesIQTM__No_Of_Approval__c;
        }else{
            processDay = 0;
        }
        processDate = System.today().addDays(integer.valueOf(processDay));

        System.debug('processDate - '+processDate);
        
        for(AxtriaSalesIQTM__Team_Instance__c t : [select id, AxtriaSalesIQTM__IC_EffStartDate__c, AxtriaSalesIQTM__IC_EffEndDate__c,AxtriaSalesIQTM__Alignment_Period__c from AxtriaSalesIQTM__Team_Instance__c]){
            DateTeamInstanceMap.put(t.id,t);
        }
        for(AxtriaSalesIQTM__Position__c ps : [select id,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Client_Position_Code__c='00000']){
            unassignedTerrIds.add(ps.Id);
            if(!UnassignedPosMap.containsKey(ps.AxtriaSalesIQTM__Team_Instance__c)){
                UnassignedPosMap.put(ps.AxtriaSalesIQTM__Team_Instance__c,ps.id);
            }
        }
        for(AxtriaSalesIQTM__Position_Team_Instance__c ps : [select id , AxtriaSalesIQTM__Team_Instance_ID__c from AxtriaSalesIQTM__Position_Team_Instance__c where AxtriaSalesIQTM__Position_ID__c in : unassignedTerrIds]){
            if(!UnassignedPosTeamInsMap.containsKey(ps.AxtriaSalesIQTM__Team_Instance_ID__c)){
                UnassignedPosTeamInsMap.put(ps.AxtriaSalesIQTM__Team_Instance_ID__c,ps.id);
            }
        }
        System.debug('DateTeamInstanceMap -'+DateTeamInstanceMap);
        System.debug('UnassignedPosMap -'+UnassignedPosMap);
        System.debug('UnassignedPosTeamInsMap - '+UnassignedPosTeamInsMap);
        System.debug('inactivePosId  -'+inactivePosId );

        if(type == 'Position_Account__c'){
            query = 'Select AxtriaSalesIQTM__Account_Alignment_Type__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Account_Target_Type__c,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Assignment_Status__c,AxtriaSalesIQTM__Change_Status__c,AxtriaSalesIQTM__Comments__c,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Effective_Start_Date__c,Id,AxtriaSalesIQTM__Metric1__c,AxtriaSalesIQTM__Metric2__c,AxtriaSalesIQTM__Metric3__c,AxtriaSalesIQTM__Metric4__c,AxtriaSalesIQTM__Metric5__c,AxtriaSalesIQTM__Metric6__c,AxtriaSalesIQTM__Metric7__c,AxtriaSalesIQTM__Metric8__c,AxtriaSalesIQTM__Metric9__c,AxtriaSalesIQTM__Metric10__c,AxtriaSalesIQTM__Metric_1__c,Name,AxtriaSalesIQTM__Position_Team_Instance__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Segment_1__c,AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c From AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Effective_End_Date__c >=: processDate and AxtriaSalesIQTM__Position__c in : inactivePosId order by LastModifiedDate desc';

        }else if(type == 'Position_Geography__c'){
            query = 'Select AxtriaSalesIQTM__Assignment_Status__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Change_Status__c,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__Geography__c,Id,AxtriaSalesIQTM__Metric1__c,AxtriaSalesIQTM__Metric2__c,AxtriaSalesIQTM__Metric3__c,AxtriaSalesIQTM__Metric4__c,AxtriaSalesIQTM__Metric5__c,AxtriaSalesIQTM__Metric6__c,AxtriaSalesIQTM__Metric7__c,AxtriaSalesIQTM__Metric8__c,AxtriaSalesIQTM__Metric9__c,AxtriaSalesIQTM__Metric10__c,Name,AxtriaSalesIQTM__Position_Team_Instance__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c From AxtriaSalesIQTM__Position_Geography__c where AxtriaSalesIQTM__Effective_End_Date__c >=: processDate and AxtriaSalesIQTM__Position__c in : inactivePosId order by LastModifiedDate desc';

        }else if(type == 'Position_Employee__c'){
            query = 'Select Id, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Employee__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c From AxtriaSalesIQTM__Position_Employee__c where AxtriaSalesIQTM__Effective_End_Date__c >=: processDate and AxtriaSalesIQTM__Employee__c != null and  AxtriaSalesIQTM__Position__c in : inactivePosId order by LastModifiedDate desc';
            
        }
        
        System.debug('### query :'+query);

    }

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        System.debug('query -'+query);
        return Database.getQueryLocator(query);
    }   

    global void execute(Database.BatchableContext BC,List<sObject> scope)
    {

        System.debug('scope -'+scope.size());
        if(type == 'Position_Account__c'){
            updatePosAcc(scope);
        }else if(type == 'Position_Geography__c'){
            updatePosGeo(scope);
        }else if(type == 'Position_Employee__c'){
            updatePosEmp(scope);
        }   
    } 

    global void finish(Database.BatchableContext BC){

    	System.debug('--- ProcessCRAssignmentBatch Excuted SuccessFully ------');
        /*if(type == 'CR_Geography'){
            //invoke sync with ESRI job to update ZIP terr on ESRI for deleted position.
            Id jobId = System.System.enqueueJob(new QueueESRIBatchProcess(new list<String>{crID}));
        }
        //Extent calculation in case of cascade delete assignments.
        if(processedRecIds.size() > 0){
            HandlerPosGeo.rollupExtentsOnTerritoryFuture(processedRecIds);
        }*/
    }

    // Update the Position Account
    public void updatePosAcc(List<AxtriaSalesIQTM__Position_Account__c> scope)
    {
        System.debug('---- updatePosAcc----');
        list<AxtriaSalesIQTM__Position_Account__c> PosAccntList=new list<AxtriaSalesIQTM__Position_Account__c>();
        for(AxtriaSalesIQTM__Position_Account__c pa :scope)
        {
            /*posAcc.Position__c = unassignedTerritory;
            posAcc.Position_Team_Instance__c = unassignedPosTeamInsId;*/
            if(DateTeamInstanceMap.get(pa.AxtriaSalesIQTM__Team_Instance__c).AxtriaSalesIQTM__Alignment_Period__c== 'Future')
                pa.AxtriaSalesIQTM__Effective_Start_Date__c = pa.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c.addDays(-1);
            
            //pa.AxtriaSalesIQTM__Effective_End_Date__c = processDate.addDays(-1);
            pa.AxtriaSalesIQTM__Effective_End_Date__c = pa.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c.addDays(-1);
            PosAccntList.add(pa);

            AxtriaSalesIQTM__Position_Account__c clonedPosAccntRecord = pa.clone(false,true,false,false);
            clonedPosAccntRecord.AxtriaSalesIQTM__Position__c = UnassignedPosMap.get(pa.AxtriaSalesIQTM__Team_Instance__c);
            clonedPosAccntRecord.AxtriaSalesIQTM__Position_Team_Instance__c = UnassignedPosTeamInsMap.get(pa.AxtriaSalesIQTM__Team_Instance__c);
            if(DateTeamInstanceMap.get(pa.AxtriaSalesIQTM__Team_Instance__c).AxtriaSalesIQTM__Alignment_Period__c== 'Future')
            	clonedPosAccntRecord.AxtriaSalesIQTM__Effective_Start_Date__c = DateTeamInstanceMap.get(pa.AxtriaSalesIQTM__Team_Instance__c).AxtriaSalesIQTM__IC_EffstartDate__c;
            else
            	clonedPosAccntRecord.AxtriaSalesIQTM__Effective_Start_Date__c = pa.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c.addDays(1);
            
            clonedPosAccntRecord.AxtriaSalesIQTM__Effective_End_Date__c = DateTeamInstanceMap.get(pa.AxtriaSalesIQTM__Team_Instance__c).AxtriaSalesIQTM__IC_EffEndDate__c;

            PosAccntList.add(clonedPosAccntRecord);
        }
        //update scope;
        System.debug('--- PosAccntList :'+PosAccntList.size());
        upsert PosAccntList;
    }

    //Update Position Geography
    public void updatePosGeo(List<AxtriaSalesIQTM__Position_Geography__c> scope)
    {
        System.debug('---- updatePosGeo ----- ');
        list<AxtriaSalesIQTM__Position_Geography__c> PosGeoList = new list<AxtriaSalesIQTM__Position_Geography__c>();
        for(AxtriaSalesIQTM__Position_Geography__c pg :scope)
        {
            /*processedRecIds.add(posGeo.Id);
            posGeo.Position__c = unassignedTerritory;
            posGeo.Position_Team_Instance__c = unassignedPosTeamInsId;*/
            if(DateTeamInstanceMap.get(pg.AxtriaSalesIQTM__Team_Instance__c).AxtriaSalesIQTM__Alignment_Period__c == 'Future')
                pg.AxtriaSalesIQTM__Effective_Start_Date__c = pg.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c.addDays(-1);

            pg.AxtriaSalesIQTM__Effective_End_Date__c = pg.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c.addDays(-1);
            pg.AxtriaSalesIQTM__Restrict_Position_Geography_Trigger__c = true;
            PosGeoList.add(pg);
            
            AxtriaSalesIQTM__Position_Geography__c clonedPosGeoRecord = pg.clone(false,true,false,false);
            clonedPosGeoRecord.AxtriaSalesIQTM__Position__c = UnassignedPosMap.get(pg.AxtriaSalesIQTM__Team_Instance__c);
            clonedPosGeoRecord.AxtriaSalesIQTM__Position_Team_Instance__c = UnassignedPosTeamInsMap.get(pg.AxtriaSalesIQTM__Team_Instance__c);

            if(DateTeamInstanceMap.get(pg.AxtriaSalesIQTM__Team_Instance__c).AxtriaSalesIQTM__Alignment_Period__c== 'Future')
            	clonedPosGeoRecord.AxtriaSalesIQTM__Effective_Start_Date__c = DateTeamInstanceMap.get(pg.AxtriaSalesIQTM__Team_Instance__c).AxtriaSalesIQTM__IC_EffstartDate__c;
            else
            	clonedPosGeoRecord.AxtriaSalesIQTM__Effective_Start_Date__c = pg.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c.addDays(1);

            clonedPosGeoRecord.AxtriaSalesIQTM__Effective_End_Date__c = DateTeamInstanceMap.get(pg.AxtriaSalesIQTM__Team_Instance__c).AxtriaSalesIQTM__IC_EffEndDate__c;

            clonedPosGeoRecord.AxtriaSalesIQTM__Restrict_Position_Geography_Trigger__c = true;

            PosGeoList.add(clonedPosGeoRecord);
        }
        //update scope;
        System.debug('--- PosGeoList:'+PosGeoList.size());
        upsert PosGeoList;
    }

    //Update Position Employee
    public void updatePosEmp(List<AxtriaSalesIQTM__Position_Employee__c> scope)
    {
        //Expiring the PositionEmployee records related to the Expired Positions 
        list<AxtriaSalesIQTM__Position_Employee__c> PosEmployeeList=new list<AxtriaSalesIQTM__Position_Employee__c>();
        for(AxtriaSalesIQTM__Position_Employee__c pe : scope){
            pe.AxtriaSalesIQTM__Effective_End_Date__c = pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c.addDays(-1);
            PosEmployeeList.add(pe);
        }
        update PosEmployeeList;
        /*for(Position_Employee__c posEmp:scope)
        {
            posEmp.Effective_End_Date__c = endDate;
        }
        update scope;*/
    }
}