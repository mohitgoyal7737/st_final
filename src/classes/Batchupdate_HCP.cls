global class Batchupdate_HCP implements Database.Batchable<sObject> {
    public String query;

    global Batchupdate_HCP() {
        this.query = query;
        query ='Select id,Account__c,Type__c from Temp_Acc_Affliation__c';
        system.debug('=============Query is:::'+query);
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Temp_Acc_Affliation__c> scope) {
        set<string>acid = new set<string>();
        for(Temp_Acc_Affliation__c temp : scope){
            acid.add(temp.Account__c);
        }
        system.debug('==========acid.size():====='+acid.size());
        list<Account>aclist = [select id from Account where id IN:acid];
        Database.update (aclist,false);
        Database.delete(scope,false);
        Database.emptyRecycleBin(scope);
        
    }

    global void finish(Database.BatchableContext BC) {
        Database.executeBatch(new BatchInBoundAffToAccRoleUpdate(), 2000);

    }
}