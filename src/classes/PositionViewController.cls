public class PositionViewController {
    public string EmployeeName{get;set;}
    public string selPos{get;set;}
    //public list<wrapperClass> wrapperList                   {get;set;} 
    public list<AxtriaSalesIQTM__Position_Employee__c> posAssignment     {get;set;}     

    public PositionViewController(ApexPages.StandardController controller){
        //wrapperList=new List<wrapperClass>();
        AxtriaSalesIQTM__Position__c posId=(AxtriaSalesIQTM__Position__c)controller.getRecord();
        posAssignment = [Select Id, AxtriaSalesIQTM__Position__c, 
                                                  AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c,
                                                  AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__c,
                                                  AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Assignment_Type__c,
                                                  AxtriaSalesIQTM__Effective_Start_Date__c, 
                                                  AxtriaSalesIQTM__Employee__c,AxtriaSalesIQTM__Employee__r.name,
                                                  AxtriaSalesIQTM__Reason_Code__c From 
                                                  AxtriaSalesIQTM__Position_Employee__c Where 
                                                  //AxtriaSalesIQTM__Effective_End_Date__c >=: system.today()
                                                  //and 
                         AxtriaSalesIQTM__Position__c =: posId.id]; 

        
        /*for(AxtriaSalesIQTM__Position_Employee__c assign:posAssignment) {
                wrapperList.add(new wrapperClass(assign));                      
        }*/
    }
    
    
    public class wrapperClass {      
        public AxtriaSalesIQTM__Position_Employee__c assignList      {get;set;}        
       
        
        public wrapperClass(AxtriaSalesIQTM__Position_Employee__c assignList) { 
            this.assignList=assignList;           
        }
    }
}