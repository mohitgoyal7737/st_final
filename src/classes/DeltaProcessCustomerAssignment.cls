public with sharing class DeltaProcessCustomerAssignment {
public list<Account>AccountList {get;set;}
//	public map<String,String>InactAcct{get;set;}
//	public map<String,String>ActAcct{get;set;}
	public list<AxtriaSalesIQTM__Position_Account__c>posacc {get;set;}
	public set<String>ActAcct {get;set;}
	public set<String>InactAcct {get;set;}
	public DateTime lastjobDate=null;
	Public String query {get;set;}

	public DeltaProcessCustomerAssignment(){
		AccountList = new list<Account>();
	//	InactAcct = new map<String,String>();
	//	ActAcct = new map<String,String>();
		ActAcct = new set<String>();
		InactAcct = new set<String>();
		posacc = new list<AxtriaSalesIQTM__Position_Account__c>();
		query = '';
	}
    
    public void updatePosAcc(){
        List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
        schLogList=[Select Id,CreatedDate,Created_Date__c from Scheduler_Log__c where Job_Name__c='Accounts Delta' and Job_Status__c='Successful' Order By Created_Date__c desc];
        if(schLogList.size()>0){
            lastjobDate=schLogList[0].CreatedDate;  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
        else{
        lastjobDate=null;       //else we set the lastjobDate to null
        }
        query ='SELECT AccountNumber,Id,isAddressChanged__c,isSpecialtyChanged__c,isTypeChanged__c,LastModifiedDate, ' +
               'Mini_Brick__c FROM Account Where (isAddressChanged__c=true or isSpecialtyChanged__c=true or  isTypeChanged__c=true) ';
    	if(lastjobDate!=null){
        query = query + 'and LastModifiedDate  >=:  lastjobDate '; 
        }
        System.debug('query'+ query);
                
        list<Account>acctList  = Database.query(query);      
        list<AxtriaSalesIQTM__Position_Account__c>posacct = new list<AxtriaSalesIQTM__Position_Account__c>();
        list<AxtriaSalesIQTM__Position_Account__c>ActAcctList=new list<AxtriaSalesIQTM__Position_Account__c>();
        list<AxtriaSalesIQTM__Position_Account__c>InActAcctList=new list<AxtriaSalesIQTM__Position_Account__c>();
        list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>pacp = new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        map<String,list<String>> accPosList=new map<String,list<String>>();
        list<id> accounts = new list<id>();
        for(Account ac : acctList){
           accounts.add(ac.id);
         }
        posacct=[select id,Acc_Pos__c,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Assignment_Status__c from AxtriaSalesIQTM__Position_Account__c where LastModifiedDate  >=:  lastjobDate and AxtriaSalesIQTM__Account__c in:acctList ];
        
        
        for(Account a:acctList){
            list<String> posList=new list<String>();
              for(AxtriaSalesIQTM__Position_Account__c pa:posacct){
               /* if(pa.AxtriaSalesIQTM__Assignment_Status__c.equals('Active') && pa.Account__c=a.id){
                    posList.add(pa.Position__c);
                }*/
            }
             accPosList.put(a.id,posList);
        }
        
        if(posacct.size()>0){
            for(AxtriaSalesIQTM__Position_Account__c pa:posacct){
                if(pa.AxtriaSalesIQTM__Assignment_Status__c.equals('Active')){
                    ActAcctList.add(pa);
                    ActAcct.add(pa.AxtriaSalesIQTM__Account__c+'_'+pa.AxtriaSalesIQTM__Position__c);
                }
                if(pa.AxtriaSalesIQTM__Assignment_Status__c.equals('Inactive')){
                    InActAcctList.add(pa);
                    InactAcct.add(pa.AxtriaSalesIQTM__Account__c+'_'+pa.AxtriaSalesIQTM__Position__c);
                }
            }
        }
          pacp=[select id,Acc_Pos__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Account__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Account__c in:acctList];
          if(pacp.size()>0){
            for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pa:pacp){
                
            }
        }
            

    }
    }