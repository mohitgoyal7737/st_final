/**********************************************************************************************
@author     : Dhruv Mahajan
@date       : 10 July'2017
@description: This Class is used for show Employee Universe and redirect to Manage Assignment Page.
Revison(s)  : Dhruv Mahajan - 10 July'17
**********************************************************************************************/

public class EmployeeListCtlr {
    public List<AxtriaSalesIQTM__Employee__c> empList           {get;set;}
    public List<AxtriaSalesIQTM__Employee__c> empListed         {get;set;}
    public List<String> headersList                             {get;set;}
    public String baseUrl                                       {get;set;}
    public boolean editpopup                                    {get;set;}
    public list<AxtriaSalesIQTM__Employee__c> selectedEmpList   {get;set;}  
    public AxtriaSalesIQTM__Employee__c empnewAddress           {get;set;}
    public string selectedEmployeeId                            {get;set;}
    public string redirectCall                                  {get;set;}
    public boolean showerrMsg                                   {get;set;}
    public string tabName                                       {get;set;}
    // --- Addition By Dhruv-- 10-July'17
    public integer totalEmpCount                                {get;set;}
    public integer totalUnassignedEmp                           {get;set;}
    public integer totalNewHires                                {get;set;}
    public integer totalTerminations                            {get;set;}
    public integer totalWorkBenchEmp                            {get;set;}
    public integer totalRehireEmp                               {get;set;}
    public integer totalLOA                                     {get;set;}
    public List<CR_Employee_Feed__c> CRfeed                     {get;set;}
    public boolean sendemailcolumnhidden                        {get;set;}
    //Added by Tharun -- Start
     public string rowIdSendMail                         {get;set;}
     public Boolean displayPopup         {get;set;}
    public boolean showManageAssignmentsButton {get;set;} // 
    public boolean showEditEmployeeButton {get;set;} // Added by Tharun
    //public string noEditAccessProfileCSV;
    //public string noAccessManageAssgnProfileCSV;
    Set<String> noEditAccessProfileSet = new Set<String>{'BU READ ONLY', 'VIEW ONLY','BU WRITE','SalesIQ RD','SalesIQ AD','SalesIQ VP'};
    Set<String> noAccessManageAssgnProfileSet = new Set<String>{'BU READ ONLY', 'VIEW ONLY'};
    // Added by Tharun -- End
    
    public string secondAddress1{get;set;}
    public string secondAddress2{get;set;}
    public string secondaryCity{get;set;}
    public string secondaryState{get;set;}
    public string secondaryZIP{get;set;}
    public string secondaryCountry{get;set;}
     
    public EmployeeListCtlr()
    {
        tabName=ApexPages.currentPage().getParameters().get('sfdc.tabName');
        editpopup = false;
        List<String> headersList = new List<String>();  
        baseUrl = System.Url.getSalesforceBaseUrl().toExternalForm();
        empnewAddress = new AxtriaSalesIQTM__Employee__c();
        
        showerrMsg = true;
        
        // --- Addition By Dhruv--- 10-July'17
        List<AxtriaSalesIQTM__Employee__c> empList = new List<AxtriaSalesIQTM__Employee__c>();
        empList= [Select id, Name,AxtriaSalesIQTM__Rehire_Date__c , AxtriaSalesIQTM__Employee_ID__c, AxtriaSalesIQTM__HR_Termination_Date__c,AssociateOID__c,
                   AxtriaSalesIQTM__Field_Status__c,Employee_Status__c ,JobCodeName__c,AxtriaSalesIQTM__Original_Hire_Date__c, AxtriaSalesIQTM__HR_Status__c, Hire_Date__c,Leave_End_Date__c 
                   from AxtriaSalesIQTM__Employee__c  /*WHERE Is_Field_Employee__c = true  where 
                   (Employment_Status__c != 'Terminated' OR 
                   (Employment_Status__c = 'Terminated' AND (AxtriaSalesIQTM__HR_Termination_Date__c = null OR 
                                                             AxtriaSalesIQTM__HR_Termination_Date__c > today ))) */];
        
        totalUnassignedEmp  = 0;  
        totalNewHires       = 0;
        totalTerminations   = 0;
        totalLOA            = 0;
        totalRehireEmp      = 0;
                 
        for(AxtriaSalesIQTM__Employee__c emp : empList){
            
            Date todayDate = system.today();
            Date lastThirtyDaysCheck = todayDate.adddays(-30);
            
            //changed by Akanksha on 12/27/2017.
            if(emp.AxtriaSalesIQTM__Field_Status__c == 'Unassigned'  &&  emp.employee_status__c != 'Terminated' && emp.employee_status__c != 'Retired'
                                   && emp.employee_status__c != 'Deceased'){//&& (emp.Employee_Status__c == 'Active' || emp.Employee_Status__c == 'LOA')
                totalUnassignedEmp++;
            }
            
            if(emp.AxtriaSalesIQTM__Original_Hire_Date__c >= lastThirtyDaysCheck){
                totalNewHires++;
            }
            /*if(emp.AxtriaSalesIQTM__HR_Termination_Date__c != null && emp.AxtriaSalesIQTM__HR_Termination_Date__c >= lastThirtyDaysCheck){
                totalTerminations++;
            }
            if(emp.Leave_End_Date__c >= lastThirtyDaysCheck){
                totalLOA++;
            }*/
            if(emp.Employee_Status__c == 'Terminated'|| emp.Employee_Status__c == 'Retired' || emp.Employee_Status__c == 'Deceased'){
                totalTerminations++;
            }
            if(emp.Employee_Status__c == 'LOA' ){
                totalLOA++;
            }
            if(emp.AxtriaSalesIQTM__Rehire_Date__c  >= lastThirtyDaysCheck){
                totalRehireEmp++;
            }
        }  
          
        system.debug('totalUnassignedEmp+++++'+totalUnassignedEmp);     
        totalEmpCount = empList.size();
        
        list<AggregateResult> workBenchEmpCount = [select count(id) from CR_Employee_Feed__c where  IsRemoved__c = false group by Employee__c];
        totalWorkBenchEmp = workBenchEmpCount.size();
        
        
        // Added by Tharun -- Start
        showManageAssignmentsButton = true;
        showEditEmployeeButton = true;
        
        id id1 = userinfo.getProfileId();
        string profileName = [select Name from profile where id = :id1].Name;
        system.debug('profileName'+profileName+'   '+noEditAccessProfileSet.contains(profileName.toUpperCase())+noEditAccessProfileSet);
        
        /*noEditAccessProfileCSV = 'BU READ ONLY,VIEW ONLY';
        noAccessManageAssgnProfileCSV = 'BU READ ONLY,VIEW ONLY';
        list<string> noEditAccessProfileList = noEditAccessProfileCSV.split(',');
        list<string> noAccessManageAssgnProfileList = noAccessManageAssgnProfileCSV.split(',');*/
        
        if(noEditAccessProfileSet.contains(profileName)){//profileName.toUpperCase()
             
            showEditEmployeeButton = false;        
        }
        if(noAccessManageAssgnProfileSet.contains(profileName)){//profileName.toUpperCase()
            showManageAssignmentsButton = false;        
        }
        
        // Added by Tharun -- End
        
        if(profileName == system.label.ColumnProfileWise){
            sendemailcolumnhidden = true;
        }
        
    }
   
    // Starts by dhruv===============//       
    public void editAddress(){
        if(string.isBlank(selectedEmployeeId)){
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Please select a employee to edit.'));
        }
        else{
            editpopup = true;
            empnewAddress = [select id, Name, AxtriaSalesIQTM__Employee_ID__c,Secondary_Address_Line_1__c,AssociateOID__c,
                                Secondary_Address_Line_2__c,Secondary_City__c,SS_Country__c,Secondary_State__c,
                                Secondary_ZIP__c
                                from AxtriaSalesIQTM__Employee__c 
                                where id =:selectedEmployeeId]; 
                                
        secondAddress1   = empnewAddress.Secondary_Address_Line_1__c;
        secondAddress2   = empnewAddress.Secondary_Address_Line_2__c;
        secondaryCity    = empnewAddress.Secondary_City__c;
        secondaryState   = empnewAddress.Secondary_State__c;
        secondaryZIP     = empnewAddress.Secondary_ZIP__c;
        secondaryCountry = empnewAddress.SS_Country__c;
        }
        
    }
     
    public void saveAddress(){
        boolean isReturn = true;
        empnewAddress.Secondary_Address_Line_1__c = secondAddress1;
        empnewAddress.Secondary_Address_Line_2__c = secondAddress2;
        empnewAddress.Secondary_City__c           = secondaryCity;
        empnewAddress.Secondary_State__c          = secondaryState;
        empnewAddress.Secondary_ZIP__c            = secondaryZIP;
        empnewAddress.SS_Country__c               = secondaryCountry;
         if(string.isBlank(empnewAddress.Secondary_Address_Line_1__c) && string.isBlank(empnewAddress.Secondary_City__c) 
            && string.isBlank(empnewAddress.Secondary_State__c) && string.isBlank(empnewAddress.Secondary_ZIP__c) && string.isBlank(empnewAddress.SS_Country__c)){
            
            update empnewAddress;
            editpopup = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Secondary Address saved successfully.'));
            showerrMsg = true;
        }
        else if(string.isNOTBlank(empnewAddress.Secondary_Address_Line_1__c) && string.isNOTBlank(empnewAddress.Secondary_City__c) 
                && string.isNOTBlank(empnewAddress.Secondary_State__c) && string.isNOTBlank(empnewAddress.Secondary_ZIP__c) && string.isNOTBlank(empnewAddress.SS_Country__c)){
             
            if(!empnewAddress.Secondary_ZIP__c.isNumeric() || empnewAddress.Secondary_ZIP__c.length() < 5){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'ZIP Code should be numeric and 5 digits.'));
                showerrMsg = false;
                isReturn= false;
            }
                else{
                    update empnewAddress;
                    editpopup = false;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Employee Address saved successfully.'));
                    showerrMsg = true;
                }
        }
        else{ 
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please fill all the address details'));
            showerrMsg = false;
            isReturn= false;
        }
    }
    
    public void cancelAddress(){
        editpopup = false;
    }
    
    // ends by dhruv===============//
    
    
    public PageReference blankMethod(){
        return null;
    }
    
    public void showPopup(){
        displayPopup = true;    
    }
    public void closePopup(){    
        //errorFlag=false;
        displayPopup = false;
    }        
    public void sendEmail(){
        list<AxtriaSalesIQTM__Employee__c> employeeList = new list<AxtriaSalesIQTM__Employee__c>();
        employeeList = [select id,Name,AxtriaSalesIQTM__Employee_ID__c,AssociateOID__c,
                    AxtriaSalesIQTM__HR_Termination_Date__c,Seperation_Date__c from AxtriaSalesIQTM__Employee__c where id = :rowIdSendMail];
        Contact con = new Contact();
                            con.FirstName = 'Test';
                            con.LastName = 'Contact';
                            con.Email = 'no-reply@axtria.com';
                            insert con;
        RMS_Email_Configurations__c RMC = RMS_Email_Configurations__c.getOrgDefaults();
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        string sendToCSV = RMC.Separation_Change_Mail_CSV__c;
        list<string> sendTo;
        if(sendTOCSV != null){
            sendTo = sendToCSV.split(',');    
        }
        mail.setToAddresses(sendTo);
        mail.setSubject('TevaRMS - Employee\'s Expected Separation Date and Separation Date');
        mail.setTargetObjectID(con.ID);
        mail.setSenderDisplayName('Teva Portal');
        //mail.setBccAddresses(new String[] {'Perumalla.Venkata@axtria.com','Ruchi.Jain@axtria.com'});
        mail.setUseSignature(false);
        mail.setSaveAsActivity(false); 
        string msg = '<html><body>'+
                            '<p>This is an automated email.Please do not reply to this email.'+
                            '<br/><br/>Date: '+Date.Today().format()+'<br/><br/>'+
                            //'Below are the details of the employees who have different Separation Date and Expected Separation Date.</b></p> '+ 
                            '</p>'+
                            '<table style="border-width:0px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0" width="100%"><tr>'+
                            '<th style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">Employee Name</th>'+                            
                           // '<th style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">Employee Email</th>'+
                            //'<th style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">Local Employee ID</th>'+
                          //  '<th style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">Global Employee ID</th>'+
                            '<th style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">Separation Date</th>'+
                            '<th style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">Expected Separation Date</th>'+
                            '</tr>';
        for(AxtriaSalesIQTM__Employee__c Emp : employeeList){
            String formattedHRTD = '';
            String formattedExSD = '';
            if(emp.AxtriaSalesIQTM__HR_Termination_Date__c != null){
               formattedHRTD = emp.AxtriaSalesIQTM__HR_Termination_Date__c.format();
               
            }
            if(emp.Seperation_Date__c != null){
                formattedExSD = emp.Seperation_Date__c.format();
            }
            msg +=  '<tr><td align="center" style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">'+emp.Name+'</td>'+
                    //   '<td align="center" style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">'+emp.Work_Email__c+'</td>'+
                    //   '<td align="center" style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">'+emp.Local_Employee_Id__c+'</td>'+
                    //   '<td align="center" style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">'+emp.Global_Employee_ID__c+'</td>'+
                       '<td align="center" style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">'+formattedHRTD+'</td>'+
                       '<td align="center" style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">'+formattedExSD+'</td>'+
                       '</tr>';
        }
        msg += '</table>';
        list<AxtriaSalesIQTM__Position_Employee__c> posEmpList = new list<AxtriaSalesIQTM__Position_Employee__c>();
        posEmpList = [select AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__Effective_End_Date__c,
                        AxtriaSalesIQTM__Assignment_Type__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,
                        AxtriaSalesIQTM__Employee__r.name,AxtriaSalesIQTM__Position__r.name from AxtriaSalesIQTM__Position_Employee__c
                        where AxtriaSalesIQTM__Employee__c in :employeeList AND AxtriaSalesIQTM__Employee__r.Is_Field_Employee__c = true ];
        if(posEmpList.size() > 0){
            msg += '<br/><br/>';
            msg += '<table style="border-width:0px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0" width="100%"><tr>'+
                            '<th style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">Employee Name</th>'+                            
                            '<th style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">Territory Name</th>'+
                            '<th style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">Territory ID</th>'+
                            '<th style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">Start Date</th>'+
                            '<th style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">End Date</th>'+
                            '<th style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">Assignment Type</th>'+
                            '</tr>';
                            
            for(AxtriaSalesIQTM__Position_Employee__c posEmp : posEmpList){
                String formattedStartDate = '';
                String formattedEndDate = '';
                if(posEmp.AxtriaSalesIQTM__Effective_Start_Date__c != null){
                    formattedStartDate = posEmp.AxtriaSalesIQTM__Effective_Start_Date__c.format();
                }
                if(posEmp.AxtriaSalesIQTM__Effective_End_Date__c != null){
                    formattedEndDate = posEmp.AxtriaSalesIQTM__Effective_End_Date__c.format();
                }
                msg += '<tr><td align="center" style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">'+posEmp.AxtriaSalesIQTM__Employee__r.name+'</td>'+
                       '<td align="center" style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">'+posEmp.AxtriaSalesIQTM__Position__r.name+'</td>'+
                       '<td align="center" style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">'+posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c+'</td>'+
                       '<td align="center" style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">'+formattedStartDate+'</td>'+
                       '<td align="center" style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">'+formattedEndDate+'</td>'+
                       '<td align="center" style="border-width:1px;border-color:black;border-style:solid;" cellpadding="0" cellspacing="0">'+posEmp.AxtriaSalesIQTM__Assignment_Type__c+'</td>'+
                       '</tr>';
            }
            msg += '</table>';
             
        }
                                
        msg += '<br/><br/>Thank you,<br/>Axtria SalesIQ Support<br/></body></html>' ;
        mail.setHtmlBody(msg);
        if(sendTo != null){ 
            mails.add(mail);
        }
        try{
            if(!mails.isEmpty()){
                Messaging.sendEmail(mails);
            }
        }
        catch(Exception e){}
        Delete con;
        closePopup();
        system.debug('Successful');
        
    }
    
  @RemoteAction
    public static List<AxtriaSalesIQTM__Employee__c> empList(String status)
    {
        List<AxtriaSalesIQTM__Employee__c> empList=new List<AxtriaSalesIQTM__Employee__c>();
        List<CR_Employee_Feed__c> CRfeed = new List<CR_Employee_Feed__c>();
        
       if(status==null){
        empList = [Select id, Name, 
                   AxtriaSalesIQTM__Employee_ID__c,  
                   Employee_Status__c,Current_Position__r.Team_Name__c,JobCodeName__c,AssociateOID__c,
                   AxtriaSalesIQTM__Manager__c,Hire_Date__c,AxtriaSalesIQTM__Original_Hire_Date__c,
                   AxtriaSalesIQTM__Manager__r.Name, AxtriaSalesIQTM__Job_Title__c,Current_Position__r.AxtriaSalesIQTM__Position_Type__c,
                   AxtriaSalesIQTM__Field_Status__c,AxtriaSalesIQTM__HR_Status__c,
                   AxtriaSalesIQTM__Current_Territory__r.Team_Name__c,Current_Position__r.AxtriaSalesIQTM__Client_Position_Code__c,
                   Current_Position__c,CurrentPositionId__c,Current_Position__r.Name,
                   AxtriaSalesIQTM__Employee_Type__c
                   from AxtriaSalesIQTM__Employee__c /*where Is_Field_Employee__c = true*/ ];
       }
        
       else if(status == 'Unassigned'){
            empList = [Select id, Name, 
                       AxtriaSalesIQTM__Employee_ID__c,  
                       Employee_Status__c,Current_Position__r.Team_Name__c,JobCodeName__c,AssociateOID__c,
                       AxtriaSalesIQTM__Manager__c,Hire_Date__c,AxtriaSalesIQTM__Original_Hire_Date__c,
                       AxtriaSalesIQTM__Manager__r.Name, AxtriaSalesIQTM__Job_Title__c,Current_Position__r.AxtriaSalesIQTM__Position_Type__c,
                       AxtriaSalesIQTM__Field_Status__c,AxtriaSalesIQTM__HR_Status__c,
                       AxtriaSalesIQTM__Current_Territory__r.Team_Name__c,
                       CurrentPositionId__c,Current_Position__r.Name,Current_Position__r.AxtriaSalesIQTM__Client_Position_Code__c,
                       AxtriaSalesIQTM__Employee_Type__c
                       from AxtriaSalesIQTM__Employee__c where AxtriaSalesIQTM__Field_Status__c =:status and /*(Employment_Status__c != 'Terminated' OR 
                   (Employment_Status__c = 'Terminated' AND (AxtriaSalesIQTM__HR_Termination_Date__c = null OR 
                                                             AxtriaSalesIQTM__HR_Termination_Date__c > today )))
                                   Is_Field_Employee__c = true and */ employee_status__c != 'Terminated' and employee_status__c != 'Retired'
                                   and employee_status__c != 'Deceased'];
       }
       else if(status == 'New Hire'){
            Date lastThirtyDaysCheck = system.today().adddays(-30);
        empList = [Select id, Name, 
                       AxtriaSalesIQTM__Employee_ID__c,  
                       Employee_Status__c,Current_Position__r.Team_Name__c,AssociateOID__c,
                       AxtriaSalesIQTM__Manager__c,Hire_Date__c,AxtriaSalesIQTM__Original_Hire_Date__c,
                       AxtriaSalesIQTM__Manager__r.Name, AxtriaSalesIQTM__Job_Title__c,Current_Position__r.AxtriaSalesIQTM__Position_Type__c,
                       AxtriaSalesIQTM__Field_Status__c,AxtriaSalesIQTM__HR_Status__c,
                       AxtriaSalesIQTM__Current_Territory__r.Team_Name__c,JobCodeName__c,
                       CurrentPositionId__c,Current_Position__r.Name,Current_Position__r.AxtriaSalesIQTM__Client_Position_Code__c,
                       AxtriaSalesIQTM__Employee_Type__c
                       from AxtriaSalesIQTM__Employee__c where AxtriaSalesIQTM__Original_Hire_Date__c >= :lastThirtyDaysCheck /*AND Is_Field_Employee__c = true*/];
       }
       else if(status == 'Termination'){
           empList = [Select id, Name, 
                       AxtriaSalesIQTM__Employee_ID__c,  
                       Employee_Status__c,Current_Position__r.Team_Name__c,AssociateOID__c,
                       AxtriaSalesIQTM__Manager__c,Hire_Date__c,AxtriaSalesIQTM__Original_Hire_Date__c,
                       AxtriaSalesIQTM__Manager__r.Name, AxtriaSalesIQTM__Job_Title__c,Current_Position__r.AxtriaSalesIQTM__Position_Type__c,
                       AxtriaSalesIQTM__Field_Status__c,AxtriaSalesIQTM__HR_Status__c,
                       AxtriaSalesIQTM__Current_Territory__r.Team_Name__c,Current_Position__r.AxtriaSalesIQTM__Client_Position_Code__c,
                       CurrentPositionId__c,Current_Position__r.Name,JobCodeName__c,
                       AxtriaSalesIQTM__Employee_Type__c
                       from AxtriaSalesIQTM__Employee__c where /*(Is_Field_Employee__c = true) AND*/ 
                       (Employee_Status__c = 'Terminated' OR Employee_Status__c='Retired' OR Employee_Status__c='Separated' OR Employee_Status__c='Deceased')
                       ];    
       }
       else if(status == 'Leave of Absence'){
           empList = [Select id, Name, 
                       AxtriaSalesIQTM__Employee_ID__c,  
                       Employee_Status__c,Current_Position__r.Team_Name__c,AssociateOID__c,Current_Position__r.AxtriaSalesIQTM__Client_Position_Code__c,
                       AxtriaSalesIQTM__Manager__c,Hire_Date__c,AxtriaSalesIQTM__Original_Hire_Date__c,
                       AxtriaSalesIQTM__Manager__r.Name, AxtriaSalesIQTM__Job_Title__c,Current_Position__r.AxtriaSalesIQTM__Position_Type__c,
                       AxtriaSalesIQTM__Field_Status__c,AxtriaSalesIQTM__HR_Status__c,
                       AxtriaSalesIQTM__Current_Territory__r.Team_Name__c,JobCodeName__c,
                       CurrentPositionId__c,Current_Position__r.Name,
                       AxtriaSalesIQTM__Employee_Type__c
                       from AxtriaSalesIQTM__Employee__c where Employee_Status__c = 'LOA' /*AND Is_Field_Employee__c = true*/];
       }
        else if(status == 'Employees in Workbench'){
            
            CRfeed = [Select id, Event_Name__c, Employee__c from CR_Employee_Feed__c where  IsRemoved__c = false];
            set<String> employees = new set<String>();
            for(CR_Employee_Feed__c cr  : CRfeed){
                employees.add(cr.Employee__c); 
            }   
        
            empList = [Select id, Name, 
                       AxtriaSalesIQTM__Employee_ID__c,  
                       Employee_Status__c,Current_Position__r.Team_Name__c,
                       AxtriaSalesIQTM__Manager__c,Hire_Date__c,AxtriaSalesIQTM__Original_Hire_Date__c, 
                       AxtriaSalesIQTM__Manager__r.Name,JobCodeName__c,AssociateOID__c, 
                       AxtriaSalesIQTM__Field_Status__c,AxtriaSalesIQTM__HR_Status__c,Current_Position__r.AxtriaSalesIQTM__Client_Position_Code__c,
                       AxtriaSalesIQTM__Current_Territory__r.Team_Name__c,AxtriaSalesIQTM__Job_Title__c,Current_Position__r.AxtriaSalesIQTM__Position_Type__c,
                       CurrentPositionId__c,Current_Position__r.Name,
                       AxtriaSalesIQTM__Employee_Type__c
                       from AxtriaSalesIQTM__Employee__c where ID IN :employees /*AND Is_Field_Employee__c = true*/ ];
        }  
        else if(status == 'ReHire'){
            Date lastThirtyDaysCheck = system.today().adddays(-30);
            empList = [Select id, Name, 
                       AxtriaSalesIQTM__Employee_ID__c,  
                       Employee_Status__c,Current_Position__r.Team_Name__c,AssociateOID__c,
                       AxtriaSalesIQTM__Manager__c,Hire_Date__c,AxtriaSalesIQTM__Original_Hire_Date__c,
                       AxtriaSalesIQTM__Manager__r.Name, AxtriaSalesIQTM__Job_Title__c,Current_Position__r.AxtriaSalesIQTM__Position_Type__c,
                       AxtriaSalesIQTM__Field_Status__c,AxtriaSalesIQTM__HR_Status__c,Current_Position__r.AxtriaSalesIQTM__Client_Position_Code__c,
                       AxtriaSalesIQTM__Current_Territory__r.Team_Name__c,JobCodeName__c,
                       CurrentPositionId__c,Current_Position__r.Name,
                       AxtriaSalesIQTM__Employee_Type__c
                       from AxtriaSalesIQTM__Employee__c where AxtriaSalesIQTM__Rehire_Date__c >= :lastThirtyDaysCheck /*AND Is_Field_Employee__c = true*/];
       } 
        return empList;
    }
}