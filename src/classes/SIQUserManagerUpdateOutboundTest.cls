@isTest
public class SIQUserManagerUpdateOutboundTest {
    
    static testMethod void testMethod1() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Name ='USA';
        insert countr;
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc.AxtriaSalesIQTM__Country__c = countr.id;
        acc.AccountNumber = 'BH10461999';
        acc.AxtriaSalesIQTM__AccountType__c = 'HCP';
        acc.Status__c = 'Active';
        acc.Type = 'HCP';
        insert acc;

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        scen.AxtriaSalesIQTM__Scenario_Stage__c ='Live';
        insert scen;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Name = 'TESTTEAMINS';
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        //teamins.AxtriaSalesIQTM__Team_Cycle_Name__c = 'Current';
        //teamins.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        insert teamins;
        AxtriaSalesIQTM__Employee__c emp = new AxtriaSalesIQTM__Employee__c();
        emp.Name = 'emp123';
        emp.AxtriaSalesIQTM__Country_Name__c  = countr.id;
        emp.AxtriaSalesIQTM__FirstName__c = 'Raju';
        emp.AxtriaSalesIQTM__Last_Name__c = 'uhj';
        insert emp;
        AxtriaSalesIQTM__Employee__c empm = new AxtriaSalesIQTM__Employee__c();
        empm.Name = 'emp1ccc23';
        empm.AxtriaSalesIQTM__Country_Name__c  = countr.id;
        empm.AxtriaSalesIQTM__FirstName__c = 'Rajrffu';
        empm.AxtriaSalesIQTM__Last_Name__c = 'uhggggj';
        insert empm;
        

        AxtriaSalesIQTM__Position__c pos1= TestDataFactory.createPosition(team,teamins);
        pos1.Employee1__c = empm.id;
        
        pos1.AxtriaSalesIQTM__Client_Position_Code__c = 'N003';
        
        insert pos1;
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.Employee1__c = emp.id;
        pos.AxtriaSalesIQTM__Employee__c = emp.id;
        pos.AxtriaSalesIQTM__Parent_Position__c = pos1.id;
        pos.AxtriaSalesIQTM__Client_Position_Code__c = 'N003';
        pos.Employee1_Assignment_Type__c ='test';
        insert pos;
        
        
        AxtriaSalesIQTM__Position_Employee__c posemp = TestDataFactory.createPositionEmployee(pos, emp.id, 'Primary', date.today(), date.today()+1);
        
        insert posemp;
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        posAccount.AxtriaSalesIQTM__Account__c = acc.id;
        posAccount.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        posAccount.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        insert posAccount;
        
        
        SIQ_User_Manager_Role_Outbound__c siq = new SIQ_User_Manager_Role_Outbound__c();
        siq.Employee__c = emp.id;
        siq.Manager__c = empm.id;
        siq.Status__c ='Updated';
        siq.Client_Territory_Code__c ='N003';
        insert siq;
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            SIQUserManagerUpdateOutbound obj=new SIQUserManagerUpdateOutbound();
            obj.query = 'Select Id,Employee_PRID__c,AxtriaSalesIQTM__Manager__c,Current_Territory1__c from AxtriaSalesIQTM__Employee__c ';
            Database.executeBatch(obj);
            
        }
        Test.stopTest();
    }
    
}