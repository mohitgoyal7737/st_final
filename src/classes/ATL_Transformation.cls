public with sharing class ATL_Transformation implements Schedulable {

    public List<SelectOption> allMarketingCodesSelect {get;set;}
    public String selectedMarketingCode {get;set;}

    public List<SelectOption> allCountriesSelect {get;set;}
    public String selectedCountry {get;set;}

    public List<String> selectedCountries;
    public List<String> allMarketingCodeList;
    
    public ATL_Transformation() 
    {
        allMarketingCodesSelect = new List<SelectOption>();
        selectedCountries = new List<String>();
        allCountriesSelect = new List<SelectOption>();
        allMarketingCodeList = new List<String>();

        fillMarketingCode();
    }
  public void execute(System.SchedulableContext SC){
       ATL_Transformation atl=new ATL_Transformation ();
    } 
    public void fillMarketingCode()
    {
        List<AxtriaSalesIQTM__Organization_Master__c> allMarketingCodesList = [select id, Name from AxtriaSalesIQTM__Organization_Master__c];
        
        if(allMarketingCodesList.size() > 0)
        {
            selectedMarketingCode = 'All';
        
            allMarketingCodesSelect.add(new SelectOption('All', 'All'));

            for(AxtriaSalesIQTM__Organization_Master__c om : allMarketingCodesList)
            {
                allMarketingCodeList.add(om.ID);
                allMarketingCodesSelect.add(new SelectOption(om.ID, om.Name));
            }

            marketingCodeChanged();
        }
    }

    public void marketingCodeChanged()
    {
        List<AxtriaSalesIQTM__Country__c> allCountriesList;

        if(selectedMarketingCode == 'All')
        {
            allCountriesList = [select id, Name from AxtriaSalesIQTM__Country__c where AxtriaSalesIQTM__Parent_Organization__c in :allMarketingCodeList];
        }
        else
        {
            allCountriesList = [select id, Name from AxtriaSalesIQTM__Country__c where AxtriaSalesIQTM__Parent_Organization__c = :selectedMarketingCode];
        }
        
        allCountriesSelect = new List<SelectOption>();

        system.debug('+++++++ All Countries List '+ allCountriesList);
        if(allCountriesList.size() > 0 )
        {
            selectedCountry = 'All';

            allCountriesSelect.add(new SelectOption('All', 'All'));
            for(AxtriaSalesIQTM__Country__c country : allCountriesList)
            {
                allCountriesSelect.add(new SelectOption(country.ID, country.Name));
            }
        }
    }

    public void pushATL()
    {

        if(selectedCountry == 'All')
        {
            List<AxtriaSalesIQTM__Country__c> selectedCountryList;
        
            if(selectedMarketingCode == 'All')
            {
                selectedCountryList = [select id, Name from AxtriaSalesIQTM__Country__c where AxtriaSalesIQTM__Parent_Organization__c in :allMarketingCodeList];
            }
            else
            {
                selectedCountryList = [select id, Name from AxtriaSalesIQTM__Country__c where AxtriaSalesIQTM__Parent_Organization__c = :selectedMarketingCode];
            }

            if(selectedCountryList.size() > 0)
            {
                for(AxtriaSalesIQTM__Country__c country : selectedCountryList)
                {
                    selectedCountries.add(country.ID);
                }
            }
        }
        else
        {
            selectedCountries.add(selectedCountry);
        }

        system.debug('++++++++ Hey Selected Country is ' + selectedCountries);
        List<Staging_ATL__c> sa = [select id from Staging_ATL__c LIMIT 1];

        if(sa.size()> 0)
        Database.executeBatch(new ATL_Delete_Recs(selectedCountries));
        else
        {
         Database.executeBatch(new ATL_TransformationBatch(selectedCountries));
        }

        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Request Received, Admin will be notified when Job finishes'));

    }
}