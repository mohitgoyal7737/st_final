public with sharing class tryCatchEmailAlert
{
    //public static list<VeevaEmailAlert__c> emailIds{get;set;}//commented due to object purge activity A1450
    //Replacing VeevaEmailAlert__c Custom Setting with Logger_Email__c
    public static list<Logger_Email__c> emailIds{get;set;}
    //public static List<Veeva_Job_Scheduling__c> veevaList;
    public  static List<string> emaillist;


    public static void veevaLoad(String status)
	{
	     /*veevaList=new List<Veeva_Job_Scheduling__c>();	 
         List<Veeva_Job_Scheduling__c> jobs= [Select id,Load_Type__c from Veeva_Job_Scheduling__c];
         for(Veeva_Job_Scheduling__c veeva: jobs)
         {
         	veeva.Load_Type__c= 'No Load';
         	veevaList.add(veeva);
         }
         update veevaList;*/
         veevaAlert(status);
	}
    public static void  veevaAlert(String status)
    {
        emaillist= new List<string>();
        Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
        String subject = 'Veeva Tranformation Jobs Fail Alert'+'  '+'in'+'  '+ UserInfo.getOrganizationName();
        //emailIds = VeevaEmailAlert__c.getall().values();//commented due to object purge activity A1450

        //Replacing VeevaEmailAlert__c Custom Setting with Logger_Email__c
        emailIds = new list<Logger_Email__c>();
        emailIds = [Select Id,Name from Logger_Email__c where Type__c = 'VeevaEmail'];
        if(emailIds!=null)
        {
            for(Logger_Email__c LEM:emailIds)
            {
                emaillist.add(LEM.Name);
            }
        }

        String header= Status;
        Messaging.SingleEmailMessage emailwithattch = new Messaging.SingleEmailMessage(); 
        emailwithattch.setSubject(subject);   
        emailwithattch.setToAddresses(emaillist);
        emailwithattch.setPlainTextBody(header);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {emailwithattch});
    }
    

    
}