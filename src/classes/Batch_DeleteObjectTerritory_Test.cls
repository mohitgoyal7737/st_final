/**********************************************************************************************
Author      : Himanshu Tariyal (A0994)
Date        : 23rd September'2020
Description : Test class for Batch_DeleteObjectTerritory
Revision(s) : v1.0
**********************************************************************************************/
@isTest
public with sharing class Batch_DeleteObjectTerritory_Test 
{
    public static testMethod void testMethod1() 
    {
    	String className = 'Batch_DeleteObjectTerritory_Test';

    	AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    	SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Load_Type__c = 'Full Load';
        SnTDMLSecurityUtil.insertRecords(countr,className);

    	Scheduler_Log__c schLog = TestDataFactory.createSchLog('Full Load','ObjectTerritory');
        SnTDMLSecurityUtil.insertRecords(schLog,className);

        Staging_Position_Account__c spa = new Staging_Position_Account__c();
        spa.Country__c = countr.Id;
        SnTDMLSecurityUtil.insertRecords(spa,className);

        System.Test.startTest();

        String nameSpace = MCCP_Utility.sntNamespace(className);

        List<String> PROD_PRIORITY_READ_FIELD = new List<String>{nameSpace+'Cycle__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Scheduler_Log__c.SObjectType, PROD_PRIORITY_READ_FIELD, false));
        Database.executeBatch(new Batch_DeleteObjectTerritory());

        System.Test.stopTest();
    }

    public static testMethod void testMethod2() 
    {
    	String className = 'Batch_DeleteObjectTerritory_Test';

    	AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    	SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Load_Type__c = 'Delta';
        SnTDMLSecurityUtil.insertRecords(countr,className);

    	Scheduler_Log__c schLog = TestDataFactory.createSchLog('Delta','ObjectTerritory');
        SnTDMLSecurityUtil.insertRecords(schLog,className);

        Staging_Position_Account__c spa = new Staging_Position_Account__c();
        spa.Record_Status__c = 'Updated';
        spa.Country__c = countr.Id;
        SnTDMLSecurityUtil.insertRecords(spa,className);

        System.Test.startTest();

        String nameSpace = MCCP_Utility.sntNamespace(className);

        List<String> PROD_PRIORITY_READ_FIELD = new List<String>{nameSpace+'Cycle__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Scheduler_Log__c.SObjectType, PROD_PRIORITY_READ_FIELD, false));
        Database.executeBatch(new Batch_DeleteObjectTerritory());

        System.Test.stopTest();
    }

    public static testMethod void testMethod3() 
    {
    	String className = 'Batch_DeleteObjectTerritory_Test';

    	AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    	SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Load_Type__c = 'Full Load';
        SnTDMLSecurityUtil.insertRecords(countr,className);

    	Scheduler_Log__c schLog = TestDataFactory.createSchLog('Full Load','ObjectTerritory');
        SnTDMLSecurityUtil.insertRecords(schLog,className);

        Staging_Position_Account__c spa = new Staging_Position_Account__c();
        SnTDMLSecurityUtil.insertRecords(spa,className);

        System.Test.startTest();

        String nameSpace = MCCP_Utility.sntNamespace(className);

        List<String> PROD_PRIORITY_READ_FIELD = new List<String>{nameSpace+'Cycle__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Scheduler_Log__c.SObjectType, PROD_PRIORITY_READ_FIELD, false));
        Database.executeBatch(new Batch_DeleteObjectTerritory());

        System.Test.stopTest();
    }
}