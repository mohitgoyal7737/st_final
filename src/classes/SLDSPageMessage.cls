public class SLDSPageMessage {

    public string msg {
        get;
        set;
    }
    public string type {
        get;
        set;
    }

    public static List <SLDSPageMessage> add(List<SLDSPageMessage> pageMessages,string msg, string type) {

        if (PageMessages == null) {
            PageMessages = new List <SLDSPageMessage> ();
        }

        SLDSPageMessage msgObj = new SLDSPageMessage();
        msgObj.type = type.tolowercase();
        msgObj.msg = msg;
        Boolean contains = false;
        
        for(SLDSPageMessage pgMsg:pageMessages){
           
           if(pgMsg.msg == msg && pgMsg.type == type){
             contains = true;
             break;
           }
        
        }
        
        if(!contains)
        PageMessages.add(msgObj);
        
        return PageMessages;

    }
}