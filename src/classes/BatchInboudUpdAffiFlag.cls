global class BatchInboudUpdAffiFlag implements Database.Batchable < sObject > , Database.Stateful, schedulable {
    public Integer recordsProcessed = 0;
    public String batchID;
    global DateTime lastjobDate = null;
    global String query;
   public list < AxtriaSalesIQTM__Account_Affiliation__c  > afflist {get;set;}
    
    Set<id> accAffId;
    
    global BatchInboudUpdAffiFlag(Set<id> affIds) { //set<String> Accountid
    //  System.debug('===========affId==IN =BatchInboudUpdAffiFlag::'+affId);
      
        accAffId = new Set<id>(affIds);
        system.debug(accAffId );
      
        query='Select Id,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Parent_Account__c from AxtriaSalesIQTM__Account_Affiliation__c  where AxtriaSalesIQTM__Account__c IN:accAffId or AxtriaSalesIQTM__Parent_Account__c IN:accAffId ';

        System.debug(' BATCH UPDATE AFF FLAG QUERY IS:' + query);
        //Create a new record for Scheduler Batch with values, Job_Type, Job_Status__c as Failed, Created_Date__c as Today’s Date.
    }
    
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
       
        return Database.getQueryLocator(query);
    }
    
    
    public void execute(System.SchedulableContext SC) {
    }
    
    
    global void execute(Database.BatchableContext bc, List < AxtriaSalesIQTM__Account_Affiliation__c  > records) {
        // process each batch of records
         System.debug('++++++++++++++Records Size is:'+records.size());                           
         Set<id> AclList = new Set < id > ();
        for (AxtriaSalesIQTM__Account_Affiliation__c  SAA: records) {
            if(SAA.AxtriaSalesIQTM__Account__c!=null){
            AclList.add(SAA.AxtriaSalesIQTM__Account__c);
            }
         if(SAA.AxtriaSalesIQTM__Parent_Account__c!=null){
            AclList.add(SAA.AxtriaSalesIQTM__Parent_Account__c);
            }
            
        }
                                     
       // List < Account > AccountList = [select id, Affiliation_Delta_Flag__c from Account where External_Account_Number__c IN: AclList];
        
        
        List < Account > accounts = new List < Account > ();
        for (id acc: AclList) {
           Account account = new Account(id=acc);
           if(!account.Affiliation_Delta_Flag__c){
                account.Affiliation_Delta_Flag__c=true;
                accounts.add(account);
                system.debug(' INSIDE FOR LOOP recordsProcessed+' + recordsProcessed);
           }
           recordsProcessed++;
           //comments
            
        }
       System.debug('============accounts Size==='+accounts.size());
        if(accounts!=null && accounts.size()>0){
            update accounts;
        }
        
        
    }
    
    
    global void finish(Database.BatchableContext bc) {
        // execute any post-processing operations
        System.debug(recordsProcessed + ' records processed. ');
        Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID);
      
    }
}