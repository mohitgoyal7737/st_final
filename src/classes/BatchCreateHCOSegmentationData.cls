global class BatchCreateHCOSegmentationData implements Database.Batchable<sObject> {
    public String query;
    public Map<String,Map<String,Decimal>> hcoToMapOfParamValMap;
    public String destinationRuleId;
    public String sourceRuleId;
    public Set<String> hcoSet;

    public static Boolean isInteger(String s){
        Boolean ReturnValue;
       /* try{
            Integer.valueOf(s);
            ReturnValue = TRUE; 
        } catch (Exception e) {
            ReturnValue = FALSE;
        }*/
        return ReturnValue;
    }

    global BatchCreateHCOSegmentationData(String sourceRuleId,String destinationRuleId,Map<String,Map<String,Decimal>> hcoToMapOfParamValMap) {
       /* this.hcoToMapOfParamValMap = hcoToMapOfParamValMap;
        this.destinationRuleId = destinationRuleId;
        this.sourceRuleId = sourceRuleId;
        hcoSet = hcoToMapOfParamValMap.keySet();
        query = 'Select Id,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Position__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Account__c in :hcoSet';
        this.query = query;*/
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
       // System.debug('query --> ' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Account__c> scope) {
        
       /* List<Account_Compute_Final__c> acfListToInsert = new List<Account_Compute_Final__c>();
        Account_Compute_Final__c acf;
        Integer counter = 1;
        for(AxtriaSalesIQTM__Position_Account__c posAcc : scope)
        {
            if(hcoToMapOfParamValMap.containsKey(posAcc.AxtriaSalesIQTM__Account__c))
            {
                counter = 1;
                acf = new Account_Compute_Final__c();
                acf.Measure_Master__c = destinationRuleId;
                acf.Physician_2__c = posAcc.AxtriaSalesIQTM__Account__c;
                acf.Position_ID__c = posAcc.AxtriaSalesIQTM__Position__c;
                acf.Physician__c = posAcc.Id;
                acf.External_Id__c = posAcc.Id + '_' + destinationRuleId;
                for(String str : hcoToMapOfParamValMap.get(posAcc.AxtriaSalesIQTM__Account__c).keySet())
                {
                    Decimal d = hcoToMapOfParamValMap.get(posAcc.AxtriaSalesIQTM__Account__c).get(str);
                    Boolean flag = isInteger(String.valueOf(d));
                    d = flag == true? d : d.setScale(3);
                    acf.put('Output_Name_'+counter+'__c',str);
                    acf.put('Output_Value_'+counter+'__c',String.valueOf(d));
                    counter++;
                }
                acfListToInsert.add(acf);
            }
        }
        upsert acfListToInsert External_Id__c;*/
    }

    global void finish(Database.BatchableContext BC) {
       // Database.executeBatch(new BatchCreateStagingSurveyDataForHCO(sourceRuleId,destinationRuleId),2000);
    }
}