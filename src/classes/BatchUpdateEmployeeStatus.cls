global class BatchUpdateEmployeeStatus implements Database.Batchable<sObject>,schedulable
{
    string query;
    public String batchID;
    public Date todayDate = System.today();
    public map<Id, list<AxtriaSalesIQTM__Position_Employee__c>> mapPostoPosEmp = new map<Id, list<AxtriaSalesIQTM__Position_Employee__c>>();
    public map<Id, list<AxtriaSalesIQTM__Position_Employee__c>> mapEmptoPosEmp = new map<Id, list<AxtriaSalesIQTM__Position_Employee__c>>();
    public list<AxtriaSalesIQTM__Position_Employee__c> posEmpUp = new list<AxtriaSalesIQTM__Position_Employee__c>();
     
    global BatchUpdateEmployeeStatus(String ID)
    {   batchID=ID;
        query = 'Select id, AxtriaSalesIQTM__Employee__c, ' +
            'AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.id, '+
            'AxtriaSalesIQTM__Employee__r.id, AxtriaSalesIQTM__Effective_Start_Date__c From ' +
            'AxtriaSalesIQTM__Position_Employee__c Where AxtriaSalesIQTM__Assignment_Type__c = \'Primary\' and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__IsMaster__c = true ' +
            'ORDER BY AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Effective_End_Date__c ';        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {         
        return Database.getQueryLocator(query);
    }
    
    global void execute(SchedulableContext SC){
        BatchUpdateEmployeeStatus execfile = new BatchUpdateEmployeeStatus(batchID);
        database.executeBatch(execfile,100);
    }
    
    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Employee__c> posEmp)
    {
        if(posEmp.size()>0)
        {
            for(AxtriaSalesIQTM__Position_Employee__c pe : posEmp)
            {
                // Fill mapPostoPosEmp
                if(mapPostoPosEmp.containsKey(pe.AxtriaSalesIQTM__Position__c))
                {
                   // Id ids = pe.AxtriaSalesIQTM__Position__c;
                    mapPostoPosEmp.get(pe.AxtriaSalesIQTM__Position__c).add(pe);
                }
                if(!mapPostoPosEmp.containsKey(pe.AxtriaSalesIQTM__Position__c))
                {
                    Id posId = pe.AxtriaSalesIQTM__Position__c;
                    list<AxtriaSalesIQTM__Position_Employee__c> temppe = new list<AxtriaSalesIQTM__Position_Employee__c>();
                    temppe.add(pe);
                    mapPostoPosEmp.put(posId, temppe);
                }
                
                // Fill mapEmptoPosEmp
                if(mapEmptoPosEmp.containsKey(pe.AxtriaSalesIQTM__Employee__c))
                {
                    //Id empId = pe.AxtriaSalesIQTM__Employee__c;
                    mapEmptoPosEmp.get(pe.AxtriaSalesIQTM__Employee__c).add(pe);
                }
                if(!mapEmptoPosEmp.containsKey(pe.AxtriaSalesIQTM__Employee__c))
                {
                    Id empId = pe.AxtriaSalesIQTM__Employee__c;
                    list<AxtriaSalesIQTM__Position_Employee__c> temppe = new list<AxtriaSalesIQTM__Position_Employee__c>();
                    temppe.add(pe);
                    mapEmptoPosEmp.put(empId, temppe);
                }
            }    
           
            map<id, AxtriaSalesIQTM__Position__c> mapPos= new map<id, AxtriaSalesIQTM__Position__c>();
            
            for(AxtriaSalesIQTM__Position__c pos: [select id,AxtriaSalesIQTM__Employee__c,AxtriaSalesIQTM__Employee__r.Id,AxtriaSalesIQTM__Assignment_status__c
                                                   from AxtriaSalesIQTM__Position__c where 
                                                   id in: mapPostoPosEmp.keySet()])
            {
                mapPos.put(pos.id, pos);
            }
            map<id, AxtriaSalesIQTM__Employee__c> mapEmp= new map<id, AxtriaSalesIQTM__Employee__c>();
            for(AxtriaSalesIQTM__Employee__c emp: [select id,AxtriaSalesIQTM__Field_Status__c
                                                   from AxtriaSalesIQTM__Employee__c where 
                                                   id in: mapEmptoPosEmp.keySet()])
            {
                mapEmp.put(emp.id, emp);
            }
            
            list<AxtriaSalesIQTM__Position__c> lstPos = new list<AxtriaSalesIQTM__Position__c>();
            set<AxtriaSalesIQTM__Position__c> lstPosSet = new set<AxtriaSalesIQTM__Position__c>();
            list<AxtriaSalesIQTM__Employee__c> lstEmp = new list<AxtriaSalesIQTM__Employee__c>();
            set<AxtriaSalesIQTM__Employee__c> lstEmpSet = new set<AxtriaSalesIQTM__Employee__c>();
            for(AxtriaSalesIQTM__Position_Employee__c pe : posEmp)
            {
                AxtriaSalesIQTM__Position__c position = new AxtriaSalesIQTM__Position__c();
                position = mapPos.get(pe.AxtriaSalesIQTM__Position__r.id);  
                System.debug('++++++ Position = ' + position);
                AxtriaSalesIQTM__Employee__c employee = new AxtriaSalesIQTM__Employee__c();  
                employee = mapEmp.get(pe.AxtriaSalesIQTM__Employee__r.id);
                System.debug('++++++ Employee = ' + employee);
                
                //if(pe.AxtriaSalesIQTM__Assignment_Type__c == 'Primary')
                
                if(pe.AxtriaSalesIQTM__Effective_End_Date__c <= todayDate)
                {          
                    if(position != Null)
                    {
                        if(position.AxtriaSalesIQTM__Employee__c != Null)
                        {
                            position.AxtriaSalesIQTM__Employee__c= Null;
                        }
                        position.AxtriaSalesIQTM__Assignment_status__c = 'Vacant';                                            
                    }
                    if(employee != null)
                    {
                        employee.AxtriaSalesIQTM__Field_Status__c = 'Unassigned';
                        employee.CurrentPositionId__c='';
                        employee.AxtriaSalesIQTM__Current_Territory__c=null;
                    }
                    System.debug('++++ Entered Effective_End_Date Check ++++');
                }
                
                if(pe.AxtriaSalesIQTM__Effective_Start_Date__c <= todayDate && pe.AxtriaSalesIQTM__Effective_End_Date__c > todayDate)
                {
                    if(position != Null)
                    {
                        position.AxtriaSalesIQTM__Employee__c = pe.AxtriaSalesIQTM__Employee__c;
                        position.AxtriaSalesIQTM__Assignment_status__c = 'Filled';                        
                    }
                    if(employee != null)
                    {
                        employee.AxtriaSalesIQTM__Field_Status__c = 'Assigned'; 
                        employee.AxtriaSalesIQTM__Current_Territory__c = pe.AxtriaSalesIQTM__Position__c;                 
                    }
                    System.debug('++++ Entered Effective_Start_Date Check ++++');
                }
                
                if(position != Null)
                  lstPos.add(position);
                if(employee != Null)
                  lstEmp.add(employee);
            }
      
            lstPosSet.addAll(lstPos);
            lstEmpSet.addAll(lstEmp);
            lstPos = new list<AxtriaSalesIQTM__Position__c>();
            lstPos.addAll(lstPosSet);
            lstEmp = new list<AxtriaSalesIQTM__Employee__c>();
            lstEmp.addAll(lstEmpSet);
            
            if(lstPos.size()>0)
                update lstPos;
            system.debug('lstPos++++++'+lstPos);
            if(lstEmp.size()>0)
                update lstEmp;  
            system.debug('lstEmp++++++'+lstEmp);
        }
            
            
    }
    
    global void finish(Database.BatchableContext BC)
    { 
        BatchUpdatePositionVacancy obj= new BatchUpdatePositionVacancy(batchID);
        database.executeBatch(obj);
    }
   
}