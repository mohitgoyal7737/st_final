@isTest
public class Snt_FLS_SecurityUtil_Test {
    private static testMethod void firstTest(){
        //create a new test user.
        User loggedInUser = new User(id=UserInfo.getUserId());  
        
        System.Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.debug('RULEPARAMETER_READ_FIELD'+RULEPARAMETER_READ_FIELD);
                
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkInsert(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));  
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkUpdate(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkObjectIsDeletable(Rule_Parameter__c.SObjectType));
            
            SnT_FLS_SecurityUtil.OperationType Op = SnT_FLS_SecurityUtil.OperationType.C;
            SnT_FLS_SecurityUtil.CrudException sc = new SnT_FLS_SecurityUtil.CrudException(Op,Rule_Parameter__c.SObjectType);
            SnT_FLS_SecurityUtil.OperationType Op1 = SnT_FLS_SecurityUtil.OperationType.R;
            SnT_FLS_SecurityUtil.CrudException sc1 = new SnT_FLS_SecurityUtil.CrudException(Op1,Rule_Parameter__c.SObjectType);
            SnT_FLS_SecurityUtil.OperationType Op2 = SnT_FLS_SecurityUtil.OperationType.U;
            SnT_FLS_SecurityUtil.CrudException sc2 = new SnT_FLS_SecurityUtil.CrudException(Op2,Rule_Parameter__c.SObjectType);
            SnT_FLS_SecurityUtil.OperationType Op3 = SnT_FLS_SecurityUtil.OperationType.D;
            SnT_FLS_SecurityUtil.CrudException sc3 = new SnT_FLS_SecurityUtil.CrudException(Op3,Rule_Parameter__c.SObjectType);
        	
            Schema.DescribeFieldResult fr = Rule_Parameter__c.Parameter__c.getDescribe();
            Schema.sObjectField f = fr.getSObjectField();
            SnT_FLS_SecurityUtil.OperationType Ot = SnT_FLS_SecurityUtil.OperationType.C;
        	SnT_FLS_SecurityUtil.FlsException sf = new SnT_FLS_SecurityUtil.FlsException(Ot,Rule_Parameter__c.SObjectType,f);
            SnT_FLS_SecurityUtil.OperationType Ot1 = SnT_FLS_SecurityUtil.OperationType.R;
        	SnT_FLS_SecurityUtil.FlsException sf1 = new SnT_FLS_SecurityUtil.FlsException(Ot1,Rule_Parameter__c.SObjectType,f);
            SnT_FLS_SecurityUtil.OperationType Ot2 = SnT_FLS_SecurityUtil.OperationType.U;
        	SnT_FLS_SecurityUtil.FlsException sf2 = new SnT_FLS_SecurityUtil.FlsException(Ot2,Rule_Parameter__c.SObjectType,f);
            SnT_FLS_SecurityUtil.OperationType Ot3 = SnT_FLS_SecurityUtil.OperationType.D;
        	SnT_FLS_SecurityUtil.FlsException sf3 = new SnT_FLS_SecurityUtil.FlsException(Ot3,Rule_Parameter__c.SObjectType,f);
        
        }
        System.Test.stopTest();
    }
}