/**********************************************************************************************
Author      : Himanshu Tariyal (A0994)
Date        : 24th September'2020
Description : Test class for BatchPopulatePAMetrics
Revision(s) : v1.0
**********************************************************************************************/
@isTest
public with sharing class BatchPopulatePAMetricsTest 
{
    static testMethod void testMethod1() 
    {
        String className = 'BatchPopulatePAMetricsTest';

        Account acc= TestDataFactory.createAccount();
        acc.AccountNumber = 'ES206265';
        acc.Type = 'HCO';
        SnTDMLSecurityUtil.insertRecords(acc,className);

        Account acc1= TestDataFactory.createAccount();
        acc1.AccountNumber = 'ES206211';
        acc1.Type = 'HCO';
        SnTDMLSecurityUtil.insertRecords(acc1,className);

        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Name = 'USA';
        SnTDMLSecurityUtil.insertRecords(countr,className);
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);

        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins1,className);
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
        SnTDMLSecurityUtil.insertRecords(scen,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        SnTDMLSecurityUtil.insertRecords(teamins,className);

        AxtriaSalesIQTM__Team_Instance__c teamins2 = TestDataFactory.createTeamInstance(team);
        teamins2.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins2.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        SnTDMLSecurityUtil.insertRecords(teamins2,className);
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        SnTDMLSecurityUtil.insertRecords(pcc,className);
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        SnTDMLSecurityUtil.insertRecords(mmc,className);
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        SnTDMLSecurityUtil.insertRecords(pos,className);

        AxtriaSalesIQTM__Position__c pos1= TestDataFactory.createPosition(team,teamins2);
        SnTDMLSecurityUtil.insertRecords(pos1,className);

        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        SnTDMLSecurityUtil.insertRecords(posAccount,className);

        AxtriaSalesIQTM__Position_Account__c posAccount1 = TestDataFactory.createPositionAccount(acc1,pos1,teamins2);
        SnTDMLSecurityUtil.insertRecords(posAccount1,className);

        Product_Priority__c pPriority = TestDataFactory.productPriority();
        SnTDMLSecurityUtil.insertRecords(pPriority,className);
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        positionAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c = true;
        positionAccountCallPlan.Final_TCF_Approved__c = 3.0;
        positionAccountCallPlan.Final_TCF__c = 3.0;
        positionAccountCallPlan.P1__c = 'GIST';
        SnTDMLSecurityUtil.insertRecords(positionAccountCallPlan,className);

        temp_Obj__c newpa= new temp_Obj__c();
        newpa.AccountNumber__c = acc.AccountNumber;
        newpa.Territory_ID__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        newpa.Team_Name__c = teamins.AxtriaSalesIQTM__Team__r.name;
        newpa.Account_Type__c = acc.type;
        newpa.Object__c = 'Customer Metrics Load';
        newpa.Status__c = 'New';
        newpa.Team_Instance_Name__c = teamins1.Name;
        SnTDMLSecurityUtil.insertRecords(newpa,className);

        System.Test.startTest();

        String nameSpace = MCCP_Utility.sntNamespace(className);
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));

        BatchPopulatePAMetrics obj=new BatchPopulatePAMetrics(teamins1.Id); 
        Database.executeBatch(obj);

        System.Test.stopTest();
    }

    static testMethod void testMethod2() 
    {
        String className = 'BatchPopulatePAMetricsTest';

        Account acc= TestDataFactory.createAccount();
        acc.AccountNumber = 'ES206265';
        acc.Type = 'HCO';
        SnTDMLSecurityUtil.insertRecords(acc,className);

        Account acc1= TestDataFactory.createAccount();
        acc1.AccountNumber = 'ES206211';
        acc1.Type = 'HCO';
        SnTDMLSecurityUtil.insertRecords(acc1,className);

        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Name = 'USA';
        SnTDMLSecurityUtil.insertRecords(countr,className);
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);

        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins1,className);
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
        SnTDMLSecurityUtil.insertRecords(scen,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        SnTDMLSecurityUtil.insertRecords(teamins,className);

        AxtriaSalesIQTM__Team_Instance__c teamins2 = TestDataFactory.createTeamInstance(team);
        teamins2.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins2.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        SnTDMLSecurityUtil.insertRecords(teamins2,className);
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        SnTDMLSecurityUtil.insertRecords(pcc,className);
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        SnTDMLSecurityUtil.insertRecords(mmc,className);
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        SnTDMLSecurityUtil.insertRecords(pos,className);

        AxtriaSalesIQTM__Position__c pos1= TestDataFactory.createPosition(team,teamins2);
        SnTDMLSecurityUtil.insertRecords(pos1,className);

        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        SnTDMLSecurityUtil.insertRecords(posAccount,className);

        AxtriaSalesIQTM__Position_Account__c posAccount1 = TestDataFactory.createPositionAccount(acc1,pos1,teamins2);
        SnTDMLSecurityUtil.insertRecords(posAccount1,className);

        Product_Priority__c pPriority = TestDataFactory.productPriority();
        SnTDMLSecurityUtil.insertRecords(pPriority,className);
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        positionAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c = true;
        positionAccountCallPlan.Final_TCF_Approved__c = 3.0;
        positionAccountCallPlan.Final_TCF__c = 3.0;
        positionAccountCallPlan.P1__c = 'GIST';
        SnTDMLSecurityUtil.insertRecords(positionAccountCallPlan,className);

        temp_Obj__c newpa= new temp_Obj__c();
        newpa.AccountNumber__c = acc.AccountNumber;
        newpa.Territory_ID__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        newpa.Team_Name__c = teamins.AxtriaSalesIQTM__Team__r.name;
        newpa.Account_Type__c = acc.type;
        newpa.Object__c = 'Customer Metrics Load';
        newpa.Status__c = 'New';
        newpa.Team_Instance_Name__c = teamins1.Name;
        SnTDMLSecurityUtil.insertRecords(newpa,className);

        System.Test.startTest();

        String nameSpace = MCCP_Utility.sntNamespace(className);
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));

        BatchPopulatePAMetrics obj=new BatchPopulatePAMetrics('All'); 
        Database.executeBatch(obj);

        System.Test.stopTest();
    }

    static testMethod void testMethod3() 
    {
        String className = 'BatchPopulatePAMetricsTest';

        Account acc= TestDataFactory.createAccount();
        acc.AccountNumber = 'ES206265';
        acc.Type = 'HCO';
        SnTDMLSecurityUtil.insertRecords(acc,className);

        Account acc1= TestDataFactory.createAccount();
        acc1.AccountNumber = 'ES206211';
        acc1.Type = 'HCO';
        SnTDMLSecurityUtil.insertRecords(acc1,className);

        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Name = 'USA';
        SnTDMLSecurityUtil.insertRecords(countr,className);
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);

        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins1,className);
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
        SnTDMLSecurityUtil.insertRecords(scen,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        SnTDMLSecurityUtil.insertRecords(teamins,className);

        AxtriaSalesIQTM__Team_Instance__c teamins2 = TestDataFactory.createTeamInstance(team);
        teamins2.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins2.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        SnTDMLSecurityUtil.insertRecords(teamins2,className);
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        SnTDMLSecurityUtil.insertRecords(pcc,className);
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        SnTDMLSecurityUtil.insertRecords(mmc,className);
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        SnTDMLSecurityUtil.insertRecords(pos,className);

        AxtriaSalesIQTM__Position__c pos1= TestDataFactory.createPosition(team,teamins2);
        SnTDMLSecurityUtil.insertRecords(pos1,className);

        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        SnTDMLSecurityUtil.insertRecords(posAccount,className);

        AxtriaSalesIQTM__Position_Account__c posAccount1 = TestDataFactory.createPositionAccount(acc1,pos1,teamins2);
        SnTDMLSecurityUtil.insertRecords(posAccount1,className);

        Product_Priority__c pPriority = TestDataFactory.productPriority();
        SnTDMLSecurityUtil.insertRecords(pPriority,className);
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        positionAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c = true;
        positionAccountCallPlan.Final_TCF_Approved__c = 3.0;
        positionAccountCallPlan.Final_TCF__c = 3.0;
        positionAccountCallPlan.P1__c = 'GIST';
        SnTDMLSecurityUtil.insertRecords(positionAccountCallPlan,className);

        AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
        cr.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.Id;
        SnTDMLSecurityUtil.insertRecords(cr,className);

        temp_Obj__c newpa= new temp_Obj__c();
        newpa.Change_Request__c = cr.Id;
        newpa.AccountNumber__c = acc.AccountNumber;
        newpa.Territory_ID__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        newpa.Team_Name__c = teamins.AxtriaSalesIQTM__Team__r.name;
        newpa.Account_Type__c = acc.type;
        newpa.Object__c = 'Customer Metrics Load';
        newpa.Status__c = 'New';
        newpa.Team_Instance_Name__c = teamins1.Name;
        SnTDMLSecurityUtil.insertRecords(newpa,className);

        System.Test.startTest();

        String nameSpace = MCCP_Utility.sntNamespace(className);
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));

        BatchPopulatePAMetrics obj=new BatchPopulatePAMetrics(teamins1.Id,cr.Id); 
        Database.executeBatch(obj);

        System.Test.stopTest();
    }

    static testMethod void testMethod4() 
    {
        String className = 'BatchPopulatePAMetricsTest';

        Account acc= TestDataFactory.createAccount();
        acc.AccountNumber = 'ES206265';
        acc.Type = 'HCO';
        SnTDMLSecurityUtil.insertRecords(acc,className);

        Account acc1= TestDataFactory.createAccount();
        acc1.AccountNumber = 'ES206211';
        acc1.Type = 'HCO';
        SnTDMLSecurityUtil.insertRecords(acc1,className);

        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Name = 'USA';
        SnTDMLSecurityUtil.insertRecords(countr,className);
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);

        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins1,className);
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
        SnTDMLSecurityUtil.insertRecords(scen,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        SnTDMLSecurityUtil.insertRecords(teamins,className);

        AxtriaSalesIQTM__Team_Instance__c teamins2 = TestDataFactory.createTeamInstance(team);
        teamins2.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins2.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        SnTDMLSecurityUtil.insertRecords(teamins2,className);
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        SnTDMLSecurityUtil.insertRecords(pcc,className);
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        SnTDMLSecurityUtil.insertRecords(mmc,className);
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        SnTDMLSecurityUtil.insertRecords(pos,className);

        AxtriaSalesIQTM__Position__c pos1= TestDataFactory.createPosition(team,teamins2);
        SnTDMLSecurityUtil.insertRecords(pos1,className);

        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        SnTDMLSecurityUtil.insertRecords(posAccount,className);

        AxtriaSalesIQTM__Position_Account__c posAccount1 = TestDataFactory.createPositionAccount(acc1,pos1,teamins2);
        SnTDMLSecurityUtil.insertRecords(posAccount1,className);

        Product_Priority__c pPriority = TestDataFactory.productPriority();
        SnTDMLSecurityUtil.insertRecords(pPriority,className);
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        positionAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c = true;
        positionAccountCallPlan.Final_TCF_Approved__c = 3.0;
        positionAccountCallPlan.Final_TCF__c = 3.0;
        positionAccountCallPlan.P1__c = 'GIST';
        SnTDMLSecurityUtil.insertRecords(positionAccountCallPlan,className);

        AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
        cr.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.Id;
        SnTDMLSecurityUtil.insertRecords(cr,className);

        temp_Obj__c newpa= new temp_Obj__c();
        newpa.Change_Request__c = cr.Id;
        newpa.AccountNumber__c = acc.AccountNumber;
        newpa.Territory_ID__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        newpa.Team_Name__c = teamins.AxtriaSalesIQTM__Team__r.name;
        newpa.Account_Type__c = acc.type;
        newpa.Object__c = 'Customer Metrics Load';
        newpa.Status__c = 'New';
        newpa.Team_Instance_Name__c = teamins1.Name;
        SnTDMLSecurityUtil.insertRecords(newpa,className);

        System.Test.startTest();

        String nameSpace = MCCP_Utility.sntNamespace(className);
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));

        BatchPopulatePAMetrics obj=new BatchPopulatePAMetrics('All',cr.Id); 
        Database.executeBatch(obj);

        System.Test.stopTest();
    }

    static testMethod void testMethod5() 
    {
        String className = 'BatchPopulatePAMetricsTest';

        Account acc= TestDataFactory.createAccount();
        acc.AccountNumber = 'ES206265';
        acc.Type = 'HCO';
        SnTDMLSecurityUtil.insertRecords(acc,className);

        Account acc1= TestDataFactory.createAccount();
        acc1.AccountNumber = 'ES206211';
        acc1.Type = 'HCO';
        SnTDMLSecurityUtil.insertRecords(acc1,className);

        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Name = 'USA';
        SnTDMLSecurityUtil.insertRecords(countr,className);
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);

        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins1,className);
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
        SnTDMLSecurityUtil.insertRecords(scen,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        SnTDMLSecurityUtil.insertRecords(teamins,className);

        AxtriaSalesIQTM__Team_Instance__c teamins2 = TestDataFactory.createTeamInstance(team);
        teamins2.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins2.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        SnTDMLSecurityUtil.insertRecords(teamins2,className);
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        SnTDMLSecurityUtil.insertRecords(pcc,className);
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        SnTDMLSecurityUtil.insertRecords(mmc,className);
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        SnTDMLSecurityUtil.insertRecords(pos,className);

        AxtriaSalesIQTM__Position__c pos1= TestDataFactory.createPosition(team,teamins2);
        SnTDMLSecurityUtil.insertRecords(pos1,className);

        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        SnTDMLSecurityUtil.insertRecords(posAccount,className);

        AxtriaSalesIQTM__Position_Account__c posAccount1 = TestDataFactory.createPositionAccount(acc1,pos1,teamins2);
        SnTDMLSecurityUtil.insertRecords(posAccount1,className);

        Product_Priority__c pPriority = TestDataFactory.productPriority();
        SnTDMLSecurityUtil.insertRecords(pPriority,className);
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        positionAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c = true;
        positionAccountCallPlan.Final_TCF_Approved__c = 3.0;
        positionAccountCallPlan.Final_TCF__c = 3.0;
        positionAccountCallPlan.P1__c = 'GIST';
        SnTDMLSecurityUtil.insertRecords(positionAccountCallPlan,className);

        AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
        cr.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.Id;
        SnTDMLSecurityUtil.insertRecords(cr,className);

        temp_Obj__c newpa= new temp_Obj__c();
        newpa.Change_Request__c = cr.Id;
        newpa.AccountNumber__c = acc.AccountNumber;
        newpa.Territory_ID__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        newpa.Team_Name__c = teamins.AxtriaSalesIQTM__Team__r.name;
        newpa.Account_Type__c = acc.type;
        newpa.Object__c = 'Customer Metrics Load';
        newpa.Status__c = 'New';
        newpa.Team_Instance_Name__c = teamins1.Name;
        SnTDMLSecurityUtil.insertRecords(newpa,className);

        String alignNmsp = MCCP_Utility.alignmentNamespace();
        String nameSpace = MCCP_Utility.sntNamespace(className);

        Source_to_Destination_Mapping__c sdm = new Source_to_Destination_Mapping__c();
        sdm.Team_Instance__c = teamins1.Id;
        sdm.Load_Type__c = 'Customer Metrics Load';
        sdm.Source_Object_Field__c = 'Metric1__c';
        sdm.Destination_Object_Field__c = alignNmsp+'Metric1__c';
        SnTDMLSecurityUtil.insertRecords(sdm,className);

        System.Test.startTest();

        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));

        BatchPopulatePAMetrics obj=new BatchPopulatePAMetrics(teamins1.Id,cr.Id,true); 
        Database.executeBatch(obj);

        System.Test.stopTest();
    }

    static testMethod void testMethod6() 
    {
        String className = 'BatchPopulatePAMetricsTest';

        Account acc= TestDataFactory.createAccount();
        acc.AccountNumber = 'ES206265';
        acc.Type = 'HCO';
        SnTDMLSecurityUtil.insertRecords(acc,className);

        Account acc1= TestDataFactory.createAccount();
        acc1.AccountNumber = 'ES206211';
        acc1.Type = 'HCO';
        SnTDMLSecurityUtil.insertRecords(acc1,className);

        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Name = 'USA';
        SnTDMLSecurityUtil.insertRecords(countr,className);
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);

        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins1,className);
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
        SnTDMLSecurityUtil.insertRecords(scen,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        SnTDMLSecurityUtil.insertRecords(teamins,className);

        AxtriaSalesIQTM__Team_Instance__c teamins2 = TestDataFactory.createTeamInstance(team);
        teamins2.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins2.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        SnTDMLSecurityUtil.insertRecords(teamins2,className);
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        SnTDMLSecurityUtil.insertRecords(pcc,className);
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        SnTDMLSecurityUtil.insertRecords(mmc,className);
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        SnTDMLSecurityUtil.insertRecords(pos,className);

        AxtriaSalesIQTM__Position__c pos1= TestDataFactory.createPosition(team,teamins2);
        SnTDMLSecurityUtil.insertRecords(pos1,className);

        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        SnTDMLSecurityUtil.insertRecords(posAccount,className);

        AxtriaSalesIQTM__Position_Account__c posAccount1 = TestDataFactory.createPositionAccount(acc1,pos1,teamins2);
        SnTDMLSecurityUtil.insertRecords(posAccount1,className);

        Product_Priority__c pPriority = TestDataFactory.productPriority();
        SnTDMLSecurityUtil.insertRecords(pPriority,className);
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        positionAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c = true;
        positionAccountCallPlan.Final_TCF_Approved__c = 3.0;
        positionAccountCallPlan.Final_TCF__c = 3.0;
        positionAccountCallPlan.P1__c = 'GIST';
        SnTDMLSecurityUtil.insertRecords(positionAccountCallPlan,className);

        AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
        cr.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.Id;
        SnTDMLSecurityUtil.insertRecords(cr,className);

        temp_Obj__c newpa= new temp_Obj__c();
        newpa.Change_Request__c = cr.Id;
        newpa.AccountNumber__c = acc.AccountNumber;
        newpa.Territory_ID__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        newpa.Team_Name__c = teamins.AxtriaSalesIQTM__Team__r.name;
        newpa.Account_Type__c = acc.type;
        newpa.Object__c = 'Customer Metrics Load';
        newpa.Status__c = 'New';
        newpa.Team_Instance_Name__c = teamins1.Name;
        SnTDMLSecurityUtil.insertRecords(newpa,className);

        System.Test.startTest();

        String nameSpace = MCCP_Utility.sntNamespace(className);
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));

        BatchPopulatePAMetrics obj=new BatchPopulatePAMetrics('All',cr.Id); 
        Database.executeBatch(obj);

        System.Test.stopTest();
    }
}