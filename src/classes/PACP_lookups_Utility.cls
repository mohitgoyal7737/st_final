global class PACP_lookups_Utility implements Database.Batchable<sObject> {
    public String query;
    public String selectedTeamInstance;
    public String selectedTeam;
    public string country;
    //public AxtriaSalesIQTM__Team_Instance__C notSelectedTeamInstance;
    Map<String,Id> teamInstanceNametoIDMap;
    Map<String,string> teamInstanceNametoTeamNameMap;
    Map<String,boolean> teamInstanceNametoAffiliationMap;
    map<string,string>alignmentperiodmap ;
    String alignmnetperiod;
    public boolean affiliationFlag; 
    public list<AxtriaSalesIQTM__Team_Instance__c> countrylst=new list<AxtriaSalesIQTM__Team_Instance__c>();

  
    global PACP_lookups_Utility(String teamInstance) {
       /* teamInstanceNametoIDMap = new Map<String,Id>();
        teamInstanceNametoTeamNameMap = new Map<String,String>();
        teamInstanceNametoAffiliationMap = new Map<String,Boolean>();
        selectedTeamInstance = teamInstance;
        alignmentperiodmap = new map<string,string>();
        alignmnetperiod = '';

        //Added by Sukirty 8/11/2019
        affiliationFlag = [Select Enable_Affiliation_CallPlan__c From AxtriaSalesIQTM__Team_Instance__c where name =: teamInstance].Enable_Affiliation_CallPlan__c;
        System.debug('affiliationFlag--'+affiliationFlag);
        
        countrylst =[select id,name,AxtriaSalesIQTM__Country__c from AxtriaSalesIQTM__Team_Instance__c where name=:teamInstance];
        country = countrylst[0].AxtriaSalesIQTM__Country__c;
        System.debug('country--'+country);

        for(AxtriaSalesIQTM__Team_Instance__C teamIns : [Select Id,Name,AxtriaSalesIQTM__Team__r.name,AxtriaSalesIQTM__Alignment_Period__c,Enable_Affiliation_CallPlan__c From AxtriaSalesIQTM__Team_Instance__C]){
            System.debug('teamIns.Name---'+teamIns.Name);
            System.debug('teamIns.Enable_Affiliation_CallPlan__c---'+teamIns.Enable_Affiliation_CallPlan__c);
            teamInstanceNametoIDMap.put(teamIns.Name,teamIns.Id);
            teamInstanceNametoTeamNameMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__r.name);
            alignmentperiodmap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Alignment_Period__c);
            teamInstanceNametoAffiliationMap.put(teamIns.Name,teamIns.Enable_Affiliation_CallPlan__c); //Added by Sukirty (7/11/19)
        }
        
        selectedTeam = teamInstanceNametoTeamNameMap.get(selectedTeamInstance);
        alignmnetperiod = alignmentperiodmap.get(selectedTeamInstance);
        System.debug('affiliation val--'+teamInstanceNametoAffiliationMap.get(selectedTeamInstance));
        //notSelectedTeamInstance = [Select Id, Name from AxtriaSalesIQTM__Team_Instance__C where Name LIKE : 'NS%' and Name != :teamInstance];
        
        query = 'Select Id,Name,Cycle__c, Account_ID__c, Parent_Account_ID__c, Team_ID__c,Product_Name__c, Territory_ID__c, Segment__c, Objective__c, Product_ID__c, Adoption__c, Potential__c, Target__c, Previous_Cycle_Calls__c, Account__c, Parent_Account__c, Position__c, Position_Team_Instance__c, Team_Instance__c, isError__c FROM Call_Plan__c  Where Team_ID__c = \'' + teamInstance + '\' and Status__c != \'Processing\'';*/
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
       /* system.debug('query+++++++++++++   ' + query);*/
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Call_Plan__c> scopeRecs) {
     /* system.debug('Hello++++++++++++++++++++++++++++');
        Set<string> posCodeSet = new Set<string>();
        Set<string> accountNumberSet = new Set<string>();
        Set<string> parentAccountNumberSet = new Set<string>();
        Set<string>productid = new set<string>();
        Map<string,id> posCodeIdMap = new Map<string,id>();
        Map<string,id> inactivePosCodeIdMap = new Map<string,id>();
        Map<string,string> activeAffiliationMap = new Map<string,string>();
        Map<string,string> inactiveAffiliationMap = new Map<string,string>();
        Map<string,id> accountNumberIdMap = new Map<string,id>();
        Map<string,id> parentAccountNumberIdMap = new Map<string,id>();
        Map<string,id> accountNumberPosIDtoPosAccIdMap = new Map<string,id>();
        Map<string,id> posTeamInstMap = new Map<string,id>();
        map<string,string>Prodcodetonamemap = new map<string,string>();
        set<string> positionset= new set<string>();
        Map<String,Set<String>> postoproductmap = new Map<String,Set<String>>();
        set<String> productset = new set<string>();
        
        
        List<Error_Object__c> errorList = new List<Error_object__c>();
        Error_object__c tempErrorObj;

        for(Call_Plan__c cp : scopeRecs){      //Positions Set
            if(!string.IsBlank(cp.Territory_ID__c)){
                posCodeSet.add(cp.Territory_ID__c);
            }

            if(!string.IsBlank(cp.Account_ID__c)){            //Accounts Set
                accountNumberSet.add(cp.Account_ID__c);
            }
            if((!string.IsBlank(cp.Parent_Account_ID__c)) && affiliationFlag==true){            //Added by Sukirty-Parent Accounts Set (HCO)
                parentAccountNumberSet.add(cp.Parent_Account_ID__c);
            }
            if(!string.isBlank(cp.Product_ID__c)){ //Product set
                productid.add(cp.Product_ID__c);
            }
        }

        system.debug('============posCodeSet::::'+posCodeSet);
        system.debug('============posCodeSet size::::'+posCodeSet.size());
        system.debug('============productid::::'+productid);
        system.debug('============productid size::::'+productid.size());
        system.debug('============parentAccountNumberSet::::'+parentAccountNumberSet);
        system.debug('============parentAccountNumberSet size::::'+parentAccountNumberSet.size());
        String key;
        for(AxtriaSalesIQTM__Position__c  pos : [Select id,  AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__inactive__c from AxtriaSalesIQTM__Position__c 
                                                where AxtriaSalesIQTM__Client_Position_Code__c IN: posCodeSet]){
            if(pos.AxtriaSalesIQTM__inactive__c == false){
                key = (string)pos.AxtriaSalesIQTM__Client_Position_Code__c + (string)pos.AxtriaSalesIQTM__Team_Instance__r.Name;
                posCodeIdMap.put(key,pos.id);
            }
            else{
              key = (string)pos.AxtriaSalesIQTM__Client_Position_Code__c + (string)pos.AxtriaSalesIQTM__Team_Instance__r.Name;
                inactivePosCodeIdMap.put(key,pos.id);
            }
        } 
        //-------- Position and PTI Map
        for(AxtriaSalesIQTM__Position_Team_Instance__c  posTeamInst : [Select Id,AxtriaSalesIQTM__Team_Instance_ID__r.Name,AxtriaSalesIQTM__Team_Instance_ID__c,AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Position_ID__c,AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__inactive__c from AxtriaSalesIQTM__Position_Team_Instance__c 
                                                where AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__Client_Position_Code__c IN: posCodeSet and AxtriaSalesIQTM__Team_Instance_ID__r.AxtriaSalesIQTM__Alignment_Period__c=:alignmnetperiod]){
            key = (string)posTeamInst.AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__Client_Position_Code__c + (string)posTeamInst.AxtriaSalesIQTM__Team_Instance_ID__r.Name;
            if(posTeamInst.AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__inactive__c == false){
                
                posCodeIdMap.put(key,posTeamInst.AxtriaSalesIQTM__Position_ID__c);        //Map of Active Positions and their lookups
            }
            else{
                inactivePosCodeIdMap.put(key,posTeamInst.AxtriaSalesIQTM__Position_ID__c);    //Map of InActive Positions and their lookups
            }
            String position = (string)posTeamInst.AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__Client_Position_Code__c;
          String teamInstance = posTeamInst.AxtriaSalesIQTM__Team_Instance_ID__r.Name;
            posTeamInstMap.put(posTeamInst.AxtriaSalesIQTM__Position_ID__c,posTeamInst.Id);
            
        }
        
        system.debug('============  ::::'+accountNumberSet);
        system.debug('============accountNumberSet size::::'+accountNumberSet.size());
        system.debug('============posTeamInstMap::::'+posTeamInstMap);
        system.debug('============posTeamInstMap size::::'+posTeamInstMap.size());
        //-------- Account Map
         for(Account acc : [Select id,AccountNumber from Account where AccountNumber IN:accountNumberSet and Country_ID__c=:country  ])
         { //,Team_Name__c   and Team_Name__c=: selectedTeam
            accountNumberIdMap.put(acc.AccountNumber,acc.id);                  //Map of accounts and their lookups
            
        }
        system.debug('=========accountNumberIdMap==========='+accountNumberIdMap);
        system.debug('=========accountNumberIdMap size==========='+accountNumberIdMap.size());
        //-------- Position Account Map
         for(AxtriaSalesIQTM__Position_Account__c acc : [Select id,AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Account__r.AccountNumber IN:accountNumberSet and AxtriaSalesIQTM__Team_Instance__c = :teamInstanceNametoIDMap.get(selectedTeamInstance) and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=:alignmnetperiod and Country__c=:country and AxtriaSalesIQTM__Assignment_Status__c!='Inactive'])
        { //,Team_Name__c   and Team_Name__c=: selectedTeam
            accountNumberPosIDtoPosAccIdMap.put(acc.AxtriaSalesIQTM__Account__r.AccountNumber+acc.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,acc.id);                  //Map of accounts and their lookups    
        }

        //HCO map - Added By Sukirty (7/11/19)
        if(parentAccountNumberSet.size()>0) {
            for(Account acc : [Select id,AccountNumber from Account where AccountNumber IN:parentAccountNumberSet and Country_ID__c=:country and Type = 'HCO' ])
            {
                parentAccountNumberIdMap.put(acc.AccountNumber,acc.id);                  //Map of HCO accounts and their lookups   
            }

            //To check for active and inactive account affiliations when call plan is HCP-HCO based
            for(AxtriaSalesIQTM__Account_Affiliation__c aff : [select id, AxtriaSalesIQTM__Active__c, AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Parent_Account__r.AccountNumber, Account_Number__c, Parent_Account_Number__c, Unique_Id__c from AxtriaSalesIQTM__Account_Affiliation__c where Account_Number__c IN:accountNumberSet]) {
                if(aff.AxtriaSalesIQTM__Active__c == true) {
                    System.debug('inside active affiliation');
                    activeAffiliationMap.put(aff.Unique_Id__c,aff.Unique_Id__c); //Map of active affiliations
                }
                else {
                    System.debug('inside inactiveAffiliationMap');
                    inactiveAffiliationMap.put(aff.Unique_Id__c,aff.Unique_Id__c); //Map of inactive affiliations
                }
            }
        }
        system.debug('=========parentAccountNumberIdMap==========='+parentAccountNumberIdMap);
        system.debug('=========parentAccountNumberIdMap size==========='+parentAccountNumberIdMap.size());
        system.debug('=========activeAffiliationMap==========='+activeAffiliationMap);
        system.debug('=========activeAffiliationMap size==========='+activeAffiliationMap.size());
        system.debug('=========inactiveAffiliationMap==========='+inactiveAffiliationMap);
        system.debug('=========inactiveAffiliationMap size==========='+inactiveAffiliationMap.size());     
        
        //------- Product ID to Name Map
        system.debug('==============selectedTeamInstance::'+selectedTeamInstance);
        system.debug('==============productid::'+productid);
        
        for(Product_Catalog__c prod :[select id,Name,Veeva_External_ID__c from Product_Catalog__c where Veeva_External_ID__c IN:productid and Team_Instance__r.Name =: selectedTeamInstance and Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c =:country and IsActive__c=true] )
        {
            Prodcodetonamemap.put(prod.Veeva_External_ID__c,prod.Name);
        }
        system.debug('=========Prodcodetonamemap==========='+Prodcodetonamemap);
        system.debug('=========Prodcodetonamemap size==========='+Prodcodetonamemap.size());
        

        //------ Position product
        map<string,string>prodidtoname = new map<string,string>(); //prod ID to name map
        map<string,set<string>>postoprodid = new map<string,set<string>>(); 
        
        for(AxtriaSalesIQTM__Position_Product__c posProduct :[SELECT Product_Catalog__c,Product_Catalog__r.Veeva_External_ID__c,Product_Catalog__r.Name,AxtriaSalesIQTM__isActive__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Product_Weight__c,AxtriaSalesIQTM__Team_Instance__c,Id,Name,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c FROM AxtriaSalesIQTM__Position_Product__c where AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c in : posCodeSet and AxtriaSalesIQTM__Team_Instance__r.name =:selectedTeamInstance and AxtriaSalesIQTM__isActive__c=true])
        {
            
            if(!postoproductmap.containsKey(posProduct.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c))
            {   
                 productset.add(posProduct.Product_Catalog__r.Veeva_External_ID__c);
                 postoproductmap.put(posProduct.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,productset);
                 system.debug('++++productset+++'+productset);
                 system.debug('++++postoproductmap+++'+postoproductmap);

            }
             else
            {
                set<string>productset1 = new set<string>();
                productset1.addall(postoproductmap.get(posProduct.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c));
                productset1.add(posProduct.Product_Catalog__r.Veeva_External_ID__c);  
                postoproductmap.put(posProduct.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,productset1);
                system.debug('++++productset1+++'+productset1);
                system.debug('++++postoproductmap+++'+postoproductmap);
            }
            
             prodidtoname.put(posProduct.Product_Catalog__r.Veeva_External_ID__c,posProduct.Product_Catalog__r.Name);
              
             if(postoprodid.containsKey)
             prodidtoname.put(posProduct.Product_Catalog__r.Veeva_External_ID__c,posProduct.Product_Catalog__r.Name);
             postoprodid.put(posProduct.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,)
             
        }
        system.debug('++++postoproductmap OUT SIDE+++'+postoproductmap);
        system.debug('++++prodidtoname+++'+prodidtoname);
        system.debug('++++postoproductmap OUT SIDE size+++'+postoproductmap.size());
        system.debug('++++prodidtoname size+++'+prodidtoname.size());
        
        for(Call_Plan__c cp : scopeRecs)
        {
          
            //---------- Team Instance lookups
            if(!string.IsBlank(cp.Team_ID__c)){
                if(teamInstanceNametoIDMap.containsKey(cp.Team_ID__c)){
                    cp.Team_Instance__c = teamInstanceNametoIDMap.get(cp.Team_ID__c);    //Filling lookups of Team Instance
                }
                else{
                    cp.isError__c = true;
                    cp.Status__c = 'Processing';
                    tempErrorObj = new Error_object__c();
                    tempErrorObj.Record_Id__c = cp.Id;
                    tempErrorObj.Object_Name__c = 'Call_Plan__c';
                    tempErrorObj.Field_Name__c = 'Team_Instance__c';
                    tempErrorObj.Comments__c = 'Problem in Name of Team Instance: Team Instance name not found in map';
                    tempErrorObj.Team_Instance_Name__c = cp.Team_ID__c;
                    errorList.add(tempErrorObj);
                }
            }
            else{
              
                cp.isError__c = true;
                cp.Status__c = 'Processing';
                tempErrorObj = new Error_object__c();
                tempErrorObj.Record_Id__c = cp.Id;
                tempErrorObj.Object_Name__c = 'Call_Plan__c';
                tempErrorObj.Field_Name__c = 'Team_Instance__c';
                tempErrorObj.Comments__c = 'Team Instance Name is Blank';
                tempErrorObj.Team_Instance_Name__c = cp.Team_ID__c;
                errorList.add(tempErrorObj);
            }
            //---------- Account lookups
            if(!string.isBlank(cp.Account_ID__c)){
              if(accountNumberIdMap.containsKey(cp.Account_ID__c)){
                cp.Account__c = accountNumberIdMap.get(cp.Account_ID__c);        //Filling lookups of Accounts
              }
              else{
                cp.isError__c = true;
                cp.Status__c = 'Processing';
                    tempErrorObj = new Error_object__c();
                    tempErrorObj.Record_Id__c = cp.Id;
                    tempErrorObj.Object_Name__c = 'Call_Plan__c';
                    tempErrorObj.Field_Name__c = 'Account__c';
                    tempErrorObj.Comments__c = 'Account Number not found in HCP List' + cp.Account_ID__c; //added by Sukirty
                    tempErrorObj.Team_Instance_Name__c = cp.Team_ID__c;
                    errorList.add(tempErrorObj);
              }
            }
            else{
              cp.isError__c = true;
              cp.Status__c = 'Processing';
                tempErrorObj = new Error_object__c();
                tempErrorObj.Record_Id__c = cp.Id;
                tempErrorObj.Object_Name__c = 'Call_Plan__c';
                tempErrorObj.Field_Name__c = 'Account__c';
                tempErrorObj.Comments__c = 'Account Number is Blank';
                tempErrorObj.Team_Instance_Name__c = cp.Team_ID__c;
                errorList.add(tempErrorObj);
              
            }

            //---------- Parent Account lookups //Added by sukirty (7/11/2019)
            System.debug('affiliation check 1--'+teamInstanceNametoAffiliationMap.get(selectedTeamInstance));
            if(teamInstanceNametoAffiliationMap.get(selectedTeamInstance) == true) {
                String affiliationKey = (string)cp.Account_ID__c+'_'+(string)cp.Parent_Account_ID__c;
                //if affiliation is active fill lookup
                if(!string.isBlank(cp.Parent_Account_ID__c) && activeAffiliationMap.containsKey(affiliationKey)) {
                  if(parentAccountNumberIdMap.containsKey(cp.Parent_Account_ID__c)){ 
                    cp.Parent_Account__c = parentAccountNumberIdMap.get(cp.Parent_Account_ID__c);        //Filling lookups of parent Accounts
                  }
                  else{
                    cp.isError__c = true;
                    cp.Status__c = 'Processing';
                        tempErrorObj = new Error_object__c();
                        tempErrorObj.Record_Id__c = cp.Id;
                        tempErrorObj.Object_Name__c = 'Call_Plan__c';
                        tempErrorObj.Field_Name__c = 'Parent_Account__c';
                        tempErrorObj.Comments__c = 'Parent Account Number not found in parentAccount Map' + cp.Parent_Account_ID__c; //added by Sukirty
                        tempErrorObj.Team_Instance_Name__c = cp.Team_ID__c;
                        errorList.add(tempErrorObj);
                  }
                }
                //if Affiliation is inactive
                else if(!string.isBlank(cp.Parent_Account_ID__c) && inactiveAffiliationMap.containsKey(affiliationKey)) {
                    cp.isError__c = true;
                    cp.Status__c = 'Processing';
                    tempErrorObj = new Error_object__c();
                    tempErrorObj.Record_Id__c = cp.Id;
                    tempErrorObj.Object_Name__c = 'Call_Plan__c';
                    tempErrorObj.Field_Name__c = 'Parent_Account__c';
                    tempErrorObj.Comments__c = 'Parent Account Affiliation is inactive '+cp.Account_ID__c+'_'+cp.Parent_Account_ID__c;
                    tempErrorObj.Team_Instance_Name__c = cp.Team_ID__c;
                    errorList.add(tempErrorObj);
                }
                //if Allifiation record is not created for any HCP-HCO combination
                else if((!string.isBlank(cp.Parent_Account_ID__c)) && (!activeAffiliationMap.containsKey(affiliationKey)) && (!inactiveAffiliationMap.containsKey(affiliationKey)) && accountNumberIdMap.containsKey(cp.Account_ID__c)){
                    System.debug('inside elseif--'+cp.Parent_Account_ID__c);
                    cp.isError__c = true;
                    cp.Status__c = 'Processing';
                    tempErrorObj = new Error_object__c();
                    tempErrorObj.Record_Id__c = cp.Id;
                    tempErrorObj.Object_Name__c = 'Call_Plan__c';
                    tempErrorObj.Field_Name__c = 'Parent_Account__c';
                    tempErrorObj.Comments__c = 'Account Affiliation record is not created for '+cp.Account_ID__c+'_'+cp.Parent_Account_ID__c;
                    tempErrorObj.Team_Instance_Name__c = cp.Team_ID__c;
                    errorList.add(tempErrorObj);
                  
                }
                //when parent Account number is blank in call plan object
                else {
                    cp.isError__c = true;
                    cp.Status__c = 'Processing';
                    tempErrorObj = new Error_object__c();
                    tempErrorObj.Record_Id__c = cp.Id;
                    tempErrorObj.Object_Name__c = 'Call_Plan__c';
                    tempErrorObj.Field_Name__c = 'Parent_Account__c';
                    tempErrorObj.Comments__c = 'Parent Account Number is Blank';
                    tempErrorObj.Team_Instance_Name__c = cp.Team_ID__c;
                    errorList.add(tempErrorObj);
                }
            }
            

            //---------Product lookup
            system.debug('=========cp.Product_ID__c:::'+cp.Product_ID__c);
            system.debug('====postoproduct-------:'+postoproductmap);
            if(!string.isBlank(cp.Product_ID__c))
            {
               // String prodkey = cp.Product_ID__c;
                  String prodkey = cp.Territory_ID__c;
                  system.debug('====prodkey-------:'+prodkey);
                  set<string>products = new set<string>();
                  boolean prodexist=false;
                  string prodname ='';
                if(postoproductmap.containsKey(prodkey))
                {
                    products = postoproductmap.get(prodkey);
                }
                system.debug('++products++'+products);
                if(products.contains(cp.Product_ID__c))
                {
                    system.debug('++++++++++++  prod Exists'+prodexist);
                    prodexist=true;
                    prodname=prodidtoname.get(cp.Product_ID__c);
                    System.debug('----Prodname---'+prodname);
                    System.debug('----prodexist---'+prodexist);
                    cp.Product_Name__c = prodname;
                }
                
                system.debug('++++++++++++  prod Exists' + prodexist);
                if(prodexist == false)
                {
                    cp.isError__c = true;
                    cp.Status__c = 'Processing';
                    tempErrorObj = new Error_object__c();
                    tempErrorObj.Record_Id__c = cp.Id;
                    tempErrorObj.Object_Name__c = 'Call_Plan__c';
                    tempErrorObj.Field_Name__c = 'Product_ID__c';
                    tempErrorObj.Comments__c = 'Product Does not exists for '+ cp.Territory_ID__c ;
                    tempErrorObj.Team_Instance_Name__c = cp.Team_ID__c;
                    errorList.add(tempErrorObj);
                }

            }else{
                    cp.isError__c = true;
                    cp.Status__c = 'Processing';
                    tempErrorObj = new Error_object__c();
                    tempErrorObj.Record_Id__c = cp.Id;
                    tempErrorObj.Object_Name__c = 'Call_Plan__c';
                    tempErrorObj.Field_Name__c = 'Product_ID__c';
                    tempErrorObj.Comments__c = 'Product ID is Blank';
                    tempErrorObj.Team_Instance_Name__c = cp.Team_ID__c;
                    errorList.add(tempErrorObj);
            }
            
            //------------  Position and PTI lookups
            String keyClientCodeTeamName;
            keyClientCodeTeamName = (string)cp.Territory_ID__c + (string)cp.Team_ID__c ;
            
            if(string.IsBlank(cp.Territory_ID__c)){
                cp.isError__c = true;
                cp.Status__c = 'Processing';
                tempErrorObj = new Error_object__c();
                tempErrorObj.Record_Id__c = cp.Id;
                tempErrorObj.Object_Name__c = 'Call_Plan__c';
                tempErrorObj.Field_Name__c = 'Position__c';
                tempErrorObj.Comments__c = 'Position Code is blank';
                tempErrorObj.Team_Instance_Name__c = cp.Team_ID__c;
                errorList.add(tempErrorObj);
            }
            
            else if(!string.IsBlank(cp.Territory_ID__c) && posCodeIdMap.containsKey(keyClientCodeTeamName)){
              String activePosID = posCodeIdMap.get(keyClientCodeTeamName);          
                cp.Position__c = activePosID;                          //Filling lookups of Active Positions
                cp.Position_Team_Instance__c = posTeamInstMap.get(activePosID);        //Filling lookups of PTI
                
            }
            else if(!string.IsBlank(cp.Territory_ID__c) && inactivePosCodeIdMap.containsKey(keyClientCodeTeamName))
            {
              String inActivePosID = inactivePosCodeIdMap.get(keyClientCodeTeamName);      
              cp.Position__c = inActivePosID;                        //Filling lookups of InActive Positions
              cp.Position_Team_Instance__c = posTeamInstMap.get(inActivePosID);        //Filling lookups of PTI
              
              
                cp.isError__c = true;
                cp.Status__c = 'Processing';
                tempErrorObj = new Error_object__c();
                tempErrorObj.Record_Id__c = cp.Id;
                tempErrorObj.Object_Name__c = 'Call_Plan__c';
                tempErrorObj.Field_Name__c = 'Position__c';
                tempErrorObj.Comments__c = 'Warning: Position is Inactive in system, but its lookup is filled';
                tempErrorObj.Team_Instance_Name__c = cp.Team_ID__c;
                errorList.add(tempErrorObj);
            }
            else if((!posCodeIdMap.containsKey(keyClientCodeTeamName)) && (!inactivePosCodeIdMap.containsKey(keyClientCodeTeamName))){
              cp.isError__c = true;
              cp.Status__c = 'Processing';
                tempErrorObj = new Error_object__c();
                tempErrorObj.Record_Id__c = cp.Id;
                tempErrorObj.Object_Name__c = 'Call_Plan__c';
                tempErrorObj.Field_Name__c = 'Position__c';
                tempErrorObj.Comments__c = 'Either Position or its Position Team Instance does not exist';
                tempErrorObj.Team_Instance_Name__c = cp.Team_ID__c;
                errorList.add(tempErrorObj);
              
            }
            
            //Pos Account
            if(!string.isBlank(cp.Account_ID__c) && !string.isBlank(cp.Territory_ID__c)){
              if(accountNumberPosIDtoPosAccIdMap.containsKey(cp.Account_ID__c+cp.Territory_ID__c)){
                cp.Position_Account__c = accountNumberPosIDtoPosAccIdMap.get(cp.Account_ID__c+cp.Territory_ID__c);        //Filling lookups of Accounts
              }
              else{
                cp.isError__c = true;
                cp.Status__c = 'Processing';
                    tempErrorObj = new Error_object__c();
                    tempErrorObj.Record_Id__c = cp.Id;
                    tempErrorObj.Object_Name__c = 'Call_Plan__c';
                    tempErrorObj.Field_Name__c = 'Account__c';
                    tempErrorObj.Comments__c = 'Position Account not found in map';
                    tempErrorObj.Team_Instance_Name__c = cp.Team_ID__c;
                    errorList.add(tempErrorObj);
              }
            }
            else{
              cp.isError__c = true;
              cp.Status__c = 'Processing';
                tempErrorObj = new Error_object__c();
                tempErrorObj.Record_Id__c = cp.Id;
                tempErrorObj.Object_Name__c = 'Call_Plan__c';
                tempErrorObj.Field_Name__c = 'Account__c';
                tempErrorObj.Comments__c = 'Position or Account is Blank';
                tempErrorObj.Team_Instance_Name__c = cp.Team_ID__c;
                errorList.add(tempErrorObj);
              
            }
            
            
        }
        
        if(errorList.size() > 0){
            insert errorList;
        }
        update scopeRecs ;*/

    }

    global void finish(Database.BatchableContext BC) {
      // Database.executeBatch(new  Call_Plan_Data_to_PACP_New(selectedTeamInstance),2000);  
    }
}