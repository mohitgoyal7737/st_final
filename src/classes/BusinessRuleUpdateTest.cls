@isTest
public class BusinessRuleUpdateTest{
 static testmethod void BusinessRuleUpdateTest(){
 
 
 AxtriaSalesIQTM__Organization_Master__c OM = new AxtriaSalesIQTM__Organization_Master__c();
    OM.Name='Dummy1';
    OM.AxtriaSalesIQTM__Org_Level__c='Global';
    OM.Marketing_Code__c='ES';
    OM.AxtriaSalesIQTM__Parent_Country_Level__c=TRUE;
    insert OM;
    
    AxtriaSalesIQTM__Country__c CON = new AxtriaSalesIQTM__Country__c();
    CON.AxtriaSalesIQTM__Country_Code__c='ES';
    CON.AxtriaSalesIQTM__Status__c='ACTIVE';
    CON.AxtriaSalesIQTM__Parent_Organization__c=OM.id;
    insert CON;

     AxtriaSalesIQTM__Workspace__c WK = new AxtriaSalesIQTM__Workspace__c();
     WK.Name='W1';
     WK.AxtriaSalesIQTM__Workspace_Start_Date__c=system.today().adddays(-10);
     
     WK.AxtriaSalesIQTM__Workspace_Description__c='test class for Scenario Queue Trigger';
     insert WK;
     
AxtriaSalesIQTM__Team__c TN = new AxtriaSalesIQTM__Team__c();
TN.Name='ABC';
TN.AxtriaSalesIQTM__Effective_Start_Date__c=system.today()-1;
TN.AxtriaSalesIQTM__Effective_Start_Date__c=system.today();
TN.AxtriaSalesIQTM__Alignment_Type__c='Account';
TN.AxtriaSalesIQTM__Team_Code__c='TE01111';
TN.AxtriaSalesIQTM__Country__c=CON.id;
TN.AxtriaSalesIQTM__Type__c='BASE';
    insert TN;
     
AxtriaSalesIQTM__Scenario__c SS = new AxtriaSalesIQTM__Scenario__c();
     SS.AxtriaSalesIQTM__Workspace__c=WK.id;
     insert SS;
     
AxtriaSalesIQTM__Team_Instance__c  TI = new AxtriaSalesIQTM__Team_Instance__c ();
    TI.Name='ESABCD';
    TI.AxtriaSalesIQTM__Team__c=TN.id;
     
    insert TI;
     
AxtriaSalesIQTM__Business_Rules__c BR = new AxtriaSalesIQTM__Business_Rules__c();
     BR.AxtriaSalesIQTM__Scenario__c=SS.id;
     insert BR;
     ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
     update BR;
     update SS;
    
   
    }
}