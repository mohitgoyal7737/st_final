global class ScheduleOutboundjobs implements Schedulable {
	global list<AxtriaSalesIQTM__Team_Instance__c>teamlist {get;set;}
	global set<string>setcountries {get;set;}
	global string country {get;set;}
	global boolean flag=false;
	global set<string>codeset {get;set;}
	global string countrycode {get;set;}
	global ScheduleOutboundjobs()
	{
		/*teamlist = new list<AxtriaSalesIQTM__Team_Instance__c>();
		setcountries = new set<string>();
		country='';
		codeset = new set<string>();
		countrycode = '';

		teamlist = [select id,Country_Name__c,AxtriaSalesIQTM__Alignment_Period__c,AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c from  AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c= 'Current' and AxtriaSalesIQTM__IC_EffstartDate__c = TODAY and ( AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c ='LIVE' or AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c='Published')];
		if(teamlist !=null && teamlist.size() >0)
		{
			for(AxtriaSalesIQTM__Team_Instance__c ti : teamlist)
			{
				setcountries.add(ti.Country_Name__c);
				if(ti.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c !=null)
					codeset.add(ti.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c);
				flag = true;
			}

		}
		if(flag)
		{
			//calljobs();
			list<string>temp = new list<string>();
			temp.addall(setcountries);
			country=string.join(temp, ',');
			list<string>temp2 =new list<string>();
			temp2.addall(codeset);
			countrycode = string.join(temp2, ',');
			//country=
			calljobs();
		}*/

	}
	global void calljobs()
	{
		/*system.debug('==================country:::::'+country);
		system.debug('==============countrycode:::::'+countrycode);
		//Call the outboundjobs here
		
		
		database.executeBatch(new BatchOutboundWorkspace (country),2000);
		database.executeBatch(new BatchOutboundUpdTeamInstance (country),2000);
		database.executeBatch(new BatchOutBoundPosition(country),2000);
		database.executeBatch(new BatchOutBoundEmployee(country),2000);
		database.executeBatch(new BatchOutBoundPosEmp(country),2000);
		database.executeBatch(new BatchOutBoundPositionProduct2(country),2000);
		database.executeBatch(new BatchOutBounUpdPositionAccount (country,'Any_String'),2000);
		database.executeBatch(new BatchOutBoundCustomerSegment(country),2000);
		//database.executebatch(new BatchCreateGeography_Outbound(countrycode),2000);
*/
		

	}
    global void execute(SchedulableContext sc) {
        ScheduleOutboundjobs obj = new ScheduleOutboundjobs();
    }
}