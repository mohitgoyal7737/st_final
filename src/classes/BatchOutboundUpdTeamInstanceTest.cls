@isTest
private class BatchOutboundUpdTeamInstanceTest {
     @testSetup 
    static void setup() {
 
        AxtriaSalesIQTM__Team__c t=new AxtriaSalesIQTM__Team__c();
        t.Name='xx';
        insert t;
        String teamId=t.id;
        AxtriaSalesIQTM__Team__c t1=new AxtriaSalesIQTM__Team__c();
        t1.Name='yy';
        insert t1;
        String parentteamId=t1.id;
        AxtriaSalesIQTM__Team_Instance__c ti=new AxtriaSalesIQTM__Team_Instance__c();
        ti.AxtriaSalesIQTM__Team__c=teamId;
        ti.AxtriaSalesIQTM__Alignment_Period__c='current';
        insert ti;
        String teamInstId=ti.id;
        
        
       
    }
    static testmethod void test() {        
        Test.startTest();
         AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
        team.Name = 'Specialty';
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamIns = new AxtriaSalesIQTM__Team_Instance__c();
        teamIns.AxtriaSalesIQTM__Team__c = team.id;
        teamIns.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        teamIns.Name = 'abc';
        insert teamIns;
        BatchOutboundUpdTeamInstance usa = new BatchOutboundUpdTeamInstance();
        Id batchId = Database.executeBatch(usa);
      //  BatchOutBoundPositionProduct obj = new BatchOutBoundPositionProduct();
        
        Test.stopTest();
        // after the testing stops, assert records were updated 
    }
}