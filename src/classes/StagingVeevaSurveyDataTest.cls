/*Author - Himanshu Tariyal(A0994)
Date : 16th January 2018*/
@isTest
private class StagingVeevaSurveyDataTest 
{
    private static testMethod void firstTest() 
    {
        /*Create initial test data for all the objs reqd.*/
        AxtriaSalesIQTM__Organization_Master__c aom = new AxtriaSalesIQTM__Organization_Master__c(AxtriaSalesIQTM__Org_Level__c='Global',AxtriaSalesIQTM__Parent_Country_Level__c=true);
        insert aom;
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='Italy',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active');
        insert country;
        
        /*Cycle__c cycle = new Cycle__c(Name='Test Cycle',Country__c = country.id);
        insert cycle;*/
        
        Account acc = new Account(Name='test acc',Marketing_Code__c='EU');
        insert acc;
        
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='Test Team');
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU',AxtriaSalesIQTM__Team__c=team.id/*,Cycle__c=cycle.id*/);
        insert ti;
        
        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c(Name='Pos',AxtriaSalesIQTM__Team_iD__c=team.id);
        insert pos;

        Product_Catalog__c pc = new Product_Catalog__c(Name='Test Product',Team_Instance__c=ti.id);
        pc.Product_Code__c = '172_002000017000_IT';
        pc.Veeva_External_ID__c = '172_002000017000_IT';
        pc.IsActive__c=true;
        pc.Country_Lookup__c=country.id;
        insert pc;
        
        Staging_Veeva_Cust_Data__c svcd = new Staging_Veeva_Cust_Data__c();
        svcd.BRAND_ID__c = '172_002000017000_IT';
        svcd.BRAND_NAME__c = 'Methanol';
        svcd.PARTY_ID__c = 'IT100501';
        svcd.QUESTION_ID__c = 1;
        svcd.QUESTION_SHORT_TEXT__c = 'Test1';
        svcd.RESPONSE__c = '5';
        svcd.SURVEY_ID__c = '1';
        svcd.SURVEY_NAME__c = 'Methanol_pcp';
        insert svcd;
        
        Staging_Veeva_Cust_Data__c svcd2 = new Staging_Veeva_Cust_Data__c();
        svcd2.BRAND_ID__c = '172_002000017000_IT';
        svcd2.BRAND_NAME__c = 'Methanol';
        svcd2.PARTY_ID__c = 'IT100501';
        svcd2.QUESTION_ID__c = 2;
        svcd2.QUESTION_SHORT_TEXT__c = 'Test2';
        svcd2.RESPONSE__c = '5';
        svcd2.SURVEY_ID__c = '1';
        svcd2.SURVEY_NAME__c = 'Methanol_pcp';
        insert svcd2;
        
        System.test.startTest();
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        StagingVeevaSurveyData batch = new StagingVeevaSurveyData();        
        StagingVeevaSurveyData batch1 = new StagingVeevaSurveyData(true);
        StagingVeevaSurveyData batch2 = new StagingVeevaSurveyData('2018');
        //StagingVeevaSurveyData batch3 = new StagingVeevaSurveyData('test','test1');                
        Database.executeBatch(batch,2000);
        System.test.stopTest();
    }
    private static testMethod void secondTest() 
    {
        /*Create initial test data for all the objs reqd.*/
        String survey = 'test';
        AxtriaSalesIQTM__Organization_Master__c aom = new AxtriaSalesIQTM__Organization_Master__c(AxtriaSalesIQTM__Org_Level__c='Global',AxtriaSalesIQTM__Parent_Country_Level__c=true);
        insert aom;
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='Italy',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active');
        insert country;
        
        /*Cycle__c cycle = new Cycle__c(Name='Test Cycle',Country__c = country.id);
        insert cycle;*/
        
        Account acc = new Account(Name='test acc',Marketing_Code__c='EU');
        insert acc;
        
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='Test Team');
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU',AxtriaSalesIQTM__Team__c=team.id/*,Cycle__c=cycle.id*/);
        insert ti;
        
        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c(Name='Pos',AxtriaSalesIQTM__Team_iD__c=team.id);
        insert pos;

        Product_Catalog__c pc = new Product_Catalog__c(Name='Test Product',Team_Instance__c=ti.id);
        pc.Product_Code__c = '172_002000017000_IT';
        pc.Veeva_External_ID__c = '172_002000017000_IT';
        pc.IsActive__c=true;
        pc.Country_Lookup__c=country.id;
        insert pc;
        
        Staging_Veeva_Cust_Data__c svcd = new Staging_Veeva_Cust_Data__c();
        svcd.BRAND_ID__c = '172_002000017000_IT';
        svcd.BRAND_NAME__c = 'Methanol';
        svcd.PARTY_ID__c = 'IT100501';
        svcd.QUESTION_ID__c = 1;
        svcd.QUESTION_SHORT_TEXT__c = 'Test1';
        svcd.RESPONSE__c = '5';
        svcd.SURVEY_ID__c = '1';
        svcd.SURVEY_NAME__c = 'Methanol_pcp';
        insert svcd;
        
        Staging_Veeva_Cust_Data__c svcd2 = new Staging_Veeva_Cust_Data__c();
        svcd2.BRAND_ID__c = '172_002000017000_IT';
        svcd2.BRAND_NAME__c = 'Methanol';
        svcd2.PARTY_ID__c = 'IT100501';
        svcd2.QUESTION_ID__c = 2;
        svcd2.QUESTION_SHORT_TEXT__c = 'Test2';
        svcd2.RESPONSE__c = '5';
        svcd2.SURVEY_ID__c = '1';
        svcd2.SURVEY_NAME__c = 'Methanol_pcp';
        insert svcd2;
        
        System.test.startTest();
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        StagingVeevaSurveyData batch = new StagingVeevaSurveyData(true,survey);
        Database.executeBatch(batch,2000);
        System.test.stopTest();
    }
}