global class BulkApprovalWebService {

    public BulkApprovalWebService(ApexPages.StandardSetController controller) {
        List<AxtriaSalesIQTM__Change_Request__c> records = (AxtriaSalesIQTM__Change_Request__c[])controller.getSelected();
        system.debug('+++++++++++++ '+records);
    }


    public BulkApprovalWebService(ApexPages.StandardController controller) {

    }


  /**
  * Bulk request approval from list view
  * @param  recordIds String
  * @return JSON String
  */
 



  webService static String bulkRequest( String recordIds ) {
    Map<String, Object> response = new Map<String, Object>();
    try{
        List<String> ids = recordIds.split(',');
        List<Approval.ProcessSubmitRequest> requests = new List<Approval.ProcessSubmitRequest>();
    
        for(String rId : ids){
          Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
          req.setObjectId(rId);
          //If the next step in your approval process is another Apex approval process, you specify exactly one user ID as the next approver. 
          //If not, you cannot specify a user ID and this method must be null.
          //req.setNextApproverIds(null);
          requests.add(req);
        }
        Approval.ProcessResult[] processResults = Approval.process(requests);
        //Valid values are: Approved, Rejected, Removed or Pending.
        System.assertEquals('Pending', processResults[0].getInstanceStatus());
    } catch ( Exception ex ){
        response.put('status', '500');
        response.put('error', ex.getMessage());
        return JSON.serialize(response);
    }
    response.put('status', '200');
    return JSON.serialize(response);
  }

  /**
   * Bulk approve records from list view
   * @param  recordIds String
   * @return JSON String
   */
  webService static String bulkApprove( String recordIds ) {
    Map<String, Object> response = new Map<String, Object>();
    try{
        List<String> ids = recordIds.split(',');
        List<Approval.ProcessWorkitemRequest> requests = new List<Approval.ProcessWorkitemRequest>();
    
        List<ProcessInstanceWorkitem> workItems = [SELECT Id, ProcessInstanceId FROM ProcessInstanceWorkitem WHERE ProcessInstance.TargetObjectId IN :ids ];
        for(ProcessInstanceWorkitem workItem : workItems){
          Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
          req.setWorkitemId(workItem.Id);
          //Valid values are: Approve, Reject, or Removed. 
          //Only system administrators can specify Removed.
          req.setAction('Approve');
          req.setComments('No Comment.');
          requests.add(req);
        }
        Approval.ProcessResult[] processResults = Approval.process(requests);
    } catch ( Exception ex ){
      response.put('status', '500');
      response.put('error', ex.getMessage());
      return JSON.serialize(response);
    }
    response.put('status', '200');
    return JSON.serialize(response);
  }
    
}