global class BatchFullLoadPositionEmployee implements Database.Batchable<sObject>, Database.Stateful {
    public String query;
    public String countryName;
    public Date startDate;
    public String teamIns;
    public Date endDate;

    global BatchFullLoadPositionEmployee(String country) {
        query = '';
        countryName='';
        teamIns='';
        startDate=Date.newInstance(1, 1, 1);
        endDate=Date.newInstance(1, 1, 1);
        countryName=country;
        List<AxtriaSalesIQTM__Team_Instance__c> startDateList=[Select Id,AxtriaSalesIQTM__IC_EffstartDate__c from AxtriaSalesIQTM__Team_Instance__c where Country_Name__c = :countryName and AxtriaSalesIQTM__Alignment_Period__c='Current' limit 1];
        startDate=startDateList[0].AxtriaSalesIQTM__IC_EffstartDate__c;
        teamIns=startDateList[0].Id;
        endDate=startDate-1;
        System.debug('=====teamIns constructor::::::' +teamIns);
        System.debug('=====startDate constructor::::::' +startDate);
        System.debug('=====endDate constructor::::::' +endDate);
        query='select Id,Name,AxtriaSalesIQTM__Assignment_Status__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Assignment_Type__c ,AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Employee__c, AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Country_Name__c,AxtriaSalesIQTM__Employee__r.Employee_PRID__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c from AxtriaSalesIQTM__Position_Employee__c where AxtriaSalesIQTM__Position__r.Country_Name__c = :countryName and AxtriaSalesIQTM__Effective_End_Date__c =:endDate and AxtriaSalesIQTM__Position__c != null and AxtriaSalesIQTM__Employee__c != null';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Employee__c> scope) 
    {
        System.debug('====Query:::::::::::::' +scope);
        System.debug('=====teamIns::::::' +teamIns);
        System.debug('=====startDate::::::' +startDate);

        List<Full_load_Position_Employee__c> peList = new List<Full_load_Position_Employee__c>();
        //Map<String,String> mapId2PosCode=new Map<String,String>();
        //Map<String,String> mapPosCode2PosId=new Map<String,String>();
        
        /*for(AxtriaSalesIQTM__Position_Employee__c peRec : scope)
        {
            mapId2PosCode.put(peRec.id,peRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
        }
        System.debug('====mapId2PosCode:::::::::::::' +mapId2PosCode);
        
        System.debug('====Check Position in new instance::::::::::::');
        List<AxtriaSalesIQTM__Position__c> posList= [select Id,AxtriaSalesIQTM__Client_Position_Code__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__inactive__c=false and AxtriaSalesIQTM__Effective_Start_Date__c=:startDate and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c='Current' and AxtriaSalesIQTM__Client_Position_Code__c in :mapId2PosCode.values()];
        System.debug('=====posList.size()::::::' +posList.size());
        
        for(AxtriaSalesIQTM__Position__c posRec : posList)
        {
            mapPosCode2PosId.put(posRec.AxtriaSalesIQTM__Client_Position_Code__c,posRec.Id);
        }
        System.debug('====mapPosCode2PosId:::::::::::::' +mapPosCode2PosId);*/
        
        for(AxtriaSalesIQTM__Position_Employee__c peRec : scope)
        {
            Full_load_Position_Employee__c peRecClone = new Full_load_Position_Employee__c();
            //peRecClone = peRec.clone();
            peRecClone.Assignment_Type__c='Secondary';
            peRecClone.Effective_Start_Date__c=startDate;
            peRecClone.Effective_End_Date__c=endDate;
            peRecClone.Country_Name__c=countryName;
            peRecClone.Employee_PRID__c=peRec.AxtriaSalesIQTM__Employee__r.Employee_PRID__c;
            peRecClone.Position_Code__c=peRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
            peRecClone.Country__c=peRec.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Country_Name__c;
            //peRecClone.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__c=teamIns;
            //String pos=mapId2PosCode.get(peRec.Id);
            // System.debug('====pos:::::::::::::' +pos);
            // System.debug('====mapPosCode2PosId.get(pos):::::::::::::' +mapPosCode2PosId.get(pos));
            //peRecClone.AxtriaSalesIQTM__Position__c=mapPosCode2PosId.get(pos);
            //System.debug('====peRecClone.AxtriaSalesIQTM__Position__c:::::::::::::' +peRecClone.AxtriaSalesIQTM__Position__c);
            peList.add(peRecClone);
            System.debug('=====peRecClone::::::' +peRecClone);
        }

        System.debug('=====peList::::::' +peList);
        System.debug('=====peList.size()::::::' +peList.size());

        if(peList.size() > 0)
            insert peList;
        
    }

    global void finish(Database.BatchableContext BC) {

    }
}