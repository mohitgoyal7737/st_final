public with sharing class UpdateProfileParameterAccounts {
  @InvocableMethod(label='update Accounts' description='update profile parameters of inserted accounts.')
  public static void updateParameters(List<Account> accounts)
   {
    
    if(!System.isBatch()){ //-- Condition Added by RT for STIMPS-482 for resolving error System.AsyncException: Database.executeBatch cannot be called from a batch start, batch execute, or future method.
      list<Account> acclist=new list<Account>();
      list<Account> inactivelist=new list<Account>();
      for(Account a : accounts){
        /*if((String.isBlank(a.Profile_Consent__c) || a.Profile_Consent__c=='NO'))
        {
          acclist.add(a);
        }*/
        if((a.Status__c=='Inactive') ||(a.Right_to_be_forgotten__c == 'True'))
        {
          inactivelist.add(a);
        }
      }
      SnTDMLSecurityUtil.printDebugMessage('---Account in trigger are:'+inactivelist);

    if(inactivelist.size()>0){
        deletecallplan(inactivelist);
        updateposacct(inactivelist);
      }
    }
  }
  
   public static void updateposacct(list<Account>updatedaccounts){
         AxtriaSalesIQTM__TriggerContol__c customsetting ;
         
         SnTDMLSecurityUtil.printDebugMessage('---Updateposacct called--');
         List<id> accounts = new list<id>();
     List<AxtriaSalesIQTM__Position_Account__c> posacct = new list<AxtriaSalesIQTM__Position_Account__c>();
        
     //  SR-357 Starts--
    Set<String> futurePosAccIds = new Set<String>();
        List<AxtriaSalesIQTM__Position_Account__c> posAcctTodeleteList = new list<AxtriaSalesIQTM__Position_Account__c>();
        List<Account_Compute_Final__c> acfToDeleteList = new List<Account_Compute_Final__c>();
        List<BU_Response__c> buResToDeleteList = new List<BU_Response__c>();
        List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacpToDeleteList = new List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        //  SR-357 changes ends--
    
    for(Account ac : updatedaccounts){
           accounts.add(ac.id);
         }
         SnTDMLSecurityUtil.printDebugMessage('---Account id are:'+accounts);
        // posacct=[select id,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Assignment_Status__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c from AxtriaSalesIQTM__Position_Account__c where (AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c= 'Current' or AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c='Future') and AxtriaSalesIQTM__Account__c in:accounts and (AxtriaSalesIQTM__Assignment_Status__c = 'Active' or AxtriaSalesIQTM__Assignment_Status__c = 'Future Active') WITH SECURITY_ENFORCED];
         posacct=[select id,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Assignment_Status__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Account__c in:accounts and (AxtriaSalesIQTM__Assignment_Status__c = 'Active' or AxtriaSalesIQTM__Assignment_Status__c = 'Future Active') WITH SECURITY_ENFORCED]; // Query modified by RT for SR-357 to query future active records per 'Assignment status' field of PA
         
     // Added by RT for SR-357 on 3rdNov,2020         
    if(!posacct.isEmpty()){
      for(AxtriaSalesIQTM__Position_Account__c pa : posacct){

        if(pa.AxtriaSalesIQTM__Assignment_Status__c =='Active' ){
          pa.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addDays(-1);
        }
        if(pa.AxtriaSalesIQTM__Assignment_Status__c == 'Future Active'){
          futurePosAccIds.add(pa.Id);
          System.debug('futurePosAccIds :: '+futurePosAccIds);
        
        }
      }
       SnTDMLSecurityUtil.updateRecords(posacct, 'UpdateProfileParameterAccounts');
    }

    if(!futurePosAccIds.isEmpty()){
      acfToDeleteList = [Select id from Account_Compute_Final__c where Physician__c in:futurePosAccIds];
      buResToDeleteList = [Select id from BU_Response__c where Position_Account__c in:futurePosAccIds];
      pacpToDeleteList = [Select id from AxtriaSalesIQTM__Position_Account_Call_Plan__c where Party_ID__c in:futurePosAccIds];
      posAcctTodeleteList = [Select id from AxtriaSalesIQTM__Position_Account__c where id in:futurePosAccIds];
      
      try{
        if(!buResToDeleteList.isEmpty()){
          SnTDMLSecurityUtil.deleteRecords(buResToDeleteList,'UpdateProfileParameterAccounts');
        }
        if(!acfToDeleteList.isEmpty()){
          SnTDMLSecurityUtil.deleteRecords(acfToDeleteList,'UpdateProfileParameterAccounts');
        }
        if(!pacpToDeleteList.isEmpty()){
          SnTDMLSecurityUtil.deleteRecords(pacpToDeleteList,'UpdateProfileParameterAccounts');
        }
        if(!posAcctTodeleteList.isEmpty()){
           SnTDMLSecurityUtil.deleteRecords(posAcctTodeleteList,'UpdateProfileParameterAccounts'); 
        }
      }
      
      catch(DMLException ex){
        System.debug('Exception in Position Account and related records deletion in UpdateProfileParameterAccounts class:: '+ex.getMessage()+'Line Number::'+ex.getLineNumber());
      }
    }
    //  SR-357 changes ends here 
        
       
       
    }
    
    
    public static void deletecallplan(list<Account>updatedaccounts){
         SnTDMLSecurityUtil.printDebugMessage('---Updatecallplan1 called--');
       list<id> accounts = new list<id>(); 
       list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>callplandelete = new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
       for(Account ac : updatedaccounts){
           accounts.add(ac.id);
       }
       SnTDMLSecurityUtil.printDebugMessage('---Account id are:'+accounts);
       callplandelete=[select id,AxtriaSalesIQTM__Team_Instance__c ,AxtriaSalesIQTM__isAccountTarget__c,AxtriaSalesIQTM__segment10__c,Segment10__c,AxtriaSalesIQTM__isincludedCallPlan__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Account__c in:accounts and AxtriaSalesIQTM__lastApprovedTarget__c = true WITH SECURITY_ENFORCED]; // updated by RT for STIMPS -238 on Aug6,2020
       
       // Query CIM based on Team instance
       Set<String> teamInstIdSet = new Set<String>(); // STIMPS-238
       List<AxtriaSalesIQTM__CIM_Config__c> cimConfigList= new List<AxtriaSalesIQTM__CIM_Config__c>();// STIMPS-238
       if(callplandelete.size()>0){
       for(AxtriaSalesIQTM__Position_Account_Call_Plan__c p : callplandelete)
       {
         //p.AxtriaSalesIQTM__isAccountTarget__c=false;
         p.AxtriaSalesIQTM__isincludedCallPlan__c=false;
         //Added by Ayushi on 28-09-2018
         p.AxtriaSalesIQTM__lastApprovedTarget__c=false;
         p.Segment10__c='Inactive';
         teamInstIdSet.add(p.AxtriaSalesIQTM__Team_Instance__c ); // Added by RT for STIMPS -238 on Aug6,2020
         
       }
        // --Added by RT for STIMPS-238 Starts here---
        
        // call batch to reflect the in-active PACP corresponding to inactive Accounts in KPI cards on Call Plan
        if(!teamInstIdSet.isEmpty()){
      cimConfigList = [select id,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__CIM_Config__c where AxtriaSalesIQTM__CR_Type_Name__c = :SalesIQGlobalConstants.CR_TYPE_CALL_PLAN and AxtriaSalesIQTM__Enable__c =true and
                                 AxtriaSalesIQTM__Team_Instance__c in:teamInstIdSet WITH SECURITY_ENFORCED];
            System.debug('Calling class RT cimConfigList>>>' + cimConfigList);
       if(!cimConfigList.isEmpty()){
        Database.executeBatch(new BatchCIMPositionMatrixSummaryUpdate(cimConfigList),2000);
       }
        }   
        // --Added by RT for STIMPS-238 ends here---
        
       SnTDMLSecurityUtil.printDebugMessage('delete call plan list'+callplandelete);
       //update callplandelete;
       SnTDMLSecurityUtil.updateRecords(callplandelete, 'UpdateProfileParameterAccounts');
       }
    }
  
  
  /*  public static void updatecallplan(list<Account>updatedaccounts)
    {
        SnTDMLSecurityUtil.printDebugMessage('---Updatecallplan called--');
       list<id> accounts = new list<id>(); 
       list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>pacp = new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
       list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>updatepacp = new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
       for(Account ac : updatedaccounts){
           accounts.add(ac.id);
       }
       SnTDMLSecurityUtil.printDebugMessage('---Account id are:'+accounts);
       pacp=[select id,segment__c,Final_TCF__c,Final_TCF_Approved__c,Final_TCF_Original__c,Proposed_TCF__c,Proposed_TCF2__c,Calculated_TCF__c,Calculated_TCF2__c,Segment2__c,Parameter1__c,Parameter2__c,Parameter3__c,Parameter4__c,Parameter5__c,Parameter6__c,Parameter7__c,Parameter8__c,P2_Parameter1__c,P2_Parameter2__c,P2_Parameter3__c,P2_Parameter4__c,P2_Parameter5__c,P2_Parameter6__c,P2_Parameter7__c,P2_Parameter8__c,P3_Parameter1__c,P3_Parameter2__c,P3_Parameter3__c,P3_Parameter4__c,P3_Parameter5__c,P3_Parameter6__c,P3_Parameter7__c,P3_Parameter8__c,P4_Parameter1__c,P4_Parameter2__c,P4_Parameter3__c,P4_Parameter4__c,P4_Parameter5__c,P4_Parameter6__c,P4_Parameter7__c,P4_Parameter8__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.name from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Account__c in:accounts WITH SECURITY_ENFORCED];
       if(pacp.size()>0){
           for(AxtriaSalesIQTM__Position_Account_Call_Plan__c p : pacp){
                AxtriaSalesIQTM__Position_Account_Call_Plan__c pac = new AxtriaSalesIQTM__Position_Account_Call_Plan__c();
                    pac.id= p.id;
                
                    pac.P2_Parameter1__c = ''; 
                    pac.P2_Parameter2__c = '';
                    pac.P2_Parameter3__c = '';
                    pac.P2_Parameter4__c = '';
                    pac.P2_Parameter5__c = '';
                    pac.P2_Parameter6__c = '';
                    pac.P2_Parameter7__c = '';
                    pac.P2_Parameter8__c = '';
                   // pac.P3__c = '';
                    pac.P3_Parameter1__c = '';
                    pac.P3_Parameter2__c = '';
                    pac.P3_Parameter3__c = '';
                    pac.P3_Parameter4__c = '';
                    pac.P3_Parameter5__c = '';
                    pac.P3_Parameter6__c = '';
                    pac.P3_Parameter7__c = '';
                    pac.P3_Parameter8__c = '';
                    
                    //pac.P4__c = '';
                    pac.P4_Parameter1__c = '';
                    pac.P4_Parameter2__c = '';
                    pac.P4_Parameter3__c  = '';
                    pac.P4_Parameter4__c = '';
                    pac.P4_Parameter6__c = '';
                    pac.P4_Parameter7__c = '';
                    pac.P4_Parameter8__c = '';
                    
                    
                    pac.Parameter3__c = '';
                    pac.Parameter4__c = '';
                    pac.Parameter5__c = '';
                    pac.Parameter6__c = '';
                    pac.Parameter7__c = '';
                    pac.Parameter8__c= '';
                    if(pac.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.name !='ONCOLOGY'){
                      pac.Parameter1__c = '';
                      pac.Parameter2__c = ''; 
                    }



                    pac.segment__c = '';
                    pac.Final_TCF__c=null;
                    pac.Final_TCF_Approved__c = null;
                    pac.Final_TCF_Original__c = null;
                    pac.Proposed_TCF__c = null;
                    pac.Proposed_TCF2__c = null;
                    pac.Calculated_TCF__c = null;
                    pac.Calculated_TCF2__c = null;
                    
                    pac.segment__c = 'ND';
                    
                    
                    
                    
                    pac.Segment2__c = '';
                    updatepacp.add(pac);
               //SnTDMLSecurityUtil.printDebugMessage('----Pacp Cout is:'+pacp.size());
               //SnTDMLSecurityUtil.printDebugMessage('----Pacp  is:'+pacp);
           }
       }
       
     
       
       try{
           CallPlanSummaryTriggerHandler.execute_trigger = false;
           if(updatepacp.size()>0){
           //update updatepacp;
           SnTDMLSecurityUtil.updateRecords(updatepacp, 'UpdateProfileParameterAccounts');
           }
           CallPlanSummaryTriggerHandler.execute_trigger = true;
           SnTDMLSecurityUtil.printDebugMessage('----updatepacp  is:'+updatepacp);
           list<String> updateCallPlanSegment = new list<String>();
           if(updatepacp != null && updatepacp.size() > 0){
                for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pa: updatepacp){
                    updateCallPlanSegment.add(pa.Id);
                }
            }
            if(updateCallPlanSegment != null && updateCallPlanSegment.size() > 0){
                SnTDMLSecurityUtil.printDebugMessage('----Calling Execute helper---');
                ruleExecuteHelper.updateSegment(updateCallPlanSegment, true);
                
                SnTDMLSecurityUtil.printDebugMessage('----Called Execute helper---');
                
            }
           
       }
       catch(Exception e){
       SnTDMLSecurityUtil.printDebugMessage('---Exception Caught in updating the profile perametrs---');    
        SnTDMLSecurityUtil.printDebugMessage('--- error: ' + e.getMessage());
        SnTDMLSecurityUtil.printDebugMessage('--- error: ' + e.getStackTraceString());
       }
        
    } */
    
    
    
}