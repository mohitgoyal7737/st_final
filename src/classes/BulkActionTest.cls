@isTest
private class BulkActionTest { 
    static testMethod void testMethod1() {
        String className = 'UpdatePosAcc_Test';
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__AccountType__c ='HCO';
        acc.AccountNumber = '12345';
        insert acc;
        
        AxtriaSalesIQTM__Organization_Master__c orgmas1 = TestDataFactory.createOrganizationMaster();
        orgmas1.Name = 'SnT';
        insert orgmas1;
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(); 
        country.Load_Type__c = 'Full Load';
        country.AxtriaSalesIQTM__Parent_Organization__c = orgmas1.id;
        country.AxtriaSalesIQTM__Status__c = 'Active';
        insert country;
        
        AxtriaSalesIQTM__Geography_Type__c type = new AxtriaSalesIQTM__Geography_Type__c();
        type.AxtriaSalesIQTM__Country__c = country.Id;
        insert type;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(country);
        team.Name = 'ONCO';
        team.AxtriaSalesIQTM__Country__c = country.id;        
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        teamins.AxtriaSalesIQTM__Geography_Type_Name__c  = type.id;
        insert teamins;
        
        AxtriaSalesIQTM__Change_Request_Type__c changereqType = new AxtriaSalesIQTM__Change_Request_Type__c();
        changereqType.AxtriaSalesIQTM__CR_Type_Name__c = 'Call_Plan_Change';
        insert changereqType;
        
        List<AxtriaSalesIQTM__Change_Request__c> changereq= new List<AxtriaSalesIQTM__Change_Request__c>();
        AxtriaSalesIQTM__Change_Request__c g = new AxtriaSalesIQTM__Change_Request__c();
        g.AxtriaSalesIQTM__Status__c = 'Pending';
        //AxtriaSalesIQTM__Approver_One__c
        //AxtriaSalesIQTM__Approver_Two__c
        g.AxtriaSalesIQTM__Account_Moved_Id__c = 'test';
        g.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.id;
        g.AxtriaSalesIQTM__RecordTypeID__c = changereqType.id;
        changereq.add(g);
        insert changereq;
                
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = :className];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            ApexPages.StandardSetController sc = new Apexpages.StandardSetController(changereq);
            BulkAction obj=new BulkAction(sc);
            obj.approveAll();
            
        }
        Test.stopTest();
    }
    
    static testMethod void testMethod2() {
        String className = 'UpdatePosAcc_Test';
        User loggedInUser = new User(id=UserInfo.getUserId());
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__AccountType__c ='HCO';
        acc.AccountNumber = '12345';
        insert acc;
        
        AxtriaSalesIQTM__Organization_Master__c orgmas1 = TestDataFactory.createOrganizationMaster();
        orgmas1.Name = 'SnT';
        insert orgmas1;
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(); 
        country.Load_Type__c = 'Full Load';
        country.AxtriaSalesIQTM__Parent_Organization__c = orgmas1.id;
        country.AxtriaSalesIQTM__Status__c = 'Active';
        insert country;
        
        AxtriaSalesIQTM__Geography_Type__c type = new AxtriaSalesIQTM__Geography_Type__c();
        type.AxtriaSalesIQTM__Country__c = country.Id;
        insert type;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(country);
        team.Name = 'ONCO';
        team.AxtriaSalesIQTM__Country__c = country.id;        
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        teamins.AxtriaSalesIQTM__Geography_Type_Name__c  = type.id;
        insert teamins;
        
        AxtriaSalesIQTM__Change_Request_Type__c changereqType = new AxtriaSalesIQTM__Change_Request_Type__c();
        changereqType.AxtriaSalesIQTM__CR_Type_Name__c = 'Call_Plan_Change';
        insert changereqType;
        
        List<AxtriaSalesIQTM__Change_Request__c> changereq= new List<AxtriaSalesIQTM__Change_Request__c>();
        AxtriaSalesIQTM__Change_Request__c g = new AxtriaSalesIQTM__Change_Request__c();
        g.AxtriaSalesIQTM__Status__c = 'Pending';
        //AxtriaSalesIQTM__Approver_One__c
        //AxtriaSalesIQTM__Approver_Two__c
        g.AxtriaSalesIQTM__Account_Moved_Id__c = 'test';
        g.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.id;
        g.AxtriaSalesIQTM__RecordTypeID__c = changereqType.id;
        changereq.add(g);
        insert changereq;
                
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = :className];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            ApexPages.StandardSetController sc = new Apexpages.StandardSetController(changereq);
            BulkAction obj=new BulkAction(sc);
            obj.rejectRequests();
            
        }
        Test.stopTest();
    }
}