global  with sharing class Integration_Batch_GAS_History_Data2 implements Database.Batchable<sObject>, Database.Stateful, Schedulable {
    /*Replaced Gas_History_Error_Records__c with AxtriaSalesIQTM__Error_Log__c ******* object purge A1422*/
    public String query;
    public List<String> teamInstances;

    public Integer totalRecs;
    public Integer errorRecsCount;
    public Set<String> uniqueGas;
    public Boolean callOtherBatches = false;
    public boolean errorBatches=false;
    public Set<String> gasHistorySet;
    //public Set<String> activityLogIDSet;

    //public List<Scheduler_Log__c> activityLogList = new List<Scheduler_Log__c>();
    
    global Integration_Batch_GAS_History_Data2()
    {
        uniqueGas = new Set<String>();
        gasHistorySet = new Set<String>();
        List<AxtriaSalesIQTM__Team_Instance__c> ti = [select id from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c = 'Current' and (AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Live' OR AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Published')];
        LIst<String> allTeams = new List<String>();
        teamInstances = new List<String>();
        for(AxtriaSalesIQTM__Team_Instance__c t : ti)
        {
            teamInstances.add(t.ID);
        }  
        
        totalRecs = 0;
        errorRecsCount = 0;
        this.query = 'select id, SIQ_Added_Territory__c, SIQ_Account__c, Run_Count__c, Deployment_Status__c from SIQ_GAS_History__c where (Deployment_Status__c = \'New\' OR Deployment_Status__c = null) OR (Deployment_Status__c = \'Rejected\' AND Run_Count__c <3)';

    }
    global Integration_Batch_GAS_History_Data2(Boolean chain)
    {
        callOtherBatches = chain;
        uniqueGas = new Set<String>();
        gasHistorySet = new Set<String>();
        List<AxtriaSalesIQTM__Team_Instance__c> ti = [select id from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c = 'Current' and (AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Live' OR AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Published')];
        LIst<String> allTeams = new List<String>();
        teamInstances = new List<String>();
        for(AxtriaSalesIQTM__Team_Instance__c t : ti)
        {
            teamInstances.add(t.ID);
        }  
        
        totalRecs = 0;
        errorRecsCount = 0;
        this.query = 'select id, SIQ_Added_Territory__c, SIQ_Account__c, Run_Count__c, Deployment_Status__c from SIQ_GAS_History__c where (Deployment_Status__c = \'New\' OR Deployment_Status__c = null) OR (Deployment_Status__c = \'Rejected\' AND Run_Count__c <3)';

    }

    global Integration_Batch_GAS_History_Data2(List<String> teamInstances) {

        uniqueGas = new Set<String>();
        gasHistorySet = new Set<String>();
        this.teamInstances = teamInstances;
        this.query = 'select id, SIQ_Added_Territory__c, SIQ_Account__c, Run_Count__c, Deployment_Status__c from SIQ_GAS_History__c where (Deployment_Status__c = \'New\' OR Deployment_Status__c = null) OR (Deployment_Status__c = \'Rejected\' AND Run_Count__c <3)';

        totalRecs = 0;
        errorRecsCount = 0;

    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }


   global void execute(System.SchedulableContext SC){

              
        database.executeBatch(new Integration_Batch_GAS_History_Data2(),2000);
    }

global void execute(Database.BatchableContext BC, list<SIQ_GAS_History__c> scope) 
    {      
        
        /*try
        {
            */
            Set<String> allAccounts = new Set<String>();
            Set<String> allPositionsSet = new Set<String>();
            Map<String, String> atlMap = new Map<String,String>();
            Map<String,String> veevaToSalesIQID = new Map<String,String>();
            Map<String,String> posIDMapping = new Map<String,String>();
            Map<String,String> posToTeaInstanceMap = new Map<String,String>();
            Map<String,Date> teamInstEnddateMap = new Map<String,Date>();

            Set<String> accountsList = new Set<String>();
            Set<String> posAccAll = new Set<String>();

            List<AxtriaSalesIQTM__Position_Account__c> newPositionAccounts = new List<AxtriaSalesIQTM__Position_Account__c>();


            for(SIQ_GAS_History__c sgh : scope)
            {
                allAccounts.add(sgh.SIQ_Account__c);
                if(sgh.SIQ_Added_Territory__c != null)
                for(String str : sgh.SIQ_Added_Territory__c.split(';'))
                {
                    allPositionsSet.add(str);    
                }
                totalRecs ++;
            }

            for(Account acc : [select id, AZ_VeevaID__c, AccountNumber from Account where AccountNumber in :allAccounts and AxtriaSalesIQTM__Active__c = 'Active'])
            {
                veevaToSalesIQID.put(acc.AccountNumber, acc.id);
                accountsList.add(acc.id);
            }

            List<AxtriaSalesIQTM__Position_Account__c> allStagingRecs = [select AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__c,  AxtriaSalesIQTM__Assignment_Status__c,isGasAssignment__c, AxtriaSalesIQTM__Account_Alignment_Type__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Account__c in :accountsList and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c in :allPositionsSet and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = 'Current' and (AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Live' OR AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Published') and (AxtriaSalesIQTM__Assignment_Status__c = 'Active' or AxtriaSalesIQTM__Assignment_Status__c = 'Future Active')];

            List<AxtriaSalesIQTM__Position__c> allPositions = [select id, AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Client_Position_Code__c in :allPositionsSet and AxtriaSalesIQTM__Team_Instance__c in :teamInstances and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = 'Current' and (AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Live' OR AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Published') and AxtriaSalesIQTM__inactive__c = false];

            for(AxtriaSalesIQTM__Position__c pos : allPositions)
            {
                posIDMapping.put(pos.AxtriaSalesIQTM__Client_Position_Code__c, pos.ID);
                posToTeaInstanceMap.put(pos.AxtriaSalesIQTM__Client_Position_Code__c, pos.AxtriaSalesIQTM__Team_Instance__c);
                if(pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c != null){
                    teamInstEnddateMap.put(pos.AxtriaSalesIQTM__Team_Instance__c,pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c);
                }else {
                    teamInstEnddateMap.put(pos.AxtriaSalesIQTM__Team_Instance__c,Date.newInstance(2099,12,31));
                }
                
            }

            for(AxtriaSalesIQTM__Position_Account__c sa : allStagingRecs)
            {
                posAccAll.add(sa.AxtriaSalesIQTM__Account__c + sa.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
                if(sa.isGasAssignment__c && sa.AxtriaSalesIQTM__Account_Alignment_Type__c == 'Explicit'){
                    gasHistorySet.add(sa.AxtriaSalesIQTM__Account__c + sa.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
                }
            }

            //NEED TO ADD RECORDS TO CP AFTER Global exclusion rule
            List<AxtriaSalesIQTM__Error_Log__c> errorRecs = new List<AxtriaSalesIQTM__Error_Log__c>();

            //List to Inactivate position Accounts which came again from Gas assignment (explicitly).
            List<AxtriaSalesIQTM__Position_Account__c> posAccListToInactivate = new List<AxtriaSalesIQTM__Position_Account__c>();
            for(SIQ_GAS_History__c sgh : scope)
            {
                if(sgh.SIQ_Added_Territory__c != null)
                for(String str : sgh.SIQ_Added_Territory__c.split(';'))
                {
                    String accInfo;

                    if(veevaToSalesIQID.containsKey(sgh.SIQ_Account__c))
                    {
                        accInfo = veevaToSalesIQID.get(sgh.SIQ_Account__c);
                    }
                    else
                    {
                        accInfo = '';
                    }
                    String accIdPosCode = accInfo + str;

                    // To avoid duplicate records in Gas History for same Account & same position
                    if(!gasHistorySet.contains(accIdPosCode)) {
                        gasHistorySet.add(accIdPosCode);
                        
                        // To Inactivate position Accounts which came again from Gas assignment (explicitly).
                        if(posAccAll.contains(accIdPosCode)){
                            for(AxtriaSalesIQTM__Position_Account__c obj : allStagingRecs) {  
                                if(obj.AxtriaSalesIQTM__Account__c == accInfo  && obj.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c == str && obj.AxtriaSalesIQTM__Team_Instance__c == posToTeaInstanceMap.get(str)){
                                obj.AxtriaSalesIQTM__Effective_End_Date__c = Date.today().addDays(-1);
                                posAccListToInactivate.add(obj);
                            }
                        }
                    }

                    /*if(!posAccAll.contains(accInfo))
                    {*/
                            AxtriaSalesIQTM__Position_Account__c posAcc = new AxtriaSalesIQTM__Position_Account__c();
                            posAcc.AxtriaSalesIQTM__Team_Instance__c = posToTeaInstanceMap.get(str);
                            posAcc.AxtriaSalesIQTM__Account_Alignment_Type__c  = 'Explicit';
                            posAcc.Batch_Name__c = 'Gas Assignment Batch';
                            posAcc.AxtriaSalesIQTM__TempDestinationrgb__c = 'Gas Assign';
                            posAcc.AxtriaSalesIQTM__Segment_1__c   = 'Primary';
                            sgh.Run_Count__c = sgh.Run_Count__c + 1;
                            posAcc.isGasAssignment__c = true;
                            if(veevaToSalesIQID.containsKey(sgh.SIQ_Account__c))
                            {
                                posAcc.AxtriaSalesIQTM__Account__c = veevaToSalesIQID.get(sgh.SIQ_Account__c);    
                            }
                            else
                            {
                                errorRecs.add(new AxtriaSalesIQTM__Error_Log__c(SIQ_GAS_History__c = sgh.id, Reason__c = 'Account Not Found',Type__c = 'GAS History Error Records'));
                                //sgh.Run_Count__c = sgh.Run_Count__c + 1;
                                sgh.Deployment_Status__c = 'Rejected';
                                errorRecsCount++;
                                continue;
                            }

                            if(posIDMapping.containsKey(str))
                                posAcc.AxtriaSalesIQTM__Position__c = posIDMapping.get(str);
                            else
                            {
                                errorRecs.add(new AxtriaSalesIQTM__Error_Log__c(SIQ_GAS_History__c = sgh.id, Reason__c = 'Territory Not Found',Type__c = 'GAS History Error Records'));
                                //sgh.Run_Count__c = sgh.Run_Count__c + 1;
                                sgh.Deployment_Status__c = 'Rejected';
                                errorRecsCount++;
                                continue;
                            }

                            posAcc.AxtriaSalesIQTM__Effective_Start_Date__c =  System.today();
                            //posAcc.AxtriaSalesIQTM__Effective_End_Date__c = Date.newInstance(2099,12,31);
                            posAcc.AxtriaSalesIQTM__Effective_End_Date__c = teamInstEnddateMap.get(posAcc.AxtriaSalesIQTM__Team_Instance__c);

                            if(!uniqueGas.contains(posAcc.AxtriaSalesIQTM__Account__c + '_'+ posAcc.AxtriaSalesIQTM__Position__c + '_'+  posAcc.AxtriaSalesIQTM__Team_Instance__c))
                            {
                                newPositionAccounts.add(posAcc);                        
                                uniqueGas.add(posAcc.AxtriaSalesIQTM__Account__c + '_'+ posAcc.AxtriaSalesIQTM__Position__c + '_'+ posAcc.AxtriaSalesIQTM__Team_Instance__c);  
                            }
                            sgh.Deployment_Status__c = 'Processed';
                    /*}
                    else
                    {
                        errorRecs.add(new AxtriaSalesIQTM__Error_Log__c(SIQ_GAS_History__c = sgh.id, Reason__c = 'Account already assigned to territory'));
                        sgh.Deployment_Status__c = 'Rejected';
                        sgh.Run_Count__c = sgh.Run_Count__c + 1;
                        errorRecsCount++;
                        //sgh.Deployment_Status__c = 'Rejected';
                        //sgh.Deployment_Status__c = 'Not Required';
                    }*/
                }
                }

                //}
                /*else
                {
                errorRecs.add(new AxtriaSalesIQTM__Error_Log__c(SIQ_GAS_History__c = sgh.id, Reason__c = 'Account Not Found'));
                //sgh.Run_Count__c = sgh.Run_Count__c + 1;
                sgh.Run_Count__c = sgh.Run_Count__c + 1;
                errorRecsCount++;
                continue;
                }*/

            }

            if(errorRecs.size()>0){
                //insert errorRecs;
                SnTDMLSecurityUtil.insertRecords(errorRecs, 'Integration_Batch_GAS_History_Data2');
            }

            if(posAccListToInactivate != null && posAccListToInactivate.size()>0){
                //update posAccListToInactivate;
                SnTDMLSecurityUtil.updateRecords(posAccListToInactivate, 'Integration_Batch_GAS_History_Data2');
            }

            //insert newPositionAccounts;
            SnTDMLSecurityUtil.insertRecords(newPositionAccounts, 'Integration_Batch_GAS_History_Data2');
            //update scope;
            SnTDMLSecurityUtil.updateRecords(scope, 'Integration_Batch_GAS_History_Data2');

        /*}catch(Exception e)
           {
                errorBatches=true;
                system.debug('++inside catch');
                String Header='Gas History batch is failed, so delta has been haulted of this org for Today';
                tryCatchEmailAlert.veevaLoad(Header);
           }*/
    }

    global void finish(Database.BatchableContext BC) 
    {
        Scheduler_Log__c slog = new Scheduler_Log__c();
        slog.Changes__c = 'GAS Assignment';
        //slog.Created_Date__c = DateTime.now();
        slog.Created_Date2__c = DateTime.now();
        //s1.Cycle__c = 
        slog.Job_Type__c = 'Inbound';
        slog.Job_Name__c = 'GAS Assignment';
        slog.Job_Status__c = 'Executed';
        slog.No_of_Records_Processed__c = totalRecs;
        slog.Total_Errors__c = errorRecsCount;
        //insert slog;
         SnTDMLSecurityUtil.insertRecords(slog, 'Integration_Batch_GAS_History_Data2');
        
        if(callOtherBatches)
        {
            

            list<string> teaminstancelistis = new list<string>();
            teaminstancelistis = StaticTeaminstanceList.getAllCountries();
            database.executeBatch(new ATL_Delete_Recs(teaminstancelistis, callOtherBatches),2000);
            
            /*list<string> teaminstancelistForTSFis = new list<string>();
            teaminstancelistForTSFis = StaticTeaminstanceList.getFullLoadTeamInstances();
            database.executeBatch(new changeTSFStatus(teaminstancelistForTSFis, true),2000);*/
        }
    }
}