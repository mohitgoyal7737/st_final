@isTest
public class Grid_Matrix_Clone_Test 
{
    @istest static void Grid_Matrix_TestOther()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        AxtriaSalesIQTM__Organization_Master__c om=new AxtriaSalesIQTM__Organization_Master__c();
        om.name='ES';
        om.CurrencyIsoCode='USD';
        om.AxtriaSalesIQTM__Parent_Country_Level__c=true;
        om.AxtriaSalesIQTM__Org_Level__c='Global';
        insert om;
        
        AxtriaSalesIQTM__Country__c c=new AxtriaSalesIQTM__Country__c();
        c.name='ES';
        c.CurrencyIsoCode='USD';
        c.AxtriaSalesIQTM__Parent_Organization__c=om.id;
        c.AxtriaSalesIQTM__Status__c='Active';
        insert c;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(c);
        insert team;

        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = c.id;
        insert workspace;

        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;

        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;

        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, c);
        insert pcc;
        
        Grid_Master__c g= TestDataFactory.gridMaster(c);
        g.Brand__c = pcc.id;
        g.Grid_Type__c = '1D';        
        insert g;
        
        Step__c  s=new Step__c();
        s.Matrix__c =g.id;
        insert s;

        Grid_Details__c gDetails = TestDataFactory.gridDetails(g);
        insert gDetails;

        list<Grid_Details__c>updategriddetails=new list<Grid_Details__c>();
        updategriddetails.add(gDetails);

        System.Test.startTest();
        System.runAs(loggedInUser)
        {
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));

            SalesIQUtility.setCookieString('CountryID', c.id);
            ApexPages.StandardController sc = new ApexPages.standardController(g);

            Grid_Matrix_Clone obj=new Grid_Matrix_Clone(sc);
            obj.ViewBlock('View', g.id);
            obj.CloneBlock('Clone', g.id); 
            obj.EditBlock('Edit', g.id);
            obj.show2D();
            obj.createtable();
            obj.createtable2();
            obj.Clonegriddetails();
            obj.comp_grid();

            obj.gridMaster = TestDataFactory.gridMaster(c);
            obj.gridMaster.Brand__c = pcc.Id;
            obj.gridMaster.Grid_Type__c = '1D';
            obj.JSONdata = JSON.serialize(updategriddetails);
            obj.savegriddetail();
            obj.cancelfun();
            
            obj.JSONdata = JSON.serialize(updategriddetails);
            obj.updategrid(); 
        }
        System.Test.stopTest();
    }
}