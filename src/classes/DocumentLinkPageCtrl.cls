public class DocumentLinkPageCtrl {
    public String Documentid {get;set;}
    public String Bu {get;set;}
    public String Cycle {get;set;}
    public String UserType {get;set;}
    public String Documentname {get;set;}
    public String Positionname {get;set;}
    public List<AxtriaSalesIQTM__User_Access_Permission__c> loggedInUserData  {get;set;}
    public list<Document>DocList {set;get;}
    public String User {get;set;}
    
    public DocumentLinkPageCtrl(){
        loggedInUserData     = new List<AxtriaSalesIQTM__User_Access_Permission__c>();
        loggedInUserData = SalesIQUtility.getUserAccessPermistion(Userinfo.getUserId());
        DocList = new list<Document>();
        Documentid='';
        Bu='';
        Cycle='';
        UserType = '';
        Documentname='';
        Positionname='';
        User='';
        if(loggedInUserData !=null && loggedInUserData.size()!=0){
            UserType = loggedInUserData[0].AxtriaSalesIQTM__Position__r.AXTRIASALESIQTM__HIERARCHY_LEVEL__C;
            Bu = loggedInUserData[0].AxtriaSalesIQTM__Team_Instance__r.name;
            Cycle = loggedInUserData[0].AxtriaSalesIQTM__Team_Instance__r.Cycle__r.name;
           Positionname = loggedInUserData[0].AxtriaSalesIQTM__Position__r.Name;
        }
    }
    public Void fetchdocument(){
       // if(UserType !='4' && UserType !='3'){
            try{
             if(Positionname !='Nation' && UserType !='4'){
                //System.debug('&&&Positionname1:'+Positionname);
                //System.debug('&&&UserType1:'+UserType);
                User='Non_HO';
                Documentname=Bu+'_'+Cycle;
            }
            
            if(Documentname!='' && Documentname !=null){
                //Documentname='RIA CRESTOR_S2 2017';
                System.debug('@@Document Name is:'+Documentname);
                Document DD = [select id,Name from Document where name =:Documentname limit 1 ];
                Documentid=DD.id;
            }
            if(Positionname == 'Nation' || Positionname =='nation'){
                string cyc = '%'+cycle+'%';
                User = 'HO';
                //System.debug('&&&Positionname:'+Positionname);
                //System.debug('&&&UserType:'+UserType);
                //System.debug('$$$cyc:'+cyc);
                DocList = [select id,Name from Document where Name LIKE :cyc ];
            }
            system.debug('@@@DocList:'+DocList);
            }
            Catch(Exception e){
                System.debug('@@@@Exception Caught:'+e);
            }
    }
    
    
}