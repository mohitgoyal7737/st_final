//Changed by Himanshu Tariyal on 8th January 2018 for Unit Testing
global class BatchDeleteRecsBeforereExecuteACFcopy implements Database.Batchable<sObject> {
    public String query;
    public String ruleId;
    public String whereClause;

    global BatchDeleteRecsBeforereExecuteACFcopy(String ruleId, String whereClause) 
    {       
        this.ruleId = ruleId;
        this.whereClause = whereClause;
        this.query = 'select id from Account_Compute_Final_Copy__c where Measure_Master__c = \''+ruleId+'\'';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Sobject> scope) 
    {
        if(scope.size()>0 && scope!=null)
            Database.DeleteResult[] srList = Database.delete(scope, false);
    }

    global void finish(Database.BatchableContext BC) 
    {
        BatchDeleteRecsBeforereExecuteSS batchExecute = new BatchDeleteRecsBeforereExecuteSS(ruleId, WhereClause );
        Database.executeBatch(batchExecute, 2000);
        
    }
}