global class BatchCreate_UnassignedPosGeo1 implements Database.Batchable<sObject>, Database.stateful, Schedulable{
    
    global String geoDeltaQuery;
    public String batchID;
    global DateTime lastjobDate=null;
    public Integer recordsProcessed=0;
    global List<AxtriaARSnt__Zip_Type__mdt> zipMdt = null;
    List<String> countryList = null;
    Set<String> zipTypes = null;
    global List<AxtriaSalesIQTM__Team_Instance__c>teamInstList ;
    global set<string> geotype;
    
    global BatchCreate_UnassignedPosGeo1(){

        List<AxtriaARSnt__Scheduler_Log__c> schLogList = new List<AxtriaARSnt__Scheduler_Log__c>();
        System.debug('Before scheduler');
        schLogList = [Select Id,AxtriaARSnT__Created_Date__c, AxtriaARSnT__Created_Date2__c from AxtriaARSnt__Scheduler_Log__c where AxtriaARSnt__Job_Name__c='Unassigned PosGeo Delta' and AxtriaARSnt__Job_Status__c='Successful' Order By AxtriaARSnt__Created_Date2__c desc];
        System.debug('After scheduler'  + schLogList);
        if(schLogList.size() > 0){
          lastjobDate=schLogList[0].AxtriaARSnt__Created_Date2__c;  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
        
        System.debug('last job'+lastjobDate);
        
        AxtriaARSnt__Scheduler_Log__c sJob = new AxtriaARSnt__Scheduler_Log__c();
        sJob.AxtriaARSnt__Job_Name__c = 'Unassigned PosGeo Delta';
        sJob.AxtriaARSnt__Job_Status__c = 'Failed';
        sJob.AxtriaARSnt__Job_Type__c='Inbound';
        sJob.AxtriaARSnt__Created_Date2__c = DateTime.now();
        insert sJob;
        batchID = sJob.Id;

        zipMdt = [Select Id, Label From AxtriaARSnt__Zip_Type__mdt];
        zipTypes = new Set<String>();
        for(AxtriaARSnt__Zip_Type__mdt zipType:zipMdt){
            zipTypes.add(zipType.Label);
        }

        countryList = new List<String>();
        for(AxtriaSalesIQTM__Country__c country: [select id,Name,AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c]) {
            if(!countryList.contains(country.AxtriaSalesIQTM__Country_Code__c)) {
                countryList.add(country.AxtriaSalesIQTM__Country_Code__c);
            }  
        }
        System.debug('Countries List :: ' + countryList);

        geotype = new Set<String>();
        teamInstList = new List<AxtriaSalesIQTM__Team_Instance__c>();
        teaminstlist = [select id,name,AxtriaSalesIQTM__Geography_Type_Name__r.name,AxtriaSalesIQTM__Geography_Type_Name__c,AxtriaSalesIQTM__Team__c,AxtriaSalesIQTM__Team__r.name from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c IN  :countryList and AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Live' and AxtriaSalesIQTM__Alignment_Period__c='Current'];
        for(AxtriaSalesIQTM__Team_Instance__c teamInst: teamInstList){
            geotype.add(teamInst.AxtriaSalesIQTM__Geography_Type_Name__c);
        }
        System.debug('Geotypes List :: ' + geotype);
        if(geotype != null && geotype.size()>0){
            geoDeltaQuery = 'SELECT Id, Name, AxtriaARSnT__Country_Code__c, AxtriaSalesIQTM__Geography_Type__c FROM AxtriaSalesIQTM__Geography__c where AxtriaSalesIQTM__Zip_Type__c IN: zipTypes and AxtriaSalesIQTM__Geography_Type__c IN :geotype';
            if(lastjobDate!=null){
              geoDeltaQuery = geoDeltaQuery + ' and CreatedDate >=: lastjobDate '; 
            }
            this.geoDeltaQuery = geoDeltaQuery;
        }else {
            this.geoDeltaQuery = 'SELECT Id, Name, AxtriaARSnT__Country_Code__c, AxtriaSalesIQTM__Geography_Type__c FROM AxtriaSalesIQTM__Geography__c where Id = null';
        }
        system.debug('======The query is:::'+geoDeltaQuery);

    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(geoDeltaQuery);
    }
    
    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Geography__c> geoDeltaList) {
        
        System.debug('countries :: ' + countryList);
        System.debug('GeoDeltaList size ' + geoDeltaList.size());

        //Create Map of <Geography Type, Geography>
        Map<String, List<AxtriaSalesIQTM__Geography__c>> geoType2geoMap = new Map<String, List<AxtriaSalesIQTM__Geography__c>>();
        for(AxtriaSalesIQTM__Geography__c geo: geoDeltaList){
            if(!geoType2geoMap.containsKey(geo.AxtriaSalesIQTM__Geography_Type__c)){
                geoType2geoMap.put(geo.AxtriaSalesIQTM__Geography_Type__c, new list<AxtriaSalesIQTM__Geography__c>{geo});
            }else{
                geoType2geoMap.get(geo.AxtriaSalesIQTM__Geography_Type__c).add(geo);
            }
        }
        System.debug('geoType2geoMap size :: ' + geoType2geoMap.size());

        //Create Map of <Geography Type, Position Team Instance>
        Map<String, List<AxtriaSalesIQTM__Position_Team_Instance__c>> geoType2posTeamInsMap = new Map<String, List<AxtriaSalesIQTM__Position_Team_Instance__c>>();
        set<string>period= new set<string>();
        period.add('Current');
        period.add('Future');

        List<AxtriaSalesIQTM__Position_Team_Instance__c> posTeamInsLst = new List<AxtriaSalesIQTM__Position_Team_Instance__c>();
        posTeamInsLst = [select id,name,AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Position_ID__c,AxtriaSalesIQTM__Team_Instance_ID__c,AxtriaSalesIQTM__Team_Instance_ID__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c, AxtriaSalesIQTM__Team_Instance_ID__r.AxtriaSalesIQTM__Geography_Type_Name__c from AxtriaSalesIQTM__Position_Team_Instance__c where AxtriaSalesIQTM__Team_Instance_ID__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c IN : countryList and AxtriaSalesIQTM__Team_Instance_ID__r.AxtriaSalesIQTM__Geography_Type_Name__c IN : geoType2geoMap.keySet() and AxtriaSalesIQTM__Position_ID__r.name='Unassigned' and AxtriaSalesIQTM__Team_Instance_ID__r.AxtriaSalesIQTM__Alignment_Period__c IN : period and AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__Client_Position_Code__c = '00000' ];
        
        System.debug('posTeamInsLst size is ' + posTeamInsLst.size());
        for(AxtriaSalesIQTM__Position_Team_Instance__c posTeamIns : posTeamInsLst){
            if(posTeamIns.AxtriaSalesIQTM__Team_Instance_ID__r.AxtriaSalesIQTM__Geography_Type_Name__c != null){
                if(!geoType2posTeamInsMap.containsKey(posTeamIns.AxtriaSalesIQTM__Team_Instance_ID__r.AxtriaSalesIQTM__Geography_Type_Name__c)){
                    geoType2posTeamInsMap.put(posTeamIns.AxtriaSalesIQTM__Team_Instance_ID__r.AxtriaSalesIQTM__Geography_Type_Name__c, new List<AxtriaSalesIQTM__Position_Team_Instance__c>{posTeamIns});
                }else{
                    geoType2posTeamInsMap.get(posTeamIns.AxtriaSalesIQTM__Team_Instance_ID__r.AxtriaSalesIQTM__Geography_Type_Name__c).add(posTeamIns);
                }
            }
        }
        System.debug('geoType2posTeamInsMap size :: ' + geoType2posTeamInsMap.size());

        List<AxtriaSalesIQTM__Position_Geography__c> positionGeoListToInsert = new List<AxtriaSalesIQTM__Position_Geography__c>();
        for(String geoType : geoType2geoMap.keySet()){

            list<AxtriaSalesIQTM__Geography__c>geolist = geoType2geoMap.get(geoType);
            list<AxtriaSalesIQTM__Position_Team_Instance__c>posteamlst = geoType2posTeamInsMap.get(geoType);
            if(geolist!=null && posteamlst!=null){
                System.debug('geoType : '+ geoType + ', geolist size : ' + geolist.size() + ', posteamlst size : ' + posteamlst.size());
                for(AxtriaSalesIQTM__Geography__c geography: geolist){
                    for(AxtriaSalesIQTM__Position_Team_Instance__c posTeamIns : posteamlst){
                        AxtriaSalesIQTM__Position_Geography__c positionGeo = new AxtriaSalesIQTM__Position_Geography__c();
                        positionGeo.AxtriaSalesIQTM__Position__c =  posTeamIns.AxtriaSalesIQTM__Position_ID__c;
                        positionGeo.AxtriaSalesIQTM__Position_Team_Instance__c = posTeamIns.Id;
                        positionGeo.AxtriaSalesIQTM__Geography__c = geography.Id;
                        positionGeo.AxtriaSalesIQTM__Proposed_Position__c = posTeamIns.AxtriaSalesIQTM__Position_ID__c;
                        positionGeo.AxtriaSalesIQTM__Position_Id_External__c = posTeamIns.AxtriaSalesIQTM__Position_ID__c;
                        positionGeo.AxtriaSalesIQTM__Effective_End_Date__c = posTeamIns.AxtriaSalesIQTM__Effective_End_Date__c;
                        positionGeo.AxtriaSalesIQTM__Effective_Start_Date__c = posTeamIns.AxtriaSalesIQTM__Effective_Start_Date__c;
                        positionGeo.AxtriaSalesIQTM__Team_Instance__c = posTeamIns.AxtriaSalesIQTM__Team_Instance_ID__c;
                        positionGeo.Name = geography.Name;
                        positionGeoListToInsert.add(positionGeo);
                    }
                }
            }
        }

        System.debug('positionGeoListToInsert size is ' + positionGeoListToInsert.size());
        recordsProcessed += positionGeoListToInsert.size();
        System.debug('recordsProcessed is ' + recordsProcessed);
        if(positionGeoListToInsert.size()>0){
            insert positionGeoListToInsert;
        }
    }
    
    global void execute(SchedulableContext sc){
        Database.executeBatch(new BatchCreate_UnassignedPosGeo1(), 200);
    }
    global void finish(Database.BatchableContext BC) {
        AxtriaARSnt__Scheduler_Log__c sJob = new AxtriaARSnt__Scheduler_Log__c(id = batchID); 
        sJob.AxtriaARSnt__No_Of_Records_Processed__c=recordsProcessed;
        sJob.AxtriaARSnt__Job_Status__c='Successful';
        update sJob;
    }
    
    
}