/*Author - Himanshu Tariyal(A0994)
Date : 25th January 2018*/
global with sharing class ScheduleBUResponsePushProcess implements Schedulable 
{
    global void execute(SchedulableContext sc) 
    {
        List<ScheduledTeamInstances__c> scheduledTIList = new List<ScheduledTeamInstances__c>();
        //scheduledTIList = [select id from ScheduledTeamInstances__c];
        scheduledTIList = [select id from ScheduledTeamInstances__c where Type__c = 'File'];
        SnTDMLSecurityUtil.printDebugMessage('scheduledTIList size()-->'+scheduledTIList.size());
        if(scheduledTIList!=null && scheduledTIList.size()>0){
            //delete scheduledTIList;
            SnTDMLSecurityUtil.deleteRecords(scheduledTIList, 'ScheduleBUResponsePushProcess');
        }
        
        String teamInstQuery = 'select id,Name from AxtriaSalesIQTM__Team_Instance__c where isVeevaSurveyScheduled__c = true WITH SECURITY_ENFORCED';
        SnTDMLSecurityUtil.printDebugMessage('teamInstQuery-->'+teamInstQuery);
        List<AxtriaSalesIQTM__Team_Instance__c> listTI = Database.query(teamInstQuery);
        SnTDMLSecurityUtil.printDebugMessage('listTI size()-->'+listTI.size());
        if(listTI!=null && listTI.size()>0)
        {
            scheduledTIList = new List<ScheduledTeamInstances__c>();
            for(AxtriaSalesIQTM__Team_Instance__c ti : listTI)
            {
                ScheduledTeamInstances__c sti = new ScheduledTeamInstances__c(Name=ti.id);
                sti.Team_Instance_Name__c = ti.Name;
                sti.Type__c = 'File';
                scheduledTIList.add(sti);
            }
            if(scheduledTIList!=null && scheduledTIList.size()>0)
            {
                SnTDMLSecurityUtil.printDebugMessage('scheduledTIList size to be inserted-->'+scheduledTIList.size());
                //insert scheduledTIList;
                SnTDMLSecurityUtil.insertRecords(scheduledTIList, 'ScheduleBUResponsePushProcess');
                String teamInstName = scheduledTIList[0].Team_Instance_Name__c;
               //delete scheduledTIList[0];
                SnTDMLSecurityUtil.deleteRecords(scheduledTIList[0], 'ScheduleBUResponsePushProcess');
                if(!System.Test.isRunningTest()){
                    Survey_Def_Prof_Para_Meta_Insert_Utility svsd = new Survey_Def_Prof_Para_Meta_Insert_Utility(teamInstName,true);
                    Database.executeBatch(svsd,500);
                }
            }
        }
    }
}