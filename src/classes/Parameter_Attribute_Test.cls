/*
@author - Prince Richard Augustin (A2661)
@description - Test class to test Parameter_Attribute Controller.
*/

@isTest
private class Parameter_Attribute_Test {


    static testMethod void testMethod1() {
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;
        Parameter_Attribute_Setting__c sett = TestDataFactory.createParamAttrSetting();
        sett.API_Name__c = 'Name';
        insert sett; 
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        Parameter_Attribute p = new Parameter_Attribute();
        p.selectedDate = '02/20/2019';
        p.selectedTeamInstance = teamins.Id;
        p.TeamInstancename = teamins.Name;
        p.selectedcycle = teamins.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c;        
        p.getBusinessUnit(); 
        p.getApiNames();
        p.accField = sett.API_Name__c;
        p.selectedTeamInstanceName = 'ALL';
        p.run_BU_Response_Account_Update_Batch();
        p.run_BU_Response_TSF_Accessibility_Batch();
        
    }

    static testMethod void testMethod3() {
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;
        Parameter_Attribute_Setting__c sett = TestDataFactory.createParamAttrSetting();
        sett.API_Name__c = 'Name';
        insert sett; 
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team,teamins,countr);
		pcc.isActive__c = true;
        insert pcc;
        system.debug('check pcc team'+pcc.Team_Instance__c);
        Parameter__c pc = new Parameter__c();
        pc.Name__c = 'test';
        pc.Team_Instance__c = teamins.id;
        pc.Product_Catalog__c = pcc.id;
        insert pc;
         
         MetaData_Definition__c mdt = new MetaData_Definition__c();
         mdt.Display_Name__c = 'test';
         mdt.Team_Instance__c = teamins.id;
         mdt.Product_Catalog__c = pcc.id;
         insert mdt;
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        Parameter_Attribute p = new Parameter_Attribute();
        p.selectedcycle = teamins.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c;  
		p.selectedTeamInstance = teamins.id;
        p.getBusinessUnit();
        p.getApiNames();
        p.accField = sett.API_Name__c;
        p.run_BU_Response_Account_Update_Batch();
        p.run_BU_Response_TSF_Accessibility_Batch();
    }
}