/*Author - Himanshu Tariyal(A0994)
Date : 7th January 2018*/
@isTest
private class BatchDeleteRecsBeforeReexecuteTest 
{
	private static testMethod void firstTest() 
	{
		/*Create initial test data for all the objs reqd.*/
		User loggedInUser = new User(id=UserInfo.getUserId());

		AxtriaSalesIQTM__Organization_Master__c aom = new AxtriaSalesIQTM__Organization_Master__c(AxtriaSalesIQTM__Org_Level__c='Global',AxtriaSalesIQTM__Parent_Country_Level__c=true);
		insert aom;
		
		AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='Italy',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active');
		insert country;
		
		
		Account acc = new Account(Name='test acc',Marketing_Code__c='EU');
		insert acc;
		
		Account acc2 = new Account(Name='test acc2',Marketing_Code__c='EU');
		insert acc2;
		
		AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='Test Team',AxtriaSalesIQTM__Country__c=country.id);
		insert team;
		
		AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU',AxtriaSalesIQTM__Team__c=team.id);
		insert ti;
		
		AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c(Name='Pos',AxtriaSalesIQTM__Team_iD__c=team.id);
		insert pos;
		
		AxtriaSalesIQTM__Position__c pos2 = new AxtriaSalesIQTM__Position__c(Name='Pos2',AxtriaSalesIQTM__Team_iD__c=team.id);
		insert pos2;

		Product_Catalog__c pc = new Product_Catalog__c(Name='Test Product',Team_Instance__c=ti.id,Veeva_External_ID__c = 'ProdId',Product_Code__c= 'ProdId',IsActive__c=true,Country_Lookup__c=country.id);
		insert pc;
		
	    /*Brand_Team_Instance__c bti = [select id from Brand_Team_Instance__c where Brand__c=:pc.id and Team_Instance__c=:ti.id];
	    */
	    Measure_Master__c mm = new Measure_Master__c(Team_Instance__c=ti.id,Brand_Lookup__c=pc.id,State__c='Executed');
	    mm.Team__c=team.id;
	    insert mm;
	    
	    AxtriaSalesIQTM__Position_Account__c pa = new AxtriaSalesIQTM__Position_Account__c(AxtriaSalesIQTM__Team_Instance__c=ti.id,AxtriaSalesIQTM__Account__c=acc.id,AxtriaSalesIQTM__Position__c=pos.id);
	    insert pa;
	    
	    AxtriaSalesIQTM__Position_Account__c pa2 = new AxtriaSalesIQTM__Position_Account__c(AxtriaSalesIQTM__Team_Instance__c=ti.id,AxtriaSalesIQTM__Account__c=acc.id,AxtriaSalesIQTM__Position__c=pos2.id);
	    insert pa2;
	    
	    AxtriaSalesIQTM__Position_Account__c pa3 = new AxtriaSalesIQTM__Position_Account__c(AxtriaSalesIQTM__Team_Instance__c=ti.id,AxtriaSalesIQTM__Account__c=acc2.id,AxtriaSalesIQTM__Position__c=pos.id);
	    insert pa3;
	    
	    AxtriaSalesIQTM__Position_Account__c pa4 = new AxtriaSalesIQTM__Position_Account__c(AxtriaSalesIQTM__Team_Instance__c=ti.id,AxtriaSalesIQTM__Account__c=acc2.id,AxtriaSalesIQTM__Position__c=pos2.id);
	    insert pa4;
	    
	    //inserting BU Response data
	    BU_Response__c bur = new BU_Response__c(Team_Instance__c=ti.id,Position_Account__c=pa.id,Product__c=pc.id);
	    bur.Physician__c = acc.id;
	    
	    bur.Response1__c = '10';
	    bur.Response2__c = '12';
	    insert bur;
	    
	    BU_Response__c bur2 = new BU_Response__c(Team_Instance__c=ti.id,Position_Account__c=pa2.id,Product__c=pc.id);
	    bur2.Physician__c = acc.id;
	    
	    bur2.Response1__c = '17';
	    bur2.Response2__c = '12';
	    insert bur2;
	    
	    BU_Response__c bur3 = new BU_Response__c(Team_Instance__c=ti.id,Position_Account__c=pa3.id,Product__c=pc.id);
	    bur3.Physician__c = acc2.id;
	    
	    bur3.Response1__c = '20';
	    bur3.Response2__c = '127';
	    insert bur3;
	    
	    BU_Response__c bur4 = new BU_Response__c(Team_Instance__c=ti.id,Position_Account__c=pa4.id,Product__c=pc.id);
	    bur4.Physician__c = acc2.id;
	    
	    bur4.Response1__c = '96';
	    bur4.Response2__c = '112';
	    insert bur4;

        //inserting MetaData_Definition__c data
	    MetaData_Definition__c md = new MetaData_Definition__c();
	    md.Source_Field__c = 'Response1__c';
	    md.Display_Name__c = 'H2-Potenziale BYDUREON';
	    md.Type__c = 'Text';
	    md.Sequence__c = 1;
	    md.Product_Catalog__c = pc.id;
	    md.Source_Object__c = 'BU_Response__c';
	    md.Team_Instance__c = ti.id;
	    insert md;
	    
	    MetaData_Definition__c md2 = new MetaData_Definition__c();
	    md2.Source_Field__c = 'Response2__c';
	    md2.Display_Name__c = 'H2-CL BYDUREON';
	    md2.Type__c = 'Text';
	    md2.Sequence__c = 2;
	    md2.Product_Catalog__c = pc.id;
	    md2.Source_Object__c = 'BU_Response__c';
	    md2.Team_Instance__c = ti.id;
	    insert md2;
	    
	    //push Rule Parameter and its associated step
	    Step__c testStep = new Step__c(Name='H2-Potenziale BYDUREON');
	    insert testStep;
	    
	    Step__c testStep2 = new Step__c(Name='H2-CL BYDUREON');
	    insert testStep2;
	    
	    Rule_Parameter__c rp1 = new Rule_Parameter__c(Step__c=testStep.id,Measure_Master__c=mm.id);
	    insert rp1;
	    
	    Rule_Parameter__c rp2 = new Rule_Parameter__c(Step__c=testStep2.id,Measure_Master__c=mm.id);
	    insert rp2;
	    
	    //insert grid
	    Grid_Master__c matrix = new Grid_Master__c(Name='Test Matrix',Grid_Type__c='2D',Dimension_1_Name__c='Dim1',Dimension_2_Name__c='Dim2',Country__c=country.id);
	    insert matrix;
	    
	    //insert step data for all 3 values
	    Compute_Master__c cm1 = new Compute_Master__c();
	    cm1.Expression__c = '{"elif":[{"andOr":[],"input":"H2-Potenziale BYDUREON","condition":"less than","match":"70","returVal":"Average"},'+
	    '{"andOr":[],"input":"H2-Potenziale BYDUREON","condition":"less than or equal to","match":"90","returVal":"Medium"}],'+
	    '"ifCase":{"andOr":[],"input":"H2-Potenziale BYDUREON","condition":"less than","match":"50","returVal":"Low"},'+
	    '"elseCase":{"andOr":[],"returVal":"High"}}';
	    insert cm1;
	    
	    Compute_Master__c cm2 = new Compute_Master__c();
	    cm2.Expression__c = '{"elif":[{"andOr":[],"input":"H2-CL BYDUREON","condition":"greater than or equal to","match":"70","returVal":"Medium"},'+
	    '{"andOr":[],"input":"H2-CL BYDUREON","condition":"greater than","match":"50","returVal":"Average"}],'+
	    '"ifCase":{"andOr":[],"input":"H2-CL BYDUREON","condition":"greater than","match":"90","returVal":"High"},'+
	    '"elseCase":{"andOr":[],"returVal":"Low"}}';
	    insert cm2;
	    Step__c potStep = new Step__c(UI_Location__c = 'Compute Values',Name='AdoptVal',Matrix__c=matrix.id,Measure_Master__c=mm.id,Step_Type__c='Matrix',Modelling_Type__c = 'Potential',Compute_Master__c=cm1.id);
	    potStep.Sequence__c=1;
	    insert potStep;
	    
	    Step__c adoptStep = new Step__c(UI_Location__c = 'Compute Values',Name='PotenVal',Matrix__c=matrix.id,Measure_Master__c=mm.id,Step_Type__c='Matrix',Modelling_Type__c = 'Adoption',Compute_Master__c=cm2.id);
	    adoptStep.Sequence__c=2;
	    insert adoptStep;
	    
	    Step__c segmentStep = new Step__c(UI_Location__c = 'Compute Segment',Name='Segment',Matrix__c=matrix.id,Measure_Master__c=mm.id,Step_Type__c='Matrix',Grid_Param_1__c=rp1.id,Grid_Param_2__c=rp2.id);
	    segmentStep.Sequence__c=3;
	    insert segmentStep; 
	    
	    Step__c tcfStep = new Step__c(UI_Location__c = 'Compute TCF',Name='TCFCall',Matrix__c=matrix.id,Measure_Master__c=mm.id,Step_Type__c='Matrix',Grid_Param_1__c=rp1.id,Grid_Param_2__c=rp2.id);
	    tcfStep.Sequence__c=4;
	    insert tcfStep;
	    
	    Account_Compute_Final__c acf = new Account_Compute_Final__c(Measure_Master__c=mm.id,Output_Name_1__c='Dim1');
	    insert acf;
	    
	    /*Account_Compute_Final_Copy__c acfc = new Account_Compute_Final_Copy__c(Measure_Master__c=mm.id,Output_Name_1__c='Dim1');
	    insert acfc;*/ 
	    
	    Segment_Simulation__c ssc = new Segment_Simulation__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim1 Value',Dimension2__c='Dim2 Value',
	    	Measure_Master__c=mm.id,Name='ss',Segment__c='A',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
	    	Sum_Proposed_TCF__c=20,Sum_TCF__c=20);
	    
	    Segment_Simulation__c ssc2 = new Segment_Simulation__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim11 Value',Dimension2__c='Dim21 Value',
	    	Measure_Master__c=mm.id,Name='ss',Segment__c='B',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
	    	Sum_Proposed_TCF__c=20,Sum_TCF__c=20);
	    
	    Segment_Simulation__c ssc3 = new Segment_Simulation__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim111 Value',Dimension2__c='Dim211 Value',
	    	Measure_Master__c=mm.id,Name='ss',Segment__c='C',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
	    	Sum_Proposed_TCF__c=20,Sum_TCF__c=20);
	    
	    Segment_Simulation__c ssc4 = new Segment_Simulation__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim122 Value',Dimension2__c='Dim222 Value',
	    	Measure_Master__c=mm.id,Name='ss',Segment__c='D',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
	    	Sum_Proposed_TCF__c=20,Sum_TCF__c=20);
	    insert ssc;
	    insert ssc2;
	    insert ssc3;
	    insert ssc4;
	    
	    Segment_Simulation_Copy__c sscc = new Segment_Simulation_Copy__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim1 Value',Dimension2__c='Dim2 Value',
	    	Measure_Master__c=mm.id,Name='ss',Segment__c='A',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
	    	Sum_Proposed_TCF__c=20,Sum_TCF__c=20);
	    
	    Segment_Simulation_Copy__c sscc2 = new Segment_Simulation_Copy__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim11 Value',Dimension2__c='Dim21 Value',
	    	Measure_Master__c=mm.id,Name='ss',Segment__c='B',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
	    	Sum_Proposed_TCF__c=20,Sum_TCF__c=20);
	    
	    Segment_Simulation_Copy__c sscc3 = new Segment_Simulation_Copy__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim111 Value',Dimension2__c='Dim211 Value',
	    	Measure_Master__c=mm.id,Name='ss',Segment__c='C',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
	    	Sum_Proposed_TCF__c=20,Sum_TCF__c=20);
	    
	    Segment_Simulation_Copy__c sscc4 = new Segment_Simulation_Copy__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim122 Value',Dimension2__c='Dim222 Value',
	    	Measure_Master__c=mm.id,Name='ss',Segment__c='D',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
	    	Sum_Proposed_TCF__c=20,Sum_TCF__c=20);
	    insert sscc;
	    insert sscc2;
	    insert sscc3;
	    insert sscc4;
	    
	    Modelling_Simulation__c ms1 = new Modelling_Simulation__c(HCP_Count__c = 5,Value__c='45',Measure_Master__c=mm.id,Parameter_Name__c='Test Step 1');
	    insert ms1;
	    
	    Modelling_Simulation__c ms11 = new Modelling_Simulation__c(HCP_Count__c = 15,Value__c='115',Measure_Master__c=mm.id,Parameter_Name__c='Test Step 1');
	    insert ms11;
	    
	    Modelling_Simulation__c ms2 = new Modelling_Simulation__c(HCP_Count__c = 9,Value__c='72',Measure_Master__c=mm.id,Parameter_Name__c='Test Step 2');
	    insert ms2;
	    
	    Modelling_Simulation__c ms21 = new Modelling_Simulation__c(HCP_Count__c = 19,Value__c='25',Measure_Master__c=mm.id,Parameter_Name__c='Test Step 2');
	    insert ms21;
	    /*Test Data ends here*/
	    
	    System.test.startTest();
	    System.runAs(loggedInUser){
	    	ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
	    	String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
	    	List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
	    	System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
	    	BatchDeleteRecsBeforereExecute batchExecute = new BatchDeleteRecsBeforereExecute(mm.ID,'');
	    	Database.executeBatch(batchExecute, 2000);
	    }
	    System.test.stopTest();
	}
	private static testMethod void secondTest() 
	{
		/*Create initial test data for all the objs reqd.*/
		User loggedInUser = new User(id=UserInfo.getUserId());

		AxtriaSalesIQTM__Organization_Master__c aom = new AxtriaSalesIQTM__Organization_Master__c(AxtriaSalesIQTM__Org_Level__c='Global',AxtriaSalesIQTM__Parent_Country_Level__c=true);
		insert aom;
		
		AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='Italy',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active');
		insert country;
		
		
		Account acc = new Account(Name='test acc',Marketing_Code__c='EU');
		insert acc;
		
		Account acc2 = new Account(Name='test acc2',Marketing_Code__c='EU');
		insert acc2;
		
		AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='Test Team',AxtriaSalesIQTM__Country__c=country.id);
		insert team;
		
		AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU',AxtriaSalesIQTM__Team__c=team.id);
		insert ti;
		
		AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c(Name='Pos',AxtriaSalesIQTM__Team_iD__c=team.id);
		insert pos;
		
		AxtriaSalesIQTM__Position__c pos2 = new AxtriaSalesIQTM__Position__c(Name='Pos2',AxtriaSalesIQTM__Team_iD__c=team.id);
		insert pos2;

		Product_Catalog__c pc = new Product_Catalog__c(Name='Test Product',Team_Instance__c=ti.id,Veeva_External_ID__c = 'ProdId',Product_Code__c= 'ProdId',IsActive__c=true,Country_Lookup__c=country.id);
		insert pc;
		
	    /*Brand_Team_Instance__c bti = [select id from Brand_Team_Instance__c where Brand__c=:pc.id and Team_Instance__c=:ti.id];
	    */
	    Measure_Master__c mm = new Measure_Master__c(Team_Instance__c=ti.id,Brand_Lookup__c=pc.id,State__c='Executed');
	    mm.Team__c=team.id;
	    insert mm;
	    
	    AxtriaSalesIQTM__Position_Account__c pa = new AxtriaSalesIQTM__Position_Account__c(AxtriaSalesIQTM__Team_Instance__c=ti.id,AxtriaSalesIQTM__Account__c=acc.id,AxtriaSalesIQTM__Position__c=pos.id);
	    insert pa;
	    
	    AxtriaSalesIQTM__Position_Account__c pa2 = new AxtriaSalesIQTM__Position_Account__c(AxtriaSalesIQTM__Team_Instance__c=ti.id,AxtriaSalesIQTM__Account__c=acc.id,AxtriaSalesIQTM__Position__c=pos2.id);
	    insert pa2;
	    
	    AxtriaSalesIQTM__Position_Account__c pa3 = new AxtriaSalesIQTM__Position_Account__c(AxtriaSalesIQTM__Team_Instance__c=ti.id,AxtriaSalesIQTM__Account__c=acc2.id,AxtriaSalesIQTM__Position__c=pos.id);
	    insert pa3;
	    
	    AxtriaSalesIQTM__Position_Account__c pa4 = new AxtriaSalesIQTM__Position_Account__c(AxtriaSalesIQTM__Team_Instance__c=ti.id,AxtriaSalesIQTM__Account__c=acc2.id,AxtriaSalesIQTM__Position__c=pos2.id);
	    insert pa4;
	    
	    //inserting BU Response data
	    BU_Response__c bur = new BU_Response__c(Team_Instance__c=ti.id,Position_Account__c=pa.id,Product__c=pc.id);
	    bur.Physician__c = acc.id;
	    
	    bur.Response1__c = '10';
	    bur.Response2__c = '12';
	    insert bur;
	    
	    BU_Response__c bur2 = new BU_Response__c(Team_Instance__c=ti.id,Position_Account__c=pa2.id,Product__c=pc.id);
	    bur2.Physician__c = acc.id;
	    
	    bur2.Response1__c = '17';
	    bur2.Response2__c = '12';
	    insert bur2;
	    
	    BU_Response__c bur3 = new BU_Response__c(Team_Instance__c=ti.id,Position_Account__c=pa3.id,Product__c=pc.id);
	    bur3.Physician__c = acc2.id;
	    
	    bur3.Response1__c = '20';
	    bur3.Response2__c = '127';
	    insert bur3;
	    
	    BU_Response__c bur4 = new BU_Response__c(Team_Instance__c=ti.id,Position_Account__c=pa4.id,Product__c=pc.id);
	    bur4.Physician__c = acc2.id;
	    
	    bur4.Response1__c = '96';
	    bur4.Response2__c = '112';
	    insert bur4;

        //inserting MetaData_Definition__c data
	    MetaData_Definition__c md = new MetaData_Definition__c();
	    md.Source_Field__c = 'Response1__c';
	    md.Display_Name__c = 'H2-Potenziale BYDUREON';
	    md.Type__c = 'Text';
	    md.Sequence__c = 1;
	    md.Product_Catalog__c = pc.id;
	    md.Source_Object__c = 'BU_Response__c';
	    md.Team_Instance__c = ti.id;
	    insert md;
	    
	    MetaData_Definition__c md2 = new MetaData_Definition__c();
	    md2.Source_Field__c = 'Response2__c';
	    md2.Display_Name__c = 'H2-CL BYDUREON';
	    md2.Type__c = 'Text';
	    md2.Sequence__c = 2;
	    md2.Product_Catalog__c = pc.id;
	    md2.Source_Object__c = 'BU_Response__c';
	    md2.Team_Instance__c = ti.id;
	    insert md2;
	    
	    //push Rule Parameter and its associated step
	    Step__c testStep = new Step__c(Name='H2-Potenziale BYDUREON');
	    insert testStep;
	    
	    Step__c testStep2 = new Step__c(Name='H2-CL BYDUREON');
	    insert testStep2;
	    
	    Rule_Parameter__c rp1 = new Rule_Parameter__c(Step__c=testStep.id,Measure_Master__c=mm.id);
	    insert rp1;
	    
	    Rule_Parameter__c rp2 = new Rule_Parameter__c(Step__c=testStep2.id,Measure_Master__c=mm.id);
	    insert rp2;
	    
	    //insert grid
	    Grid_Master__c matrix = new Grid_Master__c(Name='Test Matrix',Grid_Type__c='2D',Dimension_1_Name__c='Dim1',Dimension_2_Name__c='Dim2',Country__c=country.id);
	    insert matrix;
	    
	    //insert step data for all 3 values
	    Compute_Master__c cm1 = new Compute_Master__c();
	    cm1.Expression__c = '{"elif":[{"andOr":[],"input":"H2-Potenziale BYDUREON","condition":"less than","match":"70","returVal":"Average"},'+
	    '{"andOr":[],"input":"H2-Potenziale BYDUREON","condition":"less than or equal to","match":"90","returVal":"Medium"}],'+
	    '"ifCase":{"andOr":[],"input":"H2-Potenziale BYDUREON","condition":"less than","match":"50","returVal":"Low"},'+
	    '"elseCase":{"andOr":[],"returVal":"High"}}';
	    insert cm1;
	    
	    Compute_Master__c cm2 = new Compute_Master__c();
	    cm2.Expression__c = '{"elif":[{"andOr":[],"input":"H2-CL BYDUREON","condition":"greater than or equal to","match":"70","returVal":"Medium"},'+
	    '{"andOr":[],"input":"H2-CL BYDUREON","condition":"greater than","match":"50","returVal":"Average"}],'+
	    '"ifCase":{"andOr":[],"input":"H2-CL BYDUREON","condition":"greater than","match":"90","returVal":"High"},'+
	    '"elseCase":{"andOr":[],"returVal":"Low"}}';
	    insert cm2;
	    Step__c potStep = new Step__c(UI_Location__c = 'Compute Values',Name='AdoptVal',Matrix__c=matrix.id,Measure_Master__c=mm.id,Step_Type__c='Matrix',Modelling_Type__c = 'Potential',Compute_Master__c=cm1.id);
	    potStep.Sequence__c=1;
	    insert potStep;
	    
	    Step__c adoptStep = new Step__c(UI_Location__c = 'Compute Values',Name='PotenVal',Matrix__c=matrix.id,Measure_Master__c=mm.id,Step_Type__c='Matrix',Modelling_Type__c = 'Adoption',Compute_Master__c=cm2.id);
	    adoptStep.Sequence__c=2;
	    insert adoptStep;
	    
	    Step__c segmentStep = new Step__c(UI_Location__c = 'Compute Segment',Name='Segment',Matrix__c=matrix.id,Measure_Master__c=mm.id,Step_Type__c='Matrix',Grid_Param_1__c=rp1.id,Grid_Param_2__c=rp2.id);
	    segmentStep.Sequence__c=3;
	    insert segmentStep; 
	    
	    Step__c tcfStep = new Step__c(UI_Location__c = 'Compute TCF',Name='TCFCall',Matrix__c=matrix.id,Measure_Master__c=mm.id,Step_Type__c='Matrix',Grid_Param_1__c=rp1.id,Grid_Param_2__c=rp2.id);
	    tcfStep.Sequence__c=4;
	    insert tcfStep;
	    
	    Account_Compute_Final__c acf = new Account_Compute_Final__c(Measure_Master__c=mm.id,Output_Name_1__c='Dim1');
	    insert acf;
	    
	    /*Account_Compute_Final_Copy__c acfc = new Account_Compute_Final_Copy__c(Measure_Master__c=mm.id,Output_Name_1__c='Dim1');
	    insert acfc;*/ 
	    
	    Segment_Simulation__c ssc = new Segment_Simulation__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim1 Value',Dimension2__c='Dim2 Value',
	    	Measure_Master__c=mm.id,Name='ss',Segment__c='A',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
	    	Sum_Proposed_TCF__c=20,Sum_TCF__c=20);
	    
	    Segment_Simulation__c ssc2 = new Segment_Simulation__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim11 Value',Dimension2__c='Dim21 Value',
	    	Measure_Master__c=mm.id,Name='ss',Segment__c='B',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
	    	Sum_Proposed_TCF__c=20,Sum_TCF__c=20);
	    
	    Segment_Simulation__c ssc3 = new Segment_Simulation__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim111 Value',Dimension2__c='Dim211 Value',
	    	Measure_Master__c=mm.id,Name='ss',Segment__c='C',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
	    	Sum_Proposed_TCF__c=20,Sum_TCF__c=20);
	    
	    Segment_Simulation__c ssc4 = new Segment_Simulation__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim122 Value',Dimension2__c='Dim222 Value',
	    	Measure_Master__c=mm.id,Name='ss',Segment__c='D',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
	    	Sum_Proposed_TCF__c=20,Sum_TCF__c=20);
	    insert ssc;
	    insert ssc2;
	    insert ssc3;
	    insert ssc4;
	    
	    Segment_Simulation_Copy__c sscc = new Segment_Simulation_Copy__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim1 Value',Dimension2__c='Dim2 Value',
	    	Measure_Master__c=mm.id,Name='ss',Segment__c='A',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
	    	Sum_Proposed_TCF__c=20,Sum_TCF__c=20);
	    
	    Segment_Simulation_Copy__c sscc2 = new Segment_Simulation_Copy__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim11 Value',Dimension2__c='Dim21 Value',
	    	Measure_Master__c=mm.id,Name='ss',Segment__c='B',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
	    	Sum_Proposed_TCF__c=20,Sum_TCF__c=20);
	    
	    Segment_Simulation_Copy__c sscc3 = new Segment_Simulation_Copy__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim111 Value',Dimension2__c='Dim211 Value',
	    	Measure_Master__c=mm.id,Name='ss',Segment__c='C',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
	    	Sum_Proposed_TCF__c=20,Sum_TCF__c=20);
	    
	    Segment_Simulation_Copy__c sscc4 = new Segment_Simulation_Copy__c(Distinct_Recs__c=2,Count_of_Accounts__c=2, AdopPtSegDistinct__c=2, Dimension1__c='Dim122 Value',Dimension2__c='Dim222 Value',
	    	Measure_Master__c=mm.id,Name='ss',Segment__c='D',Adoption__c=10,Potential__c=20,Territory__c=pos.id,
	    	Sum_Proposed_TCF__c=20,Sum_TCF__c=20);
	    insert sscc;
	    insert sscc2;
	    insert sscc3;
	    insert sscc4;
	    
	    Modelling_Simulation__c ms1 = new Modelling_Simulation__c(HCP_Count__c = 5,Value__c='45',Measure_Master__c=mm.id,Parameter_Name__c='Test Step 1');
	    insert ms1;
	    
	    Modelling_Simulation__c ms11 = new Modelling_Simulation__c(HCP_Count__c = 15,Value__c='115',Measure_Master__c=mm.id,Parameter_Name__c='Test Step 1');
	    insert ms11;
	    
	    Modelling_Simulation__c ms2 = new Modelling_Simulation__c(HCP_Count__c = 9,Value__c='72',Measure_Master__c=mm.id,Parameter_Name__c='Test Step 2');
	    insert ms2;
	    
	    Modelling_Simulation__c ms21 = new Modelling_Simulation__c(HCP_Count__c = 19,Value__c='25',Measure_Master__c=mm.id,Parameter_Name__c='Test Step 2');
	    insert ms21;
	    /*Test Data ends here*/
	    Grid_Details_Copy__c g = new Grid_Details_Copy__c();
	    g.Grid_Master__c = matrix.id;
	    insert g;
	    System.test.startTest();
	    System.runAs(loggedInUser){
	    	ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
	    	String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
	    	List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
	    	System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
	    	BatchDeleteRecsBeforereExecuteGDC batchExecute = new BatchDeleteRecsBeforereExecuteGDC(mm.ID,'');
	    	batchExecute.query = 'select id, Grid_Master__c from Grid_Details_Copy__c';
	    	Database.executeBatch(batchExecute, 2000);
	    }
	    System.test.stopTest();
	}
}