@isTest
public class UpdateProfileParameterAccountsTest {
   static testMethod void testMethod1() {
       String classname = 'UpdateProfileParameterAccountsTest';
           AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
              SnTDMLSecurityUtil.insertRecords(orgmas,className);
            AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
            countr.Name ='USA';
            SnTDMLSecurityUtil.insertRecords(countr,className);
           Account a1= TestDataFactory.createAccount();
            a1.AxtriaSalesIQTM__Country__c = countr.id;
            a1.AccountNumber = 'BH10477999';
            a1.Type = 'HCO';
            a1.Status__c ='Inactive';
           SnTDMLSecurityUtil.insertRecords(a1,className);
           List<Account> acclist = new List<Account>();
           acclist.add(a1);
         AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
        SnTDMLSecurityUtil.insertRecords(team,className);
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
       SnTDMLSecurityUtil.insertRecords(teamins1,className);
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        SnTDMLSecurityUtil.insertRecords(scen,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Name = 'test';
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        //teamins.AxtriaSalesIQTM__Team_Cycle_Name__c = 'Current';
        //teamins.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        SnTDMLSecurityUtil.insertRecords(teamins,className);
              Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        SnTDMLSecurityUtil.insertRecords(pcc,className);
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
       SnTDMLSecurityUtil.insertRecords(mmc,className);
          AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        SnTDMLSecurityUtil.insertRecords(pos,className);
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(a1,pos,teamins);
        SnTDMLSecurityUtil.insertRecords(posAccount,className);
        
        // -SR-357 changes by RT on 5thNov --
         posAccount.AxtriaSalesIQTM__Effective_Start_Date__c = Date.Today().addDays(1);
         posAccount.AxtriaSalesIQTM__Effective_End_Date__c = posAccount.AxtriaSalesIQTM__Effective_Start_Date__c ;
         SnTDMLSecurityUtil.updateRecords(posAccount,className);
         // -SR-357 ends here --
         Product_Priority__c pPriority = TestDataFactory.productPriority();
       SnTDMLSecurityUtil.insertRecords(pPriority,className);
        AxtriaSalesIQTM__Position_Product__c posprod = TestDataFactory.createPositionProduct(teamins, pos, pcc);
        posprod.Calls_Day__c = 1;
        // posprod.Effective_Days_in_Field_Formula__c = 1;
         SnTDMLSecurityUtil.insertRecords(posprod,className);
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,a1,teamins,posAccount,pPriority,pos);
        positionAccountCallPlan.Final_TCF__c = 10;
        positionAccountCallPlan.P1__c = pcc.name;
        positionAccountCallPlan.p1_original__c = pcc.name;
        positionAccountCallPlan.Final_TCF_Original__c = 10;
        positionAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c = true;
         SnTDMLSecurityUtil.insertRecords(positionAccountCallPlan,className);
         Test.startTest();
         ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'UpdateProfileParameterAccountsTest'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> FIELD_LIST = new List<String>{nameSpace+'Sequence__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Step__c.SObjectType, FIELD_LIST, false));
        
         UpdateProfileParameterAccounts.updateParameters(acclist);
         //obj.deletecallplan(acclist);
         //obj.updateposacct(acclist);
         Test.stopTest();
     }
}