/**********************************************************************************************
@author       : Himanshu Tariyal (A0994)
@modifiedDate : 17th June'2020
@description  : Batch class for updating Status of Temp obj recs
@Revison(s)   : v1.0
**********************************************************************************************/
global with sharing class BatchUpdateTempObjRecsCR implements Database.Batchable<sObject>,Database.Stateful
{
    public List<sObject> tempObjListUpdate;

    public Boolean tempDataLoaded;

    public String changeReqID;
    public String changeReqStatus;
    public String batchName;
    public String query;
    public String errorMessage;

    public Integer failedRecs = 0;
    public Integer successfulRecs = 0;
    public Set<string> teamins;
    public Date dt = system.today();
     
    public BatchUpdateTempObjRecsCR(String crID,Boolean tempLoadSuccess,String errMsg,String crStatus)
    {
        batchName = 'BatchUpdateTempObjRecsCR';
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : constructor invoked--');

        changeReqID = crID;
        tempDataLoaded = tempLoadSuccess;
        errorMessage = errMsg;
        changeReqStatus = crStatus;
        teamins = new Set<String>();
        SnTDMLSecurityUtil.printDebugMessage('changeReqID--'+changeReqID);
        SnTDMLSecurityUtil.printDebugMessage('tempDataLoaded--'+tempDataLoaded);
        SnTDMLSecurityUtil.printDebugMessage('errorMessage--'+errorMessage);
        SnTDMLSecurityUtil.printDebugMessage('changeReqStatus--'+changeReqStatus);
 
        if(changeReqID!=null && changeReqID!=''){
            query = 'select id,Status__c,SalesIQ_Error_Message__c,isError__c,Team_Instance_Name__c,Object_Name__c from temp_obj__c '+
                    'where Change_Request__c =:changeReqID WITH SECURITY_ENFORCED';
        }
        else
            query = '';
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : constructor invoked--');
        SnTDMLSecurityUtil.printDebugMessage('query--'+query);
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List<sObject> tempRecsList)
    {
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : execute() invoked--');
        SnTDMLSecurityUtil.printDebugMessage('tempRecsList size--'+tempRecsList.size());

        try
        {
            tempObjListUpdate = new List<sObject>();
            teamins = new Set<String>();
            String status;
            for(sObject obj : tempRecsList)
            {
                status = (String)obj.get('Status__c');
                if((String)obj.get('Object_name__c') == 'Position_Product')
                {
                    teamins.add((String)obj.get('Team_Instance_Name__c'));
                }
                //Temp data is not fully loaded, so mark fail for all the inserted recs
                if(!tempDataLoaded)
                {
                    obj.put('Status__c', 'Rejected');
                    obj.put('SalesIQ_Error_Message__c',errorMessage);
                    obj.put('isError__c', true);
                    tempObjListUpdate.add(obj);
                    failedRecs++;
                }
                else //As substitute for Direct_Load_Records_Status_Update class
                {
                    if(status!='Processed' && status!='Rejected')
                    {
                        obj.put('Status__c', 'Rejected');
                        obj.put('SalesIQ_Error_Message__c', 'Mandatory Field Missing or Incorrect Team Instance');
                        obj.put('isError__c', true);
                        tempObjListUpdate.add(obj);
                        failedRecs++;
                    }
                    else{
                        if(status=='Processed')
                            successfulRecs++;
                    }
                }
            }

            if(!tempRecsList.isEmpty()){
                SnTDMLSecurityUtil.updateRecords(tempRecsList,batchName);
            }
        }
        catch(Exception e)
        {
            SnTDMLSecurityUtil.printDebugMessage('Error in execute() method--'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack trace--'+e.getStackTraceString());
        }
    }
    
    public void finish(Database.BatchableContext BC)
    {
        List<String> cimlist = new List<String>();
        try
        {
            SnTDMLSecurityUtil.printDebugMessage(batchName+' : finish() invoked--');
            SnTDMLSecurityUtil.printDebugMessage('changeReqID--'+changeReqID);
            SnTDMLSecurityUtil.printDebugMessage('successfulRecs--'+successfulRecs);
            SnTDMLSecurityUtil.printDebugMessage('failedRecs--'+failedRecs);

            if(changeReqID!=null && changeReqID!='')
            {
                AxtriaSalesIQTM__Change_Request__c changeReq = new AxtriaSalesIQTM__Change_Request__c();
                changeReq.Records_Updated__c = successfulRecs;
                changeReq.Job_Status__c = changeReqStatus;
                changeReq.ID = changeReqID;
                SnTDMLSecurityUtil.updateRecords(changeReq,batchName);
            }
        }
        catch(Exception e)
        {
            SnTDMLSecurityUtil.printDebugMessage('Error in finish() method--'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack trace--'+e.getStackTraceString());
        }

        if(!teamins.isEmpty())
        {
        	List<AxtriaSalesIQTM__CIM_Config__c> ciminsertlist = [Select id,AxtriaSalesIQTM__Team_Instance__r.name,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__CIM_Config__c where isCallCapacity__c = true and AxtriaSalesIQTM__Team_Instance__r.name = :teamins and createddate >=:dt];
	        for(AxtriaSalesIQTM__CIM_Config__c cm: ciminsertlist)
	        {
	            cimlist.add(cm.id);
	        }
	        database.executeBatch(new BatchCIMPositionMatrixSummaryUpdate(cimlist, ciminsertlist[0].AxtriaSalesIQTM__Team_Instance__c),2000);
        }
    }
}