public with sharing class AsyncCalloutQuantile implements Queueable, Database.AllowsCallouts
{
    String ruleId;
    String decileStepId;
    Map<String, String> stepNameToOutputFieldMap;
    Map<String, String> ruleParamIdToStepNameMap;
    public AsyncCalloutQuantile(String ruleId, String decileStepId, Map<String, String> stepNameToOutputFieldMap, Map<String, String> ruleParamIdToStepNameMap)
    {

        try{
            this.ruleId = ruleId;
            this.decileStepId = decileStepId;
            this.stepNameToOutputFieldMap = stepNameToOutputFieldMap;
            this.ruleParamIdToStepNameMap = ruleParamIdToStepNameMap;
        }
        catch(Exception qe) {
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'AsyncCalloutQuantile',ruleId);
            } 
    }
    public void execute(QueueableContext context)
    {
        callFutureMethod();
    }

    public void callFutureMethod()
    {
        try{
        AxtriaSalesIQTM__ETL_Config__c etlConfig = [Select Id, AxtriaSalesIQTM__End_Point__c, AxtriaSalesIQTM__SF_UserName__c,
                                       AxtriaSalesIQTM__SF_Password__c, AxtriaSalesIQTM__S3_Security_Token__c,
                                       AxtriaSalesIQTM__S3_Bucket__c, AxtriaSalesIQTM__S3_Filename__c, AxtriaSalesIQTM__Org_Type__c
                                       from AxtriaSalesIQTM__ETL_Config__c where Name = 'Deciling' WITH SECURITY_ENFORCED];

        Step__c tempStep = [SELECT Id, Name, Step_Type__c, IsQuantileComputed__c, Compute_Master__r.Field_1__r.Parameter_Name__c, Compute_Master__r.Field_2_val__c,
                            Compute_Master__r.Decile_Field_API__c, Compute_Master__r.Decile_Field_Output_API__c, Compute_Master__r.Decile_Field_Step_Name_API__c,
                            Compute_Master__r.Expression__c FROM Step__c WHERE Measure_Master__c = : ruleId and Id = : decileStepId WITH SECURITY_ENFORCED];
        if(etlConfig !=  null && tempStep != null)
        {
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'BatchExecuteRuleEngine' WITH SECURITY_ENFORCED];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            String fileName = etlConfig.AxtriaSalesIQTM__S3_Filename__c;
            String fileNameOutput = etlConfig.AxtriaSalesIQTM__S3_Bucket__c;
            fileName = fileName.replace('stepName', tempStep.Id);

            fileName = fileName.replace('measureId', ruleId);

            fileNameOutput = fileNameOutput.replace('stepName', tempStep.Id);
            fileNameOutput = fileNameOutput.replace('measureId', ruleId);

            List<String> tempStringList = new List<String>();
            tempStringList = (tempStep.Compute_Master__r.Decile_Field_Output_API__c).split('__');
            String columnNameToPushDecile = nameSpace + tempStringList[0].toUpperCase() + '__c';

            tempStringList = new List<String>();
            tempStringList = (tempStep.Compute_Master__r.Decile_Field_Step_Name_API__c).split('__');
            String columnStepNameToPushDecile = nameSpace + tempStringList[0].toUpperCase() + '__c';

            String queryCols = '';
            Integer countMaxCol = Integer.valueOf(tempStringList[0].split('_')[2]);

            for(Integer i = 1; i < countMaxCol; i++)
            {
                queryCols += nameSpace + 'OUTPUT_VALUE_' + i + '__c ,';
            }
            queryCols = queryCols.removeEnd(',');

            tempStringList = new List<String>();
            tempStringList = (tempStep.Compute_Master__r.Decile_Field_API__c).split('__');

            String columnNameForDecile = nameSpace + tempStringList[0].toUpperCase() + '__c';

            String endPointURL = etlConfig.AxtriaSalesIQTM__End_Point__c ;

            Map<String,String> parameterMap = new Map<String,String>();
            parameterMap.put('fileName',fileName);

            //String postRequestBody = 'fileName=' + fileName;

            String exp = tempStep.Compute_Master__r.Expression__c;

            String condition = ' where ' + nameSpace + 'Measure_Master__c = \'' + ruleId + '\' and isDeleted = false';
            if(exp != null && exp != '')
            {
                //condition += ' and (';
                exp = createPythonReadableFilters(exp);

                List<String> ruleParamIdSet = new List<String>();
                ruleParamIdSet.addAll(ruleParamIdToStepNameMap.keySet());
                String tempKey;
                for(Integer i = 0, j = ruleParamIdSet.Size(); i < j; i++)
                {
                    tempKey = ruleParamIdToStepNameMap.get(ruleParamIdSet[i]);
                    if(stepNameToOutputFieldMap.containsKey(tempKey))
                    {
                        tempStringList = (stepNameToOutputFieldMap.get(tempKey)).split('__');
                        exp = exp.replaceAll(ruleParamIdSet[i], nameSpace + tempStringList[0].toUpperCase() + '__c');
                    }
                }
                //condition += ' ' + exp + ' )';
            }
            SnTDMLSecurityUtil.printDebugMessage('condition ----> ' + condition);

            parameterMap.put('fileNameOutput',fileNameOutput);
            parameterMap.put('noOfCuts',tempStep.Compute_Master__r.Field_2_val__c+'');
            parameterMap.put('columnNameForDecile',columnNameForDecile);
            parameterMap.put('columnNameToPushDecile',columnNameToPushDecile);
            parameterMap.put('columnStepNameToPushDecile',columnStepNameToPushDecile);
            parameterMap.put('stepName',tempStep.Name);
            parameterMap.put('stepId',tempStep.Id);
            parameterMap.put('sfdcUserName',etlConfig.AxtriaSalesIQTM__SF_UserName__c);
            parameterMap.put('sfdcPassword',etlConfig.AxtriaSalesIQTM__SF_Password__c);
            parameterMap.put('sfdcSecurityToken',etlConfig.AxtriaSalesIQTM__S3_Security_Token__c);
            parameterMap.put('orgType',etlConfig.AxtriaSalesIQTM__Org_Type__c);
            parameterMap.put('measureMasterId',ruleId);
            parameterMap.put('objectName',nameSpace + 'Account_Compute_Final__c');
            parameterMap.put('condition',exp);
            parameterMap.put('whereClause',condition);
            parameterMap.put('queryCols',queryCols);
            parameterMap.put('nameSpace',nameSpace);
            parameterMap.put('segUniv','Survey Data Accounts');


            //postRequestBody += '&fileNameOutput=' + fileNameOutput + '&noOfCuts=' + tempStep.Compute_Master__r.Field_2_val__c + '&columnNameForDecile=' + columnNameForDecile;
            //postRequestBody += '&columnNameToPushDecile=' + columnNameToPushDecile + '&columnStepNameToPushDecile=' + columnStepNameToPushDecile;
            //postRequestBody += '&stepName=' + tempStep.Name + '&stepId=' + tempStep.Id + '&sfdcUserName=' + etlConfig.AxtriaSalesIQTM__SF_UserName__c + '&sfdcPassword=' + etlConfig.AxtriaSalesIQTM__SF_Password__c;
            //postRequestBody += '&sfdcSecurityToken=' + etlConfig.AxtriaSalesIQTM__S3_Security_Token__c + '&orgType=' + etlConfig.AxtriaSalesIQTM__Org_Type__c + '&measureMasterId=' + ruleId + '&objectName=' + nameSpace + 'Account_Compute_Final__c&condition=' + exp;
            //postRequestBody += '&whereClause=' + condition + '&queryCols=' + queryCols + '&nameSpace=' + nameSpace + '&segUniv=Survey Data Accounts';
            SnTDMLSecurityUtil.printDebugMessage('endPointURL ==== ' + endPointURL);
            String postRequestBody = JSON.Serialize(parameterMap);
            SnTDMLSecurityUtil.printDebugMessage('postRequestBody ==== ' + postRequestBody);
            endPointURL = endPointURL.replaceAll( '\\s+', '%20');
            endPointURL = endPointURL.replaceAll('\'', '%27');
            Http h = new Http();
            HttpRequest request = new HttpRequest();
            HttpResponse response = new HttpResponse();
            String errorMessage = '';
            try
            {

                request.setEndPoint(endPointURL);
                request.setHeader('Content-type', 'application/json');
                request.setMethod('POST');
                request.setbody(postRequestBody);
                request.setTimeout(120000);
                response = h.send(request);
                SnTDMLSecurityUtil.printDebugMessage('response body ---> ' + response.getBody());
                if(response.getBody() != 'OK')
                {
                    errorMessage = response.getBody();
                }
            }
            catch(Exception e)
            {
                SnTDMLSecurityUtil.printDebugMessage('Error --> ' + e.getStackTraceString());
                SnTDMLSecurityUtil.printDebugMessage('getCause --> ' + e.getCause());
                SnTDMLSecurityUtil.printDebugMessage('getLineNumber --> ' + e.getLineNumber());
                SnTDMLSecurityUtil.printDebugMessage('getMessage --> ' + e.getMessage());
                SnTDMLSecurityUtil.printDebugMessage('getTypeName --> ' + e.getTypeName());
                    SalesIQSnTLogger.createUnHandledErrorLogs(e, SalesIQSnTLogger.BR_MODULE, 'AsyncCalloutQuantile',ruleId);
                if(e.getTypeName() == 'System.CalloutException')
                {
                    if(e.getMessage() != 'Read timed out')
                    {
                        errorMessage = e.getMessage();
                    }
                }


                else
                {
                    errorMessage = e.getStackTraceString();
                }

            }
            if(errorMessage.length() > 130000)
            {
                errorMessage = errorMessage.substring(0, 129998);
            }
            SnTDMLSecurityUtil.printDebugMessage('response code ---> ' + response.getStatusCode());
            if(String.isNotBlank(errorMessage))
            {
                    SnTDMLSecurityUtil.printDebugMessage('Inside if check --->'+errorMessage);
                ST_utility.updateRuleStatus( ruleId, SalesIQGlobalConstants.RULE_EXECUTION_FAILED, errorMessage, System.Label.Python_Service_Error, false);
            }


        }
        //SNT-516
        else
        {

            ST_utility.updateRuleStatus( ruleId, SalesIQGlobalConstants.RULE_EXECUTION_FAILED, System.Label.ETL_Config_Decile_Missing, '', false);
            }
        }
        catch(Exception qe) {
            //PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'AsyncCalloutQuantile',ruleId);
            } 

    }
    public String createPythonReadableFilters(String dummy)
    {

        try{
        dummy = dummy.replaceAll('\n', ' ');
        SnTDMLSecurityUtil.printDebugMessage('dummy ---> ' + dummy);

        List<String> listOfString = new List<String>();
        listOfString = dummy.split(' ');
        String temp = '';
        String finalString = '';
        List<String> finalListOfString = new List<String>();
        for(Integer i = 0, j = listOfString.Size(); i < j; i++)
        {
            if(listOfString[i].startsWith('\'') && listOfString[i] != '\'\'')
            {
                temp = listOfString[i].substring(1, listOfString[i].length() - 1);
                //SnTDMLSecurityUtil.printDebugMessage('s starts With quotes --> '+s);
                //SnTDMLSecurityUtil.printDebugMessage('text between quotes --> '+temp);
                if(isNumericString(temp))
                {
                    finalListOfString.add(temp.trim());
                }
                else if(isBooleanString(temp))
                {
                    if(Boolean.valueOf(temp))
                    {
                        finalListOfString.add('True');
                    }
                    else
                    {
                        finalListOfString.add('False');
                    }
                }
                else
                {
                    finalListOfString.add(listOfString[i].trim());
                }
            }
            else if(listOfString[i].contains('LIKE'))
            {
                Integer firstindex = listOfString[i].indexOf('%');
                Integer lastindex = listOfString[i].indexOf('%', firstindex + 1);
                temp = listOfString[i].substring(firstindex + 1, lastindex);
                temp = '.str.contains(\'' + temp.trim() + '\')';
                finalListOfString.add(temp);
            }
            else
            {
                finalListOfString.add(listOfString[i].trim());
            }
        }

        finalString = String.join(finalListOfString, ' ');
        finalString = finalString.replaceAll(' = ', ' == ');
        finalString = finalString.replaceAll(' .str.contains', '.str.contains');

        SnTDMLSecurityUtil.printDebugMessage('finalString ---> ' + finalString);
        return finalString;
        }
         catch(Exception qe) {
            //PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'AsyncCalloutQuantile',ruleId);
            return null;
            } 
    }
    public Boolean isBooleanString(String str)
    {
        try
        {
            Boolean d = Boolean.valueOf(str);
        }
        catch(Exception e)
        {
            SalesIQSnTLogger.createUnHandledErrorLogs(e, SalesIQSnTLogger.BR_MODULE, 'AsyncCalloutQuantile',ruleId);
            return false;
        }
        return true;
    }
    public Boolean isNumericString(String str)
    {
        try
        {
            Decimal d = Decimal.valueOf(str);
        }
        catch(Exception e)
        {
            SalesIQSnTLogger.createUnHandledErrorLogs(e, SalesIQSnTLogger.BR_MODULE, 'AsyncCalloutQuantile',ruleId);
            return false;
        }
        return true;
    }

}