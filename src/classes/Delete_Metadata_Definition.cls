global with sharing class Delete_Metadata_Definition implements Database.Batchable<sObject>
{
    public String query;
    public String teamInstance;
    public String type;
    public boolean scheduledjob = false;
    public Set<string> teaminstancelist;
    public list<String> teamInst_ProdList; //Added by dhiren
    public String teamInstance_Prod;//Added by dhiren
    public String product;//Added by dhiren
    public boolean ondemand = false; //Added by dhiren
    
    //Added by Chirag Ahuja for STIMPS-286
    public boolean HCOFlag = false;
    public String selectedSourceRule;
    public String selectedDestinationRule;

    //Added by HT(A0094) on 17th June 2020 for SNT-428
    public String changeReqID;
    public Boolean proceedBatch = true;

    //Added by HT(A0094) on 17th June 2020 for SNT-428
    global Delete_Metadata_Definition(String teamInstance, String type,String crID)
    {
        this.type = type;
        scheduledjob = false;
        this.teamInstance = teamInstance;

        //Added by HT(A0094) on 17th June 2020 for SNT-428
        changeReqID = crID;
        SnTDMLSecurityUtil.printDebugMessage('teamInstance--'+teamInstance);
        SnTDMLSecurityUtil.printDebugMessage('type--'+type);
        SnTDMLSecurityUtil.printDebugMessage('changeReqID--'+changeReqID);

        query = 'SELECT Id FROM MetaData_Definition__c WHERE Team_Instance__r.Name = \''+
                teamInstance +'\' WITH SECURITY_ENFORCED';
    }

    global Delete_Metadata_Definition(String teamInstance, String type)
    {
        this.type = type;
        scheduledjob = false;
        this.teamInstance = teamInstance;
        query = 'SELECT Id FROM MetaData_Definition__c WHERE Team_Instance__r.Name = \'' + teamInstance + '\' WITH SECURITY_ENFORCED';
    }

    global Delete_Metadata_Definition()
    {
        scheduledjob = true;
        SnTDMLSecurityUtil.printDebugMessage('+++empty construct');
        teaminstancelist = new Set<String>();
        teaminstancelist = StaticTeaminstanceList.getCompleteRuleTeamInstances();
        SnTDMLSecurityUtil.printDebugMessage('+++teaminstancelist' + teaminstancelist);
        query = 'SELECT Id FROM MetaData_Definition__c  WHERE Team_Instance__c not in:teaminstancelist WITH SECURITY_ENFORCED';
    }

    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Added  by dhiren<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    global Delete_Metadata_Definition(String teamInstanceProd)
    {
        SnTDMLSecurityUtil.printDebugMessage('++Inside whole survey response');

        teamInstance_Prod = teamInstanceProd;
        SnTDMLSecurityUtil.printDebugMessage('<><><><><><><><>' + teamInstance_Prod);
        teamInst_ProdList = new list<String>();
        if(teamInstance_Prod.contains(';'))
        {
            teamInst_ProdList = teamInstance_Prod.split(';');
        }

        SnTDMLSecurityUtil.printDebugMessage('<><>teamInst_ProdList<><<>' + teamInst_ProdList);
        teamInstance = teamInst_ProdList[0];
        product = teamInst_ProdList[1];
        scheduledJob = false;
        ondemand = true;

        SnTDMLSecurityUtil.printDebugMessage('<>>>>>>><>teamInstance<><><><><><><> ' + teamInstance + '  <>>>>>>>>product>>>>>>>>>> ' + product);

        /*query='SELECT Id FROM MetaData_Definition__c  WHERE Team_Instance__r.name =: teamInstance AND Brand_Team_Instance__r.Brand__r.Veeva_External_ID__c =: product ';*/
        query = 'SELECT Id FROM MetaData_Definition__c  WHERE Team_Instance__r.Name =: teamInstance AND Product_Catalog__r.Veeva_External_ID__c =: product WITH SECURITY_ENFORCED';

    }
    //<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>End of code by dhiren<><><>>>>>>><><><><><><><><><><><>><><><<

    //Added by Chirag Ahuja for STIMPS-286. Related to HCO Segmentation
    global Delete_Metadata_Definition(String sourceRuleId, String destinationRuleId, String teamInstance, String prod, String type, Boolean flag)
    {
        this.selectedSourceRule = sourceRuleId;
        this.selectedDestinationRule = destinationRuleId;
        this.type = type;
        scheduledjob = false;
        this.teamInstance = teamInstance;
        this.product = prod;
        this.HCOFlag = flag;
        query = 'SELECT Id FROM MetaData_Definition__c WHERE Team_Instance__r.Name = \'' + teamInstance + '\' and Parameter_Type__c = \'' + type + '\' and Product_Catalog__r.Veeva_External_ID__c = \'' + product + '\' WITH SECURITY_ENFORCED';
    }

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        SnTDMLSecurityUtil.printDebugMessage('query-->' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<MetaData_Definition__c> scope)
    {
        SnTDMLSecurityUtil.printDebugMessage('scope size-->' + scope.size());
        if(scope.size() > 0 && scope != null)
        {
            if(MetaData_Definition__c.sObjectType.getDescribe().isDeletable())
            {
                Database.DeleteResult[] srList = Database.delete(scope, false);
            }
            else
            {
                proceedBatch = false;
                SnTDMLSecurityUtil.printDebugMessage('You dont have permission to delete MetaData_Definition__c', 'Delete_Metadata_Definition');
            }
        }
    }

    global void finish(Database.BatchableContext BC)
    {   //Commented by Chirag Ahuja for STIMPS-286
        // if(scheduledjob)
        // {
        //     Delete_Staging_BU_Response batch = new Delete_Staging_BU_Response();
        //     database.executeBatch(batch, 500);
        // }
        // else
        // {
        //     if(ondemand)
        //     {
        //         Delete_Staging_BU_Response batch = new Delete_Staging_BU_Response(teamInstance_Prod);
        //         Database.executeBatch(batch, 500);
        //     }

        //     else
        //     {
        //         //Modified by HT(A0994) on 17th June 2020 for SNT-428
        //         if(changeReqID!=null && changeReqID!='')
        //         {
        //             if(proceedBatch)
        //             {
        //                 Delete_Staging_BU_Response batch = new Delete_Staging_BU_Response(teamInstance,type,changeReqID);
        //                 Database.executeBatch(batch, 500);
        //             }
        //             else{
        //                 SalesIQUtility.updateCRStatusDirectLoad(changeReqID,'Error',0);
        //             }
        //         }
        //         else
        //         {
        //             Delete_Staging_BU_Response batch = new Delete_Staging_BU_Response(teamInstance,type);
        //             Database.executeBatch(batch, 500);
        //         }
        //     }
        // }

        //Added by Chirag Ahuja for STIMPS-286
        if(scheduledjob){
            Delete_Staging_Cust_Survey_Profile batch = new Delete_Staging_Cust_Survey_Profile();
            database.executeBatch(batch, 500);
        }
        else{
            if(ondemand){
                Delete_Staging_Cust_Survey_Profile batch = new Delete_Staging_Cust_Survey_Profile(teamInstance_Prod);
                Database.executeBatch(batch, 500);
            }
            else{//Modified by HT(A0994) on 17th June 2020 for SNT-428
                if(changeReqID!=null && changeReqID!=''){
                    if(proceedBatch){
                        Delete_Staging_Cust_Survey_Profile batch = new Delete_Staging_Cust_Survey_Profile(teamInstance,type,changeReqID);
                        Database.executeBatch(batch, 500);
                    }
                    else{
                        SalesIQUtility.updateCRStatusDirectLoad(changeReqID,'Error',0);
                    }
                }
                else{
                    //Added by Chirag Ahuja for STIMPS-286. Related to HCO Segmentation
                    if(HCOFlag){
                        Delete_Staging_Cust_Survey_Profile batch = new Delete_Staging_Cust_Survey_Profile(selectedSourceRule,selectedDestinationRule,teamInstance,product,type,HCOFlag);
                        Database.executeBatch(batch, 500);
                    }
                    else{
                        Delete_Staging_Cust_Survey_Profile batch = new Delete_Staging_Cust_Survey_Profile(teamInstance,type);
                        Database.executeBatch(batch, 500);
                    }
                }
            }
        }
    }
}