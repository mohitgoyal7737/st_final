global class updateAffiliationAcc implements Database.Batchable<sObject>, Database.Stateful {
    public String query;
    public list<Account> loosingList = new list<Account>();

    public Set<String> setHcaAccNum = new Set<String>();
    public Set<String> setHcpAccNum = new Set<String>();
    public Set<String> setScenarioID = new Set<String>();
    public Set<String> setBUaccnum = new Set<String>();
    public Set<String> setTDaccnum = new Set<String>();
    public Set<String> setBuHcp = new Set<String>();
    public Set<String> setTdHca = new Set<String>();
    public Set<String> setaccID = new Set<String>();
    public Set<String> inactiveALooserAffSet = new Set<String>();
    public Set<String> loosingset = new Set<String>();
    //public Set<String> lossingaccounts = new Set<String>();
    //List<String> marketCode = new List<String>();
    
    //public Integer hcpProcessed = 0;
    //public Integer hcaProcessed = 0;
    public String hcpQuery;
    public String hcaQuery;
        
    public Map<String,String> mapPosAcc2Scenario = new Map<String,String>();
    public Map<String,String> mapScenario2BusRule = new Map<String,String>();
    
    global updateAffiliationAcc(list<Account> inactivelist) {
        query = '';
        loosingList = inactivelist;

    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        query = 'SELECT id,AccountNumber,Type FROM account where AccountNumber != null and id IN :loosingList';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Account> scope) {

        List<AxtriaSalesIQTM__Account_Affiliation__c> affAccList = new List<AxtriaSalesIQTM__Account_Affiliation__c>();
        List<AxtriaSalesIQTM__Position_Account__c> posAccList = new List<AxtriaSalesIQTM__Position_Account__c>();
        List <AxtriaSalesIQTM__Account_Address__c> addList = new List <AxtriaSalesIQTM__Account_Address__c>(); 
        
        List<AxtriaSalesIQTM__Account_Affiliation__c> affList = new List<AxtriaSalesIQTM__Account_Affiliation__c>();
        List<Account> accountList = new List<Account>();
        List<AxtriaSalesIQTM__Position_Account__c> posList = new List<AxtriaSalesIQTM__Position_Account__c>();
        List<AxtriaSalesIQTM__Account_Affiliation__c> inactiveAccAffListHCP = new List<AxtriaSalesIQTM__Account_Affiliation__c>();
        List<AxtriaSalesIQTM__Account_Affiliation__c> inactiveAccAffListHCA = new List<AxtriaSalesIQTM__Account_Affiliation__c>();

        for(Account acc : scope){
            loosingset.add(acc.AccountNumber);
         }

         System.debug('==Loosing Set::::'+loosingset);

        System.debug('========Inactive Account HCA/HCP============');
        for(Account acc : scope){
            if(acc.Type == 'HCP'){
                setHcpAccNum.add(acc.AccountNumber);
            }
            if(acc.Type == 'HCA'){
                setHcaAccNum.add(acc.AccountNumber);
            }
        }
        System.debug('======setHcpAccNum : ' +setHcpAccNum);
        System.debug('======setHcaAccNum : ' +setHcaAccNum);        

        System.debug('=======Inactive Position Account List========');
        List<AxtriaSalesIQTM__Position_Account__c> inactivePosAccList = [SELECT id, AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__c FROM AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Account__c IN:loosingList and (AxtriaSalesIQTM__Assignment_Status__c = 'Active' or AxtriaSalesIQTM__Assignment_Status__c = 'Future Active') and AxtriaSalesIQTM__Account_Alignment_Type__c!=null and AxtriaSalesIQTM__Position__c!=null and AxtriaSalesIQTM__Account__c!=null];
        System.debug('=====Debug check for Inactive PA====' +inactivePosAccList);  

        System.debug('=======Inactive Position Account Business Rule========');
        for(AxtriaSalesIQTM__Position_Account__c tempPosAcc : inactivePosAccList){
            setScenarioID.add(tempPosAcc.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__c);
            mapPosAcc2Scenario.put(tempPosAcc.id,tempPosAcc.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__c);
        }

        System.debug('======mapPosAcc2Scenario:::: ' +mapPosAcc2Scenario);

        List<AxtriaSalesIQTM__Business_Rules__c> inactiveBussRuleList = [SELECT id, AxtriaSalesIQTM__Rule_Type__c,AxtriaSalesIQTM__Scenario__c FROM AxtriaSalesIQTM__Business_Rules__c where AxtriaSalesIQTM__Scenario__c in :setScenarioID];

         for(AxtriaSalesIQTM__Business_Rules__c busRule : inactiveBussRuleList){
            mapScenario2BusRule.put(busRule.AxtriaSalesIQTM__Scenario__c,busRule.AxtriaSalesIQTM__Rule_Type__c);
        }
        System.debug('======mapScenario2BusRule : ' +mapScenario2BusRule);

        System.debug('========Inactive Position Account Rule type============');
        for(AxtriaSalesIQTM__Position_Account__c posAcc : inactivePosAccList){
            String scenario = mapPosAcc2Scenario.get(posAcc.id);
            String ruleType = mapScenario2BusRule.get(scenario);
            if(ruleType == 'Bottom Up'){
                setBUaccnum.add(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber);
            }
            if(ruleType == 'Top Down'){
                setTDaccnum.add(posAcc.AxtriaSalesIQTM__Account__r.AccountNumber);
            }
        }
        System.debug('========Inactive Bottom UP Account Number======= ' +setBUaccnum);
        System.debug('========Inactive Top Down Account Number======= ' +setTDaccnum);

        System.debug('==========Inactive Affiliation Handling===============');
        String hcpQuery = 'SELECT id, AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Account__r.Status__c,Account_Number__c,Parent_Account_Number__c,AxtriaSalesIQTM__Affiliation_Network__c,Country__c,IsDeleted,AxtriaSalesIQTM__Parent_Account__c,AxtriaSalesIQTM__Root_Account__c FROM   AxtriaSalesIQTM__Account_Affiliation__c where Affiliation_Status__c=\'Active\' and Account_Number__c in :setBUaccnum';
        
        String hcaQuery = 'SELECT id, AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Account__r.Status__c,Account_Number__c,Parent_Account_Number__c,AxtriaSalesIQTM__Affiliation_Network__c,Country__c,IsDeleted,AxtriaSalesIQTM__Parent_Account__c,AxtriaSalesIQTM__Root_Account__c FROM   AxtriaSalesIQTM__Account_Affiliation__c where Affiliation_Status__c=\'Active\' and Parent_Account_Number__c in :setTDaccnum and AxtriaSalesIQTM__Is_Primary__c=true and AxtriaSalesIQTM__Active__c=true';

        //List<AxtriaSalesIQTM__Account_Affiliation__c> inactiveHCPAccAffList =  Database.query(query);
        //List<AxtriaSalesIQTM__Account_Affiliation__c> inactiveHCAAccAffList =  Database.query(query1);
        
       // System.debug('=====inactiveHCPAccAffList ::::::::' +inactiveHCPAccAffList);
        //System.debug('=====inactiveHCAAccAffList ::::::::' +inactiveHCAAccAffList );
        if(hcpQuery != null)
          inactiveAccAffListHCP =  Database.query(hcpQuery);
        
        if(hcaQuery != null)
            inactiveAccAffListHCA =  Database.query(hcaQuery);

        System.debug('=====HCP inactive and HCA Affiliation=====' +inactiveAccAffListHCP);
        System.debug('=====HCA inactive and HCP Affiliation=====' +inactiveAccAffListHCA);
        
        for(AxtriaSalesIQTM__Account_Affiliation__c losAff : inactiveAccAffListHCP){
            if(setBUaccnum.contains(losAff.Account_Number__c)){
                if(setHcpAccNum.contains(losAff.Account_Number__c)) {
                    setBuHcp.add(losAff.Account_Number__c);
                    //hcpProcessed+= 1;
                }
            }
        }
        
        for(AxtriaSalesIQTM__Account_Affiliation__c losAff : inactiveAccAffListHCA){
            if(setTDaccnum.contains(losAff.Parent_Account_Number__c)){
                if(setHcaAccNum.contains(losAff.Parent_Account_Number__c)) {
                    setTdHca.add(losAff.Parent_Account_Number__c);
                    //hcaProcessed+= 1;
                }
            }
        }

        /*if(hcpProcessed > 0){
            hcpQuery = query + ' and Account_Number__c IN :setBuHcp';
        }
        if(hcaProcessed > 0){
            hcaQuery = query1 + ' and Parent_Account_Number__c IN :setTdHca';
        }*/

        system.debug('=========hcpQuery::'+hcpQuery);
        system.debug('=========hcaQuery::'+hcaQuery);

        system.debug('=========setBuHcp::'+setBuHcp);
        system.debug('=========setTdHca::'+setTdHca);
        
        if(hcpQuery != null)
          inactiveAccAffListHCP =  Database.query(hcpQuery);
        
        if(hcaQuery != null)
            inactiveAccAffListHCA =  Database.query(hcaQuery);

        System.debug('=====HCP inactive and HCA Affiliation=====' +inactiveAccAffListHCP);
        System.debug('=====HCA inactive and HCP Affiliation=====' +inactiveAccAffListHCA);
        Map<String,String> mapHCPlos2HCAaff = new Map<String,String>();
        Map<String,Set<String>> mapHCAlos2HCPaff = new Map<String,Set<String>>();

        System.debug('========Map for Affiliation Account and respective inactive Account===========');
        if(inactiveAccAffListHCP != null){
            for(AxtriaSalesIQTM__Account_Affiliation__c hcpLosAff : inactiveAccAffListHCP){
                mapHCPlos2HCAaff.put(hcpLosAff.Account_Number__c,hcpLosAff.Parent_Account_Number__c);
                inactiveALooserAffSet.add(hcpLosAff.id);
            }
        }

        if(inactiveAccAffListHCA != null){
            for(AxtriaSalesIQTM__Account_Affiliation__c hcaLosAff : inactiveAccAffListHCA){
                if(!mapHCAlos2HCPaff.containsKey(hcaLosAff.Parent_Account_Number__c)){
                    Set<String> hcp = new Set<String>();
                    hcp.add(hcaLosAff.Account_Number__c);
                   mapHCAlos2HCPaff.put(hcaLosAff.Parent_Account_Number__c,hcp);
                }
                else{
                    mapHCAlos2HCPaff.get(hcaLosAff.Parent_Account_Number__c).add(hcaLosAff.Account_Number__c);
                }
                inactiveALooserAffSet.add(hcaLosAff.id);
                //sethcp.add(hcaLosAff.Account_Number__c);
            }
        }

        System.debug('========Map for Affiliation Account and respective Looser Account mapHCPlos2HCAaff===========' +mapHCPlos2HCAaff);
        System.debug('========Map for Affiliation Account and respective Looser Account mapHCAlos2HCPaff===========' + mapHCAlos2HCPaff);

        System.debug('========Further affiliation check for inactive Account===========');
        Map<String,Set<String>> mapHCALooser2Pos = new Map<String,Set<String>>();
        Map<String,Set<String>> mapHCPLooser2Pos = new Map<String,Set<String>>();
        Map<String,Set<String>> mapAff2Pos = new Map<String,Set<String>>();
        Map<String,Set<String>> mapsecAff2Pos= new Map<String,Set<String>>();

        List<AxtriaSalesIQTM__Position_Account__c> looserPosAcc = [select Id, AxtriaSalesIQTM__Account__c, AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Account__r.Type,AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Position__c != null and AxtriaSalesIQTM__Account__c != null and (AxtriaSalesIQTM__Assignment_Status__c = 'Active' or AxtriaSalesIQTM__Assignment_Status__c = 'Future Active') and AxtriaSalesIQTM__Account__c in :loosingList];

        if(looserPosAcc != null){
            for(AxtriaSalesIQTM__Position_Account__c looserPosAccRec : looserPosAcc){
                if(looserPosAccRec.AxtriaSalesIQTM__Account__r.Type == 'HCP'){
                    if(!mapHCPLooser2Pos.containsKey(looserPosAccRec.AxtriaSalesIQTM__Account__r.AccountNumber)){
                        Set<String> temp = new Set<String>();
                        temp.add(looserPosAccRec.AxtriaSalesIQTM__Position__c);
                       mapHCPLooser2Pos.put(looserPosAccRec.AxtriaSalesIQTM__Account__r.AccountNumber,temp);
                   }
                   else{
                        //Set<String> tempPos=new set<string>();
                        //tempPos.addall(mapHCPLooser2Pos.get(looserPosAccRec.AxtriaSalesIQTM__Account__r.AccountNumber));
                        //tempPos.add(looserPosAccRec.AxtriaSalesIQTM__Position__c);
                        //mapHCPLooser2Pos.put(looserPosAccRec.AxtriaSalesIQTM__Account__r.AccountNumber,tempPos);
                         mapHCPLooser2Pos.get(looserPosAccRec.AxtriaSalesIQTM__Account__r.AccountNumber).add(looserPosAccRec.AxtriaSalesIQTM__Position__c);
                   }
                }
                if(looserPosAccRec.AxtriaSalesIQTM__Account__r.Type == 'HCA'){
                    if(!mapHCALooser2Pos.containsKey(looserPosAccRec.AxtriaSalesIQTM__Account__r.AccountNumber)){
                        Set<String> temp = new Set<String>();
                        temp.add(looserPosAccRec.AxtriaSalesIQTM__Position__c);
                        mapHCALooser2Pos.put(looserPosAccRec.AxtriaSalesIQTM__Account__r.AccountNumber,temp);
                   }
                   else{
                        /*Set<String> tempPos=mapHCALooser2Pos.get(looserPosAccRec.AxtriaSalesIQTM__Account__r.AccountNumber).add(looserPosAccRec.AxtriaSalesIQTM__Position__c);
                        mapHCALooser2Pos.put(looserPosAccRec.AxtriaSalesIQTM__Account__r.AccountNumber,tempPos);*/
                        mapHCALooser2Pos.get(looserPosAccRec.AxtriaSalesIQTM__Account__r.AccountNumber).add(looserPosAccRec.AxtriaSalesIQTM__Position__c);
                   }
                   //mapHCALooser2Pos.put(looserPosAccRec.AxtriaSalesIQTM__Account__r.AccountNumber,looserPosAccRec.AxtriaSalesIQTM__Position__c);
                }
            }
        }
        System.debug('========Affiliation Position===========');
        System.debug('========mapHCPLooser2Pos:::' +mapHCPLooser2Pos);
        System.debug('========mapHCALooser2Pos:::' +mapHCALooser2Pos);

        Set<String> setHCAFurtherAff = new Set<String>();
        Set<String> setHCPFurtherAff = new Set<String>();
        Set<String> inactiveAccSet = new Set<String>();
        Set<String> inactivePosSet = new Set<String>();
        
        Map<String,String> mapHCPAffAcc2Pos = new Map<String,String>();
        Map<String,String> mapHCAAffAcc2Pos = new Map<String,String>();

        List<AxtriaSalesIQTM__Position_Account__c> firstAffPosAcc = [select Id, AxtriaSalesIQTM__Account__c, AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Account__r.Type,AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Position__c != null and AxtriaSalesIQTM__Account__c != null and (AxtriaSalesIQTM__Assignment_Status__c = 'Active' or AxtriaSalesIQTM__Assignment_Status__c = 'Future Active') and AxtriaSalesIQTM__Account__r.AccountNumber in :mapHCPlos2HCAaff.values()];

        if(firstAffPosAcc != null){
            for(AxtriaSalesIQTM__Position_Account__c firstAffPosAccRec : firstAffPosAcc){
                if(!mapAff2Pos.containsKey(firstAffPosAccRec.AxtriaSalesIQTM__Account__r.AccountNumber)){
                        Set<String> temp = new Set<String>();
                        temp.add(firstAffPosAccRec.AxtriaSalesIQTM__Position__c);
                        mapAff2Pos.put(firstAffPosAccRec.AxtriaSalesIQTM__Account__r.AccountNumber,temp);
                   }
                   else{
                        mapAff2Pos.get(firstAffPosAccRec.AxtriaSalesIQTM__Account__r.AccountNumber).add(firstAffPosAccRec.AxtriaSalesIQTM__Position__c);
                   }
               }
        }
        System.debug('=====mapAff2Pos:::::::' +mapAff2Pos);
        Map<String,Set<String>> mapHCAaff2furtherPos = new Map<String,Set<String>>();

        for(String loosers : loosingset){
            if(mapHCPlos2HCAaff.containsKey(loosers)){
                System.debug('Looser is HCP');
                Set<String> hcpPos = mapHCPLooser2Pos.get(loosers);
                System.debug('====hcpPos::' +hcpPos);
                System.debug('Affiliation is HCA');
                String hcaAff = mapHCPlos2HCAaff.get(loosers);
                System.debug('====hcaAff::' +hcaAff);
                System.debug('Position set of Affiliation HCA');
                Set<String> hcaAffPos = mapAff2Pos.get(hcaAff);
                System.debug('====hcaAffPos::' +hcaAffPos);
                if(hcpPos != null){
                    for(String childPos : hcpPos){
                        if(hcaAffPos.contains(childPos) && hcaAffPos !=null){
                            System.debug('same Position further HCP--> HCA');
                            if(!mapHCAaff2furtherPos.containsKey(hcaAff)){
                                Set<String> frthrPoscheck = new Set<String>();
                                frthrPoscheck.add(childPos);
                                mapHCAaff2furtherPos.put(hcaAff,frthrPoscheck);
                            }
                            else{
                                mapHCAaff2furtherPos.get(hcaAff).add(childPos);
                            }
                            setHCAFurtherAff.add(hcaAff);
                            inactivePosSet.add(childPos);
                            inactiveAccSet.add(loosers);
                        }
                        else
                        {
                            System.debug('Position are not equal');
                            inactivePosSet.add(childPos);
                            inactiveAccSet.add(hcaAff);
                        }
                    }
                }
            }
        }

        System.debug('=====mapHCAaff2furtherPos====' +mapHCAaff2furtherPos);

        Set<String> setHCP = new Set<String>();
        Set<String> setPOS = new Set<String>();

        if(mapHCAlos2HCPaff != null){
            for(String s : mapHCAlos2HCPaff.keySet()){
                for(String hcp : mapHCAlos2HCPaff.get(s)){
                    setHCP.add(hcp);
                }
            }
        }

        if(mapHCALooser2Pos != null){
            for(String s : mapHCALooser2Pos.keySet()){
                for(String pos : mapHCALooser2Pos.get(s)){
                    setPOS.add(pos);
                }
            }
        }

        List<AxtriaSalesIQTM__Position_Account__c> firsthcaAffPosAcc = [select Id, AxtriaSalesIQTM__Account__c, AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Account__r.Type,AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Position__c != null and AxtriaSalesIQTM__Account__c != null and (AxtriaSalesIQTM__Assignment_Status__c = 'Active' or AxtriaSalesIQTM__Assignment_Status__c = 'Future Active') and AxtriaSalesIQTM__Account__r.AccountNumber IN :setHCP and AxtriaSalesIQTM__Position__c in :setPOS];

        if(firsthcaAffPosAcc != null){
            for(AxtriaSalesIQTM__Position_Account__c inactivePos : firsthcaAffPosAcc){
                
                    inactivePosSet.add(inactivePos.AxtriaSalesIQTM__Position__c);
                    inactiveAccSet.add(inactivePos.AxtriaSalesIQTM__Account__r.AccountNumber);
            }
        }
        
        
        
        System.debug('=======Inactive Position Account');
        System.debug('=====Inactive Looser Account set====' +inactiveAccSet);
        System.debug('=====Inactive Looser Position set====' +inactivePosSet);
        /*List<AxtriaSalesIQTM__Account_Affiliation__c> inactiveAffList = [select id from AxtriaSalesIQTM__Account_Affiliation__c where Account_Number__c in :inactiveAccSet];
        

        List<Account> accountList = new List<Account>();
            
        if(inactiveAffList != null){
            for(AxtriaSalesIQTM__Account_Affiliation__c aff : inactiveAffList){
                //AxtriaSalesIQTM__Account_Affiliation__c aff = new AxtriaSalesIQTM__Account_Affiliation__c(id = temp1);
                //aff.AxtriaSalesIQTM__Account__r.Status__c = 'Inactive';
                aff.Affiliation_Status__c = 'Inactive';
                aff.AxtriaSalesIQTM__Active__c = false;
                aff.Affiliation_End_Date__c = Date.today().addDays(-1);
                affAccList.add(aff);
            }
            System.debug('====affAccList:::::' +affAccList);*/

            List<AxtriaSalesIQTM__Position_Account__c> inactPosAccList = [select id, AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c,AxtriaSalesIQTM__Effective_End_Date__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Position__c in :inactivePosSet and AxtriaSalesIQTM__Account__r.AccountNumber in :inactiveAccSet];
            
            
            if(inactPosAccList != null){
                for(AxtriaSalesIQTM__Position_Account__c pa : inactPosAccList){
                    if(pa.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=='Current')
                        pa.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addDays(-1);

                    if(pa.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=='Future')
                        pa.AxtriaSalesIQTM__Effective_End_Date__c=pa.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c.addDays(-1);
                    
                    posAccList.add(pa);
                }
            }

            System.debug('====posAccList:::::' +posAccList);
            System.debug('====posAccList.size():::::' +posAccList.size());

            if(posAccList.size() > 0)
                update posAccList;

            /*List<AxtriaSalesIQTM__Account_Address__c> inactiveAddList = [select id,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.AccountNumber,Status__c from AxtriaSalesIQTM__Account_Address__c where AxtriaSalesIQTM__Account__r.AccountNumber IN:inactiveAccSet ];                                                                                                                                                                                                                 
            for(AxtriaSalesIQTM__Account_Address__c add : inactiveAddList){
               add.Status__c = 'Inactive';
               addList.add(add);
            }
            System.debug('====addList:::::' +addList);

            List<Account> inactiveAccList = [select id from Account where AccountNumber in :inactiveAccSet];
            for(Account acc : inactiveAccList){
                //Account acc = new Account(id = temp);
                acc.Status__c = 'Inactive';
                accountList.add(acc);
            }
            System.debug('====accountList:::::' +accountList);
        }

        if(affAccList.size() > 0 && accountList.size() > 0 && posAccList.size() > 0 && addList.size() > 0){
            update affAccList;
            update posAccList;
            update addList;
            update accountList;         
        }*/
        System.debug('==========Further Affiliation Handling for HCA Affiliation===============');
        String affHCAQuery = 'SELECT AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Account__r.Status__c,Account_Number__c,Parent_Account_Number__c,AxtriaSalesIQTM__Affiliation_Network__c,AxtriaSalesIQTM__Parent_Account__c,AxtriaSalesIQTM__Root_Account__c FROM   AxtriaSalesIQTM__Account_Affiliation__c where Parent_Account_Number__c IN:setHCAFurtherAff and Account_Number__c not in :loosingset and Affiliation_Status__c=\'Active\'';

        
        List<AxtriaSalesIQTM__Account_Affiliation__c> furtherAffHCPList =  Database.query(affHCAQuery);
        //List<AxtriaSalesIQTM__Account_Affiliation__c> FurtherAffHCPList =  Database.query(affHCPQuery);
        Set<String> hcpfurtherAffSet = new Set<String>();
        //Set<String> hcafurtherAffSet = new Set<String>();
        Map<String,Set<String>> mapHCAaff2HCPfrthrAff = new Map<String,Set<String>>();
        
        System.debug('===HCA Affiliation to further HCP Affiliation====' +furtherAffHCPList);

        Set<String> inactiveAffPos = new Set<String>();
        //Set<String> affKeysetList = new Set<String>();
        //affKeysetList=;
        System.debug('========Assignment End here if further affiliation is null or zero');
        if(furtherAffHCPList.size() == 0 || furtherAffHCPList ==null){
            for(String s : mapHCAaff2furtherPos.keySet()){
                inactiveAffPos = mapHCAaff2furtherPos.get(s);
            }

            List<AxtriaSalesIQTM__Position_Account__c> inactiveFrthrPosList = [select id, AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Position__c in :inactiveAffPos and AxtriaSalesIQTM__Account__r.AccountNumber in :mapHCAaff2furtherPos.keySet()];
        List<AxtriaSalesIQTM__Position_Account__c> affpaList = new List<AxtriaSalesIQTM__Position_Account__c>();
        
        if(inactiveFrthrPosList != null){
            for(AxtriaSalesIQTM__Position_Account__c affPA : inactiveFrthrPosList){
                if(affPA.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=='Current')
                    affPA.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addDays(-1);

                if(affPA.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=='Future')
                    affPA.AxtriaSalesIQTM__Effective_End_Date__c=affPA.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c.addDays(-1);
                
                affpaList.add(affPA);
            }
            System.debug('======affpaList====' +affpaList);
        }

        if(affpaList.size() > 0)
            update affpaList;
        }

        if(furtherAffHCPList != null){
            for(AxtriaSalesIQTM__Account_Affiliation__c tempList : furtherAffHCPList){
               if(!mapHCAaff2HCPfrthrAff.containsKey(tempList.Parent_Account_Number__c)){

                   Set<String> furtherHCP = new Set<String>();
                   furtherHCP.add(tempList.Account_Number__c);
                   mapHCAaff2HCPfrthrAff.put(tempList.Parent_Account_Number__c,furtherHCP);
               }
               else{
                mapHCAaff2HCPfrthrAff.get(tempList.Parent_Account_Number__c).add(tempList.Account_Number__c);
               }

                /*if(!loosingset.contains(tempList.Account_Number__c)){
                    hcpfurtherAffSet.add(tempList.Account_Number__c);
                }*/
            }
        }
        System.debug('=====mapHCAaff2HCPfrthrAff:::' +mapHCAaff2HCPfrthrAff);

        
        System.debug('======Position Account check for Further Affiliation Account=========');

        Set<String> secHCPSet = new Set<String>();
        for(String s : mapHCAaff2HCPfrthrAff.keySet()){
            for(String secHCP : mapHCAaff2HCPfrthrAff.get(s)){
                secHCPSet.add(secHCP);
            }
        }
        Set<String> frthrPosSetcheck = new Set<String>();
        for(String s : mapHCAaff2furtherPos.keySet()){
            for(String posCheck : mapHCAaff2furtherPos.get(s)){
                frthrPosSetcheck.add(posCheck);
            }
        }
        System.debug('=====secHCPSet:::' +secHCPSet);

        List<AxtriaSalesIQTM__Position_Account__c> secAffPosAcc = [select Id, AxtriaSalesIQTM__Account__c, AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Account__r.Type,AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Position__c != null and AxtriaSalesIQTM__Account__c != null and (AxtriaSalesIQTM__Assignment_Status__c = 'Active' or AxtriaSalesIQTM__Assignment_Status__c = 'Future Active') and AxtriaSalesIQTM__Account__r.AccountNumber in :secHCPSet and AxtriaSalesIQTM__Position__c in :frthrPosSetcheck];

        Set<String> inactiveAssign = new Set<String>();

         if(secAffPosAcc != null){
            for(AxtriaSalesIQTM__Position_Account__c secAffPosAccRec : secAffPosAcc){
                if(!mapsecAff2Pos.containsKey(secAffPosAccRec.AxtriaSalesIQTM__Account__r.AccountNumber)){
                        Set<String> temp = new Set<String>();
                        temp.add(secAffPosAccRec.AxtriaSalesIQTM__Position__c);
                        mapsecAff2Pos.put(secAffPosAccRec.AxtriaSalesIQTM__Account__r.AccountNumber,temp);
                   }
                   else{
                        mapsecAff2Pos.get(secAffPosAccRec.AxtriaSalesIQTM__Account__r.AccountNumber).add(secAffPosAccRec.AxtriaSalesIQTM__Position__c);
                   }
               }
        }
        System.debug('=====mapsecAff2Pos:::' +mapsecAff2Pos);

        Set<String> inactiveAcc=new Set<String>();
        
        /*if(mapAff2Pos!=null && mapsecAff2Pos!=null){
            for(String s : mapAff2Pos.keySet()){
                System.debug('====s::::' +s);
                System.debug('====mapsecAff2Pos.containsKey(s)::::' +mapsecAff2Pos.containsKey(s));
                if(mapsecAff2Pos.containsKey(s)){
                    for(String h : mapsecAff2Pos.get(s)){
                        System.debug('====h::::' +h);
                        Set<String> posChild = mapsecAff2Pos.get(h);
                        System.debug('====posChild::::' +posChild);
                        Set<String> posParent = mapAff2Pos.get(s);
                        System.debug('====posParent::::' +posParent);
                        for(String pos : posChild){
                            System.debug('====pos::::' +pos);
                            System.debug('====!posParent.contains(pos)::::' +!posParent.contains(pos));
                            if(!posParent.contains(pos)){
                                inactiveAssign.add(pos);
                                inactiveAcc.add(s);
                            }
                        }
                    }
                }
            }
        }*/
        //Set<String> posSet = new Set<String>();
        if(mapHCAaff2furtherPos!=null && mapsecAff2Pos!=null){
            for(String s : mapHCAaff2furtherPos.keySet()){
                //posSet = mapAff2Pos.get(s);      
                System.debug('====s::::' +s);
                //System.debug('====posSet::::' +posSet);
                for(String pos : mapHCAaff2furtherPos.get(s)){
                    System.debug('====pos::::' +pos);
                    if(mapHCAaff2HCPfrthrAff != null){
                        for(String hcp : mapHCAaff2HCPfrthrAff.get(s)){
                            System.debug('====hcp::::' +hcp);
                            Set<String> futherSecPos = new Set<String>();
                            //System.debug('====h::::' +h);
                            futherSecPos = mapsecAff2Pos.get(hcp);
                            System.debug('====futherSecPos::::' +futherSecPos);
                            //for(String p : posSet){
                              //  System.debug('====!futherPos.contains(p)::::' + !futherPos.contains(p));
                                if(futherSecPos !=null){
                                    if(!futherSecPos.contains(pos)){
                                        System.debug('====pos::::' +pos);
                                        inactiveAssign.add(pos);
                                        inactiveAcc.add(s);
                                    }
                                }
                                else{
                                    inactiveAssign.add(pos);
                                    inactiveAcc.add(s);  
                                }
                            }
                    }
                }
            }
        }
                    

        System.debug('======Inactive affiliation Position set====' +inactiveAssign);
        System.debug('======Inactive affiliation Account set====' +inactiveAcc);
        System.debug('======Inactive affiliation Assignment==========');
        List<AxtriaSalesIQTM__Position_Account__c> inactiveAssignList = [select id, AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Position__c in :inactiveAssign and AxtriaSalesIQTM__Account__r.AccountNumber in :inactiveAcc];
        List<AxtriaSalesIQTM__Position_Account__c> paList = new List<AxtriaSalesIQTM__Position_Account__c>();
        
        if(inactiveAssignList != null){
            for(AxtriaSalesIQTM__Position_Account__c affPA : inactiveAssignList){
                if(affPA.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=='Current')
                    affPA.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addDays(-1);

                if(affPA.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=='Future')
                    affPA.AxtriaSalesIQTM__Effective_End_Date__c=affPA.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c.addDays(-1);
                
                paList.add(affPA);
            }
            System.debug('======paList====' +paList);
        }

        if(paList.size() > 0)
            update paList;
       

        /*** ADDED BY SIVA****/
        String query2 = 'SELECT id, AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Account__r.Status__c,Account_Number__c,Parent_Account_Number__c,AxtriaSalesIQTM__Affiliation_Network__c,AxtriaSalesIQTM__Parent_Account__c,AxtriaSalesIQTM__Root_Account__c FROM   AxtriaSalesIQTM__Account_Affiliation__c where AxtriaSalesIQTM__Account__c IN:loosingList or AxtriaSalesIQTM__Parent_Account__c IN:loosingList';

        List<AxtriaSalesIQTM__Account_Affiliation__c> LosAccAffList2 =  Database.query(query2);

        if(LosAccAffList2!=null && LosAccAffList2.size() >0){
            for(AxtriaSalesIQTM__Account_Affiliation__c af: LosAccAffList2) {
                af.Affiliation_Status__c = 'Inactive';
                af.Affiliation_End_Date__c = Date.today().addDays(-1);
                af.AxtriaSalesIQTM__Is_Primary__c=false;
                af.AxtriaSalesIQTM__Active__c = false;
            }
            System.debug('======LosAccAffList2====' +LosAccAffList2);
            update LosAccAffList2;   
        }

        
    }

    global void finish(Database.BatchableContext BC) {

    }
}