public with sharing class DeassignBatch {

    public Boolean flagval{get;set;}
    public String selectedCountry {get;set;}
    public String selectedCountry2 {get;set;}
    public List<SelectOption> country {get;set;}

    public DeassignBatch() {

        selectedCountry='--None--';
        country = new List<SelectOption>();
        list<AxtriaSalesIQTM__Country__c> countryList = [select Id, Name from AxtriaSalesIQTM__Country__c ]; //where Country_Name__c=:countryName
        country.add(new SelectOption('', '--None--'));
        for(AxtriaSalesIQTM__Country__c countryRec : countryList){
            country.add(new SelectOption(countryRec.Id,countryRec.Name));
        }
        country.sort();
        //JuncList.add(new SelectOption('','--None--'));
        
    }

    

    public void callDeassignPositionAccount(){
       
        System.debug('flag::::::::' +flagval);
        System.debug('selectedCountry::::::::' +selectedCountry);
        List<AxtriaSalesIQTM__Country__c> countryName = [select Name from AxtriaSalesIQTM__Country__c where Id = :selectedCountry];
        String countryValue = countryName[0].Name;
        System.debug('countryName::::::::' +countryValue);
        Database.executeBatch(new BatchPopulateAccRuleType_new(flagval,selectedCountry),20);
        
    }


    public void DeassignPositionGeography(){
       
        System.debug('selectedCountry::::::::' +selectedCountry2);
        List<AxtriaSalesIQTM__Country__c> countryName = [select Name from AxtriaSalesIQTM__Country__c where Id = :selectedCountry2];
        String countryValue = countryName[0].Name;
        System.debug('countryName::::::::' +countryValue);
        Database.executeBatch(new BatchFillLookupBulkPosGeo_Delete_new(countryValue));
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Position Geography Deassignment is in Progress !!'));
        
    }
    
}