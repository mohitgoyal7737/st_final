global with sharing class BatchManualSurveyCreation implements Database.Batchable<sObject>, database.stateful
{
    public String query;
    List<ScheduledTeamInstances__c> scheduledTIList;
    List<String> teaminstancelist;
    public String teamInstName;

    global BatchManualSurveyCreation()
    {
        teaminstancelist = new List<String>();
        List<Measure_Master__c> mm = [Select id, State__c, Team_Instance__c from Measure_Master__c where State__c = 'Complete' WITH SECURITY_ENFORCED];
        if(mm != null && mm.size() > 0)
        {
            for(Measure_Master__c measure : mm)
            {
                teaminstancelist.add(measure.Team_Instance__c);
            }
        }

        SnTDMLSecurityUtil.printDebugMessage('teaminstancelist+' + teaminstancelist);
        this.query = 'select id,Name from AxtriaSalesIQTM__Team_Instance__c where Manual_Survey_Load__c = true and id not in :teaminstancelist WITH SECURITY_ENFORCED';
    }

    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<AxtriaSalesIQTM__Team_Instance__c> scope)
    {
        scheduledTIList  = new List<ScheduledTeamInstances__c>();
        //scheduledTIList = [select id from ScheduledTeamInstances__c];
        scheduledTIList = [select id from ScheduledTeamInstances__c where Type__c = 'File' WITH SECURITY_ENFORCED];
        SnTDMLSecurityUtil.printDebugMessage('scheduledTIList size()-->' + scheduledTIList.size());
        if(scheduledTIList != null && scheduledTIList.size() > 0)
        {
            //delete scheduledTIList;
            SnTDMLSecurityUtil.deleteRecords(scheduledTIList, 'BatchManualSurveyCreation');
        }
        scheduledTIList  = new List<ScheduledTeamInstances__c>();
        for(AxtriaSalesIQTM__Team_Instance__c ti : scope)
        {
            ScheduledTeamInstances__c sti = new ScheduledTeamInstances__c(Name = ti.Name);
            sti.Team_Instance_Name__c = ti.id;
            sti.Type__c = 'File';
            scheduledTIList.add(sti);
        }
        SnTDMLSecurityUtil.printDebugMessage('++scheduledTIList' + scheduledTIList);
        if(scheduledTIList != null && scheduledTIList.size() > 0)
        {
            SnTDMLSecurityUtil.printDebugMessage('scheduledTIList size to be inserted-->' + scheduledTIList.size());
            //insert scheduledTIList;
            SnTDMLSecurityUtil.insertRecords(scheduledTIList, 'BatchManualSurveyCreation');
            teamInstName = scheduledTIList[0].Name;
            //delete scheduledTIList[0];
            SnTDMLSecurityUtil.deleteRecords(scheduledTIList[0], 'BatchManualSurveyCreation');
            SnTDMLSecurityUtil.printDebugMessage('++teamInstName' + teamInstName);
        }
    }

    global void finish(Database.BatchableContext BC)
    {
        if(!System.Test.isRunningTest())
        {
            SnTDMLSecurityUtil.printDebugMessage('++teamInstName' + teamInstName);
            Survey_Def_Prof_Para_Meta_Insert_Utility svsd = new Survey_Def_Prof_Para_Meta_Insert_Utility(teamInstName, true, 'cust1');
            Database.executeBatch(svsd, 500);
        }
    }
}