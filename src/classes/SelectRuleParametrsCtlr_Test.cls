@isTest
public class SelectRuleParametrsCtlr_Test {
    @istest static void SelectRuleParametrsCtlr_Test()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        
        
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team,teamins,countr);
        insert pcc;
        
        Parameter__c pp = TestDataFactory.parameter(pcc,team,teamins);
        insert pp;
        
        Measure_Master__c  mMaster = TestDataFactory.createMeasureMaster(pcc,team,teamins);
        insert mMaster;
        
        Grid_Master__c gMaster = TestDataFactory.gridMaster(countr);
        insert gMaster;
        
        Rule_Parameter__c rps = new Rule_Parameter__c();
        rps.Measure_Master__c = mMaster.id;
        rps.Parameter__c = pp.id;
        rps.CurrencyIsoCode='USD';
        insert rps;
        
        Compute_Master__c ccMaster = TestDataFactory.createComputeMaster(rps);
        insert ccMaster;
        
        Step__c steps = TestDataFactory.step(ccMaster,gMaster,mMaster,rps);
        insert steps;
        
        Rule_Parameter__c rps1 = TestDataFactory.ruleParameter(mMaster,pp,steps);
        insert rps1;
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            
            SelectRuleParametrsCtlr obj=new SelectRuleParametrsCtlr();
            obj.getParameterList();
        }
        Test.stopTest();
    }
    
    @istest static void SelectRuleParametrsCtlr_Test1()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        
        
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team,teamins,countr);
        insert pcc;
        
        Parameter__c pp = TestDataFactory.parameter(pcc,team,teamins);
        insert pp;
        
        Measure_Master__c  mMaster = TestDataFactory.createMeasureMaster(pcc,team,teamins);
        insert mMaster;
        
        Grid_Master__c gMaster = TestDataFactory.gridMaster(countr);
        insert gMaster;
        
        Rule_Parameter__c rps = new Rule_Parameter__c();
        rps.Measure_Master__c = mMaster.id;
        rps.Parameter__c = pp.id;
        rps.CurrencyIsoCode='USD';
        insert rps;
        
        Compute_Master__c ccMaster = TestDataFactory.createComputeMaster(rps);
        insert ccMaster;
        
        Step__c steps = TestDataFactory.step(ccMaster,gMaster,mMaster,rps);
        insert steps;
        
        Rule_Parameter__c rps1 = TestDataFactory.ruleParameter(mMaster,pp,steps);
        insert rps1;
        
        String newParameter = pp.Name;
        String selectedParameters = 'Test';
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            
            SelectRuleParametrsCtlr obj=new SelectRuleParametrsCtlr();
            
            obj.selectedParameters = '["'+pp.id+'"]';
            obj.selectedBuisnessUnit = teamins.id;
            obj.selectedBrand= pcc.id;
            obj.selectedCycle = teamins.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c;
            obj.selectedCustType = '1';
            obj.save();
            SelectRuleParametrsCtlr.addNewParameter(newParameter);
            obj.setMyPicklistvalue(selectedParameters);
            obj.SaveParamSaveAndNext();
            obj.saveAndNext();
            obj.hidePopup();
            obj.getMyPicklistvalue();
        }
        Test.stopTest();
    }
}