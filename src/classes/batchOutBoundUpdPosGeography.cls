global class batchOutBoundUpdPosGeography implements Database.Batchable<sObject>, Database.Stateful,schedulable{
     public Integer recordsProcessed=0;
     public String batchID;
     global DateTime lastjobDate=null;
     global String query;
      public String UnAsgnPos='00000';
    public String UnAsgnPos1='0';
    public map<String,String>Countrymap {get;set;}
    public map<String,String>mapVeeva2Mktcode {get;set;}
    public set<String> Uniqueset {get;set;}
     public String cycle {get;set;}
    
    global batchOutBoundUpdPosGeography (){
    }
    
    
    global Database.QueryLocator  start(Database.BatchableContext bc) {
         return Database.getQueryLocator(query);
    }
     public void execute(System.SchedulableContext SC){
       
    }
    global void execute(Database.BatchableContext bc, List<AxtriaSalesIQTM__Position_Geography__c> records){
    }    
    global void finish(Database.BatchableContext bc){
    }  
}