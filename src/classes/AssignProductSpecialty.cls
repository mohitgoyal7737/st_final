global with sharing class AssignProductSpecialty implements Database.Batchable<sObject>,Database.Stateful  {
    public String query;
    public String selectedTeamInstance;
    public String cycleName;
    public String Ids;
    public integer recordsUpdated = 0;
    public integer totalRecrods;
    public integer recordsFailed;   
    Boolean flag= true;

    global AssignProductSpecialty(String teamInstance,String Ids) 
    {
        selectedTeamInstance = teamInstance;
        this.Ids = Ids;
        //Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c
        //cycleName = [select Cycle__r.Name from AxtriaSalesIQTM__Team_Instance__c where id = :selectedTeamInstance].Cycle__r.Name;
        cycleName = [select AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Name from AxtriaSalesIQTM__Team_Instance__c where id = :selectedTeamInstance].AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Name;
        
    query = ' Select P1__c,Product_Type__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.name,AxtriaSalesIQTM__position__r.name,AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__Speciality__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c  where AxtriaSalesIQTM__Team_Instance__c = \'' + selectedTeamInstance + '\' and AxtriaSalesIQTM__lastApprovedTarget__c=True and P1__c!=null';
    }
    
    global AssignProductSpecialty(String teamInstance) 
    {
        selectedTeamInstance = teamInstance;
        //Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c
        //cycleName = [select Cycle__r.Name from AxtriaSalesIQTM__Team_Instance__c where id = :selectedTeamInstance].Cycle__r.Name;
        cycleName = [select AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Name from AxtriaSalesIQTM__Team_Instance__c where id = :selectedTeamInstance].AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Name;
        
    query = ' Select P1__c,Product_Type__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.name,AxtriaSalesIQTM__position__r.name,AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__Speciality__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c  where AxtriaSalesIQTM__Team_Instance__c = \'' + selectedTeamInstance + '\' and AxtriaSalesIQTM__lastApprovedTarget__c=True and P1__c!=null';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scope) 
    {
        
        Set<String> allPositions = new Set<String>();
        Set<String> allProducts = new Set<String>();

        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : scope)
        {
            allPositions.add(pacp.AxtriaSalesIQTM__Position__r.name);
            allProducts.add(pacp.P1__c);
        }
       SnTDMLSecurityUtil.printDebugMessage('++allPositions'+allPositions);
       SnTDMLSecurityUtil.printDebugMessage('++allProducts'+allProducts);

       
        Map<String,String> specialtytoProduct = new Map<String,String>();
        SnTDMLSecurityUtil.printDebugMessage('++cycleName'+cycleName);

        //Shivansh - A1450 -- Replacing Account_To_ProductType__c with Product_Priority__c
        //List<AccountTo_ProductType__c> acctoprod= [select Product_Name__c, Product_Type__c, CycleName__c, Position_Name__c, Specialty__c from AccountTo_ProductType__c where CycleName__c = :cycleName and Position_Name__c in :allPositions and Product_Name__c in :allProducts];
        List<Product_Priority__c> acctoprod= [select Product_Name__c, Product_Type__c, CycleName__c, Position_Name__c, Speciality_Name__c from Product_Priority__c where CycleName__c = :cycleName and Position_Name__c in :allPositions and Product_Name__c in :allProducts and Type__c = 'Territory'];
        for(Product_Priority__c accProd:acctoprod)
           {
               string key=accProd.Speciality_Name__c+accProd.Product_Name__c+accProd.CycleName__c;
               specialtytoProduct.put(key,accProd.Product_Type__c);
           }
           SnTDMLSecurityUtil.printDebugMessage('+++specialtytoProduct'+specialtytoProduct);
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp: scope)
        {
            if(pacp.AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__Speciality__c!=null)
            {
                string key1= pacp.AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__Speciality__c+pacp.P1__c+pacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.name;
                SnTDMLSecurityUtil.printDebugMessage('+++key1'+key1);
                if(specialtytoProduct.containsKey(key1))
                {
                   pacp.Product_Type__c= specialtytoProduct.get(key1);
                }
                SnTDMLSecurityUtil.printDebugMessage('+++pacp.Product_Type__c'+pacp.Product_Type__c);
            }
        }
        totalRecrods= scope.size();
        database.saveresult[] ds = Database.update(scope,false);
        for(database.SaveResult d : ds){
                if(d.issuccess()){
                recordsUpdated++;
                }
                else {
                    flag= false;
                }
            }
            recordsFailed = totalRecrods - recordsUpdated;
        
    }

    global void finish(Database.BatchableContext BC) 
    {
    
    if(Ids!=null){
        AxtriaSalesIQTM__Change_Request__c changerequest = [select Job_Status__c,Records_Updated__c,Records_Failed__c from AxtriaSalesIQTM__Change_Request__c where id =: Ids];
            if(flag){
                changerequest.Job_Status__c = 'Done';
            }
            else {
                changerequest.Job_Status__c = 'Error';
            }
        changerequest.Records_Updated__c = recordsUpdated;
        changerequest.Records_Failed__c = recordsFailed;
        //update changerequest;
        SnTDMLSecurityUtil.updateRecords(changerequest, 'AssignProductSpecialty');
        
        }
    }
}