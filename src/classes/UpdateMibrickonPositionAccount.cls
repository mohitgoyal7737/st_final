global class UpdateMibrickonPositionAccount implements Database.Batchable<sObject>, Database.Stateful,Schedulable{
    
    public String query; 
    public Integer recordsProcessed=0;
    public String batchID;
    global DateTime lastjobDate=null;
    public String cycle {get;set;}
    public map <ID,string> AccounttoPincode;
    global Set<string> AccountsID;
    public list<AxtriaSalesIQTM__Position_Account__c> commentsfil;
    

    global UpdateMibrickonPositionAccount (){

        List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
       schLogList=[Select Id,CreatedDate,Created_Date2__c from Scheduler_Log__c where Job_Name__c='Update_Mini_Brick' and Job_Status__c='Successful' Order By Created_Date2__c desc];
        if(schLogList.size()>0)
        {
          lastjobDate=schLogList[0].Created_Date2__c;  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
        else
        {
          lastjobDate=null;       //else we set the lastjobDate to null
        }
        System.debug('last job'+lastjobDate);
        //Last Batch run ID
        Scheduler_Log__c sJob = new Scheduler_Log__c();

        sJob.Job_Name__c = 'Update_Mini_Brick';
        sJob.Job_Status__c = 'Failed';
        sJob.Job_Type__c='Inbound';
        if(cycle!=null && cycle!='')
          sJob.Cycle__c=cycle;
        sJob.Created_Date2__c = DateTime.now();

        insert sJob;
        batchID = sJob.Id;

        recordsProcessed =0;
       query ='SELECT id,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Comments__c,AxtriaSalesIQTM__Affiliation_Based_Alignment__c,AxtriaSalesIQTM__Account_Alignment_Type__c from AxtriaSalesIQTM__Position_Account__c where (AxtriaSalesIQTM__Account_Alignment_Type__c =\'Explicit\' OR  (AxtriaSalesIQTM__Account_Alignment_Type__c = \'Implicit\' AND AxtriaSalesIQTM__Affiliation_Based_Alignment__c = true)) AND AxtriaSalesIQTM__Comments__c = null AND (AxtriaSalesIQTM__Assignment_Status__c = \'Future Active\' OR AxtriaSalesIQTM__Assignment_Status__c = \'Active\') ' ;
        
        if(lastjobDate!=null){
          query = query + 'AND CreatedDate  >=:  lastjobDate  '; 
        }

        System.debug('query'+ query);
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<AxtriaSalesIQTM__Position_Account__c> scope) {


        AccounttoPincode=new map <Id,string>();
        AccountsID = new set<String>();
        commentsfil = new list<AxtriaSalesIQTM__Position_Account__c>();

      system.debug('Scope Size$$$$$$$$$$$$$$$$$'+Scope.size());

       for (AxtriaSalesIQTM__Position_Account__c PA : Scope){

          AccountsID.add(PA.AxtriaSalesIQTM__Account__c);


       }


        list<AxtriaSalesIQTM__Account_Address__c > AcctAddress = [SELECT id,AxtriaSalesIQTM__Account__c,LastModifiedDate,AxtriaSalesIQTM__Pincode__c,AxtriaSalesIQTM__Address_Type__c FROM AxtriaSalesIQTM__Account_Address__c where AxtriaSalesIQTM__Account__c IN :AccountsID  AND AxtriaSalesIQTM__Address_Type__c = 'Primary'];


        for (AxtriaSalesIQTM__Account_Address__c AA : AcctAddress ){

           AccounttoPincode.put(AA.AxtriaSalesIQTM__Account__c,AA.AxtriaSalesIQTM__Pincode__c);

        }

        list<AxtriaSalesIQTM__Position_Account__c > PosAccountfill = [SELECT id,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Comments__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Account__c IN : AccounttoPincode.keySet() AND (AxtriaSalesIQTM__Account_Alignment_Type__c = 'Explicit' OR AxtriaSalesIQTM__Account_Alignment_Type__c = 'Implicit') AND (AxtriaSalesIQTM__Assignment_Status__c = 'Future Active' OR AxtriaSalesIQTM__Assignment_Status__c = 'Active')];

        for (AxtriaSalesIQTM__Position_Account__c PAupd : PosAccountfill){


         //AxtriaSalesIQTM__Position_Account__c PAtoupdate = new AxtriaSalesIQTM__Position_Account__c();
         PAupd.AxtriaSalesIQTM__Comments__c = AccounttoPincode.get(PAupd.AxtriaSalesIQTM__Account__c);

         //commentsfil.add(PAtoupdate);
       }

       update PosAccountfill;
        system.debug('PosAccountfill**********'+PosAccountfill.size());
       recordsProcessed = recordsProcessed + PosAccountfill.size();

       }

       



//EndproductCatalog

   
global void execute(SchedulableContext sc){
        Database.executeBatch(new UpdateMibrickonPositionAccount (), 100);
    }
    
    global void finish(Database.BatchableContext BC) {
       Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID); 
      system.debug('schedulerObj++++before'+sJob);
      //Update the scheduler log with successful
      sjob.Object_Name__c = 'Update_Mini_Brick';
      //sjob.Changes__c                                       
           
      sJob.No_Of_Records_Processed__c=recordsProcessed;
      sJob.Job_Status__c='Successful';
      //system.debug('error message'+msg);
      //sjob.Changes__c = msg;                    

      system.debug('sJob++++++++'+sJob);
      update sJob;
    }
}