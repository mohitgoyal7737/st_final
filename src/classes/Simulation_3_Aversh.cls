public class Simulation_3_Aversh {
    
 public boolean checktable {get;set;}
 public Decimal totalcapacity {get;set;}
 public String selectedCycle {get;set;}
 public Integer terrcount {get;set;}
 public Decimal totalDays {get;set;}
 public Decimal totalcalls {get;set;}
 public List<AxtriaSalesIQTM__Team_Instance__c> teaminst {get;set;}
 public String selectedBu {get;set;}
 public String selectedbu1 {get;set;}
 public String selectedLine {get;set;}
 public String selectedBrand {get;set;}
 public List<AggregateResult> position {get;set;}
 public List<AggregateResult> userlist {get;set;}
 public List<SimulationRecs> results {get;set;}
 public List<SelectOption> allSimulations {get;set;}
 public String selectedSimulation {get;set;}
 public integer sumCustUniverse {get;set;}
 public integer sumPriority {get;set;}
 public integer sumCallFrequency {get;set;}
 public integer sumTargetCalls {get;set;}
 public String changedBrand{get;set;}
 public String jsonMap {get;set;}

 public List<SelectOption> allTeamInstance  {get;set;}
 public String countryID {get;set;}
 public AxtriaSalesIQTM__Country__c Country;
 public string userType {get;set;}
 public List<AxtriaSalesIQTM__User_Access_Permission__c> loggedInUserData  {get;set;}
 public Map<string,string> successErrorMap;
  public string selectedTeamInstance                       {get;set;}
    Public String TeamInstancename {get;set;}
    public string cycleSelected{get; set;}
    public list<SelectOption>allcycles {get;set;}
    public list<SelectOption> cycles {get;set;}
    public list<SelectOption>businessUnits {get;set;}
    public boolean segSimulation {get;set;}
     public String selectedMatrix{get;set;}
     public String  selectedMatrixCopy{get;set;}


    public Simulation_3_Aversh()
    {
      checktable=false;  
      changedBrand = '';
    }

    public PageReference redirectToPage()
    {
        system.debug('+++++++'+selectedSimulation);
        if(selectedSimulation == 'Segment Simulation')
        {
            PageReference pf = new PageReference('/apex/Simulation2');
            pf.setRedirect(true);
            return pf;
        }
        else if (selectedSimulation == 'Workload Simulation')
        {
            PageReference pf = new PageReference('/apex/Aversh_Simulation_3_change');
            pf.setRedirect(true);
            return pf;
        }
        else
        {
            return null;
        }
    }
    
    public Simulation_3_Aversh(ApexPages.StandardController controller)
    {
        userlist = [Select count(id) usrcount from User where isActive=TRUE];
        results = new List<SimulationRecs>();
        totalDays = 0;
        totalcalls= 0;
        terrcount = 0;
        totalcapacity = 0;
        allSimulations = new List<SelectOption>();
        businessUnits  = new list<SelectOption>();
        selectedSimulation = 'Workload Simulation';
        allSimulations.add(new SelectOption('Segment Simulation', 'Segment Simulation'));
        allSimulations.add(new SelectOption('Workload Simulation', 'Workload Simulation'));
        countryID = SalesIQUtility.getCookie('CountryID');
        system.debug('##### countryID ' + countryID);
        successErrorMap = SalesIQUtility.checkCountryAccess(countryID);
        system.debug('############ successErrorMap ' + successErrorMap);
        if(successErrorMap.containsKey('Success'))
        {
           countryID = successErrorMap.get('Success');               
           system.debug('########## countryID from Map ' + countryID);
           //As soon as we get Country ID set it in the cookie and it will applicable for whole application.
           SalesIQUtility.setCookieString('CountryID',countryID);
            Country = new AxtriaSalesIQTM__Country__c();
            Country = [select AxtriaSalesIQTM__Country_Flag__c,Name from AxtriaSalesIQTM__Country__c where id =:countryID limit 1];
        }
        fillCycleOptions();
        
    }
   
     public void fillCycleOptions(){
        cycles = new list<SelectOption>();
        cycles.add(new SelectOption('None', '--None--'));
        List<Cycle__c> allCycles = [SELECT Id, Name FROM Cycle__c where Country__c =:countryID ];

        if(allCycles != null && allCycles.size() > 0)
        {
            selectedCycle = allCycles[0].ID;

            for(Cycle__c cycle:allCycles){
                cycles.add(new SelectOption(cycle.Id, cycle.Name));
            }            
        }
         businessUnits = new list<SelectOption>();
        businessUnits.add(new SelectOption('None','--None--'));
    }

   public void getBusinessUnit()
    {
        //List <SelectOption> businessUnits = new List<SelectOption>();
        system.debug('====================selectedCycle::: in getbu is:'+cycleSelected);
        if(cycleSelected ==null)
            cycleSelected =selectedCycle;
        //system.debug('=======Country id is:::'+countryID);
        businessUnits = new list<SelectOption>();
        businessUnits.add(new SelectOption('None','--None--'));
        //AxtriaSalesIQTM__Country__c = :countryID and
        for(AxtriaSalesIQTM__Team_Instance__c bu : [SELECT Id, Name FROM AxtriaSalesIQTM__Team_Instance__c  where  Cycle__c= :cycleSelected and AxtriaSalesIQTM__Country__c = :countryID])
        {
            businessUnits.add(new SelectOption(bu.Id, bu.Name));
        }
        system.debug('===========businessUnits:::'+businessUnits);
    }

    /*public List <SelectOption> getLines(){
        system.debug('--Selected bu inside line change():'+selectedbu);
        List <SelectOption> listValues = new List<SelectOption>();
        listValues.add(new SelectOption('','--None--'));
        for(AxtriaSalesIQTM__Team_Instance__c s: [select id,Name from AxtriaSalesIQTM__Team_Instance__c where Team_Instance__c =: selectedbu])
        {
            listValues.add(new SelectOption(s.Id,s.Name));
        }
        return listValues;
    }*/

    

  /* public List <SelectOption> getListVal3()
{
    List <SelectOption> listvalues3 = new List<SelectOption>();
    listValues3.add(new SelectOption('','--None--'));
    for(Cycle__c t: [select Name from Cycle__c where Name != null])
    {
        listValues3.add(new SelectOption(t.Name,t.Name));
    }
    return listValues3;
}*/
    
    public Map<String, list<SimulationRecs>> rule2Records{get;set;}
    public Map<String, list<SelectOption>> brand2Rule{get;set;}
    public list<BrandRuleLookup> brandRules{get;set;}
    
    public List<graphData> graphDataList {get;set;}
    public List<AxtriaSalesIQTM__Position__c> positionlist {get;set;}
    public map<string,Integer> positionIdAcComputeMap {get;set;}
    
    public void capacityChart(){
        
    }
    
    public void changeBrand(){
        for(BrandRuleLookup brl: brandRules){
            system.debug('--changedBrand-- ' + changedBrand);
            if(brl.brandName == changedBrand){
                brl.selected = true;
            }else{
                brl.selected = false;
            }
        }
        List<Team_Instance_Product_AZ__c> allTeamInstanceProds = [ select Calls_Day__c, Effective_Days_in_Field_Formula__c from Team_Instance_Product_AZ__c where Team_Instance__c = :selectedBu and Product_Catalogue__r.Name = :changedBrand];

        system.debug('++++++++++++++ brandLookup.brandName' + allTeamInstanceProds);
        position = [Select sum(Days_In_Field__c) totalDays , sum(Calls_Per_Day__c) totalcalls, count(AxtriaSalesIQTM__Team_iD__c) territoryid from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_Instance__c =:selectedBu and AxtriaSalesIQTM__Hierarchy_Level__c = '1' ];//and Line__c =:selectedLine

        if(allTeamInstanceProds.size() > 0)
        {
            totaldays = allTeamInstanceProds[0].Effective_Days_in_Field_Formula__c;
            totalcalls = allTeamInstanceProds[0].Calls_Day__c;
        }
        else
        {
            if(position[0].get('totalDays') == null)
            {
                totaldays = 0;
                totalcalls = 0;
            }
            else
            {
                totaldays = ((Decimal)position[0].get('totalDays')/(Decimal)position[0].get('territoryid')).setscale(2);
                totalcalls = ((Decimal)position[0].get('totalcalls')/(Decimal)position[0].get('territoryid')).setscale(2);   
            }
        }
         terrcount = ((Integer)position[0].get('territoryid'));
         totalcapacity = (totaldays * totalcalls * terrcount).setscale(1);
         checktable = true;
    }
    
    public void search(){
        
             
        results = new list<SimulationRecs>();
        Map<String, Measure_Master__c> rules = new Map<String, Measure_Master__c>([SELECT Id, Name FROM Measure_Master__c WHERE Team_Instance__c =: selectedBu]); // AND Line_2__c =: selectedLine
        system.debug('rules-- ' + rules);
       // system.debug('selectedLine-- ' + selectedLine);
        system.debug('selectedBu-- ' + selectedBu);
        
        brand2Rule = new Map<String, list<SelectOption>>();
        rule2Records = new Map<String, list<SimulationRecs>>();
        brandRules = new list<BrandRuleLookup>();
        map<String, set<String>> brand2ruleSet = new map<String, set<String>>();
        for(AggregateResult ar: [Select sum(Count_of_Accounts__c) phyCount, count(id) totalRecs, sum(Distinct_Recs__c) distinctCount, sum(Sum_Proposed_TCF__c) freq, sum(Sum_Proposed_TCF__c) final1, Measure_Master__r.Name ruleName, Measure_Master__c ruleId, Measure_Master__r.Brand_Lookup__r.Name BrandName,Segment__c segmentName from Segment_Simulation__c where Measure_Master__c IN : rules.keySet() GROUP BY Segment__c,Measure_Master__r.Brand_Lookup__r.Name, Measure_Master__c, Measure_Master__r.Name])
         {
            system.debug('++++++++++++ Hey '+ ar);
            list<SelectOption> tempOption;
            set<String> tempSet;
            if(brand2Rule.containsKey((String)ar.get('BrandName')) && brand2ruleSet.containsKey((String)ar.get('BrandName'))){
                tempOption = brand2Rule.get((String)ar.get('BrandName'));
                tempSet = brand2ruleSet.get((String)ar.get('BrandName'));
            }else{
                tempOption = new list<SelectOption>();
                tempSet = new set<String>();
            }
            
            brand2ruleSet.put((String)ar.get('BrandName'), tempSet);
            if(!brand2ruleSet.get((String)ar.get('BrandName')).contains((String)ar.get('ruleId'))){
                tempOption.add(new SelectOption((String)ar.get('ruleId'), (String)ar.get('ruleName')));
                brand2Rule.put((String)ar.get('BrandName'), tempOption); 
            }
            tempSet.add((String)ar.get('ruleId'));
            
            
            list<SimulationRecs> tempOption1;
            if(rule2Records.containsKey((String)ar.get('ruleId'))){
                tempOption1 = rule2Records.get((String)ar.get('ruleId'));
            }else{
                tempOption1 = new list<SimulationRecs>();
            }
            tempOption1.add(new SimulationRecs(ar));
            rule2Records.put((String)ar.get('ruleId'), tempOption1);
         }
         
         system.debug('Hey Rules map is');
         system.debug(rule2Records);

         /*List<AggregateResult> agg = [select count(DISTINCT(Physician_2__c)) phyCount , Measure_Master__c, Measure_Master__r.Name ruleName, Measure_Master__c ruleId, Measure_Master__r.Brand_Lookup__r.Name BrandName,Final_Segment__c segmentName from Account_Compute_Final_Copy__c where Measure_Master__c IN : rules.keySet() AND Physician_2__c != null GROUP BY Final_Segment__c,Priority__c,Measure_Master__r.Brand_Lookup__r.Name, Measure_Master__c, Measure_Master__r.Name LIMIT 20000]]
*/
         Boolean first = true;
         for(String brandName: brand2Rule.keySet()){
             if(first){
                changedBrand = brandName; 
                brandRules.add(new BrandRuleLookup(first, brandName, brand2Rule.get(brandName)[0].getValue(), brand2Rule.get(brandName)));
                first = false;
             }else{
                brandRules.add(new BrandRuleLookup(first, brandName, brand2Rule.get(brandName)[0].getValue(), brand2Rule.get(brandName)));
             }
             
         }
         List<Team_Instance_Product_AZ__c> allTeamInstanceProds = [ select Calls_Day__c, Effective_Days_in_Field_Formula__c from Team_Instance_Product_AZ__c where Team_Instance__c = :selectedBu and Product_Catalogue__r.Name = :changedBrand];

        system.debug('++++++++++++++ brandLookup.brandName' + allTeamInstanceProds);
        position = [Select sum(Days_In_Field__c) totalDays , sum(Calls_Per_Day__c) totalcalls, count(AxtriaSalesIQTM__Team_iD__c) territoryid from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_Instance__c =:selectedBu and AxtriaSalesIQTM__Hierarchy_Level__c = '1' ];//and Line__c =:selectedLine

        if(allTeamInstanceProds.size() > 0)
        {
            totaldays = allTeamInstanceProds[0].Effective_Days_in_Field_Formula__c;
            totalcalls = allTeamInstanceProds[0].Calls_Day__c;
        }
        else
        {
            if(position[0].get('totalDays') == null)
            {
                totaldays = 0;
                totalcalls = 0;
            }
            else
            {
                totaldays = ((Decimal)position[0].get('totalDays')/(Decimal)position[0].get('territoryid')).setscale(2);
                totalcalls = ((Decimal)position[0].get('totalcalls')/(Decimal)position[0].get('territoryid')).setscale(2);   
            }
        }
         terrcount = ((Integer)position[0].get('territoryid'));
         totalcapacity = (totaldays * totalcalls * terrcount).setscale(1);
         checktable = true;
         capacityChart();
                  
         /*graphDataList = new List<graphData>();
        positionlist = [Select Days_In_Field__c , Calls_Per_Day__c, id,name from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_Instance__c =:selectedBu  ]; //and Line__c =:selectedLine
        
        List <AggregateResult> AllAccComputeMap = new List <AggregateResult>();
        AllAccComputeMap = [Select count(Physician_2__c) phyCount, Priority__c pri, sum(calculated_tcf__c) finaltcf,sum(Final_TCF__c) final1 ,Physician__r.AxtriaSalesIQTM__Position__c posid from Account_Compute_Final_Copy__c where Measure_Master__c IN : rules.keySet() AND Physician_2__c != null group by Physician__r.AxtriaSalesIQTM__Position__c, Priority__c  ];
        
        positionIdAcComputeMap =new map<string,Integer>();
        
        for(AggregateResult x : AllAccComputeMap){
            if(Integer.Valueof(x.get('finaltcf')) != 0 && Integer.Valueof(x.get('finaltcf'))!= null){
            positionIdAcComputeMap.put(String.valueOf(x.get('posid')),(Integer.Valueof(x.get('phyCount'))  * Integer.Valueof(x.get('finaltcf'))));
            }
        }
        
        
        
        for(AxtriaSalesIQTM__Position__c p : positionlist){
            graphData graphDataobj = new graphData('',0,0);
            graphDataobj.name=p.name;
            system.debug('= Hey Values are '+ p.Days_In_Field__c + ' '+p.Calls_Per_Day__c);

            if(p.Days_In_Field__c ==null || p.Calls_Per_Day__c== null)
            {
                p.Days_In_Field__c = 101;
                p.Calls_Per_Day__c = 9.50;
            }
            graphDataobj.Capacity=Integer.Valueof(p.Days_In_Field__c)*Integer.Valueof(p.Calls_Per_Day__c);
            if(positionIdAcComputeMap.get(String.Valueof(p.id))!= null){
                graphDataobj.WorkLoad=Integer.Valueof(positionIdAcComputeMap.get(String.Valueof(p.id)));
            }
            else{
                graphDataobj.WorkLoad=0;
            }
            
            graphDataList.add(graphDataobj);
        }*/
         
         
    }
    public void save1(){
        system.debug('@@@@@@@@@@InsideMethod');
        
             totalcapacity = ((Decimal)totalDays * (Decimal)totalcalls* (Decimal)terrcount);
    }

    @RemoteAction
    public static Decimal calculateFinalTCF(String ruleId, String segment, Decimal changedFreq, Decimal priority){
        Account_Compute_Final_Copy__c accountComputeFinal = [SELECT Id, Physician_2__c, Output_Name_1__c, Output_Name_2__c, Output_Name_3__c, Output_Name_4__c, Output_Name_5__c, Output_Name_6__c, Output_Name_7__c, Output_Name_8__c, Output_Name_9__c, Output_Name_10__c, Output_Name_11__c, Output_Name_12__c, Output_Name_13__c, Output_Name_14__c, Output_Name_15__c, Output_Name_16__c, Output_Name_17__c, Output_Name_18__c, Output_Name_19__c, Output_Name_20__c, Output_Name_21__c, Output_Name_22__c, Output_Name_23__c, Output_Name_24__c, Output_Name_25__c  FROM Account_Compute_Final_Copy__c WHERE Measure_Master__c=:ruleId  LIMIT 1];
        
        List<Step__c> s1 =  [Select Id, Name FROM Step__c WHERE Measure_Master__c =: ruleId AND UI_Location__c = 'Compute Accessibility' AND Matrix__c != null LIMIT 1];

        String field1;
        Decimal finalTCF = 0;

        if(s1.size() > 0 && s1 != null)
        {
            String accessiblityStepName = [Select Id, Name FROM Step__c WHERE Measure_Master__c =: ruleId AND UI_Location__c = 'Compute Accessibility' AND Matrix__c != null LIMIT 1].Name;
            
            Integer counter = 1;
            System.debug('+++++++++++++ Accessibility name is '+ accessiblityStepName);
            system.debug('Acc Rec is '+accountComputeFinal);
            while(counter <= 25){
                String key = (String)accountComputeFinal.get('Output_Name_'+counter+'__c');
                if(key == accessiblityStepName){
                    field1 = 'Output_Value_'+counter+'__c';
                }
                counter += 1;
            }
            
            System.debug('+++++++++++++ Field name is '+ field1);

            list<Account_Compute_Final_Copy__c> copyFinals = Database.query('Select Physician_2__c, Final_TCF__c, '+field1+' from Account_Compute_Final_Copy__c where Measure_Master__c =: ruleId AND Final_Segment__c =: segment AND Physician_2__c != null');
            for(Account_Compute_Final_Copy__c acf: copyFinals){
                Decimal acc = (String)acf.get(field1) != null ? Decimal.valueOf((String)acf.get(field1)) : 0;
                if(changedFreq > acc){
                    finalTCF += acc;
                }else{
                    finalTCF += changedFreq;
                }
            }
        }
        else
        {
            list<Segment_Simulation__c> copyFinals = Database.query('Select Count_of_Accounts__c  from Segment_Simulation__c where Measure_Master__c =: ruleId AND Segment__c =: segment');
            for(Segment_Simulation__c acf: copyFinals){
                Decimal acc = 0;
                
                    finalTCF += (changedFreq * acf.Count_of_Accounts__c);
                
            }
        }


        

        
        return finalTCF * priority/100;
    }

    public void pushToBusinessRules()
    {
        system.debug('Hey Json String is '+ jsonMap);

        Map<String, Map<String,String>> changedTCFmaps = (Map<String, Map<String,String>>)JSON.deserialize(jsonMap, Map<String, Map<String,String>>.Class);

        system.debug('Wuhu');
        system.debug(changedTCFmaps);

        List<Grid_Details__c> allGridDetails = new List<Grid_Details__c>();
        Boolean flag = false;

        if(changedTCFmaps.keySet().size() > 0 )
        {
            for(String ruleID : changedTCFmaps.keySet())
            {
                List<Step__c> allRequiredSteps = [SELECT Id, Matrix__c, Matrix__r.Name, Matrix__r.Brand__c FROM Step__c WHERE Measure_Master__c = :ruleID AND UI_Location__c = 'Compute TCF' and Step_Type__c = 'Matrix'];

                if(allRequiredSteps.size() != 1)
                {
                    flag = true;
                     ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Publishing not allowed since multiple Matrixes are being used id '+ ruleID));
                }
                else
                {
                    List<Grid_Details__c> gds = [select Dimension_1_Value__c, Dimension_2_Value__c, Output_Value__c,Name from Grid_Details__c where Grid_Master__c = :allRequiredSteps[0].Matrix__c];
                    
                    Integer counter = 2;
                    for(Grid_Details__c gd : gds)
                    {
                       if(changedTCFmaps.get(ruleID).containsKey(gd.Dimension_1_Value__c))
                       {
                            gd.Output_Value__c = changedTCFmaps.get(ruleID).get(gd.Dimension_1_Value__c);
                            allGridDetails.add(gd);
                       }
                    }
                    update allGridDetails;


                    //delete gds;
                }
                
                
                
                if(segSimulation== true)
                {
                    list<Step__c> matricesStep = [SELECT Id, Name, Matrix__c, Matrix__r.Name, Grid_Param_1__r.Parameter_Name__c, Grid_Param_2__r.Parameter_Name__c FROM Step__c WHERE Measure_Master__c =: ruleID AND Step_Type__c ='Matrix' AND UI_Location__c = 'Compute Segment' AND Matrix__r.Grid_Type__c = '2D'];

                    if(matricesStep.size() > 1)
                    {
                        flag = true;
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Segment Simulation cannot be simulated since multiple matrices are being used'));    
                    }
                    else if(matricesStep.size() > 0)
                    {
                        selectedMatrix = matricesStep[0].Matrix__c;
                         Grid_Master__c gm = [SELECT Brand_Team_Instance__c,Brand__c,Col__c,Country__c, Description__c,Dimension_1_Name__c,Dimension_2_Name__c,DM1_Output_Type__c,DM2_Output_Type__c,Grid_Type__c, Name,Output_Name__c,Output_Type__c,OwnerId,Row__c ,Unique_Name__c FROM Grid_Master__c where id = :selectedMatrix];

                        List<Grid_Details_Copy__c> changeGd = [SELECT Id, Name, Output_Value__c, Dimension_1_Value__c, Dimension_2_Value__c FROM Grid_Details_Copy__c WHERE Grid_Master__c =: selectedMatrix];

                        delete [select id from Grid_Details__c where Grid_Master__c = :selectedMatrix];

                        List<Grid_Details__c> gdList = new List<Grid_Details__c>();

                        for(Grid_Details_Copy__c gdc : changeGd)
                        {
                            Grid_Details__c gd = new Grid_Details__c();
                            gd.Name = gdc.Name;
                            gd.Output_Value__c = gdc.Output_Value__c;
                            gd.Dimension_1_Value__c = gdc.Dimension_1_Value__c;
                            gd.Dimension_2_Value__c = gdc.Dimension_2_Value__c;
                            gd.Grid_Master__c = selectedMatrix;

                            gdList.add(gd);
                        }

                        insert gdList;
                    }
                }

                if(!flag)
                {
                    Measure_Master__c rule = [SELECT Id, State__c, Brand_Lookup__c,Team_Instance__c, Cycle__c FROM Measure_Master__c WHERE Id=:ruleId];

            if(rule.State__c == 'Executed')
            {
                 String WhereClause = ' Brand__r.Brand__c =\''+rule.Brand_Lookup__c+'\' AND Cycle__c =\''+rule.Team_Instance__c+'\' AND Cycle2__c =\''+rule.Cycle__c+'\' ';//'\' AND Cycle__c =\''+rule.Team_Instance__c+
                BatchDeleteRecsBeforereExecute batchExecute = new BatchDeleteRecsBeforereExecute(ruleId, WhereClause );
                Database.executeBatch(batchExecute, 1000);
                rule.State__c = 'Executing';
                update rule;

                

                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Business Rule work in Progress'));    
                    }
                    else
                    {
                         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Business Rule not allowed to be executed Again'));     
                    }


                }
            }
        }
        else
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No Changed Made'));     
        }

        
    }

    public class SimulationRecs
    {
        public integer tcfcalculated {get;set;}
        public integer phyCount {get;set;}
        public String segmentName {get;set;}
        public String callFrequency {get;set;}
        public Decimal TCFProposed {get;set;}
        public String CallsProposed {get;set;}
        public String TcfCalls {get;set;}
        public String Potential {get;set;}
        public String oldcallFrequency {get;set;}
        public String priority {get;set;}
        public String oldPriority {get;set;}
        public integer targetCalls {get;set;}
        public integer accbFreq {get;set;}
        public Integer distinctRecs {get;set;}
        public SimulationRecs(AggregateResult result)
        {
            
            tcfcalculated = integer.valueof(result.get('freq'));
            phyCount = integer.valueof(result.get('phyCount'));
            segmentName = String.valueof(result.get('segmentName'));
            oldcallFrequency = String.valueof(result.get('freq'));
            callFrequency = String.valueOf(integer.valueof(result.get('freq'))/integer.valueof(result.get('phyCount')));
            TCFProposed = ((integer.valueof(result.get('final1')) * 1.0)/phyCount).setScale(1);
            accbFreq = Integer.valueOf(TCFProposed);
            oldPriority = '100';

            distinctRecs = Integer.valueof(result.get('distinctCount'))/ Integer.valueof(result.get('totalRecs'));
            //priority = String.valueof(result.get('priority'));
            priority = '100';
            system.debug('+++++++++ Hey Distinct Count ' + distinctRecs);
            system.debug('@@@@@@@@@@1' + phyCount);
            system.debug('@@@@@@@@@@2' + priority);
            system.debug('@@@@@@@@@@3' + callFrequency);
            if(priority != null && callFrequency!= null )    {
            targetCalls =  integer.valueof(result.get('final1')); //(integer.valueof(phyCount)  *
            }
            else
            targetCalls=0;
        }
        
        
    }

    public class BrandRuleLookup{
        public string brandName{get;set;}
        public string selectedRuleID{get;set;}
        public list<SelectOption> rules{get;set;}
        public boolean selected{get;set;}
        public BrandRuleLookup(boolean sel, String brandName, String ruleId, list<SelectOption> rules){
            this.selected  = sel;
            this.brandName = brandName;
            this.selectedRuleID = ruleId;
            this.rules = rules;
        }
    }
 public class graphData {
        public String name { get; set; }
        public Integer Capacity { get; set; }
        public Integer WorkLoad { get; set; }
        public graphData(String name, Integer Capacity, Integer WorkLoad) {
            this.name = name;
            this.Capacity = Capacity;
            this.WorkLoad = WorkLoad;
        }
    }
}