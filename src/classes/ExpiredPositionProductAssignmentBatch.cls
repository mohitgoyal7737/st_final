global with sharing class ExpiredPositionProductAssignmentBatch implements Database.Batchable<sObject> {
    public String query;
    public Set<Id> inactivePosId;
    public Date processDate;
    public String type;
    public Set<String> clientPosSet;
    public Set<String> teamSet;
    public Set<String> clientPosTeamKey;

    global ExpiredPositionProductAssignmentBatch(set<Id> positionId) {
        this.query = query;

        inactivePosId = new Set<Id>();
        teamSet = new Set<String>();
        clientPosSet = new Set<String>();
        clientPosTeamKey = new Set<String>();

        inactivePosId.addAll(positionId);
        double processDay;
        
        processDate = System.today();

        SnTDMLSecurityUtil.printDebugMessage('processDate - '+processDate);
        
        List<AxtriaSalesIQTM__Position__c> allPosList = [Select Id, AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Team_iD__c,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position__c where Id in :inactivePosId];

        SnTDMLSecurityUtil.printDebugMessage('allPosList - '+allPosList);

        for(AxtriaSalesIQTM__Position__c posRec : allPosList)
        {
            clientPosSet.add(posRec.AxtriaSalesIQTM__Client_Position_Code__c);
            teamSet.add(posRec.AxtriaSalesIQTM__Team_iD__c);
            clientPosTeamKey.add(posRec.AxtriaSalesIQTM__Client_Position_Code__c + '_' + posRec.AxtriaSalesIQTM__Team_iD__c);
        }

        SnTDMLSecurityUtil.printDebugMessage('teamSet - '+teamSet);
        SnTDMLSecurityUtil.printDebugMessage('clientPosSet - '+allPosList);

        query = 'Select Id,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c,Product_Catalog__c,AxtriaSalesIQTM__isActive__c,AxtriaSalesIQTM__Effective_End_Date__c From AxtriaSalesIQTM__Position_Product__c where AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c <: processDate and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c in : clientPosSet and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__c in :teamSet and AxtriaSalesIQTM__Team_Instance__c != null and Product_Catalog__c != null and AxtriaSalesIQTM__isActive__c = true order by LastModifiedDate desc';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Product__c> scope) {
        
        SnTDMLSecurityUtil.printDebugMessage('---- updatePosPro----');

        list<AxtriaSalesIQTM__Position_Product__c> posProductList=new list<AxtriaSalesIQTM__Position_Product__c>();

        for(AxtriaSalesIQTM__Position_Product__c posProRec :scope)
        {
            if(clientPosTeamKey.contains(posProRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' + posProRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__c))
            {
                if(posProRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c <= System.today())
                {
                    posProRec.AxtriaSalesIQTM__isActive__c = false;
                }

                if(posProRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c < posProRec.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c)
                    posProRec.AxtriaSalesIQTM__Effective_End_Date__c = posProRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c;
                
                posProductList.add(posProRec);  
                SnTDMLSecurityUtil.printDebugMessage('---- posProRec----' +posProRec);
            }        
        }

        SnTDMLSecurityUtil.printDebugMessage('---- posProductList----' +posProductList);
        SnTDMLSecurityUtil.printDebugMessage('---- posProductList.size()----' +posProductList.size());

        if(posProductList.size() > 0){
            //update posProductList;
            SnTDMLSecurityUtil.updateRecords(posProductList, 'ExpiredPositionProductAssignmentBatch');
        }
    }

    global void finish(Database.BatchableContext BC) {

    }
}