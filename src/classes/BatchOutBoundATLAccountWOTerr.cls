global class BatchOutBoundATLAccountWOTerr implements Database.Batchable<sObject>,Database.stateful,Schedulable  {
    public String query;
    
    global List<id> Country_1;

    Set<String> Set_of_Acc_StgATL = new Set<String>();
    
      
     global BatchOutBoundATLAccountWOTerr(List<id> Country1){
        Country_1 = new List<id>();
        Country_1.addAll(Country1);
        
        System.debug('>>>>>>>>>>>>COuntry<<<<<<<<<<<<'+Country_1);

        query = 'SELECT AccountNumber,AxtriaSalesIQTM__Country__c,Country_Code__c,Marketing_Code__c FROM Account where AccountNumber != NULL AND Status__c = \'Active\' AND AxtriaSalesIQTM__Country__c IN: Country_1 ';

    }
    
   
   global void execute(System.SchedulableContext SC){
        database.executeBatch(new BatchOutBoundPosition());
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Account> scope) {

        Set<String> AccSet = new Set<String>();
        Set<String> Set_of_Acc_StgATL = new Set<String>();

        for(Account ac : scope)
        {
            AccSet.add(ac.AccountNumber);
        }

        for(Staging_ATL__c stg : [SELECT Account__c FROM Staging_ATL__c WHERE Account__c IN: AccSet]){

            Set_of_Acc_StgATL.add(stg.Account__c);

        }

        list<Staging_ATL__c> stg_ATL = new list<Staging_ATL__c>();

        System.debug('<<<<<<<<<<<<Scope>>>>>>>>>'+scope);

        for(Account acc : scope){
            if(!Set_of_Acc_StgATL.contains(acc.AccountNumber)){
                Staging_ATL__c obj = new Staging_ATL__c();

                obj.Account__c = acc.AccountNumber;
                obj.Axtria_Account_ID__c = acc.AccountNumber;
                obj.Country__c = acc.AxtriaSalesIQTM__Country__c;
                obj.Count_of_Territory_Added__c = '0';
                obj.Count_of_Territory_Dropped__c = '0';
                obj.External_ID_AZ__c = acc.AccountNumber;
                obj.External_ID__c = acc.AccountNumber;
                obj.Territory__c = ';';
                obj.Territory_Old__c = ';';

                stg_ATL.add(obj);
            }

        }

        System.debug('<<<<<<<<<<<<stg_ATL>>>>>>>>>'+stg_ATL);


        Upsert stg_ATL External_ID_AZ__c;
        
    }

    global void finish(Database.BatchableContext BC) {

    }
}