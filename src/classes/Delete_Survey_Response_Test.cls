/*Author - Himanshu Tariyal(A0994)
Date : 21st January 2018*/
@isTest
private class Delete_Survey_Response_Test 
{
    private static testMethod void firstTest() 
    {
        /*Create initial test data for all the objs reqd.*/
        AxtriaSalesIQTM__Organization_Master__c aom = new AxtriaSalesIQTM__Organization_Master__c(AxtriaSalesIQTM__Org_Level__c='Global',AxtriaSalesIQTM__Parent_Country_Level__c=true);
        insert aom;
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='Italy',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active');
        insert country;
        
        Global_Question_ID_AZ__c gq = new Global_Question_ID_AZ__c(Name='1');
        insert gq;
        
        Cycle__c cycle = new Cycle__c(Name='Test Cycle',Country__c = country.id);
        insert cycle;
        
        Account acc = new Account(Name='test acc',Marketing_Code__c='EU',AccountNumber='123456');
	    insert acc;
	    
        Business_Unit__c bu = new Business_Unit__c(Name='Test BU Loopup');
        insert bu;
        
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='Test Team',Business_Unit_Loopup__c=bu.id);
        team.AxtriaSalesIQTM__Country__c = country.id;
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU',AxtriaSalesIQTM__Team__c=team.id,Cycle__c=cycle.id);
        insert ti;

        Product_Catalog__c pc = new Product_Catalog__c(Name='Test Product',Team_Instance__c=ti.id,Veeva_External_ID__c = 'ProdId',Product_Code__c= 'ProdId',IsActive__c=true,AxtriaARSnT__Country_Lookup__c=country.id);
        insert pc;
        
        Brand_Team_Instance__c bti = new Brand_Team_Instance__c(Brand__c=pc.id,Team_Instance__c=ti.id);
	    insert bti;
	    
	    /*Prepare Survey_Response__c data*/
	    Survey_Response__c sr = new Survey_Response__c();
	    sr.Name = 'Test SR';
	    sr.Team_Instance__c = ti.id;
	    insert sr;

        System.test.startTest();
        Delete_Survey_Response batch = new Delete_Survey_Response(ti.Name,'file');
        Database.executeBatch(batch,2000);
        System.test.stopTest();
    }
    private static testMethod void secondTest() 
    {
        AxtriaSalesIQTM__Organization_Master__c aom = new AxtriaSalesIQTM__Organization_Master__c(AxtriaSalesIQTM__Org_Level__c='Global',AxtriaSalesIQTM__Parent_Country_Level__c=true);
        insert aom;
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='Italy',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active');
        insert country;
        
        Global_Question_ID_AZ__c gq = new Global_Question_ID_AZ__c(Name='1');
        insert gq;
        
        Cycle__c cycle = new Cycle__c(Name='Test Cycle',Country__c = country.id);
        insert cycle;
        
        Account acc = new Account(Name='test acc',Marketing_Code__c='EU',AccountNumber='123456');
        insert acc;
        
        Business_Unit__c bu = new Business_Unit__c(Name='Test BU Loopup');
        insert bu;
        
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='Test Team',Business_Unit_Loopup__c=bu.id);
        team.AxtriaSalesIQTM__Country__c = country.id;
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU',AxtriaSalesIQTM__Team__c=team.id,Cycle__c=cycle.id);
        insert ti;

        Product_Catalog__c pc = new Product_Catalog__c(Name='Test Product',Team_Instance__c=ti.id,Veeva_External_ID__c = 'ProdId',Product_Code__c= 'ProdId',IsActive__c=true,AxtriaARSnT__Country_Lookup__c=country.id);
        insert pc;
        
        Brand_Team_Instance__c bti = new Brand_Team_Instance__c(Brand__c=pc.id,Team_Instance__c=ti.id);
        insert bti;
        
        /*Prepare Survey_Response__c data*/
        Survey_Response__c sr = new Survey_Response__c();
        sr.Name = 'Test SR';
        sr.Team_Instance__c = ti.id;
        insert sr;

        System.test.startTest();
        Delete_Survey_Response batch = new Delete_Survey_Response();
        Database.executeBatch(batch,2000);
        System.test.stopTest();

      }

}