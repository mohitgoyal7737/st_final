global class UpdateTeamInstance implements Database.Batchable<sObject>, Database.Stateful,schedulable{
	 public Integer recordsProcessed=0;
     public String batchID;
     global DateTime lastjobDate=null;
     global String query;
     
     global UpdateTeamInstance(){//set<String> Accountid
     	
       /* List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
        schLogList=[Select Id,CreatedDate,Created_Date__c from Scheduler_Log__c where Job_Name__c='Team Instance' and Job_Status__c='Successful' Order By Created_Date__c desc];
        if(schLogList.size()>0){
            lastjobDate=schLogList[0].CreatedDate;  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
	    else{
	    	lastjobDate=null;       //else we set the lastjobDate to null
  		}
  		System.debug('last job'+lastjobDate);
        //Last Bacth run ID
		Scheduler_Log__c sJob = new Scheduler_Log__c();
		
		sJob.Job_Name__c = 'Team Instance';
		sJob.Job_Status__c = 'Failed';
		sJob.Job_Type__c='Outbound';
		//sJob.CreatedDate = System.today();
	
		insert sJob;
	    batchID = sJob.Id;
	   
	    recordsProcessed =0;
	   query = 'Select AxtriaSalesIQTM__Country__c,Name,AxtriaSalesIQTM__Base_Team__c,AxtriaSalesIQTM__Team__r.name,AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Type__c,AxtriaSalesIQTM__IC_EffstartDate__c, ' +
	    'AxtriaSalesIQTM__IC_EffEndDate__c,CreatedDate,LastModifiedById,LastModifiedDate,OwnerId,SystemModstamp ' +
	    'FROM AxtriaSalesIQTM__Team_Instance__c ' ;
	  
        
        if(lastjobDate!=null){
        	query = query + 'Where LastModifiedDate  >=:  lastjobDate '; 
        }
                System.debug('query'+ query);
		*/
	        
    }
    
    
 	global Database.QueryLocator  start(Database.BatchableContext bc) {
         return Database.getQueryLocator(query);
    }
     public void execute(System.SchedulableContext SC){
       
    }
     global void execute(Database.BatchableContext bc, List<AxtriaSalesIQTM__Team_Instance__c> records){
        // process each batch of records
       /*

        List<SIQ_Sales_Cycle_O__c> salesCycles = new List<SIQ_Sales_Cycle_O__c>();
        for (AxtriaSalesIQTM__Team_Instance__c team : records) {
            //if(acc.SIQ_Parent_Account_Number__c!=''){
               SIQ_Sales_Cycle_O__c salesCycle=new SIQ_Sales_Cycle_O__c();
           
		           salesCycle.SIQ_Country_Code__c=team.AxtriaSalesIQTM__Country__c;
					//salesCycle.SIQ_External_Team_Id__c=
					salesCycle.SIQ_Team_Instance__c=team.Name;
				//	salesCycle.SIQ_Event__c=
				//	salesCycle.SIQ_Marketing_Code__c=
					
					salesCycle.SIQ_Parent_Team__c=team.AxtriaSalesIQTM__Base_Team__c;
					salesCycle.Name=team.Name;
					salesCycle.SIQ_Team_Name__c=team.AxtriaSalesIQTM__Team__r.name;
					salesCycle.SIQ_Effective_Start_Date__c=team.AxtriaSalesIQTM__IC_EffstartDate__c;
					salesCycle.SIQ_Effective_End_Date__c=team.AxtriaSalesIQTM__IC_EffEndDate__c;
					salesCycle.SIQ_Created_Date__c=team.CreatedDate;
					salesCycle.SIQ_Updated_Date__c=team.LastModifiedDate;
					salesCycle.SIQ_Team_Type__c=team.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Type__c;
                salesCycles.add(salesCycle);
                system.debug('recordsProcessed+'+recordsProcessed);
                recordsProcessed++;
                //comments
            }
     //   }
     
        upsert salesCycles;
       
      */
        
    }    
    global void finish(Database.BatchableContext bc){
      /*  // execute any post-processing operations
         System.debug(recordsProcessed + ' records processed. ');
                Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID); 
                system.debug('schedulerObj++++before'+sJob);
                //Update the scheduler log with successful
                sJob.No_Of_Records_Processed__c=recordsProcessed;
                sJob.Job_Status__c='Successful';
                system.debug('sJob++++++++'+sJob);
                update sJob;
      */  
    }   
}