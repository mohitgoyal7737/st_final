global with sharing class ExpiredSNTAssignmentBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {
    public String query;
    public Set<Id> inactivePosId;
    public Date processDate;
    public String type;
    public Set<String> clientPosSet;
    public Set<String> teamSet;
    public Set<String> clientPosTeamKey;
    public Boolean flag = false;

    global ExpiredSNTAssignmentBatch(set<Id> positionId,String assignmentType) {
        this.query = query;
        type = '';
        //flag = false;
        inactivePosId = new Set<Id>();
        teamSet = new Set<String>();
        clientPosSet = new Set<String>();
        clientPosTeamKey = new Set<String>();

        inactivePosId.addAll(positionId);
        type = assignmentType;

        double processDay;
        
        processDate = System.today();

        SnTDMLSecurityUtil.printDebugMessage('processDate - '+processDate);
        SnTDMLSecurityUtil.printDebugMessage('flag in constructor - '+flag);

        List<AxtriaSalesIQTM__Position__c> allPosList = [Select Id, AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Team_iD__c,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position__c where Id in :inactivePosId];

        SnTDMLSecurityUtil.printDebugMessage('allPosList - '+allPosList);

        for(AxtriaSalesIQTM__Position__c posRec : allPosList)
        {
            clientPosSet.add(posRec.AxtriaSalesIQTM__Client_Position_Code__c);
            teamSet.add(posRec.AxtriaSalesIQTM__Team_iD__c);
            clientPosTeamKey.add(posRec.AxtriaSalesIQTM__Client_Position_Code__c + '_' + posRec.AxtriaSalesIQTM__Team_iD__c);
        }

        SnTDMLSecurityUtil.printDebugMessage('teamSet - '+teamSet);
        SnTDMLSecurityUtil.printDebugMessage('clientPosSet - '+allPosList);

        if(type == 'Position_Account_Call_Plan__c')
        {
            query = 'Select Id, AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__c,AxtriaSalesIQTM__isincludedCallPlan__c, AxtriaSalesIQTM__lastApprovedTarget__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c,AxtriaSalesIQTM__Effective_End_Date__c From AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c <=: processDate and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c in : clientPosSet and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__c in :teamSet and AxtriaSalesIQTM__Team_Instance__c != null and AxtriaSalesIQTM__Account__c != null and Party_ID__c != null and AxtriaSalesIQTM__isincludedCallPlan__c = true and AxtriaSalesIQTM__lastApprovedTarget__c = true order by LastModifiedDate desc';

        }
        else if(type == 'Position_Product__c')
        {
            query = 'Select Id,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c,Product_Catalog__c,AxtriaSalesIQTM__isActive__c,AxtriaSalesIQTM__Effective_End_Date__c From AxtriaSalesIQTM__Position_Product__c where AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c <=: processDate and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c in : clientPosSet and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__c in :teamSet and AxtriaSalesIQTM__Team_Instance__c != null and Product_Catalog__c != null and AxtriaSalesIQTM__isActive__c = true order by LastModifiedDate desc';

        }

        SnTDMLSecurityUtil.printDebugMessage('### query :'+query);
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Sobject> scope) {
        
        SnTDMLSecurityUtil.printDebugMessage('scope size::::::::::::::::::::::'+scope.size());
        if(type == 'Position_Account_Call_Plan__c')
        {
            updateCallPlan(scope);
            
        }
        else if(type == 'Position_Product__c')
        {
            updatePosPro(scope);
            
        }
    }

    global void finish(Database.BatchableContext BC) {

        SnTDMLSecurityUtil.printDebugMessage('--- ExpiredSNTAssignmentBatch Excuted SuccessFully ------');
        SnTDMLSecurityUtil.printDebugMessage('flag in finish - '+flag);
        if(flag == false)
        {
            ExpiredSNTAssignmentBatch b5 = new ExpiredSNTAssignmentBatch(inactivePosId,'Position_Product__c');
            Id batchinstanceid5 = Database.executeBatch(b5,2000);
            flag = true;
        }

    }

    public void updateCallPlan(List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scope)
    {
        SnTDMLSecurityUtil.printDebugMessage('---- updateCallPlan----');

        flag=false;
        SnTDMLSecurityUtil.printDebugMessage('flag in updateCallPlan- '+flag);

        list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> callPlanList=new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();

        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c callPlanRec :scope)
        {
            if(clientPosTeamKey.contains(callPlanRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' + callPlanRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__c))
            {
                if(callPlanRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c <= System.today())
                {
                    callPlanRec.AxtriaSalesIQTM__isincludedCallPlan__c = false;
                    callPlanRec.AxtriaSalesIQTM__lastApprovedTarget__c = false;
                }

                if(callPlanRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c < callPlanRec.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c)
                    callPlanRec.AxtriaSalesIQTM__Effective_End_Date__c = callPlanRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c.addDays(-1);
                
                callPlanList.add(callPlanRec);  
                SnTDMLSecurityUtil.printDebugMessage('---- callPlanRec----' +callPlanRec);
            }
        }

        SnTDMLSecurityUtil.printDebugMessage('---- callPlanList----' +callPlanList);
        SnTDMLSecurityUtil.printDebugMessage('---- callPlanList.size()----' +callPlanList.size());

        if(callPlanList.size() > 0){
            //update callPlanList;
            SnTDMLSecurityUtil.updateRecords(callPlanList, 'ExpiredSNTAssignmentBatch');
        }

    }

    public void updatePosPro(List<AxtriaSalesIQTM__Position_Product__c> scope)
    {
        SnTDMLSecurityUtil.printDebugMessage('---- updatePosPro----');

        flag = true;
        SnTDMLSecurityUtil.printDebugMessage('flag in updatePosPro - '+flag);

        list<AxtriaSalesIQTM__Position_Product__c> posProductList=new list<AxtriaSalesIQTM__Position_Product__c>();

        for(AxtriaSalesIQTM__Position_Product__c posProRec :scope)
        {
            if(clientPosTeamKey.contains(posProRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' + posProRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__c))
            {
                if(posProRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c <= System.today())
                {
                    posProRec.AxtriaSalesIQTM__isActive__c = false;
                }

                if(posProRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c < posProRec.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c)
                    posProRec.AxtriaSalesIQTM__Effective_End_Date__c = posProRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c.addDays(-1);
                
                posProductList.add(posProRec);  
                SnTDMLSecurityUtil.printDebugMessage('---- posProRec----' +posProRec);
            }        
        }

        SnTDMLSecurityUtil.printDebugMessage('---- posProductList----' +posProductList);
        SnTDMLSecurityUtil.printDebugMessage('---- posProductList.size()----' +posProductList.size());

        if(posProductList.size() > 0){
            //update posProductList;
            SnTDMLSecurityUtil.updateRecords(posProductList, 'ExpiredSNTAssignmentBatch');
        }
    }
}