global with sharing class Batch_TeamInstanceMapping implements Database.Batchable<sObject> {
    list<SIQ_MC_Cycle_vod_O__c> listTI;
    map<string, SIQ_MC_Cycle_vod_O__c> mapTI;
    string jobType = util.getJobType('TeamInstance');
    set<string> allCountries = jobType == 'Full Load'?util.getFulloadCountries():util.getDeltaLoadCountries();
   // map<string, boolean> mapProdStatus;
    string query = 'Select Id, Name, AxtriaSalesIQTM__IC_EffstartDate__c, AxtriaSalesIQTM__IC_EffEndDate__c, AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c, Increment_Field__c, AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c From AxtriaSalesIQTM__Team_Instance__c Where AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c in :allCountries and AxtriaSalesIQTM__Alignment_Period__c = \'Current\' WITH SECURITY_ENFORCED';// and AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = \'Live\' and AxtriaSalesIQTM__Team__r.hasCallPlan__c = true';
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return database.getQueryLocator(query);
    }
    
    public void initiateVariables(){
        listTI = new list<SIQ_MC_Cycle_vod_O__c>();
        mapTI = new map<string, SIQ_MC_Cycle_vod_O__c>();
        //mapProdStatus = new map<string,boolean>();
    }
    
    public void createMaps(){
        for(SIQ_MC_Cycle_vod_O__c p : [Select Id, Name, SIQ_Description_vod_del__c, SIQ_End_Date_vod__c, SIQ_External_Id_vod__c, SIQ_Country_Code_AZ__c, SIQ_Start_Date_vod__c, SIQ_Status_vod__c from SIQ_MC_Cycle_vod_O__c WHERE Name != NULL WITH SECURITY_ENFORCED]){
            mapTI.put(p.SIQ_External_Id_vod__c, p);
        }
    }
        
    global void execute(Database.BatchableContext bc, List<AxtriaSalesIQTM__Team_Instance__c> scope){
        initiateVariables();
        createMaps();
        for(AxtriaSalesIQTM__Team_Instance__c pe : scope){
            SIQ_MC_Cycle_vod_O__c TI = new SIQ_MC_Cycle_vod_O__c();
            string externalId = pe.Name+'_'+String.valueof(pe.Increment_Field__c);
            if(!mapTI.containsKey(externalId)){
                TI.SIQ_End_Date_vod__c = pe.AxtriaSalesIQTM__IC_EffEndDate__c;
                TI.SIQ_Description_vod_del__c = pe.Name;
                TI.SIQ_External_Id_vod__c = externalId;
                TI.SIQ_Country_Code_AZ__c = pe.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                TI.SIQ_Start_Date_vod__c = pe.AxtriaSalesIQTM__IC_EffstartDate__c;
                TI.SIQ_Status_vod__c = 'Current';
                TI.Record_Status__c = 'Updated';
                listTI.add(TI);
            }
            else if((mapTI.get(externalId).SIQ_End_Date_vod__c != pe.AxtriaSalesIQTM__IC_EffEndDate__c)||(mapTI.get(externalId).SIQ_Start_Date_vod__c != pe.AxtriaSalesIQTM__IC_EffstartDate__c)||(mapTI.get(externalId).SIQ_Description_vod_del__c != pe.Name)||(mapTI.get(externalId).SIQ_Status_vod__c != 'Current')){
                TI.Id = mapTI.get(externalId).Id;
                TI.SIQ_End_Date_vod__c = pe.AxtriaSalesIQTM__IC_EffEndDate__c;
                TI.SIQ_Description_vod_del__c = pe.Name;
                TI.SIQ_Country_Code_AZ__c = pe.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                TI.SIQ_Start_Date_vod__c = pe.AxtriaSalesIQTM__IC_EffstartDate__c;
                TI.SIQ_Status_vod__c = 'Current';
                TI.Record_Status__c = 'Updated';
                listTI.add(TI);
            }
        }
        if(listTI.size()>0){
            //upsert listTI;
            SnTDMLSecurityUtil.upsertRecords(listTI, 'Batch_TeamInstanceMapping');
        }
    }
    
    global void finish(Database.BatchableContext bc){
    }
}