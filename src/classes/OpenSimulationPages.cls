public class OpenSimulationPages {
    
    public List<SelectOption> allSimulations {get;set;}
    public String selectedSimulation {get;set;}
    
    public OpenSimulationPages()
    {
        allSimulations = new List<SelectOption>();
        allSimulations.add(new SelectOption('Segment Simulation', 'Segment Simulation'));
        allSimulations.add(new SelectOption('Workload Simulation', 'Workload Simulation'));
    }
    
    public PageReference redirectToPage()
    {
        system.debug('+++++++'+selectedSimulation);
        if(selectedSimulation == 'Segment Simulation')
        {
            PageReference pf = new PageReference('/apex/Simulation2');
            pf.setRedirect(true);
            return pf;
        }
        else if (selectedSimulation == 'Workload Simulation')
        {
            PageReference pf = new PageReference('/apex/Aversh_Simulation_3_change');
            pf.setRedirect(true);
            return pf;
        }
        else
        {
            return null;
        }
    }

}