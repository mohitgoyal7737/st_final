@isTest
public class StepMatrix_Test {
   @istest static void StepMatrix_Test()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        Account acc= TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        mmc.State__c ='Executed';
        insert mmc;
        Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
        pp.Name__c = 'test';
        insert pp;
        Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
        insert rp;
        Compute_Master__c ccMaster = TestDataFactory.createComputeMaster(rp);
        insert ccMaster;
        Grid_Master__c gMaster = TestDataFactory.gridMaster(countr);
        insert gMaster;
        Step__c s = TestDataFactory.step(ccMaster, gMaster, mmc, rp);
        s.Step_Type__c = 'Quantile';
        insert s;
        Grid_Details__c g = TestDataFactory.gridDetails(gMaster);
        insert g;
        AxtriaSalesIQTM__Position__c position = TestDataFactory.createPosition(team,teamins);
        insert position;
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,position,teamins);
        insert posAccount;
        BU_Response__c bu = TestDataFactory.createBuResponse(posAccount,pcc,teamins,team,acc);
        insert bu;
        Map<String, String> nameFieldMap = new Map<String, String>();
        nameFieldMap.put('test','test1');
        Map<String, String> acfMap = new Map<String, String>();
        acfMap.put('test','test1');
        
        List<BU_Response__c> allBuResponses =new List<BU_Response__c>();
        allBuResponses.add(bu);
        List<String> allAggregateFunc = new List<String>();
        allAggregateFunc.add('SUM');
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            StepMatrix obj=new StepMatrix(s);
            obj.solveStep(s,nameFieldMap,acfMap,bu,allBuResponses,allAggregateFunc);
            
        }
        Test.stopTest();
    }
    
    
    @istest static void StepMatrix_Test1()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        Account acc= TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        mmc.State__c ='Executed';
        insert mmc;
        Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
        pp.Name__c = 'test';
        insert pp;
        
        Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
        rp.Parameter__c = pp.Id;
        insert rp;
        
        Rule_Parameter__c rp1= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
        rp1.Parameter__c = pp.Id;
        insert rp1;
        
        Compute_Master__c ccMaster = TestDataFactory.createComputeMaster(rp);
        insert ccMaster;
        Grid_Master__c gMaster = TestDataFactory.gridMaster(countr);
        
        gMaster.Grid_Type__c = '1D';
        insert gMaster;
        Step__c step = TestDataFactory.step(ccMaster, gMaster, mmc, rp);
        step.Step_Type__c = 'Quantile';
        step.Grid_Param_1__c = rp.id;
        step.Grid_Param_2__c = rp1.id;
        insert step ;
        rp.Step__c = step.Id;
        update rp1;
        rp1.Step__c = step.Id;
        update rp;
        
        Grid_Details__c g = TestDataFactory.gridDetails(gMaster);
        insert g;
        AxtriaSalesIQTM__Position__c position = TestDataFactory.createPosition(team,teamins);
        insert position;
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,position,teamins);
        insert posAccount;
        BU_Response__c bu = TestDataFactory.createBuResponse(posAccount,pcc,teamins,team,acc);
        insert bu;
        Map<String, String> nameFieldMap = new Map<String, String>();
        nameFieldMap.put('MARKET_ RCC','Parameter_Name__c');
        Map<String, String> acfMap = new Map<String, String>();
        acfMap.put('MARKET_ RCC','NO');
        
        List<BU_Response__c> allBuResponses =new List<BU_Response__c>();
        allBuResponses.add(bu);
        List<String> allAggregateFunc = new List<String>();
        allAggregateFunc.add('SUM');
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            StepMatrix obj=new StepMatrix(Step);
            Step__c stepobj = [Select Id,Grid_Param_1__r.Parameter__c,Grid_Param_1__r.Parameter_Name__c,Grid_Param_2__r.Parameter_Name__c from Step__c limit 1];
            system.debug('+++++++'+stepobj );
            obj.solveStep(Step,nameFieldMap,acfMap,bu,allBuResponses,allAggregateFunc);
            gMaster.Grid_Type__c ='2D';
            update gMaster;
            obj.solveStep(Step,nameFieldMap,acfMap,bu,allBuResponses,allAggregateFunc);
            
        }
        Test.stopTest();
    }
    
    @istest static void StepMatrix_Test2()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        Account acc= TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        mmc.State__c ='Executed';
        insert mmc;
        Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
        insert pp;
        Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
        
        insert rp;
        Compute_Master__c ccMaster = TestDataFactory.createComputeMaster(rp);
        insert ccMaster;
        Grid_Master__c gMaster = TestDataFactory.gridMaster(countr);
        
        gMaster.Grid_Type__c = '1D';
        insert gMaster;
        Step__c s = TestDataFactory.step(ccMaster, gMaster, mmc, rp);
        s.Step_Type__c = 'Quantile';
        insert s;
        Grid_Details__c g = TestDataFactory.gridDetails(gMaster);
        insert g;
        AxtriaSalesIQTM__Position__c position = TestDataFactory.createPosition(team,teamins);
        insert position;
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,position,teamins);
        insert posAccount;
        BU_Response__c bu = TestDataFactory.createBuResponse(posAccount,pcc,teamins,team,acc);
        insert bu;
        Map<String, String> nameFieldMap = new Map<String, String>();
        nameFieldMap.put('MARKET_ RCC','Parameter_Name__c');
        Map<String, String> acfMap = new Map<String, String>();
        acfMap.put('MARKET_ RCC','NO');
        
        List<BU_Response__c> allBuResponses =new List<BU_Response__c>();
        allBuResponses.add(bu);
        List<String> allAggregateFunc = new List<String>();
        allAggregateFunc.add('SUM');
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            StepMatrix obj=new StepMatrix(s);
            obj.matrixMap.put('dim1_DIM2','dim1_DIM2');
            obj.solveStep(s,nameFieldMap,'dim1',bu,'DIM2');
            obj.solveStep(s,nameFieldMap,'dim',bu,'dim');
            
        }
        Test.stopTest();
    }

}