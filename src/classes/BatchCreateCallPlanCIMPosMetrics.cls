/**********************************************************************************************
@author       : SnT Team
@modifiedBy   : Himanshu Tariyal (A0994)
@modifiedDate : 2nd June'2020
@description  : Batch class for creating CIM Position Metric Summary for a particular Team Instance
@Revison(s)   : v1.0
**********************************************************************************************/
global with sharing class BatchCreateCallPlanCIMPosMetrics implements Database.Batchable<sObject>, Database.Stateful 
{
    public String thequery;
    String teamInstanceID;
    String cimConfigId;
    String objectName;
    String attributeAPIname;
    String aggregationType;
    String aggregationObjectName;
    String aggregationAttributeAPIName;
    String aggregationConditionAttributeAPIName;
    String objectToBeQueried;
    Map<String,Decimal> mapAggregate;
    Map<String,Set<String>> mapAggregateSet;
    Map<String,Decimal> mapCount;
    Map<String,Decimal> mapSum;
    List<String> list_cimID;

    public String sourceTeamInst;
    public String destTeamInst;
    public String callPlanLoggerID;
    public String alignNmsp;
    public String batchName;

    public Boolean proceedNextBatch = true;

    //Added by HT(A0994) on 12th June 2020
    global BatchCreateCallPlanCIMPosMetrics(List<String> list_cim, String teamInst,String sourceTeamInstance,String loggerID) 
    {
        batchName = 'BatchCreateCallPlanCIMPosMetrics';
        teamInstanceID  = teamInst;
        sourceTeamInst = sourceTeamInstance;
        destTeamInst = teamInst;
        callPlanLoggerID = loggerID;

        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ScenarioAlignmentCtlr'];
        alignNmsp = cs.NamespacePrefix!=null && cs.NamespacePrefix!='' ? cs.NamespacePrefix+'__' : '';
        
        cimConfigId = list_cim[0];
        list_cimID = list_cim;
        list_cimID.remove(0); 
        mapAggregate = new Map<String,Decimal>();
        mapCount = new Map<String,Decimal>();
        mapSum = new Map<String,Decimal>();
        mapAggregateSet = new Map<String,Set<String>>();
        AxtriaSalesIQTM__CIM_Config__c cim = new AxtriaSalesIQTM__CIM_Config__c();

        cim = [select id, name, AxtriaSalesIQTM__Object_Name__c, AxtriaSalesIQTM__Attribute_API_Name__c, 
                AxtriaSalesIQTM__Aggregation_Type__c, AxtriaSalesIQTM__Aggregation_Object_Name__c, 
                AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c, 
                AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c,
                AxtriaSalesIQTM__Aggregation_Condition_Attribute_Value__c  
                from AxtriaSalesIQTM__CIM_Config__c where id=:cimConfigId and 
                AxtriaSalesIQTM__team_instance__c =:teamInstanceID and 
                AxtriaSalesIQTM__Aggregation_Object_Name__c != null and 
                AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c!= null WITH SECURITY_ENFORCED limit 1];

        objectName = cim.AxtriaSalesIQTM__Object_Name__c;
        attributeAPIname = cim.AxtriaSalesIQTM__Attribute_API_Name__c;
        aggregationType = cim.AxtriaSalesIQTM__Aggregation_Type__c.toUpperCase();
        aggregationObjectName = cim.AxtriaSalesIQTM__Aggregation_Object_Name__c;
        aggregationAttributeAPIName = cim.AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c;
        aggregationConditionAttributeAPIName = cim.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c;
        
        if(objectName != aggregationObjectName){
            objectToBeQueried =  objectName.removeEnd('c');
            objectToBeQueried = objectToBeQueried + 'r.'+attributeAPIname;            
        }
        else{
            objectToBeQueried = attributeAPIname;
            
        } 
        if(aggregationConditionAttributeAPIName != null){
            theQuery = 'select ' + objectToBeQueried + ', ' + aggregationAttributeAPIName + ' from ' +
            aggregationObjectName + ' where AxtriaSalesIQTM__team_instance__c =:teamInstanceID and ' + aggregationConditionAttributeAPIName;
            SnTDMLSecurityUtil.printDebugMessage('======= '+ aggregationConditionAttributeAPIName);
            
        }
        else{
            theQuery = 'select '+ objectToBeQueried + ', ' + aggregationAttributeAPIName + ' from ' +
            aggregationObjectName + ' where AxtriaSalesIQTM__team_instance__c =:teamInstanceID ';
        }
        SnTDMLSecurityUtil.printDebugMessage('the Query '+ theQuery);              
    }

    //Added by HT(A0994) on 2nd June 2020
    global BatchCreateCallPlanCIMPosMetrics(List<String> list_cim, String teamInst,String sourceTeamInstance) 
    {
        teamInstanceID  = teamInst;
        sourceTeamInst = sourceTeamInstance;
        destTeamInst = teamInst;
        
        cimConfigId = list_cim[0];
        list_cimID = list_cim;
        list_cimID.remove(0); 
        mapAggregate = new Map<String,Decimal>();
        mapCount = new Map<String,Decimal>();
        mapSum = new Map<String,Decimal>();
        mapAggregateSet = new Map<String,Set<String>>();
        AxtriaSalesIQTM__CIM_Config__c cim = new AxtriaSalesIQTM__CIM_Config__c();

        cim = [select id, name, AxtriaSalesIQTM__Object_Name__c, AxtriaSalesIQTM__Attribute_API_Name__c, 
                AxtriaSalesIQTM__Aggregation_Type__c, AxtriaSalesIQTM__Aggregation_Object_Name__c, 
                AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c, 
                AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c,
                AxtriaSalesIQTM__Aggregation_Condition_Attribute_Value__c  
                from AxtriaSalesIQTM__CIM_Config__c where id=:cimConfigId and 
                AxtriaSalesIQTM__team_instance__c =:teamInstanceID and 
                AxtriaSalesIQTM__Aggregation_Object_Name__c != null and 
                AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c!= null WITH SECURITY_ENFORCED limit 1];

        objectName = cim.AxtriaSalesIQTM__Object_Name__c;
        attributeAPIname = cim.AxtriaSalesIQTM__Attribute_API_Name__c;
        aggregationType = cim.AxtriaSalesIQTM__Aggregation_Type__c.toUpperCase();
        aggregationObjectName = cim.AxtriaSalesIQTM__Aggregation_Object_Name__c;
        aggregationAttributeAPIName = cim.AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c;
        aggregationConditionAttributeAPIName = cim.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c;
        
        if(objectName != aggregationObjectName){
            objectToBeQueried =  objectName.removeEnd('c');
            objectToBeQueried = objectToBeQueried + 'r.'+attributeAPIname;            
        }
        else{
            objectToBeQueried = attributeAPIname;
            
        } 
        if(aggregationConditionAttributeAPIName != null){
            theQuery = 'select ' + objectToBeQueried + ', ' + aggregationAttributeAPIName + ' from ' +
            aggregationObjectName + ' where AxtriaSalesIQTM__team_instance__c =:teamInstanceID and ' + aggregationConditionAttributeAPIName;
            SnTDMLSecurityUtil.printDebugMessage('======= '+ aggregationConditionAttributeAPIName);
            
        }
        else{
            theQuery = 'select '+ objectToBeQueried + ', ' + aggregationAttributeAPIName + ' from ' +
            aggregationObjectName + ' where AxtriaSalesIQTM__team_instance__c =:teamInstanceID ';
        }
        SnTDMLSecurityUtil.printDebugMessage('the Query '+ theQuery);              
    }

    global BatchCreateCallPlanCIMPosMetrics(List<String> list_cim, String teamInst) 
    {
        teamInstanceID  = teamInst;
        
        cimConfigId = list_cim[0];
        list_cimID = list_cim;
        list_cimID.remove(0); 
        mapAggregate = new Map<String,Decimal>();
        mapCount = new Map<String,Decimal>();
        mapSum = new Map<String,Decimal>();
        mapAggregateSet = new Map<String,Set<String>>();
        AxtriaSalesIQTM__CIM_Config__c cim = new AxtriaSalesIQTM__CIM_Config__c();

        cim = [select id, name, AxtriaSalesIQTM__Object_Name__c, AxtriaSalesIQTM__Attribute_API_Name__c, 
                AxtriaSalesIQTM__Aggregation_Type__c, AxtriaSalesIQTM__Aggregation_Object_Name__c, 
                AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c, 
                AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c,
                AxtriaSalesIQTM__Aggregation_Condition_Attribute_Value__c  
                from AxtriaSalesIQTM__CIM_Config__c where id=:cimConfigId and 
                AxtriaSalesIQTM__team_instance__c =:teamInstanceID and 
                AxtriaSalesIQTM__Aggregation_Object_Name__c != null and 
                AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c!= null WITH SECURITY_ENFORCED limit 1];

        objectName = cim.AxtriaSalesIQTM__Object_Name__c;
        attributeAPIname = cim.AxtriaSalesIQTM__Attribute_API_Name__c;
        aggregationType = cim.AxtriaSalesIQTM__Aggregation_Type__c.toUpperCase();
        aggregationObjectName = cim.AxtriaSalesIQTM__Aggregation_Object_Name__c;
        aggregationAttributeAPIName = cim.AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c;
        aggregationConditionAttributeAPIName = cim.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c;
        
        if(objectName != aggregationObjectName){
            objectToBeQueried =  objectName.removeEnd('c');
            objectToBeQueried = objectToBeQueried + 'r.'+attributeAPIname;            
        }
        else{
            objectToBeQueried = attributeAPIname;
            
        } 
        if(aggregationConditionAttributeAPIName != null){
            theQuery = 'select ' + objectToBeQueried + ', ' + aggregationAttributeAPIName + ' from ' +
            aggregationObjectName + ' where AxtriaSalesIQTM__team_instance__c =:teamInstanceID and ' + aggregationConditionAttributeAPIName;
            SnTDMLSecurityUtil.printDebugMessage('======= '+ aggregationConditionAttributeAPIName);
            
        }
        else{
            theQuery = 'select '+ objectToBeQueried + ', ' + aggregationAttributeAPIName + ' from ' +
            aggregationObjectName + ' where AxtriaSalesIQTM__team_instance__c =:teamInstanceID ';
        }
        SnTDMLSecurityUtil.printDebugMessage('the Query '+ theQuery);              
    }

    global Database.QueryLocator start(Database.BatchableContext bc) 
    {    
        return Database.getQueryLocator(theQuery);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) 
    {
        try
        {
            String key,value_string;
            Decimal value=0,agg_val=0,agg_count=0,agg_sum=0;
            Set<String> tempSet = new Set<String>();
            
            Integer i = 0,j=scope.size();
            
            for(i=0;i<j;i++){
                key = String.valueOf(scope[i].get(aggregationAttributeAPIName));
                if(objectToBeQueried.contains('__r')){
                    SnTDMLSecurityUtil.printDebugMessage('objectToBeQueried.split=='+objectToBeQueried.split('\\.'));
                    value_string = String.valueOf(scope[i].getsObject(objectToBeQueried.split('\\.')[0]).get(objectToBeQueried.split('\\.')[1]));
                }
                else{
                    value_string = String.valueOf(scope[i].get(objectToBeQueried));
                }
                
                value = value_string == null ? 0 : Decimal.valueOf(isNumber(value_string));
                if(!mapAggregate.containsKey(key)){

                    mapAggregate.put(key,value);
                    mapCount.put(key,1);
                    mapSum.put(key,value);
                    tempSet = new Set<String>();
                }
                else{
                    tempSet = mapAggregateSet.get(key);
                    tempSet.add(value_string);

                    agg_val = mapAggregate.get(key);
                    agg_count = mapCount.get(key);
                    agg_sum = mapSum.get(key);

                    mapCount.put(key,agg_count+1);
                    mapSum.put(key,agg_sum+value);

                    if(aggregationType == 'SUM'){   //to upper case 
                        agg_val+= value;
                    }
                    else if(aggregationType == 'MAX'){
                        agg_val = Math.max(agg_val, value);
                    }
                    else if(aggregationType == 'MIN'){
                        agg_val = Math.min(agg_val, value);
                    }
                    else if(aggregationType == 'COUNT'){
                        agg_val = mapCount.get(key);
                    }
                    else if(aggregationType == 'AVG'){
                        agg_val= mapSum.get(key)/mapCount.get(key);
                    }
                    else if(aggregationType == 'COUNT_DISTINCT'){
                        agg_val= tempSet.size();
                    }
                                
                    mapAggregate.put(key,agg_val);
                }
                tempSet.add(value_string);
                mapAggregateSet.put(key,tempSet);
            }
        }
        catch(Exception e)
        {
            SnTDMLSecurityUtil.printDebugMessage('Error in '+batchName+' : execute()-->'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack Trace-->'+e.getStackTraceString());
            proceedNextBatch = false;

            SnTDMLSecurityUtil.printDebugMessage('callPlanLoggerID--'+callPlanLoggerID);

            if(callPlanLoggerID!=null && callPlanLoggerID!='')
            {
                SalesIQUtility.updateCallPlanLoggerRecord(callPlanLoggerID,'Failed at copying CIM Position '+
                                                            'Metric Summary data','Error',alignNmsp);
            }
        }
    }

    global void finish(Database.BatchableContext BC) 
    {             
        try
        {
            String originalVal;
            List<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c> updateListPostionMetric= new list<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c>();
            List<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c> existingCIM_Summary = [Select id from AxtriaSalesIQTM__CIM_Position_Metric_Summary__c where AxtriaSalesIQTM__CIM_Config__c = :cimConfigId and AxtriaSalesIQTM__Team_Instance__c = :teamInstanceID WITH SECURITY_ENFORCED];
            if(existingCIM_Summary.size()>0){
                //delete existingCIM_Summary;
                SnTDMLSecurityUtil.deleteRecords(existingCIM_Summary, 'BatchCreateCallPlanCIMPosMetrics');
            }


            for(AxtriaSalesIQTM__Position_Team_Instance__c pti :  [select ID from AxtriaSalesIQTM__position_team_instance__c 
                                                                    where AxtriaSalesIQTM__Team_Instance_ID__c=:teamInstanceID 
                                                                    WITH SECURITY_ENFORCED])
            {    
                originalVal = '0';
                if(mapAggregate.containsKey(pti.ID)){
                    originalVal = String.valueOf(mapAggregate.get(pti.ID));
                    if(originalVal == null)
                        originalVal = '0';
                    
                }
                AxtriaSalesIQTM__CIM_Position_Metric_Summary__c objCIMPositionMetric = new AxtriaSalesIQTM__CIM_Position_Metric_Summary__c(AxtriaSalesIQTM__CIM_Config__c = cimConfigId, AxtriaSalesIQTM__Original__c = originalVal, AxtriaSalesIQTM__Team_Instance__c = teamInstanceID, AxtriaSalesIQTM__Proposed__c = originalVal, AxtriaSalesIQTM__Approved__c = originalVal, AxtriaSalesIQTM__Position_Team_Instance__c = pti.Id);
                updateListPostionMetric.add(objCIMPositionMetric);
            }
            if(updateListPostionMetric.size()>0){
                //insert updateListPostionMetric;
                SnTDMLSecurityUtil.insertRecords(updateListPostionMetric, 'BatchCreateCallPlanCIMPosMetrics');
            }

            //Else clause added by HT(A0994) on 2nd June 2020 for chaining functions
            if(list_cimID.size()>0)
            {
                if(destTeamInst!=null && sourceTeamInst!=null && callPlanLoggerID!=null)
                    Database.executeBatch(new BatchCreateCallPlanCIMPosMetrics(list_cimID,destTeamInst,sourceTeamInst,callPlanLoggerID),2000);
                else if(destTeamInst!=null && sourceTeamInst!=null)
                    Database.executeBatch(new BatchCreateCallPlanCIMPosMetrics(list_cimID,destTeamInst,sourceTeamInst),2000);
                else
                    Database.executeBatch(new BatchCreateCallPlanCIMPosMetrics(list_cimID,destTeamInst),2000);
            }
            else
            {
                if(destTeamInst!=null && sourceTeamInst!=null)
                {
                    CopyCallPlanScenarioTriggerHandler copy = new CopyCallPlanScenarioTriggerHandler(sourceTeamInst,destTeamInst,callPlanLoggerID);
                    copy.copyDependencyControl();
                }
            }
        }
        catch(Exception e)
        {
            SnTDMLSecurityUtil.printDebugMessage('Error in '+batchName+' : finish()-->'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack Trace-->'+e.getStackTraceString());
            proceedNextBatch = false;

            SnTDMLSecurityUtil.printDebugMessage('callPlanLoggerID--'+callPlanLoggerID);

            if(callPlanLoggerID!=null && callPlanLoggerID!='')
            {
                SalesIQUtility.updateCallPlanLoggerRecord(callPlanLoggerID,'Failed at copying CIM Position '+
                                                            'Metric Summary data','Error',alignNmsp);
            }
        }
    }

    public String isNumber(String str){
        try{
            Decimal i = Decimal.valueOf(str);
            return str;
        }
        catch(Exception e){
            return '0';
        }
    }
}