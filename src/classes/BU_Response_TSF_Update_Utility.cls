/**
Name        :   BU_Response_TSF_Update_Utility
Author      :   Ritwik Shokeen
Date        :   05/10/2018
Description :   BU_Response_TSF_Update_Utility
*/
global class BU_Response_TSF_Update_Utility implements Database.Batchable<sObject>,Database.Stateful {
    public String query;
    public String selectedTeamInstance;
    public String selectedTeam;
    //public AxtriaSalesIQTM__Team_Instance__c notSelectedTeamInstance;
    Map<String,Id> teamInstanceNametoIDMap;
    Map<String,string> teamInstanceNametoTeamNameMap;
    Map<String,string> teamInstanceNametoAZCycleMap;
    Map<String,string> teamInstanceNametoAZCycleNameMap;
    Map<String,string> teamInstanceNametoTeamIDMap;
    Map<String,string> teamInstanceNametoBUMap;
    Map<String,string> brandIDteamInstanceNametoBrandTeamInstIDMap;
    //public Map<String,BU_Response__c> questionToObjectMap;
    //public Map<String,Integer> questionToResponseFieldNoMap;
    public String accountfieldname;
    public String questionname;
    //public Integer availableRespField;
    public Map<String,Integer> productToavailableRespFieldMap;
    public Integer GlobalCounter = 1;
    Map<string,id> allTIPositionCodesToIDMap;
    Map<string,Integer> TSFAccNoTerrCodeToAccessibilityMap;
    
    global BU_Response_TSF_Update_Utility(String teamInstance) {//,String accfieldname,String qname
        questionname='Accessibility';
        //availableRespField=10;//Should start from 1 then ++ //Currently Manually at Response10__c
        
        allTIPositionCodesToIDMap = new Map<string,id>();
        List<AxtriaSalesIQTM__Position__c> posList = new List<AxtriaSalesIQTM__Position__c>([select id,AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_instance__r.name from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_instance__r.name = :teamInstance]);
        
        for (AxtriaSalesIQTM__Position__c p: posList){
            allTIPositionCodesToIDMap.put(p.AxtriaSalesIQTM__Client_Position_Code__c,p.id);
        }
        
        teamInstanceNametoIDMap = new Map<String,Id>();
        teamInstanceNametoTeamNameMap = new Map<String,String>();
        teamInstanceNametoAZCycleMap = new Map<String,String>();
        teamInstanceNametoAZCycleNameMap = new Map<String,String>();
        teamInstanceNametoTeamIDMap = new Map<String,String>();
        teamInstanceNametoBUMap = new Map<String,String>();
        brandIDteamInstanceNametoBrandTeamInstIDMap = new Map<String,String>();
        productToavailableRespFieldMap = new Map<String,Integer>();
        //questionToObjectMap = new Map<String,Staging_Cust_Survey_Profiling__c>();
        //questionToResponseFieldNoMap = new Map<String,Integer>();
        selectedTeamInstance = teamInstance;
        GlobalCounter = 1;
        
        for(AxtriaSalesIQTM__Team_Instance__c teamIns : [Select Id,Name,AxtriaSalesIQTM__Team__c,/*AxtriaSalesIQTM__Team__r.Business_Unit_Loopup__c*/AxtriaSalesIQTM__Team__r.name,Cycle__c,Cycle__r.name From AxtriaSalesIQTM__Team_Instance__c]){
            teamInstanceNametoIDMap.put(teamIns.Name,teamIns.Id);
            teamInstanceNametoTeamNameMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__r.name);
            teamInstanceNametoAZCycleMap.put(teamIns.Name,teamIns.Cycle__c);
            teamInstanceNametoAZCycleNameMap.put(teamIns.Name,teamIns.Cycle__r.name);
            teamInstanceNametoTeamIDMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__c);
            //teamInstanceNametoBUMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__r.Business_Unit_Loopup__c);
        }
        
        /*for(Brand_Team_Instance__c bti :[select id,Brand__c,Brand__r.name,Brand__r.Veeva_External_ID__c,Team_Instance__c,Team_Instance__r.name from Brand_Team_Instance__c]){
            brandIDteamInstanceNametoBrandTeamInstIDMap.put(bti.Brand__r.Veeva_External_ID__c+bti.Brand__r.name+bti.Team_Instance__r.name,bti.id);
        }*/
        for(Product_Catalog__c bti :[select id,name,Veeva_External_ID__c,Team_Instance__c,Team_Instance__r.name from Product_Catalog__c]){
            brandIDteamInstanceNametoBrandTeamInstIDMap.put(bti.Veeva_External_ID__c+bti.name+bti.Team_Instance__r.name,bti.id);
        }
        
        selectedTeam = teamInstanceNametoTeamNameMap.get(selectedTeamInstance);
        
        
        
        
        List<Product_Catalog__c> pcList = new List<Product_Catalog__c>();
        pcList = [select id,name,External_ID__c from Product_Catalog__c where Team_Instance__c = :teamInstanceNametoIDMap.get(teamInstance) and IsActive__c=true];//and IsActive__c = true
        
        for(Product_Catalog__c pcRec1 : pcList)
        {
            productToavailableRespFieldMap.put(pcRec1.External_ID__c+pcRec1.name,10);
        }
        
        
        List<Parameter__c> parameterList = new List<Parameter__c>();
        List<MetaData_Definition__c> metaDataDefinitionList = new List<MetaData_Definition__c>();
        List<Survey_Definition__c> surveyDefinitionList = new List<Survey_Definition__c>();
        Parameter__c tempParameterRec = new Parameter__c();
        MetaData_Definition__c tempMetaDataDefinitionRec = new MetaData_Definition__c();
        Survey_Definition__c tempsurveyDefinitionRec = new Survey_Definition__c();
        
        Map<string,Parameter__c> parameterAlreadyExistList = new Map<string,Parameter__c>();
        for(Parameter__c p : [select id,Name__c,Product_Catalog__r.External_ID__c,Product_Catalog__r.name, Team_Instance__c from Parameter__c]){
            parameterAlreadyExistList.put(p.Name__c+p.Product_Catalog__r.External_ID__c+p.Product_Catalog__r.name+p.Team_Instance__c,p);
        }
        
        Map<string,MetaData_Definition__c> metaDataDefinitionAlreadyExistList = new Map<string,MetaData_Definition__c>();
        for(MetaData_Definition__c mdd : [select id,Display_Name__c,Team_Instance__c,Team_Instance__r.name,Product_Catalog__r.External_ID__c,Product_Catalog__r.name from MetaData_Definition__c where Sequence__c != 10 ]){//and  Sequence__c != 9
            metaDataDefinitionAlreadyExistList.put(mdd.Display_Name__c+mdd.Product_Catalog__r.External_ID__c+mdd.Product_Catalog__r.name+mdd.Team_Instance__c,mdd);
            if(mdd.Team_Instance__r.name == teamInstance){
                //availableRespField++;
                if(!productToavailableRespFieldMap.containsKey(mdd.Product_Catalog__r.External_ID__c+mdd.Product_Catalog__r.name)){
                    productToavailableRespFieldMap.put(mdd.Product_Catalog__r.External_ID__c+mdd.Product_Catalog__r.name,10);
                }
                else{
                    productToavailableRespFieldMap.put(mdd.Product_Catalog__r.External_ID__c+mdd.Product_Catalog__r.name,10);//productToavailableRespFieldMap.get(mdd.Product_Catalog__r.External_ID__c+mdd.Product_Catalog__r.name)+1
                }
            }
        }
        
        Map<string,Survey_Definition__c> surveyDefinitionAlreadyExistList = new Map<string,Survey_Definition__c>();
        for(Survey_Definition__c sd : [select id,name,Brand_ID__c,Brand_Name__c,Question_Short_Text__c,Team_Instance__c,Cycle__c from Survey_Definition__c]){
            surveyDefinitionAlreadyExistList.put(sd.Brand_ID__c+sd.Brand_Name__c+sd.Question_Short_Text__c+sd.Team_Instance__c,sd);
        }
        //System.debug('questionToObjectMap+++++'+questionToObjectMap);
        //System.debug('questionToResponseFieldNoMap+++++'+questionToResponseFieldNoMap);
        
        //Integer counter1 = 1;
        //Integer counter2 = 1;
        /*if(!parameterAlreadyExistList.containsKey(questionname + teamInstanceNametoIDMap.get(teamInstance))){
            tempParameterRec = new Parameter__c();
            
            tempParameterRec.Name = questionname;
            tempParameterRec.isActive__c = true;
            tempParameterRec.Team_Instance__c = teamInstanceNametoIDMap.get(teamInstance);
            tempParameterRec.Team__c = teamInstanceNametoTeamIDMap.get(teamInstance);
            tempParameterRec.Business_Unit__c = teamInstanceNametoBUMap.get(teamInstance);
            tempParameterRec.Type__c = 'Text'; // ??
            tempParameterRec.CurrencyIsoCode = 'EUR';
            
            parameterList.add(tempParameterRec);
        }*/
        
        
        for(Product_Catalog__c pcRec : pcList)
        {
            
            if(!parameterAlreadyExistList.containsKey(questionname + pcRec.External_ID__c + pcRec.name + teamInstanceNametoIDMap.get(teamInstance))){
                tempParameterRec = new Parameter__c();
                
                tempParameterRec.Name__c = questionname;
                tempParameterRec.isActive__c = true;
                tempParameterRec.Team_Instance__c = teamInstanceNametoIDMap.get(teamInstance);
                tempParameterRec.Team__c = teamInstanceNametoTeamIDMap.get(teamInstance);
                //tempParameterRec.Business_Unit__c = teamInstanceNametoBUMap.get(teamInstance);
                tempParameterRec.Type__c = 'Text'; // ??
                //tempParameterRec.CurrencyIsoCode = 'EUR';
                tempMetaDataDefinitionRec.Product_Catalog__c = brandIDteamInstanceNametoBrandTeamInstIDMap.get(pcRec.External_ID__c + pcRec.name + teamInstance);  //??
                
                parameterList.add(tempParameterRec);
            }
        
            if(!metaDataDefinitionAlreadyExistList.containsKey(questionname + pcRec.External_ID__c + pcRec.name + teamInstanceNametoIDMap.get(teamInstance))){
                tempMetaDataDefinitionRec = new MetaData_Definition__c();
                
                tempMetaDataDefinitionRec.Source_Object__c = 'BU_Response__c';
                tempMetaDataDefinitionRec.Type__c = 'Text'; // ??
                tempMetaDataDefinitionRec.Sequence__c = productToavailableRespFieldMap.get(pcRec.External_ID__c+pcRec.name);//availableRespField;//Integer.ValueOf(questionToObjectMap.get(qtext).QUESTION_ID__c)+1; //counter1;
                tempMetaDataDefinitionRec.Source_Field__c = 'Response'+String.ValueOf(productToavailableRespFieldMap.get(pcRec.External_ID__c+pcRec.name))+'__c';//String.ValueOf(Integer.ValueOf(questionToObjectMap.get(qtext).QUESTION_ID__c)+1)//String.valueOf(counter1)
                tempMetaDataDefinitionRec.Display_Name__c = questionname;
                tempMetaDataDefinitionRec.Team_Instance__c = teamInstanceNametoIDMap.get(teamInstance);
                tempMetaDataDefinitionRec.Team__c = teamInstanceNametoTeamIDMap.get(teamInstance);
                //tempMetaDataDefinitionRec.CurrencyIsoCode = 'EUR';
                tempMetaDataDefinitionRec.Product_Catalog__c = brandIDteamInstanceNametoBrandTeamInstIDMap.get(pcRec.External_ID__c + pcRec.name + teamInstance);  //??
                //counter1++;
                
                System.debug('pcRec.External_ID__c + teamInstance  +++++'+pcRec.External_ID__c+ pcRec.name + teamInstance);
                metaDataDefinitionList.add(tempMetaDataDefinitionRec);
            }
            
            if(!surveyDefinitionAlreadyExistList.containsKey(pcRec.External_ID__c+ pcRec.name + questionname + teamInstanceNametoIDMap.get(teamInstance))){
                tempsurveyDefinitionRec = new Survey_Definition__c();
                
                tempsurveyDefinitionRec.Brand_ID__c = pcRec.External_ID__c;
                tempsurveyDefinitionRec.Brand_Name__c = pcRec.name;
                tempsurveyDefinitionRec.Cycle__c = teamInstanceNametoAZCycleMap.get(teamInstance);
                tempsurveyDefinitionRec.Cycle_ID__c = teamInstanceNametoAZCycleNameMap.get(teamInstance);
                tempsurveyDefinitionRec.Cycle_Name__c = teamInstanceNametoAZCycleNameMap.get(teamInstance);
                /*tempsurveyDefinitionRec.Cycle2__c = ; //??
                tempsurveyDefinitionRec.MetaData_Input__c = ''; //??*/
                tempsurveyDefinitionRec.Question_ID__c = String.valueOf(productToavailableRespFieldMap.get(pcRec.External_ID__c+pcRec.name));//String.valueOf(Integer.ValueOf(questionToObjectMap.get(qtext).QUESTION_ID__c)+1);//String.valueOf(counter2);//
                tempsurveyDefinitionRec.Question_Short_Text__c = questionname;
                tempsurveyDefinitionRec.Question_Text__c = questionname;
                /*tempsurveyDefinitionRec.Read_Only__c = ; //??
                tempsurveyDefinitionRec.Response_Options__c = ; //??*/
                tempsurveyDefinitionRec.Survey_ID__c = 'Axtria Internal';//questionToObjectMap.get(qtext).Survey_ID__c;
                tempsurveyDefinitionRec.Survey_Name__c = 'Axtria Internal';//questionToObjectMap.get(qtext).Survey_Name__c;
                tempsurveyDefinitionRec.Team_Instance__c = teamInstanceNametoIDMap.get(teamInstance); //??
                //tempsurveyDefinitionRec.CurrencyIsoCode = 'EUR';
                //counter2++;
                
                surveyDefinitionList.add(tempsurveyDefinitionRec);
            }
        }
        
        System.debug('parameterList+++++++++++'+parameterList);
        System.debug('metaDataDefinitionList+++++++++++'+metaDataDefinitionList);
        
        insert parameterList;
        insert metaDataDefinitionList;
        insert surveyDefinitionList;
        
        //query = 'Select Id, Name, CurrencyIsoCode, SIQ_Veeva_Accessibility_AZ_EU__c, SIQ_Veeva_Account_vod__c, SIQ_Veeva_Territory_vod__c, SIQ_Veeva_External_ID__c FROM SIQ_Veeva_TSF_vod_Inbound__c where SIQ_Veeva_Territory_vod__c in :allTIPositionCodesToIDMap.keyset() ';//CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, LastActivityDate, 
        
        List<SIQ_Veeva_TSF_vod_Inbound__c> TSFvodList = new List<SIQ_Veeva_TSF_vod_Inbound__c>([Select Id, Name, CurrencyIsoCode, SIQ_Veeva_Accessibility_AZ_EU__c, SIQ_Veeva_Account_vod__c, SIQ_Veeva_Territory_vod__c, SIQ_Veeva_External_ID__c FROM SIQ_Veeva_TSF_vod_Inbound__c where SIQ_Veeva_Territory_vod__c in :allTIPositionCodesToIDMap.keyset()]);
        
        for (SIQ_Veeva_TSF_vod_Inbound__c TSFvodRec : TSFvodList){
            if(TSFvodRec.SIQ_Veeva_Accessibility_AZ_EU__c!=null && TSFvodRec.SIQ_Veeva_Account_vod__c!=null && TSFvodRec.SIQ_Veeva_Territory_vod__c!=null){
                TSFAccNoTerrCodeToAccessibilityMap.put(String.valueOf(TSFvodRec.SIQ_Veeva_Account_vod__c)+String.valueOf(TSFvodRec.SIQ_Veeva_Territory_vod__c),Integer.Valueof(TSFvodRec.SIQ_Veeva_Accessibility_AZ_EU__c));
            }
        }
        
        //Removed BU and Profiling_Response__c
        query = 'Select Id, Name, CurrencyIsoCode, Acc_Pos__c,  Brand_ID__c, Product__c, Cycle2__c, Cycle__c, External_ID__c, Is_Active__c,  Physician__c, Physician__r.AccountNumber, Position_Account__c, Position_Account__r.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, Position__c, Response10__c, Response1__c, Response2__c, Response3__c, Response4__c, Response5__c, Response6__c, Response7__c, Response8__c, Response9__c, Team_Instance__c, Team_Instance__r.name, Team__c, is_inserted__c, is_updated__c FROM BU_Response__c  Where Team_Instance__r.name = \'' + teamInstance + '\'';
        //Where Team_Instance__r.name = \'' + teamInstance + '\'';
        
        /*query = 'Select Id, Name, CurrencyIsoCode, Acc_Pos__c, BU__c, Brand_ID__c, Brand__c,Brand__r.Brand__c, Business_Unit__c, Cycle2__c, Cycle__c, External_ID__c, Is_Active__c, LineFormula__c, Line__c, Physician__c, Physician__r.AccountNumber, ';
        if(accfieldname!=null){
            query += 'Physician__r.'+accfieldname+' ,';
            accountfieldname=accfieldname;
        }
        else{
            query += 'Physician__r.AxtriaSalesIQTM__Speciality__c ,';
            accountfieldname='AxtriaSalesIQTM__Speciality__c';
        }
        query += ' Position_Account__c, Position__c, Profiling_Response__c, Response10__c, Response1__c, Response2__c, Response3__c, Response4__c, Response5__c, Response6__c, Response7__c, Response8__c, Response9__c, Survey_Master__c, Team_Instance__c, Team_Instance__r.name, Team__c, is_inserted__c, is_updated__c FROM BU_Response__c  Where Team_Instance__r.name = \'' + teamInstance + '\'';
        */
        
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        system.debug('query+++++++++++++   ' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<BU_Response__c> scopeRecs) {
        
        system.debug('Hello++++++++++++++++++++++++++++');
        
        //List<Survey_Response__c> surveyResponseList = new List<Survey_Response__c>();//commented due to object purge activity A1450
        //Survey_Response__c tempSurveyResponseRec = new Survey_Response__c();//commented due to object purge activity A1450
        
        
        for(BU_Response__c BUresp : scopeRecs){
            //tempSurveyResponseRec = new Survey_Response__c();//commented due to object purge activity A1450
            
            //commented due to object purge activity A1450
            //tempSurveyResponseRec.Question_ID__c = productToavailableRespFieldMap.get(BUresp.Product__r.External_ID__c+BUresp.Product__r.name);
            //tempSurveyResponseRec.Question_Short_Text__c = questionname;
            //if(TSFAccNoTerrCodeToAccessibilityMap.containsKey(BUresp.Physician__r.AccountNumber+BUresp.Position_Account__r.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c)){
            //    tempSurveyResponseRec.Response__c = String.valueOf(TSFAccNoTerrCodeToAccessibilityMap.get(BUresp.Physician__r.AccountNumber+BUresp.Position_Account__r.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c));
            //}
            //else{
            //    tempSurveyResponseRec.Response__c = '999';
            //}
            //commented due to object purge activity A1450
            // tempSurveyResponseRec.name = 'Axtria Internal';
            // tempSurveyResponseRec.Team_Instance__c = BUresp.Team_Instance__c;
            // tempSurveyResponseRec.Product__c = BUresp.Brand__r.Brand__c;
            // tempSurveyResponseRec.Physician__c = BUresp.Physician__c;
            

            //tempSurveyResponseRec.MetaData_Input__c // commented earlier


            // tempSurveyResponseRec.Cycle__c=teamInstanceNametoAZCycleMap.get(BUresp.Team_Instance__r.name);//commented due to object purge activity A1450
            // tempSurveyResponseRec.Cycle_ID__c = teamInstanceNametoAZCycleNameMap.get(BUresp.Team_Instance__r.name);//commented due to object purge activity A1450
            
            
            //BUresp.id=ToBURespIDMap.get(TSF_vodRecs.);//commented Earlier
            if(productToavailableRespFieldMap.get(BUresp.Product__r.External_ID__c+BUresp.Product__r.name) == 1){
                if(TSFAccNoTerrCodeToAccessibilityMap.containsKey(BUresp.Physician__r.AccountNumber+BUresp.Position_Account__r.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c)){
                    BUresp.Response1__c = String.valueOf(TSFAccNoTerrCodeToAccessibilityMap.get(BUresp.Physician__r.AccountNumber+BUresp.Position_Account__r.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c));
                }
                else{
                    BUresp.Response1__c = '999';
                }
            }
            else if(productToavailableRespFieldMap.get(BUresp.Product__r.External_ID__c+BUresp.Product__r.name) == 2){
                if(TSFAccNoTerrCodeToAccessibilityMap.containsKey(BUresp.Physician__r.AccountNumber+BUresp.Position_Account__r.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c)){
                    BUresp.Response2__c = String.valueOf(TSFAccNoTerrCodeToAccessibilityMap.get(BUresp.Physician__r.AccountNumber+BUresp.Position_Account__r.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c));
                }
                else{
                    BUresp.Response2__c = '999';
                }
            }
            else if(productToavailableRespFieldMap.get(BUresp.Product__r.External_ID__c+BUresp.Product__r.name) == 3){
                if(TSFAccNoTerrCodeToAccessibilityMap.containsKey(BUresp.Physician__r.AccountNumber+BUresp.Position_Account__r.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c)){
                    BUresp.Response3__c = String.valueOf(TSFAccNoTerrCodeToAccessibilityMap.get(BUresp.Physician__r.AccountNumber+BUresp.Position_Account__r.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c));
                }
                else{
                    BUresp.Response3__c = '999';
                }
            }
            else if(productToavailableRespFieldMap.get(BUresp.Product__r.External_ID__c+BUresp.Product__r.name) == 4){
                if(TSFAccNoTerrCodeToAccessibilityMap.containsKey(BUresp.Physician__r.AccountNumber+BUresp.Position_Account__r.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c)){
                    BUresp.Response4__c = String.valueOf(TSFAccNoTerrCodeToAccessibilityMap.get(BUresp.Physician__r.AccountNumber+BUresp.Position_Account__r.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c));
                }
                else{
                    BUresp.Response4__c = '999';
                }
            }
            else if(productToavailableRespFieldMap.get(BUresp.Product__r.External_ID__c+BUresp.Product__r.name) == 5){
                if(TSFAccNoTerrCodeToAccessibilityMap.containsKey(BUresp.Physician__r.AccountNumber+BUresp.Position_Account__r.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c)){
                    BUresp.Response5__c = String.valueOf(TSFAccNoTerrCodeToAccessibilityMap.get(BUresp.Physician__r.AccountNumber+BUresp.Position_Account__r.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c));
                }
                else{
                    BUresp.Response5__c = '999';
                }
            }
            else if(productToavailableRespFieldMap.get(BUresp.Product__r.External_ID__c+BUresp.Product__r.name) == 6){
                if(TSFAccNoTerrCodeToAccessibilityMap.containsKey(BUresp.Physician__r.AccountNumber+BUresp.Position_Account__r.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c)){
                    BUresp.Response6__c = String.valueOf(TSFAccNoTerrCodeToAccessibilityMap.get(BUresp.Physician__r.AccountNumber+BUresp.Position_Account__r.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c));
                }
                else{
                    BUresp.Response6__c = '999';
                }
            }
            else if(productToavailableRespFieldMap.get(BUresp.Product__r.External_ID__c+BUresp.Product__r.name) == 7){
                if(TSFAccNoTerrCodeToAccessibilityMap.containsKey(BUresp.Physician__r.AccountNumber+BUresp.Position_Account__r.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c)){
                    BUresp.Response7__c = String.valueOf(TSFAccNoTerrCodeToAccessibilityMap.get(BUresp.Physician__r.AccountNumber+BUresp.Position_Account__r.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c));
                }
                else{
                    BUresp.Response7__c = '999';
                }
            }
            else if(productToavailableRespFieldMap.get(BUresp.Product__r.External_ID__c+BUresp.Product__r.name) == 8){
                if(TSFAccNoTerrCodeToAccessibilityMap.containsKey(BUresp.Physician__r.AccountNumber+BUresp.Position_Account__r.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c)){
                    BUresp.Response8__c = String.valueOf(TSFAccNoTerrCodeToAccessibilityMap.get(BUresp.Physician__r.AccountNumber+BUresp.Position_Account__r.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c));
                }
                else{
                    BUresp.Response8__c = '999';
                }
            }
            else if(productToavailableRespFieldMap.get(BUresp.Product__r.External_ID__c+BUresp.Product__r.name) == 9){
                if(TSFAccNoTerrCodeToAccessibilityMap.containsKey(BUresp.Physician__r.AccountNumber+BUresp.Position_Account__r.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c)){
                    BUresp.Response9__c = String.valueOf(TSFAccNoTerrCodeToAccessibilityMap.get(BUresp.Physician__r.AccountNumber+BUresp.Position_Account__r.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c));
                }
                else{
                    BUresp.Response9__c = '999';
                }
            }
            else if(productToavailableRespFieldMap.get(BUresp.Product__r.External_ID__c+BUresp.Product__r.name) == 10){
                if(TSFAccNoTerrCodeToAccessibilityMap.containsKey(BUresp.Physician__r.AccountNumber+BUresp.Position_Account__r.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c)){
                    BUresp.Response10__c = String.valueOf(TSFAccNoTerrCodeToAccessibilityMap.get(BUresp.Physician__r.AccountNumber+BUresp.Position_Account__r.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c));
                }
                else{
                    BUresp.Response10__c = '999';
                }
            }
            
            /*else if(productToavailableRespFieldMap.get(BUresp.Product__r.External_ID__c+BUresp.Product__r.name) == 11){
                if(TSFAccNoTerrCodeToAccessibilityMap.containsKey(BUresp.Physician__r.AccountNumber+BUresp.Position_Account__r.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c)){
                    BUresp.Response11__c = String.valueOf(TSFAccNoTerrCodeToAccessibilityMap.get(BUresp.Physician__r.AccountNumber+BUresp.Position_Account__r.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c));
                }
                else{
                    BUresp.Response11__c = '999';
                }
            }
            else if(productToavailableRespFieldMap.get(BUresp.Product__r.External_ID__c+BUresp.Product__r.name) == 12){
                if(TSFAccNoTerrCodeToAccessibilityMap.containsKey(BUresp.Physician__r.AccountNumber+BUresp.Position_Account__r.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c)){
                    BUresp.Response12__c = String.valueOf(TSFAccNoTerrCodeToAccessibilityMap.get(BUresp.Physician__r.AccountNumber+BUresp.Position_Account__r.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c));
                }
                else{
                    BUresp.Response12__c = '999';
                }
            }
            else if(productToavailableRespFieldMap.get(BUresp.Product__r.External_ID__c+BUresp.Product__r.name) == 13){
                if(TSFAccNoTerrCodeToAccessibilityMap.containsKey(BUresp.Physician__r.AccountNumber+BUresp.Position_Account__r.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c)){
                    BUresp.Response13__c = String.valueOf(TSFAccNoTerrCodeToAccessibilityMap.get(BUresp.Physician__r.AccountNumber+BUresp.Position_Account__r.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c));
                }
                else{
                    BUresp.Response13__c = '999';
                }
            }
            else if(productToavailableRespFieldMap.get(BUresp.Product__r.External_ID__c+BUresp.Product__r.name) == 14){
                if(TSFAccNoTerrCodeToAccessibilityMap.containsKey(BUresp.Physician__r.AccountNumber+BUresp.Position_Account__r.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c)){
                    BUresp.Response14__c = String.valueOf(TSFAccNoTerrCodeToAccessibilityMap.get(BUresp.Physician__r.AccountNumber+BUresp.Position_Account__r.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c));
                }
                else{
                    BUresp.Response14__c = '999';
                }
            }
            else if(productToavailableRespFieldMap.get(BUresp.Product__r.External_ID__c+BUresp.Product__r.name) == 15){
                if(TSFAccNoTerrCodeToAccessibilityMap.containsKey(BUresp.Physician__r.AccountNumber+BUresp.Position_Account__r.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c)){
                    BUresp.Response15__c = String.valueOf(TSFAccNoTerrCodeToAccessibilityMap.get(BUresp.Physician__r.AccountNumber+BUresp.Position_Account__r.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c));
                }
                else{
                    BUresp.Response15__c = '999';
                }
            }*/
            
            
            //surveyResponseList.add(tempSurveyResponseRec);//commented due to object purge activity A1450
        }
        
        update scopeRecs;
        //insert surveyResponseList;//commented due to object purge activity A1450

    }

    global void finish(Database.BatchableContext BC) {
        
    }
}