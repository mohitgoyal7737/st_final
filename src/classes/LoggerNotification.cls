global with sharing class LoggerNotification implements Schedulable{
	global list<Scheduler_Log__c>loggerlist{get;set;} 
	//global list<Scheduler_Log__c>loggerList1{get;set;}
	global list<Logger_Email__c> emailIds{get;set;}
    global LoggerNotification() {
    	loggerlist = new list<Scheduler_Log__c>();
    	//loggerList1 = new list<Scheduler_Log__c>();
        fetchdata();
        //senddata();
    }

    global void fetchdata()
    {
    	//system.debug('-----------------INSIDE Fetch DATA----------');
    	loggerlist = [select id,Changes__c,createdDate,Cycle__c,Job_Type__c,Job_Name__c,Job_Status__c,Object_Name__c,No_of_Records_Processed__c,Total_Errors__c from Scheduler_Log__c where createdDate= TODAY and Job_Type__c='Outbound'  ORDER BY Job_Type__c ]; //where createddate=:today()

    	system.debug('=======loggerlist size():='+loggerlist.size());
    	/*if(loggerlist!=null && loggerlist.size() >0)
    	{
    	}*/
    }
    global void sendEmailsInbound()
    {
    		Messaging.SingleEmailMessage emailTobeSent = new Messaging.SingleEmailMessage();
    		Messaging.EmailFileAttachment attachmentPDf = new Messaging.EmailFileAttachment();
    		list<string>emaillist = new list<string>();
    		PageReference PDf ;
    		
			Blob b ;

			try {
				   if(!test.isRunningTest()){
				   		PDF =  Page.Loggerpage;//Replace attachmentPDf with the page you have rendered as PDF
				   		PDF.setRedirect(true);
						b = PDf.getContentAsPDF();
				    	//b = pdf.getContentAsPDF();
				    }
				    if(test.isRunningTest()){
				    	b = Blob.valueOf('Test Email');
				    }
				} catch (VisualforceException e) {
				    b = Blob.valueOf('Error occurred. Not able to convert content to required format');
				}
			attachmentPDf.setFileName('Logger Information');
			attachmentPDf.setBody(b);
			attachmentPDf.setContentType('application/pdf');
			emailTobeSent.setSubject('Today Delta Jobs Logger Info');
			emailIds = Logger_Email__c.getall().values();
			if(emailIds!=null){
				for(Logger_Email__c LEM:emailIds){
					emaillist.add(LEM.Name);
				}
			}
			
			emailTobeSent.setToAddresses(emaillist);
			emailTobeSent.setPlainTextBody('Hi,Please find the Attachment');
			emailTobeSent.setFileAttachments(new Messaging.EmailFileAttachment[] {attachmentPDf});
			Messaging.sendEmail(new Messaging.Email[] {emailTobeSent});
    }

    global void execute(SchedulableContext sc)
    {
       LoggerNotification LN=new LoggerNotification();
       LN.sendEmailsInbound();
    }
   
}