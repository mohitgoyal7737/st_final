global class BatchParentPacpUpdate implements Database.Batchable<sObject> 
{
    public String query;
    public Datetime Lmd;
    List<string> pacpUpdateList;
    set<string> positionset;
    set<string> accountset;
    set<string> teaminstanceset; 
    set<string> parentpacp;
    List<String> allChannels;
    List<String> allTeamInstances;
    String teamInstanceSelected;
	public Boolean chaining = false;
    Set<String> activityLogIDSet;

    global BatchParentPacpUpdate(Date lastModifiedDate,string teamInstanceSelectedTemp,List<String> allChannelsTemp) 
    {
        /*teamInstanceSelected = teamInstanceSelectedTemp;
        lmd= lastModifiedDate;
        query = 'Select id, P1__c, Segment__c, AxtriaSalesIQTM__Account__r.AZ_VeevaID__c , AxtriaSalesIQTM__Account__r.AccountNumber ,Adoption__c, Potential__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c,Country__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c,Accessibility_Range__c, Party_ID__r.Accessibility_Range__c, AxtriaSalesIQTM__Team_Instance__c,Parent_PACP__c  from AxtriaSalesIQTM__Position_Account_Call_Plan__c WHERE LastModifiedDate = Last_N_Days:1 and AxtriaSalesIQTM__Team_Instance__c =teamInstanceSelected';*/
    }

    global BatchParentPacpUpdate(Date lastModifiedDate,List<string> teamInstanceSelectedTemp,List<String> allChannelsTemp) 
    {
         /*allTeamInstances = new List<String>(teamInstanceSelectedTemp);
        lmd= lastModifiedDate;
        query = 'Select id, P1__c, Segment__c, AxtriaSalesIQTM__Account__r.AZ_VeevaID__c , AxtriaSalesIQTM__Account__r.AccountNumber ,Adoption__c, Potential__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c,Country__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c,Accessibility_Range__c, Party_ID__r.Accessibility_Range__c, AxtriaSalesIQTM__Team_Instance__c,Parent_PACP__c  from AxtriaSalesIQTM__Position_Account_Call_Plan__c WHERE LastModifiedDate = Last_N_Days:1 and AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances';
        allChannels = allChannelsTemp;*/
    }
     global BatchParentPacpUpdate(Datetime lastjobDate,string teamInstanceSelectedTemp,List<String> allChannelsTemp) 
    {
        teamInstanceSelected = teamInstanceSelectedTemp;
        Lmd= lastjobDate;
       query = 'Select id, P1__c, Segment__c, AxtriaSalesIQTM__Account__r.AZ_VeevaID__c , AxtriaSalesIQTM__Account__r.AccountNumber ,Adoption__c, Potential__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c,Country__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c,Accessibility_Range__c, Party_ID__r.Accessibility_Range__c, AxtriaSalesIQTM__Team_Instance__c,Parent_PACP__c  from AxtriaSalesIQTM__Position_Account_Call_Plan__c WHERE LastModifiedDate >=: Lmd and AxtriaSalesIQTM__Team_Instance__c =teamInstanceSelected';
    }

    global BatchParentPacpUpdate(Datetime lastjobDate,List<string> teamInstanceSelectedTemp,List<String> allChannelsTemp) 
    {
        allTeamInstances = new List<String>(teamInstanceSelectedTemp);
        Lmd= lastjobDate;
        query = 'Select id, P1__c, Segment__c, AxtriaSalesIQTM__Account__r.AZ_VeevaID__c , AxtriaSalesIQTM__Account__r.AccountNumber ,Adoption__c, Potential__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c,Country__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c,Accessibility_Range__c, Party_ID__r.Accessibility_Range__c, AxtriaSalesIQTM__Team_Instance__c,Parent_PACP__c  from AxtriaSalesIQTM__Position_Account_Call_Plan__c WHERE LastModifiedDate >=: Lmd and AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances';
        allChannels = allChannelsTemp;
    }
    
    global BatchParentPacpUpdate(Datetime lastjobDate,List<string> teamInstanceSelectedTemp,List<String> allChannelsTemp, boolean chain) 
    {
        Chaining = chain;
        allTeamInstances = new List<String>(teamInstanceSelectedTemp);
        Lmd= lastjobDate;
        query = 'Select id, P1__c, Segment__c, AxtriaSalesIQTM__Account__r.AZ_VeevaID__c , AxtriaSalesIQTM__Account__r.AccountNumber ,Adoption__c, Potential__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c,Country__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c,Accessibility_Range__c, Party_ID__r.Accessibility_Range__c, AxtriaSalesIQTM__Team_Instance__c,Parent_PACP__c  from AxtriaSalesIQTM__Position_Account_Call_Plan__c WHERE LastModifiedDate >=: Lmd and AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances';
        allChannels = allChannelsTemp;
    }
    global BatchParentPacpUpdate(Datetime lastjobDate,List<string> teamInstanceSelectedTemp,List<String> allChannelsTemp, boolean chain,Set<String> activityLogSet) 
    {
        Chaining = chain;
        allTeamInstances = new List<String>(teamInstanceSelectedTemp);
        Lmd= lastjobDate;
         activityLogIDSet = new Set<String>();
        activityLogIDSet.addAll(activityLogSet);
        query = 'Select id, P1__c, Segment__c, AxtriaSalesIQTM__Account__r.AZ_VeevaID__c , AxtriaSalesIQTM__Account__r.AccountNumber ,Adoption__c, Potential__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c,Country__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c,Accessibility_Range__c, Party_ID__r.Accessibility_Range__c, AxtriaSalesIQTM__Team_Instance__c,Parent_PACP__c  from AxtriaSalesIQTM__Position_Account_Call_Plan__c WHERE LastModifiedDate >=: Lmd and AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances';
        allChannels = allChannelsTemp;
    }
    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        system.debug('+++query++'+query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scope)
    {
        pacpUpdateList= new List<String>();
        positionset= new Set<String>();
        accountset= new Set<String>();
        teaminstanceset= new Set<String>();
        parentpacp=new Set<String>();
        list<Parent_PACP__c>updatelist = new list<Parent_PACP__c>();

        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c callplan: scope)
        {
            if(!parentpacp.contains(callplan.Parent_PACP__c))
            {
                Parent_PACP__c pp = new Parent_PACP__c(id=callplan.Parent_PACP__c);
                updatelist.add(pp);
                parentpacp.add(callplan.Parent_PACP__c);              
            }

        }
        system.debug('++++updatelist'+updatelist);
        update updatelist;
    }

    global void finish(Database.BatchableContext BC) 
    {
            //lmd=Date.Today();
        if(chaining)
        {
            if(activityLogIDSet != null)
            {
                changeMCTargetDelta u2 = new changeMCTargetDelta(lmd,allTeamInstances,allChannels,true,activityLogIDSet);
                database.executeBatch(u2,2000); 
            }
            else
            {
                changeMCTargetDelta u2 = new changeMCTargetDelta(lmd,allTeamInstances,allChannels,true);
                database.executeBatch(u2,2000);
            }
        }
    }
}