public class CRCallPlan {
    
    AxtriaSalesIQTM__Change_Request__c changeRequest;
    public String selectedPosition;
    public String selectedTeamInstance;
    public List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> allChangedRec;
    public List<phyWrapper> allPhysicians {get;set;}
    public String jsonString {get;set;}
    
    public CRCallPlan (ApexPages.StandardController ctrl)
    {
        allPhysicians = new List<phyWrapper>();
        AxtriaSalesIQTM__Change_Request__c allChangeRequests = (AxtriaSalesIQTM__Change_Request__c)ctrl.getRecord();
        
        changeRequest = [select id, AxtriaSalesIQTM__Account_Moved_Id__c, AxtriaSalesIQTM__Destination_Position__c, AxtriaSalesIQTM__Team_Instance_ID__c from AxtriaSalesIQTM__Change_Request__c where id = :allChangeRequests.ID];
        displayCRrecords();
    }
    
    public void displayCRrecords()
    {
        List<String> accChanged ;
        
        selectedPosition = changeRequest.AxtriaSalesIQTM__Destination_Position__c;
        selectedTeamInstance = changeRequest.AxtriaSalesIQTM__Team_Instance_ID__c;
        
        if((changeRequest.AxtriaSalesIQTM__Account_Moved_Id__c).length() > 0)
            accChanged = changeRequest.AxtriaSalesIQTM__Account_Moved_Id__c.split(','); 
            
        string soql ='select AxtriaSalesIQTM__Account__r.ID,AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__External_Account_Number__c,AxtriaSalesIQTM__isAccountTarget__c,AxtriaSalesIQTM__Account__r.Name,AxtriaSalesIQTM__Metric3__c,AxtriaSalesIQTM__Metric3_Approved__c,Final_TCF__c,AxtriaSalesIQTM__Metric3_Updated__c,AxtriaSalesIQTM__Picklist1_Updated__c,AxtriaSalesIQTM__Picklist1_Segment_Approved__c,AxtriaSalesIQTM__Picklist3_Updated__c,AxtriaSalesIQTM__Picklist3_Segment_Approved__c,AxtriaSalesIQTM__Segment7__c,AxtriaSalesIQTM__Picklist2_Updated__c,AxtriaSalesIQTM__Picklist2_Segment_Approved__c,AxtriaSalesIQTM__Change_Status__c,AxtriaSalesIQTM__isIncludedCallPlan__c,AxtriaSalesIQTM__Segment2__c,AxtriaSalesIQTM__Segment8__c,Final_TCF_Original__c,Final_TCF_Approved__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__External_Account_Number__c in :accChanged and AxtriaSalesIQTM__Position__c =:selectedPosition and AxtriaSalesIQTM__Team_Instance__c =:selectedTeamInstance';
        allChangedRec = Database.query(soql);
        
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : allChangedRec)
        {
            String accName = pacp.AxtriaSalesIQTM__Account__r.Name;
            String finalTCFafter = String.valueof(pacp.Final_TCF__c);
            String finalTCFbefore = String.valueof(pacp.Final_TCF_Original__c);
            String accNo = pacp.AxtriaSalesIQTM__Account__r.AccountNumber;
            String recordStatus = ' '; 
            if(pacp.AxtriaSalesIQTM__isIncludedCallPlan__c == true && pacp.AxtriaSalesIQTM__isAccountTarget__c == false)
            {
                recordStatus = 'Added';
            }
            else if(pacp.AxtriaSalesIQTM__isIncludedCallPlan__c == false && pacp.AxtriaSalesIQTM__isAccountTarget__c == true)
            {
                recordStatus = 'Dropped';
            }
            else if(pacp.Final_TCF__c != pacp.Final_TCF_Original__c)
            {
                recordStatus = 'TCF Changed';
            }
            
            allPhysicians.add(new phyWrapper(accName, accNo, finalTCFafter, finalTCFbefore, recordStatus ) );
            
            jsonString = JSON.serialize(allPhysicians);
        }
        
    }
    
    public class phyWrapper
    {
        public String accountName {get;set;}
        public String accNo {get;set;}
        public String finalTCFafter {get;set;}
        public String finalTCFbefore {get;set;}
        public String recordStatus {get;set;}
        
        public phyWrapper(String accountName,String accNo,String finalTCFafter,String finalTCFbefore,String recordStatus)
        {
            this.accountName = accountName;
            this.accNo = accNo;
            this.finalTCFafter = finalTCFafter;
            this.finalTCFbefore = finalTCFbefore;
            this.recordStatus = recordStatus;
        }
    }
}