global with sharing class update_position_Acc_new implements Database.Batchable<sObject>, Database.Stateful
{
    /*Replaced temp_acc_Terr__c with temp_obj__c due to object purge*/
    global string query;
    global string teamID;
    global string teamInstance;
    String Ids;
    Boolean flag = true;
    
    public integer recordsProcessed;
    
    global list<AxtriaSalesIQTM__Team_Instance__c> teminslst = new list<AxtriaSalesIQTM__Team_Instance__c>();
    global list<AxtriaSalesIQTM__Team_Instance__c> teamins = new list<AxtriaSalesIQTM__Team_Instance__c>();
    global Map<Id, Set<Id>> mapTI2Acc = new Map<Id, Set<Id>>();
    global Map<Id, Set<Id>> mapTI2id = new Map<Id, Set<Id>>();
    global list<AxtriaSalesIQTM__Position_Team_Instance__c> posTI = new list<AxtriaSalesIQTM__Position_Team_Instance__c>();
    global List<TempPosAcc__c> tempPAlist = new List<TempPosAcc__c>();
    public ID BCc {get; set;}
    global String batchStatus {get; set;}
    global string Assignmentstatus ;

    //Added by HT(A0994) on 17th June 2020
    @deprecated
    global String changeReqID;
    @deprecated
    global Boolean flagValue;

    global update_position_Acc_new(String Team, String TeamIns)
    {
        /*customsetting1 = new AxtriaSalesIQTM__TriggerContol__c();
        customsettinglist1 = new list<AxtriaSalesIQTM__TriggerContol__c>();
        customsetting2 = new AxtriaSalesIQTM__TriggerContol__c();
        customsettinglist2 = new list<AxtriaSalesIQTM__TriggerContol__c>(); */
        teamInstance = TeamIns;
        teamID = Team;
        recordsProcessed = 0;
        teminslst = [select id, AxtriaSalesIQTM__IC_EffEndDate__c, AxtriaSalesIQTM__IC_EffstartDate__c, AxtriaSalesIQTM__Alignment_Period__c from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Team__c = :teamID and id = :teamInstance WITH SECURITY_ENFORCED];

        // SnTDMLSecurityUtil.printDebugMessage('=====Custom Setting disable=====');
        // customsetting1 = AxtriaSalesIQTM__TriggerContol__c.getValues('PositionAccountTrigger');
        // if(customsetting1 != null)
        // {
        //   customsetting1.AxtriaSalesIQTM__IsStopTrigger__c = true ;
        //   customsettinglist1.add(customsetting1);
        //   update customsettinglist1;
        // }

        query = 'SELECT Account__c,Account_Type__c,Metric1__c,Metric2__c,Metric3__c,Metric4__c,Metric5__c,Metric6__c,Metric7__c,Metric8__c,Metric9__c,Metric10__c,AccountNumber__c,Id,Territory__c,Territory_ID__c,Status__c, Territory__r.AxtriaSalesIQTM__inactive__c, Account__r.AxtriaSalesIQTM__Active__c,Change_Request__c, isError__c, SalesIQ_Error_Message__c, Error_message__c FROM temp_Obj__c where Team__c=: teamID and status__c = \'New\'';
        query += ' and Object__c = \'Acc_Terr\'';
        Assignmentstatus = teminslst[0].AxtriaSalesIQTM__Alignment_Period__c;
    }

    global update_position_Acc_new(String Team, String TeamIns, String Ids)
    {
        teamInstance = TeamIns;
        teamID = Team;
        this.Ids = Ids;
        recordsProcessed =0;
        teminslst = [select id, AxtriaSalesIQTM__IC_EffEndDate__c, AxtriaSalesIQTM__IC_EffstartDate__c, AxtriaSalesIQTM__Alignment_Period__c from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Team__c = :teamID and id = :teamInstance WITH SECURITY_ENFORCED];
        query = 'SELECT Account__c,Account_Type__c,Metric1__c,Metric2__c,Metric3__c,Metric4__c,Metric5__c,Metric6__c,Metric7__c,Metric8__c,Metric9__c,Metric10__c,AccountNumber__c,Id,Territory__c,Territory_ID__c,Team_Name__c,Status__c, Territory__r.AxtriaSalesIQTM__inactive__c, Account__r.AxtriaSalesIQTM__Active__c,Change_Request__c, isError__c, SalesIQ_Error_Message__c, Error_message__c FROM temp_Obj__c where Team__c=: teamID and status__c = \'New\'';
        query += ' and Object__c = \'Acc_Terr\'  and Territory_ID__c!=null and AccountNumber__c!= null and Team_Name__c != null and Account_Type__c !=null';
        Assignmentstatus = teminslst[0].AxtriaSalesIQTM__Alignment_Period__c;
    }

    //Added by HT(A0994) on 17th June 2020
    @deprecated
    global update_position_Acc_new(String Team, String TeamIns, String Ids,Boolean flag)
    {
        teamInstance = TeamIns;
        teamID = Team;
        this.Ids = Ids;
        changeReqID = Ids;
        flagValue = flag;
        recordsProcessed =0;
        teminslst = [select id, AxtriaSalesIQTM__IC_EffEndDate__c, AxtriaSalesIQTM__IC_EffstartDate__c,
                        AxtriaSalesIQTM__Alignment_Period__c from AxtriaSalesIQTM__Team_Instance__c 
                        where AxtriaSalesIQTM__Team__c = :teamID and id = :teamInstance WITH SECURITY_ENFORCED];
        Assignmentstatus = teminslst[0].AxtriaSalesIQTM__Alignment_Period__c;

        query = 'SELECT Account__c,Account_Type__c,Metric1__c,Metric2__c,Metric3__c,Metric4__c,Metric5__c,'+
                'Metric6__c,Metric7__c,Metric8__c,Metric9__c,Metric10__c,AccountNumber__c,Id,Territory__c,'+
                'Territory_ID__c,Team_Name__c,Status__c, Territory__r.AxtriaSalesIQTM__inactive__c,'+
                'Account__r.AxtriaSalesIQTM__Active__c,Change_Request__c, isError__c,'+
                'SalesIQ_Error_Message__c, Error_message__c FROM temp_Obj__c where '+
                'Team__c=: teamID and status__c = \'New\' and Object__c = \'Acc_Terr\' '+
                'and Territory_ID__c!=null and AccountNumber__c!= null and Team_Name__c != null '+
                'and Account_Type__c !=null and Change_Request__c =:changeReqID WITH SECURITY_ENFORCED';
    }

    global Database.Querylocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<temp_Obj__c> accterrlist)
    {
        //set<string> approvedAccountSet= new set<string>();
        //set<string>accountIdSet = new set<string>();
        //Added by Ayushi
        Map<String, Set<String>> mapAccNumTypekey2AccTerrID = new Map<String, Set<String>>();
        list<String> accID                 = new list<String>();
        list<String> list1                 = new list<String>();
        list<String> list2                 = new list<String>();
        set<string> setPosAccTeamInstance  = new set<string>();
        set<string> processedID  = new set<string>();
        list<temp_Obj__c> lsTempRecordToUpdate = new list<temp_Obj__c>();
        List<TempPosAcc__c> tempPosAccList  = new List<TempPosAcc__c>();
        Map<String, String> mapKey2AccID = new Map<String, String>();
        map<string, temp_Obj__c>tempaccmap = new map<string, temp_Obj__c>();

        System.debug(accterrlist);
        for(temp_Obj__c rec : accterrlist)
        {
            System.debug(rec);
            tempaccmap.put(rec.id, rec);
            //accountIdSet.add(rec.AccountNumber__c);
            SnTDMLSecurityUtil.printDebugMessage('rec.Account__c:' + rec.Account__c);
            if(rec.Account__c != null)
            {
                accID.add(rec.Account__c);
                mapKey2AccID.put(rec.AccountNumber__c + '_' + rec.Account_Type__c, rec.Account__c);
                SnTDMLSecurityUtil.printDebugMessage('mapKey2AccID' + mapKey2AccID);
                SnTDMLSecurityUtil.printDebugMessage('accID:::' + accID);
                if(!mapAccNumTypekey2AccTerrID.containsKey(rec.AccountNumber__c + '_' + rec.Account_Type__c))
                {
                    Set<String> accTerrId = new Set<String>();
                    accTerrId.add(rec.Id);
                    mapAccNumTypekey2AccTerrID.put(rec.AccountNumber__c + '_' + rec.Account_Type__c, accTerrID);
                    SnTDMLSecurityUtil.printDebugMessage('mapAccNumTypekey2AccTerrID inside if:::::' + mapAccNumTypekey2AccTerrID);
                }
                else
                {
                    mapAccNumTypekey2AccTerrID.get(rec.AccountNumber__c + '_' + rec.Account_Type__c).add(rec.Id);
                    SnTDMLSecurityUtil.printDebugMessage('mapAccNumTypekey2AccTerrID inside else:::::' + mapAccNumTypekey2AccTerrID);
                }

            }
            if(rec.Territory__c != null )
            {
                list2.add(rec.Territory__c);
                SnTDMLSecurityUtil.printDebugMessage('list2:::' + list2);
            }
        }


        //Added by Ayushi
        List<Account> accList = [select Id, AccountNumber, Type from Account where Id in :accID WITH SECURITY_ENFORCED]; //AxtriaSalesIQTM__Active__c='Active' and
        Set<String> accNumTypeSet = new Set<String>();
        for(Account acc : accList)
        {
            SnTDMLSecurityUtil.printDebugMessage('exist::::' + acc.AccountNumber + '_' + acc.Type);
            accNumTypeSet.add(acc.AccountNumber + '_' + acc.Type);
        }
        //     if(mapAccNumTypekey2AccTerrID.containsKey(acc.AccountNumber+'_'+acc.Type))
        //     {
        //         SnTDMLSecurityUtil.printDebugMessage('If part');
        //         list1.add(acc.Id);
        //         SnTDMLSecurityUtil.printDebugMessage('list1:::'+list1);
        //     }
        //     else
        //     {
        //         SnTDMLSecurityUtil.printDebugMessage('Else part');

        if(mapAccNumTypekey2AccTerrID != null)
        {
            SnTDMLSecurityUtil.printDebugMessage('map not null');
            for(String key : mapAccNumTypekey2AccTerrID.keyset())
            {
                if(accNumTypeSet.contains(key))
                {
                    SnTDMLSecurityUtil.printDebugMessage('If part');
                    String accountID = mapKey2AccID.get(key);
                    SnTDMLSecurityUtil.printDebugMessage('accountID::::::' + accountID);
                    list1.add(accountID);
                    SnTDMLSecurityUtil.printDebugMessage('list1:::' + list1);
                }
                else
                {
                    SnTDMLSecurityUtil.printDebugMessage('Else part');
                    SnTDMLSecurityUtil.printDebugMessage('key:::' + key);
                    for(String accTerrID : mapAccNumTypekey2AccTerrID.get(key))
                    {
                        SnTDMLSecurityUtil.printDebugMessage('accTerrID:::' + accTerrID);
                        temp_Obj__c tempAccTerrRec = tempaccmap.get(accTerrID);

                        tempAccTerrRec.Status__c = 'Rejected';
                        tempAccTerrRec.Reason_Code__c = 'Incorrect Account Type in SalesIQ';
                        tempAccTerrRec.SalesIQ_Error_Message__c = 'Incorrect Account Type in SalesIQ';
                        tempAccTerrRec.Change_Request__c = Ids;//A1422
                        tempAccTerrRec.isError__c = true;//A1422
                        SnTDMLSecurityUtil.printDebugMessage('======tempAccTerrRec.Account__c==' + tempAccTerrRec.Account__c);

                        SnTDMLSecurityUtil.printDebugMessage('======tempAccTerrRec.Territory__c==' + tempAccTerrRec.Territory__c);

                        if(tempAccTerrRec.Account__c != null && tempAccTerrRec.Territory__c != null)
                        {
                            lsTempRecordToUpdate.add(tempAccTerrRec);
                            SnTDMLSecurityUtil.printDebugMessage('tempAccTerrRec:::' + tempAccTerrRec);
                            processedID.add(accTerrID);
                        }

                    }
                }
            }
        }

        //Alignmnet period modification
        SnTDMLSecurityUtil.printDebugMessage('========Assignmentstatus=====' + Assignmentstatus);
        if(Assignmentstatus == 'Current')
        {
            Assignmentstatus = 'Active';
        }
        else if(Assignmentstatus == 'Future')
        {
            Assignmentstatus = 'Future Active';
        }
        SnTDMLSecurityUtil.printDebugMessage('======MOdified==Assignmentstatus=====' + Assignmentstatus);
        //query existing position accounts with the key = Position+AccountNumber+TeamInstanceName+Assignment status
        list<AxtriaSalesIQTM__Position_Account__c> lsposAcc = [select id, name, AxtriaSalesIQTM__Account__r.AccountNumber, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Account__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__Assignment_Status__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Position__c in:list2 and AxtriaSalesIQTM__Account__c in:list1 and (AxtriaSalesIQTM__Assignment_Status__c = 'Active' or AxtriaSalesIQTM__Assignment_Status__c = 'Future Active') and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__inactive__c != true and AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__Active__c = 'Active' WITH SECURITY_ENFORCED];

        for(AxtriaSalesIQTM__Position_Account__c pa : lsposAcc)
        {
            setPosAccTeamInstance.add(string.valueof(pa.AxtriaSalesIQTM__Position__c) + string.valueof(pa.AxtriaSalesIQTM__Account__c) + string.valueof(pa.AxtriaSalesIQTM__Team_Instance__c) + string.valueOf(pa.AxtriaSalesIQTM__Assignment_Status__c));
        }
        //removing check for affiliation on 22-11-2018
        /*for(AxtriaSalesIQTM__Account_Affiliation__c accAffiliation:[SELECT Id, Name,Account_Number__c, Affiliation_Hierarchy__c, AxtriaSalesIQTM__Affiliation_Type__c, AxtriaSalesIQTM__Active__c, AxtriaSalesIQTM__Is_Primary__c FROM AxtriaSalesIQTM__Account_Affiliation__c where ((AxtriaSalesIQTM__Is_Primary__c=true and AxtriaSalesIQTM__Active__c= true and AxtriaSalesIQTM__Account__r.type='HCP') OR AxtriaSalesIQTM__Account__r.type!='HCP')  and Account_Number__c in: accountIdSet])
        {
           approvedAccountSet.add(accAffiliation.Account_Number__c);
        }
        SnTDMLSecurityUtil.printDebugMessage('approvedAccountSet================'+approvedAccountSet); */
        SnTDMLSecurityUtil.printDebugMessage('====teminslst ::' + teminslst);
        map<string, string> posmap = new map<string, string>();
        for(AxtriaSalesIQTM__Position_Team_Instance__c posTI : [select AxtriaSalesIQTM__Position_ID__c, id from AxtriaSalesIQTM__Position_Team_Instance__c where AxtriaSalesIQTM__Position_ID__c in: list2 and  AxtriaSalesIQTM__Team_Instance_ID__c = :teamInstance WITH SECURITY_ENFORCED])
        {
            posmap.put(posTI.AxtriaSalesIQTM__Position_ID__c, posTI.id);
        }
        /*map<string,string> accmap=new map<string,string>();
        for(AxtriaSalesIQTM__Team_Instance_Account__c accTI:[select AxtriaSalesIQTM__Account_ID__c,id from AxtriaSalesIQTM__Team_Instance_Account__c where AxtriaSalesIQTM__Account_ID__c in :list1 and AxtriaSalesIQTM__Team_Instance__c=:teamInstance])
        {
            accmap.put(accTI.AxtriaSalesIQTM__Account_ID__c,accTI.id);
        }*/

        list<AxtriaSalesIQTM__Position_Account__c> posAccListUpdate = new list<AxtriaSalesIQTM__Position_Account__c>();
        for(temp_Obj__c cprcd : accterrlist)
        {
            if(cprcd.Account__c != null && cprcd.Territory__c != null && !processedID.contains(cprcd.id))
            {
                if(!setPosAccTeamInstance.contains(string.valueof(cprcd.Territory__c) + string.valueof(cprcd.Account__c) + teamInstance + Assignmentstatus))
                {
                 if(cprcd.Territory__r.AxtriaSalesIQTM__inactive__c == True)
                 {
                    SnTDMLSecurityUtil.printDebugMessage('Inactive Position in SalesIQ');
                    cprcd.status__c = 'Rejected';
                    cprcd.Reason_Code__c = 'Inactive Position in SalesIQ';
                    cprcd.SalesIQ_Error_Message__c = 'Inactive Position in SalesIQ';
                        cprcd.Change_Request__c = Ids;//A1422
                        cprcd.isError__c = true;//A1422
                        lsTempRecordToUpdate.add(cprcd);
                    }
                    else if(cprcd.Account__r.AxtriaSalesIQTM__Active__c == 'Active' && cprcd.Territory__r.AxtriaSalesIQTM__inactive__c != True )     //&& approvedAccountSet.contains(cprcd.AccountNumber__c)
                    {
                        AxtriaSalesIQTM__Position_Account__c posAcc = new AxtriaSalesIQTM__Position_Account__c();

                        posAcc.AxtriaSalesIQTM__Account__c                  = cprcd.Account__c;
                        posAcc.AxtriaSalesIQTM__Position__c                 = cprcd.Territory__c;
                        posAcc.AxtriaSalesIQTM__Team_Instance__c            = teamInstance;
                        posAcc.AxtriaSalesIQTM__Position_Team_Instance__c   = posmap.get(cprcd.Territory__c);
                        posAcc.AxtriaSalesIQTM__Change_Status__c            = 'No Change';
                        posAcc.AxtriaSalesIQTM__Effective_End_Date__c       = teminslst[0].AxtriaSalesIQTM__IC_EffEndDate__c;
                        posAcc.AxtriaSalesIQTM__Effective_Start_Date__c     = teminslst[0].AxtriaSalesIQTM__IC_EffstartDate__c;
                        posAcc.AxtriaSalesIQTM__Account_Alignment_Type__c   = 'Explicit';
                        //Added on 29-08 by Ayushi
                        posAcc.AxtriaSalesIQTM__Segment_1__c   = 'Primary';
                        posAcc.Batch_Name__c = 'Direct Load Position Account';
                        posAcc.AxtriaSalesIQTM__TempDestinationrgb__c = 'Direct Load';

                        posAccListUpdate.add(posAcc);
                        //Update Temp Record
                        cprcd.status__c = 'Processed';
                        cprcd.Change_Request__c = Ids; //A1422
                        cprcd.isError__c = false;//A1422
                        lsTempRecordToUpdate.add(cprcd);
                    }

                    /*else{
                        cprcd.status__c = 'Rejected';
                    cprcd.Reason_Code__c = 'Inactive Position/Account in SalesIQ';
                        lsTempRecordToUpdate.add(cprcd);

                    }
                    */ // commented by Saiba
                    else
                    {
                        SnTDMLSecurityUtil.printDebugMessage('Inactive Account in SalesIQ');
                        cprcd.status__c = 'Rejected';
                        cprcd.Reason_Code__c = 'Inactive Account in SalesIQ';
                        cprcd.SalesIQ_Error_Message__c = 'Inactive Account in SalesIQ';
                        cprcd.Change_Request__c = Ids;//A1422
                        cprcd.isError__c = true;//A1422
                        lsTempRecordToUpdate.add(cprcd);
                    }

                    /*else if(! approvedAccountSet.contains(cprcd.AccountNumber__c))
                    {
                      SnTDMLSecurityUtil.printDebugMessage('Affiliation not available');
                      cprcd.status__c = 'Rejected';
                      cprcd.Reason_Code__c = 'Affiliation not available';
                      lsTempRecordToUpdate.add(cprcd);
                    }
                    */
                }
                else
                {
                    SnTDMLSecurityUtil.printDebugMessage('Overlapping Assignment');
                    cprcd.status__c = 'Rejected';
                    cprcd.Reason_Code__c = 'Overlapping Assignment';

                    lsTempRecordToUpdate.add(cprcd);
                }
            }

            /* commented by Saiba
            else{
              cprcd.status__c = 'Rejected';
              cprcd.Reason_Code__c = 'Account/Territory/Team Instance does not exist in SalesIQ';
              lsTempRecordToUpdate.add(cprcd);
            }
            */

            else if(cprcd.Account__c == null)
            {
                SnTDMLSecurityUtil.printDebugMessage('Account does not exist in SalesIQ');
                cprcd.status__c = 'Rejected';
                cprcd.Reason_Code__c = 'Account does not exist in SalesIQ';
                cprcd.SalesIQ_Error_Message__c = 'Account does not exist in SalesIQ';
                cprcd.Change_Request__c = Ids;//A1422
                cprcd.isError__c = true;//A1422
                lsTempRecordToUpdate.add(cprcd);
            }

            else if(cprcd.Territory__c == null)
            {
                SnTDMLSecurityUtil.printDebugMessage('Territory does not exist in SalesIQ');
                cprcd.status__c = 'Rejected';
                cprcd.Reason_Code__c = 'Territory does not exist in SalesIQ';
                cprcd.SalesIQ_Error_Message__c = 'Territory does not exist in SalesIQ';
                cprcd.Change_Request__c = Ids;//A1422
                cprcd.isError__c = true;//A1422
                lsTempRecordToUpdate.add(cprcd);
            }
        }

        /*else
        {
          cprcd.status__c = 'Rejected';
          cprcd.Reason_Code__c = 'Team Instance does not exist in SalesIQ';
          lsTempRecordToUpdate.add(cprcd);

      }*/

        //Change By Ayushi
      set<string>accountid = new set<string>();
      set<string>teaminstancelist = new set<string>();
      if(posAccListUpdate.size() != 0)
      {
            //database.saveresult[] ds = Database.insert(posAccListUpdate, false);
                    //Changed for security review
        SObjectAccessDecision securityDecision = Security.stripInaccessible(AccessType.CREATABLE,posAccListUpdate);
        SnTDMLSecurityUtil.printDebugMessage('AxtriaSalesIQTM__Position_Account__c',' in insertRecords '); 
                    //insert securityDecision.getRecords();
        //list<AxtriaSalesIQTM__Position_Account__c> posAccListUpdatetemp = securityDecision.getRecords();
        database.saveresult[] ds = Database.insert(securityDecision.getRecords(), false);
        SnTDMLSecurityUtil.printDebugMessage('AxtriaSalesIQTM__Position_Account__c',' in getRemovedFields::: '+securityDecision.getRemovedFields());             
                    //throw exception if permission is missing 
        if(!securityDecision.getRemovedFields().isEmpty() ){
         throw new SnTException(securityDecision, 'AxtriaSalesIQTM__Position_Account__c', SnTException.OperationType.C ); 
     }
                    //End of security review change
     for(database.SaveResult d : ds)
     {
        if(d.isSuccess())
        {
            recordsProcessed++;
        }
        else
        {
            flag = false;
        }
    }
    if(posAccListUpdate != null && posAccListUpdate.size() > 0)
    {
        for(AxtriaSalesIQTM__Position_Account__c posAccRec : posAccListUpdate)
        {
            TempPosAcc__c tempobj = new TempPosAcc__c();
            tempobj.Team_Instance__c = posAccRec.AxtriaSalesIQTM__Team_Instance__c;
            tempobj.Account__c = posAccRec.AxtriaSalesIQTM__Account__c;
            tempobj.Status__c = 'New';
            tempPosAccList.add(tempobj);
            accountid.add(posAccRec.AxtriaSalesIQTM__Account__c);
            teaminstancelist.add(posAccRec.AxtriaSalesIQTM__Team_Instance__c);
        }
    }
    if(tempPosAccList.size() != 0)
    {
                //insert tempPosAccList;
        SnTDMLSecurityUtil.insertRecords(tempPosAccList, 'update_position_Acc');
    }
}


if(tempPosAccList != null && tempPosAccList.size() > 0)
{
    List<TempPosAcc__c> templist = [select Id, Team_Instance__c, Account__c, Status__c from TempPosAcc__c where Status__c = 'New' and Account__c IN :accountid and Team_Instance__c IN:teaminstancelist WITH SECURITY_ENFORCED];
    if(templist != null)
    {
        for(TempPosAcc__c temp : templist)
        {
            if(mapTI2Acc.containsKey(temp.Team_Instance__c))
            {
                mapTI2Acc.get(temp.Team_Instance__c).add(temp.Account__c);
                mapTI2id.get(temp.Team_Instance__c).add(temp.Id);
            }
            else
            {
                Set<Id> tempSet = new Set<Id>();
                Set<Id> idSet = new Set<Id>();
                tempSet.add(temp.Account__c);
                idSet.add(temp.Id);
                mapTI2Acc.put(temp.Team_Instance__c, tempSet);
                mapTI2id.put(temp.Team_Instance__c, idSet);
            }
        }
    }
}

        //Till here...


if(lsTempRecordToUpdate.size()!=0){
 
            //update lsTempRecordToUpdate;
    SnTDMLSecurityUtil.updateRecords(lsTempRecordToUpdate, 'update_position_Acc');
}



SnTDMLSecurityUtil.printDebugMessage('============insert done===================');

}

global void finish(Database.BatchableContext BC)
{
    if(Ids != null)
    {
        AxtriaSalesIQTM__Change_Request__c changerequest = new AxtriaSalesIQTM__Change_Request__c();
        if(flag && ST_Utility.getJobStatus(BC.getJobId()))
        {
            changerequest.Job_Status__c = 'Done';
        }
        else
        {
            changerequest.Job_Status__c = 'Error';
        }
        SnTDMLSecurityUtil.printDebugMessage('recordsProcessed'+recordsProcessed);
        changerequest.Records_Updated__c = recordsProcessed;
        changerequest.Id = IDs;
            //update changerequest;
        SnTDMLSecurityUtil.updateRecords(changerequest, 'update_position_Acc');
        Direct_Load_Records_Status_Update.Direct_Load_Records_Status_Update(Ids,'update_position_Acc');
    }

}
}