global class BatchPosAccCopyValueAddedDropped implements Database.Batchable<sObject>, Database.Stateful,schedulable{
    
    Public String country;
    
   global BatchPosAccCopyValueAddedDropped(){
     
    }
    
    global BatchPosAccCopyValueAddedDropped(String Country){
     this.country = country;
    }
    
     global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT Id,AxtriaSalesIQTM__Hierarchy_Level__c , Name,AxtriaARSnT__Current_Count__c,PA_Active__c,PA_Inactive__c  FROM AxtriaSalesIQTM__Position__c where  AxtriaSalesIQTM__Hierarchy_Level__c = \'1\' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = \'Current\' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = \'Live\'and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name =: Country' ;
        return Database.getQueryLocator(query);
      }
      
    global void execute(Database.BatchableContext BC, List<AxtriaSalesIQTM__Position__c> scope){  
    
       for(AxtriaSalesIQTM__Position__c POS : scope){
           POS.AxtriaARSnT__One_day_Previous_Count__c =  POS.AxtriaARSnT__Current_Count__c; 
               if(POS.AxtriaARSnT__One_day_Previous_Count__c == null){
                   POS.AxtriaARSnT__One_day_Previous_Count__c = 1;
               }
          }
        update scope; 
    }
    
    global void execute(System.SchedulableContext SC){
       
        List<AxtriaSalesIQTM__Country__c> Countr = [Select Name from AxtriaSalesIQTM__Country__c];
            system.debug(Countr + 'Countr ');
            
         for(AxtriaSalesIQTM__Country__c Coun : Countr){
                 system.debug(Countr + 'Coun');
             database.executeBatch(new BatchPosAccCopyValueAddedDropped(Coun.Name),2000);     
         } 
    }
    
    global void finish(Database.BatchableContext BC) {    
        database.executebatch(new BatchPositionAccountCount(Country));
    }
}