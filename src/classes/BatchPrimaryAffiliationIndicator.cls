global class BatchPrimaryAffiliationIndicator implements Database.Batchable <sObject> , Database.Stateful, Schedulable{

    public String query;
    public DateTime lastChangesDate;
    public Set<String> circularKeyset = new Set<String>();
        
    global BatchPrimaryAffiliationIndicator()
    {
        query='';
        query = 'SELECT id, AxtriaSalesIQTM__Account__c, CheckCircularDependency__c, AxtriaSalesIQTM__Parent_Account__c FROM AxtriaSalesIQTM__Account_Affiliation__c where Affiliation_Status__c = \'Active\' and Primary_Affiliation_Indicator__c = \'Y\' and CheckCircularDependency__c=false';
    }

    @Deprecated
    global BatchPrimaryAffiliationIndicator(Date changedRecs)
    {
    }

    global BatchPrimaryAffiliationIndicator(DateTime changedRecs)
    {
        query='';
        lastChangesDate=changedRecs;
        query = 'SELECT id, AxtriaSalesIQTM__Account__c, AxtriaSalesIQTM__Parent_Account__c,CheckCircularDependency__c FROM AxtriaSalesIQTM__Account_Affiliation__c where Affiliation_Status__c = \'Active\' and Primary_Affiliation_Indicator__c = \'Y\' and lastModifiedDate >= :lastChangesDate and CheckCircularDependency__c=false';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        /*String query = 'SELECT id, AxtriaSalesIQTM__Account__c, AxtriaSalesIQTM__Parent_Account__c FROM AxtriaSalesIQTM__Account_Affiliation__c where Affiliation_Status__c = \'Active\' and Primary_Affiliation_Indicator__c = \'Y\'';*/
        return Database.getQueryLocator(query);
    }

    global void execute(System.SchedulableContext SC){

        database.executeBatch(new BatchPrimaryAffiliationIndicator(Date.today().addDays(-2)));                                                           
       
    }
    
    global void execute(Database.BatchableContext bc, List<AxtriaSalesIQTM__Account_Affiliation__c> scope) {
        List<Account> accounts = new List<Account>();
        Set<ID> accs = new Set<ID>();

         for(AxtriaSalesIQTM__Account_Affiliation__c accaff : scope)
         {
            
                if(!accs.contains(accaff.AxtriaSalesIQTM__Account__c) && accaff.AxtriaSalesIQTM__Account__c!=null)
                {
                     Account acc = new Account(id = accaff.AxtriaSalesIQTM__Account__c);
                     if(acc.ParentId != accaff.AxtriaSalesIQTM__Parent_Account__c)
                     {
                         acc.ParentId = accaff.AxtriaSalesIQTM__Parent_Account__c;
                         accounts.add(acc);
                     }
                         accs.add(accaff.AxtriaSalesIQTM__Account__c);                    
                }
          }

        System.debug('accounts.size():::::' +accounts.size());

        if(accounts.size() > 0)
            update accounts;

        System.debug('::::::Circular Dependency Handling via EMAIL:::::');
        List<AxtriaSalesIQTM__Account_Affiliation__c> affList = [Select Id, Unique_Id__c,AxtriaSalesIQTM__Account__c, CheckCircularDependency__c, AxtriaSalesIQTM__Parent_Account__c from AxtriaSalesIQTM__Account_Affiliation__c where CheckCircularDependency__c=true and AxtriaSalesIQTM__Account__c != null and AxtriaSalesIQTM__Parent_Account__c != null and Affiliation_Status__c = 'Active'];

        

        for(AxtriaSalesIQTM__Account_Affiliation__c affRec : affList)
        {
             circularKeyset.add(affRec.Unique_Id__c);
        }
        System.debug('circularKeyset:::::' +circularKeyset);

       

    }   
    
    global void finish(Database.BatchableContext bc) {

        if(circularKeyset != null && circularKeyset.size() > 0)
        {
            PrimaryAffiliationIndicatorEmail emailTrigger = new PrimaryAffiliationIndicatorEmail(circularKeyset);
            Database.executeBatch(emailTrigger,2000);

          
        }
        Database.executeBatch(new BatchPopulateHCP_HCA_Affliation(), 20);
    }

}