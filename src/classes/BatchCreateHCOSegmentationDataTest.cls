/*
@author - Prince Richard Augustin (A2661)
@description - Test class to test the BatchCreateHCOParameterValueMap.
*/

@isTest
private class BatchCreateHCOSegmentationDataTest {
    static testMethod void testMethod1() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;
        Account acc= TestDataFactory.createAccount();
        acc.Type = 'HCO';
        insert acc;
        Account acc1= TestDataFactory.createAccount();
        acc1.Type = 'HCP';
        insert acc1;
        Account acc2= TestDataFactory.createAccount();
        acc2.Type = 'HCO';
        insert acc2;

        AxtriaSalesIQTM__Affiliation_Network__c affnet = TestDataFactory.createAffliNet(countr);
        insert affnet;
        AxtriaSalesIQTM__Account_Affiliation__c accaff = TestDataFactory.createAcctAffli(acc,affnet);
        accaff.BU_Rank__c = 1.00;
        accaff.AxtriaSalesIQTM__Parent_Account__c = acc1.Id;
        accaff.AxtriaSalesIQTM__Account__c = acc.Id;
        insert accaff;
        /*AxtriaSalesIQTM__Account_Affiliation__c accaff1 = TestDataFactory.createAcctAffli(acc2,affnet);
        accaff1.BU_Rank__c = 3.00;
        accaff1.AxtriaSalesIQTM__Parent_Account__c = acc.Id;
        accaff1.AxtriaSalesIQTM__Account__c = acc2.Id;
        insert accaff1;*/
        

        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        Measure_Master__c mmc1 = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc1.Measure_Type__c = 'HCO';
        mmc1.Brand__c = mmc.Brand__c;
        insert mmc1;
        Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
        pp.Name ='MARKET_ CRC';
        insert pp;
        Parameter__c pp1 = TestDataFactory.parameter(pcc, team, teamins);
        pp1.Name ='MARKET_ CRC';
        insert pp1;
        Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
        rp.Parameter__c = pp.id;
        insert rp;
        Rule_Parameter__c rp1= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
        rp1.Parameter__c = pp1.id;
        insert rp1;
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        posAccount.AxtriaSalesIQTM__Account__c = acc.id;
        insert posAccount;
        AxtriaSalesIQTM__Position_Account__c posAccount1 = TestDataFactory.createPositionAccount(acc1,pos,teamins);
        posAccount1.AxtriaSalesIQTM__Account__c = acc1.id;
        insert posAccount1;

        BU_Response__c bu = TestDataFactory.createBuResponse(posAccount,pcc,teamins,team,acc);
        insert bu;
        HCO_Parameters__c hcoParam = TestDataFactory.createHCOParameter(rp,mmc);
        hcoParam.Source_Rule__c = mmc.id;
        hcoParam.Destination_Rule__c = mmc1.id;
        hcoParam.Source_Rule_Parameter__c = rp.Id;
        insert hcoParam;
        
        Account_Compute_Final__c compFinal = TestDataFactory.createComputeFinal(mmc,acc,posAccount,bu,pos);
        for(integer i=1;i<=50;i++){
            compFinal.put('OUTPUT_NAME_'+i+'__c',pp.Name);
        }
        compFinal.Measure_Master__c = mmc.id;
        insert compFinal;
        
        BU_Response__c bu1 = TestDataFactory.createBuResponse(posAccount,pcc,teamins,team,acc1);
        insert bu1;
        
        Account_Compute_Final__c compFinal1 = TestDataFactory.createComputeFinal(mmc,acc1,posAccount1,bu1,pos);
        for(integer i=1;i<=50;i++){
            compFinal1.put('OUTPUT_NAME_'+i+'__c',pp.Name);
        }
        compFinal1.Measure_Master__c = mmc.id;
        insert compFinal1;
        Map<String,Decimal> value = new Map<String,Decimal>();
        value.put('key', 1.0);
        Map<String,Map<String,Decimal>> hcoToMapOfParamValMap = new Map<String,Map<String,Decimal>>();
        hcoToMapOfParamValMap.put(acc.Id, value);
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            CreateHCOSegBatch obj = new CreateHCOSegBatch(mmc.id,mmc1.Id,hcoToMapOfParamValMap);            
            obj.query = 'Select Id from Account';
            Database.executeBatch(obj);
        }
        Test.stopTest();
    }
}