global with sharing class Batch_DeletePositionProduct implements Database.Batchable<sObject> {
    list<SIQ_My_Setup_Products_vod_O__c> listProd;
    string jobType = util.getJobType('PositionProduct');
    set<string> allCountries = jobType == 'Full Load'?util.getFulloadCountries():util.getDeltaLoadCountries();
   // map<string, boolean> mapProdStatus;
    string query;
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        if(jobType == 'Full Load'){
            query = 'Select Id, Status__c, SIQ_Country__c, Record_Status__c from SIQ_My_Setup_Products_vod_O__c WITH SECURITY_ENFORCED';
        }
        else{    
            query = 'Select Id, Status__c, Record_Status__c from SIQ_My_Setup_Products_vod_O__c Where Record_Status__c = \'Updated\' WITH SECURITY_ENFORCED';
        }
        return database.getQueryLocator(query);
    }
            
    global void execute(Database.BatchableContext bc, List<SIQ_My_Setup_Products_vod_O__c> scope){
        listProd = new list<SIQ_My_Setup_Products_vod_O__c>();
        if(jobType == 'Full Load'){
            for(SIQ_My_Setup_Products_vod_O__c pe : scope){ 
                if(allCountries.contains(pe.SIQ_Country__c)){
                    pe.Status__c = 'Deleted';
                    pe.Record_Status__c = 'Updated';
                    listProd.add(pe);
                }
                else{
                    pe.Record_Status__c = 'No Change';
                    listProd.add(pe);    
                }
            }
        }
        else{
            for(SIQ_My_Setup_Products_vod_O__c pe : scope){
                pe.Record_Status__c = 'No Change';
                listProd.add(pe);
            }
        }
        if(listProd.size()>0){
            //upsert listProd;
            SnTDMLSecurityUtil.upsertRecords(listProd, 'Batch_DeletePositionProduct');
        }
    }
    
    global void finish(Database.BatchableContext bc){
        if(!System.Test.isRunningTest())
            database.executeBatch(new Batch_PositionProductMapping(), 2000);
    }
}