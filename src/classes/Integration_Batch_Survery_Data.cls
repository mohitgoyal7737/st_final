global class Integration_Batch_Survery_Data implements Database.Batchable<sObject> {
    public String query;

    global Integration_Batch_Survery_Data() {

        
        this.query = 'select id, SIQ_Added_Territory__c, SIQ_Account__c, Deployment_Status__c from SIQ_GAS_History__c where Deployment_Status__c = \'New\' OR Deployment_Status__c = null';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<SIQ_GAS_History__c> scope) {
        
        
        Set<String> allAccounts = new Set<String>();
        Set<String> allPositionsSet = new Set<String>();
        Map<String, String> atlMap = new Map<String,String>();
        Map<String,String> veevaToSalesIQID = new Map<String,String>();
        Map<String,String> posIDMapping = new Map<String,String>();

        List<AxtriaSalesIQTM__Position_Account__c> newPositionAccounts = new List<AxtriaSalesIQTM__Position_Account__c>();


        for(SIQ_GAS_History__c sgh : scope)
        {
            allAccounts.add(sgh.SIQ_Account__c);
            allPositionsSet.add(sgh.SIQ_Added_Territory__c);
        }  

        List<Staging_ATL__c> allStagingRecs = [select Axtria_Account_ID__c, Account__c, Territory__c from Staging_ATL__c where Account__c in :allAccounts];
        List<AxtriaSalesIQTM__Position__c> allPositions = [select id, AxtriaSalesIQTM__Client_Position_Code__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Client_Position_Code__c in :allPositionsSet];

        for(AxtriaSalesIQTM__Position__c pos : allPositions)
        {
            posIDMapping.put(pos.AxtriaSalesIQTM__Client_Position_Code__c, pos.ID);
        }

        for(Staging_ATL__c sa : allStagingRecs)
        {
            veevaToSalesIQID.put(sa.Account__c, sa.Axtria_Account_ID__c);
            atlMap.put(sa.Account__c, sa.Territory__c);
        }

        for(SIQ_GAS_History__c sgh : scope)
        {
            if(atlMap.containsKey(sgh.SIQ_Account__c))
            {
                List<String> territoriesList = new List<String>((atlMAp.get(sgh.SIQ_Account__c)).split(';'));
                if(territoriesList.contains(sgh.SIQ_Added_Territory__c))
                {
                    AxtriaSalesIQTM__Position_Account__c posAcc = new AxtriaSalesIQTM__Position_Account__c();
                    posAcc.AxtriaSalesIQTM__Account__c = veevaToSalesIQID.get(sgh.SIQ_Account__c);
                    posAcc.AxtriaSalesIQTM__Position__c = posIDMapping.get(sgh.SIQ_Added_Territory__c);
                    posAcc.AxtriaSalesIQTM__Effective_Start_Date__c =  System.today();
                    posAcc.AxtriaSalesIQTM__Effective_End_Date__c = Date.newInstance(2099,12,31);
                    newPositionAccounts.add(posAcc);
                    sgh.Deployment_Status__c = 'Deployed';
                }
                else
                {
                    sgh.Deployment_Status__c = 'Not Required';
                }
            }

        }

        insert newPositionAccounts;
        update scope;
    }

    global void finish(Database.BatchableContext BC) {

    }
}