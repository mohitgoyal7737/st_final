/**********************************************************************************************
@author     : Ritu Pandey
@date       : 6 May 2010
@description: This Batch Class is used for deleting all data in currentPersonFeed__c
@Client     : AZ Global
Revison(s)  :
**********************************************************************************************/
global class DeleteCurrentPersonFeed implements Database.Batchable<sObject>, Database.Stateful,schedulable{ 
	global String query;
	
    global DeleteCurrentPersonFeed(){
	    query  = 'select id from CurrentPersonFeed__c';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){  
        return Database.getQueryLocator(query);
    }
    
    public void execute(System.SchedulableContext SC){
    	
        database.executeBatch(new DeleteCurrentPersonFeed(),200);
    }
    
    global void execute(Database.BatchableContext BC,list<CurrentPersonFeed__c> lsCurrPerFeed){ 
    	list<string> feedIds = new list<string>();
    	for(CurrentPersonFeed__c feed: lsCurrPerFeed){
    		feedIds.add(feed.id);
    	}
    	if(lsCurrPerFeed.size()!=0){
    		Database.delete(feedIds);
    	} 
    }
    
    global void finish(Database.BatchableContext BC){
    	
    	Database.executeBatch(new BatchCopySOAInbound2CurrentFeed(),200);
    }
}