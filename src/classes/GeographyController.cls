global class GeographyController {
    
    public string countrySelected{get; set;}
    public string geoTypeSelected{get; set;}
    public list<SelectOption>countryView {get;set;}
    public list<AxtriaSalesIQTM__Country__c> countryList = null;
    public list<SelectOption>geoTypeView {get; set;}
    public list<SelectOption>geoZipTypeView {get;set;}
    public list<AxtriaSalesIQTM__Geography_Type__c> geoTypeList = null;
    public List<GeographyView> geographyView {get;set;}
    public Boolean reNamePublishFlag {get;set;}
    public Boolean createPublishFlag {get;set;}
    public Boolean hieChangePublishFlag {get;set;}
    public Boolean messagePublishFlag {get;set;}
    public Boolean errorPublishFlag {get;set;}
    public GeographyView currentGeo {get;set;}
    public GeographyView createGeoReq {get;set;}
    public String popupOption {get;set;}
    public String successMsg {get;set;}
    public String errorMsg {get;set;}
    public list<SelectOption> prntZipCodeView {get;set;}
    public String prntGeoFoundMsg {get;set;}
    public String countryID {get;set;}
    public Map<string,string> successErrorMap;
    global String firstGeoTypeId  {get;set;}

    Public AxtriaSalesIQTM__Country__c Country {get;set;}

    global GeographyController() {  
        
        currentGeo = new GeographyView();
        createGeoReq = new GeographyView();
        countryView = new list<SelectOption>();
        countryView.add(new SelectOption('None', '--None--'));
        geoTypeView = new list<SelectOption>();
        geoTypeView.add(new SelectOption('None', '--None--'));
        countryList = new List<AxtriaSalesIQTM__Country__c>();
        countryList = [SELECT Id, Name FROM AxtriaSalesIQTM__Country__c];

        //Fetch Country ID from cookie
        countryID = SalesIQUtility.getCookie(SalesIQUtility.getCountryCookieName());
        system.debug('##### countryID ' + countryID);
        successErrorMap = SalesIQUtility.checkCountryAccess(countryID);
        system.debug('############ successErrorMap ' + successErrorMap);
        if(successErrorMap.containsKey('Success')){
           countryID = successErrorMap.get('Success');               
           system.debug('########## countryID from Map ' + countryID);
           //As soon as we get Country ID set it in the cookie and it will applicable for whole application.
           SalesIQUtility.setCookieString('CountryID',countryID);
            Country = new AxtriaSalesIQTM__Country__c();
            Country = [select AxtriaSalesIQTM__Country_Flag__c,Name from AxtriaSalesIQTM__Country__c where id =:countryID limit 1];
        }

        //Fill Geography Type Drop down
        geoTypeList = new List<AxtriaSalesIQTM__Geography_Type__c>();
        if(countryID != null){
            geoTypeList = [SELECT Id,Name FROM AxtriaSalesIQTM__Geography_Type__c WHERE AxtriaSalesIQTM__Country__c =: countryID];
            geoTypeView.clear();
            for(AxtriaSalesIQTM__Geography_Type__c geoType:geoTypeList){
                if(geoType.Name != null)
                    geoTypeView.add(new SelectOption(geoType.Id, geoType.Name));
            }
            if(geoTypeList == null || geoTypeList.size() == 0){
                geoTypeView.add(new SelectOption('None', '--None--'));
            }
        }

        //Fill DataTable with By default values
        if(geoTypeList.size() > 0){
            firstGeoTypeId = geoTypeList[0].Id;
        }
        System.debug('geoType1 is '+ firstGeoTypeId);
    }
    
    global void countryChanged(){
        
    }
    
    global void geoTypeChanged(){

    }
    
    global void populateGeo(){
    
    }
    
    @RemoteAction
    global static List<GeographyView> populateGeography(String country, String selectedGeoType) {
        
        System.debug('selectedGeoType is ' + selectedGeoType);

        list<AxtriaSalesIQTM__Geography__c> geographyList = new list<AxtriaSalesIQTM__Geography__c>();
        if( selectedGeoType.length() > 0 && selectedGeoType != 'None'){
            geographyList = [SELECT Id, Name, AxtriaSalesIQTM__Zip_Name__c, AxtriaSalesIQTM__Zip_Type__c, AxtriaSalesIQTM__Parent_Zip_Code__c, AxtriaSalesIQTM__Parent_Zip_Code__r.Name, AxtriaSalesIQTM__Parent_Zip_Code__r.AxtriaSalesIQTM__Zip_Type__c, Country_Code__c, Geo_Level__c, AxtriaSalesIQTM__Geography_Type__c,AxtriaSalesIQTM__Geography_Type__r.Name FROM AxtriaSalesIQTM__Geography__c WHERE Country_ID__c =:country AND   AxtriaSalesIQTM__Geography_Type__c =:selectedGeoType]; 
        }else{
           // geographyList = [SELECT Id, Name, AxtriaSalesIQTM__Zip_Name__c, AxtriaSalesIQTM__Zip_Type__c, AxtriaSalesIQTM__Parent_Zip_Code__c, AxtriaSalesIQTM__Parent_Zip_Code__r.Name, AxtriaSalesIQTM__Parent_Zip_Code__r.AxtriaSalesIQTM__Zip_Type__c, Country_Code__c, Geo_Level__c, AxtriaSalesIQTM__Geography_Type__c FROM AxtriaSalesIQTM__Geography__c]; 
        }
        List<GeographyView> geographyView = new list<GeographyView>();
        System.debug('geography list size ' + geographyList.size());
        for(AxtriaSalesIQTM__Geography__c geography:geographyList){
            GeographyView geoView = new GeographyView();
            geoView.id = geography.Id;
            geoView.geoName = geography.AxtriaSalesIQTM__Zip_Name__c;
            geoView.geoType = geography.AxtriaSalesIQTM__Zip_Type__c;
            geoView.geoId = geography.Name;
            geoView.parentGeoId = geography.AxtriaSalesIQTM__Parent_Zip_Code__c;
            geoView.parentGeoName = geography.AxtriaSalesIQTM__Parent_Zip_Code__r.Name;
            geoView.parentGeoType  = geography.AxtriaSalesIQTM__Parent_Zip_Code__r.AxtriaSalesIQTM__Zip_Type__c;
            geoView.countryCode = geography.Country_Code__c;
            geoView.geoLevel = geography.Geo_Level__c;
            geoView.salesTeam = geography.AxtriaSalesIQTM__Geography_Type__r.Name;
            geographyView.add(geoView);
        }
        
        return geographyView;
    }

    global void upsertGeography(){
        AxtriaSalesIQTM__Geography__c geography = new AxtriaSalesIQTM__Geography__c();
        AxtriaSalesIQTM__Geography__c prntGeo = new AxtriaSalesIQTM__Geography__c();

        if(currentGeo.id != null){
            prntGeo = [SELECT Id, Name, AxtriaSalesIQTM__Zip_Name__c,AxtriaSalesIQTM__Parent_Zip_Code__r.Name, AxtriaSalesIQTM__Zip_Type__c,AxtriaSalesIQTM__Parent_Zip_Code__c, AxtriaSalesIQTM__Parent_Zip_Code__r.AxtriaSalesIQTM__Zip_Type__c, Country_Code__c, Geo_Level__c, AxtriaSalesIQTM__Geography_Type__c,AxtriaSalesIQTM__Geography_Type__r.Name,AxtriaSalesIQTM__Geography_Type__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c FROM AxtriaSalesIQTM__Geography__c WHERE ID = :currentGeo.id]; 
            
            geography.AxtriaSalesIQTM__Parent_Zip__c = prntGeo.AxtriaSalesIQTM__Parent_Zip_Code__r.Name; //Need to ask
            geography.Geography_Type1__c = prntGeo.AxtriaSalesIQTM__Geography_Type__r.Name;
            geography.Country_Code__c =  prntGeo.AxtriaSalesIQTM__Geography_Type__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
        }
        System.debug('createPublishFlag  '+ createPublishFlag);
        if(createPublishFlag!=null && createPublishFlag == true){

            if(currentGeo!=null){

                System.debug('createGeoReq.geoId : '+createGeoReq.geoId+', createGeoReq.geoName : '+createGeoReq.geoName+', createGeoReq.geoType : '+createGeoReq.geoType);
                if(createGeoReq.geoId == null || createGeoReq.geoId.length() ==0 || createGeoReq.geoId==null || createGeoReq.geoName.length()==0 || createGeoReq.geoType == null){
                    errorMsg = 'Please provide data for all required fields !!!';
                    System.debug('errorMsg is '+errorMsg);

                }else{
                    if(currentGeo.geoLevel != null)
                        geography.Geo_Level__c = String.valueOf(Integer.valueOf(currentGeo.geoLevel) -1);

                    List<AxtriaSalesIQTM__Geography__c> duplicateGeo = [SELECT Id FROM AxtriaSalesIQTM__Geography__c WHERE Name =:createGeoReq.geoId];
                    if(duplicateGeo!=null && duplicateGeo.size()>0){
                        successMsg = 'Geography ID already Exist!! Please use different Geo ID.';
                    }else{
                        geography.AxtriaSalesIQTM__Zip_Name__c = createGeoReq.geoName;
                        geography.AxtriaSalesIQTM__Zip_Type__c = createGeoReq.geoType;
                        geography.Name = createGeoReq.geoId;
                        geography.AxtriaSalesIQTM__Parent_Zip_Code__c = createGeoReq.id;
                        geography.AxtriaSalesIQTM__Neighbor_Geography__c  = '0';

                        System.debug('Inserted Geography is ' + geography);
                        Insert geography;
                        successMsg = 'Geography Created Successfully.';
                    }
                    System.debug('Success Msg :: ' + successMsg);
                }
            }
            System.debug('when CurrentGeo is null Success Msg :: ' + successMsg);
            createPublishFlag = false;
        }else if(reNamePublishFlag!=null && reNamePublishFlag == true){
            prntGeo.AxtriaSalesIQTM__Zip_Name__c = currentGeo.geoName;
            prntGeo.Name = currentGeo.geoId;

            Update prntGeo;
            successMsg = 'Geography Renamed Successfully.';
            reNamePublishFlag = false;
        }else if(hieChangePublishFlag!=null && hieChangePublishFlag == true){
            prntGeo.AxtriaSalesIQTM__Parent_Zip_Code__c = currentGeo.parentGeoId;

            Update prntGeo;
            successMsg = 'Geography Hierarchy changed Successfully.';
            hieChangePublishFlag = false;
        }


        //this.hidePopup();
        if(errorMsg!=null && errorMsg.length()>0){
            errorPublishFlag = true;
        }else{
            messagePublishFlag = true;
        }
        System.debug('errorPublishFlag : '+errorPublishFlag+', messagePublishFlag : '+messagePublishFlag);
        //geoTypeView.clear();

    }

    global void searchPrntGeoId(){
        if(currentGeo.parentGeoName != null){
            List<AxtriaSalesIQTM__Geography__c> prntZipCode = [SELECT AxtriaSalesIQTM__Parent_Zip_Code__c, AxtriaSalesIQTM__Parent_Zip_Code__r.Name FROM AxtriaSalesIQTM__Geography__c WHERE Geo_Level__c =:currentGeo.geoLevel AND AxtriaSalesIQTM__Geography_Type__r.Name =:currentGeo.salesTeam AND AxtriaSalesIQTM__Parent_Zip_Code__r.Name =:currentGeo.parentGeoName];
            if(prntZipCode != null && prntZipCode.size()>0){
                currentGeo.parentGeoName = prntZipCode[0].AxtriaSalesIQTM__Parent_Zip_Code__r.Name;
                currentGeo.parentGeoId = prntZipCode[0].AxtriaSalesIQTM__Parent_Zip_Code__c;
                prntGeoFoundMsg = 'Found';
            }else{
                prntGeoFoundMsg = 'Not Found';
            }
        }
    }
    
    global class GeographyView {
        public String id {get;set;}
        public String geoId {get;set;}
        public String geoName {get;set;}
        public String geoType {get;set;}
        public String parentGeoId {get;set;}
        public String parentGeoType {get;set;}
        public String countryCode {get;set;}
        public String geoLevel {get;set;}
        public String salesTeam {get;set;}
        public String parentGeoName {get;set;}
    }
     
    
    global void initiatePopup() {
        if(popupOption.equals('1')){ //CREATE PopupAxtriaSalesIQTM__Geography_Type__c

            Schema.DescribeFieldResult fieldResult = AxtriaSalesIQTM__Geography__c.AxtriaSalesIQTM__Zip_Type__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            geoZipTypeView = new List<SelectOption>();
            for( Schema.PicklistEntry f : ple)
               {
                  geoZipTypeView.add(new SelectOption(f.getLabel(), f.getValue()));
               }  

               createGeoReq.countryCode = currentGeo.countryCode;
               createGeoReq.geoId = '';//currentGeo.geoId;
               //createGeoReq.geoLevel = currentGeo.geoLevel;
               createGeoReq.geoName = '';//currentGeo.geoName;
               createGeoReq.geoType = '';//currentGeo.geoType;
               createGeoReq.parentGeoId = currentGeo.geoId;
               createGeoReq.parentGeoType = currentGeo.geoType;
               createGeoReq.salesTeam = currentGeo.salesTeam;
               createGeoReq.parentGeoName = currentGeo.geoName;
            createPublishFlag = true;
        }
        if(popupOption.equals('2')) //RENAME Popup
            reNamePublishFlag = true;
        if(popupOption.equals('3')){ //HIE_CHANGE Popup

            hieChangePublishFlag = true;
        }
    }

    global void hidePopup() {
        if(createPublishFlag != null)
            createPublishFlag = false;
        if(reNamePublishFlag != null)
            reNamePublishFlag = false;
        if(hieChangePublishFlag != null)
            hieChangePublishFlag = false;
        if(messagePublishFlag != null){
            messagePublishFlag = false;
            successMsg = '';
        }
        if(errorPublishFlag != null){
            errorPublishFlag = false;
            errorMsg = '';
        }
        //this.populateGeo();
    }
}