global with sharing class ScheduleBatchConsumeAlert implements Schedulable{
    
    global void execute(SchedulableContext ctx) {
         //SendAlerts(); 
    }
        
    /*@future (callout=true)  
    public static void SendAlerts(){
        Integer processedBatchLimit = 7000; 
        boolean isAlert = false;
       
        String[] recipientsMail =   new String[] {'vikas.pandey@axtria.com','Yennapusa.Reddy@axtria.com','Sahil.Mahajan@Axtria.com','Bichiter.Singh@axtria.com','INCHGLOBALSALESIQSUPPORT@astrazeneca.com'};
        String errorMsg = '';
        String requestUrl = '/750';
        Integer processedBatchesValue;
            
        //Get the Monitor Bulk Data Load Jobs Page, prepare to scrape
        PageReference pg = new PageReference( requestUrl );
        String htmlCode = pg.getContent().toString();
        
        //Find the pattern 
        Pattern patternToSearch = Pattern.compile('Your organization has processed <strong>\\d+(,\\d+|\\d*)'); 
        Matcher matcherPattern = patternToSearch.matcher(htmlCode);
        
        if(matcherPattern.find()){
           String matchedStr =  htmlCode.substring(matcherPattern.start(), matcherPattern.end());
           Pattern subpatternToSearch = Pattern.compile('\\d+(,\\d+|\\d*)'); 
           Matcher matcherPatternProcessedBatch = subpatternToSearch.matcher(matchedStr);
           String processedBatches;           
           if ( matcherPatternProcessedBatch.find() ) {  
                processedBatches = matchedStr.substring(matcherPatternProcessedBatch.start(), matcherPatternProcessedBatch.end());
                processedBatches = processedBatches.remove(',');
                processedBatchesValue = Integer.valueof(processedBatches); 
               if(processedBatchesValue >= processedBatchLimit){
                   isAlert = true;
               } //else number of consume batches is under custom limit
           }else{
               errorMsg = 'Error occurs while fetching number of batches proccessed ';
           }
        }else{
            errorMsg = 'Error occurs while fetching Monitor Bulk data Jobs page';
        }
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String bodyMessage = '';
        
        //If consumed batches are greater than processedBatchLimit
        if(isAlert){
            mail.setSubject('Notice ALERT For Bulk Data Load Jobs Batches consumption : ' + UserInfo.getOrganizationName() + ' - ID: '+ UserInfo.getOrganizationId());
            mail.setToAddresses(recipientsMail);
            mail.setSenderDisplayName('Batches consumption alert');
            bodyMessage  = ' Bulk Api Batch consumption alert has been generated.';
            bodyMessage  += '</br> </br>Your organization has processed : <b>' + processedBatchesValue + '</b> Batches.';
            mail.setHtmlBody(bodyMessage);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        //If error occured during fetching batch consumption value
        else if(errorMsg.length() != 0){
            mail.setSubject(' Error ALERT For Bulk Data Load Jobs Batches consumption : ' + UserInfo.getOrganizationName() + ' - ID: '+ UserInfo.getOrganizationId());
            mail.setToAddresses(recipientsMail);
            mail.setSenderDisplayName('Batches consumption alert');
            mail.setHtmlBody(errorMsg);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        //else no need to send alert mail
    }*/
    
    
}