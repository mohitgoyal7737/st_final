@isTest
private class BatchEmployeeChangeStatusForOutboundTest {
     @testSetup 
    static void setup() {
        List<CurrentPersonFeed__c> ScopeEmpStagingList = new List<CurrentPersonFeed__c>();
        Map<string,CurrentPersonFeed__c> empStagingMap = new Map<string,CurrentPersonFeed__c>();
         for (Integer i=0;i<5;i++) {
            ScopeEmpStagingList.add(new CurrentPersonFeed__c(First_Name__c='A '+i,isRejected__c=false,PRID__c='x'+i));
        } 
        
        insert ScopeEmpStagingList;
       List<AxtriaSalesIQTM__Employee__c> employeeOldStatus=TestDataFactoryAZ.createEmployees('x','x','x','x','x',System.today(),5);
        insert employeeOldStatus;
        List<AxtriaSalesIQTM__Employee_Status__c> es=TestDataFactoryAZ.createEmpStatus(employeeOldStatus,'Active');
        insert es;
        set<string> emplList= new set<string>();
        
        for(CurrentPersonFeed__c currentStatus : ScopeEmpStagingList)
        {
            empStagingMap.put(currentStatus.PRID__c,currentStatus);     
            emplList.add(currentStatus.PRID__c);
            
        }
        
    }
    static testmethod void test() {        
            
        map<string,Date> mapEmp2Event=new map<String,Date>(); 
             mapEmp2Event.put('a',System.today());
        Test.startTest();
        BatchEmployeeChangeStatusForOutbound usa = new BatchEmployeeChangeStatusForOutbound(mapEmp2Event);
        Id batchId1 = Database.executeBatch(usa);
        Test.stopTest();
    }
}