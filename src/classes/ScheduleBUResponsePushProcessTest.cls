/*Author - Himanshu Tariyal(A0994)
Date : 25th January 2018*/
@isTest
private class ScheduleBUResponsePushProcessTest 
{
    static testMethod void firstTest() 
    {
        /*Create initial test data for all the objs reqd.*/
        AxtriaSalesIQTM__Organization_Master__c aom = new AxtriaSalesIQTM__Organization_Master__c(AxtriaSalesIQTM__Org_Level__c='Global',AxtriaSalesIQTM__Parent_Country_Level__c=true);
        insert aom;
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='Italy',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active');
        insert country;
        
        /*Cycle__c cycle = new Cycle__c(Name='Test Cycle',Country__c = country.id);
        insert cycle;*/
        
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='Test Team');
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU',AxtriaSalesIQTM__Team__c=team.id/*,Cycle__c=cycle.id*/);
        ti.isVeevaSurveyScheduled__c = true;
        insert ti;
        
        AxtriaSalesIQTM__Team_Instance__c ti2 = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU2',AxtriaSalesIQTM__Team__c=team.id/*,Cycle__c=cycle.id*/);
        insert ti2;
        
        ScheduledTeamInstances__c sti = new ScheduledTeamInstances__c(Name='abcd');
        insert sti;
        
        System.Test.startTest();
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        
        Datetime dt = Datetime.now().addMinutes(1);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
        System.schedule('Test_SCSP_Veeva_Schedular_to_BU', CRON_EXP, new ScheduleBUResponsePushProcess());    
        System.Test.stopTest();
    }
    static testMethod void secondTest() 
    {
        /*Create initial test data for all the objs reqd.*/
        AxtriaSalesIQTM__Organization_Master__c aom = new AxtriaSalesIQTM__Organization_Master__c(AxtriaSalesIQTM__Org_Level__c='Global',AxtriaSalesIQTM__Parent_Country_Level__c=true);
        insert aom;
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='Italy',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active');
        insert country;
        
        /*Cycle__c cycle = new Cycle__c(Name='Test Cycle',Country__c = country.id);
        insert cycle;*/
        
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='Test Team');
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU',AxtriaSalesIQTM__Team__c=team.id/*,Cycle__c=cycle.id*/);
        ti.isVeevaSurveyScheduled__c = true;
        insert ti;
        
        AxtriaSalesIQTM__Team_Instance__c ti2 = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU2',AxtriaSalesIQTM__Team__c=team.id/*,Cycle__c=cycle.id*/);
        insert ti2;
        
        ScheduledTeamInstances__c sti = new ScheduledTeamInstances__c(Name='abcd');
        insert sti;
        AxtriaSalesIQTM__ETL_Config__c etl = TestDataFactory.configureETL();
        etl.Name = 'ATL Delta Job';
        insert etl;
        System.Test.startTest();
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        TalendHitATLJobQueueable queueable = new TalendHitATLJobQueueable();
        System.enqueueJob(queueable);
        System.Test.stopTest();
    }
    static testMethod void thirdTest() 
    {
        /*Create initial test data for all the objs reqd.*/
        AxtriaSalesIQTM__Organization_Master__c aom = new AxtriaSalesIQTM__Organization_Master__c(AxtriaSalesIQTM__Org_Level__c='Global',AxtriaSalesIQTM__Parent_Country_Level__c=true);
        insert aom;
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='Italy',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active');
        insert country;
        
        /*Cycle__c cycle = new Cycle__c(Name='Test Cycle',Country__c = country.id);
        insert cycle;*/
        
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='Test Team');
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU',AxtriaSalesIQTM__Team__c=team.id/*,Cycle__c=cycle.id*/);
        ti.isVeevaSurveyScheduled__c = true;
        insert ti;
        
        AxtriaSalesIQTM__Team_Instance__c ti2 = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU2',AxtriaSalesIQTM__Team__c=team.id/*,Cycle__c=cycle.id*/);
        insert ti2;
        
        ScheduledTeamInstances__c sti = new ScheduledTeamInstances__c(Name='abcd');
        insert sti;
        AxtriaSalesIQTM__ETL_Config__c etl = TestDataFactory.configureETL();
        etl.Name = 'Gas Assignment';
        insert etl;
        System.Test.startTest();
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        Datetime dt = Datetime.now().addMinutes(1);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
        System.schedule('Test_SCSP_Veeva_Schedular_to_BU', CRON_EXP, new talendGasAssignmentHit());    
        System.Test.stopTest();
    }
}