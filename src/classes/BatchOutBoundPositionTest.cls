@isTest
private class BatchOutBoundPositionTest {
     @testSetup 
    static void setup() {
 
              AxtriaSalesIQTM__Organization_Master__c org=new AxtriaSalesIQTM__Organization_Master__c();
        org.name='US';
        org.AxtriaSalesIQTM__Org_Level__c='Global';
        org.AxtriaSalesIQTM__Parent_Country_Level__c=true;
        insert org;
        String orgId=org.id;
        List<AxtriaSalesIQTM__Country__c> countrylist = new List<AxtriaSalesIQTM__Country__c>();
        List<AxtriaSalesIQTM__Country__c> country = new List<AxtriaSalesIQTM__Country__c>();
          country.add(new AxtriaSalesIQTM__Country__c(AxtriaSalesIQTM__Country_Code__c='US',AxtriaSalesIQTM__Parent_Organization__c=orgId, AxtriaSalesIQTM__Status__c='Active'));
        insert country;
        AxtriaSalesIQTM__Team__c t=new AxtriaSalesIQTM__Team__c();
        t.Name='xx';
        insert t;
        String teamId=t.id;
        AxtriaSalesIQTM__Team__c t1=new AxtriaSalesIQTM__Team__c();
        t1.Name='yy';
        insert t1;
        String parentteamId=t1.id;
        AxtriaSalesIQTM__Team_Instance__c ti=new AxtriaSalesIQTM__Team_Instance__c();
        ti.AxtriaSalesIQTM__Team__c=teamId;
        ti.AxtriaSalesIQTM__Alignment_Period__c='current';
        insert ti;
        String teamInstId=ti.id;
         AxtriaSalesIQTM__Position__c pos1=new AxtriaSalesIQTM__Position__c();
        pos1.AxtriaSalesIQTM__Team_iD__c=parentteamId;
        insert pos1;
        String parentposId=pos1.id;
        AxtriaSalesIQTM__Position__c pos=new AxtriaSalesIQTM__Position__c();
        pos.AxtriaSalesIQTM__Team_iD__c=teamId;
        pos.AxtriaSalesIQTM__Team_Instance__c=teamInstId;
        pos.AxtriaSalesIQTM__Parent_Position__c=parentposId;
        insert pos;
    }
    static testmethod void test() {        
        Test.startTest();
         AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
        team.Name = 'Specialty';
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamIns = new AxtriaSalesIQTM__Team_Instance__c();
        teamIns.AxtriaSalesIQTM__Team__c = team.id;
        teamIns.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        teamIns.Name = 'abc';
        insert teamIns;
        BatchOutBoundPosition usa = new BatchOutBoundPosition();
        Id batchId = Database.executeBatch(usa);
      //  BatchOutBoundPositionProduct obj = new BatchOutBoundPositionProduct();
        
        Test.stopTest();
        // after the testing stops, assert records were updated 
    }
}