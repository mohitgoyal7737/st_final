global class BatchUserInactive implements Database.Batchable<sObject>,database.stateful,Schedulable
 {
    public String query;
    public Set<String> prid;
    public Set<String> userprid;
    public string status='terminated';

    global BatchUserInactive() 
    {
        query='Select id,SIQ_PRID__c,SIQ_Employee_ID__c,SIQ_HR_Status__c from SIQ_Employee_Master__c where SIQ_HR_Status__c=:status';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        system.debug('===================Query:::'+query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<SIQ_Employee_Master__c> scope) 
    {
        system.debug('++query++'+query);
        prid=new Set<String>();
        userprid=new Set<String>();
        for(SIQ_Employee_Master__c emp:scope)
        {
            prid.add(emp.SIQ_PRID__c);
        }
        system.debug('++prid++'+prid);

        List<User> us=[SELECT FederationIdentifier,Id,Country,IsActive FROM User where FederationIdentifier in : prid];
        for(User u:us)
        {
             system.debug('++helloo++');
             u.IsActive=False;
        }
        update us;

    }

    global void finish(Database.BatchableContext BC) 
    {

    }
   global void execute(SchedulableContext sc)
    {
        database.executeBatch(new BatchUserInactive(),2000);       
    }
}