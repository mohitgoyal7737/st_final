global with sharing class BatchCreateParentPacp implements Database.Batchable<sObject>,Database.stateful, Schedulable {
    public String query;
    public string Ids;
    Boolean thirdFlag;
    Boolean flag= true;
    Boolean fourthFlag = true;
    public string selectedteaminstance {get;set;}
    public set<string>uniquevalues{get;set;}
    public string ruleidis {get;set;}
    public boolean directFlag=false;
    public boolean directFlag2=false;

    //Added by HT(A0944) on 12th June 2020
    public String alignNmsp;
    public String batchName;
    public String callPlanLoggerID;
    public Boolean proceedNextBatch = true;

    global BatchCreateParentPacp(String teaminstance,string ruleID) {
        selectedteaminstance = '';
        uniquevalues = new set<string>();
        ruleidis = '';
        ruleidis = ruleID;
        query = 'Select id,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c!=null and Parent_PACP__c =null ';
        if(teaminstance!=null){
            selectedteaminstance = teaminstance;
            query += ' and AxtriaSalesIQTM__Team_Instance__c =:selectedteaminstance';
            
        }
        system.debug('=======The Query is:'+query);
    }

    global BatchCreateParentPacp(String teaminstance) {
        directFlag=true;
        selectedteaminstance = '';
        uniquevalues = new set<string>();
        //ruleidis = '';
        //ruleidis = ruleID;
        query = 'Select id,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c!=null and Parent_PACP__c =null ';
        if(teaminstance!=null){
            selectedteaminstance = teaminstance;
            query += ' and AxtriaSalesIQTM__Team_Instance__r.Name =:selectedteaminstance';
            
        }
        system.debug('=======The Query is:'+query);
    }

    //Added by HT(A0944) on 12th June 2020
    global BatchCreateParentPacp(String teaminstance,String loggerID,String alignNmspPrefix) 
    {
        directFlag=true;
        selectedteaminstance = '';
        uniquevalues = new set<string>();
        //ruleidis = '';
        //ruleidis = ruleID;

        callPlanLoggerID = loggerID;
        alignNmsp = alignNmspPrefix;
        SnTDMLSecurityUtil.printDebugMessage('callPlanLoggerID--'+callPlanLoggerID);
        SnTDMLSecurityUtil.printDebugMessage('alignNmsp--'+alignNmsp);

        query = 'Select id,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Team_Instance__c '+
                'from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c!=null and Parent_PACP__c =null ';
        if(teaminstance!=null){
            selectedteaminstance = teaminstance;
            query += ' and AxtriaSalesIQTM__Team_Instance__r.Name =:selectedteaminstance';
            
        }
        system.debug('=======The Query is:'+query);
    }
    
    global BatchCreateParentPacp(String teaminstance,string Ids,Boolean thirdFlag) {
        directFlag=true;
        this.Ids = Ids;
        this.thirdFlag = thirdFlag;
        selectedteaminstance = '';
        uniquevalues = new set<string>();
        //ruleidis = '';
        //ruleidis = ruleID;
        query = 'Select id,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c!=null and Parent_PACP__c =null ';
        if(teaminstance!=null){
            selectedteaminstance = teaminstance;
            query += ' and AxtriaSalesIQTM__Team_Instance__r.Name =:selectedteaminstance';
            
        }
        system.debug('=======The Query is:'+query);
    }
    
    global BatchCreateParentPacp() {
        directFlag2=true;
        uniquevalues = new set<string>();
        query = 'Select id,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c!=null and Parent_PACP__c =null ';
      }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scope) 
    {
        try
        {
            list<Parent_PACP__c>insertparentlist = new list<Parent_PACP__c>();
            for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : scope){
                string key=pacp.AxtriaSalesIQTM__Account__c+'_'+pacp.AxtriaSalesIQTM__Position__c+'_'+pacp.AxtriaSalesIQTM__Team_Instance__c;
                if(!uniquevalues.contains(key)){
                    Parent_PACP__c parentpacp = new Parent_PACP__c();
                    parentpacp.Account__c = pacp.AxtriaSalesIQTM__Account__c;
                    parentpacp.Position__c = pacp.AxtriaSalesIQTM__Position__c;
                    parentpacp.Team_Instance__c = pacp.AxtriaSalesIQTM__Team_Instance__c;
                    parentpacp.External_Id__c = pacp.AxtriaSalesIQTM__Account__c+'_'+pacp.AxtriaSalesIQTM__Position__c+'_'+pacp.AxtriaSalesIQTM__Team_Instance__c;
                    insertparentlist.add(parentpacp);
                    uniquevalues.add(key);
                }
            }
            
            if(insertparentlist !=null && insertparentlist.size()>0){
                system.debug('=====insertparentlist======='+insertparentlist);
                upsert insertparentlist External_Id__c;
            }
        }
        catch(Exception e)
        {
            //Added by HT(A0994) on 12th June 2020
            SnTDMLSecurityUtil.printDebugMessage('Error in BatchCreateParentPacp : execute()-->'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack Trace-->'+e.getStackTraceString());
            proceedNextBatch = false;
            SnTDMLSecurityUtil.printDebugMessage('callPlanLoggerID--'+callPlanLoggerID);

            if(callPlanLoggerID!=null && callPlanLoggerID!=''){
                SalesIQUtility.updateCallPlanLoggerRecord(callPlanLoggerID,'Failed at creating Parent of Cycle Plan data','Error',alignNmsp);
            }
            //End
        } 
    }

     global void execute(SchedulableContext sc){
        /*List<Veeva_Job_Scheduling__c> fetchAllCountries = [Select Country__r.Name from Veeva_Job_Scheduling__c where Load_Type__c != 'No Load'];
            system.debug(fetchAllCountries + 'Countr ');
            
        for(Veeva_Job_Scheduling__c Coun : fetchAllCountries){
            system.debug(fetchAllCountries + 'Coun');
            database.executeBatch(new BatchCreateParentPacp(Coun.Country__r.Name),10);     
        } */
        /*Replaced Veeva_Job_Scheduling__c with field at country*/
        List<AxtriaSalesIQTM__Country__c> fetchAllCountries = [Select Name from AxtriaSalesIQTM__Country__c where Load_Type__c != 'No Load'];
            system.debug(fetchAllCountries + 'Countr ');
            
        for(AxtriaSalesIQTM__Country__c Coun : fetchAllCountries){
            system.debug(fetchAllCountries + 'Coun');
            database.executeBatch(new BatchCreateParentPacp(Coun.Name),10);     
        } 
    }  

    global void finish(Database.BatchableContext BC) 
    {
        try
        {
            if(directFlag2==true){
                Database.executeBatch(new BatchUpdateParentPacpOnPacp(), 2000);
            }
            else if(directFlag==true)
            {
                if(callPlanLoggerID!=null && callPlanLoggerID!='')
                {
                    if(proceedNextBatch)
                        Database.executeBatch(new BatchUpdateParentPacpOnPacp(selectedteaminstance,callPlanLoggerID,alignNmsp), 2000);
                }
                else
                    Database.executeBatch(new BatchUpdateParentPacpOnPacp(selectedteaminstance), 2000);     
            }
            else{
                Database.executeBatch(new BatchUpdateParentPacpOnPacp(selectedteaminstance,ruleidis), 2000);
            }
        }
        catch(Exception e)
        {
            //Added by HT(A0994) on 12th June 2020
            SnTDMLSecurityUtil.printDebugMessage('Error in BatchCreateParentPacp : finish()-->'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack Trace-->'+e.getStackTraceString());
            SnTDMLSecurityUtil.printDebugMessage('callPlanLoggerID--'+callPlanLoggerID);

            if(callPlanLoggerID!=null && callPlanLoggerID!=''){
                SalesIQUtility.updateCallPlanLoggerRecord(callPlanLoggerID,'Failed at creating Parent of '+
                                                            'Cycle Plan data','Error',alignNmsp);
            }
            //End
        }
    }
}