global with sharing class BatchDeleteHCOSegmentPrevResult implements Database.Batchable<sObject> {

    public String query;
    String selectedSourceRule;
    String selectedDestinationRule;
    //Added by Chirag Ahuja for STIMPS-286
    public String teamInsId;
    public String product;

    global BatchDeleteHCOSegmentPrevResult(String sourceRuleId,String destinationRuleId) {
        this.selectedDestinationRule = destinationRuleId;
        this.selectedSourceRule = sourceRuleId;
        query = 'Select Id from Account_Compute_Final__c where Measure_Master__c =:selectedDestinationRule WITH SECURITY_ENFORCED';
        /*if(objAPIName != '' && objAPIName != null){
            this.objAPIName = objAPIName;
            query += objAPIName;
        }
        if(condition != '' && condition != null){
            this.condition = condition;
            query += ' ' + condition;
        }*/
        this.query = query;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Sobject> scope) {

        if(scope.size()>0 && scope!=null){
            if(Account_Compute_Final__c.sObjectType.getDescribe().isDeletable()){
                Database.DeleteResult[] srList = Database.delete(scope, false);
                Database.EmptyRecycleBinResult[] emptyRecycleBinResults = DataBase.emptyRecycleBin(scope); 
            }
            else{
                SnTDMLSecurityUtil.printDebugMessage('You dont have permission to delete Account_Compute_Final__c','BatchDeleteHCOSegmentPrevResult');
            }
        }
    }

    global void finish(Database.BatchableContext BC) {
        //Added by Chirag Ahuja for STIMPS-286
        Measure_Master__c mm = [Select Team_Instance__r.Name, Brand_Lookup__r.Veeva_External_ID__c from Measure_Master__c where Id =: selectedDestinationRule WITH SECURITY_ENFORCED];
        System.debug('=============mm==========='+mm);
        teamInsId = mm.Team_Instance__r.Name;
        product = mm.Brand_Lookup__r.Veeva_External_ID__c;
        //Database.executeBatch(new BatchCreateHCOParameterValueMap(selectedSourceRule,selectedDestinationRule),2000);
        //call delete staging survey data.
        Delete_Staging_Survey_Data batch = new Delete_Staging_Survey_Data(selectedSourceRule,selectedDestinationRule,teamInsId,product,'HCO',true);
        Database.executeBatch(batch, 500);
    }
}