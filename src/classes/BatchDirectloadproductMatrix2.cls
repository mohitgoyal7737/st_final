global with sharing class BatchDirectloadproductMatrix2 implements Database.Batchable<sObject> {
   
    global Database.QueryLocator start(Database.BatchableContext BC) {
        //String query = 'SELECT Id,Name,Account_ID__c,Scenario_Name__c,POSITION__c,Objective__c,Target__c,Product_Name__c,Workspace__c,Adoption__c,Potential__c,Product_ID__c,Segment__c FROM Master_List_AccProd__c where Status__c = \'New\'';


        String query = 'SELECT Id,Name,CreatedDate,Account__c,Account__r.AccountNumber,Account_Name__c,AccountProductKey__c,Adoption__c,Objective__c,POSITION__c,Potential__c,Previous_Cycle_Calls__c,Product__c,Product_ID__c,Scenario_Name__c,Segment__c ,Target__c,Team_Instance__c,Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,Workspace__c FROM Master_List_AccProd__c where Status__c = \'New\' AND Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = \'Current\' ';


        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Master_List_AccProd__c> scope) {
        
        Set<String> workSpace_set = new Set<String>();
        Map<String,String> workSpace_ProductID = new Map<String,String>();
        Set<String> productTeamInst_Set = new Set<String>();

        List<Master_List_AccProd__c> updateScope = new List<Master_List_AccProd__c>();

        for(Master_List_AccProd__c work : scope){
            /*if(work.Workspace__c!=null){
                workSpace_set.add(work.Workspace__c);
            }*/

                String product_Name = work.Product__c;
                productTeamInst_Set.add(product_Name.toUpperCase()+'_'+work.Team_Instance__c);
            

            /*if(work.Product_ID__c!=null){
                workSpace_ProductID.add(work.Product_ID__c);
            }*/
        }


        for(Product_Catalog__c prodCat : [Select Name,Team_Instance__c,Veeva_External_ID__c from Product_Catalog__c WHERE Team_Instance__c != NULL] )
        {
            String product_Name = prodCat.Name;
            String key = product_Name.toUpperCase()+'_'+prodCat.Team_Instance__c;

            if(productTeamInst_Set.contains(key))
            {
                if(!workSpace_ProductID.containsKey(key))
                    {
                        workSpace_ProductID.put(key,prodCat.Veeva_External_ID__c);
                    }
            }
        }
        SnTDMLSecurityUtil.printDebugMessage('<><><<<<<<<<<<<<<<<<<>>>'+workSpace_ProductID);
        /*List<AxtriaSalesIQTM__Workspace__c> partworkspacelist = [select Id,Name,Countrycode__c from AxtriaSalesIQTM__Workspace__c where Name in: workSpace_set];
        Map<String,String> workspacemap = new Map<String,String>(); */
     
        /*for(AxtriaSalesIQTM__Workspace__c WS : partworkspacelist){
            workspacemap.put(WS.Name,WS.Countrycode__c);
        }*/
    
         List<ProductToDetailGroup__c> partproductdetailslist = [select Id,Veeva_External_ID__c,Detail_Group__c from ProductToDetailGroup__c where Veeva_External_ID__c in: workSpace_ProductID.values()];
         Map<String,String> productdetailsmap = new Map<String,String >(); 
     
        for(ProductToDetailGroup__c pdrec : partproductdetailslist){
            productdetailsmap.put(pdrec.Veeva_External_ID__c,pdrec.Detail_Group__c);
        }
        SnTDMLSecurityUtil.printDebugMessage('<><><><><><>><'+productdetailsmap);
    
    
        List<SIQ_Product_Metrics_vod_O__c> partOrderList = New List<SIQ_Product_Metrics_vod_O__c>();
           
        for(Master_List_AccProd__c fixNon_Target : scope){ 
    
             SIQ_Product_Metrics_vod_O__c part_ProductMatrix = New SIQ_Product_Metrics_vod_O__c();
             part_ProductMatrix.Name = fixNon_Target.Name;
             part_ProductMatrix.SIQ_Account_vod__c = fixNon_Target.Account__r.AccountNumber;
             part_ProductMatrix.SIQ_Account_Number__c= fixNon_Target.Account__r.AccountNumber;
             part_ProductMatrix.SIQ_Adoption_AZ__c = fixNon_Target.Adoption__c != null ? fixNon_Target.Adoption__c : '0';
             part_ProductMatrix.SIQ_Potential_AZ__c = fixNon_Target.Potential__c != null ? fixNon_Target.Potential__c : '0';
             String product_Name = fixNon_Target.Product__c;
             String key = product_Name.toUpperCase()+'_'+fixNon_Target.Team_Instance__c;
             String product_code = workSpace_ProductID.get(key);

             SnTDMLSecurityUtil.printDebugMessage('<><><>product_code<><><><><'+product_code);

             part_ProductMatrix.SIQ_Products_vod__c = product_code;
             part_ProductMatrix.SIQ_Segment__c = fixNon_Target.Segment__c;
             part_ProductMatrix.SIQ_Country__c = fixNon_Target.Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
             part_ProductMatrix.SIQ_Product_Name__c = fixNon_Target.Product__c;
             part_ProductMatrix.Detail_Group__c=productdetailsmap.get(product_code);
             part_ProductMatrix.SIQ_Status__c='Updated';
             part_ProductMatrix.SIQ_Updated_Date__c = System.now();
             part_ProductMatrix.SIQ_Created_Date__c = fixNon_Target.CreatedDate;
             part_ProductMatrix.SIQ_External_ID__c=fixNon_Target.Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c + '_' + fixNon_Target.Account__r.AccountNumber + '_' + product_code + '_' + productdetailsmap.get(product_code);
             partOrderList.add(part_ProductMatrix);
        }

        SnTDMLSecurityUtil.printDebugMessage('<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'+partOrderList);

        if(partOrderList.size() > 0)
           upsert partOrderList part_ProductMatrix.SIQ_External_ID__c;

       for(Master_List_AccProd__c inputRec : scope)
       {
          inputRec.Status__c = 'Processed';
          updateScope.add(inputRec);
       }
       if(partOrderList.size() > 0){
           //update updateScope;
           SnTDMLSecurityUtil.updateRecords(updateScope, 'BatchDirectloadproductMatrix2');
       }
  }  
    
    global void finish(Database.BatchableContext BC) {
     //database.executeBatch(new batchCustmoreSegmentOutboundload2(), 2000); //commented due to object purge activity A1450
    }
}