global class update_AccTerr implements Database.Batchable<sObject>, Database.Stateful  {
    
    global string query ;
    global string teamID;
    global string teamInstance;
    global string teamName;
    global list<AxtriaSalesIQTM__Team__c> temlst=new list<AxtriaSalesIQTM__Team__c>();
    global list<temp_Acc_Terr__c> accTerrlist=new list<temp_Acc_Terr__c>();
    Global Id BC {get;set;}
    global string country;
    global list<AxtriaSalesIQTM__Team_Instance__c> countrylst=new list<AxtriaSalesIQTM__Team_Instance__c>();
    
    global update_AccTerr(String Team,String TeamIns){
        
       /* teamInstance=TeamIns;
        teamID=Team;
        temlst=[select name from AxtriaSalesIQTM__Team__c where id=:teamID];
        countrylst =[select AxtriaSalesIQTM__Country__c from AxtriaSalesIQTM__Team_Instance__c where id=:teamInstance];
        country = countrylst[0].AxtriaSalesIQTM__Country__c;
        teamName=temlst[0].name;
        System.debug('======team ====='+teamName);
        System.debug('======team ID ====='+teamID);
        System.debug('======teamInstance ====='+teamInstance);
        query='SELECT id,Territory_ID__c,AccountNumber__c,TeamName__c FROM temp_Acc_Terr__c where TeamName__c=:teamName and status__c = \'New\'';*/
       

    }
    
    global Database.Querylocator start(Database.BatchableContext bc){
        return Database.getQueryLocator(query);
        
    } 
    
    global void execute (Database.BatchableContext BC, List<temp_Acc_Terr__c>accTerrlist){

      /*  System.debug('======team ====='+teamName);
        System.debug('======team ID ====='+teamID);
        System.debug('======teamInstance ====='+teamInstance);
        
        list<String> accNolst=new List<String>();
        for(temp_Acc_Terr__c a:accTerrlist){
            accNolst.add(a.AccountNumber__c);
        }
        
        map<string,string> accmap=new map<string,string>();
        for(Account a: [select AccountNumber,id from Account where AccountNumber in : accNolst and Country_ID__c=:country]){
        accmap.put(a.AccountNumber,a.id);
        }
        
        map<string,string> posmap=new map<string,string>();
        for(AxtriaSalesIQTM__Position__c p: [select AxtriaSalesIQTM__Client_Position_Code__c,id from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_iD__c=:teamID and AxtriaSalesIQTM__Team_Instance__c=:teamInstance]){
        posmap.put(p.AxtriaSalesIQTM__Client_Position_Code__c,p.id);
        }
    
        
        list<temp_Acc_Terr__c> accTerListUpdate = new list<temp_Acc_Terr__c>();
        for(temp_Acc_Terr__c rec:accTerrlist){
            rec.Team__c=teamID;
            rec.Account__c=accmap.get(rec.AccountNumber__c);
            rec.Territory__c=posmap.get(rec.Territory_ID__c);
                
            accTerListUpdate.add(rec);
        }
        update accTerListUpdate;
        
        system.debug('******************************update done************************************');*/
          
    }
    
          global void finish(Database.BatchableContext BC){

             Database.executeBatch(new  update_position_Acc(teamID,teamInstance),500);     //batch to update position_account
         
            
       }
}