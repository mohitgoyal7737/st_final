/*
@author - Chirag Ahuja (A3109)
@description - Test class to test the Delete_Staging_Cust_Survey_Profile.
*/
@isTest
public class Delete_Staging_Cust_Survey_Profile_Test {
    static testMethod void testMethod1() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        
        Account acc = TestDataFactory.createAccount();
        insert acc;
        
        AxtriaSalesIQTM__Team_Instance__c teamInstance = TestDataFactory.createTeamInstance(team);
        insert teamInstance ;
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamInstance, countr);
        pcc.IsActive__c=true;
        insert pcc;
        List<Product_Catalog__c> product =  new List<Product_Catalog__c>();
        
        product.add(pcc);
        
        Staging_Cust_Survey_Profiling__c s = new Staging_Cust_Survey_Profiling__c();
        s.BRAND_NAME__c ='testing';
        insert s;
        Test.startTest();
        System.runAs(loggedInUser){
            List<String> FIELD_LIST = new List<String>{'AxtriaSalesIQTM__Account_Alignment_Type__c','AxtriaSalesIQTM__Account_Target_Type__c'};
                System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(AxtriaSalesIQTM__Position_Account__c.SObjectType, FIELD_LIST, false));
            Delete_Staging_Cust_Survey_Profile obj=new Delete_Staging_Cust_Survey_Profile();
            Database.executeBatch(obj);
            
        }
        Test.stopTest();
    }  
    static testMethod void testMethod2() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        
        Account acc = TestDataFactory.createAccount();
        acc.Profile_Consent__c='yes';
        acc.AxtriaSalesIQTM__Speciality__c ='testspecs';
        
        insert acc;
        
        Account acc2 = TestDataFactory.createAccount();
        acc2.Profile_Consent__c='yes';
        acc2.AxtriaSalesIQTM__Speciality__c ='testspecs';
        acc2.name = 'testacc2';
        insert acc2;
        
        AxtriaSalesIQTM__Team_Instance__c teamInstance = TestDataFactory.createTeamInstance(team);
        teamInstance.AxtriaSalesIQTM__Team__c = team.id;
        teamInstance.Name = 'testteamins';
        teamInstance.Segmentation_Universe__c = 'Full S&T Input Customers';
        insert teamInstance ;
        
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team,teamInstance,countr);
        pcc.Country_Lookup__c = countr.id;
        //pcc.Veeva_External_ID__c = 'test';
        insert pcc;
        
        Measure_Master__c measureMaster= TestDataFactory.createMeasureMaster(pcc,team,teamInstance);
        measureMaster.Team__c = team.id;
        measuremaster.Brand_Lookup__c = pcc.Id;
        measureMaster.Team_Instance__c = teamInstance.id;
        insert measureMaster;
        
        insert  new MetaData_Definition__c (Display_Name__c='ACCESSIBILITY',Source_Field__c='Accessibility_Range__c',Source_Object__c='BU_Response__c',Team_Instance__c=teamInstance.id,Product_Catalog__c=pcc.Id);
        
        AxtriaSalesIQTM__Position__c pos = TestDataFactory.createPosition(team,teamInstance);
        insert pos;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamInstance);
        insert posAccount;
        
        Product_Priority__c pp = new Product_Priority__c();
        pp.CurrencyIsoCode = 'USD';
        pp.Product__c = pcc.id;
        pp.Speciality_ID__c='testSpecs';
        pp.priority__c='P2';
        insert pp;
        BU_Response__c bu = TestDataFactory.createBuResponse(posAccount,pcc,teamInstance,team,acc);
        bu.Team_Instance__c = teamInstance.id;
        bu.Product__c = pcc.id;
        //bu.Brand_c = pcc.id;
        
        insert bu;
        Staging_BU_Response__c sbu = TestDataFactory.createStagingBuResponse(posAccount,pcc,teamInstance,team,acc);
        
        insert sbu;
        
        Staging_Cust_Survey_Profiling__c s = new Staging_Cust_Survey_Profiling__c();
        s.BRAND_NAME__c ='testing';
        insert s;
        Test.startTest();
        System.runAs(loggedInUser){
            List<String> FIELD_LIST = new List<String>{'AxtriaSalesIQTM__Account_Alignment_Type__c','AxtriaSalesIQTM__Account_Target_Type__c'};
                System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(AxtriaSalesIQTM__Position_Account__c.SObjectType, FIELD_LIST, false));
            Delete_Staging_Cust_Survey_Profile obj=new Delete_Staging_Cust_Survey_Profile(teamInstance.Name+';'+pcc.Veeva_External_ID__c);
            Database.executeBatch(obj);
            
        }
        Test.stopTest();
    }  
    static testMethod void testMethod3() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        
        Account acc = TestDataFactory.createAccount();
        acc.Profile_Consent__c='yes';
        acc.AxtriaSalesIQTM__Speciality__c ='testspecs';
        
        insert acc;
        
        Account acc2 = TestDataFactory.createAccount();
        acc2.Profile_Consent__c='yes';
        acc2.AxtriaSalesIQTM__Speciality__c ='testspecs';
        acc2.name = 'testacc2';
        insert acc2;
        
        AxtriaSalesIQTM__Team_Instance__c teamInstance = TestDataFactory.createTeamInstance(team);
        teamInstance.AxtriaSalesIQTM__Team__c = team.id;
        teamInstance.Name = 'testteamins';
        teamInstance.Segmentation_Universe__c = 'Full S&T Input Customers';
        insert teamInstance ;
        
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team,teamInstance,countr);
        pcc.Country_Lookup__c = countr.id;
        //pcc.Veeva_External_ID__c = 'test';
        insert pcc;
        
        Measure_Master__c measureMaster= TestDataFactory.createMeasureMaster(pcc,team,teamInstance);
        measureMaster.Team__c = team.id;
        measuremaster.Brand_Lookup__c = pcc.Id;
        measureMaster.Team_Instance__c = teamInstance.id;
        insert measureMaster;
        
        insert  new MetaData_Definition__c (Display_Name__c='ACCESSIBILITY',Source_Field__c='Accessibility_Range__c',Source_Object__c='BU_Response__c',Team_Instance__c=teamInstance.id,Product_Catalog__c=pcc.Id);
        
        AxtriaSalesIQTM__Position__c pos = TestDataFactory.createPosition(team,teamInstance);
        insert pos;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamInstance);
        insert posAccount;
        
        Product_Priority__c pp = new Product_Priority__c();
        pp.CurrencyIsoCode = 'USD';
        pp.Product__c = pcc.id;
        pp.Speciality_ID__c='testSpecs';
        pp.priority__c='P2';
        insert pp;
        BU_Response__c bu = TestDataFactory.createBuResponse(posAccount,pcc,teamInstance,team,acc);
        bu.Team_Instance__c = teamInstance.id;
        bu.Product__c = pcc.id;
        //bu.Brand_c = pcc.id;
        
        insert bu;
        
        
        Staging_Cust_Survey_Profiling__c s = new Staging_Cust_Survey_Profiling__c();
        s.BRAND_NAME__c ='testing';
        s.Team_Instance__c = teamInstance.Name;
        insert s;
        Test.startTest();
        System.runAs(loggedInUser){
            List<String> FIELD_LIST = new List<String>{'AxtriaSalesIQTM__Account_Alignment_Type__c','AxtriaSalesIQTM__Account_Target_Type__c'};
                System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(AxtriaSalesIQTM__Position_Account__c.SObjectType, FIELD_LIST, false));
            Delete_Staging_Cust_Survey_Profile obj=new Delete_Staging_Cust_Survey_Profile(teamInstance.Name,'File');
            Database.executeBatch(obj);
            
        }
        Test.stopTest();
    }
    
    static testMethod void testMethod4() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        
        Account acc = TestDataFactory.createAccount();
        acc.Profile_Consent__c='yes';
        acc.AxtriaSalesIQTM__Speciality__c ='testspecs';
        
        insert acc;
        
        Account acc2 = TestDataFactory.createAccount();
        acc2.Profile_Consent__c='yes';
        acc2.AxtriaSalesIQTM__Speciality__c ='testspecs';
        acc2.name = 'testacc2';
        insert acc2;
        
        AxtriaSalesIQTM__Team_Instance__c teamInstance = TestDataFactory.createTeamInstance(team);
        teamInstance.AxtriaSalesIQTM__Team__c = team.id;
        teamInstance.Name = 'testteamins';
        teamInstance.Segmentation_Universe__c = 'Full S&T Input Customers';
        insert teamInstance ;
        
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team,teamInstance,countr);
        pcc.Country_Lookup__c = countr.id;
        //pcc.Veeva_External_ID__c = 'test';
        insert pcc;
       
        
        Measure_Master__c mm1= TestDataFactory.createMeasureMaster(pcc,team,teamInstance);
        mm1.Team__c = team.id;
        mm1.Brand_Lookup__c = pcc.Id;
        mm1.Team_Instance__c = teamInstance.id;
        insert mm1;
        
        Measure_Master__c mm2= TestDataFactory.createMeasureMaster(pcc,team,teamInstance);
        mm2.Team__c = team.id;
        mm2.Brand_Lookup__c = pcc.Id;
        mm2.Team_Instance__c = teamInstance.id;
        insert mm2;
        
        insert  new MetaData_Definition__c (Display_Name__c='ACCESSIBILITY',Source_Field__c='Accessibility_Range__c',Source_Object__c='BU_Response__c',Team_Instance__c=teamInstance.id,Product_Catalog__c=pcc.Id);
        
        AxtriaSalesIQTM__Position__c pos = TestDataFactory.createPosition(team,teamInstance);
        insert pos;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamInstance);
        insert posAccount;
        
        Product_Priority__c pp = new Product_Priority__c();
        pp.CurrencyIsoCode = 'USD';
        pp.Product__c = pcc.id;
        pp.Speciality_ID__c='testSpecs';
        pp.priority__c='P2';
        insert pp;
        
        BU_Response__c bu = TestDataFactory.createBuResponse(posAccount,pcc,teamInstance,team,acc);
        bu.Team_Instance__c = teamInstance.id;
        bu.Product__c = pcc.id;
        //bu.Brand_c = pcc.id;
        insert bu;
        
        Staging_Cust_Survey_Profiling__c s = new Staging_Cust_Survey_Profiling__c();
        s.BRAND_NAME__c ='testing';
        s.Team_Instance__c = teamInstance.Name;
        insert s;
        Test.startTest();
        System.runAs(loggedInUser){
            List<String> FIELD_LIST = new List<String>{'AxtriaSalesIQTM__Account_Alignment_Type__c','AxtriaSalesIQTM__Account_Target_Type__c'};
                System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(AxtriaSalesIQTM__Position_Account__c.SObjectType, FIELD_LIST, false));
            //Delete_Staging_Cust_Survey_Profile obj=new Delete_Staging_Cust_Survey_Profile(teamInstance.Name,'File');
            Delete_Staging_Cust_Survey_Profile obj=new Delete_Staging_Cust_Survey_Profile(mm1.id,mm2.id,teamInstance.Name,pcc.Veeva_External_ID__c,'HCO',true);
            Database.executeBatch(obj);
            
        }
        Test.stopTest();
    }
}