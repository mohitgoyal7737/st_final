/**********************************************************************************************
@author       : Himanshu Tariyal (A0994)
@createdDate  : 12th June'2020
@description  : Test class for covering CallPlanCopyScenarioCtlr class
@Revision(s)  : v1.0
**********************************************************************************************/
@isTest
public class CallPlanCopyScenarioCtlrTestNew
{
    @istest 
    static void CallPlanCopyScenarioCtlrTest()
    {
        String className = 'CallPlanCopyScenarioCtlrTestNew';
        User loggedInUser = new User(id=UserInfo.getUserId());

        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__AccountType__c ='HCP';
        SnTDMLSecurityUtil.insertRecords(acc,className);

        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);

        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'ONCO';
        SnTDMLSecurityUtil.insertRecords(team,className);

        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins,className);

        AxtriaSalesIQTM__Team_Instance__c teamins2 = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins2,className);
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        SnTDMLSecurityUtil.insertRecords(scen,className);

        AxtriaSalesIQTM__Scenario__c scen2 = TestDataFactory.newcreateScenario(teamins2, team, workspace);
        SnTDMLSecurityUtil.insertRecords(scen2,className);

        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        pcc.Name='Product Name22';
        pcc.Product_Code__c='Veeva';
        pcc.Veeva_External_ID__c='Veeva';
        pcc.External_ID__c = 'Veeva';
        SnTDMLSecurityUtil.insertRecords(pcc,className);

        Product_Catalog__c pcc1 = TestDataFactory.productCatalog(team, teamins2, countr);
        pcc1.Name='Product Name';
        pcc1.Product_Code__c='Veeva1';
        pcc1.Veeva_External_ID__c='Veeva1';
        pcc1.External_ID__c = 'Veeva1';
        SnTDMLSecurityUtil.insertRecords(pcc1,className);

        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        SnTDMLSecurityUtil.insertRecords(mmc,className);

        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        SnTDMLSecurityUtil.insertRecords(pos,className);

        AxtriaSalesIQTM__User_Access_Permission__c u = TestDataFactory.createUserAccessPerm(pos, teamins, UserInfo.getUserId());
        u.AxtriaSalesIQTM__Position__c=pos.id;
        u.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        u.AxtriaSalesIQTM__User__c = loggedInUser.id;  
        SnTDMLSecurityUtil.insertRecords(u,className);      
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        SnTDMLSecurityUtil.insertRecords(posAccount,className);
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        SnTDMLSecurityUtil.insertRecords(pPriority,className);
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        positionAccountCallPlan.AxtriaSalesIQTM__isIncludedCallPlan__c = true;
        positionAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c = true;
        SnTDMLSecurityUtil.insertRecords(positionAccountCallPlan,className);

        //Create Team Instance Config
        Team_Instance_Config__c teamInstConfig = TestDataFactory.createTeamInstConfig(teamins);
        SnTDMLSecurityUtil.insertRecords(teamInstConfig,className);

        //Create Source to Destination Mapping
        Source_to_Destination_Mapping__c sourceDestMapping = TestDataFactory.createACF_PA_mapping(teamins,pcc);
        SnTDMLSecurityUtil.insertRecords(sourceDestMapping,className);

        //Create Source Team Instance Object Attribute
        AxtriaSalesIQTM__Team_Instance_Object_Attribute__c tioaSource = TestDataFactory.createTeamInstanceObjectAttribute(teamins,pcc);
        SnTDMLSecurityUtil.insertRecords(tioaSource,className);

        //Create Destination Team Instance Object Attribute
        AxtriaSalesIQTM__Team_Instance_Object_Attribute__c tioaDest = TestDataFactory.createTeamInstanceObjectAttribute(teamins2,pcc1);
        SnTDMLSecurityUtil.insertRecords(tioaDest,className);

        //Create Source Team Instance Object Attribute Detail
        AxtriaSalesIQTM__Team_Instance_Object_Attribute_Detail__c tioadSource = TestDataFactory.createTeamInstanceObjectAttribute(teamins,tioaSource);
        SnTDMLSecurityUtil.insertRecords(tioadSource,className);

        //Create Destination Team Instance Object Attribute Detail
        AxtriaSalesIQTM__Team_Instance_Object_Attribute_Detail__c tioadDest = TestDataFactory.createTeamInstanceObjectAttribute(teamins2,tioaDest);
        SnTDMLSecurityUtil.insertRecords(tioadDest,className);

        //Create Position Product record
        AxtriaSalesIQTM__Position_Product__c posproduct = TestDataFactory.createPositionProduct(teamins,pos,pcc);
        SnTDMLSecurityUtil.insertRecords(posproduct,className);

        //Create Change Request Type record
        AxtriaSalesIQTM__Change_Request_Type__c changeReqType = TestDataFactory.createChangeReqType('Call_Plan_Change');
        SnTDMLSecurityUtil.insertRecords(changeReqType,className);

        //Create CIM Config record
        AxtriaSalesIQTM__CIM_Config__c cimConfig = TestDataFactory.createCPCIMConfig(teamins,changeReqType);
        SnTDMLSecurityUtil.insertRecords(cimConfig,className);

        ///Create Dependency Control record
        AxtriaSalesIQTM__Dependency_Control__c depControl = TestDataFactory.createDepControl(teamins);
        SnTDMLSecurityUtil.insertRecords(depControl,className);

        //Create Dependency Control Condition record
        AxtriaSalesIQTM__Dependency_Control_Condition__c depControlCondition = TestDataFactory.createDepControlCondition(teamins,depControl);
        SnTDMLSecurityUtil.insertRecords(depControlCondition,className);

        sObject copyCallLog = Schema.getGlobalDescribe().get('AxtriaSalesIQTM__SalesIQ_Logger__c').newSObject();
       // copyCallLog.put('Workspace__c',workspace.Id);
       // copyCallLog.put('Team__c',team.Id);
       // copyCallLog.put('Source_Team_Instance__c',teamins.Id);
       // copyCallLog.put('Destination_Team_Instance__c',teamins2.Id);
        copyCallLog.put('AxtriaSalesIQTM__Status__c','In Progress');
        copyCallLog.put('Object_Name__c','Copy Call Plan Scenario');
        SnTDMLSecurityUtil.insertRecords(copyCallLog,className);

        Test.startTest();

        System.runAs(loggedInUser)
        {
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));

            CallPlanCopyScenarioCtlr classCall = new CallPlanCopyScenarioCtlr();
            classCall.selectedWorkspace = workspace.Id;
            classCall.getTeamSelectList();

            classCall.selectedTeam = team.Id;
            classCall.getTeamInstList();

            classCall.selectedSourceTeamInst = teamins.Id;
            classCall.selectedDestTeamInst = teamins2.Id;
            classCall.callCopyCPFunction();
        }
        Test.stopTest();    
    }
}