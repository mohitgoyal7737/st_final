/*Author - Himanshu Tariyal(A0994)
Date : 23rd January 2018*/
@isTest
private class BatchFillUniqueAccountCountMMTest {
	static testMethod void firstTest() 
	{
		
		/*Create initial test data for all the objs reqd.*/
		User loggedInUser = new User(id=UserInfo.getUserId());

		AxtriaSalesIQTM__Organization_Master__c aom = new AxtriaSalesIQTM__Organization_Master__c(AxtriaSalesIQTM__Org_Level__c='Global',AxtriaSalesIQTM__Parent_Country_Level__c=true);
		insert aom;
		
		AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='USA',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active');
		insert country;
		
		Account acc = new Account(Name='test acc',Marketing_Code__c='EU',AxtriaSalesIQTM__AccountType__c = 'HCP');
		insert acc;
		
		AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='Test Team');
		insert team;
		
		AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU',AxtriaSalesIQTM__Team__c=team.id);
		insert ti;
		
		AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c(Name='Pos',AxtriaSalesIQTM__Team_iD__c=team.id);
		insert pos;

		Product_Catalog__c pc = new Product_Catalog__c(Name='Test Product',Team_Instance__c=ti.id,Veeva_External_ID__c = 'ProdId',Product_Code__c= 'ProdId',IsActive__c=true,Country_Lookup__c=country.id);
		insert pc;
		
	    /*Brand_Team_Instance__c bti = new Brand_Team_Instance__c(Brand__c=pc.id,Team_Instance__c=ti.id);
	    insert bti;*/
	    
	    Measure_Master__c mm = new Measure_Master__c(Team_Instance__c=ti.id,Brand_Lookup__c=pc.id,State__c='Executed',All_Segments__c = 'All',Measure_Type__c ='HCP');
	    insert mm;
	    
	    AxtriaSalesIQTM__Position_Account__c pa = new AxtriaSalesIQTM__Position_Account__c(AxtriaSalesIQTM__Team_Instance__c=ti.id,AxtriaSalesIQTM__Account__c=acc.id);
	    insert pa;
	    System.test.startTest();
	    /*Create an instance of the class*/
	    System.runAs(loggedInUser){
	    	ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
	    	String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
	    	List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
	    	System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
	    	BatchFillUniqueAccountCountMM bat = new BatchFillUniqueAccountCountMM(ti.id,mm.id,'All');
	    	bat.query = 'select id from Account';
	    	Database.executeBatch(bat,2000);
	    }
	    System.test.stopTest();
	    
	}
}