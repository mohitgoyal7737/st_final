global with sharing class PACP_lookups_Utility_New implements Database.Batchable<sObject>, Database.Stateful 
{
    //Error_Object__c replaced with AxtriaSalesIQTM__SalesIQ_Logger__c due to object purge activity
    public String query;
    public String selectedTeamInstance;
    public String selectedTeam;
    public string country;
    public string Ids;
    Boolean flag= true;
    public integer errorcount;
    public integer recordsTotal;
    //public AxtriaSalesIQTM__Team_Instance__C notSelectedTeamInstance;
    Map<String,Id> teamInstanceNametoIDMap;
    Map<String,string> teamInstanceNametoTeamNameMap;
    Map<String,boolean> teamInstanceNametoAffiliationMap;
    map<string,string>alignmentperiodmap ;
    String alignmnetperiod;
    public boolean affiliationFlag; 
    public list<AxtriaSalesIQTM__Team_Instance__c> countrylst=new list<AxtriaSalesIQTM__Team_Instance__c>();

    //Added by HT(A0944) on 12th June 2020
    public String alignNmsp;
    public String batchName;
    public String callPlanLoggerID;
    public Boolean proceedNextBatch = true;
    
    //Added by HT(A0994) on 17th June 2020
    global String changeReqID;
    global Boolean flagValue = false;

    //Added by HT(A0944) on 12th June 2020
    global PACP_lookups_Utility_New(String teamInstance,String loggerID,String alignNmspPrefix) 
    {
        }
  
    global PACP_lookups_Utility_New(String teamInstance) {
       
    }
    
    global PACP_lookups_Utility_New(String teamInstance,string Ids) {
       
    }
    
    //Added by HT(A0994) on 17th June 2020
    global PACP_lookups_Utility_New(String teamInstance,string Ids,Boolean flag) 
    {
       
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        SnTDMLSecurityUtil.printDebugMessage('query+++++++++++++   ' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<temp_Obj__c> scopeRecs) 
    {
       
    }

    global void finish(Database.BatchableContext BC) 
    {
      }
}