global with sharing class Batchcheckpacp_posacc implements Database.Batchable<sObject> {
    public String query;
    public string country ;

    global Batchcheckpacp_posacc(string countryname) {
        /*country='';
        country = countryname;
        query = 'select id,AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Account__r.type,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.name from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c !=null and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c =\'Current\' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c =\'Live\' and AxtriaSalesIQTM__Team_Instance__r.Country_Name__c=:country and AxtriaSalesIQTM__lastApprovedTarget__c = true  ';
        //and AxtriaSalesIQTM__Account__r.AccountNumber=\'ES206265\'
        this.query = query;
        SnTDMLSecurityUtil.printDebugMessage('==========query:'+query);*/
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        //return null;
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scope) {
        /*SnTDMLSecurityUtil.printDebugMessage('==========Execute=======');
        set<string>accountnumber = new set<string>();
        set<string>positions = new set<string>();
        set<string>teaminstance = new set<string>();
        //set<string>pacpset = new set<string>();
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : scope){
            //string key =pacp.AxtriaSalesIQTM__Position__c+'_'+pacp.AxtriaSalesIQTM__Account__r.AccountNumber+'_'+pacp.AxtriaSalesIQTM__Team_Instance__c;
            accountnumber.add(pacp.AxtriaSalesIQTM__Account__r.AccountNumber);
            positions.add(pacp.AxtriaSalesIQTM__Position__c);
            teaminstance.add(pacp.AxtriaSalesIQTM__Team_Instance__c);
            //pacpset.add(key);
        }
        SnTDMLSecurityUtil.printDebugMessage('============teaminstance:::'+teaminstance);
        SnTDMLSecurityUtil.printDebugMessage('==========positions::::'+positions);
        SnTDMLSecurityUtil.printDebugMessage('==========accountnumber::::'+accountnumber);

        set<string>posaccset = new set<string>();
        list<temp_Obj__c>temppalist = new list<temp_Obj__c>();
        set<string>uniqset = new set<string>();
        list<AxtriaSalesIQTM__Position_Account__c>posacclist = [select id,AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Account__r.AccountNumber IN : accountnumber and AxtriaSalesIQTM__Position__c in : positions and AxtriaSalesIQTM__Team_Instance__c in : teaminstance and (AxtriaSalesIQTM__Assignment_Status__c='Active' or AxtriaSalesIQTM__Assignment_Status__c='Future Active')];

        SnTDMLSecurityUtil.printDebugMessage('==========posacclist:::::::'+posacclist.size());
        if(posacclist !=null && posacclist.size() >0)
        {
            for(AxtriaSalesIQTM__Position_Account__c pa :posacclist)
            {
                string key2 = pa.AxtriaSalesIQTM__Position__c+'_'+pa.AxtriaSalesIQTM__Account__r.AccountNumber+'_'+pa.AxtriaSalesIQTM__Team_Instance__c;
                posaccset.add(key2);
            }
            SnTDMLSecurityUtil.printDebugMessage('====posaccset:::::'+posaccset);
            for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : scope)
            {
                string key =pacp.AxtriaSalesIQTM__Position__c+'_'+pacp.AxtriaSalesIQTM__Account__r.AccountNumber+'_'+pacp.AxtriaSalesIQTM__Team_Instance__c;
                SnTDMLSecurityUtil.printDebugMessage('===========Call plan key is::::'+key);
                if(!posaccset.contains(key) && !uniqset.contains(key))
                {   
                    SnTDMLSecurityUtil.printDebugMessage('======INSIDE NO POS ACC====');
                    temp_Obj__c newpa= new temp_Obj__c();
                    newpa.AccountNumber__c = pacp.AxtriaSalesIQTM__Account__r.AccountNumber;
                    newpa.Territory_ID__c = pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
                    newpa.TeamName__c = pacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.name;
                    newpa.Account_Type__c = pacp.AxtriaSalesIQTM__Account__r.type;
                    string uniq = (String)pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c+(String)pacp.AxtriaSalesIQTM__Account__r.AccountNumber+(String)pacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.name;
                    newpa.UniqueKey__c = uniq;
                    newpa.Object__c = 'Acc_Terr';
                    
                    temppalist.add(newpa);
                    uniqset.add(key);

                }
            }
            SnTDMLSecurityUtil.printDebugMessage('===========temppalist.size():::::'+temppalist.size());
            if(temppalist !=null && temppalist.size()>0)
            {
                //Database.insert(temppalist,false);
                //Changed for security review
                        SObjectAccessDecision securityDecision = Security.stripInaccessible(AccessType.CREATABLE,temppalist);
                        //insert securityDecision.getRecords();
                        //list<temp_Obj__c> temp = securityDecision.getRecords();
                        Database.insert(securityDecision.getRecords(),false);
                        //throw exception if permission is missing 
                        if(!securityDecision.getRemovedFields().isEmpty() ){
                           throw new SnTException(securityDecision, 'temp_Obj__c', SnTException.OperationType.C ); 
                        }
                        //End of security review change
                //insert temppalist;
                //Upsert temppalist UniqueKey__c;
            }

        }*/
        
    }

    global void finish(Database.BatchableContext BC) {

    }
}