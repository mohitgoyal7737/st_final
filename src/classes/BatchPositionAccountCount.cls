global class BatchPositionAccountCount implements Database.Batchable<sObject>, Database.Stateful{
    
    map<String,Integer> Active_Map = New map<String,Integer>();
    Public integer intPA;
    Public String country;
    
    global BatchPositionAccountCount(String Country){
         this.country = country;
     }
      
       global Database.QueryLocator start(Database.BatchableContext BC) {
       // myDate = date.newInstance(2019, 01, 23);
        String query = 'SELECT Id, Name,AxtriaSalesIQTM__Assignment_Status__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Hierarchy_Level__c = \'1\' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = \'Current\' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = \'Live\' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name =: Country and AxtriaSalesIQTM__Assignment_Status__c = \'Active\'';        
        system.debug('Position Account Records:::::::::::::' + query);        
        return Database.getQueryLocator(query);
    }
    
     global void execute(Database.BatchableContext BC, List<AxtriaSalesIQTM__Position_Account__c> scope){     
         for(AxtriaSalesIQTM__Position_Account__c PA : Scope){
             if(PA.AxtriaSalesIQTM__Assignment_Status__c == 'Active' ){
                 if(!Active_Map.containsKey(PA.AxtriaSalesIQTM__Position__c)){
                     Active_Map.put(PA.AxtriaSalesIQTM__Position__c ,1);
                  }
                  else{
                    intPA = Active_Map.get(PA.AxtriaSalesIQTM__Position__c);
                    intPA++;
                    Active_Map.Put(PA.AxtriaSalesIQTM__Position__c,intPA);
                  }
                } 
      } 
      
      List<AxtriaSalesIQTM__Position__c> PosList = [SELECT Id,AxtriaSalesIQTM__Hierarchy_Level__c ,AxtriaARSnT__One_day_Previous_Count__c, Name,AxtriaARSnT__PA_Active__c,AxtriaARSnT__PA_Inactive__c  FROM AxtriaSalesIQTM__Position__c where  AxtriaSalesIQTM__Hierarchy_Level__c = '1' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = 'Current' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Live'and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name =: Country];
   for(AxtriaSalesIQTM__Position__c POS : PosList){
       POS.AxtriaARSnT__Current_Count__c = Active_Map.get(POS.ID); 
       POS.AxtriaARSnT__PA_Active__c = 0;
       POS.AxtriaARSnT__PA_Inactive__c = 0;
          }
    update PosList;  
      

    }
        
    global void finish(Database.BatchableContext BC) { 
      database.executebatch(new BatchPositionAccountAddedDropped2(Country));     
       }
   }