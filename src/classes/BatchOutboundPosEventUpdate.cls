global class BatchOutboundPosEventUpdate implements Database.Batchable<sObject> {
    public String query;
    public Set<String> currentTeamInstSet;

    global BatchOutboundPosEventUpdate() {

        currentTeamInstSet  = new Set<String>();
        for(AxtriaSalesIQTM__Team_Instance__c teaminst :  [SELECT Name FROM AxtriaSalesIQTM__Team_Instance__c WHERE AxtriaSalesIQTM__Alignment_Period__c = 'Current']){

            currentTeamInstSet.add(teaminst.Name);
        }

        query = 'SELECT Id,SIQ_Event__c,SIQ_EFFECTIVE_END_DATE__c,SIQ_Team_Instance__c,Unique_Id__c FROM SIQ_Position_O__c where OrgMstr__c =false and SIQ_Event__c!=\'Delete\' and SIQ_Team_Instance__c IN :currentTeamInstSet ';
        this.query = query;
        system.debug('==========Query is:'+query);
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<SIQ_Position_O__c> scope) {
        set<string>teaminstanceset = new set<string>();
        set<string>stagunq = new set<string>();
        set<string>salesiqset = new set<string>();
        list<SIQ_Position_O__c>updlist = new list<SIQ_Position_O__c>();
        for(SIQ_Position_O__c pos : scope)
        {
            teaminstanceset.add(pos.SIQ_Team_Instance__c);
            stagunq.add(pos.Unique_Id__c);
        }
        system.debug('======team instance names:::'+teaminstanceset);
        for(AxtriaSalesIQTM__Position__c p : [select id,AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_iD__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.name,AxtriaSalesIQTM__Team_Instance__r.name from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_Instance__r.name In :teaminstanceset and AxtriaSalesIQTM__inactive__c = false ])
        {
            String Childcode=p.AxtriaSalesIQTM__Client_Position_Code__c;
            string parentcode = '';
            string team = p.AxtriaSalesIQTM__Team_Instance__r.name;
            if(p.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Position_Code__c != null)
            {                               
                parentcode=p.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
            }
            if(p.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Position_Code__c==null)
            {
                parentcode=p.AxtriaSalesIQTM__Team_iD__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.name;
            }
            string key=Childcode.toUpperCase()+'_'+parentcode.toUpperCase()+'_'+team.toUpperCase();
            salesiqset.add(key);
        }

        for(SIQ_Position_O__c stg : scope)
        {
            string unq1 = stg.Unique_Id__c;
            string unq= unq1.toUpperCase();
            if(!salesiqset.contains(unq))
            {
                if(unq.contains('_'))
                {
                    stg.SIQ_Event__c='Delete';
                    stg.SIQ_EFFECTIVE_END_DATE__c = System.today().addDays(-1);
                    stg.SIQ_Updated_Date__c=System.today();
                    updlist.add(stg);
                }  
            }
        }
        system.debug('=====updlist.size()::::'+updlist.size());
        if(updlist!=null && updlist.size() >0)
        {
            update updlist;
        }
        
    }

    global void finish(Database.BatchableContext BC) {

    }
}