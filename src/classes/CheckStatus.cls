global class CheckStatus implements Database.Batchable<sObject>, Database.Stateful {
    
    // instance member to retain state across transactions
    global Integer recordsProcessed = 0;
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(
            'SELECT id,AxtriaARSnT__Channel1__c,AxtriaARSnT__Channel__c,AxtriaARSnT__Description__c, AxtriaARSnT__Status__c, AxtriaARSnT__Description1__c,AxtriaARSnT__Position__r.AxtriaARSnT__Position_Description__c from AxtriaARSnT__Veeva_Position_Hierarchy__c'
        );
    }
    global void execute(Database.BatchableContext bc, List<AxtriaARSnT__Veeva_Position_Hierarchy__c> scope)
    {
        // process each batch of records
        //List<AxtriaARSnT__Veeva_Position_Hierarchy__c> contacts = new List<AxtriaARSnT__Veeva_Position_Hierarchy__c>();
        for (AxtriaARSnT__Veeva_Position_Hierarchy__c vc : scope) 
        {
            
            if(vc.AxtriaARSnT__Description__c!='')
            {
            
                if(vc.AxtriaARSnT__Description__c!=vc.AxtriaARSnT__Description1__c )
                {
                    
                    vc.AxtriaARSnT__Description1__c=vc.AxtriaARSnT__Description__c;
                    vc.AxtriaARSnT__Status__c='Updated';
                    vc.AxtriaARSnT__IsStatusChanged__c='True';
                }
            
            }
            
            if(vc.AxtriaARSnT__Channel__c!='')
            {
            
                if(vc.AxtriaARSnT__Channel1__c!=vc.AxtriaARSnT__Channel__c)
                {
                    vc.AxtriaARSnT__Channel1__c=vc.AxtriaARSnT__Channel__c; 
                    vc.AxtriaARSnT__Status__c='Updated';
                    vc.AxtriaARSnT__IsStatusChanged__c='True';
                }
            
            }
            
        }
        update scope;
    }    
    global void finish(Database.BatchableContext bc){
        
        
    }    
}