global class CallOrphanHCPClass implements Schedulable  {     
 
    global CallOrphanHCPClass() {
        
        Set<String> allCountriesList = new Set<String>();
        Set<String> teamInsSet = new Set<String>();
        List<Veeva_Job_Scheduling__c> countryList = [select id, Country__c from Veeva_Job_Scheduling__c where Load_Type__c = 'Delta' or Load_Type__c = 'Full Load'];
        System.debug('<<<<<<<<<<---- Query --->>>>>>>>>>' +countryList);

        for(Veeva_Job_Scheduling__c vcs : countryList)
        {
             allCountriesList.add(vcs.Country__c);
        }

        System.debug('allCountriesList::::::::' +allCountriesList);

        List<AxtriaSalesIQTM__Team_Instance__c> teamInsList = [select Id from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c = 'Current' and AxtriaSalesIQTM__Scenario__c != null and AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Live' and AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c in :allCountriesList];

        System.debug('teamInsList::::::::' +teamInsList);

        for(AxtriaSalesIQTM__Team_Instance__c tiRec : teamInsList)
        {
            teamInsSet.add(tiRec.Id);
        }
        System.debug('teamInsSet:::::: ' +teamInsSet);

        for(String teamIns : teamInsSet)
        {
            DeleteDataCheckMissingAffiliation deleteBatch = new DeleteDataCheckMissingAffiliation(teamIns);
            Database.executeBatch(deleteBatch,2000);
        }
    }

    public void execute(System.SchedulableContext SC){
        
        CallOrphanHCPClass obj = new CallOrphanHCPClass();
    }
}