global class BatchCopyCurrDayToPrevDayLOA implements Database.Batchable<sObject>, Database.stateful{
    
    global BatchCopyCurrDayToPrevDayLOA(){}
    
    global Database.QueryLocator start(Database.BatchableContext BC){  
        string query = 'SELECT Id, Name, AssociateOID__c, LOA_StartDate__c, LOA_EndDate__c, Paid_Time_Off_Policy__c, Total_Quantity__c'
                        +'  FROM Current_Leave_Data_Feed__c where isRejected__c = false ';
        system.debug('inside getQueryLocator :: query: '+query);
        return Database.getQueryLocator(query);
    }
    
     global void execute(Database.BatchableContext BC, list<Current_Leave_Data_Feed__c> currentLOAList){
        list<Previous_Leave_Data_Feed__c> prevList = new list<Previous_Leave_Data_Feed__c>();
        for(Current_Leave_Data_Feed__c curr : currentLOAList){
            Previous_Leave_Data_Feed__c prev = new Previous_Leave_Data_Feed__c();
            prev.AssociateOID__c           = curr.AssociateOID__c;         
            prev.LOA_StartDate__c          = curr.LOA_StartDate__c;        
            prev.LOA_EndDate__c            = curr.LOA_EndDate__c;          
            prev.Paid_Time_Off_Policy__c   = curr.Paid_Time_Off_Policy__c; 
            prev.Total_Quantity__c         = curr.Total_Quantity__c;
            //prev.Rejection_Reason__c     = curr.Rejection_Reason__c;
            //prev.isRejected__c               = curr.isRejected__c;
            prevList.add(prev);       
        }
        
        if(prevList.size() > 0){
            insert prevList;
        }
     }
    
    global void finish(Database.BatchableContext BC){   
        system.debug('--finished---');
    }
}