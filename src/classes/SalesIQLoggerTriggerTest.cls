@isTest
public class SalesIQLoggerTriggerTest {
static testmethod void SalesIQLoggerTriggerTest(){
    
    AxtriaSalesIQTM__Organization_Master__c OM = new AxtriaSalesIQTM__Organization_Master__c();
    OM.Name='Dummy1';
    OM.AxtriaSalesIQTM__Org_Level__c='Global';
    //OM.AxtriaARSnT__Marketing_Code__c='ES';
    OM.AxtriaSalesIQTM__Parent_Country_Level__c=TRUE;
    insert OM;
    
    AxtriaSalesIQTM__Country__c CON = new AxtriaSalesIQTM__Country__c();
    CON.AxtriaSalesIQTM__Country_Code__c='ES';
    CON.AxtriaSalesIQTM__Status__c='ACTIVE';
    CON.AxtriaSalesIQTM__Parent_Organization__c=OM.id;
    insert CON;

     AxtriaSalesIQTM__TriggerContol__c aaa=new AxtriaSalesIQTM__TriggerContol__c(Name='OverlappingWorkspace',AxtriaSalesIQTM__IsStopTrigger__c=false);
     insert aaa;
        
        if(!AxtriaSalesIQTM__TriggerContol__c.getValues('OverlappingWorkspace').AxtriaSalesIQTM__IsStopTrigger__c)
              {


     AxtriaSalesIQTM__Workspace__c WK = new AxtriaSalesIQTM__Workspace__c();
     WK.Name='W1';
     WK.AxtriaSalesIQTM__Workspace_Start_Date__c=system.today().adddays(-10);
     
     WK.AxtriaSalesIQTM__Workspace_Description__c='test class for Scenario Queue Trigger';
     insert WK;
     
    AxtriaSalesIQTM__Team__c TN = new AxtriaSalesIQTM__Team__c();
    TN.Name='ABC';
    TN.AxtriaSalesIQTM__Effective_Start_Date__c=system.today()-1;
    TN.AxtriaSalesIQTM__Effective_Start_Date__c=system.today();
    TN.AxtriaSalesIQTM__Alignment_Type__c='Account';
    TN.AxtriaSalesIQTM__Team_Code__c='TE01111';
    TN.AxtriaSalesIQTM__Country__c=CON.id;
    TN.AxtriaSalesIQTM__Type__c='BASE';
    insert TN;
     
AxtriaSalesIQTM__Scenario__c SS = new AxtriaSalesIQTM__Scenario__c();
     SS.AxtriaSalesIQTM__Workspace__c=WK.id;
     SS.AxtriaSalesIQTM__Rule_Execution_Status__c ='Queued';
     insert SS;
     
     System.debug('SS%%%%%%%%%%'+SS);
    
AxtriaSalesIQTM__SalesIQ_Logger__c  SL= new AxtriaSalesIQTM__SalesIQ_Logger__c();
    SL.AxtriaSalesIQTM__Type__c ='Master Data Syncing';
    SL.AxtriaSalesIQTM__Module__c ='BRMS';
    
    insert SL;
    
    SL.AxtriaSalesIQTM__Status__c  = 'Success';
    update SL;
    System.debug('SL%%%%%%%%%%'+SL);
    
     List<AxtriaSalesIQTM__Scenario__c> queueScenarioList = new List<AxtriaSalesIQTM__Scenario__c>();
       queueScenarioList = [ Select Id from AxtriaSalesIQTM__Scenario__c where AxtriaSalesIQTM__Rule_Execution_Status__c = 'Queued' AND Id =:SS.id Limit 1 ];
        system.debug('queueScenarioList ******'+queueScenarioList );
        if(queueScenarioList != null && queueScenarioList.size() > 0)
        {
            UPDATE queueScenarioList;
        }
    List<AxtriaSalesIQTM__Scenario__c> queueScenarioList1 = new List<AxtriaSalesIQTM__Scenario__c>();
        queueScenarioList1 = [ Select Id, AxtriaSalesIQTM__Rule_Execution_Status__c, AxtriaSalesIQTM__Run_Response_Details__c from AxtriaSalesIQTM__Scenario__c where AxtriaSalesIQTM__Rule_Execution_Status__c = 'Queued' Limit 10000];
        if(queueScenarioList1 != null && queueScenarioList1.size() > 0)
        {

            for(AxtriaSalesIQTM__Scenario__c scenario:queueScenarioList1)
            {
                scenario.AxtriaSalesIQTM__Rule_Execution_Status__c = 'Success';
                scenario.AxtriaSalesIQTM__Run_Response_Details__c = '';
            }
            UPDATE queueScenarioList;
    
    }
    }
}
}