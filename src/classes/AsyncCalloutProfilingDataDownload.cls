public with sharing class AsyncCalloutProfilingDataDownload implements Queueable, Database.AllowsCallouts{
	String endPointURL;
    String postRequestBody;
    
	public AsyncCalloutProfilingDataDownload(String endpoint, String postBody){
        try{
            this.endPointURL = endpoint;
            this.postRequestBody = postBody;
        }
        catch(Exception qe) {
            //SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'AsyncCalloutQuantile',ruleId);
        }
    }

    public void execute(QueueableContext context){
        callFutureMethod();
    }
    
    public void callFutureMethod(){
        Http h = new Http();
        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        String errorMessage = '';

        try{
            request.setEndPoint(endPointURL);
            request.setHeader('Content-type','application/json');
            request.setMethod('POST');
            request.setbody(postRequestBody);
            request.setTimeout(120000);
            response = h.send(request);

            SnTDMLSecurityUtil.printDebugMessage('Response status ---> ' + response.getStatus());
            if(response.getStatus() == 'OK'){
                System.debug('Success');
            }
        }catch(Exception e){
            SnTDMLSecurityUtil.printDebugMessage('Error --> ' + e.getStackTraceString());
            SnTDMLSecurityUtil.printDebugMessage('getCause --> ' + e.getCause());
            SnTDMLSecurityUtil.printDebugMessage('getLineNumber --> ' + e.getLineNumber());
            SnTDMLSecurityUtil.printDebugMessage('getMessage --> ' + e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('getTypeName --> ' + e.getTypeName());

            SalesIQSnTLogger.createUnHandledErrorLogsforProfilingData(e,SalesIQSnTLogger.PROFILING_DATA_MODULE,'AsyncCalloutProfilingDataDownload');
        }
        System.debug('errorMessage : '+errorMessage);
    }
}