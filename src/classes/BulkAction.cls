//Modified by Sukirty on 30/3/2020
//Included things to make it functionally better like as alert if no CR is selected and clicked on approve/reject buttons
//Only approver of that CR is allowed to make bulk approval
//Non mandatory approval comments
public with sharing class BulkAction
{
    List<AxtriaSalesIQTM__Change_Request__c> records {get; set;}
    List<AxtriaSalesIQTM__Change_Request__c> allRecords {get; set;}
    List<ProcessInstance> processStepDetail {get; set;}
    public String profileName {get; set;}
    public String reasonCode {get; set;}
    public List<String> ids {get; set;}
    public Integer recordSize {get; set;}
    public List<String> approverOneList {get; set;}
    public List<String> approverTwoList {get; set;}
    public Map<String, String> changeRequestToActorMap {get; set;}
    public Integer countActorId {get; set;}
    public Integer dml_size {get; set;}
    public Boolean showPopUp {get; set;}

    public BulkAction(ApexPages.StandardSetController controller)
    {
        System.debug('Inside Bulk Action Constructor');
        countActorId = 0;
        System.debug('ProfileId--' + UserInfo.getProfileId());
        profileName = [select Name from profile where id = :userinfo.getProfileId()].Name;
        System.debug('profile name--' + profileName);
        String userId = UserInfo.getUserId();
        System.debug('print userId--' + userId);
        approverOneList = new List<String>();
        approverTwoList = new List<String>();
        changeRequestToActorMap = new Map<String, String>();
        records = (AxtriaSalesIQTM__Change_Request__c[])controller.getSelected();
        system.debug('selected records--' + records);
        system.debug('selected records size--' + records.size());

        List<Id> allSfdcIDs = new List<Id>();
        List<String> list_acc = new List<String>();
        Set<String> set_ti = new Set<String>();
        Integer cr_count = 0, acc_count = 0, kpi_count = 0, custom_dml_size = 0;
        dml_size = 0;


        for(AxtriaSalesIQTM__Change_Request__c request : records)
        {
            allSfdcIDs.add(request.ID);
        }
        System.debug('allSfdcIDs--' + allSfdcIDs);
        allRecords = [select id, AxtriaSalesIQTM__Status__c, AxtriaSalesIQTM__Approver_One__c, AxtriaSalesIQTM__Approver_Two__c, AxtriaSalesIQTM__Account_Moved_Id__c, AxtriaSalesIQTM__Team_Instance_ID__c from AxtriaSalesIQTM__Change_Request__c where id in :allSfdcIDs and AxtriaSalesIQTM__RecordTypeID__c != null and  AxtriaSalesIQTM__RecordTypeID__r.AxtriaSalesIQTM__CR_Type_Name__c = 'Call_Plan_Change'];
        System.debug('allRecords--' + allRecords);
        ids = new List<String>();

        for(AxtriaSalesIQTM__Change_Request__c changeRequest : allRecords)
        {
            if(changeRequest.AxtriaSalesIQTM__Status__c == 'Pending')
            {
                ids.add(changeRequest.Id);
                //populate approver list for each pending CR and use to check on page whether user has access to spprove/reject the CR - method1
                if(!approverOneList.contains(changeRequest.AxtriaSalesIQTM__Approver_One__c))
                {
                    approverOneList.add(changeRequest.AxtriaSalesIQTM__Approver_One__c);
                }

                if(!approverTwoList.contains(changeRequest.AxtriaSalesIQTM__Approver_Two__c))
                {
                    approverTwoList.add(changeRequest.AxtriaSalesIQTM__Approver_Two__c);
                }
            }
            if(changeRequest.AxtriaSalesIQTM__Account_Moved_Id__c != null)
            {
                list_acc.addAll(changeRequest.AxtriaSalesIQTM__Account_Moved_Id__c.split(','));
            }

            set_ti.add(changeRequest.AxtriaSalesIQTM__Team_Instance_ID__c);

        }

        System.debug('get ids--' + ids);
        System.debug('approverOneList--' + approverOneList);
        System.debug('approverTwoList--' + approverTwoList);

        //Run query on processinstance object to check the approver of CR at every step
        String processStepQuery = 'Select Id, Status,TargetObjectId, (Select Id, ActorId, OriginalActorId, StepStatus, Comments FROM StepsAndWorkitems where StepStatus IN (\'Pending\', \'Reassigned\')) FROM ProcessInstance where TargetObjectId in: ids';
        System.debug('processStepQuery--' + processStepQuery);
        processStepDetail = Database.query(processStepQuery);
        System.debug('processStepDetail--' + processStepDetail);
        System.debug('processStepDetail size--' + processStepDetail.size());

        for(ProcessInstance pi : processStepDetail)
        {
            for(ProcessInstanceHistory step : pi.StepsAndWorkitems)
            {
                changeRequestToActorMap.put(pi.TargetObjectId, step.ActorId);
            }
        }
        System.debug('changeRequestToActorMap--' + changeRequestToActorMap);
        recordSize = ids.size();

        //iterate over oending CRs to populate the count. This will be used to display the alert on page after checking whether user is allowed to approve/reject CR
        for(String iterator : ids)
        {
            if(changeRequestToActorMap.get(iterator) == userId)
            {
                System.debug('iterator print--' + changeRequestToActorMap.get(iterator));
                countActorId++;
            }
        }
        System.debug('countActorId--' + countActorId);

        cr_count = allRecords.size();
        acc_count = list_acc.size();
        kpi_count = [Select count() from AxtriaSalesIQTM__CIM_Config__c where AxtriaSalesIQTM__Team_Instance__c in :set_ti and AxtriaSalesIQTM__Change_Request_Type__c != null and AxtriaSalesIQTM__Change_Request_Type__r.AxtriaSalesIQTM__CR_Type_Name__c = 'Call_Plan_Change'];
        dml_size = cr_count + acc_count * 2 + kpi_count * cr_count;
        List<Batch_Size_Configuration__mdt> dml_config = new List<Batch_Size_Configuration__mdt>();
        dml_config = [Select Batch_Class_Name__c, Batch_Size__c from Batch_Size_Configuration__mdt where Batch_Class_Name__c = 'BulkAction' and Batch_Size__c != null];
        if(dml_config.size() > 0)
        {
            custom_dml_size = (Integer)dml_config[0].Batch_Size__c;
        }
        dml_size += custom_dml_size;

        if(recordSize == 0 || dml_size > 9999 || countActorId == 0 || countActorId < recordSize)
        {
            showPopUp = false;
        }
        else
            showPopUp = true;

    }

    public PageReference approveAll()
    {
        System.debug('Inside Approve all/selected function');
        System.debug('Approve Reason is ' + reasonCode);
        Map<String, Object> response = new Map<String, Object>();
        System.debug('get ids--' + ids);
        List<Approval.ProcessWorkitemRequest> requests = new List<Approval.ProcessWorkitemRequest>();

        List<ProcessInstanceWorkitem> workItems = [SELECT Id, ProcessInstanceId FROM ProcessInstanceWorkitem WHERE ProcessInstance.TargetObjectId IN :ids ];
        System.debug('workItems--' + workItems);
        for(ProcessInstanceWorkitem workItem : workItems)
        {
            Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
            req.setWorkitemId(workItem.Id);
            //Valid values are: Approve, Reject, or Removed.
            //Only system administrators can specify Removed.
            req.setAction('Approve');
            req.setComments(reasonCode);
            requests.add(req);
        }
        Approval.ProcessResult[] processResults = Approval.process(requests);
        PageReference pg = new PageReference('/one/one.app#/sObject/AxtriaSalesIQTM__Change_Request__c/list');
        pg.setRedirect(true);
        return pg;
    }

    public PageReference rejectRequests()
    {
        System.debug('Reject Reason is ' + reasonCode);
        Map<String, Object> response = new Map<String, Object>();
        System.debug('get ids in Rejection--' + ids);
        List<Approval.ProcessWorkitemRequest> requests = new List<Approval.ProcessWorkitemRequest>();

        List<ProcessInstanceWorkitem> workItems = [SELECT Id, ProcessInstanceId FROM ProcessInstanceWorkitem WHERE ProcessInstance.TargetObjectId IN :ids ];
        System.debug('workItems--' + workItems);
        for(ProcessInstanceWorkitem workItem : workItems)
        {
            Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
            req.setWorkitemId(workItem.Id);
            //Valid values are: Approve, Reject, or Removed.
            //Only system administrators can specify Removed.
            req.setAction('Reject');
            req.setComments(reasonCode);
            requests.add(req);
        }
        Approval.ProcessResult[] processResults = Approval.process(requests);
        PageReference pg = new PageReference('/one/one.app#/sObject/AxtriaSalesIQTM__Change_Request__c/list');
        pg.setRedirect(true);
        return pg;
    }
}