/*Author - Himanshu Tariyal(A0994)
Date : 23rd January 2018*/
global with sharing class BatchFillUniqueAccountCountMM implements Database.Batchable<sObject>,Database.Stateful {
    public String query;
    public String mmID;
    public String teamInstanceID;
    public String brandID;
    public Set<String> uniqueAcctsSet;
    public String allSegments;
    
  @deprecated
  global BatchFillUniqueAccountCountMM(String teamInstanceID,String mmID) {
   
        this.mmID = mmID;
        this.teamInstanceID = teamInstanceID;
        List<Measure_Master__c> mmList = [select Measure_Type__c from Measure_Master__c where id=:mmID];
        if(mmList.size()>0 && mmList!=null){
            uniqueAcctsSet = new Set<String>();
        this.query = 'select id from Account where id in (select AxtriaSalesIQTM__Account__c '+
                      'from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Team_Instance__c =\''+teamInstanceID+'\') and AxtriaSalesIQTM__AccountType__c = \'' + mmList[0].Measure_Type__c +'\'';
        }
        
    }
    
    global BatchFillUniqueAccountCountMM(String teamInstanceID,String mmID, String allSegments) {
        this.mmID = mmID;
        this.allSegments = allSegments;
        this.teamInstanceID = teamInstanceID;
        List<Measure_Master__c> mmList = [select Measure_Type__c from Measure_Master__c where id=:mmID  WITH SECURITY_ENFORCED];
        if(mmList.size()>0 && mmList!=null){
            if(mmList[0].Measure_Type__c != 'None'){
                uniqueAcctsSet = new Set<String>();
                this.query = 'select id from Account where id in (select AxtriaSalesIQTM__Account__c '+
                'from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Team_Instance__c =\''+teamInstanceID+'\') and Type = \'' + mmList[0].Measure_Type__c +'\' WITH SECURITY_ENFORCED';
            }
            else
            {
                uniqueAcctsSet = new Set<String>();
                this.query = 'select id from Account where id in (select AxtriaSalesIQTM__Account__c '+
                'from AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Team_Instance__c =\''+teamInstanceID+'\')';
            }
        }
        
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Account> accList) 
    {
        if(accList!=null && accList.size()>0)
        {
            for(Account acc : accList){
                uniqueAcctsSet.add(acc.id);
            }
        }
    }

    global void finish(Database.BatchableContext BC) 
    {
        List<Measure_Master__c> mmList = [select Number_of_Unique_Accounts__c,Brand_Lookup__c from Measure_Master__c where id=:mmID WITH SECURITY_ENFORCED];
        if(mmList.size()>0 && mmList!=null){
            Measure_Master__c mm = mmList[0];
            brandID = mmList[0].Brand_Lookup__c;
            mm.All_Segments__c = allSegments;
            mm.Number_of_Unique_Accounts__c = uniqueAcctsSet!=null ? uniqueAcctsSet.size() : 0;
            //update mm;
            SnTDMLSecurityUtil.updateRecords(mm, 'BatchFillUniqueAccountCountMM');
        }
        if(ST_Utility.getJobStatus(BC.getJobId(),mmId)){
            BatchFillUniqueCountBUMM bat = new BatchFillUniqueCountBUMM(mmID,teamInstanceID,brandID);
            Database.executeBatch(bat,2000);    
        }
        
    }
}