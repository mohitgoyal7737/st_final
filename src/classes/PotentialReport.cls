public with sharing class PotentialReport {

	String buName;
	String lineName;
   public List<ShowPotentialData> allPotentialData {get;set;}
   public String allData {get;set;}
   

   public PotentialReport() 
   {
      allPotentialData = new List<ShowPotentialData>();

   	buName = ApexPages.currentPage().getParameters().get('buSelectedName'); 
      lineName = ApexPages.currentPage().getParameters().get('lineSelectedName');
   	getAllPosAdoptionMap();
   }
    public Integer noOfDelta {get;set;}
    public Integer noOfcallDelta {get;set;}
    public Integer cnoOfDelta {get;set;}
    public Integer cnoOfcallDelta {get;set;}
   public Integer countSumOriginal {get;set;}
   public Integer countSumApproved {get;set;}
   public Integer sumCountOriginal {get;set;}
   public Integer sumCountApproved {get;set;}
   Public String percOfHCPsUpdatedString{get;set;}
   Public String percOfHCPsOriginalString{get;set;}
   Public String percOfCallsUpdatedString{get;set;}
   Public String percOfCallsOriginalString{get;set;}
   Public String originalCallspercString {get;set;}

   public Decimal originalHCPperc {get;set;}
   Public String originalHCPpercString {get;set;}
   public Decimal updatedHCPper {get;set;}

   public Decimal originalCallsperc {get;set;}
   public Decimal updatedCallsper {get;set;}

   public Integer targetsDelta {get;set;}
   public Integer callsDelta {get;set;}
   Public String updatedCallsperString {get;set;}
   public String userLocale {get;set;}
   Public String updatedHCPperString {get;set;}
   public String fileDelimiter {get;set;}
   

   public void getAllPosAdoptionMap()
   {
   	List<AxtriaSalesIQTM__User_Access_Permission__c> allUserAccessPerm = [select AxtriaSalesIQTM__Position__c from AxtriaSalesIQTM__User_Access_Permission__c where AxtriaSalesIQTM__User__c = :UserInfo.getUserId()];
   	String selectedPosition;

      userLocale = UserInfo.getLocale();
      system.debug('@@@@@@@@@qwqw' + userLocale);
      List<Country__c> countryInfo = [select Delimiter__c from Country__c where name =: userLocale];
      
      if(countryInfo.size()>0)
      {
          fileDelimiter = countryInfo[0].Delimiter__c;
      }
      else
      {
          fileDelimiter = ',';
      }

   	if(allUserAccessPerm.size() > 0 )
   	{
   		selectedPosition = allUserAccessPerm[0].AxtriaSalesIQTM__Position__c;
   	}
    system.debug ('@@@@@@@@Position@@@@@@@@@@@ '+ selectedPosition);
   	List<AggregateResult> allRecsSum = [select Potential__c name, sum(CountOriginal__c) countOrg, sum(CountApproved__c) countAppro ,sum(CountTCForiginal__c) countTCForiginal , sum(CountTCFapproved__c) countTCFappr from AxtriaSalesIQTM__Position_Account_Call_Plan__c where  (AxtriaSalesIQTM__Position__c = :selectedPosition or AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__c = :selectedPosition or AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__c =:selectedPosition or AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__c =:selectedPosition) and AxtriaSalesIQTM__Position__c != '' and Line__r.Name = :lineName group by Potential__c];
     noOfDelta =0;
     noOfcallDelta=0;
     cnoOfDelta =0;
     cnoOfcallDelta=0;
   	countSumOriginal = 0;
      countSumApproved = 0;
      sumCountOriginal = 0;
      sumCountApproved = 0;
      originalHCPperc = 0;
      updatedHCPper = 0;
      originalCallsperc = 0;
      updatedCallsper = 0;

   	for(AggregateResult agg : allRecsSum)
   	{
   		countSumOriginal = countSumOriginal + Integer.valueof(agg.get('countOrg'));
   		countSumApproved = countSumApproved + Integer.valueof(agg.get('countAppro'));
   		sumCountOriginal = sumCountOriginal + Integer.valueof(agg.get('countTCForiginal'));
   		sumCountApproved = sumCountApproved + Integer.valueof(agg.get('countTCFappr'));
   	}

      system.debug('+++++++++++++++++ Agg Result '+ allRecsSum);

   	for(AggregateResult agg : allRecsSum)
   	{
   	  String AdoptionName = String.valueof(agg.get('name'));
        Integer noOfHCPsOriginal = Integer.valueof(agg.get('countOrg'));
        Decimal percOfHCPsOriginal;
         noOfDelta = Integer.valueof(agg.get('countAppro')) - Integer.valueof(agg.get('countOrg'))  ;
         noOfcallDelta = Integer.valueof(agg.get('countTCFappr'))- Integer.valueof(agg.get('countTCForiginal'));
         cnoOfcallDelta = cnoOfcallDelta + noOfcallDelta;
         cnoOfDelta = cnoOfDelta+noOfDelta;
        
        if(countSumOriginal != 0)
        percOfHCPsOriginal = ((noOfHCPsOriginal * 100.0)/countSumOriginal).setScale(1);
        
        else
        percOfHCPsOriginal = 0;
        percOfHCPsOriginalString = String.valueOf(percOfHCPsOriginal);
          percOfHCPsOriginalString= percOfHCPsOriginalString.replace('.', ',');
        
        originalHCPperc = originalHCPperc + percOfHCPsOriginal;
        originalHCPpercString = String.valueOf(originalHCPperc);
          originalHCPpercString= originalHCPpercString.replace('.', ',');

        Integer noOfHCPsUpdated = Integer.valueof(agg.get('countAppro'));
        
        Decimal percOfHCPsUpdated = 0;

        if(countSumApproved != 0)
          percOfHCPsUpdated = ((noOfHCPsUpdated * 100.0)/countSumApproved).setScale(1);
          percOfHCPsUpdatedString = String.valueOf(percOfHCPsUpdated);
          percOfHCPsUpdatedString= percOfHCPsUpdatedString.replace('.', ',');
        updatedHCPper = updatedHCPper  + percOfHCPsUpdated;
        updatedHCPperString = String.valueOf(updatedHCPper);
          updatedHCPperString= updatedHCPperString.replace('.', ',');
        

        Integer noOfCallsOriginal = Integer.valueof(agg.get('countTCForiginal'));

        Decimal percOfCallsOriginal = 0;

        if(sumCountOriginal != 0)
        percOfCallsOriginal = ((noOfCallsOriginal * 100.0)/sumCountOriginal).setScale(1);
        percOfCallsOriginalString = String.valueOf(percOfCallsOriginal);
          percOfCallsOriginalString= percOfCallsOriginalString.replace('.', ',');
        
        originalCallsperc = originalCallsperc + percOfCallsOriginal;
        originalCallspercString = String.valueOf(originalCallsperc);
          originalCallspercString= originalCallspercString.replace('.', ',');
        

        Integer noOfCallsUpdated = Integer.valueof(agg.get('countTCFappr'));

        Decimal percOfCallsUpdated = 0;

        if(sumCountApproved != 0)
        percOfCallsUpdated = ((noOfCallsUpdated * 100.0)/sumCountApproved).setScale(1);
        percOfCallsUpdatedString = String.valueOf(percOfCallsUpdated);
          percOfCallsUpdatedString= percOfCallsUpdatedString.replace('.', ',');
        
        updatedCallsper = updatedCallsper + percOfCallsUpdated;
         updatedCallsperString = String.valueOf(updatedCallsper);
          updatedCallsperString= updatedCallsperString.replace('.', ',');
        
        
        allPotentialData.add(new ShowPotentialData(AdoptionName, noOfHCPsOriginal, percOfHCPsOriginalString, noOfHCPsUpdated, percOfHCPsUpdatedString, noOfCallsOriginal, percOfCallsOriginalString, noOfCallsUpdated, percOfCallsUpdatedString,noOfDelta,noOfcallDelta ));
   	}

      allPotentialData.add(new ShowPotentialData('Total', countSumOriginal, originalHCPpercString, countSumApproved, updatedHCPperString, sumCountOriginal, originalCallspercString, sumCountApproved, updatedCallsperString,cnoOfDelta,cnoOfcallDelta));
      allData = JSON.serialize(allPotentialData);

      targetsDelta = countSumApproved - countSumOriginal;
      callsDelta = sumCountApproved - sumCountOriginal;

   }

   public class ShowPotentialData
   {
   	public String potentialName {get;set;}
   	public Integer noOfHCPsOriginal {get;set;} 
   	public String  percOfHCPsOriginal {get;set;} 
   	public Integer noOfHCPsUpdated {get;set;} 
   	public String  percOfHCPsUpdated {get;set;} 
   	public Integer noOfCallsOriginal {get;set;} 
   	public String  percOfCallsOriginal {get;set;} 
   	public Integer noOfCallsUpdated {get;set;} 
   	public String percOfCallsUpdated {get;set;} 
   	public Integer noOfDelta {get;set;} 
   	public Integer noOfcallDelta {get;set;} 
   	

   	public ShowPotentialData(String potentialName,Integer noOfHCPsOriginal ,String percOfHCPsOriginal ,Integer noOfHCPsUpdated ,String percOfHCPsUpdated ,Integer noOfCallsOriginal ,String percOfCallsOriginal ,Integer noOfCallsUpdated ,String percOfCallsUpdated, integer noOfDelta, integer noOfcallDelta)
   	{
   		this.potentialName = potentialName;
   		this.noOfHCPsOriginal = noOfHCPsOriginal;
   		this.percOfHCPsOriginal = percOfHCPsOriginal;
   		this.noOfHCPsUpdated = noOfHCPsUpdated;
   		this.percOfHCPsUpdated = percOfHCPsUpdated;
   		this.noOfCallsOriginal = noOfCallsOriginal;
   		this.percOfCallsOriginal = percOfCallsOriginal;
   		this.noOfCallsUpdated = noOfCallsUpdated;
   		this.percOfCallsUpdated = percOfCallsUpdated;
   		this.noOfDelta = noOfDelta;
   		this.noOfcallDelta = noOfcallDelta;
   				
   	}
   }
}