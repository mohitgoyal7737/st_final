global class DeleteDuplicateAssignmentData implements Database.Batchable<sObject>, Database.Stateful {
    public String query;
    public String type;
    public Set<String> existedExternalID;
    public Set<String> deleteID;

    global DeleteDuplicateAssignmentData(String assignmentType) {
        this.query = query;
        type = assignmentType;
        existedExternalID = new Set<String>();
        deleteID = new Set<String>();

        if(type == 'AxtriaSalesIQTM__Position_Product__c')
        {
            query = 'Select Id, AxtriaARSnT__External_ID__c,AxtriaSalesIQTM__isActive__c from AxtriaSalesIQTM__Position_Product__c where AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__c != null and (AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = \'Current\' or AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = \'Future\') and AxtriaARSnT__External_ID__c != null and AxtriaSalesIQTM__isActive__c=true order by AxtriaARSnT__External_ID__c desc';
        }
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<sObject> scope) {

        System.debug('scope -'+scope.size());
        if(type == 'AxtriaSalesIQTM__Position_Product__c')
        {
            updatePosProduct(scope);
        }
        
    }

    global void finish(Database.BatchableContext BC) {

    }

    public void updatePosProduct(List<AxtriaSalesIQTM__Position_Product__c> scope)
    {
        System.debug('---- updatePosProduct----');
        for(AxtriaSalesIQTM__Position_Product__c posProRec : scope)
        {
            if(!existedExternalID.contains(posProRec.AxtriaARSnT__External_ID__c))
            {
                existedExternalID.add(posProRec.AxtriaARSnT__External_ID__c);
                System.debug('First time External ID:::::::' +posProRec.AxtriaARSnT__External_ID__c);
            }
            else
            {
                deleteID.add(posProRec.Id);
                System.debug('Delete Duplicate:::::::' +posProRec.Id);
            }
        }

        List<AxtriaSalesIQTM__Position_Product__c> deletePosProList = [Select Id from AxtriaSalesIQTM__Position_Product__c where Id in :deleteID];

        if(deleteID.size() > 0)
            delete deletePosProList;
    }
}