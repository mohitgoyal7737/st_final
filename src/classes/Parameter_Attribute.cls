public with sharing class Parameter_Attribute {
  public List<AxtriaSalesIQTM__Team_Instance__c> teamInstanceNames = new List<AxtriaSalesIQTM__Team_Instance__c>();
    public string selectedTeamInstanceName { get; set; }
    public string selectedDate {get;set;}
    public string accField {get;set;}
    public string qname {get;set;}
    
    public String countryID {get;set;}
    public Map<string,string> successErrorMap;
    public string selectedTeamInstance      {get;set;}
    public String TeamInstancename {get;set;}
    public string cycleSelected{get; set;}
    public list<SelectOption>allcycles {get;set;}
    public list<SelectOption> cycles {get;set;}
    public list<SelectOption>businessUnits {get;set;}
    public AxtriaSalesIQTM__Country__c Country;
    public String selectedCycle {get;set;}
     public String selectedBu {get;set;}


     public list<Parameter_Attribute_Setting__c> displayNames {get;set;}
     public List<SelectOption> surveyparameters {get;set;}
     public string displayselected{get; set;}
     public String selecteddisplay{get;set;}
     
    
    public Parameter_Attribute()
     {
       
       // A1930 due to cookie issue 
        /*countryID = SalesIQUtility.getCookie('CountryID');
        system.debug('##### countryID ' + countryID);
        successErrorMap = SalesIQUtility.checkCountryAccess(countryID);
        system.debug('############ successErrorMap ' + successErrorMap);
        if(successErrorMap.containsKey('Success'))
        {
           countryID = successErrorMap.get('Success');               
           system.debug('########## countryID from Map ' + countryID);
           //As soon as we get Country ID set it in the cookie and it will applicable for whole application.
           SalesIQUtility.setCookieString('CountryID',countryID);
           */
               countryID = MCCP_Utility.getKeyValueFromPlatformCache('SIQCountryID');
              SnTDMLSecurityUtil.printDebugMessage('countryID--'+countryID);
        
            Country = new AxtriaSalesIQTM__Country__c();
            Country = [select AxtriaSalesIQTM__Country_Flag__c,Name from AxtriaSalesIQTM__Country__c where id =:countryID limit 1];
       // }
        fillCycleOptions();
        
        displayParameter();

        
        }
        
   public void fillCycleOptions()
   {
       cycles = new list<SelectOption>();
        cycles.add(new SelectOption('None', '--None--'));

        //Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c
        //List<Cycle__c> allCycles = [SELECT Id, Name FROM Cycle__c where Country__c =:countryID ];
        List<AxtriaSalesIQTM__Workspace__c> allCycles = [SELECT Id, Name FROM AxtriaSalesIQTM__Workspace__c where AxtriaSalesIQTM__Country__c = :countryID ];

        if(allCycles != null || allCycles.size() > 0)
        {
            selectedCycle = allCycles[0].ID;

            for(AxtriaSalesIQTM__Workspace__c cycle:allCycles){
                cycles.add(new SelectOption(cycle.Id, cycle.Name));
            }            
        }
        //Shivansh - A1450 -- Changes Ends here

         businessUnits = new list<SelectOption>();
        businessUnits.add(new SelectOption('None','--None--'));
    }

   public void getBusinessUnit()
    {
       if(cycleSelected ==null)
            cycleSelected =selectedCycle;
        businessUnits = new list<SelectOption>();
        businessUnits.add(new SelectOption('None','--None--'));
        system.debug('++++++++++++++ cycle selected '+ cycleSelected);

        //Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c
        List<AxtriaSalesIQTM__Team_Instance__c> allTeamInstances = [SELECT Id, Name FROM AxtriaSalesIQTM__Team_Instance__c  where  AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c= :cycleSelected];
        system.debug('++++++++++++++ BU Records '+ allTeamInstances);
        for(AxtriaSalesIQTM__Team_Instance__c bu : allTeamInstances)
        {
            businessUnits.add(new SelectOption(bu.Name, bu.Name));
        }
        system.debug('===========businessUnits:::'+businessUnits);
        //displayParameter();
    }
   
   public void displayParameter()
   {

      surveyparameters = new List<SelectOption>();
      surveyparameters.add(new SelectOption('None', '--None--'));
      displayNames=new List<Parameter_Attribute_Setting__c>();

      //List<string> attribute = new List<String>();
      displayNames = [Select ID,Name,Display_Name__c,API_Name__c from Parameter_Attribute_Setting__c  WHERE Name != NULL];//Parameter_Attribute_Setting__c.getall().values();
      system.debug('+++displayNames++'+displayNames);
      if(displayNames.size() > 0)
        selecteddisplay=displayNames[0].Display_Name__c;
      else
        selecteddisplay='None';
       system.debug('+++selecteddisplay++'+selecteddisplay);

      for(Parameter_Attribute_Setting__c para :displayNames)
      {
      //attribute.add(para.Display_Name__c);
      surveyparameters.add(new SelectOption(para.API_Name__c,para.Display_Name__c));
      }
       system.debug('+++surveyparameters++'+surveyparameters);
   }
   public void getApiNames()
   {  
         //selecteddisplay=displayselected;
        system.debug('+++displayselected++'+displayselected);
        accField = displayselected;
        system.debug('+++accField++'+accField);

   }

    
    public void run_BU_Response_Account_Update_Batch()
    {   
        //qname=selecteddisplay;
        List<Parameter_Attribute_Setting__c> attributeApi= [Select ID,Name,Display_Name__c,API_Name__c from Parameter_Attribute_Setting__c where API_Name__c=:accField];
        qname=attributeApi[0].Display_Name__c;
        if(selectedTeamInstanceName == 'All')
        {
        //Database.executeBatch(new BU_Response_Insert_Utility(),2000);
        system.debug('Dont run utility as you need to select a team name.');
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select team instance!!')); 
       }
      else
      {
        system.debug('+++qname+++'+qname);
         system.debug('+++accField+++'+accField);

        BU_Response_Account_Update_Utility_New pacpBatch = new BU_Response_Account_Update_Utility_New(selectedTeamInstanceName,accField,qname);
        Database.executeBatch(pacpBatch,2000);
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Survey Response Batch Ran for Selected Team Instance Successfully!!'));
      }
    }
    
    public void run_BU_Response_TSF_Accessibility_Batch()
      {
        List<Parameter_Attribute_Setting__c> attributeApi= [Select ID,Name,Display_Name__c,API_Name__c from Parameter_Attribute_Setting__c where API_Name__c=:accField];
        qname=attributeApi[0].Display_Name__c;
        if(selectedTeamInstanceName == 'All'){
        //Database.executeBatch(new BU_Response_Insert_Utility(),2000);
        system.debug('Dont run utility as you need to select a team name.');
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select team instance!!')); 
      }
      else
      {
         
        BU_Response_TSF_Update_Utility_New pacpBatch = new BU_Response_TSF_Update_Utility_New(selectedTeamInstanceName,qname);
        Database.executeBatch(pacpBatch,2000);
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Survey Response Batch Ran for Selected Team Instance Successfully!!'));
      }
    }
}