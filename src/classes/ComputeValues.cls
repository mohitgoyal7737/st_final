/*Author - Himanshu Tariyal(A0994)
Date : 2nd January 2018

Shivansh - A1450
Changes done:
1. Quantile step save from UI on Compute Values UI
*/
/*Replaced BTI with product catalog*/
public with sharing class ComputeValues extends BusinessRule implements IBusinessRule
{
    public List<SelectOption> adoptPotList {get; set;}
    public List<Step__c> listSteps;
    public List<SelectOption> adoptPotConcCurveList {get; set;}
    public list<Decimal> sumList {get; set;}

    public String selectedAdopt {get; set;}
    public String selectedPot {get; set;}
    public String graphString {get; set;}
    public String individualGraphString {get; set;}
    public String adoptOrPot {get; set;}

    public Boolean showCurveData {get; set;}
    public Set<String> setParamNames;
    public transient List<SLDSPageMessage> PageMessages{get;set;}
    public String ruleId {get;set;} 
    public Boolean isDimensionEmpty{get;set;}//Added by Chirag Ahuja for SR-197
    public Boolean isValidError {get;set;} // Added by RT for SR-453

    public ComputeValues()
    {
        init();
            ruleId = ApexPages.currentPage().getParameters().get('rid');
        adoptPotList = new List<SelectOption>();
        adoptPotConcCurveList = new List<SelectOption>();
        isAddNew = false;
        showCurveData = false;
        uiLocation = 'Compute Values';
        adoptOrPot = 'Adoption';
        selectedMatrix = '';
        selectedAdopt = '';
        selectedPot = '';
        graphString = '';
        individualGraphString = '';
        retUrl = '/apex/ComputeValues?mode=' + mode + '&rid=' + ruleId;
        initStep();
        createAdoptPotList();
        //createConcCurve();
    }

    public void save()
    {
        try{
        SnTDMLSecurityUtil.printDebugMessage('in saveAndNext fn:-->');
        string result = validateStep(uiLocation);
	isValidError = false; // Added by RT for SR-453
        SnTDMLSecurityUtil.printDebugMessage('result-->' + result);
        SnTDMLSecurityUtil.printDebugMessage('step-->' + step);
	if(String.isNotBlank(result))
	 {
	    isValidError = true; // Added by RT for SR-453
            isDimensionEmpty = true;	//Added by Chirag Ahuja for SR-197
            System.debug('--isDimensionEmpty0--'+isDimensionEmpty);
            PageMessages = SLDSPageMessage.add(PageMessages,result,'error');
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, result));
        }
	else{
        if(step != null && String.isBlank(result))
        {
            isDimensionEmpty = false;	//Added by Chirag Ahuja for SR-197
            System.debug('We are inside Save & in if');
            step.UI_Location__c = uiLocation;
            /*SnTDMLSecurityUtil.printDebugMessage('---step.Step_Type__c--- ' + step.Step_Type__c);*/
            if(step.Step_Type__c == 'Matrix')
            {
                step.Matrix__c = selectedMatrix;
                step.Grid_Param_1__c = gridParam1;
                if(gridMap.Grid_Type__c == '2D')
                {
                    step.Grid_Param_2__c = gridParam2;
                }
            }
            else if(step.Step_Type__c == 'Compute')
            {
                
               /* System.debug('expression--'+expression);
                if(!validateExpressionBeforeSave()){
                    return;
                }
                computeObj.Expression__c = expression;*/ // Commented to revert SR-9 changes
		 /*SnTDMLSecurityUtil.printDebugMessage('--selectedCompF2-- ' + selectedCompF2);*/
                /*SnTDMLSecurityUtil.printDebugMessage('--selectedCompF1-- ' + selectedCompF1);*/
                computeObj.Field_1__c = selectedCompF1;
                computeObj.Field_2_Type__c = field2Type;
                if(String.isNotBlank(selectedCompF2))
                    computeObj.Field_2__c = selectedCompF2;
                else
                {
                    computeObj.Field_2_val__c = selectedCompF3;
                }
                //upsert computeObj;
                SnTDMLSecurityUtil.upsertRecords(computeObj, 'ComputeValues');
                step.Compute_Master__c = computeObj.Id;
            }
            /* Shivansh - A1450 */
            else if(step.Step_Type__c == 'Quantile')
            {
                /*SnTDMLSecurityUtil.printDebugMessage('--selectedCompF2-- ' + selectedCompF2);*/
                /*SnTDMLSecurityUtil.printDebugMessage('--selectedCompF1-- ' + selectedCompF1);*/
                computeObj.Field_1__c = selectedCompF1;
                computeObj.Field_2_val__c = selectedCompF3;
                computeObj.Expression__c = expression;
                Map<Id, String> parmeterIdToName = new Map<Id, String>();//Map to store old parameterid to name for editing the expression in Quantile step
                /*for(Rule_Parameter__c rp: [SELECT Id, Parameter_Name__c FROM Rule_Parameter__c WHERE Measure_Master__c =: ruleObject.Id]){
                    parmeterIdToName.put(rp.Id,rp.Parameter_Name__c);
                }*/

                list<Rule_Parameter__c> rptemp ;
                try
                {
                    rptemp = [SELECT Id, Parameter_Name__c FROM Rule_Parameter__c WHERE Measure_Master__c = : ruleObject.Id WITH SECURITY_ENFORCED];
                }
                    catch(Exception qe)
                {
                        PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                        SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
                    SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
                }
                for(Rule_Parameter__c rp : rptemp)
                {
                    parmeterIdToName.put(rp.Id, rp.Parameter_Name__c);
                }
                String exp = expression;
                for(Id s : parmeterIdToName.keySet())
                {
                    exp = exp.replaceAll(s, parmeterIdToName.get(s));
                }
                computeObj.User_View_Expression__c = exp;
                //upsert computeObj;
                SnTDMLSecurityUtil.upsertRecords(computeObj, 'ComputeValues');
                step.Compute_Master__c = computeObj.Id;
                step.isQuantileComputed__c = false;
            }
            /* Shivansh - A1450 till Here*/
            else if(step.Step_Type__c == 'Cases')
            {
                computeObj.Expression__c = expression;
                //upsert computeObj;
                SnTDMLSecurityUtil.upsertRecords(computeObj, 'ComputeValues');
                step.Compute_Master__c = computeObj.Id;
            }
            /*SnTDMLSecurityUtil.printDebugMessage('---saving step -- ' + step);*/
            //upsert step;
            SnTDMLSecurityUtil.upsertRecords(step, 'ComputeValues');

            if(!isStepEditMode)
            {
                //Update Next steps if any
                //list<Step__c> allNextSteps = [SELECT Id, Sequence__c FROM Step__c WHERE Measure_Master__c =:ruleObject.Id AND Sequence__c >=: step.Sequence__c ORDER BY Sequence__c];
                list<Step__c> allNextSteps ;
                try
                {
                    allNextSteps = [SELECT Id, Sequence__c FROM Step__c WHERE Measure_Master__c = :ruleObject.Id AND Sequence__c >= : step.Sequence__c WITH SECURITY_ENFORCED ORDER BY Sequence__c];
                }
                    catch(Exception qe)
                {
                        PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                        SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
                    SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
                }

                Decimal duplicateSeq = 0;
                map<Decimal, list<Step__c>> stepSeqMap = new map<Decimal, list<Step__c>>();
                if(allNextSteps != null && allNextSteps.size() > 0)
                {
                    // creating a map of id and sequence

                    for(Step__c stepObj : allNextSteps)
                    {
                        if(stepSeqMap.get(stepObj.Sequence__c) == null)
                            stepSeqMap.put(stepObj.Sequence__c, new list<Step__c> {stepObj});
                        else
                        {
                            list<Step__c> stepList = stepSeqMap.get(stepObj.Sequence__c);
                            stepList.add(stepObj);
                            stepSeqMap.put(stepObj.Sequence__c, stepList);
                        }
                    }

                    SnTDMLSecurityUtil.printDebugMessage('stepSeqMap :' + stepSeqMap);

                    for(Step__c st : allNextSteps)
                    {
                        SnTDMLSecurityUtil.printDebugMessage('st - ' + st);
                        SnTDMLSecurityUtil.printDebugMessage('duplicateSeq :' + duplicateSeq);
                        list<Step__c> tempStepList = stepSeqMap.get(st.Sequence__c);
                        if(tempStepList.size() > 1)
                        {
                            for(Step__c obj : tempStepList)
                            {
                                if(st.Id != step.Id && step.Id != obj.Id)
                                {
                                    st.Sequence__c += 1;
                                    duplicateSeq = st.Sequence__c;
                                }
                            }
                        }
                        else
                        {
                            //duplicateSeq = st.Sequence__c;
                            if(st.Sequence__c == duplicateSeq)
                            {
                                st.Sequence__c += 1;
                                duplicateSeq = st.Sequence__c;
                            }
                        }
                    }

                    SnTDMLSecurityUtil.printDebugMessage('allNextSteps after update:' + allNextSteps);
                    /*for(Step__c st: allNextSteps){
                        SnTDMLSecurityUtil.printDebugMessage('st - '+st);
                        if(st.Id != step.Id){
                            st.Sequence__c += 1;
                        }
                    }*/
                }
                //Update allNextSteps;
                SnTDMLSecurityUtil.updateRecords(allNextSteps, 'ComputeValues');
            }
            Rule_Parameter__c rp = new Rule_Parameter__c();
            if(String.isNotBlank(step.Id))
            {
                //list<Rule_Parameter__c> rps = [select id from Rule_Parameter__c WHERE Step__c =: step.Id];

                list<Rule_Parameter__c> rps ;
                try
                {
                    rps = [select id from Rule_Parameter__c WHERE Step__c = : step.Id WITH SECURITY_ENFORCED];
                }
                    catch(Exception qe)
                {
                        PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                        SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
                    SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
                }
                if(rps != null && rps.size() > 0)
                {
                    rp.Id = rps[0].Id;
                }
            }
            rp.Measure_Master__c = ruleObject.Id;
            rp.Step__c = step.Id;
            rp.Type__c = step.Type__c;
            //upsert rp;
            SnTDMLSecurityUtil.upsertRecords(rp, 'ComputeValues');
            updateRule(uiLocation, 'Profiling Parameter');
            createAdoptPotList();
        }

        
       }
    }
        catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'ComputeValues',ruleId);
        } 
    }

    public void createAdoptPotList()
    {
        SnTDMLSecurityUtil.printDebugMessage('in createAdoptPotMap fn-->');
        try{
        adoptPotConcCurveList = new List<SelectOption>();
        adoptPotConcCurveList.add(new SelectOption('Adoption', 'Adoption'));
        adoptPotConcCurveList.add(new SelectOption('Potential', 'Potential'));

        adoptPotList = new List<SelectOption>();


        try
        {
            listSteps = [SELECT Id, name, Modelling_Type__c, Compute_Master__r.Expression__c FROM Step__c WHERE Measure_Master__c = :ruleObject.Id and Step_Type__c = 'Cases' and UI_Location__c = 'Compute Values' and Compute_Master__c != NULL WITH SECURITY_ENFORCED];
        }
            catch(Exception qe)
        {
                PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }
        SnTDMLSecurityUtil.printDebugMessage('listSteps-->' + listSteps);
        adoptPotList.add(new SelectOption('--NONE--', '--NONE--'));

        if(listSteps != null && listSteps.size() > 0)
        {
            for(Step__c step : listSteps)
            {
                if(String.isNotBlank(step.Compute_Master__r.Expression__c) && isExpressionValidForModellingSimulation(step.Compute_Master__r.Expression__c))
                {
                    SnTDMLSecurityUtil.printDebugMessage('valid case -->' + step.name);
                    adoptPotList.add(new SelectOption(step.id, step.name));
                    if(step.Modelling_Type__c == 'Potential')
                    {
                        selectedPot = step.id;
                    }
                    if(step.Modelling_Type__c == 'Adoption')
                    {
                        selectedAdopt = step.id;
                    }
                }
            }
            SnTDMLSecurityUtil.printDebugMessage('selectedAdopt-->' + selectedAdopt + '*****selectedPot-->' + selectedPot);
            }
        }
        catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'ComputeValues',ruleId);
        } 
    }



    public void createConcCurve()
    {
        try
        {
            SnTDMLSecurityUtil.printDebugMessage('in createConcCurve() fn-->');
            showCurveData = false;
            graphString = '';
            individualGraphString = '';

            Decimal totalCount = 0;
            Decimal cumulativeSum = 0;
            Decimal respVal = 0;
            Integer i = 1;

            List<BU_Response__c> buList = new List<BU_Response__c>();
            set<String> uniqueAccs = new set<String>();
            sumList = new list<Decimal>();

            map<Integer, Decimal> mapCountToIndividualVal = new map<Integer, Decimal>();
            String productSelected;
            String teaminstanceSelected;
            //List<Measure_Master__c> mmList = [select id,Team_Instance__c,Brand_Lookup__c FROM Measure_Master__c WHERE ID=:ruleObject.id];
            List<Measure_Master__c> mmList;
            try
            {
                mmList = [select id, Team_Instance__c, Brand_Lookup__c FROM Measure_Master__c WHERE ID = :ruleObject.id WITH SECURITY_ENFORCED];
            }
            catch(Exception qe)
            {
                PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
                SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
            }

            if(mmList != null && mmList.size() > 0)
            {
                productSelected = mmList[0].Brand_Lookup__c;
                teaminstanceSelected = mmList[0].Team_Instance__c;
            }

            SnTDMLSecurityUtil.printDebugMessage('productSelected-->' + productSelected + '*****teaminstanceSelected-->' + teaminstanceSelected + '*****adoptOrPot-->' + adoptOrPot);
            //List<Product_Catalog__c> btiList = [select id from Product_Catalog__c where Id=:productSelected and Team_Instance__c=:teaminstanceSelected];
            List<Product_Catalog__c> btiList;
            try
            {
                btiList = [select id from Product_Catalog__c where Id = :productSelected and Team_Instance__c = :teaminstanceSelected WITH SECURITY_ENFORCED];
            }
            catch(Exception qe)
            {
                PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
                SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
            }
            SnTDMLSecurityUtil.printDebugMessage('btiList size-->' + btiList.size());
            if(btiList != null && btiList.size() > 0)
            {
                String field = adoptOrPot == 'Adoption' ? 'AdoptionVal__c' : 'PotentialVal__c';
                /*String query = 'select '+field+',Physician__c from BU_Response__c where Brand__c= \''+btiList[0].id+'\' order by '+field+ ' desc limit 30000';*/
                String query = 'select ' + field + ',Physician__c from BU_Response__c where Product__c= \'' + btiList[0].id + '\' order by ' + field + ' desc limit 30000';
                buList = Database.query(query);
                if(buList != null && buList.size() > 0)
                {
                    for(BU_Response__c BU : buList)
                    {
                        if(!uniqueAccs.contains(bu.Physician__c))
                        {
                            respVal = 0;
                            try
                            {
                                respVal = bu.get(field) != null ? (Decimal)bu.get(field) : 0;
                            }
                            catch(Exception qe)
                            {
                                PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                                SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
                            }
                            totalCount += respVal;
                            sumList.add(totalCount);
                            mapCountToIndividualVal.put(i, respVal);
                            i++;
                        }
                    }
                    buList.clear();
                    if(totalCount > 0)
                    {
                        for(i = 0; i < sumList.size(); i++)
                        {
                            //sumList[i] = (sumList[i]/totalCount).setScale(3);
                            graphString += String.valueOf((sumList[i] / totalCount).setScale(3)) + ';';
                        }
                        if(graphString != null)
                            graphString = graphString.removeEnd(';');
                    }
                    if(mapCountToIndividualVal != null)
                    {
                        individualGraphString = JSON.serialize(mapCountToIndividualVal);
                        if(graphString != '')
                            showCurveData = true;
                    }
                    sumList.clear();
                    mapCountToIndividualVal.clear();
                    SnTDMLSecurityUtil.printDebugMessage('individualGraphString-->' + individualGraphString);
                    SnTDMLSecurityUtil.printDebugMessage('graphString-->' + graphString);
                }
            }

        }
        catch(Exception e)
        {
            showCurveData = false;
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(e, SalesIQSnTLogger.BR_MODULE, 'ComputeValues',ruleId);
            SnTDMLSecurityUtil.printDebugMessage('Error message-->' + e.getMessage());
        }
    }

    public static Boolean numberPresent(String val)
    {
        Boolean isValidDecimal = true;
        try
        {
            Decimal.valueOf(val);
        }
        catch(TypeException e)
        {
            isValidDecimal = false;
        }
        return isValidDecimal;
    }

    public class AddressWrapper implements Comparable
    {
        public String phyName;
        public Integer responseVal;
        public AddressWrapper(String phyName, Integer responseVal)
        {
            this.phyName = phyName;
            this.responseVal = responseVal;
        }
        public Integer compareTo(Object other)
        {
            return responseVal - ((AddressWrapper)other).responseVal;
        }
    }

    public PageReference saveAdoptPotValAndExit()
    {
        try{
        SnTDMLSecurityUtil.printDebugMessage('saveAdoptPotValAndExit function-->selectedAdopt-->' + selectedAdopt + '*********selectedPot-->' + selectedPot);
   
        try
        {
            listSteps = [select id, Modelling_Type__c from Step__c where Measure_Master__c = :ruleObject.Id and Step_Type__c = 'Cases'
                         and UI_Location__c = 'Compute Values' WITH SECURITY_ENFORCED];
        }
            catch(Exception qe)
        {
                PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }
        if(listSteps != null && listSteps.size() > 0)
        {
            for(Step__c step : listSteps)
            {
                step.Modelling_Type__c = null;
                if(selectedAdopt != null && selectedAdopt != '--NONE--')
                {
                    if(step.id == selectedAdopt)
                        step.Modelling_Type__c = 'Adoption';
                }
                if(selectedPot != null && selectedPot != '--NONE--')
                {
                    if(step.id == selectedPot)
                        step.Modelling_Type__c = 'Potential';
                }
            }
            //update listSteps;
            SnTDMLSecurityUtil.updateRecords(listSteps, 'ComputeValues');
        }
        updateRule('Compute Segment', uiLocation);
        return nextPage('ComputeSegment');
    }
        catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'ComputeValues',ruleId);
            return null;
        } 
    }

    public PageReference saveAndNext()
    {
        SnTDMLSecurityUtil.printDebugMessage('in saveAndNext fn:-->');
        save();
        //Commented by Himanshu Tariyal on 2nd Jan 2019 for including the Modal popup for selecting the Adoption/Potential step
        //updateRule('Compute Segment', uiLocation);
        return null;
        //Commented by Himanshu Tariyal on 2nd Jan 2019 for including the Modal popup for selecting the Adoption/Potential step
        //return nextPage('ComputeSegment');
    }

    public PageReference skipStep()
    {
        updateRule('Compute Segment', uiLocation);
        return nextPage('ComputeSegment');
    }

    public void Openpopup()
    {
        SnTDMLSecurityUtil.printDebugMessage('============INSIDE OPEN POPUP FUNCTION');
        SnTDMLSecurityUtil.printDebugMessage('=========Select Matrix is::' + selectedMatrix);
        //list<Step__c> Step = [Select id,Name,Measure_Master__r.Name from Step__c where Matrix__c=:selectedMatrix];
        try{
        list<Step__c> Step ;
        try
        {
            Step = [Select id, Name, Measure_Master__r.Name from Step__c where Matrix__c = :selectedMatrix WITH SECURITY_ENFORCED];
        }
            catch(Exception qe)
        {
                PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }
        // if(!step.isEmpty()){
        Measure_Master__c MM = [SELECT Id, Name FROM Measure_Master__c WHERE id = :ruleObject.Id WITH SECURITY_ENFORCED];
        //removed BTI
        Grid_Master__c GM = [select id, name, Brand__c, Col__c, Country__c, Description__c, Dimension_1_Name__c, Dimension_2_Name__c,
                             DM1_Output_Type__c, DM2_Output_Type__c, Grid_Type__c, Output_Name__c, Output_Type__c, Row__c from Grid_Master__c
                             where id = :selectedMatrix WITH SECURITY_ENFORCED limit 1];
        Grid_Master__c CloneGM = GM.Clone();
        String name = GM.Name;

        String nameMatrix = GM.name;
        String tempName = nameMatrix;
        Integer i = 1;
        //Added Where Clause foe Security Review
        //List<Grid_Master__c> allGridMasters = [select Name from Grid_Master__c ];
        List<Grid_Master__c> allGridMasters = [select Name from Grid_Master__c where Country__c = : countryID WITH SECURITY_ENFORCED];
        SnTDMLSecurityUtil.printDebugMessage('allGridMasters-->' + allGridMasters);
        List<String> allgms = new List<String>();
        SnTDMLSecurityUtil.printDebugMessage('tempName-->' + tempName);
        for(Grid_Master__c g : allGridMasters)
        {
            allgms.add(g.Name);
        }

        Boolean flag = true;
        while(flag)
        {
            SnTDMLSecurityUtil.printDebugMessage('+++++++++++ Hey Matrix is ' + tempName);
            if(allgms.contains(tempName))
            {
                tempName = nameMatrix + '_' + String.valueof(i);
                i++;
            }
            else
            {
                allgms.add(tempName);
                flag = false;
            }
        }
        SnTDMLSecurityUtil.printDebugMessage('tempName now-->' + tempName);
        CloneGM.Name = tempName;
        CloneGM.Country__c = GM.Country__c;
        CloneGM.CurrencyIsoCode = 'EUR';
        SnTDMLSecurityUtil.printDebugMessage('====CloneGM===:' + CloneGM);
        try
        {
            //insert CloneGM;
            SnTDMLSecurityUtil.insertRecords(CloneGM, 'ComputeValues');
            SnTDMLSecurityUtil.printDebugMessage('CloneGM.id-->' + CloneGM.id);
            SnTDMLSecurityUtil.printDebugMessage('CloneGM.id-->' + CloneGM.Name);
            matrixList.add(new SelectOption(CloneGM.Id, CloneGM.Name));
            list<Grid_Details__c> GD = new list<Grid_Details__c>();
            list<Grid_Details__c> CloneGD = new list<Grid_Details__c>();

            GD = [select id, Name, Grid_Master__c, colvalue__c, Dimension_1_Value__c, Dimension_2_Value__c, Output_Value__c, Rowvalue__c from Grid_Details__c
                  where Grid_Master__c = :selectedMatrix WITH SECURITY_ENFORCED];
            for(Grid_Details__c g : GD)
            {
                Grid_Details__c newGD = g.clone();
                newGD.Grid_Master__c = CloneGM.id;
                //newGD.CurrencyIsoCode = 'EUR';
                newGD.Name = g.name;
                CloneGD.add(newGD);
            }
            //insert CloneGD;
            SnTDMLSecurityUtil.insertRecords(CloneGD, 'ComputeValues');
            SnTDMLSecurityUtil.printDebugMessage('========CloneGD====:' + CloneGD);
            selectedMatrix = CloneGM.id;
        }
        catch(Exception ex)
        {
              PageMessages = SLDSPageMessage.add(PageMessages,'Matrix Name Should be Unique','info');
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Matrix Name Should be Unique'));
        }
        }
        catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'ComputeValues',ruleId);
        } 
    }

    public Boolean isExpressionValidForModellingSimulation(String expression)
    {
        StepCases.ExpressionWrapper parsedExpression;
        StepCases obj = new StepCases();
        parsedExpression = obj.parseExpression(expression);
        SnTDMLSecurityUtil.printDebugMessage('parsedExpression --' + parsedExpression);
        setParamNames = new Set<String>();

        if(parsedExpression.ifCase != null)
        {
            setParamNames.add(parsedExpression.ifCase.input);
            if(!isIfCaseExpressionValidForModellingSimulation(parsedExpression.ifCase))
            {
                return false;
            }

            if(parsedExpression.elif != null && parsedExpression.elif.size() > 0)
            {
                for(Integer i = 0; i < parsedExpression.elif.size(); i++)
                {
                    setParamNames.add(parsedExpression.elif[i].input);

                    if(!isIfCaseExpressionValidForModellingSimulation(parsedExpression.elif[i]))
                    {
                        return false;
                    }

                }
            }
            return true;
        }
        else
        {
            return false;
        }
    }
    public Boolean isIfCaseExpressionValidForModellingSimulation(StepCases.IfCaseWrapper ifCase)
    {

        Integer numericalParamCount = 0;
        SnTDMLSecurityUtil.printDebugMessage('inside isIfCaseExpressionValidForModellingSimulation -- case exp--' + ifCase);
        if(isMissingArgument(ifCase.input) || isMissingArgument(ifCase.condition) || isMissingArgument(ifCase.match) || isMissingArgument(ifCase.returVal))
        {
            SnTDMLSecurityUtil.printDebugMessage('mandatory cell blank');
            return false;
        }
        if(ifCase.condition.containsIgnoreCase('greater') || ifCase.condition.containsIgnoreCase('less'))
        {
            numericalParamCount++;
        }
        if(ifCase.andOr == null || (ifCase.andOr != null && ifCase.andOr.size() <= 1))
        {

            if(ifCase.andOr != null && ifCase.andOr.size() == 1)
            {
                setParamNames.add(ifCase.andOr[0].input);
                if(setParamNames.size() > 2)
                {
                    SnTDMLSecurityUtil.printDebugMessage('setParamNames' + setParamNames);
                    SnTDMLSecurityUtil.printDebugMessage('more than 2  param');
                    return false;
                }
                if(ifCase.andOr[0].condition.containsIgnoreCase('greater') || ifCase.andOr[0].condition.containsIgnoreCase('less'))
                {
                    numericalParamCount++;
                    if(numericalParamCount >= 2)
                    {
                        SnTDMLSecurityUtil.printDebugMessage('more than 2 numerical param');
                        return false;
                    }
                }
            }

            SnTDMLSecurityUtil.printDebugMessage('valid yes');
            return true;

        }
        else
        {
            SnTDMLSecurityUtil.printDebugMessage('more than 2 param');
            return false;
        }
    }
    public Boolean isMissingArgument(String argument)
    {
        if(String.isBlank(argument) || (String.isNotBlank(argument) && argument.equalsIgnoreCase('-none-')))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}