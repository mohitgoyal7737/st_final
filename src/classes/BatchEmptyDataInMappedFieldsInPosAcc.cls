global with sharing class BatchEmptyDataInMappedFieldsInPosAcc implements Database.Batchable<sObject>
{
    public String query;
    String teamInstance, ruleId, prodID;
    Map<String, Schema.DisplayType> mapFieldToType;

    global BatchEmptyDataInMappedFieldsInPosAcc(String teamIns, String rId, String product)
    {

        List<String> fieldList =  new List<String>();
        mapFieldToType = new Map<String, Schema.DisplayType>();

        String field;
        teamInstance = teamIns;
        ruleId = rId;
        prodID = product;

        query = 'Select Id, ';
        for(Source_to_Destination_Mapping__c fieldMapping : [Select Destination_Object_Field__c from Source_to_Destination_Mapping__c where Team_Instance__c = :teamInstance and Product__c = :product and Destination_Object_Field__c != '' and  Source_Object_Field__c != '' and (Load_Type__c = '' or Load_Type__c = null) WITH SECURITY_ENFORCED])
        {
            field = fieldMapping.Destination_Object_Field__c;
            if(!fieldList.contains(field))
            {
                fieldList.add(field);
                mapFieldToType.put(field, fetchDataType(field));
                query += field + ', ';
            }

        }

        query = query.removeEnd(', ');
        query += ' from AxtriaSalesIQTM__Position_Account__c';
        query += ' where AxtriaSalesIQTM__Team_Instance__c =:teamInstance WITH SECURITY_ENFORCED';
        SnTDMLSecurityUtil.printDebugMessage('query==' + query);



    }

    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Account__c> scope)
    {
        List<AxtriaSalesIQTM__Position_Account__c>  listPosAcc = new List<AxtriaSalesIQTM__Position_Account__c>();
        for(AxtriaSalesIQTM__Position_Account__c posAcc : scope)
        {
            for(String field : mapFieldToType.keySet())
            {
                if(mapFieldToType.get(field) == Schema.DisplayType.Double)
                {
                    posAcc.put(field, 0);
                }
                else if(mapFieldToType.get(field) == Schema.DisplayType.String)
                {
                    posAcc.put(field, '');
                }
            }
            listPosAcc.add(posAcc);
        }
        if(listPosAcc.size() > 0)
        {
            //update listPosAcc;
            SnTDMLSecurityUtil.updateRecords(listPosAcc, 'BatchEmptyDataInMappedFieldsInPosAcc');
        }
    }

    global void finish(Database.BatchableContext BC)
    {
        Database.executeBatch(new BatchPushToAlignment(teamInstance, ruleId, prodID), 2000);
    }

    public Schema.DisplayType fetchDataType(String fieldAPI_Name)
    {
        String objectName = 'AxtriaSalesIQTM__Position_Account__c';
        String fieldName =  fieldAPI_Name;

        Schema.DisplayType dataType = Schema.getGlobalDescribe() // or Schema.SOAPType
                                      .get(objectName)
                                      .getDescribe()
                                      .fields
                                      .getMap()
                                      .get(fieldName)
                                      .getDescribe()
                                      .getType(); // or getSOAPType()
        SnTDMLSecurityUtil.printDebugMessage('fieldName==' + fieldName + 'dataType==' + dataType);
        return dataType;
    }
}