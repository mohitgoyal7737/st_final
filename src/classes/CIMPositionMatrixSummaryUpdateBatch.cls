global class CIMPositionMatrixSummaryUpdateBatch implements Database.Batchable<sObject> {
    public String query;
    public list<AxtriaSalesIQTM__CIM_Config__c> listCIMconfig{get; set;}
    public string teamInstanceID;
    public list<string> cimConfigId;
    public list<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c> updateListPostionMetric{get; set;}
    public transient string theQuery{get; set;}
    public transient Boolean display{get;set;}    
    public transient string aggregationAttributeAPIName{get; set;}
    public transient list<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c> updateListPostionMetricDisplay{get; set;}
    public transient string configAlreadyPresent{get;set;}
    public transient string message{get;set;}

    public transient integer countRecords {get;set;}
    public transient boolean isConfigRecordAlreadyPresent{get;set;}
    public CIMPositionMatrixSummaryUpdateBatch(list<string> listconfig,String teamIns) {
    system.debug('---cimConfigId---from batch constructor' + listconfig+ '------teamInstanceID---' + teamIns);
        this.query = query;
        cimConfigId = listconfig;
        teamInstanceID = teamIns;
        
    }

    public Database.QueryLocator start(Database.BatchableContext bc) {
    system.debug('---cimConfigId---from start' + cimConfigId + '------teamInstanceID---' + teamInstanceID);
        query = 'select id, name, AxtriaSalesIQTM__Object_Name__c, AxtriaSalesIQTM__Attribute_API_Name__c, AxtriaSalesIQTM__Aggregation_Type__c, AxtriaSalesIQTM__Aggregation_Object_Name__c, AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c, AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c, AxtriaSalesIQTM__Aggregation_Condition_Attribute_Value__c  from AxtriaSalesIQTM__CIM_Config__c where id IN :cimConfigId and AxtriaSalesIQTM__team_instance__c =:teamInstanceID';
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__CIM_Config__c> scope) {
        for(AxtriaSalesIQTM__CIM_Config__c cim: scope){
         displayQuery(cim);
         if(isConfigRecordAlreadyPresent == false && updateListPostionMetric.size() > 0){
        system.debug('.....updateListPostionMetric .from if ' + updateListPostionMetric);
        insert updateListPostionMetric;
            
           // theQuery = 'The values have been inserted, thanks!';
            //message =  'The values have been inserted, thanks!';
        }
        /*else if(isConfigRecordAlreadyPresent == true && updateListPostionMetric.size()>0){
           system.debug('.....updateListPostionMetric .++++ from else if' + updateListPostionMetric);
             //insert updateListPostionMetric;
           //theQuery = 'The configuration is already present in the system!';
            configAlreadyPresent = configAlreadyPresent.removeEnd(',');
            message  = configAlreadyPresent+' --- already present in the system! <Br/> Other Configurations inserted.';
            
        }
        */
        
        else if(isConfigRecordAlreadyPresent == true ){
        system.debug('---cimConfigId---' + cimConfigId + '------teamInstanceID---' + teamInstanceID);
           //theQuery = 'The configuration is already present in the system!';
            message  = 'Configurations are already present in the system.';
        }else{
            message  = 'No Value selected';
        }
        }
        
    }

    public void finish(Database.BatchableContext BC) {

    }
    
    public void displayQuery(AxtriaSalesIQTM__CIM_Config__c obj){
        system.debug('---cimConfigId---' + cimConfigId + '------teamInstanceID---' + teamInstanceID);
   
        if(display==null){
          display = true;
        }
        isConfigRecordAlreadyPresent = false;
        
        list<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c> updateListPostionMetrictemp= new list<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c>();
        updateListPostionMetricDisplay= new list<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c>();
        countRecords = 0;
        theQuery = '';
        string theQuerytemp ='';
        configAlreadyPresent = '';
        
        
        //listCIMconfig = [select id, Object_Name__c, Attribute_API_Name__c, Aggregation_Type__c, Aggregation_Object_Name__c, Aggregation_Attribute_API_Name__c, Aggregation_Condition_Attribute_API_Name__c, Aggregation_Condition_Attribute_Value__c  from CIM_Config__c where team_instance__c=:teaminstanceID and Aggregation_Object_Name__c='Position_Account_Call_Plan__c'];
        //listCIMconfig = [select id, name, AxtriaSalesIQTM__Object_Name__c, AxtriaSalesIQTM__Attribute_API_Name__c, AxtriaSalesIQTM__Aggregation_Type__c, AxtriaSalesIQTM__Aggregation_Object_Name__c, AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c, AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c, AxtriaSalesIQTM__Aggregation_Condition_Attribute_Value__c  from AxtriaSalesIQTM__CIM_Config__c where id=:cimConfigId and AxtriaSalesIQTM__team_instance__c =:teamInstanceID];
        system.debug('....listCIMconfig...... ++ '+ listCIMconfig);
        countRecords=0;
        
       // for(AxtriaSalesIQTM__CIM_Config__c obj : listCIMconfig){
            
            updateListPostionMetric= new list<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c>();

            string objectName = obj.AxtriaSalesIQTM__Object_Name__c;
            string attributeAPIname = obj.AxtriaSalesIQTM__Attribute_API_Name__c;
            string aggregationType = obj.AxtriaSalesIQTM__Aggregation_Type__c;
            string aggregationObjectName = obj.AxtriaSalesIQTM__Aggregation_Object_Name__c;
            aggregationAttributeAPIName = obj.AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c;
            string aggregationConditionAttributeAPIName = obj.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c;
            //string aggregationConditionAttributeValue = obj.AxtriaSalesIQTM__Aggregation_Condition_Attribute_Value__c;
            
            string objectToBeQueried;
            if(aggregationAttributeAPIName!= null && aggregationObjectName!=null ){
                
                if(objectName != aggregationObjectName){
                    objectToBeQueried =  objectName.removeEnd('c');
                    objectToBeQueried = objectToBeQueried + 'r.'+attributeAPIname;
                    
                    
                    if(aggregationConditionAttributeAPIName != null){
                        theQuery = 'select '+aggregationType+'(' + objectToBeQueried + '), ' + aggregationAttributeAPIName + ' from ' +
                        aggregationObjectName + ' where AxtriaSalesIQTM__team_instance__c =:teamInstanceID and ' + aggregationConditionAttributeAPIName + ' Group by '+  aggregationAttributeAPIName;
                    }
                    else{
                        theQuery = 'select '+aggregationType+'(' + objectToBeQueried + '), ' + aggregationAttributeAPIName + ' from ' +
                        aggregationObjectName + ' where AxtriaSalesIQTM__team_instance__c =:teamInstanceID '+ ' group by '+  aggregationAttributeAPIName;
                    }
                    
                    system.debug('the Query '+ theQuery);
                }
                else{
                    objectToBeQueried = attributeAPIname;
                    if(aggregationConditionAttributeAPIName != null){
                        theQuery = 'select '+aggregationType+'(' + objectToBeQueried + '), ' + aggregationAttributeAPIName + ' from ' +
                        aggregationObjectName + ' where AxtriaSalesIQTM__team_instance__c =:teamInstanceID and ' + aggregationConditionAttributeAPIName + ' Group by '+  aggregationAttributeAPIName;
                    }
                    else{
                        theQuery = 'select '+aggregationType+'(' + objectToBeQueried + '), ' + aggregationAttributeAPIName + ' from ' +
                        aggregationObjectName + ' where AxtriaSalesIQTM__team_instance__c =:teamInstanceID '+ ' Group by '+  aggregationAttributeAPIName;
                  
                    system.debug('inside Query '+ theQuery);
                    }      
                    system.debug('======= '+ aggregationConditionAttributeAPIName);
                    system.debug('the Query '+ theQuery);
                    
                }
                
                AggregateResult[] updateList = Database.query(theQuery);
               
                system.debug('UpdateList '+ updateList);
                for(AggregateResult  objCIMConfig : updateList){
                    //aggregationObjectName objCimFromSobject = (aggregationObjectName)objCIMConfig;
                    String originalCalls = String.valueOF(objCIMConfig.get('expr0'));
                    
                    if(originalCalls == null)
                        originalCalls = '0';
                    AxtriaSalesIQTM__CIM_Position_Metric_Summary__c objCIMPositionMetric = new AxtriaSalesIQTM__CIM_Position_Metric_Summary__c(AxtriaSalesIQTM__CIM_Config__c = obj.Id, AxtriaSalesIQTM__Original__c = originalCalls, AxtriaSalesIQTM__Team_Instance__c = teamInstanceID, AxtriaSalesIQTM__Proposed__c = originalCalls, AxtriaSalesIQTM__Approved__c = originalCalls, AxtriaSalesIQTM__Position_Team_Instance__c = String.valueOF(objCIMConfig.get(aggregationAttributeAPIName)));
                    updateListPostionMetric.add(objCIMPositionMetric);
                    
                }
                
                
                set<string> givenPosTeamIns = new set<string>();
                for(AxtriaSalesIQTM__CIM_Position_Metric_Summary__c updPosMetr : updateListPostionMetric){
                    givenPosTeamIns.add(updPosMetr.AxtriaSalesIQTM__Position_Team_Instance__c);
                }
                
                string soqlQuery = 'select id, name from AxtriaSalesIQTM__position_team_instance__c where AxtriaSalesIQTM__Team_Instance_ID__c=:teamInstanceID';
                list<AxtriaSalesIQTM__Position_team_instance__c> listPosTeamIns = Database.query(soqlQuery);
                system.debug('-----listPosTeamIns ---' + listPosTeamIns[0]);
                set<string> allPosTeamIns = new set<string>();
                for(AxtriaSalesIQTM__Position_team_instance__c posTeamInsObj : listPosTeamIns){
                    allPosTeamIns.add(posTeamInsObj.id);
                }
                 
                allPosTeamIns.removeAll(givenPosTeamIns);
                system.debug('----allPosTeamIns---' + allPosTeamIns + '=======' + givenPosTeamIns);
                for(string str : allPosTeamIns){
                    system.debug('===str===' + str);
                    AxtriaSalesIQTM__CIM_Position_Metric_Summary__c objCIMPositionMetric = new AxtriaSalesIQTM__CIM_Position_Metric_Summary__c(AxtriaSalesIQTM__CIM_Config__c = obj.Id, AxtriaSalesIQTM__Original__c = '0', AxtriaSalesIQTM__Team_Instance__c = teamInstanceID, AxtriaSalesIQTM__Proposed__c = '0', AxtriaSalesIQTM__Approved__c = '0', AxtriaSalesIQTM__Position_Team_Instance__c = str);
                    updateListPostionMetric.add(objCIMPositionMetric);
                }

                countRecords = countRecords + updateListPostionMetric.size();
                set<id> positionTeamInstance = new set<id>();
                set<id> cimConfigSet = new set<id>();
                for(AxtriaSalesIQTM__CIM_Position_Metric_Summary__c objCIMpos : updateListPostionMetric){
                    positionTeamInstance.add(objCIMpos.AxtriaSalesIQTM__Position_Team_Instance__c);
                    cimConfigSet.add(objCIMpos.AxtriaSalesIQTM__CIM_Config__c);
                }
                
                list<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c> cimPositionMetriclist = [select id from AxtriaSalesIQTM__CIM_Position_Metric_Summary__c where AxtriaSalesIQTM__CIM_Config__c in:cimConfigSet and AxtriaSalesIQTM__Position_Team_Instance__c in:positionTeamInstance];
                if(cimPositionMetriclist.size() > 0){
                    isConfigRecordAlreadyPresent = true;
                    theQuery = theQuery + '\n The configuration is present in CIM_Position_Metric_Summary table already!';
                    if(display && updateListPostionMetric.size()>0){
                      updateListPostionMetrictemp.addAll(updateListPostionMetric);  
                      system.debug('updateListPostionMetrictemp....++++ '+updateListPostionMetrictemp );
                    }else{
                      configAlreadyPresent = configAlreadyPresent +obj.name + ',';               
                    }
                    
                }else{
                    
                   if(updateListPostionMetric.size()>0){
                    updateListPostionMetrictemp.addAll(updateListPostionMetric);
                     system.debug('updateListPostionMetrictemp....++++ second '+updateListPostionMetrictemp );
                   }
                }
                system.debug('updateListPostionMetric '+updateListPostionMetric);    
                
                theQuerytemp = theQuerytemp +'<br/><br/>'+ theQuery;   
                theQuery = theQuerytemp;
                
                if(updateListPostionMetric.size()>0){
                   updateListPostionMetricdisplay.addAll(updateListPostionMetric);
                }
                
                
               
            }   
        //}
        
          updateListPostionMetric = updateListPostionMetrictemp;
                
        
        
    }
}