global with sharing class talendFullLoadHit implements Database.AllowsCallouts{

    // Set<String> activityLogIDSet = new Set<String>();
    // Set<String> allTeamInstances = new Set<String>();

    // List<AxtriaSalesIQTM__Activity_Log__c> activityLogList = new List<AxtriaSalesIQTM__Activity_Log__c>();

    @Deprecated
    global talendFullLoadHit(List<String> teamInstanceList, Set<String> activitylogSet){}

    global talendFullLoadHit()
    {
        // activityLogIDSet.addAll(activitylogSet);
        // allTeamInstances.addAll(teamInstanceList);
        

        //System.debug('Check all teamIns Status');

        /*List<AxtriaSalesIQTM__Activity_Log__c> finallogList = [select Id,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Job_Status__c from AxtriaSalesIQTM__Activity_Log__c where Id in :activityLogIDSet and AxtriaSalesIQTM__Module__c = 'Veeva Full Load' and AxtriaSalesIQTM__Job_Status__c = 'Success'];

        System.debug('====finallogList.size() :::::::::' +finallogList.size());
        System.debug('====activityLogIDSet.size() :::::::::' +activityLogIDSet.size());

        Integer activityLogSize = activityLogIDSet.size();
        Integer finalLogSize = finallogList.size();
        if(activityLogSize == finalLogSize)
        {*/
            ETLWebServiceCallOutForFullLoad();
        //}
    }

    @future(callout=true) 
    public static void ETLWebServiceCallOutForFullLoad(){
        
    

        //if(activityLogSize == finalLogSize)
        //{
            system.debug('#### Hit Talend job for Full load called ####');
            
            string TalendEndpoint='http://54.72.147.25:8080/FullLoadDataTest/services/FullLoadData?method=runJob&arg0=--context_param AZ_ARSNT_EUFULL_SIQ_Username=az.asia@salesiq.com.asiafull&arg1=--context_param AZ_ARSNT_EUFULL_SIQ_Password=azasia%21%40%23%24123&arg2=--context_param AZ_Veeva_Username=salesiq.admin@az.ie.iefull&arg3=--context_param AZ_Veeva_Password=AZadmin@2019&arg4=--context_param AZ_ARSNT_EUFULL_SIQ_URL=https://test.salesforce.com/services/Soap/u/39.0&arg5=--context_param AZ_Veeva_URL=https://test.salesforce.com/services/Soap/u/39.0'; 
 
            try{
                
                
                system.debug('#### TalendEndpoint : '+TalendEndpoint);

                Http h = new Http();
                HttpRequest request = new HttpRequest();
                TalendEndpoint = TalendEndpoint.replaceAll( '\\s+', '%20');
                request.setEndPoint(TalendEndpoint);
                request.setHeader('Content-type', 'application/json');
                request.setMethod('GET');
                request.setTimeout(20000);
                system.debug('request '+request);
                HttpResponse response = h.send(request);
                if (response.getStatusCode() == 200)
                {
                        System.debug('Job ran successfully');
                }
                else
                {
                    //system.debug('Error in Talend job for Full load' +ex.getMessage());
                    String subject = 'Error in Talend job for Full load';
                    String body = 'Error while execution Talend Job for Full load';
                    String[] address = new String[]{'Ayushi.Jain@Axtria.com'};
                    //String []ccAdd=new String[]{'AZ_ROW_SalesIQ_MktDeployment@Axtria.com'};
                    Messaging.SingleEmailMessage emailwithattch = new Messaging.SingleEmailMessage();
                    emailwithattch.setSubject(subject);
                    emailwithattch.setToaddresses(address);
                    emailwithattch.setPlainTextBody(body);
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] {emailwithattch});
                }
                system.debug('#### Talend Delete Response : '+response);
            }
            catch(exception ex)
            {
                system.debug('Error in Talend job for Full load' +ex.getMessage());
                String subject = 'Error in Talend job for Full load';
                String body = 'Error while execution Talend Job for Full load:::::  ' +ex.getMessage();
                String[] address = new String[]{'Ayushi.Jain@Axtria.com'};
                //String []ccAdd=new String[]{'AZ_ROW_SalesIQ_MktDeployment@Axtria.com'};
                Messaging.SingleEmailMessage emailwithattch = new Messaging.SingleEmailMessage();
                emailwithattch.setSubject(subject);
                emailwithattch.setToaddresses(address);
                emailwithattch.setPlainTextBody(body);
                //emailwithattch.setCcAddresses(ccAdd);

                //emailwithattch.setFileAttachments(new Messaging.EmailFileAttachment[]{attach});

                // Sends the email
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] {emailwithattch});
            }
        //}
            
        
    }
    
}