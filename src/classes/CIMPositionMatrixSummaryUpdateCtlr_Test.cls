/*
@author - Prince Richard Augustin (A2661)
@description - Test class to test the CIMPositionMatrixSummaryUpdateCtlr_Test Controller.
*/
@isTest
private class CIMPositionMatrixSummaryUpdateCtlr_Test {
    
    static testMethod void testMethod1() {
        Account acc= TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        AxtriaSalesIQTM__Position__c pos =TestDataFactory.createPosition(team,teamins);
        insert pos;
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        insert positionAccountCallPlan;        list<AxtriaSalesIQTM__CIM_Config__c> listCIMconfigtemp = new list<AxtriaSalesIQTM__CIM_Config__c> ();
        AxtriaSalesIQTM__CIM_Config__c cimcc =TestDataFactory.createCIMConfig(teamins);
        cimcc.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c =NULL;
        
        insert cimcc;
        listCIMconfigtemp.add(cimcc);
        AxtriaSalesIQTM__Position_Team_Instance__c productposition =TestDataFactory.createPositionTeamInstance(pos.Id,pos.Id,teamins.Id);
        insert productposition;
        
        list<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c> updateListPostionMetrictemp= new list<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c>();
        AxtriaSalesIQTM__CIM_Position_Metric_Summary__c cimposumm =TestDataFactory.createCIMPosMetSum(cimcc,productposition,teamins);
        insert cimposumm;
        updateListPostionMetrictemp.add(cimposumm);
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        
        CIMPositionMatrixSummaryUpdateCtlr c = new CIMPositionMatrixSummaryUpdateCtlr();
        //c.cimConfigId = cimcc.Id;
        c.teaminstanceSelected = teamins.Id;
        c.listCIMconfig = listCIMconfigtemp;
        c.cimconfigrefresh();
        c.updateListPostionMetric = updateListPostionMetrictemp;     
        c.updatePostionMatrxiSummary();
    }
    static testMethod void testMethod2() {
        Account acc= TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        AxtriaSalesIQTM__Position__c pos =TestDataFactory.createPosition(team,teamins);
        insert pos;
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        insert positionAccountCallPlan;
        list<AxtriaSalesIQTM__CIM_Config__c> listCIMconfigtemp = new list<AxtriaSalesIQTM__CIM_Config__c> ();
        AxtriaSalesIQTM__CIM_Config__c cimcc =TestDataFactory.createCIMConfig(teamins);
        cimcc.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c ='AxtriaSalesIQTM__isincludedCallPlan__c=true';
        cimcc.AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c = 'AxtriaSalesIQTM__Position_Team_Instance__c';
        cimcc.AxtriaSalesIQTM__Aggregation_Type__c = 'COUNT_DISTINCT';
        
        insert cimcc;
        listCIMconfigtemp.add(cimcc);
        AxtriaSalesIQTM__Position_Team_Instance__c productposition =TestDataFactory.createPositionTeamInstance(pos.Id,pos.Id,teamins.Id);
        insert productposition;
        
        list<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c> updateListPostionMetrictemp= new list<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c>();
        AxtriaSalesIQTM__CIM_Position_Metric_Summary__c cimposumm =TestDataFactory.createCIMPosMetSum(cimcc,productposition,teamins);
        insert cimposumm;
        updateListPostionMetrictemp.add(cimposumm);
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        
        CIMPositionMatrixSummaryUpdateCtlr c = new CIMPositionMatrixSummaryUpdateCtlr();
        //c.cimConfigId = cimcc.Id;
        c.teaminstanceSelected = teamins.Id;
        c.listCIMconfig = listCIMconfigtemp;
        c.updateListPostionMetric = updateListPostionMetrictemp;     
        c.updateChannelPreference();
    }
    static testMethod void testMethod3() {
        Account acc= TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        AxtriaSalesIQTM__Position__c pos1 =TestDataFactory.createPosition(team,teamins);
        insert pos1;
        
        AxtriaSalesIQTM__Position__c pos =TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__Parent_Position__c= pos1.Id;
        insert pos;
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        insert positionAccountCallPlan;
        AxtriaSalesIQTM__Position_Team_Instance__c productposition =TestDataFactory.createPositionTeamInstance(pos.Id,pos1.Id,teamins.Id);
        insert productposition;
        
        list<AxtriaSalesIQTM__CIM_Config__c> listCIMconfigtemp = new list<AxtriaSalesIQTM__CIM_Config__c> ();
        AxtriaSalesIQTM__CIM_Config__c cimcc =TestDataFactory.createCIMConfig(teamins);
        cimcc.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c = NULL;
        cimcc.AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c = 'AxtriaSalesIQTM__Position_Team_Instance__c';
        cimcc.AxtriaSalesIQTM__Aggregation_Object_Name__c = 'AxtriaSalesIQTM__Team_Instance_Account__c';
        cimcc.AxtriaSalesIQTM__Object_Name__c ='AxtriaSalesIQTM__Position_Account_Call_Plan__c';
        cimcc.AxtriaSalesIQTM__Aggregation_Type__c = 'Sum';
        insert cimcc;
        listCIMconfigtemp.add(cimcc);
        
        
        list<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c> updateListPostionMetrictemp= new list<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c>();
        AxtriaSalesIQTM__CIM_Position_Metric_Summary__c cimposumm =TestDataFactory.createCIMPosMetSum(cimcc,productposition,teamins);
        insert cimposumm;
        updateListPostionMetrictemp.add(cimposumm);
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        CIMPositionMatrixSummaryUpdateCtlr c = new CIMPositionMatrixSummaryUpdateCtlr();
        //c.cimConfigId = cimcc.Id;
        c.teaminstanceSelected = teamins.Id;
        c.listCIMconfig = listCIMconfigtemp;
        c.updateListPostionMetric = updateListPostionMetrictemp;     
        c.displayQuery();
    }
    static testMethod void testMethod4() {
        Account acc= TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        AxtriaSalesIQTM__Position__c pos =TestDataFactory.createPosition(team,teamins);
        insert pos;
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        insert positionAccountCallPlan;
        AxtriaSalesIQTM__Position_Team_Instance__c productposition =TestDataFactory.createPositionTeamInstance(pos.Id,pos.Id,teamins.Id);
        insert productposition;
        list<AxtriaSalesIQTM__CIM_Config__c> listCIMconfigtemp = new list<AxtriaSalesIQTM__CIM_Config__c> ();
        AxtriaSalesIQTM__CIM_Config__c cimcc =TestDataFactory.createCIMConfig(teamins);
        
        cimcc.AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c = 'AxtriaSalesIQTM__Position_Team_Instance__c';
        cimcc.AxtriaSalesIQTM__Aggregation_Object_Name__c = 'AxtriaSalesIQTM__Team_Instance_Account__c';
        cimcc.AxtriaSalesIQTM__Object_Name__c ='AxtriaSalesIQTM__Position_Account_Call_Plan__c';
        cimcc.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c ='AxtriaSalesIQTM__isincludedCallPlan__c=true';
        cimcc.AxtriaSalesIQTM__Attribute_API_Name__c = 'AxtriaSalesIQTM__Metric1__c';        
        cimcc.AxtriaSalesIQTM__Aggregation_Type__c = 'Sum';
        insert cimcc;
        listCIMconfigtemp.add(cimcc);
        
        
        list<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c> updateListPostionMetrictemp= new list<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c>();
        AxtriaSalesIQTM__CIM_Position_Metric_Summary__c cimposumm =TestDataFactory.createCIMPosMetSum(cimcc,productposition,teamins);
        cimposumm.AxtriaSalesIQTM__Team_Instance__c= teamins.Id;
        cimposumm.AxtriaSalesIQTM__CIM_Config__c= cimcc.Id;
        insert cimposumm;
        updateListPostionMetrictemp.add(cimposumm);
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        
        CIMPositionMatrixSummaryUpdateCtlr c = new CIMPositionMatrixSummaryUpdateCtlr();
        //c.cimConfigId = cimcc.Id;
        c.teaminstanceSelected = teamins.Id;
        c.listCIMconfig = listCIMconfigtemp;
        c.updateListPostionMetric = updateListPostionMetrictemp;  
        c.isConfigRecordAlreadyPresent =false;
        c.scenarioChanged();
    }
}