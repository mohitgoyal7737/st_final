@isTest
private class CallCapacityCtrl_Test {
    static testMethod void testMethod1() {

        User loggedInUser = [select id,userrole.name from User where id=:UserInfo.getUserId()];
        
        system.runAs(loggedInUser){

            AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
            insert orgmas;

            AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
            insert countr;

            AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
            insert team;

            AxtriaSalesIQTM__Team_Instance__c teamins1 = new AxtriaSalesIQTM__Team_Instance__c();
            teamins1.Name = 'abcde';
            teamins1.IsHCOSegmentationEnabled__c = true;
            teamIns1.AxtriaSalesIQTM__Team__c = team.id;
        //teamins.Cycle__c = cycle.id;
            teamins1.Include_Secondary_Affiliations__c = true;
            teamins1.Include_Territory__c = true;
            teamins1.Product_Priority__c= true;
            teamins1.AxtriaSalesIQTM__IC_EffEndDate__c = date.today()+1;
            teamins1.AxtriaSalesIQTM__IC_EffstartDate__c = date.today();
            insert teamins1;

            AxtriaSalesIQTM__Workspace__c workspace = new AxtriaSalesIQTM__Workspace__c();
            workspace.Name = 'HCO';
            workspace.AxtriaSalesIQTM__Workspace_Description__c = 'Some Description';
            workspace.AxtriaSalesIQTM__Workspace_Start_Date__c = date.today();
            workspace.AxtriaSalesIQTM__Workspace_End_Date__c = date.today()+1;
            insert workspace;

            AxtriaSalesIQTM__Scenario__c scenario = TestDataFactory.newcreateScenario(teamins1,team,workspace);
            insert scenario;

            AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
            teamins.AxtriaSalesIQTM__Team__c = team.id;
            teamins.AxtriaSalesIQTM__Scenario__c = scenario.id;
        //teamins.Cycle__c = cyc.id;
            insert teamins;

            AxtriaSalesIQTM__Position__c pos = TestDataFactory.createPosition(team,teamins);
            pos.AxtriaSalesIQTM__Team_iD__c = team.Id;
        //insert pos;

            AxtriaSalesIQTM__Position__c pos1 = TestDataFactory.createPosition(team,teamins);
            pos1.AxtriaSalesIQTM__Team_iD__c = team.Id;
        //pos1.AxtriaSalesIQTM__Parent_Position__c = pos.id;
        //insert pos1;

            AxtriaSalesIQTM__User_Access_Permission__c uAccessPerm = TestDataFactory.createUserAccessPerm(pos1,teamins,UserInfo.getUserId());
            uAccessPerm.AxtriaSalesIQTM__Position__c=pos1.id;
            uAccessPerm.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
            uAccessPerm.AxtriaSalesIQTM__User__c = loggedInUser.id;
            insert uAccessPerm;

            AxtriaSalesIQTM__TriggerContol__c ccd= new AxtriaSalesIQTM__TriggerContol__c();
            ccd.Name = 'FillExternalIDonPosProduct';
            ccd.AxtriaSalesIQTM__IsStopTrigger__c = false;
            insert ccd;

            AxtriaSalesIQTM__TriggerContol__c ccd1= new AxtriaSalesIQTM__TriggerContol__c();
            ccd1.Name = 'TeamInstanceProductTrigger';
            ccd1.AxtriaSalesIQTM__IsStopTrigger__c = True;
        //ccd.Value__c = 'FillExternalIDonPosProduct';
            insert ccd1;
            Test.startTest();
            System.runAs(loggedInUser){
                ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
                String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
                List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
                System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
                CallCapacityCtrl ct = new CallCapacityCtrl();
                system.debug(''+ct.Country+ct.TIProduct+ct.jSONStringForTIProduct+ct.teaminst+ct.Cyclename+ct.allinstances);
        //ct.cycleSelected = cyc.id;
        //ct.selectedCycle = cyc.Name;
                ct.scenarioChanged();
            }
            Test.stopTest();

        }
    } 
    static testMethod void testMethod2() {

        User loggedInUser = [select id,userrole.name from User where id=:UserInfo.getUserId()];
        
        system.runAs(loggedInUser){
            AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
            insert orgmas;

            AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
            insert countr;

            AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
            insert team;

            AxtriaSalesIQTM__Team_Instance__c teamins1 = new AxtriaSalesIQTM__Team_Instance__c();
            teamins1.Name = 'abcde';
            teamins1.IsHCOSegmentationEnabled__c = true;
            teamIns1.AxtriaSalesIQTM__Team__c = team.id;
        //teamins.Cycle__c = cycle.id;
            teamins1.Include_Secondary_Affiliations__c = true;
            teamins1.Include_Territory__c = true;
            teamins1.Product_Priority__c= true;
            teamins1.AxtriaSalesIQTM__IC_EffEndDate__c = date.today()+1;
            teamins1.AxtriaSalesIQTM__IC_EffstartDate__c = date.today();
            insert teamins1;

            AxtriaSalesIQTM__Workspace__c workspace = new AxtriaSalesIQTM__Workspace__c();
            workspace.Name = 'HCO';
            workspace.AxtriaSalesIQTM__Workspace_Description__c = 'Some Description';
            workspace.AxtriaSalesIQTM__Workspace_Start_Date__c = date.today();
            workspace.AxtriaSalesIQTM__Workspace_End_Date__c = date.today()+1;
            insert workspace;

            AxtriaSalesIQTM__Scenario__c scenario = TestDataFactory.newcreateScenario(teamins1,team,workspace);
            insert scenario;

            AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
            teamins.AxtriaSalesIQTM__Team__c = team.id;
            teamins.AxtriaSalesIQTM__Scenario__c = scenario.id;
        //teamins.Cycle__c = cyc.id;
            insert teamins;

            AxtriaSalesIQTM__Position__c pos = TestDataFactory.createPosition(team,teamins);
            pos.AxtriaSalesIQTM__Team_iD__c = team.Id;
        //insert pos;

            AxtriaSalesIQTM__Position__c pos1 = TestDataFactory.createPosition(team,teamins);
            pos1.AxtriaSalesIQTM__Team_iD__c = team.Id;
        //pos1.AxtriaSalesIQTM__Parent_Position__c = pos.id;
        //insert pos1;

            AxtriaSalesIQTM__User_Access_Permission__c uAccessPerm = TestDataFactory.createUserAccessPerm(pos1,teamins,UserInfo.getUserId());
            uAccessPerm.AxtriaSalesIQTM__Position__c=pos1.id;
            uAccessPerm.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
            uAccessPerm.AxtriaSalesIQTM__User__c = loggedInUser.id;
            insert uAccessPerm;

            AxtriaSalesIQTM__TriggerContol__c ccd= new AxtriaSalesIQTM__TriggerContol__c();
            ccd.Name = 'FillExternalIDonPosProduct';
            ccd.AxtriaSalesIQTM__IsStopTrigger__c = false;
            insert ccd;

            AxtriaSalesIQTM__TriggerContol__c ccd1= new AxtriaSalesIQTM__TriggerContol__c();
            ccd1.Name = 'TeamInstanceProductTrigger';
            ccd1.AxtriaSalesIQTM__IsStopTrigger__c = True;
        //ccd.Value__c = 'FillExternalIDonPosProduct';
            insert ccd1;
            Test.startTest();
            System.runAs(loggedInUser){
                ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
                String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
                List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
                System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
                CallCapacityCtrl ct = new CallCapacityCtrl();
                ct.teamInstanceChanged();
            }
            Test.stopTest();
        } 
    }
    
  /*  static testMethod void testMethod3() {
        User loggedInUser = [select id,userrole.name from User where id=:UserInfo.getUserId()];

        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            CallCapacityCtrl ct = new CallCapacityCtrl();
        //TIProductInnerWrapper wrapper = new TIProductInnerWrapper();
        //string ProdName = wrapper.ProdName;                           
            ct.openCallCapacity();
        }
        Test.stopTest();

    }

    static testMethod void testMethod4() {
        User loggedInUser = [select id,userrole.name from User where id=:UserInfo.getUserId()];

        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            CallCapacityCtrl ct = new CallCapacityCtrl();
            ct.TeamProdLevelChanged();
        }
        Test.stopTest();

    }*/

    static testMethod void testMethod5() {

        User loggedInUser = [select id,userrole.name from User where id=:UserInfo.getUserId()];

        system.runAs(loggedInUser){
            AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
            insert orgmas;
            
            AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
            insert countr;
            
            AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
            insert team;
            
            AxtriaSalesIQTM__Team_Instance__c teamins1 = new AxtriaSalesIQTM__Team_Instance__c();
            teamins1.Name = 'abcde';
            teamins1.IsHCOSegmentationEnabled__c = true;
            teamIns1.AxtriaSalesIQTM__Team__c = team.id;
        //teamins.Cycle__c = cycle.id;
            teamins1.Include_Secondary_Affiliations__c = true;
            teamins1.Include_Territory__c = true;
            teamins1.Product_Priority__c= true;
            teamins1.AxtriaSalesIQTM__IC_EffEndDate__c = date.today()+1;
            teamins1.AxtriaSalesIQTM__IC_EffstartDate__c = date.today();
            insert teamins1;
            
            AxtriaSalesIQTM__Workspace__c workspace = new AxtriaSalesIQTM__Workspace__c();
            workspace.Name = 'HCO';
            workspace.AxtriaSalesIQTM__Workspace_Description__c = 'Some Description';
            workspace.AxtriaSalesIQTM__Workspace_Start_Date__c = date.today();
            workspace.AxtriaSalesIQTM__Workspace_End_Date__c = date.today()+1;
            insert workspace;
            
            AxtriaSalesIQTM__Scenario__c scenario = TestDataFactory.newcreateScenario(teamins1,team,workspace);
            insert scenario;
            
            AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
            teamins.Name = 'Oncology-Q4-2019';
            teamins.AxtriaSalesIQTM__Team__c = team.id;
            teamins.AxtriaSalesIQTM__Scenario__c = scenario.id;
        //teamins.Cycle__c = cyc.id;
            insert teamins;

            AxtriaSalesIQTM__Position__c pos = TestDataFactory.createPosition(team,teamins);
            pos.AxtriaSalesIQTM__Team_iD__c = team.Id;
        //insert pos;
            
            AxtriaSalesIQTM__Position__c pos1 = TestDataFactory.createPosition(team,teamins);
            pos1.AxtriaSalesIQTM__Team_iD__c = team.Id;
        //pos1.AxtriaSalesIQTM__Parent_Position__c = pos.id;
        //insert pos1;
            
            AxtriaSalesIQTM__User_Access_Permission__c uAccessPerm = TestDataFactory.createUserAccessPerm(pos1,teamins,UserInfo.getUserId());
            uAccessPerm.AxtriaSalesIQTM__Position__c=pos1.id;
            uAccessPerm.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
            uAccessPerm.AxtriaSalesIQTM__User__c = loggedInUser.id;
            insert uAccessPerm;
            
            AxtriaSalesIQTM__Organization_Master__c org = TestDataFactory.createOrganizationMaster();
            insert org;
            

            Product2 pp2= TestDataFactory.newprod();
            pp2.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
            insert pp2;

            Product_Catalog__c prodCat = TestDataFactory.productCatalog(team,teamins,countr);
            prodCat.Team__c= team.id;
            prodCat.Team_Instance__c = teamins.id;
            prodCat.Country_Lookup__c = countr.id;
            insert prodCat;
            
            AxtriaSalesIQTM__Product__c prod = TestDataFactory.createProduct(team,teamins);
            prod.Team__c = team.id;
            prod.Team_Instance__c = teamins.id; 
            insert prod;
            
            AxtriaSalesIQTM__Position_Product__c ppc = TestDataFactory.createPositionProduct(teamins,pos1,prodCat);
        //ppc.AxtriaSalesIQTM__Position__c = pos1.id;
            ppc.Product_Catalog__c = prodCat.id;
            ppc.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
            ppc.AxtriaSalesIQTM__Product_Master__c = prod.id;
            insert ppc;
            
            AxtriaSalesIQTM__Team_Instance_Product__c teaminsproduct = TestDataFactory.teamInstanceProduct(team,teamins,prod);
            teaminsproduct.AxtriaSalesIQTM__Product__c = pp2.id;
            teaminsproduct.AxtriaSalesIQTM__Product_Master__c = prod.id;
            teaminsproduct.AxtriaSalesIQTM__Team__c = team.id;
            teaminsproduct.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
            insert teaminsproduct;
            
            Team_Instance_Product_AZ__c teaminsprod = TestDataFactory.teamInstanceProductAZ(prodCat,teamins);
            teaminsprod.Team_Instance__c = teamins.id;
            teaminsprod.Product_Catalogue__c = prodCat.id;
            insert teaminsprod;
            
            AxtriaSalesIQTM__TriggerContol__c ccd= new AxtriaSalesIQTM__TriggerContol__c();
            ccd.Name = 'FillExternalIDonPosProduct';
            ccd.AxtriaSalesIQTM__IsStopTrigger__c = false;
            insert ccd;
            
            AxtriaSalesIQTM__TriggerContol__c ccd1= new AxtriaSalesIQTM__TriggerContol__c();
            ccd1.Name = 'TeamInstanceProductTrigger';
            ccd1.AxtriaSalesIQTM__IsStopTrigger__c = True;
        //ccd.Value__c = 'FillExternalIDonPosProduct';
            insert ccd1;
            
            map<string,string> testmap = new map<string,string>();
            testmap.put('VacationDays','1');
            testmap.put('Holidays','1');
            testmap.put('OtherDaysOff','1');
            
            map<string, decimal> testmap2 = new map<string, decimal>();
            testmap2.put('test', 1);
            
            CallPlanCallCapacity__c callplancallcapacity = new CallPlanCallCapacity__c();
            callplancallcapacity.is_Team_Level__c= true;
            callplancallcapacity.Name= teamins.Name;
            insert callplancallcapacity;
            List <CallPlanCallCapacity__c> sm= [select id,is_Team_Level__c from CallPlanCallCapacity__c where id=:callplancallcapacity.id];
            for ( CallPlanCallCapacity__c v:sm)
            {
                v.is_Team_Level__c=true;
            } 
            update sm;
            
            Test.StartTest();
            System.runAs(loggedInUser){
                ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
                String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
                List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
                System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
                CallCapacityCtrl.saveCallCapacity(teamins.Name, testmap , 'By Team', 'All Positions', '1');
        //CallCapacityCtrl.saveCallCapacity(teamins.Name, testmap , 'By Area', 'Unchanged Positions', '1');
                CallCapacityCtrl.saveCallCapacity(teamins.id, testmap , 'By Team', 'Unchanged Positions', '4');
                CallCapacityCtrl.saveCallCapacity(teamins.id, testmap , 'By Team', 'Unchanged Positions', '3');
                CallCapacityCtrl.saveCallCapacity(teamins.id, testmap , 'By Team', 'Unchanged Positions', '2');
                CallCapacityCtrl.saveCallCapacity2(teamins.id, 'By Team', 'Unchanged Positions', '4' , testmap2);
                List<CallCapacityCtrl.TIProductInnerWrapper> wraplist = new List<CallCapacityCtrl.TIProductInnerWrapper>(); 
                CallCapacityCtrl.TIProductInnerWrapper obj = new CallCapacityCtrl.TIProductInnerWrapper('id','name',1,1);
                string tempid = obj.id;
                Decimal testCallsPerDay = obj.CallsPerDay;                                           
                Decimal testCallCapFormula = obj.CallCapFormula;
                String testProdName = obj.ProdName;  

                CallCapacityCtrl.TIProductWrapper obj1 = new CallCapacityCtrl.TIProductWrapper(teaminsprod,'name',1,wraplist);
                Date testendDate = obj1.endDate;
                Decimal testmetric1 = obj1.metric1;
                Decimal testmetric2 = obj1.metric2;
                Decimal testmetric3 = obj1.metric3;
                Decimal testmetric4 = obj1.metric4;
                Decimal testmetric5 = obj1.metric5;
                Decimal testmetric6 = obj1.metric6;
                Decimal testmetric7 = obj1.metric7;
                Decimal testmetric8 = obj1.metric8;
                Decimal testmetric9 = obj1.metric9;
                Decimal testmetric10 = obj1.metric10;
                String testposition = obj1.position;
                String testproduct = obj1.product;
                String testproductMaster = obj1.productMaster;
                String testpositionName = obj1.positionName;
                String testproductName =obj1.productName;
                String productMasterName =obj1.productMasterName;
                String teamInstance = obj1.teamInstance; 
                String teamInstanceName =obj1.teamInstanceName;                                                                                                                                     
                boolean testisActive = obj1.isActive;
                Decimal productWeight =obj1.productWeight; 
                Decimal Business_Days_in_Cycle_Formula = obj1.Business_Days_in_Cycle_Formula; 
                Decimal Business_Days_in_Cycle = obj1.Business_Days_in_Cycle;
                Decimal Call_Capacity_Formula = obj1.Call_Capacity_Formula;
                Decimal Calls_Day = obj1.Calls_Day;
                Decimal Effective_Days_in_Field_Formula = obj1.Effective_Days_in_Field_Formula;
                Decimal Holidays = obj1.Holidays;
                Decimal Other_Days_Off = obj1.Other_Days_Off;
                Decimal Vacation_Days = obj1.Vacation_Days;
                string selectedradio = obj1.selectedradio;
                string ProductCatalog =obj1.ProductCatalog;
                string ProductCatalogName = obj1.ProductCatalogName;                                   

                Decimal  Total_Call_Capacity = obj1.Total_Call_Capacity;     

                system.debug(obj1.posProdListString);
                system.debug(obj1.posProdInnerMap);
                system.debug(obj1.posProdInnerMapString);
                system.debug(obj1.getOptions());   
            }                                                                                                                                                                                                                  
            Test.StopTest();

        }
    }
}