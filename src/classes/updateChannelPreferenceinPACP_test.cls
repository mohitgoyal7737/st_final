@isTest
private class updateChannelPreferenceinPACP_test {
    static testMethod void testMethod1() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc = TestDataFactory.createAccount();
        acc.Type = 'HCP';
        insert acc;
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.Product_Priority__c = false;
        teamins.MultiChannel__c  = true;
        teamins.Enable_Channel_Preference__c = true;
        insert teamins;
        
        AxtriaSalesIQTM__Position__c pos = TestDataFactory.createPosition(team,teamins);
        insert pos;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;

        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;

        Measure_Master__c mMaster = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        insert mMaster;

        
       
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        pPriority.Type__c = 'Channel Preference';
        pPriority.Account__c = acc.Id;
        pPriority.Channel_1_Name__c = 'F2F';
        pPriority.Channel_1_Preference__c = '2';
        pPriority.Channel_2_Name__c = 'Virtual Webinar';
        pPriority.Channel_2_Preference__c = '2';
        pPriority.Channel_3_Name__c = 'Email';
        pPriority.Channel_3_Preference__c = '2';
        pPriority.Channel_4_Name__c = 'Skype';
        pPriority.Channel_4_Preference__c = '2';
        pPriority.Channel_5_Name__c = 'Telephone';
        pPriority.Channel_5_Preference__c = '2';
        pPriority.Product_Name__c = 'GIST';
        insert pPriority;
        
        Team_Instance_Config__c teamconfig = new Team_Instance_Config__c();
        teamconfig.Team_Instance__c = teamins.id;
        teamconfig.Cutomer_Type__c ='HCP';
        teamconfig.IsChannelPreferenceProductIndependent__c = true;
        teamconfig.Channel_Preferene_Filter_Criteria__c = '2';
        insert teamconfig;

        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mMaster,acc,teamins,posAccount,new Product_Priority__c(),pos);
        positionAccountCallPlan.AxtriaSalesIQTM__isIncludedCallPlan__c = true;
        positionAccountCallPlan.Share__c = true;
        positionAccountCallPlan.AxtriaSalesIQTM__Change_Status__c ='Pending for Submission';
        positionAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c = true;
        insert positionAccountCallPlan;

        List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacplist = new List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        pacplist.add(positionAccountCallPlan);

        AxtriaSalesIQTM__SalesIQ_Logger__c slog = new AxtriaSalesIQTM__SalesIQ_Logger__c();
        insert slog;
        AxtriaSalesIQTM__Change_Request_Type__c crtype = TestDataFactory.createChangeReqType(SalesIQGlobalConstants.CR_TYPE_CALL_PLAN );
        insert crtype;

        AxtriaSalesIQTM__CIM_Config__c cim = TestDataFactory.createCIMConfig(teamins);
        cim.AxtriaSalesIQTM__Change_Request_Type__c  = crtype.id;
        insert cim;
        
        AxtriaSalesIQTM__TriggerContol__c ccd1= new AxtriaSalesIQTM__TriggerContol__c();
        ccd1.Name = 'ChangeRequestTrigger';
        ccd1.AxtriaSalesIQTM__IsStopTrigger__c = true;
        insert ccd1;
        
        AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
        cr.AxtriaSalesIQTM__RecordTypeID__c = crtype.id;
        insert cr;

        temp_Obj__c tempobj = new temp_Obj__c();
        tempobj.Channel_1__c = 'F2F';
        tempobj.Channel_2__c = 'Virtual Webinar';
        tempobj.Channel_3__c = 'Email';
        tempobj.Channel_4__c = 'Skype';
        tempobj.Channel_5__c = 'Telephone';
        tempobj.Objective_1__c='2';
        tempobj.Objective_2__c='2';
        tempobj.Objective_3__c='2';
        tempobj.Objective_4__c='2';
        tempobj.Objective_5__c='2';
        tempobj.AccountNumber__c = acc.AccountNumber;
        tempobj.Product_Name__c = 'GIST';
        tempobj.Object__c = 'Channel Preference';
        tempobj.Change_Request__c = cr.id;
        tempobj.Status__c = 'New';
        insert tempobj;
         

        
        
        updateChannelPreferenceinPACP obj = new updateChannelPreferenceinPACP();
        obj.updateChannelPreferenceinPACP(pacplist, false);

        teamconfig.IsChannelPreferenceProductIndependent__c = false;
        update teamconfig;

        obj = new updateChannelPreferenceinPACP();
        obj.updateChannelPreferenceinPACP(pacplist, false);

        teamconfig.Cutomer_Type__c = null;
        update teamconfig;

        obj = new updateChannelPreferenceinPACP();
        obj.updateChannelPreferenceinPACP(pacplist, false);

        BatchUpdateChannelPreferenceinPACP obj1 = new BatchUpdateChannelPreferenceinPACP(new List<String>{teamins.id});
        Database.executeBatch(obj1);
        obj1 = new BatchUpdateChannelPreferenceinPACP(new List<String>{teamins.id},(String)mMaster.id,false,false);
        Database.executeBatch(obj1);
        obj1 = new BatchUpdateChannelPreferenceinPACP(new List<String>{teamins.id},(String)mMaster.id,false,false,(String)slog.id,'AxtriaSalesIQTM__');
        Database.executeBatch(obj1);
        obj1 = new BatchUpdateChannelPreferenceinPACP(new List<String>{teamins.id},(String)slog.id,'AxtriaSalesIQTM__');
        Database.executeBatch(obj1);

        BatchPopulateChannelPreferenceData obj2 = new BatchPopulateChannelPreferenceData((String)cr.id);
        Database.executeBatch(obj2);
        obj2 = new BatchPopulateChannelPreferenceData((String)cr.id,true);
        Database.executeBatch(obj2);

   
        String className = 'updateChannelPreferenceinPACP_test';
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = :className];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));

        
    }
   
    
}