global with sharing class Batch_DeleteObjectTerritory implements Database.Batchable<sObject> {
    list<Staging_Position_Account__c> listPosAcc;
    string jobType = util.getJobType('ObjectTerritory');
    set<string> allCountries = jobType == 'Full Load'?util.getFulloadCountries():util.getDeltaLoadCountries();
   // map<string, boolean> mapProdStatus;
    string query ;
    
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        if(jobType == 'Full Load'){
            query = 'Select Id, Assignment_Status__c, Country__c, Record_Status__c from Staging_Position_Account__c WITH SECURITY_ENFORCED';
        }
        else{    
            query = 'Select Id, Assignment_Status__c, Record_Status__c from Staging_Position_Account__c Where Record_Status__c = \'Updated\' WITH SECURITY_ENFORCED';
        }
        return database.getQueryLocator(query);
    }
            
    global void execute(Database.BatchableContext bc, List<Staging_Position_Account__c> scope){
        listPosAcc = new list<Staging_Position_Account__c>();
        if(jobType == 'Full Load'){
            for(Staging_Position_Account__c pe : scope){
                if(allCountries.contains(pe.Country__c)){
                    pe.Assignment_Status__c = 'Inactive';
                    pe.Record_Status__c = 'Updated';
                    listPosAcc.add(pe);
                }
                else{
                    pe.Record_Status__c = 'No Change';
                    listPosAcc.add(pe);    
                }
            }
        }
        else{
            for(Staging_Position_Account__c pe : scope){
                pe.Record_Status__c = 'No Change';
                listPosAcc.add(pe);
            }    
        }
        if(listPosAcc.size()>0){
            //upsert listPosAcc;
            SnTDMLSecurityUtil.upsertRecords(listPosAcc, 'Batch_DeleteObjectTerritory');
        }
    }
    
    global void finish(Database.BatchableContext bc){
        if(!System.Test.isRunningTest())
            database.executeBatch(new Batch_ObjectTerritoryMapping(), 2000);
    }
}