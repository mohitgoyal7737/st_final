public inherited sharing class SalesIQSnTLogger{
 
    public static final String BR_MODULE = 'BR Module' ;
    public static final String CPF_MODULE = 'CPF Module' ;
    public static final String MCCP_MODULE = system.label.MCCP_Error_Handling_Module;
    public static final String PROFILING_DATA_MODULE = 'Profiling Data Module';
        
    public static void createHandledErrorLogs(Exception e, String moduleName,String businessRule) {
        SnT_Error_Logger__e objHandledPE = new SnT_Error_Logger__e(
            Message__c = e.getMessage(),
            Exception_Type__c = e.getTypeName(),
            Line_Number__c = e.getLineNumber(),
            Module__c = moduleName,
            Stack_Trace__c = e.getStackTraceString(), 
            Type__c = string.valueOf(SalesIQExceptionType.HANDLED),
            BusinessRule__c = businessRule
            );
           //List<Database.SaveResult> results = EventBus__e.publish(objHandledPE);
        EventBus.publish(objHandledPE);
    }

    public static void createUnHandledErrorLogs(Exception e, String moduleName, String className, String businessRule) {
        SnT_Error_Logger__e objHandledPE = new SnT_Error_Logger__e(
            Message__c = e.getMessage(),
            Exception_Type__c = e.getTypeName(),
            Line_Number__c = e.getLineNumber(),
            Module__c = moduleName,
            //Status__c = string.valueOf(e.SnT_ErrorCode),
            Stack_Trace__c = className +': '+e.getStackTraceString(),             
            Type__c = string.valueOf(SalesIQExceptionType.UNHANDLED),
            BusinessRule__c = businessRule
            );

        EventBus.publish(objHandledPE);
    }

    public static void createHandledErrorLogsforCPF(Exception e, String moduleName,String TeamInstance) {
        SnT_Error_Logger__e objHandledPE = new SnT_Error_Logger__e(
            Message__c = e.getMessage(),
            Exception_Type__c = e.getTypeName(),
            Line_Number__c = e.getLineNumber(),
            Module__c = moduleName,
            Stack_Trace__c = e.getStackTraceString(), 
            Type__c = string.valueOf(SalesIQExceptionType.HANDLED),
            //BusinessRule__c = businessRule
            TeamInstance__c = TeamInstance
            );
           //List<Database.SaveResult> results = EventBus__e.publish(objHandledPE);
        EventBus.publish(objHandledPE);
    }

    public static void createUnHandledErrorLogsforCPF(Exception e, String moduleName, String className, String TeamInstance) {
        SnT_Error_Logger__e objHandledPE = new SnT_Error_Logger__e(
            Message__c = e.getMessage(),
            Exception_Type__c = e.getTypeName(),
            Line_Number__c = e.getLineNumber(),
            Module__c = moduleName,
            //Status__c = string.valueOf(e.SnT_ErrorCode),
            Stack_Trace__c = className +': '+e.getStackTraceString(),             
            Type__c = string.valueOf(SalesIQExceptionType.UNHANDLED),
            //BusinessRule__c = businessRule
            TeamInstance__c = TeamInstance
            );
        EventBus.publish(objHandledPE);
    } 

    public static void createHandledErrorLogsforMatrix(Exception e, String moduleName,String Matrix) {
        SnT_Error_Logger__e objHandledPE = new SnT_Error_Logger__e(
            Message__c = e.getMessage(),
            Exception_Type__c = e.getTypeName(),
            Line_Number__c = e.getLineNumber(),
            Module__c = moduleName,
            Stack_Trace__c = e.getStackTraceString(), 
            Type__c = string.valueOf(SalesIQExceptionType.HANDLED),
            Grid__c = Matrix
            );
        EventBus.publish(objHandledPE);
    }

    public static void createUnHandledErrorLogsforMatrix(Exception e, String moduleName, String className, String Matrix) {
        SnT_Error_Logger__e objHandledPE = new SnT_Error_Logger__e(
            Message__c = e.getMessage(),
            Exception_Type__c = e.getTypeName(),
            Line_Number__c = e.getLineNumber(),
            Module__c = moduleName,
            Stack_Trace__c = className +': '+e.getStackTraceString(),             
            Type__c = string.valueOf(SalesIQExceptionType.UNHANDLED),
            Grid__c = Matrix
            );
        EventBus.publish(objHandledPE);
    }  

    public static void createUnHandledErrorLogsforProfilingData(Exception e, String moduleName, String className) {
        SnT_Error_Logger__e objHandledPE = new SnT_Error_Logger__e(
            Message__c = e.getMessage(),
            Exception_Type__c = e.getTypeName(),
            Line_Number__c = e.getLineNumber(),
            Module__c = moduleName,
            Stack_Trace__c = className +': '+e.getStackTraceString(),             
            Type__c = string.valueOf(SalesIQExceptionType.UNHANDLED)
            );
        EventBus.publish(objHandledPE);
    }        
}