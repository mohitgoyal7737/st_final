global with sharing class SIQUserManagerUpdateOutbound implements Database.Batchable<sObject>,Database.Stateful {
    public String query;
    public Set<String> setOutboundEmpID;
    public Set<String> insertEmpID;
    public Set<String> insertedSet;
    public List<SIQ_User_Manager_Role_Outbound__c> statusUpdList;
    public List<SIQ_User_Manager_Role_Outbound__c> insertEmpRecList;
    public List<SIQ_User_Manager_Role_Outbound__c> updateOutboundEmpList;
    public Map<String,String> mapPos2Emp;
    public Map<String,String> mapPos2ParentPos;
    public Map<String,String> newMapEmp2Pos;
    public Map<String,Map<String,String>> emp2MapType2Pos;
    public Map<String,String> oldMapEmp2Manager;
    public Map<String,String> oldMapEmp2Pos;
    public Set<ID> duplicateRecs;

    global SIQUserManagerUpdateOutbound() {
        query='';
        duplicateRecs = new Set<ID>();
        setOutboundEmpID=new Set<String>();
        insertedSet=new Set<String>();
        insertEmpID=new Set<String>();
        
        mapPos2Emp=new Map<String,String>();
        mapPos2ParentPos=new Map<String,String>();
        newMapEmp2Pos=new Map<String,String>();
        emp2MapType2Pos=new Map<String,Map<String,String>>();
        oldMapEmp2Manager=new Map<String,String>();
        oldMapEmp2Pos=new Map<String,String>();
        query = 'Select Id,Employee_PRID__c,AxtriaSalesIQTM__Manager__c,Current_Territory1__c from AxtriaSalesIQTM__Employee__c where AxtriaSalesIQTM__HR_Status__c =\'Active\'';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Employee__c> empMasterList) {

        statusUpdList=new List<SIQ_User_Manager_Role_Outbound__c>();
        insertEmpRecList=new List<SIQ_User_Manager_Role_Outbound__c>();
        updateOutboundEmpList=new List<SIQ_User_Manager_Role_Outbound__c>();
        
        List<SIQ_User_Manager_Role_Outbound__c> outboundList = [select Id,Client_Territory_Code__c,Employee__c,Employee_PRID__c,Manager__c,Manager_PRID__c,Status__c from SIQ_User_Manager_Role_Outbound__c  WHERE Employee__c != NULL WITH SECURITY_ENFORCED];
        SnTDMLSecurityUtil.printDebugMessage('=====Query::::' +outboundList);
        if(outboundList != null){
            for(SIQ_User_Manager_Role_Outbound__c outbountEmpRec : outboundList){
                oldMapEmp2Pos.put(outbountEmpRec.Employee__c,outbountEmpRec.Client_Territory_Code__c);
                oldMapEmp2Manager.put(outbountEmpRec.Employee__c,outbountEmpRec.Manager__c);
                setOutboundEmpID.add(outbountEmpRec.Employee__c);
                //outbountEmpRec.Status__c='';
                //statusUpdList.add(outbountEmpRec);
            }
        }
        List<SIQ_User_Manager_Role_Outbound__c> outboundListstatus = [select Id,Client_Territory_Code__c,Employee__c,Employee_PRID__c,Manager__c,Manager_PRID__c,Status__c from SIQ_User_Manager_Role_Outbound__c where Status__c = 'Updated' WITH SECURITY_ENFORCED];
        if(outboundListstatus != null){
            for(SIQ_User_Manager_Role_Outbound__c statusoutbountEmpRec : outboundListstatus){
                statusoutbountEmpRec.Status__c='';
                statusUpdList.add(statusoutbountEmpRec);
            }
        }

        SnTDMLSecurityUtil.printDebugMessage('====statusUpdList.size():::::' +statusUpdList.size());
        if(statusUpdList.size() > 0){
           // update statusUpdList;
           SnTDMLSecurityUtil.updateRecords(statusUpdList, 'SIQUserManagerUpdateOutbound');
       }

       SnTDMLSecurityUtil.printDebugMessage('====To check if employee exists in Outbound object======');
       /*List<AxtriaSalesIQTM__Employee__c> empMasterList = [Select Id,Employee_PRID__c,AxtriaSalesIQTM__Manager__c,Current_Territory1__c from AxtriaSalesIQTM__Employee__c where AxtriaSalesIQTM__HR_Status__c ='Active'];*/
       for(AxtriaSalesIQTM__Employee__c empRec : empMasterList){
        if(!setOutboundEmpID.contains(empRec.Id)){
            insertEmpID.add(empRec.Id);
        }
    }

    SnTDMLSecurityUtil.printDebugMessage('====To be inserted Employee set:::::' +insertEmpID);
    SnTDMLSecurityUtil.printDebugMessage('====To be inserted Employee set size:::::' +insertEmpID.size()); 

    SnTDMLSecurityUtil.printDebugMessage('====Query on Position List=========');
    List<AxtriaSalesIQTM__Position__c> posList =[select Id,Employee1__c,AxtriaSalesIQTM__Employee__c,AxtriaSalesIQTM__Parent_Position__c,AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Client_Position_Code__c,Employee1_Assignment_Type__c,AxtriaSalesIQTM__Parent_Position__r.Employee1__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__inactive__c=false and Employee1__c != null and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c ='Current' and (AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c ='Live' or AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c ='Published') WITH SECURITY_ENFORCED];

    for(AxtriaSalesIQTM__Position__c posRec : posList)
    {
        mapPos2Emp.put(posRec.AxtriaSalesIQTM__Client_Position_Code__c,posRec.Employee1__c);
        mapPos2ParentPos.put(posRec.AxtriaSalesIQTM__Client_Position_Code__c,posRec.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
        
    }
        //SnTDMLSecurityUtil.printDebugMessage('======emp2MapType2Pos::::::'+emp2MapType2Pos);
    SnTDMLSecurityUtil.printDebugMessage('======mapPos2Emp::::::'+mapPos2Emp);
    SnTDMLSecurityUtil.printDebugMessage('======mapPos2ParentPos::::::'+mapPos2ParentPos);

    SnTDMLSecurityUtil.printDebugMessage('====Query on Position Employee List=========');
    List<AxtriaSalesIQTM__Position_Employee__c> peList = [select Id,AxtriaSalesIQTM__Employee__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Assignment_Type__c from AxtriaSalesIQTM__Position_Employee__c where AxtriaSalesIQTM__Assignment_Status__c='Active' and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__inactive__c=false and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c ='Current' and (AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c ='Live' or AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c ='Published') WITH SECURITY_ENFORCED];

    SnTDMLSecurityUtil.printDebugMessage('============Fill new maps for Emp2Pos==============');
    for(AxtriaSalesIQTM__Position_Employee__c posempRec : peList)
    {
        
        if(posempRec.AxtriaSalesIQTM__Assignment_Type__c=='Primary'){
            newMapEmp2Pos.put(posempRec.AxtriaSalesIQTM__Employee__c,posempRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
        }
        
    }

    for(AxtriaSalesIQTM__Position_Employee__c posempRec : peList)
    {
        
        if(!newMapEmp2Pos.containsKey(posempRec.AxtriaSalesIQTM__Employee__c)){
            if(posempRec.AxtriaSalesIQTM__Assignment_Type__c=='Secondary'){
                newMapEmp2Pos.put(posempRec.AxtriaSalesIQTM__Employee__c,posempRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
            }
        }
        
    }


    SnTDMLSecurityUtil.printDebugMessage('======newMapEmp2Pos::::::'+newMapEmp2Pos);


    SnTDMLSecurityUtil.printDebugMessage('====Insert or Update Outbound Employee Rec');
    List<SIQ_User_Manager_Role_Outbound__c> outboundEmpList = [select Id,Client_Territory_Code__c,Employee__c,Employee_PRID__c,Manager__c,Manager_PRID__c,Status__c from SIQ_User_Manager_Role_Outbound__c WHERE Employee__c != NULL WITH SECURITY_ENFORCED];
    SnTDMLSecurityUtil.printDebugMessage('==============outboundEmpList.size()::::'+outboundEmpList.size());
    SnTDMLSecurityUtil.printDebugMessage('=================outboundEmpList::::'+outboundEmpList);
    if(outboundEmpList != null || outboundEmpList.size() > 0){
        for(SIQ_User_Manager_Role_Outbound__c insertEmp : outboundEmpList){
            if(!insertEmpID.contains(insertEmp.Employee__c) && insertEmpID !=null && insertEmpID.size() > 0)
            {
                
                SnTDMLSecurityUtil.printDebugMessage('Insert that Employee Records::::::');
                for(String s : insertEmpID)
                {
                    if(!insertedSet.contains(s)){
                        SIQ_User_Manager_Role_Outbound__c insertEmp1 = new SIQ_User_Manager_Role_Outbound__c();
                        insertEmp1.Employee__c=s;
                        insertEmp1.Client_Territory_Code__c=newMapEmp2Pos.get(s);
                        String pos=newMapEmp2Pos.get(s);
                        String parentPos=mapPos2ParentPos.get(pos);
                        String manager =mapPos2Emp.get(parentPos);
                        insertEmp1.Manager__c=manager;
                        insertEmp1.Status__c='Updated';
                        insertEmpRecList.add(insertEmp1);
                        insertedSet.add(insertEmp1.Employee__c);
                    }
                }
            }
            if(newMapEmp2Pos.containsKey(insertEmp.Employee__c)){
                SnTDMLSecurityUtil.printDebugMessage('Check for the updated Manager Position Data:::::::');
                Boolean flag = false;
                    //if(newMapEmp2Pos.containsKey(insertEmp.Employee__c))
                    //{
                SnTDMLSecurityUtil.printDebugMessage('Inside New mapE2P contains the employee' +insertEmp.Employee__c);
                if(oldMapEmp2Pos.get(insertEmp.Employee__c) != newMapEmp2Pos.get(insertEmp.Employee__c))
                {
                    flag=true;
                    insertEmp.Client_Territory_Code__c=newMapEmp2Pos.get(insertEmp.Employee__c);
                    insertEmp.Status__c='Updated';
                    SnTDMLSecurityUtil.printDebugMessage('====oldMapEmp2Pos.get(insertEmp.Employee__c):::' +oldMapEmp2Pos.get(insertEmp.Employee__c));
                }
                String pos =newMapEmp2Pos.get(insertEmp.Employee__c);
                String parentPos=mapPos2ParentPos.get(pos);
                String emp =mapPos2Emp.get(parentPos);
                if(oldMapEmp2Manager.get(insertEmp.Employee__c) != emp)
                {
                        //empRec.AxtriaSalesIQTM__Manager__c = emp;
                    if(emp != null && emp != insertEmp.Employee__c)
                    {
                        insertEmp.Manager__c = emp;
                    }
                    else
                    {
                        insertEmp.Manager__c =null;
                    }
                    insertEmp.Status__c='Updated';
                    flag=true;
                    SnTDMLSecurityUtil.printDebugMessage('====oldMapEmp2Manager.get(insertEmp.Employee__c):::' +oldMapEmp2Manager.get(insertEmp.Employee__c));
                    SnTDMLSecurityUtil.printDebugMessage('====pos:::' +pos);
                    SnTDMLSecurityUtil.printDebugMessage('====parentPos:::' +parentPos);
                    SnTDMLSecurityUtil.printDebugMessage('====emp:::' +emp);
                }
                SnTDMLSecurityUtil.printDebugMessage('==============emp::::'+emp);
                SnTDMLSecurityUtil.printDebugMessage('==============oldMapEmp2Manager.get(insertEmp.Employee__c)::::'+oldMapEmp2Manager.get(insertEmp.Employee__c));
                
                if(flag)
                {
                    if(duplicateRecs.contains(insertEmp.Employee__c))
                    {
                        SnTDMLSecurityUtil.printDebugMessage('+++++++++++++++++ Duplicate Recs ' + updateOutboundEmpList);
                    }
                    else
                    {
                        updateOutboundEmpList.add(insertEmp);
                        duplicateRecs.add(insertEmp.Employee__c);       
                    }
                    
                }
            }
                else //if(!newMapEmp2Pos.containsKey(insertEmp.Employee__c))
                {
                    SnTDMLSecurityUtil.printDebugMessage('Inside else part which does not contains the employee' +insertEmp.Employee__c);
                    if(oldMapEmp2Pos.get(insertEmp.Employee__c) != null || oldMapEmp2Manager.get(insertEmp.Employee__c) !=null)
                    {
                        insertEmp.Manager__c = null;
                        insertEmp.Client_Territory_Code__c=null;
                        insertEmp.Status__c='Updated';
                        
                        if(duplicateRecs.contains(insertEmp.Employee__c))
                        {
                            SnTDMLSecurityUtil.printDebugMessage('+++++++++++++++++ Duplicate Recs ' + updateOutboundEmpList);
                        }
                        else
                        {
                            updateOutboundEmpList.add(insertEmp);
                            duplicateRecs.add(insertEmp.Employee__c);       
                        }
                        SnTDMLSecurityUtil.printDebugMessage('Employee Record======' +insertEmp);
                    }

                }
                //}
                
                
            }

        }
        //commenting else
        SnTDMLSecurityUtil.printDebugMessage('==========else part==outboundEmpList :'+outboundEmpList.size());
        if(outboundEmpList ==null || outboundEmpList.size()==0){
            SnTDMLSecurityUtil.printDebugMessage('=====Else Part::::::::::::');
            SnTDMLSecurityUtil.printDebugMessage('Loop on insert Employee List::::::' +insertEmpID);
            for(String s : insertEmpID){
                SnTDMLSecurityUtil.printDebugMessage('Insert all Employees at first');
                SIQ_User_Manager_Role_Outbound__c insertEmp = new SIQ_User_Manager_Role_Outbound__c();    
                insertEmp.Employee__c=s;
                insertEmp.Client_Territory_Code__c=newMapEmp2Pos.get(s);
                String pos=newMapEmp2Pos.get(s);
                String parentPos=mapPos2ParentPos.get(pos);
                String manager =mapPos2Emp.get(parentPos);
                insertEmp.Manager__c=manager;
                insertEmp.Status__c='Updated';
                insertEmpRecList.add(insertEmp);
            }
        }

        SnTDMLSecurityUtil.printDebugMessage('=======insertEmpRecList.size()=====' +insertEmpRecList.size());
        if(insertEmpRecList.size() > 0){
            //insert insertEmpRecList;
            SnTDMLSecurityUtil.insertRecords(insertEmpRecList, 'SIQUserManagerUpdateOutbound');
        }
        
        

        SnTDMLSecurityUtil.printDebugMessage('=======updateOutboundEmpList.size()=====' +updateOutboundEmpList.size());
        if(updateOutboundEmpList.size() > 0){
            //update updateOutboundEmpList;
            SnTDMLSecurityUtil.updateRecords(updateOutboundEmpList, 'SIQUserManagerUpdateOutbound');
        }
        
        

    }

    global void finish(Database.BatchableContext BC) {

    }
}