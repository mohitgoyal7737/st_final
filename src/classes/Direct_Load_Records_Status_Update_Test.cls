/**********************************************************************************************
Author      : Himanshu Tariyal (A0994)
Date        : 24th September'2020
Description : Test class for Direct_Load_Records_Status_Update
Revision(s) : v1.0
**********************************************************************************************/
@isTest
private class Direct_Load_Records_Status_Update_Test 
{
    static testMethod void testMethod1() 
    {
    	String className = 'Direct_Load_Records_Status_Update_Test';
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'test';
        SnTDMLSecurityUtil.insertRecords(team,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins1 = new AxtriaSalesIQTM__Team_Instance__c();
        teamins1.Name = 'abcde';
        teamins1.AxtriaSalesIQTM__Team__c = team.id;
        teamins1.AxtriaSalesIQTM__IC_EffEndDate__c = date.today()+1;
        teamins1.AxtriaSalesIQTM__IC_EffstartDate__c = date.today();
        SnTDMLSecurityUtil.insertRecords(teamins1,className);
        
        AxtriaSalesIQTM__Workspace__c workspace = new AxtriaSalesIQTM__Workspace__c();
        workspace.Name = 'HCO';
        workspace.AxtriaSalesIQTM__Workspace_Description__c = 'Some Description';
        workspace.AxtriaSalesIQTM__Workspace_Start_Date__c = date.today();
        workspace.AxtriaSalesIQTM__Workspace_End_Date__c = date.today()+1;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        AxtriaSalesIQTM__Scenario__c scenario = TestDataFactory.newcreateScenario(teamins1,team,workspace);
        SnTDMLSecurityUtil.insertRecords(scenario,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.Name = 'Oncology-Q4-2019';
        teamins.AxtriaSalesIQTM__Team__c = team.id;
        teamins.AxtriaSalesIQTM__Scenario__c = scenario.id;
        teamins.AxtriaSalesIQTM__IC_EffEndDate__c = date.today()+1;
        teamins.AxtriaSalesIQTM__IC_EffstartDate__c = date.today();
        SnTDMLSecurityUtil.insertRecords(teamins,className);           
        
        AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
        cr.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.Id;
        SnTDMLSecurityUtil.insertRecords(cr,className);
        
        temp_Obj__c zt = new temp_Obj__c();
        zt.Change_Request__c = cr.Id;
        zt.Status__c ='New';//
        SnTDMLSecurityUtil.insertRecords(zt,className);

        System.Test.startTest();

        String nameSpace = MCCP_Utility.sntNamespace(className);
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Status__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(temp_Obj__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        
        Direct_Load_Records_Status_Update.Direct_Load_Records_Status_Update(cr.Id,className);

        System.Test.stopTest();
    }
}