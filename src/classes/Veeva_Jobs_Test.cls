@isTest
private class Veeva_Jobs_Test {
    /* static testMethod void testMethod1() {
User loggedInUser = new User(id=UserInfo.getUserId());


AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
insert orgmas;
AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
insert countr;

AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
insert team;
AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
insert teamins;

AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
workspace.AxtriaSalesIQTM__Country__c = countr.id;
insert workspace;


AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
insert scen;
Account acc= TestDataFactory.createAccount();
acc.AccountNumber ='BH10643156';
insert acc;

AxtriaSalesIQTM__Affiliation_Network__c affnet = TestDataFactory.createAffliNet(countr);
insert affnet;
AxtriaSalesIQTM__Account_Affiliation__c accaff = TestDataFactory.createAcctAffli(acc,affnet);
insert accaff;

Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
insert pcc;

Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
mmc.Team_Instance__c = teamins.id;
insert mmc;
Measure_Master__c mmc1 = TestDataFactory.createMeasureMaster(pcc, team, teamins);
mmc1.Team_Instance__c = teamins.id;
insert mmc1;
Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
pp.Name ='MARKET_ CRC';
insert pp;
Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
rp.Parameter__c = pp.id;
insert rp;

AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
insert pos;
AxtriaSalesIQTM__User_Access_Permission__c u = TestDataFactory.createUserAccessPerm(pos, teamins, UserInfo.getUserId());
u.AxtriaSalesIQTM__Position__c=pos.id;
u.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
u.AxtriaSalesIQTM__User__c = loggedInUser.id;        
insert u;

AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
insert posAccount;

BU_Response__c bu = TestDataFactory.createBuResponse(posAccount,pcc,teamins,team,acc);
insert bu;
HCO_Parameters__c hcoParam = TestDataFactory.createHCOParameter(rp,mmc);
hcoParam.Source_Rule__c = mmc.id;
hcoParam.Destination_Rule__c = mmc.id;

insert hcoParam;

Account_Compute_Final__c compFinal = TestDataFactory.createComputeFinal(mmc,acc,posAccount,bu,pos);
compFinal.Output_Name_1__c ='MARKET_ CRC';
insert compFinal;


insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'UpdatePositionCode')};
Test.startTest();
System.runAs(loggedInUser){
ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
Veeva_Jobs obj=new Veeva_Jobs();
obj.selectedJob='GAS Assignment';
obj.Full_Job();
Veeva_Jobs obj1=new Veeva_Jobs();
obj1.selectedJob='Product Metrics';
obj1.Full_Job();
Veeva_Jobs obj2=new Veeva_Jobs();
obj2.selectedJob='TSF';
obj2.Full_Job();
Veeva_Jobs obj3=new Veeva_Jobs();
obj3.selectedJob='Veeva Hierarchy';
obj3.Full_Job();
Veeva_Jobs obj4=new Veeva_Jobs();
obj4.selectedJob='Call Plan';
obj4.Full_Job();
Veeva_Jobs obj5=new Veeva_Jobs();
obj5.selectedJob='MySetUp Product';
obj5.Full_Job();
Veeva_Jobs obj6=new Veeva_Jobs();
obj6.selectedJob='User Role Manager Job';
obj6.Full_Job();
obj.Delta_Job();
}
Test.stopTest();
}
static testMethod void testMethod11() {
User loggedInUser = new User(id=UserInfo.getUserId());


AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
insert orgmas;
AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
insert countr;

AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
insert team;
AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
insert teamins1;

AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
workspace.AxtriaSalesIQTM__Country__c = countr.id;
insert workspace;


AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
insert scen;
AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current' ;
teamins.AxtriaSalesIQTM__Scenario__c = scen.Id;
insert teamins;
Account acc= TestDataFactory.createAccount();
acc.AccountNumber ='BH10643156';
insert acc;

AxtriaSalesIQTM__Affiliation_Network__c affnet = TestDataFactory.createAffliNet(countr);
insert affnet;
AxtriaSalesIQTM__Account_Affiliation__c accaff = TestDataFactory.createAcctAffli(acc,affnet);
insert accaff;

Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
insert pcc;

Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
mmc.Team_Instance__c = teamins.id;
insert mmc;
Measure_Master__c mmc1 = TestDataFactory.createMeasureMaster(pcc, team, teamins);
mmc1.Team_Instance__c = teamins.id;
insert mmc1;
Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
pp.Name ='MARKET_ CRC';
insert pp;
Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
rp.Parameter__c = pp.id;
insert rp;

AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
insert pos;
AxtriaSalesIQTM__User_Access_Permission__c u = TestDataFactory.createUserAccessPerm(pos, teamins, UserInfo.getUserId());
u.AxtriaSalesIQTM__Position__c=pos.id;
u.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
u.AxtriaSalesIQTM__User__c = loggedInUser.id;        
insert u;

AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
insert posAccount;

BU_Response__c bu = TestDataFactory.createBuResponse(posAccount,pcc,teamins,team,acc);
insert bu;
HCO_Parameters__c hcoParam = TestDataFactory.createHCOParameter(rp,mmc);
hcoParam.Source_Rule__c = mmc.id;
hcoParam.Destination_Rule__c = mmc.id;

insert hcoParam;

Account_Compute_Final__c compFinal = TestDataFactory.createComputeFinal(mmc,acc,posAccount,bu,pos);
compFinal.Output_Name_1__c ='MARKET_ CRC';
insert compFinal;


insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'UpdatePositionCode')};
Test.startTest();
System.runAs(loggedInUser){
ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
Veeva_Jobs obj=new Veeva_Jobs();
obj.selectedJob='GAS Assignment';
obj.Full_Job();
Veeva_Jobs obj1=new Veeva_Jobs();
obj1.selectedJob='Product Metrics';
obj1.Full_Job();
Veeva_Jobs obj2=new Veeva_Jobs();
obj2.selectedJob='TSF';
obj2.Full_Job();
Veeva_Jobs obj3=new Veeva_Jobs();
obj3.selectedJob='Veeva Hierarchy';
obj3.Full_Job();
Veeva_Jobs obj4=new Veeva_Jobs();
obj4.selectedJob='Call Plan';
obj4.Full_Job();
Veeva_Jobs obj5=new Veeva_Jobs();
obj5.selectedJob='MySetUp Product';
obj5.Full_Job();
Veeva_Jobs obj6=new Veeva_Jobs();
obj6.selectedJob='User Role Manager Job';
obj6.Full_Job();
obj.Delta_Job();
}
Test.stopTest();
}*/
static testMethod void testMethod2() {
    User loggedInUser = new User(id=UserInfo.getUserId());
    
    
    AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    insert orgmas;
    AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
    insert countr;
    
    AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
    insert team;
    AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
    insert teamins1;
    
    AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
    workspace.AxtriaSalesIQTM__Country__c = countr.id;
    insert workspace;
    
    
    AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
    scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
    insert scen;
    AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
    teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current' ;
    teamins.AxtriaSalesIQTM__Scenario__c = scen.Id;
    insert teamins;
    SIQ_GAS_History__c s = new SIQ_GAS_History__c();
    s.SIQ_Added_Territory__c ='test';
    s.SIQ_Account__c ='test';
    s.Run_Count__c =1;
    s.Deployment_Status__c ='New';
    insert s; 
    
    insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
    new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'UpdatePositionCode')};
    Test.startTest();
    System.runAs(loggedInUser){
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        Veeva_Jobs obj=new Veeva_Jobs();
        obj.selectedJob='GAS Assignment';
        obj.Full_Job();
        obj.Delta_Job();
    }
    Test.stopTest();
}
static testMethod void testMethod22() {
    User loggedInUser = new User(id=UserInfo.getUserId());
    
    
    AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    insert orgmas;
    AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
    insert countr;
    
    AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
    insert team;
    AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
    insert teamins1;
    
    AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
    workspace.AxtriaSalesIQTM__Country__c = countr.id;
    insert workspace;
    
    
    AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
    scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
    insert scen;
    AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
    teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current' ;
    teamins.AxtriaSalesIQTM__Scenario__c = scen.Id;
    insert teamins;
    SIQ_GAS_History__c s = new SIQ_GAS_History__c();
    s.SIQ_Added_Territory__c ='test';
    s.SIQ_Account__c ='test';
    s.Run_Count__c =1;
    s.Deployment_Status__c ='New';
    insert s; 
    
    insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
    new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'UpdatePositionCode')};
    Test.startTest();
    System.runAs(loggedInUser){
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        Veeva_Jobs obj=new Veeva_Jobs();
        obj.selectedJob='Product Metrics';
        obj.Full_Job();
        obj.Delta_Job();
    }
    Test.stopTest();
}
static testMethod void testMethod3() {
    User loggedInUser = new User(id=UserInfo.getUserId());
    
    
    AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    insert orgmas;
    AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
    insert countr;
    
    AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
    insert team;
    AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
    insert teamins1;
    
    AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
    workspace.AxtriaSalesIQTM__Country__c = countr.id;
    insert workspace;
    
    
    AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
    scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
    insert scen;
    AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
    teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current' ;
    teamins.AxtriaSalesIQTM__Scenario__c = scen.Id;
    insert teamins;
    SIQ_GAS_History__c s = new SIQ_GAS_History__c();
    s.SIQ_Added_Territory__c ='test';
    s.SIQ_Account__c ='test';
    s.Run_Count__c =1;
    s.Deployment_Status__c ='New';
    insert s; 
    
    insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
    new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'UpdatePositionCode')};
    Test.startTest();
    System.runAs(loggedInUser){
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        Veeva_Jobs obj=new Veeva_Jobs();
        obj.selectedJob='MySetUp Product';
        obj.Full_Job();
        obj.Delta_Job();
    }
    Test.stopTest();
}
static testMethod void testMethod22222() {
    User loggedInUser = new User(id=UserInfo.getUserId());
    
    
    AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    insert orgmas;
    AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
    insert countr;
    
    AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
    insert team;
    AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
    insert teamins1;
    
    AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
    workspace.AxtriaSalesIQTM__Country__c = countr.id;
    insert workspace;
    
    
    AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
    scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
    insert scen;
    AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
    teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current' ;
    teamins.AxtriaSalesIQTM__Scenario__c = scen.Id;
    insert teamins;
    SIQ_GAS_History__c s = new SIQ_GAS_History__c();
    s.SIQ_Added_Territory__c ='test';
    s.SIQ_Account__c ='test';
    s.Run_Count__c =1;
    s.Deployment_Status__c ='New';
    insert s; 
    
    insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
    new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'UpdatePositionCode')};
    Test.startTest();
    System.runAs(loggedInUser){
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        Veeva_Jobs obj=new Veeva_Jobs();
        obj.selectedJob='User Role Manager Job';
        obj.Full_Job();
        obj.Delta_Job();
    }
    Test.stopTest();
}
static testMethod void testMethod2222() {
    User loggedInUser = new User(id=UserInfo.getUserId());
    
    
    AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    insert orgmas;
    AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
    insert countr;
    
    AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
    insert team;
    AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
    insert teamins1;
    
    AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
    workspace.AxtriaSalesIQTM__Country__c = countr.id;
    insert workspace;
    
    
    AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
    scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
    insert scen;
    AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
    teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current' ;
    teamins.AxtriaSalesIQTM__Scenario__c = scen.Id;
    insert teamins;
    SIQ_GAS_History__c s = new SIQ_GAS_History__c();
    s.SIQ_Added_Territory__c ='test';
    s.SIQ_Account__c ='test';
    s.Run_Count__c =1;
    s.Deployment_Status__c ='New';
    insert s; 
    
    insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
    new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'UpdatePositionCode')};
    Test.startTest();
    System.runAs(loggedInUser){
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        Veeva_Jobs obj=new Veeva_Jobs();
        obj.selectedJob='Veeva Hierarchy';
        obj.Full_Job();
        obj.Delta_Job();
    }
    Test.stopTest();
}
static testMethod void testMethod222() {
    User loggedInUser = new User(id=UserInfo.getUserId());
    
    
    AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    insert orgmas;
    AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
    insert countr;
    
    AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
    insert team;
    AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
    insert teamins1;
    
    AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
    workspace.AxtriaSalesIQTM__Country__c = countr.id;
    insert workspace;
    
    
    AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
    scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
    insert scen;
    AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
    teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current' ;
    teamins.AxtriaSalesIQTM__Scenario__c = scen.Id;
    insert teamins;
    SIQ_GAS_History__c s = new SIQ_GAS_History__c();
    s.SIQ_Added_Territory__c ='test';
    s.SIQ_Account__c ='test';
    s.Run_Count__c =1;
    s.Deployment_Status__c ='New';
    insert s; 
    
    insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
    new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'UpdatePositionCode')};
    Test.startTest();
    System.runAs(loggedInUser){
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        Veeva_Jobs obj=new Veeva_Jobs();
        obj.selectedJob='TSF';
        obj.Full_Job();
        obj.Delta_Job();
    }
    Test.stopTest();
}
}