public with sharing class GenerateCRReport {
    
    String buName;
    String lineName;
    Map<String, String> posLineToUserMap;
    public List<dontKnowWhyRequireThisReport> showList {get;set;}
    public String dataString {get;set;}
    public String userLocale {get;set;}
    public String fileDelimiter {get;set;}
    public string hierarchylevel {get;set;}
    public List<AxtriaSalesIQTM__Position__c> allPositionsList {get;set;}
    public GenerateCRReport()
    {
        userLocale = UserInfo.getLocale();
        system.debug('@@@@@@@@@qwqw' + userLocale);
      //List<Country__c> countryInfo = [select Delimiter__c,Numeric_Delimiter__c from Country__c where name =: userLocale];//Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c
      List<Country_wise_Delimiter__mdt> countryInfo = [select Delimiter__c,Numeric_Delimiter__c from Country_wise_Delimiter__mdt where DeveloperName =: userLocale];//Shivansh - A1450
      
      if(countryInfo.size()>0)
      {
          fileDelimiter = countryInfo[0].Delimiter__c;
      }
      else
      {
          fileDelimiter = ',';
      }
        buName = ApexPages.currentPage().getParameters().get('buSelectedName'); 
        //lineName = ApexPages.currentPage().getParameters().get('lineSelectedName');
        posLineToUserMap = new Map<String,String>();
        showList = new List<dontKnowWhyRequireThisReport>();
        allPositionsList = new list<AxtriaSalesIQTM__Position__c>();
        dataString = '';
        getAllPositions();
    }
    
    public void getAllPositions()
    {
        String selectedPositon;

        List<AxtriaSalesIQTM__User_Access_Permission__c> allUserAccessPerm = [select AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AXTRIASALESIQTM__HIERARCHY_LEVEL__C from AxtriaSalesIQTM__User_Access_Permission__c where AxtriaSalesIQTM__User__c = :UserInfo.getUserId() and AxtriaSalesIQTM__Team_Instance__r.Name = :buName];
        
        if(allUserAccessPerm.size()>0)
        {
            selectedPositon = allUserAccessPerm[0].AxtriaSalesIQTM__Position__c;
            hierarchylevel = allUserAccessPerm[0].AxtriaSalesIQTM__Position__r.AXTRIASALESIQTM__HIERARCHY_LEVEL__C;
        }
        
        //selectedPositon ='a0g0Y000009zYJe';
        system.debug('---Selected position is:'+selectedPositon);
        system.debug('=========hierarchylevel::'+hierarchylevel);
        if(hierarchylevel == '1' || hierarchylevel == '2' || hierarchylevel == '3' || hierarchylevel == '4'){
            allPositionsList = new list<AxtriaSalesIQTM__Position__c>();
            allPositionsList = [select id, Name,AxtriaSalesIQTM__Parent_Position__r.Employee1__r.name,AxtriaSalesIQTM__Parent_Position__r.Name, AxtriaSalesIQTM__Employee__r.Name, AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__Client_Position_Code__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_Instance__r.Name = :buName and (AxtriaSalesIQTM__Parent_Position__c =:selectedPositon or AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__c =:selectedPositon or AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__c =:selectedPositon) and (AxtriaSalesIQTM__Position_Type__c = 'Territory' or  AxtriaSalesIQTM__Hierarchy_Level__c ='1')]; //and Line__r.Name = :lineName 
        }
        else{
            allPositionsList = new list<AxtriaSalesIQTM__Position__c>();
            allPositionsList = [select id, Name,AxtriaSalesIQTM__Parent_Position__r.Employee1__r.name,AxtriaSalesIQTM__Parent_Position__r.Name, AxtriaSalesIQTM__Employee__r.Name, AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__Client_Position_Code__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_Instance__r.Name = :buName and  (AxtriaSalesIQTM__Position_Type__c = 'Territory' or  AxtriaSalesIQTM__Hierarchy_Level__c ='1')]; //and Line__r.Name = :lineName (AxtriaSalesIQTM__Parent_Position__c =:selectedPositon or AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__c =:selectedPositon or AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__c =:selectedPositon) and
        }
        List<String> allPositions = new List<String>();
        List<String> allPositionsPart1 = new List<String>();
        List<String> allPositionsPart2 = new List<String>();
        List<String> allPositionsPart3 = new List<String>();
        List<String> allPositionsPart4 = new List<String>();
        List<String> allPositionsPart5 = new List<String>();
        List<String> allPositionsPart6 = new List<String>();
        List<String> allPositionsPart7 = new List<String>();
        List<String> allPositionsPart8 = new List<String>();
        List<String> allPositionsPart9 = new List<String>();
        List<String> allPositionsPart10 = new List<String>();
                
        Integer counter = 0;
        
        for(AxtriaSalesIQTM__Position__c pos : allPositionsList)
        {
            counter ++ ;
            allPositions.add(pos.ID);
        }
        system.debug('+++++++++ Hey'+counter);
        system.debug('::All positions::'+allPositions);
        List<AxtriaSalesIQTM__User_Access_Permission__c> allUAP = [select id, AxtriaSalesIQTM__User__r.Name, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Team_Instance__r.Name from AxtriaSalesIQTM__User_Access_Permission__c where AxtriaSalesIQTM__Position__c in :allPositions and AxtriaSalesIQTM__Team_Instance__r.Name = :buName];
        system.debug('===============allUAP:::'+allUAP);
        for(AxtriaSalesIQTM__User_Access_Permission__c uap : allUAP)
        {
            String tempString = uap.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' + uap.AxtriaSalesIQTM__Team_Instance__r.Name ;
            
            posLineToUserMap.put(tempString, uap.AxtriaSalesIQTM__User__r.Name);
        }
        system.debug('=========posLineToUserMap::::'+posLineToUserMap);
        system.debug('========allPositions2222::::::'+allPositions);
        List<AxtriaSalesIQTM__Change_Request__c> allChangeRequests = [select id, AxtriaSalesIQTM__Status__c,AxtriaSalesIQTM__Destination_Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Team_Instance_ID__r.Name, AxtriaSalesIQTM__Submitted_By__c, AxtriaSalesIQTM__Submitted_On__c from AxtriaSalesIQTM__Change_Request__c where AxtriaSalesIQTM__Destination_Position__c in :allPositions and AxtriaSalesIQTM__Team_Instance_ID__r.Name = :buName  and (AxtriaSalesIQTM__Request_Type_Change__c ='Call Plan'  or AxtriaSalesIQTM__Request_Type_Change__c ='Call Plan Change' or AxtriaSalesIQTM__Request_Type_Change__c = 'Call_Plan_Change') order by Name];
        
        Map<String,AxtriaSalesIQTM__Change_Request__c> latestChangeRequest = new Map<String,AxtriaSalesIQTM__Change_Request__c>();
        Map<String,Integer> changeRequestCount = new Map<String,Integer>();
        
        for(AxtriaSalesIQTM__Change_Request__c changeRequest : allChangeRequests)
        {
            String tempString = changeRequest.AxtriaSalesIQTM__Destination_Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
            system.debug('=============:tempString::::'+tempString); 
            latestChangeRequest.put(tempString, changeRequest);
            
            Integer count = 1;
            if(changeRequestCount.containsKey(tempString))
            {
                count = Integer.valueof(changeRequestCount.get(tempString)) + 1;
            }
            changeRequestCount.put(tempString, count);
        }
        system.debug('+++++++++ changeRequestCount' + changeRequestCount);
        //Map<String, Integer> originalPosToCountMap = new Map<String, Integer>();
        
        /*List<AggregateResult> totalPosAcc = [select count(id) countRecs, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c cpCode from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c in :allPositions and AxtriaSalesIQTM__Team_Instance__r.Name = :buName and Line__r.Name = :lineName and AxtriaSalesIQTM__isAccountTarget__c = true group by AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c];
        
        for(AggregateResult agg : totalPosAcc)
        {
            originalPosToCountMap.put(String.Valueof(agg.get('cpCode')), Integer.valueOf(agg.get('countRecs')));
        }
        
        
        List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> totalPosAcc = [select id, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c in :allPositions and AxtriaSalesIQTM__Team_Instance__r.Name = :buName and Line__r.Name = :lineName and AxtriaSalesIQTM__isAccountTarget__c = true];
        
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : totalPosAcc)
        {
            Integer newValue = 0;
            if(originalPosToCountMap.containsKey(pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c))
            {
                newValue = originalPosToCountMap.get(pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
            }
            newValue ++;
            originalPosToCountMap.put(pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, newValue);
        }
        
        Map<String, Integer> changedPosToCountMap = new Map<String, Integer>();
        */
        system.debug('+++++++++++ HEY BU Name is '+ buName);
       // system.debug('+++++++++++ HEY LINE Name is '+ lineName);
        /*List<AggregateResult> totalPosAccChanges = [select count(id) countRecs, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c cpCode from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c in :allPositions and AxtriaSalesIQTM__Team_Instance__r.Name = :buName and Line__r.Name = :lineName and AxtriaSalesIQTM__Change_Status__c != 'No Change' group by AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c];
        
        for(AggregateResult agg : totalPosAccChanges)
        {
            changedPosToCountMap.put(String.Valueof(agg.get('cpCode')), Integer.valueOf(agg.get('countRecs')));
        }
        
        
        //AggregateResult countRecords = [select count(id) countrecs from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c in :allPositions and AxtriaSalesIQTM__Team_Instance__r.Name = :buName and Line__r.Name = :lineName and AxtriaSalesIQTM__Change_Status__c != 'No Change'];
        
        //system.debug('+++++++ CountRecords '+ String.valueof(countRecords.get('countrecs')));
        List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> allPosAccRecs = [select id, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c in :allPositions and AxtriaSalesIQTM__Team_Instance__r.Name = :buName and Line__r.Name = :lineName and AxtriaSalesIQTM__Change_Status__c != 'No Change'];        
        
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : allPosAccRecs)
        {
            Integer newValue = 0;
            if(changedPosToCountMap.containsKey(pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c))
            {
                newValue = changedPosToCountMap.get(pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
            }
            newValue ++;r
            changedPosToCountMap.put(pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, newValue);
        }
        */
        for(AxtriaSalesIQTM__Position__c pos : allPositionsList)
        {
            String cpCodeLine = pos.AxtriaSalesIQTM__Client_Position_Code__c + '_' + pos.AxtriaSalesIQTM__Team_Instance__r.Name ;
            String territoryID = pos.AxtriaSalesIQTM__Client_Position_Code__c;
            String parentEmp = pos.AxtriaSalesIQTM__Parent_Position__r.Employee1__r.name;
            String parentName = pos.AxtriaSalesIQTM__Parent_Position__r.Name;
            String repName = pos.AxtriaSalesIQTM__Employee__r.Name;
            
           /* if(posLineToUserMap.containsKey(cpCodeLine))
             repName = posLineToUserMap.get(cpCodeLine);
             */
             
             /*
            Integer totalNoOfHCP = 0;
            
            if(originalPosToCountMap.containsKey(territoryID))
            {
                totalNoOfHCP = originalPosToCountMap.get(territoryID);
            }
            
            Integer totalNoOfHCPchanged = 0;
            
            if(changedPosToCountMap.containsKey(territoryID))
            {
                totalNoOfHCPchanged = changedPosToCountMap.get(territoryID);
            }
            */
            Integer totalCRs = 0;
            system.debug('+++++++ changeRequestCount' + changeRequestCount);
            if(changeRequestCount.containsKey(territoryID))
            {
                totalCRs = changeRequestCount.get(territoryID);
            }
            
            String latestCRStatus = ' ';
            String submittedBy = ' ';
            DateTime submittedDate;
            if(latestChangeRequest.containsKey(territoryID))
            {
                AxtriaSalesIQTM__Change_Request__c cs = latestChangeRequest.get(territoryID);
                latestCRStatus = cs.AxtriaSalesIQTM__Status__c;
                submittedBy= cs.AxtriaSalesIQTM__Submitted_By__c;
                submittedDate = cs.AxtriaSalesIQTM__Submitted_On__c;
            }
            
            showList.add(new dontKnowWhyRequireThisReport(territoryID, parentName, repName, totalCRs, latestCRStatus, submittedBy, submittedDate,parentEmp ));
            if(showList != null && showList.size() > 0){
                system.debug('--showList---' + showList);
                dataString = JSON.serialize(showList);
            }else{
                dataString = '';
            }
            
        }
    }
    
    public class dontKnowWhyRequireThisReport
    {
        public String territoryID {get;set;}
        public String repName {get;set;}
        public String parentEmp {get;set;}
        public Integer totalNoOfHCP {get;set;}
        public Integer totalNoOfHCPchanged {get;set;}
        public Integer totalCRs {get;set;}
        public String latestCRStatus {get;set;}
        public String submittedBy {get;set;}
        public DateTime submittedDate {get;set;}
        public String parentName {get;set;}
        
        public dontKnowWhyRequireThisReport(String territoryID, String parentName ,String repName,Integer totalCRs,String latestCRStatus,String submittedBy,DateTime submittedDate,string parentEmp)
        {
            this.territoryID = territoryID;
            this.parentName = parentName;
            this.repName = repName;
            this.parentEmp = parentEmp;
            this.totalCRs = totalCRs;
            this.latestCRStatus = latestCRStatus;
            this.submittedBy = submittedBy;
            this.submittedDate = submittedDate;
            
        }
    }
}