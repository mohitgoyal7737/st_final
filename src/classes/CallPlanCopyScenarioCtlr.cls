/**********************************************************************************************
@author       : Himanshu Tariyal (A0994)
@date         : 4th June'2020
@description  : Class for selecting Base and Destination Team Instances and calling the 
			    CopyCallPlanScenarioTriggerHandler functions
Revision(s)   : v1.0
**********************************************************************************************/
global with sharing class CallPlanCopyScenarioCtlr 
{
    public List<SelectOption> workspaceList {get;set;}
    public List<SelectOption> teamList {get;set;}
    public List<SelectOption> sourceTeamInstanceList {get;set;}
    public List<SelectOption> destTeamInstanceList {get;set;}
    public List<LoggerClass> listCallPlanCopyLogger {get;set;}

    public String alignNmsp;
    public String selectedWorkspace {get;set;}
    public String selectedTeam {get;set;}
    public String selectedSourceTeamInst {get;set;}
    public String selectedDestTeamInst {get;set;}
    public String className;
    public String loggerObjectName;
    public String securityQuery = 'WITH SECURITY_ENFORCED';

    public Boolean skipCallPlanCopy {get;set;}
    public Boolean noData {get;set;}
    public Boolean showTeamsList {get;set;}
    public Boolean showTeamInstList {get;set;}
    public Boolean showCopyButton {get;set;}

    public CallPlanCopyScenarioCtlr()
    {
        try
        {
            className = 'CallPlanCopyScenarioCtlr';
            SnTDMLSecurityUtil.printDebugMessage(className+' : constructor invoked');

            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ScenarioAlignmentCtlr'];
            alignNmsp = cs.NamespacePrefix!=null ? cs.NamespacePrefix+'__' : '';
            SnTDMLSecurityUtil.printDebugMessage('alignNmsp--'+alignNmsp);

            loggerObjectName = alignNmsp+'SalesIQ_Logger__c';
            SnTDMLSecurityUtil.printDebugMessage('loggerObjectName--'+loggerObjectName);

            listCallPlanCopyLogger = new List<LoggerClass>();

            skipCallPlanCopy = false;
            noData = true;
            showTeamsList = false;
            showTeamInstList = false;
            showCopyButton = false;

            selectedWorkspace = '';
            selectedTeam = '';
            selectedSourceTeamInst = '';
            selectedDestTeamInst = '';

            workspaceList = new List<SelectOption>();
            workspaceList.add(new SelectOption('','--NONE--'));

            String workspaceQuery = 'select '+alignNmsp+'Workspace__c,'+alignNmsp+'Workspace__r.Name from '+alignNmsp+'Scenario__c '+
                                    securityQuery+' group by '+alignNmsp+'Workspace__c,'+alignNmsp+'Workspace__r.Name';
            SnTDMLSecurityUtil.printDebugMessage('workspaceQuery--'+workspaceQuery);                        
            List<sObject> workspaceDataList = Database.query(workspaceQuery);
            SnTDMLSecurityUtil.printDebugMessage('workspaceDataList size--'+workspaceDataList.size());

            if(workspaceDataList!=null && workspaceDataList.size()>0)
            {
                String workspaceName;
                String workspaceID;

                for(sObject work : workspaceDataList)
                {
                    workspaceID = work.get(alignNmsp+'Workspace__c')!=null ? (String)work.get(alignNmsp+'Workspace__c') : '';
                    workspaceName = work.get(alignNmsp+'Workspace__c')!=null ? (String)work.get('Name') : ''; 

                    if(workspaceID!='' && workspaceName!=''){
                        workspaceList.add(new SelectOption(workspaceID,workspaceName));  
                    }
                }
                SnTDMLSecurityUtil.printDebugMessage('workspaceList size--'+workspaceList.size());

                if(workspaceList!=null && workspaceList.size()>0){
                    noData = false;
                }
            }
            fetchPreviousCopyCPLogs();
            SnTDMLSecurityUtil.printDebugMessage('noData--'+noData);
        }
        catch(Exception e)
        {
            SnTDMLSecurityUtil.printDebugMessage(className+' : Error in constructor--'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage(className+' : Stack Trace--'+e.getStackTraceString());
        }
    }

    public void fetchPreviousCopyCPLogs()
    {
        try
        {
            SnTDMLSecurityUtil.printDebugMessage(className+' : fetchPreviousCopyCPLogs() invoked--');
            listCallPlanCopyLogger = new List<LoggerClass>();

            String logQuery = 'select CreatedDate,Completed_Date__c,Source_Team_Instance__c,Source_Team_Instance__r.Name,'+
                                'Object_Name__c,Team__c,Team__r.Name,Destination_Team_Instance__c,Workspace__c,Skip_Call_Plan__c,'+
                                'Destination_Team_Instance__r.Name,Workspace__r.Name,'+alignNmsp+'Status__c,'+alignNmsp+
                                'Message__c from '+loggerObjectName+' where Object_Name__c = \'Copy Call Plan Scenario\' '+
                                securityQuery+' order by CreatedDate desc limit 500';
            SnTDMLSecurityUtil.printDebugMessage('logQuery--'+logQuery);
            List<sObject> logDataList = Database.query(logQuery);
            SnTDMLSecurityUtil.printDebugMessage('logDataList size--'+logDataList.size());
            
            if(logDataList!=null && logDataList.size()>0)
            {
                String createdDate;
                String completedDate;
                String wsName;
                String wsID;
                String teamID;
                String teamName;
                String sourceTeamInstName;
                String sourceTeamInstID;
                String destTeamInstName;
                String destTeamInstID;
                String status;
                String message;
                Boolean skipCP;

                for(sObject rec : logDataList)
                {
                    createdDate = String.valueOf((Datetime)rec.get('CreatedDate'));
                    completedDate = String.valueOf((Datetime)rec.get('Completed_Date__c'));

                    wsID = rec.get('Workspace__c')!=null ? (String)rec.get('Workspace__c') : '';
                    wsName = rec.get('Workspace__c')!=null ? (String)rec.getSObject('Workspace__r').get('Name') : '';

                    teamID = rec.get('Team__c')!=null ? (String)rec.get('Team__c') : '';
                    teamName = rec.get('Team__c')!=null ? (String)rec.getSObject('Team__r').get('Name') : '';

                    sourceTeamInstID = rec.get('Source_Team_Instance__c')!=null ? (String)rec.get('Source_Team_Instance__c') : '';
                    sourceTeamInstName = rec.get('Source_Team_Instance__c')!=null ? (String)rec.getSObject('Source_Team_Instance__r').get('Name') : '';

                    destTeamInstID = rec.get('Destination_Team_Instance__c')!=null ? (String)rec.get('Destination_Team_Instance__c') : '';
                    destTeamInstName = rec.get('Destination_Team_Instance__c')!=null ? (String)rec.getSObject('Destination_Team_Instance__r').get('Name') : '';

                    status = rec.get(alignNmsp+'Status__c')!=null ? (String)rec.get(alignNmsp+'Status__c') : '';
                    message = rec.get(alignNmsp+'Message__c')!=null ? (String)rec.get(alignNmsp+'Message__c') : '';
                    skipCP = rec.get('Skip_Call_Plan__c')!=null ? (Boolean)rec.get('Skip_Call_Plan__c') : false;

                    listCallPlanCopyLogger.add(new LoggerClass(createdDate,completedDate,wsName,wsID,teamName,teamID,sourceTeamInstName,
                                                sourceTeamInstID,destTeamInstName,destTeamInstID,status,message,skipCP));
                }

                SnTDMLSecurityUtil.printDebugMessage('listCallPlanCopyLogger size--'+listCallPlanCopyLogger.size());
            }   
        }
        catch(Exception e)
        {
            SnTDMLSecurityUtil.printDebugMessage(className+' : Error in fetchPreviousCopyCPLogs()--'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage(className+' : Stack Trace--'+e.getStackTraceString());
        }
    }

    public void getTeamSelectList()
    {
        try
        {
            SnTDMLSecurityUtil.printDebugMessage(className+' : getTeamSelectList() invoked--');
            SnTDMLSecurityUtil.printDebugMessage('selectedWorkspace--'+selectedWorkspace);

            teamList = new List<SelectOption>();
            teamList.add(new SelectOption('','--NONE--'));

            selectedTeam = '';

            showTeamsList = false;
            showCopyButton = false;
            showTeamInstList = false;

            String teamQuery = 'select '+alignNmsp+'Team_Name__c,'+alignNmsp+'Team_Name__r.Name from '+alignNmsp+'Scenario__c where '+
                                alignNmsp+'Workspace__c = :selectedWorkspace '+securityQuery+' group by '+alignNmsp+'Team_Name__c,'+
                                alignNmsp+'Team_Name__r.Name order by '+alignNmsp+'Team_Name__r.Name';
            SnTDMLSecurityUtil.printDebugMessage('teamQuery--'+teamQuery);                        
            List<sObject> teamDataList = Database.query(teamQuery);
            SnTDMLSecurityUtil.printDebugMessage('teamDataList size--'+teamDataList.size());

            if(teamDataList!=null && teamDataList.size()>0)
            {
                showTeamsList = true;

                String teamName;
                String teamID;

                for(sObject team : teamDataList)
                {
                    teamID = team.get(alignNmsp+'Team_Name__c')!=null ? (String)team.get(alignNmsp+'Team_Name__c') : '';
                    teamName = team.get(alignNmsp+'Team_Name__c')!=null ? (String)team.get('Name') : ''; 

                    if(teamID!='' && teamName!=''){
                        teamList.add(new SelectOption(teamID,teamName));  
                    }
                }
                SnTDMLSecurityUtil.printDebugMessage('teamList size--'+teamList.size());
            }
        }
        catch(Exception e)
        {
            SnTDMLSecurityUtil.printDebugMessage(className+' : Error in getTeamSelectList()--'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage(className+' : Stack Trace--'+e.getStackTraceString());
        }
    }

    public void getTeamInstList()
    {
        try
        {
            SnTDMLSecurityUtil.printDebugMessage(className+' : getTeamInstList() invoked--');
            SnTDMLSecurityUtil.printDebugMessage('selectedTeam--'+selectedTeam);

            sourceTeamInstanceList = new List<SelectOption>();
            destTeamInstanceList = new List<SelectOption>();

            showCopyButton = false;
            showTeamInstList = false;

            String teamInstQuery = 'select '+alignNmsp+'Team_Instance__c,'+alignNmsp+'Team_Instance__r.Name from '+alignNmsp+'Scenario__c where '+
                                    alignNmsp+'Team_Name__c = :selectedTeam '+securityQuery+' order by '+alignNmsp+'Team_Instance__r.Name';
            SnTDMLSecurityUtil.printDebugMessage('teamInstQuery--'+teamInstQuery);                        
            List<sObject> teamInstDataList = Database.query(teamInstQuery);
            SnTDMLSecurityUtil.printDebugMessage('teamInstDataList size--'+teamInstDataList.size());

            if(teamInstDataList!=null && teamInstDataList.size()>0)
            {
                String teamInstID;
                String teamInstName;

                for(sObject teamInst : teamInstDataList)
                {
                    teamInstID = teamInst.get(alignNmsp+'Team_Instance__c')!=null ? (String)teamInst.get(alignNmsp+'Team_Instance__c') : '';
                    teamInstName = teamInst.get(alignNmsp+'Team_Instance__c')!=null ? (String)teamInst.getSObject(alignNmsp+'Team_Instance__r').get('Name') : ''; 

                    if(teamInstID!='' && teamInstName!=''){
                        sourceTeamInstanceList.add(new SelectOption(teamInstID,teamInstName));
                        destTeamInstanceList.add(new SelectOption(teamInstID,teamInstName));  
                    }

                    if(!sourceTeamInstanceList.isEmpty())
                    {
                        showCopyButton = true;
                        showTeamInstList = true;

                        selectedSourceTeamInst = sourceTeamInstanceList[0].getValue();
                        selectedDestTeamInst = sourceTeamInstanceList[sourceTeamInstanceList.size()-1].getValue();
                    }
                }
                SnTDMLSecurityUtil.printDebugMessage('sourceTeamInstanceList size--'+sourceTeamInstanceList.size());
                SnTDMLSecurityUtil.printDebugMessage('destTeamInstanceList size--'+destTeamInstanceList.size());
            }
        }
        catch(Exception e)
        {
            SnTDMLSecurityUtil.printDebugMessage(className+' : Error in getTeamInstList()--'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage(className+' : Stack Trace--'+e.getStackTraceString());
        }
    }
    
    public void callCopyCPFunction()
    {
        Savepoint sp = Database.setSavepoint();
        try
        {
        	SnTDMLSecurityUtil.printDebugMessage(className+' : callCopyCPFunction() invoked');
        	SnTDMLSecurityUtil.printDebugMessage('selectedSourceTeamInst--'+selectedSourceTeamInst);
        	SnTDMLSecurityUtil.printDebugMessage('selectedDestTeamInst--'+selectedDestTeamInst);

        	if(selectedSourceTeamInst!=null && selectedDestTeamInst!=null)
        	{
                //create Copy Call Scenario record
                sObject copyCallLog = Schema.getGlobalDescribe().get(loggerObjectName).newSObject();
                copyCallLog.put('Workspace__c',selectedWorkspace);
                copyCallLog.put('Team__c',selectedTeam);
                copyCallLog.put('Source_Team_Instance__c',selectedSourceTeamInst);
                copyCallLog.put('Destination_Team_Instance__c',selectedDestTeamInst);
                copyCallLog.put(alignNmsp+'Status__c','In Progress');
                copyCallLog.put('Object_Name__c','Copy Call Plan Scenario');
                copyCallLog.put('Skip_Call_Plan__c',skipCallPlanCopy);
                SnTDMLSecurityUtil.insertRecords(copyCallLog,className);

                String loggerID = (String)copyCallLog.get('Id');
                SnTDMLSecurityUtil.printDebugMessage('loggerID--'+loggerID);

                //Call batch
                /*CopyCallPlanScenarioTriggerHandler classCall = new CopyCallPlanScenarioTriggerHandler(selectedSourceTeamInst,selectedDestTeamInst,loggerID);
                classCall.updateConfigsOnTeamInst();*/

                if(!System.Test.isRunningTest())
                {
                    //Call batch
                    CopyCallPlanScenarioTriggerHandler classCall = new CopyCallPlanScenarioTriggerHandler(selectedSourceTeamInst,selectedDestTeamInst,loggerID);
                    classCall.updateConfigsOnTeamInst();
                }
    
                //Fetch previous logs
                fetchPreviousCopyCPLogs();
        	}
        }
        catch(Exception e)
        {
            Database.rollback(sp);
            SnTDMLSecurityUtil.printDebugMessage(className+' : Error in callCopyCPFunction()--'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage(className+' : Stack Trace--'+e.getStackTraceString());
        }
    }

    public class LoggerClass
    {
        public String createdDateString {get;set;}
        public String completedDateString {get;set;}
        public String workspaceName {get;set;}
        public String workspaceId {get;set;}
        public String teamName {get;set;}
        public String teamID {get;set;}
        public String sourceTeamInstName {get;set;}
        public String sourceTeamInstID {get;set;}
        public String destTeamInstName {get;set;}
        public String destTeamInstID {get;set;}
        public String status {get;set;}
        public String message {get;set;}
        public Boolean skipCallPlan {get;set;}

        public LoggerClass(String createdDateString,String completedDateString,String workspaceName,String workspaceId,
                            String teamName,String teamID,String sourceTeamInstName,String sourceTeamInstID,
                            String destTeamInstName,String destTeamInstID,String status,String message,Boolean skipCP)
        {
            this.createdDateString = createdDateString;
            this.completedDateString = completedDateString;
            this.workspaceName = workspaceName;
            this.workspaceId = workspaceId;
            this.teamName = teamName;
            this.teamID = teamID;
            this.sourceTeamInstName = sourceTeamInstName;
            this.sourceTeamInstID = sourceTeamInstID;
            this.destTeamInstName = destTeamInstName;
            this.destTeamInstID = destTeamInstID;
            this.status = status;
            this.message = message;
            this.skipCallPlan = skipCP;
        }
    }
}