/*
@author - Prince Richard Augustin (A2661)
@description - Test class to test BatchEmptyDataInMappedFieldsInPosAcc.
*/
@isTest
private class BatchEmptyDataInMappedFieldsInPosAccTest {
    @istest static  void testMethod1() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        Account acc= TestDataFactory.createAccount();
        insert acc;
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        
        AxtriaSalesIQTM__Organization_Master__c org =TestDataFactory.createOrganizationMaster();
        insert org;
        
        AxtriaSalesIQTM__Country__c country = TestDataFactory.createCountry(org);
        insert country;
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, country);
        insert pcc;
        
        Measure_Master__c rule = TestDataFactory.createMeasureMaster(pcc,team,teamins);
        insert rule;
        
        AxtriaSalesIQTM__Position__c position = TestDataFactory.createPosition(team,teamins);
        insert position;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,position,teamins);
        insert posAccount;
        
        BU_Response__c bcc = TestDataFactory.createBuResponse(posAccount,pcc,teamins,team,acc);
        insert bcc;
        
        Account_Compute_Final__c compFinal = TestDataFactory.createComputeFinal(rule, acc,posAccount,bcc,position);
        insert compFinal;
        
        Source_to_Destination_Mapping__c mapping = TestDataFactory.createACF_PA_mapping(teamins,pcc);
        insert mapping;
        
        mapping = TestDataFactory.createACF_PA_mapping(teamins,pcc);
        mapping.Source_Object_Field__c = 'Final_Segment__c';
        mapping.Destination_Object_Field__c = 'AxtriaSalesIQTM__Segment_1__c';
        insert mapping;
        Test.startTest();
        System.runAs(loggedInUser){
            List<String> FIELD_LIST = new List<String>{'AxtriaSalesIQTM__Account_Alignment_Type__c','AxtriaSalesIQTM__Account_Target_Type__c'};
                System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(AxtriaSalesIQTM__Position_Account__c.SObjectType, FIELD_LIST, false));
            Database.executeBatch(new BatchEmptyDataInMappedFieldsInPosAcc(teamins.id,rule.id,pcc.id));
        }
        Test.stopTest();
    }
}