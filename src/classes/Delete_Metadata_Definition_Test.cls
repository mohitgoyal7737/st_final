/*
@author - Prince Richard Augustin (A2661)
@description - Test class to test the Delete_Metadata_Definition.
*/

@isTest
private class Delete_Metadata_Definition_Test {
    static testMethod void testMethod1() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        Account acc = TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Team__c = team.id;
        insert teamins ;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        AxtriaSalesIQTM__Product__c product = TestDataFactory.createProduct(team, teamins);
        insert product;
        AxtriaSalesIQTM__Team_Instance_Product__c tip = TestDataFactory.teamInstanceProduct(team, teamins, product);
        insert tip;
        MetaData_Definition__c meta = TestDataFactory.createMetaDataDefinition(teamins, pcc, team);
        meta.Team_Instance__c =teamins.id;
        insert meta;
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            Delete_Metadata_Definition obj=new Delete_Metadata_Definition();
            obj.query = 'select id from MetaData_Definition__c ';
            Database.executeBatch(obj);
        }
        Test.stopTest();
    }  
   /* static testMethod void testMethod2() {
     User loggedInUser = new User(id=UserInfo.getUserId());
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        
        Account acc = TestDataFactory.createAccount();
        acc.Profile_Consent__c='yes';
        acc.AxtriaSalesIQTM__Speciality__c ='testspecs';
        
        insert acc;
        
        Account acc2 = TestDataFactory.createAccount();
        acc2.Profile_Consent__c='yes';
        acc2.AxtriaSalesIQTM__Speciality__c ='testspecs';
        acc2.name = 'testacc2';
        insert acc2;
              
        AxtriaSalesIQTM__Team_Instance__c teamInstance = TestDataFactory.createTeamInstance(team);
        teamInstance.AxtriaSalesIQTM__Team__c = team.id;
         teamInstance.Name = 'testteamins';
          teamInstance.Segmentation_Universe__c = 'Full S&T Input Customers';
        insert teamInstance ;
        

        Product_Catalog__c pcc = TestDataFactory.productCatalog(team,teamInstance,countr);
        pcc.Country_Lookup__c = countr.id;
        pcc.Veeva_External_ID__c = 'test';
        insert pcc;
        Product_Catalog__c pcc1 = TestDataFactory.productCatalog(team,teamInstance,countr);
        pcc1.Country_Lookup__c = countr.id;
        pcc1.Veeva_External_ID__c = 'test';
        insert pcc1;
        Measure_Master__c measureMaster= TestDataFactory.createMeasureMaster(pcc,team,teamInstance);
        measureMaster.Team__c = team.id;
        measuremaster.Brand_Lookup__c = pcc.Id;
        measureMaster.Team_Instance__c = teamInstance.id;
        insert measureMaster;
        
        insert  new MetaData_Definition__c (Display_Name__c='ACCESSIBILITY',Source_Field__c='Accessibility_Range__c',Source_Object__c='BU_Response__c',Team_Instance__c=teamInstance.id,Product_Catalog__c=pcc.Id);
 
        AxtriaSalesIQTM__Position__c pos = TestDataFactory.createPosition(team,teamInstance);
        insert pos;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamInstance);
        insert posAccount;
        
        Product_Priority__c pp = new Product_Priority__c();
        pp.CurrencyIsoCode = 'USD';
        pp.Product__c = pcc.id;
        pp.Speciality_ID__c='testSpecs';
        pp.priority__c='P2';
        insert pp;
         BU_Response__c bu = TestDataFactory.createBuResponse(posAccount,pcc,teamInstance,team,acc);
         bu.Team_Instance__c = teamInstance.id;
         bu.Product__c = pcc.id;
        //bu.Brand_c = pcc.id;
         
        MetaData_Definition__c meta = TestDataFactory.createMetaDataDefinition(teamInstance, pcc, team);
        meta.Team_Instance__c =teamInstance.id;
        insert meta;
        Test.startTest();
        System.runAs(loggedInUser){
ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            Delete_Metadata_Definition obj=new Delete_Metadata_Definition(teamInstance.Name+';'+pcc.Veeva_External_ID__c);
            obj.query = 'select id from MetaData_Definition__c ';
            Database.executeBatch(obj);
        }
        Test.stopTest();
    }  */
    static testMethod void testMethod3() {
       User loggedInUser = new User(id=UserInfo.getUserId());
       AxtriaSalesIQTM__Organization_Master__c aom = new AxtriaSalesIQTM__Organization_Master__c(AxtriaSalesIQTM__Org_Level__c='Global',AxtriaSalesIQTM__Parent_Country_Level__c=true);
       insert aom;
       
       AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='USA',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active');
       insert country;
       
        /*Global_Question_ID_AZ__c gq = new Global_Question_ID_AZ__c(Name='1');
        insert gq;*/
        
        /*Cycle__c cycle = new Cycle__c(Name='Test Cycle',Country__c = country.id);
        insert cycle;*/
        
        Account acc = new Account(Name='test acc',Marketing_Code__c='EU',AccountNumber='123456');
        insert acc;
        
        /*Business_Unit__c bu = new Business_Unit__c(Name='Test BU Loopup');
        insert bu;*/
        
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='Test Team'/*,Business_Unit_Loopup__c=bu.id*/);
        team.AxtriaSalesIQTM__Country__c = country.id;
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU',AxtriaSalesIQTM__Team__c=team.id/*,Cycle__c=cycle.id*/);
        insert ti;

        Product_Catalog__c pc = new Product_Catalog__c(Name='Test Product',Team_Instance__c=ti.id,Veeva_External_ID__c = 'ProdId',Product_Code__c= 'ProdId',IsActive__c=true,Country_Lookup__c=country.id);
        insert pc;
        
        /*Brand_Team_Instance__c bti = new Brand_Team_Instance__c(Brand__c=pc.id,Team_Instance__c=ti.id);
        insert bti;*/
        
        /*Prepare Staging_Survey_Data__c data*/
        Staging_Survey_Data__c ssd = new Staging_Survey_Data__c();
        ssd.CurrencyIsoCode = 'USD';
        ssd.Sales_Cycle__c = '2018CICLO2';
        ssd.SURVEY_ID__c = '1';
        ssd.SURVEY_NAME__c = 'Methanol_pcp';
        ssd.Team_Instance__c = '';
        ssd.Team__c = 'RIA_CRESTOR';
        ssd.Account_Number__c = 'IT100501';
        ssd.Product_Code__c = '172_002000017000_IT';
        ssd.Product_Name__c = 'Methanol';
        ssd.Position_Code__c = '';
        ssd.Question_ID1__c = 1;
        ssd.Question_ID2__c = 2;
        ssd.Question_ID3__c = 3;
        ssd.Question_ID4__c = 4;
        ssd.Question_ID5__c = 5;
        ssd.Question_ID6__c = 6;
        ssd.Question_ID7__c = 7;
        ssd.Question_ID8__c = 8;
        ssd.Question_ID9__c = 9;
        ssd.Question_ID10__c = 10;
        ssd.Response1__c = '1';
        ssd.Response2__c = '2';
        ssd.Response3__c = '3';
        ssd.Response4__c = '4';
        ssd.Response5__c = '5';
        ssd.Response6__c = '6';
        ssd.Response7__c = '7';
        ssd.Response8__c = '8';
        ssd.Response9__c = '9';
        ssd.Response10__c = '10';
        ssd.Short_Question_Text1__c = 'a';
        ssd.Short_Question_Text2__c = 'b';
        ssd.Short_Question_Text3__c = 'c';
        ssd.Short_Question_Text4__c = 'd';
        ssd.Short_Question_Text5__c = 'e';
        ssd.Short_Question_Text6__c = 'f';
        ssd.Short_Question_Text7__c = 'g';
        ssd.Short_Question_Text8__c = 'h';
        ssd.Short_Question_Text9__c = 'i';
        ssd.Short_Question_Text10__c = 'j';
        insert ssd;
        
        /*Prepare Staging_Cust_Survey_Profiling__c data*/
        Staging_Cust_Survey_Profiling__c scsp = new Staging_Cust_Survey_Profiling__c();
        scsp.CurrencyIsoCode = 'USD';
        scsp.SURVEY_ID__c = '1';
        scsp.SURVEY_NAME__c = 'Methanol_pcp';
        scsp.BRAND_ID__c = 'ProdId';
        scsp.BRAND_NAME__c = 'Test Product';
        scsp.PARTY_ID__c = '123456';
        scsp.QUESTION_SHORT_TEXT__c = 'Question data';
        scsp.RESPONSE__c = '4';
        scsp.Team_Instance__c = ti.Name;
        scsp.QUESTION_ID__c = 1;
        insert scsp;
        
        Staging_Cust_Survey_Profiling__c scsp2 = new Staging_Cust_Survey_Profiling__c();
        scsp2.CurrencyIsoCode = 'USD';
        scsp2.SURVEY_ID__c = '2';
        scsp2.SURVEY_NAME__c = 'Methanol_pcp';
        scsp2.BRAND_ID__c = 'ProdId';
        scsp2.BRAND_NAME__c = 'Test Product';
        scsp2.PARTY_ID__c = '123456';
        scsp2.QUESTION_SHORT_TEXT__c = 'Question data 2';
        scsp2.RESPONSE__c = '5';
        scsp2.Team_Instance__c = ti.Name;
        scsp2.QUESTION_ID__c = 2;
        insert scsp2;
        
        /*Prepare Survey_Response__c data*/

        //commented due to object purge activity A1450
        // Survey_Response__c sr = new Survey_Response__c();
        // sr.Name = 'Test SR';
        // sr.Team_Instance__c = ti.id;
        // insert sr;
        
        /*Prepare Survey_Response__c data*/
        Parameter__c param = new Parameter__c();
        param.Name = 'Test param';
        param.Team_Instance__c = ti.id;
        insert param; 
        
        /*Prepare MetaData_Definition__c data*/
        MetaData_Definition__c md = new MetaData_Definition__c();
        md.Team_Instance__c = ti.id;
        insert md;
        
        /*Prepare Survey_Definition__c data*/
       /* Survey_Definition__c sd = new Survey_Definition__c();
        sd.Team_Instance__c = ti.id;
        insert sd; */
        
        /*Prepare Staging_BU_Response__c data*/
        Staging_BU_Response__c sbur = new Staging_BU_Response__c();
        sbur.Team_Instance__c = ti.id;
        insert sbur; 
        
        /*Prepare BU_Response__c data*/
        BU_Response__c bur = new BU_Response__c();
        bur.Team_Instance__c = ti.id;
        insert bur;
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            Delete_Metadata_Definition obj=new Delete_Metadata_Definition(ti.Name,'File');
            obj.query = 'select id from MetaData_Definition__c ';
            Database.executeBatch(obj);
        }
        Test.stopTest();
    } 
}