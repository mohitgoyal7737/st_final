global class BatchOutBoundPositionProduct implements Database.Batchable<sObject> {
    public String query;
    global BatchOutBoundPositionProduct() {
        
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<sobject> scope) {

        
    }

    global void finish(Database.BatchableContext BC) {

    }
}