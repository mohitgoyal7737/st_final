public with sharing class CIMPositionMatrixSummaryUpdateCtlr {


    public List<AxtriaSalesIQTM__Workspace__c> workspace  {get;set;}
    public List<AxtriaSalesIQTM__Team_Instance__c> teamins  {get;set;}
    public String countryID {get;set;}
    public Map<string,string> successErrorMap;
    public AxtriaSalesIQTM__Country__c Country;
    public String selectedCountry {get;set;}
    public string selectedcycle {get;set;}
    public string cycleSelected{get; set;}
    public string selectedTeamInstance                       {get;set;}
    Public String TeamInstancename {get;set;}
    public string teaminstanceSelected{get; set;}

    public list<AxtriaSalesIQTM__CIM_Config__c> listCIMconfig{get; set;}
    public List<String> cimConfigId{get; set;}
    public list<SelectOption> allcycles{get; set;}
    public list<SelectOption> allTeamInstance{get;set;}
    public list<SelectOption> cimConfigOptions{get; set;}
    public string theQuery{get; set;}
    public string aggregationAttributeAPIName{get; set;}
    public list<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c> updateListPostionMetric{get; set;}
    public integer countRecords {get;set;}
    public boolean isConfigRecordAlreadyPresent{get;set;}
    public boolean disableAnalyse {get;set;}

    public CIMPositionMatrixSummaryUpdateCtlr(){     
        theQuery = '';
        disableAnalyse = true;
        allTeamInstance = new List<SelectOption>();
        cimConfigOptions = new list<SelectOption>();
        allteaminstance.add(new SelectOption('', '--None--'));
        cimConfigOptions.add(new SelectOption('','--None--'));
        allcycles = new list<SelectOption>(); 
        cimConfigId = new List<String>();

       /* countryID = SalesIQUtility.getCookie('CountryID');
        SnTDMLSecurityUtil.printDebugMessage('##### countryID ' + countryID);
        successErrorMap = SalesIQUtility.checkCountryAccess(countryID);
        SnTDMLSecurityUtil.printDebugMessage('############ successErrorMap ' + successErrorMap);
        if(successErrorMap.containsKey('Success')){
           countryID = successErrorMap.get('Success');               
           SnTDMLSecurityUtil.printDebugMessage('########## countryID from Map ' + countryID);
           //As soon as we get Country ID set it in the cookie and it will applicable for whole application.
           SalesIQUtility.setCookieString('CountryID',countryID);
           Country = new AxtriaSalesIQTM__Country__c();
           Country = [select AxtriaSalesIQTM__Country_Flag__c,Name from AxtriaSalesIQTM__Country__c where id =:countryID WITH SECURITY_ENFORCED limit 1];
       }*/
       countryID = MCCP_Utility.getKeyValueFromPlatformCache('SIQCountryID');
    Country = new AxtriaSalesIQTM__Country__c();
    Country = [select AxtriaSalesIQTM__Country_Flag__c,Name from AxtriaSalesIQTM__Country__c where id =:countryID limit 1];
       workspace = [SELECT Id,Name FROM AxtriaSalesIQTM__Workspace__c WHERE AxtriaSalesIQTM__Country__c  = :countryID  WITH SECURITY_ENFORCED order by Name];

       if(workspace!=null && workspace.size()>0)
       {  
             allcycles = new list<SelectOption>();
             allcycles.add(new SelectOption('', 'Choose any Workspace'));

             Set<String> uniqueCycleRecs = new Set<String>();

             for(AxtriaSalesIQTM__Workspace__c userInfo : workspace)
             { 
                    allcycles.add(new SelectOption(userInfo.Id, userInfo.Name));
            }
        SnTDMLSecurityUtil.printDebugMessage('+++allcycles'+allcycles);
        scenarioChanged();
    }

}


public void scenarioChanged()
{   disableAnalyse = true;
    allteaminstance = new list<SelectOption>();
    if(cycleSelected!=null)
    {
        selectedCycle = cycleSelected;
        SnTDMLSecurityUtil.printDebugMessage('============selectedCycle:::'+selectedCycle+'::::cycleSelected:'+cycleSelected);
        teamins = [SELECT Id,Name FROM AxtriaSalesIQTM__Team_Instance__c WHERE AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c = :selectedCycle and AxtriaSalesIQTM__Country__c  = :countryID  WITH SECURITY_ENFORCED order by Name];
        SnTDMLSecurityUtil.printDebugMessage('=========teamins====='+teamins);

        if(teamins != null && teamins.size() >0 )
        {
            allteaminstance.add(new SelectOption('', 'Choose any Team Instance'));
            SnTDMLSecurityUtil.printDebugMessage('++selectedCountry'+selectedCountry);

            for(AxtriaSalesIQTM__Team_Instance__c userInfo : teamins)
            {   
                 if(userInfo.Id !=null)
                 {
                     allteaminstance.add(new SelectOption(userInfo.Id,userInfo.Name));
                     TeamInstancename = userInfo.Name;
                 }
             }
             cimconfigrefresh();
         }
         else{
                allteaminstance.add(new SelectOption('','--None--'));
                cimconfigrefresh();
        }
     }
     else{
        allteaminstance.add(new SelectOption('', '--None--'));
        cimconfigrefresh();
    }
}


public void analyseCheck(){
    if(cimConfigId.size() == 1 && cimConfigId[0] != NULL)
        disableAnalyse = false;
    else 
        disableAnalyse = true;
}

public void cimconfigrefresh(){
    disableAnalyse = true;
    Apexpages.addMessage(new ApexPages.Message (ApexPages.Severity.INFO,'Please press CTRL key to choose multiple CIM Configs.'));
    cimConfigOptions = new list<SelectOption>();
    if(teaminstanceSelected != NULL){
        listCIMconfig = [select id, name, AxtriaSalesIQTM__Object_Name__c, AxtriaSalesIQTM__Attribute_API_Name__c, AxtriaSalesIQTM__Aggregation_Type__c, AxtriaSalesIQTM__Aggregation_Object_Name__c, AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c, AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c, AxtriaSalesIQTM__Aggregation_Condition_Attribute_Value__c  from AxtriaSalesIQTM__CIM_Config__c where AxtriaSalesIQTM__team_instance__c =:teaminstanceSelected and AxtriaSalesIQTM__Enable__c=true and (AxtriaSalesIQTM__Hierarchy_Level__c ='1' or AxtriaSalesIQTM__Hierarchy_Level__c = null or AxtriaSalesIQTM__Hierarchy_Level__c = '') limit 1000];
        if(listCIMconfig.size() > 0)
        {
            for(AxtriaSalesIQTM__CIM_Config__c objCIMConfig : listCIMconfig){
                cimConfigOptions.add(new SelectOption(objCIMConfig.id,objCIMConfig.name));
            }
        }
        else{
            cimConfigOptions.add(new SelectOption('','--None--'));
        }
    }
    else{
        cimConfigOptions.add(new SelectOption('','--None--'));
    }
}

public void updatePostionMatrxiSummary(){
    /*displayQuery();
    if(isConfigRecordAlreadyPresent == false){
        if(updateListPostionMetric.size() > 0){
            insert updateListPostionMetric;
        }
        theQuery = 'The values have been inserted, thanks!';
    }
    else if(isConfigRecordAlreadyPresent == true){
        theQuery = 'The configuration is already present in the system!';
    }*/
    if(cimConfigId.size() > 0 && teaminstanceSelected != null){
        Apexpages.addMessage(new ApexPages.Message (ApexPages.Severity.INFO,'The values have been submitted for Insertion, Thanks!'));
        Database.executeBatch(new BatchCIMPositionMatrixSummaryUpdate(cimConfigId,teaminstanceSelected),2000);
    }
    else if(teaminstanceSelected == null){
        Apexpages.addMessage(new ApexPages.Message (ApexPages.Severity.ERROR,'Please choose atleast one Team Instance'));
    }
    else if(cimConfigId.size() == 0){
        Apexpages.addMessage(new ApexPages.Message (ApexPages.Severity.ERROR,'Please choose atleast one CIM Config'));
    }
    
}

public void displayQuery(){
    system.debug('---cimConfigId---' + cimConfigId + '------teaminstanceSelected---' + teaminstanceSelected);
    updateListPostionMetric= new list<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c>();
    //listCIMconfig = [select id, Object_Name__c, Attribute_API_Name__c, Aggregation_Type__c, Aggregation_Object_Name__c, Aggregation_Attribute_API_Name__c, Aggregation_Condition_Attribute_API_Name__c, Aggregation_Condition_Attribute_Value__c  from CIM_Config__c where team_instance__c=:teaminstanceSelected and Aggregation_Object_Name__c='Position_Account_Call_Plan__c'];
    listCIMconfig = [select id, name, AxtriaSalesIQTM__Object_Name__c, AxtriaSalesIQTM__Attribute_API_Name__c, AxtriaSalesIQTM__Aggregation_Type__c, AxtriaSalesIQTM__Aggregation_Object_Name__c, AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c, AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c, AxtriaSalesIQTM__Aggregation_Condition_Attribute_Value__c  from AxtriaSalesIQTM__CIM_Config__c where id=:cimConfigId and AxtriaSalesIQTM__team_instance__c =:teaminstanceSelected];
    
    for(AxtriaSalesIQTM__CIM_Config__c obj : listCIMconfig){
        string objectName = obj.AxtriaSalesIQTM__Object_Name__c;
        string attributeAPIname = obj.AxtriaSalesIQTM__Attribute_API_Name__c;
        string aggregationType = obj.AxtriaSalesIQTM__Aggregation_Type__c;
        string aggregationObjectName = obj.AxtriaSalesIQTM__Aggregation_Object_Name__c;
        aggregationAttributeAPIName = obj.AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c;
        string aggregationConditionAttributeAPIName = obj.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c;
        //string aggregationConditionAttributeValue = obj.AxtriaSalesIQTM__Aggregation_Condition_Attribute_Value__c;
        
        string objectToBeQueried;
        if(aggregationAttributeAPIName!= null && aggregationObjectName!=null ){

            if(objectName != aggregationObjectName){
                objectToBeQueried =  objectName.removeEnd('c');
                objectToBeQueried = objectToBeQueried + 'r.'+attributeAPIname;
                
                
                if(aggregationConditionAttributeAPIName != null){
                    theQuery = 'SELECT '+aggregationType+'(' + objectToBeQueried + '), ' + aggregationAttributeAPIName + ' FROM ' +
                    aggregationObjectName + ' WHERE  AxtriaSalesIQTM__team_instance__c =:teaminstanceSelected and ' + aggregationConditionAttributeAPIName + ' GROUP BY '+  aggregationAttributeAPIName;
                }
                else{
                    theQuery = 'SELECT '+aggregationType+'(' + objectToBeQueried + '), ' + aggregationAttributeAPIName + ' FROM ' +
                    aggregationObjectName + ' WHERE AxtriaSalesIQTM__team_instance__c =:teaminstanceSelected '+ ' GROUP BY '+  aggregationAttributeAPIName;
                }
                
                system.debug('the Query '+ theQuery);
            }
            else{
                objectToBeQueried = attributeAPIname;
                if(aggregationConditionAttributeAPIName != null){
                    theQuery = 'SELECT '+aggregationType+'(' + objectToBeQueried + '), ' + aggregationAttributeAPIName + ' FROM ' +
                    aggregationObjectName + ' WHERE AxtriaSalesIQTM__team_instance__c =:teaminstanceSelected and ' + aggregationConditionAttributeAPIName + ' GROUP BY '+  aggregationAttributeAPIName;
                }
                else{
                    theQuery = 'SELECT '+aggregationType+'(' + objectToBeQueried + '), ' + aggregationAttributeAPIName + ' FROM ' +
                    aggregationObjectName + ' WHERE AxtriaSalesIQTM__team_instance__c =:teaminstanceSelected '+ ' GROUP BY '+  aggregationAttributeAPIName;
                    system.debug('inside Query '+ theQuery);
                }      
                system.debug('======= '+ aggregationConditionAttributeAPIName);
                system.debug('the Query '+ theQuery);
                
            }
            
            AggregateResult[] updateList = Database.query(theQuery);
            system.debug('UpdateList '+ updateList);
            for(AggregateResult  objCIMConfig : updateList){
                //aggregationObjectName objCimFromSobject = (aggregationObjectName)objCIMConfig;
                String originalCalls = String.valueOF(objCIMConfig.get('expr0'));
                
                if(originalCalls == null)
                    originalCalls = '0';
                AxtriaSalesIQTM__CIM_Position_Metric_Summary__c objCIMPositionMetric = new AxtriaSalesIQTM__CIM_Position_Metric_Summary__c(AxtriaSalesIQTM__CIM_Config__c = obj.ID, AxtriaSalesIQTM__Original__c = originalCalls, AxtriaSalesIQTM__Team_Instance__c = teaminstanceSelected, AxtriaSalesIQTM__Proposed__c = originalCalls, AxtriaSalesIQTM__Approved__c = originalCalls, AxtriaSalesIQTM__Position_Team_Instance__c = String.valueOF(objCIMConfig.get(aggregationAttributeAPIName)));
                updateListPostionMetric.add(objCIMPositionMetric);
                
            }
            
            
            set<string> givenPosTeamIns = new set<string>();
            for(AxtriaSalesIQTM__CIM_Position_Metric_Summary__c updPosMetr : updateListPostionMetric){
                givenPosTeamIns.add(updPosMetr.AxtriaSalesIQTM__Position_Team_Instance__c);
            }
            
            string soqlQuery = 'select id, name from AxtriaSalesIQTM__position_team_instance__c where AxtriaSalesIQTM__Team_Instance_ID__c=:teaminstanceSelected';
            list<AxtriaSalesIQTM__Position_team_instance__c> listPosTeamIns = Database.query(soqlQuery);
            system.debug('-----listPosTeamIns ---' + listPosTeamIns[0]);
            set<string> allPosTeamIns = new set<string>();
            for(AxtriaSalesIQTM__Position_team_instance__c posTeamInsObj : listPosTeamIns){
                allPosTeamIns.add(posTeamInsObj.id);
            }

            allPosTeamIns.removeAll(givenPosTeamIns);
            system.debug('----allPosTeamIns---' + allPosTeamIns + '=======' + givenPosTeamIns);
            for(string str : allPosTeamIns){
                system.debug('===str===' + str);
                AxtriaSalesIQTM__CIM_Position_Metric_Summary__c objCIMPositionMetric = new AxtriaSalesIQTM__CIM_Position_Metric_Summary__c(AxtriaSalesIQTM__CIM_Config__c = obj.ID, AxtriaSalesIQTM__Original__c = '0', AxtriaSalesIQTM__Team_Instance__c = teaminstanceSelected, AxtriaSalesIQTM__Proposed__c = '0', AxtriaSalesIQTM__Approved__c = '0', AxtriaSalesIQTM__Position_Team_Instance__c = str);
                updateListPostionMetric.add(objCIMPositionMetric);
            }

            countRecords = updateListPostionMetric.size();
            set<id> positionTeamInstance = new set<id>();
            set<id> cimConfigSet = new set<id>();
            for(AxtriaSalesIQTM__CIM_Position_Metric_Summary__c objCIMpos : updateListPostionMetric){
                positionTeamInstance.add(objCIMpos.AxtriaSalesIQTM__Position_Team_Instance__c);
                cimConfigSet.add(objCIMpos.AxtriaSalesIQTM__CIM_Config__c);
            }
            
            list<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c> cimPositionMetriclist = [select id from AxtriaSalesIQTM__CIM_Position_Metric_Summary__c where AxtriaSalesIQTM__CIM_Config__c in:cimConfigSet and AxtriaSalesIQTM__Position_Team_Instance__c in:positionTeamInstance];
            if(cimPositionMetriclist.size() > 0){
                isConfigRecordAlreadyPresent = true;
                Apexpages.addMessage(new ApexPages.Message (ApexPages.Severity.INFO,'The configuration is present in CIM_Position_Metric_Summary table already!'));
            }else{
                isConfigRecordAlreadyPresent = false;
            }
            system.debug('updateListPostionMetric '+updateListPostionMetric);       
        }   
    }
}
public void updateChannelPreference(){
    List<String> list_ti = new List<String>();  
    if(String.isNotBlank(teaminstanceSelected)){
        list_ti.add(teaminstanceSelected);
        Database.executeBatch(new BatchUpdateChannelPreferenceinPACP(list_ti),2000);  
        Apexpages.addMessage(new ApexPages.Message (ApexPages.Severity.INFO,'Channel Preference Update has been initiated.'));
    }
    
}

}