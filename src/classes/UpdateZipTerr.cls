global with sharing class UpdateZipTerr implements Database.Batchable<sObject>, Database.Stateful
{
    /*Replaced temp_Zip_Terr__c with temp_Obj__c due to object purge*/
    global string query;
    global string teamID;
    global string teamInstance;
    global string teamName;
    global string country;
    String Ids;
    Boolean flag = true;
    global list<AxtriaSalesIQTM__Team__c> temlst = new list<AxtriaSalesIQTM__Team__c>();
    global list<AxtriaSalesIQTM__Geography__c> ziplist = new list<AxtriaSalesIQTM__Geography__c>();
    global list<AxtriaSalesIQTM__Position__c> poslist = new list<AxtriaSalesIQTM__Position__c>();
    global list<temp_Obj__c> zipTerrlist = new list<temp_Obj__c>();
    global list<AxtriaSalesIQTM__Team_Instance__c> countrylst = new list<AxtriaSalesIQTM__Team_Instance__c>();
    global string geotype ;

    //Added by HT(A0994) on 17th June 2020
    global String changeReqID;
    global Boolean flagValue = false;

    global UpdateZipTerr(String Team, String TeamIns)
    {
        teamInstance = TeamIns;
        teamID = Team;
        geotype = '';
        temlst = [select name from AxtriaSalesIQTM__Team__c where id = :teamID];
        teamName = temlst[0].name;
        countrylst = [select AxtriaSalesIQTM__Country__c, AxtriaSalesIQTM__Geography_Type_Name__c from AxtriaSalesIQTM__Team_Instance__c where id = :teamInstance];
        country = countrylst[0].AxtriaSalesIQTM__Country__c;
        geotype = countrylst[0].AxtriaSalesIQTM__Geography_Type_Name__c;
        poslist = [select id, AxtriaSalesIQTM__Client_Position_Code__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_iD__c = :teamID and AxtriaSalesIQTM__Team_Instance__c = :teamInstance];
        /*query='SELECT id,Territory_ID__c,Zip_Name__c,Team_Name__c FROM temp_Zip_Terr__c where Team_Name__c=:teamName and Event__c = \'Insert\' ';*/
        query = 'SELECT id,Territory_ID__c,Zip_Name__c,Team_Name__c FROM temp_Obj__c where Team_Name__c=:teamName and Event__c = \'Insert\' and Object__c = \'Zip_Terr\'';
    }

    global UpdateZipTerr(String Team, String TeamIns, String Ids)
    {
        teamInstance = TeamIns;
        teamID = Team;
        this.Ids = Ids;
        geotype = '';
        temlst = [select name from AxtriaSalesIQTM__Team__c where id = :teamID];
        teamName = temlst[0].name;
        countrylst = [select AxtriaSalesIQTM__Country__c, AxtriaSalesIQTM__Geography_Type_Name__c 
                    from AxtriaSalesIQTM__Team_Instance__c where id = :teamInstance WITH SECURITY_ENFORCED];
        country = countrylst[0].AxtriaSalesIQTM__Country__c;
        geotype = countrylst[0].AxtriaSalesIQTM__Geography_Type_Name__c;
        poslist = [select id, AxtriaSalesIQTM__Client_Position_Code__c from AxtriaSalesIQTM__Position__c 
                    where AxtriaSalesIQTM__Team_iD__c = :teamID and 
                    AxtriaSalesIQTM__Team_Instance__c = :teamInstance WITH SECURITY_ENFORCED];

        /*query='SELECT id,Territory_ID__c,Zip_Name__c,Team_Name__c FROM temp_Zip_Terr__c where Team_Name__c=:teamName and Event__c = \'Insert\' ';*/
        
        //Changed by HT(A0994) on 17th June 2020 
        query = 'SELECT id,Territory_ID__c,Zip_Name__c,Team_Name__c FROM temp_Obj__c where '+
                'Team_Name__c=:teamName and Event__c = \'Insert\' and Object__c = \'Zip_Terr\' '+
                'and Territory_ID__c !=null and Zip_Name__c !=null and Team_Name__c !=null '+
                'and Event__c !=null';
    }

    //Added by HT(A0994) on 17th June 2020
    global UpdateZipTerr(String Team, String TeamIns, String Ids, Boolean flag)
    {
        teamInstance = TeamIns;
        teamID = Team;
        this.Ids = Ids;
        changeReqID = Ids;
        flagValue = flag;
        geotype = '';
        temlst = [select name from AxtriaSalesIQTM__Team__c where id = :teamID];
        teamName = temlst[0].name;
        countrylst = [select AxtriaSalesIQTM__Country__c, AxtriaSalesIQTM__Geography_Type_Name__c 
                    from AxtriaSalesIQTM__Team_Instance__c where id = :teamInstance WITH SECURITY_ENFORCED];
        country = countrylst[0].AxtriaSalesIQTM__Country__c;
        geotype = countrylst[0].AxtriaSalesIQTM__Geography_Type_Name__c;
        poslist = [select id, AxtriaSalesIQTM__Client_Position_Code__c from AxtriaSalesIQTM__Position__c 
                    where AxtriaSalesIQTM__Team_iD__c = :teamID and 
                    AxtriaSalesIQTM__Team_Instance__c = :teamInstance WITH SECURITY_ENFORCED];

        /*query='SELECT id,Territory_ID__c,Zip_Name__c,Team_Name__c FROM temp_Zip_Terr__c where Team_Name__c=:teamName and Event__c = \'Insert\' ';*/
        
        //Changed by HT(A0994) on 17th June 2020 
        /*query = 'SELECT id,Territory_ID__c,Zip_Name__c,Team_Name__c FROM temp_Obj__c where '+
                'Team_Name__c=:teamName and Event__c = \'Insert\' and Object__c = \'Zip_Terr\' '+
                'and Territory_ID__c !=null and Zip_Name__c !=null and Team_Name__c !=null '+
                'and Event__c !=null'*/
        query = 'SELECT id,Territory_ID__c,Zip_Name__c,Team_Name__c FROM temp_Obj__c where '+
                'Team_Name__c=:teamName and Event__c = \'Insert\' and Object__c = \'Zip_Terr\' '+
                'and Territory_ID__c !=null and Zip_Name__c !=null and Team_Name__c !=null '+
                'and Event__c !=null and Change_Request__c =:changeReqID WITH SECURITY_ENFORCED';
    }

    global Database.Querylocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator(query);
    }

    global void execute (Database.BatchableContext BC, List<temp_Obj__c>zipTerrlist)
    {
        /*for(temp_Zip_Terr__c rec:zipTerrlist){
            while (rec.Zip_Name__c.length() < 5)
               {
              rec.Zip_Name__c = '0' + rec.Zip_Name__c;
              }
        }*/                                                    //formating zip to "00000"

        //NISHANT Update

        list<string> list1 = new list<string>();
        for(temp_Obj__c a : zipTerrlist)
        {
            list1.add(a.Zip_Name__c);
        }

        list<temp_Obj__c> zipTerListUpdate = new list<temp_Obj__c>();
        ziplist = [select id, name from AxtriaSalesIQTM__Geography__c where name in: list1 and Country_ID__c = :country and AxtriaSalesIQTM__Geography_Type__c = :geotype WITH SECURITY_ENFORCED];
        for(temp_Obj__c rec : zipTerrlist)
        {
            /*while (rec.Zip_Name__c.length() < 5)
               {
              rec.Zip_Name__c = '0' + rec.Zip_Name__c;
              }*/                                                            //formating zip to "00000" for update in object

            for(AxtriaSalesIQTM__Geography__c zip : ziplist)
            {
                if(zip.name == rec.Zip_Name__c)  rec.Geography__c = zip.id;             //geography lookup
            }
            for(AxtriaSalesIQTM__Position__c pos : poslist)
            {
                if(rec.Territory_ID__c == pos.AxtriaSalesIQTM__Client_Position_Code__c)  rec.Position__c = pos.id; //position lookup
            }
            rec.Team__c = teamID;
            rec.Team_Instance__c = teamInstance;
            zipTerListUpdate.add(rec);
        }
        //update zipTerListUpdate;
        SnTDMLSecurityUtil.updateRecords(zipTerListUpdate, 'UpdateZipTerr');

        system.debug('******************************update done************************************');
    }

    global void finish(Database.BatchableContext BC)
    {   
        if(ST_Utility.getJobStatus(BC.getJobId()))
        {
            if(Ids != null)
            {
                if(flagValue)
                    Database.executeBatch(new UpdatePosGeography(teamID,teamInstance,IDs,flagValue), 500);
                else
                    Database.executeBatch(new UpdatePosGeography(teamID,teamInstance,IDs), 500);
            }
            else
            {
                Database.executeBatch(new UpdatePosGeography(teamID, teamInstance), 500);
            }
        }
        else if(Ids != null){
            AxtriaSalesIQTM__Change_Request__c changerequest = new  AxtriaSalesIQTM__Change_Request__c();
            changerequest.Id = Ids;
            changerequest.Job_Status__c = 'Error';
            //update changerequest;     
            SnTDMLSecurityUtil.updateRecords(changerequest, 'UpdateZipTerr');  
        }  
    }
}