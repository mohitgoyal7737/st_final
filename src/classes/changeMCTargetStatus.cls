global with sharing class changeMCTargetStatus implements Database.Batchable<sObject> {
    

    List<String> allChannels;
    String teamInstanceSelected;
    List<String> allTeamInstances;
    String queryString;
    Set<String> activityLogIDSet;
    
    //List<Parent_PACP__c> pacpRecs;
    
    global changeMCTargetStatus(string teamInstanceSelectedTemp, List<String> allChannelsTemp)
    { 

       queryString = 'select id, Rec_Status__c from SIQ_MC_Cycle_Plan_Target_vod_O__c where Team_Instance__c =   :teamInstanceSelected';

        teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;
    }

    global changeMCTargetStatus(List<string> teamInstanceSelectedTemp, List<String> allChannelsTemp)
    { 
        allTeamInstances = new List<String>(teamInstanceSelectedTemp);

       queryString = 'select id, Rec_Status__c from SIQ_MC_Cycle_Plan_Target_vod_O__c where Team_Instance__c in   :allTeamInstances';
        //teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;
    }

    global changeMCTargetStatus(List<string> teamInstanceSelectedTemp, List<String> allChannelsTemp, Set<String> activityLogSet)
    { 
        allTeamInstances = new List<String>(teamInstanceSelectedTemp);
        activityLogIDSet = new Set<String>();

       queryString = 'select id, Rec_Status__c from SIQ_MC_Cycle_Plan_Target_vod_O__c where Team_Instance__c in   :allTeamInstances';
        //teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;
        activityLogIDSet.addAll(activityLogSet);

        //Added By Ayushi
        // List<Error_Object__c> insertErrorObj = new List<Error_Object__c>();
        // for(String teamIns : allTeamInstances)
        // {
        //     Error_Object__c rec = new Error_Object__c();
        //     rec.Object_Name__c = 'changeMCTargetStatus';
        //     rec.Team_Instance_Name__c = teamIns;
        //     insertErrorObj.add(rec);
        // }
        // insert insertErrorObj;
        //till here
    }
    
    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        return Database.getQueryLocator(queryString);
    }
    
    
    global void execute(Database.BatchableContext BC, List<SIQ_MC_Cycle_Plan_Target_vod_O__c> scopePacpProRecs)
    {
        for(SIQ_MC_Cycle_Plan_Target_vod_O__c mcTarget : scopePacpProRecs)
        {
            mcTarget.Rec_Status__c = '';
        }
        
         //update scopePacpProRecs;
        SnTDMLSecurityUtil.updateRecords(scopePacpProRecs, 'changeMCTargetStatus');

    }

    global void finish(Database.BatchableContext BC)
    {
        if(activityLogIDSet != null)
        {
            AZ_Integration_MC_Target_MP_EU u1New = new AZ_Integration_MC_Target_MP_EU(allTeamInstances, allChannels, activityLogIDSet);
            Database.executeBatch(u1New, 1000);
        }
        else
        {
            AZ_Integration_MC_Target_MP_EU u1 = new AZ_Integration_MC_Target_MP_EU(allTeamInstances, allChannels);
            Database.executeBatch(u1, 1000);
            //Database.executeBatch(new MarkMCCPtargetDeleted());
        }
    }
}