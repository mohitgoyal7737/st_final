global class BatchDeletePrevDayLOAFeed implements Database.Batchable<sObject>, Database.stateful{

    global BatchDeletePrevDayLOAFeed(){}
    
    global Database.QueryLocator start(Database.BatchableContext BC){  
        string query = 'SELECT Id, Name, AssociateOID__c, LOA_StartDate__c, LOA_EndDate__c, Paid_Time_Off_Policy__c, Total_Quantity__c FROM Previous_Leave_Data_Feed__c ';
        system.debug('inside getQueryLocator :: query: '+query);
        return Database.getQueryLocator(query);
    }
    
     global void execute(Database.BatchableContext BC, list<Previous_Leave_Data_Feed__c> prevLOAList){
        delete prevLOAList;
     }
    
    global void finish(Database.BatchableContext BC){   
        system.debug('--finished---');
        Database.executeBatch(new BatchCopyCurrDayToPrevDayLOA(), 100);
    }
}