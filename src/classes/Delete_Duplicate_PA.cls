global class Delete_Duplicate_PA implements Database.Batchable<sObject>, Database.Stateful{
    
    global string query;
    global string teamID;
    global string workspace {get;set;}
    global map<String, String> mapCountryCode ;
    global list<AxtriaSalesIQTM__Country__c>countrylist {get;set;}
    global map<String,String> PAMPAP ;
    global string Str1;
    global list<string> PAlist {get;set;}
    global list<string> PAlist1 {get;set;}
    global list<string> PAlist2 {get;set;}
    global list<string> PAlist3 {get;set;}
    global list<string> NoCountryList {get;set;}
    
    global Delete_Duplicate_PA(String wrkspace){
        workspace = '';
        workspace = wrkspace;
        NoCountryList = new List<String>();
        
        
       
        NoCountryList.add('ES');
        NoCountryList.add('IT');
        query='SELECT Id,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.type,AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__Active__c,AxtriaSalesIQTM__Assignment_Status__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c  FROM AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c =:workspace AND AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c NOT IN :NoCountryList AND (AxtriaSalesIQTM__Assignment_Status__c =\'Active\' OR AxtriaSalesIQTM__Assignment_Status__c =\'Future Active\') ';
       
    }
    
    global Database.Querylocator start(Database.BatchableContext bc){
        system.debug('============'+Database.getQueryLocator(query));
        return Database.getQueryLocator(query);
        
    }
    
    global void execute (Database.BatchableContext BC, List<AxtriaSalesIQTM__Position_Account__c>scope){
        
        PAMPAP = new map<String,String> ();
        PAlist1 = new list<string>();
        //PAlist2 = new list<string>();
        //PAlist3 = new list<string>();
        PAlist = new list<string>();
     for (AxtriaSalesIQTM__Position_Account__c PA : scope){
         
         Str1 = string.valueof(PA.AxtriaSalesIQTM__Account__c)+'_'+string.valueof(PA.AxtriaSalesIQTM__Position__c)+'_'+string.valueof(PA.AxtriaSalesIQTM__Team_Instance__c)+'_'+string.valueof(PA.AxtriaSalesIQTM__Assignment_Status__c);
         if(!PAMPAP.containsKey(PA.id))
         PAMPAP.put(PA.id,Str1);
         
     }
 
     system.debug('****PAMPAP**********'+PAMPAP);
      for(String key:PAMPAP.keyset()){
          
          system.debug('****key**********'+key);
         String key2 = PAMPAP.get(key);
          
          system.debug('******key2********'+key2);
          system.debug('****PAlist**********'+PAlist);
          if(!PAlist.contains(key2))
          {
              PAlist.add(key2);
              system.debug('****PAlist**********'+PAlist);
              PAlist1.add(key);
              system.debug('****PAlist**********'+PAlist1);
              String[] str = key2.split('_');
              delete[Select ID from AxtriaSalesIQTM__Position_Account__c where ID not IN :PAlist1 and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c =:workspace and AxtriaSalesIQTM__Account__c = :str[0] and AxtriaSalesIQTM__Position__c = :str[1] and AxtriaSalesIQTM__Team_Instance__c = :str[2] and AxtriaSalesIQTM__Assignment_Status__c = :str[3]];
          }
      }
          
      
      //system.debug(PAlist);

    }
    
    global void finish(Database.BatchableContext BC){
        
         
        
}
}