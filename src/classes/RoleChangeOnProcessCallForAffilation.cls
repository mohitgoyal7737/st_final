public class RoleChangeOnProcessCallForAffilation {
    @invocableMethod(Label = 'updateAccountRole' description='Update Account role on Affilation role change')
    public static void updateAccount(List<AxtriaSalesIQTM__Account_Affiliation__c> AccAffRecord)
    {
        Set<String> AccNumber=new Set<String>();
        List<Account> Acc = new List<Account>();
        
        System.debug('>>>>AccAffREcord>>'+AccAffRecord);
        for(AxtriaSalesIQTM__Account_Affiliation__c AccAff:AccAffRecord){
            String AccNum=AccAff.Account_Number__c;
            AccNumber.add(AccNum);
        }
        System.Debug('<<<<<<<<<<<<<<<<<<<<<<<AccNumber>>>>>>>>>>>>>>>'+AccNumber);
        
        Map<String,Set<String>> MapAccAff = new Map<String,Set<String>>();
        for(AxtriaSalesIQTM__Account_Affiliation__c ActiveAff : [Select id,Account_Number__c,Role_Name__c,AxtriaSalesIQTM__Active__c from AxtriaSalesIQTM__Account_Affiliation__c where Account_Number__c IN :AccNumber AND AxtriaSalesIQTM__Active__c = true AND AxtriaSalesIQTM__Affiliation_Type__c = 'HCP_to_HCA' AND Role_Name__c NOT IN (null,'NA')])
        {
            if(MapAccAff.containsKey(ActiveAff.Account_Number__c))
            {
                MapAccAff.get(ActiveAff.Account_Number__c).add(ActiveAff.Role_Name__c);
            }
            
            else
            {
                Set<String> temp=new Set<String>();
                temp.add(ActiveAff.Role_Name__c);
                MapAccAff.put(ActiveAff.Account_Number__c,temp);
            }
        }
        System.debug('>>>>>>>>>>MapAccAff>>>>>>>>>'+MapAccAff);
        Set<String> AccWithNullRole = new Set<String>();
        Set<String> MapToSet = new Set<String>();
        MapToSet.addAll(MapAccAff.keySet());
        
        for(String ACC_Num :AccNumber){
            if(!MapToSet.contains(ACC_Num))
            {
                AccWithNullRole.add(ACC_Num);
            }
        }
        
        for(Account AccRole : [Select id,AccountNumber,Role_Name__c from Account where AccountNumber IN : MapAccAff.keySet()])
        {
            //AccRole.Role_Name__c='';
            Set<String> RoleSet= new Set<String>();
            RoleSet=MapAccAff.get(AccRole.AccountNumber);
            
            System.debug('>>>>RoleSet>>>>'+RoleSet);
            // if(AccRole.Role_Name__c!=null){
            //     String tempRole =AccRole.Role_Name__c+';'+String.join(new List<String>(RoleSet),';');
            //     List<String> tempRoleList=new List<String>();
            //     tempRoleList=tempRole.split(';');
            //     Set<String> tempRoleSet = new Set<String>();
            //     tempRoleSet.addAll(tempRoleList);
            //     AccRole.Role_Name__c =String.join(new List<String>(tempRoleSet),';');
            // }
            //  else {
            String tempRole =String.join(new List<String>(RoleSet),';');
            List<String> tempRoleList=new List<String>();
            tempRoleList=tempRole.split(';');
            Set<String> tempRoleSet = new Set<String>();
            tempRoleSet.addAll(tempRoleList);
            AccRole.Role_Name__c =String.join(new List<String>(tempRoleSet),';');
            //  }
            
            System.debug('>>>>>AccRole>>>>>'+AccRole);
            acc.add(AccRole);
            System.debug('>>>>acc>>>>'+acc);
            
        }
        
        if(!AccWithNullRole.isEmpty()){
            
            for(Account AccRole : [Select id,AccountNumber,Role_Name__c from Account where AccountNumber IN : AccWithNullRole])
            {
                AccRole.Role_Name__c='';
                acc.add(AccRole);
            }  
        }
        
        if(acc!=null){
            update acc;
        } 
    }
    
}