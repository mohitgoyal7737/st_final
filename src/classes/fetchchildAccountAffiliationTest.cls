@isTest()
public class fetchchildAccountAffiliationTest 
{
    static testMethod void testMethod1() 
    {
        
        string Str1;
        AxtriaARSnT__Scheduler_Log__c sJob = new AxtriaARSnT__Scheduler_Log__c();

         sJob.AxtriaARSnT__Job_Name__c = 'childAccountAffiliation';
        sJob.AxtriaARSnT__Job_Status__c = 'Failed';
        sJob.AxtriaARSnT__Job_Type__c='Inbound';
        sJob.AxtriaARSnT__Cycle__c='Sc2019';
        sJob.AxtriaARSnT__Created_Date2__c = DateTime.now();

         sjob.AxtriaARSnT__Object_Name__c = 'childAccountAffiliation';

        sjob.AxtriaARSnT__Job_Status__c='Successful';

        insert sJob;

        


        AxtriaSalesIQTM__Organization_Master__c orgmaster = new AxtriaSalesIQTM__Organization_Master__c();
        orgmaster.name = 'GB';
        orgmaster.AxtriaSalesIQTM__Org_Level__c= 'Global';
        orgmaster.CurrencyIsoCode='EUR';
        orgmaster.AxtriaSalesIQTM__Parent_Country_Level__c=TRUE;

        insert orgmaster;

        String id=orgmaster.id;
        system.debug('orgmaster^^^^^^^^^^'+id);

        AxtriaSalesIQTM__Country__c Contr = new AxtriaSalesIQTM__Country__c();
        Contr.name= 'Great Britain';
        Contr.AxtriaSalesIQTM__Parent_Organization__c= orgmaster.Id;
        contr.CurrencyIsoCode= 'EUR';
        contr.AxtriaSalesIQTM__Status__c= 'Active';
        insert contr;

        system.debug('Contr^^^^^^^^^^'+contr.id);

        AxtriaSalesIQTM__Team__c Team  = new AxtriaSalesIQTM__Team__c();
        Team.name = 'GB_ACC';
        Team.CurrencyIsoCode= 'EUR';
        Team.AxtriaSalesIQTM__Country__c = Contr.id;
        insert Team;

        system.debug('Team^^^^^^^^^^'+Team.id);

        AxtriaSalesIQTM__Team_Instance__c TIs = new AxtriaSalesIQTM__Team_Instance__c();
       TIs.Name= 'UK_2019_H2_ACCESS';
       TIs.AxtriaSalesIQTM__Team__c = Team.id;
       TIs.AxtriaSalesIQTM__Alignment_Period__c='Current';
       insert TIs;
       
       system.debug('TIs^^^^^^^^^^'+Team.id);
       
       Schema.DescribeFieldResult Pickvalue= AxtriaSalesIQTM__Position__c.AxtriaARSnT__Sales_Team_Attribute_MS__c.getDescribe();
       List<Schema.PicklistEntry> PickListValue = Pickvalue.getPicklistValues();

        AxtriaSalesIQTM__Position__c Pos = new AxtriaSalesIQTM__Position__c();
        Pos.name = 'GBAZ90RM';
        Pos.AxtriaSalesIQTM__Team_iD__c = Team.id;
        Pos.AxtriaSalesIQTM__Team_Instance__c = TIs.id;
        Pos.CurrencyIsoCode= 'EUR';
        Pos.AxtriaSalesIQTM__Client_Position_Code__c='CE1346';
        Pos.AxtriaSalesIQTM__Description__c= 'XYZ';
        Pos.AxtriaARSnT__Position_Description__c= 'XYA';
        Pos.AxtriaARSnT__Sales_Team_Attribute_MS__c = String.valueof(PickListValue[0].getValue());
        Pos.AxtriaARSnT__Channel_Id_AZ__c= 'MR';
        Pos.AxtriaARSnT__Channel_AZ__c= 'MR';
        insert Pos;

        system.debug('Pos^^^^^^^^^^'+Pos.id);

        Account ACC = new Account();
        ACC.name= 'Katie Elizabeth Dennis';
        ACC.AxtriaARSnT__Marketing_Code__c= 'GB';
        ACC.CurrencyIsoCode= 'EUR';
        ACC.Type = 'HCA';
        ACC.AxtriaSalesIQTM__Country__c = Contr.id;
        insert ACC;
         
        system.debug('ACC^^^^^^^^^^'+ACC.id);
       
       Account ACC1 = new Account();
        ACC1.name= 'Kat will';
        ACC1.AxtriaARSnT__Marketing_Code__c= 'GB';
        ACC1.CurrencyIsoCode= 'EUR';
        ACC1.Type = 'HCA';
        ACC1.AxtriaSalesIQTM__Country__c = Contr.id;
        insert ACC1;
       

       AxtriaSalesIQTM__Position_Team_Instance__c PTI = new AxtriaSalesIQTM__Position_Team_Instance__c();
       PTI.AxtriaSalesIQTM__Effective_Start_Date__c = System.today();
       PTI.AxtriaSalesIQTM__Effective_End_Date__c = System.today()+1;
       PTI.AxtriaSalesIQTM__Position_ID__c = Pos.id;
       PTI.AxtriaSalesIQTM__Team_Instance_ID__c = TIs.ID;
       PTI.CurrencyIsoCode = 'EUR';
       Insert PTI;
       system.debug('PTI^^^^^^^^^^'+PTI.id);
       
       AxtriaSalesIQTM__Change_Request__c CR = new AxtriaSalesIQTM__Change_Request__c();
       CR.AxtriaSalesIQTM__Source_Position__c = pos.id;
       CR.AxtriaSalesIQTM__Team_Instance_ID__c= TIs.id;
       CR.AxtriaSalesIQTM__Request_Type_Change__c= 'Account Movement';
       CR.AxtriaSalesIQTM__Destination_Position__c=Pos.id;
       CR.AxtriaSalesIQTM__Status__c='Approved';
       CR.CurrencyIsoCode= 'EUR';
       insert CR;

       system.debug('CR^^^^^^^^^^'+CR.id);

       AxtriaSalesIQTM__CR_Account__c CRAcc = new AxtriaSalesIQTM__CR_Account__c();
        CRacc.name = 'CR-02001';
        CRacc.CurrencyIsoCode = 'EUR';
        CRAcc.AxtriaSalesIQTM__Account__c = ACC1.id;
        CRacc.AxtriaSalesIQTM__Source_Position__c = Pos.id;
        CRacc.AxtriaSalesIQTM__Change_Request__c =CR.id;
        CRacc.AxtriaSalesIQTM__Destination_Position__c =Pos.id;
        CRacc.AxtriaSalesIQTM__Destination_Position_Team_Instance__c = PTI.id;
        //CRacc.AxtriaSalesIQTM__Account__c = (new Account(Type = 'HCA',AxtriaSalesIQTM__Country__c = (new AxtriaSalesIQTM__Country__c(Name = 'Great Britain')).id)).ID; 
        //CRacc.AxtriaSalesIQTM__Change_Request__C = (new AxtriaSalesIQTM__Change_Request__C(AxtriaSalesIQTM__Request_Type_Change__c = 'Account Movement')).id;
       // CRacc.AxtriaSalesIQTM__Destination_Position_Team_Instance__c = (new AxtriaSalesIQTM__Position_Team_Instance__c(AxtriaSalesIQTM__Team_Instance_ID__c = (new AxtriaSalesIQTM__Team_Instance__c(AxtriaSalesIQTM__Alignment_Period__c = 'Future')).id)).id;
        
        //CRacc.AxtriaSalesIQTM__Change_Request__r.AxtriaSalesIQTM__Request_Type_Change__c = 'Account Movement';
       // AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__Country__r.name =\'Great Britain\'
       //     AxtriaSalesIQTM__Destination_Position_Team_Instance__r.AxtriaSalesIQTM__Team_Instance_ID__r.AxtriaSalesIQTM__Alignment_Period__c = \'Future\' 
        insert CRacc;
        system.debug('CRacc^^^^^^^^^^'+CRacc);

        AxtriaSalesIQTM__Affiliation_Network__c Affnet = new AxtriaSalesIQTM__Affiliation_Network__c();
        Affnet.name='AZ Network';
        Affnet.CurrencyIsoCode= 'EUR';
        Affnet.AxtriaSalesIQTM__Country__c = contr.id;
        insert Affnet;
        
        AxtriaSalesIQTM__Account_Affiliation__c Accaff = new AxtriaSalesIQTM__Account_Affiliation__c();
        Accaff.AxtriaSalesIQTM__Account__c = ACC.Id;
        Accaff.AxtriaSalesIQTM__Parent_Account__c = ACC1.Id;
        Accaff.CurrencyIsoCode= 'EUR';
        Accaff.AxtriaSalesIQTM__Affiliation_Network__c = Affnet.id;
        Accaff.AxtriaSalesIQTM__Active__c= TRUE;
        Accaff.AxtriaARSnT__Affiliation_Status__c = 'Active';
        Accaff.AxtriaARSnT__Country_Code__c = 'GB';

        insert Accaff;
       
         //AxtriaSalesIQTM__Position_Account__c newobj = new AxtriaSalesIQTM__Position_Account__c();
        list<AxtriaSalesIQTM__Position_Account__c> insertnewPosAccount = new list<AxtriaSalesIQTM__Position_Account__c>();
        for(Integer i = 0; i <1000 ; i ++)
      {
      
           AxtriaSalesIQTM__Position_Account__c newobj = new AxtriaSalesIQTM__Position_Account__c();
           newobj.AxtriaSalesIQTM__Position__c = Pos.id;
           newobj.AxtriaARSnT__Accessibility_Range__c = '9999';
           newobj.AxtriaARSnT__isDirectLoad__c = 'TRUE';
           newobj.AxtriaSalesIQTM__Account__c = ACC.id;
           newobj.AxtriaSalesIQTM__Account_Alignment_Type__c = 'Explicit';
           newobj.AxtriaSalesIQTM__Effective_End_Date__c = System.today()+1;
           newobj.AxtriaSalesIQTM__Effective_Start_Date__c = System.today();
           newobj.AxtriaSalesIQTM__Team_Instance__c = TIs.id;
           newobj.AxtriaSalesIQTM__Segment_1__c= 'Primary';
           newobj.AxtriaSalesIQTM__Position_Team_Instance__c = PTI.id;
           newobj.AxtriaSalesIQTM__Proposed_Position__c = Pos.id;

      /*     Str1 = String.valueof(newobj.AxtriaSalesIQTM__Account__c).substring(0, 15)+'_'+String.valueof(newobj.AxtriaSalesIQTM__Position__c).substring(0, 15)+'_'+String.valueof(newobj.AxtriaSalesIQTM__Team_Instance__c).substring(0, 15)+'_'+'Active';*/

           insertnewPosAccount.add(newobj);

        }

        insert insertnewPosAccount;


        //list<AxtriaSalesIQTM__Account_Affiliation__c> Accaff = new list<AxtriaSalesIQTM__Account_Affiliation__c>();
       
        Test.startTest();
       
        fetchchildAccountAffiliation obj = new fetchchildAccountAffiliation ();
        Database.executeBatch(obj);
     
        Test.stopTest();
    }
}