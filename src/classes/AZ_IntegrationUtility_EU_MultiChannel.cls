global class AZ_IntegrationUtility_EU_MultiChannel implements Database.Batchable<sObject>{   
    
    List<SIQ_MC_Cycle_Plan_Product_vod_O__c> mcCyclePlanProduct;
    List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacpProRecs;
    List<String> allChannels ;
    String queryString;
    string teamInstanceSelected;
    List<String> allString;
    public List<string> allTeamInstances;
    public List<Segment_Veeva_Mapping__c> segmentVeevaMapping;
    public Map<String,String> segmentVeevaMappingMap;
    List<String> teamprodacc;
    List<String> teamProdAccConcat;
    public Map<String,Decimal> teamSellValues;
    public Map<String,Decimal> teamSellValues1;


    global AZ_IntegrationUtility_EU_MultiChannel(string teamInstanceSelectedTemp, List<String> allChannelsTemp)
    { 
    queryString = 'select id, Segment__c,Segment_Approved__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,AxtriaSalesIQTM__Metric2_Approved__c,AxtriaSalesIQTM__Metric3_Approved__c,AxtriaSalesIQTM__Metric4_Approved__c, AxtriaSalesIQTM__Metric5_Approved__c, Final_TCF__c,Final_TCF_Approved__c,AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Account__r.AccountNumber ,  P1__c,AxtriaSalesIQTM__Team_Instance__r.Team_Goals__c, AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Position__r.Original_Country_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c,AxtriaSalesIQTM__Team_Instance__r.Cycle__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Team_Instance__c = :teamInstanceSelected and  AxtriaSalesIQTM__lastApprovedTarget__c = true and AxtriaSalesIQTM__Position__c !=null and P1__c != null AND Parent_Pacp__c != null';
        
        mcCyclePlanProduct = new List<SIQ_MC_Cycle_Plan_Product_vod_O__c>();
        teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;
        allString = new List<String>();
    }

    global AZ_IntegrationUtility_EU_MultiChannel(string teamInstanceSelectedTemp, List<String> allChannelsTemp, Integer count)
    { 
    queryString = 'select id, Segment__c,Segment_Approved__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,AxtriaSalesIQTM__Metric2_Approved__c,AxtriaSalesIQTM__Metric3_Approved__c,AxtriaSalesIQTM__Metric4_Approved__c, AxtriaSalesIQTM__Metric5_Approved__c, Final_TCF__c,Final_TCF_Approved__c,AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Account__r.AccountNumber ,  P1__c,AxtriaSalesIQTM__Team_Instance__r.Team_Goals__c, AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Position__r.Original_Country_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c,AxtriaSalesIQTM__Team_Instance__r.Cycle__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Team_Instance__c = :teamInstanceSelected and AxtriaSalesIQTM__Position__c !=null and P1__c != null AND Parent_Pacp__c != null';
        
        mcCyclePlanProduct = new List<SIQ_MC_Cycle_Plan_Product_vod_O__c>();
        teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;
        allString = new List<String>();
    }

    global AZ_IntegrationUtility_EU_MultiChannel(List<string> teamInstanceSelectedTemp, List<String> allChannelsTemp)
    { 
        allTeamInstances = new List<String>(teamInstanceSelectedTemp);

        system.debug('+++++++++++++ Hey All Teams are '+ allTeamInstances);

    queryString = 'select id, Segment__c,Segment_Approved__c,AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,AxtriaSalesIQTM__Metric2_Approved__c,AxtriaSalesIQTM__Metric3_Approved__c,AxtriaSalesIQTM__Metric4_Approved__c, AxtriaSalesIQTM__Metric5_Approved__c, Final_TCF__c,Final_TCF_Approved__c,AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Account__r.AccountNumber ,  P1__c,AxtriaSalesIQTM__Position__r.Original_Country_Code__c,AxtriaSalesIQTM__Team_Instance__r.Team_Goals__c,AxtriaSalesIQTM__Team_Instance__r.Cycle__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances and  AxtriaSalesIQTM__lastApprovedTarget__c = true and AxtriaSalesIQTM__Position__c !=null and P1__c != null AND Parent_Pacp__c != null';
        
        mcCyclePlanProduct = new List<SIQ_MC_Cycle_Plan_Product_vod_O__c>();
        //teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;
        allString = new List<String>();
    }
    
    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        return Database.getQueryLocator(queryString);
    }
    
    public void create_MC_Cycle_Plan_Product_vod( List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scopePacpProRecs)
    {
        

        String selectedMarket = [select AxtriaSalesIQTM__Team__r.Country_Name__c from AxtriaSalesIQTM__Team_Instance__c where id = :allTeamInstances[0]].AxtriaSalesIQTM__Team__r.Country_Name__c;
        
         List<Veeva_Market_Specific__c> veevaCriteria = [select Metric_Name__c,Weight__c,Channel_Name__c,MCCP_Target_logic__c,Channel_Criteria__c,Market__c,MC_Cycle_Threshold_Max__c,MC_Cycle_Threshold_Min__c,MC_Cycle_Channel_Record_Type__c,MC_Cycle_Plan_Channel_Record_Type__c,MC_Cycle_Plan_Product_Record_Type__c,MC_Cycle_Plan_Record_Type__c,MC_Cycle_Plan_Target_Record_Type__c,MC_Cycle_Product_Record_Type__c,MC_Cycle_Record_Type__c from Veeva_Market_Specific__c where Market__c = :selectedMarket];
        
         list<string>allChannels = new list<string>();
        Map<string,string> channelMatricMapping = new Map<string,string>();
         
        for(integer i=0; i < veevaCriteria[0].Channel_Name__c.split(',').size();i++){
            
            channelMatricMapping.put(veevaCriteria[0].Channel_Name__c.split(',')[i],veevaCriteria[0].Metric_Name__c.split(',')[i]);
            allChannels.add(veevaCriteria[0].Channel_Name__c.split(',')[i]); 
           
        }
        segmentVeevaMapping = new List<Segment_Veeva_Mapping__c>();
        segmentVeevaMappingMap = new Map<String, String>();
        teamprodacc = new List<String>();
        teamProdAccConcat = new List<String>();
        teamSellValues= new Map<String,Decimal>();
        teamSellValues1= new Map<String,Decimal>();

        segmentVeevaMapping = [select id, Segment_Axtria__c, Segment_Veeva__c, Team_Instance__c from Segment_Veeva_Mapping__c where Team_Instance__c in :allTeamInstances];

        for(Segment_Veeva_Mapping__c svm : segmentVeevaMapping)
        {
            segmentVeevaMappingMap.put(svm.Segment_Axtria__c+ '_' + svm.Team_Instance__c, svm.Segment_Veeva__c);
        }
      
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacpTeam : scopePacpProRecs)
        {
            teamProdAccConcat.add(pacpTeam.AxtriaSalesIQTM__Team_Instance__r.Cycle__c +'&'+pacpTeam.AxtriaSalesIQTM__Account__r.AccountNumber+'&'+pacpTeam.P1__c);
            system.debug('++teamProdAccConcat'+teamProdAccConcat);

        }
        if(scopePacpProRecs[0].AxtriaSalesIQTM__Team_Instance__r.Team_Goals__c=='Individual')
        {
            teamSellValues= Team_Selling.updateIndividualGoals(teamProdAccConcat);
        }
        else if(scopePacpProRecs[0].AxtriaSalesIQTM__Team_Instance__r.Team_Goals__c=='Sum Team')
        {
            teamSellValues= Team_Selling.updateSumTeamGoals(teamProdAccConcat);
            teamSellValues1= Team_Selling.updateMaxTeamGoals(teamProdAccConcat);

        }
        else if(scopePacpProRecs[0].AxtriaSalesIQTM__Team_Instance__r.Team_Goals__c=='Max Team')
        {
            teamSellValues= Team_Selling.updateMaxTeamGoals(teamProdAccConcat);
            teamSellValues1= Team_Selling.updateSumTeamGoals(teamProdAccConcat);

        }
        else if(scopePacpProRecs[0].AxtriaSalesIQTM__Team_Instance__r.Team_Goals__c=='Sum Team+Individual')
        {
            teamSellValues= Team_Selling.updateSumTeamGoals(teamProdAccConcat);
            teamSellValues1= Team_Selling.updateMaxTeamGoals(teamProdAccConcat);

        }
        else if(scopePacpProRecs[0].AxtriaSalesIQTM__Team_Instance__r.Team_Goals__c=='Max Team+Individual')
        {
            teamSellValues= Team_Selling.updateMaxTeamGoals(teamProdAccConcat);
            teamSellValues1= Team_Selling.updateSumTeamGoals(teamProdAccConcat);


        }

        mcCyclePlanProduct = new List<SIQ_MC_Cycle_Plan_Product_vod_O__c>();
        pacpProRecs = scopePacpProRecs;
        Set<String> allRecs = new Set<String>();
        Set<String> allPros = new Set<String>();
        Decimal value;
        Decimal value1;
        
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacpPro : pacpProRecs)
        {
            allPros.add(pacpPro.P1__c);
        }
        
        List<Product_Catalog__c> pc = [select id, Name, Veeva_External_ID__c, Team_Instance__c from Product_Catalog__c where Name  in :allPros and Team_Instance__c = :allTeamInstances and IsActive__c=true];
        
        Map<String, String> proToID = new Map<String,String>();
        
        for(Product_Catalog__c pcRec : pc)
        {
            proToID.put(pcRec.Name.toUpperCase() + '_'+pcRec.Team_Instance__c, pcRec.Veeva_External_ID__c);
        }
        
        
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacpPro : pacpProRecs)
        {
            for(string channel : allChannels)
            { 
                system.debug('+++pacpTeamPro'+pacpPro);
                
                string teamPos = pacpPro.AxtriaSalesIQTM__Team_Instance__r.Name +'_' + pacpPro.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
                string cyclePlan         =   pacpPro.AxtriaSalesIQTM__Team_Instance__r.Name +'_'+pacpPro.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
                string cyclePlanTarget   =   teamPos + '_' + pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber;
                
                String accNumPos = pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber + pacpPro.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
    
                Integer f2fCalls = Integer.valueOf(pacpPro.Final_TCF_Approved__c);
                //String channel = 'F2F';
                String productExternalID= proToID.get(pacpPro.P1__c.toUpperCase() + '_' + pacpPro.AxtriaSalesIQTM__Team_Instance__c);
                String productName = pacpPro.P1__c;
                value = 0;
                value1=0;
                teamProdAccConcat.add(pacpPro.AxtriaSalesIQTM__Team_Instance__r.Cycle__c +'&'+pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber+'&'+pacpPro.P1__c);
                
    
                SIQ_MC_Cycle_Plan_Product_vod_O__c mcpp = new SIQ_MC_Cycle_Plan_Product_vod_O__c();
                            
                string cycleChannel;
                
                string object1 ;
                if(channel.contains('Email')){
                    object1 = 'Sent_Email_vod__c';
                }
                else if(channel.contains('Events')){
                    object1 = 'Multichannel_Activity_vod__c';
                }
                else{
                    object1 = '-Call2_vod__c';
                }
                cycleChannel      =   pacpPro.AxtriaSalesIQTM__Team_Instance__r.Name + '-' + channel + object1;//'-Call2_vod__c';
                
                mcpp.SIQ_Cycle_Plan_Channel_vod__c = teamPos+ '-'  + channel + '-' + pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber;
                
               /* if(channel == 'F2F')
                {
                    mcpp.SIQ_Product_Activity_Goal_vod__c = Decimal.valueof(f2fCalls);    
                }*/
               
                
                mcpp.SIQ_Cycle_Product_vod__c = pacpPro.AxtriaSalesIQTM__Team_Instance__r.Name + '__' + channel + '__' +productExternalID;
    
                //mcpp.SIQ_Cycle_Product_New__c = pacpPro.AxtriaSalesIQTM__Team_Instance__r.Name + '__' + channel + '__' +productExternalID;
                //mcpp.SIQ_Product_Activity_Max_vod__c = mcpp.SIQ_Product_Activity_Goal_vod__c;
                
                if(segmentVeevaMappingMap.containsKey(pacpPro.Segment_Approved__c +'_'+pacpPro.AxtriaSalesIQTM__Team_Instance__c))
                {
                    mcpp.SIQ_Segment_AZ__c   = segmentVeevaMappingMap.get(pacpPro.Segment_Approved__c +'_'+pacpPro.AxtriaSalesIQTM__Team_Instance__c);
                }
                else
                {
                    mcpp.SIQ_Segment_AZ__c   = pacpPro.Segment_Approved__c;
                }
    
                mcpp.SIQ_External_Id_vod__c = mcpp.SIQ_Cycle_Plan_Channel_vod__c + '_'+ productExternalID;
                //mcpp.SIQ_Segment_AZ__c = pacpPro.Segment_Approved__c;
                mcpp.Team_Instance__c = pacpPro.AxtriaSalesIQTM__Team_Instance__c;
                mcpp.External_ID_Axtria__c = mcpp.SIQ_External_Id_vod__c;
                mcpp.Rec_Status__c = 'Updated';
                mcpp.Account__c = pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber;
                mcpp.Position__c = pacpPro.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
    
                //for team selling
                if(pacpPro.AxtriaSalesIQTM__Team_Instance__r.Team_Goals__c=='Individual')
                {
    
                    system.debug('++inidividual'+f2fCalls);
                    system.debug(pacpPro.AxtriaSalesIQTM__Team_Instance__r.Cycle__c+'&'+pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber+'&'+pacpPro.P1__c);
                   
                    mcpp.SIQ_Product_Activity_Goal_vod__c= (Decimal)pacpPro.get(channelMatricMapping.get(channel));//Decimal.valueof(f2fCalls);
                    mcpp.SIQ_Product_Activity_Max_vod__c=(Decimal)pacpPro.get(channelMatricMapping.get(channel));//Decimal.valueof(f2fCalls);
                    
                    system.debug('+++mcpp.SIQ_Product_Activity_Goal_vod__c'+mcpp.SIQ_Product_Activity_Goal_vod__c);
                }
    
                else if(pacpPro.AxtriaSalesIQTM__Team_Instance__r.Team_Goals__c=='Sum Team')
                {
                    system.debug('++inside sum');
                    system.debug(pacpPro.AxtriaSalesIQTM__Team_Instance__r.Cycle__c+'&'+pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber+'&'+pacpPro.P1__c);
                    if(veevaCriteria[0].MCCP_Target_logic__c == 'SUM' || veevaCriteria[0].MCCP_Target_logic__c == 'Sum' || veevaCriteria[0].MCCP_Target_logic__c == 'sum')
                    {
                        if(teamSellValues.containsKey(pacpPro.AxtriaSalesIQTM__Team_Instance__r.Cycle__c+'&'+pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber+'&'+pacpPro.P1__c))
                        {   
                            system.debug('++inside');
                            value=teamSellValues.get(pacpPro.AxtriaSalesIQTM__Team_Instance__r.Cycle__c+'&'+pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber+'&'+pacpPro.P1__c);
                            mcpp.SIQ_Team_Product_Activity_Goal_vod__c= value;
                            mcpp.SIQ_Team_Product_Activity_Max_vod__c=value;  
                            mcpp.SIQ_Product_Activity_Goal_vod__c= 0;
                            mcpp.SIQ_Product_Activity_Max_vod__c=0;
                        }   
                        system.debug('++value'+mcpp.SIQ_Team_Product_Activity_Goal_vod__c);
                    }  
                    else if(veevaCriteria[0].MCCP_Target_logic__c == 'MAX' || veevaCriteria[0].MCCP_Target_logic__c == 'Max' || veevaCriteria[0].MCCP_Target_logic__c == 'max')
                    {
                        if(teamSellValues1.containsKey(pacpPro.AxtriaSalesIQTM__Team_Instance__r.Cycle__c+'&'+pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber+'&'+pacpPro.P1__c))
                        {   
                            system.debug('++yes inside 2');
                            value=teamSellValues1.get(pacpPro.AxtriaSalesIQTM__Team_Instance__r.Cycle__c+'&'+pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber+'&'+pacpPro.P1__c);
                            mcpp.SIQ_Team_Product_Activity_Goal_vod__c= value;
                            mcpp.SIQ_Team_Product_Activity_Max_vod__c=value;  
                            mcpp.SIQ_Product_Activity_Goal_vod__c= 0;
                            mcpp.SIQ_Product_Activity_Max_vod__c=0;
                        }  
                        system.debug('++value'+mcpp.SIQ_Team_Product_Activity_Goal_vod__c);
                    }
                    
                }
                else if(pacpPro.AxtriaSalesIQTM__Team_Instance__r.Team_Goals__c=='Max Team')
                {
                    system.debug('++inside max');
                   if(veevaCriteria[0].MCCP_Target_logic__c == 'MAX' || veevaCriteria[0].MCCP_Target_logic__c == 'Max' || veevaCriteria[0].MCCP_Target_logic__c == 'max')
                   {
                        if(teamSellValues.containsKey(pacpPro.AxtriaSalesIQTM__Team_Instance__r.Cycle__c+'&'+pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber+'&'+pacpPro.P1__c))
                        {   
                            value=teamSellValues.get(pacpPro.AxtriaSalesIQTM__Team_Instance__r.Cycle__c+'&'+pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber+'&'+pacpPro.P1__c);
                            mcpp.SIQ_Team_Product_Activity_Goal_vod__c= value;
                            mcpp.SIQ_Team_Product_Activity_Max_vod__c=value;  
                            mcpp.SIQ_Product_Activity_Goal_vod__c=0;
                            mcpp.SIQ_Product_Activity_Max_vod__c=0;
                        }
                    } 
                    else if(veevaCriteria[0].MCCP_Target_logic__c == 'SUM' || veevaCriteria[0].MCCP_Target_logic__c == 'Sum' || veevaCriteria[0].MCCP_Target_logic__c == 'sum')
                    {
                           system.debug('++inside max-sum');
                        if(teamSellValues1.containsKey(pacpPro.AxtriaSalesIQTM__Team_Instance__r.Cycle__c+'&'+pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber+'&'+pacpPro.P1__c))
                        {   
                            system.debug('++inside max-sum'+teamSellValues1);
    
                            value=teamSellValues1.get(pacpPro.AxtriaSalesIQTM__Team_Instance__r.Cycle__c+'&'+pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber+'&'+pacpPro.P1__c);
                            mcpp.SIQ_Team_Product_Activity_Goal_vod__c= value;
                            mcpp.SIQ_Team_Product_Activity_Max_vod__c=value;  
                            mcpp.SIQ_Product_Activity_Goal_vod__c=0;
                            mcpp.SIQ_Product_Activity_Max_vod__c=0;
                        }
                    }            
                    system.debug('+++mcpp.SIQ_Product_Activity_Goal_vod__c'+mcpp.SIQ_Team_Product_Activity_Goal_vod__c);
                    system.debug('+++mcpp.SIQ_Product_Activity_Goal_vod__c'+mcpp.SIQ_Product_Activity_Goal_vod__c);
    
                }
                else if(pacpPro.AxtriaSalesIQTM__Team_Instance__r.Team_Goals__c=='Sum Team+Individual')
                {
                    if(veevaCriteria[0].MCCP_Target_logic__c == 'SUM' || veevaCriteria[0].MCCP_Target_logic__c == 'Sum' || veevaCriteria[0].MCCP_Target_logic__c == 'sum')
                    {
                       if(teamSellValues.containsKey(pacpPro.AxtriaSalesIQTM__Team_Instance__r.Cycle__c+'&'+pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber+'&'+pacpPro.P1__c))
                        {
                          value=teamSellValues.get(pacpPro.AxtriaSalesIQTM__Team_Instance__r.Cycle__c+'&'+pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber+'&'+pacpPro.P1__c);
                          mcpp.SIQ_Team_Product_Activity_Goal_vod__c =value;
                          mcpp.SIQ_Team_Product_Activity_Max_vod__c= value;
                          
                        } 
                    }
                    else if (veevaCriteria[0].MCCP_Target_logic__c == 'MAX' || veevaCriteria[0].MCCP_Target_logic__c == 'Max' || veevaCriteria[0].MCCP_Target_logic__c == 'max')
                    {
                         if(teamSellValues1.containsKey(pacpPro.AxtriaSalesIQTM__Team_Instance__r.Cycle__c+'&'+pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber+'&'+pacpPro.P1__c))
                        {
                          value=teamSellValues1.get(pacpPro.AxtriaSalesIQTM__Team_Instance__r.Cycle__c+'&'+pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber+'&'+pacpPro.P1__c);
                          mcpp.SIQ_Team_Product_Activity_Goal_vod__c =value;
                          mcpp.SIQ_Team_Product_Activity_Max_vod__c= value;
                        } 
                    }
                        mcpp.SIQ_Product_Activity_Goal_vod__c= Decimal.valueof(f2fCalls);
                        mcpp.SIQ_Product_Activity_Max_vod__c=Decimal.valueof(f2fCalls);
                        system.debug('++value'+mcpp.SIQ_Team_Product_Activity_Max_vod__c);
                        system.debug('++value'+mcpp.SIQ_Product_Activity_Max_vod__c);
                    
                }
                else if(pacpPro.AxtriaSalesIQTM__Team_Instance__r.Team_Goals__c=='Max Team+Individual')
                {
                    if(veevaCriteria[0].MCCP_Target_logic__c == 'MAX' || veevaCriteria[0].MCCP_Target_logic__c == 'Max' || veevaCriteria[0].MCCP_Target_logic__c == 'max')
                     {
                        if(teamSellValues.containsKey(pacpPro.AxtriaSalesIQTM__Team_Instance__r.Cycle__c+'&'+pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber+'&'+pacpPro.P1__c))
                        {
                            value=teamSellValues.get(pacpPro.AxtriaSalesIQTM__Team_Instance__r.Cycle__c+'&'+pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber+'&'+pacpPro.P1__c);
                            mcpp.SIQ_Team_Product_Activity_Goal_vod__c =value;
                            mcpp.SIQ_Team_Product_Activity_Max_vod__c= value;
                        }
                     }
                    
                    else if(veevaCriteria[0].MCCP_Target_logic__c == 'SUM' || veevaCriteria[0].MCCP_Target_logic__c == 'Sum' || veevaCriteria[0].MCCP_Target_logic__c == 'sum')
                    {
                         if(teamSellValues1.containsKey(pacpPro.AxtriaSalesIQTM__Team_Instance__r.Cycle__c+'&'+pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber+'&'+pacpPro.P1__c))
                        {
                          value=teamSellValues1.get(pacpPro.AxtriaSalesIQTM__Team_Instance__r.Cycle__c+'&'+pacpPro.AxtriaSalesIQTM__Account__r.AccountNumber+'&'+pacpPro.P1__c);
                          mcpp.SIQ_Team_Product_Activity_Goal_vod__c =value;
                          mcpp.SIQ_Team_Product_Activity_Max_vod__c= value;
                        } 
                    }
                    mcpp.SIQ_Product_Activity_Goal_vod__c= Decimal.valueof(f2fCalls);
                    mcpp.SIQ_Product_Activity_Max_vod__c=Decimal.valueof(f2fCalls);
                        system.debug('+++mcpp.SIQ_Product_Activity_Goal_vod__c'+mcpp.SIQ_Team_Product_Activity_Goal_vod__c);
                    system.debug('+++mcpp.SIQ_Product_Activity_Goal_vod__c'+mcpp.SIQ_Product_Activity_Goal_vod__c);
                }
            
    
                     
            
                //mcpp.Country__c = pacpPro.r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                if(pacpPro.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c=='No Cluster')
                {
                   mcpp.Country__c = pacpPro.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c ;
                }
                else if(pacpPro.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c=='SalesIQ Cluster')
                {
                   mcpp.Country__c =pacpPro.AxtriaSalesIQTM__Position__r.Original_Country_Code__c;
                }
                else if (pacpPro.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c=='Veeva Cluster')
                {
                    mcpp.Country__c = pacpPro.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c;
                }
    
                //mcpp.Country__c = pacpPro.AxtriaSalesIQTM__Position__r.Original_Country_Code__c;
                mcpp.RecordTypeId__c = veevaCriteria[0].MC_Cycle_Plan_Product_Record_Type__c;
                
                if(!allRecs.contains(mcpp.SIQ_External_Id_vod__c))
                {
                    allRecs.add(mcpp.SIQ_External_Id_vod__c);
                    mcCyclePlanProduct.add(mcpp);
                }
         }        
            }
        system.debug('+++++++++ '+ mcCyclePlanProduct);
        system.debug('+++++++++++++'+mcCyclePlanProduct.size()); 
        
    }
    
    global void execute(Database.BatchableContext BC, List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scopePacpProRecs)
    {
        //List<String> allString = new List<String>();
        create_MC_Cycle_Plan_Product_vod(scopePacpProRecs);
        
        upsert mcCyclePlanProduct External_ID_Axtria__c;
        //insert newTempObj;
    }
    
    global void finish(Database.BatchableContext BC)
    {
    //Database.executeBatch(new MarkMCCPproductDeleted_EU(teamInstanceSelected),2000);
    }
}