global class changeMCTtest implements Database.Batchable<sObject> {
    public String query;

       List<String> allChannels;
    String teamInstanceSelected;
    List<String> allTeamInstances;
    String queryString;
    public Boolean flag = true;
    public Datetime lmd;
    public Boolean chaining = false;
    //List<Parent_PACP__c> pacpRecs;
    
     global changeMCTtest(string teamInstanceSelectedTemp, List<String> allChannelsTemp)
    { 
      /*  //allTeamInstances = new List<String>(teamInstanceSelectedTemp);

       queryString = 'select id, Rec_Status__c from SIQ_MC_Cycle_Plan_Target_vod_O__c where Team_Instance__c = :teamInstanceSelected';
        
        teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;*/
        
    }
    global changeMCTtest(List<string> teamInstanceSelectedTemp, List<String> allChannelsTemp)
    { 
        /*allTeamInstances = new List<String>(teamInstanceSelectedTemp);

       queryString = 'select id, Rec_Status__c from SIQ_MC_Cycle_Plan_Target_vod_O__c where Team_Instance__c in   :allTeamInstances';
        allChannels = allChannelsTemp;*/
        
    }
    global changeMCTtest(Datetime lastjobDate,string teamInstanceSelectedTemp, List<String> allChannelsTemp)
    { 
        //allTeamInstances = new List<String>(teamInstanceSelectedTemp);
         lmd=lastjobDate;
       queryString = 'select id, Rec_Status__c from SIQ_MC_Cycle_Plan_Target_vod_O__c where Team_Instance__c = :teamInstanceSelected';
        
        teamInstanceSelected = teamInstanceSelectedTemp;
        allChannels = allChannelsTemp;
        
    }
    global changeMCTtest(Datetime lastjobDate,List<string> teamInstanceSelectedTemp, List<String> allChannelsTemp)

    { 
        lmd= lastjobDate;
        allTeamInstances = new List<String>(teamInstanceSelectedTemp);

       queryString = 'select id, Rec_Status__c from SIQ_MC_Cycle_Plan_Target_vod_O__c where Team_Instance__c in :allTeamInstances';
        allChannels = allChannelsTemp;
        
    }
    
     global changeMCTtest(Datetime lastjobDate,List<string> teamInstanceSelectedTemp, List<String> allChannelsTemp, Boolean chain)
    { 
        chaining = chain;
        lmd= lastjobDate;
        allTeamInstances = new List<String>(teamInstanceSelectedTemp);

       queryString = 'select id, Rec_Status__c from SIQ_MC_Cycle_Plan_Target_vod_O__c where Team_Instance__c in :allTeamInstances';
        allChannels = allChannelsTemp;
        
    }
    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        return Database.getQueryLocator(queryString);
    }
    
    
    global void execute(Database.BatchableContext BC, List<SIQ_MC_Cycle_Plan_Target_vod_O__c> scopePacpProRecs)
    {

        system.debug('++queryString++'+queryString);
        for(SIQ_MC_Cycle_Plan_Target_vod_O__c mcTarget : scopePacpProRecs)
        {
            mcTarget.Rec_Status__c = '';
        }
        
        update scopePacpProRecs;

    }

    global void finish(Database.BatchableContext BC)
    {
         
            //lmd=Date.Today();
            system.debug('++lmd++'+lmd);     
            BatchDeltaMCTargetStatus u2 = new BatchDeltaMCTargetStatus(lmd,allTeamInstances,allChannels);
            database.executeBatch(u2,2000);
        
        
        
    }
}