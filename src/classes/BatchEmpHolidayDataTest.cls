@isTest
public with sharing class BatchEmpHolidayDataTest {
    public static testMethod void testMethod1() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Name ='USA';
        insert countr;
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc.AxtriaSalesIQTM__Country__c = countr.id;
        acc.AccountNumber = 'BH10461999';
        acc.AxtriaSalesIQTM__AccountType__c = 'HCP';
        acc.Status__c = 'Active';
        acc.Type = 'HCP';
        insert acc;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        insert scen;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Name = 'test';
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        //teamins.AxtriaSalesIQTM__Team_Cycle_Name__c = 'Current';
        //teamins.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        insert teamins;
        
        AxtriaSalesIQTM__Employee__c emp = TestDataFactory.createEmployee('Herry', 'Dsouza');
        emp.AxtriaSalesIQTM__Employee_ID__c ='f1234';
        insert emp;
        AxtriaSalesIQTM__Employee__c emp1 = TestDataFactory.createEmployee('Herdfdry', 'Dsouggza');
        emp1.AxtriaSalesIQTM__Employee_ID__c ='f123434';
        insert emp1;
        AxtriaSalesIQTM__Employee__c emp2 = TestDataFactory.createEmployee('Herddry', 'Dsouggza');
        emp2.AxtriaSalesIQTM__Employee_ID__c ='f12cvvb34';
        insert emp2;
        
        
        Emp_Holiday_Data__c empData1 = new Emp_Holiday_Data__c();
        empData1.PRID__c= emp.AxtriaSalesIQTM__Employee_ID__c;
        empData1.Holiday_Date__c = Date.today();
        empData1.Name__c = 'Christmas Day 2018';
        empData1.Employee_ID__c = emp.Id;
        insert empData1;
        
        Emp_Holiday_Data__c empData2 = new Emp_Holiday_Data__c();
        empData2.PRID__c= emp1.AxtriaSalesIQTM__Employee_ID__c;
        empData2.Holiday_Date__c = Date.today();
        empData2.Name__c = 'Good Friday 2018';
        empData2.Employee_ID__c =emp1.Id;
        insert empData2;
        
        Emp_Holiday_Data__c empData3 = new Emp_Holiday_Data__c();
        empData3.PRID__c= emp2.AxtriaSalesIQTM__Employee_ID__c;
        empData3.Holiday_Date__c = Date.today();
        empData3.Name__c = 'Boxing Day 2018';
        empData3.Employee_ID__c = emp2.Id;
        insert empData3;
        
        
        
        
        
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;        
        
        AxtriaSalesIQTM__Position_Employee__c posEmp = new AxtriaSalesIQTM__Position_Employee__c();
        posEmp.AxtriaSalesIQTM__Employee__c = emp.id;
        //posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c = 'POS123';
        //posEmp.Team_Instance__c = teamIns.id;
        posEmp.AxtriaSalesIQTM__Assignment_Type__c = 'Primary';
        insert posEmp;
        
        AxtriaSalesIQTM__Position_Employee__c posEmp1 = new AxtriaSalesIQTM__Position_Employee__c();
        posEmp.AxtriaSalesIQTM__Employee__c = emp1.id;
        //posEmp1.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c = 'POS345';
        //posEmp1.Team_Instance__c = teamIns.id;
        posEmp1.AxtriaSalesIQTM__Assignment_Type__c = 'Primary';
        insert posEmp1;
        
        AxtriaSalesIQTM__Position_Employee__c posEmp2 = new AxtriaSalesIQTM__Position_Employee__c();
        posEmp.AxtriaSalesIQTM__Employee__c = emp2.id;
        //posEmp2.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c = 'POS987';
        //posEmp2.Team_Instance__c = teamIns.id;
        posEmp2.AxtriaSalesIQTM__Assignment_Type__c = 'Primary';
        insert posEmp2;
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            BatchEmpHolidayData obj=new BatchEmpHolidayData(teamins.Id);
            Database.executeBatch(obj);
            
        }
        Test.stopTest();
        
        
    }
    public static testMethod void testMethod2() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Name ='USA';
        insert countr;
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc.AxtriaSalesIQTM__Country__c = countr.id;
        acc.AccountNumber = 'BH10461999';
        acc.AxtriaSalesIQTM__AccountType__c = 'HCP';
        acc.Status__c = 'Active';
        acc.Type = 'HCP';
        insert acc;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        insert scen;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Name = 'test';
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        //teamins.AxtriaSalesIQTM__Team_Cycle_Name__c = 'Current';
        //teamins.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        insert teamins;
        Product_Catalog__c pc = new Product_Catalog__c();
        pc.Name='BRILIQUE';
        pc.Team_Instance__c = teamins.Id;
        pc.Veeva_External_ID__c ='ProdId';
        pc.Product_Code__c= 'ProdId';
        pc.IsActive__c=true;
        pc.Country_Lookup__c=countr.id;
        insert pc;
        AxtriaSalesIQTM__Employee__c emp = TestDataFactory.createEmployee('Herry', 'Dsouza');
        emp.AxtriaSalesIQTM__Employee_ID__c ='f1234';
        insert emp;
        AxtriaSalesIQTM__Employee__c emp1 = TestDataFactory.createEmployee('Herdfdry', 'Dsouggza');
        emp1.AxtriaSalesIQTM__Employee_ID__c ='f123434';
        insert emp1;
        AxtriaSalesIQTM__Employee__c emp2 = TestDataFactory.createEmployee('Herddry', 'Dsouggza');
        emp2.AxtriaSalesIQTM__Employee_ID__c ='f12cvvb34';
        insert emp2;
        
        
        Emp_Holiday_Data__c empData1 = new Emp_Holiday_Data__c();
        empData1.PRID__c= emp.AxtriaSalesIQTM__Employee_ID__c;
        empData1.Holiday_Date__c = Date.today();
        empData1.Name__c = 'Christmas Day 2018';
        empData1.Employee_ID__c = emp.Id;
        insert empData1;
        
        Emp_Holiday_Data__c empData2 = new Emp_Holiday_Data__c();
        empData2.PRID__c= emp1.AxtriaSalesIQTM__Employee_ID__c;
        empData2.Holiday_Date__c = Date.today();
        empData2.Name__c = 'Good Friday 2018';
        empData2.Employee_ID__c =emp1.Id;
        insert empData2;
        
        Emp_Holiday_Data__c empData3 = new Emp_Holiday_Data__c();
        empData3.PRID__c= emp2.AxtriaSalesIQTM__Employee_ID__c;
        empData3.Holiday_Date__c = Date.today();
        empData3.Name__c = 'Boxing Day 2018';
        empData3.Employee_ID__c = emp2.Id;
        insert empData3;
        
        
        
        
        
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;        
        
        AxtriaSalesIQTM__Position_Employee__c posEmp = new AxtriaSalesIQTM__Position_Employee__c();
        posEmp.AxtriaSalesIQTM__Employee__c = emp.id;
        //posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c = 'POS123';
        //posEmp.Team_Instance__c = teamIns.id;
        posEmp.AxtriaSalesIQTM__Assignment_Type__c = 'Primary';
        insert posEmp;
        
        AxtriaSalesIQTM__Position_Employee__c posEmp1 = new AxtriaSalesIQTM__Position_Employee__c();
        posEmp.AxtriaSalesIQTM__Employee__c = emp1.id;
        //posEmp1.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c = 'POS345';
        //posEmp1.Team_Instance__c = teamIns.id;
        posEmp1.AxtriaSalesIQTM__Assignment_Type__c = 'Primary';
        insert posEmp1;
        
        AxtriaSalesIQTM__Position_Employee__c posEmp2 = new AxtriaSalesIQTM__Position_Employee__c();
        posEmp.AxtriaSalesIQTM__Employee__c = emp2.id;
        //posEmp2.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c = 'POS987';
        //posEmp2.Team_Instance__c = teamIns.id;
        posEmp2.AxtriaSalesIQTM__Assignment_Type__c = 'Primary';
        insert posEmp2;
        
        Emp_holiday_code__c e = new Emp_holiday_code__c();
        e.Team_instance__c = teamins.id;
        e.Position_code__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        e.Holiday__c ='1';
        e.Team_instance_Lookup__c = teamins.id;
        insert e ;
        
        AxtriaSalesIQTM__Position_Product__c p = new AxtriaSalesIQTM__Position_Product__c();
        p.Business_Days_in_Cycle__c=5;
        p.Calls_Day__c = 3;
        p.AxtriaSalesIQTM__Position__c = pos.id;
        p.Product_Catalog__c = pc.id;
        p.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        p.External_ID__c = pos.id + '_' + pc.id;
        p.Holidays__c = 1.0;
        p.AxtriaSalesIQTM__Effective_Start_Date__c = system.today();
        p.AxtriaSalesIQTM__Effective_End_Date__c = system.today();
        p.AxtriaSalesIQTM__Product_Weight__c = 3;
        insert p;
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            PopulatePositionProductHoliday obj=new PopulatePositionProductHoliday(teamins.Id);
            Database.executeBatch(obj);
            
        }
        Test.stopTest();
        
        
    }
}