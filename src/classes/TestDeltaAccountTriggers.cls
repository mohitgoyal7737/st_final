@isTest
public class TestDeltaAccountTriggers {
    public static testMethod void testDeltaUpdates()
    {
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        Account testAcc = new Account(name = 'testAccount',Marketing_Code__c = '0001');
        insert testAcc;
        
        AxtriaSalesIQTM__Affiliation_Network__c affNw = new AxtriaSalesIQTM__Affiliation_Network__c(Name = 'test aff');
        insert affNw;
        
        AxtriaSalesIQTM__Account_Address__c accountAddressRecord = new AxtriaSalesIQTM__Account_Address__c(AxtriaSalesIQTM__Account__c = testAcc.id, AxtriaSalesIQTM__Is_Primary__c = false);
        insert accountAddressRecord;
        AxtriaSalesIQTM__Account_Affiliation__c accountaffilationRecord = new AxtriaSalesIQTM__Account_Affiliation__c(AxtriaSalesIQTM__Affiliation_Network__c = affNw.Id ,AxtriaSalesIQTM__Parent_Account__c = testAcc.id,AxtriaSalesIQTM__Account__c = testAcc.id,AxtriaSalesIQTM__Is_Primary__c = false);
        insert accountaffilationRecord;
        AxtriaSalesIQTM__Position_Account__c positionAccountRecord = new AxtriaSalesIQTM__Position_Account__c(AxtriaSalesIQTM__Account__c = testAcc.id,AxtriaSalesIQTM__Affiliation_Based_Alignment__c = false);
        insert positionAccountRecord;

    }

}