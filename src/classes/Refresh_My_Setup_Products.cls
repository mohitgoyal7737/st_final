global with sharing class Refresh_My_Setup_Products implements Database.Batchable<sObject>, Schedulable {
    public String query;
    List<String> allCountries;
    public Boolean chaining;

    global Refresh_My_Setup_Products(List<String> allTeamInstances, Boolean chain) 
    {
        //allCountries = allCountriesTemp;
        List<AxtriaSalesIQTM__Team_Instance__c> allCount = [select AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Team_Instance__c where id in :allTeamInstances];
        allCountries = new List<String>();
        
        for(AxtriaSalesIQTM__Team_Instance__c cc : allCount)
        {
            allCountries.add(cc.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c);
        }
        chaining = chain;
        this.query = 'select id from SIQ_My_Setup_Products_vod_O__c where SIQ_Country__c in :allCountries ';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<SIQ_My_Setup_Products_vod_O__c> scope) 
    {

        for(SIQ_My_Setup_Products_vod_O__c msp : scope)
        {
            msp.Status__c = 'Deleted';
        }
        //update scope;
        SnTDMLSecurityUtil.updateRecords(scope, 'Refresh_My_Setup_Products');
    }

    global void finish(Database.BatchableContext BC) {

        list<string> Teaminstancelistis = new list<string>();
        teaminstancelistis = StaticTeaminstanceList.getFullLoadTeamInstances();
        database.executeBatch(new MySetUpProd_Insert_Utility(teaminstancelistis, chaining), 2000);
    }

     global void execute(System.SchedulableContext SC){
        list<string> teaminstancelistis = new list<string>();
        teaminstancelistis = StaticTeaminstanceList.getAllCountries();
        database.executeBatch(new Refresh_My_Setup_Products(teaminstancelistis, false), 2000);
    } 
}