global class FillMergeAccountNumberBatch implements Database.Batchable<sObject> {
    public String query;
    public Set<String> countrySet;

    global FillMergeAccountNumberBatch() {
        this.query = query;
        query='Select id,Merge_Account_Number__c,AccountNumber,Country_Code__c from Account where AccountNumber != null and Country_Code__c != null';
    }

    global FillMergeAccountNumberBatch(Set<String> country) {
        this.query = query;
        countrySet = new Set<String>();
        countrySet.addAll(country);
        query='Select id,Merge_Account_Number__c,AccountNumber,Country_Code__c from Account where AccountNumber != null and Country_Code__c != null and AxtriaSalesIQTM__Country__c in :countrySet';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Account> scope) {

        for(Account acc : scope)
        {
            acc.Merge_Account_Number__c = acc.AccountNumber + '_' + acc.Country_Code__c;
        }

        update scope;
        
    }

    global void finish(Database.BatchableContext BC) {

    }
}