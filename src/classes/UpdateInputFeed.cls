public with sharing class UpdateInputFeed {
    public UpdateInputFeed() {
        
    }
    Public static void UpdateData(List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>CallPlan1, list<String> Teaminstancename){
    	String teaminstance = Teaminstancename[0];
    	map<String,String> pacpapivalue = new map<string,string>();
    	set<String> Accounid = new set<String>();
        set<Id>pacpid = new set<Id>();
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c p : CallPlan1){
            pacpid.add(p.id);
        }
        
       // list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>CallPlan =[select id,parameter1__c,AxtriaSalesIQTM__Account__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where id IN: CallPlan1];
        
       /* String Soql = 'Select id,AxtriaSalesIQTM__Account__c, ';
        List<AxtriaSalesIQTM__Team_Instance_Object_Attribute__c> fetchAllRequiredColumns;
        fetchAllRequiredColumns = [select AxtriaSalesIQTM__Attribute_API_Name__c, Brand_Team_Instance__c,AxtriaSalesIQTM__WrapperFieldMap__c  from AxtriaSalesIQTM__Team_Instance_Object_Attribute__c where AxtriaSalesIQTM__isRequired__c = true and AxtriaSalesIQTM__Team_Instance__r.Name =:teaminstance and AxtriaSalesIQTM__Interface_Name__c = 'Call Plan' and  Brand_Team_Instance__c!=null];
        for(AxtriaSalesIQTM__Team_Instance_Object_Attribute__c tobj : fetchAllRequiredColumns){
            Soql+=tobj.AxtriaSalesIQTM__Attribute_API_Name__c+',';
        }
        */
        

        String Soql = 'Select id,AxtriaSalesIQTM__Account__c,';
    	Map<String, String> ParamFieldMap = new Map<String, String>();
        for(AxtriaSalesIQTM__Team_Instance_Object_Attribute__c tioa: [select AxtriaSalesIQTM__Attribute_API_Name__c, AxtriaSalesIQTM__Attribute_Display_Name__c from AxtriaSalesIQTM__Team_Instance_Object_Attribute__c where AxtriaSalesIQTM__isEnabled__c = true and AxtriaSalesIQTM__Team_Instance__r.Name =:teaminstance and AxtriaSalesIQTM__Interface_Name__c = 'Call Plan'  AND Brand_team_instance__c!=null]){ //AND Line__c =: rule.Line_2__c
            ParamFieldMap.put(tioa.AxtriaSalesIQTM__Attribute_Display_Name__c, tioa.AxtriaSalesIQTM__Attribute_API_Name__c);
            Soql+=tioa.AxtriaSalesIQTM__Attribute_API_Name__c+',';
        }

        Soql = Soql.removeEnd(',');
        soql = Soql+' from AxtriaSalesIQTM__Position_Account_Call_Plan__c where id IN :pacpid';
        System.debug('=========SOQL IS :'+Soql);
        
        list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> CallPlanRecs =   Database.Query(Soql);

        for(String Dspname : ParamFieldMap.keySet()){
        	String Apiname = ParamFieldMap.get(Dspname);

        	System.debug('=====DSPNAME==:'+Dspname+'===Apiname===:'+Apiname);
        	for(Sobject pacp : CallPlanRecs){
        		Accounid.add((String)pacp.get('AxtriaSalesIQTM__Account__c'));
        		String Account = (String)pacp.get('AxtriaSalesIQTM__Account__c');
        		String value = (string)pacp.get(Apiname);
        		String Key = Account+'_'+Dspname.toUpperCase();
                System.debug('=======VALUE ====:'+value);
                System.debug('=======Account ====:'+Account);

        		if(!pacpapivalue.containsKey(Key) ){ //&& value!=null && value!=''
        			pacpapivalue.put(Key, value);
        			System.debug('========Inside if loop==');
        		}

        	}
        }
        System.debug('=====pacpapivalue=====:'+pacpapivalue);
        System.debug('=====pacpapivalue.Size=====:'+pacpapivalue.size());

        List<Survey_Response__c> Surveydata = [select id,Question_Short_Text__c,Physician__c,Response__c from Survey_Response__c where Physician__c  IN :Accounid and Team_Instance__r.name =:teaminstance ];
        list<Rating_Response__c> Ratingdata = [select id,Physician__c,Rating_Attribute_Short__c,Response__c from Rating_Response__c where Physician__c  IN :Accounid and Team_Instance__r.name =:teaminstance];

        map<String,Map<String,String>> Surveymap = new map<String,Map<String,String>>();
        map<String,Map<String,String>> Ratingmap = new map<String,Map<String,String>>();
        
        /*for(Survey_Response__c sr : Surveydata){
        	map<String,String> submap = new map<String,String>();
        	String Key = sr.Physician__c+'_'+((String)sr.Question_Short_Text__c).toUpperCase();
        	String value = sr.Response__c;
        	if(!Surveymap.containsKey(sr.id) && !submap.containsKey(Key)){
        		submap.put(Key,value);
        		Surveymap.put(sr.id,submap);	
        	}
        }

        for(Rating_Response__c RR : Ratingdata){
        	map<String,String> submap = new map<String,String>();
        	String Key = RR.Physician__c+'_'+((String)RR.Rating_Attribute_Short__c).toUpperCase();
        	String value = RR.Response__c;
        	if(!Ratingmap.containsKey(RR.id) && !submap.containsKey(Key)){
        		submap.put(Key,value);
        		Ratingmap.put(RR.id,submap);	
        	}
        }


        System.debug('======Surveymap===:'+Surveymap);
        System.debug('======Ratingmap===:'+Ratingmap);
        */
        System.debug('=====pacpapivalue.keySet()=====:'+pacpapivalue.keySet());

        list<Survey_Response__c>SurveyList = new list<Survey_Response__c>();
        for(Survey_Response__c SR : Surveydata){
        	String Key = SR.Physician__c+'_'+((String)SR.Question_Short_Text__c).toUpperCase();
        	System.debug('======NEW KEY===='+Key);
        	Survey_Response__c SurRes = new Survey_Response__c();
        	SurRes.id = SR.id;
        	if(pacpapivalue.containsKey(Key)){
        		SurRes.Response__c = pacpapivalue.get(Key);
        	}
        	SurveyList.add(SurRes);
        }

        list<Rating_Response__c>RatingList = new list<Rating_Response__c>();
        for(Rating_Response__c RR : Ratingdata){
        	String Key = RR.Physician__c+'_'+((String)RR.Rating_Attribute_Short__c).toUpperCase();
        	System.debug('======NEW KEY===='+Key);
        	Rating_Response__c RatRes = new Rating_Response__c();
        	RatRes.id = RR.id;
        	if(pacpapivalue.containsKey(Key)){
        		RatRes.Response__c = pacpapivalue.get(Key);
        	}
        	RatingList.add(RatRes);
        }

        System.debug('========SurveyList====:'+SurveyList);
        System.debug('========RatingList====:'+RatingList);

        try{
        	Update SurveyList;
        	Update RatingList;
        }
        catch(Exception E){
        	System.debug('==========EXCEPTION CAUGHT');

        }

    }

}