@istest

public class UpdateLevel5PositionTEST 
{
 static testmethod void updateLevel5fieldTEST()
{
    AxtriaSalesIQTM__Organization_Master__c OM = new AxtriaSalesIQTM__Organization_Master__c();
    OM.Name='Dummy1';
    OM.AxtriaSalesIQTM__Org_Level__c='Global';
    OM.AxtriaARSnT__Marketing_Code__c='ES';
    OM.AxtriaSalesIQTM__Parent_Country_Level__c=TRUE;
    insert OM;
    
    AxtriaSalesIQTM__Country__c CON = new AxtriaSalesIQTM__Country__c();
    CON.AxtriaSalesIQTM__Country_Code__c='ES';
    CON.AxtriaSalesIQTM__Status__c='ACTIVE';
    CON.AxtriaSalesIQTM__Parent_Organization__c=OM.id;
    insert CON;

AxtriaSalesIQTM__Team__c TN = new AxtriaSalesIQTM__Team__c();
TN.Name='ABC';
TN.AxtriaSalesIQTM__Effective_Start_Date__c=system.today()-1;
TN.AxtriaSalesIQTM__Effective_Start_Date__c=system.today();
TN.AxtriaSalesIQTM__Alignment_Type__c='Account';
TN.AxtriaSalesIQTM__Team_Code__c='TE01111';
TN.AxtriaSalesIQTM__Country__c=CON.id;
TN.AxtriaSalesIQTM__Type__c='BASE';
    insert TN;
    
    AxtriaSalesIQTM__Team_Instance__c  TI = new AxtriaSalesIQTM__Team_Instance__c ();
    TI.Name='ESABCD';
    TI.AxtriaSalesIQTM__Team__c=TN.id;
    insert TI;
    
    list<AxtriaSalesIQTM__Position__c> poslist = new list<AxtriaSalesIQTM__Position__c >();
    
    AxtriaSalesIQTM__Position__c POS = new AxtriaSalesIQTM__Position__c();
    
    POS.Name='Position1';
    POS.AxtriaSalesIQTM__Team_iD__c=TN.id;
   POS.AxtriaSalesIQTM__Level_5_Position__c='1';
    POS.AxtriaSalesIQTM__Hierarchy_Level__c='6';
    POS.AxtriaSalesIQTM__Team_Instance__c=TI.id;
    
    insert POS;
    
      AxtriaSalesIQTM__Position__c POS1 = new AxtriaSalesIQTM__Position__c();
    
    POS1.Name='Position2';
    POS1.AxtriaSalesIQTM__Team_iD__c=TN.id;
   POS1.AxtriaSalesIQTM__Level_5_Position__c='5';
    POS1.AxtriaSalesIQTM__Hierarchy_Level__c='5';
    POS1.AxtriaSalesIQTM__Parent_Position__c = POS.id;
    POS1.AxtriaSalesIQTM__Team_Instance__c=TI.id;
    
    insert POS1;
    
      AxtriaSalesIQTM__Position__c POS2 = new AxtriaSalesIQTM__Position__c();
    
    POS2.Name='Position3';
    POS2.AxtriaSalesIQTM__Team_iD__c=TN.id;
   POS2.AxtriaSalesIQTM__Level_5_Position__c='5';
    POS2.AxtriaSalesIQTM__Hierarchy_Level__c='4';
    POS2.AxtriaSalesIQTM__Parent_Position__c = POS1.id;
    POS2.AxtriaSalesIQTM__Team_Instance__c=TI.id;
    insert POS2;
    
       AxtriaSalesIQTM__Position__c POS3 = new AxtriaSalesIQTM__Position__c();
    
    POS3.Name='Position4';
    POS3.AxtriaSalesIQTM__Team_iD__c=TN.id;
   POS3.AxtriaSalesIQTM__Level_5_Position__c='1';
    POS3.AxtriaSalesIQTM__Hierarchy_Level__c='4';
    POS3.AxtriaSalesIQTM__Parent_Position__c = POS2.id;
    POS3.AxtriaSalesIQTM__Team_Instance__c=TI.id;
    
    insert POS3;
    
      AxtriaSalesIQTM__Position__c POS4 = new AxtriaSalesIQTM__Position__c();
    
    POS4.Name='Position5';
    POS4.AxtriaSalesIQTM__Team_iD__c=TN.id;
   POS4.AxtriaSalesIQTM__Level_5_Position__c='3';
    POS4.AxtriaSalesIQTM__Hierarchy_Level__c='3';
    POS4.AxtriaSalesIQTM__Parent_Position__c = POS3.id;
    POS4.AxtriaSalesIQTM__Team_Instance__c=TI.id;
    insert POS4;
    
      AxtriaSalesIQTM__Position__c POS5 = new AxtriaSalesIQTM__Position__c();
    
    POS5.Name='Position6';
    POS5.AxtriaSalesIQTM__Team_iD__c=TN.id;
   POS5.AxtriaSalesIQTM__Level_5_Position__c='2';
    POS5.AxtriaSalesIQTM__Hierarchy_Level__c='2';
    POS5.AxtriaSalesIQTM__Parent_Position__c = POS4.id;
    POS5.AxtriaSalesIQTM__Team_Instance__c=TI.id;
    
    insert POS5;
    
    AxtriaSalesIQTM__Position__c POS6 = new AxtriaSalesIQTM__Position__c();
    
    POS6.Name='Position7';
    POS6.AxtriaSalesIQTM__Team_iD__c=TN.id;
   POS6.AxtriaSalesIQTM__Level_5_Position__c='1';
    POS6.AxtriaSalesIQTM__Hierarchy_Level__c='1';
    POS6.AxtriaSalesIQTM__Team_Instance__c=TI.id;
    
    insert POS6;
    
    Test.startTest();
    UpdateLevel5Position  up = new UpdateLevel5Position ();
    up.updateLevel5field(TI.id);
    Test.stopTest();
    
    
 }
}