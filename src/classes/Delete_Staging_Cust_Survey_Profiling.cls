global with sharing class Delete_Staging_Cust_Survey_Profiling implements Database.Batchable<sObject>,Schedulable
{
    public String query;
    public String teamInstance;
    public String type;
    public boolean scheduledJobCall=false;
    public Set<string> teaminstancelist;
    public Set<String> productList;
    public list<String> teamInst_ProdList; //Added by dhiren
    public String teamInstance_Prod;//Added by dhiren
    public String product;//Added by dhiren
    public Boolean ondemand = false; //Added by dhiren

    global Delete_Staging_Cust_Survey_Profiling(String teamInstance,String type)
    {   
        this.teamInstance = teamInstance;
        this.type=type;
        scheduledJobCall=false;
        query='select id from Staging_Cust_Survey_Profiling__c where Team_Instance__c = \''+teamInstance+'\' WITH SECURITY_ENFORCED';
    }
    global Delete_Staging_Cust_Survey_Profiling()
    {   
        this.teamInstance = teamInstance;
        this.type=type;
        scheduledJobCall=true;
        teaminstancelist= new Set<String>();
        productList= new Set<String>();
        teaminstancelist= StaticTeaminstanceList.getCompleteRuleTeamInstances();
        system.debug('+++teaminstancelist'+teaminstancelist);
    List<Product_Catalog__c> product= [Select id,name From Product_Catalog__c where Team_Instance__c in:teaminstancelist and IsActive__c=true WITH SECURITY_ENFORCED] ;

        for(Product_Catalog__c pc: product)
        {
            productList.add(pc.name);
        }
         system.debug('++productList'+productList);

        query='select id from Staging_Cust_Survey_Profiling__c where BRAND_NAME__c not in:productList WITH SECURITY_ENFORCED';
    }

     //<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Added by Dhiren <><><>>>>>>>>>>>>>>>>>><>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    global Delete_Staging_Cust_Survey_Profiling(String teamInstanceProd)
    {   

        teamInstance_Prod = teamInstanceProd;
        System.debug('<><><><><><><><>'+teamInstance_Prod);
        teamInst_ProdList=new list<String>();
        if(teamInstance_Prod.contains(';'))
        {
            teamInst_ProdList=teamInstance_Prod.split(';');
        }
        
        system.debug('<><>teamInst_ProdList<><<>'+teamInst_ProdList);
        teamInstance = teamInst_ProdList[0];
        product = teamInst_ProdList[1];
        scheduledJobCall=false;
        ondemand = true;

        system.debug('<>>>>>>><>teamInstance<><><><><><><> '+teamInstance+'  <>>>>>>>>product>>>>>>>>>> '+product);
        query='select id from Staging_Cust_Survey_Profiling__c where BRAND_ID__c =: product WITH SECURITY_ENFORCED ';
    }
    //<><><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<End Of Code <><><<<<<<<<<><><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        System.debug('query-->'+query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        System.debug('scope size-->'+scope.size());
        if(scope!=null && scope.size()>0)
        {
            //delete scope;
            SnTDMLSecurityUtil.deleteRecords(scope, 'Delete_Staging_Cust_Survey_Profiling');
            //Database.emptyRecycleBin(scope);  
        }
    }

    global void finish(Database.BatchableContext BC)
    {   
        //commented due to object purge activity A1450
        // if(scheduledJobCall)
        // {
        //    Delete_Survey_Response batch = new Delete_Survey_Response();
        //    Database.executeBatch(batch,500);
        // }
        // else
        // {
        //     if(ondemand)
        //     {
        //         Database.executeBatch(new Delete_Survey_Response(teamInstance_Prod),500);
        //     }

        //     else
        //     {
        //         Delete_Survey_Response batch = new Delete_Survey_Response(teamInstance,type);
        //         Database.executeBatch(batch,500);
        //     }
        // }

        //Changed due to purging
        if(scheduledJobCall)
        {
            Delete_Metadata_Definition batch = new Delete_Metadata_Definition();
            database.executebatch(batch,500);
        } 
        else
        {
            if(ondemand)
            {
                Delete_Metadata_Definition batch = new Delete_Metadata_Definition(teamInstance_Prod);
                Database.executeBatch(batch,500);
            }

            else
            {
                Delete_Metadata_Definition batch = new Delete_Metadata_Definition(teamInstance,type);
                Database.executeBatch(batch,500);
            }
        }
    }
    global void execute(SchedulableContext sc)
    {
        database.executeBatch(new Delete_Staging_Cust_Survey_Profiling(),200);
    }
}