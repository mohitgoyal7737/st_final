/**********************************************************************************************
Author      : Rashmi Tyagi
Date        : 5th October 2020
Description : Test class for Insertprofileparam
Revision(s) : v1.0
**********************************************************************************************/
@isTest
private class InsertprofileparamTest {
   static testMethod void testMethod1() {
        String classname = 'InsertprofileparamTest';
        User loggedInUser = new User(id=UserInfo.getUserId());
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Name ='USA';
        SnTDMLSecurityUtil.insertRecords(countr,className);
        Account a1= TestDataFactory.createAccount();
        a1.AxtriaSalesIQTM__Country__c = countr.id;
        a1.AccountNumber = 'BH10477999';
        SnTDMLSecurityUtil.insertRecords(a1,className);

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        SnTDMLSecurityUtil.insertRecords(team,className);
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins1,className);
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        SnTDMLSecurityUtil.insertRecords(scen,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Name = 'test';
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        
        SnTDMLSecurityUtil.insertRecords(teamins,className);
              
        Product_Catalog__c pc = TestDataFactory.productCatalog(team, teamins, countr);
        
        SnTDMLSecurityUtil.insertRecords(pc,className);
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        SnTDMLSecurityUtil.insertRecords(mmc,className);
        
        AxtriaSalesIQTM__Position__c parentPos= TestDataFactory.createPosition(team,teamins);
        parentPos.AxtriaSalesIQTM__Hierarchy_Level__c = '3';
        SnTDMLSecurityUtil.insertRecords(parentPos,className);
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__Parent_Position__c = parentPos.id;
        pos.AxtriaSalesIQTM__Hierarchy_Level__c = '2';
        SnTDMLSecurityUtil.insertRecords(pos,className);
        
        
        AxtriaSalesIQTM__Position_Product__c posp = new AxtriaSalesIQTM__Position_Product__c();
        posp.Business_Days_in_Cycle__c=5;
        posp.Calls_Day__c = 3;
        posp.AxtriaSalesIQTM__Position__c = pos.id;
        posp.Product_Catalog__c = pc.id;
        posp.External_ID__c = pos.id + '_' + pc.id;
        posp.AxtriaSalesIQTM__Effective_Start_Date__c = system.today();
        posp.AxtriaSalesIQTM__Effective_End_Date__c = system.today();
        posp.AxtriaSalesIQTM__Product_Weight__c = 3;
        SnTDMLSecurityUtil.insertRecords(posp,className);
        
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(a1,pos,teamins);
        SnTDMLSecurityUtil.insertRecords(posAccount,className);
          
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        pPriority.Type__c = 'Specialty';
        pPriority.Product__c = pc.id;
        pPriority.Priority__c ='P1';
        pPriority.TeamInstance__c = teamins.Id;
        
        SnTDMLSecurityUtil.insertRecords(pPriority,className);
        System.assertEquals(pPriority.Type__c,'Specialty');
        
        AxtriaSalesIQTM__Team_Instance_Object_Attribute__c teamInsOA = TestDataFactory.createTeamInstanceObjectAttribute(teamins,pc);
        teamInsOA.AxtriaSalesIQTM__Interface_Name__c ='Call Plan';
        teamInsOA.NoCustomLabel__c = true;
        SnTDMLSecurityUtil.insertRecords(teamInsOA,className);
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,a1,teamins,posAccount,pPriority,pos);
        positionAccountCallPlan.Final_TCF__c = 10;
        positionAccountCallPlan.P1__c = pc.name;
        positionAccountCallPlan.p1_original__c = pc.name;
        positionAccountCallPlan.Final_TCF_Original__c = 10;
        positionAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c = true;
         
        SnTDMLSecurityUtil.insertRecords(positionAccountCallPlan,className);
        
        Field_Api_mapping__c fieldAPI = new Field_Api_mapping__c(Field_Api__c ='P2_Parameter1__c',Priority__c='2',wrapper__c='param2');
        SnTDMLSecurityUtil.insertRecords(fieldAPI,className);
         
         Test.startTest();
         System.runAs(loggedInUser){
             string teaminstance = teamins.Id;
             Map<String,Set<String>> qaParam = new Map<String,Set<String>>();
             Set<String> strSet = new Set<String>();
             strSet.add('NONSPECIALTY');
             strSet.add('Product');
             qaParam.put('NONSPECIALTY',strSet);
             String prod = pc.id;
             Insertprofileparam.Insertprofileparamvalues(teaminstance,qaParam,prod);
             
          }
        
         Test.stopTest();
     }
     
static testMethod void testMethod2() {
        String classname = 'InsertprofileparamTest';
        User loggedInUser = new User(id=UserInfo.getUserId());
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Name ='USA';
        SnTDMLSecurityUtil.insertRecords(countr,className);
        Account a1= TestDataFactory.createAccount();
        a1.AxtriaSalesIQTM__Country__c = countr.id;
        a1.AccountNumber = 'BH10477999';
        SnTDMLSecurityUtil.insertRecords(a1,className);

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        SnTDMLSecurityUtil.insertRecords(team,className);
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins1,className);
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        SnTDMLSecurityUtil.insertRecords(scen,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Name = 'test';
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        
        SnTDMLSecurityUtil.insertRecords(teamins,className);
              
        Product_Catalog__c pc = TestDataFactory.productCatalog(team, teamins, countr);
        
        SnTDMLSecurityUtil.insertRecords(pc,className);
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        SnTDMLSecurityUtil.insertRecords(mmc,className);
        
        AxtriaSalesIQTM__Position__c parentPos= TestDataFactory.createPosition(team,teamins);
        parentPos.AxtriaSalesIQTM__Hierarchy_Level__c = '3';
        SnTDMLSecurityUtil.insertRecords(parentPos,className);
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__Parent_Position__c = parentPos.id;
        pos.AxtriaSalesIQTM__Hierarchy_Level__c = '2';
        SnTDMLSecurityUtil.insertRecords(pos,className);
        
        
        AxtriaSalesIQTM__Position_Product__c posp = new AxtriaSalesIQTM__Position_Product__c();
        posp.Business_Days_in_Cycle__c=5;
        posp.Calls_Day__c = 3;
        posp.AxtriaSalesIQTM__Position__c = pos.id;
        posp.Product_Catalog__c = pc.id;
        posp.External_ID__c = pos.id + '_' + pc.id;
        posp.AxtriaSalesIQTM__Effective_Start_Date__c = system.today();
        posp.AxtriaSalesIQTM__Effective_End_Date__c = system.today();
        posp.AxtriaSalesIQTM__Product_Weight__c = 3;
        SnTDMLSecurityUtil.insertRecords(posp,className);
        
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(a1,pos,teamins);
        SnTDMLSecurityUtil.insertRecords(posAccount,className);
          
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        pPriority.Type__c = 'Specialty';
        pPriority.Product__c = pc.id;
        pPriority.Priority__c ='P2';
        pPriority.TeamInstance__c = teamins.Id;
        
        SnTDMLSecurityUtil.insertRecords(pPriority,className);
        System.assertEquals(pPriority.Type__c,'Specialty');
        
        AxtriaSalesIQTM__Team_Instance_Object_Attribute__c teamInsOA = TestDataFactory.createTeamInstanceObjectAttribute(teamins,pc);
        teamInsOA.AxtriaSalesIQTM__Interface_Name__c ='Call Plan';
        teamInsOA.NoCustomLabel__c = true;
        SnTDMLSecurityUtil.insertRecords(teamInsOA,className);
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,a1,teamins,posAccount,pPriority,pos);
        positionAccountCallPlan.Final_TCF__c = 10;
        positionAccountCallPlan.P1__c = pc.name;
        positionAccountCallPlan.p1_original__c = pc.name;
        positionAccountCallPlan.Final_TCF_Original__c = 10;
        positionAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c = true;
        
        SnTDMLSecurityUtil.insertRecords(positionAccountCallPlan,className);
         
        Field_Api_mapping__c fieldAPI = new Field_Api_mapping__c(Field_Api__c ='P2_Parameter1__c',Priority__c='2',wrapper__c='param2');
        SnTDMLSecurityUtil.insertRecords(fieldAPI,className);
         
         Test.startTest();
         System.runAs(loggedInUser){
             string teaminstance = teamins.Id;
             Map<String,Set<String>> qaParam = new Map<String,Set<String>>();
             Set<String> strSet = new Set<String>();
             strSet.add('NONSPECIALTY');
             strSet.add('Product');
             qaParam.put('NONSPECIALTY',strSet);
             String prod = pc.id;
             Insertprofileparam.Insertprofileparamvalues(teaminstance,qaParam,prod);
             
          }
        
         Test.stopTest();
     }
     
    static testMethod void testMethod3() {
        String classname = 'InsertprofileparamTest';
        User loggedInUser = new User(id=UserInfo.getUserId());
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Name ='USA';
        SnTDMLSecurityUtil.insertRecords(countr,className);
        Account a1= TestDataFactory.createAccount();
        a1.AxtriaSalesIQTM__Country__c = countr.id;
        a1.AccountNumber = 'BH10477999';
        SnTDMLSecurityUtil.insertRecords(a1,className);

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        SnTDMLSecurityUtil.insertRecords(team,className);
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins1,className);
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        SnTDMLSecurityUtil.insertRecords(scen,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Name = 'test';
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        
        SnTDMLSecurityUtil.insertRecords(teamins,className);
              
        Product_Catalog__c pc = TestDataFactory.productCatalog(team, teamins, countr);
        
        SnTDMLSecurityUtil.insertRecords(pc,className);
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        SnTDMLSecurityUtil.insertRecords(mmc,className);
        
        AxtriaSalesIQTM__Position__c parentPos= TestDataFactory.createPosition(team,teamins);
        parentPos.AxtriaSalesIQTM__Hierarchy_Level__c = '3';
        SnTDMLSecurityUtil.insertRecords(parentPos,className);
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__Parent_Position__c = parentPos.id;
        pos.AxtriaSalesIQTM__Hierarchy_Level__c = '2';
        SnTDMLSecurityUtil.insertRecords(pos,className);
        
        
        AxtriaSalesIQTM__Position_Product__c posp = new AxtriaSalesIQTM__Position_Product__c();
        posp.Business_Days_in_Cycle__c=5;
        posp.Calls_Day__c = 3;
        posp.AxtriaSalesIQTM__Position__c = pos.id;
        posp.Product_Catalog__c = pc.id;
        posp.External_ID__c = pos.id + '_' + pc.id;
        posp.AxtriaSalesIQTM__Effective_Start_Date__c = system.today();
        posp.AxtriaSalesIQTM__Effective_End_Date__c = system.today();
        posp.AxtriaSalesIQTM__Product_Weight__c = 3;
        SnTDMLSecurityUtil.insertRecords(posp,className);
        
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(a1,pos,teamins);
        SnTDMLSecurityUtil.insertRecords(posAccount,className);
          
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        pPriority.Type__c = 'Specialty';
        pPriority.Product__c = pc.id;
        pPriority.Priority__c ='P3';
        pPriority.TeamInstance__c = teamins.Id;
        
        SnTDMLSecurityUtil.insertRecords(pPriority,className);
        System.assertEquals(pPriority.Type__c,'Specialty');
        
        AxtriaSalesIQTM__Team_Instance_Object_Attribute__c teamInsOA = TestDataFactory.createTeamInstanceObjectAttribute(teamins,pc);
        teamInsOA.AxtriaSalesIQTM__Interface_Name__c ='Call Plan';
        teamInsOA.NoCustomLabel__c = true;
        SnTDMLSecurityUtil.insertRecords(teamInsOA,className);
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,a1,teamins,posAccount,pPriority,pos);
        positionAccountCallPlan.Final_TCF__c = 10;
        positionAccountCallPlan.P1__c = pc.name;
        positionAccountCallPlan.p1_original__c = pc.name;
        positionAccountCallPlan.Final_TCF_Original__c = 10;
        positionAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c = true;
        
        SnTDMLSecurityUtil.insertRecords(positionAccountCallPlan,className);
         
        Field_Api_mapping__c fieldAPI = new Field_Api_mapping__c(Field_Api__c ='P3_Parameter1__c',Priority__c='3',wrapper__c='param3');
        SnTDMLSecurityUtil.insertRecords(fieldAPI,className);
         
         Test.startTest();
         System.runAs(loggedInUser){
             string teaminstance = teamins.Id;
             Map<String,Set<String>> qaParam = new Map<String,Set<String>>();
             Set<String> strSet = new Set<String>();
             strSet.add('NONSPECIALTY');
             strSet.add('Product');
             qaParam.put('NONSPECIALTY',strSet);
             String prod = pc.id;
             Insertprofileparam.Insertprofileparamvalues(teaminstance,qaParam,prod);
             
          }
        
         Test.stopTest();
     }
     
    static testMethod void testMethod4() {
        String classname = 'InsertprofileparamTest';
        User loggedInUser = new User(id=UserInfo.getUserId());
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Name ='USA';
        SnTDMLSecurityUtil.insertRecords(countr,className);
        Account a1= TestDataFactory.createAccount();
        a1.AxtriaSalesIQTM__Country__c = countr.id;
        a1.AccountNumber = 'BH10477999';
        SnTDMLSecurityUtil.insertRecords(a1,className);

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        SnTDMLSecurityUtil.insertRecords(team,className);
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins1,className);
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        SnTDMLSecurityUtil.insertRecords(scen,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Name = 'test';
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        
        SnTDMLSecurityUtil.insertRecords(teamins,className);
              
        Product_Catalog__c pc = TestDataFactory.productCatalog(team, teamins, countr);
        
        SnTDMLSecurityUtil.insertRecords(pc,className);
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        SnTDMLSecurityUtil.insertRecords(mmc,className);
        
        AxtriaSalesIQTM__Position__c parentPos= TestDataFactory.createPosition(team,teamins);
        parentPos.AxtriaSalesIQTM__Hierarchy_Level__c = '3';
        SnTDMLSecurityUtil.insertRecords(parentPos,className);
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__Parent_Position__c = parentPos.id;
        pos.AxtriaSalesIQTM__Hierarchy_Level__c = '2';
        SnTDMLSecurityUtil.insertRecords(pos,className);
        
        
        AxtriaSalesIQTM__Position_Product__c posp = new AxtriaSalesIQTM__Position_Product__c();
        posp.Business_Days_in_Cycle__c=5;
        posp.Calls_Day__c = 3;
        posp.AxtriaSalesIQTM__Position__c = pos.id;
        posp.Product_Catalog__c = pc.id;
        posp.External_ID__c = pos.id + '_' + pc.id;
        posp.AxtriaSalesIQTM__Effective_Start_Date__c = system.today();
        posp.AxtriaSalesIQTM__Effective_End_Date__c = system.today();
        posp.AxtriaSalesIQTM__Product_Weight__c = 3;
        SnTDMLSecurityUtil.insertRecords(posp,className);
        
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(a1,pos,teamins);
        SnTDMLSecurityUtil.insertRecords(posAccount,className);
          
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        pPriority.Type__c = 'Specialty';
        pPriority.Product__c = pc.id;
        pPriority.Priority__c ='P1';
        pPriority.TeamInstance__c = teamins.Id;
        
        SnTDMLSecurityUtil.insertRecords(pPriority,className);
        System.assertEquals(pPriority.Type__c,'Specialty');
        
        AxtriaSalesIQTM__Team_Instance_Object_Attribute__c teamInsOA = TestDataFactory.createTeamInstanceObjectAttribute(teamins,pc);
        teamInsOA.AxtriaSalesIQTM__Interface_Name__c ='Call Plan';
        teamInsOA.NoCustomLabel__c = true;
        SnTDMLSecurityUtil.insertRecords(teamInsOA,className);
        
        Field_Api_mapping__c fieldAPI = new Field_Api_mapping__c(Field_Api__c ='P3_Parameter1__c',Priority__c='1',wrapper__c='param3');
        SnTDMLSecurityUtil.insertRecords(fieldAPI,className);
         
         Test.startTest();
         System.runAs(loggedInUser){
             string teaminstance = teamins.Id;
             Map<String,Set<String>> qaParam = new Map<String,Set<String>>();
             Set<String> strSet = new Set<String>();
             strSet.add('NONSPECIALTY');
             strSet.add('Product');
             qaParam.put('NONSPECIALTY',strSet);
             String prod = pc.id;
             Insertprofileparam.Insertprofileparamvalues(teaminstance,qaParam,prod);
             
          }
        
         Test.stopTest();
     }
     

    static testMethod void testMethod5() {
        String classname = 'InsertprofileparamTest';
        User loggedInUser = new User(id=UserInfo.getUserId());
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Name ='USA';
        SnTDMLSecurityUtil.insertRecords(countr,className);
        Account a1= TestDataFactory.createAccount();
        a1.AxtriaSalesIQTM__Country__c = countr.id;
        a1.AccountNumber = 'BH10477999';
        SnTDMLSecurityUtil.insertRecords(a1,className);

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        SnTDMLSecurityUtil.insertRecords(team,className);
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins1,className);
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        SnTDMLSecurityUtil.insertRecords(scen,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Name = 'test';
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        
        SnTDMLSecurityUtil.insertRecords(teamins,className);
              
        Product_Catalog__c pc = TestDataFactory.productCatalog(team, teamins, countr);
        
        SnTDMLSecurityUtil.insertRecords(pc,className);
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        SnTDMLSecurityUtil.insertRecords(mmc,className);
        
        AxtriaSalesIQTM__Position__c parentPos= TestDataFactory.createPosition(team,teamins);
        parentPos.AxtriaSalesIQTM__Hierarchy_Level__c = '3';
        SnTDMLSecurityUtil.insertRecords(parentPos,className);
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__Parent_Position__c = parentPos.id;
        pos.AxtriaSalesIQTM__Hierarchy_Level__c = '2';
        SnTDMLSecurityUtil.insertRecords(pos,className);
        
        
        AxtriaSalesIQTM__Position_Product__c posp = new AxtriaSalesIQTM__Position_Product__c();
        posp.Business_Days_in_Cycle__c=5;
        posp.Calls_Day__c = 3;
        posp.AxtriaSalesIQTM__Position__c = pos.id;
        posp.Product_Catalog__c = pc.id;
        posp.External_ID__c = pos.id + '_' + pc.id;
        posp.AxtriaSalesIQTM__Effective_Start_Date__c = system.today();
        posp.AxtriaSalesIQTM__Effective_End_Date__c = system.today();
        posp.AxtriaSalesIQTM__Product_Weight__c = 3;
        SnTDMLSecurityUtil.insertRecords(posp,className);
        
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(a1,pos,teamins);
        SnTDMLSecurityUtil.insertRecords(posAccount,className);
          
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        pPriority.Type__c = 'Specialty';
        pPriority.Product__c = pc.id;
        pPriority.Priority__c ='P3';
        pPriority.TeamInstance__c = teamins.Id;
        
        SnTDMLSecurityUtil.insertRecords(pPriority,className);
        System.assertEquals(pPriority.Type__c,'Specialty');
        
        AxtriaSalesIQTM__Team_Instance_Object_Attribute__c teamInsOA = TestDataFactory.createTeamInstanceObjectAttribute(teamins,pc);
        teamInsOA.AxtriaSalesIQTM__Interface_Name__c ='Call Plan';
        teamInsOA.NoCustomLabel__c = true;
        SnTDMLSecurityUtil.insertRecords(teamInsOA,className);
        
        
        Field_Api_mapping__c fieldAPI = new Field_Api_mapping__c(Field_Api__c ='P4_Parameter1__c',Priority__c='3',wrapper__c='param3');
        SnTDMLSecurityUtil.insertRecords(fieldAPI,className);
         
         Test.startTest();
         System.runAs(loggedInUser){
             string teaminstance = teamins.Id;
             Map<String,Set<String>> qaParam = new Map<String,Set<String>>();
             Set<String> strSet = new Set<String>();
             strSet.add('NONSPECIALTY');
             strSet.add('Product');
             qaParam.put('NONSPECIALTY',strSet);
             String prod = pc.id;
             Insertprofileparam.Insertprofileparamvalues(teaminstance,qaParam,prod);
             
          }
        
         Test.stopTest();
     }
     
     static testMethod void testMethod6() {
        String classname = 'InsertprofileparamTest';
        User loggedInUser = new User(id=UserInfo.getUserId());
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Name ='USA';
        SnTDMLSecurityUtil.insertRecords(countr,className);
        Account a1= TestDataFactory.createAccount();
        a1.AxtriaSalesIQTM__Country__c = countr.id;
        a1.AccountNumber = 'BH10477999';
        SnTDMLSecurityUtil.insertRecords(a1,className);

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        SnTDMLSecurityUtil.insertRecords(team,className);
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins1,className);
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        SnTDMLSecurityUtil.insertRecords(scen,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Name = 'test';
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        
        SnTDMLSecurityUtil.insertRecords(teamins,className);
              
        Product_Catalog__c pc = TestDataFactory.productCatalog(team, teamins, countr);
        
        SnTDMLSecurityUtil.insertRecords(pc,className);
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        SnTDMLSecurityUtil.insertRecords(mmc,className);
        
        AxtriaSalesIQTM__Position__c parentPos= TestDataFactory.createPosition(team,teamins);
        parentPos.AxtriaSalesIQTM__Hierarchy_Level__c = '3';
        SnTDMLSecurityUtil.insertRecords(parentPos,className);
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__Parent_Position__c = parentPos.id;
        pos.AxtriaSalesIQTM__Hierarchy_Level__c = '2';
        SnTDMLSecurityUtil.insertRecords(pos,className);
        
        
        AxtriaSalesIQTM__Position_Product__c posp = new AxtriaSalesIQTM__Position_Product__c();
        posp.Business_Days_in_Cycle__c=5;
        posp.Calls_Day__c = 3;
        posp.AxtriaSalesIQTM__Position__c = pos.id;
        posp.Product_Catalog__c = pc.id;
        posp.External_ID__c = pos.id + '_' + pc.id;
        posp.AxtriaSalesIQTM__Effective_Start_Date__c = system.today();
        posp.AxtriaSalesIQTM__Effective_End_Date__c = system.today();
        posp.AxtriaSalesIQTM__Product_Weight__c = 3;
        SnTDMLSecurityUtil.insertRecords(posp,className);
        
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(a1,pos,teamins);
        SnTDMLSecurityUtil.insertRecords(posAccount,className);
          
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        pPriority.Type__c = 'Specialty';
        pPriority.Product__c = pc.id;
        pPriority.Priority__c ='P3';
        pPriority.TeamInstance__c = teamins.Id;
        
        SnTDMLSecurityUtil.insertRecords(pPriority,className);
        System.assertEquals(pPriority.Type__c,'Specialty');
        
        AxtriaSalesIQTM__Team_Instance_Object_Attribute__c teamInsOA = TestDataFactory.createTeamInstanceObjectAttribute(teamins,pc);
        teamInsOA.AxtriaSalesIQTM__Interface_Name__c ='Call Plan';
        teamInsOA.NoCustomLabel__c = true;
        SnTDMLSecurityUtil.insertRecords(teamInsOA,className);
        
        
        Field_Api_mapping__c fieldAPI = new Field_Api_mapping__c(Field_Api__c ='P5_Parameter1__c',Priority__c='3',wrapper__c='param3');
        SnTDMLSecurityUtil.insertRecords(fieldAPI,className);
         
         Test.startTest();
         System.runAs(loggedInUser){
             string teaminstance = teamins.Id;
             Map<String,Set<String>> qaParam = new Map<String,Set<String>>();
             Set<String> strSet = new Set<String>();
             strSet.add('NONSPECIALTY');
             strSet.add('Product');
             qaParam.put('NONSPECIALTY',strSet);
             String prod = pc.id;
             Insertprofileparam.Insertprofileparamvalues(teaminstance,qaParam,prod);
             
          }
        
         Test.stopTest();
     }
     static testMethod void testMethod7() {
        String classname = 'InsertprofileparamTest';
        User loggedInUser = new User(id=UserInfo.getUserId());
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Name ='USA';
        SnTDMLSecurityUtil.insertRecords(countr,className);
        Account a1= TestDataFactory.createAccount();
        a1.AxtriaSalesIQTM__Country__c = countr.id;
        a1.AccountNumber = 'BH10477999';
        SnTDMLSecurityUtil.insertRecords(a1,className);

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        SnTDMLSecurityUtil.insertRecords(team,className);
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins1,className);
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        SnTDMLSecurityUtil.insertRecords(scen,className);
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Name = 'test';
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        
        SnTDMLSecurityUtil.insertRecords(teamins,className);
              
        Product_Catalog__c pc = TestDataFactory.productCatalog(team, teamins, countr);
        
        SnTDMLSecurityUtil.insertRecords(pc,className);
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        SnTDMLSecurityUtil.insertRecords(mmc,className);
        
        AxtriaSalesIQTM__Position__c parentPos= TestDataFactory.createPosition(team,teamins);
        parentPos.AxtriaSalesIQTM__Hierarchy_Level__c = '3';
        SnTDMLSecurityUtil.insertRecords(parentPos,className);
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__Parent_Position__c = parentPos.id;
        pos.AxtriaSalesIQTM__Hierarchy_Level__c = '2';
        SnTDMLSecurityUtil.insertRecords(pos,className);
        
        
        AxtriaSalesIQTM__Position_Product__c posp = new AxtriaSalesIQTM__Position_Product__c();
        posp.Business_Days_in_Cycle__c=5;
        posp.Calls_Day__c = 3;
        posp.AxtriaSalesIQTM__Position__c = pos.id;
        posp.Product_Catalog__c = pc.id;
        posp.External_ID__c = pos.id + '_' + pc.id;
        posp.AxtriaSalesIQTM__Effective_Start_Date__c = system.today();
        posp.AxtriaSalesIQTM__Effective_End_Date__c = system.today();
        posp.AxtriaSalesIQTM__Product_Weight__c = 3;
        SnTDMLSecurityUtil.insertRecords(posp,className);
        
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(a1,pos,teamins);
        SnTDMLSecurityUtil.insertRecords(posAccount,className);
          
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        pPriority.Type__c = 'Specialty';
        pPriority.Product__c = pc.id;
        pPriority.Priority__c ='P2';
        pPriority.TeamInstance__c = teamins.Id;
        
        SnTDMLSecurityUtil.insertRecords(pPriority,className);
        System.assertEquals(pPriority.Type__c,'Specialty');
        
        AxtriaSalesIQTM__Team_Instance_Object_Attribute__c teamInsOA = TestDataFactory.createTeamInstanceObjectAttribute(teamins,pc);
        teamInsOA.AxtriaSalesIQTM__Interface_Name__c ='Call Plan';
        teamInsOA.NoCustomLabel__c = true;
        SnTDMLSecurityUtil.insertRecords(teamInsOA,className);
        
        
        Field_Api_mapping__c fieldAPI = new Field_Api_mapping__c(Field_Api__c ='P4_Parameter1__c',Priority__c='2',wrapper__c='param3');
        SnTDMLSecurityUtil.insertRecords(fieldAPI,className);
         
         Test.startTest();
         System.runAs(loggedInUser){
             string teaminstance = teamins.Id;
             Map<String,Set<String>> qaParam = new Map<String,Set<String>>();
             Set<String> strSet = new Set<String>();
             strSet.add('NONSPECIALTY');
             strSet.add('Product');
             qaParam.put('NONSPECIALTY',strSet);
             String prod = pc.id;
             Insertprofileparam.Insertprofileparamvalues(teaminstance,qaParam,prod);
             
          }
        
         Test.stopTest();
     
     } 
     

     
}