global with sharing class PositionProductLoad_new implements Database.Batchable<sObject>, Database.Stateful
{
    public String query;
    public String selectedTeamInstance;
    Map<String, Id> teamInstanceNametoIDMap;
    Map<String, string> teamInstanceNametoTeamNameMap;
    public  List<temp_Obj__c> posp;
    String Ids;
    public integer recordsProcessed;
    public integer recordsTotal;
    Boolean flag = true;
    public String selectedTeam;
    public String teamInstanceName1;
    public Date startDate;
    public Date endDate;
    public String teamInsName;
    public boolean fullload {get; set;}
    Set<String> allproduct;
    Set<String> positionset;
    Set<String> teamProductPositionSet;
    
    //Added by HT(A0994) on 17th June 2020
    global String changeReqID;
    global Boolean flagValue = false;

    global PositionProductLoad_new(string teaminstance)
    {
       

    }

    global PositionProductLoad_new(string teaminstance, String Ids)
    {
        

    }
    
    //Added by HT(A0994) on 17th June 2020
    global PositionProductLoad_new(string teaminstance, String Ids,Boolean flag)
    {
       
    }

    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<temp_Obj__c> posprod)
    {


    }

   global void finish(Database.BatchableContext BC)
    {
       
    }
}