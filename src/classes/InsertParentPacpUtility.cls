public with sharing class InsertParentPacpUtility {
    public InsertParentPacpUtility() {
        
    }


    public static void createParentPos(List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> allPacp)
    {
    	List<String> allAccs = new List<String>();
    	List<String> allPosition = new List<String>();
    	List<String> allTeamInstances = new List<String>();

    	List<Parent_Pacp__c> allParents = [select Account__c, Position__c, Team_Instance__c from Parent_Pacp__c where Account__c in :allAccs AND Position__c in :allPosition AND Team_Instance__c in :allTeamInstances ];
    	List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> requiredPacp = new List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();

    	Map<String, String> newMap = new Map<String, String>();

    	for(Parent_Pacp__c pp : allParents)
    	{
    		String key = pp.Account__c + ' ' + pp.Position__c + '_' + pp.Team_Instance__c;
    		newMap.put(key, pp.ID);
    	}

    	List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> haveParentPos = new List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
    	List<String> duplicateParent = new List<String>();
    	List<Parent_Pacp__c> ppList = new List<Parent_Pacp__c>();

    	for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : allPacp)
    	{
    		if(pacp.Parent_Pacp__c == null)
    		{
    			String key = pacp.AxtriaSalesIQTM__Account__c + ' ' + pacp.AxtriaSalesIQTM__Position__c + '_' + pacp.AxtriaSalesIQTM__Team_Instance__c;

    			if(newMap.containsKey(key))
    			{
    				/*pacp.Parent_PACP__c = newMap.get(key);
    				haveParentPos.add(pacp);*/
    			}
    			else
    			{
    				if(!duplicateParent.contains(key))
    				{
    					Parent_Pacp__c pp = new Parent_Pacp__c();	
		 	   			pp.Account__c = pacp.AxtriaSalesIQTM__Account__c;
		 	   			pp.Position__c = pacp.AxtriaSalesIQTM__Position__c;
		 	   			pp.Team_Instance__c = pacp.AxtriaSalesIQTM__Team_Instance__c;

		 	   			ppList.add(pp);
		 	   			duplicateParent.add(key);
    				}
    				//requiredPacp.add(pacp);
    			}

    		}
    	}

    	insert ppList;

    	//newMap = new Map<String,String>();
        
        if(!Test.isRunningTest())
        {
        	for(Parent_Pacp__c pp : ppList)
        	{
        		String key = pp.Account__c + ' ' + pp.Position__c + '_' + pp.Team_Instance__c;
        		newMAp.put(key, pp.ID);
        	}
            
            
        	for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : allPacp)
        	{
        		String key = pacp.AxtriaSalesIQTM__Account__c + ' ' + pacp.AxtriaSalesIQTM__Position__c + '_' + pacp.AxtriaSalesIQTM__Team_Instance__c;
     	   		pacp.Parent_PACP__c = newMap.get(key);
        	}
            
        }

    	update requiredPacp;
    	
    }
}