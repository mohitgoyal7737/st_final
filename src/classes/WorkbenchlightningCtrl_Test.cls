/*
@author - Prince Richard Augustin (A2661)
@description - Test class to test WorkbenchlightningCtrl.
*/

@isTest
public class WorkbenchlightningCtrl_Test {
    static testMethod void testMethod1() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        AxtriaSalesIQTM__Employee__c e = new AxtriaSalesIQTM__Employee__c();
        e.AxtriaSalesIQTM__Country_Name__c = countr.id;
        e.AxtriaSalesIQTM__FirstName__c = 'test';
        e.AxtriaSalesIQTM__Last_Name__c = 'test';
        e.AxtriaSalesIQTM__Employee_ID__c = 'test';
        insert e;
        AxtriaSalesIQTM__Employee__c employee = new AxtriaSalesIQTM__Employee__c();
        employee.Name = 'test';
        employee.AxtriaSalesIQTM__Country_Name__c = countr.id;
        employee.AxtriaSalesIQTM__Employee_ID__c = '12345';
        employee.AxtriaSalesIQTM__Last_Name__c = 'lastName';
        employee.AxtriaSalesIQTM__FirstName__c = 'firstName';
        employee.AxtriaSalesIQTM__Gender__c = 'M';
        insert employee;
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__Employee__c = employee.id;
        pos.AxtriaSalesIQTM__Position_Type__c ='Region';
        insert pos;
        AxtriaSalesIQTM__Position_Employee__c posemp = TestDataFactory.createPositionEmployee(pos,e.id,'test',system.date.today(),system.date.today()+1);
        insert posemp;
        CR_Employee_Feed__c cr1 = new CR_Employee_Feed__c();
        cr1.Employee__c = employee.id;
        cr1.IsRemoved__c = false;
        cr1.Event_Name__c  = 'test';
        cr1.Position__c = pos.id;
        // cr1.Position_Employee__c = posemp.id;
        cr1.Request_Date1__c = system.date.today();
        insert cr1;
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            List<WorkbenchlightningCtrl.wrapperReport> test6 = WorkbenchlightningCtrl.Feed('test', countr.id);
        }
        Test.stopTest();
    }
    static testMethod void testMethod11() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        AxtriaSalesIQTM__Employee__c e = new AxtriaSalesIQTM__Employee__c();
        e.AxtriaSalesIQTM__Country_Name__c = countr.id;
        e.AxtriaSalesIQTM__FirstName__c = 'test';
        e.AxtriaSalesIQTM__Last_Name__c = 'test';
        e.AxtriaSalesIQTM__Employee_ID__c = 'test';
        insert e;
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__Employee__c = e.id;
        insert pos;
        CR_Employee_Feed__c cr = new CR_Employee_Feed__c();
        cr.Event_Name__c = system.label.Terminate_Employee;
        cr.Employee__c = e.id;
        cr.IsRemoved__c = false;
        cr.Position__c = pos.id;
        insert cr;
        CR_Employee_Feed__c cr1 = new CR_Employee_Feed__c();
        cr1.Employee__c = e.id;
        cr1.IsRemoved__c = false;
        cr1.Position__c = pos.id;
        cr1.Request_Date1__c = system.date.today();
        insert cr1;
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            
            WorkbenchlightningCtrl obj=new WorkbenchlightningCtrl();
            String test = WorkbenchlightningCtrl.getCountryAttributes(countr.id);
            map<String,list<String>>  test1 = WorkbenchlightningCtrl.getProfileNameMap();
            CountWrapper test3 = WorkbenchlightningCtrl.GetCount(countr.id);
            String test4 = WorkbenchlightningCtrl.isStatusMarkCompleted(cr.id);
            String test5 = WorkbenchlightningCtrl.isRemovedEventfromWB(cr.id);
            WorkbenchlightningCtrl.wrapperReport w = new WorkbenchlightningCtrl.wrapperReport( 'ids', 
              'Name', 
              'IsRemoved', 
              'Request_Date1', 
              'Territory_ID',
              'Position_Name', 
              'Position_id',
              'Employee_Name',
              'Employee_record_Id', 
              'Training_Completion_Date1', 
              'Assignment_Type', 
              'Position_Type', 
              'Employee_ID', 
              'Event_Name', 
              'Parent_Position_Name', 
              'Field_Force', 
              'Manager_Name', 
              'Status',
              'Hire_Date',
              'Term_Date',
              'Team_Instance_End_Date' );
            
        }
        Test.stopTest();
    }
    static testMethod void testMethod2() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Employee__c e = new AxtriaSalesIQTM__Employee__c();
        e.AxtriaSalesIQTM__Country_Name__c = countr.Id;
        e.AxtriaSalesIQTM__FirstName__c = 'test';
        e.AxtriaSalesIQTM__Last_Name__c = 'test';
        insert e;
        
        CR_Employee_Feed__c cr = new CR_Employee_Feed__c();
        cr.Event_Name__c = system.label.Transfer_to_HO;
        cr.Employee__c = e.id;
        cr.IsRemoved__c = false;
        insert cr;
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            
            WorkbenchlightningCtrl obj=new WorkbenchlightningCtrl();
            String test = WorkbenchlightningCtrl.getCountryAttributes(countr.id);
            map<String,list<String>>  test1 = WorkbenchlightningCtrl.getProfileNameMap();
            CountWrapper test3 = WorkbenchlightningCtrl.GetCount(countr.id);
            
        }
        Test.stopTest();
    }
    static testMethod void testMethod3() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Employee__c e = new AxtriaSalesIQTM__Employee__c();
        e.AxtriaSalesIQTM__Country_Name__c = countr.Id;
        e.AxtriaSalesIQTM__FirstName__c = 'test';
        e.AxtriaSalesIQTM__Last_Name__c = 'test';
        insert e;
        
        CR_Employee_Feed__c cr = new CR_Employee_Feed__c();
        cr.Event_Name__c =system.label.Leave_of_Absence;
        cr.Employee__c = e.id;
        cr.IsRemoved__c = false;
        insert cr;
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            
            WorkbenchlightningCtrl obj=new WorkbenchlightningCtrl();
            String test = WorkbenchlightningCtrl.getCountryAttributes(countr.id);
            map<String,list<String>>  test1 = WorkbenchlightningCtrl.getProfileNameMap();
            CountWrapper test3 = WorkbenchlightningCtrl.GetCount(countr.id);
            
        }
        Test.stopTest();
    }
    static testMethod void testMethod4() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Employee__c e = new AxtriaSalesIQTM__Employee__c();
        e.AxtriaSalesIQTM__Country_Name__c = countr.Id;
        e.AxtriaSalesIQTM__FirstName__c = 'test';
        e.AxtriaSalesIQTM__Last_Name__c = 'test';
        insert e;
        
        CR_Employee_Feed__c cr = new CR_Employee_Feed__c();
        cr.Event_Name__c = system.label.Promote_Employee;
        cr.Employee__c = e.id;
        cr.IsRemoved__c = false;
        insert cr;
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            
            WorkbenchlightningCtrl obj=new WorkbenchlightningCtrl();
            String test = WorkbenchlightningCtrl.getCountryAttributes(countr.id);
            map<String,list<String>>  test1 = WorkbenchlightningCtrl.getProfileNameMap();
            CountWrapper test3 = WorkbenchlightningCtrl.GetCount(countr.id);
            
        }
        Test.stopTest();
    }
    static testMethod void testMethod5() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Employee__c e = new AxtriaSalesIQTM__Employee__c();
        e.AxtriaSalesIQTM__Country_Name__c = countr.Id;
        e.AxtriaSalesIQTM__FirstName__c = 'test';
        e.AxtriaSalesIQTM__Last_Name__c = 'test';
        insert e;
        
        CR_Employee_Feed__c cr = new CR_Employee_Feed__c();
        cr.Event_Name__c = system.label.Employee_NewHire;
        cr.Employee__c = e.id;
        cr.IsRemoved__c = false;
        insert cr;
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            
            WorkbenchlightningCtrl obj=new WorkbenchlightningCtrl();
            String test = WorkbenchlightningCtrl.getCountryAttributes(countr.id);
            map<String,list<String>>  test1 = WorkbenchlightningCtrl.getProfileNameMap();
            CountWrapper test3 = WorkbenchlightningCtrl.GetCount(countr.id);
            
        }
        Test.stopTest();
    }
    static testMethod void testMethod6() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Employee__c e = new AxtriaSalesIQTM__Employee__c();
        e.AxtriaSalesIQTM__Country_Name__c = countr.Id;
        e.AxtriaSalesIQTM__FirstName__c = 'test';
        e.AxtriaSalesIQTM__Last_Name__c = 'test';
        insert e;
        
        CR_Employee_Feed__c cr = new CR_Employee_Feed__c();
        cr.Event_Name__c = system.label.Transfer_Employee;
        cr.Employee__c = e.id;
        cr.IsRemoved__c = false;
        insert cr;
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            
            WorkbenchlightningCtrl obj=new WorkbenchlightningCtrl();
            String test = WorkbenchlightningCtrl.getCountryAttributes(countr.id);
            map<String,list<String>>  test1 = WorkbenchlightningCtrl.getProfileNameMap();
            CountWrapper test3 = WorkbenchlightningCtrl.GetCount(countr.id);
            
        }
        Test.stopTest();
    }
    static testMethod void testMethod7() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Employee__c e = new AxtriaSalesIQTM__Employee__c();
        e.AxtriaSalesIQTM__Country_Name__c = countr.Id;
        e.AxtriaSalesIQTM__FirstName__c = 'test';
        e.AxtriaSalesIQTM__Last_Name__c = 'test';
        insert e;
        
        CR_Employee_Feed__c cr = new CR_Employee_Feed__c();
        cr.Event_Name__c = system.label.Transfer_to_Field;
        cr.Employee__c = e.id;
        cr.IsRemoved__c = false;
        insert cr;
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            
            WorkbenchlightningCtrl obj=new WorkbenchlightningCtrl();
            String test = WorkbenchlightningCtrl.getCountryAttributes(countr.id);
            map<String,list<String>>  test1 = WorkbenchlightningCtrl.getProfileNameMap();
            CountWrapper test3 = WorkbenchlightningCtrl.GetCount(countr.id);
            
        }
        Test.stopTest();
    }
    static testMethod void testMethod8() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Employee__c e = new AxtriaSalesIQTM__Employee__c();
        e.AxtriaSalesIQTM__Country_Name__c = countr.Id;
        e.AxtriaSalesIQTM__FirstName__c = 'test';
        e.AxtriaSalesIQTM__Last_Name__c = 'test';
        insert e;
        
        CR_Employee_Feed__c cr = new CR_Employee_Feed__c();
        cr.Event_Name__c = system.label.Employee_Rehire;
        cr.Employee__c = e.id;
        cr.IsRemoved__c = false;
        insert cr;
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            
            WorkbenchlightningCtrl obj=new WorkbenchlightningCtrl();
            String test = WorkbenchlightningCtrl.getCountryAttributes(countr.id);
            map<String,list<String>>  test1 = WorkbenchlightningCtrl.getProfileNameMap();
            CountWrapper test3 = WorkbenchlightningCtrl.GetCount(countr.id);
            
        }
        Test.stopTest();
    }
    static testMethod void testMethod9() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Employee__c e = new AxtriaSalesIQTM__Employee__c();
        e.AxtriaSalesIQTM__Country_Name__c = countr.Id;
        e.AxtriaSalesIQTM__FirstName__c = 'test';
        e.AxtriaSalesIQTM__Last_Name__c = 'test';
        insert e;
        
        CR_Employee_Feed__c cr = new CR_Employee_Feed__c();
        cr.Event_Name__c = system.label.Transfer_out_of_Sales_team;
        cr.Employee__c = e.id;
        cr.IsRemoved__c = false;
        insert cr;
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            
            WorkbenchlightningCtrl obj=new WorkbenchlightningCtrl();
            String test = WorkbenchlightningCtrl.getCountryAttributes(countr.id);
            map<String,list<String>>  test1 = WorkbenchlightningCtrl.getProfileNameMap();
            CountWrapper test3 = WorkbenchlightningCtrl.GetCount(countr.id);
            
        }
        Test.stopTest();
    }
}