@isTest
public class update_Geography_Test {
    
    static testMethod void testMethod1() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Geography_Type__c type = new AxtriaSalesIQTM__Geography_Type__c();
        type.AxtriaSalesIQTM__Country__c = countr.Id;
        insert type;
        AxtriaSalesIQTM__Geography__c zt0 = new AxtriaSalesIQTM__Geography__c();
        zt0.name = 'Y';
        zt0.AxtriaSalesIQTM__Parent_Zip__c = '07059';
        zt0.AxtriaSalesIQTM__Geography_Type__c = type.Id;
        insert zt0;
        
        AxtriaSalesIQTM__Geography__c zt = new AxtriaSalesIQTM__Geography__c();
        zt.name = 'X';
        zt.AxtriaSalesIQTM__Parent_Zip__c = '07060';
        zt.AxtriaSalesIQTM__Parent_Zip_Code__c=zt0.id;
        zt.AxtriaSalesIQTM__Geography_Type__c = type.Id;
        insert zt;
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            update_Geography obj=new update_Geography(countr.Id);
            obj.query='select id,name,AxtriaSalesIQTM__Parent_Zip__c,AxtriaSalesIQTM__Parent_Zip_Code__c,Geography_Type1__c,Country_Code__c ,AxtriaSalesIQTM__External_Geo_Type__c ,AxtriaSalesIQTM__Zip_Type__c  from AxtriaSalesIQTM__Geography__c';
            Database.executeBatch(obj);
            
        }
        Test.stopTest();
    }
    
    
}