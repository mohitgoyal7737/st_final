global class Batch_Integration_TSF implements Database.Batchable<sObject>, Database.Stateful{
    public String query;
    public List<String> allProcessedIds;
    public List<String> allTeamInstances;

    public List<SIQ_TSF_vod_O__c> allStagingTSF;

    global Batch_Integration_TSF(List<String> allTeamInstances) {
        this.allTeamInstances  = allTeamInstances;
        allProcessedIds = new List<String>();
        this.query = 'select id, P1__c, Segment__c, AxtriaSalesIQTM__Account__r.AZ_VeevaID__c , AxtriaSalesIQTM__Account__r.AccountNumber ,Adoption__c, Potential__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name, Country__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, Accessibility_Range__c, Party_ID__r.Accessibility_Range__c, AxtriaSalesIQTM__Team_Instance__c  from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Team_Instance__c in :allTeamInstances and AxtriaSalesIQTM__lastApprovedTarget__c = true and AxtriaSalesIQTM__Position__c != null';

       
        // Account_vod__c  Territory   AZ External Id  My Target   Accessibility';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scope) {

         allStagingTSF = new List<SIQ_TSF_vod_O__c>();

         for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : scope)
         {
            SIQ_TSF_vod_O__c stc = new SIQ_TSF_vod_O__c();
            stc.SIQ_My_Target_vod__c = true;
            stc.SIQ_Account_Number__c = pacp.AxtriaSalesIQTM__Account__r.AccountNumber;
            stc.SIQ_Account_vod__c = pacp.AxtriaSalesIQTM__Account__r.AZ_VeevaID__c;
            stc.SIQ_Territory_vod__c = pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;

            if(pacp.Party_ID__r.Accessibility_Range__c !=null)
            stc.SIQ_Accessibility_AZ_EU__c = pacp.Party_ID__r.Accessibility_Range__c;

            if(pacp.Accessibility_Range__c !=null)
            stc.SIQ_Accessibility_AZ_EU__c = pacp.Accessibility_Range__c;

            
            stc.Team_Instance__c = pacp.AxtriaSalesIQTM__Team_Instance__c;
            stc.Position_Account_Call_Plan__c = pacp.ID;
            stc.SIQ_External_ID_AZ__c = stc.SIQ_Account_vod__c +'_'+ stc.SIQ_Territory_vod__c;
            stc.Status__c = 'Updated';
            if(!allProcessedIds.contains(stc.SIQ_External_ID_AZ__c))
            {
                allStagingTSF.add(stc);
                allProcessedIds.add(stc.SIQ_External_ID_AZ__c);
            }   
         }

         upsert allStagingTSF SIQ_External_ID_AZ__c;

    }

    global void finish(Database.BatchableContext BC) {

        Database.executeBatch(new Batch_Integration_TSF_Extended(allProcessedIds));
    }
}