@isTest
private class PosProdInactivewithCallPlanTrgrHandlerT { 
    static testMethod void testMethod1() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc.AxtriaSalesIQTM__Country__c = countr.id;
        insert acc;
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        insert scen;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.AxtriaSalesIQTM__Team_Cycle_Name__c = 'Current';
        teamins.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        insert teamins;
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        AxtriaSalesIQTM__Product__c product = new AxtriaSalesIQTM__Product__c();
        product.Team_Instance__c = teamins.id;
        product.CurrencyIsoCode = 'USD'; 
        product.Name= 'Test';
        product.AxtriaSalesIQTM__Product_Code__c = 'testproduct'; 
        product.AxtriaSalesIQTM__Effective_End_Date__c = date.today();
        product.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        insert product;


        AxtriaSalesIQTM__Position_Product__c positionproduct = new AxtriaSalesIQTM__Position_Product__c();
        positionproduct.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        positionproduct.AxtriaSalesIQTM__Product_Master__c = product.id;
        positionproduct.AxtriaSalesIQTM__Position__c = pos.id;
        positionproduct.Product_Catalog__c = pcc.id;
        positionproduct.CurrencyIsoCode = 'USD'; 
        positionproduct.AxtriaSalesIQTM__Product_Weight__c = 1;
        positionproduct.AxtriaSalesIQTM__isActive__c = true;
        positionproduct.Business_Days_in_Cycle__c = 1;
        positionproduct.Calls_Day__c = 1;
        positionproduct.Holidays__c = 1;
        positionproduct.Other_Days_Off__c = 1;
        positionproduct.Vacation_Days__c = 1;
        positionproduct.AxtriaSalesIQTM__Product_Weight__c = 1;
        positionproduct.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        positionproduct.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();

        insert positionproduct;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        pPriority.Product_Name__c ='GIST';
        pPriority.Speciality_Name__c ='Cardiology';
        pPriority.CycleName__c = 'Oncology';
        
        pPriority.Product_Type__c ='Speciality';
        
        pPriority.Product__c = pcc.id;
        pPriority.TeamInstance__c =teamins.id;
        pPriority.Type__c ='Territory';
        pPriority.Position_Name__c =pos.Name;
        pPriority.TeamInstance__c =teamins.id;
        insert pPriority;
        
        AxtriaSalesIQTM__Position_Team_Instance__c posteamins = TestDataFactory.createPositionTeamInstance(pos.id,null,teamins.id);
        insert posteamins;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        positionAccountCallPlan.Product_Type__c = 'Speciality';
        positionAccountCallPlan.P1__c = 'GIST';
        positionAccountCallPlan.AxtriaSalesIQTM__Position_Team_Instance__c = posteamins.id;
        positionAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c = True;
        positionAccountCallPlan.AxtriaSalesIQTM__Team_Instance__c= teamins.Id;
        
        insert positionAccountCallPlan;
        
        List<AxtriaSalesIQTM__Position_Product__c> PosProdList = new List<AxtriaSalesIQTM__Position_Product__c>();
        PosProdList.add(positionproduct);
        Map<id,AxtriaSalesIQTM__Position_Product__c> oldMap = new Map<id,AxtriaSalesIQTM__Position_Product__c>();
        oldMap.put(positionproduct.id,positionproduct);
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            
            PosProdInactivewithCallPlanTrgrHandler obj=new PosProdInactivewithCallPlanTrgrHandler();
            PosProdInactivewithCallPlanTrgrHandler.InactivePosProd(PosProdList,oldMap);

        }
        Test.stopTest();
    }
}