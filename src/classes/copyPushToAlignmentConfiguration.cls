public with sharing class copyPushToAlignmentConfiguration {
    
    public void copyPushToAlignmentConfiguration(ID sourceTeamIns, ID destTeamIns) {
        Map<String,String> mapProductCodeToID = new Map<String, String>();
        List<Source_to_Destination_Mapping__c> clonedRecordList = new List<Source_to_Destination_Mapping__c>();
        for(Product_Catalog__c pc : [Select Id, Product_code__c from Product_Catalog__c where Team_Instance__c =:destTeamIns]){
            mapProductCodeToID.put(pc.Product_code__c,pc.Id);
        }
        for(Source_to_Destination_Mapping__c mapping : [Select Source_Object_Field__c, Destination_Object_Field__c,Team_Instance__c,Product__c,Product__r.Product_code__c,Load_Type__c from Source_to_Destination_Mapping__c where Team_Instance__c = :sourceTeamIns and  Destination_Object_Field__c != '' and Source_Object_Field__c != '']){

            Source_to_Destination_Mapping__c rec = new Source_to_Destination_Mapping__c();
            rec = mapping.clone();
            rec.Team_Instance__c = destTeamIns;
            if(rec.Product__c != null && mapProductCodeToID.containsKey(rec.Product__r.Product_code__c))
                rec.Product__c = mapProductCodeToID.get(rec.Product__r.Product_code__c);
            
            clonedRecordList.add(rec);
        }
        
        if(clonedRecordList.size()>0){
            //insert clonedRecordList;
            SnTDMLSecurityUtil.insertRecords(clonedRecordList, 'copyPushToAlignmentConfiguration');
        }
    }
}