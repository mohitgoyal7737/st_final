global class batchPopulateComparisonObject_BR implements Database.Batchable<sObject> {
    public String query;
    public String teamInstance;
    
    global batchPopulateComparisonObject_BR(String teamInstance) {
        this.teamInstance = teamInstance;

        this.query = 'SELECT Acc_Pos__c, Position__c, OUTPUT_VALUE_30__c, OUTPUT_VALUE_29__c, OUTPUT_VALUE_28__c, OUTPUT_VALUE_27__c, '+
                     'OUTPUT_VALUE_26__c, OUTPUT_VALUE_25__c, OUTPUT_VALUE_24__c, OUTPUT_VALUE_23__c, OUTPUT_VALUE_22__c, OUTPUT_VALUE_21__c, '+
                     'OUTPUT_NAME_30__c, OUTPUT_NAME_29__c, OUTPUT_NAME_28__c, OUTPUT_NAME_27__c, OUTPUT_NAME_26__c, OUTPUT_NAME_25__c, '+
                     'OUTPUT_NAME_24__c, OUTPUT_NAME_23__c, OUTPUT_NAME_22__c, OUTPUT_NAME_21__c, Potential__c, OUTPUT_VALUE_20__c, '+
                     'OUTPUT_VALUE_19__c, OUTPUT_VALUE_18__c, OUTPUT_VALUE_17__c, OUTPUT_VALUE_16__c, OUTPUT_NAME_20__c, OUTPUT_NAME_19__c, '+
                     'OUTPUT_NAME_18__c, OUTPUT_NAME_17__c, OUTPUT_NAME_16__c, Line__c, LinePublished__c, Download_ID__c, Brand_Name__c, BU__c, '+
                     'BUPublished__c, Final_TCF_Copy__c, OUTPUT_VALUE_15__c, OUTPUT_VALUE_14__c, OUTPUT_VALUE_13__c, OUTPUT_VALUE_12__c, OUTPUT_VALUE_11__c, '+
                     'OUTPUT_NAME_15__c, OUTPUT_NAME_14__c, OUTPUT_NAME_13__c, OUTPUT_NAME_12__c, OUTPUT_NAME_11__c, Final_Segment__c, Updated_Frequency__c, '+
                     'OUTPUT_NAME_7__c, OUTPUT_NAME_10__c, OUTPUT_NAME_9__c, OUTPUT_NAME_8__c, OUTPUT_NAME_6__c, OUTPUT_NAME_5__c, OUTPUT_NAME_4__c, OUTPUT_NAME_3__c, OUTPUT_NAME_2__c, OUTPUT_VALUE_10__c, OUTPUT_VALUE_9__c, OUTPUT_VALUE_8__c, OUTPUT_VALUE_7__c, OUTPUT_VALUE_6__c, OUTPUT_VALUE_5__c, OUTPUT_VALUE_4__c, OUTPUT_VALUE_3__c, OUTPUT_VALUE_2__c, OUTPUT_NAME_1__c, OUTPUT_VALUE_1__c, '+
                     'Name, Physician_2__c, Measure_Master__c,Measure_Master__r.Team_Instance__c ,Measure_Master__r.Brand_Lookup__c, Physician__r.AxtriaSalesIQTM__Position__c,Physician__r.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, Id, Calculated_TCF__c, Priority__c, Call_Mid_Percentage__c, Call_Low_Percentage__c, Call_High_Percentage__c, Account_Mid_Percentage__c, Account_Low_Percentage__c, Account_High_Percentage__c, Final_TCF__c, Proposed_TCF__c, IsPublished__c '+
                     'FROM Account_Compute_Final__c WHERE Measure_Master__r.Team_Instance__c =: teamInstance';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Account_Compute_Final__c> scope) {
        
        map<string,string> configPotFieldMap = new map<string,string>();
        map<string,string> configAdoptFieldMap = new map<string,string>();  
              
        for(Config_Potential_Field_Meaning__c configPotField : [SELECT Id,Name,Product__c,Potential_Field_Name__c,Adoption_Field_Name__c,Team_Instance__c FROM Config_Potential_Field_Meaning__c WHERE Team_Instance__c =:teamInstance]){
            configPotFieldMap.put(configPotField.Product__c +','+ configPotField.Team_Instance__c,configPotField.Potential_Field_Name__c);
            configAdoptFieldMap.put(configPotField.Product__c +','+ configPotField.Team_Instance__c,configPotField.Adoption_Field_Name__c);
        }
        
        map<string,map<string,decimal>> AdpValueMap = new map<string,map<string,decimal>>();
        map<string,map<string,decimal>> potValueMap = new map<string,map<string,decimal>>();
        string mapKey;
        for(Adoption_Potential_Val_Map__c adpPotval : [SELECT id,name,Adoption_Text__c,Adoption_Value__c,Potential__c,Potential_Val__c,Product__c,Measure_Master__c,Measure_Master__r.Team_Instance__c 
                                                      FROM Adoption_Potential_Val_Map__c  WHERE Measure_Master__r.Team_Instance__c =:teamInstance]){
            mapKey = adpPotval.Product__c +',' + adpPotval.Measure_Master__r.Team_Instance__c ;
            
            if(!AdpValueMap.containsKey(mapKey)){
                AdpValueMap.put(mapKey,new map<string,decimal>());
                potValueMap.put(mapKey,new map<string,decimal>());
            }
            if(AdpValueMap.containsKey(mapKey)){
                AdpValueMap.get(mapKey).put(adpPotval.Adoption_Text__c,adpPotval.Adoption_Value__c);
                potValueMap.get(mapKey).put(adpPotval.Potential__c,adpPotval.Potential_Val__c);
            }
            
        }
        
        system.debug('-----1----' + configPotFieldMap );
        system.debug('-----22----' + potValueMap );
        
        map<string,Adoption_Potential_Comparison__c> AdpPotCmprMap = new map<string,Adoption_Potential_Comparison__c>();
        for(Adoption_Potential_Comparison__c adpPot : [select id,B_R_Adoption__c,B_R_Potential__c,External_ID__c,Product_Catalog__c,
                                                        S_D_Adoption__c,S_D_Potential__c,Team_Instance__c,Territory__c,Territory__r.AxtriaSalesIQTM__Client_Position_Code__c 
                                                         FROM Adoption_Potential_Comparison__c where Team_Instance__c =:teamInstance]){
            AdpPotCmprMap.put(adpPot.Team_Instance__c +','+adpPot.Territory__r.AxtriaSalesIQTM__Client_Position_Code__c ,adpPot);
        }
        
        
        string computeKey;
        string uniqueKey ;        
        for(Account_Compute_Final__c accCompute : scope){
            computeKey = accCompute.Measure_Master__r.Brand_Lookup__c + ',' + accCompute.Measure_Master__r.Team_Instance__c ;
            uniqueKey = accCompute.Measure_Master__r.Team_Instance__c + ',' +accCompute.Physician__r.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c ;
            
            Adoption_Potential_Comparison__c adpPotComparison ;
            
            if(AdpPotCmprMap.containsKey(uniqueKey)){
                adpPotComparison = AdpPotCmprMap.get(uniqueKey);
            }
            else{
                adpPotComparison = new Adoption_Potential_Comparison__c();
            }
            adpPotComparison.Team_Instance__c = accCompute.Measure_Master__r.Team_Instance__c ;
            adpPotComparison.Territory__c = accCompute.Physician__r.AxtriaSalesIQTM__Position__c;
            adpPotComparison.External_ID__c = uniqueKey ;
            
            if(AdpValueMap.containsKey(computeKey)){
                if(adpPotComparison.B_R_Adoption__c == Null && configAdoptFieldMap.containsKey(computeKey) && !AdpPotCmprMap.containsKey(uniqueKey)){
                    adpPotComparison.B_R_Adoption__c = string.valueof(AdpValueMap.get(computeKey).get((string.valueof(accCompute.get(configAdoptFieldMap.get(computeKey))))));
                }
                else if(adpPotComparison.B_R_Adoption__c != Null && configAdoptFieldMap.containsKey(computeKey) && AdpPotCmprMap.containsKey(uniqueKey)){
                    if(adpValueMap.get(computeKey).get((string.valueof(accCompute.get(configAdoptFieldMap.get(computeKey))))) != Null)
                        adpPotComparison.B_R_Adoption__c = string.valueof(decimal.valueof(adpPotComparison.B_R_Adoption__c) + AdpValueMap.get(computeKey).get((string.valueof(accCompute.get(configAdoptFieldMap.get(computeKey))))));
                }
            }
            if(potValueMap.containsKey(computeKey)){    
                if(configPotFieldMap.containsKey(computeKey) &&  adpPotComparison.B_R_Potential__c == Null && !AdpPotCmprMap.containsKey(uniqueKey)){
                    adpPotComparison.B_R_Potential__c = string.valueof(potValueMap.get(computeKey).get((string.valueof(accCompute.get(configPotFieldMap.get(computeKey))))));
                }
                else if(configPotFieldMap.containsKey(computeKey) && adpPotComparison.B_R_Potential__c != Null && AdpPotCmprMap.containsKey(uniqueKey)){
                    if(potValueMap.get(computeKey).get((string.valueof(accCompute.get(configPotFieldMap.get(computeKey))))) != Null)
                    adpPotComparison.B_R_Potential__c = string.valueof(decimal.valueof(adpPotComparison.B_R_Potential__c ) + potValueMap.get(computeKey).get((string.valueof(accCompute.get(configPotFieldMap.get(computeKey))))));
                }
            }      
            
            AdpPotCmprMap.put(uniqueKey,adpPotComparison);
                     
        }  
        
        if(AdpPotCmprMap.size() > 0){
            upsert AdpPotCmprMap.values() External_ID__c ;
        }  
    }

    global void finish(Database.BatchableContext BC) {

    }
}