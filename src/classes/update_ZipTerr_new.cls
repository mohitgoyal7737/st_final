global with sharing class update_ZipTerr_new implements Database.Batchable<sObject>, Database.Stateful
{
    /*Replaced temp_Zip_Terr__c with temp_Obj__c due to object purge*/
    global string query;
    global string teamID;
    global string teamInstance;
    global string teamName;
    global string country;
    String Ids;
    Boolean flag = true;
    global list<AxtriaSalesIQTM__Team__c> temlst = new list<AxtriaSalesIQTM__Team__c>();
    global list<AxtriaSalesIQTM__Geography__c> ziplist = new list<AxtriaSalesIQTM__Geography__c>();
    global list<AxtriaSalesIQTM__Position__c> poslist = new list<AxtriaSalesIQTM__Position__c>();
    global list<temp_Obj__c> zipTerrlist = new list<temp_Obj__c>();
    global list<AxtriaSalesIQTM__Team_Instance__c> countrylst = new list<AxtriaSalesIQTM__Team_Instance__c>();
    global string geotype ;

    //Added by HT(A0994) on 17th June 2020
    global String changeReqID;
    global Boolean flagValue = false;

    global update_ZipTerr_new(String Team, String TeamIns)
    {
       }

    global update_ZipTerr_new(String Team, String TeamIns, String Ids)
    {
       
    }

    //Added by HT(A0994) on 17th June 2020
    global update_ZipTerr_new(String Team, String TeamIns, String Ids, Boolean flag)
    {
       
    }

    global Database.Querylocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator(query);
    }

    global void execute (Database.BatchableContext BC, List<temp_Obj__c>zipTerrlist)
    {
        
    }

    global void finish(Database.BatchableContext BC)
    {   
       
    }
}