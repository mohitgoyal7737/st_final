public class EmployeeViewController {   
   public list<AxtriaSalesIQTM__Position_Employee__c> posAssignment     {get;set;}     

    public EmployeeViewController(ApexPages.StandardController controller){        
        AxtriaSalesIQTM__Employee__c posId=(AxtriaSalesIQTM__Employee__c)controller.getRecord();
        posAssignment = [Select Id, AxtriaSalesIQTM__Position__c, 
                                                  AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c,
                                                  AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__c,
                                                  AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Assignment_Type__c,
                                                  AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Position__r.Name,
                                                  AxtriaSalesIQTM__Employee__c,AxtriaSalesIQTM__Employee__r.name,
                                                  AxtriaSalesIQTM__Reason_Code__c 
                                                  From AxtriaSalesIQTM__Position_Employee__c 
                                                  Where 
                         //AxtriaSalesIQTM__Effective_End_Date__c >=: system.today() and
                                                   AxtriaSalesIQTM__Employee__c =: posId.id];

    }
 
}