@isTest
private class BatchPositionAccountAddedDropped_Test {
     static testmethod void testBatchPositionAccountActiveInactive() {
    date myDate;
     
    AxtriaSalesIQTM__Team__c Team = New AxtriaSalesIQTM__Team__c();
    Team.Name = 'Test';
    insert Team;
    
    AxtriaSalesIQTM__Team_Instance__c Teaminstance = new AxtriaSalesIQTM__Team_Instance__c();
    Teaminstance.Name = 'TestR';
    Teaminstance.AxtriaSalesIQTM__Team__c = Team.id;
    insert Teaminstance;
    
    AxtriaSalesIQTM__Position__c Position = New AxtriaSalesIQTM__Position__c();
    Position.Name = 'TestRe';
    Position.AxtriaSalesIQTM__Team_iD__c = Team.id;
    insert Position;
    
    Account acc = new Account();
    acc.Name = 'Testrec';
    acc.Marketing_Code__c = 'IT';
    insert acc;
    
    AxtriaSalesIQTM__Position_Account__c PA = new AxtriaSalesIQTM__Position_Account__c();
    //PA.Name = 'Testreco';
    PA.AxtriaSalesIQTM__Team_Instance__c = Teaminstance.id;
    PA.AxtriaSalesIQTM__Position__c = Position.id;
    PA.AxtriaSalesIQTM__Account__c = acc.id;
   // PA.lastmodifieddate  myDate;
    insert PA;
    
    String TeamInstanceA;
  
    myDate = date.newInstance(2019, 01, 23);
    
      Database.executeBatch(new BatchPositionAccountAddedDropped(myDate, TeamInstanceA));
    
   }
}