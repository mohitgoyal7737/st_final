@isTest
private class TestRemainingTriggers {

    private static testMethod void test() {
        
        
        AxtriaSalesIQTM__Employee__c e = new AxtriaSalesIQTM__Employee__c();
        e.name = 'abc';
        e.AxtriaSalesIQTM__FirstName__c='abc';
        e.AxtriaSalesIQTM__Last_Name__c='abc';
        insert e;
        
        /*AxtriaSalesIQTM__Employee__c e2 = new AxtriaSalesIQTM__Employee__c();
        e2.id=e.id;
        e2.name = 'abc2';
        update e2;*/
        
        AxtriaSalesIQTM__Organization_Master__c aom = new AxtriaSalesIQTM__Organization_Master__c(AxtriaSalesIQTM__Org_Level__c='Global',AxtriaSalesIQTM__Parent_Country_Level__c=true);
        insert aom;
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='Italy',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active');
        insert country;

        Parameter__c pm = new Parameter__c();
        pm.id=pm.id;
        pm.name = 'abc';
        insert pm;
        
        Parameter__c pm2 = new Parameter__c();
        pm2.id=pm.id;
        pm2.name = 'abc2';
        update pm2;
        
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
        team.Name = 'Specialty';
        team.AxtriaSalesIQTM__Country__c=country.id;
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c();
        ti.name='abc';
        ti.AxtriaSalesIQTM__Team__c=team.id;
        //ti.AxtriaSalesIQTM__Country__c=team.AxtriaS;
        insert ti;
        
        AxtriaSalesIQTM__Product__c p = new AxtriaSalesIQTM__Product__c();
        p.name = 'abc';
        p.AxtriaSalesIQTM__Effective_End_Date__c = date.parse('01/01/2019');
        p.AxtriaSalesIQTM__Effective_Start_Date__c = date.parse('01/01/2018');
        p.AxtriaSalesIQTM__Product_Code__c = 'Brilique_ES';
        p.Team_Instance__c =ti.id;
        //p.AxtriaSalesIQTM__Product_Code__c = 'PCODE';
        insert p;
        
        AxtriaSalesIQTM__Product__c p2 = new AxtriaSalesIQTM__Product__c();
        p2.id=p.id;
        p2.name = 'abc2';
        p2.Team_Instance__c =ti.id;
        update p2;
        
        
        
        
        AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
        cr.AxtriaSalesIQTM__Status__c='Pending';
        cr.AxtriaSalesIQTM__Team_Instance_ID__c=ti.id;
        insert cr;
        
        AxtriaSalesIQTM__Change_Request__c cr2 = new AxtriaSalesIQTM__Change_Request__c();
        cr2.id=cr.id;
        cr2.AxtriaSalesIQTM__Status__c='Approved';
        update cr2;
        
        AxtriaSalesIQTM__Change_Request__c cr3 = new AxtriaSalesIQTM__Change_Request__c();
        cr3.id=cr.id;
        delete cr3;
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        
        Measure_Master__c mm = new Measure_Master__c();
        mm.Name = 'Rule';

        insert mm;

        mm.Name = 'RUle2';

        update mm;
        
        
    } 

}