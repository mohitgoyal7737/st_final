@isTest

public class Integration_Batch_GAS_History_Data2Test {
    
    static testMethod void testMethod1() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        acc.AZ_VeevaID__c ='test';
        acc.AxtriaSalesIQTM__Active__c = 'Active';
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Published';
        insert scen;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current' ;
        teamins.AxtriaSalesIQTM__Scenario__c = scen.Id;
        insert teamins;
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        SIQ_GAS_History__c s = new SIQ_GAS_History__c();
        s.SIQ_Added_Territory__c =pos.Id;
        s.SIQ_Account__c=acc.Id;
        s.Run_Count__c =1;
        s.Deployment_Status__c ='Rejected';
        insert s;
        
        List<AxtriaSalesIQTM__Team_Instance__c>  t = new  List<AxtriaSalesIQTM__Team_Instance__c> ();
        t.add(teamins);
        
        List<String>  t1 = new  List<String> ();
        t1.add(teamins.id);
        
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            Integration_Batch_GAS_History_Data2 obj=new Integration_Batch_GAS_History_Data2();
            obj.query = 'select id, SIQ_Added_Territory__c, SIQ_Account__c, Run_Count__c, Deployment_Status__c from SIQ_GAS_History__c ';
            Database.executeBatch(obj);
            
            
        }
        Test.stopTest();
    }
    
    static testMethod void testMethod2() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__Active__c = 'Active';
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Published';
        insert scen;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current' ;
        teamins.AxtriaSalesIQTM__Scenario__c = scen.Id;
        insert teamins;
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        SIQ_GAS_History__c s = new SIQ_GAS_History__c();
        s.SIQ_Added_Territory__c =pos.Id;
        s.SIQ_Account__c =acc.Id;
        s.Run_Count__c =1;
        s.Deployment_Status__c ='Rejected';
        insert s;
        List<AxtriaSalesIQTM__Team_Instance__c>  t = new  List<AxtriaSalesIQTM__Team_Instance__c> ();
        t.add(teamins);
        List<String>  t1 = new  List<String> ();
        t1.add(teamins.id);
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            
            Integration_Batch_GAS_History_Data2 o2=new Integration_Batch_GAS_History_Data2(true);
            o2.query = 'select id, SIQ_Added_Territory__c, SIQ_Account__c, Run_Count__c, Deployment_Status__c from SIQ_GAS_History__c ';
            Database.executeBatch(o2);
            
        }
        Test.stopTest();
    }
    
    static testMethod void testMethod3() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__Active__c = 'Active';
        acc.AZ_VeevaID__c='test234';
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Stage__c = 'Published';
        insert scen;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current' ;
        teamins.AxtriaSalesIQTM__Scenario__c = scen.Id;
        insert teamins;
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        /*IF(AxtriaSalesIQTM__Effective_Start_Date__c > Today() && AxtriaSalesIQTM__Effective_End_Date__c >= AxtriaSalesIQTM__Effective_Start_Date__c,'Future Active', 
IF(AxtriaSalesIQTM__Effective_Start_Date__c <= Today() && AxtriaSalesIQTM__Effective_End_Date__c >= Today(),'Active', 
IF(AxtriaSalesIQTM__Effective_End_Date__c < Today(),'Inactive','')
))*/
posAccount.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
posAccount.AxtriaSalesIQTM__Effective_Start_Date__c=date.today()-1;

        //posAccount.AxtriaSalesIQTM__Assignment_Status__c ='Active';

insert posAccount;


SIQ_GAS_History__c s = new SIQ_GAS_History__c();
s.SIQ_Added_Territory__c ='N003';
s.SIQ_Account__c =posAccount.Id;
s.Run_Count__c =1;
s.Deployment_Status__c ='Rejected';
insert s;





List<AxtriaSalesIQTM__Team_Instance__c>  t = new  List<AxtriaSalesIQTM__Team_Instance__c> ();
t.add(teamins);
List<String>  t1 = new  List<String> ();
t1.add(teamins.id);

Test.startTest();
System.runAs(loggedInUser){
    ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
    String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
    List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
    System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
    
    Integration_Batch_GAS_History_Data2 o3=new Integration_Batch_GAS_History_Data2(t1);
    o3.query = 'select id, SIQ_Added_Territory__c, SIQ_Account__c, Run_Count__c, Deployment_Status__c from SIQ_GAS_History__c ';
    Database.executeBatch(o3);
}
Test.stopTest();
}
}