public class HOImpersonation {
    
    //public List<Contact> oppList {get; set;}
    
    public static ID user_ID;
    
    public List<user> usr  {get; set;}
    
    public list<Wrapper> wrapperList {get;set;}
    public id userid;

    public List<String> profile  {get; set;}
    
    public HOImpersonation(){
        //userid='';
        usr = new List<user> ();
        profile = new list<String>();
        getUSers();
    }
    
    
    public static string userFetchCountry(){ //returns the country
        user_ID= UserInfo.getUserId();
        list<user> current_user = new list<user>();
        current_user = [select Country from User where ID = :user_ID];
        system.debug('Value is+++++++++++++++++++' + current_user);
        return current_user[0].country;
        
       
        
    }
    
    
public void getUSers() {
    
     String OrgId = UserInfo.getOrganizationId();
     String BaseUrl = URL.getSalesforceBaseUrl().toExternalForm();
     wrapperList = new List<Wrapper>();
     userid=UserInfo.getUserId();
     
     List<AxtriaARSnT__ImpersonationHO__c> obj = AxtriaARSnT__ImpersonationHO__c.getAll().values();

     for (AxtriaARSnT__ImpersonationHO__c mc : obj){

          profile.add(mc.AxtriaARSnT__Profile__c);

     }


     system.debug(obj);
    
    
  for (User u : [Select Name, UserName , id , Country , profile.name  from User where  Country =: HOImpersonation.userFetchCountry() AND profile.Name IN :profile  AND IsActive = True  ]) 
    { 
       
       usr.add(u);
    }                                                                                         
   
    if (!usr.isEmpty()) {
                for (User u: usr) {
                    String link = Baseurl + '/servlet/servlet.su?oid=' + OrgId + '&suorgadminid=' + u.id + '&retURL=%2Fapex%2FUserImpersonation&targetURL=%2Flightning%2Fpage%2Fhome';
                     System.debug('Linkis+++++++++++++++++++'+link);
                    wrapperList.add(new Wrapper(u, link));
                }
               
            }
            
    }
    
    public class Wrapper {

        public User usr {
            get;
            set;
        }
        public String link {
            get;
            set;
        }

        public Wrapper(User usr, String link) {
            this.usr = usr;
            this.link = link;
        }
    }
    
    
}