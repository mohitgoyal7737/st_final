global class changeMySetupStatus implements Database.Batchable<sObject> {
    public String query;
    Set<String> countries;
    public Boolean chaining = false;
    public Boolean flag = true;
     public Date lmd;
     List<string> allCountries;

    global changeMySetupStatus() 
    {
      countries = new Set<String>();
      for(AxtriaSalesIQTM__Team_Instance__c ti : [select AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Team_Instance__c])
      {
        countries.add(ti.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c);
      }
      system.debug('+++countries'+countries);
      this.query = 'select id from SIQ_My_Setup_Products_vod_O__c where SIQ_Country__c in :countries and Status__c = \'Updated\'';
        //flag=check;
    }
    global changeMySetupStatus(List<String> allCountriesTemp) 
    {
      countries = new Set<String>();
      for(AxtriaSalesIQTM__Team_Instance__c ti : [select AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c in :allCountriesTemp])
      {
        countries.add(ti.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c);
      }
      system.debug('+++countries'+countries);
      this.query = 'select id from SIQ_My_Setup_Products_vod_O__c where SIQ_Country__c in :countries and Status__c = \'Updated\'';
    }

    global changeMySetupStatus(List<String> allCountriesTemp, Boolean chain) 
    {
      chaining = chain;
      countries = new Set<String>();

      for(AxtriaSalesIQTM__Team_Instance__c ti : [select AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Team_Instance__c])
      {
        countries.add(ti.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c);
      }
      system.debug('+++countries'+countries);
      this.query = 'select id from SIQ_My_Setup_Products_vod_O__c where SIQ_Country__c in :countries and Status__c = \'Updated\' ';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
      return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<SIQ_My_Setup_Products_vod_O__c> scope) 
    {
      for(SIQ_My_Setup_Products_vod_O__c msp : scope)
      {
        msp.Status__c = '';
      }
        update scope;
    }

    global void finish(Database.BatchableContext BC) 
    {
        //lmd=Date.Today();
        
        list<string> Teaminstancelistis = new list<string>();
        teaminstancelistis = StaticTeaminstanceList.getDeltaTeamInstances();
        system.debug('teaminstancelistis++'+teaminstancelistis);
        if(chaining)
        {
          BatchDeltaMySetupProduct u2 = new BatchDeltaMySetupProduct(lmd,teaminstancelistis,true);
          database.executeBatch(u2,2000);
        }
        
    }

     global void execute(System.SchedulableContext SC)
     {
        list<string> teaminstancelistis = new list<string>();
        teaminstancelistis = StaticTeaminstanceList.getAllCountries();
        //database.executeBatch(new Refresh_My_Setup_Products(teaminstancelistis, false), 2000);
        database.executebatch(new changeMySetupStatus(teaminstancelistis,true),2000);
         
     } 
}