global with sharing class BatchMergeTopDownHandling implements Database.Batchable<sObject>, Database.Stateful {
    public String query;
    public Set<String> deassignID;
    public Set<String> deassignHCAPosSet; 
    public Set<String> deassignHCPPosSet; 
    public Set<String> deassignHCATeamInsSet;
    public Set<String> deassignHCAKey;
    public Set<String> deassignHCPTeamInsSet;
    public Set<String> deassignHCPKey;
    public Set<String> inputHCAset;
    public Set<String> inputHCPset;
    public Boolean flag{get;set;}
    public Set<String> looserSet;
    public Set<String> allLooserSet;
    public Set<String> setBUDeassignID;


    global BatchMergeTopDownHandling(Set<String> setDeassignID,Boolean flagg) {}

    global BatchMergeTopDownHandling(Set<String> setDeassignID,Boolean flagg, Set<String> looserRecSet) {

        query = '';
        deassignID=new Set<String>();
        allLooserSet=new Set<String>();
        //setBUDeassignID=new Set<String>();
        flag=flagg;
        deassignID.addAll(setDeassignID);
        allLooserSet.addAll(looserRecSet);
        //setBUDeassignID.addAll(setBUDeassignIDRec);
        query='select Id,Account__c,Position__c,Team_Instance__c,Status__c,Account_Type__c,Rule_Type__c,Country_Name__c from Deassign_Postiton_Account__c where Status__c=\'New Merge\' and Id in :deassignID';

    }

    global BatchMergeTopDownHandling(Set<String> setDeassignID,Boolean flagg, Set<String> looserRecSet, Set<String> setBUDeassignIDRec) {
        query = '';
        deassignID=new Set<String>();
        allLooserSet=new Set<String>();
        setBUDeassignID=new Set<String>();
        flag=flagg;
        deassignID.addAll(setDeassignID);
        allLooserSet.addAll(looserRecSet);
        setBUDeassignID.addAll(setBUDeassignIDRec);
        query='select Id,Account__c,Position__c,Team_Instance__c,Status__c,Account_Type__c,Rule_Type__c,Country_Name__c from Deassign_Postiton_Account__c where Status__c=\'New Merge\' and Id in :deassignID';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Deassign_Postiton_Account__c> scope) {

        SnTDMLSecurityUtil.printDebugMessage('=====Query::::::::::' +scope);
        deassignHCAPosSet=new Set<String>();
        looserSet=new Set<String>();
        deassignHCATeamInsSet=new Set<String>();
        deassignHCAKey=new Set<String>();
        deassignHCPKey=new Set<String>();
        inputHCAset=new Set<String>();
        inputHCPset=new Set<String>();
        deassignHCPPosSet=new Set<String>();
        deassignHCPTeamInsSet=new Set<String>();
        Map<String,Set<String>> mapinputHCA2key=new Map<String,Set<String>>();
        Map<String,Set<String>> mapinputHCP2key=new Map<String,Set<String>>();
        List<AxtriaSalesIQTM__Position_Account__c> deassignPosAccList = new List<AxtriaSalesIQTM__Position_Account__c>();
        List<Deassign_Postiton_Account__c> deassignList = new List<Deassign_Postiton_Account__c>();
        List<String> hcoList = new List<String>{'HCA','HCO','CMOP'};
        List<String> hcpList = new List<String>{'HCP','Physician'};

        for(Deassign_Postiton_Account__c deassignRec : scope)
        {
            //if(deassignRec.Rule_Type__c == 'Top Down')
            //{
                looserSet.add(deassignRec.Account__c);
                SnTDMLSecurityUtil.printDebugMessage('Rule Type is Top Down');
                if(hcoList.contains(deassignRec.Account_Type__c))
                {
                    SnTDMLSecurityUtil.printDebugMessage('Account is HCA and Rule is Top Down');
                    inputHCAset.add(deassignRec.Account__c);
                    deassignHCAPosSet.add(deassignRec.Position__c);
                    //deassignAccSet.add(deassignRec.Account__c);
                    deassignHCATeamInsSet.add(deassignRec.Team_Instance__c);
                    deassignHCAKey.add(deassignRec.Account__c + '_' + deassignRec.Position__c + '_' + deassignRec.Team_Instance__c);
                    deassignRec.Status__c='Processed Merge';
                }
                else if(hcpList.contains(deassignRec.Account_Type__c))
                {
                    SnTDMLSecurityUtil.printDebugMessage('Account is HCP and Rule is Top Down'); 
                    inputHCPset.add(deassignRec.Account__c);
                    deassignHCPPosSet.add(deassignRec.Position__c);
                    deassignHCPTeamInsSet.add(deassignRec.Team_Instance__c);
                    deassignHCPKey.add(deassignRec.Account__c + '_' + deassignRec.Position__c + '_' + deassignRec.Team_Instance__c);
                    deassignRec.Status__c='Processed Merge';
                }

                
                deassignList.add(deassignRec);
            //}
        }
        SnTDMLSecurityUtil.printDebugMessage('======inputHCAset::::' +inputHCAset);
        SnTDMLSecurityUtil.printDebugMessage('======inputHCPset::::' +inputHCPset);

        SnTDMLSecurityUtil.printDebugMessage('=========Handling Top Down------ Input is HCA::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::');

        SnTDMLSecurityUtil.printDebugMessage('===========Querying Position Account for Deassign Account-Position Set For HCA================================================');
        if(inputHCAset != null)
        {
            List<AxtriaSalesIQTM__Position_Account__c> deassignHCAPosAccList = [select Id,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.Merge_Account_Number__c,IsGasAssignment__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__c,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c from AxtriaSalesIQTM__Position_Account__c where (AxtriaSalesIQTM__Assignment_Status__c = 'Active' or AxtriaSalesIQTM__Assignment_Status__c = 'Future Active') and AxtriaSalesIQTM__Position__c!=null and AxtriaSalesIQTM__Account__c!=null and AxtriaSalesIQTM__Team_Instance__c!=null and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c in :deassignHCAPosSet and AxtriaSalesIQTM__Account__r.Merge_Account_Number__c in :inputHCAset and AxtriaSalesIQTM__Team_Instance__r.Name in :deassignHCATeamInsSet];
          
            SnTDMLSecurityUtil.printDebugMessage('=====deassign HCA Position Account=====' +deassignHCAPosAccList.size());

            

            if(deassignHCAPosAccList != null)
            {
                for(AxtriaSalesIQTM__Position_Account__c deassignPA : deassignHCAPosAccList)
                {
                    String key = deassignPA.AxtriaSalesIQTM__Account__r.Merge_Account_Number__c + '_' +deassignPA.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' + deassignPA.AxtriaSalesIQTM__Team_Instance__r.Name;
                    if(deassignHCAKey.contains(key))
                    {
                        if(!mapinputHCA2key.containsKey(deassignPA.AxtriaSalesIQTM__Account__r.Merge_Account_Number__c))
                        {
                            String tempKey = deassignPA.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' +deassignPA.AxtriaSalesIQTM__Team_Instance__r.Name;
                            Set<String> tempKeySet= new Set<String>();
                            tempKeySet.add(tempKey);
                            mapinputHCA2key.put(deassignPA.AxtriaSalesIQTM__Account__r.Merge_Account_Number__c,tempKeySet);
                        }
                        else
                        {
                            String tempKey = deassignPA.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' +deassignPA.AxtriaSalesIQTM__Team_Instance__r.Name;
                            mapinputHCA2key.get(deassignPA.AxtriaSalesIQTM__Account__r.Merge_Account_Number__c).add(tempKey);
                        }
                        //if(deassignPA.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=='Current')
                        deassignPA.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addDays(-1);

                        //if(deassignPA.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=='Future')
                        //deassignPA.AxtriaSalesIQTM__Effective_End_Date__c=deassignPA.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c.addDays(-1);
                        if(flag == true)
                        {
                            deassignPosAccList.add(deassignPA);
                        }
                        else if(flag== false)
                        {
                            if(deassignPA.IsGasAssignment__c == false)
                            {
                                deassignPosAccList.add(deassignPA);
                            }
                        }


                    }
                }
              }
              SnTDMLSecurityUtil.printDebugMessage('=====mapinputHCA2key::::::' +mapinputHCA2key);

              SnTDMLSecurityUtil.printDebugMessage('==========Affiliation Handling for HCAs========================================================================================');
              List<AxtriaSalesIQTM__Account_Affiliation__c> inputHCA2affHCPList = [select Id, AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.Merge_Account_Number__c, AxtriaSalesIQTM__Account__r.Status__c,Account_Number__c,Parent_Account_Number__c,AxtriaSalesIQTM__Affiliation_Network__c,Country__c,IsDeleted,AxtriaSalesIQTM__Parent_Account__c,AxtriaSalesIQTM__Parent_Account__r.Merge_Account_Number__c,AxtriaSalesIQTM__Root_Account__c from   AxtriaSalesIQTM__Account_Affiliation__c where (Affiliation_Status__c='Active' or Affiliation_Status__c='Future Active') and AxtriaSalesIQTM__Parent_Account__r.Merge_Account_Number__c in :inputHCAset and AxtriaSalesIQTM__Active__c=true];

             SnTDMLSecurityUtil.printDebugMessage('=====deassign HCA and HCP Affiliation=====' +inputHCA2affHCPList);

             Map<String,Set<String>> mapinputHCA2affHCPset=new Map<String,Set<String>>();

             if(inputHCA2affHCPList != null)
             {
                for(AxtriaSalesIQTM__Account_Affiliation__c inputHCA2affHCPRec : inputHCA2affHCPList)
                {
                    //mapinputHCA2affHCPset.put(inputHCA2affHCPRec.AxtriaSalesIQTM__Account__r.Merge_Account_Number__c,inputHCA2affHCPRec.AxtriaSalesIQTM__Parent_Account__r.Merge_Account_Number__c);
                    if(mapinputHCA2affHCPset.containsKey(inputHCA2affHCPRec.AxtriaSalesIQTM__Parent_Account__r.Merge_Account_Number__c))
                    {
                        mapinputHCA2affHCPset.get(inputHCA2affHCPRec.AxtriaSalesIQTM__Parent_Account__r.Merge_Account_Number__c).add(inputHCA2affHCPRec.AxtriaSalesIQTM__Account__r.Merge_Account_Number__c);
                    }
                    else
                    {
                        Set<String> tempKey = new Set<String>();
                        tempkey.add(inputHCA2affHCPRec.AxtriaSalesIQTM__Account__r.Merge_Account_Number__c);
                        mapinputHCA2affHCPset.put(inputHCA2affHCPRec.AxtriaSalesIQTM__Parent_Account__r.Merge_Account_Number__c,tempkey);
                    }
                }
             }
             SnTDMLSecurityUtil.printDebugMessage('=====mapinputHCA2affHCPset:::::::::' +mapinputHCA2affHCPset);

             Set<String> affHCPSet = new Set<String>();
             if(mapinputHCA2affHCPset != null)
             {
                for(String inputHCA : mapinputHCA2affHCPset.keySet())
                {
                    affHCPSet.addAll(mapinputHCA2affHCPset.get(inputHCA));
                }
             }
             SnTDMLSecurityUtil.printDebugMessage('=====affHCPSet:::::::::' +affHCPSet);

             SnTDMLSecurityUtil.printDebugMessage('===========Querying Position Account for affiliated HCP (HCA-->HCP) First level====================================================');
             List<AxtriaSalesIQTM__Position_Account__c> affHCPposAccList = [select Id,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.Merge_Account_Number__c,IsGasAssignment__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__c,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c from AxtriaSalesIQTM__Position_Account__c where (AxtriaSalesIQTM__Assignment_Status__c = 'Active' or AxtriaSalesIQTM__Assignment_Status__c = 'Future Active') and AxtriaSalesIQTM__Position__c!=null and AxtriaSalesIQTM__Account__c!=null and AxtriaSalesIQTM__Team_Instance__c!=null and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c in :deassignHCAPosSet and AxtriaSalesIQTM__Account__r.Merge_Account_Number__c in :affHCPSet and AxtriaSalesIQTM__Team_Instance__r.Name in :deassignHCATeamInsSet];

             SnTDMLSecurityUtil.printDebugMessage('=====HCA Position Account=====' +affHCPposAccList.size());
             Map<String,Set<String>> mapaffHCP2key=new Map<String,Set<String>>();

             if(affHCPposAccList != null)
            {
                for(AxtriaSalesIQTM__Position_Account__c affHCPRec : affHCPposAccList)
                {
                    if(!mapaffHCP2key.containsKey(affHCPRec.AxtriaSalesIQTM__Account__r.Merge_Account_Number__c))
                    {
                        String tempKey = affHCPRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' +affHCPRec.AxtriaSalesIQTM__Team_Instance__r.Name;
                        Set<String> tempKeySet= new Set<String>();
                        tempKeySet.add(tempKey);
                        mapaffHCP2key.put(affHCPRec.AxtriaSalesIQTM__Account__r.Merge_Account_Number__c,tempKeySet);
                    }
                    else
                    {
                        String tempKey = affHCPRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' +affHCPRec.AxtriaSalesIQTM__Team_Instance__r.Name;
                        mapaffHCP2key.get(affHCPRec.AxtriaSalesIQTM__Account__r.Merge_Account_Number__c).add(tempKey);
                    }
                }
            }
            SnTDMLSecurityUtil.printDebugMessage('=====mapaffHCP2key:::::::::' +mapaffHCP2key);

            SnTDMLSecurityUtil.printDebugMessage('===========Check for common Positions in input HCA ----> affiliated HCPs========================================================');
            Map<String,Set<String>> mapfurthercheckHCP=new Map<String,Set<String>>();
            //Set<String> setHCA2HCP = new Set<String>();

            if(mapinputHCA2key != null)
            {
                for(String inputhca : mapinputHCA2key.keySet())
                {
                    Set<String> hcaPosTIKey = mapinputHCA2key.get(inputhca);
                    SnTDMLSecurityUtil.printDebugMessage('=====hcaPosTIKey:::::::::' +hcaPosTIKey);
                    Set<String> hcpSet = mapinputHCA2affHCPset.get(inputhca);
                    SnTDMLSecurityUtil.printDebugMessage('=====hcpSet:::::::::' +hcpSet);
                    if(hcpSet != null)
                    {
                        for(String affhcp : hcpSet)
                        {
                            SnTDMLSecurityUtil.printDebugMessage('=====affhcp:::::::::' +affhcp);
                            if(mapaffHCP2key.get(affhcp) != null)
                            {
                                for(String hcpPosTIKey : mapaffHCP2key.get(affhcp))
                                {
                                    SnTDMLSecurityUtil.printDebugMessage('=====hcpPosTIKey:::::::::' +hcpPosTIKey);
                                    if(hcaPosTIKey.contains(hcpPosTIKey))
                                    {
                                        SnTDMLSecurityUtil.printDebugMessage('=====Key matched:::::::::');
                                        if(!mapfurthercheckHCP.containsKey(affhcp))
                                        {
                                            Set<String> commonKeySET = new Set<String>();
                                            commonKeySET.add(hcpPosTIKey);
                                            mapfurthercheckHCP.put(affhcp,commonKeySET);
                                            SnTDMLSecurityUtil.printDebugMessage('=====put mapfurthercheckHCP:::::::::' +mapfurthercheckHCP);
                                        }
                                        else
                                        {
                                            mapfurthercheckHCP.get(affhcp).add(hcpPosTIKey); 
                                            SnTDMLSecurityUtil.printDebugMessage('=====add more mapfurthercheckHCP:::::::::' +mapfurthercheckHCP);  
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            SnTDMLSecurityUtil.printDebugMessage('=====mapfurthercheckHCP:::::::::' +mapfurthercheckHCP);
            
            SnTDMLSecurityUtil.printDebugMessage('==========Further Affiliation Handling for HCPs===========================================================================');
            List<AxtriaSalesIQTM__Account_Affiliation__c> furtherAffHCP2HCAsList = [select Id, AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.Merge_Account_Number__c, AxtriaSalesIQTM__Account__r.Status__c,Account_Number__c,Parent_Account_Number__c,AxtriaSalesIQTM__Affiliation_Network__c,Country__c,IsDeleted,AxtriaSalesIQTM__Parent_Account__c,AxtriaSalesIQTM__Parent_Account__r.Merge_Account_Number__c,AxtriaSalesIQTM__Root_Account__c from   AxtriaSalesIQTM__Account_Affiliation__c where (Affiliation_Status__c='Active' or Affiliation_Status__c='Future Active') and AxtriaSalesIQTM__Account__r.Merge_Account_Number__c in :mapfurthercheckHCP.keySet() and AxtriaSalesIQTM__Active__c=true];

            SnTDMLSecurityUtil.printDebugMessage('=====Affiliated HCPs to further HCAs list size=====' +furtherAffHCP2HCAsList.size());
            SnTDMLSecurityUtil.printDebugMessage('=====Affiliated HCPs to further HCAs=====' +furtherAffHCP2HCAsList);
            Map<String,Set<String>> mapFurtherHCP2affHCA=new Map<String,Set<String>>();

            if(furtherAffHCP2HCAsList != null)
            {
                for(AxtriaSalesIQTM__Account_Affiliation__c hcp2hcaRec : furtherAffHCP2HCAsList)
                {
                    //String key = hcp2hcaRec.AxtriaSalesIQTM__Parent_Account__r.Merge_Account_Number__c + '_' + hcp2hcaRec.AxtriaSalesIQTM__Account__r.Merge_Account_Number__c;
                    
                    if(mapFurtherHCP2affHCA.containsKey(hcp2hcaRec.AxtriaSalesIQTM__Account__r.Merge_Account_Number__c))
                    {
                        mapFurtherHCP2affHCA.get(hcp2hcaRec.AxtriaSalesIQTM__Account__r.Merge_Account_Number__c).add(hcp2hcaRec.AxtriaSalesIQTM__Parent_Account__r.Merge_Account_Number__c);
                    }
                    else
                    {
                        Set<String> hca = new Set<String>();
                        hca.add(hcp2hcaRec.AxtriaSalesIQTM__Parent_Account__r.Merge_Account_Number__c);
                        mapFurtherHCP2affHCA.put(hcp2hcaRec.AxtriaSalesIQTM__Account__r.Merge_Account_Number__c,hca);  ////Doubt................
                    }
                    
                }
            }

            SnTDMLSecurityUtil.printDebugMessage('=====mapFurtherHCP2affHCA=====' +mapFurtherHCP2affHCA);

            SnTDMLSecurityUtil.printDebugMessage('=====Maintain further affiliated HCAs to check the Position Account=================================================');
            Set<String> furtheraffHCAs = new Set<String>();

            if(mapFurtherHCP2affHCA != null)
            {
                for(String hcp : mapFurtherHCP2affHCA.keySet())
                {
                    if(mapFurtherHCP2affHCA.get(hcp) != null)
                    {
                        furtheraffHCAs.addAll(mapFurtherHCP2affHCA.get(hcp));
                    }
                }
            }
            SnTDMLSecurityUtil.printDebugMessage('=====furtheraffHCAs=====' +furtheraffHCAs);

            SnTDMLSecurityUtil.printDebugMessage('=====Querying Position Account for further affiliated HCAs ===============================================================');
            List<AxtriaSalesIQTM__Position_Account__c> furtherAffHCAsPosAccList = [select Id,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.Merge_Account_Number__c,IsGasAssignment__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__c,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c from AxtriaSalesIQTM__Position_Account__c where (AxtriaSalesIQTM__Assignment_Status__c = 'Active' or AxtriaSalesIQTM__Assignment_Status__c = 'Future Active') and AxtriaSalesIQTM__Position__c!=null and AxtriaSalesIQTM__Account__c!=null and AxtriaSalesIQTM__Team_Instance__c!=null and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c in :deassignHCAPosSet and AxtriaSalesIQTM__Account__r.Merge_Account_Number__c in :furtheraffHCAs and AxtriaSalesIQTM__Team_Instance__r.Name in :deassignHCATeamInsSet];

            SnTDMLSecurityUtil.printDebugMessage('=====further affiliated HCAs Position Account========' +furtherAffHCAsPosAccList.size());
            Map<String,Set<String>> mapFurtheraffHCA2key=new Map<String,Set<String>>();

            if(furtherAffHCAsPosAccList != null)
            {
                for(AxtriaSalesIQTM__Position_Account__c furtherHCA2key : furtherAffHCAsPosAccList)
                {
                    if(!mapFurtheraffHCA2key.containsKey(furtherHCA2key.AxtriaSalesIQTM__Account__r.Merge_Account_Number__c))
                    {
                        String tempKey = furtherHCA2key.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' +furtherHCA2key.AxtriaSalesIQTM__Team_Instance__r.Name;
                        Set<String> tempKeySet= new Set<String>();
                        tempKeySet.add(tempKey);
                        mapFurtheraffHCA2key.put(furtherHCA2key.AxtriaSalesIQTM__Account__r.Merge_Account_Number__c,tempKeySet);
                    }
                    else
                    {
                        String tempKey = furtherHCA2key.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' +furtherHCA2key.AxtriaSalesIQTM__Team_Instance__r.Name;
                        mapFurtheraffHCA2key.get(furtherHCA2key.AxtriaSalesIQTM__Account__r.Merge_Account_Number__c).add(tempKey);
                    }
                }
            }
            SnTDMLSecurityUtil.printDebugMessage('=====mapFurtheraffHCA2key:::::::::' +mapFurtheraffHCA2key);

            SnTDMLSecurityUtil.printDebugMessage('=====check for commom Position Check::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::');
            Map<String,Set<String>> mapHCPkey2HCAkey = new Map<String,Set<String>>();

            if(mapfurthercheckHCP != null)
            {
                for(String hcp : mapfurthercheckHCP.keySet())
                {
                    SnTDMLSecurityUtil.printDebugMessage('=====hcp:::::::::' +hcp);
                    Set<String> furtherPosTI_HCP = mapfurthercheckHCP.get(hcp);
                    SnTDMLSecurityUtil.printDebugMessage('=====furtherPosTI_HCP:::::::::' +furtherPosTI_HCP);
                    if(furtherPosTI_HCP != null)
                    {
                        for(String hcpkey : furtherPosTI_HCP)
                        {
                            SnTDMLSecurityUtil.printDebugMessage('=====hcpkey:::::::::' +hcpkey);
                            String hcpkeyCombination = hcp+'_'+hcpkey;
                            SnTDMLSecurityUtil.printDebugMessage('=====hcpkeyCombination:::::::::' +hcpkeyCombination);
                            if(mapFurtherHCP2affHCA.get(hcp) != null)
                            {
                                for(String hca : mapFurtherHCP2affHCA.get(hcp))
                                {
                                    SnTDMLSecurityUtil.printDebugMessage('=====hca:::::::::' +hca);
                                    if(mapFurtheraffHCA2key.get(hca) != null)
                                    {
                                        for(String hcakeycheck : mapFurtheraffHCA2key.get(hca))
                                        {
                                            SnTDMLSecurityUtil.printDebugMessage('=====hcakeycheck:::::::::' +hcakeycheck);
                                            if(furtherPosTI_HCP.contains(hcakeycheck))
                                            {
                                                SnTDMLSecurityUtil.printDebugMessage('=====hcakeycheck:::::::::' +hcakeycheck);
                                                String hcakeyCombination = hca+'_'+hcakeycheck;
                                                SnTDMLSecurityUtil.printDebugMessage('=====hcakeyCombination:::::::::' +hcakeyCombination);
                                                if(!mapHCPkey2HCAkey.containsKey(hcpkeyCombination))
                                                {
                                                    Set<String> setkeyHCA = new Set<String>();
                                                    setkeyHCA.add(hcakeyCombination);
                                                    mapHCPkey2HCAkey.put(hcpkeyCombination,setkeyHCA);
                                                    SnTDMLSecurityUtil.printDebugMessage('=====mapHCPkey2HCAkey inside if:::::::::' +mapHCPkey2HCAkey);
                                                }
                                                else
                                                {
                                                    mapHCPkey2HCAkey.get(hcpkeyCombination).add(hcakeyCombination);
                                                    SnTDMLSecurityUtil.printDebugMessage('=====mapHCPkey2HCAkey inside else:::::::::' +mapHCPkey2HCAkey.get(hcpkeyCombination));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }  
            SnTDMLSecurityUtil.printDebugMessage('=====mapHCPkey2HCAkey:::::::::' +mapHCPkey2HCAkey);

            SnTDMLSecurityUtil.printDebugMessage('=====Remove the input from further affiliated HCA====================================================================================');

            if(mapHCPkey2HCAkey != null)
            {
                for(String hcpkey : mapHCPkey2HCAkey.keySet())
                {
                    SnTDMLSecurityUtil.printDebugMessage('=====hcpkey:::::::::' +hcpkey);
                    for(String hcakey : mapHCPkey2HCAkey.get(hcpkey))
                    {
                        SnTDMLSecurityUtil.printDebugMessage('=====hcakey:::::::::' +hcakey);
                        if(deassignHCAKey.contains(hcakey))
                        {
                            mapHCPkey2HCAkey.get(hcpkey).remove(hcakey);
                        }
                    }
                }
            }
            SnTDMLSecurityUtil.printDebugMessage('=====mapHCPkey2HCAkey after removing input HCAs key:::::::::' +mapHCPkey2HCAkey);
            Set<String> inactiveHCPkey = new Set<String>();

            if(mapHCPkey2HCAkey != null)
            {
                for(String key_HCP : mapHCPkey2HCAkey.keySet())
                {
                    SnTDMLSecurityUtil.printDebugMessage('=====key_HCP:::::::::' +key_HCP);
                    SnTDMLSecurityUtil.printDebugMessage('=====mapHCPkey2HCAkey.get(key_HCP).size():::::::::' +mapHCPkey2HCAkey.get(key_HCP).size());
                    if(mapHCPkey2HCAkey.get(key_HCP).size() == 0)
                    {
                        inactiveHCPkey.add(key_HCP);
                    }
                }
            }
            SnTDMLSecurityUtil.printDebugMessage('=====inactiveHCPkey:::::::::' +inactiveHCPkey);

            SnTDMLSecurityUtil.printDebugMessage('=====Remove the extra affiliated HCPs if included through cross join==================================================================');
            Set<String> furtherHCPkeyset = new Set<String>();

            if(mapfurthercheckHCP != null)
            {
                for(String hcp : mapfurthercheckHCP.keySet())
                {
                    for(String posTI : mapfurthercheckHCP.get(hcp))
                    {
                        furtherHCPkeyset.add(hcp+'_'+posTI);
                    }
                }
            }
            SnTDMLSecurityUtil.printDebugMessage('=====furtherHCPkeyset:::::::::' +furtherHCPkeyset);

            if(inactiveHCPkey != null)
            {
                for(String inactivekey : inactiveHCPkey)
                {
                    SnTDMLSecurityUtil.printDebugMessage('=====inactivekey:::::::::' +inactivekey);
                    if(!furtherHCPkeyset.contains(inactivekey))
                    {
                        SnTDMLSecurityUtil.printDebugMessage('=====furtherHCPkeyset does not contain inactivekey:::::::::' +inactivekey);
                        inactiveHCPkey.remove(inactivekey);
                    }
                }
            }
            SnTDMLSecurityUtil.printDebugMessage('=====inactiveHCPkey after removing:::::::::' +inactiveHCPkey);

            SnTDMLSecurityUtil.printDebugMessage('====================Handling Position Accounts of affiliated inactive HCP==========================================================');
            List<AxtriaSalesIQTM__Position_Account__c> inactiveaffhcpPosAccList = new List<AxtriaSalesIQTM__Position_Account__c>();

            if(affHCPposAccList != null)
            {
                for(AxtriaSalesIQTM__Position_Account__c inactiveAffHCPRec : affHCPposAccList)
                {
                    String tempKey = inactiveAffHCPRec.AxtriaSalesIQTM__Account__r.Merge_Account_Number__c + '_' + inactiveAffHCPRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' +inactiveAffHCPRec.AxtriaSalesIQTM__Team_Instance__r.Name;
                    if(inactiveHCPkey.contains(tempKey))
                    {
                        inactiveAffHCPRec.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addDays(-1);

                        if(flag == true)
                        {
                            inactiveaffhcpPosAccList.add(inactiveAffHCPRec);
                        }
                        else if(flag== false)
                        {
                            if(inactiveAffHCPRec.IsGasAssignment__c == false)
                            {
                                inactiveaffhcpPosAccList.add(inactiveAffHCPRec);
                            }
                        }

                        SnTDMLSecurityUtil.printDebugMessage('========inactiveaffhcpPosAccList::::::::::' +inactiveaffhcpPosAccList);
                    }
                }
            }

            SnTDMLSecurityUtil.printDebugMessage('=====inactiveaffhcpPosAccList.size():::::::::' +inactiveaffhcpPosAccList.size());
            SnTDMLSecurityUtil.printDebugMessage('=====inactiveaffhcpPosAccList:::::::::' +inactiveaffhcpPosAccList);

            if(inactiveaffhcpPosAccList.size() > 0){
                //update inactiveaffhcpPosAccList;
                SnTDMLSecurityUtil.updateRecords(inactiveaffhcpPosAccList, 'BatchMergeTopDownHandling');  
            }

        }

        SnTDMLSecurityUtil.printDebugMessage('=========Handling Top Down------ Input is HCP::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::');

        if(inputHCPset != null)
        {
            SnTDMLSecurityUtil.printDebugMessage('===========Querying Position Account for Deassign Account-Position Set For HCP================================================');
            List<AxtriaSalesIQTM__Position_Account__c> deassignHCPPosAccList = [select Id,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.Merge_Account_Number__c,IsGasAssignment__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__c,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c from AxtriaSalesIQTM__Position_Account__c where (AxtriaSalesIQTM__Assignment_Status__c = 'Active' or AxtriaSalesIQTM__Assignment_Status__c = 'Future Active') and AxtriaSalesIQTM__Position__c!=null and AxtriaSalesIQTM__Account__c!=null and AxtriaSalesIQTM__Team_Instance__c!=null and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c in :deassignHCPPosSet and AxtriaSalesIQTM__Account__r.Merge_Account_Number__c in :inputHCPset and AxtriaSalesIQTM__Team_Instance__r.Name in :deassignHCPTeamInsSet];
          
            SnTDMLSecurityUtil.printDebugMessage('=====deassign HCP Position Account=====' +deassignHCPPosAccList.size());

            //List<AxtriaSalesIQTM__Position_Account__c> deassignPosAccList = new List<AxtriaSalesIQTM__Position_Account__c>();

            if(deassignHCPPosAccList != null)
            {
                for(AxtriaSalesIQTM__Position_Account__c deassignPA : deassignHCPPosAccList)
                {
                    String key = deassignPA.AxtriaSalesIQTM__Account__r.Merge_Account_Number__c + '_' +deassignPA.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' + deassignPA.AxtriaSalesIQTM__Team_Instance__r.Name;
                    if(deassignHCPKey.contains(key))
                    {
                        
                        
                        deassignPA.AxtriaSalesIQTM__Effective_End_Date__c=Date.today().addDays(-1);

                        if(flag == true)
                        {
                            deassignPosAccList.add(deassignPA);
                        }
                        else if(flag== false)
                        {
                            if(deassignPA.IsGasAssignment__c == false)
                            {
                                deassignPosAccList.add(deassignPA);
                            }
                        }


                    }
                }
              }
          }

        SnTDMLSecurityUtil.printDebugMessage('deassignPosAccList:::::::' +deassignPosAccList);
        SnTDMLSecurityUtil.printDebugMessage('deassignPosAccList.size():::::::' +deassignPosAccList.size());

        if(deassignPosAccList.size() > 0){
            //update deassignPosAccList;
            SnTDMLSecurityUtil.updateRecords(deassignPosAccList, 'BatchMergeTopDownHandling');
        }

        SnTDMLSecurityUtil.printDebugMessage('deassignList:::::' +deassignList);
        SnTDMLSecurityUtil.printDebugMessage('deassignList.size():::::::' +deassignList.size());


        if(deassignList.size() > 0){
            //update deassignList;
            SnTDMLSecurityUtil.updateRecords(deassignList, 'BatchMergeTopDownHandling');
        }


        /*SnTDMLSecurityUtil.printDebugMessage('Inactive affiliation Record for Looser==================================================================================');

            String query2 = 'SELECT id, AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.Merge_Account_Number__c, AxtriaSalesIQTM__Account__r.Status__c,Account_Number__c,Parent_Account_Number__c,AxtriaSalesIQTM__Affiliation_Network__c,Country__c,IsDeleted,AxtriaSalesIQTM__Parent_Account__c,AxtriaSalesIQTM__Root_Account__c,Affiliation_Status__c,Affiliation_End_Date__c,AxtriaSalesIQTM__Active__c FROM   AxtriaSalesIQTM__Account_Affiliation__c where AxtriaSalesIQTM__Account__r.Merge_Account_Number__c IN:looserSet or AxtriaSalesIQTM__Parent_Account__r.Merge_Account_Number__c IN:looserSet';

            List<AxtriaSalesIQTM__Account_Affiliation__c> losAccAffList2 =  Database.query(query2);

            if(losAccAffList2!=null && losAccAffList2.size() >0)
            {
                for(AxtriaSalesIQTM__Account_Affiliation__c af: losAccAffList2) 
                {
                    af.Affiliation_Status__c = 'Inactive';
                    af.Affiliation_End_Date__c = Date.today().addDays(-1);
                    af.AxtriaSalesIQTM__Active__c = false;
                }
                update losAccAffList2;   
            }

        list<Account>updateAclist = new list<Account>();
        set<String> lossingaccounts = new set<String>();
        list<Account> accList = [Select Id from Account where Merge_Account_Number__c in :looserSet];
        if(accList != null)
        {
            for(Account acc : accList)
            {
                lossingaccounts.add(acc.Id);   
            }
        }


        if(lossingaccounts!=null &&  lossingaccounts.size()>0)
        {
          for(string sfdcid : lossingaccounts)
          {
            if(sfdcid!=null)
            {
                    Account acc = new Account(id=sfdcid);
                    SnTDMLSecurityUtil.printDebugMessage('==========DEACTIVATING ACCOUNTS::'+acc);
                    acc.Status__c = 'Inactive';
                    acc.AxtriaSalesIQTM__Active__c = 'Inactive';
                    acc.Active__c = 'Inactive';
                    updateAclist.add(acc);
                    SnTDMLSecurityUtil.printDebugMessage('acc:::::' +acc);
              }
          }
        }

          if(updateAclist!=null && updateAclist.size()>0)
             Update updateAclist;*/

    }

    global void finish(Database.BatchableContext BC) {

            // SnTDMLSecurityUtil.printDebugMessage('=========Calling Inactive Account and Affiliation Batch:::::::::::::::::::::::::::');
            // BatchMergeInactiveAffAcc acc = new BatchMergeInactiveAffAcc(allLooserSet);
            // Database.executeBatch(acc,2);
        if(setBUDeassignID != null)
        {
            SnTDMLSecurityUtil.printDebugMessage('=========Calling Deassignment Position Account Batch for Bottom Up Rule Type:::::::::::::::::::::::::::');
            BatchMergeBottomUpHandling buPosAcc = new BatchMergeBottomUpHandling(setBUDeassignID,true,allLooserSet);
            Database.executeBatch(buPosAcc,1);
        }
        else
        {
            SnTDMLSecurityUtil.printDebugMessage('=========Calling Inactive Account and Affiliation Batch:::::::::::::::::::::::::::');
            BatchMergeInactiveAffAcc acc = new BatchMergeInactiveAffAcc(allLooserSet);
            Database.executeBatch(acc,2);
        }
    }
}