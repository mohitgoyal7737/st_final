global with sharing class update_AccTerr_New1 implements Database.Batchable<sObject>, Database.Stateful
{
    /*Replaced temp_acc_Terr__c with temp_obj__c due to object purge*/
    global string query ;
    global string teamID;
    global string teamInstance;
    global string teamName;
    String Ids;
    Boolean flag = true;
    global list<AxtriaSalesIQTM__Team__c> temlst = new list<AxtriaSalesIQTM__Team__c>();
    global list<temp_Obj__c> accTerrlist = new list<temp_Obj__c>();
    Global Id BC {get; set;}
    global string country;
    global list<AxtriaSalesIQTM__Team_Instance__c> countrylst = new list<AxtriaSalesIQTM__Team_Instance__c>();
    public List<AxtriaSalesIQTM__Change_Request__c> cr = new List<AxtriaSalesIQTM__Change_Request__c>();
    public Integer recordsCreated;

    //Added by HT(A0994) on 17th June 2020
    global String changeReqID;
    global Boolean flagValue = false;

    global update_AccTerr_New1(String Team, String TeamIns)
    {
       }

    global update_AccTerr_New1(String Team, String TeamIns, String Ids)
    {
        }

    //Added by HT(A0994) on 17th June 2020
    global update_AccTerr_New1(String Team, String TeamIns, String Ids, Boolean flag)
    {
      
    }

    global Database.Querylocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator(query);
    }

    global void execute (Database.BatchableContext BC, List<temp_Obj__c>accTerrlist)
    {
      
        }

    global void finish(Database.BatchableContext BC)
    {   
        
    }
}