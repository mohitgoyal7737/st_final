@isTest
public class updatePosGeoSharing_Test {
     
    
    public static testMethod void test()  {
        
          
        AxtriaSalesIQTM__TriggerContol__c  triggerctrl =  new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false , Name='updatePosGeoSharing');
        insert triggerctrl;
         
 

         List<AxtriaSalesIQTM__Geography__c> geo = new List<AxtriaSalesIQTM__Geography__c>();
         for(Integer i = 1; i<10 ; i++){
            AxtriaSalesIQTM__Geography__c zip = new AxtriaSalesIQTM__Geography__c();
            zip.name = '0000'+i;
            geo.add(zip);
        
    }
        
        insert geo;
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(name = 'T1');
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_instance__c();
        ti.name = 'T1-Q1';
        ti.AxtriaSalesIQTM__Team__c = [Select id from AxtriaSalesIQTM__Team__c where name = 'T1'][0].Id;
        ti.AxtriaSalesIQTM__IC_EffstartDate__c = date.today() - 20 ;
        ti.AxtriaSalesIQTM__IC_EffEndDate__c = date.today() + 100 ;
        insert ti;
        
        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();
        pos.name = 'XYZ';
        pos.AxtriaSalesIQTM__Team_iD__c = [Select id from AxtriaSalesIQTM__Team__c where name = 'T1'][0].Id;
        pos.AxtriaSalesIQTM__Team_Instance__c = [Select id from AxtriaSalesIQTM__Team_Instance__c where name = 'T1-Q1'][0].Id;
        pos.AxtriaSalesIQTM__Client_Position_Code__c = 'X';
        
        insert pos;
        
        AxtriaSalesIQTM__Position_Team_Instance__c pti = new AxtriaSalesIQTM__Position_Team_Instance__c(); 
        pti.AxtriaSalesIQTM__Team_Instance_ID__c = ti.id;
        pti.AxtriaSalesIQTM__Position_ID__c = pos.id;
        pti.AxtriaSalesIQTM__Effective_Start_Date__c =date.today() - 20;
        pti.AxtriaSalesIQTM__Effective_End_Date__c= date.today() + 20;
        insert pti;
    
   // AxtriaSalesIQTM__Workspace__c newwrkspcae = createWorkspace('workspacenew',date.today() - 20,date.today() + 20);
    
        //public static AxtriaSalesIQTM__Workspace__c createWorkspace(String workspaceName, Date startDate, Date endDate) {
        AxtriaSalesIQTM__Workspace__c workspace = new AxtriaSalesIQTM__Workspace__c();
        workspace.Name = 'workspaceName';
        workspace.AxtriaSalesIQTM__Workspace_Description__c = 'Some Description';
        workspace.AxtriaSalesIQTM__Workspace_Start_Date__c = date.today() - 20;
        workspace.AxtriaSalesIQTM__Workspace_End_Date__c = date.today() + 20;
        insert workspace;
       // return workspace ;
    

    //public static AxtriaSalesIQTM__Scenario__c createScenario(Id workspaceId, Id scenarioTeamInstanceId, Id sourceTeamInstanceId, String stage, String processStage, String status, Date startDate, Date endDate) {
        AxtriaSalesIQTM__Scenario__c scenario = new AxtriaSalesIQTM__Scenario__c();
       // if(scenarioTeamInstanceId != null)
        //    scenario.AxtriaSalesIQTM__Team_Instance__c = scenarioTeamInstanceId;
       // if(sourceTeamInstanceId != null)
      //scenario.AxtriaSalesIQTM__Source_Team_Instance__c = sourceTeamInstanceId;
        scenario.AxtriaSalesIQTM__Effective_Start_Date__c = date.today() - 20;
        scenario.AxtriaSalesIQTM__Effective_End_Date__c = date.today() + 20;
        scenario.AxtriaSalesIQTM__Scenario_Stage__c = 'Design';
        scenario.AxtriaSalesIQTM__Request_Process_Stage__c = 'Delete In Queue';
        scenario.AxtriaSalesIQTM__Scenario_Status__c = 'Active';
        scenario.AxtriaSalesIQTM__Workspace__c = workspace.Id;
        insert scenario;
        //return scenario ;
          
        
        test.startTest();
        AxtriaSalesIQTM__Scenario__c scn= new AxtriaSalesIQTM__Scenario__c(id=scenario.id);
        //AxtriaSalesIQTM__Scenario__c newobj=  createScenario('','','')
        scn.AxtriaSalesIQTM__Request_Process_Stage__c = 'CIM Ready';
        update  scn;
       
        test.stopTest();
       
    } 

}