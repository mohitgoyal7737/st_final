public with sharing class CenterHospitalViewCtrl {
  //Removed occurance of Line__c due to object purge activity A1422
   String buName;
   String lineName;
   public List<ShowSegmentData> allAdoptionData {get;set;}
   public String allData {get;set;}
   public String userLocale {get;set;}
   public List<SelectOption> allHospitals {get;set;}
   public String hospitalName {get;set;}
   public String selectedPosition;
   public String brandName;
   public String fileDelimiter {get;set;}

   public CenterHospitalViewCtrl() 
   {
      allHospitals = new List<SelectOption>();
      hospitalName = 'All';

      userLocale = UserInfo.getLocale();
      system.debug('@@@@@@@@@qwqw' + userLocale);
      //List<Country__c> countryInfo = [select Delimiter__c from Country__c where name =: userLocale];//Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c
      List<Country_wise_Delimiter__mdt> countryInfo = [select Delimiter__c,Numeric_Delimiter__c from Country_wise_Delimiter__mdt where DeveloperName =: userLocale];//Shivansh - A1450
      
      if(countryInfo.size()>0)
      {
          fileDelimiter = countryInfo[0].Delimiter__c;
      }
      else
      {
          fileDelimiter = ',';
      }

      

      buName = ApexPages.currentPage().getParameters().get('buSelectedName'); 
      lineName = ApexPages.currentPage().getParameters().get('lineSelectedName');
      brandName = ApexPages.currentPage().getParameters().get('brandSelectedName');

      system.debug('++++++++ Brand Name '+ brandName);
      //system.debug((brandName.split(',')).size());
      getAllPosAdoptionMap();
      
   }
   

   public void getAllPosAdoptionMap()
   {
        List<AxtriaSalesIQTM__User_Access_Permission__c> allUserAccessPerm = [select AxtriaSalesIQTM__Position__c from AxtriaSalesIQTM__User_Access_Permission__c where AxtriaSalesIQTM__User__c = :UserInfo.getUserId()/* and AxtriaSalesIQTM__Position__r.Line__r.Name = :lineName*/];
        selectedPosition = '';

        if(allUserAccessPerm.size() > 0 )
        {
            selectedPosition = allUserAccessPerm[0].AxtriaSalesIQTM__Position__c;
        }


        system.debug('++++++++++++ Hey Selected Position is '+ selectedPosition);
        system.debug('++++++++++++ Hey Selected lineName is '+ lineName);

        getAllHospitalsInfo();
        getAllInfo();
    }

   public void getAllHospitalsInfo()
   {
      List<AggregateResult> allPosAccDistintRecs = [select AxtriaSalesIQTM__Account__r.Parent_Name__c parentName from AxtriaSalesIQTM__Position_Account_Call_Plan__c where (AxtriaSalesIQTM__Position__c = :selectedPosition or AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__c = :selectedPosition or AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__c =:selectedPosition or AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__c =:selectedPosition) and AxtriaSalesIQTM__Position__c!=''and /*Line__r.Name = :lineName and*/ AxtriaSalesIQTM__lastApprovedTarget__c = true and AxtriaSalesIQTM__Account__r.Parent_Name__c != null group by AxtriaSalesIQTM__Account__r.Parent_Name__c];

      allHospitals.add(new SelectOption('All', 'All'));

      for(AggregateResult aggResult : allPosAccDistintRecs)
      {
         String hospName = String.valueof(aggResult.get('parentName'));
         allHospitals.add(new SelectOption(hospName, hospName));
      }
   }

   public void getAllInfo()
   {
      List<AggregateResult> allRecsSum;
      allData = '';
      system.debug('++++++++++++++ hospitalName' + hospitalName);

      Integer sumTotalTCF = 0;
      Integer totalHCP = 0;
      decimal totalfreq = 0;
      decimal percHCP = 0;
      allAdoptionData = new List<ShowSegmentData>();

      if(hospitalName == 'All')
      {
         system.debug('Inside Same Hospital');
         if(brandName != null)
         {
             if((brandName.split(',')).size()>1)
             allRecsSum = [select Segment__c name, count(id) countRecs, sum(Final_TCF_Approved__c) countAppro from AxtriaSalesIQTM__Position_Account_Call_Plan__c where  (AxtriaSalesIQTM__Position__c = :selectedPosition or AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__c = :selectedPosition or AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__c =:selectedPosition or AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__c =:selectedPosition) and AxtriaSalesIQTM__Position__c!=''/*and Line__r.Name = :lineName*/ and AxtriaSalesIQTM__lastApprovedTarget__c = true group by Segment__c];         
             else
                allRecsSum = [select Segment__c name, count(id) countRecs, sum(Final_TCF_Approved__c) countAppro from AxtriaSalesIQTM__Position_Account_Call_Plan__c where  (AxtriaSalesIQTM__Position__c = :selectedPosition or AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__c = :selectedPosition or AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__c =:selectedPosition or AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__c =:selectedPosition) and AxtriaSalesIQTM__Position__c!=''/*and Line__r.Name = :lineName*/ and AxtriaSalesIQTM__lastApprovedTarget__c = true and P1__c = :brandName group by Segment__c];             
         }

      }
      else
      {
         system.debug('Inside Different Hospital');
         
         if((brandName.split(',')).size()>1)
         allRecsSum = [select Segment__c name, count(id) countRecs, sum(Final_TCF_Approved__c) countAppro from AxtriaSalesIQTM__Position_Account_Call_Plan__c where  (AxtriaSalesIQTM__Position__c = :selectedPosition or AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__c = :selectedPosition or AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__c =:selectedPosition or AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__c =:selectedPosition) and AxtriaSalesIQTM__Position__c!=''/*and Line__r.Name = :lineName */and AxtriaSalesIQTM__lastApprovedTarget__c = true and  AxtriaSalesIQTM__Account__r.Parent_Name__c = :hospitalName group by Segment__c];  
         else
            allRecsSum = [select Segment__c name, count(id) countRecs, sum(Final_TCF_Approved__c) countAppro from AxtriaSalesIQTM__Position_Account_Call_Plan__c where  (AxtriaSalesIQTM__Position__c = :selectedPosition or AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__c = :selectedPosition or AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__c =:selectedPosition or AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__c =:selectedPosition) and AxtriaSalesIQTM__Position__c!=''/*and Line__r.Name = :lineName*/ and AxtriaSalesIQTM__lastApprovedTarget__c = true and  AxtriaSalesIQTM__Account__r.Parent_Name__c = :hospitalName and P1__c = :brandName group by Segment__c];  
      }

      system.debug('+++++++++ Hey ');
      system.debug(allRecsSum);

        for(AggregateResult agg : allRecsSum)
        {
            sumTotalTCF = sumTotalTCF + Integer.valueof(agg.get('countAppro'));
        }

        for(AggregateResult agg : allRecsSum)
        {
            String segmentName = String.valueof(agg.get('name'));
            Integer totoalCount = Integer.valueof(agg.get('countRecs'));
            Integer sumTCF = Integer.valueof(agg.get('countAppro'));
            Decimal avgTCF = 0;

            if(totoalCount !=0)
            avgTCF  = ((sumTCF * 1.0)/totoalCount).setScale(1);

            Decimal percTCF = 0;
            if(sumTotalTCF !=0)
            percTCF = ((sumTCF * 100.0)/sumTotalTCF).setScale(1);
  
            String avgTCFString = String.valueOf(avgTCF);
            avgTCFString= avgTCFString.replace('.', ',');
            String percTCFString = String.valueOf(percTCF);
           percTCFString= percTCFString.replace('.', ',');

            totalHCP = totalHCP+ totoalCount;

            percHCP =percHCP +percTCF;

            allAdoptionData.add(new ShowSegmentData(segmentName, totoalCount, sumTCF, avgTCFString, percTCFString));
        }
       
       
         if(totalHCP != 0)
         totalfreq = ((sumTotalTCF * 1.0)/totalHCP).setScale(1);
         
       
        
        String totalfreqString = String.valueOf(totalfreq);
            totalfreqString= totalfreqString.replace('.', ',');
            string percTCFString = '100';

        allAdoptionData.add(new ShowSegmentData('Totale complessivo', totalHCP, sumTotalTCF, totalfreqString, percTCFString));
       if(!Test.isRunningTest()) 
       allData = JSON.serialize(allAdoptionData);
      system.debug('Hey All Data is ');
      system.debug(allData);
   }
   public class ShowSegmentData
   {
    public String segmentName {get;set;}
    public Integer totalCount  {get;set;} 
    public Integer sumTCF {get;set;} 
    public String avgTCFString {get;set;}
    public string percTCFString {get;set;} 
    
    public ShowSegmentData(String segmentName,Integer totalCount ,Integer sumTCF ,String avgTCFString ,string percTCFString)
    {
        this.segmentName = segmentName;
        this.totalCount = totalCount;
        this.sumTCF = sumTCF;
        this.avgTCFString = avgTCFString;
        this.percTCFString = percTCFString;
    }
   }
}