@isTest
private class Batch_ObjectTerritoryMapping_Test {
    static testMethod void testMethod1() {
        
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__AccountType__c ='HCO';
        acc.AccountNumber = '12345';
        insert acc;
        
        AxtriaSalesIQTM__Organization_Master__c orgmas1 = TestDataFactory.createOrganizationMaster();
        orgmas1.Name = 'SnT';
        insert orgmas1;
        
        AxtriaSalesIQTM__Employee__c emp = TestDataFactory.createEmployee('test','test');
        insert emp;
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(); 
        country.Load_Type__c = 'Full Load';
        country.AxtriaSalesIQTM__Parent_Organization__c = orgmas1.id;
        country.AxtriaSalesIQTM__Status__c = 'Active';
        insert country; 
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(country);
        team.Name = 'ONCO';        
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        insert teamins;
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        pos.AxtriaSalesIQTM__Client_Position_Code__c = 'N003';
        pos.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        pos.AxtriaSalesIQTM__Effective_Start_Date__c= date.today();
        pos.AxtriaSalesIQTM__Inactive__c = false;
        pos.AxtriaSalesIQTM__Team_iD__c = team.Id;
        pos.AxtriaSalesIQTM__Employee__c = emp.id;
        //pos.AxtriaSalesIQTM__Parent_Position__c = pos1.id;
        pos.AXTRIASALESIQTM__HIERARCHY_LEVEL__C = '1';
        //pos.AxtriaSalesIQTM__Position_Status__c = 'Active';
        insert pos;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
         
        
        Staging_Position_Account__c stagPosAcc = new Staging_Position_Account__c ();
        stagPosAcc.Assignment_Status__c = 'Active';
        stagPosAcc.Record_Status__c = 'Updated';
        stagPosAcc.PositionCode__c = 'test';
        stagPosAcc.AccountNumber__c = 'test';
        stagPosAcc.External_Id__c = 'test';
        stagPosAcc.Account__c = acc.id;
        stagPosAcc.Account_Type__c = 'test';
        stagPosAcc.Association_Cause__c = 'test';
        stagPosAcc.Country__c = country.id;
        stagPosAcc.Position__c = pos.id;
        insert stagPosAcc;
        
        Scheduler_Log__c sclogs= new Scheduler_Log__c();
        sclogs.Cycle__c = 'Full Load';
        sclogs.Job_Name__c = 'ObjectTerritory';
        sclogs.Country__c = country.id;
        insert sclogs;
        
        
        /*System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            Batch_ObjectTerritoryMapping o3=new Batch_ObjectTerritoryMapping();
            //o3.query = 'select id, P1__c, AxtriaSalesIQTM__Segment10__c, Segment_Approved__c, AxtriaSalesIQTM__Account__r.AZ_VeevaID__c , AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__External_Account_Number__c, AxtriaSalesIQTM__Account__r.AccountNumber , Adoption__c, Potential__c, Country__c,AxtriaSalesIQTM__lastApprovedTarget__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,AxtriaSalesIQTM__Position__r.Country_Code_Formula__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c ';
            Database.executeBatch(o3);
        }*/
        
        Test.startTest();
        System.runAs(loggedInUser)
        {
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));

            Batch_ObjectTerritoryMapping o3=new Batch_ObjectTerritoryMapping();
            Database.executeBatch(o3);
        }
        Test.stopTest();
    }
}