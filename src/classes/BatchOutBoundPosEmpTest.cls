@isTest
private class BatchOutBoundPosEmpTest {
     @testSetup 
    static void setup() {
        Group gp = new Group();
        gp.Name = 'Global Admins';
        insert gp;
        Group gp1 = new Group();
        gp1.Name = 'Admins';
        insert gp1;
        AxtriaSalesIQTM__Organization_Master__c org1=new AxtriaSalesIQTM__Organization_Master__c();
        org1.name='AZ';
        org1.AxtriaSalesIQTM__Org_Level__c='Global';
        org1.AxtriaSalesIQTM__Parent_Country_Level__c=false;
        org1.AxtriaARSnT__Public_Group_SFDC_id__c = gp.id;
        org1.AxtriaARSnT__Public_Group__c = gp.name;
         
        insert org1;
        AxtriaSalesIQTM__Organization_Master__c org=new AxtriaSalesIQTM__Organization_Master__c();
        org.name='US';
        org.AxtriaSalesIQTM__Org_Level__c='Global';
        org.AxtriaSalesIQTM__Parent_Country_Level__c=true;
        org.AxtriaSalesIQTM__Parent_Organization_Name__c = org1.id;
        org.AxtriaARSnT__Public_Group_SFDC_id__c = gp1.id;
        org.AxtriaARSnT__Public_Group__c = gp1.name;
         
        insert org;
        
        String orgId=org.id;
        List<AxtriaSalesIQTM__Country__c> countrylist = new List<AxtriaSalesIQTM__Country__c>();
        List<AxtriaSalesIQTM__Country__c> country = new List<AxtriaSalesIQTM__Country__c>();
          country.add(new AxtriaSalesIQTM__Country__c(AxtriaSalesIQTM__Country_Code__c='US',AxtriaSalesIQTM__Parent_Organization__c=orgId, AxtriaSalesIQTM__Status__c='Active'));
        insert country;
        AxtriaSalesIQTM__Employee__c empList= new AxtriaSalesIQTM__Employee__c();
        empList.AxtriaSalesIQTM__FirstName__c='X';
        empList.AxtriaSalesIQTM__Last_Name__c='y';
         insert empList;
        String empId=empList.id;
        AxtriaSalesIQTM__Team__c t=new AxtriaSalesIQTM__Team__c();
        t.Name='xx';
        insert t;
        String teamId=t.id;
        AxtriaSalesIQTM__Team__c t1=new AxtriaSalesIQTM__Team__c();
        t1.Name='yy';
        insert t1;
        String parentteamId=t1.id;
        AxtriaSalesIQTM__Team_Instance__c ti=new AxtriaSalesIQTM__Team_Instance__c();
        ti.AxtriaSalesIQTM__Team__c=teamId;
        ti.AxtriaSalesIQTM__Alignment_Period__c='current';
        insert ti;
        String teamInstId=ti.id;
         AxtriaSalesIQTM__Position__c pos1=new AxtriaSalesIQTM__Position__c();
        pos1.AxtriaSalesIQTM__Team_iD__c=parentteamId;
        insert pos1;
        String parentposId=pos1.id;
        AxtriaSalesIQTM__Position__c pos=new AxtriaSalesIQTM__Position__c();
        pos.AxtriaSalesIQTM__Team_iD__c=teamId;
        pos.AxtriaSalesIQTM__Team_Instance__c=teamInstId;
        pos.AxtriaSalesIQTM__Parent_Position__c=parentposId;
        insert pos;
        String posId=pos.id;
        AxtriaSalesIQTM__Position_Employee__c PE=new AxtriaSalesIQTM__Position_Employee__c();
        pe.AxtriaSalesIQTM__Employee__c=empId;
        pe.AxtriaSalesIQTM__Position__c=posId;
        insert PE;
        
       
    }
    static testmethod void test() {
 AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
        team.Name = 'Specialty';
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamIns = new AxtriaSalesIQTM__Team_Instance__c();
        teamIns.AxtriaSalesIQTM__Team__c = team.id;
        teamIns.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        teamIns.Name = 'abc';
        insert teamIns;        
        Test.startTest();
        //purge
        /*BatchOutBoundPosEmp usa = new BatchOutBoundPosEmp();
        Id batchId = Database.executeBatch(usa);*/
      //  BatchOutBoundPositionProduct obj = new BatchOutBoundPositionProduct();
        
        Test.stopTest();
        // after the testing stops, assert records were updated 
    }
}