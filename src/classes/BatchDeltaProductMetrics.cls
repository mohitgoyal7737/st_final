global class BatchDeltaProductMetrics implements Database.Batchable<sObject>
 {
    public String query;
    public Date Lmd;
    public Set<String> selectedTeamInstances;
    public List<SIQ_Product_Metrics_vod_O__c> allProductMetricsData;
     public Map<String,String> detailGroupMap;
     public Set<String> allRecs;
     public Map<String,String> productToVeevaID;
     public List<Segment_Veeva_Mapping__c> segmentVeevaMapping;
    public Map<String,String> segmentVeevaMappingMap;
    public Map<String,String> AdoptionVeevaMappingMap;
    public Map<String,String> PotentialVeevaMappingMap;
    public List<SIQ_Product_Metrics_vod_O__c> prodList;
     public String cycle{get;set;}
       public Set<String> teamInsSet;
       public integer errorcount{get;set;}
       public String nonProcessedAcs {get;set;}
       public Integer recordsProcessed=0;
    public String batchID;
    global DateTime lastjobDate=null;
     public List<String> selectedTeamInstancesinit;
	public Boolean chain = false;

     global BatchDeltaProductMetrics(Date lastModifiedDate)
    {

      /*lmd= lastModifiedDate;
      system.debug('+++Lmd++'+Lmd);
      query='Select id, P1__c, Segment__c, AxtriaSalesIQTM__Account__r.AZ_VeevaID__c , AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c,AxtriaSalesIQTM__Account__r.AccountNumber ,Adoption__c, Potential__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,Country__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, Accessibility_Range__c, Party_ID__r.Accessibility_Range__c,AxtriaSalesIQTM__lastApprovedTarget__c, AxtriaSalesIQTM__Team_Instance__c ,AxtriaSalesIQTM__Segment10__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c WHERE LastModifiedDate= Last_N_Days:1 and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = \'Current\' and (AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = \'Live\'OR AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = \'Published\')';
      detailGroupMap=new Map<String,String>();
      selectedTeamInstances = new Set<String>();*/
      
    }
      global BatchDeltaProductMetrics(Date lastModifiedDate,List<String> selectedTeamInstancesinit)
    {
        lmd= lastModifiedDate;
        system.debug('+++Lmd++'+Lmd);
        List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
        List<AxtriaSalesIQTM__Team_Instance__c> cycleList = new List<AxtriaSalesIQTM__Team_Instance__c>();
        cycleList=[Select Id,Name, Cycle__r.Name from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current' and Id in :teamInsSet];
        if(cycleList!=null)
          {
            for(AxtriaSalesIQTM__Team_Instance__c t1: cycleList)
              {
                if(t1.Cycle__r.Name !=null && t1.Cycle__r.Name !='')
                cycle = t1.Cycle__r.Name;
              }
          }         
        System.debug(cycle);
        schLogList=[Select Id,CreatedDate,Created_Date2__c from Scheduler_Log__c where Job_Name__c='Product Metrics Delta' and Job_Status__c='Successful'  Order By CreatedDate desc LIMIT 1];
            if(schLogList.size()>0)
            {
                lastjobDate=schLogList[0].Created_Date2__c.addDays(-1);  //set the lastjobDate to the last successfull batch job run if there exists an entry
            }
            else
            {
                lastjobDate=null;       //else we set the lastjobDate to null
            }
        System.debug('last job'+lastjobDate);
        Scheduler_Log__c sJob = new Scheduler_Log__c();        
        sJob.Job_Name__c = 'Product Metrics Delta';
        sJob.Job_Status__c = 'Failed';
        sJob.Job_Type__c='Outbound';
        if(cycle!=null && cycle!='')
           sJob.Cycle__c=cycle;
        sJob.Created_Date2__c = datetime.now();
        system.debug(datetime.now());
        system.debug(sJob);
        insert sJob;
        batchID = sJob.Id;
        recordsProcessed =0;

      if(lastjobDate == null )
        {
          query='Select id, P1__c, Segment__c, AxtriaSalesIQTM__Account__r.AZ_VeevaID__c , AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c,AxtriaSalesIQTM__Account__r.AccountNumber ,Adoption__c, Potential__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,Country__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, Accessibility_Range__c, Party_ID__r.Accessibility_Range__c,AxtriaSalesIQTM__lastApprovedTarget__c, AxtriaSalesIQTM__Team_Instance__c ,AxtriaSalesIQTM__Segment10__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c WHERE AxtriaSalesIQTM__Team_Instance__c in :selectedTeamInstancesinit';
        }
      else
       {
          query='Select id, P1__c, Segment__c, AxtriaSalesIQTM__Account__r.AZ_VeevaID__c , AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c,AxtriaSalesIQTM__Account__r.AccountNumber ,Adoption__c, Potential__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,Country__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, Accessibility_Range__c, Party_ID__r.Accessibility_Range__c,AxtriaSalesIQTM__lastApprovedTarget__c, AxtriaSalesIQTM__Team_Instance__c ,AxtriaSalesIQTM__Segment10__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c WHERE lastModifiedDate >:lastjobDate and AxtriaSalesIQTM__Team_Instance__c in :selectedTeamInstancesinit';
        }

        detailGroupMap=new Map<String,String>();
        selectedTeamInstances = new Set<String>();
        this.selectedTeamInstancesinit = new List<String>(selectedTeamInstancesinit);
    }
     
     global BatchDeltaProductMetrics(Date lastModifiedDate,List<String> selectedTeamInstancesinit, Boolean chaining)
    {
		    chain = chaining;
        lmd= lastModifiedDate;
        system.debug('+++Lmd++'+Lmd);
        List<Scheduler_Log__c> schLogList = new List<Scheduler_Log__c>();
        List<AxtriaSalesIQTM__Team_Instance__c> cycleList = new List<AxtriaSalesIQTM__Team_Instance__c>();
        cycleList=[Select Id,Name, Cycle__r.Name from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Alignment_Period__c ='Current' and Id in :teamInsSet];
          if(cycleList!=null)
            {
              for(AxtriaSalesIQTM__Team_Instance__c t1: cycleList)
              {
                  if(t1.Cycle__r.Name !=null && t1.Cycle__r.Name !='')
                  cycle = t1.Cycle__r.Name;
              }
            }         
        System.debug(cycle);
        schLogList=[Select Id,CreatedDate,Created_Date2__c from Scheduler_Log__c where Job_Name__c='Product Metrics Delta' and Job_Status__c='Successful'  Order By CreatedDate desc LIMIT 1];
            if(schLogList.size()>0)
            {
                lastjobDate=schLogList[0].Created_Date2__c.addDays(-1);  //set the lastjobDate to the last successfull batch job run if there exists an entry
            }
            else
            {
                lastjobDate=null;       //else we set the lastjobDate to null
            }
        System.debug('last job'+lastjobDate);
        Scheduler_Log__c sJob = new Scheduler_Log__c();        
        sJob.Job_Name__c = 'Product Metrics Delta';
        sJob.Job_Status__c = 'Failed';
        sJob.Job_Type__c='Outbound';
        if(cycle!=null && cycle!='')
           sJob.Cycle__c=cycle;
        sJob.Created_Date2__c = datetime.now();
        system.debug(datetime.now());
        system.debug(sJob);
        insert sJob;
        batchID = sJob.Id;
        recordsProcessed =0;
      if(lastjobDate == null )
        {
          query='Select id, P1__c, Segment__c, AxtriaSalesIQTM__Account__r.AZ_VeevaID__c , AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c,AxtriaSalesIQTM__Account__r.AccountNumber ,Adoption__c, Potential__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,Country__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, Accessibility_Range__c, Party_ID__r.Accessibility_Range__c,AxtriaSalesIQTM__lastApprovedTarget__c, AxtriaSalesIQTM__Team_Instance__c ,AxtriaSalesIQTM__Segment10__c,AxtriaARSnT__Segment_Approved__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c WHERE AxtriaSalesIQTM__Team_Instance__c in :selectedTeamInstancesinit';
        }
      else
       {
          query='Select id, P1__c, Segment__c, AxtriaSalesIQTM__Account__r.AZ_VeevaID__c , AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c,AxtriaSalesIQTM__Account__r.AccountNumber ,Adoption__c, Potential__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,Country__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, Accessibility_Range__c, Party_ID__r.Accessibility_Range__c,AxtriaSalesIQTM__lastApprovedTarget__c, AxtriaSalesIQTM__Team_Instance__c ,AxtriaSalesIQTM__Segment10__c,AxtriaARSnT__Segment_Approved__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c WHERE lastModifiedDate >:lastjobDate and AxtriaSalesIQTM__Team_Instance__c in :selectedTeamInstancesinit';
        }
        detailGroupMap=new Map<String,String>();
        selectedTeamInstances = new Set<String>();
        this.selectedTeamInstancesinit = new List<String>(selectedTeamInstancesinit);
    }


    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List< AxtriaSalesIQTM__Position_Account_Call_Plan__c> scope)
    {
        system.debug('++query++'+query);
        segmentVeevaMapping= new List<Segment_Veeva_Mapping__c>();
        segmentVeevaMappingMap= new Map<String, String>();
        allRecs= new Set<String>();
        productToVeevaID=new Map<String,String>();
        prodList=new List<SIQ_Product_Metrics_vod_O__c> ();

        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacpTeaminstance : scope)
        {
          selectedTeamInstances.add(pacpTeaminstance.AxtriaSalesIQTM__Team_Instance__c);
        }
          system.debug('++selectedTeamInstances++'+selectedTeamInstances);

        segmentVeevaMapping = [select id, Segment_Axtria__c, Segment_Veeva__c,Adoption_Axtria__c,Adoption_Veeva__c,Potential_Axtria__c,Potential_Veeva__c, Team_Instance__c from Segment_Veeva_Mapping__c where Team_Instance__c in :selectedTeamInstances];

        for(Segment_Veeva_Mapping__c svm : segmentVeevaMapping)
        {
          segmentVeevaMappingMap.put(svm.Segment_Axtria__c+ '_' + svm.Team_Instance__c, svm.Segment_Veeva__c);
          AdoptionVeevaMappingMap.put(svm.Adoption_Axtria__c+ '_' + svm.Team_Instance__c, svm.Adoption_Veeva__c);
          PotentialVeevaMappingMap.put(svm.Potential_Axtria__c+ '_' + svm.Team_Instance__c, svm.Potential_Veeva__c);
        }
        system.debug('+++segmentVeevaMappingMap++'+segmentVeevaMappingMap);
        system.debug('+++segmentVeevaMapping++'+segmentVeevaMapping);

        List<Product_Catalog__c> allProducts = [Select Id, Veeva_External_ID__c, Country__c,Detail_Group__c,  Name from Product_Catalog__c where Team_Instance__c in :selectedTeamInstances and AxtriaARSnT__IsActive__c=true];

        for(Product_Catalog__c pc : allProducts)
        {
          productToVeevaID.put(pc.Name + '_' + pc.Country__c , pc.Veeva_External_ID__c);
          if(pc.Detail_Group__c != null)
          detailGroupMap.put(pc.Name + '_' + pc.Country__c , pc.Detail_Group__c);
        }
        system.debug('++allProducts++'+allProducts);
        system.debug('++productToVeevaID++'+productToVeevaID);
        system.debug('++detailGroupMap++'+detailGroupMap);
        allProductMetricsData = new List<SIQ_Product_Metrics_vod_O__c>();

        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : scope)
        {
          SIQ_Product_Metrics_vod_O__c spm = new SIQ_Product_Metrics_vod_O__c();

          if(pacp.AxtriaSalesIQTM__lastApprovedTarget__c==true)
          {       
            system.debug('++inside if++');
            spm.SIQ_Account_vod__c = pacp.AxtriaSalesIQTM__Account__r.AccountNumber;
            spm.SIQ_Account_Number__c = pacp.AxtriaSalesIQTM__Account__r.AccountNumber;
            if(AdoptionVeevaMappingMap.containsKey(pacp.Adoption__c+'_'+pacp.AxtriaSalesIQTM__Team_Instance__c))
            {
                spm.SIQ_Adoption_AZ__c = AdoptionVeevaMappingMap.get(pacp.Adoption__c + '_'+pacp.AxtriaSalesIQTM__Team_Instance__c);
            }
            else
            {
                spm.SIQ_Adoption_AZ__c = pacp.Adoption__c;
            }

            if(PotentialVeevaMappingMap.containsKey(pacp.Potential__c+'_'+pacp.AxtriaSalesIQTM__Team_Instance__c))
            {
                spm.SIQ_Potential_AZ__c = PotentialVeevaMappingMap.get(pacp.Potential__c+'_'+pacp.AxtriaSalesIQTM__Team_Instance__c);
            }
            else
            {
                spm.SIQ_Potential_AZ__c = pacp.Potential__c;
            }
            if(pacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c=='No Cluster')
            {
              spm.SIQ_Country__c = pacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
            }
            else if(pacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c=='SalesIQ Cluster')
            {
              spm.SIQ_Country__c = pacp.AxtriaSalesIQTM__Position__r.Country_Code_Formula__c;
            }
            else if (pacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c=='Veeva Cluster')
            {
              spm.SIQ_Country__c = pacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c;
            }
            spm.SIQ_Product_Name__c = pacp.P1__c;
            spm.Siq_Status__c ='Updated';
            spm.Detail_Group__c = detailGroupMap.get(pacp.P1__c + '_' + pacp.Country__c);
            system.debug('+++spm.SIQ_Product_Name__c++'+spm.SIQ_Product_Name__c);
            system.debug('+++pacp.P1__c++'+pacp.P1__c);
            if(segmentVeevaMappingMap.containsKey(pacp.AxtriaARSnT__Segment_Approved__c +'_'+pacp.AxtriaSalesIQTM__Team_Instance__c))
            {
            spm.SIQ_Segment__c  = segmentVeevaMappingMap.get(pacp.AxtriaARSnT__Segment_Approved__c +'_'+pacp.AxtriaSalesIQTM__Team_Instance__c);
            }
            else
            {
            spm.SIQ_Segment__c  = pacp.AxtriaARSnT__Segment_Approved__c;    
            }
          }
          else
          {
            system.debug('++else++');
            if(pacp.AxtriaSalesIQTM__Segment10__c == 'Inactive')
            {
            spm.SIQ_Status__c = 'Deleted';
            }
            if(pacp.AxtriaSalesIQTM__Segment10__c == 'Loser Account')
            {
            spm.SIQ_Segment__c   = '';
            spm.SIQ_Status__c = 'Updated';
            } 
          }
          spm.SIQ_Products_vod__c = productToVeevaID.get(pacp.P1__c + '_' + pacp.Country__c);
          system.debug('spm.SIQ_Products_vod__c'+spm.SIQ_Products_vod__c);
          if(spm.Detail_Group__c!= null)
          spm.SIQ_External_ID__c =  pacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c +'_'+ pacp.AxtriaSalesIQTM__Account__r.AccountNumber+'_'+ spm.SIQ_Products_vod__c + '_' + spm.Detail_Group__c;
          else
          spm.SIQ_External_ID__c = pacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c +'_'+ pacp.AxtriaSalesIQTM__Account__r.AccountNumber+'_'+ spm.SIQ_Products_vod__c + '_';
          system.debug(' spm.SIQ_External_ID__c++'+ spm.SIQ_External_ID__c);  

          if(!allRecs.contains(spm.SIQ_External_ID__c) && pacp.P1__c != null)
          {
            allProductMetricsData.add(spm);
            system.debug('+++allProductMetricsData'+allProductMetricsData);
            allRecs.add(spm.SIQ_External_ID__c);
            system.debug('recordsProcessed+'+recordsProcessed);
            recordsProcessed++;
          }
          system.debug('+++allProductMetricsData'+allProductMetricsData);
        }
        upsert allProductMetricsData SIQ_External_ID__c;   
    }

    global void finish(Database.BatchableContext BC) 
    {
        Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID); 
        String ErrorMsg ='ERROR COUNT:-'+errorcount+'--'+nonProcessedAcs; 
        system.debug('schedulerObj++++before'+sJob);       
        //Update the scheduler log with successful
        sJob.No_Of_Records_Processed__c=recordsProcessed;
        sjob.Object_Name__c = 'Product Metrics Delta';
        //sjob.Changes__c        
        sJob.Job_Status__c='Successful';
        sjob.Changes__c = ErrorMsg;               
        system.debug('sJob++++++++'+sJob);
        update sJob;
		
        if(chain)
        {
          Database.executeBatch(new changeMySetupStatus(selectedTeamInstancesinit,true),2000);
        }
    }
}