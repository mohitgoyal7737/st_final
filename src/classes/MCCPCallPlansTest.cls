/**
@author       : A1942
@createdDate  : 27-07-2020
@description  : Test class for the MCCPCallPlans.cls 
@Revison(s)   : v1.0
 */
@isTest
public with sharing class MCCPCallPlansTest 
{
    public static testmethod void MCCPCallPlansTest() 
    {
        String className = 'MCCPCallPlansTest';
        //Basic Data preperation
        AxtriaSalesIQTM__Organization_Master__c orgMaster = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgMaster,className);

        AxtriaSalesIQTM__Country__c country = TestDataFactory.createCountry(orgMaster);
        SnTDMLSecurityUtil.insertRecords(country,className);

        String workspaceName = 'TestWorkspace';
        Date startDate = Date.newInstance(2020, 2, 17);
        Date endDate = Date.newInstance(2020, 8, 17);
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace(workspaceName, startDate, endDate);
        SnTDMLSecurityUtil.insertRecords(workspace,className);

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(country);
        SnTDMLSecurityUtil.insertRecords(team,className);

        AxtriaSalesIQTM__Team_Instance__c teamInstance = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamInstance,className);

        AxtriaSalesIQTM__Position__c position = TestDataFactory.createPosition(team, teamInstance);
        SnTDMLSecurityUtil.insertRecords(position,className);

        AxtriaSalesIQTM__User_Access_Permission__c userPosition = TestDataFactory.createUserAccessPerm(position, teamInstance, UserInfo.getUserId());
		SnTDMLSecurityUtil.insertRecords(userPosition,className);

        String stage = 'Design';
        String processStage = 'Ready';
        String status = 'Active';
        AxtriaSalesIQTM__Scenario__c scenario = TestDataFactory.createScenario(workspace.id, teamInstance.id, null, stage, processStage, status, startDate, endDate); 
        SnTDMLSecurityUtil.insertRecords(scenario,className);

        Product_Catalog__c prodCatalog = TestDataFactory.productCatalog(team, teamInstance, country);
        prodCatalog.Veeva_External_ID__c = prodCatalog.Product_Code__c;//Veeva_External_Id must be equal to Product_Code;
        SnTDMLSecurityUtil.insertRecords(prodCatalog,className);

        Measure_Master__c measureMaster = TestDataFactory.createMeasureMaster(prodCatalog, team, teamInstance); 
        measureMaster.Rule_Type__c='MCCP';
        measureMaster.is_Deactivated__c = false;
        measureMaster.PDE_Method__c= 'PDE Optimised & Workload Balanced';
        measureMaster.PDE_Frequencies__c = '1,4,8,12,16,20,24';
        SnTDMLSecurityUtil.insertRecords(measureMaster,className);

        String nameSpace = MCCP_Utility.sntNamespace(className);
        List<String> PROD_PRIORITY_READ_FIELD = new List<String>{nameSpace+'MCCP_Selected_Products__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Measure_Master__c.SObjectType, PROD_PRIORITY_READ_FIELD, false));

        //Method calls
        MCCPCallPlans.checkMCCPEnabled();
        MCCPCallPlans.getMCCPCallPlans();
        MCCPCallPlans.getUserCountryId();
    }

    /*Added by HT(A0994) on 2nd August 2020 for SMCCP-9 : Start*/
    public static testmethod void deleteBRTest() 
    {
        String className = 'MCCPCallPlansTest';

    	AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    	SnTDMLSecurityUtil.insertRecords(orgmas,className);

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);

        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today().addDays(90));
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);

        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins,className);

        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        SnTDMLSecurityUtil.insertRecords(scen,className);

        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        pcc.Product_Code__c='PROD1';
        pcc.Veeva_External_ID__c='PROD1';
        SnTDMLSecurityUtil.insertRecords(pcc,className);

        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        mmc.State__c ='In Progress';
        mmc.Rule_Type__c='MCCP';
        mmc.is_Deactivated__c = false;
        mmc.PDE_Method__c= 'PDE Optimised & Workload Balanced';
        mmc.PDE_Frequencies__c = '1,4,8,12,16,20,24';
        SnTDMLSecurityUtil.insertRecords(mmc,className);

        MCCP_CNProd__c rec = new MCCP_CNProd__c();
        rec.Call_Plan__c = mmc.Id;
        SnTDMLSecurityUtil.insertRecords(rec,className);
        
        AxtriaSalesIQTM__ETL_Config__c etlConfig = new AxtriaSalesIQTM__ETL_Config__c();
        etlConfig.MCCP_S3_Bucket__c = 's3-mccp';
        etlConfig.MCCP_S3_Folder__c = 'testing';
        etlConfig.MCCP_S3_Access_Key__c = 'AKIA3I72APVE73PSXNUJ';
        etlConfig.MCCP_S3_Secret_Key__c = '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';
        etlConfig.Name = 'MCCP Rule Diagnostics';
        SnTDMLSecurityUtil.insertRecords(etlConfig,className);

        System.Test.startTest();

        String nameSpace = MCCP_Utility.sntNamespace(className);
        List<String> PROD_PRIORITY_READ_FIELD = new List<String>{nameSpace+'MCCP_Selected_Products__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Measure_Master__c.SObjectType, PROD_PRIORITY_READ_FIELD, false));

        //Test cloneBusinessRule() static function in the class
        MCCPCallPlans.cloneBusinessRule(null);
        MCCPCallPlans.cloneBusinessRule('');
        MCCPCallPlans.cloneBusinessRule(mmc.Id);

        //Test executeBusinessRule() static function in the class
        MCCPCallPlans.executeBusinessRule(null);
        MCCPCallPlans.executeBusinessRule('');
        MCCPCallPlans.executeBusinessRule(mmc.Id);

        //Test publishBusinessRule() static function in the class
        MCCPCallPlans.publishBusinessRule(null);
        MCCPCallPlans.publishBusinessRule('');
        MCCPCallPlans.publishBusinessRule(mmc.Id);
        
        MCCPCallPlans.checkBRExecuted(mmc.Id);
        MCCPCallPlans.getComparisonData(mmc.Id,pcc.Product_Code__c);
        MCCPCallPlans.getMCCPRuleLogs(mmc.Id);
        MCCPCallPlans.getDiagnosticData(mmc.Id);
        MCCPCallPlans.downloadOutputFile(mmc.Id, 'Testoutput');

        //Test deleteBusinessRule() static function in the class
        MCCPCallPlans.deleteBusinessRule(null);
        MCCPCallPlans.deleteBusinessRule('');
        MCCPCallPlans.deleteBusinessRule(mmc.Id);

        System.Test.stopTest();
    }
    /*Added by HT(A0994) on 2nd August 2020 for SMCCP-9 : End*/
}