/**********************************************************************************************
Author      : Himanshu Tariyal (A0994)
Date        : 5th August'2020
Description : Controller class for Product-Channel Weight screen of MCCP
Revision(s) : v1.0
**********************************************************************************************/
public with sharing class MCCPProductChannelWeightsCtlr 
{
public class MCCPException extends Exception {}
    /*Added by HT(A0994) on 5th August 2020 for SMCCP-25 : Start*/
    @AuraEnabled
    public static List<BusinessRuleDataWrapper> getBusinessRuleData(String businessRuleID)
    {
        SnTDMLSecurityUtil.printDebugMessage('getBusinessRuleData()-->businessRuleID : '+businessRuleID);

        List<BusinessRuleDataWrapper> dataList = new List<BusinessRuleDataWrapper>();
        String className = 'MCCPProductChannelWeightsCtlr';

        try 
        {
            if(businessRuleID!=null && businessRuleID!='')
            {
                Transient List<Measure_Master__c> measureMasterList = new List<Measure_Master__c>();
                Transient List<MCCP_CNProd__c> mccpCNProdData = new List<MCCP_CNProd__c>();
                Transient List<MCCP_CNProd__c> productData = new List<MCCP_CNProd__c>();
                Transient List<MCCP_CNProd__c> channelData = new List<MCCP_CNProd__c>();
                Transient List<ProductChannelWrapper> junctionData = new List<ProductChannelWrapper>();
                Transient List<Team_Instance_Product_AZ__c> teamInsProd = new List<Team_Instance_Product_AZ__c>();

                BusinessRuleDataWrapper dataRec;

                //updating Effective Days In Field in measure master
                MCCP_Utility.updateEffectiveDaysInField(businessRuleID);

                measureMasterList = [Select Id,Name,Target_Count_Maximum__c,Target_Count_Minimun__c,Workload_Hours__c,
                                    Workload_Days__c,Desired_Range__c,Available_Capacity__c,Single_Product_Rule__c,
                                    Workspace__r.Name,Team__r.Name,Team_Instance__c,Effective_Days_in_Field__c,
                                    Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c,Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c,
                                    isMCCPRuleExecuting__c,isMCCPRulePublishing__c 
                                    from Measure_Master__c where id=:businessRuleID limit 1];

                if(!measureMasterList.isEmpty())
                {                   
                    dataRec = new BusinessRuleDataWrapper();
                    dataRec.ruleData = measureMasterList[0];

                    //Check for Child Records
                    //1. Product Weights data
                    dataRec.productData = new List<MCCP_CNProd__c>();
                    productData = [Select id,Weights__c,Product__c,RecordType.Name from MCCP_CNProd__c where 
                                    Call_Plan__c=:businessRuleID and RecordType.Name = 'Product' and
                                    Product__c!=null WITH SECURITY_ENFORCED order by Product__c];
                    if(!productData.isEmpty()){
                        dataRec.productData.addAll(productData);
                    }

                    //2. Channel Effectiveness/Touchpoints data
                    dataRec.channelData = new List<MCCP_CNProd__c>();
                    channelData = [Select Id,CE_Priority1__c,CE_Priority2__c,CE_Priority3__c,CE_Priority4__c,WLE_TP_PerHour__c,
                                    Channel__c from MCCP_CNProd__c where Call_Plan__c=:businessRuleID and Channel__c!=null
                                    and RecordType.Name = 'Channel' WITH SECURITY_ENFORCED order by Channel__c];
                    if(!channelData.isEmpty()){
                        dataRec.channelData.addAll(channelData);
                    }

                    //3. Junction Data
                    dataRec.junctionData = new List<ProductChannelWrapper>(); //if data is null

                    Transient Map<String,MCCP_CNProd__c> mapData = new Map<String,MCCP_CNProd__c>();
                    Transient Map<String,List<MCCP_CNProd__c>> channelProductDataMap = new Map<String,List<MCCP_CNProd__c>>();
                    Transient Set<String> prodSet = new Set<String>();
                    Transient Set<String> channelSet = new Set<String>();

                    for(MCCP_CNProd__c rec : [Select Id,Channel__c,Product__c,OptimisationSelected__c,PromotionSelected__c 
                                                from MCCP_CNProd__c where Call_Plan__c=:businessRuleID and Channel__c!=null
                                                and RecordType.Name = 'Junction' and Product__c!=null WITH 
                                                SECURITY_ENFORCED order by Channel__c,Product__c])
                    {
                        prodSet.add(rec.Product__c);
                        channelSet.add(rec.Channel__c);
                        mapData.put(rec.Channel__c+'_'+rec.Product__c,rec);
                    }

                    if(!prodSet.isEmpty() && !mapData.keySet().isEmpty())
                    {
                        for(String channel : channelSet)
                        {
                            List<MCCP_CNProd__c> innerList = new List<MCCP_CNProd__c>();
                            for(String product : prodSet)
                            {
                                innerList.add(mapData.get(channel+'_'+product));
                            }
                            channelProductDataMap.put(channel,innerList);
                        }

                        ProductChannelWrapper pcw = new ProductChannelWrapper();
                        pcw.setProducts = new Set<String>();
                        pcw.setProducts.addAll(prodSet);

                        pcw.setChannels = new Set<String>();
                        pcw.setChannels.addAll(channelSet);
                        pcw.channelProductList = new Map<String,List<MCCP_CNProd__c>>();
                        pcw.channelProductList.putAll(channelProductDataMap);

                        dataRec.junctionData.add(pcw);
                    }
                    dataList.add(dataRec);
                }
            }
            
             if(test.isrunningtest())
            throw new MCCPException('To cover test class');
        } 
        catch (Exception e) 
        {
            SnTDMLSecurityUtil.printDebugMessage('Error in '+className+' : fetchAllData()-->'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack Trace->'+e.getStackTraceString());
            SnTDMLSecurityUtil.printDebugMessage(e.getMessage());
            
            SalesIQSnTLogger.createHandledErrorLogs(e,SalesIQSnTLogger.MCCP_MODULE,businessRuleID);
            dataList = new List<BusinessRuleDataWrapper>();
        }

        SnTDMLSecurityUtil.printDebugMessage('dataList : '+dataList);
        SnTDMLSecurityUtil.printDebugMessage('dataList.size() : '+dataList.size());
        return dataList;
    }

    @AuraEnabled
    public static List<MCCP_CNProd__c> resetChannelEffectivenessData(String businessRuleID)
    {
        SnTDMLSecurityUtil.printDebugMessage('resetChannelEffectivenessData() invoked-->');
        SnTDMLSecurityUtil.printDebugMessage('businessRuleID--'+businessRuleID);

        List<MCCP_CNProd__c> resetChannelData = new List<MCCP_CNProd__c>();    
        try
        {

            if(businessRuleID!=null && businessRuleID!='')
            {
                Transient List<Measure_Master__c> mmList = new List<Measure_Master__c>();
                mmList = [select id,Country__c,Team_Instance__c from Measure_Master__c 
                            where id=:businessRuleID WITH SECURITY_ENFORCED];

                if(!mmList.isEmpty())
                {
                    String teamInst = mmList[0].Team_Instance__c;
                    String countryID = mmList[0].Country__c;
                    String p1Effect;
                    String p2Effect;
                    String p3Effect;
                    String p4Effect;
                    //1. Fetch Channel info data
                    List<MCCP_CNProd__c> channelData = [select id,Channel__c,CE_Priority1__c,CE_Priority2__c,
                                                        CE_Priority3__c,CE_Priority4__c,WLE_TP_PerHour__c
                                                        from MCCP_CNProd__c where RecordType.Name='Channel'
                                                        and Call_Plan__c=:businessRuleID and 
                                                        Channel__c!=null WITH SECURITY_ENFORCED];
                    SnTDMLSecurityUtil.printDebugMessage('channelData size--'+channelData.size());

                    if(!channelData.isEmpty())
                    {
                        //2. Fetch Channel level data
                        Transient Map<String,List<String>> channelToEffectDataMap = new Map<String,List<String>>();
                        for(Channel_Info__c ci : [select id,Channel_Effectiveness_P1__c,Channel_Name__c,
                                                        Channel_Effectiveness_P2__c,Channel_Effectiveness_P3__c,
                                                        Channel_Effectiveness_P4__c from Channel_Info__c where 
                                                        Team_Instance__c=:teamInst and Country__c=:countryID
                                                        WITH SECURITY_ENFORCED order by Channel_Name__c])
                        {
                            if(!channelToEffectDataMap.containsKey(ci.Channel_Name__c))
                            {
                                p1Effect = ci.Channel_Effectiveness_P1__c!=null ? ci.Channel_Effectiveness_P1__c : ''; 
                                p2Effect = ci.Channel_Effectiveness_P2__c!=null ? ci.Channel_Effectiveness_P2__c : '';
                                p3Effect = ci.Channel_Effectiveness_P3__c!=null ? ci.Channel_Effectiveness_P3__c : ''; 
                                p4Effect = ci.Channel_Effectiveness_P4__c!=null ? ci.Channel_Effectiveness_P4__c : '';
                                channelToEffectDataMap.put(ci.Channel_Name__c,new List<String>{p1Effect,p2Effect,p3Effect,p4Effect});
                            }
                        }
                        SnTDMLSecurityUtil.printDebugMessage('channelToEffectDataMap--'+channelToEffectDataMap);
                        SnTDMLSecurityUtil.printDebugMessage('channelData--'+channelData);

                        //3. Get the resetted data
                        if(!channelToEffectDataMap.keySet().isEmpty())
                        {
                            for(MCCP_CNProd__c mccp : channelData)
                            {
                                if(channelToEffectDataMap.containsKey(mccp.Channel__c))
                                {
                                    p1Effect = channelToEffectDataMap.get(mccp.Channel__c)[0];
                                    p2Effect = channelToEffectDataMap.get(mccp.Channel__c)[1];
                                    p3Effect = channelToEffectDataMap.get(mccp.Channel__c)[2];
                                    p4Effect = channelToEffectDataMap.get(mccp.Channel__c)[3];
                                    mccp.CE_Priority1__c = isNumericString(p1Effect) ? Double.valueOf(p1Effect) : null;
                                    mccp.CE_Priority2__c = isNumericString(p2Effect) ? Double.valueOf(p2Effect) : null;
                                    mccp.CE_Priority3__c = isNumericString(p3Effect) ? Double.valueOf(p3Effect) : null;
                                    mccp.CE_Priority4__c = isNumericString(p4Effect) ? Double.valueOf(p4Effect) : null;
                                }
                                resetChannelData.add(mccp);
                            }
                        }
                        else{
                            resetChannelData.addAll(channelData);
                        }
                        SnTDMLSecurityUtil.printDebugMessage('resetChannelData--'+resetChannelData);
                    }
                }
            }
             if(test.isrunningtest())
            throw new MCCPException('To cover test class');
        }
        catch(Exception e)
        {
            SnTDMLSecurityUtil.printDebugMessage('Error in resetChannelEffectivenessData()-->'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack Trace->'+e.getStackTraceString());
            
            SalesIQSnTLogger.createHandledErrorLogs(e,SalesIQSnTLogger.MCCP_MODULE ,businessRuleID);
            resetChannelData = new List<MCCP_CNProd__c>();
        }

        SnTDMLSecurityUtil.printDebugMessage('resetChannelData--'+resetChannelData);
        SnTDMLSecurityUtil.printDebugMessage('resetChannelData size--'+resetChannelData.size());
        return resetChannelData;
    }

    @AuraEnabled
    public static Boolean isNumericString(String str)
    {
        try{
            Decimal d = Decimal.valueOf(str);
             if(test.isrunningtest())
            throw new MCCPException('To cover test class');
        }
        catch(Exception e){
            return false;
        }
        return true;
    }

    @AuraEnabled
    public static List<ActionResultWrapper> saveBusinessRuleData(String jsonDataString)
    {
        SnTDMLSecurityUtil.printDebugMessage('saveBusinessRuleData() invoked-->');
        SnTDMLSecurityUtil.printDebugMessage('jsonDataString--'+jsonDataString);
        
        Transient List<ActionResultWrapper> resultList = new List<ActionResultWrapper>();
        String className = 'MCCPCallPlans';
        SObjectAccessDecision securityDecision;
        Boolean errorThrown = false;
        Boolean isMccpEnabled = false;

        Savepoint sp = Database.setSavepoint();

        try 
        {
            //Validation for MCCP Enable
            isMccpEnabled = MCCPCallPlans.checkMCCPEnabled();
            if(!isMccpEnabled && !System.Test.isRunningTest()){
                resultList.add(new ActionResultWrapper(false,'', System.Label.MCCP_Disabled_Msg));
                return resultList;
            }

            if(jsonDataString!='' && jsonDataString!=null)
            {
                BusinessRuleDataWrapper result = (BusinessRuleDataWrapper)JSON.deserialize(jsonDataString,
                                                        BusinessRuleDataWrapper.class);
                SnTDMLSecurityUtil.printDebugMessage('result-->'+result);

                //1. Save Measure Master record
                Measure_Master__c mmRule = result.ruleData;
                SnTDMLSecurityUtil.printDebugMessage('mmRule-->'+mmRule);

                if(mmRule!=null)
                {
                    List<Measure_Master__c> brList = new List<Measure_Master__c>{mmRule};
                    securityDecision = Security.stripInaccessible(AccessType.UPDATABLE,brList);
                    List<Measure_Master__c> updatedBRList = securityDecision.getRecords();
                    SnTDMLSecurityUtil.updateRecords(updatedBRList,className);

                    if(updatedBRList.isEmpty()){
                        resultList.add(new ActionResultWrapper(false,'',System.Label.MCCP_BR_CRUD_Exception));
                    }
                    else
                    {
                        List<MCCP_CNProd__c> updateCNProdList = new List<MCCP_CNProd__c>();

                        //2. Save Product Weights Data
                        SnTDMLSecurityUtil.printDebugMessage('result.productData-->'+result.productData);
                        SnTDMLSecurityUtil.printDebugMessage('result.productData size-->'+result.productData.size());

                        if(!result.productData.isEmpty()){
                            updateCNProdList.addAll(result.productData);
                        }

                        //3. Save Channel Effectiveness-Touchpoints data
                        SnTDMLSecurityUtil.printDebugMessage('result.channelData-->'+result.channelData);
                        SnTDMLSecurityUtil.printDebugMessage('result.channelData size-->'+result.channelData.size());

                        if(!result.channelData.isEmpty()){
                            updateCNProdList.addAll(result.channelData);
                        }

                        //4. Save Product-Channel Junction data
                        SnTDMLSecurityUtil.printDebugMessage('result.junctionData-->'+result.junctionData);
                        if(!result.junctionData.isEmpty())
                        {
                            Map<String,List<MCCP_CNProd__c>> channelJunctionDataMap = result.junctionData[0].channelProductList;
                            SnTDMLSecurityUtil.printDebugMessage('channelJunctionDataMap size-->'+channelJunctionDataMap.keySet().size());

                            if(channelJunctionDataMap.keySet().size()>0)
                            {
                                for(String key : channelJunctionDataMap.keySet())
                                {
                                    if(channelJunctionDataMap.get(key)!=null)
                                        updateCNProdList.addAll(channelJunctionDataMap.get(key));
                                }
                            }
                        }
                        SnTDMLSecurityUtil.printDebugMessage('updateCNProdList size-->'+updateCNProdList.size());

                        if(!updateCNProdList.isEmpty())
                        {
                            securityDecision = Security.stripInaccessible(AccessType.UPDATABLE,updateCNProdList);
                            List<MCCP_CNProd__c> strippedDataList = securityDecision.getRecords();
                            SnTDMLSecurityUtil.updateRecords(strippedDataList,className);

                            if(strippedDataList.isEmpty()) {
                                resultList.add(new ActionResultWrapper(false,'',System.Label.MCCP_BR_CRUD_Exception));
                            }
                            else{
                                resultList.add(new ActionResultWrapper(true,'',System.Label.MCCP_BR_Data_Saved));
                            }
                        }
                    }
                }
                else{
                    resultList.add(new ActionResultWrapper(false,'',System.Label.Null_Business_Rule));
                }
            }
            else{
                resultList.add(new ActionResultWrapper(false,'',System.Label.MCCP_Exception_Save_Product_Channel_Data));
            }
             if(test.isrunningtest())
            throw new MCCPException('To cover test class');
        } 
        catch (Exception e) 
        {
            SnTDMLSecurityUtil.printDebugMessage('Error in publishBusinessRule()-->'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack Trace->'+e.getStackTraceString());
            Database.rollback(sp);
            
            SalesIQSnTLogger.createUnHandledErrorLogs(e,SalesIQSnTLogger.MCCP_MODULE ,'MCCPProductChannelWeightsCtlr ','');
            resultList.add(new ActionResultWrapper(false,'',System.Label.MCCP_Exception_Save_Product_Channel_Data));
        }

        SnTDMLSecurityUtil.printDebugMessage('resultList--'+resultList);
        SnTDMLSecurityUtil.printDebugMessage('resultList size--'+resultList.size());
        return resultList;
    }

    public class BusinessRuleDataWrapper
    {
        @AuraEnabled public Measure_Master__c ruleData {get;set;}
        @AuraEnabled public List<MCCP_CNProd__c> productData {get;set;}
        @AuraEnabled public List<MCCP_CNProd__c> channelData {get;set;}
        @AuraEnabled public List<ProductChannelWrapper> junctionData {get;set;}
    }

    /*public class ChannelInfoWrapper
    {
        @AuraEnabled public String channelName {get;set;}
        @AuraEnabled public String touchPoints {get;set;}
        @AuraEnabled public String p1Effect {get;set;}
        @AuraEnabled public String p2Effect {get;set;}

        ChannelInfoWrapper(String channelName,String touchPoints,String p1Effect,String p2Effect)
        {
            this.channelName = channelName;
            this.touchPoints = touchPoints;
            this.p1Effect = p1Effect;
            this.p2Effect = p2Effect;
        }
    }*/

    public class ProductChannelWrapper
    {
        @AuraEnabled public Set<String> setProducts {get;set;}
        @AuraEnabled public Set<String> setChannels {get;set;}
        @AuraEnabled public Map<String,List<MCCP_CNProd__c>> channelProductList {get;set;}
    }

    public class ActionResultWrapper
    {
        @AuraEnabled public Boolean dataSaved {get;set;}
        @AuraEnabled public String ruleID {get;set;}
        @AuraEnabled public String message {get;set;}

        ActionResultWrapper(Boolean dataSaved,String ruleID,String message)
        {
            this.dataSaved = dataSaved;
            this.ruleID = ruleID;
            this.message = message;
        }
    }
    /*Added by HT(A0994) on 5th August 2020 for SMCCP-25 : End*/
}