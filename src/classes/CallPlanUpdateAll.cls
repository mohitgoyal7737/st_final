global class CallPlanUpdateAll implements Database.Batchable<sObject>{
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'SELECT Id, Share__c FROM AxtriaSalesIQTM__Position_Account_Call_Plan__c';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, list<Sobject> scope) {
        list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> callPlansUpdate = new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        for(sObject s: scope){
            AxtriaSalesIQTM__Position_Account_Call_Plan__c cp = new AxtriaSalesIQTM__Position_Account_Call_Plan__c();
            cp.Id = (String)s.get('Id');
            cp.Share__c = false;
            callPlansUpdate.add(cp);
        }
        
        CallPlanSummaryTriggerHandler.execute_trigger = false; //Using variable to stop from executing trigger while running this batch.
        update callPlansUpdate;
        CallPlanSummaryTriggerHandler.execute_trigger = true; //Using variable to start from executing trigger after runnig the batch update.
    }
    
    global void finish(Database.BatchableContext BC) {}
}