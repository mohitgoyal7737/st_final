global class ProcessExpiredPositionBatchSNT implements Database.Batchable<sObject>, Database.Stateful{

    static Date changeEffectiveDate;
    public set<ID> inactivePositionIdSet = new set<Id>();
    public List<String> posCodeList=new List<String>();

    global ProcessExpiredPositionBatchSNT(){}


    global Database.QueryLocator start(Database.BatchableContext bc){
        system.debug('#### Start method');

        posCodeList.add('0');
        posCodeList.add('00000');
        posCodeList.add('Unassigned');
        posCodeList.add('Unassigned Territory');
        
        string soql = 'Select Id,AxtriaSalesIQTM__IsMaster__c, AxtriaSalesIQTM__Assignment_Status__c, AxtriaSalesIQTM__inactive__c, AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Employee__c,AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team_Cycle_Name__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c,AxtriaSalesIQTM__Client_Territory_Name__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Inactive__c = false and AxtriaSalesIQTM__Client_Position_Code__c not in: posCodeList and AxtriaSalesIQTM__Client_Position_Code__c != null';
        return Database.getQueryLocator(soql);
    }

    //Execute method : Process all the positions end dating today.
    global void execute(Database.BatchableContext bc, List<AxtriaSalesIQTM__Position__c> positions){

        system.debug('#### Execute method | scope : '+positions.size());


        system.debug(Logginglevel.ERROR,'#### updatePosition method | positions : '+positions.size());
        
        Date processDate = System.today();
        changeEffectiveDate = System.today();
        system.debug('#### processDate : '+processDate);
        
        set<Id> positionIds = new set<Id>();
        Map<String,String> mapPosId2PosCode = new Map<String,String>();

        for(AxtriaSalesIQTM__Position__c pos : positions){
            positionIds.add(pos.Id);
            mapPosId2PosCode.put(pos.Id, pos.AxtriaSalesIQTM__Client_Position_Code__c);
        }

        system.debug('Position Inactive code Starts');
        map<Id,list<AxtriaSalesIQTM__Position_Employee__c>> positionAssignmentMap = new map<Id,list<AxtriaSalesIQTM__Position_Employee__c>>();


        //Added by A1734
        //map<String,list<AxtriaSalesIQTM__Position_Employee__c>> posCodetoEmpAssignmentMap = new map<String,list<AxtriaSalesIQTM__Position_Employee__c>>();
        //Till here

        Map<String,String> mapPosId2EmpName = new Map<String,String>();
        

        for(AxtriaSalesIQTM__Position_Employee__c pe : [Select Id, AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Employee__c,AxtriaSalesIQTM__Employee__r.Name From AxtriaSalesIQTM__Position_Employee__c where AxtriaSalesIQTM__Assignment_Type__c = 'Primary' and 
                                       AxtriaSalesIQTM__Effective_End_Date__c >=: processDate and AxtriaSalesIQTM__Effective_Start_Date__c <=: processDate and AxtriaSalesIQTM__Employee__c != null and 
                                       AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c in :mapPosId2PosCode.values() order by LastModifiedDate desc]){
            positionAssignmentMap.put(pe.AxtriaSalesIQTM__Position__c,new list<AxtriaSalesIQTM__Position_Employee__c>{pe});

            mapPosId2EmpName.put(pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,pe.AxtriaSalesIQTM__Employee__r.Name);
            

            system.debug('#### pe.Position__c : '+pe.AxtriaSalesIQTM__Position__c);
        }


        system.debug('#### mapPosId2EmpName : '+mapPosId2EmpName);
        system.debug('#### mapPosId2PosCode : '+mapPosId2PosCode);

        
        set<Id> InactivePositionIds = new set<Id>();
        
        for(AxtriaSalesIQTM__Position__c pos : positions){
            System.debug('pos before changes - '+pos);
                        
            if(pos.AxtriaSalesIQTM__Effective_End_Date__c < System.today() || pos.AxtriaSalesIQTM__Effective_End_Date__c  < processDate){
                pos.AxtriaSalesIQTM__Inactive__c = true;
                //pos.AxtriaSalesIQTM__Employee__c=null;
                InactivePositionIds.add(pos.id);
                inactivePositionIdSet.add(pos.id);
            }
            //System.debug('-- InactivePositionIds :'+InactivePositionIds);
           
            //Update Employee and Assignment Status on Position if active primary employee is assigned
            System.debug(positionAssignmentMap.containsKey(pos.Id));
            System.debug(positionAssignmentMap.get(pos.Id));
            System.debug(InactivePositionIds.contains(pos.Id));
            if(positionAssignmentMap.containsKey(pos.Id) && positionAssignmentMap.get(pos.Id).size() > 0 && !InactivePositionIds.contains(pos.Id)){
                System.debug('-- inside if ----');
                pos.AxtriaSalesIQTM__Assignment_Status__c = 'Filled';
                pos.AxtriaSalesIQTM__Employee__c = positionAssignmentMap.get(pos.Id)[0].AxtriaSalesIQTM__Employee__c;


                String posCode = mapPosId2PosCode.get(pos.Id);
                String employee = mapPosId2EmpName.get(posCode);

                pos.AxtriaSalesIQTM__Client_Territory_Name__c = posCode + ' (' + employee + ')';

            }else{
                System.debug('-- inside else ----');
                pos.AxtriaSalesIQTM__Assignment_Status__c = 'Vacant';
                pos.AxtriaSalesIQTM__Employee__c = null;
                String employee = '';

                String posCode = mapPosId2PosCode.get(pos.Id);

                if(mapPosId2EmpName.containsKey(posCode))
                    employee = mapPosId2EmpName.get(posCode);

                else
                    employee = 'Vacant';

                pos.AxtriaSalesIQTM__Client_Territory_Name__c = posCode + ' (' + employee + ')';
            }

            //inactivePositionIdSet.add(pos.id);

            System.debug('pos after changes - '+pos);
        }
        update positions;

/*        if(InactivePositionIds != null && InactivePositionIds.size() > 0)
            inactivePositionIdSet.addAll(InactivePositionIds);*/

        system.debug('Position Inactive code ends');

        
        System.debug('-- inactivePositionIdSet :'+inactivePositionIdSet);
        System.debug('-- InactivePositionIds :'+InactivePositionIds);

        //updatePosGeoAccntEmployExprirePos(InactivePositionIds);
        
        //SIMPS-423 - Inactivate user position when position employee expired
        inactivateUserPosForExpiredAssignment(InactivePositionIds);

        //updatePosition(scope);
    }
    
    //Deprecated in 4.5
    global static void updatePosition(list<AxtriaSalesIQTM__Position__c> positions,  boolean isBatchProcess){
    }
    
    global static void updatePosition(list<AxtriaSalesIQTM__Position__c> positions){

        
        /*system.debug(Logginglevel.ERROR,'#### updatePosition method | positions : '+positions.size());
        
        Date processDate = System.today();
        changeEffectiveDate = System.today();
        system.debug('#### processDate : '+processDate);
        
        set<Id> positionIds = new set<Id>();
        for(AxtriaSalesIQTM__Position__c pos : positions){
            positionIds.add(pos.Id);
        }

        system.debug('Position Inactive code Starts');
        map<Id,list<AxtriaSalesIQTM__Position_Employee__c>> positionAssignmentMap = new map<Id,list<AxtriaSalesIQTM__Position_Employee__c>>();
        for(AxtriaSalesIQTM__Position_Employee__c pe : [Select Id, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Employee__c From AxtriaSalesIQTM__Position_Employee__c where AxtriaSalesIQTM__Assignment_Type__c = 'Primary' and 
                                       AxtriaSalesIQTM__Effective_End_Date__c >=: processDate and AxtriaSalesIQTM__Effective_Start_Date__c <=: processDate and AxtriaSalesIQTM__Employee__c != null and 
                                       AxtriaSalesIQTM__Position__c in : positionIds order by LastModifiedDate desc]){
            positionAssignmentMap.put(pe.AxtriaSalesIQTM__Position__c,new list<AxtriaSalesIQTM__Position_Employee__c>{pe});
            system.debug('#### pe.Position__c : '+pe.AxtriaSalesIQTM__Position__c);
        }
        
        set<Id> InactivePositionIds = new set<Id>();
        
        for(AxtriaSalesIQTM__Position__c pos : positions){
            System.debug('pos before changes - '+pos);
                        
            if(pos.AxtriaSalesIQTM__Effective_End_Date__c <= System.today() || pos.AxtriaSalesIQTM__Effective_End_Date__c  <= processDate){
                pos.AxtriaSalesIQTM__Inactive__c = true;
                //pos.AxtriaSalesIQTM__Employee__c=null;
                InactivePositionIds.add(pos.id);
                inactivePositionIdSet.add(pos.id);
            }*/
            //System.debug('-- InactivePositionIds :'+InactivePositionIds);
           
            //Update Employee and Assignment Status on Position if active primary employee is assigned
            // System.debug(positionAssignmentMap.containsKey(pos.Id));
            // System.debug(positionAssignmentMap.get(pos.Id));
            // System.debug(InactivePositionIds.contains(pos.Id));
            // if(positionAssignmentMap.containsKey(pos.Id) && positionAssignmentMap.get(pos.Id).size() > 0 && !InactivePositionIds.contains(pos.Id)){
            //     System.debug('-- inside if ----');
            //     pos.AxtriaSalesIQTM__Assignment_Status__c = 'Filled';
            //     pos.AxtriaSalesIQTM__Employee__c = positionAssignmentMap.get(pos.Id)[0].AxtriaSalesIQTM__Employee__c;
            // }else{
            //     System.debug('-- inside else ----');
            //     pos.AxtriaSalesIQTM__Assignment_Status__c = 'Vacant';
            //     pos.AxtriaSalesIQTM__Employee__c = null;
            // }

            //inactivePositionIdSet.add(pos.id);

//             System.debug('pos after changes - '+pos);
//         }
//         update positions;

// /*        if(InactivePositionIds != null && InactivePositionIds.size() > 0)
//             inactivePositionIdSet.addAll(InactivePositionIds);*/

//         system.debug('Position Inactive code ends');

        
//         System.debug('-- inactivePositionIdSet :'+inactivePositionIdSet);
//         System.debug('-- InactivePositionIds :'+InactivePositionIds);

//         //updatePosGeoAccntEmployExprirePos(InactivePositionIds);
        
//         //SIMPS-423 - Inactivate user position when position employee expired
//         inactivateUserPosForExpiredAssignment(positionIds);
    }
    
    
    global static void updatePosGeoAccntEmployExprirePos(set<id> InactivePositionIds)
    {

        system.debug('-- calling ProcessCRAssignmentBatch ---');
        if(InactivePositionIds != null && InactivePositionIds.size() > 0){
            ProcessCRAssignmentBatchSNT b1 = new ProcessCRAssignmentBatchSNT(InactivePositionIds,'Position_Account__c');
            ProcessCRAssignmentBatchSNT b2 = new ProcessCRAssignmentBatchSNT(InactivePositionIds,'Position_Geography__c');
            ProcessCRAssignmentBatchSNT b3 = new ProcessCRAssignmentBatchSNT(InactivePositionIds,'Position_Employee__c');

            Id batchinstanceid1 = Database.executeBatch(b1,9000);
            Id batchinstanceid2 = Database.executeBatch(b2,9000);
            Id batchinstanceid3 = Database.executeBatch(b3,9000);

            //inactivateUserPosForExpiredAssignment(InactivePositionIds);

            //Added Call Plan and Position Product Changes
            ExpiredCallPlanAssignmentBatch b4 = new ExpiredCallPlanAssignmentBatch(InactivePositionIds);
            //ExpiredSNTAssignmentBatch b5 = new ExpiredSNTAssignmentBatch(InactivePositionIds,'Position_Product__c');
            Id batchinstanceid4 = Database.executeBatch(b4,200);
            //Id batchinstanceid5 = Database.executeBatch(b5,2000);

        }
        //Date processDate = System.today();
        /*Date processDate = changeEffectiveDate;
        system.debug(Logginglevel.ERROR,'#### processDate : '+processDate);
        
        map<id,Team_Instance__c> DateTeamInstanceMap = new map<id,Team_Instance__c>();
        for(Team_Instance__c t : [select id, IC_EffStartDate__c, IC_EffEndDate__c from Team_Instance__c]){
            DateTeamInstanceMap.put(t.id,t);
        }
            
        map<id,id> UnassignedPosMap = new map<id,id>();
        set<Id> unassignedTerrIds = new set<Id>();
        for(Position__c ps : [select id,Team_Instance__c from Position__c where Client_Position_Code__c='00000']){
            unassignedTerrIds.add(ps.Id);
            if(!UnassignedPosMap.containsKey(ps.Team_Instance__c)){
                UnassignedPosMap.put(ps.Team_Instance__c,ps.id);
            }
        }
        
        map<id,id> UnassignedPosTeamInsMap = new map<id,id>();
        for(Position_Team_Instance__c ps : [select id , Team_Instance_ID__c from Position_Team_Instance__c where Position_ID__c in : unassignedTerrIds]){
            if(!UnassignedPosTeamInsMap.containsKey(ps.Team_Instance_ID__c)){
                UnassignedPosTeamInsMap.put(ps.Team_Instance_ID__c,ps.id);
            }
        }
            
        system.debug(Logginglevel.ERROR,'Position Geography Inactive code Starts'); 
        //Assigning Unassigned Positions to the Expired Position Geography Record Having Expired Positions
        map<Id,list<Position_Geography__c>> geoAssignmentMap = new map<Id,list<Position_Geography__c>>();
        list<Position_Geography__c> PosGeoList = new list<Position_Geography__c>();
        list<Position_Geography__c> PosGeoListToDelete = new list<Position_Geography__c>();
        for(Position_Geography__c pg : [Select Assignment_Status__c,Change_Status__c,Effective_End_Date__c,Effective_Start_Date__c,Geography__c,Id,Metric1__c,Metric2__c,Metric3__c,Metric4__c,Metric5__c,Metric6__c,Metric7__c,Metric8__c,Metric9__c,Metric10__c,Name,Position_Team_Instance__c,Position__c,Team_Instance__c, Team_Instance__r.Alignment_Period__c From Position_Geography__c where Effective_End_Date__c >: processDate and 
                                       Position__c in : InactivePositionIds order by LastModifiedDate desc]){
            
            Position_Geography__c clonedPosGeoRecord = pg.clone(false,true,false,false);
            
            clonedPosGeoRecord.Position__c = UnassignedPosMap.get(pg.Team_Instance__c);
            clonedPosGeoRecord.Position_Team_Instance__c = UnassignedPosTeamInsMap.get(pg.Team_Instance__c);
            
            
                clonedPosGeoRecord.Effective_Start_Date__c = processDate;
                pg.Effective_End_Date__c = processDate.addDays(-1);
                PosGeoList.add(pg);
            
            clonedPosGeoRecord.Effective_End_Date__c = DateTeamInstanceMap.get(pg.Team_Instance__c).IC_EffEndDate__c;

            PosGeoList.add(clonedPosGeoRecord);
        }

        system.debug(Logginglevel.ERROR,'Position Geography Inactive code Ends'); 
        
        //Assigning Unassigned Positions to the Expired Position Account Record Having Expired Positions
        map<Id,list<Position_Account__c>> AccntAssignmentMap = new map<Id,list<Position_Account__c>>();
        list<Position_Account__c> PosAccntList=new list<Position_Account__c>();
        list<Position_Account__c> PosAccntListToDelete = new list<Position_Account__c>();
        for(Position_Account__c pa : [Select Account_Alignment_Type__c,Account_Target_Type__c,Account__c,Assignment_Status__c,Change_Status__c,Comments__c,Effective_End_Date__c,Effective_Start_Date__c,Id,Metric1__c,Metric2__c,Metric3__c,Metric4__c,Metric5__c,Metric6__c,Metric7__c,Metric8__c,Metric9__c,Metric10__c,Metric_1__c,Name,Position_Team_Instance__c,Position__c,Segment_1__c,Team_Instance__c, Team_Instance__r.Alignment_Period__c From Position_Account__c where Effective_End_Date__c >=: processDate and 
                                      Position__c in : InactivePositionIds order by LastModifiedDate desc]){
            
            Position_Account__c clonedPosAccntRecord = pa.clone(false,true,false,false);
            
            clonedPosAccntRecord.Position__c = UnassignedPosMap.get(pa.Team_Instance__c);
            clonedPosAccntRecord.Position_Team_Instance__c = UnassignedPosTeamInsMap.get(pa.Team_Instance__c);
            
                clonedPosAccntRecord.Effective_Start_Date__c = processDate;
                pa.Effective_End_Date__c = processDate.addDays(-1);
                PosAccntList.add(pa);
            //}
            clonedPosAccntRecord.Effective_End_Date__c = DateTeamInstanceMap.get(pa.Team_Instance__c).IC_EffEndDate__c;

            PosAccntList.add(clonedPosAccntRecord);
        }

        //Expiring the PositionEmployee records related to the Expired Positions 
        list<Position_Employee__c> PosEmployeeList=new list<Position_Employee__c>();
        list<Position_Employee__c> PosEmployeeListToDelete = new list<Position_Employee__c>();
        map<Id,list<Position_Employee__c>> positionAssignmentMap = new map<Id,list<Position_Employee__c>>();
        for(Position_Employee__c pe : [Select Id, Position__c, Employee__c, Position__r.Team_Instance__c, Position__r.Team_Instance__r.Alignment_Period__c From Position_Employee__c where 
                                       Effective_End_Date__c >=: processDate and Employee__c != null and 
                                       Position__c in : InactivePositionIds order by LastModifiedDate desc]){
            
            
                pe.Effective_End_Date__c = processDate.addDays(-1);
                PosEmployeeList.add(pe);
            //}
        }
        
        //Check if the total records to be updated in the process are more than 10K then throw DML exception.
        integer dmlSize = PosGeoList.size() + PosGeoListToDelete.size() + PosAccntList.size() + PosAccntListToDelete.size() + PosEmployeeList.size() + PosEmployeeListToDelete.size();
        system.debug('#### dmlSize : '+dmlSize);
        if(dmlSize >= 10000){
            throw new DMLException('My DML EXCEPTION');
        }

        system.debug(Logginglevel.ERROR,'PosGeoList : '+PosGeoList.size()); 
        if(PosGeoList != null && PosGeoList.size() > 0){
           // upsert(PosGeoList);
            

        }
        

        system.debug(Logginglevel.ERROR,'PosAccntList : '+PosAccntList.size()); 
        if(PosAccntList != null && PosAccntList.size() > 0){
            //upsert(PosAccntList);
            

        }

       
            
        system.debug(Logginglevel.ERROR,'PosEmployeeList : '+PosEmployeeList.size()); 
        if(PosEmployeeList != null && PosEmployeeList.size() > 0){
            //update(PosEmployeeList);
           
        }*/
    }
    
    public static void inactivateUserPosForExpiredAssignment(set<Id> positionIds){
        //Fetch configured day of batch process
        double processDay;
        // if(AxtriaSalesIQTM__TotalApproval__c.getValues('ProcessDay') != null){
        //     processDay = AxtriaSalesIQTM__TotalApproval__c.getValues('ProcessDay').AxtriaSalesIQTM__No_Of_Approval__c;
        // }else{
        //     processDay = 0;
        // }
        Date processDate = System.today();
        system.debug('#### processDate : '+processDate);

        //set<Id> teamInstanceIds = new set<Id>();
        //set<Id> posIds = new set<Id>();
        set<Id> userIds = new set<Id>();
        set<String> clientPosCodeSet = new set<String>();
        set<String> teamSet = new set<String>();
        set<String> posCodeTeamKey = new set<String>();

        map<String,set<Id>> posToUserMap = new map<String,set<Id>>();

        //map<Id,map<Id,set<Id>>> empAssMap = new map<Id,map<Id,set<Id>>>();
        for(AxtriaSalesIQTM__Position_Employee__c pe : [Select Id, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__c, AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__USER__c From AxtriaSalesIQTM__Position_Employee__c 
                                       WHERE AxtriaSalesIQTM__Position__c IN : positionIds AND AxtriaSalesIQTM__Employee__c != null AND AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__USER__c != null AND AxtriaSalesIQTM__Effective_End_Date__c <: processDate]){
            //teamInstanceIds.add(pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__c);
            //posIds.add(pe.AxtriaSalesIQTM__Position__c);
            userIds.add(pe.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__USER__c);
            clientPosCodeSet.add(pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
            teamSet.add(pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__c);
            posCodeTeamKey.add(pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' + pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__c);

            System.debug('posIds for User Position::::::::::::' +pe.AxtriaSalesIQTM__Position__c);
          

            if(!posToUserMap.containsKey(pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' + pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__c))
            {
                Set<ID> tempUsers = new Set<ID>();
                tempUsers.add(pe.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__USER__c);
                posToUserMap.put(pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' + pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__c,tempUsers);
                System.debug('If part::::::::::::' +pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' + pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__c);
                System.debug('If Part tempUsers::::::::::::' +tempUsers);
            }
            else
            {
                posToUserMap.get(pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' + pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__c).add(pe.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__USER__c);   
                System.debug('Else part::::::::::::' +pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' + pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__c);
                System.debug('Else Part tempUsers::::::::::::' +pe.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__USER__c);
            }



            //map<Id,set<Id>> posToUserMap = empAssMap.get(pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__c);
            // if(posToUserMap != null)
            // {
            //     set<Id> userIdSet = posToUserMap.get(pe.AxtriaSalesIQTM__Position__c);
            //     if(userIdSet == null){
            //         userIdSet = new set<Id>();
            //     }
            //     userIdSet.add(pe.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__USER__c);
            //     posToUserMap.put(pe.AxtriaSalesIQTM__Position__c,userIdSet);
            //     System.debug('If part::::::::::::' +pe.AxtriaSalesIQTM__Position__c);
            //     System.debug('If Part userIdSet::::::::::::' +userIdSet);
            // }
            // else{
            //     posToUserMap = new map<Id, set<Id>>();
            //     posToUserMap.put(pe.AxtriaSalesIQTM__Position__c, new set<Id>{pe.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__USER__c});

            //     System.debug('Else part::::::::::::' +pe.AxtriaSalesIQTM__Position__c);
            //     System.debug('Else Part userIdSet::::::::::::' +pe.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__USER__c);
            // }
            // empAssMap.put(pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__c,posToUserMap);
        }

        list<AxtriaSalesIQTM__User_Access_Permission__c> implicitUAP = new list<AxtriaSalesIQTM__User_Access_Permission__c>();
        for(AxtriaSalesIQTM__User_Access_Permission__c uap : [SELECT Id, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__User__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__c, AxtriaSalesIQTM__Sharing_Type__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Is_Active__c FROM AxtriaSalesIQTM__User_Access_Permission__c 
                                             WHERE AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__c IN : teamSet AND AxtriaSalesIQTM__User__c IN : userIds AND AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c IN : clientPosCodeSet AND AxtriaSalesIQTM__Is_Active__c = true]){
            //map<Id,set<Id>> posToUserMap = empAssMap.get(uap.AxtriaSalesIQTM__Team_Instance__c);
            if(posToUserMap.containsKey(uap.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' + uap.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__c)){
                if(posToUserMap.get(uap.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' + uap.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__c).contains(uap.AxtriaSalesIQTM__User__c)){
                    //if(posCodeTeamKey.contains(uap.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c + '_' + uap.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__c))
                    //{
                        uap.AxtriaSalesIQTM__Is_Active__c = false;
                        implicitUAP.add(uap);
                        System.debug('######User position record inactive########## ' +uap);
                    //}
                }
            }
        }
        if(implicitUAP.size() > 0){
            update implicitUAP;
        }
    }
    
    global void finish(Database.BatchableContext bc){
        system.debug('#### Finish method:' +inactivePositionIdSet.size());
        updatePosGeoAccntEmployExprirePos(inactivePositionIdSet);
        
    }

    //This method will be called from Change request trigger handler for expire positions (cascade delete)
   /* public static void updateExpiredPosition(list<AxtriaSalesIQTM__Position__c> positions, AxtriaSalesIQTM__Change_Request__c cr){
        try{

            list<AxtriaSalesIQTM__CR_Position__c> crPos = [SELECT Id, AxtriaSalesIQTM__Effective_End_Date__c FROM AxtriaSalesIQTM__CR_Position__c WHERE AxtriaSalesIQTM__Change_Request__c =: cr.Id];
            
            if(crPos.size() > 0){
                changeEffectiveDate = crPos[0].AxtriaSalesIQTM__Effective_End_Date__c;
            }else{
                changeEffectiveDate = system.today();
            }

            system.debug(Logginglevel.ERROR,'#### updatePosition method | positions : '+positions.size());
            
            //Date processDate = System.today().addDays(integer.valueOf(processDay));
            //SIMPS-644 - This code will only execute for delete position. Process date will be position end date.
            Date processDate = changeEffectiveDate;
            
            system.debug(Logginglevel.ERROR,'#### processDate : '+processDate);
            
            set<Id> positionIds = new set<Id>();
            for(AxtriaSalesIQTM__Position__c pos : positions){
                positionIds.add(pos.Id);
            }
            system.debug('Position Inactive code Starts');
            map<Id,list<Position_Employee__c>> positionAssignmentMap = new map<Id,list<Position_Employee__c>>();
            for(Position_Employee__c pe : [Select Id, Position__c, Employee__c From Position_Employee__c where Assignment_Type__c = 'Primary' and 
                                           Effective_End_Date__c >: processDate and Effective_Start_Date__c <=: processDate and Employee__c != null and 
                                           Position__c in : positionIds order by LastModifiedDate desc]){
                positionAssignmentMap.put(pe.Position__c,new list<Position_Employee__c>{pe});
                system.debug(Logginglevel.ERROR,'#### pe.Position__c : '+pe.Position__c);
            }
            
            set<Id> InactivePositionIds = new set<Id>();
            for(AxtriaSalesIQTM__Position__c pos : positions){
                //Mark the position as inactive if Effective end date is smaller than today
                if(pos.AxtriaSalesIQTM__Effective_End_Date__c <= processDate || pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c == SalesIQGlobalConstants.FUTURE_TEAM_CYCLE_TYPE){
                    pos.AxtriaSalesIQTM__inactive__c = true;
                    InactivePositionIds.add(pos.id);
                    system.debug('The Inactive Position is'+pos.id); 
                    //SIMPS-644 - Position is getting deleted and the employee assignments will be removed. Hence updating the Assignment_Status__c to vacant
                    pos.AxtriaSalesIQTM__Assignment_Status__c = 'Vacant';
                    pos.AxtriaSalesIQTM__Employee__c = null;
                }

            }
            update positions;
            system.debug('Position Inactive code ends');
            updatePosGeoAccntEmployExprirePos(InactivePositionIds);
            
            //SIMPS-423 - Inactivate user position when position employee expired
            set<Id> positionIdsSet = new set<Id>(positionIds);
            inactivateUserPosForExpiredAssignment(positionIdsSet);
        }catch(System.DmlException dmlEx){
            system.debug(Logginglevel.ERROR,'Error message approveRejectDeletePosition '+dmlEx.getMessage());
            cr.AddError('Salesforce Limit Exception : Operation cannot be completed as data volume has exceeded the threshold. Please contact SalesIQ admin to process this request.');
        }
        catch(Exception ex){
            system.debug(Logginglevel.ERROR,'Error message approveRejectDeletePosition '+ex.getMessage());
            cr.AddError('Error in updateExpiredPosition : '+ex.getMessage());
        }
    }*/
}