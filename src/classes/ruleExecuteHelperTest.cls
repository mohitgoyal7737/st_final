@isTest
public class ruleExecuteHelperTest {
    @istest static void ruleExecuteHelperTest()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        insert acc;
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        
        
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        insert pcc;
        
        Measure_Master__c rule = TestDataFactory.createMeasureMaster(pcc,team,teamins);
        insert rule;
        
        AxtriaSalesIQTM__Position__c position = TestDataFactory.createPosition(team,teamins);
        insert position;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,position,teamins);
        insert posAccount;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(rule,acc,teamins,posAccount,pPriority,position);
        insert positionAccountCallPlan;        
        BU_Response__c bcc = TestDataFactory.createBuResponse(posAccount,pcc,teamins,team,acc);
        insert bcc;
        
        Grid_Master__c ggMaster = TestDataFactory.gridMaster(countr);
        insert ggMaster;
        
        Parameter__c param = TestDataFactory.parameter(pcc,team,teamins);
        insert param;
        
        Rule_Parameter__c ruleparameter = new Rule_Parameter__c();
        ruleparameter.Measure_Master__c = rule.id;
        ruleparameter.Parameter__c = param.id;
        ruleparameter.CurrencyIsoCode='USD';
        insert ruleparameter;
        
        
        Compute_Master__c ccMaster = TestDataFactory.createComputeMaster(ruleparameter);
        insert ccMaster;
        
        Step__c st= TestDataFactory.step(ccMaster,ggMaster,rule, ruleparameter);
        st.UI_Location__c = 'Compute Accessibility';
        st.Step_Type__c = 'Quantile';
        st.Name = ccMaster.Decile_Field_Output_API__c;
        insert st;
        
        Rule_Parameter__c ruleparameter1 = TestDataFactory.ruleParameter(rule,param,st);
        //ruleparameter.Parameter_Name__c = rpp.Parameter_Name__c;
        insert ruleparameter1;
        
        MetaData_Definition__c metaDataDef = TestDataFactory.createMetaDataDefinition(teamins,pcc,team);
        insert metaDataDef;
        
        map<String,BU_Response__c> buresponsemap = new map<String, BU_Response__c>();
        buresponsemap.put(positionAccountCallPlan.id,bcc);
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            RuleStepFactory obj1 = new RuleStepFactory();
            obj1.getRuleStep(st);
            ruleExecuteHelper obj= new ruleExecuteHelper();
            ruleExecuteHelper.updateTCFSegmentChangeExecute(buresponsemap);
        }
        Test.stopTest();
    }
    @isTest static void ruleExecuteHelperTest11()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        insert acc;
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        
        AxtriaSalesIQTM__Organization_Master__c org =TestDataFactory.createOrganizationMaster();
        insert org;
        
        AxtriaSalesIQTM__Country__c country = TestDataFactory.createCountry(org);
        insert country;
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, country);
        insert pcc;
        
        Measure_Master__c rule = TestDataFactory.createMeasureMaster(pcc,team,teamins);
        insert rule;
        
        //List<String> pacps = new List<String>();
        
        AxtriaSalesIQTM__Position__c position = TestDataFactory.createPosition(team,teamins);
        insert position;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,position,teamins);
        insert posAccount;
        
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        
        BU_Response__c bcc = TestDataFactory.createBuResponse(posAccount,pcc,teamins,team,acc);
        insert bcc;
       
        
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(rule,acc,teamins,posAccount,pPriority,position);
        positionAccountCallPlan.Rule__c = rule.Id;
        positionAccountCallPlan.Segment__c = 'B';
        //positionAccountCallPlan.P1__c='';
        insert positionAccountCallPlan;
        

        
        list<Rule_Parameter__c> ruleParams = [SELECT Id, Name, Parameter_Name__c, Measure_Master__c FROM Rule_Parameter__c WHERE Measure_Master__c =: rule.Id];
        
        Grid_Master__c ggMaster = TestDataFactory.gridMaster(country);
        insert ggMaster;
        
        Parameter__c param = TestDataFactory.parameter(pcc,team,teamins);
        insert param;
        
        
        Rule_Parameter__c ruleparameter = new Rule_Parameter__c();
        ruleparameter.Measure_Master__c = rule.id;
        ruleparameter.Parameter__c = param.id;
        ruleparameter.CurrencyIsoCode='USD';
        insert ruleparameter;
        
        
        Compute_Master__c ccMaster = TestDataFactory.createComputeMaster(ruleparameter);
        
        insert ccMaster;
        
        
        Step__c st= TestDataFactory.step(ccMaster,ggMaster,rule, ruleparameter);
        st.UI_Location__c = 'Compute Accessibility';
        st.Step_Type__c = 'Quantile';
        st.Name = ccMaster.Decile_Field_Output_API__c;
        insert st;
        
        Rule_Parameter__c ruleparameter1 = TestDataFactory.ruleParameter(rule,param,st);
        //ruleparameter1.Parameter_Name__c = ruleparameter.Parameter_Name__c;
        insert ruleparameter1;
        
        MetaData_Definition__c metaDataDef = TestDataFactory.createMetaDataDefinition(teamins,pcc,team);
        insert metaDataDef;
        Account_Compute_Final__c compFinally = TestDataFactory.createComputeFinal(rule,acc,posAccount,bcc,position);
        insert compFinally;
        map<String,BU_Response__c> buresponsemap = new map<String, BU_Response__c>();
        buresponsemap.put(positionAccountCallPlan.id,bcc);
        
        list<String> pacps  = new list<String> ();
        Boolean fromNewPhy;
          list<String> tempList = new   list<String> ();
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            ruleExecuteHelper obj= new ruleExecuteHelper();
            ruleExecuteHelper.updateSegment(pacps,fromNewPhy);
        }
        Test.stopTest();
    }
    
    /* @istest static void ruleExecuteHelperTest2()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        insert acc;
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        
        AxtriaSalesIQTM__Organization_Master__c org =TestDataFactory.createOrganizationMaster();
        insert org;
        
        AxtriaSalesIQTM__Country__c country = TestDataFactory.createCountry(org);
        insert country;
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, country);
        insert pcc;
        
        Measure_Master__c rule = TestDataFactory.createMeasureMaster(pcc,team,teamins);
        insert rule;
        
        AxtriaSalesIQTM__Position__c position = TestDataFactory.createPosition(team,teamins);
        insert position;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,position,teamins);
        insert posAccount;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(rule,acc,teamins,posAccount,pPriority,position);
        insert positionAccountCallPlan;
        
        BU_Response__c bcc = TestDataFactory.createBuResponse(posAccount,pcc,teamins,team,acc);
        insert bcc;
        
        list<Rule_Parameter__c> ruleParams = [SELECT Id, Name, Parameter_Name__c, Measure_Master__c FROM Rule_Parameter__c WHERE Measure_Master__c =: rule.Id];
        
        
        Grid_Master__c ggMaster = TestDataFactory.gridMaster(country);
        insert ggMaster;
        
        Parameter__c param = TestDataFactory.parameter(pcc,team,teamins);
        insert param;
        
        
        Rule_Parameter__c ruleparameter = new Rule_Parameter__c();
        ruleparameter.Measure_Master__c = rule.id;
        ruleparameter.Parameter__c = param.id;
        ruleparameter.CurrencyIsoCode='USD';
        insert ruleparameter;
        
        
        Compute_Master__c ccMaster = TestDataFactory.createComputeMaster(ruleparameter);
        insert ccMaster;
        
        Step__c st= TestDataFactory.step(ccMaster,ggMaster,rule, ruleparameter);
        st.UI_Location__c = 'Compute TCF';
        insert st;
        
        Rule_Parameter__c ruleparameter1 = TestDataFactory.ruleParameter(rule,param,st);
        //ruleparameter.Parameter_Name__c = rpp.Parameter_Name__c;
        insert ruleparameter1;
        
        MetaData_Definition__c metaDataDef = TestDataFactory.createMetaDataDefinition(teamins,pcc,team);
        insert metaDataDef;
        
        map<String,BU_Response__c> buresponsemap = new map<String, BU_Response__c>();
        buresponsemap.put(positionAccountCallPlan.id,bcc);
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            ruleExecuteHelper.updateTCFSegmentChangeExecute(buresponsemap);
        }
        Test.stopTest();
    }*/
    
    @istest static void ruleExecuteHelperTest3()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        insert acc;
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        
        AxtriaSalesIQTM__Organization_Master__c org =TestDataFactory.createOrganizationMaster();
        insert org;
        
        AxtriaSalesIQTM__Country__c country = TestDataFactory.createCountry(org);
        insert country;
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, country);
        insert pcc;
        
        Measure_Master__c rule = TestDataFactory.createMeasureMaster(pcc,team,teamins);
        insert rule;
        
        AxtriaSalesIQTM__Position__c position = TestDataFactory.createPosition(team,teamins);
        insert position;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,position,teamins);
        insert posAccount;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(rule,acc,teamins,posAccount,pPriority,position);
        insert positionAccountCallPlan;
        
        BU_Response__c bcc = TestDataFactory.createBuResponse(posAccount,pcc,teamins,team,acc);
        insert bcc;
        
        list<Rule_Parameter__c> ruleParams = [SELECT Id, Name, Parameter_Name__c, Measure_Master__c FROM Rule_Parameter__c WHERE Measure_Master__c =: rule.Id];
        
        
        Grid_Master__c ggMaster = TestDataFactory.gridMaster(country);
        insert ggMaster;
        
        Parameter__c param = TestDataFactory.parameter(pcc,team,teamins);
        insert param;
        
        Rule_Parameter__c ruleparameter = new Rule_Parameter__c();
        ruleparameter.Measure_Master__c = rule.id;
        ruleparameter.Parameter__c = param.id;
        ruleparameter.CurrencyIsoCode='USD';
        insert ruleparameter;
        
        Compute_Master__c ccMaster = TestDataFactory.createComputeMaster(ruleparameter);
        insert ccMaster;
        
        Step__c st= TestDataFactory.step(ccMaster,ggMaster,rule, ruleparameter);
        st.UI_Location__c = 'Compute TCF';
        insert st;
        
        Rule_Parameter__c ruleparameter1 = TestDataFactory.ruleParameter(rule,param,st);
        //ruleparameter.Parameter_Name__c = rpp.Parameter_Name__c;
        insert ruleparameter1;
        
        MetaData_Definition__c metaDataDef = TestDataFactory.createMetaDataDefinition(teamins,pcc,team);
        insert metaDataDef;
        
        List<String> pacps = new List<String>();
        pacps.add(positionAccountCallPlan.id);
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            ruleExecuteHelper.updateTCFSahil(pacps);
        }
        Test.stopTest();
    }
    
  /*  @istest static void ruleExecuteHelperTest4()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        insert acc;
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        
        AxtriaSalesIQTM__Organization_Master__c org =TestDataFactory.createOrganizationMaster();
        insert org;
        
        AxtriaSalesIQTM__Country__c country = TestDataFactory.createCountry(org);
        insert country;
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, country);
        insert pcc;
        
        Measure_Master__c rule = TestDataFactory.createMeasureMaster(pcc,team,teamins);
        insert rule;
        
        AxtriaSalesIQTM__Position__c position = TestDataFactory.createPosition(team,teamins);
        insert position;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,position,teamins);
        insert posAccount;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(rule,acc,teamins,posAccount,pPriority,position);
        insert positionAccountCallPlan;
        
        BU_Response__c bcc = TestDataFactory.createBuResponse(posAccount,pcc,teamins,team,acc);
        insert bcc;
        
        //list<Rule_Parameter__c> ruleParams = [SELECT Id, Name, Parameter_Name__c, Measure_Master__c FROM Rule_Parameter__c WHERE Measure_Master__c =: rule.Id];
        
        
        
        Grid_Master__c ggMaster = TestDataFactory.gridMaster(country);
        insert ggMaster;
        
        Parameter__c param = TestDataFactory.parameter(pcc,team,teamins);
        insert param;
        
        Rule_Parameter__c ruleparameter = new Rule_Parameter__c();
        ruleparameter.Measure_Master__c = rule.id;
        ruleparameter.Parameter__c = param.id;
        ruleparameter.CurrencyIsoCode='USD';
        insert ruleparameter;
        
        Compute_Master__c ccMaster = TestDataFactory.createComputeMaster(ruleparameter);
        insert ccMaster;
        
        Step__c st= TestDataFactory.step(ccMaster,ggMaster,rule, ruleparameter);
        st.UI_Location__c = 'Compute TCF';
        insert st;
        
        Rule_Parameter__c ruleparameter1 = TestDataFactory.ruleParameter(rule,param,st);
        //ruleparameter.Parameter_Name__c = rpp.Parameter_Name__c;
        insert ruleparameter1;
        
        MetaData_Definition__c metaDataDef = TestDataFactory.createMetaDataDefinition(teamins,pcc,team);
        insert metaDataDef;
        
        AxtriaSalesIQTM__Team_Instance_Object_Attribute__c teaminstanceobjectattribute = TestDataFactory.createTeamInstanceObjectAttribute(teamins,pcc);
        insert teaminstanceobjectattribute;
        
        Account_Compute_Final__c compFinally = TestDataFactory.createComputeFinal(rule,acc,posAccount,bcc,position);
        insert compFinally;
        
        
        List<String> pacps = new List<String>();
        pacps.add(positionAccountCallPlan.id);
        pacps.add(positionAccountCallPlan.P1__c);
        
        Boolean fromNewPhy;
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
         //ruleExecuteHelper.updateSegment(pacps,fromNewPhy);
        }
        Test.stopTest();
    }
    
    @istest static void ruleExecuteHelperTest5()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        insert acc;
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        
        AxtriaSalesIQTM__Organization_Master__c org =TestDataFactory.createOrganizationMaster();
        insert org;
        
        AxtriaSalesIQTM__Country__c country = TestDataFactory.createCountry(org);
        insert country;
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, country);
        insert pcc;
        
        Measure_Master__c rule = TestDataFactory.createMeasureMaster(pcc,team,teamins);
        rule.Brand_Lookup__c = pcc.id;
        insert rule;
        
        AxtriaSalesIQTM__Position__c position = TestDataFactory.createPosition(team,teamins);
        insert position;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,position,teamins);
        insert posAccount;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(rule,acc,teamins,posAccount,pPriority,position);
        insert positionAccountCallPlan;
        
        BU_Response__c bcc = TestDataFactory.createBuResponse(posAccount,pcc,teamins,team,acc);
        insert bcc;
        
        
        
        
        Grid_Master__c ggMaster = TestDataFactory.gridMaster(country);
        insert ggMaster;
        
        Parameter__c param = TestDataFactory.parameter(pcc,team,teamins);
        insert param;
        
        Rule_Parameter__c ruleparameter = new Rule_Parameter__c();
        ruleparameter.Measure_Master__c = rule.id;
        ruleparameter.Parameter__c = param.id;
        ruleparameter.CurrencyIsoCode='USD';
        insert ruleparameter;
        
        Compute_Master__c ccMaster = TestDataFactory.createComputeMaster(ruleparameter);
        insert ccMaster;
        
        Step__c st= TestDataFactory.step(ccMaster,ggMaster,rule, ruleparameter);
        st.UI_Location__c = 'Compute TCF';
        insert st;
        
        Rule_Parameter__c ruleparameter1 = TestDataFactory.ruleParameter(rule,param,st);
        //ruleparameter.Parameter_Name__c = rpp.Parameter_Name__c;
        insert ruleparameter1;
        
        MetaData_Definition__c metaDataDef = TestDataFactory.createMetaDataDefinition(teamins,pcc,team);
        insert metaDataDef;
        
        AxtriaSalesIQTM__Team_Instance_Object_Attribute__c teaminstanceobjectattribute = TestDataFactory.createTeamInstanceObjectAttribute(teamins,pcc);
        insert teaminstanceobjectattribute;
        
        Account_Compute_Final__c compFinally = TestDataFactory.createComputeFinal(rule,acc,posAccount,bcc,position);
        insert compFinally;
        
        List<String> pacps = new List<String>();
        pacps.add(positionAccountCallPlan.id);
        
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        
         //ruleExecuteHelper.updateTCFSahil(pacps);
        
        
    }*/
    
    @istest static void ruleExecuteHelperTest6()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        insert acc;
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        
        AxtriaSalesIQTM__Organization_Master__c org =TestDataFactory.createOrganizationMaster();
        insert org;
        
        AxtriaSalesIQTM__Country__c country = TestDataFactory.createCountry(org);
        insert country;
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, country);
        insert pcc;
        
        Measure_Master__c rule = TestDataFactory.createMeasureMaster(pcc,team,teamins);
        insert rule;
        
        AxtriaSalesIQTM__Position__c position = TestDataFactory.createPosition(team,teamins);
        insert position;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,position,teamins);
        insert posAccount;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(rule,acc,teamins,posAccount,pPriority,position);
        insert positionAccountCallPlan;
        
        BU_Response__c bcc = TestDataFactory.createBuResponse(posAccount,pcc,teamins,team,acc);
        insert bcc;
        
        //list<Rule_Parameter__c> ruleParams = [SELECT Id, Name, Parameter_Name__c, Measure_Master__c FROM Rule_Parameter__c WHERE Measure_Master__c =: rule.Id];
        
        
        
        Grid_Master__c ggMaster = TestDataFactory.gridMaster(country);
        insert ggMaster;
        
        Parameter__c param = TestDataFactory.parameter(pcc,team,teamins);
        insert param;
        
        Rule_Parameter__c ruleparameter = new Rule_Parameter__c();
        ruleparameter.Measure_Master__c = rule.id;
        ruleparameter.Parameter__c = param.id;
        ruleparameter.CurrencyIsoCode='USD';
        insert ruleparameter;
        
        Compute_Master__c ccMaster = TestDataFactory.createComputeMaster(ruleparameter);
        insert ccMaster;
        Step__c st= TestDataFactory.step(ccMaster,ggMaster,rule, ruleparameter);
        st.UI_Location__c = 'Compute TCF';
        insert st;
        
        Rule_Parameter__c ruleparameter1 = TestDataFactory.ruleParameter(rule,param,st);
        //ruleparameter.Parameter_Name__c = rpp.Parameter_Name__c;
        insert ruleparameter1;
        
        MetaData_Definition__c metaDataDef = TestDataFactory.createMetaDataDefinition(teamins,pcc,team);
        insert metaDataDef;
        
        AxtriaSalesIQTM__Team_Instance_Object_Attribute__c teaminstanceobjectattribute = TestDataFactory.createTeamInstanceObjectAttribute(teamins,pcc);
        insert teaminstanceobjectattribute;
        
        Account_Compute_Final__c compFinally = TestDataFactory.createComputeFinal(rule,acc,posAccount,bcc,position);
        insert compFinally;
        
        List<String> accids= new List<String>();
        accids.add(acc.id);
        
        Boolean fromNewPhy;
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
           // ruleExecuteHelper.executeRule(rule,accids);
        }
        Test.stopTest();
    }

  /* @istest static void ruleExecuteHelperTest7()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactory.createAccount();
        insert acc;
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        
        AxtriaSalesIQTM__Organization_Master__c org =TestDataFactory.createOrganizationMaster();
        insert org;
        
        AxtriaSalesIQTM__Country__c country = TestDataFactory.createCountry(org);
        insert country;
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, country);
        insert pcc;
        
        Measure_Master__c rule = TestDataFactory.createMeasureMaster(pcc,team,teamins);
        insert rule;
        
        AxtriaSalesIQTM__Position__c position = TestDataFactory.createPosition(team,teamins);
        insert position;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,position,teamins);
        insert posAccount;
        
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(rule,acc,teamins,posAccount,pPriority,position);
        insert positionAccountCallPlan;
        
        BU_Response__c bcc = TestDataFactory.createBuResponse(posAccount,pcc,teamins,team,acc);
        insert bcc;
        
        //list<Rule_Parameter__c> ruleParams = [SELECT Id, Name, Parameter_Name__c, Measure_Master__c FROM Rule_Parameter__c WHERE Measure_Master__c =: rule.Id];
        
        
        
        Grid_Master__c ggMaster = TestDataFactory.gridMaster(country);
        insert ggMaster;
        
        Parameter__c param = TestDataFactory.parameter(pcc,team,teamins);
        insert param;
        
        Rule_Parameter__c ruleparameter = new Rule_Parameter__c();
        ruleparameter.Measure_Master__c = rule.id;
        ruleparameter.Parameter__c = param.id;
        ruleparameter.CurrencyIsoCode='USD';
        insert ruleparameter;
        
        Compute_Master__c ccMaster = TestDataFactory.createComputeMaster(ruleparameter);
        insert ccMaster;
        
        Step__c st= TestDataFactory.step(ccMaster,ggMaster,rule, ruleparameter);
        st.UI_Location__c = 'Compute TCF';
        insert st;
        
        Rule_Parameter__c ruleparameter1 = TestDataFactory.ruleParameter(rule,param,st);
        //ruleparameter.Parameter_Name__c = rpp.Parameter_Name__c;
        insert ruleparameter1;
        
        MetaData_Definition__c metaDataDef = TestDataFactory.createMetaDataDefinition(teamins,pcc,team);
        insert metaDataDef;
        
        AxtriaSalesIQTM__Team_Instance_Object_Attribute__c teaminstanceobjectattribute = TestDataFactory.createTeamInstanceObjectAttribute(teamins,pcc);
        insert teaminstanceobjectattribute;
        
        Account_Compute_Final__c compFinally = TestDataFactory.createComputeFinal(rule,acc,posAccount,bcc,position);
        insert compFinally;
        
        List<String> accids= new List<String>();
        accids.add(acc.id);
        
        Boolean fromNewPhy;
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
         //ruleExecuteHelper.publishRule(rule,accids);
        }
        Test.stopTest();
    }*/

}