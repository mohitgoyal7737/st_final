public with sharing class BusinessRuleConstructCtlr extends BusinessRule implements IBusinessRule{
    public list<SelectOption> businessUnits {get;set;}
    public list<SelectOption> lines {get;set;}
    public list<SelectOption> brands {get;set;}
    public list<SelectOption> cycles {get;set;}
    public List<SelectOption> selectChannelList {get;set;}
    public list<Measure_Master__c>Bulist {get;set;}
    public set<String>Buset {get;set;}
    public transient List<SLDSPageMessage> PageMessages{get;set;}

    public String countryID {get;set;}
    public String ruleId {get;set;}
    Map<string,string> successErrorMap;

    public list<SelectOption> customerTypes {get;set;} //Shivansh - A1450 options list to select Customer Type
    public Boolean showCustomerType {get;set;}
    public Boolean showChannelList {get;set;}

    public BusinessRuleConstructCtlr(){
        try{
        showChannelList = false;
        ruleId = ApexPages.currentPage().getParameters().get('rid');
       /* countryID = SalesIQUtility.getCookie(SalesIQUtility.getCountryCookieName());
        SnTDMLSecurityUtil.printDebugMessage('##### countryID ' + countryID);
        successErrorMap = SalesIQUtility.checkCountryAccess(countryID);
        SnTDMLSecurityUtil.printDebugMessage('############ successErrorMap ' + successErrorMap);
        if(successErrorMap.containsKey('Success')){
           countryID = successErrorMap.get('Success');               
           SnTDMLSecurityUtil.printDebugMessage('########## countryID from Map ' + countryID);
           //As soon as we get Country ID set it in the cookie and it will applicable for whole application.
           SalesIQUtility.setCookieString('CountryID',countryID);
        }*/
        countryID = MCCP_Utility.getKeyValueFromPlatformCache('SIQCountryID');

        Bulist = new list<Measure_Master__c>();
        Buset= new set<String>();
        fillBulist();

        init();
        fillBusinessUnitOptions();
        fillLineOptions(); 
        //fillBrandOptions();
        fillCycleOptions();
        }
        catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'ComputeTCFCtlr',ruleId);
            } 
    }

    public void fillBulist(){
        try{
                //Added Where clause ---A2661
                Bulist= [select id,Name from Measure_Master__c where Id!=:ruleId and  Country__c=: countryID WITH SECURITY_ENFORCED];
            }
            catch(Exception qe) 
            {
                PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
                SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
            }
        
        for(Measure_Master__c mm : Bulist){
            Buset.add(mm.Name.toUpperCase());
        }
    }




    public void fillCycleOptions(){
        cycles = new list<SelectOption>();
        cycles.add(new SelectOption('None', '--None--'));
        SnTDMLSecurityUtil.printDebugMessage('countryID --->' + countryID);
        List<AxtriaSalesIQTM__Workspace__c> allCycles;
        //Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c
        //List<Cycle__c> allCycles = [SELECT Id, Name FROM Cycle__c where Country__c = :countryID ];
        try{
                allCycles = [SELECT Id, Name FROM AxtriaSalesIQTM__Workspace__c where AxtriaSalesIQTM__Country__c = :countryID WITH SECURITY_ENFORCED];
            }
            catch(Exception qe) 
            {
                PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
                SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
                SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
            }


        SnTDMLSecurityUtil.printDebugMessage('allCycles --->' + allCycles);
        if(allCycles != null && allCycles.size() > 0)
        {
            //selectedCycle = allCycles[0].ID;
            for(AxtriaSalesIQTM__Workspace__c cycle: allCycles){
                cycles.add(new SelectOption(cycle.Id, cycle.Name));
            }            
        }

    }

    public void fillBusinessUnitOptions(){
        businessUnits = new list<SelectOption>();
        businessUnits.add(new SelectOption('None', '--None--'));
        SnTDMLSecurityUtil.printDebugMessage('============ HEY COUNTRY ID IS '+countryID);
        
        //Shivansh - A1450 -- Replacing AZ Cycle with Workspace__c
        /*for(AxtriaSalesIQTM__Team_Instance__c bu: [SELECT Id, Name FROM AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Country__c = :countryID and AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c= :selectedCycle WITH SECURITY_ENFORCED order by AxtriaSalesIQTM__isActiveCycle__c Desc]){
            businessUnits.add(new SelectOption(bu.Id, bu.Name));
        }*/
        List<AxtriaSalesIQTM__Team_Instance__c> butemp;
        try{
            butemp = [SELECT Id, Name FROM AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Country__c = :countryID and AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c= :selectedCycle WITH SECURITY_ENFORCED order by AxtriaSalesIQTM__isActiveCycle__c Desc];
        }
        catch(Exception qe) 
        {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }

        for(AxtriaSalesIQTM__Team_Instance__c bu:butemp){
            businessUnits.add(new SelectOption(bu.Id, bu.Name));
        }
    }

    public void fillLineOptions(){
        /*lines = new list<SelectOption>();
        lines.add(new SelectOption('None', '--None--'));
        if(String.isNotBlank(selectedBuisnessUnit)){
            for(Line__c line: [SELECT Id, Name FROM Line__c WHERE Team_Instance__c =:selectedBuisnessUnit]){
                lines.add(new SelectOption(line.Id, line.Name));
            }
        }*/

        fillBrandOptions();
    }

    public void fillBrandOptions(){
        brands = new list<SelectOption>();
        brands.add(new SelectOption('None', '--None--'));
   try {   
        SnTDMLSecurityUtil.printDebugMessage('Hey Selcted Business unit is '+ selectedBuisnessUnit);
        /*for(Product_Catalog__c product: [SELECT Id, Name FROM Product_Catalog__c where Team_Instance__c =:selectedBuisnessUnit and isActive__c = true WITH SECURITY_ENFORCED]){
            brands.add(new SelectOption(product.Id, product.Name));
        }*/
        List<Product_Catalog__c> producttemp;
        try{
            producttemp = [SELECT Id, Name FROM Product_Catalog__c where Team_Instance__c =:selectedBuisnessUnit and isActive__c = true WITH SECURITY_ENFORCED];
        }
        catch(Exception qe) 
        {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE,ruleId);
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }

        for(Product_Catalog__c product:producttemp){
            brands.add(new SelectOption(product.Id, product.Name));
        }

        //Shivansh - A1450

        //Shivansh - A1450 options list to select Customer Type 
        showCustomerType = false;
        customerTypes = new list<SelectOption>();
        customerTypes.add(new SelectOption('None', '--None--'));
        customerTypes.add(new SelectOption('HCP', 'HCP'));
        customerTypes.add(new SelectOption('HCO', 'HCO'));
        List<AxtriaSalesIQTM__Team_Instance__c> ti = [Select IsHCOSegmentationEnabled__c,MCCP_Enabled__c,AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.MCCP_Enabled__c from AxtriaSalesIQTM__Team_Instance__c where Id =:selectedBuisnessUnit WITH SECURITY_ENFORCED];
        if(ti!=null && ti.size() > 0)
        {
            if(ti[0].IsHCOSegmentationEnabled__c)
            {
                showCustomerType = true;
            }
            else{
                showCustomerType = false;
            }
            if(ti[0].MCCP_Enabled__c && ti[0].AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.MCCP_Enabled__c){
                showChannelList = true;
            }
            else{
                showChannelList = false;
            }
        }

        fillChannelList();
        
        }
     catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'BusinessRuleConstructCtlr',ruleId);
        }

    }
    public void fillChannelList(){
        selectChannelList = new List<SelectOption>();
        List<AxtriaSalesIQTM__Country__c> countryList = [Select Channels__c from AxtriaSalesIQTM__Country__c where ID =:countryID and Channels__c != null and MCCP_Enabled__c = true WITH SECURITY_ENFORCED limit 1];
        List<String> channelList = new List<String>();
        if(countryList!=null && countryList.size()>0){
            channelList = countryList[0].Channels__c.split(';');
        }
        for(String channels : channelList){
            selectChannelList.add(new SelectOption(channels,channels));
        }
    }

    public String msg;
    //Updated by Chirag Ahuja for SR-117
    public void save(){
       try { 
        if(isRuleValid()){
            updateRule('Basic Information', 'Basic Information');
        boolean check = fetchRuleParametersToDisplay();
            if(!check){
            if(errorcheck){
                    msg = System.label.ComputeTCFStep;
                    PageMessages = SLDSPageMessage.add(PageMessages,msg,'error');
                    errorcheck = false;
            }
            /*//Added by Chirag Ahuja for SR-209//
            else if(isEmptyChannel){
                    msg = 'Select Channels';
                    PageMessages = SLDSPageMessage.add(PageMessages,msg,'error');
            }*/
            else{
                    errorcheck = false;
                    PageMessages = SLDSPageMessage.add(PageMessages,'Rule Saved Successfully','info');
                }
            //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Rule Saved Successfully');
            //ApexPages.addMessage(myMsg);
            }
            else
            {
                msg = System.label.ParamaterErrorBR;
                PageMessages = SLDSPageMessage.add(PageMessages,msg,'error');
            }
        }else{
              PageMessages = SLDSPageMessage.add(PageMessages,msg,'error');
            //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, msg);
            //ApexPages.addMessage(myMsg);
        }
        
       }
     catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'BusinessRuleConstructCtlr',ruleId);
        } 
       
    }

    public PageReference saveAndNext(){
        //Updated by Chirag Ahuja for SR-117
        try{
        if(isRuleValid()){
            updateRule('Profiling Parameter', 'Basic Information');
            boolean check = fetchRuleParametersToDisplay();
            if(!check){
                if(errorcheck){
                    msg = System.label.ComputeTCFStep;
                    PageMessages = SLDSPageMessage.add(PageMessages,msg,'error');
                    errorcheck = false;
                    return null;
                }
                /*//Added by Chirag Ahuja for SR-209
                else if(isEmptyChannel){
                    msg = 'Select Channels';
                    PageMessages = SLDSPageMessage.add(PageMessages,msg,'error');
                    return null;
                }*/
                else
                {
                    errorcheck = false;
                    return nextPage('SelectRuleParameters');
                }
            }
            else{
                msg = System.label.ParamaterErrorBR;
                PageMessages = SLDSPageMessage.add(PageMessages,msg,'error');
                return null;
            }
        }else{
                PageMessages = SLDSPageMessage.add(PageMessages,msg,'error');
                //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, msg);
                //ApexPages.addMessage(myMsg);
                return null;
        }
    }
        catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'BusinessRuleConstructCtlr',ruleId);
            return null;
        } 
    }
    @TestVisible
    private boolean isRuleValid(){
        try{
        msg = '';
        if(String.isBlank(ruleObject.Name)){
            msg += 'Rule name cannot be empty <br/>';
        }

        if(String.isBlank(selectedCycle) || selectedCycle == 'None'){
            msg += 'Select a Cycle <br/>';
        }

        if(String.isBlank(selectedBuisnessUnit) || selectedBuisnessUnit == 'None'){
            msg += 'Select a Business Unit <br/>';
        }

       /*if(String.isBlank(selectedLine) || selectedLine == 'None'){
            msg += 'Select a Line <br/>';
        }
        */
        //Shivansh - A1450
        if(showCustomerType && (String.isBlank(selectedCustType) || selectedCustType == 'None')){
            msg += 'Select the Customer Type <br/>';
        }

        if(String.isBlank(selectedBrand) || selectedBrand == 'None'){
            msg += 'Select a Brand <br/>';
        }
        if(showChannelList && (selectedChannels == null || (selectedChannels != null && selectedChannels.isEmpty()))){
            msg += System.Label.ChannelSelectionValidation + '<br/>';
        }
        if(String.isNotBlank(mode) && Buset!= null && Buset.size()>0  &&  String.isNotBlank(ruleObject.Name) && Buset.contains(ruleObject.Name.toUpperCase()) && (mode=='new' || mode== 'edit')){
            msg += 'Rule Name already exists.Kindly Give unique name.';
        }


        if(String.isNotBlank(msg)){
            return false;
        }

        return true;
        }
        catch(Exception qe) {
            PageMessages = SLDSPageMessage.add(PageMessages, 'Some Unexpected Error has occured,please contact your system admin', 'error');
            SalesIQSnTLogger.createUnHandledErrorLogs(qe, SalesIQSnTLogger.BR_MODULE, 'BusinessRuleConstructCtlr',ruleId);
            return null;
        } 
    }
}