@isTest
private class MySetUpProd_Insert_UtilityTest {
    static testMethod void testMethod1() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        
        insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
        new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'VeevaFullLoad')};
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;
        Account acc= TestDataFactory.createAccount();
        acc.AccountNumber ='BH10643156';
        insert acc;
        
        AxtriaSalesIQTM__Affiliation_Network__c affnet = TestDataFactory.createAffliNet(countr);
        insert affnet;
        AxtriaSalesIQTM__Account_Affiliation__c accaff = TestDataFactory.createAcctAffli(acc,affnet);
        insert accaff;
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        //Product_Type__c= :'Detail Topic' and IsActive__c=true]
        pcc.Product_Type__c= 'Detail Topic';
        pcc.IsActive__c=true;
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        Measure_Master__c mmc1 = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc1.Team_Instance__c = teamins.id;
        insert mmc1;
        Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
        pp.Name ='MARKET_ CRC';
        insert pp;
        Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
        rp.Parameter__c = pp.id;
        insert rp;
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        
        AxtriaSalesIQTM__Position_Product__c posprod=TestDataFactory.createPositionProduct(teamins, pos, pcc);
        insert posprod;
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        AxtriaSalesIQTM__Employee__c emp = TestDataFactory.createEmployee('firstName', 'lastName');
        insert emp;
        AxtriaSalesIQTM__Position_Employee__c posemp =TestDatafactory.createPositionEmployee(pos, emp.id, 'Primary', date.today(), date.today()+1);
        
        
        /*IF(AxtriaSalesIQTM__Effective_Start_Date__c > Today(),'Future Active', 
IF(AxtriaSalesIQTM__Effective_Start_Date__c <= Today() && AxtriaSalesIQTM__Effective_End_Date__c >= Today(),'Active', 
IF(AxtriaSalesIQTM__Effective_End_Date__c < Today(),'Inactive','')))*/

insert posemp;

Product_Priority__c pPriority = TestDataFactory.productPriority();
insert pPriority;

AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
positionAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c = true;
insert positionAccountCallPlan;
List<String> allTeamInstances = new List<String>();
allTeamInstances.add(teamins.id);
insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'UpdatePositionCode')};
Test.startTest();
System.runAs(loggedInUser){
    ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
    String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
    List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
    System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
    MySetUpProd_Insert_Utility o3=new MySetUpProd_Insert_Utility(allTeamInstances);
    o3.query = 'Select Id, Name, CurrencyIsoCode, AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Metric10__c, AxtriaSalesIQTM__Metric1__c, AxtriaSalesIQTM__Metric2__c, AxtriaSalesIQTM__Metric3__c, AxtriaSalesIQTM__Metric4__c, AxtriaSalesIQTM__Metric5__c, AxtriaSalesIQTM__Metric6__c, AxtriaSalesIQTM__Metric7__c, AxtriaSalesIQTM__Metric8__c, AxtriaSalesIQTM__Metric9__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Position__r.Employee1__r.Employee_PRID__c, AxtriaSalesIQTM__Position__r.Country_Code_Formula__c, AxtriaSalesIQTM__Position__r.Country_Code__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Product_Master__c, AxtriaSalesIQTM__Product_Weight__c, AxtriaSalesIQTM__Product__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__isActive__c, Business_Days_in_Cycle_Formula__c, Business_Days_in_Cycle__c, Call_Capacity_Formula__c, Calls_Day__c, Effective_Days_in_Field_Formula__c, Holidays__c, Other_Days_Off__c, Product_Catalog__c, Product_Catalog__r.Detail_Group__c, Product_Catalog__r.Veeva_External_ID__c, Vacation_Days__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c  FROM AxtriaSalesIQTM__Position_Product__c ';
    
    Database.executeBatch(o3);
}
Test.stopTest();
}
static testMethod void testMethod2() {
    User loggedInUser = new User(id=UserInfo.getUserId());
    
    insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
    new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'VeevaFullLoad')};
    
    AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    insert orgmas;
    AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
    insert countr;
    
    AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
    insert team;
    AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
    insert teamins;
    
    AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
    workspace.AxtriaSalesIQTM__Country__c = countr.id;
    insert workspace;
    
    
    AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
    insert scen;
    Account acc= TestDataFactory.createAccount();
    acc.AccountNumber ='BH10643156';
    insert acc;
    
    AxtriaSalesIQTM__Affiliation_Network__c affnet = TestDataFactory.createAffliNet(countr);
    insert affnet;
    AxtriaSalesIQTM__Account_Affiliation__c accaff = TestDataFactory.createAcctAffli(acc,affnet);
    insert accaff;
    
    Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
    pcc.Product_Type__c= 'Detail Topic';
    pcc.IsActive__c=true;
    insert pcc;
    
    Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
    mmc.Team_Instance__c = teamins.id;
    insert mmc;
    Measure_Master__c mmc1 = TestDataFactory.createMeasureMaster(pcc, team, teamins);
    mmc1.Team_Instance__c = teamins.id;
    insert mmc1;
    Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
    pp.Name ='MARKET_ CRC';
    insert pp;
    Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
    rp.Parameter__c = pp.id;
    insert rp;
    
    AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
    insert pos;
    
    AxtriaSalesIQTM__Position_Product__c posprod=TestDataFactory.createPositionProduct(teamins, pos, pcc);
    insert posprod;
    AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
    insert posAccount;
    AxtriaSalesIQTM__Employee__c emp = TestDataFactory.createEmployee('firstName', 'lastName');
    emp.AxtriaSalesIQTM__Employee_ID__c ='test';
    insert emp;
    AxtriaSalesIQTM__Position_Employee__c posemp = TestDatafactory.createPositionEmployee(pos, emp.id, 'HCO', date.today(), date.today()+1);
    pos.AxtriaSalesIQTM__Client_Position_Code__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
    insert posemp;
    
    Product_Priority__c pPriority = TestDataFactory.productPriority();
    insert pPriority;
    
    Parent_PACP__c p = TestDataFactory.createParentPACP(teamins,acc,pos);
    insert p;
    SIQ_MC_Cycle_Plan_Target_vod_O__c calsum = TestDataFactory.createSIQ_MC_Cycle_Plan_Target_vod(teamins);
    insert calsum;
    SIQ_MC_Cycle_Plan_Channel_vod_O__c ch = new SIQ_MC_Cycle_Plan_Channel_vod_O__c();
    ch.Team_Instance__c = teamins.id;
    insert ch;
    
    AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
    positionAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c = true;
    positionAccountCallPlan.Final_TCF_Approved__c = 3.0;
    positionAccountCallPlan.Final_TCF__c = 3.0;
    positionAccountCallPlan.P1__c = 'GIST';
    positionAccountCallPlan.Parent_Pacp__c = p.id;
    insert positionAccountCallPlan;
    Veeva_Market_Specific__c v = TestDataFactory.createVeevaMarketSpecific();
    insert v;
    
    
    
    
    List<String> allTeamInstances = new List<String>();
    allTeamInstances.add(teamins.id);
    insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
    new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'VeevaFullLoad')};
    Test.startTest();
    System.runAs(loggedInUser){
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        MySetUpProd_Insert_Utility o3=new MySetUpProd_Insert_Utility(allTeamInstances,false);
        o3.query = 'Select Id, Name, CurrencyIsoCode, AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Metric10__c, AxtriaSalesIQTM__Metric1__c, AxtriaSalesIQTM__Metric2__c, AxtriaSalesIQTM__Metric3__c, AxtriaSalesIQTM__Metric4__c, AxtriaSalesIQTM__Metric5__c, AxtriaSalesIQTM__Metric6__c, AxtriaSalesIQTM__Metric7__c, AxtriaSalesIQTM__Metric8__c, AxtriaSalesIQTM__Metric9__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Position__r.Employee1__r.Employee_PRID__c, AxtriaSalesIQTM__Position__r.Country_Code_Formula__c, AxtriaSalesIQTM__Position__r.Country_Code__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Product_Master__c, AxtriaSalesIQTM__Product_Weight__c, AxtriaSalesIQTM__Product__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__isActive__c, Business_Days_in_Cycle_Formula__c, Business_Days_in_Cycle__c, Call_Capacity_Formula__c, Calls_Day__c, Effective_Days_in_Field_Formula__c, Holidays__c, Other_Days_Off__c, Product_Catalog__c, Product_Catalog__r.Detail_Group__c, Product_Catalog__r.Veeva_External_ID__c, Vacation_Days__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c  FROM AxtriaSalesIQTM__Position_Product__c ';
        
        Database.executeBatch(o3);
    }
    Test.stopTest();
}
static testMethod void testMethod3() {
    User loggedInUser = new User(id=UserInfo.getUserId());
    
    insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
    new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'VeevaFullLoad')};
    
    AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    insert orgmas;
    AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
    insert countr;
    
    AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
    insert team;
    AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
    insert teamins;
    
    AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
    workspace.AxtriaSalesIQTM__Country__c = countr.id;
    insert workspace;
    
    
    AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
    insert scen;
    Account acc= TestDataFactory.createAccount();
    acc.AccountNumber ='BH10643156';
    insert acc;
    
    AxtriaSalesIQTM__Affiliation_Network__c affnet = TestDataFactory.createAffliNet(countr);
    insert affnet;
    AxtriaSalesIQTM__Account_Affiliation__c accaff = TestDataFactory.createAcctAffli(acc,affnet);
    insert accaff;
    
    Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
    pcc.Product_Type__c= 'Detail Topic';
    pcc.IsActive__c=true;
    insert pcc;
    
    Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
    mmc.Team_Instance__c = teamins.id;
    insert mmc;
    Measure_Master__c mmc1 = TestDataFactory.createMeasureMaster(pcc, team, teamins);
    mmc1.Team_Instance__c = teamins.id;
    insert mmc1;
    Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
    pp.Name ='MARKET_ CRC';
    insert pp;
    Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
    rp.Parameter__c = pp.id;
    insert rp;
    
    AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
    insert pos;
    
    AxtriaSalesIQTM__Position_Product__c posprod=TestDataFactory.createPositionProduct(teamins, pos, pcc);
    insert posprod;
    AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
    insert posAccount;
    AxtriaSalesIQTM__Employee__c emp = TestDataFactory.createEmployee('firstName', 'lastName');
    emp.AxtriaSalesIQTM__Employee_ID__c ='test';
    insert emp;
    AxtriaSalesIQTM__Position_Employee__c posemp = TestDatafactory.createPositionEmployee(pos, emp.id, 'HCO', date.today(), date.today()+1);
    pos.AxtriaSalesIQTM__Client_Position_Code__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
    insert posemp;
    
    Product_Priority__c pPriority = TestDataFactory.productPriority();
    insert pPriority;
    
    Parent_PACP__c p = TestDataFactory.createParentPACP(teamins,acc,pos);
    insert p;
    SIQ_MC_Cycle_Plan_Target_vod_O__c calsum = TestDataFactory.createSIQ_MC_Cycle_Plan_Target_vod(teamins);
    insert calsum;
    SIQ_MC_Cycle_Plan_Channel_vod_O__c ch = new SIQ_MC_Cycle_Plan_Channel_vod_O__c();
    ch.Team_Instance__c = teamins.id;
    insert ch;
    
    AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
    positionAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c = true;
    positionAccountCallPlan.Final_TCF_Approved__c = 3.0;
    positionAccountCallPlan.Final_TCF__c = 3.0;
    positionAccountCallPlan.P1__c = 'GIST';
    positionAccountCallPlan.Parent_Pacp__c = p.id;
    insert positionAccountCallPlan;
    Veeva_Market_Specific__c v = TestDataFactory.createVeevaMarketSpecific();
    insert v;
    
    
    
    
    List<String> allTeamInstances = new List<String>();
    allTeamInstances.add(teamins.id);
    insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
    new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'VeevaFullLoad')};
    Test.startTest();
    System.runAs(loggedInUser){
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        MySetUpProd_Insert_Utility o3=new MySetUpProd_Insert_Utility(teamins.Id);
        o3.query = 'Select Id, Name, CurrencyIsoCode, AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Metric10__c, AxtriaSalesIQTM__Metric1__c, AxtriaSalesIQTM__Metric2__c, AxtriaSalesIQTM__Metric3__c, AxtriaSalesIQTM__Metric4__c, AxtriaSalesIQTM__Metric5__c, AxtriaSalesIQTM__Metric6__c, AxtriaSalesIQTM__Metric7__c, AxtriaSalesIQTM__Metric8__c, AxtriaSalesIQTM__Metric9__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Position__r.Employee1__r.Employee_PRID__c, AxtriaSalesIQTM__Position__r.Country_Code_Formula__c, AxtriaSalesIQTM__Position__r.Country_Code__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Product_Master__c, AxtriaSalesIQTM__Product_Weight__c, AxtriaSalesIQTM__Product__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__isActive__c, Business_Days_in_Cycle_Formula__c, Business_Days_in_Cycle__c, Call_Capacity_Formula__c, Calls_Day__c, Effective_Days_in_Field_Formula__c, Holidays__c, Other_Days_Off__c, Product_Catalog__c, Product_Catalog__r.Detail_Group__c, Product_Catalog__r.Veeva_External_ID__c, Vacation_Days__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c  FROM AxtriaSalesIQTM__Position_Product__c ';
        
        Database.executeBatch(o3);
    }
    Test.stopTest();
}
static testMethod void testMethod111() {
    User loggedInUser = new User(id=UserInfo.getUserId());
    
    
    insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
    new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'VeevaFullLoad')};
    
    AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    insert orgmas;
    AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
    countr.Cluster_Information__c = 'SalesIQ Cluster';
    countr.Small_Country__c =true;
    insert countr;
    
    AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
    insert team;
    AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
    insert teamins;
    
    AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
    workspace.AxtriaSalesIQTM__Country__c = countr.id;
    insert workspace;
    
    
    AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
    insert scen;
    Account acc= TestDataFactory.createAccount();
    acc.AccountNumber ='BH10643156';
    insert acc;
    
    AxtriaSalesIQTM__Affiliation_Network__c affnet = TestDataFactory.createAffliNet(countr);
    insert affnet;
    AxtriaSalesIQTM__Account_Affiliation__c accaff = TestDataFactory.createAcctAffli(acc,affnet);
    insert accaff;
    
    Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        //Product_Type__c= :'Detail Topic' and IsActive__c=true]
    pcc.Product_Type__c= 'Detail Topic';
    pcc.IsActive__c=true;
    insert pcc;
    
    Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
    mmc.Team_Instance__c = teamins.id;
    insert mmc;
    Measure_Master__c mmc1 = TestDataFactory.createMeasureMaster(pcc, team, teamins);
    mmc1.Team_Instance__c = teamins.id;
    insert mmc1;
    Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
    pp.Name ='MARKET_ CRC';
    insert pp;
    Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
    rp.Parameter__c = pp.id;
    insert rp;
    
    AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
    insert pos;
    
    AxtriaSalesIQTM__Position_Product__c posprod=TestDataFactory.createPositionProduct(teamins, pos, pcc);
    insert posprod;
    AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
    insert posAccount;
    AxtriaSalesIQTM__Employee__c emp = TestDataFactory.createEmployee('firstName', 'lastName');
    insert emp;
    AxtriaSalesIQTM__Position_Employee__c posemp =TestDatafactory.createPositionEmployee(pos, emp.id, 'Primary', date.today(), date.today()+1);
    
    
        /*IF(AxtriaSalesIQTM__Effective_Start_Date__c > Today(),'Future Active', 
IF(AxtriaSalesIQTM__Effective_Start_Date__c <= Today() && AxtriaSalesIQTM__Effective_End_Date__c >= Today(),'Active', 
IF(AxtriaSalesIQTM__Effective_End_Date__c < Today(),'Inactive','')))*/

insert posemp;

Product_Priority__c pPriority = TestDataFactory.productPriority();
insert pPriority;

AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
positionAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c = true;
insert positionAccountCallPlan;
List<String> allTeamInstances = new List<String>();
allTeamInstances.add(teamins.id);
insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'UpdatePositionCode')};
Test.startTest();
System.runAs(loggedInUser){
    ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
    String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
    List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
    System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
    MySetUpProd_Insert_Utility o3=new MySetUpProd_Insert_Utility(allTeamInstances);
    o3.query = 'Select Id, Name, CurrencyIsoCode, AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Metric10__c, AxtriaSalesIQTM__Metric1__c, AxtriaSalesIQTM__Metric2__c, AxtriaSalesIQTM__Metric3__c, AxtriaSalesIQTM__Metric4__c, AxtriaSalesIQTM__Metric5__c, AxtriaSalesIQTM__Metric6__c, AxtriaSalesIQTM__Metric7__c, AxtriaSalesIQTM__Metric8__c, AxtriaSalesIQTM__Metric9__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Position__r.Employee1__r.Employee_PRID__c, AxtriaSalesIQTM__Position__r.Country_Code_Formula__c, AxtriaSalesIQTM__Position__r.Country_Code__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Product_Master__c, AxtriaSalesIQTM__Product_Weight__c, AxtriaSalesIQTM__Product__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__isActive__c, Business_Days_in_Cycle_Formula__c, Business_Days_in_Cycle__c, Call_Capacity_Formula__c, Calls_Day__c, Effective_Days_in_Field_Formula__c, Holidays__c, Other_Days_Off__c, Product_Catalog__c, Product_Catalog__r.Detail_Group__c, Product_Catalog__r.Veeva_External_ID__c, Vacation_Days__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c  FROM AxtriaSalesIQTM__Position_Product__c ';
    
    Database.executeBatch(o3);
}
Test.stopTest();
}
static testMethod void testMethod1112() {
    User loggedInUser = new User(id=UserInfo.getUserId());
    
    
    insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
    new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'VeevaFullLoad')};
    
    AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    insert orgmas;
    AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
    countr.Cluster_Information__c = 'Veeva Cluster';
    countr.Small_Country__c =false;
    insert countr;
    
    AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
    insert team;
    AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
    insert teamins;
    
    AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
    workspace.AxtriaSalesIQTM__Country__c = countr.id;
    insert workspace;
    
    
    AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
    insert scen;
    Account acc= TestDataFactory.createAccount();
    acc.AccountNumber ='BH10643156';
    insert acc;
    
    AxtriaSalesIQTM__Affiliation_Network__c affnet = TestDataFactory.createAffliNet(countr);
    insert affnet;
    AxtriaSalesIQTM__Account_Affiliation__c accaff = TestDataFactory.createAcctAffli(acc,affnet);
    insert accaff;
    
    Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        //Product_Type__c= :'Detail Topic' and IsActive__c=true]
    pcc.Product_Type__c= 'Detail Topic';
    pcc.IsActive__c=true;
    insert pcc;
    
    Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
    mmc.Team_Instance__c = teamins.id;
    insert mmc;
    Measure_Master__c mmc1 = TestDataFactory.createMeasureMaster(pcc, team, teamins);
    mmc1.Team_Instance__c = teamins.id;
    insert mmc1;
    Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
    pp.Name ='MARKET_ CRC';
    insert pp;
    Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
    rp.Parameter__c = pp.id;
    insert rp;
    
    AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
    insert pos;
    
    AxtriaSalesIQTM__Position_Product__c posprod=TestDataFactory.createPositionProduct(teamins, pos, pcc);
    insert posprod;
    AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
    insert posAccount;
    AxtriaSalesIQTM__Employee__c emp = TestDataFactory.createEmployee('firstName', 'lastName');
    insert emp;
    AxtriaSalesIQTM__Position_Employee__c posemp =TestDatafactory.createPositionEmployee(pos, emp.id, 'Primary', date.today(), date.today()+1);
    
    
        /*IF(AxtriaSalesIQTM__Effective_Start_Date__c > Today(),'Future Active', 
IF(AxtriaSalesIQTM__Effective_Start_Date__c <= Today() && AxtriaSalesIQTM__Effective_End_Date__c >= Today(),'Active', 
IF(AxtriaSalesIQTM__Effective_End_Date__c < Today(),'Inactive','')))*/

insert posemp;

Product_Priority__c pPriority = TestDataFactory.productPriority();
insert pPriority;

AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
positionAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c = true;
insert positionAccountCallPlan;
List<String> allTeamInstances = new List<String>();
allTeamInstances.add(teamins.id);
insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'UpdatePositionCode')};
Test.startTest();
System.runAs(loggedInUser){
    ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
    String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
    List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
    System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
    MySetUpProd_Insert_Utility o3=new MySetUpProd_Insert_Utility(allTeamInstances);
    o3.query = 'Select Id, Name, CurrencyIsoCode, AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Metric10__c, AxtriaSalesIQTM__Metric1__c, AxtriaSalesIQTM__Metric2__c, AxtriaSalesIQTM__Metric3__c, AxtriaSalesIQTM__Metric4__c, AxtriaSalesIQTM__Metric5__c, AxtriaSalesIQTM__Metric6__c, AxtriaSalesIQTM__Metric7__c, AxtriaSalesIQTM__Metric8__c, AxtriaSalesIQTM__Metric9__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Position__r.Employee1__r.Employee_PRID__c, AxtriaSalesIQTM__Position__r.Country_Code_Formula__c, AxtriaSalesIQTM__Position__r.Country_Code__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Product_Master__c, AxtriaSalesIQTM__Product_Weight__c, AxtriaSalesIQTM__Product__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__isActive__c, Business_Days_in_Cycle_Formula__c, Business_Days_in_Cycle__c, Call_Capacity_Formula__c, Calls_Day__c, Effective_Days_in_Field_Formula__c, Holidays__c, Other_Days_Off__c, Product_Catalog__c, Product_Catalog__r.Detail_Group__c, Product_Catalog__r.Veeva_External_ID__c, Vacation_Days__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Cluster_Information__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Country_Veeva_Code__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c  FROM AxtriaSalesIQTM__Position_Product__c ';
    
    Database.executeBatch(o3);
}
Test.stopTest();
}
}