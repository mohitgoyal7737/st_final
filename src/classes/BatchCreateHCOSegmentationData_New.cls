global with sharing class BatchCreateHCOSegmentationData_New implements Database.Batchable<sObject> {
    public String query;
    public Map<String,Map<String,Decimal>> hcoToMapOfParamValMap;
    public String destinationRuleId;
    public String sourceRuleId;
    public Set<String> hcoSet;

    public static Boolean isInteger(String s){
        Boolean ReturnValue;
        try{
            Integer.valueOf(s);
            ReturnValue = TRUE; 
        } catch (Exception e) {
            ReturnValue = FALSE;
        }
        return ReturnValue;
    }

    global BatchCreateHCOSegmentationData_New(String sourceRuleId,String destinationRuleId,Map<String,Map<String,Decimal>> hcoToMapOfParamValMap) {
       
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        System.debug('query --> ' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Account> scope) {
        
     
    }

    global void finish(Database.BatchableContext BC) {
       
    }
}