global class BatchOutboundActivePosGeoData implements Database.Batchable<sObject>, Database.Stateful {
    public String query;
    public Integer recordsProcessed=0;
    public List<String> posCodeList;
    public Set<String> teamInsSet;
    public Set<String> inputGeoIDs;
    public Set<String> countrySet;
    public Set<String> countryIDSet;
    public Set<String> mktCode;

    global BatchOutboundActivePosGeoData() {
        /*query = '';
        posCodeList = new List<String>();
        posCodeList.add('0');
        posCodeList.add('00000');
        posCodeList.add('Unassigned');
        posCodeList.add('Unassigned Territory');

        query = 'SELECT Id,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Geography_Type_Name__c,AxtriaSalesIQTM__Assignment_Status__c,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__Geography__r.Name,AxtriaSalesIQTM__Geography__r.AxtriaSalesIQTM__Parent_Zip_Code__r.Name,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Hierarchy_Level__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.Name, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.Marketing_Code__c,CreatedDate, LastModifiedDate FROM AxtriaSalesIQTM__Position_Geography__c' +
        ' where AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c=\'Current\' and (AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c=\'Live\' or AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c=\'Published\') and  AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c not in: posCodeList and AxtriaSalesIQTM__Team_Instance__c != null and AxtriaSalesIQTM__Position__c != null and AxtriaSalesIQTM__Geography__c != null and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__c != null and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c != null and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__c != null and AxtriaSalesIQTM__Assignment_Status__c =\'Active\' order by AxtriaSalesIQTM__Team_Instance__c';

        system.debug('<<<<<<<<<<------------ Query according to country------------>>>>>>>>>' + query);
        */
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Geography__c> scope) {
        
    }

    global void finish(Database.BatchableContext BC) {

    }
}