@isTest
public class TierByHCPCountCtrl_Test {
    @istest static void TierByHCPCountCtrl_Test()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Test.startTest();
         System.runAs(loggedinuser){
             
         TierByHCPCountCtrl obj=new TierByHCPCountCtrl();
             obj.getAllPosAdoptionMap();
         }
        Test.stopTest();
    }
}
/*@isTest
public class TierByHCPCountCtrl_Test {
    @istest static void TierByHCPCountCtrl_Test()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
         String userLocale = UserInfo.getLocale();
        Country__c country = new Country__c();
        country.name = 'ES';
        country.Delimiter__c =userLocale;
        insert country;
        
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
        team.Name = 'Specialty';
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamIns = new AxtriaSalesIQTM__Team_Instance__c();
        teamIns.AxtriaSalesIQTM__Team__c = team.id;
        insert teamIns;
        
        AxtriaSalesIQTM__Position__c SuperposNation = new AxtriaSalesIQTM__Position__c();
        SuperposNation.AxtriaSalesIQTM__Position_Type__c = 'Nation';
        SuperposNation.Name = 'Chico CA_SPEC22';
        SuperposNation.AxtriaSalesIQTM__Team_iD__c    = team.id;
       // posNation.AXTRIASALESIQTM__HIERARCHY_LEVEL__C='4';
       // posNation.Line__c = line.id;
        insert SuperposNation;
        
        AxtriaSalesIQTM__Position__c posNation = new AxtriaSalesIQTM__Position__c();
        posNation.AxtriaSalesIQTM__Position_Type__c = 'Nation';
        posNation.Name = 'Chico CA_SPEC';
        posNation.AxtriaSalesIQTM__Team_iD__c    = team.id;
        posNation.AxtriaSalesIQTM__Parent_Position__c = SuperposNation.id;
       // posNation.AXTRIASALESIQTM__HIERARCHY_LEVEL__C='4';
       // posNation.Line__c = line.id;
        insert posNation;
        
        AxtriaSalesIQTM__Position__c pos2 = new AxtriaSalesIQTM__Position__c();
        pos2.AxtriaSalesIQTM__Position_Type__c = 'Region';
        pos2.Name = 'Chicago';
        pos2.AxtriaSalesIQTM__Team_iD__c = team.id;
        pos2.AxtriaSalesIQTM__Parent_Position__c = posNation.id;
       // pos2.AXTRIASALESIQTM__HIERARCHY_LEVEL__C='3';
        insert pos2;
        
        AxtriaSalesIQTM__Position__c posdm = new AxtriaSalesIQTM__Position__c();
        posdm.AxtriaSalesIQTM__Position_Type__c = 'District';
        posdm.Name = 'Chico CA_SPEC';
        posdm.AxtriaSalesIQTM__Team_iD__c    = team.id;
        posdm.AxtriaSalesIQTM__Parent_Position__c = pos2.id;
       // posdm.Line__c = line.id;
        //posdm.AXTRIASALESIQTM__HIERARCHY_LEVEL__C='2';
        insert posdm;
        
        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();
        pos.AxtriaSalesIQTM__Position_Type__c = 'Territory';
        pos.Name = 'Chico CA_SPEC';
        pos.AxtriaSalesIQTM__Team_iD__c  = team.id;
        pos.AxtriaSalesIQTM__Parent_Position__c = posdm.id;
        //pos.Line__c = line.id;
        //pos.AXTRIASALESIQTM__HIERARCHY_LEVEL__C='1';
        insert pos;
		System.debug('+++SuperposNation::'+SuperposNation);
        System.debug('+++Nation::'+posNation);
        System.debug('+++Reg::'+pos2);
        System.debug('+++DIS::'+posdm);
        System.debug('+++Ter::'+pos);
        
        AxtriaSalesIQTM__User_Access_Permission__c accessRecs = new AxtriaSalesIQTM__User_Access_Permission__c();
        accessRecs.AxtriaSalesIQTM__Is_Active__c = true;
        accessRecs.AxtriaSalesIQTM__User__c = loggedInUser.id;
        accessRecs.AxtriaSalesIQTM__Team_Instance__c = teamIns.id;
        accessRecs.AxtriaSalesIQTM__Position__c =  pos.id;
        insert accessRecs;
        
        System.debug('=====USerAccesspos:'+accessRecs);
        
        Account acc = new Account();
        acc.Name = 'Chelsea Parson';
        acc.Marketing_Code__c = 'ES';
        acc.AxtriaSalesIQTM__External_Account_Number__c='123';
        acc.BillingStreet='abc';
        insert acc;
        
        
        Account acc2 = new Account();
        acc2.Name = 'a b';
        acc2.Marketing_Code__c = 'ES';
        acc2.AxtriaSalesIQTM__External_Account_Number__c='456';
        acc2.BillingStreet='abc';
        acc2.AxtriaSalesIQTM__FirstName__c='a';
        insert acc2;
        AxtriaSalesIQTM__Position_Account_Call_Plan__c posAcc = new AxtriaSalesIQTM__Position_Account_Call_Plan__c();
        //posAcc.isAccountTarget__c = true;
        posAcc.AxtriaSalesIQTM__Position__c = pos.id;
        posAcc.AxtriaSalesIQTM__Team_Instance__c = teamIns.id;
        posAcc.AxtriaSalesIQTM__Account__c = acc2.id;
        posAcc.AxtriaSalesIQTM__Effective_End_Date__c = Date.newInstance(2016,08,09);
        posAcc.AxtriaSalesIQTM__Effective_Start_Date__c = Date.newInstance(2016,04,04);
        //posAcc.AxtriaSalesIQTM__isIncludedCallPlan__c = true;
        posAcc.AxtriaSalesIQTM__Picklist1_Updated__c = '12';
        posAcc.AxtriaSalesIQTM__Picklist1_Segment_Approved__c = '';
        posAcc.AxtriaSalesIQTM__Picklist1_Segment__c = '6';
        posAcc.AxtriaSalesIQTM__Metric6__c = 4;
        posAcc.AxtriaSalesIQTM__lastApprovedTarget__c = true;
        posAcc.AxtriaSalesIQTM__Change_Status__c = 'Pending for Submission';
        posAcc.Segment__c = 'A';
        posAcc.P1__c = 'BRILIQUE';
        posAcc.Final_TCF_Approved__c = 10;
        posAcc.potential__C = 'High';
        insert posAcc;
        
        System.debug('PACPPPPP:::'+posAcc);
        list<String> posAccList = new list<string>();
        for(integer i=0; i<2; i++){
            AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp = new AxtriaSalesIQTM__Position_Account_Call_Plan__c();
            pacp.AxtriaSalesIQTM__Position__c = pos.id;
            pacp.AxtriaSalesIQTM__Team_Instance__c = teamIns.id;
            pacp.AxtriaSalesIQTM__Account__c = acc.id;
            pacp.AxtriaSalesIQTM__Effective_End_Date__c = Date.newInstance(2016,08,09);
            pacp.AxtriaSalesIQTM__Effective_Start_Date__c = Date.newInstance(2016,04,04);
            //posAcc.AxtriaSalesIQTM__isIncludedCallPlan__c = true;
            pacp.AxtriaSalesIQTM__Picklist1_Updated__c = '18';
            pacp.AxtriaSalesIQTM__Picklist1_Segment_Approved__c = '6';
            pacp.AxtriaSalesIQTM__Picklist1_Segment__c = '6';
            pacp.AxtriaSalesIQTM__Metric6__c = 4;
            pacp.AxtriaSalesIQTM__Change_Status__c = 'Pending for Submission';
            pacp.Segment__c = 'A';
            pacp.P1__c = 'BRILIQUE';
            pacp.Final_TCF_Approved__c = 10;
            pacp.potential__C = 'High';
                //pacp.Line__c= line.id;
            insert pacp;
           // posAccList.add(pacp.id);
        }
        
        
        
    //    Area_Line_Junction__c area = new Area_Line_Junction__c();
    //    area.Team_Instance__c = teamIns.id;
    //    area.Area__c = pos.id;
    //    insert area;
         
        
        Test.startTest();
         System.runAs(loggedinuser){
         String BrandName = 'BRILIQUE';
         
         PageReference myVfPage = Page.TierByHCPPage;
		 Test.setCurrentPage(myVfPage);
         ApexPages.currentPage().getParameters().put('selectedBrandName','BRILIQUE');
         String selectedPosition = (String)pos.id;
         TierByHCPCountCtrl obj=new TierByHCPCountCtrl();
             //obj.getAllPosAdoptionMap();
         }
        Test.stopTest();
    }
}*/