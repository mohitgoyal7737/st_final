global with sharing class BatchdeleteRecordsMCCP implements database.batchable<sobject> {
                   
    string query ;
    string objName;
    List<string> counCode;
    
    global BatchdeleteRecordsMCCP(String ObjectName,String countryAPI,List<String> countryCode){
        objName = ObjectName;
        counCode= new List<String>(countryCode);
        query = 'SELECT Id FROM '+objName +' where '+countryAPI +' in :counCode WITH SECURITY_ENFORCED';
                     
    }
    
    global BatchdeleteRecordsMCCP(List<String> countryCode){
        String countryAPI;
        objName = 'SIQ_MC_Cycle_vod_O__c';
        countryAPI = 'SIQ_Country_Code_AZ__c';
        counCode= new List<String>(countryCode);
        
        system.debug(counCode);
        query = 'SELECT Id FROM '+objName +' where '+countryAPI +' in :counCode WITH SECURITY_ENFORCED';
        system.debug('HKJHKJ' + query);
                     
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){         
        return Database.getQueryLocator(query);
    }      
    
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
      
        //delete scope ;
        SnTDMLSecurityUtil.deleteRecords(scope, 'BatchdeleteRecordsMCCP');
    }
    
    global void finish(Database.BatchableContext BC){
      
        if(objName.contains('SIQ_MC_Cycle_vod_O__c')){
            database.executeBatch(new BatchdeleteRecordsMCCP ('SIQ_MC_Cycle_Plan_vod_O__c','SIQ_Country_Code_AZ__c',counCode));
        }
        else if(objName.contains('SIQ_MC_Cycle_Plan_vod_O__c')){
            database.executeBatch(new BatchdeleteRecordsMCCP ('SIQ_MC_Cycle_Channel_vod_O__c','SIQ_Country_Code_AZ__c',counCode));
        }
        else if(objName.contains('SIQ_MC_Cycle_Channel_vod_O__c')){
            database.executeBatch(new BatchdeleteRecordsMCCP ('SIQ_MC_Cycle_Product_vod_O__c','Country_ID__c',counCode));
        }
        else if(objName.contains('SIQ_MC_Cycle_Product_vod_O__c')){
            database.executeBatch(new BatchdeleteRecordsMCCP ('SIQ_MC_Cycle_Plan_Product_vod_O__c','Country__c',counCode));
        }
        else if(objName.contains('SIQ_MC_Cycle_Plan_Product_vod_O__c')){
            database.executeBatch(new BatchdeleteRecordsMCCP ('SIQ_MC_Cycle_Plan_Target_vod_O__c','CountryID__c',counCode));
        }
        else if(objName.contains('SIQ_MC_Cycle_Plan_Target_vod_O__c')){
            database.executeBatch(new BatchdeleteRecordsMCCP ('SIQ_MC_Cycle_Plan_Channel_vod_O__c','CountryID__c',counCode));
        }
        
    } 

}