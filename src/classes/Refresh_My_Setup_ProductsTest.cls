@isTest
private class Refresh_My_Setup_ProductsTest {

    static testMethod void testMethod2() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
        new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'VeevaFullLoad')};
        
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        insert teamins;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        insert scen;
        Account acc= TestDataFactory.createAccount();
        acc.AccountNumber ='BH10643156';
        insert acc;
        
        AxtriaSalesIQTM__Affiliation_Network__c affnet = TestDataFactory.createAffliNet(countr);
        insert affnet;
        AxtriaSalesIQTM__Account_Affiliation__c accaff = TestDataFactory.createAcctAffli(acc,affnet);
        insert accaff;
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        pcc.Product_Type__c= 'Detail Topic';
        pcc.IsActive__c=true;
        insert pcc;
        
        Measure_Master__c mmc = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc.Team_Instance__c = teamins.id;
        insert mmc;
        Measure_Master__c mmc1 = TestDataFactory.createMeasureMaster(pcc, team, teamins);
        mmc1.Team_Instance__c = teamins.id;
        insert mmc1;
        Parameter__c pp = TestDataFactory.parameter(pcc, team, teamins);
        pp.Name ='MARKET_ CRC';
        insert pp;
        Rule_Parameter__c rp= TestDataFactory.ruleParameterWithoutSteps(mmc, pp);
        rp.Parameter__c = pp.id;
        insert rp;
        
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        
        AxtriaSalesIQTM__Position_Product__c posprod=TestDataFactory.createPositionProduct(teamins, pos, pcc);
        insert posprod;
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        AxtriaSalesIQTM__Employee__c emp = TestDataFactory.createEmployee('firstName', 'lastName');
        emp.AxtriaSalesIQTM__Employee_ID__c ='test';
        insert emp;
        AxtriaSalesIQTM__Position_Employee__c posemp = TestDatafactory.createPositionEmployee(pos, emp.id, 'HCO', date.today(), date.today()+1);
        pos.AxtriaSalesIQTM__Client_Position_Code__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        insert posemp;
        
        Product_Priority__c pPriority = TestDataFactory.productPriority();
        insert pPriority;
        
        Parent_PACP__c p = TestDataFactory.createParentPACP(teamins,acc,pos);
        insert p;
        SIQ_MC_Cycle_Plan_Target_vod_O__c calsum = TestDataFactory.createSIQ_MC_Cycle_Plan_Target_vod(teamins);
        insert calsum;
        SIQ_MC_Cycle_Plan_Channel_vod_O__c ch = new SIQ_MC_Cycle_Plan_Channel_vod_O__c();
        ch.Team_Instance__c = teamins.id;
        insert ch;
        SIQ_My_Setup_Products_vod_O__c sss = new SIQ_My_Setup_Products_vod_O__c();
        sss.SIQ_Country__c = countr.id;
        insert sss;

        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactory.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        positionAccountCallPlan.AxtriaSalesIQTM__lastApprovedTarget__c = true;
        positionAccountCallPlan.Final_TCF_Approved__c = 3.0;
        positionAccountCallPlan.Final_TCF__c = 3.0;
        positionAccountCallPlan.P1__c = 'GIST';
        positionAccountCallPlan.Parent_Pacp__c = p.id;
        insert positionAccountCallPlan;
        Veeva_Market_Specific__c v = TestDataFactory.createVeevaMarketSpecific();
        insert v;
        
        
        
        
        List<String> allTeamInstances = new List<String>();
        allTeamInstances.add(teamins.id);
        insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
        new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'VeevaFullLoad')};
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            Refresh_My_Setup_Products o3=new Refresh_My_Setup_Products(allTeamInstances,false);
            o3.query = 'select id from SIQ_My_Setup_Products_vod_O__c ';
            
            Database.executeBatch(o3);
            
        }
        Test.stopTest();
    }
}