global with sharing class UpdateAccIDInStagingSurveyData implements Database.Batchable<sObject> {
    public String query;
    public String teamInstance;
    public boolean isBatchChainedSurvey = false;
    public boolean isBatchChainedVeeva = false;

    public List<String> myTIproductsIDs;
    public String changeReqID;
    public Boolean check;
    public String status;
    //public List<String> myTIproductsnames;

    global UpdateAccIDInStagingSurveyData(String teamInst, String crId, Boolean chk, String stat) {
        
        myTIproductsIDs = new List<string>();
        //myTIproductsnames = new List<string>();
        for(Product_Catalog__c pc : [Select Id, Veeva_External_ID__c, Name FROM Product_Catalog__c WHERE Team_Instance__r.Name =:teamInst AND IsActive__c = true WITH SECURITY_ENFORCED])
        {
            myTIproductsIDs.add(pc.Veeva_External_ID__c);
            //myTIproductsnames.add(pc.name);
        }

        query = 'Select Id, Account_Number__c FROM Staging_Survey_Data__c WHERE Account_Number__c != Null AND Product_Code__c IN :myTIproductsIDs';
        
        this.query = query;
        this.teamInstance = teamInst;
        this.changeReqID = crId;
        this.check = chk;
        this.status = stat;
        isBatchChainedSurvey = true;
    }

    global UpdateAccIDInStagingSurveyData(String teamInst) {

        myTIproductsIDs = new List<string>();
        //myTIproductsnames = new List<string>();
        for(Product_Catalog__c pc : [Select Id, Veeva_External_ID__c, Name FROM Product_Catalog__c WHERE Team_Instance__r.Name =:teamInst AND IsActive__c = true WITH SECURITY_ENFORCED])
        {
            myTIproductsIDs.add(pc.Veeva_External_ID__c);
            //myTIproductsnames.add(pc.name);
        }
        query = 'Select Id, Account_Number__c FROM Staging_Survey_Data__c WHERE Account_Number__c != Null AND Product_Code__c IN :myTIproductsIDs';
        this.query = query;
        this.teamInstance = teamInst;
        isBatchChainedVeeva = true;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Staging_Survey_Data__c> scope) {
        List<String> accNumSS = new List<String>();
        Map<String,Id> accNumToAccId = new Map<String,Id>();

        List<Staging_Survey_Data__c> surveyDataAcctoBeUpdated = new List<Staging_Survey_Data__c>();
        if(scope != null && scope.size() > 0){
            for(Staging_Survey_Data__c s : scope){
                accNumSS.add(s.Account_Number__c);
            }
        }
        System.debug('accNumSS Size : '+ accNumSS.size());
        List<Account> accID = [Select Id, AccountNumber FROM Account WHERE AccountNumber IN :accNumSS];
        System.debug('accID Size : '+ accID.size());

        for(Account a:accID){
            accNumToAccId.put(a.AccountNumber, a.Id);
        }
        System.debug('accNumToAccId Size : '+ accNumToAccId.keySet().size());
        
        for(Staging_Survey_Data__c s : scope){
            s.Account_Lookup__c = accNumToAccId.get(s.Account_Number__c);
            surveyDataAcctoBeUpdated.add(s);
        }
        System.debug('surveyDataAcctoBeUpdated Size : '+surveyDataAcctoBeUpdated.size());
        if(surveyDataAcctoBeUpdated.size()>0){
            SObjectAccessDecision securityDecision = Security.stripInaccessible(AccessType.UPDATABLE, surveyDataAcctoBeUpdated);
            List<Staging_Survey_Data__c> surveyUpdateList = securityDecision.getRecords();
            update surveyDataAcctoBeUpdated;
        }
    }

    global void finish(Database.BatchableContext BC) {

        List<String> accNumVeeva = new List<String>();
        Map<String,Id> accNumToAccId = new Map<String,Id>();
        List<Staging_Veeva_Cust_Data__c> veevaAcctoBeUpdated = new List<Staging_Veeva_Cust_Data__c>();

        List<Staging_Veeva_Cust_Data__c> veevaAcc = [Select Id, PARTY_ID__c FROM Staging_Veeva_Cust_Data__c WHERE PARTY_ID__c != Null];
        if(veevaAcc != null && veevaAcc.size() > 0){
            for(Staging_Veeva_Cust_Data__c v : veevaAcc){
                accNumVeeva.add(v.PARTY_ID__c);
            }
        }
        System.debug('accNumVeeva Size : '+ accNumVeeva.size());
        List<Account> accID = [Select Id, AccountNumber FROM Account WHERE AccountNumber IN :accNumVeeva];
        System.debug('accID Size : '+ accID.size());

        for(Account a:accID){
            accNumToAccId.put(a.AccountNumber, a.Id);
        }
        System.debug('accNumToAccId Size : '+ accNumToAccId.keySet().size());

        for(Staging_Veeva_Cust_Data__c v : veevaAcc){
            v.Account_Lookup__c = accNumToAccId.get(v.PARTY_ID__c);
            veevaAcctoBeUpdated.add(v);
        }
        System.debug('veevaAcctoBeUpdated Size : '+veevaAcctoBeUpdated.size());
        if(veevaAcctoBeUpdated.size()>0){
            SObjectAccessDecision securityDecision = Security.stripInaccessible(AccessType.UPDATABLE, veevaAcctoBeUpdated);
            List<Staging_Veeva_Cust_Data__c> veevaUpdateList = securityDecision.getRecords();
            update veevaUpdateList;
        }
        //Call Batch to Update Profiling Data Counts in Product catalog. SR-336
        if(isBatchChainedSurvey){
            //Call batch
            populateCountFieldsInSSD batch = new populateCountFieldsInSSD(teamInstance,true,changeReqID,check,status);
            Database.executeBatch(batch,5);
        }
        else if(isBatchChainedVeeva){
            populateCountFieldsInSSD batch = new populateCountFieldsInSSD(teamInstance);
            Database.executeBatch(batch,5);
        }
    }
}