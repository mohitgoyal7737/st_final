public with sharing class SharedHCPCallCalculation {
    /*public List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacprecs  {get;set;}
    public list<string> callplanid ;
    public string query ;
    public set<String>Accountid ;
    public map<string,integer>accprodidmap ;
    public set<string>products ;
    
    public SharedHCPCallCalculation(list<String> pacprecid) {
        callplanid = new list<string>();
        query = '';
        callplanid = pacprecid;
        pacprecs = new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        Accountid = new set<string>();
        accprodidmap = new map<string,integer>();
    }
*/
    public SharedHCPCallCalculation(){

    }
    public  static void calcualtesum(list<String> pacprecid){
        list<string>callplanid = new list<string>();
        string query = '';
        string query2 = '';
        string teaminstance='';
        //callplanid = pacprecid;
        List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacprecs = new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> sharedrecs = new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        set<String>Accountid = new set<string>();
        map<string,integer>accprodidmap = new map<string,integer>();
        set<string>Products = new set<string>();
        //callplanid.add(pacprecid);

        /*query= 'select id,AxtriaSalesIQTM__Account__c,P1__c,Share__c,allcall__c,Final_TCF__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.Cycle__r.name from AxtriaSalesIQTM__Position_Account_Call_Plan__c where ID IN:pacprecid';*/
        query= 'select id,AxtriaSalesIQTM__Account__c,P1__c,Share__c,allcall__c,Final_TCF__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where ID IN:pacprecid';
        SnTDMLSecurityUtil.printDebugMessage('=======Query is:'+query);
        pacprecs = Database.Query(query);
        String cycle = '';

        if(pacprecs !=null && pacprecs.size() >0){
            for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp: pacprecs){
                    teaminstance=pacp.AxtriaSalesIQTM__Team_Instance__c;
                    Accountid.add(pacp.AxtriaSalesIQTM__Account__c);
                    Products.add(pacp.P1__c);
                    //cycle = pacp.AxtriaSalesIQTM__Team_Instance__r.Cycle__c;
                    cycle = pacp.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c;
                    
            }
        }
        SnTDMLSecurityUtil.printDebugMessage('========Accountid:::'+Accountid);
        SnTDMLSecurityUtil.printDebugMessage('========Products:::'+Products);
        SnTDMLSecurityUtil.printDebugMessage('========teaminstance:::'+teaminstance);
        // AxtriaSalesIQTM__Team_Instance__c ti = [select id, AxtriaSalesIQTM__Team__c,Cycle__c from AxtriaSalesIQTM__Team_Instance__c where id = :teaminstance];
        query2= 'select id,AxtriaSalesIQTM__Account__c,P1__c,Share__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c,allcall__c,Final_TCF__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.name from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Account__c IN :Accountid and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c =: cycle and P1__c IN :Products';
        SnTDMLSecurityUtil.printDebugMessage('===QUERY222222:'+query2);
        sharedrecs =  Database.Query(query2);       
        if(sharedrecs !=null && sharedrecs.size() >0){
            for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp: sharedrecs){
                string key=pacp.AxtriaSalesIQTM__Account__c+'_'+pacp.P1__c;
                if(pacp.Share__c){
                    if(!accprodidmap.containsKey(key)){
                        accprodidmap.put(key,Integer.valueOf(pacp.Final_TCF__c));
                    }
                    else
                    {
                        if(accprodidmap.containsKey(key)){
                            integer temp;
                            if(pacp.Final_TCF__c!=null){
                                temp=accprodidmap.get(key)+integer.valueof(pacp.Final_TCF__c);
                                accprodidmap.put(key,temp);
                            }
                            //accprodidmap.put(key,Integer.valueOf(pacp.Final_TCF__c));
                        }
                    }
                }
            }
            SnTDMLSecurityUtil.printDebugMessage('=====accprodidmap::::'+accprodidmap);
            for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp: sharedrecs){
                if(pacp.Share__c){
                    string key=pacp.AxtriaSalesIQTM__Account__c+'_'+pacp.P1__c;
                    SnTDMLSecurityUtil.printDebugMessage('======================KEY FINAL is:::'+key);
                    if(accprodidmap.containsKey(key)){
                        SnTDMLSecurityUtil.printDebugMessage('-------------INSIDE CONTAINS KEY:::'+accprodidmap.containsKey(key));
                        pacp.allcall__c = accprodidmap.get(key)!=null ?accprodidmap.get(key):0;
                    }
                }
                else{
                    pacp.allcall__c = pacp.Final_TCF__c !=null ? pacp.Final_TCF__c : 0;
                }
            }
            SnTDMLSecurityUtil.printDebugMessage('======sharedrecs==:'+sharedrecs);
            //update sharedrecs;
            SnTDMLSecurityUtil.updateRecords(sharedrecs, 'SharedHCPCallCalculation');
        }

    }
}