public class MasterRosterUpgrade {
    public string selectedCountry {get;set;}
    public string selectedTeam {get;set;}
    public map<Id,list<AxtriaSalesIQTM__Position__c>> teamInstancePositionMap {get;set;}
    public map<Id,AxtriaSalesIQTM__Team_Instance__c> teamInstanceMap {get;set;}
    map<string,Id> masterPositionMap;
    set<Id> teamInstanceIds;
    public map<string,list<AxtriaSalesIQTM__Position_Employee__c>> positionEmployeeMap{get;set;}
    public List<SelectOption> teamOptions {get;set;}
    
    public MasterRosterUpgrade(){
        getTeamOptions();
        teamInstancePositionMap = new map<Id,list<AxtriaSalesIQTM__Position__c>>();
        selectedTeam = apexpages.currentpage().getparameters().get('selectedTeam');
        if(selectedTeam != null && selectedTeam != ''){
            validateUpgrade();
        }
    }
    
    public List<SelectOption> getCountryOptions() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('None','None'));
        for(AxtriaSalesIQTM__Country__c country : [SELECT Id, Name FROM AxtriaSalesIQTM__Country__c]){
            options.add(new SelectOption(country.Id,country.Name));
        }
        return options;
    }
    
    
    public void getTeamOptions() {
        system.debug('#### selectedCountry : '+selectedCountry);
        teamOptions = new List<SelectOption>();
        teamOptions.add(new SelectOption('None','None'));
        if(selectedCountry != 'None' && selectedCountry != '' && selectedCountry != null){
            for(AxtriaSalesIQTM__Team__c team : [SELECT Id, Name FROM AxtriaSalesIQTM__Team__c WHERE AxtriaSalesIQTM__Country__c =: selectedCountry]){
                teamOptions.add(new SelectOption(team.Id,team.Name));
            }
        }else{
            for(AxtriaSalesIQTM__Team__c team : [SELECT Id, Name FROM AxtriaSalesIQTM__Team__c Order by AxtriaSalesIQTM__Country__c]){
                teamOptions.add(new SelectOption(team.Id,team.Name));
            }
        }
    }
    
    public void validateUpgrade(){
        try{
            teamInstancePositionMap = new map<Id,list<AxtriaSalesIQTM__Position__c>>();
            
            if(selectedTeam == 'None' || selectedTeam == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Error: Please select a team.'));
                return;
            }
            
            boolean isFutureMaster = false;
            system.debug('#### selectedTeam : '+selectedTeam);
            map<string,list<AxtriaSalesIQTM__Team_Instance__c>> scenarioMap = new map<string,list<AxtriaSalesIQTM__Team_Instance__c>>();
            
            list<AxtriaSalesIQTM__Team_Instance__c> teamInstance = [SELECT Id, Name, AxtriaSalesIQTM__Alignment_Period__c, AxtriaSalesIQTM__Alignment_Type__c, AxtriaSalesIQTM__Team__c, 
                                                                    AxtriaSalesIQTM__IC_EffstartDate__c, AxtriaSalesIQTM__IC_EffEndDate__c, AxtriaSalesIQTM__Team_Instance_Code__c,
                                                                    AxtriaSalesIQTM__Scenario__c, AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Employee_Assignment__c 
                                                                    FROM AxtriaSalesIQTM__Team_Instance__c WHERE AxtriaSalesIQTM__Team__c =: selectedTeam AND 
                                                                    (AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Status__c = 'Active' AND AxtriaSalesIQTM__Scenario__c != NULL) 
                                                                    Order by AxtriaSalesIQTM__IC_EffstartDate__c ASC];
        
            system.debug('#### teamInstance : '+teamInstance);
            
            teamInstanceMap = new map<Id,AxtriaSalesIQTM__Team_Instance__c>(teamInstance);
            
            teamInstanceIds = new set<Id>();
            for(AxtriaSalesIQTM__Team_Instance__c ti : teamInstance){
                teamInstanceIds.add(ti.Id);
                list<AxtriaSalesIQTM__Team_Instance__c> temp = scenarioMap.get(ti.AxtriaSalesIQTM__Alignment_Period__c);
                if(temp == null){
                    temp = new list<AxtriaSalesIQTM__Team_Instance__c>();
                }
                temp.add(ti);
                scenarioMap.put(ti.AxtriaSalesIQTM__Alignment_Period__c,temp);
                if(ti.AxtriaSalesIQTM__Alignment_Period__c == 'Future' && ti.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Employee_Assignment__c == true){
                    isFutureMaster = true;
                }
            }
            
            system.debug('#### scenarioMap : '+scenarioMap);
            system.debug('#### isFutureMaster : '+isFutureMaster);
            
            map<Id, AxtriaSalesIQTM__Position__c> positionMap;
            positionMap = new map<Id,AxtriaSalesIQTM__Position__c>([SELECT Id, Name, AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Effective_End_Date__c, 
                                                                    AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Hierarchy_Level__c, AxtriaSalesIQTM__inactive__c, AxtriaSalesIQTM__IsMaster__c,
                                                                    AxtriaSalesIQTM__Is_Unassigned_Position__c, AxtriaSalesIQTM__Master_Position_Reference__c, AxtriaSalesIQTM__Position_Type__c, 
                                                                    AxtriaSalesIQTM__Related_Position_Type__c, AxtriaSalesIQTM__Team_iD__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c 
                                                                    FROM AxtriaSalesIQTM__Position__c WHERE AxtriaSalesIQTM__Team_Instance__c IN : teamInstanceIds]);
            
            map<string,AxtriaSalesIQTM__Position__c> positionDateMap = new map<string,AxtriaSalesIQTM__Position__c>();
            for(AxtriaSalesIQTM__Position__c pos : positionMap.values()){
                if(!positionDateMap.containsKey(pos.AxtriaSalesIQTM__Client_Position_Code__c)){
                    positionDateMap.put(pos.AxtriaSalesIQTM__Client_Position_Code__c,pos);
                }else{
                    AxtriaSalesIQTM__Position__c tempPos = positionDateMap.get(pos.AxtriaSalesIQTM__Client_Position_Code__c);
                    if(pos.AxtriaSalesIQTM__Effective_Start_Date__c < tempPos.AxtriaSalesIQTM__Effective_Start_Date__c){
                        tempPos.AxtriaSalesIQTM__Effective_Start_Date__c = pos.AxtriaSalesIQTM__Effective_Start_Date__c;
                    }
                    if(pos.AxtriaSalesIQTM__Effective_End_Date__c > tempPos.AxtriaSalesIQTM__Effective_End_Date__c){
                        tempPos.AxtriaSalesIQTM__Effective_End_Date__c = pos.AxtriaSalesIQTM__Effective_End_Date__c;
                    }
                    positionDateMap.put(pos.AxtriaSalesIQTM__Client_Position_Code__c,tempPos);
                }
            }
            
            Date highEndDate = date.newinstance(4000, 12, 31);
            for(AxtriaSalesIQTM__Position__c pos : positionMap.values()){
                pos.AxtriaSalesIQTM__Effective_Start_Date__c = positionDateMap.get(pos.AxtriaSalesIQTM__Client_Position_Code__c).AxtriaSalesIQTM__Effective_Start_Date__c;
                if(pos.AxtriaSalesIQTM__Effective_End_Date__c < pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c){
                    pos.AxtriaSalesIQTM__Effective_End_Date__c = positionDateMap.get(pos.AxtriaSalesIQTM__Client_Position_Code__c).AxtriaSalesIQTM__Effective_End_Date__c;
                }else{
                    pos.AxtriaSalesIQTM__Effective_End_Date__c = highEndDate;
                }
                if(pos.AxtriaSalesIQTM__Effective_End_Date__c > system.today()){
                    pos.AxtriaSalesIQTM__inactive__c = false;
                }else{
                    pos.AxtriaSalesIQTM__inactive__c = true;
                }
            }
            
            
            for(AxtriaSalesIQTM__Position__c pos : positionMap.values()){
                list<AxtriaSalesIQTM__Position__c> tempPosList = teamInstancePositionMap.get(pos.AxtriaSalesIQTM__Team_Instance__c);
                if(tempPosList == null){
                   tempPosList = new list<AxtriaSalesIQTM__Position__c>();
                }
                tempPosList.add(pos);
                teamInstancePositionMap.put(pos.AxtriaSalesIQTM__Team_Instance__c,tempPosList);
            }
            
            system.debug('#### teamInstancePositionMap : '+teamInstancePositionMap);
            
            map<string,Id> masterPositionRefMap = new map<string,Id>();
            masterPositionMap = new map<string,Id>();
            
            if(scenarioMap.containsKey('Past')){
                for(AxtriaSalesIQTM__Team_Instance__c pastTeamInstance : scenarioMap.get('Past')){
                    if(teamInstancePositionMap.containsKey(pastTeamInstance.Id)){
                        for(AxtriaSalesIQTM__Position__c pos : teamInstancePositionMap.get(pastTeamInstance.Id)){
                            if(!masterPositionRefMap.containsKey(pos.AxtriaSalesIQTM__Client_Position_Code__c)){
                                masterPositionRefMap.put(pos.AxtriaSalesIQTM__Client_Position_Code__c,pos.Id);
                            }
                            if(pos.AxtriaSalesIQTM__Effective_End_Date__c > pastTeamInstance.AxtriaSalesIQTM__IC_EffEndDate__c){
                                pos.AxtriaSalesIQTM__IsMaster__c = false;
                            }else{
                                pos.AxtriaSalesIQTM__IsMaster__c = true;
                                masterPositionMap.put(pos.AxtriaSalesIQTM__Client_Position_Code__c,pos.Id);
                            }
                        }
                    }
                }
            }
            
            if(scenarioMap.containsKey('Current')){
                for(AxtriaSalesIQTM__Team_Instance__c currentTeamInstance : scenarioMap.get('Current')){
                    if(teamInstancePositionMap.containsKey(currentTeamInstance.Id)){
                        for(AxtriaSalesIQTM__Position__c pos : teamInstancePositionMap.get(currentTeamInstance.Id)){
                            if(!masterPositionRefMap.containsKey(pos.AxtriaSalesIQTM__Client_Position_Code__c)){
                                masterPositionRefMap.put(pos.AxtriaSalesIQTM__Client_Position_Code__c,pos.Id);
                            }
                            
                            if(isFutureMaster && pos.AxtriaSalesIQTM__Effective_End_Date__c > currentTeamInstance.AxtriaSalesIQTM__IC_EffEndDate__c){
                                pos.AxtriaSalesIQTM__IsMaster__c = false;
                            }else{
                                if(currentTeamInstance.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Employee_Assignment__c){
                                    pos.AxtriaSalesIQTM__IsMaster__c = true;
                                    masterPositionMap.put(pos.AxtriaSalesIQTM__Client_Position_Code__c,pos.Id);
                                }else{
                                    pos.AxtriaSalesIQTM__IsMaster__c = false;
                                }
                            }
                        }
                    }
                }
            }
            
            if(scenarioMap.containsKey('Future')){
                for(AxtriaSalesIQTM__Team_Instance__c futureTeamInstance : scenarioMap.get('Future')){
                    if(teamInstancePositionMap.containsKey(futureTeamInstance.Id)){
                        for(AxtriaSalesIQTM__Position__c pos : teamInstancePositionMap.get(futureTeamInstance.Id)){
                            if(!masterPositionRefMap.containsKey(pos.AxtriaSalesIQTM__Client_Position_Code__c)){
                                masterPositionRefMap.put(pos.AxtriaSalesIQTM__Client_Position_Code__c,pos.Id);
                            }
                            
                            if(teamInstanceMap.get(futureTeamInstance.Id).AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Employee_Assignment__c){
                                pos.AxtriaSalesIQTM__IsMaster__c = true;
                                masterPositionMap.put(pos.AxtriaSalesIQTM__Client_Position_Code__c,pos.Id);
                            }else{
                                pos.AxtriaSalesIQTM__IsMaster__c = false;
                            }
                        }
                    }
                }
            }
            
            for(Id teamInstanceId : teamInstancePositionMap.keyset()){
                /*system.debug('================================================================');
                system.debug('Team Instance : '+teamInstanceMap.get(teamInstanceId).Name);
                system.debug('================================================================');*/
                for(AxtriaSalesIQTM__Position__c pos : teamInstancePositionMap.get(teamInstanceId)){
                    if(masterPositionRefMap.containsKey(pos.AxtriaSalesIQTM__Client_Position_Code__c)){
                        pos.AxtriaSalesIQTM__Master_Position_Reference__c = masterPositionRefMap.get(pos.AxtriaSalesIQTM__Client_Position_Code__c);
                    }
                    /*system.debug('Position Id : '+pos.Id);
                    system.debug('Position Name : '+pos.Name);
                    system.debug('Position code : '+pos.AxtriaSalesIQTM__Client_Position_Code__c);
                    system.debug('Is Master : '+pos.AxtriaSalesIQTM__IsMaster__c);
                    system.debug('Master Reference : '+pos.AxtriaSalesIQTM__Master_Position_Reference__c);
                    system.debug('-------------------------------------------------------------------------------');*/
                }
            }

            /*Validate employee assignments*/
            map<string, AxtriaSalesIQTM__Position_Employee__c> empAsgmtMap = new map<string,AxtriaSalesIQTM__Position_Employee__c>();
            list<AxtriaSalesIQTM__Position_Employee__c> employeeAssignments = [SELECT Id, AxtriaSalesIQTM__Assignment_Status__c,AxtriaSalesIQTM__Assignment_Type__c,AxtriaSalesIQTM__Effective_End_Date__c,
                                                                               AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__Employee__c,AxtriaSalesIQTM__Employee__r.Name, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Position__r.Name, 
                                                                               AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Status__c, 
                                                                               AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__c
                                                                               FROM AxtriaSalesIQTM__Position_Employee__c WHERE AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__c IN : teamInstanceIds 
                                                                               ORDER BY AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Assignment_Type__c, AxtriaSalesIQTM__Assignment_Status__c];
            for(AxtriaSalesIQTM__Position_Employee__c pe : employeeAssignments){
                string mapkey = pe.AxtriaSalesIQTM__Employee__r.Name+'#'+pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c+'#'+pe.AxtriaSalesIQTM__Assignment_Type__c;
                AxtriaSalesIQTM__Position_Employee__c peRec = empAsgmtMap.get(mapkey);
                if(peRec == null){
                    pe.AxtriaSalesIQTM__Position__c = masterPositionMap.get(pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
                    peRec = pe;
                }else{
                    if(peRec.AxtriaSalesIQTM__Effective_Start_Date__c > pe.AxtriaSalesIQTM__Effective_Start_Date__c){
                        peRec.AxtriaSalesIQTM__Effective_Start_Date__c = pe.AxtriaSalesIQTM__Effective_Start_Date__c;
                    }
                    if(peRec.AxtriaSalesIQTM__Effective_End_Date__c < pe.AxtriaSalesIQTM__Effective_End_Date__c){
                        peRec.AxtriaSalesIQTM__Effective_End_Date__c = pe.AxtriaSalesIQTM__Effective_End_Date__c;
                    }
                }
                empAsgmtMap.put(mapkey,peRec);
            }

            positionEmployeeMap = new map<string,list<AxtriaSalesIQTM__Position_Employee__c>>();
            for(AxtriaSalesIQTM__Position_Employee__c empAsgmt : empAsgmtMap.values()){
                if(empAsgmt.AxtriaSalesIQTM__Effective_End_Date__c >= positionMap.get(empAsgmt.AxtriaSalesIQTM__Position__c).AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c){
                    empAsgmt.AxtriaSalesIQTM__Effective_End_Date__c = positionMap.get(empAsgmt.AxtriaSalesIQTM__Position__c).AxtriaSalesIQTM__Effective_End_Date__c;
                }
                list<AxtriaSalesIQTM__Position_Employee__c> temp = positionEmployeeMap.get(empAsgmt.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
                if(temp == null){
                    temp = new list<AxtriaSalesIQTM__Position_Employee__c>();
                }
                temp.add(empAsgmt);
                positionEmployeeMap.put(empAsgmt.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,temp);
            }


        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()+' | '+e.getStackTraceString()));
        }
    }
    
    public void executeUpgrade(){
        try{
            if(selectedTeam == 'None' || selectedTeam == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Error: Please select a team.'));
                return;
            }
            list<AxtriaSalesIQTM__Position__c> positionsToUpdate = new list<AxtriaSalesIQTM__Position__c>();
            map<string,Id> latestRosterScenarioMap = new map<string,Id>();
            for(Id teamInstanceId : teamInstancePositionMap.keyset()){
                AxtriaSalesIQTM__Team_Instance__c teamInstance = teamInstanceMap.get(teamInstanceId);
                if(teamInstance.AxtriaSalesIQTM__Alignment_Period__c == 'Future' && teamInstance.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Employee_Assignment__c){
                    latestRosterScenarioMap.put('latestscenario',teamInstance.AxtriaSalesIQTM__Scenario__c);
                }else if(teamInstance.AxtriaSalesIQTM__Alignment_Period__c == 'Current' && !latestRosterScenarioMap.containsKey('latestscenario')){
                    latestRosterScenarioMap.put('latestscenario',teamInstance.AxtriaSalesIQTM__Scenario__c);
                }
                positionsToUpdate.addAll(teamInstancePositionMap.get(teamInstanceId));
            }
            
            update new list<AxtriaSalesIQTM__Scenario__c>{new AxtriaSalesIQTM__Scenario__c(Id = latestRosterScenarioMap.get('latestscenario'), AxtriaSalesIQTM__IsLatest_Employee_Assignment__c = true)};
            update positionsToUpdate;

            upgradeEmployeeAssignments(teamInstanceIds,masterPositionMap);
            selectedTeam = 'None';
            teamInstancePositionMap = new map<Id,list<AxtriaSalesIQTM__Position__c>>();
            teamInstanceMap = new map<Id,AxtriaSalesIQTM__Team_Instance__c>();

            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Master roster upgraded successfully. Employee assignment backup can be found in Chatter files.'));
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()+' | '+e.getStackTraceString()));
        }
    }
    
    @Future
    private static void upgradeEmployeeAssignments(set<Id> teamInstanceIds, map<string,Id> masterPositionMap){
        system.debug('#### masterPositionMap : '+masterPositionMap);
        string teamId;
        map<Id,AxtriaSalesIQTM__Position__c> positionMap = new map<Id,AxtriaSalesIQTM__Position__c>([SELECT Id, AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c 
                                                                                                     FROM AxtriaSalesIQTM__Position__c WHERE Id IN : masterPositionMap.values()]);

        map<string, AxtriaSalesIQTM__Position_Employee__c> empAsgmtMap = new map<string,AxtriaSalesIQTM__Position_Employee__c>();
        list<AxtriaSalesIQTM__Position_Employee__c> employeeAssignments = [SELECT Id, AxtriaSalesIQTM__Assignment_Status__c,AxtriaSalesIQTM__Assignment_Type__c,AxtriaSalesIQTM__Effective_End_Date__c,
                                                                           AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__Employee__c,AxtriaSalesIQTM__Employee__r.Name, AxtriaSalesIQTM__Position__r.Name, 
                                                                           AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Status__c, 
                                                                           AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__c
                                                                           FROM AxtriaSalesIQTM__Position_Employee__c WHERE AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__c IN : teamInstanceIds 
                                                                           ORDER BY AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Assignment_Type__c, AxtriaSalesIQTM__Assignment_Status__c];

        String csv = 'Id,AxtriaSalesIQTM__Employee__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Assignment_Type__c,AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__Effective_End_Date__c\n';

        list<AxtriaSalesIQTM__Position_Employee__c> peIdsToDelete = new list<AxtriaSalesIQTM__Position_Employee__c>();
        for(AxtriaSalesIQTM__Position_Employee__c pe : employeeAssignments){
            teamId = pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__c;
            /***********************************************************************************/
            csv += pe.id + ',';
            csv += pe.AxtriaSalesIQTM__Employee__c + ',';
            csv += pe.AxtriaSalesIQTM__Position__c + ',';
            csv += pe.AxtriaSalesIQTM__Assignment_Type__c.escapeCsv() + ',';
            csv += pe.AxtriaSalesIQTM__Effective_Start_Date__c + ',';
            csv += pe.AxtriaSalesIQTM__Effective_End_Date__c + '\n';
            /************************************************************************************/

            string mapkey = pe.AxtriaSalesIQTM__Employee__r.Name+'#'+pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c+'#'+pe.AxtriaSalesIQTM__Assignment_Type__c;
            //system.debug('#### mapkey : '+mapkey);
            AxtriaSalesIQTM__Position_Employee__c peRec = empAsgmtMap.get(mapkey);
            if(peRec == null){
                pe.AxtriaSalesIQTM__Position__c = masterPositionMap.get(pe.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
                peRec = pe;
            }else{
                if(peRec.AxtriaSalesIQTM__Effective_Start_Date__c > pe.AxtriaSalesIQTM__Effective_Start_Date__c){
                    peRec.AxtriaSalesIQTM__Effective_Start_Date__c = pe.AxtriaSalesIQTM__Effective_Start_Date__c;
                }
                if(peRec.AxtriaSalesIQTM__Effective_End_Date__c < pe.AxtriaSalesIQTM__Effective_End_Date__c){
                    peRec.AxtriaSalesIQTM__Effective_End_Date__c = pe.AxtriaSalesIQTM__Effective_End_Date__c;
                }
                peIdsToDelete.add(pe);
            }
            empAsgmtMap.put(mapkey,peRec);
        }


        ContentVersion file = new ContentVersion(title = 'PositionEmployees_'+teamId+'_'+system.now()+'.csv',versionData = Blob.valueOf( csv ),pathOnClient = '/PositionEmployee.csv');
        insert file;
        System.debug( file );
        
        system.debug('#### Assignments To Update : '+empAsgmtMap.size());
        system.debug('#### Assignments To Delete : '+peIdsToDelete.size());
        
        for(AxtriaSalesIQTM__Position_Employee__c empAsgmt : empAsgmtMap.values()){
            if(empAsgmt.AxtriaSalesIQTM__Effective_End_Date__c >= positionMap.get(empAsgmt.AxtriaSalesIQTM__Position__c).AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c){
                empAsgmt.AxtriaSalesIQTM__Effective_End_Date__c = positionMap.get(empAsgmt.AxtriaSalesIQTM__Position__c).AxtriaSalesIQTM__Effective_End_Date__c;
            }
            system.debug('#### Employee Assignment : '+empAsgmt.AxtriaSalesIQTM__Employee__c + ' | '+ empAsgmt.AxtriaSalesIQTM__Position__c + ' | '+ empAsgmt.AxtriaSalesIQTM__Effective_Start_Date__c + ' | ' + empAsgmt.AxtriaSalesIQTM__Effective_End_Date__c);
        }

        delete peIdsToDelete;
        update empAsgmtMap.values();
    }

    public PageReference downloadResult(){
        if(teamInstancePositionMap == null || selectedTeam == 'None'){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Error: Please select a team.'));
            return null;
        }
        PageReference pg = new PageReference('/apex/MasterRosterUpgradeDownload?selectedTeam='+selectedTeam);
        pg.setRedirect(true);
        return pg;
    }
}