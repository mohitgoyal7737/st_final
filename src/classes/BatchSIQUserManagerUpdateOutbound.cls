global with sharing class BatchSIQUserManagerUpdateOutbound implements Database.Batchable<sObject>,Database.Stateful {
    public String query;
    public Set<String> setOutboundEmpID;
    public Set<String> insertEmpID;
    public Set<String> insertedSet;
    public Set<String> countrySet;
    public List<SIQ_User_Manager_Role_Outbound__c> statusUpdList;
    public List<SIQ_User_Manager_Role_Outbound__c> insertEmpRecList;
    public List<SIQ_User_Manager_Role_Outbound__c> updateOutboundEmpList;
    public Map<String,String> mapPos2Emp;
    public Map<String,String> mapPos2Channel;
    public Map<String,String> mapPos2ParentPos;
    public Map<String,String> newMapEmp2Pos;
    public Map<String,Map<String,String>> emp2MapType2Pos;
    public Map<String,String> oldMapEmp2Manager;
    public Map<String,String> oldMapEmp2Channel;
    public Map<String,String> oldMapEmp2Pos;
    public Set<ID> duplicateRecs;
    public boolean errorBatches=false;
    public Date fetchDay;
    //public Set<String> insertOrgMasterEmp;
    //public Map<String,String> maporgEmp2Pos;

    global BatchSIQUserManagerUpdateOutbound()
    {
    }

    global BatchSIQUserManagerUpdateOutbound(Set<String> country) 
    {
        query='';
        duplicateRecs = new Set<ID>();
        setOutboundEmpID=new Set<String>();
        insertedSet=new Set<String>();
        insertEmpID=new Set<String>();
        //insertOrgMasterEmp=new Set<String>();
        countrySet=new Set<String>();

        countrySet.addAll(country);
        
        fetchDay = Date.today();
        
        //maporgEmp2Pos=new Map<String,String>();
        mapPos2Emp=new Map<String,String>();
        mapPos2Channel=new Map<String,String>();
        mapPos2ParentPos=new Map<String,String>();
        newMapEmp2Pos=new Map<String,String>();
        emp2MapType2Pos=new Map<String,Map<String,String>>();
        oldMapEmp2Manager=new Map<String,String>();
        oldMapEmp2Channel=new Map<String,String>();
        oldMapEmp2Pos=new Map<String,String>();
        query = 'select AxtriaSalesIQTM__Employee__c from AxtriaSalesIQTM__Position_Employee__c where AxtriaSalesIQTM__Assignment_Status__c=\'Active\' and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__inactive__c=false and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__IsMaster__c=true and AxtriaSalesIQTM__Employee__c != null and AxtriaSalesIQTM__Position__c != null and AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__HR_Status__c = \'Active\' and Country_Code__c in :countrySet';   //added master roster
    }

    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Employee__c> empMasterList) 
    {
        //try
        //{
            statusUpdList=new List<SIQ_User_Manager_Role_Outbound__c>();
            insertEmpRecList=new List<SIQ_User_Manager_Role_Outbound__c>();
            updateOutboundEmpList=new List<SIQ_User_Manager_Role_Outbound__c>();
            insertEmpID=new Set<String>();

            SnTDMLSecurityUtil.printDebugMessage('====To check existing data in Outbound object======');
            
            List<SIQ_User_Manager_Role_Outbound__c> outboundList = [select Id,Client_Territory_Code__c,Employee__c,Employee_PRID__c,Manager__c,Manager_PRID__c,Country__c,Status__c,Channel_ID_AZ__c from SIQ_User_Manager_Role_Outbound__c where Country__c in :countrySet];
            SnTDMLSecurityUtil.printDebugMessage('=====Query::::' +outboundList);
            if(outboundList != null)
            {
                for(SIQ_User_Manager_Role_Outbound__c outbountEmpRec : outboundList)
                {
                    oldMapEmp2Pos.put(outbountEmpRec.Employee__c,outbountEmpRec.Client_Territory_Code__c);
                    oldMapEmp2Manager.put(outbountEmpRec.Employee__c,outbountEmpRec.Manager__c);
                    oldMapEmp2Channel.put(outbountEmpRec.Employee__c,outbountEmpRec.Channel_ID_AZ__c);
                    setOutboundEmpID.add(outbountEmpRec.Employee__c);
                    SnTDMLSecurityUtil.printDebugMessage('====outbountEmpRec.Employee__c:::::' +outbountEmpRec.Employee__c);
                    SnTDMLSecurityUtil.printDebugMessage('====outbountEmpRec.Channel_ID_AZ__c:::::' +outbountEmpRec.Channel_ID_AZ__c);
                    //outbountEmpRec.Status__c='';
                    //statusUpdList.add(outbountEmpRec);
                }
            }
            
            SnTDMLSecurityUtil.printDebugMessage('====To check if employee exists in Outbound object======');
            
            for(AxtriaSalesIQTM__Position_Employee__c empRec : empMasterList)
            {
                if(!setOutboundEmpID.contains(empRec.AxtriaSalesIQTM__Employee__c))
                {
                    insertEmpID.add(empRec.AxtriaSalesIQTM__Employee__c);
                }
            }

            SnTDMLSecurityUtil.printDebugMessage('====To be inserted Employee set:::::' +insertEmpID);
            SnTDMLSecurityUtil.printDebugMessage('====To be inserted Employee set size:::::' +insertEmpID.size()); 

            SnTDMLSecurityUtil.printDebugMessage('====Query on Position List to fill latest pos2emp maps and pos2parent maps=========');
            List<AxtriaSalesIQTM__Position__c> posList =[select Channel_AZ__c,Id,Employee1__c,AxtriaSalesIQTM__Employee__c,AxtriaSalesIQTM__Parent_Position__c,AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Client_Position_Code__c,Employee1_Assignment_Type__c,AxtriaSalesIQTM__Parent_Position__r.Employee1__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__inactive__c=false and AxtriaSalesIQTM__Client_Position_Code__c != null and Employee1__c != null and AxtriaSalesIQTM__IsMaster__c=true and AxtriaSalesIQTM__Team_iD__r.Country_Name__c in :countrySet and AxtriaSalesIQTM__Effective_Start_Date__c  <= :fetchDay];  //added master check

            for(AxtriaSalesIQTM__Position__c posRec : posList)
            {
                mapPos2Emp.put(posRec.AxtriaSalesIQTM__Client_Position_Code__c,posRec.Employee1__c);
                SnTDMLSecurityUtil.printDebugMessage('====posRec.AxtriaSalesIQTM__Client_Position_Code__c::::=========' +posRec.AxtriaSalesIQTM__Client_Position_Code__c);
                SnTDMLSecurityUtil.printDebugMessage('====posRec.Employee1__c::::=========' +posRec.Employee1__c);
                if(posRec.AxtriaSalesIQTM__Parent_Position__c != null)
                {
                    mapPos2ParentPos.put(posRec.AxtriaSalesIQTM__Client_Position_Code__c,posRec.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
                }
                
                if(posRec.Channel_AZ__c != null)
                {
                    mapPos2Channel.put(posRec.AxtriaSalesIQTM__Client_Position_Code__c,posRec.Channel_AZ__c);    
                }
                
            }

            //SnTDMLSecurityUtil.printDebugMessage('=======mapPos2Channel ::::::::' +mapPos2Channel);

            SnTDMLSecurityUtil.printDebugMessage('====Query on Position List for Organization Master(Parent Position is NULL)==============================================');
            //Set<String> setOrgMaster=new Set<String>();
            List<AxtriaSalesIQTM__Position__c> orgposList =[select Id,AxtriaSalesIQTM__Team_iD__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.Name,AxtriaSalesIQTM__Employee__c,AxtriaSalesIQTM__Parent_Position__c,AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Client_Position_Code__c,Employee1_Assignment_Type__c,AxtriaSalesIQTM__Parent_Position__r.Employee1__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__inactive__c=false and AxtriaSalesIQTM__Parent_Position__c = null and AxtriaSalesIQTM__IsMaster__c=true and AxtriaSalesIQTM__Team_iD__r.Country_Name__c in :countrySet and AxtriaSalesIQTM__Effective_Start_Date__c  <= :fetchDay];  //added master check

            for(AxtriaSalesIQTM__Position__c posRec : orgposList)
            {
                mapPos2ParentPos.put(posRec.AxtriaSalesIQTM__Client_Position_Code__c,posRec.AxtriaSalesIQTM__Team_iD__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.Name);
                //setOrgMaster.add(posRec.AxtriaSalesIQTM__Team_iD__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.Name);
                SnTDMLSecurityUtil.printDebugMessage('posRec.AxtriaSalesIQTM__Team_iD__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.Name:::::::' +posRec.AxtriaSalesIQTM__Team_iD__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.Name);
            }

            SnTDMLSecurityUtil.printDebugMessage('========================Fetch all org Master Records for the respective country============================================');
            Set<String> mktCode = new Set<String>();

            List<AxtriaSalesIQTM__Country__c> countryList = [Select Id,Name,AxtriaSalesIQTM__Parent_Organization__c,AxtriaSalesIQTM__Parent_Organization__r.Marketing_Code__c from AxtriaSalesIQTM__Country__c where Name in :countrySet];
            for(AxtriaSalesIQTM__Country__c countryRec : countryList)
            {
                mktCode.add(countryRec.AxtriaSalesIQTM__Parent_Organization__r.Marketing_Code__c);
            }
            SnTDMLSecurityUtil.printDebugMessage('mktCode:::::::' +mktCode);


            //SnTDMLSecurityUtil.printDebugMessage('======emp2MapType2Pos::::::'+emp2MapType2Pos);
            //SnTDMLSecurityUtil.printDebugMessage('======mapPos2Emp::::::'+mapPos2Emp);
            //SnTDMLSecurityUtil.printDebugMessage('======mapPos2ParentPos::::::'+mapPos2ParentPos);



            SnTDMLSecurityUtil.printDebugMessage('Organization Master Handling:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::');
            List<AxtriaSalesIQTM__Organization_Master__c> orgMasterList = [Select id,Name,Public_Group__c from AxtriaSalesIQTM__Organization_Master__c where Marketing_Code__c in :mktCode and Public_Group_SFDC_id__c != null and AxtriaSalesIQTM__Parent_Organization_Name__c != null];
            Map<String,String> orgMaster2Group = new Map<String,String>();
            for(AxtriaSalesIQTM__Organization_Master__c orgRec : orgMasterList)
            {
                orgMaster2Group.put(orgRec.Name,orgRec.Public_Group__c);
            }
            SnTDMLSecurityUtil.printDebugMessage('======orgMaster2Group::::::'+orgMaster2Group);


            SnTDMLSecurityUtil.printDebugMessage('Group Handling:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::');
            List<Group> grpList = [SELECT Id,Name FROM Group where Name != null and Name in :orgMaster2Group.values()];
            Map<String,String> grpName2grpID = new Map<String,String>();
            for(Group grp : grpList)
            {
                grpName2grpID.put(grp.Name,grp.Id);
            }
            SnTDMLSecurityUtil.printDebugMessage('======grpName2grpID::::::'+grpName2grpID);


            SnTDMLSecurityUtil.printDebugMessage('Group member to User::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::');
            List<GroupMember> groupList = [Select GroupId,Id,UserOrGroupId from GroupMember where GroupId != null and UserOrGroupId != null and GroupId in :grpName2grpID.values() and UserOrGroupId != null and GroupId != null];

            Map<String,String> groupID2User = new Map<String,String>();
            Map<String,Set<String>> groupID2UserSet = new Map<String,Set<String>>();

            for(GroupMember grp2userRec : groupList)
            {
                groupID2User.put(grp2userRec.GroupId,grp2userRec.UserOrGroupId);

                //Added for multiple user
                if(!groupID2UserSet.containsKey(grp2userRec.GroupId))
                {
                   Set<String> temp = new Set<String>();
                   temp.add(grp2userRec.UserOrGroupId);
                   groupID2UserSet.put(grp2userRec.GroupId,temp);  
                }
                else
                {
                    groupID2UserSet.get(grp2userRec.GroupId).add(grp2userRec.UserOrGroupId);   
                }
            }
            SnTDMLSecurityUtil.printDebugMessage('======groupID2User::::::'+groupID2User);
            SnTDMLSecurityUtil.printDebugMessage('======groupID2User Set::::::'+groupID2UserSet);

            Set<String> orgMasteruserSet = new Set<String>();
            for(String user :groupID2UserSet.keySet())
            {

                orgMasteruserSet.addAll(groupID2UserSet.get(user));
            }
            

            SnTDMLSecurityUtil.printDebugMessage('User to Federation Identifier:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::');
            List<User> userList = [Select Id, FederationIdentifier from User where FederationIdentifier != null and (Id in :groupID2User.values() or Id in :orgMasteruserSet)];
            Map<String,String> user2fedID = new Map<String,String>();
            for(User userRec : userList)
            {
                user2fedID.put(userRec.Id,userRec.FederationIdentifier);
            }
            SnTDMLSecurityUtil.printDebugMessage('======user2fedID::::::'+user2fedID);


            SnTDMLSecurityUtil.printDebugMessage('Employee ID to PRID:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::');
            List<AxtriaSalesIQTM__Employee__c> employeeList = [Select Id,Employee_PRID__c from AxtriaSalesIQTM__Employee__c where Employee_PRID__c in :user2fedID.values() and AxtriaSalesIQTM__HR_Status__c='Active'];
            Map<String,String> empPRID2empID = new Map<String,String>();
            for(AxtriaSalesIQTM__Employee__c empRec : employeeList)
            {
                empPRID2empID.put(empRec.Employee_PRID__c,empRec.Id);
                SnTDMLSecurityUtil.printDebugMessage('insert org master employee');
                //insertOrgMasterEmp.add(empRec.Employee_PRID__c);
                if(!setOutboundEmpID.contains(empRec.Id)){
                    insertEmpID.add(empRec.Id);
                    SnTDMLSecurityUtil.printDebugMessage('insertEmpID org master' +insertEmpID);
                    SnTDMLSecurityUtil.printDebugMessage('empRec.Id org master' +empRec.Employee_PRID__c);
                 }
            }
            SnTDMLSecurityUtil.printDebugMessage('======empPRID2empID::::::'+empPRID2empID);


            SnTDMLSecurityUtil.printDebugMessage('====Map for Org master User=======================================================================================');

            for(String orgName : orgMaster2Group.keySet())
            {
                //SnTDMLSecurityUtil.printDebugMessage('======orgName org master map ::::::'+orgName);
                String groupName = orgMaster2Group.get(orgName);
                //SnTDMLSecurityUtil.printDebugMessage('======groupName org master map ::::::'+groupName);
                String groupID = grpName2grpID.get(groupName);
                //SnTDMLSecurityUtil.printDebugMessage('======groupID org master map ::::::'+groupID);
                String userID = groupID2User.get(groupID);
                //SnTDMLSecurityUtil.printDebugMessage('======userID org master map ::::::'+userID);
                String fedID = user2fedID.get(userID);
                //SnTDMLSecurityUtil.printDebugMessage('======fedID org master map ::::::'+fedID);
                String empID = empPRID2empID.get(fedID);
                //SnTDMLSecurityUtil.printDebugMessage('======empID org master map ::::::'+empID);
                if(empID != null)
                {
                    mapPos2Emp.put(orgName,empID);
                }
                //SnTDMLSecurityUtil.printDebugMessage('======mapPos2Emp org master map ::::::'+mapPos2Emp);
            }


            ///Till here........................................................................................................................


            SnTDMLSecurityUtil.printDebugMessage('Organization Master Data Flow Handling to Outbound object===========================================================');

            for(String orgName : orgMaster2Group.keySet())
            {
                //SnTDMLSecurityUtil.printDebugMessage('======orgName org master map Organization Master Data Flow Handling::::::'+orgName);
                String groupName = orgMaster2Group.get(orgName);
                //SnTDMLSecurityUtil.printDebugMessage('======groupName org master map Organization Master Data Flow Handling::::::'+groupName);
                String groupID = grpName2grpID.get(groupName);
                //SnTDMLSecurityUtil.printDebugMessage('======groupID org master map Organization Master Data Flow Handling::::::'+groupID);
                Set<String> userIDSet = groupID2UserSet.get(groupID);
                //SnTDMLSecurityUtil.printDebugMessage('======userID org master map Organization Master Data Flow Handling::::::'+userIDSet);
                if(userIDSet != null)
                {
                    for(String userID : userIDSet)
                    {
                        String fedID = user2fedID.get(userID);
                        //SnTDMLSecurityUtil.printDebugMessage('======fedID org master map Organization Master Data Flow Handling::::::'+fedID);
                        String empID = empPRID2empID.get(fedID);
                        //SnTDMLSecurityUtil.printDebugMessage('======empID org master map Organization Master Data Flow Handling::::::'+empID);
                        if(empID != null)
                        {
                            newMapEmp2Pos.put(empID,orgName);
                            //SnTDMLSecurityUtil.printDebugMessage('empID::::: newMapEmp2Pos org master' +empID);
                            //SnTDMLSecurityUtil.printDebugMessage('orgName::::: newMapEmp2Pos org master' +orgName);
                        }
                    }
                }
                //SnTDMLSecurityUtil.printDebugMessage('======maporgEmp2Pos org master map ::::::'+newMapEmp2Pos);
            }


            // till here...............


            SnTDMLSecurityUtil.printDebugMessage('====Query on Position Employee List to check the latest assignments=========');
            List<AxtriaSalesIQTM__Position_Employee__c> peList = [select Id,AxtriaSalesIQTM__Employee__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Assignment_Type__c from AxtriaSalesIQTM__Position_Employee__c where AxtriaSalesIQTM__Assignment_Status__c='Active' and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__inactive__c=false and AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__IsMaster__c=true and Country_Code__c in :countrySet]; //added master check

            SnTDMLSecurityUtil.printDebugMessage('============Fill new maps for Emp2Pos according to Primary and Secondary==============');
            for(AxtriaSalesIQTM__Position_Employee__c posempRec : peList)
            {
            
              if(!newMapEmp2Pos.containsKey(posempRec.AxtriaSalesIQTM__Employee__c)){    
                if(posempRec.AxtriaSalesIQTM__Assignment_Type__c=='Primary'){
                    newMapEmp2Pos.put(posempRec.AxtriaSalesIQTM__Employee__c,posempRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
                    //SnTDMLSecurityUtil.printDebugMessage('======posempRec.AxtriaSalesIQTM__Employee__c Primary::::::'+posempRec.AxtriaSalesIQTM__Employee__c);
                    //SnTDMLSecurityUtil.printDebugMessage('======posempRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c Primary::::::'+posempRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
                }
              }
                
            }

            for(AxtriaSalesIQTM__Position_Employee__c posempRec : peList)
            {
                
                if(!newMapEmp2Pos.containsKey(posempRec.AxtriaSalesIQTM__Employee__c)){
                    if(posempRec.AxtriaSalesIQTM__Assignment_Type__c=='Secondary'){
                        newMapEmp2Pos.put(posempRec.AxtriaSalesIQTM__Employee__c,posempRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
                        //SnTDMLSecurityUtil.printDebugMessage('======posempRec.AxtriaSalesIQTM__Employee__c Secondary::::::'+posempRec.AxtriaSalesIQTM__Employee__c);
                        //SnTDMLSecurityUtil.printDebugMessage('======posempRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c Secondary::::::'+posempRec.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
                    }
                }
                
            }


            SnTDMLSecurityUtil.printDebugMessage('======newMapEmp2Pos::::::'+newMapEmp2Pos);


            SnTDMLSecurityUtil.printDebugMessage('====Insert or Update Outbound Employee Rec');
            List<SIQ_User_Manager_Role_Outbound__c> outboundEmpList = [select Id,Client_Territory_Code__c,Employee__c,Employee_PRID__c,Manager__c,Manager_PRID__c,Status__c,Country__c from SIQ_User_Manager_Role_Outbound__c where Country__c in :countrySet];
            SnTDMLSecurityUtil.printDebugMessage('==============outboundEmpList.size()::::'+outboundEmpList.size());
            SnTDMLSecurityUtil.printDebugMessage('=================outboundEmpList::::'+outboundEmpList);
            if(outboundEmpList != null || outboundEmpList.size() > 0){
                for(SIQ_User_Manager_Role_Outbound__c insertEmp : outboundEmpList){
                    if(!insertEmpID.contains(insertEmp.Employee__c) && insertEmpID !=null && insertEmpID.size() > 0)
                    {
                        
                        SnTDMLSecurityUtil.printDebugMessage('Insert that Employee Records::::::' +insertEmp.Employee__c);
                        for(String s : insertEmpID)
                        {
                            SnTDMLSecurityUtil.printDebugMessage('insert emp Loop :::::::' +s);
                            if(!insertedSet.contains(s))
                            {

                                SIQ_User_Manager_Role_Outbound__c insertEmp1 = new SIQ_User_Manager_Role_Outbound__c();
                                insertEmp1.Employee__c=s;
                                insertEmp1.Client_Territory_Code__c=newMapEmp2Pos.get(s);
                                String pos=newMapEmp2Pos.get(s);
                                String parentPos=mapPos2ParentPos.get(pos);
                                String manager =mapPos2Emp.get(parentPos);                              
                                insertEmp1.Manager__c=manager;
                                insertEmp1.Status__c='Updated';
                                //Changed Recently
                                insertEmp1.External_ID__c=s;
                                insertEmp1.Channel_ID_AZ__c = mapPos2Channel.get(pos);
                                insertEmpRecList.add(insertEmp1);
                                SnTDMLSecurityUtil.printDebugMessage('=======insertEmp1::::::::=====' +insertEmp1);
                                insertedSet.add(insertEmp1.Employee__c);
                                
                            }
                        }
                    }
                    if(newMapEmp2Pos.containsKey(insertEmp.Employee__c)){
                        SnTDMLSecurityUtil.printDebugMessage('Check for the updated Manager Position Data:::::::');
                        Boolean flag = false;
                        //if(newMapEmp2Pos.containsKey(insertEmp.Employee__c))
                        //{
                        SnTDMLSecurityUtil.printDebugMessage('Inside New mapE2P contains the employee' +insertEmp.Employee__c);
                        if(oldMapEmp2Pos.get(insertEmp.Employee__c) != newMapEmp2Pos.get(insertEmp.Employee__c))
                        {
                            flag=true;
                            insertEmp.Client_Territory_Code__c=newMapEmp2Pos.get(insertEmp.Employee__c);
                            insertEmp.Status__c='Updated';
                            SnTDMLSecurityUtil.printDebugMessage('====oldMapEmp2Pos.get(insertEmp.Employee__c):::' +oldMapEmp2Pos.get(insertEmp.Employee__c));
                        }
                        String pos =newMapEmp2Pos.get(insertEmp.Employee__c);
                        SnTDMLSecurityUtil.printDebugMessage('pos' +pos);
                        String parentPos=mapPos2ParentPos.get(pos);
                        SnTDMLSecurityUtil.printDebugMessage('parentPos' +parentPos);
                        String emp =mapPos2Emp.get(parentPos);
                        SnTDMLSecurityUtil.printDebugMessage('emp' +emp);
                        if(oldMapEmp2Manager.get(insertEmp.Employee__c) != emp)
                        {
                            //empRec.AxtriaSalesIQTM__Manager__c = emp;
                            if(emp != null && emp != insertEmp.Employee__c)
                            {
                                insertEmp.Manager__c = emp;
                                insertEmp.Status__c='Updated';
                                flag=true;
                            }
                            else
                            {
                                if(oldMapEmp2Manager.get(insertEmp.Employee__c) != null)
                                {
                                    insertEmp.Manager__c =null;
                                    insertEmp.Status__c='Updated';
                                    flag=true;
                                }
                            }
                            //insertEmp.Status__c='Updated';
                            //flag=true;
                            SnTDMLSecurityUtil.printDebugMessage('====oldMapEmp2Manager.get(insertEmp.Employee__c):::' +oldMapEmp2Manager.get(insertEmp.Employee__c));
                            SnTDMLSecurityUtil.printDebugMessage('====pos:::' +pos);
                            SnTDMLSecurityUtil.printDebugMessage('====parentPos:::' +parentPos);
                            SnTDMLSecurityUtil.printDebugMessage('====emp:::' +emp);
                        }

                        //Channel Handling
                        if(oldMapEmp2Channel.get(insertEmp.Employee__c) != mapPos2Channel.get(pos))
                        {
                            insertEmp.Channel_ID_AZ__c = mapPos2Channel.get(pos);
                            flag=true;
                            insertEmp.Status__c='Updated';
                            SnTDMLSecurityUtil.printDebugMessage('====oldMapEmp2Pos.get(insertEmp.Employee__c):::' +oldMapEmp2Pos.get(insertEmp.Employee__c));
                        }
                        //till here.........
                        SnTDMLSecurityUtil.printDebugMessage('==============emp::::'+emp);
                        SnTDMLSecurityUtil.printDebugMessage('==============mapPos2Channel.get(pos)::::'+mapPos2Channel.get(pos));
                        SnTDMLSecurityUtil.printDebugMessage('==============oldMapEmp2Channel.get(insertEmp.Employee__c)::::'+oldMapEmp2Channel.get(insertEmp.Employee__c));
                       
                        if(flag)
                        {
                            if(duplicateRecs.contains(insertEmp.Employee__c))
                            {
                                SnTDMLSecurityUtil.printDebugMessage('+++++++++++++++++ Duplicate Recs ' + updateOutboundEmpList);
                            }
                            else
                            {
                                updateOutboundEmpList.add(insertEmp);
                                duplicateRecs.add(insertEmp.Employee__c);       
                            }
                            
                        }
                    }
                    else //if(!newMapEmp2Pos.containsKey(insertEmp.Employee__c))
                    {
                        SnTDMLSecurityUtil.printDebugMessage('Inside else part which does not contains the employee' +insertEmp.Employee__c);
                        if(oldMapEmp2Pos.get(insertEmp.Employee__c) != null || oldMapEmp2Manager.get(insertEmp.Employee__c) !=null || oldMapEmp2Channel.get(insertEmp.Employee__c) != null)
                        {
                            insertEmp.Manager__c = null;
                            insertEmp.Client_Territory_Code__c=null;
                            insertEmp.Channel_ID_AZ__c = null;
                            insertEmp.Status__c='Updated';
                            
                            if(duplicateRecs.contains(insertEmp.Employee__c))
                            {
                                SnTDMLSecurityUtil.printDebugMessage('+++++++++++++++++ Duplicate Recs ' + updateOutboundEmpList);
                            }
                            else
                            {
                                updateOutboundEmpList.add(insertEmp);
                                duplicateRecs.add(insertEmp.Employee__c);       
                            }
                            SnTDMLSecurityUtil.printDebugMessage('Employee Record======' +insertEmp);
                        }

                    }
                    //}                    
                }
            }
            //commenting else
            SnTDMLSecurityUtil.printDebugMessage('==========else part==outboundEmpList :'+outboundEmpList.size());
            if(outboundEmpList ==null || outboundEmpList.size()==0){
                SnTDMLSecurityUtil.printDebugMessage('=====Else Part::::::::::::');
                SnTDMLSecurityUtil.printDebugMessage('Loop on insert Employee List::::::' +insertEmpID);
                for(String s : insertEmpID)
                {
                    SnTDMLSecurityUtil.printDebugMessage('Insert all Employees at first');
                    SIQ_User_Manager_Role_Outbound__c insertEmp = new SIQ_User_Manager_Role_Outbound__c();    
                    insertEmp.Employee__c=s;
                    insertEmp.Client_Territory_Code__c=newMapEmp2Pos.get(s);
                    String pos=newMapEmp2Pos.get(s);
                    String parentPos=mapPos2ParentPos.get(pos);
                    String manager =mapPos2Emp.get(parentPos);
                    insertEmp.Manager__c=manager;
                    insertEmp.Status__c='Updated';
                    //Changed Recently
                    insertEmp.External_ID__c=s;

                    insertEmp.Channel_ID_AZ__c = mapPos2Channel.get(pos);

                    insertEmpRecList.add(insertEmp);
                    SnTDMLSecurityUtil.printDebugMessage('=======insertEmp::::::::=====' +insertEmp);
                    
                }
            }

            SnTDMLSecurityUtil.printDebugMessage('=======insertEmpRecList.size()=====' +insertEmpRecList.size());
            if(insertEmpRecList.size() > 0)
                //Changed Recently
                upsert insertEmpRecList External_ID__c;

            SnTDMLSecurityUtil.printDebugMessage('=======updateOutboundEmpList.size()=====' +updateOutboundEmpList.size());
            if(updateOutboundEmpList.size() > 0){
                //update updateOutboundEmpList;
                SnTDMLSecurityUtil.updateRecords(updateOutboundEmpList, 'BatchSIQUserManagerUpdateOutbound');
            }
       /*}  
        catch(Exception e)
           {
                errorBatches=true;
                SnTDMLSecurityUtil.printDebugMessage('++inside catch');
                String Header='User Manager Update batch is failed, so delta has been haulted of this org for Today';
                tryCatchEmailAlert.veevaLoad(Header);
           }*/

    }

    global void finish(Database.BatchableContext BC) 
    {
        // if(!errorBatches)
        // {
             Database.executeBatch(new BatchDeleteEventInUserTerritory(),2000);
        //}
    }
}