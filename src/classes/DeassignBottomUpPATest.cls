@isTest
public class DeassignBottomUpPATest{
    
    static testMethod void testMethod1() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
            new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'PositionGeographyTrigger')};
                AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Name ='USA';
        insert countr;
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc.AxtriaSalesIQTM__Country__c = countr.id;
        acc.AccountNumber = 'BH10461999';
        acc.AxtriaSalesIQTM__AccountType__c = 'HCP';
        acc.Status__c = 'Active';
        
        insert acc;
        Account acc1= TestDataFactory.createAccount();
        acc1.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc1.AxtriaSalesIQTM__Country__c = countr.id;
        acc1.AccountNumber = 'BH10461969';
        acc1.AxtriaSalesIQTM__AccountType__c = 'HCA';
        acc1.Status__c = 'Active';
        
        insert acc1;
        Account acc11= TestDataFactory.createAccount();
        acc11.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc11.AxtriaSalesIQTM__Country__c = countr.id;
        acc11.AccountNumber = 'BH10231869';
        acc11.AxtriaSalesIQTM__AccountType__c = 'HCA';
        acc11.Status__c = 'Active';
        
        insert acc11;
        Account acc111= TestDataFactory.createAccount();
        acc111.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc111.AxtriaSalesIQTM__Country__c = countr.id;
        acc111.AccountNumber = 'BH20461869';
        acc111.AxtriaSalesIQTM__AccountType__c = 'HCA';
        acc111.Status__c = 'Active';
        
        insert acc111;
        

        AxtriaSalesIQTM__Affiliation_Network__c affnet = TestDataFactory.createAffliNet(countr);
        insert affnet;
        AxtriaSalesIQTM__Account_Affiliation__c accaff = TestDataFactory.createAcctAffli(acc,affnet);
       //accaff.Account_Number__c = 'BH10461999';
        accaff.AxtriaSalesIQTM__Root_Account__c = acc1.id;
        accaff.AxtriaSalesIQTM__Parent_Account__c = acc1.Id;
        accaff.AxtriaSalesIQTM__Account__c = acc.Id;
        accaff.Affiliation_Status__c ='Active';
        accaff.AxtriaSalesIQTM__Active__c = true;
        insert accaff;
                

        AxtriaSalesIQTM__Account_Affiliation__c accaff1 = TestDataFactory.createAcctAffli(acc,affnet);
        //accaff1.Account_Number__c = 'BH10461969';
        accaff1.AxtriaSalesIQTM__Root_Account__c = acc1.id;
        accaff1.BU_Rank__c = 2.00;
        accaff1.AxtriaSalesIQTM__Parent_Account__c = acc1.Id;
        accaff1.Affiliation_Status__c ='Active';
        accaff1.AxtriaSalesIQTM__Account__c = acc11.Id;
        accaff1.AxtriaSalesIQTM__Active__c = true;
        insert accaff1;
         

        AxtriaSalesIQTM__Account_Affiliation__c accaff11 = TestDataFactory.createAcctAffli(acc1,affnet);
        //accaff11.Account_Number__c = 'BH10231869';
        accaff11.AxtriaSalesIQTM__Root_Account__c = acc11.id;
        accaff11.BU_Rank__c = 1.00;
        accaff11.AxtriaSalesIQTM__Parent_Account__c = acc11.Id;
        accaff11.Affiliation_Status__c ='Active';
        accaff11.AxtriaSalesIQTM__Account__c = acc111.Id;
        accaff11.AxtriaSalesIQTM__Active__c = true;
        insert accaff11;

         

        AxtriaSalesIQTM__Account_Affiliation__c accaff111 = TestDataFactory.createAcctAffli(acc11,affnet);
        //accaff111.Account_Number__c = 'BH20461869';
        accaff111.AxtriaSalesIQTM__Root_Account__c = acc111.id;
        accaff111.BU_Rank__c = 1.00;
        accaff111.AxtriaSalesIQTM__Parent_Account__c = acc111.Id;
        accaff111.Affiliation_Status__c ='Active';
        accaff111.AxtriaSalesIQTM__Account__c = acc1.Id;
        accaff111.AxtriaSalesIQTM__Active__c = true;
        insert accaff111;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        insert scen;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Name = 'test';
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        //teamins.AxtriaSalesIQTM__Team_Cycle_Name__c = 'Current';
        //teamins.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        insert teamins;
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        posAccount.IsGasAssignment__c = false;
        posAccount.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        posAccount.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        insert posAccount;
        
         AxtriaSalesIQTM__Position_Account__c posAccount1 = TestDataFactory.createPositionAccount(acc1,pos,teamins);
        posAccount1.IsGasAssignment__c = false;
        posAccount1.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        posAccount1.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        insert posAccount1;
        
        temp_Obj__c zt = new temp_Obj__c();
        zt.Status__c ='New';
        zt.Account_Text__c = 'BH10461999';
        zt.Position_Text__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        zt.Team_Instance_Text__c = teamins.Name;
        zt.Country__c = 'USA';
        zt.Account_Type__c = acc.AxtriaSalesIQTM__AccountType__c ;
        zt.Team_Instance_Name__c = teamins.Name;
        zt.Position_Code__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        zt.Rule_Type__c = 'test';
        zt.Object__c ='Deassign_Postiton_Account__c';
        insert zt;
        
        Set<String> setDeassignID  = new Set<String>();
        setDeassignID.add(zt.id);
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD , false));
            DeassignBottomUpPA obj=new DeassignBottomUpPA(setDeassignID,false,'USA');
            Database.executeBatch(obj);
        }
        Test.stopTest();
    }
     static testMethod void testMethod2() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        insert new list<AxtriaSalesIQTM__TriggerContol__c>{new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'ParentPacp'),
            new AxtriaSalesIQTM__TriggerContol__c(AxtriaSalesIQTM__IsStopTrigger__c=false,name = 'PositionGeographyTrigger')};
                AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        countr.Name ='USA';
        insert countr;
        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc.AxtriaSalesIQTM__Country__c = countr.id;
        acc.AccountNumber = 'BH10461999';
        acc.AxtriaSalesIQTM__AccountType__c = 'HCP';
        acc.Status__c = 'Active';
        
        insert acc;
        Account acc1= TestDataFactory.createAccount();
        acc1.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc1.AxtriaSalesIQTM__Country__c = countr.id;
        acc1.AccountNumber = 'BH10461969';
        acc1.AxtriaSalesIQTM__AccountType__c = 'HCA';
        acc1.Status__c = 'Active';
        
        insert acc1;
        Account acc11= TestDataFactory.createAccount();
        acc11.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc11.AxtriaSalesIQTM__Country__c = countr.id;
        acc11.AccountNumber = 'BH10231869';
        acc11.AxtriaSalesIQTM__AccountType__c = 'HCA';
        acc11.Status__c = 'Active';
        
        insert acc11;
        Account acc111= TestDataFactory.createAccount();
        acc111.AxtriaSalesIQTM__Speciality__c ='Cardiology';
        acc111.AxtriaSalesIQTM__Country__c = countr.id;
        acc111.AccountNumber = 'BH20461869';
        acc111.AxtriaSalesIQTM__AccountType__c = 'HCA';
        acc111.Status__c = 'Active';
        
        insert acc111;
        

        AxtriaSalesIQTM__Affiliation_Network__c affnet = TestDataFactory.createAffliNet(countr);
        insert affnet;
        AxtriaSalesIQTM__Account_Affiliation__c accaff = TestDataFactory.createAcctAffli(acc,affnet);
       //accaff.Account_Number__c = 'BH10461999';
        accaff.AxtriaSalesIQTM__Root_Account__c = acc1.id;
        accaff.AxtriaSalesIQTM__Parent_Account__c = acc1.Id;
        accaff.AxtriaSalesIQTM__Account__c = acc.Id;
        accaff.Affiliation_Status__c ='Active';
        accaff.AxtriaSalesIQTM__Active__c = true;
        insert accaff;
                

        AxtriaSalesIQTM__Account_Affiliation__c accaff1 = TestDataFactory.createAcctAffli(acc,affnet);
        //accaff1.Account_Number__c = 'BH10461969';
        accaff1.AxtriaSalesIQTM__Root_Account__c = acc1.id;
        accaff1.BU_Rank__c = 2.00;
        accaff1.AxtriaSalesIQTM__Parent_Account__c = acc1.Id;
        accaff1.Affiliation_Status__c ='Active';
        accaff1.AxtriaSalesIQTM__Account__c = acc11.Id;
        accaff1.AxtriaSalesIQTM__Active__c = true;
        insert accaff1;
         

        AxtriaSalesIQTM__Account_Affiliation__c accaff11 = TestDataFactory.createAcctAffli(acc1,affnet);
        //accaff11.Account_Number__c = 'BH10231869';
        accaff11.AxtriaSalesIQTM__Root_Account__c = acc11.id;
        accaff11.BU_Rank__c = 1.00;
        accaff11.AxtriaSalesIQTM__Parent_Account__c = acc11.Id;
        accaff11.Affiliation_Status__c ='Active';
        accaff11.AxtriaSalesIQTM__Account__c = acc111.Id;
        accaff11.AxtriaSalesIQTM__Active__c = true;
        insert accaff11;

         

        AxtriaSalesIQTM__Account_Affiliation__c accaff111 = TestDataFactory.createAcctAffli(acc11,affnet);
        //accaff111.Account_Number__c = 'BH20461869';
        accaff111.AxtriaSalesIQTM__Root_Account__c = acc111.id;
        accaff111.BU_Rank__c = 1.00;
        accaff111.AxtriaSalesIQTM__Parent_Account__c = acc111.Id;
        accaff111.Affiliation_Status__c ='Active';
        accaff111.AxtriaSalesIQTM__Account__c = acc1.Id;
        accaff111.AxtriaSalesIQTM__Active__c = true;
        insert accaff111;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'Oncology';
        team.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        //team.hasCallPlan__c = true;
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('Oncology', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        scen.AxtriaSalesIQTM__Scenario_Name__c = 'Oncology_S1';
        insert scen;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        teamins.Name = 'test';
        teamins.AxtriaSalesIQTM__Alignment_Period__c ='Current';
        //teamins.AxtriaSalesIQTM__Team_Cycle_Name__c = 'Current';
        //teamins.AxtriaSalesIQTM__Alignment_Type__c ='ZIP';
        insert teamins;
        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        insert pos;
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamins);
        posAccount.IsGasAssignment__c = false;
        posAccount.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        posAccount.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        insert posAccount;
        /*AxtriaSalesIQTM__Position_Account__c posAccount1 = TestDataFactory.createPositionAccount(acc1,pos,teamins);
        posAccount1.IsGasAssignment__c = false;
        posAccount1.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        posAccount1.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        insert posAccount1;
        AxtriaSalesIQTM__Position_Account__c posAccount11 = TestDataFactory.createPositionAccount(acc11,pos,teamins);
        posAccount11.IsGasAssignment__c = false;
        posAccount11.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        posAccount11.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        
        insert posAccount11;
        AxtriaSalesIQTM__Position_Account__c posAccount111 = TestDataFactory.createPositionAccount(acc111,pos,teamins);
        posAccount111.IsGasAssignment__c = true;
        posAccount111.AxtriaSalesIQTM__Effective_Start_Date__c = date.today();
        posAccount111.AxtriaSalesIQTM__Effective_End_Date__c = date.today()+1;
        
        insert posAccount111;
*/        
        temp_Obj__c zt = new temp_Obj__c();
        zt.Status__c ='New';
        zt.Account_Text__c = 'Teest';
        zt.Position_Text__c = 'Teest';
        zt.Account_Text__c = 'Teest';
        zt.Team_Instance_Text__c = teamins.id;
        zt.Country__c = 'USA';
        zt.Account_Type__c = acc1.AxtriaSalesIQTM__AccountType__c ;
        zt.Team_Instance_Name__c = teamins.Name;
        zt.Position_Code__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        zt.Rule_Type__c = 'test';
        zt.Object__c ='Deassign_Postiton_Account__c';
        insert zt;
        /*temp_Obj__c zt1 = new temp_Obj__c();
        zt1.Status__c ='New';
        zt1.Event__c = 'Insert';
        zt1.Team_Instance__c = teamins.id;
        zt1.AccountNumber__c = acc1.AccountNumber;
        zt1.Account_Type__c = acc1.AxtriaSalesIQTM__AccountType__c ;
        zt1.Team_Instance_Name__c = teamins.Name;
        zt1.Position_Code__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        zt1.Rule_Type__c = 'test1';
        zt1.Object__c ='Deassign_Postiton_Account__c';
        insert zt1;
        temp_Obj__c zt11 = new temp_Obj__c();
        zt11.Status__c ='New';
        zt11.Event__c = 'Insert';
        zt11.Team_Instance__c = teamins.id;
        zt11.AccountNumber__c = acc11.AccountNumber;
        zt11.Account_Type__c = acc11.AxtriaSalesIQTM__AccountType__c ;
        zt11.Team_Instance_Name__c = teamins.Name;
        zt11.Position_Code__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        zt11.Rule_Type__c = 'test11';
        zt11.Object__c ='Deassign_Postiton_Account__c';
        insert zt11;
        temp_Obj__c zt111 = new temp_Obj__c();
        zt111.Status__c ='New';
        zt111.Event__c = 'Insert';
        zt111.Team_Instance__c = teamins.id;
        zt111.AccountNumber__c = acc111.AccountNumber;
        zt111.Account_Type__c = acc111.AxtriaSalesIQTM__AccountType__c ;
        zt111.Team_Instance_Name__c = teamins.Name;
        zt111.Position_Code__c = pos.AxtriaSalesIQTM__Client_Position_Code__c;
        zt111.Rule_Type__c = 'test111';
        zt111.Object__c ='Deassign_Postiton_Account__c';
        insert zt111;*/
        Set<String> setDeassignID  = new Set<String>();
        setDeassignID.add(zt.id);
        /*
        setDeassignID.add(zt1.id);
        setDeassignID.add(zt11.id);
        setDeassignID.add(zt111.id);*/
        Test.startTest();
        System.runAs(loggedInUser){
             ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
             System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD , false));
            DeassignBottomUpPA obj=new DeassignBottomUpPA(setDeassignID,false,'USA');
            Database.executeBatch(obj);
            
        }
        Test.stopTest();
    }
      
}