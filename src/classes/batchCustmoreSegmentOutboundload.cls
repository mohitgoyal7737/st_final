/*Deprecated due to Object purge activity on Jan 13th, 2020*/

global class batchCustmoreSegmentOutboundload implements Database.Batchable<sObject> {
   
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = '';
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<AxtriaARSnT__Direct_Load_Product_Matrix__c> scope) {/*
        
        Set<String> workSpace_set = new Set<String>();
        Set<String> CustSeg_AccountID = new Set<String>();
        Set<String> CustSeg_ScenrioID = new Set<String>();
        List<AxtriaARSnT__Direct_Load_Product_Matrix__c> updateScope = new List<AxtriaARSnT__Direct_Load_Product_Matrix__c>();

        for(AxtriaARSnT__Direct_Load_Product_Matrix__c work : scope){
            workSpace_set.add(work.Workspace__c);
            CustSeg_AccountID.add(work.Account_ID__c);
            CustSeg_ScenrioID.add(work.Scenario_Name__c);
        }
        
        List<AxtriaSalesIQTM__Workspace__c> partworkspacelist = [select Id,Name,Countrycode__c,AxtriaSalesIQTM__Workspace_Start_Date__c,AxtriaSalesIQTM__Workspace_End_Date__c from AxtriaSalesIQTM__Workspace__c where Name in: workSpace_set];
        Map<String,String> workspacemap = new Map<String,String>(); 
        Map<String,Date> workspacemapStartdate = new Map<String,Date>();
        Map<String,Date> workspacemapEnddate = new Map<String,Date>();
     
        for(AxtriaSalesIQTM__Workspace__c WS : partworkspacelist){
            workspacemap.put(WS.Name,WS.Countrycode__c);
            workspacemapStartdate.put(WS.Name,WS.AxtriaSalesIQTM__Workspace_Start_Date__c);
            workspacemapEnddate.put(WS.Name,WS.AxtriaSalesIQTM__Workspace_End_Date__c );
        }
         
        List<Account> Accountlist = [select Id,Name,AccountNumber,Type from Account where AccountNumber in: CustSeg_AccountID];
        Map<String,String> CustSegType = new Map<String,String>(); 
       
       for(Account acc : Accountlist){
            CustSegType.put(acc.AccountNumber,acc.Type);
          }
          
         List<AxtriaSalesIQTM__Scenario__c> Scenariolist = [select Id,Name,AxtriaSalesIQTM__Team_Name__r.Name from AxtriaSalesIQTM__Scenario__c where Name in: CustSeg_ScenrioID];
         Map<String,String> CustSegScenrio = new Map<String,String>(); 
         
            for(AxtriaSalesIQTM__Scenario__c SC : Scenariolist){
                    CustSegScenrio.put(SC.Name,SC.AxtriaSalesIQTM__Team_Name__r.name);
                  }
                  
        List<SIQ_Customer_Segment_O__c> partCustSegmentList = New List<SIQ_Customer_Segment_O__c>();
           
        for(AxtriaARSnT__Direct_Load_Product_Matrix__c fixNDirectLoad : scope){ 
    
             SIQ_Customer_Segment_O__c part_CustSegment = New SIQ_Customer_Segment_O__c();
             
             part_CustSegment.Name = fixNDirectLoad .Name; 
             part_CustSegment.SIQ_Position_Code__c = fixNDirectLoad .POSITION__c;   
             part_CustSegment.SIQ_Objective__c = fixNDirectLoad .Objective__c;          
             part_CustSegment.SIQ_Brand_Identifier__c = fixNDirectLoad .Product_ID__c;
             part_CustSegment.SIQ_CUSTOMER_ID__c = fixNDirectLoad .Account_ID__c;
             part_CustSegment.SIQ_Customer_Class__c = CustSegType.get(fixNDirectLoad .Account_ID__c);
             part_CustSegment.SIQ_Segment__c = fixNDirectLoad .Segment__c;
             part_CustSegment.SIQ_Country_Code__c = workspacemap.get(fixNDirectLoad .Workspace__c);
             part_CustSegment.SIQ_Effective_End_Date__c = workspacemapEnddate.get(fixNDirectLoad .Workspace__c);
             part_CustSegment.SIQ_Effective_Start_Date__c = workspacemapStartdate.get(fixNDirectLoad .Workspace__c);
             part_CustSegment.SIQ_Salesforce_Name__c = CustSegScenrio.get(fixNDirectLoad .Scenario_Name__c);
             part_CustSegment.Unique_Id__c = fixNDirectLoad .Account_ID__c + '_'+ fixNDirectLoad .Product_ID__c + '_' + fixNDirectLoad .POSITION__c + '_' + workspacemap.get(fixNDirectLoad .Workspace__c)      ;
             partCustSegmentList.add(part_CustSegment );
        }

        if(partCustSegmentList .size() > 0)
           upsert partCustSegmentList part_CustSegment.Unique_Id__c;

       for(AxtriaARSnT__Direct_Load_Product_Matrix__c inputRec : scope)
       {
          inputRec.Status__c = 'Veeva SIQ Finished';
          updateScope.add(inputRec);
       }
       if(partCustSegmentList .size() > 0)
           update updateScope;
        */
       }  
    
    global void finish(Database.BatchableContext BC) {
    }
}