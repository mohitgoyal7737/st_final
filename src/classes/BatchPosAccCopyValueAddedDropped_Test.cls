@isTest
private class BatchPosAccCopyValueAddedDropped_Test{
     static testmethod void testBatchPosAccCopyValueAddedDropped() {
     
     AxtriaSalesIQTM__TriggerContol__c aaa=new AxtriaSalesIQTM__TriggerContol__c(Name='OverlappingWorkspace',AxtriaSalesIQTM__IsStopTrigger__c=false);
            insert aaa;
        
        if(!AxtriaSalesIQTM__TriggerContol__c.getValues('OverlappingWorkspace').AxtriaSalesIQTM__IsStopTrigger__c){
   AxtriaSalesIQTM__Workspace__c WorkSpace = new AxtriaSalesIQTM__Workspace__c();
       WorkSpace.Name = 'A1930';
       WorkSpace.AxtriaSalesIQTM__Workspace_Start_Date__c = System.Today();
       WorkSpace.AxtriaSalesIQTM__Workspace_End_Date__c = System.Today()+1;
       WorkSpace.AxtriaSalesIQTM__Workspace_Description__c = 'Short Discription';
       insert WorkSpace; 
       
     
   AxtriaSalesIQTM__Organization_Master__c OM = new AxtriaSalesIQTM__Organization_Master__c();
         OM.Name = 'ABC';
         OM.AxtriaSalesIQTM__Parent_Country_Level__c = True;
         OM.AxtriaSalesIQTM__Org_Level__c = 'Global';
         Insert OM;
    
    AxtriaSalesIQTM__Country__c Coun = new AxtriaSalesIQTM__Country__c();
         Coun.Name = 'ABC';
         Coun.AxtriaSalesIQTM__Status__c = 'Active';
         Coun.AxtriaSalesIQTM__Parent_Organization__c = OM.id;
         insert Coun;
         
    AxtriaSalesIQTM__Scenario__c Scenario = new AxtriaSalesIQTM__Scenario__c();
         Scenario.AxtriaSalesIQTM__Workspace__c = WorkSpace.id;
         Scenario.AxtriaSalesIQTM__Scenario_Stage__c = 'Live';
         insert Scenario;
         
     
   AxtriaSalesIQTM__Team__c Team = New AxtriaSalesIQTM__Team__c();
        Team.Name = 'Test';
        Team.AxtriaSalesIQTM__Country__c = Coun.id;
        insert Team;
        
        
    AxtriaSalesIQTM__Team_Instance__c Teaminstance = new AxtriaSalesIQTM__Team_Instance__c();
        Teaminstance.Name = 'TestR';
        Teaminstance.AxtriaSalesIQTM__Team__c = Team.id;
        Teaminstance.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        Teaminstance.AxtriaSalesIQTM__Scenario__c = Scenario.id;
        insert Teaminstance;
     
   AxtriaSalesIQTM__Position__c Position = New AxtriaSalesIQTM__Position__c();
       Position.Name = 'TestRe';
       Position.AxtriaSalesIQTM__Team_iD__c = Team.id;
       Position.AxtriaSalesIQTM__Hierarchy_Level__c = '1';
       Position.AxtriaSalesIQTM__Team_Instance__c = Teaminstance.id;
       insert Position;
       
    AxtriaSalesIQTM__Position__c Position1 = New AxtriaSalesIQTM__Position__c();
       Position1.id = Position.id;
       Position1.Name = 'TestRe';
       Position1.AxtriaSalesIQTM__Team_iD__c = Team.id;
       Position1.AxtriaSalesIQTM__Hierarchy_Level__c = '1';
       Position1.AxtriaSalesIQTM__Team_Instance__c = Teaminstance.id;
       Position1.AxtriaARSnT__Current_Count__c  = 1234;
       Position1.AxtriaARSnT__One_day_Previous_Count__c  = 1234;
       Update Position1;
     
     Database.executeBatch(new BatchPosAccCopyValueAddedDropped('ABC'));
     }
   }
}