public class RightToBeForgottenAccounts{
    
    public boolean recordsDeleted{get;set;}
    public List<account> accountList{get;set;} //contains all accounts with Right_to_be_forgotten__c = 'True' 
    
    public rightToBeForgottenAccounts(){
        accountList = [select id,name from account where Right_to_be_forgotten__c = 'True'];
        recordsDeleted = false;
    }
    
    public void deleteAllRecords(){
        Delete [SELECT id FROM Call_Plan__c WHERE Account__r.Right_to_be_forgotten__c = 'True']; 
        Delete [SELECT id FROM Account_Compute_Final__c WHERE Physician_2__r.Right_to_be_forgotten__c = 'True']; 
        Delete [SELECT id FROM Account_Compute_Final_Copy__c WHERE Physician_2__r.Right_to_be_forgotten__c = 'True']; 
        Delete [SELECT id FROM AxtriaSalesIQTM__Position_Account_Call_Plan__c WHERE AxtriaSalesIQTM__Account__r.Right_to_be_forgotten__c = 'True']; 
        Delete [SELECT id FROM AxtriaSalesIQTM__CR_Call_Plan__c WHERE AxtriaSalesIQTM__Account__r.Right_to_be_forgotten__c = 'True']; 
        Delete [SELECT id FROM AxtriaSalesIQTM__Account_Affiliation__c WHERE AxtriaSalesIQTM__Account__r.Right_to_be_forgotten__c = 'True' OR AxtriaSalesIQTM__Parent_Account__r.Right_to_be_forgotten__c = 'True' OR AxtriaSalesIQTM__Root_Account__r.Right_to_be_forgotten__c  = 'True']; 
        Delete [SELECT id FROM Affiliation__c WHERE HCO__r.Right_to_be_forgotten__c = 'True' OR HCP__r.Right_to_be_forgotten__c = 'True']; 
        
        Delete [SELECT id FROM AxtriaSalesIQTM__CR_Account__c WHERE AxtriaSalesIQTM__Account__r.Right_to_be_forgotten__c = 'True']; 
        Delete [SELECT id FROM Parent_PACP__c WHERE Account__r.Right_to_be_forgotten__c = 'True']; 
        Delete [SELECT id FROM AxtriaSalesIQTM__Position_Account__c WHERE AxtriaSalesIQTM__Account__r.Right_to_be_forgotten__c = 'True']; 
        Delete [SELECT id FROM Staging_BU_Response__c WHERE Physician__r.Right_to_be_forgotten__c = 'True']; 
        Delete [SELECT id FROM AxtriaSalesIQTM__Team_Instance_Account__c WHERE AxtriaSalesIQTM__Account_ID__r.Right_to_be_forgotten__c = 'True']; 
        Delete [SELECT id FROM Survey_Response__c WHERE Physician__r.Right_to_be_forgotten__c = 'True'];
        Delete [SELECT id FROM Rating_Response__c WHERE Physician__r.Right_to_be_forgotten__c = 'True']; 
        
        Delete [select Id from AxtriaSalesIQTM__Account_Address__c WHERE AxtriaSalesIQTM__Account__r.Right_to_be_forgotten__c = 'True'];       
        Delete [SELECT id FROM BU_Response__c WHERE Physician__r.Right_to_be_forgotten__c = 'True']; 
        Delete [SELECT id FROM AxtriaSalesIQTM__Account_Exclusion__c WHERE AxtriaSalesIQTM__Account__r.Right_to_be_forgotten__c = 'True']; 
        Delete [SELECT id FROM AxtriaSalesIQTM__Position_Account__c WHERE AxtriaSalesIQTM__Account__r.Right_to_be_forgotten__c = 'True'];     
        Delete [SELECT Id FROM AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Account__r.Right_to_be_forgotten__c = 'True'];
        Delete [select Id from Account WHERE Right_to_be_forgotten__c = 'True'];
        recordsDeleted = true;
    }
}