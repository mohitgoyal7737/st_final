global with sharing class changeTSFStatus implements Database.Batchable<sObject>,Schedulable {
    

    List<String> teamInstanceSelected;
    String queryString;
    public Boolean chain = false;
    Set<String> allCountries;
    
    list<string>teaminstancelistis ;

    global changeTSFStatus(List<String> teamInstanceSelectedTemp)
    { 
      teamInstanceSelected = new List<String>(teamInstanceSelectedTemp);
      allCountries = new Set<String>();

      for(AxtriaSalesIQTM__Team_Instance__c ti : [select AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c from AxtriaSalesIQTM__Team_Instance__c where id in :teamInstanceSelected])
      {
            allCountries.add(ti.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c);
      }

       queryString = 'select id, Status__c from SIQ_TSF_vod_O__c where Country__c in :allCountries';

        
    }

    global changeTSFStatus(List<String> teamInstanceSelectedTemp, Boolean chaining)
    { 
      teamInstanceSelected = new List<String>(teamInstanceSelectedTemp);

       allCountries = new Set<String>();

      for(AxtriaSalesIQTM__Team_Instance__c ti : [select AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c from AxtriaSalesIQTM__Team_Instance__c where id in :teamInstanceSelected])
      {
            allCountries.add(ti.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c);
      }

       queryString = 'select id, Status__c from SIQ_TSF_vod_O__c where Country__c in :allCountries';

        
        chain = chaining;
    }


    
    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        return Database.getQueryLocator(queryString);
    }
    
    
    global void execute(Database.BatchableContext BC, List<SIQ_TSF_vod_O__c> scopePacpProRecs)
    {
        for(SIQ_TSF_vod_O__c tsf : scopePacpProRecs)
        {
            tsf.Status__c = '';
        }
        
        //update scopePacpProRecs;
        SnTDMLSecurityUtil.updateRecords(scopePacpProRecs, 'changeTSFStatus');

    }

    global void finish(Database.BatchableContext BC)
    {
        Batch_Integration_TSF_NEW u1 = new Batch_Integration_TSF_NEW(teamInstanceSelected, chain);
        Database.executeBatch(u1, 1000);
    }
    global void execute(System.SchedulableContext SC){
        teaminstancelistis = new list<string>();
        teaminstancelistis = StaticTeaminstanceList.filldataCallPlan();
        database.executeBatch(new changeTSFStatus(teaminstancelistis));
    }
}