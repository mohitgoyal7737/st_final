/*
@author - Prince Richard Augustin (A2661)
@description - Test class to test the Delete_Staging_BU_Response.
*/

@isTest
private class Delete_Staging_BU_Response_Test {
    
    static testMethod void testMethod1() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        
        Account acc = TestDataFactory.createAccount();
        acc.Profile_Consent__c='yes';
        acc.AxtriaSalesIQTM__Speciality__c ='testspecs';
        
        insert acc;
        
        Account acc2 = TestDataFactory.createAccount();
        acc2.Profile_Consent__c='yes';
        acc2.AxtriaSalesIQTM__Speciality__c ='testspecs';
        acc2.name = 'testacc2';
        insert acc2;
        
        AxtriaSalesIQTM__Team_Instance__c teamInstance = TestDataFactory.createTeamInstance(team);
        teamInstance.AxtriaSalesIQTM__Team__c = team.id;
        teamInstance.Name = 'test';
        teamInstance.Segmentation_Universe__c = 'Full S&T Input Customers';
        insert teamInstance ;
        
        
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team,teamInstance,countr);
        pcc.Country_Lookup__c = countr.id;
        //pcc.Veeva_External_ID__c = 'test';
        insert pcc;
       
        
        Measure_Master__c measureMaster= TestDataFactory.createMeasureMaster(pcc,team,teamInstance);
        measureMaster.Team__c = team.id;
        measuremaster.Brand_Lookup__c = pcc.Id;
        measureMaster.Team_Instance__c = teamInstance.id;
        insert measureMaster;
        
        insert  new MetaData_Definition__c (Display_Name__c='ACCESSIBILITY',Source_Field__c='Accessibility_Range__c',Source_Object__c='BU_Response__c',Team_Instance__c=teamInstance.id,Product_Catalog__c=pcc.Id);
        
        AxtriaSalesIQTM__Position__c pos = TestDataFactory.createPosition(team,teamInstance);
        insert pos;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamInstance);
        insert posAccount;
        
        Product_Priority__c pp = new Product_Priority__c();
        pp.CurrencyIsoCode = 'USD';
        pp.Product__c = pcc.id;
        pp.Speciality_ID__c='testSpecs';
        pp.priority__c='P2';
        insert pp;
        BU_Response__c bu = TestDataFactory.createBuResponse(posAccount,pcc,teamInstance,team,acc);
        bu.Team_Instance__c = teamInstance.id;
        bu.Product__c = pcc.id;
        insert bu;
        
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            Delete_Staging_BU_Response obj=new Delete_Staging_BU_Response(teamInstance.Name,'File');
            obj.query = 'select id from Staging_BU_Response__c';
            Database.executeBatch(obj);
            
        }
        Test.stopTest();
    }  
    static testMethod void testMethod4() {
        User loggedInUser = new User(id=UserInfo.getUserId());
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        insert orgmas;
        
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        insert countr;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        insert team;
        
        Account acc = TestDataFactory.createAccount();
        acc.Profile_Consent__c='yes';
        acc.AxtriaSalesIQTM__Speciality__c ='testspecs';
        
        insert acc;
        
        Account acc2 = TestDataFactory.createAccount();
        acc2.Profile_Consent__c='yes';
        acc2.AxtriaSalesIQTM__Speciality__c ='testspecs';
        acc2.name = 'testacc2';
        insert acc2;
        
        AxtriaSalesIQTM__Team_Instance__c teamInstance = TestDataFactory.createTeamInstance(team);
        teamInstance.AxtriaSalesIQTM__Team__c = team.id;
        teamInstance.Name = 'testteamins';
        teamInstance.Segmentation_Universe__c = 'Full S&T Input Customers';
        insert teamInstance ;
        
        
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team,teamInstance,countr);
        pcc.Country_Lookup__c = countr.id;
        //pcc.Veeva_External_ID__c = 'test';
        insert pcc;
       
        Measure_Master__c measureMaster= TestDataFactory.createMeasureMaster(pcc,team,teamInstance);
        measureMaster.Team__c = team.id;
        measuremaster.Brand_Lookup__c = pcc.Id;
        measureMaster.Team_Instance__c = teamInstance.id;
        insert measureMaster;
        
        insert  new MetaData_Definition__c (Display_Name__c='ACCESSIBILITY',Source_Field__c='Accessibility_Range__c',Source_Object__c='BU_Response__c',Team_Instance__c=teamInstance.id,Product_Catalog__c=pcc.Id);
        
        AxtriaSalesIQTM__Position__c pos = TestDataFactory.createPosition(team,teamInstance);
        insert pos;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamInstance);
        insert posAccount;
        
        Product_Priority__c pp = new Product_Priority__c();
        pp.CurrencyIsoCode = 'USD';
        pp.Product__c = pcc.id;
        pp.Speciality_ID__c='testSpecs';
        pp.priority__c='P2';
        insert pp;
        BU_Response__c bu = TestDataFactory.createBuResponse(posAccount,pcc,teamInstance,team,acc);
        bu.Team_Instance__c = teamInstance.id;
        bu.Product__c = pcc.id;
        //bu.Brand_c = pcc.id;
        
        insert bu;
        Staging_BU_Response__c sbu = TestDataFactory.createStagingBuResponse(posAccount,pcc,teamInstance,team,acc);
        
        insert sbu;
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            Delete_Staging_BU_Response obj=new Delete_Staging_BU_Response();
            obj.query = 'select id from Staging_BU_Response__c ';
            Database.executeBatch(obj);
        }
        Test.stopTest();
    }  
    /*static testMethod void testMethod3() {
User loggedInUser = new User(id=UserInfo.getUserId());
AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
insert orgmas;

AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
insert countr;

AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
insert team;

Account acc = TestDataFactory.createAccount();
acc.Profile_Consent__c='yes';
acc.AxtriaSalesIQTM__Speciality__c ='testspecs';

insert acc;

Account acc2 = TestDataFactory.createAccount();
acc2.Profile_Consent__c='yes';
acc2.AxtriaSalesIQTM__Speciality__c ='testspecs';
acc2.name = 'testacc2';
insert acc2;

AxtriaSalesIQTM__Team_Instance__c teamInstance = TestDataFactory.createTeamInstance(team);
teamInstance.AxtriaSalesIQTM__Team__c = team.id;
insert teamInstance ;

AxtriaSalesIQTM__Team_Instance__c teamInstance1 = TestDataFactory.createTeamInstance(team);
teamInstance1.AxtriaSalesIQTM__Team__c = team.id;
insert teamInstance1 ;


Product_Catalog__c pcc = TestDataFactory.productCatalog(team,teamInstance,countr);
pcc.Country_Lookup__c = countr.id;
pcc.team_instance__c = teaminstance.id;
insert pcc;

Measure_Master__c measureMaster= TestDataFactory.createMeasureMaster(pcc,team,teamInstance);
measureMaster.Team__c = team.id;
measuremaster.Brand_Lookup__c = pcc.Id;
measureMaster.Team_Instance__c = teamInstance.id;
insert measureMaster;

insert  new MetaData_Definition__c (Display_Name__c='ACCESSIBILITY',Source_Field__c='Accessibility_Range__c',Source_Object__c='BU_Response__c',Team_Instance__c=teamInstance.id,Product_Catalog__c=pcc.Id);

AxtriaSalesIQTM__Position__c pos = TestDataFactory.createPosition(team,teamInstance);
insert pos;

AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactory.createPositionAccount(acc,pos,teamInstance);
insert posAccount;

Product_Priority__c pp = new Product_Priority__c();
pp.CurrencyIsoCode = 'USD';
pp.Product__c = pcc.id;
pp.Speciality_ID__c='testSpecs';
pp.priority__c='P2';
insert pp;
Staging_BU_Response__c bu = TestDataFactory.createStagingBuResponse(posAccount,pcc,teamInstance,team,acc);
bu.Physician__c = acc.id;
bu.Product_Catalog__c = pcc.id;
bu.Response1__c = '4';
insert bu;        
Test.startTest();
System.runAs(loggedInUser){
ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
Delete_Staging_BU_Response obj=new Delete_Staging_BU_Response(teamInstance.Name+';'+pcc.Veeva_External_ID__c);
obj.query = 'select id from Staging_BU_Response__c ';
Database.executeBatch(obj);
}
Test.stopTest();
}  */
}