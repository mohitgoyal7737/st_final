global class Delete_Survey_Information implements Database.Batchable<sObject>
{
    public String query;
    public String teamInstance;
    public String type;
    public Set<string> teaminstancelist;
    public boolean schedulejob=false;
    public list<String> teamInst_ProdList; //Added by dhiren
    public String teamInstance_Prod;//Added by dhiren
    public String product;//Added by dhiren
    public boolean ondemand=false;//Added by dhiren

    global Delete_Survey_Information(String teamInstance,String type)
    {   
        this.type=type;
        this.teamInstance = teamInstance;
         schedulejob=false;
        query='select id from Survey_Definition__c where Team_Instance__r.Name = \''+teamInstance+'\'';
    }
     global Delete_Survey_Information()
    {
        schedulejob=true;
        teaminstancelist= new Set<String>();
        teaminstancelist= StaticTeaminstanceList.getCompleteRuleTeamInstances();
        query='select id from Survey_Definition__c where Team_Instance__c not in:teaminstancelist';
    }

            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Added  by dhiren<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    global Delete_Survey_Information(String teamInstanceProd)
    {
        system.debug('++Inside whole survey response');

        teamInstance_Prod = teamInstanceProd;
        System.debug('<><><><><><><><>'+teamInstance_Prod);
        teamInst_ProdList=new list<String>();
        if(teamInstance_Prod.contains(';'))
        {
            teamInst_ProdList=teamInstance_Prod.split(';');
        }
        
        system.debug('<><>teamInst_ProdList<><<>'+teamInst_ProdList);
        teamInstance = teamInst_ProdList[0];
        product = teamInst_ProdList[1];
        scheduleJob=false;
        ondemand = true;

        system.debug('<>>>>>>><>teamInstance<><><><><><><> '+teamInstance+'  <>>>>>>>>product>>>>>>>>>> '+product);

        query='select id from Survey_Definition__c where Team_Instance__r.name =: teamInstance and Brand_ID__c =: product ';

    }
    //<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>End of code by dhiren<><><>>>>>>><><><><><><><><><><><>><><><<

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        System.debug('query-->'+query);
        return Database.getQueryLocator(query);
    }
   

    global void execute(Database.BatchableContext BC, List<Survey_Definition__c> scope)
    {
        System.debug('scope size-->'+scope.size());
        if(scope!=null && scope.size()>0)
        {
            delete scope;
            //Database.emptyRecycleBin(scope);  
        }
    }

    global void finish(Database.BatchableContext BC)
    {

        if(schedulejob)
        {
            Delete_Staging_BU_Response batch = new Delete_Staging_BU_Response();
            Database.executeBatch(batch,500);
        }
        else
        {
            if(ondemand)
            {
                Delete_Staging_BU_Response batch = new Delete_Staging_BU_Response(teamInstance_Prod);
                Database.executeBatch(batch,500);
            }

            else
            {
                Delete_Staging_BU_Response batch = new Delete_Staging_BU_Response(teamInstance,type);
                Database.executeBatch(batch,500);
            }
            
        }
    }
}