public class OutboundPositionProcessing {
    public String query;
    public Integer recordsProcessed=0;
    public String batchID;
    
    public map<String,String>Countrymap {get;set;}
    public set<String> Uniqueset {get;set;}                                
    public List<AxtriaSalesIQTM__Position__c> nationPosition {get;set;}
    public List<AxtriaSalesIQTM__Organization_Master__c> orgList {get;set;}
    public map<string,String> orgmap {get;set;}
    public map<string,AxtriaSalesIQTM__Organization_Master__c> organization {get;set;}
    public map<string,string>toporgmstr {get;set;}


    public OutboundPositionProcessing (List<AxtriaSalesIQTM__Position__c> position,Integer records,String batchID){
    nationPosition = new List<AxtriaSalesIQTM__Position__c>(position);
    organization =new map<String,AxtriaSalesIQTM__Organization_Master__c>();
    orgmap=new map<String,String>();
    toporgmstr = new map<string,string>();
        system.debug(nationPosition);
        orgList = new List<AxtriaSalesIQTM__Organization_Master__c>();
        orgList=[select id,Name,AxtriaSalesIQTM__Parent_Organization_Name__r.Name from AxtriaSalesIQTM__Organization_Master__c ];
        for(AxtriaSalesIQTM__Organization_Master__c org:orgList){
            orgmap.put(org.id,org.AxtriaSalesIQTM__Parent_Organization_Name__c);
            organization.put(org.id,org);
            if(org.AxtriaSalesIQTM__Parent_Organization_Name__c==null){
                toporgmstr.put(org.id,'TOP NODE');
            }
        }
        if(nationPosition.size()>0){
       positionOrganization(nationPosition,records,batchID); 
        }
    }
    public void positionOrganization(List<AxtriaSalesIQTM__Position__c> position,Integer records,String batchID){
    List<SIQ_Position_O__c> orgPosList=new List<SIQ_Position_O__c>();
    List<String> orgList=new List<String>();
    set<string>uniqsetinsert = new set<string>();

      for(AxtriaSalesIQTM__Position__c pos:position)
      {
        System.debug('***'+pos.AxtriaSalesIQTM__Position_Type__c);
        system.debug('============POSITION ID IS:'+pos);
        
        String org=pos.AxtriaSalesIQTM__Team_iD__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__c;
        System.debug('++org'+org);
        if(org!=null)
        {
            SIQ_Position_O__c obj=new SIQ_Position_O__c();
            string childn='';
            string parentn='';
            string key='';
            AxtriaSalesIQTM__Organization_Master__c orgObj=organization .get(org);

            childn=orgObj.Name;
            if(orgObj.AxtriaSalesIQTM__Parent_Organization_Name__r.Name!=null)
            {
                parentn=orgObj.AxtriaSalesIQTM__Parent_Organization_Name__r.Name;
                key=childn+'_'+parentn+'_'+pos.AxtriaSalesIQTM__Team_Instance__r.Name;
            }
            else
            {
                key=childn+'_'+pos.AxtriaSalesIQTM__Team_Instance__r.Name;
            }

            
            system.debug('=====11111 KEy:::'+key);
            system.debug('=====11111uniqsetinsert:::'+uniqsetinsert);
            if(!uniqsetinsert.contains(key))
            {
                system.debug('---->Key::::'+key+':::Does not exist');
                obj.SIQ_Marketing_Code__c=pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.Marketing_Code__c;
                obj.SIQ_Country_Code__c=pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                obj.SIQ_Salesforce_Name__c=pos.AxtriaSalesIQTM__Team_iD__r.Name;
                //obj.SIQ_POSITION_CODE__c=pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.Marketing_Code__c+'_'+pos.AxtriaSalesIQTM__Team_Instance__r.Team_Instance_Category__c+'_admin';
                obj.SIQ_POSITION_CODE__c=orgObj.Name;
                obj.SIQ_POSITION_NAME__c=orgObj.Name; 
                if(orgObj.AxtriaSalesIQTM__Parent_Organization_Name__r.Name!=null)
                {
                    obj.SIQ_PARENT_POSITION_NAME__c=orgObj.AxtriaSalesIQTM__Parent_Organization_Name__r.Name;
                    obj.SIQ_PARENT_POSITION_CODE__c=orgObj.AxtriaSalesIQTM__Parent_Organization_Name__r.Name;
                }

                obj.SIQ_Partner_Flag__c='N';
                obj.SIQ_Channel__c=pos.Channel_AZ__c;  
                obj.SIQ_Sales_Team_Attribute__c=pos.AxtriaARSnT__Sales_Team_Attribute_MS__c;
                //obj.SIQ_Position_Level__c=pos.AxtriaSalesIQTM__Position_Type__c;
                obj.SIQ_Position_Level__c='ADMIN';
                obj.SIQ_Event__c='Insert';
                obj.OrgMstr__c = true;
                obj.SIQ_POSITION_ROLE__c=pos.AxtriaSalesIQTM__Role__c;
                obj.SIQ_SALES_TEAM_CODE__c=pos.AxtriaSalesIQTM__Team_iD__r.Name;
                obj.SIQ_Team_Instance__c=pos.AxtriaSalesIQTM__Team_Instance__r.Name;
                obj.SIQ_Updated_Date__c=pos.LastModifiedDate;
                obj.SIQ_Channel_ID_AZ__c=pos.Channel_AZ__c;
                obj.SIQ_Created_Date__c=pos.CreatedDate;
                obj.SIQ_Effective_Start_Date__c=pos.AxtriaSalesIQTM__Effective_Start_Date__c;
                obj.SIQ_Effective_End_Date__c=pos.AxtriaSalesIQTM__Effective_End_Date__c;
                obj.Unique_Id__c=key;
                 uniqsetinsert.add(key);
                orgPosList.add(obj);
                records++;

            }

            org=orgmap.get(org);
            // TWO LEVEL HIERARCHY 
            if(orgmap.get(org) ==null && toporgmstr.containskey(org) && toporgmstr.get(org)=='TOP NODE' )
            {
                system.debug('================TWO LEVEL HIERARCHYYYYYYY:');
                obj=new SIQ_Position_O__c();
                orgObj=organization.get(org);
                key=orgObj.Name+'_'+pos.AxtriaSalesIQTM__Team_Instance__r.Name;
                system.debug('========2222222222 Key:::::'+key);
                system.debug('========2222222222 uniqsetinsert:::'+uniqsetinsert);
                
                if(!uniqsetinsert.contains(key))
                {
                    obj.SIQ_Marketing_Code__c=pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.Marketing_Code__c;
                    obj.SIQ_Country_Code__c=pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                    obj.SIQ_Salesforce_Name__c=pos.AxtriaSalesIQTM__Team_iD__r.Name;
                    obj.SIQ_POSITION_CODE__c=orgObj.Name;
                    obj.SIQ_POSITION_NAME__c=orgObj.Name;                                                  
                    //obj.SIQ_PARENT_POSITION_NAME__c=orgObj.AxtriaSalesIQTM__Parent_Organization_Name__r.Name;
                    //obj.SIQ_PARENT_POSITION_CODE__c=orgObj.AxtriaSalesIQTM__Parent_Organization_Name__r.Name;
                    obj.SIQ_Partner_Flag__c='N';
                    obj.SIQ_Channel__c=pos.Channel_AZ__c;  
                    obj.SIQ_Sales_Team_Attribute__c=pos.AxtriaARSnT__Sales_Team_Attribute_MS__c;
                    //obj.SIQ_Position_Level__c=pos.AxtriaSalesIQTM__Position_Type__c;
                    obj.SIQ_Position_Level__c='ADMIN';
                    obj.SIQ_Event__c='Insert';
                    obj.OrgMstr__c = true;
                    obj.SIQ_POSITION_ROLE__c=pos.AxtriaSalesIQTM__Role__c;
                    obj.SIQ_SALES_TEAM_CODE__c=pos.AxtriaSalesIQTM__Team_iD__r.Name;
                    obj.SIQ_Team_Instance__c=pos.AxtriaSalesIQTM__Team_Instance__r.Name;
                    obj.SIQ_Updated_Date__c=pos.LastModifiedDate;
                    obj.SIQ_Channel_ID_AZ__c=pos.Channel_AZ__c;
                    obj.SIQ_Created_Date__c=pos.CreatedDate;
                    obj.SIQ_Effective_Start_Date__c=pos.AxtriaSalesIQTM__Effective_Start_Date__c;
                    obj.SIQ_Effective_End_Date__c=pos.AxtriaSalesIQTM__Effective_End_Date__c;
                    obj.Unique_Id__c=key;
                    uniqsetinsert.add(key);
                    orgPosList.add(obj);
                    records++;
                }

            }
            while(orgmap.get(org)!=null){
           //  orgList.add(orgmap.get(org));
                obj=new SIQ_Position_O__c();
                orgObj=organization .get(org);
                key=orgObj.Name+'_'+orgObj.AxtriaSalesIQTM__Parent_Organization_Name__r.Name+'_'+pos.AxtriaSalesIQTM__Team_Instance__r.Name;
                system.debug('========3333333 Key::::'+key);
                system.debug('========3333333 uniqsetinsert:::'+uniqsetinsert);
                if(!uniqsetinsert.contains(key))
                {
                    obj.SIQ_Marketing_Code__c=pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.Marketing_Code__c;
                    obj.SIQ_Country_Code__c=pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                    obj.SIQ_Salesforce_Name__c=pos.AxtriaSalesIQTM__Team_iD__r.Name;
                    obj.SIQ_POSITION_CODE__c=orgObj.Name;
                    obj.SIQ_POSITION_NAME__c=orgObj.Name;                                                  
                    obj.SIQ_PARENT_POSITION_NAME__c=orgObj.AxtriaSalesIQTM__Parent_Organization_Name__r.Name;
                    obj.SIQ_PARENT_POSITION_CODE__c=orgObj.AxtriaSalesIQTM__Parent_Organization_Name__r.Name;
                    obj.SIQ_Partner_Flag__c='N';
                    obj.SIQ_Channel__c=pos.Channel_AZ__c;  
                    obj.SIQ_Sales_Team_Attribute__c=pos.AxtriaARSnT__Sales_Team_Attribute_MS__c;
                    //obj.SIQ_Position_Level__c=pos.AxtriaSalesIQTM__Position_Type__c;
                    obj.SIQ_Position_Level__c='ADMIN';
                    obj.SIQ_Event__c='Insert';
                    obj.OrgMstr__c = true;
                    obj.SIQ_POSITION_ROLE__c=pos.AxtriaSalesIQTM__Role__c;
                    obj.SIQ_SALES_TEAM_CODE__c=pos.AxtriaSalesIQTM__Team_iD__r.Name;
                    obj.SIQ_Team_Instance__c=pos.AxtriaSalesIQTM__Team_Instance__r.Name;
                    obj.SIQ_Updated_Date__c=pos.LastModifiedDate;
                    obj.SIQ_Channel_ID_AZ__c=pos.Channel_AZ__c;
                    obj.SIQ_Created_Date__c=pos.CreatedDate;
                    obj.SIQ_Effective_Start_Date__c=pos.AxtriaSalesIQTM__Effective_Start_Date__c;
                    obj.SIQ_Effective_End_Date__c=pos.AxtriaSalesIQTM__Effective_End_Date__c;
                    obj.Unique_Id__c=key;
                    uniqsetinsert.add(key);
                    orgPosList.add(obj);
                    records++;
                
                }
                
                System.debug('**records'+records);
                org=orgmap.get(org);
                if( toporgmstr.containskey(org) && toporgmstr.get(org)=='TOP NODE'){
                    system.debug('==============TOP NODE IS:::::::'+org);
                    obj=new SIQ_Position_O__c();
                    orgObj=organization.get(org);
                    key=orgObj.Name+'_'+pos.AxtriaSalesIQTM__Team_Instance__r.Name;
                     system.debug('=====44444 Key::::'+key);
                     system.debug('======4444 uniqsetinsert:::'+uniqsetinsert);
                    if(!uniqsetinsert.contains(key)){
                        obj.SIQ_Marketing_Code__c=pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Parent_Organization__r.Marketing_Code__c;
                        obj.SIQ_Country_Code__c=pos.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c;
                        obj.SIQ_Salesforce_Name__c=pos.AxtriaSalesIQTM__Team_iD__r.Name;
                        obj.SIQ_POSITION_CODE__c=orgObj.Name;
                        obj.SIQ_POSITION_NAME__c=orgObj.Name;                                                  
                        //obj.SIQ_PARENT_POSITION_NAME__c=orgObj.AxtriaSalesIQTM__Parent_Organization_Name__r.Name;
                        //obj.SIQ_PARENT_POSITION_CODE__c=orgObj.AxtriaSalesIQTM__Parent_Organization_Name__r.Name;
                        obj.SIQ_Partner_Flag__c='N';
                        obj.SIQ_Channel__c=pos.Channel_AZ__c;  
                        obj.SIQ_Sales_Team_Attribute__c=pos.AxtriaARSnT__Sales_Team_Attribute_MS__c;
                        obj.SIQ_Event__c='Insert';
                        obj.OrgMstr__c = true;
                        //obj.SIQ_Position_Level__c=pos.AxtriaSalesIQTM__Position_Type__c;
                        obj.SIQ_Position_Level__c='ADMIN';
                        obj.SIQ_POSITION_ROLE__c=pos.AxtriaSalesIQTM__Role__c;
                        obj.SIQ_SALES_TEAM_CODE__c=pos.AxtriaSalesIQTM__Team_iD__r.Name;
                        obj.SIQ_Team_Instance__c=pos.AxtriaSalesIQTM__Team_Instance__r.Name;
                        obj.SIQ_Updated_Date__c=pos.LastModifiedDate;
                        obj.SIQ_Channel_ID_AZ__c=pos.Channel_AZ__c;
                        obj.SIQ_Created_Date__c=pos.CreatedDate;
                        obj.SIQ_Effective_Start_Date__c=pos.AxtriaSalesIQTM__Effective_Start_Date__c;
                        obj.SIQ_Effective_End_Date__c=pos.AxtriaSalesIQTM__Effective_End_Date__c;
                        obj.Unique_Id__c=key;
                        uniqsetinsert.add(key);
                        orgPosList.add(obj);
                        records++;
                    }
                }
            } 
         
        }
        }
        if(orgPosList.size()>0){
        upsert orgPosList Unique_Id__c;
        }
        if(batchID!=null){
        Scheduler_Log__c sJob = new Scheduler_Log__c(id = batchID); 
        system.debug('schedulerObj++++before'+sJob);
        sJob.Job_Status__c='Successful';
        sJob.No_Of_Records_Processed__c=records;
        update sJob;
        }
    }
}