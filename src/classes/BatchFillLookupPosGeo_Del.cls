global with sharing class BatchFillLookupPosGeo_Del implements Database.Batchable<sObject>, Database.Stateful  {
     String query;
     String CountryName;
     String key;
     Map<String,String> KeyIdMap;
     Set<String> processedrecordSet;

    /*Replaced temp_Zip_Terr__c with temp_Obj__c due to object purge*/
     global BatchFillLookupPosGeo_Del(String Country){
          CountryName= Country;
          query = 'Select Id,Country__c,Event__c,Team_Instance_Name__c,Team_Name__c,Status__c,Territory_ID__c,Zip_Name__c from temp_Obj__c where Country__c =: CountryName and Event__c = \'Delete\' and Status__c = \'New\' and Object__c = \'Zip_Terr\' WITH SECURITY_ENFORCED';
     }
    
     global Database.Querylocator start(Database.BatchableContext bc){
          return Database.getQueryLocator(query);
        
     } 
    
     global void execute (Database.BatchableContext BC, List<temp_Obj__c>scope){

          SnTDMLSecurityUtil.printDebugMessage('Execute method');

          KeyIdMap = new Map<String,String>();
          processedrecordSet = new Set<String>();
          List<AxtriaSalesIQTM__Position_Geography__c> posGeoList = new  List<AxtriaSalesIQTM__Position_Geography__c>();
          List<AxtriaSalesIQTM__Position_Geography__c> futurePosGeoList = new  List<AxtriaSalesIQTM__Position_Geography__c>();


          for(temp_Obj__c posgeo : scope)
          {
               
               String key = posgeo.Territory_ID__c.toUpperCase()+'_'+posgeo.Zip_Name__c.toUpperCase()+'_'+posgeo.Team_Instance_Name__c.toUpperCase();
               KeyIdMap.put(key,posgeo.Id);
               SnTDMLSecurityUtil.printDebugMessage('inside first loop :::::'+KeyIdMap);
          }

          for(AxtriaSalesIQTM__Position_Geography__c posGeoCore : [Select Id,Unique_key__c,AxtriaSalesIQTM__Assignment_Status__c,AxtriaSalesIQTM__Geography__r.Name,AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Effective_End_Date__c from AxtriaSalesIQTM__Position_Geography__c where Unique_key__c In: KeyIdMap.keySet() and AxtriaSalesIQTM__Assignment_Status__c != 'Inactive' WITH SECURITY_ENFORCED])
          {
               processedrecordSet.add(posGeoCore.Unique_key__c);

               if(posGeoCore.AxtriaSalesIQTM__Assignment_Status__c == 'Active'){

                    posGeoCore.AxtriaSalesIQTM__Effective_End_Date__c =System.Today();
                    posGeoList.add(posGeoCore);

                    SnTDMLSecurityUtil.printDebugMessage('inside second loop :::::'+posGeoCore.AxtriaSalesIQTM__Effective_End_Date__c );
               }

               else if (posGeoCore.AxtriaSalesIQTM__Assignment_Status__c == 'Future Active')
               {
                    SnTDMLSecurityUtil.printDebugMessage('inside delete loop :::::'+posGeoCore);

                    futurePosGeoList.add(posGeoCore);
               }

          }

          if(!posGeoList.isEmpty()){
               //update posGeoList ;
               SnTDMLSecurityUtil.updateRecords(posGeoList, 'BatchFillLookupPosGeo_Del');
          }

          if(!futurePosGeoList.isEmpty()){
               //delete futurePosGeoList ; 
               SnTDMLSecurityUtil.deleteRecords(futurePosGeoList, 'BatchFillLookupPosGeo_Del');
          } 

          for(temp_Obj__c posgeo : scope)
          {
               String key = posgeo.Territory_ID__c.toUpperCase()+'_'+posgeo.Zip_Name__c.toUpperCase()+'_'+posgeo.Team_Instance_Name__c.toUpperCase();
               if(processedrecordSet.Contains(key))
               {
                    SnTDMLSecurityUtil.printDebugMessage('inside processed loop :::::' );

                    posgeo.Status__c = 'Processed';
               }
               else
               {
                    SnTDMLSecurityUtil.printDebugMessage('inside Rejected loop :::::' );
                    posgeo.Status__c = 'Rejected';
               }
          }

          //update scope;  
          SnTDMLSecurityUtil.updateRecords(scope, 'BatchFillLookupPosGeo_Del');   
          
     }
    
     global void finish(Database.BatchableContext BC){
     }
}