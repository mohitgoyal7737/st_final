global class stagingATLCopyTerritory implements Database.Batchable<sObject> 
{
    public String query;
    List<Staging_ATL__c> stagList;
    public List<String> allCountries;

    global stagingATLCopyTerritory() 
    {
        
        this.query = 'select id, Account__c, External_ID_AZ__c, Axtria_Account_ID__c,Country__c, Territory__c,Territory_Old__c from Staging_ATL__c where  Status__c = \'Updated\'';
    }

    global stagingATLCopyTerritory(List<String> allCountries1) 
    {
        allCountries = new List<String>(allCountries1);
        system.debug('+++allCountries'+allCountries);
        this.query = 'select id, Account__c, External_ID_AZ__c, Axtria_Account_ID__c,Country__c, Territory__c,Territory_Old__c from Staging_ATL__c where Country_Lookup__c in :allCountries';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Staging_ATL__c> scope) 
    {
        stagList= new List<Staging_ATL__c>();
        
        for(Staging_ATL__c stag: scope)
        {
            system.debug('stag'+stag.Territory__c);
            
            if(stag.Territory_Old__c!= null)
            {
                system.debug('inside if');
                stag.Territory_Old__c=null;
            }

            stag.Territory_Old__c=  stag.Territory__c;
            system.debug('stag'+stag.Territory_Old__c);
            //stagList.add(stag);    
        }
        //system.debug('++stagList'+stagList);
        update scope;
        //update stagList;

    }

    global void finish(Database.BatchableContext BC) 
    {
        Database.executeBatch(new ATL_TransformationBatch(allCountries, true),2000);
    }
}