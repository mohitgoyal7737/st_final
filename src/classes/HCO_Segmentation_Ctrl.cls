/*
@author - Shivansh Garg (A1450)
@description - Controller to show the HCO Parameters and store the changes being done to these parameters 
                for a source rule and destination rule.
*/
public with sharing class HCO_Segmentation_Ctrl {

    public String selectedCycle {get;set;}
        public transient List<SLDSPageMessage> PageMessages{get;set;}
    public String selectedBusinessUnit {get;set;}
    public String selectedSourceRule {get;set;}
    public String selectedDestinationRule {get;set;}
    public String selectedParameter {get;set;}

    public List<SelectOption> businessUnits {get;set;}
    public List<SelectOption> sourceRules {get;set;}
    public List<SelectOption> destinationRules {get;set;}
    public List<SelectOption> cycles {get;set;}

    public Map<String,String> parameterMap {get;set;}
    public Boolean paramFlag {get;set;}

    public String countryID {get;set;}
    public String parameterMapString {get;set;}
    public String parameterString {get;set;}

    Map<string,string> successErrorMap;
    String teamId;
    String teamInstanceId;

    Integer numberOfParams;

    public HCO_Segmentation_Ctrl() {
       // countryID = SalesIQUtility.getCookie(SalesIQUtility.getCountryCookieName());
       countryID = MCCP_Utility.getKeyValueFromPlatformCache('SIQCountryID');
        SnTDMLSecurityUtil.printDebugMessage('##### countryID ' + countryID);
        successErrorMap = SalesIQUtility.checkCountryAccess(countryID);
        SnTDMLSecurityUtil.printDebugMessage('############ successErrorMap ' + successErrorMap);
        if(successErrorMap.containsKey('Success')){
           countryID = successErrorMap.get('Success');               
           SnTDMLSecurityUtil.printDebugMessage('########## countryID from Map ' + countryID);
           //As soon as we get Country ID set it in the cookie and it will applicable for whole application.
           SalesIQUtility.setCookieString('CountryID',countryID);
        }

        parameterMap = new Map<String,String>();
        parameterMapString = '';
        fillCycleOptions();
        paramFlag = false;
        numberOfParams = 0;
    }

    public void fillCycleOptions(){
        cycles = new list<SelectOption>();
        cycles.add(new SelectOption('None', '--None--'));
        List<AxtriaSalesIQTM__Workspace__c> allCycles ;
        try{
            allCycles = [SELECT Id, Name FROM AxtriaSalesIQTM__Workspace__c where AxtriaSalesIQTM__Country__c = :countryID WITH SECURITY_ENFORCED];
        }
        catch(System.QueryException qe) 
        {
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }
        if(allCycles != null && allCycles.size() > 0)
        {
            //selectedCycle = allCycles[0].ID;
            for(AxtriaSalesIQTM__Workspace__c cycle: allCycles){
                cycles.add(new SelectOption(cycle.Id, cycle.Name));
            }            
        }
    }

    public void fillBusinessUnitOptions(){
        businessUnits = new list<SelectOption>();
        businessUnits.add(new SelectOption('None', '--None--'));
        List<AxtriaSalesIQTM__Team_Instance__c> butemp ;
        try{
            butemp = [SELECT Id, Name FROM AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__Country__c = :countryID and AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c= :selectedCycle WITH SECURITY_ENFORCED];
        }
        catch(System.QueryException qe) 
        {
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }
        for(AxtriaSalesIQTM__Team_Instance__c bu: butemp){
            businessUnits.add(new SelectOption(bu.Id, bu.Name));
        }

        selectedBusinessUnit = 'None';
        selectedSourceRule = 'None';
        selectedDestinationRule = 'None';
        
    }

    public void fillSourceRuleOptions(){
        sourceRules = new List<SelectOption>();
        sourceRules.add(new SelectOption('None', '--None--'));
        List<Measure_Master__c> mmtemp ;
        try{
            mmtemp = [select Id,Name from Measure_Master__c where Team_Instance__c =:selectedBusinessUnit and Measure_Type__c != 'HCO' WITH SECURITY_ENFORCED];
        }
        catch(System.QueryException qe) 
        {
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }
        for(Measure_Master__c mm: mmtemp)
        {
            sourceRules.add(new SelectOption(mm.Id, mm.Name));
        }

        selectedSourceRule = 'None';
        selectedDestinationRule = 'None';
    }

    public void fillDestinationRuleOptions(){
        destinationRules = new List<SelectOption>();
        destinationRules.add(new SelectOption('None', '--None--'));
        Measure_Master__c sourceMM ;
        try{
            sourceMM = [Select Id,Brand__c,Team__c,Team_Instance__c from Measure_Master__c where Id =: selectedSourceRule WITH SECURITY_ENFORCED];
        }
        catch(System.QueryException qe) 
        {
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }
        teamId = sourceMM.Team__c;
        teamInstanceId = sourceMM.Team_Instance__c;
        List<Measure_Master__c> mmtemp ;
        try{
            mmtemp = [select Id,Name from Measure_Master__c where Team_Instance__c =:selectedBusinessUnit and Measure_Type__c = 'HCO' and Brand__c =: sourceMM.Brand__c WITH SECURITY_ENFORCED];
        }
        catch(System.QueryException qe) 
        {
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }
        for(Measure_Master__c mm: mmtemp)
        {
            destinationRules.add(new SelectOption(mm.Id, mm.Name));
        }
        List<Rule_Parameter__c> paramtemp ;
        try{
            paramtemp = [SELECT Id, Name, Parameter_Name__c FROM Rule_Parameter__c WHERE Measure_Master__c =:selectedSourceRule  WITH SECURITY_ENFORCED ORDER BY Name];
        }
        catch(System.QueryException qe) 
        {
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }
        for(Rule_Parameter__c param : paramtemp)
        {
            parameterMap.put(param.Parameter_Name__c,param.Id);
        }

        SnTDMLSecurityUtil.printDebugMessage('parameterMap --> ' + parameterMap);
        parameterMapString = JSON.serialize(parameterMap);
        SnTDMLSecurityUtil.printDebugMessage('parameterMapString --> ' + parameterMapString);

        selectedDestinationRule = 'None';

    }

    public void loadParameterTable(){
        numberOfParams = 0;
        List<ParameterWrapper> paramWrapList = new List<ParameterWrapper>();
        List<HCO_Parameters__c> hcoPartemp ;
        try{
            hcoPartemp = [Select Id,Display_Name__c,Sequence__c,Source_Rule_Parameter__c,Source_Rule_Parameter__r.Parameter_Name__c,Affiliation_Level__c,Last_Published_Date__c from HCO_Parameters__c where Source_Rule__c =:selectedSourceRule and Destination_Rule__c =: selectedDestinationRule WITH SECURITY_ENFORCED ORDER BY Sequence__c];
        }
        catch(System.QueryException qe) 
        {
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }
        for(HCO_Parameters__c hcoPar : hcoPartemp){
            paramWrapList.add(new ParameterWrapper(hcoPar.Sequence__c,hcoPar.Source_Rule_Parameter__r.Parameter_Name__c,hcoPar.Display_Name__c,hcoPar.Source_Rule_Parameter__c,hcoPar.Affiliation_Level__c,hcoPar.Last_Published_Date__c));
            numberOfParams++;
        }
        SnTDMLSecurityUtil.printDebugMessage('paramWrapList --> ' + paramWrapList);
        parameterString = JSON.serialize(paramWrapList);
        SnTDMLSecurityUtil.printDebugMessage('parameterString on load --> ' + parameterString);
    }

    public void saveParameters(){
        SnTDMLSecurityUtil.printDebugMessage('parameterString --> ' + parameterString);
        List<ParameterWrapper> paramWrapList = new List<ParameterWrapper>();
        paramWrapList = (List<ParameterWrapper>) JSON.deserialize(parameterString,List<ParameterWrapper>.Class);
        SnTDMLSecurityUtil.printDebugMessage('paramWrapList --> ' + paramWrapList);

        List<HCO_Parameters__c> hcoParamUpsertList = new List<HCO_Parameters__c>();
        List<HCO_Parameters__c> hcoParamDeleteList = new List<HCO_Parameters__c>();
        Map<String,HCO_Parameters__c> hcoParamAlreadyMap = new Map<String,HCO_Parameters__c>();
        Set<String> hcoParamDisplayNamesNow = new Set<String>();
        HCO_Parameters__c hcoParam;
        List<HCO_Parameters__c> hcoPartemp ;
        try{
            hcoPartemp = [Select Id,Display_Name__c from HCO_Parameters__c where Source_Rule__c =:selectedSourceRule and Destination_Rule__c =: selectedDestinationRule WITH SECURITY_ENFORCED ];
        }
        catch(System.QueryException qe) 
        {
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }
        for(HCO_Parameters__c hcoParamTemp : hcoPartemp){
            hcoParamAlreadyMap.put(hcoParamTemp.Display_Name__c,hcoParamTemp);
        }

        for(ParameterWrapper paramWrap : paramWrapList){
            hcoParam = new HCO_Parameters__c();
            hcoParam.Display_Name__c = paramWrap.Value;
            hcoParam.Sequence__c = paramWrap.Sequence;
            hcoParam.Source_Rule__c = selectedSourceRule;
            hcoParam.Destination_Rule__c = selectedDestinationRule;
            hcoParam.Source_Rule_Parameter__c = paramWrap.API_Name;
            hcoParam.Affiliation_Level__c = paramWrap.Level_Selected;
            hcoParam.External_ID__c = selectedSourceRule + '_' + selectedDestinationRule + '_' + hcoParam.Display_Name__c;
            hcoParamUpsertList.add(hcoParam);

            hcoParamDisplayNamesNow.add(hcoParam.Display_Name__c);
        }
        numberOfParams = hcoParamUpsertList.size();
        upsert hcoParamUpsertList External_ID__c;
        /*List<HCO_Parameters__c> hcoParamUpserttoList = new List<HCO_Parameters__c>();
        SObjectAccessDecision securityDecision = Security.stripInaccessible(AccessType.UPSERTABLE, hcoParamUpsertList);
                hcoParamUpserttoList = securityDecision.getRecords();
                upsert hcoParamUpserttoList External_ID__c;

                //throw exception if permission is missing
                if(!securityDecision.getRemovedFields().isEmpty() )
                {
                    if(hcoParamUpsertList.External_ID__c != null)
                        throw new SnTException(securityDecision, 'HCO_Segmentation_Ctrl', SnTException.OperationType.C );
                    else
                        throw new SnTException(securityDecision, 'HCO_Segmentation_Ctrl', SnTException.OperationType.U );
                }*/

        for(String s : hcoParamAlreadyMap.keySet()){
            if(!hcoParamDisplayNamesNow.contains(s)){
                hcoParamDeleteList.add(hcoParamAlreadyMap.get(s));
            }
        }
        //delete hcoParamDeleteList;
        SnTDMLSecurityUtil.deleteRecords(hcoParamDeleteList,'HCO_Segmentation_Ctrl');
        
        PageMessages = SLDSPageMessage.add(PageMessages,'HCO Parameters saved successfully!!','success');
        
         
       // ApexPages.addMessage(new ApexPages.message(Apexpages.Severity.CONFIRM,'HCO Parameters saved successfully!!'));
        if(paramFlag){
            calculateHCOParam();
        }

    }

    public class ParameterWrapper{
        Decimal Sequence;
        String Column_Name_Selected;
        String Value;
        String API_Name;
        Decimal Level_Selected;
        String LastPublishedDate;

        public ParameterWrapper(Decimal seq,String colSel, String val,String ruleParam, Decimal level,DateTime lastPubDate){
            this.Sequence = seq;
            this.Column_Name_Selected = colSel;
            this.Value = val;
            this.API_Name = ruleParam;
            this.Level_Selected = level;
            this.LastPublishedDate = lastPubDate == null? '' : String.valueOf(lastPubDate);
        }
    }

    public void calculateHCOParam(){
        if(selectedDestinationRule != '' && selectedSourceRule != '' && numberOfParams > 0){
            //saveParameters();
            List<Account_Compute_Final__c> acfList ;
            try{
                acfList = [Select Id from Account_Compute_Final__c where Measure_Master__c =: selectedSourceRule  WITH SECURITY_ENFORCED limit 1];
            }
            catch(System.QueryException qe) 
            {
                SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
            }
            if(acfList.size() == 1)
            {
              Database.executeBatch(new BatchDeleteHCOSegmentPrevResult(selectedSourceRule,selectedDestinationRule),2000);
              PageMessages = SLDSPageMessage.add(PageMessages,'HCO Parameters are being calculated. Kindly wait for a while.','success');
              //ApexPages.addMessage(new ApexPages.message(Apexpages.Severity.CONFIRM,'HCO Parameters are being calculated. Kindly wait for a while.'));
            }
            else
            {
                PageMessages = SLDSPageMessage.add(PageMessages,'Kindly execute the source rule.','warning');
                //ApexPages.addMessage(new ApexPages.message(Apexpages.Severity.WARNING,'Kindly execute the source rule.'));
            }
        }
        else{
        
           PageMessages = SLDSPageMessage.add(PageMessages,'Kindly select the rules for publishing the parameters.','warning');
           // ApexPages.addMessage(new ApexPages.message(Apexpages.Severity.WARNING,'Kindly select the rules for publishing the parameters.'));
        }
    }
    
   // MCCP_Utility mccp = new MCCP_Utility();
    //MCCP_Utility.getKeyValueFromPlatformCache('SIQCountryID');
   // MCCP_Utility.getCountryIDFromCookie();
}