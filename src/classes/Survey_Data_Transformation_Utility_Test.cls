@isTest
private class Survey_Data_Transformation_Utility_Test 
{
    private static testMethod void firstTest() 
    {
        AxtriaSalesIQTM__Organization_Master__c aom = new AxtriaSalesIQTM__Organization_Master__c(AxtriaSalesIQTM__Org_Level__c='Global',AxtriaSalesIQTM__Parent_Country_Level__c=true);
        insert aom;
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='Italy',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active');
        insert country;
        
        /*Cycle__c cycle = new Cycle__c(Name='Test Cycle',Country__c = country.id);
        insert cycle;*/
        
        /*Business_Unit__c bu = new Business_Unit__c(Name='Test BU Loopup');
        insert bu;*/
        
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='Test Team'/*,Business_Unit_Loopup__c=bu.id*/);
        team.AxtriaSalesIQTM__Country__c = country.id;
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU',AxtriaSalesIQTM__Team__c=team.id);
        insert ti;

        Product_Catalog__c pc = new Product_Catalog__c(Name='Test Product',Team_Instance__c=ti.id,Veeva_External_ID__c = 'ProdId',Product_Code__c= 'ProdId',IsActive__c=true,Country_Lookup__c=country.id);
        insert pc;
        
        /*Brand_Team_Instance__c bti = new Brand_Team_Instance__c(Brand__c=pc.id,Team_Instance__c=ti.id);
        insert bti;*/
        
        /*Prepare Staging_Survey_Data__c data*/

        Staging_Survey_Data__c ssd = new Staging_Survey_Data__c();
        ssd.CurrencyIsoCode = 'USD';
        ssd.Sales_Cycle__c = '2018CICLO2';
        ssd.SURVEY_ID__c = '1';
        ssd.SURVEY_NAME__c = 'Methanol_pcp';
        ssd.Team_Instance__c = '';
        ssd.Team__c = 'RIA_CRESTOR';
        ssd.Account_Number__c = 'IT100501';
        ssd.Product_Code__c = '172_002000017000_IT';
        ssd.Product_Name__c = 'Methanol';
        ssd.Position_Code__c = '';
        ssd.Question_ID1__c = 1;
        ssd.Question_ID2__c = 2;
        ssd.Question_ID3__c = 3;
        ssd.Question_ID4__c = 4;
        ssd.Question_ID5__c = 5;
        ssd.Question_ID6__c = 6;
        ssd.Question_ID7__c = 7;
        ssd.Question_ID8__c = 8;
        ssd.Question_ID9__c = 9;
        ssd.Question_ID10__c = 10;
        ssd.Response1__c = '1';
        ssd.Response2__c = '2';
        ssd.Response3__c = '3';
        ssd.Response4__c = '4';
        ssd.Response5__c = '5';
        ssd.Response6__c = '6';
        ssd.Response7__c = '7';
        ssd.Response8__c = '8';
        ssd.Response9__c = '9';
        ssd.Response10__c = '10';
        ssd.Short_Question_Text1__c = 'a';
        ssd.Short_Question_Text2__c = 'b';
        ssd.Short_Question_Text3__c = 'c';
        ssd.Short_Question_Text4__c = 'd';
        ssd.Short_Question_Text5__c = 'e';
        ssd.Short_Question_Text6__c = 'f';
        ssd.Short_Question_Text7__c = 'g';
        ssd.Short_Question_Text8__c = 'h';
        ssd.Short_Question_Text9__c = 'i';
        ssd.Short_Question_Text10__c = 'j';
        ssd.Short_Question_Text11__c = 'k';
        ssd.Short_Question_Text12__c = 'l';
        ssd.Short_Question_Text13__c = 'm';
        ssd.Short_Question_Text14__c = 'n';
        ssd.Short_Question_Text15__c = 'o';
        ssd.Short_Question_Text16__c = 'p';
        ssd.Short_Question_Text17__c = 'q';
        ssd.Short_Question_Text18__c = 'r';
        ssd.Short_Question_Text19__c = 's';
        ssd.Short_Question_Text20__c = 't';
        ssd.Short_Question_Text21__c = 'u';
        ssd.Short_Question_Text22__c = 'v';
        ssd.Short_Question_Text23__c = 'w';
        ssd.Short_Question_Text24__c = 'x';
        ssd.Short_Question_Text25__c = 'y';
        insert ssd;

        System.test.startTest();
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        Survey_Data_Transformation_Utility batch = new Survey_Data_Transformation_Utility();
        Survey_Data_Transformation_Utility batch2 = new Survey_Data_Transformation_Utility(ti.Name);
        Survey_Data_Transformation_Utility batch3 = new Survey_Data_Transformation_Utility(ti.Name + ';test','Test');
        Survey_Data_Transformation_Utility batch4 = new Survey_Data_Transformation_Utility(ti.Name,'Testing', 'Type');
        Database.executeBatch(batch2,2000);
        System.test.stopTest();
    }
    private static testMethod void secondTest() 
    {
        AxtriaSalesIQTM__Organization_Master__c aom = new AxtriaSalesIQTM__Organization_Master__c(AxtriaSalesIQTM__Org_Level__c='Global',AxtriaSalesIQTM__Parent_Country_Level__c=true);
        insert aom;
        
        AxtriaSalesIQTM__Country__c country = new AxtriaSalesIQTM__Country__c(Name='Italy',AxtriaSalesIQTM__Parent_Organization__c=aom.id,AxtriaSalesIQTM__Status__c='Active');
        insert country;
        
        /*Cycle__c cycle = new Cycle__c(Name='Test Cycle',Country__c = country.id);
        insert cycle;*/
        
        /*Business_Unit__c bu = new Business_Unit__c(Name='Test BU Loopup');
        insert bu;
        */
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c(Name='Test Team'/*,Business_Unit_Loopup__c=bu.id*/);
        team.AxtriaSalesIQTM__Country__c = country.id;
        insert team;
        
        AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c(Name='Test BU',AxtriaSalesIQTM__Team__c=team.id);
        insert ti;

        Product_Catalog__c pc = new Product_Catalog__c(Name='Test Product',Team_Instance__c=ti.id,Veeva_External_ID__c = 'ProdId',Product_Code__c= 'ProdId',IsActive__c=true,Country_Lookup__c=country.id);
        insert pc;
        
        /*Brand_Team_Instance__c bti = new Brand_Team_Instance__c(Brand__c=pc.id,Team_Instance__c=ti.id);
        insert bti;*/
        
        /*Prepare Staging_Survey_Data__c data*/
        Staging_Survey_Data__c ssd = new Staging_Survey_Data__c();
        ssd.CurrencyIsoCode = 'USD';
        ssd.Sales_Cycle__c = '2018CICLO2';
        ssd.SURVEY_ID__c = '1';
        ssd.SURVEY_NAME__c = 'Methanol_pcp';
        ssd.Team_Instance__c = '';
        ssd.Team__c = 'RIA_CRESTOR';
        ssd.Account_Number__c = 'IT100501';
        ssd.Product_Code__c = '172_002000017000_IT';
        ssd.Product_Name__c = 'Methanol';
        ssd.Position_Code__c = '';
        ssd.Question_ID1__c = 1;
        ssd.Question_ID2__c = 2;
        ssd.Question_ID3__c = 3;
        ssd.Question_ID4__c = 4;
        ssd.Question_ID5__c = 5;
        ssd.Question_ID6__c = 6;
        ssd.Question_ID7__c = 7;
        ssd.Question_ID8__c = 8;
        ssd.Question_ID9__c = 9;
        ssd.Question_ID10__c = 10;
        ssd.Response1__c = '1';
        ssd.Response2__c = '2';
        ssd.Response3__c = '3';
        ssd.Response4__c = '4';
        ssd.Response5__c = '5';
        ssd.Response6__c = '6';
        ssd.Response7__c = '7';
        ssd.Response8__c = '8';
        ssd.Response9__c = '9';
        ssd.Response10__c = '10';
        ssd.Short_Question_Text1__c = 'a';
        ssd.Short_Question_Text2__c = 'b';
        ssd.Short_Question_Text3__c = 'c';
        ssd.Short_Question_Text4__c = 'd';
        ssd.Short_Question_Text5__c = 'e';
        ssd.Short_Question_Text6__c = 'f';
        ssd.Short_Question_Text7__c = 'g';
        ssd.Short_Question_Text8__c = 'h';
        ssd.Short_Question_Text9__c = 'i';
        ssd.Short_Question_Text10__c = 'j';
        ssd.Short_Question_Text11__c = 'k';
        ssd.Short_Question_Text12__c = 'l';
        ssd.Short_Question_Text13__c = 'm';
        ssd.Short_Question_Text14__c = 'n';
        ssd.Short_Question_Text15__c = 'o';
        ssd.Short_Question_Text16__c = 'p';
        ssd.Short_Question_Text17__c = 'q';
        ssd.Short_Question_Text18__c = 'r';
        ssd.Short_Question_Text19__c = 's';
        ssd.Short_Question_Text20__c = 't';
        ssd.Short_Question_Text21__c = 'u';
        ssd.Short_Question_Text22__c = 'v';
        ssd.Short_Question_Text23__c = 'w';
        ssd.Short_Question_Text24__c = 'x';
        ssd.Short_Question_Text25__c = 'y';
        
        insert ssd;

        System.test.startTest();
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
        String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
        List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
        System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
        //Survey_Data_Transformation_Utility batch = new Survey_Data_Transformation_Utility();
        Survey_Data_Transformation_Utility batch2 = new Survey_Data_Transformation_Utility(true);
        Database.executeBatch(batch2,2000);
        System.test.stopTest();
    }
}