global with sharing class Batch_product_portfolio implements Database.Batchable < sObject > , Database.Stateful {
  public string query;
  public string teamInstance;
  public string changeReqID;
  //public Boolean flagValue = false;
  public list < AxtriaSalesIQTM__Team_Instance__c > teaminst = new list < AxtriaSalesIQTM__Team_Instance__c > ();
  public Boolean teamInstExists;
  public List < AxtriaSalesIQTM__Change_Request__c > changeRequestCreation;
  public List < temp_Obj__c > processedTempObjList;
  public Integer recsTotal = 0;
  Public list < Product_Catalog__c > LSTProd_Cat = new list < Product_Catalog__c > (); 
  public Integer recsProcessed = 0;
  public Boolean flag = true;
  public String batchName;
  public String changeReqStatus;
  public string countryid;
  public string TIid;
  public string Teamid;
  public string extid;
   
  global Batch_product_portfolio(String TIname, String ids) 
    {
    System.debug(' ---TIname----->'+TIname);
        teamInstance = TIname;
        changeReqID = ids;
        //flagValue = flag;
        System.debug(' ---teamInstance----->'+teamInstance);
        teaminst = [select id from AxtriaSalesIQTM__Team_Instance__c where name =: teamInstance];
        teamInstExists = teaminst.size()>0 ? true : false;
        System.debug(' ---teamInstExists----->'+teamInstExists);
        
        changeRequestCreation = [Select Id, AxtriaSalesIQTM__Request_Type_Change__c,Records_Created__c from AxtriaSalesIQTM__Change_Request__c where id = :ids];
        System.debug(' ---changeRequestCreation----->'+changeRequestCreation);
        
        query = 'Select Id,Name,Weights__c,Full_Load__c,Product_Type__c,Portfolio_Name__c,Country_Code__c,Product_Description__c,Detail_Group__c,Team_Instance_Text__c,Status__c, AccountNumber__c,Position_Code__c, Parent_Account_ID__c, Team_Instance_Name__c,Product_Name__c,Product_Code__c, isError__c FROM temp_Obj__c  where Status__c = \'New\'  and Object__c = \'Product and portfolio\' and Change_Request__c=:changeReqID  and Team_Instance_Name__c  =:teamInstance  WITH SECURITY_ENFORCED';
    System.debug(' ---main query----->'+query);
        
    }
    
   global Database.Querylocator start(Database.BatchableContext bc) 
    {
        return Database.getQueryLocator(query);
    }


    global void execute(Database.BatchableContext BC, List < temp_Obj__c > Scope)
    {
    System.debug(' ---inside execute----->'+Scope.size());
     processedTempObjList = new List < temp_Obj__c > ();
     recsTotal += scope.size();
     Set<String> countrycode = new Set<String>();
     Set<String> TIname = new Set<String>();
     map<string,List<string>> mapPortfPrdCd = new map<string,List<string>>();
     map<string,temp_Obj__c> portfTemp = new map<string,temp_Obj__c>();
     map<string,string> mapcrtid = new map<string,string>();
     map<string,string> mapTInstid = new map<string,string>();
     map<string,string> mapTeamid = new map<string,string>();
     List<Product_Catalog__c> lst_pro_cat_extid = new list<Product_Catalog__c>();
        try 
        {
            for(temp_Obj__c tempObj : scope)
            {
                System.debug(' ---inside for loop execute----->'+Scope.size());
                if(!teamInstExists)
                { 
                    tempObj.status__c = 'Rejected';
                    tempObj.isError__c = true;
                    tempObj.Error_Message__c = 'Incorrect Team Instance present';
                    tempObj.SalesIQ_Error_Message__c = 'Incorrect Team Instance present';
                    processedTempObjList.add(tempObj);
                }
                
                 countrycode.add(tempObj.Country_Code__c);
                  TIname.add(tempObj.Team_Instance_Name__c);
                System.debug(' ---inside execute countrycode----->'+countrycode);
                System.debug(' ---inside execute TIname----->'+TIname);
            }
            List<AxtriaSalesIQTM__Country__c> lstcontry = new list<AxtriaSalesIQTM__Country__c>();
            List<AxtriaSalesIQTM__Team_Instance__c> teaminstname = new list<AxtriaSalesIQTM__Team_Instance__c>();
    
            lstcontry =[Select id ,name from AxtriaSalesIQTM__Country__c where name=:countrycode];
            for(AxtriaSalesIQTM__Country__c ct: lstcontry){
                mapcrtid.put(ct.name,ct.id);
            }
             System.debug(' ---inside execute mapcrtid country id ----->'+mapcrtid);
            
             teaminstname = [select id,name,AxtriaSalesIQTM__Team__c from AxtriaSalesIQTM__Team_Instance__c where Name =: TIname];
             
             for(AxtriaSalesIQTM__Team_Instance__c ct: teaminstname){
                mapTInstid.put(ct.name,ct.id);
                mapTeamid.put(ct.name,ct.AxtriaSalesIQTM__Team__c);  
             }
             
            if(teamInstExists  )
            { 
                System.debug(' ---Team instance exist----->');
                 
                for (temp_Obj__c tempObj : scope) 
                {
                
                   if((tempObj.Portfolio_Name__c != null) && mapPortfPrdCd.get(tempObj.Portfolio_Name__c) != null && mapPortfPrdCd.get(tempObj.Portfolio_Name__c).size()!= 0){
                        mapPortfPrdCd.get(tempObj.Portfolio_Name__c).add(tempObj.Product_Code__c);
                    }
                    else if(tempObj.Portfolio_Name__c != null){
                        mapPortfPrdCd.put(tempObj.Portfolio_Name__c,new list<string>{tempObj.Product_Code__c});
                    }
                    portfTemp.put(tempobj.Portfolio_Name__c,tempobj);
                    
                    
                   
                    if(tempObj.Status__c!='Rejected') 
                    {  
                     System.debug(' ---All if condation passed----->');
                        
                        
                        if(tempObj.Product_Code__c != null && tempObj.Product_Code__c != '' && tempObj.Country_Code__c != null && tempObj.Country_Code__c != '' ){
                            if(tempobj.Detail_Group__c!=null && tempobj.Detail_Group__c!='')
                            {               
                                extid = mapTInstid.get(tempobj.Team_Instance_Name__c)+'_'+tempobj.Detail_Group__c+'_'+tempobj.Product_Code__c;
                                System.debug('---if codation externam id --->'+extid);
                            }
                            else
                            {
                                extid= mapTInstid.get(tempobj.Team_Instance_Name__c)+'_null'+'_'+tempobj.Product_Code__c;
                                System.debug('---else codation externam id --->'+extid);
                            }
                            
                            lst_pro_cat_extid = [Select id,name,Team_Instance__c,Detail_Group__c,Veeva_External_ID__c,External_ID__c from Product_Catalog__c where External_ID__c =:extid ];
                            System.debug('---list of product catalog record store existing external id --->'+lst_pro_cat_extid.size());
                            
                            if( lst_pro_cat_extid.isEmpty()){
                                System.debug('In side of creating record');
                                
                                Product_Catalog__c PC = new Product_Catalog__c();
                                PC.Country_Lookup__c = mapcrtid.get(tempobj.Country_Code__c);
                                PC.Team_Instance__c= mapTInstid.get(tempobj.Team_Instance_Name__c); // Team instance alsi lookup field on PC
                                PC.Team__c=mapTeamid.get(tempobj.Team_Instance_Name__c);
                                
                                PC.Product_Code__c = tempobj.Product_Code__c;
                                PC.Veeva_External_ID__c = tempobj.Product_Code__c;
                                PC.Full_Load__c=tempobj.Full_Load__c;
                                PC.Product_Type__c =tempobj.Product_Type__c;
                                PC.name= tempobj.Product_Name__c;
                                PC.Product_Description__c=tempobj.Product_Description__c;
                                PC.Weights__c=tempobj.Weights__c;
                                PC.Detail_Group__c=tempobj.Detail_Group__c;
                              
                            
                                tempObj.status__c = 'Processed';
                                tempObj.isError__c = false;
                                processedTempObjList.add(tempObj);
                                LSTProd_Cat.add(PC);
                                 System.debug(' ---inside execute LSTProd_Cat----->'+LSTProd_Cat);
                            }  
                             else
                            {
                                tempObj.status__c = 'Rejected';
                                tempObj.isError__c = true;
                                tempObj.Error_Message__c = 'uplicate records already exists';
                                tempObj.SalesIQ_Error_Message__c = 'Duplicate records already exists';
                                processedTempObjList.add(tempObj);
                            }
                        }
                        else
                            {
                                tempObj.status__c = 'Rejected';
                                tempObj.isError__c = true;
                                tempObj.Error_Message__c = 'Product code and country code cannot be left blank';
                                tempObj.SalesIQ_Error_Message__c = 'Product code and country code cannot be left blank';
                                processedTempObjList.add(tempObj);
                            }
                        
                        
                    
                    } 
                   else 
                    {
                        tempObj.status__c = 'Rejected';
                        tempObj.isError__c = true;
                        tempObj.Error_Message__c = 'Mandatory field missing or Incorrect';
                        tempObj.SalesIQ_Error_Message__c = 'Mandatory field missing or Incorrect';
                        processedTempObjList.add(tempObj);
                    }   
                } 
        
            }
            if(mapPortfPrdCd.keyset().size() > 0){
                    for(string st: mapPortfPrdCd.keyset() ){
                          System.debug('------->in side key set- mapPortfPrdCd-----'+mapPortfPrdCd);
                            System.debug('------->in side key set- portfTemp-----'+portfTemp);
                            
                        Product_Catalog__c PC = new Product_Catalog__c();
                        
                        PC.Country_Lookup__c =mapcrtid.get(portfTemp.get(st).Country_Code__c);
                        PC.Team_Instance__c= mapTinstid.get(portfTemp.get(st).Team_Instance_Name__c); // Team instance alsi lookup field on PC
                        PC.Team__c=mapTeamid.get(portfTemp.get(st).Team_Instance_Name__c);
                        
                        PC.Product_Code__c = string.join(mapPortfPrdCd.get(st),';');
                        PC.Veeva_External_ID__c =string.join(mapPortfPrdCd.get(st),';');
                       
                        PC.Full_Load__c=portfTemp.get(st).Full_Load__c;
                        
                        PC.Product_Type__c =portfTemp.get(st).Product_Type__c;
                        PC.name= portfTemp.get(st).Portfolio_Name__c;
                        
                        PC.Product_Description__c=portfTemp.get(st).Product_Description__c;
                        PC.Weights__c=portfTemp.get(st).Weights__c;
                        PC.Detail_Group__c=portfTemp.get(st).Detail_Group__c;
                        PC.is_Catalog__c=True;
                        PC.Product_List__c=string.join(mapPortfPrdCd.get(st),';');
                         LSTProd_Cat.add(PC);
                    }
                }
        
        
            if (!LSTProd_Cat.isEmpty()) {
            
                SnTDMLSecurityUtil.insertRecords(LSTProd_Cat, 'Batch_product_portfolio');

              /*  SnTDMLSecurityUtil.printDebugMessage('LSTProd_Cat--' + LSTProd_Cat);
                SnTDMLSecurityUtil.printDebugMessage('LSTProd_Cat size--' + LSTProd_Cat.size());

                Database.SaveResult[] ds = Database.insert(LSTProd_Cat, false);
                  System.debug(' ---inside execute record update ds----->'+ds);
                for (Database.SaveResult d: ds) {
                    if (d.isSuccess()) {
                        recsProcessed++;
                    } else {
                        flag = false;
                        for (Database.Error err: d.getErrors()) {
                            SnTDMLSecurityUtil.printDebugMessage('The following error has occurred.');
                            SnTDMLSecurityUtil.printDebugMessage(err.getStatusCode() + ': ' + err.getMessage());
                            SnTDMLSecurityUtil.printDebugMessage('Fields that affected this error: ' + err.getFields());
                        }
                    }
                }*/
            }
        
            if (!processedTempObjList.isEmpty()) 
            {
                SnTDMLSecurityUtil.printDebugMessage('processedTempObjList--' + processedTempObjList);
                SnTDMLSecurityUtil.printDebugMessage('processedTempObjList size--' + processedTempObjList.size());

                SObjectAccessDecision securityDecision = Security.stripInaccessible(AccessType.UPDATABLE, processedTempObjList);
                List < temp_Obj__c > tempList = securityDecision.getRecords();
                SnTDMLSecurityUtil.updateRecords(tempList, batchName);

                if (!securityDecision.getRemovedFields().isEmpty()) {
                    throw new SnTException(securityDecision, 'temp_Obj__c', SnTException.OperationType.U);
                }
            }
        
        }
        catch (Exception e) {
            SnTDMLSecurityUtil.printDebugMessage(batchName + ' : Error in execute()--' + e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack trace--' + e.getStackTraceString());
        }
    
        
    }

    global void finish(Database.BatchableContext BC) {
        System.debug(' ---inside Finished record update ds----->');
       SnTDMLSecurityUtil.printDebugMessage(batchName + ' : constructor invoked--');

        Boolean noJobErrors;
        if (changeReqID != null && changeReqID != '') {
            AxtriaSalesIQTM__Change_Request__c changeRequest = new AxtriaSalesIQTM__Change_Request__c();
            changeRequest.Id = changeReqID;
            changeRequest.Records_Updated__c = recsProcessed;
            if (changeRequestCreation.size() > 0 && changeRequestCreation[0].AxtriaSalesIQTM__Request_Type_Change__c == 'Data Load Backend') {
                changeRequest.Records_Created__c = recsTotal;
            }
            SnTDMLSecurityUtil.updateRecords(changerequest, batchName);
            noJobErrors = ST_Utility.getJobStatus(BC.getJobId());
            changeReqStatus = flag && noJobErrors ? 'Done' : 'Error';
            BatchUpdateTempObjRecsCR batchCall = new BatchUpdateTempObjRecsCR(changeReqID, true, 'Mandatory Field Missing or Incorrect Team Instance', changeReqStatus);
            Database.executeBatch(batchCall, 2000);
            
        } 
    }
}