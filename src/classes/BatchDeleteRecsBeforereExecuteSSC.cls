/*Author - Himanshu Tariyal(A0994)
Date : 7th January 2018*/
global with sharing class BatchDeleteRecsBeforereExecuteSSC implements Database.Batchable<sObject> {
    public String query;
    public String ruleId;
    public String whereClause;

    global BatchDeleteRecsBeforereExecuteSSC(String ruleId, String whereClause) 
    {       
        this.ruleId = ruleId;
        this.whereClause = whereClause;
        this.query = 'SELECT Id FROM Segment_Simulation_Copy__c WHERE Measure_Master__c = \''+ruleId+'\' WITH SECURITY_ENFORCED';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        try{
            return Database.getQueryLocator(query);
        }
        catch(System.QueryException qe){
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }
        return null;
    }

    global void execute(Database.BatchableContext BC, list<Sobject> scope) 
    {
        if(scope.size()>0 && scope!=null){
            if(Segment_Simulation_Copy__c.sObjectType.getDescribe().isDeletable()){
                Database.DeleteResult[] srList = Database.delete(scope, false);
            }
            else{
                SnTDMLSecurityUtil.printDebugMessage('You dont have permission to delete Segment_Simulation_Copy__c','BatchDeleteRecsBeforereExecuteSSC');
            }
        }
    }

    global void finish(Database.BatchableContext BC) 
    {
        BatchDeleteRecsBeforereExecuteMS batchExecute = new BatchDeleteRecsBeforereExecuteMS(ruleId, WhereClause );
        Database.executeBatch(batchExecute, 2000);
    }
}