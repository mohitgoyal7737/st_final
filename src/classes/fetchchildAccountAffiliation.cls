//@Author
//A1691--Mohit Goyal
global class fetchchildAccountAffiliation implements Database.Batchable<sObject>,Schedulable{
    
    public String query; 
    public Integer recordsProcessed=0;
    public String batchID;
    global DateTime lastjobDate=null;
    public String cycle {get;set;}
    public map <string,string> AccounttoPosition;
    public map <string,string> AccounttoDestinationPOsTeamInstance;
    public map <string,string> AccounttoDestinationTeamInstance;
    public map <string,Date> PosTIStartDate;
    public map <string,Date> PosTIendate;
    public map <string,string> AccounttoAlignmentPeriod;
    global Set<string> AccountsID;
    public map <string,string> finalLstHCA_HCP ;
    public map <string,string> finalLstHCA_HCP1 ;
    public map <string,string> finalLstHCA_HCP2 ;
    public map <string,Date> finalLstHCA_HCP3 ;
    public map <string,Date> finalLstHCA_HCP4 ;
    public map <string,string> finalLstHCA_HCP5 ;
   public map <string,string> accId ;
   global set<String> UniquekeySet;
    global set<String> keytochekc;
    global set<string> keychekforPositionAccount {get;set;}
    global string Str1;
    global string period;
    global list<AxtriaSalesIQTM__Position_Account__c> insertnewPosAccount;
   //Integer level = 0;
    

    global fetchchildAccountAffiliation (){

      List<AxtriaARSnT__Scheduler_Log__c> schLogList = new List<AxtriaARSnT__Scheduler_Log__c>();
       schLogList=[Select Id,CreatedDate,AxtriaARSnT__Created_Date2__c from AxtriaARSnT__Scheduler_Log__c where AxtriaARSnT__Job_Name__c='childAccountAffiliation' and AxtriaARSnT__Job_Status__c='Successful' Order By AxtriaARSnT__Created_Date2__c desc];
        if(schLogList.size()>0)
        {
          lastjobDate=schLogList[0].AxtriaARSnT__Created_Date2__c;  //set the lastjobDate to the last successfull batch job run if there exists an entry
        }
        else
        {
          lastjobDate=null;       //else we set the lastjobDate to null
        }
        System.debug('last job'+lastjobDate);
        //Last Batch run ID
        AxtriaARSnT__Scheduler_Log__c sJob = new AxtriaARSnT__Scheduler_Log__c();

        sJob.AxtriaARSnT__Job_Name__c = 'childAccountAffiliation';
        sJob.AxtriaARSnT__Job_Status__c = 'Failed';
        sJob.AxtriaARSnT__Job_Type__c='Affiliation';
        if(cycle!=null && cycle!='')
          sJob.AxtriaARSnT__Cycle__c=cycle;
        sJob.AxtriaARSnT__Created_Date2__c = DateTime.now();

        insert sJob;
        batchID = sJob.Id;

        recordsProcessed =0;
       query ='SELECT id,AxtriaSalesIQTM__Account_Alignment_Type__c,AxtriaSalesIQTM__Destination_Position_Team_Instance__c,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Change_Request__c,AxtriaSalesIQTM__Comments__c,AxtriaSalesIQTM__Destination_Position__c,AxtriaSalesIQTM__IsImpactCallPlan__c,AxtriaSalesIQTM__Source_Position_Team_Instance__c,AxtriaSalesIQTM__Source_Position__c,AxtriaSalesIQTM__Team_Instance_Account__c,LastModifiedDate,Name,AxtriaSalesIQTM__Destination_Position_Team_Instance__r.AxtriaSalesIQTM__Team_Instance_ID__c,AxtriaSalesIQTM__Destination_Position_Team_Instance__r.AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Destination_Position_Team_Instance__r.AxtriaSalesIQTM__Team_Instance_ID__r.AxtriaSalesIQTM__Alignment_Period__c,AxtriaSalesIQTM__Destination_Position_Team_Instance__r.AxtriaSalesIQTM__Team_Instance_ID__r.AxtriaSalesIQTM__IC_EffstartDate__c,AxtriaSalesIQTM__Destination_Position_Team_Instance__r.AxtriaSalesIQTM__Team_Instance_ID__r.AxtriaSalesIQTM__IC_EffEndDate__c FROM AxtriaSalesIQTM__CR_Account__c where AxtriaSalesIQTM__Change_Request__r.AxtriaSalesIQTM__Request_Type_Change__c = \'Account Movement\' AND AxtriaSalesIQTM__Account__r.Type = \'HCA\' AND AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__Country__r.name =\'Great Britain\' AND AxtriaSalesIQTM__Account__c!=null AND AxtriaSalesIQTM__Destination_Position__c!=null AND (AxtriaSalesIQTM__Destination_Position_Team_Instance__r.AxtriaSalesIQTM__Team_Instance_ID__r.AxtriaSalesIQTM__Alignment_Period__c =\'Current\' OR AxtriaSalesIQTM__Destination_Position_Team_Instance__r.AxtriaSalesIQTM__Team_Instance_ID__r.AxtriaSalesIQTM__Alignment_Period__c = \'Future\' )' ;
        
        if(lastjobDate!=null){
          query = query + 'AND LastModifiedDate  >=:  lastjobDate  '; 
        }

        System.debug('query'+ query);
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<AxtriaSalesIQTM__CR_Account__c> scope) {
        
       AccounttoPosition=new map <string,string>();
       UniquekeySet = new set<String>();
       keytochekc = new set<String>();
       keychekforPositionAccount = new set<string>();
       AccounttoDestinationPOsTeamInstance = new map <string,string>();
       AccounttoDestinationTeamInstance = new map <string,string>();
       insertnewPosAccount = new list<AxtriaSalesIQTM__Position_Account__c>();
       PosTIStartDate = new map <string,Date>();
       PosTIendate = new map <string,Date>();
       AccounttoAlignmentPeriod = new map <string,string>();
       finalLstHCA_HCP = new  map <string,string>();
       finalLstHCA_HCP1 = new  map <string,string>();
       finalLstHCA_HCP2 = new  map <string,string>();
       finalLstHCA_HCP3 = new  map <string,Date>();
       finalLstHCA_HCP4 = new  map <string,Date>();
       finalLstHCA_HCP5 = new  map <string,string>();
      

      for (AxtriaSalesIQTM__CR_Account__c CA :scope){

          AccounttoPosition.put(CA.AxtriaSalesIQTM__Account__c,CA.AxtriaSalesIQTM__Destination_Position__c);
          AccounttoDestinationPOsTeamInstance.put(CA.AxtriaSalesIQTM__Account__c,CA.AxtriaSalesIQTM__Destination_Position_Team_Instance__c);
          AccounttoDestinationTeamInstance.put(CA.AxtriaSalesIQTM__Account__c,CA.AxtriaSalesIQTM__Destination_Position_Team_Instance__r.AxtriaSalesIQTM__Team_Instance_ID__c);
          PosTIStartDate.put(CA.AxtriaSalesIQTM__Account__c,CA.AxtriaSalesIQTM__Destination_Position_Team_Instance__r.AxtriaSalesIQTM__Team_Instance_ID__r.AxtriaSalesIQTM__IC_EffstartDate__c);
          PosTIendate.put(CA.AxtriaSalesIQTM__Account__c,CA.AxtriaSalesIQTM__Destination_Position_Team_Instance__r.AxtriaSalesIQTM__Team_Instance_ID__r.AxtriaSalesIQTM__IC_EffEndDate__c);
          AccounttoAlignmentPeriod.put(CA.AxtriaSalesIQTM__Account__c,CA.AxtriaSalesIQTM__Destination_Position_Team_Instance__r.AxtriaSalesIQTM__Team_Instance_ID__r.AxtriaSalesIQTM__Alignment_Period__c);
        }


        system.debug('AccounttoPosition*****************'+AccounttoPosition);


       
      fetchchildAffiliation(AccounttoPosition,AccounttoDestinationPOsTeamInstance,AccounttoDestinationTeamInstance,PosTIendate,PosTIStartDate,AccounttoAlignmentPeriod,0);
     
       system.debug('finalLstHCA_HCP()()()()()()()()()()()'+finalLstHCA_HCP);

      list<AxtriaSalesIQTM__Position_Account__c > PosAccountfill = [Select AxtriaSalesIQTM__Account__c,PAUniquekey__c,AxtriaSalesIQTM__Account_Alignment_Type__c,AxtriaSalesIQTM__Segment_1__c,
AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Assignment_Status__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position_Team_Instance__c,AxtriaSalesIQTM__Team_Instance__c,Id,AxtriaSalesIQTM__Account__r.type,AxtriaSalesIQTM__Affiliation_Based_Alignment__c,LastModifiedDate,LastModifiedBy.Name FROM AxtriaSalesIQTM__Position_Account__c where AxtriaSalesIQTM__Account__c IN : finalLstHCA_HCP.keySet() AND AxtriaSalesIQTM__Account_Alignment_Type__c = 'Explicit' AND (AxtriaSalesIQTM__Assignment_Status__c = 'Future Active' OR AxtriaSalesIQTM__Assignment_Status__c = 'Active')];


  for (AxtriaSalesIQTM__Position_Account__c PA : PosAccountfill){

       UniquekeySet.add(PA.PAUniquekey__c);

        }

     system.debug('UniquekeySet################'+UniquekeySet);



  
     if(finalLstHCA_HCP.size()>0 && finalLstHCA_HCP!=null ){


            
            for(String apuc : finalLstHCA_HCP.keySet())
            {


               AxtriaSalesIQTM__Position_Account__c newobj = new AxtriaSalesIQTM__Position_Account__c();
                   if(AccounttoPosition!=null && AccounttoPosition.size()>0 )
                    newobj.AxtriaSalesIQTM__Position__c = finalLstHCA_HCP.get(apuc);
                    newobj.AxtriaARSnT__Accessibility_Range__c = '9999';
                    newobj.AxtriaARSnT__isDirectLoad__c = 'TRUE';
                    newobj.AxtriaSalesIQTM__Account__c = apuc;
                    newobj.AxtriaSalesIQTM__Account_Alignment_Type__c = 'Explicit';
                    if(PosTIendate!=null )
                    newobj.AxtriaSalesIQTM__Effective_Start_Date__c = finalLstHCA_HCP4.get(apuc);
                    if(PosTIendate!=null )
                    newobj.AxtriaSalesIQTM__Effective_End_Date__c = finalLstHCA_HCP3.get(apuc);
                    if(AccounttoDestinationTeamInstance!=null && AccounttoDestinationTeamInstance.size()>0 )
                    newobj.AxtriaSalesIQTM__Team_Instance__c = finalLstHCA_HCP2.get(apuc);
                    newobj.AxtriaSalesIQTM__Segment_1__c= 'Primary';
                    if(AccounttoDestinationPOsTeamInstance!=null && AccounttoDestinationPOsTeamInstance.size()>0 )
                    newobj.AxtriaSalesIQTM__Position_Team_Instance__c = finalLstHCA_HCP1.get(apuc);
                    if(AccounttoPosition!=null && AccounttoPosition.size()>0 )
                    newobj.AxtriaSalesIQTM__Proposed_Position__c = finalLstHCA_HCP.get(apuc);
                    
                    if(AccounttoAlignmentPeriod!=null )
                    {
                      if(finalLstHCA_HCP5.get(apuc)=='Current')
                         period = 'Active';
                      else
                        period = 'Future Active';
                    }


                    Str1 = String.valueof(newobj.AxtriaSalesIQTM__Account__c).substring(0, 15)+'_'+String.valueof(newobj.AxtriaSalesIQTM__Position__c).substring(0, 15)+'_'+String.valueof(newobj.AxtriaSalesIQTM__Team_Instance__c).substring(0, 15)+'_'+period;
                    if(!UniquekeySet.contains(Str1)){
                      insertnewPosAccount.add(newobj);
                      UniquekeySet.add(Str1);
                    }
                  
                  
               }
               insert insertnewPosAccount;
             }
         

         }

          public void fetchchildAffiliation(map <string,string> accId,map <string,string> accId1,map <string,string> accId2,map <string,Date> accId3,map <string,Date> accId4,map <string,string> accId5, Integer level){

            if(level<5 & accId !=null & accId.size()>0){

              list<AxtriaSalesIQTM__Account_Affiliation__c> Accaff = new list<AxtriaSalesIQTM__Account_Affiliation__c>();
        
              Accaff = [SELECT id,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Parent_Account__c  FROM 
              AxtriaSalesIQTM__Account_Affiliation__c where AxtriaSalesIQTM__Parent_Account__c IN : accId.keySet() AND AxtriaSalesIQTM__Active__c = TRUE AND (AxtriaARSnT__Affiliation_Status__c= 'Active' OR AxtriaARSnT__Affiliation_Status__c= 'Future Active') AND AxtriaARSnT__Country_Code__c = 'GB' AND AxtriaSalesIQTM__Parent_Account__r.Type = 'HCA' ];
         
              map <string,string> ids = new map <string,string>();
              map <string,string> ids1 = new map <string,string>();
              map <string,string> ids2 = new map <string,string>();
              map <string,Date> ids3 = new map <string,Date>();
              map <string,Date> ids4 = new map <string,Date>();
              map <string,string> ids5 = new map <string,string>();
              for(AxtriaSalesIQTM__Account_Affiliation__c obj : Accaff){

                if(!String.isBlank(obj.AxtriaSalesIQTM__Parent_Account__c)){
                  ids.put(obj.AxtriaSalesIQTM__Account__c, accId.get(obj.AxtriaSalesIQTM__Parent_Account__c));
                  ids1.put(obj.AxtriaSalesIQTM__Account__c, accId1.get(obj.AxtriaSalesIQTM__Parent_Account__c));
                  ids2.put(obj.AxtriaSalesIQTM__Account__c, accId2.get(obj.AxtriaSalesIQTM__Parent_Account__c));
                  ids3.put(obj.AxtriaSalesIQTM__Account__c, accId3.get(obj.AxtriaSalesIQTM__Parent_Account__c));
                  ids4.put(obj.AxtriaSalesIQTM__Account__c, accId4.get(obj.AxtriaSalesIQTM__Parent_Account__c));
                  ids5.put(obj.AxtriaSalesIQTM__Account__c, accId5.get(obj.AxtriaSalesIQTM__Parent_Account__c));
                  
                }
              }
              system.debug('ids*****************'+ids);
              finalLstHCA_HCP.putAll(ids);
              finalLstHCA_HCP1.putAll(ids1);
              finalLstHCA_HCP2.putAll(ids2);
              finalLstHCA_HCP3.putAll(ids3);
              finalLstHCA_HCP4.putAll(ids4);
              finalLstHCA_HCP5.putAll(ids5);
              system.debug('finalLstHCA_HCP*****************'+finalLstHCA_HCP);
              level++;
              fetchchildAffiliation(ids,ids1,ids2,ids3,ids4,ids5,level);

            }
          }

   
global void execute(SchedulableContext sc){
        Database.executeBatch(new fetchchildAccountAffiliation (), 200);
    }
    
     global void finish(Database.BatchableContext BC) {
       AxtriaARSnT__Scheduler_Log__c sJob = new AxtriaARSnT__Scheduler_Log__c(id = batchID); 
      system.debug('schedulerObj++++before'+sJob);
      //Update the scheduler log with successful
      sjob.AxtriaARSnT__Object_Name__c = 'childAccountAffiliation';
      //sjob.Changes__c                                       
           
      //sJob.No_Of_Records_Processed__c=recordsProcessed;
      sJob.AxtriaARSnT__Job_Status__c='Successful';
      //system.debug('error message'+msg);
      //sjob.Changes__c = msg;                    

      system.debug('sJob++++++++'+sJob);
      update sJob;
    }
}