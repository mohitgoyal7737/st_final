global class ATL_Transformation_null_Accounts implements Database.Batchable<sObject> {
    public String query;
    public Boolean chain = false;
    public List<String> allCountries;
    Set<String> activityLogIDSet;

    global ATL_Transformation_null_Accounts() {
        this.query = 'select id, Territory__c, Status__c, External_ID__c from Staging_ATL__c where Status__c != \'Updated\'';
    }

    global ATL_Transformation_null_Accounts(Boolean chainining) {
        chain = chainining;
        this.query = 'select id, Territory__c, Status__c, External_ID_AZ__c from Staging_ATL__c where Status__c != \'Updated\'';
    }

    global ATL_Transformation_null_Accounts(List<String> allCountries1 , Boolean chainining) {
        chain = chainining;
        allCountries = new List<String>(allCountries1);

        this.query = 'select id, Territory__c, Status__c, External_ID_AZ__c from Staging_ATL__c where Status__c != \'Updated\' and Country_Lookup__c in :allCountries';
    }

    global ATL_Transformation_null_Accounts(List<String> allCountries1 , Boolean chainining, Set<String> activityLogSet) {
        chain = chainining;
        allCountries = new List<String>(allCountries1);
        activityLogIDSet = new Set<String>();
        activityLogIDSet.addAll(activityLogSet);

        this.query = 'select id, Territory__c, Status__c, External_ID_AZ__c from Staging_ATL__c where Status__c != \'Updated\' and Country_Lookup__c in :allCountries';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Staging_ATL__c> scope) 
    {

        for(Staging_ATL__c sa : scope)
        {
            sa.Territory__c = ';​';
            sa.Status__c = 'Updated';
        }

        upsert scope External_ID_AZ__c;
        
    }

    global void finish(Database.BatchableContext BC) 
    {

        if(activityLogIDSet != null)
        {
            database.executebatch(new stagingATLComparison(allCountries,activityLogIDSet),2000);
        }
        else
        {
            system.debug('++allCountries'+allCountries);
            database.executebatch(new stagingATLComparison(allCountries),2000);
        }
        //if(chain)
        //{
            /*list<string> teaminstancelistis = new list<string>();
            teaminstancelistis = StaticTeaminstanceList.getFullLoadTeamInstances();
            database.executeBatch(new changeTSFStatus(teaminstancelistis, true),2000);*/          
        //}
    }
}