/**********************************************************************************************
@author       : Himanshu Tariyal (A0994)
@createdDate  : 15th August 2020
@description  : Test class for BatchPopulateChannelPreference
@Revision(s)  : v1.0
**********************************************************************************************/
@isTest
private class BatchPopulateChannelPreferenceTest 
{
	//Data added via UI based test
    public static TestMethod void testMethod1() 
    {
    	String className = 'BatchPopulateChannelPreferenceTest';

    	//Create Org Master rec
    	AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    	SnTDMLSecurityUtil.insertRecords(orgmas,className);

    	//Create Country Master
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);

        //Create Team
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);

        //Create Workspace
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today().addDays(90));
        workspace.Name = 'WS';
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);

        //Create Team Instance
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins,className);

        //Create Scenario
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        SnTDMLSecurityUtil.insertRecords(scen,className);

        //Create Product Catalog
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        pcc.Product_Code__c='PROD1';
        pcc.Veeva_External_ID__c='PROD1';
        pcc.Team_Instance__c = null;
        SnTDMLSecurityUtil.insertRecords(pcc,className);

        //Create Account
        Account acc = new Account();
		acc.Name = 'Acc1';
		acc.AccountNumber = 'Acc1';
		acc.Marketing_Code__c = 'US';
		SnTDMLSecurityUtil.insertRecords(acc,className);

		//Create Change Request
		AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
		cr.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.Id;
		SnTDMLSecurityUtil.insertRecords(cr,className);

		//Create Channel Info record
		Channel_Info__c ci = new Channel_Info__c();
		ci.Country__c = countr.Id;
		ci.Channel_Name__c = 'Email';
		SnTDMLSecurityUtil.insertRecords(ci,className);

		Channel_Info__c ci2 = new Channel_Info__c();
		ci2.Country__c = countr.Id;
		ci2.Channel_Name__c = 'F2F';
		SnTDMLSecurityUtil.insertRecords(ci2,className);

		//Create Temp Obj recs
		List<temp_Obj__c> tempObjList = new List<temp_Obj__c>();

		temp_Obj__c rec1 = new temp_Obj__c();
		rec1.Channel_Name__c = '';
		rec1.Change_Request__c = cr.Id;
		tempObjList.add(rec1);

		temp_Obj__c rec1b = new temp_Obj__c();
		rec1b.Channel_Name__c = 'Email';
		rec1b.Channel_Preference__c = '';
		rec1b.Change_Request__c = cr.Id;
		tempObjList.add(rec1b);

		temp_Obj__c rec1c = new temp_Obj__c();
		rec1c.Channel_Name__c = 'Email';
		rec1c.Channel_Preference__c = 'abcd';
		rec1c.Change_Request__c = cr.Id;
		tempObjList.add(rec1c);

		temp_Obj__c rec2 = new temp_Obj__c();
		rec2.Channel_Name__c = 'Email';
		rec2.Channel_Preference__c = '1';
		rec2.AccountNumber__c = '';
		rec2.Change_Request__c = cr.Id;
		tempObjList.add(rec2);

		temp_Obj__c rec3 = new temp_Obj__c();
		rec3.Channel_Name__c = 'Email';
		rec3.Channel_Preference__c = '1';
		rec3.AccountNumber__c = 'Acc1';
		rec3.Product_Code__c = '';
		rec3.Change_Request__c = cr.Id;
		tempObjList.add(rec3);

		temp_Obj__c rec4 = new temp_Obj__c();
		rec4.Channel_Name__c = 'Email';
		rec4.Channel_Preference__c = '1';
		rec4.Product_Code__c = 'PROD1';
		rec4.AccountNumber__c = 'Acc2';
		rec4.Change_Request__c = cr.Id;
		tempObjList.add(rec4);

		temp_Obj__c rec5 = new temp_Obj__c();
		rec5.Channel_Name__c = 'Email';
		rec5.Channel_Preference__c = '1';
		rec5.AccountNumber__c = 'Acc1';
		rec5.Product_Code__c = 'PROD2';
		rec5.Change_Request__c = cr.Id;
		tempObjList.add(rec5);

		temp_Obj__c rec6 = new temp_Obj__c();
		rec6.Channel_Name__c = 'Email2';
		rec6.Channel_Preference__c = '1';
		rec6.AccountNumber__c = 'Acc1';
		rec6.Product_Code__c = 'PROD1';
		rec6.Change_Request__c = cr.Id;
		tempObjList.add(rec6);

		temp_Obj__c rec7 = new temp_Obj__c();
		rec7.Channel_Name__c = 'Email';
		rec7.Channel_Preference__c = '1';
		rec7.AccountNumber__c = 'Acc1';
		rec7.Product_Code__c = 'PROD1';
		rec7.Change_Request__c = cr.Id;
		tempObjList.add(rec7);

		temp_Obj__c rec8 = new temp_Obj__c();
		rec8.Channel_Name__c = 'Email';
		rec8.Channel_Preference__c = '1';
		rec8.AccountNumber__c = 'Acc1';
		rec8.Product_Code__c = 'PROD1';
		rec8.Change_Request__c = cr.Id;
		tempObjList.add(rec8);

		temp_Obj__c rec9 = new temp_Obj__c();
		rec9.Channel_Name__c = 'F2F';
		rec9.Channel_Preference__c = '1';
		rec9.AccountNumber__c = 'Acc1';
		rec9.Product_Code__c = 'PROD1';
		rec9.Change_Request__c = cr.Id;
		tempObjList.add(rec9);
		SnTDMLSecurityUtil.insertRecords(tempObjList,className); 

		//Create MCCP Unique Channel Preference data
		MCCP_DataLoad__c data1 = new MCCP_DataLoad__c();
		data1.RecordTypeID = Schema.SObjectType.MCCP_DataLoad__c.getRecordTypeInfosByName().get('Unique Channel Preference').getRecordTypeId();
		data1.Product__c = pcc.Id;
		data1.Country__c = countr.Id;
		data1.Channel_Name__c = 'Email';
		data1.ExternalID__c = data1.RecordTypeID+'_'+countr.Id+'_'+data1.Channel_Name__c+'_'+pcc.Id;
		SnTDMLSecurityUtil.insertRecords(data1,className);

        System.Test.startTest();

        //Check FLS of HCP__c field in MCCP DataLoad object
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = :className];
        String namespace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';

        List<String> MCCP_DATALOAD_READFIELD = new List<String>{namespace+'HCP__c'};
    	System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(MCCP_DataLoad__c.SObjectType,MCCP_DATALOAD_READFIELD,false));

        //Test BatchPopulateProductSegment
        Database.executeBatch(new BatchPopulateChannelPreference(cr.Id,countr.Id),2000);

        System.Test.stopTest();
    }

    //Data added directly in Temp obj based test
    public static TestMethod void testMethod2() 
    {
    	String className = 'BatchPopulateChannelPreferenceTest';

    	//Create Org Master rec
    	AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
    	SnTDMLSecurityUtil.insertRecords(orgmas,className);

    	//Create Country Master
        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);

        //Create Team
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        SnTDMLSecurityUtil.insertRecords(team,className);

        //Create Workspace
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today().addDays(90));
        workspace.Name = 'WS';
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);

        //Create Team Instance
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins,className);

        //Create Scenario
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins, team, workspace);
        SnTDMLSecurityUtil.insertRecords(scen,className);

        //Create Product Catalog
        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        pcc.Product_Code__c='PROD1';
        pcc.Veeva_External_ID__c='PROD1';
        pcc.Team_Instance__c = null;
        SnTDMLSecurityUtil.insertRecords(pcc,className);

        //Create Account
        Account acc = new Account();
		acc.Name = 'Acc1';
		acc.AccountNumber = 'Acc1';
		acc.Marketing_Code__c = 'US';
		SnTDMLSecurityUtil.insertRecords(acc,className);

		//Create Change Request
		AxtriaSalesIQTM__Change_Request__c cr = new AxtriaSalesIQTM__Change_Request__c();
		cr.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.Id;
		SnTDMLSecurityUtil.insertRecords(cr,className);

		//Create Channel Info record
		Channel_Info__c ci = new Channel_Info__c();
		ci.Country__c = countr.Id;
		ci.Channel_Name__c = 'Email';
		SnTDMLSecurityUtil.insertRecords(ci,className);

		//Create Temp Obj recs
		List<temp_Obj__c> tempObjList = new List<temp_Obj__c>();

		temp_Obj__c rec1 = new temp_Obj__c();
		rec1.Channel_Name__c = '';
		rec1.Change_Request__c = cr.Id;
		rec1.Status__c = 'New';
		rec1.Object_Name__c = 'MCCP Channel Preference';
		tempObjList.add(rec1);

		temp_Obj__c rec1b = new temp_Obj__c();
		rec1b.Channel_Name__c = 'Email';
		rec1b.Channel_Preference__c = '';
		rec1b.Change_Request__c = cr.Id;
		rec1b.Status__c = 'New';
		rec1b.Object_Name__c = 'MCCP Channel Preference';
		tempObjList.add(rec1b);

		temp_Obj__c rec1c = new temp_Obj__c();
		rec1c.Channel_Name__c = 'Email';
		rec1c.Channel_Preference__c = 'abcd';
		rec1c.Change_Request__c = cr.Id;
		rec1c.Status__c = 'New';
		rec1c.Object_Name__c = 'MCCP Channel Preference';
		tempObjList.add(rec1c);

		temp_Obj__c rec2 = new temp_Obj__c();
		rec2.Channel_Name__c = 'Email';
		rec2.Channel_Preference__c = '1';
		rec2.AccountNumber__c = '';
		rec2.Change_Request__c = cr.Id;
		rec2.Status__c = 'New';
		rec2.Object_Name__c = 'MCCP Channel Preference';
		tempObjList.add(rec2);

		temp_Obj__c rec3 = new temp_Obj__c();
		rec3.Channel_Name__c = 'Email';
		rec3.Channel_Preference__c = '1';
		rec3.AccountNumber__c = 'Acc1';
		rec3.Product_Code__c = '';
		rec3.Change_Request__c = cr.Id;
		rec3.Status__c = 'New';
		rec3.Object_Name__c = 'MCCP Channel Preference';
		tempObjList.add(rec3);

		temp_Obj__c rec4 = new temp_Obj__c();
		rec4.Channel_Name__c = 'Email';
		rec4.Channel_Preference__c = '1';
		rec4.Workspace__c = 'WS';
		rec4.Product_Code__c = 'PROD1';
		rec4.AccountNumber__c = 'Acc2';
		rec4.Change_Request__c = cr.Id;
		rec4.Status__c = 'New';
		rec4.Object_Name__c = 'MCCP Channel Preference';
		tempObjList.add(rec4);

		temp_Obj__c rec5 = new temp_Obj__c();
		rec5.Channel_Name__c = 'Email';
		rec5.Channel_Preference__c = '1';
		rec5.Workspace__c = 'WS';
		rec5.AccountNumber__c = 'Acc1';
		rec5.Product_Code__c = 'PROD2';
		rec5.Change_Request__c = cr.Id;
		rec5.Status__c = 'New';
		rec5.Object_Name__c = 'MCCP Channel Preference';
		tempObjList.add(rec5);

		temp_Obj__c rec6 = new temp_Obj__c();
		rec6.Channel_Name__c = 'Email2';
		rec6.Channel_Preference__c = '1';
		rec6.AccountNumber__c = 'Acc1';
		rec6.Product_Code__c = 'PROD1';
		rec6.Change_Request__c = cr.Id;
		rec6.Status__c = 'New';
		rec6.Object_Name__c = 'MCCP Channel Preference';
		tempObjList.add(rec6);

		temp_Obj__c rec7 = new temp_Obj__c();
		rec7.Channel_Name__c = 'Email';
		rec7.Channel_Preference__c = '1';
		rec7.Workspace__c = 'WS';
		rec7.AccountNumber__c = 'Acc1';
		rec7.Product_Code__c = 'PROD1';
		rec7.Change_Request__c = cr.Id;
		rec7.Status__c = 'New';
		rec7.Object_Name__c = 'MCCP Channel Preference';
		tempObjList.add(rec7);

		temp_Obj__c rec8 = new temp_Obj__c();
		rec8.Channel_Name__c = 'Email';
		rec8.Channel_Preference__c = '1';
		rec8.Workspace__c = 'WS';
		rec8.AccountNumber__c = 'Acc1';
		rec8.Product_Code__c = 'PROD1';
		rec8.Change_Request__c = cr.Id;
		rec8.Status__c = 'New';
		rec8.Object_Name__c = 'MCCP Channel Preference';
		tempObjList.add(rec8);
		SnTDMLSecurityUtil.insertRecords(tempObjList,className); 

        System.Test.startTest();

        //Check FLS of HCP__c field in MCCP DataLoad object
        ApexClass cs = [select NamespacePrefix from ApexClass where Name = :className];
        String namespace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';

        List<String> MCCP_DATALOAD_READFIELD = new List<String>{namespace+'HCP__c'};
    	System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(MCCP_DataLoad__c.SObjectType,MCCP_DATALOAD_READFIELD,false));

        //Test BatchPopulateProductSegment
        Database.executeBatch(new BatchPopulateChannelPreference(cr.Id,'MCCP Channel Preference',countr.Id),2000);

        System.Test.stopTest();
    }
}