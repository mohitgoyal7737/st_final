public with sharing class PositionProductOutBound implements Schedulable {
    Public List<Product_Catalog__c>ProductLIst {get;set;}
    public list<AxtriaSalesIQTM__Position__c>PositionList {get;set;}
    public map<String,list<String>>TeamPosition {get;set;}
    public map<String,List<Product_Catalog__c>> teamproduct {get;set;}
    public set<String>teaminstancelist {get;set;}
    public map<String,String>Countrymap {get;set;}
    public map<string,string>mcmap {get;set;}
    public set<String> posCodeList=new set<String>();

    public PositionProductOutBound() {
        /*System.debug('=====Inside Constructor=====');
        ProductLIst = new list<Product_Catalog__c>();
        PositionList = new list<AxtriaSalesIQTM__Position__c>();
        TeamPosition = new map<String,list<String>>();
        teamproduct = new map<String,List<Product_Catalog__c>>();
        teaminstancelist= new set<String>();
        Countrymap = new map<String,String>();
        mcmap = new map<string,string>();
        posCodeList.add('0');
        posCodeList.add('00');
        posCodeList.add('000');
        posCodeList.add('0000');
        posCodeList.add('00000');
        posCodeList.add('Unassigned');
        posCodeList.add('Unassigned Territory');
        FillMap();*/
    }
    public void execute(System.SchedulableContext SC){
       PositionProductOutBound ppo=new PositionProductOutBound ();
    } 
    public void FillMap(){
        /*System.debug('=======FILL MAP CALLAED=====');
        ProductLIst = [select id,External_ID__c,Team_Instance__c,Team_Instance__r.Name,Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c,Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c,Effective_End_Date__c,Effective_Start_Date__c,Team_Instance__r.AxtriaSalesIQTM__Team__r.name,name,Team__r.Country_Name__c,Team_Instance__r.AxtriaSalesIQTM__Team__r.Country_Name__c,createdDate,LastModifiedDate,Veeva_External_ID__c from Product_Catalog__c where Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = 'Current' and (Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c='Live' or Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c='Published') ];
        PositionList = [select id,AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = 'Current' and (AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c='Live' or AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c='Published') and (AxtriaSalesIQTM__Client_Position_Code__c!='00000' or AxtriaSalesIQTM__Client_Position_Code__c!='0')];
        System.debug('=======ProductLIst.Size()====='+ProductLIst.size());
        System.debug('=======PositionList.Size()====='+PositionList.size());
        list<AxtriaSalesIQTM__Country__c>CountryList = [select id,Name,AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Country__c];
        list<AxtriaARSnT__SIQ_MC_Country_Mapping__c>SIQCountry = [SELECT AxtriaARSnT__SIQ_BOT_Market_Code__c,AxtriaARSnT__SIQ_Country_Code__c,AxtriaARSnT__SIQ_MC_Code__c,AxtriaARSnT__SIQ_Veeva_Country_Code__c FROM AxtriaARSnT__SIQ_MC_Country_Mapping__c];

        //Fill the maps
        for(AxtriaSalesIQTM__Country__c country : CountryList){
            if(!Countrymap.containskey(country.name)){
                Countrymap.put(Country.Name,country.AxtriaSalesIQTM__Country_Code__c);
            }
        }

        if(SIQCountry !=null && SIQCountry.size() >0){
            for(AxtriaARSnT__SIQ_MC_Country_Mapping__c mc : SIQCountry){
                mcmap.put(mc.AxtriaARSnT__SIQ_Veeva_Country_Code__c ,mc.AxtriaARSnT__SIQ_MC_Code__c);
            }
        }
        for(Product_Catalog__c PC : ProductLIst){
            teaminstancelist.add(PC.Team_Instance__c);
            if(!teamproduct.containsKey(PC.Team_Instance__c)){
                teamproduct.put(PC.Team_Instance__c,new list<Product_Catalog__c>{ PC});
            }
            else{
                list<Product_Catalog__c>Products = new list<Product_Catalog__c>(teamproduct.get(PC.Team_Instance__c));
                Products.add(PC);
                teamproduct.put(PC.Team_Instance__c,Products);
            }
        }
        for(AxtriaSalesIQTM__Position__c Pos : PositionList){
            if(!TeamPosition.containsKey(Pos.AxtriaSalesIQTM__Team_Instance__c)){
                TeamPosition.put(Pos.AxtriaSalesIQTM__Team_Instance__c,new list<String> {pos.AxtriaSalesIQTM__Client_Position_Code__c});
            }
            else{
                list<String>positions= new list<String>(TeamPosition.get(Pos.AxtriaSalesIQTM__Team_Instance__c));
                positions.add(pos.AxtriaSalesIQTM__Client_Position_Code__c);
                TeamPosition.put(Pos.AxtriaSalesIQTM__Team_Instance__c,positions );
            }
        }
        createPosProduct();*/
    }
    public void createPosProduct(){
        /*System.debug('=========createPosProduct  Called======');
        System.debug('=========teamproduct======'+teamproduct);
        System.debug('----------------------');
        System.debug('=========TeamPosition.SIZEE======'+TeamPosition.size());
        System.debug('=========TeamPosition======'+TeamPosition);

        list<SIQ_Position_Product_O__c>Posproduct = new list<SIQ_Position_Product_O__c>();
        for(String team : teaminstancelist){
            for(Product_Catalog__c Prod :  teamproduct.get(team)){
                for(String pos : TeamPosition.get(team)){
                    if(!posCodeList.contains(pos)){
                        SIQ_Position_Product_O__c obj = new SIQ_Position_Product_O__c();
                        obj.SIQ_Position_ID__c = pos;
                        obj.SIQ_Product_Id__c = Prod.Veeva_External_ID__c;
                        obj.SIQ_Product_Name__c = Prod.Name;
                        obj.SIQ_Salesforce_Name__c = Prod.Team_Instance__r.AxtriaSalesIQTM__Team__r.name;
                        obj.SIQ_Team_Instance__c = Prod.Team_Instance__r.Name;
                        obj.SIQ_Team_Name__c = prod.Team_Instance__r.AxtriaSalesIQTM__Team__r.name;
                        obj.SIQ_Created_Date__c = prod.CreatedDate;
                        obj.SIQ_Updated_Date__c = prod.LastModifiedDate;
                        if(prod.Effective_Start_Date__c !=null){
                                obj.SIQ_Effective_Start_Date__c=prod.Effective_Start_Date__c;
                        }
                        else{
                            obj.SIQ_Effective_Start_Date__c=prod.Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c;
                        }
                        if(prod.Effective_End_Date__c!=null){
                                obj.SIQ_Effective_End_Date__c=prod.Effective_End_Date__c;    
                        }
                        else{
                            obj.SIQ_Effective_End_Date__c=prod.Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c; 
                        }
                        
                        obj.SIQ_Country_Code__c = Countrymap.get(prod.Team_Instance__r.AxtriaSalesIQTM__Team__r.Country_Name__c);
                        if(mcmap!=null ){
                            String Countrycode = Countrymap.get(prod.Team_Instance__r.AxtriaSalesIQTM__Team__r.Country_Name__c);
                            obj.SIQ_Marketing_Code__c = mcmap.get(Countrycode) !=null ? mcmap.get(Countrycode) : Countrymap.get(prod.Team__r.Country_Name__c);
                        }
                        obj.Unique_Id__c =Countrymap.get(prod.Team_Instance__r.AxtriaSalesIQTM__Team__r.Country_Name__c)+'_'+ pos+'_'+Prod.Name+'_'+prod.Team_Instance__r.Name;
                        Posproduct.add(obj);
                    }
                }

            }
        }

        System.debug('=======Posproduct.Size():'+Posproduct.Size());
        System.debug('==============Posproduct====='+Posproduct);

        Upsert Posproduct Unique_Id__c;*/
    }
}