global class stagingATLComparison implements Database.Batchable<sObject> 
{
    public String query;
    Map<string,Set<string>> mapacctoterritory;
    Map<string,Set<string>> mapacctoterritoryold;
    List<string> territorysplit;
    List<string> territoryoldsplit;
    List<string> territoryaddsplit;
    List<string> territorydelsplit;
    public List<String> allCountries;
    Set<String> activityLogIDSet;

    global stagingATLComparison() 
    {
        this.query = 'select id, Account__c, External_ID_AZ__c, Axtria_Account_ID__c,Country__c, Territory__c,Territory_Old__c,Territory_Added__c,Territory_Dropped__c from Staging_ATL__c where  Status__c = \'Updated\' ';
    }

    global stagingATLComparison(List<String> allCountries1) 
    {
        allCountries = new List<String>(allCountries1);
        system.debug('+++allCountries'+allCountries);
        this.query = 'select id, Account__c, External_ID_AZ__c, Axtria_Account_ID__c,Country__c, Territory__c,Territory_Old__c,Territory_Added__c,Territory_Dropped__c from Staging_ATL__c where  Status__c = \'Updated\' and Country_Lookup__c in :allCountries';
    }

    global stagingATLComparison(List<String> allCountries1, Set<String> activityLogSet) 
    {
        activityLogIDSet = new Set<String>();
        activityLogIDSet.addAll(activityLogSet);
        allCountries = new List<String>(allCountries1);
        system.debug('+++allCountries'+allCountries);
        this.query = 'select id, Account__c, External_ID_AZ__c, Axtria_Account_ID__c,Country__c, Territory__c,Territory_Old__c,Territory_Added__c,Territory_Dropped__c from Staging_ATL__c where  Status__c = \'Updated\' and Country_Lookup__c in :allCountries';
    }


    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Staging_ATL__c> scope) 
    {
       
        
        Set<String> currentTerritorySet;
        Set<String> oldTerritorySet ;
        List<string> atl1;
        List<string> atl2;
        mapacctoterritory=new Map<string,Set<string>>();
        mapacctoterritoryold= new Map<string,Set<string>>();

        for(Staging_ATL__c stag: scope)
        {
            territorysplit= new List<String>();
            territoryoldsplit= new List<String>();
            territoryaddsplit= new List<String>();
            territorydelsplit= new List<String>();
            system.debug('+++stag'+stag.External_ID_AZ__c);
            system.debug('+++stag terr'+stag.Territory__c);

            currentTerritorySet= new Set<String>();
            system.debug('+++currentTerritorySet size is  '+currentTerritorySet.size());

            oldTerritorySet=new Set<String>();
            atl1 = new List<string> ();
            atl2 = new List<string> ();
            stag.Count_of_Territory_Added__c=null;
            stag.Count_of_Territory_Dropped__c=null;
            stag.New_Status__c=null;
            
             if(stag.Territory__c!=null  && stag.Territory__c.length()>1 )
            {
                String str = stag.Territory__c.substring(1, stag.Territory__c.length() -1 );
                stag.Territory__c = str;
                System.debug('stag.Territory__c ::' + stag.Territory__c);
                System.debug('str::' + str);
                if(stag.Territory__c !=null & str.length()>0 & stag.Territory__c !=';' & stag.Territory__c != ';;')
                {
                    system.debug('insdie');
                    territorysplit= stag.Territory__c.split(';');
                    currentTerritorySet.addAll(territorysplit);
                }

                System.debug('territorysplit size :: ' + territorysplit.size());
                System.debug('territorysplit:: ' + territorysplit);
                system.debug('++currentTerritorySet Size  '+currentTerritorySet.size());
                system.debug('++currentTerritorySet   '+currentTerritorySet);
            }

             if(stag.Territory_Old__c!=null && stag.Territory_Old__c.length()>1)
            {  
                String Str1= stag.Territory_Old__c.substring(1, stag.Territory_Old__c.length() -1 );
                stag.Territory_Old__c= Str1;
                System.debug('stag.Territory_Old__c ::' + stag.Territory_Old__c);
                System.debug('Str1::' + Str1);
                if(stag.Territory_Old__c!=null && Str1.length()>0 && stag.Territory_Old__c!=';' & stag.Territory_Old__c != ';;')
                 {
                    system.debug('insdie old');
                    territoryoldsplit= stag.Territory_Old__c.split(';');
                    oldTerritorySet.addAll(territoryoldsplit);
                }

            }
            
            /*if(stag.Territory__c==';' || stag.Territory__c==null)
            {
                //territorysplit=new List<String>();
            }
            else
            {
                territorysplit= stag.Territory__c.split(';');
            }
            currentTerritorySet.addAll(territorysplit);*/


            /*if(stag.Territory_Old__c==';' || stag.Territory_Old__c==null || stag.Territory_Old__c==';;' )
            {
                //territoryoldsplit=new List<String>();
            }
            else
            {
                territoryoldsplit= stag.Territory_Old__c.split(';');
            }
              */
            //oldTerritorySet.addAll(territoryoldsplit);
            system.debug('+++territorysplit'+territorysplit);
            system.debug('+++territoryoldsplit'+territoryoldsplit);
            system.debug('+++currentTerritorySet'+currentTerritorySet);
            system.debug('+++currentTerritorySet size is  '+currentTerritorySet.size());
            system.debug('+++oldTerritorySet'+oldTerritorySet);
            system.debug('+++oldTerritorySet size is  '+oldTerritorySet.size());
            string add='';
            string del='';
            for(string pos:currentTerritorySet)
            {
                system.debug('++pos'+pos);
                if(pos!=null || pos!=' ')
                {
                    system.debug('++isndie pos');
                    if(!oldTerritorySet.contains(pos))
                    {
                        stag.New_Status__c='Updated';
                        atl1.add(pos);
                        system.debug('+++add first'+atl1);
                    }
                }
            }
            //stag.Territory_Added__c=add;
            system.debug('+++add first'+atl1);
            system.debug('+++add first part '+atl1.size());
            
            for(string position: oldTerritorySet)
            {
                system.debug('++position'+position);
                if(!currentTerritorySet.contains(position))
                {
                    stag.New_Status__c='Updated';
                    atl2.add(position); 
                    system.debug('+++del '+atl2);
                }
            }
             system.debug('+++territory del'+atl2.size());
             
            stag.Count_of_Territory_Added__c= string.valueOf(atl1.size());
            stag.Count_of_Territory_Dropped__c=string.valueOf(atl2.size());

            system.debug('Add count'+stag.Count_of_Territory_Added__c);
            system.debug('delete count'+stag.Count_of_Territory_Dropped__c);

            add=string.join(atl1,';');
            system.debug('+aadd'+add);

            del=string.join(atl2,';');
            system.debug('+delete'+del);
            stag.Territory_Added__c=add;
            stag.Territory_Dropped__c=del;

            system.debug('+delete Territory'+stag.Territory_Dropped__c);
            stag.Territory__c =';'+stag.Territory__c+';';
            system.debug('territory append'+ stag.Territory__c);
            stag.Territory_Old__c=';'+stag.Territory_Old__c+';';
            system.debug('territory old append'+ stag.Territory_Old__c);
        }
        update scope;
    }

    global void finish(Database.BatchableContext BC) 
    {

        if(activityLogIDSet != null)
        {
            List<Scheduler_Log__c> activityLogList = new List<AxtriaARSnT__Scheduler_Log__c>();

            AxtriaSalesIQTM__TriggerContol__c exeTrigger = new AxtriaSalesIQTM__TriggerContol__c();
            exeTrigger = AxtriaSalesIQTM__TriggerContol__c.getValues('ATLDeltaJob')!= null ? AxtriaSalesIQTM__TriggerContol__c.getValues('ATLDeltaJob'):null;

            system.debug('execute trigger' +exeTrigger );
            //public static Boolean executeTrigger; 
            if(exeTrigger != null)
            {
                if(exeTrigger.AxtriaSalesIQTM__IsStopTrigger__c != true)
                {
                    List<Scheduler_Log__c> logList = [select Id,Team_Instance__c,Job_Status__c from Scheduler_Log__c where Id in :activityLogIDSet and Country__c in :allCountries and Job_Type__c = 'ATL Delta Job' and Job_Status__c = 'Failed'];

                    for(Scheduler_Log__c rec : logList)
                    {
                        rec.Job_Status__c = 'Success';
                        activityLogList.add(rec);
                    }

                    update activityLogList;
                }
            }

            System.debug('====activityLogIDSet :::::::::' +activityLogIDSet);
            System.debug('====allCountries :::::::::' +allCountries);

            System.debug('Check all Country Status');
            List<Scheduler_Log__c> finallogList = [select Id,Team_Instance__c, Job_Status__c from Scheduler_Log__c where Id in :activityLogIDSet and Job_Type__c = 'ATL Delta Job' and Job_Status__c = 'Success'];

            System.debug('====finallogList.size() :::::::::' +finallogList.size());
            System.debug('====activityLogIDSet.size() :::::::::' +activityLogIDSet.size());

            Integer activityLogSize = activityLogIDSet.size();
            Integer finalLogSize = finallogList.size();
            if(activityLogSize == finalLogSize)
            {
                try
                {

                    AggregateResult aggResult = [select count(id) totalCount from Staging_ATL__c where New_Status__c = 'Updated'];
                    if((Integer.valueof(aggResult.get('totalCount'))) < 100000)
                    {
                        ID jobID = System.enqueueJob(new TalendHitATLJobQueueable());
                        System.debug('====jobID :::::::::' +jobID);
                    }
                    else
                    {
                        system.debug('Error in Talend job for ATL Delta Job due to huge delta');
                        String subject = 'Error in Talend job for ATL Delta Job due to huge delta';
                        String body = 'Error while execution Talend Job for ATL Delta Job because delta exceeds 1L Records';
                        String[] address = new String[]{'AZ_ROW_SalesIQ_DevSupport@Axtria.com'};
                        String []ccAdd=new String[]{'inchrowveevadi@astrazeneca.com'};
                        Messaging.SingleEmailMessage emailwithattch = new Messaging.SingleEmailMessage();
                        emailwithattch.setSubject(subject);
                        emailwithattch.setToaddresses(address);
                        emailwithattch.setPlainTextBody(body);
                        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {emailwithattch});
                    }
                }
                catch(Exception ex)
                {

                        String subject = 'Error in Talend job for ATL Delta Job due to Exception';
                        String body = 'Error while execution Talend Job for ATL Delta Job Exception:::::  ' +ex.getMessage();
                        String[] address = new String[]{'AZ_ROW_SalesIQ_DevSupport@Axtria.com'};
                        String []ccAdd=new String[]{'inchrowveevadi@astrazeneca.com'};
                        Messaging.SingleEmailMessage emailwithattch = new Messaging.SingleEmailMessage();
                        emailwithattch.setSubject(subject);
                        emailwithattch.setToaddresses(address);
                        emailwithattch.setPlainTextBody(body);
                        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {emailwithattch});
                }
            }
        }

    }
}