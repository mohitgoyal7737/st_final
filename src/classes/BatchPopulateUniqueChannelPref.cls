/**********************************************************************************************
@author       : Himanshu Tariyal (A0994)
@createdDate  : 20th August 2020
@description  : Controller for creating MCCP Unique Channel Preference data 
@Revision(s)  : v1.0
**********************************************************************************************/
global with sharing class BatchPopulateUniqueChannelPref implements Database.Batchable<sObject>,Database.Stateful
{
	public String query;
    public String batchName;
    public String crID;
    public String alignNmsp;
    public String countryID;
    public String workspaceID;
    public String productID;
    public String channelName;
    public String chPrefRTID;
    public String uniqueChPrefRTID;
    public String errorMessage;
    public String externalID;
    public String securityQuery = 'WITH SECURITY_ENFORCED';

    public Boolean flag = true;

    public List<SObject> crRec;
    public Map<String,Set<String>> mapExtIDToChannels;

    //If user loads data via UI
    global BatchPopulateUniqueChannelPref(String changeReqID,String wsCountryID) 
    {
        batchName = 'BatchPopulateUniqueChannelPref';
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : constructor invoked--');

        //Initialise variables
        crRec = new List<SObject>();
        mapExtIDToChannels = new Map<String,Set<String>>();

        //Get ART Namespace
        alignNmsp = MCCP_Utility.alignmentNamespace();
        SnTDMLSecurityUtil.printDebugMessage('alignNmsp--'+alignNmsp);

        crID = changeReqID;
        countryID = wsCountryID;
        SnTDMLSecurityUtil.printDebugMessage('crID--'+crID);
        SnTDMLSecurityUtil.printDebugMessage('countryID--'+countryID);

        //Get Record Type ID for 'Channel Preference' in MCCP DataLoad object
        chPrefRTID = Schema.SObjectType.MCCP_DataLoad__c.getRecordTypeInfosByName().get('Channel Preference').getRecordTypeId();
        SnTDMLSecurityUtil.printDebugMessage('chPrefRTID--'+chPrefRTID);

        //Get Record Type ID for 'Unique Channel Preference' in MCCP DataLoad object
        uniqueChPrefRTID = Schema.SObjectType.MCCP_DataLoad__c.getRecordTypeInfosByName().get('Unique Channel Preference').getRecordTypeId();
        SnTDMLSecurityUtil.printDebugMessage('uniqueChPrefRTID--'+uniqueChPrefRTID);
        
        //Get CR associated with the Direct Load activity
        String crQuery = 'select id,'+alignNmsp+'Request_Type_Change__c,Records_Created__c '+
                         'from '+alignNmsp+'Change_Request__c where id =:crID '+securityQuery;
        crRec = Database.query(crQuery);
        SnTDMLSecurityUtil.printDebugMessage('crRec size--'+crRec.size());

        /*query = 'select Product__c,Country__c,Channel_Name from MCCP_DataLoad__c where '+
                'Country__c=:countryID and RecordTypeId=:chConRTID and '+
                'Product__c!=null '+securityQuery;*/
        query = 'select Product__c,Country__c,Channel_Name__c from MCCP_DataLoad__c where '+
                'Country__c=:countryID and RecordTypeId=:chPrefRTID '+securityQuery;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : start() invoked--');
        SnTDMLSecurityUtil.printDebugMessage('query--'+query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC,List<MCCP_DataLoad__c> dataList) 
    {
        SnTDMLSecurityUtil.printDebugMessage(batchName+' : execute() invoked--');

        try
        {
        	Set<String> channelSet;
        	for(MCCP_DataLoad__c rec : dataList)
        	{
        		if(!mapExtIDToChannels.containsKey(rec.Product__c)){
        			mapExtIDToChannels.put(rec.Product__c,new Set<String>{rec.Channel_Name__c});
        		}
                SnTDMLSecurityUtil.printDebugMessage('rec.Channel_Name__c--'+rec.Channel_Name__c);

        		if(!mapExtIDToChannels.get(rec.Product__c).contains(rec.Channel_Name__c))
        		{
        			channelSet = mapExtIDToChannels.get(rec.Product__c);
        			channelSet.add(rec.Channel_Name__c);
        			mapExtIDToChannels.put(rec.Product__c,channelSet);
        		}
        	}
        	SnTDMLSecurityUtil.printDebugMessage('mapExtIDToChannels--'+mapExtIDToChannels);
        }
        catch(Exception e)
        {
            SnTDMLSecurityUtil.printDebugMessage(batchName+' : Error in execute()--'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack trace--'+e.getStackTraceString());
            flag = false;
        }
    }

    global void finish(Database.BatchableContext BC) 
    {
    	SnTDMLSecurityUtil.printDebugMessage(batchName+' : finish() invoked--');
    	SnTDMLSecurityUtil.printDebugMessage('mapExtIDToChannels--'+mapExtIDToChannels);

    	Savepoint sp = Database.setSavepoint();
    	Boolean noJobErrors;
        String changeReqStatus;
        MCCP_DataLoad__c newRec;
        Set<String> channelSet;
        List<MCCP_DataLoad__c> insertRecList = new List<MCCP_DataLoad__c>();

    	try
    	{
	        if(!crRec.isEmpty())
	        {
	        	if(flag==true)
	        	{
	        		//Delete previous recs
			        String delQuery = 'select Id from MCCP_DataLoad__c where RecordTypeId=:uniqueChPrefRTID '+
			        					'and Country__c=:countryID '+securityQuery;
			        List<SObject> delRecsList = Database.query(delQuery);
			        if(!delRecsList.isEmpty())
			        {
			        	if(Schema.sObjectType.MCCP_CNProd__c.isDeletable()){
			                SnTDMLSecurityUtil.deleteRecords(delRecsList,batchName);
			            }
			        }

			        //create new recs now
			        for(String key : mapExtIDToChannels.keySet())
			        {
			        	SnTDMLSecurityUtil.printDebugMessage('productID--'+key);
			        	SnTDMLSecurityUtil.printDebugMessage('channelSet--'+channelSet);

			        	channelSet = mapExtIDToChannels.get(key);
			        	for(String channel : channelSet)
			        	{
			        		SnTDMLSecurityUtil.printDebugMessage('productID--'+productID);
			        		newRec = new MCCP_DataLoad__c();
				        	newRec.Country__c = countryID;
				        	newRec.Product__c = key;
				        	newRec.ExternalID__c = uniqueChPrefRTID+'_'+countryID+'_'+key+'_'+channel;
				        	newRec.RecordTypeId = uniqueChPrefRTID;
				        	newRec.Channel_Name__c = channel;
				        	insertRecList.add(newRec);
			        	}
			        }

			        SnTDMLSecurityUtil.printDebugMessage('insertRecList size--'+insertRecList.size());
			        if(!insertRecList.isEmpty()){
			        	SnTDMLSecurityUtil.insertRecords(insertRecList,batchName);
			        }
	        	}

	            //Update temp Status of recs
	            noJobErrors = ST_Utility.getJobStatus(BC.getJobId());
	            changeReqStatus = flag && noJobErrors ? 'Done' : 'Error';

	            //Update temp obj recs as well as CR rec
	            BatchUpdateTempObjRecsCR batchCall = new BatchUpdateTempObjRecsCR(crID,true,'Mandatory Fields Missing',changeReqStatus);
	            Database.executeBatch(batchCall,2000);
	        }
    	}
    	catch(Exception e)
    	{
    		SnTDMLSecurityUtil.printDebugMessage(batchName+' : Error in finish()--'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack trace--'+e.getStackTraceString());
    		Database.rollback(sp);
    	}
    }
}