global class BatchUpdateSpeciality implements Database.Batchable<sObject>, Database.Stateful,schedulable {
    public Integer recordsProcessed=0;
     public String batchID;
     global DateTime lastjobDate=null;
     global String query;
     Public String nonProcessedAcs {get;set;}
     public integer errorcount{get;set;}

    global BatchUpdateSpeciality() {

        /*nonProcessedAcs = '';
        errorcount = 0;
        recordsProcessed =0;
        lastjobDate=date.today().addDays(-1);
         query = 'SELECT  Secondary_Specialty1__c,Secondary_Specialty2__c,Secondary_Specialty3__c,Secondary_Specialty4__c,Secondary_Specialty5__c,AxtriaSalesIQTM__Speciality1__c FROM Account ' +
                 'where lastmodifieddate >=:lastjobDate ' ;
        System.debug('query'+ query);
        //this.query = query;*/
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Account> records) {
        /*for (Account acc : records) {
                                  
               
                List<String> specList= new List<String>();
                if(acc.AxtriaSalesIQTM__Speciality1__c!=null){
                 specList= acc.AxtriaSalesIQTM__Speciality1__c.split(';');
                }
                if(specList.size()>0){
                System.debug(specList+'xxx');
                for(integer i=0;i<speclist.size();i++){
                    String v='Secondary_Specialty'+(i+1)+'__c';
                    System.debug(v+'v');
                    //String f1=(string)acc.get(v);
                    //System.debug(f1+'f1');
                    acc.put(v,speclist.get(i));
               }
                }
                              
                System.debug('specialty'+acc);
                recordsProcessed++;
                system.debug('recordsProcessed+'+recordsProcessed);
            //comments
           
           
     
        }    
        
        update records ;
        */
    }
    global void execute(System.SchedulableContext SC){
       /*database.executeBatch(new BatchUpdateSpeciality());*/
    }

    global void finish(Database.BatchableContext BC) {

    }
}