public class TalendSurveyLoadHitQueueable implements Queueable, Database.AllowsCallouts {

    String endPoint;
    String sfdcPath;
    String veevaPath;
    String axtriaUsername;
    String axtriaPassword;
    String veevaUsername;
    String veevaPassword;
    String id;
    String emailID;
    String schedulerID;
    
    public void execute(QueueableContext context)
    {
        system.debug('#### Hit Talend job for Survey load called ####');

        endPoint='';
        sfdcPath='';
        veevaPath='';
        axtriaUsername='';
        axtriaPassword='';
        veevaUsername='';
        veevaPassword=''; 
        schedulerID='';
        id = userInfo.getUserId();
        emailID =  [select Email from user where Id =: id].Email;
        System.debug('emailID:::::::: '+emailID);

        List<Scheduler_Log__c> schedulerLogRec = [Select id from Scheduler_Log__c where Job_Type__c = 'Survey Data Load' and Job_Status__c = 'Failed'];

        schedulerID = schedulerLogRec[0].Id;

        List<AxtriaSalesIQTM__ETL_Config__c> etlConfig = [Select Id, Name, AxtriaSalesIQTM__SFTP_Port__c, AxtriaSalesIQTM__Server_Type__c, AxtriaSalesIQTM__End_Point__c,AxtriaSalesIQTM__SFTP_Username__c,AxtriaSalesIQTM__SF_UserName__c,AxtriaSalesIQTM__SF_Password__c,AxtriaSalesIQTM__SFTP_Password__c from AxtriaSalesIQTM__ETL_Config__c where Name = 'Survey Data Load'];
        endPoint=etlConfig[0].AxtriaSalesIQTM__End_Point__c;
        sfdcPath=etlConfig[0].AxtriaSalesIQTM__Server_Type__c;
        veevaPath=etlConfig[0].AxtriaSalesIQTM__SFTP_Port__c; 
        axtriaUsername=etlConfig[0].AxtriaSalesIQTM__SF_UserName__c;
        axtriaPassword=etlConfig[0].AxtriaSalesIQTM__SF_Password__c;
        veevaUsername=etlConfig[0].AxtriaSalesIQTM__SFTP_Username__c;
        veevaPassword=etlConfig[0].AxtriaSalesIQTM__SFTP_Password__c;

        if(endPoint != '' && endPoint != null)
        {
          string TalendEndpoint=endPoint;
      
      
          try{

            TalendEndpoint += '&arg0=--context_param%20AZ_ARSNT_EUFULL_SIQ_Username='+axtriaUsername;
              TalendEndpoint += '&arg1=--context_param%20AZ_ARSNT_EUFULL_SIQ_Password='+axtriaPassword;
              TalendEndpoint += '&arg2=--context_param%20AZ_Veeva_Username='+veevaUsername;
              TalendEndpoint += '&arg3=--context_param%20AZ_Veeva_Password='+veevaPassword;
              TalendEndpoint += '&arg4=--context_param%20AZ_ARSNT_EUFULL_SIQ_URL='+sfdcPath;
              TalendEndpoint += '&arg5=--context_param%20AZ_Veeva_URL='+veevaPath;
              TalendEndpoint += '&arg6=--context_param%20UserEmail='+emailID;
              TalendEndpoint += '&arg7=--context_param%20sfdcID='+schedulerID;
              
              
              system.debug('#### TalendEndpoint : '+TalendEndpoint);

              Http h = new Http();
              HttpRequest request = new HttpRequest();
              TalendEndpoint = TalendEndpoint.replaceAll( '\\s+', '%20');
              request.setEndPoint(TalendEndpoint);
              request.setHeader('Content-type', 'application/json');
              request.setMethod('GET');
              //request.setTimeout(100000);
              system.debug('request '+request);
              HttpResponse response = h.send(request);
       
              system.debug('#### Talend Delete Response : '+response);
          }
          catch(exception ex)
          {
              system.debug('Error in Talend job for Survey Data load' +ex.getMessage());
              String subject = 'Error in Talend job for Survey Data load';
              String body = 'Error while execution Talend Job for Survey Data load:::::  ' +ex.getMessage();
              String[] address = new String[]{'Ayushi.Jain@Axtria.com'};
              //String []ccAdd=new String[]{'AZ_ROW_SalesIQ_MktDeployment@Axtria.com'};
              Messaging.SingleEmailMessage emailwithattch = new Messaging.SingleEmailMessage();
              emailwithattch.setSubject(subject);
              emailwithattch.setToaddresses(address);
              emailwithattch.setPlainTextBody(body);
              //emailwithattch.setCcAddresses(ccAdd);

              // Sends the email
              Messaging.sendEmail(new Messaging.SingleEmailMessage[] {emailwithattch});
          }
      }
      else
      {
        system.debug('Error in Talend job for Survey Data load due to End Point');
            String subject = 'Error in Talend job for Survey Data load due to End Point';
            String body = 'End Point Null for Survey Data load';
            String[] address = new String[]{'Ayushi.Jain@Axtria.com'};
            //String []ccAdd=new String[]{'AZ_ROW_SalesIQ_MktDeployment@Axtria.com'};
            Messaging.SingleEmailMessage emailwithattch = new Messaging.SingleEmailMessage();
            emailwithattch.setSubject(subject);
            emailwithattch.setToaddresses(address);
            emailwithattch.setPlainTextBody(body);
            //emailwithattch.setCcAddresses(ccAdd);

            // Sends the email
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {emailwithattch});
      }
    }
}