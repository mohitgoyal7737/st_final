global class PositionProductLoad implements Database.Batchable<sObject> {
    public String query;
    public String selectedTeamInstance;
    Map<String,Id> teamInstanceNametoIDMap;
    Map<String,string> teamInstanceNametoTeamNameMap;
    public  List<Staging_Position_Product__c> posp;
    public String selectedTeam;
    public String teamInstanceName1;
    public Date startDate;
    public Date endDate;
    public String teamInsName;
    public boolean fullload {get;set;}
    Set<String> allproduct;
    Set<String> positionset;
    Set<String> teamProductPositionSet;


    global PositionProductLoad(string teaminstance) 
    {

       /* teamInstanceNametoIDMap = new Map<String,Id>();
        teamInstanceNametoTeamNameMap = new Map<String,String>();
        selectedTeamInstance = teamInstance;
        
        for(AxtriaSalesIQTM__Team_Instance__c teamIns : [Select Id,Name,AxtriaSalesIQTM__IC_EffstartDate__c,AxtriaSalesIQTM__IC_EffendDate__c,AxtriaSalesIQTM__Team__r.name From AxtriaSalesIQTM__Team_Instance__c WHERE ID = :teamInstance])
        {
            teamInsName = teamIns.Name;
            teamInstanceNametoIDMap.put(teamIns.Name,teamIns.Id);
            teamInstanceNametoTeamNameMap.put(teamIns.Name,teamIns.AxtriaSalesIQTM__Team__r.name);
            startDate = teamIns.AxtriaSalesIQTM__IC_EffstartDate__c;
            endDate = teamIns.AxtriaSalesIQTM__IC_EffendDate__c;
        }
        system.debug('++++teamInsName++'+teamInsName);
         system.debug('++++teamInstanceNametoIDMap++'+teamInstanceNametoIDMap);
          system.debug('++++teamInstanceNametoTeamNameMap++'+teamInstanceNametoTeamNameMap);

        selectedTeam = teamInstanceNametoTeamNameMap.get(teamInsName);
        system.debug('++++selectedTeam++'+selectedTeam);


        query = 'Select id,Position_Name__c, Product_Code__c,Product_Name__c,Team_Instance__c,Holidays__c,Other_Days_Off__c,Vacation_Days__c,Status__c,Calls_Per_Days__c From Staging_Position_Product__c  where Team_Instance__c = \'' + teamInsName + '\' and Status__c = \'New\''; */
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Staging_Position_Product__c> posprod) 
    {    

       /* List<String> product = new List<String>();
        List<String> allposition = new List<String>();
        List<AxtriaSalesIQTM__Position_Product__c> posToInsert = new List<AxtriaSalesIQTM__Position_Product__c>();
        Map<String, Decimal> TeamInstanceProductIDtoHolidaysMap = new Map<String, Decimal>();
        Map<String, Decimal> TeamInstanceProductIDtoVacationDaysMap = new Map<String, Decimal>();
        Map<String, Decimal> TeamInstanceProductIDtoOtherDaysOffMap = new Map<String, Decimal>();
        Map<String, Decimal> TeamInstanceProductIDtoCallsperDayMap = new Map<String, Decimal>();
        Map<String, Id> mapproducttoID = new Map<String,Id>();
        Map<String, Id> mappostoId = new Map<String,Id>();
        allproduct= new Set<String>();
        positionset= new Set<String>();
        teamProductPositionSet=new Set<String>();

        //set for position and product
        for(Staging_Position_Product__c stagPosProd : posprod)
        {
            allproduct.add(stagPosProd.Product_Name__c);
            positionset.add(stagPosProd.Position_Name__c);
        }

        //query for products 
        List<Product_Catalog__c> prodC = [SELECT Id, Veeva_External_ID__c, Product_Code__c,Name,Team_Instance__c FROM Product_Catalog__c where Team_Instance__c=:selectedTeamInstance and IsActive__c=true];
        for(Product_Catalog__c pro : prodC)
        {
           product.add(pro.Veeva_External_ID__c);
           mapproducttoID.put(pro.Veeva_External_ID__c,pro.id);
        }
        system.debug('++++product++'+product);
        system.debug('++++mapproducttoID++'+mapproducttoID);
        
        //query for positions
        List<AxtriaSalesIQTM__Position__c> position = [Select id,AxtriaSalesIQTM__Client_Position_Code__c, Name from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_Instance__c =:selectedTeamInstance];
        for(AxtriaSalesIQTM__Position__c pos : position)
        {
             allposition.add(pos.AxtriaSalesIQTM__Client_Position_Code__c);
             mappostoId.put(pos.AxtriaSalesIQTM__Client_Position_Code__c,pos.id);
        }
        system.debug('++++allposition++'+allposition);
        system.debug('++++mappostoId++'+mappostoId);
        //query for team instance product
        List<Team_Instance_Product_AZ__c> allposproducts = [SELECT id,Effective_Days_in_Field_Formula__c,Vacation_Days__c,Other_Days_Off__c,Product_Catalogue__c,Product_Catalogue__r.Name,Holidays__c,Team_instance__c,Calls_Day__c FROM Team_Instance_Product_AZ__c where Team_instance__c =:selectedTeamInstance];
        for(Team_Instance_Product_AZ__c tipAZ : allposproducts)
        {
            string teamInstance = String.valueOf(tipAZ.Team_Instance__c);
            teamInstanceName1= teamInstance;
            TeamInstanceProductIDtoHolidaysMap.put(teamInstance+tipAZ.Product_Catalogue__r.Name,tipAZ.Holidays__c);
            TeamInstanceProductIDtoVacationDaysMap.put(teamInstance+tipAZ.Product_Catalogue__r.Name,tipAZ.Vacation_Days__c);
            TeamInstanceProductIDtoOtherDaysOffMap.put(teamInstance+tipAZ.Product_Catalogue__r.Name,tipAZ.Other_Days_Off__c);
            TeamInstanceProductIDtoCallsperDayMap.put(teamInstance+tipAZ.Product_Catalogue__r.Name,tipAZ.Calls_Day__c);
        }
        system.debug('++++TeamInstanceProductIDtoHolidaysMap++'+TeamInstanceProductIDtoHolidaysMap);
        system.debug('++++teamInstanceName1++'+teamInstanceName1);

         //query for position product
          List<AxtriaSalesIQTM__Position_Product__c> positionProduct1 = [SELECT id,Product_Catalog__r.name,Product_Catalog__r.Product_Code__c,AxtriaSalesIQTM__Position__r.name,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Team_Instance__r.name,Product_Catalog__c from AxtriaSalesIQTM__Position_Product__c where AxtriaSalesIQTM__Team_Instance__c=: selectedTeamInstance and Product_Catalog__r.name in:allproduct and AxtriaSalesIQTM__Position__r.name in:positionset and AxtriaSalesIQTM__isActive__c=true];
          for(AxtriaSalesIQTM__Position_Product__c positionProductSet :positionProduct1)
          {
            teamProductPositionSet.add(positionProductSet.AxtriaSalesIQTM__Team_Instance__c+'_'+positionProductSet.AxtriaSalesIQTM__Position__c+'_'+positionProductSet.Product_Catalog__c);
          }

        Set<String> updatedteamProductPositionSet = new Set<String>();
        for(Staging_Position_Product__c stag : posprod)
        {
            if(product.contains(stag.Product_Code__c)&& allposition.contains(stag.Position_Name__c))
            { 

                string key= teamInstanceNametoIDMap.get(stag.Team_Instance__c)+'_'+mappostoId.get(stag.Position_Name__c)+'_'+mapproducttoID.get(stag.Product_Code__c);
                system.debug('++key++'+key);
                if(!teamProductPositionSet.contains(key))
                {
                    AxtriaSalesIQTM__Position_Product__c positionProduct = new AxtriaSalesIQTM__Position_Product__c();
                    positionProduct.AxtriaSalesIQTM__Position__c = mappostoId.get(stag.Position_Name__c);
                    System.debug('2222222222');
                    System.debug('teamInstanceNametoIDMap.get(stag.Team_Instance__c)  :: '+teamInstanceNametoIDMap.get(stag.Team_Instance__c));
                    positionProduct.AxtriaSalesIQTM__Team_Instance__c = teamInstanceNametoIDMap.get(stag.Team_Instance__c);
                    System.debug('1111111111111');
                    positionProduct.Product_Catalog__c=mapproducttoID.get(stag.Product_Code__c);
                    positionProduct.AxtriaSalesIQTM__Effective_Start_Date__c = startDate;
                    positionProduct.AxtriaSalesIQTM__Effective_End_Date__c = endDate;
                    positionProduct.AxtriaSalesIQTM__isActive__c = true;
                    positionProduct.AxtriaSalesIQTM__Product_Weight__c = 0;

                    //Added by Prince
                    positionProduct.External_ID__c = mappostoId.get(stag.Position_Name__c) + '_' + mapproducttoID.get(stag.Product_Code__c);

                    positionProduct.Vacation_Days__c=  stag.Vacation_Days__c != null ?  stag.Vacation_Days__c :TeamInstanceProductIDtoVacationDaysMap.get(teamInstanceName1+stag.Product_Name__c);
                    positionProduct.Holidays__c = stag.Holidays__c!=null ? stag.Holidays__c :  TeamInstanceProductIDtoHolidaysMap.get(teamInstanceName1+stag.Product_Name__c);                
                    //positionProduct.Holidays__c=TeamInstanceProductIDtoHolidaysMap.get(teamInstanceName1+stag.Product_Name__c);
                    positionProduct.Other_Days_Off__c = stag.Other_Days_Off__c!=null ? stag.Other_Days_Off__c : TeamInstanceProductIDtoOtherDaysOffMap.get(teamInstanceName1+stag.Product_Name__c);
                   // positionProduct.Other_Days_Off__c=TeamInstanceProductIDtoOtherDaysOffMap.get(teamInstanceName1+stag.Product_Name__c);
                    // positionProduct.Calls_Day__c=TeamInstanceProductIDtoCallsperDayMap.get(teamInstanceName1+stag.Product_Name__c);
                    positionProduct.Calls_Day__c = stag.Calls_Per_Days__c !=null ? stag.Calls_Per_Days__c : TeamInstanceProductIDtoCallsperDayMap.get(teamInstanceName1+stag.Product_Name__c);

                    posToInsert.add(positionProduct);
                    stag.Status__c='Processed';
                    teamProductPositionSet.add(key);
                 } 
                else 
                {
                    if(!updatedteamProductPositionSet.contains(key)) 
                    {
                        system.debug('45678');
                        for(AxtriaSalesIQTM__Position_Product__c obj: positionProduct1){
                            if(obj.AxtriaSalesIQTM__Team_Instance__r.Name==stag.Team_Instance__c & obj.AxtriaSalesIQTM__Position__r.Name==stag.Position_Name__c & obj.Product_Catalog__r.Product_Code__c ==stag.Product_Code__c)
                            {
                                //AxtriaSalesIQTM__Position_Product__c obj11 = new AxtriaSalesIQTM__Position_Product__c();
                               // obj11 = obj.clone();
                              //  obj11.id = obj.id;
                                obj.Calls_Day__c = stag.Calls_Per_Days__c;
                                obj.Vacation_Days__c = stag.Vacation_Days__c;
                                obj.Holidays__c = stag.Holidays__c;
                                obj.Other_Days_Off__c = stag.Other_Days_Off__c;
                                stag.Status__c='Processed';
                                posToInsert.add(obj);
                                break;
                            }
                        }
                        updatedteamProductPositionSet.add(key);
                    }
                }
            }
            else
            {
                stag.Status__c='Product doesnot exist or position doesnot exist';
            }
           
        }
         if(!posToInsert.isEmpty())
            {
            upsert posToInsert;
            update posprod;
            } */

    }

    global void finish(Database.BatchableContext BC) {

    }
}