/**********************************************************************************************
@author     : Dhruv Mahajan
@date       : 6 July'2017
@description: This Batch Class is used for update and insert records in employee status object(Employee_Status__c) from Employee Staging
              (CurrentPersonFeed__c) and employee object(AxtriaSalesIQTM__Employee__c) data.
Revison(s)  :
**********************************************************************************************/

global class BatchEmployeeChangeStatusForOutbound implements Database.Batchable<sObject>
{
    global string query;
    global Datetime dtLastRun;
    global Datetime dtFirstRunInBatch;
    global Map<string, CurrentPersonFeed__c>                 empStagingMap;
    global Map<String, AxtriaSalesIQTM__Employee__c>        employeeIDMap;
    global map<string,Date> mapEmployee2EventTypeDate = new map<string,Date>();
    
    
    global BatchEmployeeChangeStatusForOutbound(map<string,Date> mapEmp2Event)
    { 
        empStagingMap       = new Map<string,CurrentPersonFeed__c>();
        employeeIDMap       = new Map<String, AxtriaSalesIQTM__Employee__c>();
        dtLastRun           = system.now();
        dtFirstRunInBatch   = system.now();
        mapEmployee2EventTypeDate=mapEmp2Event;
        
        query = 'Select id, AssignmentStatusValue__c, Employee_ID__c,PRID__c,JobChangeReason__c,TerminationDate__c From CurrentPersonFeed__c where isRejected__c=false ';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {   
        system.debug('==query=='+query);      
        return Database.getQueryLocator(query);
    }
    
    public void CreateMaps(List<CurrentPersonFeed__c> ScopeEmpStagingList)
    {
        system.debug('==ScopeEmpStagingList=='+ScopeEmpStagingList);
        set<string> emplList= new set<string>();
        
        for(CurrentPersonFeed__c currentStatus : ScopeEmpStagingList)
        {
            empStagingMap.put(currentStatus.PRID__c,currentStatus);     
            emplList.add(currentStatus.PRID__c);
            
        }
        
        List<AxtriaSalesIQTM__Employee__c> employeeOldStatus = [Select id, AxtriaSalesIQTM__Employee_ID__c, Employee_Status__c from AxtriaSalesIQTM__Employee__c where AxtriaSalesIQTM__Employee_ID__c in: emplList]; 
        
        Map<string, String> empandStatus = new  Map<string, String>();
        
        for(AxtriaSalesIQTM__Employee__c emp : employeeOldStatus)
        {
            if(empStagingMap.containsKey(emp.AxtriaSalesIQTM__Employee_ID__c)){
                CurrentPersonFeed__c currentempStatus = empStagingMap.get(emp.AxtriaSalesIQTM__Employee_ID__c);
               
                if(currentempStatus.AssignmentStatusValue__c != emp.Employee_Status__c)
                {
                    empandStatus.put(emp.Id, emp.Employee_Status__c);
                }
                 system.debug('==currentempStatus=='+currentempStatus);
            }
        }
        if(empandStatus!=Null && empandStatus.size()>0){
            List<AxtriaSalesIQTM__Employee_Status__c> lstEmpStatus = new List<AxtriaSalesIQTM__Employee_Status__c>();
            
            List<AxtriaSalesIQTM__Employee__c> employeeStatusKey = [Select id, Employee_Status__c, AxtriaSalesIQTM__Employee_ID__c,AxtriaSalesIQTM__HR_Status__c,
                                                                    (select id, AxtriaSalesIQTM__Effective_Start_Date__c,
                                                                    AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Employee__c,
                                                                    AxtriaSalesIQTM__Employee_Status__c,AxtriaSalesIQTM__Position__c 
                                                                    from AxtriaSalesIQTM__Employee_Status__r ORDER BY CreatedDate DESC)  
                                                                    from AxtriaSalesIQTM__Employee__c where id in: empandStatus.keySet()];
                                                                    
            for(AxtriaSalesIQTM__Employee__c newEmployeeList : employeeStatusKey){
                
                Date eventTypeDate;
                if(mapEmployee2EventTypeDate.containsKey(newEmployeeList.AxtriaSalesIQTM__Employee_ID__c)){
                    eventTypeDate=mapEmployee2EventTypeDate.get(newEmployeeList.AxtriaSalesIQTM__Employee_ID__c);
                }
                else{
                    system.debug('==newEmployeeList===='+newEmployeeList);
                    eventTypeDate=system.today();
                }
                system.debug('==newEmployeeList=='+newEmployeeList);
                AxtriaSalesIQTM__Employee_Status__c newEmpStatus = new AxtriaSalesIQTM__Employee_Status__c();
                AxtriaSalesIQTM__Employee_Status__c oldEmpStatus ;
                if(newEmployeeList.getsObjects('AxtriaSalesIQTM__Employee_Status__r')!=null){
                    oldEmpStatus = new AxtriaSalesIQTM__Employee_Status__c(Id=(string)newEmployeeList.getsObjects('AxtriaSalesIQTM__Employee_Status__r')[0].id);
                    oldEmpStatus.AxtriaSalesIQTM__Effective_End_Date__c = eventTypeDate.addDays(-1);
                    lstEmpStatus.add(oldEmpStatus);
                }
                
                newEmpStatus.AxtriaSalesIQTM__Employee__c               = newEmployeeList.id;
                newEmpStatus.AxtriaSalesIQTM__Employee_Status__c        = newEmployeeList.AxtriaSalesIQTM__HR_Status__c;
                //newEmpStatus.Employee_Status__c        = empStagingMap.get(newEmployeeList.AxtriaSalesIQTM__Employee_ID__c).AssignmentStatusValue__c;
                system.debug('==newemployeelist=='+newEmployeeList.AxtriaSalesIQTM__HR_Status__c);
                if(oldEmpStatus!=null && oldEmpStatus.AxtriaSalesIQTM__Employee__r!=null){
                    newEmpStatus.AxtriaSalesIQTM__Position__c               = oldEmpStatus.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Current_Territory__c;
                }
                newEmpStatus.AxtriaSalesIQTM__Effective_Start_Date__c   = eventTypeDate;
                newEmpStatus.AxtriaSalesIQTM__Effective_End_Date__c     = Date.newinstance(4000,12,31); 
                //if(empStagingMap.get(newEmployeeList.AxtriaSalesIQTM__Employee_ID__c).AssignmentStatusValue__c=='Terminated'){
                    //newEmpStatus.LOA_Reason__c=empStagingMap.get(newEmployeeList.AxtriaSalesIQTM__Employee_ID__c).JobChangeReason__c;
                //}               
                lstEmpStatus.add(newEmpStatus);
            }
                upsert lstEmpStatus;
        }
    }
    
    global void execute(Database.BatchableContext BC, list<CurrentPersonFeed__c> ScopeEmpStagingList)
    {
        CreateMaps(ScopeEmpStagingList);
    }
    
    global void finish(Database.BatchableContext BC)
    { 
        
    }
}