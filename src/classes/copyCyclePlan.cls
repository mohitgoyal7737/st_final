global with sharing class copyCyclePlan implements Database.Batchable<sObject>, Database.Stateful
 {
    public String query;
    public String sourceTeamInstance;
    List<temp_Obj__c> stagingList = new List<temp_Obj__c>();
    public String destinationTeamInstance;
    List<AxtriaSalesIQTM__Team_Instance__c> teamInst;
    public String teamInstanceName;
    Set<String> prodName;
    global copyCyclePlan(String teaminstance, String destTeamInstance)
    {    
        sourceTeamInstance = teaminstance;
        destinationTeamInstance = destTeamInstance;

        query= 'SELECT P1__c, Adoption__c,Potential__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__Account__r.AccountNumber,Final_TCF__c,Final_TCF_Approved__c,Segment__c,Previous_Calls__c,Previous_Segment__c,Segment_Approved__c FROM AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Team_Instance__c = :sourceTeamInstance and  AxtriaSalesIQTM__lastApprovedTarget__c=true';
    }

    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scope ) 
   
    {
      stagingList = new List<temp_Obj__c>();
      //destinationTeamInstance=  destTeamInstance;
        prodName = new Set<String>();
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c product :scope)
        {
            prodName.add(product.P1__c);
        }
       SnTDMLSecurityUtil.printDebugMessage('++++product+++'+prodName);

      List<AxtriaSalesIQTM__Team_Instance__c> teamInst = [Select Id,Name from AxtriaSalesIQTM__Team_Instance__c where id =: destinationTeamInstance];
      List<Product_Catalog__c> prod = [Select Name, Veeva_External_ID__c, Product_Code__c,Id,Team_Instance__c FROM Product_Catalog__c where Name in : prodName and Team_Instance__c =: sourceTeamInstance and IsActive__c=true];

      SnTDMLSecurityUtil.printDebugMessage('++++teamInst+++'+teamInst);
      SnTDMLSecurityUtil.printDebugMessage('++++product+++'+prod);
      Map<String,String> allProdMap = new Map<String,String>();

      for(Product_Catalog__c pc : prod)
      {
          allProdMap.put(pc.Name.toUpperCase(), pc.Veeva_External_ID__c);
      }

      teamInstanceName = teamInst[0].Name;
      SnTDMLSecurityUtil.printDebugMessage('+++++ P1__c '+ allProdMap);
      SnTDMLSecurityUtil.printDebugMessage('++++teamInst+++'+teamInstanceName);
      for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp: scope) 
        { 
          temp_Obj__c cp = new temp_Obj__c();
          cp.AccountNumber__c = pacp.AxtriaSalesIQTM__Account__r.AccountNumber;
          cp.Adoption__c = pacp.Adoption__c;
          cp.Potential__c = pacp.Potential__c;
          cp.Territory_ID__c =pacp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
          cp.Target__c = true ;
          cp.Team_ID__c= teamInst[0].name;
          cp.Cycle__c = teamInst[0].name;
          cp.Object__c = 'Call_Plan__c';
          if( pacp.Segment_Approved__c == null)
          {
            pacp.Segment_Approved__c= 'Non Target';
          }
          cp.Segment__c = pacp.Segment_Approved__c;
          if(pacp.Final_TCF_Approved__c == null )
          {
            pacp.Final_TCF_Approved__c = 0;
          }

          cp.Objective__c = String.valueof(pacp.Final_TCF_Approved__c);
          cp.Previous_Cycle_Calls__c= pacp.Previous_Calls__c;
          cp.Previous_Cycle_Segment__c = pacp.Previous_Segment__c;
          SnTDMLSecurityUtil.printDebugMessage('+++++ P1__c '+ pacp.P1__c);

          cp.Product_Code__c = allProdMap.get(pacp.P1__c.toUpperCase());
          //cp.Product_ID__c =prod[0].Product_Code__c;

          stagingList.add(cp);
        } 
         SnTDMLSecurityUtil.printDebugMessage('++++stagingList+++'+stagingList);
        if(!stagingList.isEmpty())
        {
           //insert stagingList;
           SnTDMLSecurityUtil.insertRecords(stagingList, 'copyCyclePlan');
        }
    } 

    global void finish(Database.BatchableContext BC) 
    {  
        if(!test.isRunningTest()){
           PACP_ErrorFilter_Utility pacUtil = new PACP_ErrorFilter_Utility(teamInstanceName);
           database.executeBatch(pacUtil,2000);
        }

    }
}