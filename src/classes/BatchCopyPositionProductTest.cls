/**********************************************************************************************
@author       : SnT Team
@modifiedBy   : Himanshu Tariyal (A0994)
@modifiedDate : 10th June'2020
@description  : Test class for covering BatchCopyPositionProduct class
@Revison(s)   : v1.0
**********************************************************************************************/
@isTest
public class BatchCopyPositionProductTest 
{
    @istest 
    static void BatchCopyPositionProductTest()
    {
    	String className = 'BatchCopyPositionProductTest';
        User loggedInUser = new User(id=UserInfo.getUserId());

        Account acc= TestDataFactory.createAccount();
        acc.AxtriaSalesIQTM__AccountType__c ='HCP';
        SnTDMLSecurityUtil.insertRecords(acc,className);
        //insert acc;

        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactory.createOrganizationMaster();
        SnTDMLSecurityUtil.insertRecords(orgmas,className);
        //insert orgmas;

        AxtriaSalesIQTM__Country__c countr = TestDataFactory.createCountry(orgmas);
        SnTDMLSecurityUtil.insertRecords(countr,className);
        //insert countr;

        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(countr);
        team.Name = 'ONCO';
        SnTDMLSecurityUtil.insertRecords(team,className);
        //insert team;

        AxtriaSalesIQTM__Team_Instance__c teamins1 = TestDataFactory.createTeamInstance(team);
        SnTDMLSecurityUtil.insertRecords(teamins1,className);
        //insert teamins1;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        SnTDMLSecurityUtil.insertRecords(workspace,className);
        //insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactory.newcreateScenario(teamins1, team, workspace);
        SnTDMLSecurityUtil.insertRecords(scen,className);
        //insert scen;
        
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactory.createTeamInstance(team);
        teamins.AxtriaSalesIQTM__Scenario__c = scen.id;
        SnTDMLSecurityUtil.insertRecords(teamins,className);
        //insert teamins;

        AxtriaSalesIQTM__Team_Instance__c teamins2 = TestDataFactory.createTeamInstance(team);
        teamins2.AxtriaSalesIQTM__Scenario__c = scen.id;
        SnTDMLSecurityUtil.insertRecords(teamins2,className);
        //insert teamins2;

        Product_Catalog__c pcc = TestDataFactory.productCatalog(team, teamins, countr);
        SnTDMLSecurityUtil.insertRecords(pcc,className);
        //insert pcc;
        Product_Catalog__c pcc1 = TestDataFactory.productCatalog(team, teamins2, countr);
        SnTDMLSecurityUtil.insertRecords(pcc1,className);
        //insert pcc1;

        AxtriaSalesIQTM__Position__c pos= TestDataFactory.createPosition(team,teamins);
        SnTDMLSecurityUtil.insertRecords(pos,className);
        //insert pos;

        AxtriaSalesIQTM__Position_Product__c posproduct = TestDataFactory.createPositionProduct(teamins,pos,pcc);
        SnTDMLSecurityUtil.insertRecords(posproduct,className);
        //insert posproduct;

        Test.startTest();

        System.runAs(loggedInUser)
        {
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));

            //CopyPositionProduct obj = new CopyPositionProduct(teamins.id,teamins2.id);
            

            String cols = 'Id,Name,Calls_Day__c,Holidays__c,isHolidayOverridden__c,Other_Days_Off__c,'+
		                    'AxtriaSalesIQTM__Effective_Start_Date__c,Product_Catalog__r.Name,Vacation_Days__c,'+
		                    'AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Position__c,'+
		                    'Product_Catalog__c,AxtriaSalesIQTM__isActive__c,AxtriaSalesIQTM__Product_Weight__c,'+
		                    'AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c';
		    BatchCopyPositionProduct obj = new BatchCopyPositionProduct(teamins.id,teamins2.id,cols);
		    Database.executeBatch(obj);
        }

        Test.stopTest();
    }
}