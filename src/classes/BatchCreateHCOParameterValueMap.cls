global with sharing class BatchCreateHCOParameterValueMap implements Database.Batchable<sObject>,Database.Stateful {

    public String sourceRuleId;
    public String destinationRuleId;
    public String query;
    global Map<String,HCPParamWrapperClass> hcpParamNameToFieldMap;
    public Map<String,Map<Integer,String>> hcpToMapOfHCOMap;

    public Map<String,Map<String,Decimal>> hcoToMapOfParamValMap;

    public Integer maxLevel;

    public Boolean executeFlag;

    //Added by Chirag Ahuja for STIMPS-286
    public boolean HCOFlag = false;
    public String teamInstance;
    public String type;

    /*Integer counterHCP;
    Integer counterHCO;
    Integer counter,counter2,counter3;*/

    Set<String> alreadyHCPs;

    public class HCPParamWrapperClass
    {
        String hcoParameterName;
        Integer levelOfAffiliation;
        String hcpFieldName;

        HCPParamWrapperClass(String hcoParamName,Integer levAff, String hcpField)
        {
            hcoParameterName = hcoParamName;
            levelOfAffiliation = levAff;
            hcpFieldName = hcpField;
        }
    }

    public Integer minVal(Integer a, Integer b)
    {
        return a < b ? a : b;
    }

    public void createParamToFieldMap(Account_Compute_Final__c acf)
    {
        if(acf != null)
        {
            executeFlag = true;
            for(HCO_Parameters__c hcoPar : [Select Id,Display_Name__c,Sequence__c,Source_Rule_Parameter__c,Source_Rule_Parameter__r.Parameter_Name__c,Affiliation_Level__c from HCO_Parameters__c where Source_Rule__c =:sourceRuleId and Destination_Rule__c =: destinationRuleId WITH SECURITY_ENFORCED])
            {
                for(Integer i = 1;i <= 50; i++)
                {
                    system.debug('(String)acf.get(\'Output_Name_\'+i+\'__c\') =====>>'+(String)acf.get('Output_Name_'+i+'__c'));
                    system.debug('hcoPar.Source_Rule_Parameter__r.Parameter_Name__c =====>>'+hcoPar.Source_Rule_Parameter__r.Parameter_Name__c);
                    if((String)acf.get('Output_Name_'+i+'__c') == hcoPar.Source_Rule_Parameter__r.Parameter_Name__c)
                    {
                        hcpParamNameToFieldMap.put(hcoPar.Source_Rule_Parameter__r.Parameter_Name__c,new HCPParamWrapperClass(hcoPar.Display_Name__c,Integer.valueOf(hcoPar.Affiliation_Level__c),'Output_Value_'+i+'__c'));
                        if(Integer.valueOf(hcoPar.Affiliation_Level__c) > maxLevel)
                        {
                            maxLevel = Integer.valueOf(hcoPar.Affiliation_Level__c);
                        }
                    }
                }
            }
        }
    }

    global BatchCreateHCOParameterValueMap(String sourceRuleId,String destinationRuleId)
    {

        hcpToMapOfHCOMap = new Map<String,Map<Integer,String>>();
        hcoToMapOfParamValMap = new Map<String,Map<String,Decimal>>();

        hcpParamNameToFieldMap = new Map<String,HCPParamWrapperClass>();
        this.sourceRuleId = sourceRuleId;
        this.destinationRuleId = destinationRuleId;
        maxLevel = -9999;
        executeFlag = false;
        /*counterHCP = 0;
        counterHCO = 0;
        counter = 0;
        counter2 = 0; 
        counter3 = 0;*/ 

        alreadyHCPs = new Set<String>();

        query = 'Select Id,Measure_Master__c,Physician_2__c,Measure_Master__r.Brand_Lookup__r.Product_Code__c,Measure_Master__r.Brand_Lookup__r.Name,Physician__c,Physician__r.AxtriaSalesIQTM__Account__r.AccountNumber, ';
        query += ' Physician__r.AxtriaSalesIQTM__Account__c,Physician__r.Position_Code__c, Measure_Master__r.Team__r.Name, Measure_Master__r.Team_Instance__r.Name,';
        query += ' Output_Name_1__c,Output_Name_2__c,Output_Name_3__c,Output_Name_4__c,Output_Name_5__c,Output_Name_6__c, ';
        query += ' Output_Name_7__c,Output_Name_8__c,Output_Name_9__c,Output_Name_10__c,Output_Name_11__c,Output_Name_12__c, ';
        query += ' Output_Name_13__c,Output_Name_14__c,Output_Name_15__c,Output_Name_16__c,Output_Name_17__c,Output_Name_18__c, ';
        query += ' Output_Name_19__c,Output_Name_20__c,Output_Name_21__c,Output_Name_22__c,Output_Name_23__c,Output_Name_24__c, ';
        query += ' Output_Name_25__c,Output_Name_26__c,Output_Name_27__c,Output_Name_28__c,Output_Name_29__c,Output_Name_30__c, ';
        query += ' Output_Name_31__c,Output_Name_32__c,Output_Name_33__c,Output_Name_34__c,Output_Name_35__c,Output_Name_36__c, ';
        query += ' Output_Name_37__c,Output_Name_38__c,Output_Name_39__c,Output_Name_40__c,Output_Name_41__c,Output_Name_42__c, ';
        query += ' Output_Name_43__c,Output_Name_44__c,Output_Name_45__c,Output_Name_46__c,Output_Name_47__c,Output_Name_48__c, ';
        query += ' Output_Name_49__c,Output_Name_50__c,Output_Value_1__c,Output_Value_2__c,Output_Value_3__c,Output_Value_4__c,Output_Value_5__c,Output_Value_6__c, ';
        query += ' Output_Value_7__c,Output_Value_8__c,Output_Value_9__c,Output_Value_10__c,Output_Value_11__c,Output_Value_12__c, ';
        query += ' Output_Value_13__c,Output_Value_14__c,Output_Value_15__c,Output_Value_16__c,Output_Value_17__c,Output_Value_18__c, ';
        query += ' Output_Value_19__c,Output_Value_20__c,Output_Value_21__c,Output_Value_22__c,Output_Value_23__c,Output_Value_24__c, ';
        query += ' Output_Value_25__c,Output_Value_26__c,Output_Value_27__c,Output_Value_28__c,Output_Value_29__c,Output_Value_30__c, ';
        query += ' Output_Value_31__c,Output_Value_32__c,Output_Value_33__c,Output_Value_34__c,Output_Value_35__c,Output_Value_36__c, ';
        query += ' Output_Value_37__c,Output_Value_38__c,Output_Value_39__c,Output_Value_40__c,Output_Value_41__c,Output_Value_42__c, ';
        query += ' Output_Value_43__c,Output_Value_44__c,Output_Value_45__c,Output_Value_46__c,Output_Value_47__c,Output_Value_48__c, ';
        query += ' Output_Value_49__c,Output_Value_50__c from Account_Compute_Final__c where Measure_Master__c =: sourceRuleId WITH SECURITY_ENFORCED';
        this.query = query;
    }

    //Added by Chirag Ahuja for STIMPS-286
    global BatchCreateHCOParameterValueMap(String sourceRuleId,String destinationRuleId, String teamInstance, String type, Boolean flag)
    {

        hcpToMapOfHCOMap = new Map<String,Map<Integer,String>>();
        hcoToMapOfParamValMap = new Map<String,Map<String,Decimal>>();

        hcpParamNameToFieldMap = new Map<String,HCPParamWrapperClass>();
        this.sourceRuleId = sourceRuleId;
        this.destinationRuleId = destinationRuleId;
        maxLevel = -9999;
        executeFlag = false;
        this.teamInstance = teamInstance;
        this.type = type;
        this.HCOFlag = flag;
        /*counterHCP = 0;
        counterHCO = 0;
        counter = 0;
        counter2 = 0; 
        counter3 = 0;*/ 

        alreadyHCPs = new Set<String>();

        query = 'Select Id,Measure_Master__c,Physician_2__c,Measure_Master__r.Brand_Lookup__r.Product_Code__c,Measure_Master__r.Brand_Lookup__r.Name,Physician__c,Physician__r.AxtriaSalesIQTM__Account__r.AccountNumber, ';
        query += ' Physician__r.AxtriaSalesIQTM__Account__c,Physician__r.Position_Code__c, Measure_Master__r.Team__r.Name, Measure_Master__r.Team_Instance__r.Name,';
        query += ' Output_Name_1__c,Output_Name_2__c,Output_Name_3__c,Output_Name_4__c,Output_Name_5__c,Output_Name_6__c, ';
        query += ' Output_Name_7__c,Output_Name_8__c,Output_Name_9__c,Output_Name_10__c,Output_Name_11__c,Output_Name_12__c, ';
        query += ' Output_Name_13__c,Output_Name_14__c,Output_Name_15__c,Output_Name_16__c,Output_Name_17__c,Output_Name_18__c, ';
        query += ' Output_Name_19__c,Output_Name_20__c,Output_Name_21__c,Output_Name_22__c,Output_Name_23__c,Output_Name_24__c, ';
        query += ' Output_Name_25__c,Output_Name_26__c,Output_Name_27__c,Output_Name_28__c,Output_Name_29__c,Output_Name_30__c, ';
        query += ' Output_Name_31__c,Output_Name_32__c,Output_Name_33__c,Output_Name_34__c,Output_Name_35__c,Output_Name_36__c, ';
        query += ' Output_Name_37__c,Output_Name_38__c,Output_Name_39__c,Output_Name_40__c,Output_Name_41__c,Output_Name_42__c, ';
        query += ' Output_Name_43__c,Output_Name_44__c,Output_Name_45__c,Output_Name_46__c,Output_Name_47__c,Output_Name_48__c, ';
        query += ' Output_Name_49__c,Output_Name_50__c,Output_Value_1__c,Output_Value_2__c,Output_Value_3__c,Output_Value_4__c,Output_Value_5__c,Output_Value_6__c, ';
        query += ' Output_Value_7__c,Output_Value_8__c,Output_Value_9__c,Output_Value_10__c,Output_Value_11__c,Output_Value_12__c, ';
        query += ' Output_Value_13__c,Output_Value_14__c,Output_Value_15__c,Output_Value_16__c,Output_Value_17__c,Output_Value_18__c, ';
        query += ' Output_Value_19__c,Output_Value_20__c,Output_Value_21__c,Output_Value_22__c,Output_Value_23__c,Output_Value_24__c, ';
        query += ' Output_Value_25__c,Output_Value_26__c,Output_Value_27__c,Output_Value_28__c,Output_Value_29__c,Output_Value_30__c, ';
        query += ' Output_Value_31__c,Output_Value_32__c,Output_Value_33__c,Output_Value_34__c,Output_Value_35__c,Output_Value_36__c, ';
        query += ' Output_Value_37__c,Output_Value_38__c,Output_Value_39__c,Output_Value_40__c,Output_Value_41__c,Output_Value_42__c, ';
        query += ' Output_Value_43__c,Output_Value_44__c,Output_Value_45__c,Output_Value_46__c,Output_Value_47__c,Output_Value_48__c, ';
        query += ' Output_Value_49__c,Output_Value_50__c from Account_Compute_Final__c where Measure_Master__c =: sourceRuleId WITH SECURITY_ENFORCED';
        this.query = query;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Account_Compute_Final__c> scope) 
    {
        hcpToMapOfHCOMap = new Map<String,Map<Integer,String>>();

        if(!executeFlag){
            createParamToFieldMap(scope[0]);
        }
        Set<String> hcpList = new Set<String>();
        for(Account_Compute_Final__c acf : scope)
        {
            //hcpList.add(acf.Physician__r.AxtriaSalesIQTM__Account__c);
            hcpList.add(acf.Physician_2__c);
        }
        //counterHCP = hcpList.size();
        //System.debug('counterHCP before set ---> ' + counterHCP);
        //List<Aggregateresult> hcpAffCount = [Select AxtriaSalesIQTM__Account__c,count(AxtriaSalesIQTM__Parent_Account__c) from AxtriaSalesIQTM__Account_Affiliation__c where AxtriaSalesIQTM__Account__c in: hcpList group by AxtriaSalesIQTM__Account__c];
        
        

        // Old version of Affiliation Map

        /*for(AxtriaSalesIQTM__Account_Affiliation__c accAff : [Select AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Parent_Account__c,BU_Rank__c from AxtriaSalesIQTM__Account_Affiliation__c where AxtriaSalesIQTM__Account__c in: hcpList order by AxtriaSalesIQTM__Account__c,BU_Rank__c])//and BU_Rank__c <= :maxLevel
        {
            Map<Integer,String> tempMap = new Map<Integer,String>();
            if(hcpToMapOfHCOMap.containsKey(accAff.AxtriaSalesIQTM__Account__c))
            {
                tempMap = hcpToMapOfHCOMap.get(accAff.AxtriaSalesIQTM__Account__c);
            }
            tempMap.put(Integer.valueOf(accAff.BU_Rank__c),accAff.AxtriaSalesIQTM__Parent_Account__c);
            hcpToMapOfHCOMap.put(accAff.AxtriaSalesIQTM__Account__c,tempMap);
        }*/

        // New Affiliation Map which ignores skip in BU_Rank
        system.debug('hcpList'+hcpList);
        Integer tempRank;
        for(AxtriaSalesIQTM__Account_Affiliation__c accAff : [Select AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Parent_Account__c,BU_Rank__c from AxtriaSalesIQTM__Account_Affiliation__c where AxtriaSalesIQTM__Account__c in: hcpList and AxtriaSalesIQTM__Active__c = true and BU_Rank__c != null  WITH SECURITY_ENFORCED order by AxtriaSalesIQTM__Account__c,BU_Rank__c])//and BU_Rank__c <= :maxLevel
        {
            system.debug('accAff==>'+accAff);
            Map<Integer,String> tempMap = new Map<Integer,String>();
            if(!hcpToMapOfHCOMap.containsKey(accAff.AxtriaSalesIQTM__Account__c))
            {
                tempMap.put(1,accAff.AxtriaSalesIQTM__Parent_Account__c);
            }
            else{
                tempMap = hcpToMapOfHCOMap.get(accAff.AxtriaSalesIQTM__Account__c);
                tempRank = tempMap.keySet().size() + 1;
                tempMap.put(tempRank,accAff.AxtriaSalesIQTM__Parent_Account__c);
            }
            hcpToMapOfHCOMap.put(accAff.AxtriaSalesIQTM__Account__c,tempMap);
        }

        Integer divisor;
        Decimal valAfterDiv;
        String tempStr;

        for(Account_Compute_Final__c acf : scope)
        {
            if(hcpToMapOfHCOMap.containsKey(acf.Physician_2__c) && !alreadyHCPs.contains(acf.Physician_2__c))
            {
                Map<Integer,String> hcoMap = hcpToMapOfHCOMap.get(acf.Physician_2__c);
                system.debug('hcpParamNameToFieldMap.keySet()====>'+hcpParamNameToFieldMap.keySet());
                for(String hcpParam : hcpParamNameToFieldMap.keySet())
                {
                    HCPParamWrapperClass hcpWrap = hcpParamNameToFieldMap.get(hcpParam);
                    divisor = minVal(hcoMap.keySet().size(),hcpWrap.levelOfAffiliation);
                    System.debug('divisor ---> '+divisor);
                    System.debug('Field --> ' +hcpWrap.hcpFieldName);
                    System.debug('acf --> ' + acf);
                    System.debug('acf value ---> '+acf.get(hcpWrap.hcpFieldName));
                    tempStr = (String)acf.get(hcpWrap.hcpFieldName);
                    tempStr = tempStr == '' || tempStr == null ? '0' : tempStr;
                    valAfterDiv = Decimal.valueOf((String)acf.get(hcpWrap.hcpFieldName))/divisor;

                    for(Integer i : hcoMap.keySet())
                    {
                        //counter3++;
                        Map<String,Decimal> tempMap;
                        if(i <= divisor)
                        {
                            //counter2++;
                            if(!hcoToMapOfParamValMap.containsKey(hcoMap.get(i)))
                            {
                                tempMap = new Map<String,Decimal>();
                                tempMap.put(hcpWrap.hcoParameterName,valAfterDiv);
                                //counter++;
                            }
                            else
                            {
                                tempMap = new Map<String,Decimal>();
                                tempMap = hcoToMapOfParamValMap.get(hcoMap.get(i));
                                if(!tempMap.containsKey(hcpWrap.hcoParameterName))
                                {
                                    tempMap.put(hcpWrap.hcoParameterName,valAfterDiv);
                                }
                                else
                                {
                                    Decimal d = tempMap.get(hcpWrap.hcoParameterName);
                                    tempMap.put(hcpWrap.hcoParameterName,d+valAfterDiv);
                                }
                            }
                            hcoToMapOfParamValMap.put(hcoMap.get(i),tempMap);
                        }
                        /*else{
                            System.debug('HCP ---> ' + acf.Physician__r.AxtriaSalesIQTM__Account__c);
                            System.debug('HCO ---> ' + hcoMap.get(i));
                            System.debug('BU Rank ---> ' + i);
                            System.debug('divisor ---> ' + divisor);
                        }*/
                    }

                }
                alreadyHCPs.add(acf.Physician_2__c);
            }
        }
        System.debug('size of hcpToMapOfHCOMap --> ' + hcpToMapOfHCOMap.keySet().size());
        System.debug('size of hcoToMapOfParamValMap --> ' + hcoToMapOfParamValMap.keySet().size());
        /*counterHCO = hcoToMapOfParamValMap.keySet().size();
        System.debug('counterHCO ---> ' + counterHCO);
        System.debug('counterHCP after set ---> ' + counterHCP);
        System.debug('counter of HCOs in map ---> ' + counter);
        System.debug('counter of aff of HCPs used for HCOP seg---> ' + counter2);
        System.debug('counter of aff of HCPs total ---> ' + counter3);*/

    }

    global void finish(Database.BatchableContext BC) 
    {
        //Integer countAff = 0;
        /*for(String s : hcpToMapOfHCOMap.keySet())
        {
           //System.debug('HCP Id --> ' + s);
           for(Integer str : hcpToMapOfHCOMap.get(s).keySet())
           {
               //System.debug('BU rank --> ' + str);
               //System.debug('HCO --> ' + hcpToMapOfHCOMap.get(s).get(str));
               //countAff++;
           }
        }*/

        /*for(String s : hcoToMapOfParamValMap.keySet())
        {
           System.debug('HCO Id --> ' + s);
           for(String str : hcoToMapOfParamValMap.get(s).keySet())
           {
               System.debug('HCO Parameter --> ' + str);
               System.debug('HCO Value for Parameter --> ' + hcoToMapOfParamValMap.get(s).get(str));
           }
        }*/
        //System.debug('counter of Affiliations ---> ' + countAff);
        

        /*System.debug('counter of HCOs in map ---> ' + counter);
        System.debug('counter of aff of HCPs used for HCOP seg---> ' + counter2);
        System.debug('counter of aff of HCPs total ---> ' + counter3);*/


        /*System.debug('size of HCPs  --> ' + counterHCP);
        System.debug('size of HCOs  --> ' + counterHCO);*/
        if(executeFlag)
        {
            //Updated by Chirag Ahuja for STIMPS-286
            if(HCOFlag){
                Database.executeBatch(new CreateHCOSegBatch(sourceRuleId,destinationRuleId,hcoToMapOfParamValMap,teamInstance,type,HCOFlag),2000);
            }
            else{
                Database.executeBatch(new CreateHCOSegBatch(sourceRuleId,destinationRuleId,hcoToMapOfParamValMap),2000);
            }

        }
    }
}