public with sharing class ScenarioTriggerHandler{



 public static boolean checkDuplicateWorkspaceName(Id userPersistanceCountryId, Id workspaceCountryId, String workspaceName,Date workspaceStartDate, Date workspaceEndDate){
        Boolean isDuplicate = false;
        system.debug('userPersistanceCountryId ::::::::  '+userPersistanceCountryId);
        system.debug('workspaceCountryId ::::::::  '+workspaceCountryId);
        system.debug('workspaceName ::::::::  '+workspaceName);
        id countryId = ( workspaceCountryId != null ) ? workspaceCountryId : userPersistanceCountryId;
        List<AxtriaSalesIQTM__Workspace__c> workspaceList = [select id,AxtriaSalesIQTM__Workspace_Start_Date__c,AxtriaSalesIQTM__Workspace_End_Date__c, Name from AxtriaSalesIQTM__Workspace__c where AxtriaSalesIQTM__Country__c = :countryId];
        system.debug('workspaceList ::::::::  '+workspaceList);

        for(AxtriaSalesIQTM__Workspace__c sold : workspaceList)
      //checking workspace ovealapping condition
        if( workspaceList != null && workspaceList.size() > 0){
            //string name = String.valueOf(workspaceList[0].Name).toLowerCase();
            //Date StartDate = workspaceList[0].AxtriaSalesIQTM__Workspace_Start_Date__c;
            //Date EndDate = workspaceList[0].AxtriaSalesIQTM__Workspace_End_Date__c;
            //system.debug('name ::::::::  '+name);
            
            if((sold.AxtriaSalesIQTM__Workspace_Start_Date__c<=workspaceStartDate) && (workspaceStartDate<=sold.AxtriaSalesIQTM__Workspace_End_Date__c) ||
            (sold.AxtriaSalesIQTM__Workspace_Start_Date__c<=workspaceEndDate) && (workspaceEndDate<=sold.AxtriaSalesIQTM__Workspace_Start_Date__c) ||
            (sold.AxtriaSalesIQTM__Workspace_Start_Date__c>=workspaceStartDate && sold.AxtriaSalesIQTM__Workspace_End_Date__c<=workspaceEndDate)){
                isDuplicate = true;
            }
        }
        return isDuplicate;
    }  


}