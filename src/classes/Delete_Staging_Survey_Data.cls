//Author Chirag Ahuja
global with sharing class Delete_Staging_Survey_Data implements Database.Batchable<sObject> {
    public String query;
    public String teamInstance;
    public String type;
    public String product;
    public boolean HCOFlag = false;
    public Boolean proceedBatch = true;
    String selectedSourceRule;
    String selectedDestinationRule;

    global Delete_Staging_Survey_Data(String sourceRuleId, String destinationRuleId,String teamInstance, String prod, String type, boolean flag)
    {
        this.selectedSourceRule = sourceRuleId;
        this.selectedDestinationRule = destinationRuleId;
        this.teamInstance = teamInstance;
        this.product = prod;
        this.type = type;
        this.HCOFlag = flag;
        SnTDMLSecurityUtil.printDebugMessage('teamInstance===?'+teamInstance+'==type===?'+type+'==product===?'+product+'selectedSourceRule===?'+selectedSourceRule+'==selectedDestinationRule===?'+selectedDestinationRule);
        
        query = 'SELECT Id FROM Staging_Survey_Data__c WHERE Team_Instance__c = \'' + teamInstance + '\' and Type__c = \'' + type + '\' and Product_Code__c = \'' + product + '\' WITH SECURITY_ENFORCED';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        SnTDMLSecurityUtil.printDebugMessage('query-->' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Sobject> scope) {
        SnTDMLSecurityUtil.printDebugMessage('scope size-->' + scope.size());
        if(scope.size() > 0 && scope != null)
        {
            if(Staging_Survey_Data__c.sObjectType.getDescribe().isDeletable())
            {
                Database.DeleteResult[] srList = Database.delete(scope, false);
                Database.EmptyRecycleBinResult[] emptyRecycleBinResults = DataBase.emptyRecycleBin(scope);
            }
            else
            {
                proceedBatch = false;
                SnTDMLSecurityUtil.printDebugMessage('You dont have permission to delete BU_Response__c', 'Delete_BU_Response');
            }
        }
        
    }

    global void finish(Database.BatchableContext BC) {
        if(proceedBatch){
            if(HCOFlag){
                Delete_Metadata_Definition batch = new Delete_Metadata_Definition(selectedSourceRule,selectedDestinationRule,teamInstance,product,type,HCOFlag);
                DataBase.executeBatch(batch,500);
            }
        }
    }
}