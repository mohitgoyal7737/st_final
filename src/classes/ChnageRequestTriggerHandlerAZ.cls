public with sharing class ChnageRequestTriggerHandlerAZ
{
    public static void createCRCallPlan(List<AxtriaSalesIQTM__Change_Request__c> changeRequestList)
    {
        SnTDMLSecurityUtil.printDebugMessage('---entered create CR callplan func');
        AxtriaSalesIQTM__Change_Request__c CR;
        for(AxtriaSalesIQTM__Change_Request__c objCR : changeRequestList ) CR = objCR;
        ID crID = CR.ID;
        List<String> accChanged = new List<String>();
        if(String.isNotBlank(CR.AxtriaSalesIQTM__Account_Moved_Id__c))
        {
            accChanged = CR.AxtriaSalesIQTM__Account_Moved_Id__c.split(',');
        }
        ID destPos = CR.AxtriaSalesIQTM__Destination_Position__c;
        id teamInstanceID = CR.AxtriaSalesIQTM__Team_Instance_ID__c;

        List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> allChangedRec = new list <AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        if(!accChanged.isEmpty())
        {
            string soql = 'select Segment_Approved__c,Segment__c,AxtriaSalesIQTM__Metric1__c,AxtriaSalesIQTM__Metric1_Approved__c,AxtriaSalesIQTM__Metric1_Updated__c, Adoption__c, Potential__c, AxtriaSalesIQTM__lastApprovedTarget__c,TCF_Change__c,AxtriaSalesIQTM__Account__r.ID,			AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__External_Account_Number__c,AxtriaSalesIQTM__isAccountTarget__c,AxtriaSalesIQTM__Account__r.Name,AxtriaSalesIQTM__Metric3__c,AxtriaSalesIQTM__Metric3_Approved__c,Final_TCF__c,AxtriaSalesIQTM__Metric3_Updated__c,AxtriaSalesIQTM__Picklist1_Updated__c,AxtriaSalesIQTM__Picklist1_Segment_Approved__c,AxtriaSalesIQTM__Picklist3_Updated__c,AxtriaSalesIQTM__Picklist3_Segment_Approved__c,AxtriaSalesIQTM__Segment7__c,AxtriaSalesIQTM__Picklist2_Updated__c,AxtriaSalesIQTM__Picklist2_Segment_Approved__c,AxtriaSalesIQTM__Change_Status__c,AxtriaSalesIQTM__isIncludedCallPlan__c,AxtriaSalesIQTM__Segment2__c,AxtriaSalesIQTM__Segment8__c,Final_TCF_Original__c,Final_TCF_Approved__c,P1__c,Segment1__c,Segment2__c,Segment3__c,Segment4__c,Segment5__c,AxtriaSalesIQTM__Segment1_Approved__c,AxtriaSalesIQTM__Segment1_Updated__c,AxtriaSalesIQTM__Segment2_Approved__c,AxtriaSalesIQTM__Segment2_Updated__c,AxtriaSalesIQTM__Segment3_Approved__c,AxtriaSalesIQTM__Segment3_Updated__c,AxtriaSalesIQTM__Segment4_Approved__c,AxtriaSalesIQTM__Segment4_Updated__c,AxtriaSalesIQTM__Segment5_Approved__c,AxtriaSalesIQTM__Segment5_Updated__c,AxtriaSalesIQTM__Metric2__c,AxtriaSalesIQTM__Metric2_Approved__c,AxtriaSalesIQTM__Metric2_Updated__c,AxtriaSalesIQTM__Metric4__c,AxtriaSalesIQTM__Metric4_Approved__c,AxtriaSalesIQTM__Metric4_Updated__c,AxtriaSalesIQTM__Metric5__c,AxtriaSalesIQTM__Metric5_Approved__c,AxtriaSalesIQTM__Metric5_Updated__c,AxtriaSalesIQTM__Metric6__c,AxtriaSalesIQTM__Metric6_Approved__c,AxtriaSalesIQTM__Metric6_Updated__c,AxtriaSalesIQTM__Metric7__c,AxtriaSalesIQTM__Metric7_Approved__c,AxtriaSalesIQTM__Metric7_Updated__c,AxtriaSalesIQTM__Metric8__c,AxtriaSalesIQTM__Metric8_Approved__c,AxtriaSalesIQTM__Metric8_Updated__c,AxtriaSalesIQTM__Metric9__c,AxtriaSalesIQTM__Metric9_Approved__c,AxtriaSalesIQTM__Metric9_Updated__c,AxtriaSalesIQTM__Metric10__c,AxtriaSalesIQTM__Metric10_Approved__c,AxtriaSalesIQTM__Metric10_Updated__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where id in :accChanged and AxtriaSalesIQTM__Position__c =:destPos and AxtriaSalesIQTM__Team_Instance__c =:teamInstanceID';

            allChangedRec = Database.query(soql);

            List<AxtriaSalesIQTM__CR_Call_Plan__c> crCallPlanRequests = new List<AxtriaSalesIQTM__CR_Call_Plan__c>();
            AxtriaSalesIQTM__CR_Call_Plan__c tempCRCallPlan;

            for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : allChangedRec)
            {
                tempCRCallPlan = new AxtriaSalesIQTM__CR_Call_Plan__c();
                tempCRCallPlan.AxtriaSalesIQTM__Change_Request_ID__c = crID;
                tempCRCallPlan.name = 'CR-' + SalesIQUtility.GenerateRandomNumber();
                tempCRCallPlan.TCF_Change_Reason__c = pacp.TCF_Change__c;
                // if(pacp.Final_TCF_Approved__c != (pacp.Final_TCF__c)){
                tempCRCallPlan.AxtriaSalesIQTM__Rep_Goal_Before__c = (pacp.AxtriaSalesIQTM__Picklist1_Segment_Approved__c);
                tempCRCallPlan.AxtriaSalesIQTM__Rep_Goal_After__c = pacp.AxtriaSalesIQTM__Picklist1_Updated__c;
                //Field has been updated due to field_length issue  (Modified by A1942).
                //tempCRCallPlan.AxtriaSalesIQTM__Original_Reason_Code__c = pacp.AxtriaSalesIQTM__Picklist3_Segment_Approved__c;
                tempCRCallPlan.Original_Reason_Code1__c = pacp.AxtriaSalesIQTM__Picklist3_Segment_Approved__c;

                tempCRCallPlan.AxtriaSalesIQTM__Updated_Reason_Code__c = pacp.AxtriaSalesIQTM__Picklist3_Updated__c;
                // tempCRCallPlan.AxtriaSalesIQTM__isIncludedCallPlan__c = pacp.AxtriaSalesIQTM__isIncludedCallPlan__c;
                tempCRCallPlan.Updated_Calls__c = pacp.Final_TCF__c;
                tempCRCallPlan.Original_Calls__c = pacp.Final_TCF_Original__c;
                tempCRCallPlan.AxtriaSalesIQTM__Before_Segment1__c = pacp.Segment_Approved__c;
                tempCRCallPlan.AxtriaSalesIQTM__After_Segment1__c = pacp.Segment__c;

                //SNT-304
                tempCRCallPlan.AxtriaSalesIQTM__Before_Segment2__c = pacp.AxtriaSalesIQTM__Segment2_Approved__c;
                tempCRCallPlan.AxtriaSalesIQTM__After_Segment2__c = pacp.AxtriaSalesIQTM__Segment2_Updated__c;

                tempCRCallPlan.AxtriaSalesIQTM__Before_Segment3__c = pacp.AxtriaSalesIQTM__Segment3_Approved__c;
                tempCRCallPlan.AxtriaSalesIQTM__After_Segment3__c = pacp.AxtriaSalesIQTM__Segment3_Updated__c;

                tempCRCallPlan.AxtriaSalesIQTM__Before_Segment4__c = pacp.AxtriaSalesIQTM__Segment4_Approved__c;
                tempCRCallPlan.AxtriaSalesIQTM__After_Segment4__c = pacp.AxtriaSalesIQTM__Segment4_Updated__c;

                tempCRCallPlan.AxtriaSalesIQTM__Before_Segment5__c = pacp.AxtriaSalesIQTM__Segment5_Approved__c;
                tempCRCallPlan.AxtriaSalesIQTM__After_Segment5__c = pacp.AxtriaSalesIQTM__Segment5_Updated__c;

                tempCRCallPlan.AxtriaSalesIQTM__Before_Segment10__c = pacp.AxtriaSalesIQTM__Segment1_Approved__c;
                tempCRCallPlan.AxtriaSalesIQTM__After_Segment10__c = pacp.AxtriaSalesIQTM__Segment1_Updated__c;
                //SNT-304

                //Added to store PDF values
                tempCRCallPlan.AxtriaSalesIQTM__Before_Metric1__c = pacp.AxtriaSalesIQTM__Metric1__c;
                tempCRCallPlan.AxtriaSalesIQTM__After_Metric1__c = pacp.AxtriaSalesIQTM__Metric1_Updated__c;
                //end PDF
				//Added to store multichannel fields
				tempCRCallPlan.AxtriaSalesIQTM__Before_Metric2__c = pacp.AxtriaSalesIQTM__Metric2__c;
                tempCRCallPlan.AxtriaSalesIQTM__After_Metric2__c = pacp.AxtriaSalesIQTM__Metric2_Updated__c;
				tempCRCallPlan.AxtriaSalesIQTM__Before_Metric3__c = pacp.AxtriaSalesIQTM__Metric3__c;
                tempCRCallPlan.AxtriaSalesIQTM__After_Metric3__c = pacp.AxtriaSalesIQTM__Metric3_Updated__c;
				tempCRCallPlan.AxtriaSalesIQTM__Before_Metric4__c = pacp.AxtriaSalesIQTM__Metric4__c;
                tempCRCallPlan.AxtriaSalesIQTM__After_Metric4__c = pacp.AxtriaSalesIQTM__Metric4_Updated__c;
				tempCRCallPlan.AxtriaSalesIQTM__Before_Metric5__c = pacp.AxtriaSalesIQTM__Metric5__c;
                tempCRCallPlan.AxtriaSalesIQTM__After_Metric5__c = pacp.AxtriaSalesIQTM__Metric5_Updated__c;
				tempCRCallPlan.AxtriaSalesIQTM__Before_Metric6__c = pacp.AxtriaSalesIQTM__Metric6__c;
                tempCRCallPlan.AxtriaSalesIQTM__After_Metric6__c = pacp.AxtriaSalesIQTM__Metric6_Updated__c;
				tempCRCallPlan.AxtriaSalesIQTM__Before_Metric7__c = pacp.AxtriaSalesIQTM__Metric7__c;
                tempCRCallPlan.AxtriaSalesIQTM__After_Metric7__c = pacp.AxtriaSalesIQTM__Metric7_Updated__c;
				tempCRCallPlan.AxtriaSalesIQTM__Before_Metric8__c = pacp.AxtriaSalesIQTM__Metric8__c;
                tempCRCallPlan.AxtriaSalesIQTM__After_Metric8__c = pacp.AxtriaSalesIQTM__Metric8_Updated__c;
				tempCRCallPlan.AxtriaSalesIQTM__Before_Metric9__c = pacp.AxtriaSalesIQTM__Metric9__c;
                tempCRCallPlan.AxtriaSalesIQTM__After_Metric9__c = pacp.AxtriaSalesIQTM__Metric9_Updated__c;
				tempCRCallPlan.AxtriaSalesIQTM__Before_Metric10__c = pacp.AxtriaSalesIQTM__Metric10__c;
                tempCRCallPlan.AxtriaSalesIQTM__After_Metric10__c = pacp.AxtriaSalesIQTM__Metric10_Updated__c;
				
                tempCRCallPlan.Product__c = pacp.P1__c;
                if(pacp.Adoption__c != null)
                    tempCRCallPlan.Adoption__c = pacp.Adoption__c;

                if(pacp.Potential__c != null)
                    tempCRCallPlan.Potential__c = pacp.Potential__c;

                // }
                if(tempCRCallPlan.AxtriaSalesIQTM__Updated_Reason_Code__c == 'None')
                {
                    tempCRCallPlan.AxtriaSalesIQTM__Updated_Reason_Code__c = '';
                }

                tempCRCallPlan.AxtriaSalesIQTM__isIncludedCallPlan__c = pacp.AxtriaSalesIQTM__isIncludedCallPlan__c;
                tempCRCallPlan.AxtriaSalesIQTM__isExcludedCallPlan__c = pacp.AxtriaSalesIQTM__isAccountTarget__c;
                tempCRCallPlan.lastApprovedTarget__c = pacp.AxtriaSalesIQTM__lastApprovedTarget__c; // -- Added for STIMPS-384 ---

                if((tempCRCallPlan.AxtriaSalesIQTM__isIncludedCallPlan__c == true && tempCRCallPlan.AxtriaSalesIQTM__isExcludedCallPlan__c == true) && (tempCRCallPlan.Original_Calls__c == tempCRCallPlan.Updated_Calls__c))
                {
                    tempCRCallPlan.AxtriaSalesIQTM__Updated_Reason_Code__c = '';
                }



               // -- Sequence of if blocks changed and updated to 'else if' instead of ifs for STIMPS-384 on 26th Aug,2020 --
                 /* Also comparing Original target flag in case of Add/Drop Target in place of Last approved target flag*/
               // if(tempCRCallPlan.AxtriaSalesIQTM__isIncludedCallPlan__c == true && pacp.AxtriaSalesIQTM__lastApprovedTarget__c == false)
                if(tempCRCallPlan.AxtriaSalesIQTM__isIncludedCallPlan__c == true && pacp.AxtriaSalesIQTM__isAccountTarget__c == false){
                    tempCRCallPlan.change__c = 'Added';
                }

                // if(tempCRCallPlan.AxtriaSalesIQTM__isIncludedCallPlan__c == false && pacp.AxtriaSalesIQTM__lastApprovedTarget__c == true)
                else if(tempCRCallPlan.AxtriaSalesIQTM__isIncludedCallPlan__c == false && pacp.AxtriaSalesIQTM__isAccountTarget__c == true){
                    tempCRCallPlan.change__c = 'Dropped';
                }
                else if(tempCRCallPlan.AxtriaSalesIQTM__isIncludedCallPlan__c == true && (pacp.Segment_Approved__c == pacp.Segment__c) && (pacp.Final_TCF_Approved__c != pacp.Final_TCF__c)){
                    tempCRCallPlan.change__c = 'TCF Change';
                }
                else if(tempCRCallPlan.AxtriaSalesIQTM__isIncludedCallPlan__c == true && (pacp.Segment_Approved__c != pacp.Segment__c)){
                    tempCRCallPlan.change__c = 'Segment Change';
                }
                // -- STIMPS-384 ends here --

                tempCRCallPlan.AxtriaSalesIQTM__Status__c = SalesIQGlobalConstants.REQUEST_STATUS_PENDING;
                tempCRCallPlan.AxtriaSalesIQTM__Account__c = pacp.AxtriaSalesIQTM__Account__r.ID;
                crCallPlanRequests.add(tempCRCallPlan);
            }


            if(crCallPlanRequests != null && crCallPlanRequests.size() > 0)
            {


                //insert crCallPlanRequests;
                SnTDMLSecurityUtil.insertRecords(crCallPlanRequests, 'ChnageRequestTriggerHandlerAZ');

            }

            if(CR.AxtriaSalesIQTM__Status__c == SalesIQGlobalConstants.REQUEST_STATUS_APPROVED)
            {
                updatePositionAccountCallPlan(changeRequestList);
            }
        }
    }

    /**
    * This method updates Position Account Call Plan after Call Plan Request is Approved / Rejected
    * @param List<AxtriaSalesIQTM__Change_Request__c> list of all Change Requests
    */
    public static void updatePositionAccountCallPlan(List<AxtriaSalesIQTM__Change_Request__c> changeRequestList)
    {
        /*Assumptions -
         In Bulk CR all CRs will have same status
        There will be only one CR for each Position*/

        SnTDMLSecurityUtil.printDebugMessage('---entered update PACP func');
        AxtriaSalesIQTM__Change_Request__c CR;
        Set<String> set_ti = new Set<String>();
        Set<String> set_cr = new Set<String>();
        Set<String> set_destPos = new Set<String>();
        Set<String> accChanged = new Set<String>();
        Set<String> tempset = new Set<String>();
        Map<String, Set<String>> mapPos_Acc = new Map<String, Set<String>>();
        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();
        List<AxtriaSalesIQTM__Position__c> list_pos = new List<AxtriaSalesIQTM__Position__c>();
        List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> list_pacp_upadate = new List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        for(AxtriaSalesIQTM__Change_Request__c objCR : changeRequestList )
        {
            set_ti.add(objCR.AxtriaSalesIQTM__Team_Instance_ID__c);
            set_cr.add(objCR.Id);
            if(objCR.AxtriaSalesIQTM__Account_Moved_Id__c != null)
            {
                accChanged.addAll(objCR.AxtriaSalesIQTM__Account_Moved_Id__c.split(','));
                tempset = new Set<String>();
                tempset.addAll(objCR.AxtriaSalesIQTM__Account_Moved_Id__c.split(','));
                mapPos_Acc.put(objCR.AxtriaSalesIQTM__Destination_Position__c, tempset );
            }


            if(!set_destPos.contains(objCR.AxtriaSalesIQTM__Destination_Position__c))
            {
                pos = new AxtriaSalesIQTM__Position__c();
                pos.Id = objCR.AxtriaSalesIQTM__Destination_Position__c;
                if(objCR.AxtriaSalesIQTM__Status__c == SalesIQGlobalConstants.REQUEST_STATUS_APPROVED || objCR.AxtriaSalesIQTM__Status__c == 'Approvato')
                {
                    pos.Call_Plan_Status__c = 'Approved';
                }
                else if(objCR.AxtriaSalesIQTM__Status__c ==  SalesIQGlobalConstants.REQUEST_STATUS_REJECTED || objCR.AxtriaSalesIQTM__Status__c ==  SalesIQGlobalConstants.REQUEST_STATUS_RECALLED || objCR.AxtriaSalesIQTM__Status__c == SalesIQGlobalConstants.REQUEST_STATUS_CANCELLED || objCR.AxtriaSalesIQTM__Status__c == 'Respinto')
                {
                    pos.Call_Plan_Status__c = 'Pending for Submission';
                }
                list_pos.add(pos);
            }
            set_destPos.add(objCR.AxtriaSalesIQTM__Destination_Position__c);

        }
        if(list_pos.size() > 0){
            //update list_pos;
            SnTDMLSecurityUtil.updateRecords(list_pos, 'ChnageRequestTriggerHandlerAZ');
        }

        List<AxtriaSalesIQTM__CR_Call_Plan__c> list_CR_CP =  new List<AxtriaSalesIQTM__CR_Call_Plan__c>();
        list_CR_CP = [Select AxtriaSalesIQTM__Account__c, AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__External_Account_Number__c, AxtriaSalesIQTM__isIncludedCallPlan__c, AxtriaSalesIQTM__Updated_Reason_Code__c, AxtriaSalesIQTM__Rep_Goal_After__c, AxtriaSalesIQTM__Change_Request_ID__r.AxtriaSalesIQTM__Status__c from AxtriaSalesIQTM__CR_Call_Plan__c where AxtriaSalesIQTM__Change_Request_ID__c in :set_cr];

        for(AxtriaSalesIQTM__CR_Call_Plan__c crcp : list_CR_CP)
        {
            crcp.AxtriaSalesIQTM__Status__c = crcp.AxtriaSalesIQTM__Change_Request_ID__r.AxtriaSalesIQTM__Status__c;
        }
        if(list_CR_CP.size() > 0){
            //update list_CR_CP;
            SnTDMLSecurityUtil.updateRecords(list_CR_CP, 'ChnageRequestTriggerHandlerAZ');
        }



        List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> list_pacp = new List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();

        list_pacp = [Select AxtriaSalesIQTM__Position__r.Call_Plan_Status__c, Parameter1__c, Parameter2__c, Parameter3__c, Parameter4__c, Parameter5__c, Parameter6__c, Parameter7__c, Parameter8__c, Segment_Approved__c, Segment__c, AxtriaSalesIQTM__Account__r.ID, AxtriaSalesIQTM__lastApprovedTarget__c, AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__External_Account_Number__c, AxtriaSalesIQTM__Account__r.Name, AxtriaSalesIQTM__Metric3__c, AxtriaSalesIQTM__Metric3_Approved__c, AxtriaSalesIQTM__Picklist1_Updated__c, AxtriaSalesIQTM__Picklist1_Segment__c, AxtriaSalesIQTM__Picklist1_Segment_Approved__c, AxtriaSalesIQTM__Picklist3_Updated__c, AxtriaSalesIQTM__Picklist3_Segment_Approved__c, AxtriaSalesIQTM__Picklist2_Updated__c, AxtriaSalesIQTM__Picklist2_Segment_Approved__c, AxtriaSalesIQTM__Picklist2_Segment__c, AxtriaSalesIQTM__Change_Status__c, AxtriaSalesIQTM__isIncludedCallPlan__c, Final_TCF__c, Final_TCF_Approved__c, isFinalTCFApproved__c, AxtriaSalesIQTM__Metric1_Updated__c, AxtriaSalesIQTM__Metric2_Updated__c, AxtriaSalesIQTM__Metric3_Updated__c, AxtriaSalesIQTM__Metric4_Updated__c, AxtriaSalesIQTM__Metric5_Updated__c,AxtriaSalesIQTM__Metric6_Updated__c, AxtriaSalesIQTM__Metric7_Updated__c,AxtriaSalesIQTM__Metric8_Updated__c,AxtriaSalesIQTM__Metric9_Updated__c,AxtriaSalesIQTM__Metric10_Updated__c,AxtriaSalesIQTM__Segment1_Updated__c, AxtriaSalesIQTM__Segment2_Updated__c, AxtriaSalesIQTM__Segment3_Updated__c, AxtriaSalesIQTM__Segment4_Updated__c, AxtriaSalesIQTM__Segment5_Updated__c,AxtriaSalesIQTM__Segment6_Updated__c,AxtriaSalesIQTM__Segment7_Updated__c,AxtriaSalesIQTM__Segment8_Updated__c,AxtriaSalesIQTM__Segment9_Updated__c,AxtriaSalesIQTM__Segment10_Updated__c,AxtriaSalesIQTM__Metric1_Approved__c,AxtriaSalesIQTM__Metric2_Approved__c,AxtriaSalesIQTM__Metric4_Approved__c,AxtriaSalesIQTM__Metric5_Approved__c,AxtriaSalesIQTM__Metric6_Approved__c,AxtriaSalesIQTM__Metric7_Approved__c,AxtriaSalesIQTM__Metric8_Approved__c,AxtriaSalesIQTM__Metric9_Approved__c,AxtriaSalesIQTM__Metric10_Approved__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where id in :accChanged and AxtriaSalesIQTM__Position__c in :set_destPos and AxtriaSalesIQTM__Team_Instance__c in :set_ti];
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : list_pacp)
        {
            if(mapPos_Acc.containsKey(pacp.AxtriaSalesIQTM__Position__c) && (mapPos_Acc.get(pacp.AxtriaSalesIQTM__Position__c)).contains(pacp.ID))
            {
                if(changeRequestList[0].AxtriaSalesIQTM__Status__c == SalesIQGlobalConstants.REQUEST_STATUS_APPROVED || changeRequestList[0].AxtriaSalesIQTM__Status__c == 'Approvato')
                {
                    SnTDMLSecurityUtil.printDebugMessage('---inside approved state--');
                    pacp.AxtriaSalesIQTM__Picklist3_Segment_Approved__c  =  pacp.AxtriaSalesIQTM__Picklist3_Updated__c ;
                    pacp.AxtriaSalesIQTM__Picklist1_Segment_Approved__c  =  pacp.AxtriaSalesIQTM__Picklist1_Updated__c ;
                    pacp.AxtriaSalesIQTM__Change_Status__c    =    SalesIQGlobalConstants.REQUEST_STATUS_APPROVED;
                    pacp.AxtriaSalesIQTM__Metric3_Approved__c =    pacp.AxtriaSalesIQTM__Metric3_Updated__c;
                    pacp.AxtriaSalesIQTM__Picklist2_Segment_Approved__c =  pacp.AxtriaSalesIQTM__Picklist2_Updated__c;
                    pacp.Parameter1_Approved__c = pacp.Parameter1__c;
                    pacp.Parameter2_Approved__c = pacp.Parameter2__c;
                    pacp.Parameter3_Approved__c = pacp.Parameter3__c;
                    pacp.Parameter4_Approved__c = pacp.Parameter4__c;
                    pacp.Parameter5_Approved__c = pacp.Parameter5__c;
                    pacp.Parameter6_Approved__c = pacp.Parameter6__c;
                    pacp.Parameter7_Approved__c = pacp.Parameter7__c;
                    pacp.Parameter8_Approved__c = pacp.Parameter8__c;
                    pacp.Final_TCF_Approved__c = pacp.Final_TCF__c;
                    pacp.Segment_Approved__c = pacp.Segment__c;
                    pacp.isFinalTCFApproved__c = true;
                    pacp.AxtriaSalesIQTM__Metric1_Approved__c = pacp.AxtriaSalesIQTM__Metric1_Updated__c;
                    pacp.AxtriaSalesIQTM__Metric2_Approved__c = pacp.AxtriaSalesIQTM__Metric2_Updated__c;
                    pacp.AxtriaSalesIQTM__Metric3_Approved__c = pacp.AxtriaSalesIQTM__Metric3_Updated__c;
                    pacp.AxtriaSalesIQTM__Metric4_Approved__c = pacp.AxtriaSalesIQTM__Metric4_Updated__c;
                    pacp.AxtriaSalesIQTM__Metric5_Approved__c = pacp.AxtriaSalesIQTM__Metric5_Updated__c;
					pacp.AxtriaSalesIQTM__Metric6_Approved__c = pacp.AxtriaSalesIQTM__Metric6_Updated__c;
                    pacp.AxtriaSalesIQTM__Metric7_Approved__c = pacp.AxtriaSalesIQTM__Metric7_Updated__c;
                    pacp.AxtriaSalesIQTM__Metric8_Approved__c = pacp.AxtriaSalesIQTM__Metric8_Updated__c;
                    pacp.AxtriaSalesIQTM__Metric9_Approved__c = pacp.AxtriaSalesIQTM__Metric9_Updated__c;
                    pacp.AxtriaSalesIQTM__Metric10_Approved__c = pacp.AxtriaSalesIQTM__Metric10_Updated__c;
                    //START SNT-304
                    pacp.AxtriaSalesIQTM__Segment1_Approved__c = pacp.AxtriaSalesIQTM__Segment1_Updated__c;
                    pacp.AxtriaSalesIQTM__Segment2_Approved__c = pacp.AxtriaSalesIQTM__Segment2_Updated__c;
                    pacp.AxtriaSalesIQTM__Segment3_Approved__c = pacp.AxtriaSalesIQTM__Segment3_Updated__c;
                    pacp.AxtriaSalesIQTM__Segment4_Approved__c = pacp.AxtriaSalesIQTM__Segment4_Updated__c;
                    pacp.AxtriaSalesIQTM__Segment5_Approved__c = pacp.AxtriaSalesIQTM__Segment5_Updated__c;
                    //END SNT-304
                    pacp.AxtriaSalesIQTM__lastApprovedTarget__c = pacp.AxtriaSalesIQTM__isIncludedCallPlan__c;
                }
                else if(changeRequestList[0].AxtriaSalesIQTM__Status__c ==  SalesIQGlobalConstants.REQUEST_STATUS_REJECTED || changeRequestList[0].AxtriaSalesIQTM__Status__c ==  SalesIQGlobalConstants.REQUEST_STATUS_RECALLED || changeRequestList[0].AxtriaSalesIQTM__Status__c == SalesIQGlobalConstants.REQUEST_STATUS_CANCELLED || changeRequestList[0].AxtriaSalesIQTM__Status__c == 'Respinto')
                {

                    pacp.AxtriaSalesIQTM__Change_Status__c = 'Pending for Submission';
                }
                list_pacp_upadate.add(pacp);
            }

        }
        if(list_pacp_upadate.size() > 0){
            //update list_pacp_upadate;
            SnTDMLSecurityUtil.updateRecords(list_pacp_upadate, 'ChnageRequestTriggerHandlerAZ');
        }

        updateMetricSummaryTable(changeRequestList[0].AxtriaSalesIQTM__Status__c, set_destPos, set_ti);



    }

    /**
    * This method updates metric Summary Table after call plan request is Approved or Rejected
    * @param String :- Request is approved or rejected
    * @param string Position ID
    */

    public static void updateMetricSummaryTable(String type, Set<String> posID, Set<String> teamInstance)
    {
        Set<ID> allCIMids = new Set<ID>();
        List<AxtriaSalesIQTM__CIM_Config__c> cimConfigRecs = new list <AxtriaSalesIQTM__CIM_Config__c>();
        String cimQuery = 'select id from AxtriaSalesIQTM__CIM_Config__c where AxtriaSalesIQTM__Team_Instance__c in :teamInstance and AxtriaSalesIQTM__Change_Request_Type__r.AxtriaSalesIQTM__CR_Type_Name__c = \'Call_Plan_Change\'';

        cimConfigRecs = Database.query(cimQuery);

        for(AxtriaSalesIQTM__CIM_Config__c ccm : cimConfigRecs)
        {
            allCIMids.add(ccm.ID);
        }

        List<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c> cimPosMetricRecs = new List<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c>();
        String fetchAllPosMetric = 'select AxtriaSalesIQTM__Proposed__c, AxtriaSalesIQTM__Approved__c,AxtriaSalesIQTM__CIM_Config__r.Name from AxtriaSalesIQTM__CIM_Position_Metric_Summary__c where AxtriaSalesIQTM__CIM_Config__c in :allCIMids and AxtriaSalesIQTM__Position_Team_Instance__r.AxtriaSalesIQTM__Position_ID__c in :posID ';

        cimPosMetricRecs = Database.query(fetchAllPosMetric);

        if(type == SalesIQGlobalConstants.REQUEST_STATUS_APPROVED)
        {
            for(AxtriaSalesIQTM__CIM_Position_Metric_Summary__c cpms : cimPosMetricRecs)
            {
                cpms.AxtriaSalesIQTM__Approved__c = cpms.AxtriaSalesIQTM__Proposed__c;
            }

            if(cimPosMetricRecs.size() > 0)
               // update cimPosMetricRecs;
            SnTDMLSecurityUtil.updateRecords(cimPosMetricRecs, 'ChnageRequestTriggerHandlerAZ');
        }


    }

}