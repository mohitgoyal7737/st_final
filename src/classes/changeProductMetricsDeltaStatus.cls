global class changeProductMetricsDeltaStatus implements Database.Batchable<sObject> {
    public String query;
    List<String> teamInstanceSelected;
    String queryString;
    public List<string> teaminstancelistis;
    public Boolean flag = true;
    public Date lmd;
    public Boolean chain = false;
    Set<String> allCountries;

    global changeProductMetricsDeltaStatus(List<String> teamInstanceSelectedTemp)
    { 
        allCountries = new Set<String>();
        for(AxtriaSalesIQTM__Team_Instance__c ti : [select AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c, AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Team_Instance__c  where id in :teamInstanceSelectedTemp ])
        {
            allCountries.add(ti.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c);
        }
        queryString = 'select id, SIQ_Status__c from SIQ_Product_Metrics_vod_O__c where SIQ_Status__c = \'Updated\' and SIQ_Country__c in :allCountries';// where Team_Instance__c in :allTeamInstances
        teamInstanceSelected = teamInstanceSelectedTemp;
    }

    global changeProductMetricsDeltaStatus(List<String> teamInstanceSelectedTemp, Boolean chaining)
    { 
      chain = chaining;
      allCountries = new Set<String>();

      for(AxtriaSalesIQTM__Team_Instance__c ti : [select AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__c, AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c from AxtriaSalesIQTM__Team_Instance__c  where id in :teamInstanceSelectedTemp ])
      {
            allCountries.add(ti.AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Country_Code__c);
      }
       chaining = chain;
       queryString = 'select id, SIQ_Status__c from SIQ_Product_Metrics_vod_O__c where SIQ_Status__c = \'Updated\' and SIQ_Country__c in :allCountries';// where Team_Instance__c in :allTeamInstances
      teamInstanceSelected = teamInstanceSelectedTemp;
        
    }

    global changeProductMetricsDeltaStatus()
    { 
       queryString = 'select id, SIQ_Status__c from SIQ_Product_Metrics_vod_O__c and SIQ_Country__c in :allCountries';// where Team_Instance__c in :allTeamInstances  
    }

    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        return Database.getQueryLocator(queryString);
    }

   global void execute(Database.BatchableContext BC, List<SIQ_Product_Metrics_vod_O__c> scopePacpProRecs)
    {
        for(SIQ_Product_Metrics_vod_O__c prodmet : scopePacpProRecs)
        {
            prodmet.SIQ_Status__c = '';
        }        
        update scopePacpProRecs;
    }

    global void finish(Database.BatchableContext BC)
    {
          
      BatchDeltaProductMetrics u2 = new BatchDeltaProductMetrics(lmd,teamInstanceSelected,chain);
      database.executeBatch(u2,2000);
    }
    global void execute(System.SchedulableContext SC)
    {
        teaminstancelistis = new list<string>();
        teaminstancelistis = StaticTeaminstanceList.filldata();
        database.executeBatch(new changeProductMetricsDeltaStatus(teaminstancelistis));
    } 
}