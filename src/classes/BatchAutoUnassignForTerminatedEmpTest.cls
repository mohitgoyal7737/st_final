@isTest
private class BatchAutoUnassignForTerminatedEmpTest {
	
	 @testSetup 
	 
    static void setup() {
    	Map<id,list<AxtriaSalesIQTM__Position_Employee__c>> empIdPosEmpListMap=new Map<id,list<AxtriaSalesIQTM__Position_Employee__c>>();
        List<AxtriaSalesIQTM__Employee__c> empList=TestDataFactoryAZ.createEmployees('x','x','x','Retired','x',System.today(),5);
        insert empList;
       List<AxtriaSalesIQTM__Team__c> teamList= TestDataFactoryAZ.createTeam();
       List<AxtriaSalesIQTM__Team_Instance__c> teamIns=TestDataFactoryAZ.createTeamInstance(teamList) ;
       List<AxtriaSalesIQTM__Position__c> posList=TestDataFactoryAZ.createPositions('a',5,'CAEOS',teamIns);
       insert posList;
      List<AxtriaSalesIQTM__Position_Employee__c> posEmpList=TestDataFactoryAZ.createPositionEmployees(empList,posList);
     insert posEmpList;
       for(AxtriaSalesIQTM__Position_Employee__c posEmp : posEmpList){
            if(!empIdPosEmpListMap.containsKey(posEmp.AxtriaSalesIQTM__Employee__r.id)){
                empIdPosEmpListMap.put(posEmp.AxtriaSalesIQTM__Employee__r.id,new list<AxtriaSalesIQTM__Position_Employee__c>());    
            }
            empIdPosEmpListMap.get(posEmp.AxtriaSalesIQTM__Employee__r.id).add(posEmp);
        }
    }
    static testmethod void test() { 
    	Scheduler_Log__c sJob = new Scheduler_Log__c();
		
		sJob.Job_Name__c = 'Employee Feed';
		sJob.Job_Status__c = 'Failed';
		sJob.Job_Type__c='Inbound';
		//sJob.CreatedDate = System.today();
	
		insert sJob;
	   
	    String batchID = sJob.Id; 
        Test.startTest();
        BatchAutoUnassignForTerminatedEmp usa = new BatchAutoUnassignForTerminatedEmp(batchID);
        Id batchId1 = Database.executeBatch(usa);
        Test.stopTest();
        // after the testing stops, assert records were updated 
    }
}