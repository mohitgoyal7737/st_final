public class CountWrapper{
        @AuraEnabled public Integer terminate_Employee           {get;set;}
        @AuraEnabled public Integer leave_of_Absence             {get;set;}
        @AuraEnabled public Integer promote_Employee             {get;set;}
        @AuraEnabled public Integer EmployeeNewHire              {get;set;}
        @AuraEnabled public Integer TransferEmployee             {get;set;}
        @AuraEnabled public Integer Employeedemotion             {get;set;}
        @AuraEnabled public Integer Transfer_to_HO               {get;set;}
        @AuraEnabled public Integer TransfertoField              {get;set;}
        @AuraEnabled public Integer TransferoutOfSales           {get;set;}
        @AuraEnabled public Integer allEvents                    {get;set;}
        @AuraEnabled public Integer rehireCount                  {get;set;}
}