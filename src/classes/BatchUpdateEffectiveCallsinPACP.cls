global class BatchUpdateEffectiveCallsinPACP implements Database.Batchable<sObject> {
    public String query;
    public String team_instance_id = '';
    public List<AxtriaSalesIQTM__Team_Instance__c> team_instance;
    public List<Veeva_Channels__mdt> VeevaMetadata;
    public List<String> channels = new List<String>();
    public List<String> weights = new List<String>();
    public String delimiter = ',';
    public String channel_call ='0';
    public Decimal weight=1.00, channel_call_decimal = 0.00, effective_calls = 0.00;

    global BatchUpdateEffectiveCallsinPACP(string selectedteaminstance) {

        team_instance_id = selectedteaminstance;
        
        query = 'Select id,AxtriaSalesIQTM__Account__c,Final_TCF__c,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Metric1_Updated__c,AxtriaSalesIQTM__Metric2_Updated__c,AxtriaSalesIQTM__Metric3_Updated__c, AxtriaSalesIQTM__Metric4_Updated__c, AxtriaSalesIQTM__Metric5_Updated__c, AxtriaSalesIQTM__Metric6_Updated__c, Effective_Calls__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Team_Instance__c= :team_instance_id';
        
        SnTDMLSecurityUtil.printDebugMessage('=======The Query is:'+query);
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scope) {
        try{
            team_instance = new List<AxtriaSalesIQTM__Team_Instance__c>();
            team_instance = [Select ID, Name, Country_Name__c, MCCP_Enabled__c, AxtriaSalesIQTM__Team__r.AxtriaSalesIQTM__Country__r.MCCP_Enabled__c, Channel_Weights__c, Metric_Mapping__c from AxtriaSalesIQTM__Team_Instance__c where id = :team_instance_id WITH SECURITY_ENFORCED LIMIT 1 ];
        }
        catch(System.QueryException qe) 
        {
            SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
        }
        if(team_instance[0].MCCP_Enabled__c){
            try{
                VeevaMetadata = new List<Veeva_Channels__mdt>();
                VeevaMetadata = [Select Channels__c, Team_Instance_Name__c, Metric_Mapping__c, Workload_Equivalent__c FROM Veeva_Channels__mdt where Team_Instance_Name__c =:team_instance[0].Name and Channels__c != null and Workload_Equivalent__c !=null and Metric_Mapping__c !=null LIMIT 1];
            }
            catch(System.QueryException qe) 
            {
                SnTDMLSecurityUtil.printDebugMessage(qe.getMessage());
            }
            if(VeevaMetadata != null && VeevaMetadata[0].Metric_Mapping__c != null){
                channels = VeevaMetadata[0].Metric_Mapping__c.split(delimiter);
                SnTDMLSecurityUtil.printDebugMessage('country level channels from Veeva Data---'+channels);
            }
            
            if(team_instance != null && team_instance[0].Channel_Weights__c != null){
                SnTDMLSecurityUtil.printDebugMessage('---weights from team instance---'); 
                weights =  new List<String>();
                weights = team_instance[0].Channel_Weights__c.split(delimiter);  
            }else{
                SnTDMLSecurityUtil.printDebugMessage('---weights from Country Level-VeevaMetaData---');
                weights =  new List<String>();
                weights = VeevaMetadata[0].Workload_Equivalent__c.split(delimiter);
            }
            SnTDMLSecurityUtil.printDebugMessage('---weights---'+weights);  
            
            List<Decimal> valuesToCalculateEffCall = new List<Decimal>();
            List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> lisToUpdatePACP = new List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
            List<Decimal> effCallsForAllPacp = new List<Decimal>();

            if(weights.size()==0 || (weights.size()!=0 && channels.size()==weights.size())){
                SnTDMLSecurityUtil.printDebugMessage('Inside Update PACP');
                for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp:scope){
                    for(Integer i=0; i<channels.size();i++){

                        SnTDMLSecurityUtil.printDebugMessage('==pacp.get(channels[i].trim())=='+pacp.get(channels[i].trim()));
                        channel_call = String.valueOf(pacp.get(channels[i].trim()));
                        channel_call_decimal = convertToDecimal(channel_call,0.00);                        
                        SnTDMLSecurityUtil.printDebugMessage('==channel_call_integer=='+channel_call_decimal);
                        if(weights.size()>0){
                            weight = convertToDecimal(weights[i].trim(),1.00);
                            valuesToCalculateEffCall.add((channel_call_decimal/weight));
                        }
                        SnTDMLSecurityUtil.printDebugMessage('==weight=='+weight);
                    }
                    SnTDMLSecurityUtil.printDebugMessage('==valuesToCalculateEffCall=='+valuesToCalculateEffCall);
                    for(Integer i=0;i<valuesToCalculateEffCall.size();i++){
                        effective_calls += valuesToCalculateEffCall.get(i);
                    }
                    valuesToCalculateEffCall.clear();
                    effCallsForAllPacp.add((effective_calls).setScale(2));
                    effective_calls = 0;         
                }
                SnTDMLSecurityUtil.printDebugMessage('effCallsForAllPacp for PACP'+effCallsForAllPacp);
                SnTDMLSecurityUtil.printDebugMessage('effCallsForAllPacp for PACP size'+effCallsForAllPacp.size());
                
                if(!effCallsForAllPacp.isEmpty() || effCallsForAllPacp != Null){
                    Integer recno = 0;
                    for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp:scope){
                        pacp.Effective_Calls__c = effCallsForAllPacp.get(recno);
                        lisToUpdatePACP.add(pacp);
                        recno++;
                    }
                SnTDMLSecurityUtil.printDebugMessage('==lisToUpdatePACP=='+lisToUpdatePACP);
                if(lisToUpdatePACP.size()>0){
                    SnTDMLSecurityUtil.printDebugMessage('==Updating Records==');
                    SnTDMLSecurityUtil.updateRecords(lisToUpdatePACP, 'BatchUpdateEffectiveCallsinPACP');
                }
                }
            }
        }
    }
    public static Decimal convertToDecimal(String num, Decimal replacement){
        try{
        	return Decimal.valueOf(num);
        }
        catch(Exception e){
        	return replacement;
        }
    }
    

    global void finish(Database.BatchableContext BC) {

    }
}