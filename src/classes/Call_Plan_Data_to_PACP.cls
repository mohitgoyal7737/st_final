global class Call_Plan_Data_to_PACP implements Database.Batchable<sObject>  {
    
    global string queri;
    global string teamID;
    public String selectedTeamInstance;
     public boolean affiliationFlag; // check whether call plan is HCP level or HCp-HCO level                        
    //global string teamInstance;
    global list<AxtriaSalesIQTM__Position_Team_Instance__c> posTI=new list<AxtriaSalesIQTM__Position_Team_Instance__c>();
    global list<AxtriaSalesIQTM__Team_Instance_Account__c> accTI=new list<AxtriaSalesIQTM__Team_Instance_Account__c>();
    public AxtriaSalesIQTM__TriggerContol__c customsetting ;
    public AxtriaSalesIQTM__TriggerContol__c customsetting2 ;
   // public boolean affiliationFlag;
    public list<AxtriaSalesIQTM__TriggerContol__c>customsettinglist {get;set;}
    List<AxtriaSalesIQTM__Team_Instance__c> teamMultiChannel= new List<AxtriaSalesIQTM__Team_Instance__c>();
    public Boolean multiChannel;
    Map<String,ChannelsVeeva__c> channels;
    List<String> allchannels;
    Map<String,String> channelsmapping;

    
    
    global Call_Plan_Data_to_PACP(String teamInstance)
    {
       /* system.debug('++++teamInstance'+teamInstance);
        customsetting = new AxtriaSalesIQTM__TriggerContol__c();
        customsetting2 = new AxtriaSalesIQTM__TriggerContol__c();
        customsettinglist = new list<AxtriaSalesIQTM__TriggerContol__c>();
      
        selectedTeamInstance=teamInstance;
        System.debug('selectedTeamInstance--'+selectedTeamInstance);
        teamMultiChannel= [select id,name,AxtriaSalesIQTM__Country__c,MultiChannel__c From AxtriaSalesIQTM__Team_Instance__c where name=:selectedTeamInstance];

        if(teamMultiChannel!=null && teamMultiChannel.size()>0)
        {
          multiChannel=teamMultiChannel[0].MultiChannel__c;   
        }
        System.debug('selectedTeamInstance--'+selectedTeamInstance);
        system.debug('++multiChannel'+multiChannel);

        //Added by Sukirty 8/11/2019
        affiliationFlag = [Select Enable_Affiliation_CallPlan__c From AxtriaSalesIQTM__Team_Instance__c where name =: teamInstance].Enable_Affiliation_CallPlan__c;
        System.debug('affiliationFlag in batch2--'+affiliationFlag);
        
        queri = 'Select Id,CurrencyIsoCode,Name,Cycle__c, Account_ID__c, Parent_Account_ID__c, Team_ID__c, Territory_ID__c, Segment__c, Objective__c, Product_ID__c, Adoption__c, Potential__c, Target__c, Previous_Cycle_Calls__c, Account__c, Account__r.AccountNumber, Parent_Account__c, Parent_Account__r.AccountNumber, Position__c, Position_Account__c, Position_Team_Instance__c, Team_Instance__c,Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c,Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c, Team_Instance__r.Enable_Affiliation_CallPlan__c, isError__c, Product_Name__c,Objective_1__c,Objective_2__c,Objective_3__c,Objective_4__c,Objective_5__c, Channel_1__c,Channel_2__c,Channel_3__c,Channel_4__c,Channel_5__c FROM Call_Plan__c where Team_ID__c= \'' + teamInstance + '\' and Status__c != \'Processing\' ';
        System.debug('queri--'+queri);*/
    } //Corresponding_HCP__c,Gain_Loss_Position__c,
    
    global Database.Querylocator start(Database.BatchableContext bc){
       /* system.debug('*************************Checkpoint3');
        try{
        return Database.getQueryLocator(queri);
        }
        catch(Exception e){
        System.debug(e.getMessage()); */
        return Database.getQueryLocator(queri);
       // }
         
    } 
    
    global void execute (Database.BatchableContext BC, List<Call_Plan__c>callplnlist)
    {
        
       /* system.debug('*************************Checkpoint4');
        list<String> list1 = new list<String>();
        list<String> list2 = new list<String>();
        List<Error_Object__c> errorList = new List<Error_object__c>();
        Error_object__c tempErrorObj;
        
        Set<string> accountNumberSet = new Set<string>();
        Set<string> parentAccountNumberSet = new Set<string>();
        Set<string> posIDSet = new Set<string>();
        Set<string> brandSet = new Set<string>();
        Map<string,map<string,string>> stagingMap = new Map<string,Map<string,string>>();
        
        for(Call_Plan__c cprcd : callplnlist){
            if(!string.IsBlank(cprcd.Account__c)){            //Accounts Set
                accountNumberSet.add(cprcd.Account__c);
            }
            if(!string.IsBlank(cprcd.Parent_Account_ID__c) && affiliationFlag==true){            //Added by Sukirty-Parent Accounts Set (HCO)
                parentAccountNumberSet.add(cprcd.Parent_Account_ID__c);
            }
            if(!string.IsBlank(cprcd.Position__c)){            //Pos Set
                posIDSet.add(cprcd.Position__c);
            }
            if(!string.IsBlank(cprcd.Product_Name__c)){            //brand Set
                brandSet.add(cprcd.Product_Name__c);
            }

            Map<String,String> tempMap= new Map<String,String>();
            String key= String.valueof(cprcd.Team_Instance__c)+String.valueof(cprcd.Account__c)+String.valueof(cprcd.Position__c)+String.valueof(cprcd.Product_Name__c);
            if(cprcd.Channel_1__c!=null) {tempMap.put(cprcd.Channel_1__c, cprcd.Objective_1__c);}
            if(cprcd.Channel_2__c!=null) {tempMap.put(cprcd.Channel_2__c, cprcd.Objective_2__c);}
            if(cprcd.Channel_3__c!=null) {tempMap.put(cprcd.Channel_3__c, cprcd.Objective_3__c);}
            if(cprcd.Channel_4__c!=null) {tempMap.put(cprcd.Channel_4__c, cprcd.Objective_4__c);}
            if(cprcd.Channel_5__c!=null) {tempMap.put(cprcd.Channel_5__c, cprcd.Objective_5__c);}

            stagingMap.put(key,tempMap);
        }

        system.debug('+++stagingMap'+stagingMap);

        system.debug('============batch 2 accountNumberSet::::'+accountNumberSet);
        system.debug('============batch 2 accountNumberSet size::::'+accountNumberSet.size());
        system.debug('============batch 2 parentAccountNumberSet::::'+parentAccountNumberSet);
        system.debug('============batch 2 parentAccountNumberSet size::::'+parentAccountNumberSet.size());
        system.debug('============batch 2 posIDSet::::'+posIDSet);
        system.debug('============batch 2 posIDSet size::::'+posIDSet.size());
        system.debug('============batch 2 brandSet::::'+brandSet);
        system.debug('============batch 2 brandSet size::::'+brandSet.size());
        
        List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacpListToCheck = new List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();

        pacpListToCheck = [Select id,AxtriaSalesIQTM__Team_Instance__c,AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Position__c,Party_ID__c,P1__c,AxtriaSalesIQTM__Metric1__c,AxtriaSalesIQTM__Metric2__c,AxtriaSalesIQTM__Metric3__c,AxtriaSalesIQTM__Metric4__c,AxtriaSalesIQTM__Metric5__c,AxtriaSalesIQTM__Metric1_Updated__c,AxtriaSalesIQTM__Metric2_Updated__c,AxtriaSalesIQTM__Metric3_Updated__c,AxtriaSalesIQTM__Metric4_Updated__c,AxtriaSalesIQTM__Metric5_Updated__c,AxtriaSalesIQTM__Metric1_Approved__c,AxtriaSalesIQTM__Metric2_Approved__c,AxtriaSalesIQTM__Metric3_Approved__c,AxtriaSalesIQTM__Metric4_Approved__c,AxtriaSalesIQTM__Metric5_Approved__c  from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Team_Instance__r.name = :selectedTeamInstance and AxtriaSalesIQTM__Account__c in :accountNumberSet and AxtriaSalesIQTM__Position__c in :posIDSet and P1__c in :brandSet and (AxtriaSalesIQTM__isIncludedCallPlan__c = true or AxtriaSalesIQTM__lastApprovedTarget__c = true)];// limit 30000
        Set<String> pacpSet = new Set<String>();
        
        System.debug('pacpListToCheck size--'+pacpListToCheck.size());
        System.debug('pacpListToCheck--'+pacpListToCheck);

        if(parentAccountNumberSet.size()>0) {  //To include HCO in key Added by Sukirty (8/11/2019)
            for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp :pacpListToCheck){
                pacpSet.add(String.valueof(pacp.AxtriaSalesIQTM__Team_Instance__c)+String.valueof(pacp.AxtriaSalesIQTM__Account__c)+String.valueof(pacp.Parent_Account__c)+String.valueof(pacp.AxtriaSalesIQTM__Position__c)+String.valueof(pacp.P1__c));
            }
        }
        else 
        {
            for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp :pacpListToCheck)
            {
                pacpSet.add(String.valueof(pacp.AxtriaSalesIQTM__Team_Instance__c)+String.valueof(pacp.AxtriaSalesIQTM__Account__c)+String.valueof(pacp.AxtriaSalesIQTM__Position__c)+String.valueof(pacp.P1__c));
            }
        }
        
        System.debug('pacpSet---'+pacpSet);
        System.debug('pacpSet size---'+pacpSet.size());
        system.debug('before callplnlist ' + callplnlist);
        system.debug('before callplnlist size--' + callplnlist.size());

        
        map<String,Call_Plan__c> Mac = new Map<String,Call_Plan__c>(); // Added by Dhiren---SAL 308********************************
        map<String,String> keymapid = new map<String,String>(); //Added by Dhiren---SAL 308********************************

        List<String> Accids = new List<String>(); // Added by Dhiren---SAL 308********************************

        String CycleName = [Select Cycle__r.Name From AxtriaSalesIQTM__Team_Instance__c where Name =: selectedTeamInstance].Cycle__r.Name;
        System.debug('CycleName--'+CycleName);

        channels= ChannelsVeeva__c.getAll();
        allchannels=new list<String>();
        channelsmapping=new Map<String,String>();
        if(channels.get(selectedTeamInstance)!=null)
        {
            system.debug('inside channels');
            allchannels=channels.get(selectedTeamInstance).Channels__c.split(',');
            
        }
        system.debug('++allchannels'+allchannels);
        for(Integer i=0 ; i<allchannels.size();i++)
        {
            channelsmapping.put(allchannels[i],channels.get(selectedTeamInstance).Metric_Mapping__c.split(',')[i]);
        }
        system.debug('++channelsmapping'+channelsmapping);
        
        Boolean affiliationFlag = [Select Cycle__r.Name,Enable_Affiliation_CallPlan__c From AxtriaSalesIQTM__Team_Instance__c where Name =: selectedTeamInstance].Enable_Affiliation_CallPlan__c;
        System.debug('affiliationFlag--'+affiliationFlag);
       
        list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> posAccListUpdate = new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        
            for(Call_Plan__c cprcd : callplnlist)
            {
                
                AxtriaSalesIQTM__Position_Account_Call_Plan__c posAcc= new AxtriaSalesIQTM__Position_Account_Call_Plan__c();
                if(multiChannel!=true|| multiChannel==null)
                {
                     if(cprcd.Account__c != null && cprcd.position__c != null  &&  cprcd.Position_Team_Instance__c != null && cprcd.Team_Instance__c != null && cprcd.Position_Account__c != null && cprcd.Product_Name__c != null && cprcd.Product_Name__c != '')
                    {
                        if(affiliationFlag == true && cprcd.Parent_Account__c !=null) 
                        {
                            posAcc.Parent_Account__c = cprcd.Parent_Account__c;
                        }
                        posAcc.AxtriaSalesIQTM__Account__c  =  cprcd.Account__c;
                        posAcc.AxtriaSalesIQTM__Position__c  =  cprcd.Position__c;
                        posAcc.AxtriaSalesIQTM__Position_Team_Instance__c  =  cprcd.Position_Team_Instance__c;
                        posAcc.AxtriaSalesIQTM__Team_Instance__c  =  cprcd.Team_Instance__c;
                        posAcc.Party_ID__c  =  cprcd.Position_Account__c;
                        
                        posAcc.AxtriaSalesIQTM__isincludedCallPlan__c  =  cprcd.Target__c;
                        posAcc.AxtriaSalesIQTM__isAccountTarget__c  =  cprcd.Target__c;
                        posAcc.AxtriaSalesIQTM__lastApprovedTarget__c  =  cprcd.Target__c;
                        
                        posAcc.CurrencyIsoCode  =  cprcd.CurrencyIsoCode;
                        posAcc.AxtriaSalesIQTM__Effective_Start_Date__c= cprcd.Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c;
                        posAcc.AxtriaSalesIQTM__Effective_End_Date__c= cprcd.Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c;
                        
                        
                        posAcc.Previous_Calls__c = cprcd.Previous_Cycle_Calls__c;
                        posAcc.P1__c = cprcd.Product_Name__c;
                        posAcc.P1_Original__c = cprcd.Product_Name__c;
                        posAcc.Adoption__c = cprcd.Adoption__c;
                        posAcc.Potential__c = cprcd.Potential__c;
                        if(cprcd.Objective_1__c!=null)
                        {
                            posAcc.Final_TCF__c = Integer.valueOf(cprcd.Objective_1__c);
                            posAcc.Final_TCF_Approved__c = Integer.valueOf(cprcd.Objective_1__c);
                            posAcc.Final_TCF_Original__c = Integer.valueOf(cprcd.Objective_1__c);
                            posAcc.Proposed_TCF__c=Integer.valueOf(cprcd.Objective_1__c);
                        }
                        posAcc.Segment__c = cprcd.Segment__c;
                        posAcc.Segment_Approved__c = cprcd.Segment__c;
                        posAcc.Segment_Original__c = cprcd.Segment__c;
                        if(cprcd.Previous_Cycle_Calls__c != null)
                        {
                            posAcc.AxtriaSalesIQTM__Segment10__c = cprcd.Previous_Cycle_Calls__c;
                        }

                        //***************************************Added by dhiren ----- SAL-308****************************************************
                        
                        String prodName = cprcd.Product_Name__c ;
                        System.debug('prodName--'+prodName);
                        String key = cprcd.Account__c + '_' + prodName.toUpperCase() + '_' + CycleName;
                        String key2 = cprcd.Account__r.AccountNumber.toUpperCase() + '_' + prodName.toUpperCase() + '_' + CycleName.toUpperCase();

                        Mac.put(key, cprcd);
                        keymapid.put(key, key2);
                        accids.add(cprcd.Account__c);
                        System.debug('::::::::Accids::::::::::'+Accids);

                        //******************************************End Of Code by Dhiren**********************************************

                        
                        cprcd.Status__c = 'Processing';
                        //to handle HCP-HCO call plan Added by Sukirty (8/11/2019)
                        if(affiliationFlag == false) 
                        {
                            if(!pacpSet.contains(String.valueof(posAcc.AxtriaSalesIQTM__Team_Instance__c)+String.valueof(posAcc.AxtriaSalesIQTM__Account__c)+String.valueof(posAcc.AxtriaSalesIQTM__Position__c)+String.valueof(posAcc.P1__c)))
                            {
                                posAccListUpdate.add(posAcc);
                                pacpSet.add(String.valueof(posAcc.AxtriaSalesIQTM__Team_Instance__c)+String.valueof(posAcc.AxtriaSalesIQTM__Account__c)+String.valueof(posAcc.AxtriaSalesIQTM__Position__c)+String.valueof(posAcc.P1__c));
                            }
                        }
                        else 
                        {
                            if(!pacpSet.contains(String.valueof(posAcc.AxtriaSalesIQTM__Team_Instance__c)+String.valueof(posAcc.AxtriaSalesIQTM__Account__c)+String.valueof(posAcc.Parent_Account__c)+String.valueof(posAcc.AxtriaSalesIQTM__Position__c)+String.valueof(posAcc.P1__c)))
                            {
                                posAccListUpdate.add(posAcc);
                                pacpSet.add(String.valueof(posAcc.AxtriaSalesIQTM__Team_Instance__c)+String.valueof(posAcc.AxtriaSalesIQTM__Account__c)+String.valueof(posAcc.Parent_Account__c)+String.valueof(posAcc.AxtriaSalesIQTM__Position__c)+String.valueof(posAcc.P1__c));
                            }
                        }
                    }
                }
                else
                {
                    if(cprcd.Account__c != null && cprcd.position__c != null  &&  cprcd.Position_Team_Instance__c != null && cprcd.Team_Instance__c != null && cprcd.Position_Account__c != null && cprcd.Product_Name__c != null && cprcd.Product_Name__c != '')
                    {

                        if(affiliationFlag == true && cprcd.Parent_Account__c !=null) 
                        {
                            posAcc.Parent_Account__c = cprcd.Parent_Account__c;
                        }
                        posAcc.AxtriaSalesIQTM__Account__c  =  cprcd.Account__c;
                        posAcc.AxtriaSalesIQTM__Position__c  =  cprcd.Position__c;
                        posAcc.AxtriaSalesIQTM__Position_Team_Instance__c  =  cprcd.Position_Team_Instance__c;
                        posAcc.AxtriaSalesIQTM__Team_Instance__c  =  cprcd.Team_Instance__c;
                        posAcc.Party_ID__c  =  cprcd.Position_Account__c;
                        
                        posAcc.AxtriaSalesIQTM__isincludedCallPlan__c  =  cprcd.Target__c;
                        posAcc.AxtriaSalesIQTM__isAccountTarget__c  =  cprcd.Target__c;
                        posAcc.AxtriaSalesIQTM__lastApprovedTarget__c  =  cprcd.Target__c;
                        
                        posAcc.CurrencyIsoCode  =  cprcd.CurrencyIsoCode;
                        posAcc.AxtriaSalesIQTM__Effective_Start_Date__c= cprcd.Team_Instance__r.AxtriaSalesIQTM__IC_EffstartDate__c;
                        posAcc.AxtriaSalesIQTM__Effective_End_Date__c= cprcd.Team_Instance__r.AxtriaSalesIQTM__IC_EffEndDate__c;
                        
                        
                        posAcc.Previous_Calls__c = cprcd.Previous_Cycle_Calls__c;
                        posAcc.P1__c = cprcd.Product_Name__c;
                        posAcc.P1_Original__c = cprcd.Product_Name__c;
                        posAcc.Adoption__c = cprcd.Adoption__c;
                        posAcc.Potential__c = cprcd.Potential__c;

                        Map<String,String> channelMap = new Map<String,String>();
                        
                        if(cprcd.Channel_1__c!=null && cprcd.Objective_1__c!=null){channelMap.put(cprcd.Channel_1__c, cprcd.Objective_1__c);}

                        if(cprcd.Channel_2__c!=null && cprcd.Objective_2__c!=null) {channelMap.put(cprcd.Channel_2__c, cprcd.Objective_2__c);}
                        if(cprcd.Channel_3__c!=null && cprcd.Objective_3__c!=null) {channelMap.put(cprcd.Channel_3__c, cprcd.Objective_3__c);}
                        if(cprcd.Channel_4__c!=null && cprcd.Objective_4__c!=null) {channelMap.put(cprcd.Channel_4__c, cprcd.Objective_4__c);}
                        if(cprcd.Channel_5__c!=null && cprcd.Objective_5__c!=null) {channelMap.put(cprcd.Channel_5__c, cprcd.Objective_5__c);}

                        system.debug('++channelMap'+channelMap);

                        for(String str : channelMap.keySet())
                        {
                            system.debug('++str'+str);
                            if(channelsmapping.containsKey(str))
                            {
                                String fieldApi=channelsmapping.get(str);

                                switch on fieldApi
                                {
                                    when'AxtriaARSnT__Final_TCF__c'
                                    {
                                        posAcc.Final_TCF__c=Integer.valueOf(channelMap.get(str));
                                        posAcc.Final_TCF_Approved__c = Integer.valueOf(channelMap.get(str));
                                        posAcc.Final_TCF_Original__c = Integer.valueOf(channelMap.get(str));
                                        posAcc.Proposed_TCF__c=Integer.valueOf(channelMap.get(str));
                                    }

                                    when 'AxtriaSalesIQTM__Metric1__c'
                                    {
                                        posAcc.AxtriaSalesIQTM__Metric1__c=Integer.valueOf(channelMap.get(str));
                                        posAcc.AxtriaSalesIQTM__Metric1_Approved__c=Integer.valueOf(channelMap.get(str));
                                        posAcc.AxtriaSalesIQTM__Metric1_Updated__c=Integer.valueOf(channelMap.get(str));
                                    }
                                    when 'AxtriaSalesIQTM__Metric2__c'
                                    {
                                        posAcc.AxtriaSalesIQTM__Metric2__c=Integer.valueOf(channelMap.get(str));
                                        posAcc.AxtriaSalesIQTM__Metric2_Approved__c=Integer.valueOf(channelMap.get(str));
                                        posAcc.AxtriaSalesIQTM__Metric2_Updated__c=Integer.valueOf(channelMap.get(str));
                                    }
                                    when 'AxtriaSalesIQTM__Metric3__c'
                                    {
                                        posAcc.AxtriaSalesIQTM__Metric3__c=Integer.valueOf(channelMap.get(str));
                                        posAcc.AxtriaSalesIQTM__Metric3_Approved__c=Integer.valueOf(channelMap.get(str));
                                        posAcc.AxtriaSalesIQTM__Metric3_Updated__c=Integer.valueOf(channelMap.get(str));
                                    }
                                    when 'AxtriaSalesIQTM__Metric4__c'
                                    {
                                        posAcc.AxtriaSalesIQTM__Metric4__c=Integer.valueOf(channelMap.get(str));
                                        posAcc.AxtriaSalesIQTM__Metric4_Approved__c=Integer.valueOf(channelMap.get(str));
                                        posAcc.AxtriaSalesIQTM__Metric4_Updated__c=Integer.valueOf(channelMap.get(str));
                                    }
                                }
                            }
                        }


                        posAcc.Segment__c = cprcd.Segment__c;
                        posAcc.Segment_Approved__c = cprcd.Segment__c;
                        posAcc.Segment_Original__c = cprcd.Segment__c;
                        if(cprcd.Previous_Cycle_Calls__c != null)
                        {
                            posAcc.AxtriaSalesIQTM__Segment10__c = cprcd.Previous_Cycle_Calls__c;
                        }

                        //*************************************** ----- SAL-308****************************************************
                        
                        String prodName = cprcd.Product_Name__c ;
                        String key = cprcd.Account__c + '_' + prodName.toUpperCase() + '_' + CycleName;
                        String key2 = cprcd.Account__r.AccountNumber.toUpperCase() + '_' + prodName.toUpperCase() + '_' + CycleName.toUpperCase();

                        Mac.put(key, cprcd);
                        keymapid.put(key, key2);
                        accids.add(cprcd.Account__c);
                        System.debug('::::::::Accids::::::::::'+Accids);

                        //******************************************End Of Code **********************************************
                        
                        cprcd.Status__c = 'Processing';
                        if(affiliationFlag == false) 
                        {
                            if(!pacpSet.contains(String.valueof(posAcc.AxtriaSalesIQTM__Team_Instance__c)+String.valueof(posAcc.AxtriaSalesIQTM__Account__c)+String.valueof(posAcc.AxtriaSalesIQTM__Position__c)+String.valueof(posAcc.P1__c)))
                            {
                                posAccListUpdate.add(posAcc);
                                pacpSet.add(String.valueof(posAcc.AxtriaSalesIQTM__Team_Instance__c)+String.valueof(posAcc.AxtriaSalesIQTM__Account__c)+String.valueof(posAcc.AxtriaSalesIQTM__Position__c)+String.valueof(posAcc.P1__c));
                            }
                        }
                        else 
                        {
                            if(!pacpSet.contains(String.valueof(posAcc.AxtriaSalesIQTM__Team_Instance__c)+String.valueof(posAcc.AxtriaSalesIQTM__Account__c)+String.valueof(posAcc.Parent_Account__c)+String.valueof(posAcc.AxtriaSalesIQTM__Position__c)+String.valueof(posAcc.P1__c)))
                            {
                                posAccListUpdate.add(posAcc);
                                pacpSet.add(String.valueof(posAcc.AxtriaSalesIQTM__Team_Instance__c)+String.valueof(posAcc.AxtriaSalesIQTM__Account__c)+String.valueof(posAcc.Parent_Account__c)+String.valueof(posAcc.AxtriaSalesIQTM__Position__c)+String.valueof(posAcc.P1__c));
                            }
                        }
                    }
                }             
            }
            System.debug('posAccListUpdate after loop--'+posAccListUpdate);
            System.debug('posAccListUpdate size after loop--'+posAccListUpdate.size());
            System.debug('pacpSet after loop--'+pacpSet);
            System.debug('pacpSet size after loop--'+pacpSet.size());

         Map<String,string> channelStagMap = new Map<String,String>();//MCCP COde
        if(multiChannel==true)
        {
            system.debug('+++2nd multiChannel');
            for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp :pacpListToCheck)
            {
                system.debug('+++pacp rec'+pacp);
                string pacpkey=String.valueof(pacp.AxtriaSalesIQTM__Team_Instance__c)+String.valueof(pacp.AxtriaSalesIQTM__Account__c)+String.valueof(pacp.AxtriaSalesIQTM__Position__c)+String.valueof(pacp.P1__c);
                if(stagingMap.containsKey(pacpkey))
                {
                    channelStagMap=stagingMap.get(pacpkey);
                }
                system.debug('+++channelStagMap'+channelStagMap);
                

                for(String str : channelStagMap.keySet())
                {
                    system.debug('++str'+str);
                    if(channelsmapping.containsKey(str))
                    {
                        String fieldApi=channelsmapping.get(str);

                        switch on fieldApi
                        {
                            when'AxtriaARSnT__Final_TCF__c'
                            {
                                pacp.Final_TCF__c=Integer.valueOf(channelStagMap.get(str));
                                pacp.Final_TCF_Approved__c = Integer.valueOf(channelStagMap.get(str));
                                pacp.Final_TCF_Original__c = Integer.valueOf(channelStagMap.get(str));
                                pacp.Proposed_TCF__c=Integer.valueOf(channelStagMap.get(str));
                            }

                            when 'AxtriaSalesIQTM__Metric1__c'
                            {
                                pacp.AxtriaSalesIQTM__Metric1__c=Integer.valueOf(channelStagMap.get(str));
                                pacp.AxtriaSalesIQTM__Metric1_Approved__c=Integer.valueOf(channelStagMap.get(str));
                                pacp.AxtriaSalesIQTM__Metric1_Updated__c=Integer.valueOf(channelStagMap.get(str));
                            }
                            when 'AxtriaSalesIQTM__Metric2__c'
                            {
                                pacp.AxtriaSalesIQTM__Metric2__c=Integer.valueOf(channelStagMap.get(str));
                                pacp.AxtriaSalesIQTM__Metric2_Approved__c=Integer.valueOf(channelStagMap.get(str));
                                pacp.AxtriaSalesIQTM__Metric2_Updated__c=Integer.valueOf(channelStagMap.get(str));
                            }
                            when 'AxtriaSalesIQTM__Metric3__c'
                            {
                                pacp.AxtriaSalesIQTM__Metric3__c=Integer.valueOf(channelStagMap.get(str));
                                pacp.AxtriaSalesIQTM__Metric3_Approved__c=Integer.valueOf(channelStagMap.get(str));
                                pacp.AxtriaSalesIQTM__Metric3_Updated__c=Integer.valueOf(channelStagMap.get(str));
                            }
                            when 'AxtriaSalesIQTM__Metric4__c'
                            {
                                pacp.AxtriaSalesIQTM__Metric4__c=Integer.valueOf(channelStagMap.get(str));
                                pacp.AxtriaSalesIQTM__Metric4_Approved__c=Integer.valueOf(channelStagMap.get(str));
                                pacp.AxtriaSalesIQTM__Metric4_Updated__c=Integer.valueOf(channelStagMap.get(str));
                            }
                        }
                    }
                }
            }
            upsert pacpListToCheck;
        }

          if(errorList.size() > 0)
          {
            insert errorList;
          }
            CallPlanSummaryTriggerHandler.execute_trigger = false; //Using variable to stop from executing trigger while running this batch.
            customsetting = AxtriaSalesIQTM__TriggerContol__c.getValues('ParentPacp');
            customsetting2 = AxtriaSalesIQTM__TriggerContol__c.getValues('CallPlanSummaryTrigger');
            system.debug('==========customsetting========'+customsetting);
            system.debug('==========customsetting========'+customsetting2);
            customsetting.AxtriaSalesIQTM__IsStopTrigger__c = true ;
            customsetting2.AxtriaSalesIQTM__IsStopTrigger__c = true ;
            //update customsetting ;
            customsettinglist.add(customsetting);
            customsettinglist.add(customsetting2);
            update customsettinglist;

            upsert posAccListUpdate;
             update callplnlist;
            system.debug('============upsert done===================');
            customsettinglist = new list<AxtriaSalesIQTM__TriggerContol__c>();
            customsetting.AxtriaSalesIQTM__IsStopTrigger__c = customsetting.AxtriaSalesIQTM__IsStopTrigger__c ? false : customsetting.AxtriaSalesIQTM__IsStopTrigger__c;
            customsetting2.AxtriaSalesIQTM__IsStopTrigger__c = customsetting2.AxtriaSalesIQTM__IsStopTrigger__c ? false : customsetting2.AxtriaSalesIQTM__IsStopTrigger__c;
            //update customsetting ;
            customsettinglist.add(customsetting);
            customsettinglist.add(customsetting2);
            update customsettinglist;

             // Changed By A1930
             List<AggregateResult> aggRecsAccount = new List<AggregateResult>();
             if(affiliationFlag) 
             {
                aggRecsAccount = [Select  MAX(Final_TCF__c) tcf, AxtriaSalesIQTM__Account__r.accountnumber accNumber ,Parent_Account__r.accountnumber parentacc from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c = :posIDSet and AxtriaSalesIQTM__Team_Instance__c = :selectedTeamInstance and AxtriaSalesIQTM__isincludedCallPlan__c = true group by AxtriaSalesIQTM__Account__r.accountnumber,Parent_Account__r.accountnumber];
             }
             else 
             {
                aggRecsAccount = [Select  MAX(Final_TCF__c) tcf, AxtriaSalesIQTM__Account__r.accountnumber accNumber from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c = :posIDSet and AxtriaSalesIQTM__Team_Instance__c = :selectedTeamInstance and AxtriaSalesIQTM__isincludedCallPlan__c = true group by AxtriaSalesIQTM__Account__r.accountnumber];
              }
      

        // Max Calls are display based on HCP/HCO

        List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacpFinal = New List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacpFinalUpdateLst = new List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        pacpFinal = [Select id,name,AxtriaSalesIQTM__Account__r.AccountNumber,Parent_Account__r.AccountNumber,Max_Calls__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c = :posIDSet and AxtriaSalesIQTM__Team_Instance__c = :selectedTeamInstance and AxtriaSalesIQTM__isincludedCallPlan__c = true ];

            for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacpFinalCalls : pacpFinal){

                for(AggregateResult aggAccountNumber : aggRecsAccount){

                    if(affiliationFlag && pacpFinalCalls.AxtriaSalesIQTM__Account__r.AccountNumber == aggAccountNumber.get('accNumber') && pacpFinalCalls.Parent_Account__r.AccountNumber == aggAccountNumber.get('parentacc')){
                        pacpFinalCalls.Max_Calls__c = Integer.valueof(aggAccountNumber.get('tcf'));
                        system.debug('+++MaxCalls:::' + pacpFinalCalls.Max_Calls__c);
                        pacpFinalUpdateLst.add(pacpFinalCalls);
                    }
                    else if(!affiliationFlag && pacpFinalCalls.AxtriaSalesIQTM__Account__r.AccountNumber == aggAccountNumber.get('accNumber')){
                        pacpFinalCalls.Max_Calls__c = Integer.valueof(aggAccountNumber.get('tcf'));
                        pacpFinalUpdateLst.add(pacpFinalCalls);
                    }
                }
                            
            }
            update pacpFinalUpdateLst;

                // End by Max calls 

            //******************************************************Added by dhiren for SAL - 308************************************************************
            
            
            List<Master_List_AccProd__c> updMasterAccprod = new List<Master_List_AccProd__c>();
            Map<String,Master_List_AccProd__c> mapMasterAccprod = new  Map<String,Master_List_AccProd__c>();
            //Boolean recFlag = true;
            for(Master_List_AccProd__c rec : [Select id,AccountProductKey__c,Team_Instance__c,Segment__c,Account__c,Account__r.AccountNumber,Product__c,Unique_ID__c,Status__c from Master_List_AccProd__c where Account__c IN: Accids and Team_Instance__r.Cycle__r.Name =:CycleName])
            {   
                if(!mapMasterAccprod.containsKey(rec.AccountProductKey__c))
                {
                    mapMasterAccprod.put(rec.AccountProductKey__c, rec);
                }
            }

            Master_List_AccProd__c recordofMAsterAccprod;
            for(String pacp : Mac.keySet())
                {
                    recordofMAsterAccprod = new Master_List_AccProd__c();
                    if(!mapMasterAccprod.isEmpty())
                    {
                        
                        if(mapMasterAccprod.containsKey(pacp))
                        {
                            recordofMAsterAccprod = mapMasterAccprod.get(pacp);
                            recordofMAsterAccprod.Segment__c= Mac.get(pacp).Segment__c;
                            recordofMAsterAccprod.Unique_ID__c = keymapid.get(pacp);
                            recordofMAsterAccprod.Status__c = 'New';
                            system.debug(':::::::::::Loop 1::::::::::::::::');
                        }

                        else
                        {
                            system.debug(':::::::::::Loop 2::::::::::::::::');
                            recordofMAsterAccprod.Segment__c = Mac.get(pacp).Segment__c;
                            recordofMAsterAccprod.Account__c = Mac.get(pacp).Account__c;
                            recordofMAsterAccprod.Team_Instance__c = Mac.get(pacp).Team_Instance__c;
                            recordofMAsterAccprod.Product__c = Mac.get(pacp).Product_Name__c;
                            recordofMAsterAccprod.Unique_ID__c = keymapid.get(pacp);
                            recordofMAsterAccprod.Status__c = 'New';
                        }

                        //updMasterAccprod.add(recordofMAsterAccprod);
                    }
                    else
                    {
                        system.debug(':::::::::::Loop 3::::::::::::::::');
                    
                    recordofMAsterAccprod.Segment__c = Mac.get(pacp).Segment__c;
                    recordofMAsterAccprod.Account__c = Mac.get(pacp).Account__c;
                    recordofMAsterAccprod.Team_Instance__c = Mac.get(pacp).Team_Instance__c;
                    recordofMAsterAccprod.Product__c = Mac.get(pacp).Product_Name__c;
                    recordofMAsterAccprod.Unique_ID__c = keymapid.get(pacp);
                    recordofMAsterAccprod.Status__c = 'New';

                    
                    }
                    updMasterAccprod.add(recordofMAsterAccprod);

                }

            system.debug(':::::::::::updMasterAccprod::::::::::::::::'+updMasterAccprod);

            system.debug(':::::::::::recFlag::::::::::::::::'+recFlag);

            if(recFlag)
            {
                for(String pacp : Mac.keySet())
                {
                    system.debug(':::::::::::Loop 3::::::::::::::::');
                    Master_List_AccProd__c rec2 = new Master_List_AccProd__c();
                    rec2.Segment__c = Mac.get(pacp).Segment__c;
                    rec2.Account__c = Mac.get(pacp).Account__c;
                    rec2.Team_Instance__c = Mac.get(pacp).Team_Instance__c;
                    rec2.Product__c = Mac.get(pacp).Product_Name__c;

                    updMasterAccprod.add(rec2);
                }
            }
            system.debug(':::::::::::updMasterAccprod::::::::::::::::'+updMasterAccprod);


            if(!updMasterAccprod.isEmpty())
            {
            system.debug(':::::::::::Insert::::::::::::::::');

                upsert updMasterAccprod;
            }

            //******************************************************End of Code By Dhiren**************************************************************
       
            CallPlanSummaryTriggerHandler.execute_trigger = true; //Using variable to start from executing trigger after runnig the batch update. */

          
      }
    
          global void finish(Database.BatchableContext BC){

           /* try
            {
                Checkuserpositions cup = new Checkuserpositions(selectedTeamInstance);    
            }
            catch(Exception e)
            {
                system.debug('++++++++++++ ISSUE IN User Pos '+ e.getMessage());
            }
            
            
            Database.executebatch(new BatchUpdateSharedFlag(selectedTeamInstance,'directLoad'),500);*/
            
       }

}