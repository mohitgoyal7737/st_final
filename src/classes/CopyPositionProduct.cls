global with sharing class CopyPositionProduct implements Database.Batchable<sObject>
 {
    public String query;
    public String sourceTeamInstance;
    public String destinationTeamInstance;
    Map<String,Id> positionnametoid;
    Map<String,Id> productnametoid;
    List<AxtriaSalesIQTM__Position_Product__c> positionProductList = new  List<AxtriaSalesIQTM__Position_Product__c>();
     public Boolean chain = false;

    global CopyPositionProduct(String teaminstance, String destTeamInstance)
        {
            sourceTeamInstance = teaminstance;
            destinationTeamInstance = destTeamInstance;
            query='SELECT Calls_Day__c,Holidays__c,isHolidayOverridden__c,Other_Days_Off__c,Product_Catalog__c,Product_Catalog__r.name,Vacation_Days__c,AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Effective_Start_Date__c,AxtriaSalesIQTM__isActive__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Product_Weight__c,AxtriaSalesIQTM__Team_Instance__c,Id,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c FROM AxtriaSalesIQTM__Position_Product__c where AxtriaSalesIQTM__Team_Instance__c =: sourceTeamInstance and AxtriaSalesIQTM__isActive__c = true';
        }


    global Database.QueryLocator start(Database.BatchableContext bc)
     {
        return Database.getQueryLocator(query);
     }

    global void execute(Database.BatchableContext BC, List<AxtriaSalesIQTM__Position_Product__c> scope) 
    {

        positionnametoid= new  Map<String,Id>();
        productnametoid= new  Map<String,Id>();
        //position map
        List<AxtriaSalesIQTM__Position__c> position = [Select Id,Name,AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Team_Instance__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Team_Instance__c =: destinationTeamInstance];

        for(AxtriaSalesIQTM__Position__c posP : position)
        {
             positionnametoid.put(posP.AxtriaSalesIQTM__Client_Position_Code__c,posP.Id);
        }
         SnTDMLSecurityUtil.printDebugMessage('+++positionnametoid+++'+positionnametoid);
         // product map
        List<Product_Catalog__c> prodCatalog = [Select Id, Name, Product_Code__c,Team_Instance__c from Product_Catalog__c where Team_Instance__c =: destinationTeamInstance and IsActive__c=true];
        SnTDMLSecurityUtil.printDebugMessage('+++prodCatalog+++'+prodCatalog);
        for(Product_Catalog__c product : prodCatalog)
        {
            productnametoid.put(product.Name,product.Id);
        }
        SnTDMLSecurityUtil.printDebugMessage('+++prodCatalog+++'+productnametoid);
        //team instance
        List<AxtriaSalesIQTM__Team_Instance__c> teamInst = [Select Id,Name, AxtriaSalesIQTM__IC_EffEndDate__c,AxtriaSalesIQTM__IC_EffstartDate__c from AxtriaSalesIQTM__Team_Instance__c where id =: destinationTeamInstance];

       SnTDMLSecurityUtil.printDebugMessage('+++teamInst+++'+teamInst[0].id);
        //copy of position product
        for(AxtriaSalesIQTM__Position_Product__c posProduct : scope)
        {
            AxtriaSalesIQTM__Position_Product__c posProdDest = new AxtriaSalesIQTM__Position_Product__c();
            posProdDest = posProduct.clone();
            posProdDest.AxtriaSalesIQTM__Team_Instance__c = teamInst[0].id;
            posProdDest.AxtriaSalesIQTM__Position__c = positionnametoid.get(posProduct.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c);
            posProdDest.Product_Catalog__c= productnametoid.get(posProduct.Product_Catalog__r.name);
            posProdDest.AxtriaSalesIQTM__Effective_Start_Date__c=teamInst[0].AxtriaSalesIQTM__IC_EffstartDate__c;
            posProdDest.AxtriaSalesIQTM__Effective_End_Date__c=teamInst[0].AxtriaSalesIQTM__IC_EffEndDate__c;
            positionProductList.add(posProdDest);
        }
       if(!positionProductList.isEmpty())
        {
           //insert positionProductList;
           SnTDMLSecurityUtil.insertRecords(positionProductList, 'CopyPositionProduct');
        }
        SnTDMLSecurityUtil.printDebugMessage('+++positionProductList++'+positionProductList);
    }

    global void finish(Database.BatchableContext BC)
    {
         
            copyCyclePlan cpp = new copyCyclePlan(sourceTeamInstance,destinationTeamInstance);
            database.executeBatch(cpp,2000);
        
       
    }
}