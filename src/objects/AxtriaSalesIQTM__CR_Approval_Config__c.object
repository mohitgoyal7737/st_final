<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Configuration related to change request implementation  in Given Implementation specific to Team Cycle</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>AxtriaSalesIQTM__Change_Request_Type__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Lookup to Change Request Type Object</description>
        <externalId>false</externalId>
        <label>Change Request Type</label>
        <referenceTo>AxtriaSalesIQTM__Change_Request_Type__c</referenceTo>
        <relationshipName>Change_Request_Configs</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__EsclationFlag__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>EsclationFlag</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Esclation_After__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Esclation_After</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Esclation_Period__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Esclation_Period</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>7 Days</fullName>
                    <default>false</default>
                    <label>7 Days</label>
                </value>
                <value>
                    <fullName>14 Days</fullName>
                    <default>false</default>
                    <label>14 Days</label>
                </value>
                <value>
                    <fullName>18 Days</fullName>
                    <default>false</default>
                    <label>18 Days</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__HO_Office_User__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>HO_Office_User</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__HR_User__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>HR_User</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__IsImpactedPosition__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Does this change requires approval from impacted Position</description>
        <externalId>false</externalId>
        <label>IsImpactedPosition</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Is_Auto_Approve__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Is Auto Approve</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Team_Instance_ID__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Lookup to Team Instance</description>
        <externalId>false</externalId>
        <label>Team Instance ID</label>
        <referenceTo>AxtriaSalesIQTM__Team_Instance__c</referenceTo>
        <relationshipName>Change_Request_Configs</relationshipName>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__isFinalApprover__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Specify the final Approver</description>
        <externalId>false</externalId>
        <label>isFinalApprover</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__isHigherLevelManagers__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>isHigherLevelManagers</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__isHigherLevelOverride__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>isHigherLevelOverride</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__isImpactedManagerOverride__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Does this change request has override available for Impacted Managers</description>
        <externalId>false</externalId>
        <label>isImpactedManagerOverride</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__isImpactedManager__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Does this change request require Impacted Manager approvals</description>
        <externalId>false</externalId>
        <label>isImpactedManager</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>CR Approval Config</label>
    <listViews>
        <fullName>AxtriaSalesIQTM__All</fullName>
        <columns>NAME</columns>
        <columns>AxtriaSalesIQTM__Change_Request_Type__c</columns>
        <columns>AxtriaSalesIQTM__isHigherLevelManagers__c</columns>
        <columns>AxtriaSalesIQTM__HR_User__c</columns>
        <columns>AxtriaSalesIQTM__HO_Office_User__c</columns>
        <columns>AxtriaSalesIQTM__Team_Instance_ID__c</columns>
        <columns>AxtriaSalesIQTM__Is_Auto_Approve__c</columns>
        <columns>AxtriaSalesIQTM__isFinalApprover__c</columns>
        <columns>AxtriaSalesIQTM__isImpactedManager__c</columns>
        <columns>AxtriaSalesIQTM__IsImpactedPosition__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>AxtriaSalesIQTM__Call_Plan</fullName>
        <columns>NAME</columns>
        <columns>AxtriaSalesIQTM__Change_Request_Type__c</columns>
        <columns>AxtriaSalesIQTM__isHigherLevelManagers__c</columns>
        <columns>AxtriaSalesIQTM__HR_User__c</columns>
        <columns>AxtriaSalesIQTM__HO_Office_User__c</columns>
        <columns>AxtriaSalesIQTM__Team_Instance_ID__c</columns>
        <columns>AxtriaSalesIQTM__Is_Auto_Approve__c</columns>
        <columns>AxtriaSalesIQTM__isFinalApprover__c</columns>
        <columns>AxtriaSalesIQTM__isImpactedManager__c</columns>
        <columns>AxtriaSalesIQTM__IsImpactedPosition__c</columns>
        <filterScope>Everything</filterScope>
        <label>Call Plan</label>
    </listViews>
    <nameField>
        <displayFormat>CRC-{000000000}</displayFormat>
        <label>Change_Request_Config Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>CR Approval Configs</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
