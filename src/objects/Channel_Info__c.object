<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>This object holds the master data for Channel at Country &amp; Team Instance Level</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>false</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>false</enableSharing>
    <enableStreamingApi>false</enableStreamingApi>
    <fields>
        <fullName>Channel_Effectiveness_P1__c</fullName>
        <deprecated>false</deprecated>
        <description>Channel Effectiveness P1</description>
        <externalId>false</externalId>
        <label>Channel Effectiveness P1</label>
        <length>20</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Channel_Effectiveness_P2__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Channel Effectiveness P2</label>
        <length>20</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Channel_Effectiveness_P3__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Channel Effectiveness P3</label>
        <length>20</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Channel_Effectiveness_P4__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Channel Effectiveness P4</label>
        <length>20</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Channel_Name__c</fullName>
        <deprecated>false</deprecated>
        <description>Channel Name</description>
        <externalId>false</externalId>
        <label>Channel Name</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>Channels</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>Country__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Country</description>
        <externalId>false</externalId>
        <label>Country</label>
        <referenceTo>AxtriaSalesIQTM__Country__c</referenceTo>
        <relationshipLabel>Channels Info</relationshipLabel>
        <relationshipName>Channels_Info</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ExternalID__c</fullName>
        <caseSensitive>true</caseSensitive>
        <deprecated>false</deprecated>
        <description>Stores TeamInstance+ChannelName or Country+ChannelName</description>
        <externalId>false</externalId>
        <label>ExternalID</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Team_Instance__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Team Instance</description>
        <externalId>false</externalId>
        <label>Team Instance</label>
        <referenceTo>AxtriaSalesIQTM__Team_Instance__c</referenceTo>
        <relationshipLabel>Channels Info</relationshipLabel>
        <relationshipName>Channels_Info</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Team__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Team</label>
        <referenceTo>AxtriaSalesIQTM__Team__c</referenceTo>
        <relationshipLabel>Channels Info</relationshipLabel>
        <relationshipName>Channels_Info</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Workload_Equivalent__c</fullName>
        <deprecated>false</deprecated>
        <description>Team instance specific Workload Equivalent</description>
        <externalId>false</externalId>
        <label>Workload Equivalent</label>
        <length>20</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>includeForOptimisation__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>includeForOptimisation</description>
        <externalId>false</externalId>
        <label>includeForOptimisation</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Channel Info</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>CI -{0000}</displayFormat>
        <label>Channel Info Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Channels Info</pluralLabel>
    <recordTypes>
        <fullName>Country</fullName>
        <active>true</active>
        <description>Country Specific Channel Information</description>
        <label>Country</label>
        <picklistValues>
            <picklist>Channel_Name__c</picklist>
            <values>
                <fullName>Email</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>F2F</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Virtual Webinar</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>Team_Instance</fullName>
        <active>true</active>
        <description>Team Instance Specific Channel Information</description>
        <label>Team Instance</label>
        <picklistValues>
            <picklist>Channel_Name__c</picklist>
            <values>
                <fullName>Email</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>F2F</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Virtual Webinar</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
