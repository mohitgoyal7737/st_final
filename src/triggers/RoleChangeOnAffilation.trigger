trigger RoleChangeOnAffilation on AxtriaSalesIQTM__Account_Affiliation__c (After update) {
    
    public Boolean Isrunning=true; //Switch to enable or Disable Trigger
    public boolean MethodDifferentatior = true;
    
    if(!AxtriaSalesIQTM__TriggerContol__c.getValues('RoleChangeOnAffilation').AxtriaSalesIQTM__IsStopTrigger__c){
        
        Set<String> ActiveAccountRole = new Set<String>();
        map<String,String> Map_Accounts_With_Inactive_Role = new map<String,String>();
        map<String,String> Map_Accounts_With_Changed_Role = new map<String,String>();
        
        //Set<String> UniqueAccountUpdate = new Set<String>();
        List<Account> UpdatedAccounts = new List<Account>();
        
        for(AxtriaSalesIQTM__Account_Affiliation__c ActiveAff : [Select id,Account_Number__c,Role_Name__c,AxtriaSalesIQTM__Active__c from AxtriaSalesIQTM__Account_Affiliation__c where AxtriaSalesIQTM__Active__c = true  AND AxtriaSalesIQTM__Affiliation_Type__c = 'HCP to HCA' AND  (Role_Name__c != null OR Role_Name__c != 'NA' )])
        {
            String UniqueKey=ActiveAff.Account_Number__c+ActiveAff.Role_Name__c;

            ActiveAccountRole.add(UniqueKey.toLowerCase());        }
        
        /*for(AxtriaSalesIQTM__Account_Affiliation__c Acc_Aff : Trigger.new){
            
            String Acc_Num = Acc_Aff.Account_Number__c;
            UniqueAccountUpdate.add(Acc_Num);
        }*/
        
        //-----------------------------------------Part-1 started----To handel the Scenario where Account Affiliation status has been marked inactive---------------------------------
        
        
        for(AxtriaSalesIQTM__Account_Affiliation__c AccAff : Trigger.new)
        {
            if(AccAff.AxtriaSalesIQTM__Active__c == False)
            {
                String InactiveAccountRole=AccAff.Account_Number__c+AccAff.Role_Name__c;
                InactiveAccountRole=InactiveAccountRole.toLowerCase();
                if(!ActiveAccountRole.contains(InactiveAccountRole)){
                    Map_Accounts_With_Inactive_Role.put(AccAff.Account_Number__c, AccAff.Role_Name__c);
                }
            }
        }
        
        for(Account Acc : [Select id,AccountNumber,Role_Name__c from Account where AccountNumber IN: Map_Accounts_With_Inactive_Role.keySet()])
        {
            String AccRoleRemove = Map_Accounts_With_Inactive_Role.get(Acc.AccountNumber);
            if(Acc.Role_Name__c.StartsWith(AccRoleRemove)){
                if(Acc.Role_Name__c == AccRoleRemove){
                    Acc.Role_Name__c=null;
                }
                else{
                    AccRoleRemove = AccRoleRemove+';';
                    Acc.Role_Name__c = Acc.Role_Name__c.remove(AccRoleRemove);
                }
            }
            else{
                AccRoleRemove = ';'+AccRoleRemove;            
                Acc.Role_Name__c = Acc.Role_Name__c.remove(AccRoleRemove);
            }
            if(!UpdatedAccounts.contains(acc))
                UpdatedAccounts.add(acc);
        }
        
        
        //-----------------------------------------------------------------End of Part-1-----------------------------------------------------------------------------
        
        
        
        //-------------------------------------------Part-2 Started----To handel the scenario where Affilation role has been changed---------------------------------
        for(AxtriaSalesIQTM__Account_Affiliation__c AccAff : Trigger.old)
        {
            if(AccAff.AxtriaSalesIQTM__Active__c == true && AccAff.Role_Name__c != null)
            {
                String ChangedRole = AccAff.Account_Number__c+AccAff.Role_Name__c;
                ChangedRole = ChangedRole.toLowerCase();
                if(!ActiveAccountRole.contains(ChangedRole)){
                    Map_Accounts_With_Changed_Role.put(AccAff.Account_Number__c, AccAff.Role_Name__c);
                }
            }
            
        } 
        
        for(Account Acc : [Select id,AccountNumber,Role_Name__c from Account where AccountNumber IN: Map_Accounts_With_Changed_Role.keySet()])
        {
            String AccRoleRemove = Map_Accounts_With_Changed_Role.get(Acc.AccountNumber);
            // Acc.Role_Name__c = Acc.Role_Name__c.remove(AccRoleRemove);
            if(Acc.Role_Name__c.StartsWith(AccRoleRemove)){
                if(Acc.Role_Name__c == AccRoleRemove){
                    Acc.Role_Name__c=null;
                }
                else{
                    AccRoleRemove = AccRoleRemove+';';
                    Acc.Role_Name__c = Acc.Role_Name__c.remove(AccRoleRemove);
                }
            }
            else{
                AccRoleRemove = ';'+AccRoleRemove;            
                Acc.Role_Name__c = Acc.Role_Name__c.remove(AccRoleRemove);
                
            }
            if(!UpdatedAccounts.contains(acc))
                UpdatedAccounts.add(acc);
        }
        
    //-------------------------------------------------------End of Part-2----------------------------------------------    
        update UpdatedAccounts;
        
    }
    
    
}