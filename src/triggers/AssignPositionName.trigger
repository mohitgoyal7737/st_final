trigger AssignPositionName on AxtriaSalesIQTM__Employee__c (before insert, before update, after insert, after update) {
    list<id> currentPositions = new list<id>();
    set<string> setNewGrpNames = new set<string>();
    map<id,string> positionIDCodeMap = new map<id,string>();
    
    if(!trigger.isDelete && AxtriaSalesIQTM__TriggerContol__c.getValues('AssignPositionName') != null && !AxtriaSalesIQTM__TriggerContol__c.getValues('AssignPositionName').AxtriaSalesIQTM__IsStopTrigger__c){ 
    
        if(Trigger.isbefore){
            for(AxtriaSalesIQTM__Employee__c emp : trigger.new){
                if(emp.AxtriaSalesIQTM__Current_Territory__c != null){
                   currentPositions.add(emp.AxtriaSalesIQTM__Current_Territory__c); 
                }
            }
            for(AxtriaSalesIQTM__Position__c Pos : [select id,AxtriaSalesIQTM__Client_Position_Code__c 
                                                    from AxtriaSalesIQTM__Position__c where id in :currentPositions]){
                positionIDCodeMap.put(Pos.id,Pos.AxtriaSalesIQTM__Client_Position_Code__c);    
            }
            for(AxtriaSalesIQTM__Employee__c emp : trigger.new){
                emp.Current_Position__c = emp.AxtriaSalesIQTM__Current_Territory__c;
                if(positionIDCodeMap.containsKey(emp.AxtriaSalesIQTM__Current_Territory__c)){
                    emp.currentPositionId__C = positionIDCodeMap.get(emp.AxtriaSalesIQTM__Current_Territory__c);
                    emp.AxtriaSalesIQTM__Previous_Name__c = positionIDCodeMap.get(emp.AxtriaSalesIQTM__Current_Territory__c);    
                }
                 
            }
        }
        //----Added for AZ - When an AD Group is updated in an employee, create a UserGroup record.---
        //If the AD group is changed, mark the record as deleted and insert a new record.
        if(Trigger.isAfter){
            for(AxtriaSalesIQTM__Employee__c emp : trigger.new){
                //Add New Group Names
                if(!setNewGrpNames.contains(emp.Group_Name__c)){
                    setNewGrpNames.add(emp.Group_Name__c);
                }
            }
            system.debug('inside assign');
            //UpdatePositionCodeHandler.createNewGroups(setNewGrpNames,'AD Group');
            
            if(Trigger.isInsert){
                UpdatePositionCodeHandler.handleADGroup(trigger.new,null);
            }else if(Trigger.isUpdate){
                UpdatePositionCodeHandler.handleADGroup(trigger.new,trigger.oldmap);
            }
        }
        
        //---------------------------------AZ Requirement--------------------------------
        
    }
}