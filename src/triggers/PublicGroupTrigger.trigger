trigger PublicGroupTrigger on AxtriaSalesIQTM__Organization_Master__c (after insert) 
{
    List<Group> temp = new List<Group>();
    List<AxtriaSalesIQTM__Organization_Master__c> Temp1 = new List<AxtriaSalesIQTM__Organization_Master__c>();
    for(AxtriaSalesIQTM__Organization_Master__c OM:Trigger.new)
    {
      Group GG=new Group();
      GG.Name=OM.Name;
      Temp.add(GG);
    }
    insert temp;
    
    Map<String,String> PublicNameId = new Map<String,String>();
      
    for(Group g:temp)
    {
      PublicNameId.put(g.Name,g.ID);
      
    }
    Temp1=[select id,Public_Group_SFDC_id__c,Public_Group__c ,Name from AxtriaSalesIQTM__Organization_Master__c where Id in:Trigger.New];
    for(AxtriaSalesIQTM__Organization_Master__c OM:Temp1)
    {
      string tempp = PublicNameId.get(OM.Name);
      string newtmp ='';
      if(tempp.length()==18){
        newtmp=String.valueOf(tempp).substring(0, 15);
        OM.Public_Group_SFDC_id__c=newtmp;
      }
      else{
       OM.Public_Group_SFDC_id__c=PublicNameId.get(OM.Name); 
      }
      
      OM.Public_Group__c=OM.Name;
      
    }
    update Temp1;
    
}