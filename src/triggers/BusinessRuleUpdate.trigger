trigger BusinessRuleUpdate on AxtriaSalesIQTM__Business_Rules__c (after update) {
if(AxtriaSalesIQTM__TriggerContol__c.getValues('BusinessRuleUpdate') != null && AxtriaSalesIQTM__TriggerContol__c.getValues('BusinessRuleUpdate').AxtriaSalesIQTM__IsStopTrigger__c) return ;
for(AxtriaSalesIQTM__Business_Rules__c BR:Trigger.new)
{
    list<AxtriaSalesIQTM__Business_Rules__c> BRT = new list<AxtriaSalesIQTM__Business_Rules__c>();
    
    BRT = [select id,AxtriaSalesIQTM__Scenario__c from AxtriaSalesIQTM__Business_Rules__c where id=:trigger.newMap.keySet()];
    
  // list<AxtriaSalesIQTM__Scenario__c > SC = new list<AxtriaSalesIQTM__Scenario__c>();
    //SC=[select id,AxtriaSalesIQTM__Promote_Mode__c,AxtriaSalesIQTM__Rule_Execution_Status__c from AxtriaSalesIQTM__Scenario__c];
    
    for(AxtriaSalesIQTM__Business_Rules__c bb:BRT)
    {
    AxtriaSalesIQTM__Scenario__c ST = new AxtriaSalesIQTM__Scenario__c();
        ST.id=bb.AxtriaSalesIQTM__Scenario__c;
        ST.AxtriaSalesIQTM__Promote_Mode__c='Run Full and Promote';
        ST.AxtriaSalesIQTM__Rule_Execution_Status__c='Queued';
    Update ST; 
        }
    
}    
   
}