trigger teamInstance on AxtriaSalesIQTM__Team_Instance__c (before update, before insert,after update) 
{

    public AxtriaSalesIQTM__TriggerContol__c exeTrigger = new AxtriaSalesIQTM__TriggerContol__c();
    Boolean flag = false;
    if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate))
    {
        exeTrigger = AxtriaSalesIQTM__TriggerContol__c.getValues('teamInstanceBeforeTrigger')!= null ? AxtriaSalesIQTM__TriggerContol__c.getValues('teamInstanceBeforeTrigger'):null;

        if(exeTrigger != null)
        {
            if(exeTrigger.AxtriaSalesIQTM__IsStopTrigger__c == false)
            {
                System.debug('IsConditionForPositionAccountValid Trigger is Active');
                String queryString = 'SELECT count() FROM AxtriaSalesIQTM__Position_Account__c';
                     queryString += ' WHERE (AXTRIASALESIQTM__ASSIGNMENT_STATUS__C = \'Active\' OR AXTRIASALESIQTM__ASSIGNMENT_STATUS__C = \'Future Active\' ) and  AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c != \'00000\' ';

                    for(AxtriaSalesIQTM__Team_Instance__c ti : Trigger.new)
                    {
                       
                        //queryString += ' WHERE ';
                        if(ti.Condition_For_Position_Account__c != null && ti.Condition_For_Position_Account__c != '')
                        {
                            flag = true;
                            queryString += ' and  AxtriaSalesIQTM__Team_Instance__c = \''+ti.Id +'\'  and ( ' + ti.Condition_For_Position_Account__c + ' )' ;
                        }

                       

                       
                    }  

                if(flag)
                {
                    queryString += ' LIMIT 1 ';
                    try
                     {

                         System.debug('query --> ' + queryString);
                         Database.countQuery(queryString);
                     }
                    catch(Exception ex)
                    {
                        String str = ex.getMessage();
                        System.debug('error --? '+ str);
                        if(!str.contains('Non-selective query against large object type (more than 200000 rows)')){
                            for(AxtriaSalesIQTM__Team_Instance__c ti : Trigger.new)
                                ti.addError('The condition entered for Position Account is invalid. Kindly look for the error below.<br/>' + ex,false);
                        }
                    }
                }
                 

            }
        }
    }

    if(Trigger.isAfter)
    {
        exeTrigger = new AxtriaSalesIQTM__TriggerContol__c();
        exeTrigger = AxtriaSalesIQTM__TriggerContol__c.getValues('teamInstanceAfterTrigger')!= null ? AxtriaSalesIQTM__TriggerContol__c.getValues('teamInstanceAfterTrigger'):null;
        if(exeTrigger != null)
        {
            if(exeTrigger.AxtriaSalesIQTM__IsStopTrigger__c == false)
            {
                
                Set<id> scIdSet = new Set<id>();
                for(AxtriaSalesIQTM__Team_Instance__c teamIns : trigger.new)
                {
                    if(trigger.oldMap.get(teamIns.Id).AxtriaSalesIQTM__Alignment_Period__c == 'Future' && teamIns.AxtriaSalesIQTM__Alignment_Period__c == 'Current')
                    {
                        scIdSet.add(teamIns.AxtriaSalesIQTM__Scenario__c);
                    }
                }
                List<AxtriaSalesIQTM__Scenario__c> scList = new List<AxtriaSalesIQTM__Scenario__c>();
                for(Id scId:scIdSet)
                {
                    scList.add(new AxtriaSalesIQTM__Scenario__c(id=scId,AxtriaSalesIQTM__Last_Sync_Success_Date__c = null,AxtriaSalesIQTM__Last_Promote_Success_Date__c = null));
                }
                update scList;
            }
        }
    }    

}