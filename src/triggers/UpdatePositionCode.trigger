/**********************************************************************************************
@author     : Ritu Pandey
@date       : 8 May'2018
@description: This trigger is used to update the Position Code as per AZ's requirment. The code  
              logic is configured in position_code_matrix__c object.
@Revison(s) :
**********************************************************************************************/
trigger UpdatePositionCode on AxtriaSalesIQTM__Position__c (before insert, after insert, before update,after update) {

    list<string> lsPosIds = new list<string>();
    List<AxtriaSalesIQTM__Position__c> pos_new = new List<AxtriaSalesIQTM__Position__c>();
    List<AxtriaSalesIQTM__Position__c> pos_CurrentTI = new List<AxtriaSalesIQTM__Position__c>();
    List<AxtriaSalesIQTM__Position__c> pos_old = new List<AxtriaSalesIQTM__Position__c>();
    map<Id,AxtriaSalesIQTM__Position__c> pos_oldmap = new map<Id,AxtriaSalesIQTM__Position__c>();
    set<string> grpNames = new set<string>();
    set<string> deletedPosCodes = new set<string>();
    
    if(trigger.isDelete){
        for(AxtriaSalesIQTM__Position__c pos: trigger.old){
            deletedPosCodes.add(pos.AxtriaSalesIQTM__Client_Position_Code__c);
        }
        UpdatePositionCodeHandler.deletePositionChilds(deletedPosCodes);
    }
    system.debug('====deletedPosCodes====='+deletedPosCodes);
    
    List<Id> setTI = new List<Id>();
        
    if(!trigger.isDelete && AxtriaSalesIQTM__TriggerContol__c.getValues('UpdatePositionCode') != null && !AxtriaSalesIQTM__TriggerContol__c.getValues('UpdatePositionCode').AxtriaSalesIQTM__IsStopTrigger__c){ 
        for(AxtriaSalesIQTM__Position__c pos: trigger.new){
           setTI.add(pos.AxtriaSalesIQTM__Team_Instance__c);
        }
            
        Map<Id,AxtriaSalesIQTM__Team_Instance__c> mapTI = new  Map<Id,AxtriaSalesIQTM__Team_Instance__c>([Select Id, AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Request_Process_Stage__c,AxtriaSalesIQTM__Alignment_Period__c from AxtriaSalesIQTM__Team_Instance__c where Id IN : setTI]);
                
         
        for(AxtriaSalesIQTM__Position__c pos: trigger.new){
           if(mapTI.containsKey(pos.AxtriaSalesIQTM__Team_Instance__c) 
                && mapTI.get(pos.AxtriaSalesIQTM__Team_Instance__c)!=null)
           {
               String status;
               if(mapTI.get(pos.AxtriaSalesIQTM__Team_Instance__c).AxtriaSalesIQTM__Scenario__r!=null){ 
                    status = mapTI.get(pos.AxtriaSalesIQTM__Team_Instance__c).AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Request_Process_Stage__c;
               }else {
                    status = '';
               }
               if(status != 'In Queue' && status != 'In Progress'){
                    pos_new.add(pos);
                    if(mapTI.get(pos.AxtriaSalesIQTM__Team_Instance__c).AxtriaSalesIQTM__Alignment_Period__c == 'Current'){
                        pos_CurrentTI.add(pos);
                    }
                    if(Trigger.OldMap!=null && Trigger.OldMap.get(pos.id)!=null){
                        pos_oldmap.put(Trigger.OldMap.get(pos.id).id,Trigger.OldMap.get(pos.id));
                    }
                    lsPosIds.add(pos.id);
                    if(pos.Sales_Team_Attribute_MS__c!=null){
                        grpNames.addAll(pos.Sales_Team_Attribute_MS__c.split(';'));
                    }
               }
            }else{
                
            }
        }
        UpdatePositionCodeHandler.createNewGroups(grpNames, 'Sales Team Attribute');
        system.debug('pos_new++'+pos_new);
        system.debug('pos_oldmap++'+pos_oldmap);
        if(Trigger.isInsert ){
            if(Trigger.isBefore){
                UpdatePositionCodeHandler.updatePositionCode(pos_new);
            }
            
             if(Trigger.isAfter){
                UpdatePositionCodeHandler.createPositionProduct(pos_new);
                /*if(pos_CurrentTI.size()!=0){
                  system.debug('pos_CurrentTI++'+pos_CurrentTI);
                    UpdatePositionCodeHandler.createPositionHierarchy(pos_CurrentTI,'Insert',pos_oldmap);
                }*/
            } 
        }
       
        //Create and Delete AD Groups if an employee is assigned/unassigned from a position
        if(Trigger.isUpdate){
            system.debug('inside 1');
            
            if(Trigger.isBefore){
                UpdatePositionCodeHandler.updatePositionCode(pos_new);
                //added update to veeva position hierarchy
                
            }
            /*if(Trigger.isAfter){
                UpdatePositionCodeHandler.handleSalesTeamAttribute(pos_new,pos_oldmap);
                if(pos_CurrentTI.size()!=0){
                    UpdatePositionCodeHandler.createPositionHierarchy(pos_CurrentTI,'Updated',pos_oldmap);
                }
            }*/
        }
        
        //Delete Position Employee which is getting created by default with Employee = Null
        DeletePositionEmployee.deletePEwithEmployee(lsPosIds);
     }
    
}