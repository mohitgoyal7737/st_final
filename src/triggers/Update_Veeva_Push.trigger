trigger Update_Veeva_Push on Veeva_Job_Scheduling__c (after insert,After Update) {

    set<string> setLoad = new set<string>();
    set<string> setLoad_No_action = new set<string>();
    
    for(Veeva_Job_Scheduling__c VJS : Trigger.New){
        if(VJS.Load_Type__c == 'Full Load' || VJS.Load_Type__c == 'Delta'){
             setLoad.add(VJS.Country__c);
        }    
    }
    
     for(Veeva_Job_Scheduling__c VJS : Trigger.New){
        if(VJS.Load_Type__c == 'No Load'){
             setLoad_No_action.add(VJS.Country__c);
        }    
    }
    
    
    List<AxtriaSalesIQTM__Country__c> MM = New List<AxtriaSalesIQTM__Country__c>();
    
    For (AxtriaSalesIQTM__Country__c CT : [select id,Veeva_Push__c  from AxtriaSalesIQTM__Country__c  where id IN :setLoad ]){
        CT.Veeva_Push__c  = true;
       MM.add(CT);
    }   
   
    
     For (AxtriaSalesIQTM__Country__c CT : [select id,Veeva_Push__c  from AxtriaSalesIQTM__Country__c  where id IN :setLoad_No_action ]){
        CT.Veeva_Push__c  = false;
       MM.add(CT);
    }
    
    Update MM;
    
}