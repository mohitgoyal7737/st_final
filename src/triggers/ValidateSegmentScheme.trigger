trigger ValidateSegmentScheme on Segmentation_Scheme__c (before insert, before update) {
	if(AxtriaSalesIQTM__TriggerContol__c.getValues('ValidateSegmentScheme') != null  && AxtriaSalesIQTM__TriggerContol__c.getValues('ValidateSegmentScheme').AxtriaSalesIQTM__IsStopTrigger__c){
        return;
    }
    
    if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)){

    	Set<String> schemeNameSet = new Set<String>();
    	Set<String> countrySet = new Set<String>();
    	for(Segmentation_Scheme__c obj:Trigger.new){
    		schemeNameSet.add(obj.Scheme_Name__c);
    		countrySet.add(obj.Country__c);
    	}
    	SnTDMLSecurityUtil.printDebugMessage('schemeNameSet :: '+schemeNameSet);
    	SnTDMLSecurityUtil.printDebugMessage('countrySet :: '+ countrySet);

    	List<Segmentation_Scheme__c> segmentSchemeList = [Select Id,Name,Scheme_Name__c,Country__c,Segment_Name__c,Rank__c From Segmentation_Scheme__c where Scheme_Name__c IN: schemeNameSet and Country__c IN: countrySet WITH SECURITY_ENFORCED];
    	SnTDMLSecurityUtil.printDebugMessage('segmentSchemeList :: '+segmentSchemeList);
        Map<String, Map<String, Decimal>> schemeSegmentRankMap = new Map<String, Map<String, Decimal>>();
        for(Segmentation_Scheme__c obj:segmentSchemeList){
        	if(obj.Country__c != null && String.isNotBlank(obj.Scheme_Name__c) && String.isNotBlank(obj.Segment_Name__c)){
	        	Map<String, Decimal> segmentRankMap = new Map<String, Decimal>();
	            segmentRankMap.put(obj.Segment_Name__c, obj.Rank__c);
	            if(schemeSegmentRankMap.containsKey(obj.Scheme_Name__c + obj.Country__c)){
	            	segmentRankMap.putAll(schemeSegmentRankMap.get(obj.Scheme_Name__c + obj.Country__c));
	            }
	            schemeSegmentRankMap.put(obj.Scheme_Name__c + obj.Country__c, segmentRankMap); 
	        }
        }
        SnTDMLSecurityUtil.printDebugMessage('schemeSegmentRankMap :: '+schemeSegmentRankMap);

        for(Segmentation_Scheme__c obj:Trigger.new){
        	if(obj.Country__c == null || String.isBlank(obj.Scheme_Name__c) || String.isBlank(obj.Segment_Name__c)){
        		obj.addError(System.Label.Scheme_Mandatory_fields_blank_Msg);
        	}else{
        		if(schemeSegmentRankMap.containsKey(obj.Scheme_Name__c + obj.Country__c)){
        			Map<String, Decimal> segmentRankMap = new Map<String, Decimal>();
        			segmentRankMap.putAll(schemeSegmentRankMap.get(obj.Scheme_Name__c + obj.Country__c));
        			for(String segment:segmentRankMap.keySet()){
        				if(Trigger.isInsert && segmentRankMap.containsKey(obj.Segment_Name__c)){
        					obj.addError(System.Label.Duplicate_Segments_Error_Msg);
        				}
        			}
        			List<Decimal> rankLst = segmentRankMap.values();
        			SnTDMLSecurityUtil.printDebugMessage('rankLst :: '+rankLst);
        			if(obj.Rank__c !=null && rankLst!=null && rankLst.contains(obj.Rank__c)){
        				obj.addError(System.Label.Duplicate_ranks_are_not_allowed);
        			}
        		}
        	}
        }
    }
}