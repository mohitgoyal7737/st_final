trigger MeasureMasterTrigger on Measure_Master__c(after update)
{
 
	if(trigger.isUpdate)
	{
		
		List<Measure_Master__c> sntRuleList = new List<Measure_Master__c>();
		List<Measure_Master__c> mccpRuleList = new List<Measure_Master__c>();
		for(Measure_Master__c obj:Trigger.new){
			if(String.isNotBlank(obj.Rule_Type__c) && obj.Rule_Type__c == 'MCCP'){
				mccpRuleList.add(obj);
			}else{
				sntRuleList.add(obj);
			}
		}
		if(sntRuleList.size()>0){
			SnTDMLSecurityUtil.printDebugMessage('sntRuleList :: '+sntRuleList);
			Measure_Master_Trigger_Handler.businessRule(sntRuleList);
		}
		if(mccpRuleList.size()>0){
			SnTDMLSecurityUtil.printDebugMessage('Trigger.old :: '+Trigger.old);
			SnTDMLSecurityUtil.printDebugMessage('mccpRuleList :: '+mccpRuleList);
			Measure_Master_Trigger_Handler.mccpBusinessRule(Trigger.old, mccpRuleList);
		}
	}

}