trigger MatrixUnqiuename on Grid_Master__c (before insert)
{
    if(Trigger.isBefore && Trigger.isInsert)
    {
        Set<String> Lis = new Set<String>();
        Set<String> usedNames = new Set<String>();
        //String name = trigger.new[0].Name;
        for(Grid_Master__c L1 : Trigger.new)
        {
            usedNames.add(L1.name.toUpperCase());
        }

        List<Grid_Master__c> Li = [select id, name from Grid_Master__c where Name IN :usedNames];

        if(Li.size() > 0)
        {
            for(Integer i = 0, j = Li.size(); i < j; i++)
            {
                Li[i].adderror('Kindly Use Unique Names');
                Lis.add(Li[i].name.toUpperCase());
                //Li[i].name.adderror('Kindly Use Unique Names');
            }
        }

        SnTDMLSecurityUtil.printDebugMessage('hey li is ' + Lis);
        Boolean B;
        for(Grid_Master__c L1 : Trigger.new)
        {
            if(Lis.size() > 0)
            {
                B = Lis.contains(L1.name.toUpperCase());
                SnTDMLSecurityUtil.printDebugMessage('++++++ Name is ' + L1.name);
                SnTDMLSecurityUtil.printDebugMessage('++++++ Boolean B is  ' + B);
                if(B)
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Kindly Use Unique Names'));
                    L1.adderror('Kindly Use Unique Names');
                    SnTDMLSecurityUtil.printDebugMessage('Error' + Lis);
                    L1.name.adderror('Kindly Use Unique Names');
                    
                }
            }
        }
    }
}