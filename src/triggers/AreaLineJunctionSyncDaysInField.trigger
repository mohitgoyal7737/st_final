trigger AreaLineJunctionSyncDaysInField on AxtriaSalesIQTM__Position_Product__c(before update,After Update) {
    if(Trigger.isUpdate){
        if(Trigger.isAfter){
            AreaLineJuncSyncDaysInFieldTrgrHandler.UpdateArealinejunction(trigger.new);
        }
        if(Trigger.isBefore){
          if(AxtriaSalesIQTM__TriggerContol__c.getValues('PosProdInactivewithCallPlanTrgrHandler') != null  && !AxtriaSalesIQTM__TriggerContol__c.getValues('PosProdInactivewithCallPlanTrgrHandler').AxtriaSalesIQTM__IsStopTrigger__c) {
            PosProdInactivewithCallPlanTrgrHandler.InactivePosProd(trigger.new , trigger.oldMap);
           }
        }
    }
    
}