trigger AssignManagersOnCurrentEmployeeChange on AxtriaSalesIQTM__Position__c (after update) {
    list<AxtriaSalesIQTM__Position__c> employeeChangePosList = new list<AxtriaSalesIQTM__Position__c>();
    for(AxtriaSalesIQTM__Position__c pos : trigger.new){
    	//modified by Akanksha on 1/18/18 to include condition when parent of position is changed.
        if((trigger.oldMap.get(pos.id).AxtriaSalesIQTM__Employee__c != pos.AxtriaSalesIQTM__Employee__c)
        	|| (trigger.oldMap.get(pos.id).AxtriaSalesIQTM__Parent_Position__c != pos.AxtriaSalesIQTM__Parent_Position__c)
        ){		
        	system.debug('---trigger condition met---'+pos);
            employeeChangePosList.add(pos);            
        }
    }
    if(employeeChangePosList.size()>0){
        map<id,list<AxtriaSalesIQTM__Position__c>> parentChildPositionsMap = new map<id,list<AxtriaSalesIQTM__Position__c>>();
        for(AxtriaSalesIQTM__Position__c pos : [select id,AxtriaSalesIQTM__Parent_Position__c,AxtriaSalesIQTM__Employee__c,
        										AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__c,
        										AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee_p__c
        										from AxtriaSalesIQTM__Position__c]){
             if(!parentChildPositionsMap.containsKey(pos.AxtriaSalesIQTM__Parent_Position__c)){
                 parentChildPositionsMap.put(pos.AxtriaSalesIQTM__Parent_Position__c,new list<AxtriaSalesIQTM__Position__c>());    
             }
                 parentChildPositionsMap.get(pos.AxtriaSalesIQTM__Parent_Position__c).add(pos);
        }
        
        list<AxtriaSalesIQTM__Employee__c> toBeUpdatedEmployeeList = new list<AxtriaSalesIQTM__Employee__c>();
        for(AxtriaSalesIQTM__Position__c position :employeeChangePosList){
        	if(trigger.oldMap.get(position.id).AxtriaSalesIQTM__Employee__c != position.AxtriaSalesIQTM__Employee__c){
	            if(parentChildPositionsMap.containsKey(position.id)){
	                for(AxtriaSalesIQTM__Position__c childPosition :parentChildPositionsMap.get(position.id)){
	                	system.debug('---trigger condition 1---'+childPosition);
	                	
		                    if(childPosition.AxtriaSalesIQTM__Employee__c != null){
		                        toBeUpdatedEmployeeList.add(new AxtriaSalesIQTM__Employee__c(id = childPosition.AxtriaSalesIQTM__Employee__c,
		                                                                                     AxtriaSalesIQTM__Manager__c = position.AxtriaSalesIQTM__Employee__c,
		                                                                                     AxtriaSalesIQTM__Emp_Supervisor_ID__c = position.AxtriaSalesIQTM__Employee_p__c));    
		                    }
	                }
	            }
        	}
        	else if(trigger.oldMap.get(position.id).AxtriaSalesIQTM__Parent_Position__c != position.AxtriaSalesIQTM__Parent_Position__c){
        		if(parentChildPositionsMap.containsKey(position.AxtriaSalesIQTM__Parent_Position__c) 
        			&& parentChildPositionsMap.get(position.AxtriaSalesIQTM__Parent_Position__c) != null){
        				
	                AxtriaSalesIQTM__Position__c childPosition = parentChildPositionsMap.get(position.AxtriaSalesIQTM__Parent_Position__c)[0];
	                	system.debug('---trigger condition 2---'+childPosition);
	                toBeUpdatedEmployeeList.add(new AxtriaSalesIQTM__Employee__c(id = position.AxtriaSalesIQTM__Employee__c,
	                         AxtriaSalesIQTM__Manager__c = childPosition.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__c,
	                         AxtriaSalesIQTM__Emp_Supervisor_ID__c = childPosition.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee_p__c));    
	                
        		}
        		else{	//parent position is blank now.
        			system.debug('---trigger condition 2 else ---'+position);
	                toBeUpdatedEmployeeList.add(new AxtriaSalesIQTM__Employee__c(id = position.AxtriaSalesIQTM__Employee__c,
                     AxtriaSalesIQTM__Manager__c = null,
                     AxtriaSalesIQTM__Emp_Supervisor_ID__c = ''));    
	         
        		}
	    	}    
        }
        system.debug('---trigger toBeUpdatedEmployeeList---'+toBeUpdatedEmployeeList);
        set<AxtriaSalesIQTM__Employee__c> removeDups = new set<AxtriaSalesIQTM__Employee__c>();
        removeDups.addAll(toBeUpdatedEmployeeList);
        toBeUpdatedEmployeeList = new list<AxtriaSalesIQTM__Employee__c>();
        toBeUpdatedEmployeeList.addAll(removeDups);
        update toBeUpdatedEmployeeList;
    }         
}