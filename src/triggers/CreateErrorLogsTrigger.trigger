/************************************************************************************************
Name        :   CreateErrorLogsTrigger.cls
Author      :   Ayush Rastogi
Description :   This Triger will fire to log error in loger object,when any event is published using SnT_Error_Logger__e
Date        :   12 oct 2020
                
************************************************************************************************/
trigger CreateErrorLogsTrigger on SnT_Error_Logger__e(after insert) {
    List<AxtriaSalesIQTM__SalesIQ_Logger__c> logs = new List<AxtriaSalesIQTM__SalesIQ_Logger__c>();  
    
    for(SnT_Error_Logger__e errlog : trigger.new)
    {
       AxtriaSalesIQTM__SalesIQ_Logger__c logger = new AxtriaSalesIQTM__SalesIQ_Logger__c();
           logger.AxtriaSalesIQTM__Message__c = errlog.Message__c;
           logger.AxtriaSalesIQTM__Exception_Type__c = errlog.Exception_Type__c;
           logger.AxtriaSalesIQTM__Line_Number__c = errlog.Line_Number__c;
           logger.AxtriaSalesIQTM__Module__c = errlog.Module__c;
           logger.AxtriaSalesIQTM__Stack_Trace__c = errlog.Stack_Trace__c;
           logger.AxtriaSalesIQTM__Type__c  =errlog.Type__c;
           if(errlog.BusinessRule__c!=null){
            if(errlog.BusinessRule__c == 'None'){
              logger.BusinessRule__c = null;
            }else{
              logger.BusinessRule__c = errlog.BusinessRule__c;
            }
           }
           if(errlog.TeamInstance__c!=null){
            if(errlog.TeamInstance__c == 'None'){
              logger.Destination_Team_Instance__c = null;
            }else{
              logger.Destination_Team_Instance__c = errlog.TeamInstance__c;
            }
           }
           if(errlog.Grid__c!=null){
            if(errlog.Grid__c == 'None'){
              logger.GridID__c = null;
            }else{
              logger.GridID__c = errlog.Grid__c;
            }
           }
           //RecordtypeId = Schema.SObjectType.AxtriaSalesIQTM__SalesIQ_Logger__c.getRecordTypeInfosByName().get('Error').getRecordTypeId()
        logs.add(logger);
    }
    insert logs;
    //SnTDMLSecurityUtil.insertRecord(AxtriaSalesIQTM__SalesIQ_Logger__c.SObjectType, logs);
    

}