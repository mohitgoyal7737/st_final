trigger UpdateVeevaPush on AxtriaSalesIQTM__Country__c(before update) {

	if(AxtriaSalesIQTM__TriggerContol__c.getValues('UpdateVeevaPush') != null && !AxtriaSalesIQTM__TriggerContol__c.getValues('UpdateVeevaPush').AxtriaSalesIQTM__IsStopTrigger__c){
		
		for(AxtriaSalesIQTM__Country__c cs : Trigger.new){
			if((cs.Load_Type__c == 'Full Load' && Trigger.oldMap.get(cs.ID).Load_Type__c != 'Full Load') || (cs.Load_Type__c == 'Delta' && Trigger.oldMap.get(cs.ID).Load_Type__c != 'Delta')){
				cs.Veeva_Push__c = true;
			}
			if(cs.Load_Type__c == 'No Load' && Trigger.oldMap.get(cs.ID).Load_Type__c != 'No Load'){
				cs.Veeva_Push__c = false;
			}
		}	
	}
	
    
}