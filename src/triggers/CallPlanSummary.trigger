trigger CallPlanSummary on AxtriaSalesIQTM__Position_Account_Call_Plan__c (after insert, after update, before update, before insert) {
  if(AxtriaSalesIQTM__TriggerContol__c.getValues('CallPlanSummaryTrigger') != null  && !AxtriaSalesIQTM__TriggerContol__c.getValues('CallPlanSummaryTrigger').AxtriaSalesIQTM__IsStopTrigger__c) {
          system.debug('===========CallPlanSummary Trigger is Executing===========');
      if(CallPlanSummaryTriggerHandler.execute_trigger && Trigger.isAfter){
        if(Trigger.isInsert){
          try
          {
            CallPlanSummaryTriggerHandler.afterInsert(Trigger.new, Trigger.old, Trigger.OldMap);
          }
          catch(Exception ex)
          {
            system.debug('Errror ');
          }
            
        }else if(Trigger.isUpdate){
          try
          {
            CallPlanSummaryTriggerHandler.afterUpdate(Trigger.new, Trigger.old, Trigger.OldMap);  
          }
          catch(Exception ex)
          {
            system.debug('Errror ');
          }
            
        }
        //CallPlanSummaryTriggerHandler.execute_trigger = false;
      }
      else if(Trigger.isBefore){
        if(Trigger.isUpdate || Trigger.isInsert){
          if(Trigger.isUpdate)
              {
            System.debug('Trigger update ');
            Updatepacpsequenceexisting obj = new Updatepacpsequenceexisting();
            obj.Updatepacpsequenceexisting(Trigger.new);
              }
          else{
              System.debug('Trigger insert ');
              System.debug('calling updateChannelPreferenceinPACP---');
              updateChannelPreferenceinPACP objj = new updateChannelPreferenceinPACP();
              objj.updateChannelPreferenceinPACP(Trigger.new,true);
          }
          System.debug('calling updateEffectiveCallsinPACP---');
          updateEffectiveCallsinPACP obj = new updateEffectiveCallsinPACP();
          obj.updateEffectiveCallsinPACP(Trigger.new);

         
        }
      }
    }
}