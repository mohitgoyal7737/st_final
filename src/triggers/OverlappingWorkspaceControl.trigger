trigger OverlappingWorkspaceControl on AxtriaSalesIQTM__Workspace__c (before Insert) {

  public set<string> cntry = new set<string>();
  public boolean myCCVal = true;

  AxtriaSalesIQTM__TriggerContol__c myCS1 = AxtriaSalesIQTM__TriggerContol__c.getValues('OverlappingWorkspace');
      
   if(!Test.isRunningTest())
   {
     myCCVal = myCS1.AxtriaSalesIQTM__IsStopTrigger__c ; 
   }
   else
   {
     myCCVal = true;   
   }
    
     if(myCCVal != true)

     {

      String workspaceName; Id userPersistanceCountryId; Id workspaceCountryId;
      Date workspaceStartDate;
      Date workspaceEndDate;
      AxtriaSalesIQTM__Workspace__c workspace;
    
    //INSERT COUNTRY IF AND VALIDATE FOR DUPLICATE WORKSPACE 
      if(Trigger.isInsert && Trigger.isBefore){
        for(AxtriaSalesIQTM__Workspace__c ws : trigger.new){
          workspaceName = ws.Name;
          workspaceCountryId = ws.AxtriaSalesIQTM__Country__c ;
          workspace = ws;
          workspaceStartDate = ws.AxtriaSalesIQTM__Workspace_Start_Date__c;
          workspaceEndDate = ws.AxtriaSalesIQTM__Workspace_End_Date__c;
        }
      //GET COUNTRY ID OF LOGGED IN USER WHEN USER BEFORE INSERTING WORKSPACE
        list<AxtriaSalesIQTM__User_Persistence__c> userPersistenceList = SalesIQUtility.getUserPersistenceByUserID(UserInfo.getUserId());
        if(userPersistenceList != null && userPersistenceList.size() > 0){
            if(userPersistenceList[0].AxtriaSalesIQTM__Workspace_Country__c!=null){
              userPersistanceCountryId = userPersistenceList[0].AxtriaSalesIQTM__Workspace_Country__c;
            }
        }
      //SET COUNTRY ID FROM USER PERSISTANCE ON CREATING NEW WORKSPACE
        if( userPersistanceCountryId!=null ){ 
          for(AxtriaSalesIQTM__Workspace__c ws : trigger.new){
              //ADDED CHECK IF THE WORKSPACE IN CUSTOMER EXCLUSION 
              if( !ws.AxtriaSalesIQTM__Country__r.AxtriaSalesIQTM__Enable_Account_Exclusion__c && !ws.AxtriaSalesIQTM__isUniversal__c ){
                ws.AxtriaSalesIQTM__Country__c   = userPersistanceCountryId;   
              }
          }
        }
      //CHECK DUPLICATE WORKSPACE NAME
          Boolean isDuplicate = ScenarioTriggerHandler.checkDuplicateWorkspaceName(userPersistanceCountryId,workspaceCountryId,workspaceName,workspaceStartDate,workspaceEndDate);
          if(isDuplicate){
              workspace.addError('Overlapping dates with an existing workspace.Please check start date and end date');
          }
        }




     }

}