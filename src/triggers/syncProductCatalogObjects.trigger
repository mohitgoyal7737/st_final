trigger syncProductCatalogObjects on AxtriaSalesIQTM__Product__c(after insert, after update) 
{
	if(trigger.isInsert)
		{
			Set<String>Prodname = new Set<String>();
			Set<String> teamInstanceSet = new Set<String>();
			for(AxtriaSalesIQTM__Product__c pm : Trigger.new)
			{
				Prodname.add(pm.name);
				teamInstanceSet.add(pm.Team_Instance__c);
			}
			List<Product_Catalog__c> pcList = [Select Id,Team__c,Name,Product_Code__c,Team_Instance__c
																	from Product_Catalog__c WHERE name IN:Prodname ];
			set<String>Uniquekeys = new set<String>();
			for(Product_Catalog__c PC : pcList)
			{
				Uniquekeys.add(PC.Name+'_'+PC.Team_Instance__c);
			}

			list<Product_Catalog__c> prodcat = new list<Product_Catalog__c>();
			Map<String,String> teamInsttoCountry = new  Map<String,String>();

            List<AxtriaSalesIQTM__Team_Instance__c> teaminst = [Select id, AxtriaSalesIQTM__Country__c, Name from AxtriaSalesIQTM__Team_Instance__c where id in:teamInstanceSet];
            for(AxtriaSalesIQTM__Team_Instance__c teamInstances :teaminst)
            {
                  teamInsttoCountry.put(teamInstances.Id,teamInstances.AxtriaSalesIQTM__Country__c);
            }
            
			for(AxtriaSalesIQTM__Product__c pm : Trigger.new)
			{
				if(!Uniquekeys.contains(pm.name+'_'+pm.Team_Instance__c))
				{
					Product_Catalog__c pc = new Product_Catalog__c ();
					//pc.Business_Unit__c       =  pm.Team__r.AxtriaSalesIQTM__Business_Unit__c;
					pc.External_ID__c         =  pm.AxtriaSalesIQTM__External_ID__c;
					pc.Team__c                =  pm.Team__c;
					pc.Team_Instance__c		  =  pm.Team_Instance__c;
					pc.Name                   =  pm.name;
					pc.Product_Code__c        =  pm.AxtriaSalesIQTM__Product_Code__c;
                    pc.Veeva_External_ID__c = pm.AxtriaSalesIQTM__Product_Code__c;
					pc.Product_Description__c =  pm.AxtriaSalesIQTM__Product_Description__c;
					pc.Product_Type__c        =  pm.AxtriaSalesIQTM__Product_Type__c;
					pc.IsActive__c            =  pm.AxtriaSalesIQTM__IsActive__c;
					pc.Effective_End_Date__c  =  pm.AxtriaSalesIQTM__Effective_End_Date__c;
					pc.Effective_Start_Date__c=  pm.AxtriaSalesIQTM__Effective_Start_Date__c;
					pc.Product_Master__c	  =  pm.UniqueSFDCID__c;
					pc.Country_Lookup__c=teamInsttoCountry.get( pm.Team_Instance__c);
					prodcat.add(pc);
				}
			}
			insert prodcat;
			//purge begins
			/*List<Brand_Team_Instance__c>brandteamlist = [Select id,Name,Brand__c,Brand__r.Name,Team_Instance__c from Brand_Team_Instance__c];
			Set<String>Brand_TeamInstance = new set<String>();
			for(Brand_Team_Instance__c br : brandteamlist)
			{
			    Brand_TeamInstance.add(br.Brand__c+'_'+br.Team_Instance__c);
			}
			List<Brand_Team_Instance__c>insertbrdteam = new list<Brand_Team_Instance__c>();
			for(Product_Catalog__c prod : prodcat)
			{
			    if(!Brand_TeamInstance.contains(Prod.Id+'_'+prod.Team_Instance__c))
			    {
			        Brand_Team_Instance__c newBt = new Brand_Team_Instance__c();
			        newbt.Name = prod.Name;
			        newbt.Brand__c = prod.Id;
			        newbt.Team_Instance__c = prod.Team_Instance__c;
			        insertbrdteam.add(newbt);
			    }
			}
			insert insertbrdteam;
			*/
			//purge ends

		}
    	if(trigger.isUpdate)
    	{
    		List<String>Productmasterlist = new list<String>();
    		Set<String> teamInstanceSet = new Set<String>();
    		map<String,AxtriaSalesIQTM__Product__c>mastermap = new map<String,AxtriaSalesIQTM__Product__c>();

    		for(AxtriaSalesIQTM__Product__c pm : Trigger.new)
    		{
    			Productmasterlist.add(pm.UniqueSFDCID__c);
    			teamInstanceSet.add(pm.Team_Instance__c);

    			if(!mastermap.containsKey(pm.UniqueSFDCID__c))
    			{
    				mastermap.put(pm.UniqueSFDCID__c,pm);
    			}
    		}
    		Map<String,String> teamInsttoCountry = new  Map<String,String>();

            List<AxtriaSalesIQTM__Team_Instance__c> teaminst = [Select id, AxtriaSalesIQTM__Country__c, Name from AxtriaSalesIQTM__Team_Instance__c where id in:teamInstanceSet];
            for(AxtriaSalesIQTM__Team_Instance__c teamInstances :teaminst)
            {
                  teamInsttoCountry.put(teamInstances.Id,teamInstances.AxtriaSalesIQTM__Country__c);
            }
    		list<Product_Catalog__c> prodcatlogue = [Select Id,External_ID__c,Team__c,Team_Instance__c,Product_Master__c,Name,Product_Code__c,Product_Description__c,
																	Product_Type__c,IsActive__c,Effective_End_Date__c,Effective_Start_Date__c
																	from Product_Catalog__c WHERE Product_Master__c = :Productmasterlist];

    		list<Product_Catalog__c> prodcat = new list<Product_Catalog__c>();
    		set<String>prcatlogids = new set<String>(); 
    		for(Product_Catalog__c pc : prodcatlogue)
    		{
    			AxtriaSalesIQTM__Product__c pm  = mastermap.get(pc.Product_Master__c);
    			prcatlogids.add(pc.id);
    			pc.External_ID__c         =  pm.AxtriaSalesIQTM__External_ID__c;
				pc.Team__c                =  pm.Team__c;
				pc.Name                   =  pm.name;
				pc.Product_Code__c        =  pm.AxtriaSalesIQTM__Product_Code__c;
                pc.Veeva_External_ID__c   = pm.AxtriaSalesIQTM__Product_Code__c;
				pc.Product_Description__c =  pm.AxtriaSalesIQTM__Product_Description__c;
				pc.Product_Type__c        =  pm.AxtriaSalesIQTM__Product_Type__c;
				pc.IsActive__c            =  pm.AxtriaSalesIQTM__IsActive__c;
				pc.Effective_End_Date__c  =  pm.AxtriaSalesIQTM__Effective_End_Date__c;
				pc.Effective_Start_Date__c=  pm.AxtriaSalesIQTM__Effective_Start_Date__c;
				pc.Team_Instance__c	       =  pm.Team_Instance__c;
				pc.Country_Lookup__c=teamInsttoCountry.get(pm.Team_Instance__c);
				prodcat.add(pc);
    		}
    		if(prodcat.size() > 0)
    		{
				update prodcat;
			}
            
           //Not necessary as there is lookup between the product catlogue and barand teaminstance
            
			//purge begins
            /*List<Brand_Team_Instance__c>brandteamlist = [Select id,Name,Brand__c,Brand__r.Name,Team_Instance__c from Brand_Team_Instance__c where Brand__c IN:prcatlogids];
            for(Brand_Team_Instance__c brandti : brandteamlist)
            {
                brandti.name= brandti.Brand__r.Name;
            }
            Update brandteamlist;*/
            //purge ends
			

			/*for(AxtriaSalesIQTM__Product__c pm : Trigger.new){

				Product_Catalog__c pc = [Select Id,External_ID__c,Team__c,Name,Product_Code__c,Product_Description__c,
																	Product_Type__c,IsActive__c,Effective_End_Date__c,Effective_Start_Date__c
																	from Product_Catalog__c WHERE Name = :pm.Name ];
					//pc.Business_Unit__c       =  pm.Team__r.AxtriaSalesIQTM__Business_Unit__c;
					pc.External_ID__c         =  pm.AxtriaSalesIQTM__External_ID__c;
					pc.Team__c                =  pm.Team__c;
					pc.Name                   =  pm.name;
					pc.Product_Code__c        =  pm.AxtriaSalesIQTM__Product_Code__c;
					pc.Product_Description__c =  pm.AxtriaSalesIQTM__Product_Description__c;
					pc.Product_Type__c        =  pm.AxtriaSalesIQTM__Product_Type__c;
					pc.IsActive__c            =  pm.AxtriaSalesIQTM__IsActive__c;
					pc.Effective_End_Date__c  =  pm.AxtriaSalesIQTM__Effective_End_Date__c;
					pc.Effective_Start_Date__c=  pm.AxtriaSalesIQTM__Effective_Start_Date__c;

				 prodcat.add(pc);
			}*/
			

    	}
}