trigger ProductCatalogTrigger on Product_Catalog__c(before insert,before update, after insert ,after update) {
	
	public AxtriaSalesIQTM__TriggerContol__c exeTrigger = new AxtriaSalesIQTM__TriggerContol__c();

	//Changed by HT(A0994) on 3rd June 2020
	//if(trigger.isBefore && trigger.isUpdate && trigger.isInsert)
	if(trigger.isBefore)
	{
		
	    exeTrigger = AxtriaSalesIQTM__TriggerContol__c.getValues('PopulateDetailGroupOnProduct')!= null ? AxtriaSalesIQTM__TriggerContol__c.getValues('PopulateDetailGroupOnProduct'):null;
	    
	    system.debug('execute trigger' +exeTrigger );

	    if(exeTrigger != null)
	    {
	     	if(exeTrigger.AxtriaSalesIQTM__IsStopTrigger__c != true)
	     	{
				if(Trigger.isInsert || Trigger.isUpdate)
			    {
			        PopulateDetailGroupOnProductHandler.upsertDetailGroup(trigger.new);
			    }
			}
	    }

		if(trigger.isInsert)
		{
			system.debug('in here ');
			ProductExtIdTriggerHandler.productExternalId(Trigger.new);
			system.debug('trigger.new--'+Trigger.new);
		}
		else if(Trigger.isUpdate)
		{
			ProductExtIdTriggerHandler.productExternalId(Trigger.new);
		}		
	}
	else if(trigger.isAfter)
	{
		if(Trigger.isInsert)
		{
			Populate_Position_Product_Handler_PC.populatePosition(trigger.new);
		}
		else if(Trigger.isDelete)
		{
			Populate_Position_Product_Handler_PC.deletePosition(trigger.old);	
		}
	}


}