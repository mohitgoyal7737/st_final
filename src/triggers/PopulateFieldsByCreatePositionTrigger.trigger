trigger PopulateFieldsByCreatePositionTrigger on AxtriaSalesIQTM__Position__c(before insert) 
{

	System.debug('---PopulateFieldsByCreatePositionTrigger  ---');

    public AxtriaSalesIQTM__TriggerContol__c exeTrigger = new AxtriaSalesIQTM__TriggerContol__c();

    exeTrigger = AxtriaSalesIQTM__TriggerContol__c.getValues('PopulateFieldsByCreatePosition')!= null ? AxtriaSalesIQTM__TriggerContol__c.getValues('PopulateFieldsByCreatePosition'):null;


    if(exeTrigger != null)
    {
        if(exeTrigger.AxtriaSalesIQTM__IsStopTrigger__c != true)
        {
            System.debug('Call trigger');
	
        	for(AxtriaSalesIQTM__Position__c rec : Trigger.New)
            {
            	System.debug('---rec  ---' +rec);

            	if(rec.AxtriaARSnT__Sales_Team_Attribute_MS__c == null || rec.AxtriaARSnT__Channel_AZ__c == null || rec.AxtriaARSnT__Position_Description__c == null)
            	{
            		String clientPosCode = rec.AxtriaSalesIQTM__Client_Position_Code__c;
            		String teamName = rec.AxtriaSalesIQTM__Team_iD__c;
            		String teamIns = rec.AxtriaSalesIQTM__Team_Instance__c;

        			System.debug('---clientPosCode  ---' +clientPosCode);
            		System.debug('---teamName  ---' +teamName);
            		System.debug('---teamIns  ---' +teamIns);

            		List<AxtriaSalesIQTM__Team_Instance__c> teamInsList = [Select Id, AxtriaSalesIQTM__IC_EffEndDate__c, AxtriaSalesIQTM__Alignment_Period__c, AxtriaSalesIQTM__IC_EffstartDate__c from AxtriaSalesIQTM__Team_Instance__c where Id = :teamIns and AxtriaSalesIQTM__Scenario__c != null];

            		System.debug('---teamInsList  ---' +teamInsList);

            		String alignmentPeriod = '';
            		Date futureScenarioDate = null;
            		String teamInsFuture = '';

            		System.debug('---alignmentPeriod  ---' +alignmentPeriod);

            		if(teamInsList != null && teamInsList.size() > 0)
            		{
            			alignmentPeriod = teamInsList[0].AxtriaSalesIQTM__Alignment_Period__c;

            			if(alignmentPeriod != null)
            			{
        	    			if(alignmentPeriod == 'Current')
        	    				futureScenarioDate = teamInsList[0].AxtriaSalesIQTM__IC_EffEndDate__c != null?teamInsList[0].AxtriaSalesIQTM__IC_EffEndDate__c.addDays(1):null;
            			}
            		}

            		System.debug('---futureScenarioDate  ---' +futureScenarioDate);

            		if(futureScenarioDate != null)
            		{
            			List<AxtriaSalesIQTM__Team_Instance__c> futureteamInsList = [Select Id, AxtriaSalesIQTM__IC_EffEndDate__c, AxtriaSalesIQTM__Alignment_Period__c, AxtriaSalesIQTM__IC_EffstartDate__c from AxtriaSalesIQTM__Team_Instance__c where AxtriaSalesIQTM__IC_EffstartDate__c = :futureScenarioDate and AxtriaSalesIQTM__Scenario__c != null and AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Employee_Assignment__c = true and AxtriaSalesIQTM__Team__c = :teamName];

            			if(futureteamInsList != null && futureteamInsList.size() > 0)
        					teamInsFuture = futureteamInsList[0].Id;
            		}

            		System.debug('---teamInsFuture  ---' +teamInsFuture);

            		List<AxtriaSalesIQTM__Position__c> masterPosList = [Select Id,AxtriaARSnT__Sales_Team_Attribute_MS__c,AxtriaARSnT__Channel_AZ__c,AxtriaARSnT__Position_Description__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Client_Position_Code__c = :clientPosCode and AxtriaSalesIQTM__Team_iD__c = :teamName and AxtriaSalesIQTM__Team_Instance__c = :teamInsFuture];

            		System.debug('---masterPosList  ---' +masterPosList);
            		

            		if(masterPosList != null && masterPosList.size() > 0)
            		{

            			rec.AxtriaARSnT__Sales_Team_Attribute_MS__c = masterPosList[0].AxtriaARSnT__Sales_Team_Attribute_MS__c;
        	    		rec.AxtriaARSnT__Channel_AZ__c = masterPosList[0].AxtriaARSnT__Channel_AZ__c;
        	    		rec.AxtriaARSnT__Position_Description__c = masterPosList[0].AxtriaARSnT__Position_Description__c;
            		}

            		System.debug('---rec after update  ---' +rec);
            		

            	}
            }
        }
    }
    
}