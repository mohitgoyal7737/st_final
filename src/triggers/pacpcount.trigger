trigger pacpcount on AxtriaSalesIQTM__Position_Account_Call_Plan__c (after insert,after delete,after undelete) {
    List<id>posid =new list<Id>();
    if(Trigger.isInsert || Trigger.isUndelete){
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : Trigger.new){
            posid.add(pacp.AxtriaSalesIQTM__Position__c);
        }
    }
    if(Trigger.isDelete){
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : Trigger.old){
            posid.add(pacp.AxtriaSalesIQTM__Position__c);
        }
    }
    list<AxtriaSalesIQTM__Position__c> poslist = new list<AxtriaSalesIQTM__Position__c>();
}