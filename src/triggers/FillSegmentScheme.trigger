trigger FillSegmentScheme on MCCP_CNProd__c (before insert) {

    if(AxtriaSalesIQTM__TriggerContol__c.getValues('FillSegmentScheme') != null  && AxtriaSalesIQTM__TriggerContol__c.getValues('FillSegmentScheme').AxtriaSalesIQTM__IsStopTrigger__c){
        return;
    }

    if(Trigger.isBefore && Trigger.isInsert){
        String mccpRuleId = '';
        for(integer i=0;i<Trigger.new.size();i++){
            if(String.isNotBlank(Trigger.new[i].Call_Plan__c)){
                mccpRuleId = Trigger.new[i].Call_Plan__c;
                break;
            }
        }
        SnTDMLSecurityUtil.printDebugMessage('mccpRuleId :: '+mccpRuleId);
        if(String.isNotBlank(mccpRuleId)){
            List<Measure_Master__c> mccpRules = [Select Id, Team_Instance__r.SegmentScheme__c From Measure_Master__c where Id=:mccpRuleId and Rule_Type__c = 'MCCP' WITH SECURITY_ENFORCED];
            String segmentScheme = '';
            for(integer i=0;i<mccpRules.size();i++){
                if(String.isNotBlank(mccpRules[i].Team_Instance__r.SegmentScheme__c)){
                    segmentScheme = mccpRules[i].Team_Instance__r.SegmentScheme__c;
                    break;
                }
            }
            if(String.isNotBlank(segmentScheme)){
                Id segmentRecordTypeId = Schema.SObjectType.MCCP_CNProd__c.getRecordTypeInfosByName().get('Segment').getRecordTypeId();
                SnTDMLSecurityUtil.printDebugMessage('segmentScheme :: '+segmentScheme);
                for(MCCP_CNProd__c obj:Trigger.new){
                    SnTDMLSecurityUtil.printDebugMessage('record type Id :: '+obj.RecordTypeId);
                    if(obj.RecordTypeId == segmentRecordTypeId){
                        obj.SegmentScheme__c = segmentScheme;
                    }
                }
            }
        }
    }
}