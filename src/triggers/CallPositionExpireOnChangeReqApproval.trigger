trigger CallPositionExpireOnChangeReqApproval on AxtriaSalesIQTM__Change_Request__c(before insert,after update) {

    System.debug('---CallPositionExpireOnChangeReqApproval  ---');
    if(trigger.isInsert){
        System.debug('change request object - '+trigger.new);
        System.debug('position -- '+trigger.new[0].AxtriaSalesIQTM__Source_Position__c);



    }

    if(Trigger.isUpdate)
    {
        String positionID;
        Date endDate;
        list<AxtriaSalesIQTM__Position__c> positionList = new list<AxtriaSalesIQTM__Position__c>();
        System.debug('Trigger.New CallPositionExpireOnChangeReqApproval  outside package - ' + Trigger.New);
        AxtriaSalesIQTM__CR_Position__c crPosObj = new AxtriaSalesIQTM__CR_Position__c();

        for(AxtriaSalesIQTM__Change_Request__c rec : Trigger.New)
        {
            if(rec.AxtriaSalesIQTM__Status__c == 'Approved' && rec.AxtriaSalesIQTM__Request_Type_Change__c == 'Delete Position' && rec.AxtriaSalesIQTM__Source_Position__c != null)
            {
                positionID = rec.AxtriaSalesIQTM__Source_Position__c;

                crPosObj = [select AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Effective_End_Date__c from AxtriaSalesIQTM__CR_Position__c where AxtriaSalesIQTM__Change_Request__c =: rec.Id and AxtriaSalesIQTM__Position__c =: positionID];

                System.debug('crPosObj SNT ::::::::::::' +crPosObj);
                positionList = [select Id,AxtriaSalesIQTM__IsMaster__c, AxtriaSalesIQTM__Assignment_Status__c,AxtriaSalesIQTM__inactive__c,AxtriaSalesIQTM__Employee__c,AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team_Cycle_Name__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c from AxtriaSalesIQTM__Position__c where id=: positionID]; 


                System.debug('positionList :'+positionList);
                 
               
                
            }
        }

        if(positionList!= null && positionList.size() >0){

                System.debug('positionID : '+positionID);
                AxtriaSalesIQTM__Position__c positionToUpdate = new AxtriaSalesIQTM__Position__c();
                positionToUpdate = [select AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__inactive__c,AxtriaSalesIQTM__Employee__c,AxtriaSalesIQTM__Assignment_status__c from AxtriaSalesIQTM__Position__c where id=: positionID];

                System.debug('positionToUpdate query ::::::::::::' +positionToUpdate);
                
                System.debug('System.today() ::::::::::::' +System.today());

                positionToUpdate.AxtriaSalesIQTM__Effective_End_Date__c = crPosObj.AxtriaSalesIQTM__Effective_End_Date__c;
                endDate = crPosObj.AxtriaSalesIQTM__Effective_End_Date__c;

                System.debug('Change request effective end date - '+crPosObj.AxtriaSalesIQTM__Effective_End_Date__c);
                    
                System.debug('positionToUpdate.AxtriaSalesIQTM__Effective_End_Date__c ::::::::::::' +positionToUpdate.AxtriaSalesIQTM__Effective_End_Date__c);
                //ProcessExpiredPositionBatchSNT.updatePosition(positionList);
                if(crPosObj.AxtriaSalesIQTM__Effective_End_Date__c < System.today())
                    positionToUpdate.AxtriaSalesIQTM__inactive__c = true;

                System.debug('positionToUpdate after change ::::::::::::' +positionToUpdate);

                list<AxtriaSalesIQTM__Position_Employee__c> posEmpUpdate = new list<AxtriaSalesIQTM__Position_Employee__c>();
                map<Id,list<AxtriaSalesIQTM__Position_Employee__c>> positionAssignmentMap = new map<Id,list<AxtriaSalesIQTM__Position_Employee__c>>();
                for(AxtriaSalesIQTM__Position_Employee__c pe : [Select Id, AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Assignment_status__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Employee__c,AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Field_Status__c,AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Current_Territory__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c,  AxtriaSalesIQTM__Employee__c From AxtriaSalesIQTM__Position_Employee__c where AxtriaSalesIQTM__Assignment_Type__c = 'Primary' and 
                                               AxtriaSalesIQTM__Effective_End_Date__c >=: System.today() and AxtriaSalesIQTM__Effective_Start_Date__c <=: System.today() and AxtriaSalesIQTM__Employee__c != null and 
                                               AxtriaSalesIQTM__Position__c  =:positionID order by LastModifiedDate desc]){
                    positionAssignmentMap.put(pe.AxtriaSalesIQTM__Position__c,new list<AxtriaSalesIQTM__Position_Employee__c>{pe});

                    if(crPosObj.AxtriaSalesIQTM__Effective_End_Date__c < System.today()){
                        positionToUpdate.AxtriaSalesIQTM__Assignment_status__c = 'Vacant';
                        positionToUpdate.AxtriaSalesIQTM__Employee__c = null;
                        //.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Current_Territory__c = null;
                        //pe.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Field_Status__c = 'Unassigned';
                        posEmpUpdate.add(pe);

                    }
                    system.debug('#### pe.Position__c : '+pe.AxtriaSalesIQTM__Position__c);
                }

                System.debug('positionToUpdate just before Update call ::::::::::::' +positionToUpdate);
                System.debug('posEmpUpdate ::::::::::::' +posEmpUpdate);

                update positionToUpdate;
                if(posEmpUpdate.size() >0)
                    update posEmpUpdate;

                
                System.debug('---- calling ProcessCRAssignmentBatch from trigger Starts ---- ');
                ProcessCRAssignmentBatchSNT b1 = new ProcessCRAssignmentBatchSNT(new set<ID>{positionID},'Position_Account__c');
                ProcessCRAssignmentBatchSNT b2 = new ProcessCRAssignmentBatchSNT(new set<ID>{positionID},'Position_Geography__c');
                ProcessCRAssignmentBatchSNT b3 = new ProcessCRAssignmentBatchSNT(new set<ID>{positionID},'Position_Employee__c');
                Id batchinstanceid1 = Database.executeBatch(b1,9000);
                Id batchinstanceid2 = Database.executeBatch(b2,9000);
                Id batchinstanceid3 = Database.executeBatch(b3,9000);

                //Added Call Plan and Position Product Changes
                ExpiredCallPlanAssignmentBatch b4 = new ExpiredCallPlanAssignmentBatch(new set<ID>{positionID});
                //ExpiredSNTAssignmentBatch b5 = new ExpiredSNTAssignmentBatch(new set<ID>{positionID},'Position_Product__c');
                Id batchinstanceid4 = Database.executeBatch(b4,200);
                //Id batchinstanceid5 = Database.executeBatch(b5,2000);

                updateUserPosForExpiredAssignment(positionID,endDate);

                System.debug('---- calling ProcessCRAssignmentBatch from trigger ends---- ');
        }
    }

    private static void updateUserPosForExpiredAssignment(string positionIds, Date endDate){

        String posID = positionIds;
        String clientPosCode= '';
        String team = '';
        Date posEndDate = endDate;

        list<AxtriaSalesIQTM__Position__c> newposList = new list<AxtriaSalesIQTM__Position__c>();

        newposList = [select Id, AxtriaSalesIQTM__Client_Position_Code__c, AxtriaSalesIQTM__Team_iD__c,AxtriaSalesIQTM__IsMaster__c, AxtriaSalesIQTM__Assignment_Status__c,AxtriaSalesIQTM__inactive__c,AxtriaSalesIQTM__Employee__c,AxtriaSalesIQTM__Effective_Start_Date__c, AxtriaSalesIQTM__Effective_End_Date__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team_Cycle_Name__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c from AxtriaSalesIQTM__Position__c where id=: posID];

        for(AxtriaSalesIQTM__Position__c pos : newposList)
        {
            clientPosCode = pos.AxtriaSalesIQTM__Client_Position_Code__c;
            team = pos.AxtriaSalesIQTM__Team_iD__c;
        }

        System.debug('---- calling inactivateUserPosForExpiredAssignment from trigger ---- ');
        //Fetch configured day of batch process
        /*double processDay;
        if(AxtriaSalesIQTM__TotalApproval__c.getValues('ProcessDay') != null){
            processDay = AxtriaSalesIQTM__TotalApproval__c.getValues('ProcessDay').AxtriaSalesIQTM__No_Of_Approval__c;
        }else{
            processDay = 0;
        }*/
        Date processDate = System.today();
        system.debug('#### processDate : '+processDate);

        
        list<AxtriaSalesIQTM__User_Access_Permission__c> userPositionToUpdate = new list<AxtriaSalesIQTM__User_Access_Permission__c>();
        // get userposition records for loggedin user 
        for(AxtriaSalesIQTM__User_Access_Permission__c uap : [SELECT Id, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__User__c,  AxtriaSalesIQTM__Sharing_Type__c, AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c, AxtriaSalesIQTM__Is_Active__c FROM AxtriaSalesIQTM__User_Access_Permission__c where AxtriaSalesIQTM__Is_Active__c = true AND AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c =: clientPosCode and  AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Team_iD__c =: team]){

            System.debug('System.today() ::::::::::::' +System.today());

            System.debug('uap.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c ::::::::::::' +uap.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Effective_End_Date__c);

            if(posEndDate < System.today()){
                uap.AxtriaSalesIQTM__Is_Active__c = false;
                //uap.AxtriaSalesIQTM__Effective_End_Date__c = processDate;
                userPositionToUpdate.add(uap);
            }
        }

        System.debug('userPositionToUpdate ::::::::::::' +userPositionToUpdate);

        if(userPositionToUpdate.size() >0){
            update userPositionToUpdate;
        }

        
    }
}