/****************************************************************************************

        Organization  :   Axtria
        Author        :   Saurav
        Date          :   28-May-2020
        Descripton    :   This trigger is the merged code for the two existing triggers
    	Triggers      :     1. Populate_Position_Product
              				2. TeamInstanceProductTrigger
              

****************************************************************************************/  

trigger TeamInstanceProductTrigger on Team_Instance_Product_AZ__c (before insert, after insert, before update, after update, before delete, after delete) {
	
    public AxtriaSalesIQTM__TriggerContol__c exeTrigger = new AxtriaSalesIQTM__TriggerContol__c();

    exeTrigger = AxtriaSalesIQTM__TriggerContol__c.getValues('TeamInstanceProductTriggerSnT')!= null ? AxtriaSalesIQTM__TriggerContol__c.getValues('TeamInstanceProductTriggerSnT'):null;
    if(exeTrigger != null)
    {
        if(!exeTrigger.AxtriaSalesIQTM__IsStopTrigger__c)
        {
            if(Trigger.isAfter){
                if(Trigger.isInsert){
                    Populate_Position_Product_Handler.populatePosition(trigger.new);    // From Populate_Position_Product
                    
                }else if(Trigger.isUpdate){
                    Populate_Position_Product_Handler.updatePosition(trigger.new);      // From Populate_Position_Product
                    /*TeamInstanceProductTriggerHandler.deleteTrigger(trigger.old);     // From TeamInstanceProductTrigger
                    TeamInstanceProductTriggerHandler.insertTrigger(trigger.new);   // From TeamInstanceProductTrigger*/
                }else if(Trigger.isDelete){
                    Populate_Position_Product_Handler.deletePosition(trigger.old);      // From Populate_Position_Product
                }
            }                    
        }

    }
    else
    {
        system.debug('++++++ TeamInstanceProductTriggerSnT Custom Setting Missing in Trigger Control');
    }

}