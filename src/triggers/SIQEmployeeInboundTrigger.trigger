trigger SIQEmployeeInboundTrigger on AxtriaARSnT__SIQ_Employee_Master__c (before insert,before update) {
    set<string> setPRID = new set<string>();
    for(SIQ_Employee_Master__c siqEmp: [select id, SIQ_PRID__c from SIQ_Employee_Master__c]){
        setPRID.add(siqEmp.SIQ_PRID__c );
    }
    if(trigger.isInsert){
        for(SIQ_Employee_Master__c InEmp:trigger.new){
            if(setPRID.contains(InEmp.SIQ_PRID__c)){
                InEmp.addError('Duplicate PRID is not allowed.');
            }
        }
    }
    if(trigger.isUpdate){
        for(SIQ_Employee_Master__c InEmp:trigger.new){
            if(Trigger.OldMap.get(InEmp.id)!=null && Trigger.OldMap.get(InEmp.id).SIQ_PRID__c!=null && InEmp.SIQ_PRID__c!= Trigger.OldMap.get(InEmp.id).SIQ_PRID__c 
               && setPRID.contains(InEmp.SIQ_PRID__c)){
                InEmp.addError('Duplicate PRID is not allowed.');
            }
        }
    }
}