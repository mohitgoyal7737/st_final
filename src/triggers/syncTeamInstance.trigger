trigger syncTeamInstance on Account (after update) {
    Boolean isRunTriggerHandler = false;
if(AxtriaSalesIQTM__TriggerContol__c.getValues('syncTeamInstance ') != null  && !AxtriaSalesIQTM__TriggerContol__c.getValues('syncTeamInstance ').AxtriaSalesIQTM__IsStopTrigger__c) {
            system.debug('===========CallPlanSummary Trigger is Executing===========');
             
    for (Account acc: Trigger.new) {
        Account oldAccount = Trigger.oldMap.get(acc.ID);
        if(acc.Profile_Consent__c != oldAccount.Profile_Consent__c){
            isRunTriggerHandler = true;
            break;
        }
    }
    System.debug('syncTeamInstance called');
    if(isRunTriggerHandler){
        System.debug('Profileconsenthandler.fetchaccounts called');
        Profileconsenthandler.fetchaccounts(Trigger.new, Trigger.oldMap);
    }
 }
}