//@Author
//A1691--Mohit Goyal
trigger oldandnewENDdate on AxtriaSalesIQTM__Workspace__c (after update) 
// Change the workspace dates and related objects 
    {
       for(AxtriaSalesIQTM__Workspace__c wc : Trigger.new)
        {

            AxtriaSalesIQTM__Workspace__c oldworkspace = new AxtriaSalesIQTM__Workspace__c();
            oldworkspace = Trigger.oldMap.get(wc.id);

            Date olddate = oldworkspace.AxtriaSalesIQTM__Workspace_End_Date__c;

            if(olddate!=wc.AxtriaSalesIQTM__Workspace_End_Date__c)
                {
                    //BatchUpdateEnddate_workspace Bw = new 
                    Database.executebatch(new BatchUpdateEnddate_workspace (wc.id,wc.AxtriaSalesIQTM__Workspace_End_Date__c),2000);


                }

        }



    }