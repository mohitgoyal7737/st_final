Trigger ScenarioQueueFullPromote on AxtriaSalesIQTM__Position_Geography__c (after insert) {
  
    if(AxtriaSalesIQTM__TriggerContol__c.getValues('ScenarioQueueFullPromote') != null && AxtriaSalesIQTM__TriggerContol__c.getValues('ScenarioQueueFullPromote').AxtriaSalesIQTM__IsStopTrigger__c) return ;
    Set<Id> teamInstIds = new Set<Id>();     
    
    for(AxtriaSalesIQTM__Position_Geography__c pg : trigger.new)
    {
        teamInstIds.add(pg.AxtriaSalesIQTM__Team_Instance__c);
    }
    System.debug('teamInstIds :: '+teamInstIds);
    List<AxtriaSalesIQTM__Scenario__c> selectedSceanrio = new List<AxtriaSalesIQTM__Scenario__c>();
           
    selectedSceanrio = [Select Id,AxtriaSalesIQTM__Scenario_Stage__c,AxtriaSalesIQTM__Scenario_Name__c,AxtriaSalesIQTM__Rule_Execution_Status__c, AxtriaSalesIQTM__Promote_Mode__c 
                                                             FROM AxtriaSalesIQTM__Scenario__c Where AxtriaSalesIQTM__Team_Instance__c IN :teamInstIds];
    System.debug('selectedSceanrio  :: '+selectedSceanrio );
    List<AxtriaSalesIQTM__Scenario__c> allUpdateScenarios = new List<AxtriaSalesIQTM__Scenario__c>();
    
    for(AxtriaSalesIQTM__Scenario__c obj:selectedSceanrio )
    {                                                
        if(obj.AxtriaSalesIQTM__Scenario_Stage__c == 'Live' && obj.AxtriaSalesIQTM__Rule_Execution_Status__c =='Success')
        {
            obj.AxtriaSalesIQTM__Rule_Execution_Status__c = 'Queued';
            obj.AxtriaSalesIQTM__Promote_Mode__c = 'Run Full and Promote';
            allUpdateScenarios.add(obj);
        }
    }
    System.debug('allUpdateScenarios :: '+allUpdateScenarios);
    if(allUpdateScenarios.size()>0)
    {
        System.debug('Before update');
       update allUpdateScenarios;
       System.debug('After update'); 
    }
}