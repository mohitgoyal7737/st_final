trigger ChangeRequestAZ on AxtriaSalesIQTM__Change_Request__c (before update, after insert, after update)
{

    if(trigger.isBefore)
    {
        if(trigger.isUpdate)
        {
            System.debug('+++++++++++++++++  In Change Request Before and Update');

            Map<string, AxtriaSalesIQTM__Change_Request__c> rejectedStatements = new Map<string, AxtriaSalesIQTM__Change_Request__c> {};

            String temp = '';
            for(AxtriaSalesIQTM__Change_Request__c car : trigger.new)
            {
                if(car.AxtriaSalesIQTM__Request_Type_Change__c == 'Call Plan' )
                {
                    AxtriaSalesIQTM__Change_Request__c oldcar = System.Trigger.oldMap.get(car.Id);

                    if (oldcar.AxtriaSalesIQTM__Status__c != 'Rejected' && car.AxtriaSalesIQTM__Status__c == 'Rejected')
                    {
                        temp = car.Id;
                        temp = temp.subString(0, 15);
                        system.debug('temp' + temp);
                        rejectedStatements.put(temp, car);

                    }
                }

            }


            System.debug(' rejectedStatements' + rejectedStatements.keyset());


            for (ProcessInstance pi : [SELECT TargetObjectId,
                                       (
                                           SELECT Id, StepStatus, Comments
                                           FROM Steps
                                           WHERE StepStatus = 'Rejected'
                                                   ORDER BY CreatedDate DESC
                                                   LIMIT 1
                                       )
                                       FROM ProcessInstance
                                       WHERE TargetObjectId In
                                       :rejectedStatements.keySet()
                                       ORDER BY CreatedDate DESC
                                      ])
            {

                system.debug('pi.Steps.size()' + pi.Steps.size());
                system.debug('pi.Steps[0]' + pi.Steps[0]);


                if (pi.Steps.size() > 0  && (pi.Steps[0].Comments == null || pi.Steps[0].Comments.trim().length() == 0))
                {
                    rejectedStatements.get(temp).addError(System.Label.Error_msg_for_reason_for_rejection);
                }

            }



        }
    }


    if(Trigger.isAfter)
    {
        List<AxtriaSalesIQTM__Change_Request__c> call_plan_cr_list = new List<AxtriaSalesIQTM__Change_Request__c>();
        for(AxtriaSalesIQTM__Change_Request__c cr : trigger.new)
        {
            if(cr.AxtriaSalesIQTM__Request_Type_Change__c == SalesIQGlobalConstants.MOVEMENT_TYPE_CALL_PLAN )
            {
                call_plan_cr_list.add(cr);

            }
        }
        if(call_plan_cr_list.size() > 0)
        {

            if(Trigger.isInsert)
            {

                ChnageRequestTriggerHandlerAZ.createCRCallPlan(call_plan_cr_list);
            }
            else if(Trigger.isUpdate)
            {

                ChnageRequestTriggerHandlerAZ.updatePositionAccountCallPlan(call_plan_cr_list);
            }

        }
    }


}