<apex:page controller="MasterRosterUpgrade" standardStylesheets="false">
    <apex:slds />
    <style>
        .activeTab {
            background-color: #236FBD; 
            color:white; background-image:none;
            font-weight: bolder;
            font-size: medium;
        }
        .inactiveTab {
            background-color: lightgrey; color:black; 
            background-image:none;
            font-weight: bolder;
            font-size: medium;
        }
    </style>
    <apex:sectionHeader title="Master Roster Upgrade"/>
    <apex:form >
        <apex:pageMessages id="err"/>
        <apex:actionStatus id="st">
            <apex:facet name="start" ><img src="/img/loading.gif"/> Processing...</apex:facet>
        </apex:actionStatus>
        <apex:actionFunction name="selectCountry" action="{!getTeamOptions}" rerender="filterPanel" status="st"/>
        <apex:pageBlock >
            <apex:pageBlockButtons location="top">
                <apex:commandButton action="{!validateUpgrade}" value="Validate" rerender="result,err" status="st"/>
                <apex:commandButton action="{!downloadResult}" value="Download" rerender="result,err"/>
                <apex:commandButton action="{!executeUpgrade}" value="Upgrade" rerender="result,err,filterPanel" status="st"/>
            </apex:pageBlockButtons>
            <apex:pageBlockSection id="filterPanel" columns="4">
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Select Country"/>
                    <apex:selectList value="{!selectedCountry}" multiselect="false" size="1" onchange="selectCountry();return false;">
                        <apex:selectOptions value="{!CountryOptions}"/>
                    </apex:selectList>
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem id="teamFilter">
                    <apex:outputLabel value="Select Team"/>
                    <apex:selectList value="{!selectedTeam}" multiselect="false" size="1">
                        <apex:selectOptions value="{!TeamOptions}"/>
                    </apex:selectList>
                </apex:pageBlockSectionItem>
                <table>
                    <tr>
                        <td style="background-color: cadetblue;width: 20px;">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>Scenario details</td>
                        <td style="background-color: #D3D3D3;width: 20px;">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>Master Positions</td>
                    </tr>
                </table>
            </apex:pageBlockSection>
        </apex:pageBlock>
        <apex:pageBlock id="result">
            <apex:tabPanel switchType="client" selectedTab="positions" id="resultTabPanel" tabClass="activeTab" inactiveTabClass="inactiveTab">
                <apex:tab label="Positions" name="positions" id="tabOne">
                    <table cellspacing="0" cellpadding="5">
                        <tr style="border-bottom: 1px solid black;">
                            <th>Position Id</th>
                            <th>Position Name</th>
                            <th>Position Code</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Is Master</th>
                            <th>Master Reference</th>
                        </tr>
                        <apex:repeat value="{!teamInstancePositionMap}" var="teamInstanceId">
                            <tr style="background-color: cadetblue; color: azure;">
                                <th style="text-align: left;" colspan="3">Scenario: {!teamInstanceMap[teamInstanceId].Name} ({!teamInstanceMap[teamInstanceId].AxtriaSalesIQTM__Alignment_Period__c})</th>
                                <th style="text-align: left;">
                                    <apex:outputText value="{0,date,MM/dd/yyyy}">
                                       <apex:param value="{!teamInstanceMap[teamInstanceId].AxtriaSalesIQTM__IC_EffstartDate__c}"/>
                                    </apex:outputText>
                                </th>
                                <th style="text-align: left;">
                                    <apex:outputText value="{0,date,MM/dd/yyyy}">
                                       <apex:param value="{!teamInstanceMap[teamInstanceId].AxtriaSalesIQTM__IC_EffEndDate__c}"/>
                                    </apex:outputText>
                                </th>
                                <th style="text-align: left;" colspan="2">Employee Assignment: {!IF(teamInstanceMap[teamInstanceId].AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Employee_Assignment__c,'Enabled','Disabled')}</th>
                            </tr>
                            <apex:repeat value="{!teamInstancePositionMap[teamInstanceId]}" var="position">
                                <tr style="{!IF(position.AxtriaSalesIQTM__IsMaster__c,'background-color: #D3D3D3;','')}">
                                    <td><a href="/{!position.Id}" target="_blank">{!position.Id}</a></td>
                                    <td>{!position.Name}</td>
                                    <td>{!position.AxtriaSalesIQTM__Client_Position_Code__c}</td>
                                    <td>
                                        <apex:outputText value="{0,date,MM/dd/yyyy}">
                                            <apex:param value="{!position.AxtriaSalesIQTM__Effective_Start_Date__c}"/>
                                        </apex:outputText>
                                    </td>
                                    <td>
                                        <apex:outputText value="{0,date,MM/dd/yyyy}">
                                            <apex:param value="{!position.AxtriaSalesIQTM__Effective_End_Date__c}"/>
                                        </apex:outputText>
                                    </td>
                                    <td>{!position.AxtriaSalesIQTM__IsMaster__c}</td>
                                    <td><a href="/{!position.AxtriaSalesIQTM__Master_Position_Reference__c}" target="_blank">{!position.AxtriaSalesIQTM__Master_Position_Reference__c}</a></td>
                                </tr>
                            </apex:repeat>
                        </apex:repeat>
                    </table>
                </apex:tab>
                <apex:tab label="Employee Assignments" name="employees" id="tabTwo">
                    <table cellspacing="0" cellpadding="5">
                        <tr style="border-bottom: 1px solid black;">
                            <th>Employee Id</th>
                            <th>Employee Name</th>
                            <th>Position Id</th>
                            <th>Position Name</th>
                            <th>Assignment Type</th>
                            <th>Effective Start Date</th>
                            <th>Effective End Date</th>
                        </tr>
                        <apex:repeat value="{!positionEmployeeMap}" var="posCode">
                            <apex:repeat value="{!positionEmployeeMap[posCode]}" var="emp">
                            <tr>
                                <td><a href="/{!emp.Id}" target="_blank">{!emp.Id}</a></td>
                                <td><a href="/{!emp.Id}" target="_blank">{!emp.AxtriaSalesIQTM__Employee__r.Name}</a></td>
                                <td><a href="/{!emp.AxtriaSalesIQTM__Position__c}" target="_blank">{!emp.AxtriaSalesIQTM__Position__c}</a></td>
                                <td><a href="/{!emp.AxtriaSalesIQTM__Position__c}" target="_blank">{!emp.AxtriaSalesIQTM__Position__r.Name}</a></td>
                                <td>{!emp.AxtriaSalesIQTM__Assignment_Type__c}</td>
                                <td>
                                    <apex:outputText value="{0,date,MM/dd/yyyy}">
                                        <apex:param value="{!emp.AxtriaSalesIQTM__Effective_Start_Date__c}"/>
                                    </apex:outputText>
                                </td>
                                <td>
                                    <apex:outputText value="{0,date,MM/dd/yyyy}">
                                        <apex:param value="{!emp.AxtriaSalesIQTM__Effective_End_Date__c}"/>
                                    </apex:outputText>
                                </td>
                            </tr>
                            </apex:repeat>
                        </apex:repeat>
                    </table>
                </apex:tab>
            </apex:tabPanel>
        </apex:pageBlock>
    </apex:form>
</apex:page>