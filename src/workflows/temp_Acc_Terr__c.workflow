<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Field_Update_UniqueKey</fullName>
        <field>UniqueKey__c</field>
        <formula>Territory_ID__c &amp; AccountNumber__c &amp; TeamName__c</formula>
        <name>Field Update UniqueKey</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update TempAccTerr field</fullName>
        <actions>
            <name>Field_Update_UniqueKey</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISBLANK( UniqueKey__c )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
