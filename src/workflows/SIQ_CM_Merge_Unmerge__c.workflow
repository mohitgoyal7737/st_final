<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>PopulateUniqueMergeID</fullName>
        <field>GUID1_GUID2_Key__c</field>
        <formula>SIQ_MDM_Customer_GUID_1__c + &apos;_&apos; +  SIQ_MDM_Customer_GUID_2__c</formula>
        <name>PopulateUniqueMergeID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>PopulateUniqueMergeIDs</fullName>
        <actions>
            <name>PopulateUniqueMergeID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SIQ_CM_Merge_Unmerge__c.SIQ_MDM_Customer_GUID_1__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>SIQ_CM_Merge_Unmerge__c.SIQ_MDM_Customer_GUID_2__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
