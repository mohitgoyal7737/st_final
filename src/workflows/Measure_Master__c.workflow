<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Publish_Business_Rule_Notification</fullName>
        <description>Publish Business Rule Notification</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>AZ_CallMAx_Email_Templates/Business_Rule_Published</template>
    </alerts>
    <alerts>
        <fullName>Rule_Failure_Error_details_Notification</fullName>
        <description>Rule Failure Error details Notification</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Scenario_Rule_Execution_status/Rule_Failure_Error_Details_Notification</template>
    </alerts>
    <alerts>
        <fullName>Rule_Failure_Notification</fullName>
        <description>Rule Failure Notification</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Scenario_Rule_Execution_status/Rule_Failure_Notification</template>
    </alerts>
    <rules>
        <fullName>Notify Call Plan Published</fullName>
        <actions>
            <name>Publish_Business_Rule_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Measure_Master__c.State__c</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <criteriaItems>
            <field>Measure_Master__c.is_promoted__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This is to notify that call plan has been published</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
