<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Unique_Key</fullName>
        <field>Unique_Key__c</field>
        <formula>Position_Code__c &amp; Employee_PRID__c  &amp; Assignment_Type__c &amp;  Team_Instance__c</formula>
        <name>Update Unique Key</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Field Update on tempPosEmp</fullName>
        <actions>
            <name>Update_Unique_Key</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISBLANK(Unique_Key__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
