<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Address_Changed</fullName>
        <field>isAddressChanged__c</field>
        <literalValue>1</literalValue>
        <name>Address Changed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>isspecialtychangedupdate</fullName>
        <field>isSpecialtyChanged__c</field>
        <literalValue>1</literalValue>
        <name>isspecialtychangedupdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>type_update</fullName>
        <field>isTypeChanged__c</field>
        <literalValue>1</literalValue>
        <name>type update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Account Micro Brick Changed</fullName>
        <actions>
            <name>Address_Changed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This rule is used to update the IsAddressChanged Flag on Account</description>
        <formula>ISCHANGED( Mini_Brick__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>account specialty change</fullName>
        <actions>
            <name>isspecialtychangedupdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ISCHANGED( Primary_Speciality_Code__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>type changed</fullName>
        <actions>
            <name>type_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ISCHANGED( Type )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
