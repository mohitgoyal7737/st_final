trigger RosterEventProcessTrigger on AxtriaSalesIQST__SIQ_Employee_Master__c (before insert,  after insert ) {
    AxtriaSalesIQTM__TriggerContol__c myCS2 = AxtriaSalesIQTM__TriggerContol__c.getValues('RosterTriggers');
    
    if(Trigger.isBefore && Trigger.isInsert && myCS2.AxtriaSalesIQTM__IsStopTrigger__c !=true){
        for(AxtriaSalesIQST__SIQ_Employee_Master__c siqEmp : trigger.new){
            siqEmp.isActive__c = true;
        }
        System.debug('@@@ Before Insert Trigger of RosterEventProcessTrigger  Occured');
        BatchProcessCRRosterEvents bcre = new BatchProcessCRRosterEvents();
        bcre.ValidateAndMapSiqEmp(trigger.new);
        bcre.validateAndMapPosToGeo(trigger.new);
    }
    
    if(Trigger.isAfter && Trigger.isInsert && myCS2.AxtriaSalesIQTM__IsStopTrigger__c !=true){
        System.debug('@@@ After Insert Trigger of RosterEventProcessTrigger  Occured');
        Set<Id> siqEmpIdSet = new Set<Id>();
        for(AxtriaSalesIQST__SIQ_Employee_Master__c siqEmp : trigger.new){
            if(!(siqEmp.Errorneous_Record__c)){
                siqEmpIdSet.add(siqEmp.id);
            }
        } 
        System.debug('@@@ siqEmpIdSet.size() = '+siqEmpIdSet.size());
        Database.executeBatch(new BatchProcessCRRosterEvents(siqEmpIdSet), 1);
    }
}