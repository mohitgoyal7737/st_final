trigger syncPosition on AxtriaSalesIQTM__Position__c (before insert, before update, after insert, after update) {
    System.debug('@@@ syncPosition Trigger invoked !!!');
    AxtriaSalesIQTM__TriggerContol__c myCS2 = AxtriaSalesIQTM__TriggerContol__c.getValues('RosterTriggers');

    if(myCS2.AxtriaSalesIQTM__IsStopTrigger__c !=true){
        if(trigger.isBefore){
            for(AxtriaSalesIQTM__Position__c pos : trigger.new){
                if(pos.AxtriaSalesIQTM__Employee__c == null){
                    pos.AxtriaSalesIQTM__Assignment_status__c = 'Vacant';
                }
                else if(pos.AxtriaSalesIQTM__Employee__c != null){
                    pos.AxtriaSalesIQTM__Assignment_status__c = 'Filled';
                }
            }
            if(trigger.isInsert ){
                for(AxtriaSalesIQTM__Position__c pos : trigger.new){
                    String positionDetails = pos.Position__c;
                    System.debug('@@@ positionDetails = '+positionDetails);
                    if(positionDetails != null ){
                        System.debug('@@@ positionDetails = '+positionDetails);
                        pos.Position_Code__c = positionDetails.substringBefore('-');
                        pos.Position_Name__c = positionDetails.substringAfter('-');
                        System.debug('@@@ pos.Position_Code__c = '+pos.Position_Code__c);
                        System.debug('@@@ pos.Position_Name__c = '+pos.Position_Name__c);
                    }
                    pos.AxtriaSalesIQTM__Effective_End_Date__c = date.newInstance(4000,12,31);
                    pos.AxtriaSalesIQST__Sales_Force_Code__c = pos.SalesforceCode__c;
                    pos.AxtriaSalesIQST__Salesforce_Name__c = pos.SalesforceName__c;
                }
            }
            if(trigger.isUpdate){
                for(AxtriaSalesIQTM__Position__c pos : trigger.new){
                    String positionDetails = pos.Position__c;
                    System.debug('@@@ positionDetails = '+positionDetails);
                    if(positionDetails != null ){
                        System.debug('@@@ positionDetails = '+positionDetails);
                        pos.Position_Code__c = positionDetails.substringBefore('-');
                        pos.Position_Name__c = positionDetails.substringAfter('-');
                        System.debug('@@@ pos.Position_Code__c = '+pos.Position_Code__c);
                        System.debug('@@@ pos.Position_Name__c = '+pos.Position_Name__c);
                    }
                    pos.AxtriaSalesIQST__Sales_Force_Code__c = pos.SalesforceCode__c;
                    pos.AxtriaSalesIQST__Salesforce_Name__c = pos.SalesforceName__c;
                }
            }
        }

        if(trigger.isAfter){
            set<Id>posIdset = new Set<Id>();
            for(AxtriaSalesIQTM__Position__c pos : trigger.new){
                posIdset.add(pos.Id);
            }
            /*
            if(trigger.isInsert){
                GeographyStagingInsertion gs = new GeographyStagingInsertion();
                gs.GeographyStagingInsert(posIdset);
            }
            if(trigger.isUpdate){
                GeographyStagingInsertion gs = new GeographyStagingInsertion();
                gs.GeographyStagingUpdate(posIdset);
            }
            */
        }
    }
}