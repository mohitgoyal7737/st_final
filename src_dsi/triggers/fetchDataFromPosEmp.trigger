trigger fetchDataFromPosEmp on Employee_History__c(before insert, before update, after insert, after update) {
    List<AxtriaSalesIQTM__Position_Employee__c> posEmpList = [select id, AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Email__c, AxtriaSalesIQTM__Employee__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQST__Territory_ID__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c, AxtriaSalesIQTM__Position__r.Sales_Force_Code__c, AxtriaSalesIQTM__Position__r.Sales_Force__c, AxtriaSalesIQTM__Employee__r.Termination_Date__c, AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Last_Name__c, AxtriaSalesIQTM__Employee__r.Payroll_Id__c, AxtriaSalesIQTM__Employee__r.Original_Hire_Date__c, AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__FirstName__c, AxtriaSalesIQTM__Employee__r.Personnel_Subarea_Code__c, AxtriaSalesIQTM__Position__r.Name, AxtriaSalesIQTM__Employee__r.Personnel_Subarea_Name__c, AxtriaSalesIQTM__Employee__r.Personnel_Area__c, AxtriaSalesIQTM__Employee__r.Personnel_Number__c, AxtriaSalesIQTM__Employee__r.Personnel_Area_Number__c, AxtriaSalesIQTM__Employee__r.Reason_Code__c, AxtriaSalesIQTM__Employee__r.Position_Title__c  from AxtriaSalesIQTM__Position_Employee__c];
    
    List<Employee_History__c> empHistoryDelete = new List<Employee_History__c>();
    AxtriaSalesIQTM__TriggerContol__c myCS2 = AxtriaSalesIQTM__TriggerContol__c.getValues('RosterTriggers');

    if(trigger.isBefore && myCS2.AxtriaSalesIQTM__IsStopTrigger__c !=true){
        for(Employee_History__c empHis : trigger.new){
            for(AxtriaSalesIQTM__Position_Employee__c posEmp : posEmpList){
                if(empHis.Employee__c == posEmp.AxtriaSalesIQTM__Employee__c && empHis.Position__c == posEmp.AxtriaSalesIQTM__Position__c){
                    empHis.Client_Territory_Code__c = posEmp.AxtriaSalesIQST__Territory_ID__c;
                    empHis.Position_Type__c = posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c;
                    empHis.Termination_Date__c = posEmp.AxtriaSalesIQTM__Employee__r.Termination_Date__c;
                    empHis.Sales_Force_Code__c = posEmp.AxtriaSalesIQTM__Position__r.Sales_Force_Code__c;
                    empHis.Sales_Force__c = posEmp.AxtriaSalesIQTM__Position__r.Sales_Force__c;
                    empHis.Payroll_Id__c = posEmp.AxtriaSalesIQTM__Employee__r.Payroll_Id__c;
                    empHis.Last_Name__c = posEmp.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Last_Name__c;
                    empHis.Hire_Date__c = posEmp.AxtriaSalesIQTM__Employee__r.Original_Hire_Date__c;
                    empHis.First_Name__c = posEmp.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__FirstName__c;
                    empHis.Geography_Name__c = posEmp.AxtriaSalesIQTM__Position__r.Name;
                    empHis.Personnel_Subarea_Code__c = posEmp.AxtriaSalesIQTM__Employee__r.Personnel_Subarea_Code__c;
                    empHis.Personnel_Subarea_Name__c = posEmp.AxtriaSalesIQTM__Employee__r.Personnel_Subarea_Name__c;
                    empHis.Personnel_Area__c = posEmp.AxtriaSalesIQTM__Employee__r.Personnel_Area__c;
                    empHis.Personnel_Number__c = posEmp.AxtriaSalesIQTM__Employee__r.Personnel_Number__c;
                    empHis.Personnel_Area_Number__c = posEmp.AxtriaSalesIQTM__Employee__r.Personnel_Area_Number__c;
                    empHis.Reason_Code__c = posEmp.AxtriaSalesIQTM__Employee__r.Reason_Code__c;
                    empHis.Position_Title__c = posEmp.AxtriaSalesIQTM__Employee__r.Position_Title__c; 
                    empHis.Email__c = posEmp.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Email__c;
                }
            }
        }
    }

    /*
    if(trigger.isAfter){
        for(Employee_History__c eh : trigger.new){
            if(eh.IC_Status__c == 'TERM'){
                empHistoryDelete.add(eh);
            }
        }
    }

    if(empHistoryDelete.size()>0){
        delete empHistoryDelete;
    }*/
       

}