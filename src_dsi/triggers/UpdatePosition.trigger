trigger UpdatePosition on AxtriaSalesIQTM__Position_Employee__c (after Insert, after Update, before update, before insert) {
    AxtriaSalesIQTM__TriggerContol__c myCS2 = AxtriaSalesIQTM__TriggerContol__c.getValues('UpdatePositionTrigger');
    if(myCS2.AxtriaSalesIQTM__IsStopTrigger__c !=true){
        Set<Id> positionIdSet = new Set<Id>();
        for(AxtriaSalesIQTM__Position_Employee__c posEmp : trigger.New){
            positionIdSet.add(posEmp.AxtriaSalesIQTM__Position__c);
        }
        List<AxtriaSalesIQTM__Position__c> positionList = [Select id, Name, Last_Assignment_End_Date__c, AxtriaSalesIQTM__Client_Territory_Code__c, AxtriaSalesIQTM__Assignment_status__c, AxtriaSalesIQTM__Employee__c from AxtriaSalesIQTM__Position__c where id =: positionIdSet];
        List<AxtriaSalesIQTM__Position__c> updatePositionList = new List<AxtriaSalesIQTM__Position__c>();
        List<AxtriaSalesIQTM__Employee__c> empList = [select id, Name from AxtriaSalesIQTM__Employee__c];

        if(trigger.isAfter && trigger.isInsert){
            for(AxtriaSalesIQTM__Position_Employee__c posEmp : trigger.new){
              if(positionList.size()>0){
                for(AxtriaSalesIQTM__Position__c pos : positionList ){
                    if(pos.Id == posEmp.AxtriaSalesIQTM__Position__c){
                        System.debug('@@@17 posEmp.AxtriaSalesIQTM__Employee__c = '+posEmp.AxtriaSalesIQTM__Employee__c);
                        System.debug('@@@17 posEmp.AxtriaSalesIQTM__Assignment_Status__c = '+posEmp.AxtriaSalesIQTM__Assignment_Status__c);
                        if(posEmp.AxtriaSalesIQTM__Employee__c != null && 
                            posEmp.AxtriaSalesIQTM__Assignment_Status__c == 'Active'){
                            System.debug('@@@ condition 1 satisfied');
                            pos.AxtriaSalesIQTM__Employee__c = posEmp.AxtriaSalesIQTM__Employee__c;
                            pos.AxtriaSalesIQTM__Assignment_status__c = 'Filled';
                            pos.Last_Assignment_End_Date__c = null;
                            //updatePositionList.add(pos);
                        }
                        
                        if(posEmp.AxtriaSalesIQTM__Assignment_Status__c == 'Inactive'){
                            System.debug('@@@ condition 2 satisfied');
                            pos.AxtriaSalesIQTM__Employee__c = null;
                            pos.AxtriaSalesIQTM__Assignment_status__c = 'Vacant';
                            pos.Last_Assignment_End_Date__c = posEmp.AxtriaSalesIQTM__Effective_End_Date__c+1;
                            //updatePositionList.add(pos);
                        }
                        
                    }
                }
              }
            }
            if(positionList.size()>0){
                update positionList;
            }
        }
        if(trigger.isAfter && trigger.isUpdate){
            for(AxtriaSalesIQTM__Position_Employee__c posEmp : trigger.new){
              if(positionList.size()>0){
                for(AxtriaSalesIQTM__Position__c pos : positionList ){
                    if(pos.Id == posEmp.AxtriaSalesIQTM__Position__c){
                        System.debug('@@@ posEmp.AxtriaSalesIQTM__Employee__c = '+posEmp.AxtriaSalesIQTM__Employee__c);
                        if(posEmp.AxtriaSalesIQTM__Employee__c != null && 
                            posEmp.AxtriaSalesIQTM__Assignment_Status__c == 'Active'){
                            System.debug('@@@ condition 1 satisfied');
                            pos.AxtriaSalesIQTM__Employee__c = posEmp.AxtriaSalesIQTM__Employee__c;
                            pos.AxtriaSalesIQTM__Assignment_status__c = 'Filled';
                            pos.Last_Assignment_End_Date__c = null;
                            //updatePositionList.add(pos);
                        }
                        if((trigger.oldMap.get(posEmp.id).AxtriaSalesIQTM__Assignment_Status__c == 'Active') && 
                            (trigger.newMap.get(posEmp.id).AxtriaSalesIQTM__Assignment_Status__c == 'Inactive')){
                            if(posEmp.AxtriaSalesIQTM__Assignment_Status__c == 'Inactive'){
                                System.debug('@@@ condition 2 satisfied');
                                pos.AxtriaSalesIQTM__Employee__c = null;
                                pos.AxtriaSalesIQTM__Assignment_status__c = 'Vacant';
                                pos.Last_Assignment_End_Date__c = posEmp.AxtriaSalesIQTM__Effective_End_Date__c+1;
                                //updatePositionList.add(pos);
                            }
                        }
                    }
                }
              }
            }
            if(positionList.size()>0){
                update positionList;
            }
        }
        
        if(trigger.isBefore && (trigger.isInsert || trigger.isUpdate)){
        
            for(AxtriaSalesIQTM__Position_Employee__c posEmp : trigger.New){
                if(posEmp.AxtriaSalesIQST__Territory_Start_Date__c !=null ){
                    if(posEmp.AxtriaSalesIQTM__Effective_Start_Date__c < posEmp.AxtriaSalesIQST__Territory_Start_Date__c){
                        posEmp.AxtriaSalesIQTM__Effective_Start_Date__c = posEmp.AxtriaSalesIQST__Territory_Start_Date__c;
                    }
                }
                for(AxtriaSalesIQTM__Position__c pos : positionList){
                    for(AxtriaSalesIQTM__Employee__c emp : empList){
                        if(posEmp.AxtriaSalesIQTM__Position__c == pos.Id && posEmp.AxtriaSalesIQTM__Employee__c == emp.id){
                            posEmp.Name = emp.Name+'_'+pos.AxtriaSalesIQTM__Client_Territory_Code__c+'_'+pos.Name;
                        }
                    }
                }
            }
            /*
            Id profileId= userinfo.getProfileId();
            String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
            system.debug('ProfileName'+profileName);
            
            if(profileName == 'HO'){
                for(AxtriaSalesIQTM__Position_Employee__c emp : trigger.new){
                    emp.addError('User cannot perform DML Operation on Position Employee manually !!! ');
                }
            } */
        }
    }
}