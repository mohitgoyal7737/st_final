trigger Createuser on AxtriaSalesIQTM__Employee__c (after insert) {

AxtriaSalesIQTM__TriggerContol__c myCS1 = AxtriaSalesIQTM__TriggerContol__c.getValues('Usercontrol');

Set<String> newemployees = new Set<String>();

public string profileid;
List<Profile> pro= new List<Profile>();
List<UserRole> role= new List<UserRole>();
list<user> newuserinsert=new list<user>();
list<string> listOfIds = new list<string>();
Set<String> existingemployee = new Set<String>();
Map<String,AxtriaSalesIQTM__Employee__c> pridtoemployeee  = new Map<String,AxtriaSalesIQTM__Employee__c>();

if (!test.isRunningTest() && myCS1.AxtriaSalesIQTM__IsStopTrigger__c !=true){
  if(trigger.isInsert){

       for (AxtriaSalesIQTM__Employee__c emp : trigger.new)

       {
            if(emp.AxtriaSalesIQTM__Employee_ID__c != null)
            {

                newemployees.add(emp.AxtriaSalesIQTM__Employee_ID__c);
                pridtoemployeee.put(emp.AxtriaSalesIQTM__Employee_ID__c,emp);

            }

       }


       List<User> existinguser = [select id, FederationIdentifier From User where FederationIdentifier in:newemployees];

       for (user us : existinguser){

            existingemployee.add(us.FederationIdentifier);

       }


       if(newemployees!=null && newemployees.size()>0 ){

            newemployees.removeall(existingemployee);

       }


     pro= [SELECT Id,Name FROM Profile WHERE Name = 'Rep' limit 1];
    system.debug('=================PROFILE :::'+pro);
    role=  [SELECT Id,Name FROM UserRole WHERE Name = 'Rep' OR Name = 'OTM' limit 1];
    profileid = string.valueof(pro[0].id);
    system.debug('================profileid:::::::::::'+profileid);
 
    for(string newprid: newemployees)
    {    
        //string country= mappridtocountry.get(newprid);
         system.debug('============newprid is ::'+newprid);
         /*if(timeZOne.containsKey(country)) 
        {*/
            system.debug('==========================About to create a new user:');
         //Time_Zone__c tzone= timeZOne.get(country);
         user u= new user();
         AxtriaSalesIQTM__Employee__c emp= new AxtriaSalesIQTM__Employee__c();
         emp= pridtoemployeee.get(newprid);
         u.email= emp.AxtriaSalesIQTM__Email__c;
         u.IsActive=TRUE;
         //u.Name = emp.Employee_First_Name__c;
         u.username= emp.AxtriaSalesIQTM__Email__c;
         u.FederationIdentifier= (emp.AxtriaSalesIQTM__Employee_ID__c).toLowerCase();
         u.ProfileID=profileid;
         u.UserRole=role[0];
         u.FirstName=emp.AxtriaSalesIQTM__FirstName__c;
         u.LastName=emp.AxtriaSalesIQTM__Last_Name__c;
         u.Alias= emp.AxtriaSalesIQTM__Employee_ID__c.substring(0,7);
         u.CommunityNickname=emp.AxtriaSalesIQTM__FirstName__c;
         u.EmailEncodingKey='unicode(UTF-8)';
         //u.DefaultCurrencyIsoCode='EUR-Euro';
         u.TimeZoneSidKey= 'GMT';
         u.LocaleSidKey= 'en_US';
         u.LanguageLocaleKey= 'en_US';
         u.EmailEncodingKey='ISO-8859-1';
         //u.CurrencyIsoCode='USD';
         newuserinsert.add(u);
        //}
     }
     system.debug('+++newuserinsert+++'+newuserinsert.size());
     if(newuserinsert!=null && newuserinsert.size() >0)
     {
        database.SaveResult[] insertResults=  database.insert(newuserinsert,false);
        for (Database.SaveResult sr : insertResults) 
        {
            if (sr.isSuccess())
            {
                listOfIds.add((string)sr.getId());
            }
            else
            {
                system.debug('Error Message'+sr.getErrors());
            }
        }
        system.debug('==================new inserted users count is :'+listOfIds);
        
  
    }


  }

}

}