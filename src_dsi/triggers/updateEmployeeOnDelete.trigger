trigger updateEmployeeOnDelete on Leave_Journey__c (after Delete) {
  
    AxtriaSalesIQTM__TriggerContol__c myCS2 = AxtriaSalesIQTM__TriggerContol__c.getValues('RosterTriggers');

    if(myCS2.AxtriaSalesIQTM__IsStopTrigger__c !=true){
        if(trigger.isAfter && trigger.isDelete){
            
            List <AxtriaSalesIQTM__Employee__c>empList = [Select id, Action_Type__c, On_Training__c , on_Active_Leave__c , Leave_Start_Date__c from AxtriaSalesIQTM__Employee__c where Action_Type__c = 'LOA-R'];
            
            for(AxtriaSalesIQTM__Employee__c  emp : empList){
                    if(emp.Action_Type__c == 'LOA-R'){
                        emp.Leave_Start_Date__c = null;
                    }
                    if(emp.On_Training__c == true && emp.on_Active_Leave__c == TRUE){
                        
                        emp.IC_Status__c = 'LTRN';
                    }
                    else if(emp.On_Training__c == true && emp.on_Active_Leave__c != TRUE){
                        emp.IC_Status__c = 'TRN';
                    }
                    else if(emp.On_Training__c != true && emp.on_Active_Leave__c == TRUE){
                        emp.IC_Status__c = 'LOA';
                    }
                    else{
                        emp.IC_Status__c = 'CE';
                    }
            }
            
            if(empList.size()>0){
                BatchProcessCRRosterEvents.stopExecution = true;
                update empList;
            }
        }
    }
    
}