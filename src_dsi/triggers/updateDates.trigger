trigger updateDates on AxtriaSalesIQST__CR_Employee_Feed__c (before insert, before Update) {
    
    AxtriaSalesIQTM__TriggerContol__c myCS2 = AxtriaSalesIQTM__TriggerContol__c.getValues('RosterTriggers');

    if(myCS2.AxtriaSalesIQTM__IsStopTrigger__c !=true){
        Set<String> empIdSet = new set<String>();
        for(AxtriaSalesIQST__CR_Employee_Feed__c  crEmpFeed : trigger.new){
            empIdSet.add(crEmpFeed.Employee_ID__c);
        }
        List <AxtriaSalesIQTM__Employee__c> empList = [Select id, Termination_Date__c, AxtriaSalesIQTM__Employee_ID__c , Reason_Code__c, Action_Start_Date__c from AxtriaSalesIQTM__Employee__c where AxtriaSalesIQTM__Employee_ID__c =: empIdSet];
        
        for(AxtriaSalesIQST__CR_Employee_Feed__c  crEmpFeed : trigger.new){
            for(AxtriaSalesIQTM__Employee__c emp : empList){
                if(crEmpFeed.Employee_ID__c == emp.AxtriaSalesIQTM__Employee_ID__c ){
                
                    if(crEmpFeed.AxtriaSalesIQST__IsRemoved__c == False && crEmpFeed.AxtriaSalesIQST__Event_Name__c == 'Employee New Hire' 
                         && emp.Reason_Code__c == 'Hire' ){
                        crEmpFeed.Event_Start_Date_from_Staging__c = emp.Action_Start_Date__c ;
                    }
                    
                    if(crEmpFeed.AxtriaSalesIQST__IsRemoved__c == False && crEmpFeed.AxtriaSalesIQST__Event_Name__c == emp.Reason_Code__c && crEmpFeed.isLastLeaveEvent__c == TRUE && emp.Reason_Code__c == 'Return from LOA' && crEmpFeed.AxtriaSalesIQST__Event_Name__c == 'Return from LOA'){
                        crEmpFeed.Event_Start_Date_from_Staging__c = emp.Action_Start_Date__c ;
                    }
                    if(crEmpFeed.AxtriaSalesIQST__IsRemoved__c == False && crEmpFeed.AxtriaSalesIQST__Event_Name__c == emp.Reason_Code__c && crEmpFeed.isLastLeaveEvent__c == TRUE && emp.Reason_Code__c == 'Leave of Absence' && crEmpFeed.AxtriaSalesIQST__Event_Name__c == 'Leave of Absence'){
                        crEmpFeed.Event_Start_Date_from_Staging__c = emp.Action_Start_Date__c ;
                    }
                    if(crEmpFeed.AxtriaSalesIQST__IsRemoved__c == False && emp.Reason_Code__c == 'Blank out Active Return from LOA' && crEmpFeed.AxtriaSalesIQST__Event_Name__c == 'Return from LOA' && crEmpFeed.isLastLeaveEvent__c == TRUE ){
                        crEmpFeed.AxtriaSalesIQST__IsRemoved__c = true;
                    }
                    if(crEmpFeed.AxtriaSalesIQST__IsRemoved__c == False && emp.Reason_Code__c == 'Blank out Leave of Absence' && crEmpFeed.AxtriaSalesIQST__Event_Name__c == 'Leave of Absence' && crEmpFeed.isLastLeaveEvent__c == TRUE ){
                        crEmpFeed.AxtriaSalesIQST__IsRemoved__c = true;
                    }
                    if(crEmpFeed.AxtriaSalesIQST__IsRemoved__c == False && emp.Reason_Code__c == 'Terminate Employee' && emp.Termination_Date__c == null && crEmpFeed.AxtriaSalesIQST__Event_Name__c == 'Terminate Employee'){
                        crEmpFeed.AxtriaSalesIQST__IsRemoved__c = true;
                    }
                }
            }
        }
    }
}