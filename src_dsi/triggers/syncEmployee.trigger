trigger syncEmployee on Training_Journey__c (after insert, after update) {

    Set<Id> empIdSet = new Set<Id>();
    for(Training_Journey__c trn : trigger.new){
        empIdSet.add(trn.Employee__c);
    }

    List<AxtriaSalesIQTM__Employee__c> empList = [select id, AxtriaSalesIQTM__Employee_ID__c,AxtriaSalesIQTM__Current_Territory__c, Training_Start_Date__c, IC_Status__c, Training_End_Date__c, On_Training__c, Latest_Training_Journey__c, Training_Type__c, isTrainingCompleted2__c, isTrainingCompleted__c from AxtriaSalesIQTM__Employee__c ];

    List<AxtriaSalesIQTM__Position_Employee__c> posEmpList = [select id, AxtriaSalesIQTM__Employee__c, AxtriaSalesIQTM__Position__r.SalesforceCode__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQST__Territory_ID__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c, AxtriaSalesIQTM__Position__r.Sales_Force_Code__c, AxtriaSalesIQTM__Position__r.Sales_Force__c, AxtriaSalesIQTM__Employee__r.Termination_Date__c, AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Last_Name__c, AxtriaSalesIQTM__Employee__r.Payroll_Id__c, AxtriaSalesIQTM__Employee__r.Original_Hire_Date__c, AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__FirstName__c, AxtriaSalesIQTM__Employee__r.Personnel_Subarea_Code__c, AxtriaSalesIQTM__Position__r.Name, AxtriaSalesIQTM__Employee__r.Personnel_Subarea_Name__c, AxtriaSalesIQTM__Employee__r.Personnel_Area__c, AxtriaSalesIQTM__Employee__r.Personnel_Number__c, AxtriaSalesIQTM__Employee__r.Personnel_Area_Number__c, AxtriaSalesIQTM__Employee__r.Reason_Code__c, AxtriaSalesIQTM__Employee__r.Position_Title__c, AxtriaSalesIQTM__Position__r.SalesforceName__c,  AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Email__c  from AxtriaSalesIQTM__Position_Employee__c];


    List<AxtriaSalesIQTM__Employee__c> updateEmpList = new List<AxtriaSalesIQTM__Employee__c>();
    List<AxtriaSalesIQTM__Employee__c> afterUpdateEmpList = new List<AxtriaSalesIQTM__Employee__c>();

    List<Employee_History__c> empHistoryUpdate2List = [Select id, Employee__c, LatestHistory__c, Action_Reason__c, Employee_ID__c, End_Date__c, Event_Name__c, IC_Status__c, Position__c, Start_Date__c from Employee_History__c where Employee__c =: empIdSet];
        List<Employee_History__c> empHistoryUpdateList = new list<Employee_History__c>();
        List<Employee_History__c> newEmployeeHistorylist = new list<Employee_History__c>();
    AxtriaSalesIQTM__TriggerContol__c myCS2 = AxtriaSalesIQTM__TriggerContol__c.getValues('RosterTriggers');

    if(myCS2.AxtriaSalesIQTM__IsStopTrigger__c !=true){
        if(trigger.isInsert && trigger.isAfter){
            for(Training_Journey__c trn : trigger.new){
                for(AxtriaSalesIQTM__Employee__c emp : empList){
                    if(trn.Employee__c == emp.Id){
                        emp.Training_Start_Date__c = trn.Training_Start_Date__c;
                        emp.Training_End_Date__c = trn.Training_End_Date__c;
                        emp.On_Training__c = True;
                        emp.Latest_Training_Journey__c = trn.id;
                        emp.Training_Type__c = trn.Training_Type__c;
                        emp.isTrainingCompleted__c = false;
                        updateEmpList.add(emp);
                    }
                }
            }
        }

        if(trigger.isUpdate && trigger.isAfter){

            
            List<AxtriaSalesIQTM__Employee__c> empLatestList = [select id, AxtriaSalesIQTM__Employee_ID__c,AxtriaSalesIQTM__Current_Territory__c, Training_Start_Date__c, IC_Status__c, Training_End_Date__c, On_Training__c, Latest_Training_Journey__c, Training_Type__c, isTrainingCompleted2__c, isTrainingCompleted__c from AxtriaSalesIQTM__Employee__c ];

            // End dating the training record manaually

            for(Training_Journey__c trn : trigger.new){
                
                if(trigger.oldMap.get(trn.id).Training_End_Date__c != trigger.newMap.get(trn.id).Training_End_Date__c){
                    if(trn.Training_End_Date__c != null){
                        for(AxtriaSalesIQTM__Employee__c emp : empList){
                            if(trn.id == emp.Latest_Training_Journey__c){
                                emp.Training_Start_Date__c = trn.Training_Start_Date__c;
                                emp.Training_End_Date__c = trn.Training_End_Date__c;
                                emp.On_Training__c = false;
                                emp.Latest_Training_Journey__c = trn.id;
                                emp.Training_Type__c = trn.Training_Type__c;
                                emp.isTrainingCompleted__c = true;
                                if(!afterUpdateEmpList.contains(emp)){
                                    afterUpdateEmpList.add(emp);
                                }
                            }
                        }   
                    }
                    else{
                        for(AxtriaSalesIQTM__Employee__c emp : empList){
                            if(trn.id == emp.Latest_Training_Journey__c){
                                emp.Training_Start_Date__c = trn.Training_Start_Date__c;
                                emp.Training_End_Date__c = trn.Training_End_Date__c;
                                if(trn.Training_End_Date__c == null){
                                    emp.On_Training__c = true;
                                    emp.isTrainingCompleted__c = false;
                                }
                                else{
                                    emp.On_Training__c = false;
                                    emp.isTrainingCompleted__c = true;
                                }
                                emp.Latest_Training_Journey__c = trn.id;
                                emp.Training_Type__c = trn.Training_Type__c;
                                
                                if(!afterUpdateEmpList.contains(emp)){
                                    afterUpdateEmpList.add(emp);
                                }
                            }
                        }
                    }
                }
                if(trigger.oldMap.get(trn.id).Training_Start_Date__c != trigger.newMap.get(trn.id).Training_Start_Date__c){
                    for(AxtriaSalesIQTM__Employee__c emp : empList){
                        if(trn.id == emp.Latest_Training_Journey__c){
                            emp.Training_Start_Date__c = trn.Training_Start_Date__c;
                            emp.Training_End_Date__c = trn.Training_End_Date__c;
                            if(trn.Training_End_Date__c == null){
                                emp.On_Training__c = true;
                                emp.isTrainingCompleted__c = false;
                            }
                            else{
                                emp.On_Training__c = false;
                                emp.isTrainingCompleted__c = true;
                            }
                            emp.Latest_Training_Journey__c = trn.id;
                            emp.Training_Type__c = trn.Training_Type__c;

                            if(!afterUpdateEmpList.contains(emp)){
                                afterUpdateEmpList.add(emp);
                            }
                        }
                    }
                }

                // checking the manual update of training by end user.
                if(trigger.oldMap.get(trn.id).Training_End_Date__c != trigger.newMap.get(trn.id).Training_End_Date__c){
                    if(trn.Manual_Update__c == true){
                        for(Employee_History__c empHis : empHistoryUpdate2List){
                            for(AxtriaSalesIQTM__Position_Employee__c posEmp : posEmpList){
                                if(trn.Employee__c == posEmp.AxtriaSalesIQTM__Employee__c && empHis.Position__c == posEmp.AxtriaSalesIQTM__Position__c && trn.Employee__c == empHis.Employee__c && empHis.LatestHistory__c == true){
                                    empHis.End_Date__c = trn.Training_End_Date__c-1;
                                    empHis.LatestHistory__c = false;
                                    empHis.Client_Territory_Code__c = posEmp.AxtriaSalesIQST__Territory_ID__c;
                                    empHis.Position_Type__c = posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c;
                                    empHis.Termination_Date__c = posEmp.AxtriaSalesIQTM__Employee__r.Termination_Date__c;
                                    empHis.Sales_Force_Code__c = posEmp.AxtriaSalesIQTM__Position__r.SalesforceCode__c;
                                    empHis.Sales_Force__c = posEmp.AxtriaSalesIQTM__Position__r.SalesforceName__c;
                                    empHis.Payroll_Id__c = posEmp.AxtriaSalesIQTM__Employee__r.Payroll_Id__c;
                                    empHis.Last_Name__c = posEmp.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Last_Name__c;
                                    empHis.Hire_Date__c = posEmp.AxtriaSalesIQTM__Employee__r.Original_Hire_Date__c;
                                    empHis.First_Name__c = posEmp.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__FirstName__c;
                                    empHis.Geography_Name__c = posEmp.AxtriaSalesIQTM__Position__r.Name;
                                    empHis.Personnel_Subarea_Code__c = posEmp.AxtriaSalesIQTM__Employee__r.Personnel_Subarea_Code__c;
                                    empHis.Personnel_Subarea_Name__c = posEmp.AxtriaSalesIQTM__Employee__r.Personnel_Subarea_Name__c;
                                    empHis.Personnel_Area__c = posEmp.AxtriaSalesIQTM__Employee__r.Personnel_Area__c;
                                    empHis.Personnel_Number__c = posEmp.AxtriaSalesIQTM__Employee__r.Personnel_Number__c;
                                    empHis.Personnel_Area_Number__c = posEmp.AxtriaSalesIQTM__Employee__r.Personnel_Area_Number__c;
                                    empHis.Reason_Code__c = posEmp.AxtriaSalesIQTM__Employee__r.Reason_Code__c;
                                    empHis.Position_Title__c = posEmp.AxtriaSalesIQTM__Employee__r.Position_Title__c; 
                                    empHis.Email__c = posEmp.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Email__c;
                                    empHistoryUpdateList.add(empHis);

                                    for(AxtriaSalesIQTM__Employee__c emp : empLatestList){
                                        if(emp.Id == trn.Employee__c){
                                            Employee_History__c empHis1 = new Employee_History__c();
                                            empHis1.Employee_ID__c = emp.AxtriaSalesIQTM__Employee_ID__c;
                                            empHis1.LatestHistory__c = true;
                                            if(emp.IC_Status__c == 'TRN'){
                                                empHis1.IC_Status__c = 'CE';
                                            }
                                            else if(emp.IC_Status__c == 'LTRN'){
                                                empHis1.IC_Status__c = 'LOA';
                                            }
                                            else{
                                                empHis1.IC_Status__c = emp.IC_Status__c;
                                            }
                                            
                                            empHis1.Start_Date__c = trn.Training_End_Date__c;
                                            empHis1.Event_Name__c = 'Training End Dated Manually';
                                            empHis1.Action_Reason__c = 'Generating this Entry because Training End Dated Manually';
                                            empHis1.End_Date__c = Date.newInstance(4000, 12, 31);
                                            
                                            empHis1.Employee__c = emp.Id;
                                            empHis1.Position__c = emp.AxtriaSalesIQTM__Current_Territory__c;
                                            
                                            empHis1.Client_Territory_Code__c = posEmp.AxtriaSalesIQST__Territory_ID__c;
                                            empHis1.Position_Type__c = posEmp.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c;
                                            empHis1.Termination_Date__c = posEmp.AxtriaSalesIQTM__Employee__r.Termination_Date__c;
                                            empHis1.Sales_Force_Code__c = posEmp.AxtriaSalesIQTM__Position__r.SalesforceCode__c;
                                            empHis1.Sales_Force__c = posEmp.AxtriaSalesIQTM__Position__r.SalesforceName__c;
                                            empHis1.Payroll_Id__c = posEmp.AxtriaSalesIQTM__Employee__r.Payroll_Id__c;
                                            empHis1.Last_Name__c = posEmp.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Last_Name__c;
                                            empHis1.Hire_Date__c = posEmp.AxtriaSalesIQTM__Employee__r.Original_Hire_Date__c;
                                            empHis1.First_Name__c = posEmp.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__FirstName__c;
                                            empHis1.Geography_Name__c = posEmp.AxtriaSalesIQTM__Position__r.Name;
                                            empHis1.Personnel_Subarea_Code__c = posEmp.AxtriaSalesIQTM__Employee__r.Personnel_Subarea_Code__c;
                                            empHis1.Personnel_Subarea_Name__c = posEmp.AxtriaSalesIQTM__Employee__r.Personnel_Subarea_Name__c;
                                            empHis1.Personnel_Area__c = posEmp.AxtriaSalesIQTM__Employee__r.Personnel_Area__c;
                                            empHis1.Personnel_Number__c = posEmp.AxtriaSalesIQTM__Employee__r.Personnel_Number__c;
                                            empHis1.Personnel_Area_Number__c = posEmp.AxtriaSalesIQTM__Employee__r.Personnel_Area_Number__c;
                                            empHis1.Reason_Code__c = posEmp.AxtriaSalesIQTM__Employee__r.Reason_Code__c;
                                            empHis1.Position_Title__c = posEmp.AxtriaSalesIQTM__Employee__r.Position_Title__c; 
                                            empHis1.Email__c = posEmp.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Email__c;
                                            newEmployeeHistorylist.add(empHis1);
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
            }
        }

        if(newEmployeeHistorylist.size()>0){
            BatchProcessCRRosterEvents.stopExecution = true;
            insert newEmployeeHistorylist;

        }

        if(empHistoryUpdateList.size()>0){
            BatchProcessCRRosterEvents.stopExecution = true;
            update empHistoryUpdateList;

        }

        if(updateEmpList.size()>0){
            BatchProcessCRRosterEvents.stopExecution = true;
            update updateEmpList;

        }
        if(afterUpdateEmpList.size()>0){
            BatchProcessCRRosterEvents.stopExecution = true;
            update afterUpdateEmpList;
            
        }
    }
}