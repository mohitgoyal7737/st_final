trigger updateEmployee on Leave_Journey__c (after insert, after update) {
    AxtriaSalesIQTM__TriggerContol__c myCS2 = AxtriaSalesIQTM__TriggerContol__c.getValues('RosterTriggers');

    if(myCS2.AxtriaSalesIQTM__IsStopTrigger__c !=true){
        Set<Id> empIdSet = new Set<Id>();
        for(Leave_Journey__c  lj : trigger.new){
            empIdSet.add(lj.Employee__c);
        }
        List<AxtriaSalesIQTM__Employee__c> empList = [Select id, Current_Leave_Journey__c, On_Training__c, on_Active_Leave__c, Leave_Start_Date__c, Leave_Return_Date__c from AxtriaSalesIQTM__Employee__c where id =: empIdSet];
        if(trigger.isAfter && trigger.isInsert){
            for(Leave_Journey__c  lj : trigger.new){
                for(AxtriaSalesIQTM__Employee__c emp : empList){
                    if(lj.Employee__c == emp.Id){
                        emp.Current_Leave_Journey__c = lj.id;
                        emp.Leave_Start_Date__c = lj.Leave_Start_Date__c;
                        emp.Leave_Return_Date__c = lj.Leave_End_Date__c;
                    }
                }
            }
        }
         
        if(trigger.isAfter && trigger.isupdate){
            for(Leave_Journey__c  lj : trigger.new){
                for(AxtriaSalesIQTM__Employee__c emp : empList){
                    if(lj.id== emp.Current_Leave_Journey__c){
                        emp.Leave_Start_Date__c = lj.Leave_Start_Date__c;
                        emp.Leave_Return_Date__c = lj.Leave_End_Date__c;
                    }
                }
            }
        }
        
        if(empList.size()>0){
            BatchProcessCRRosterEvents.stopExecution = true;
            update empList;
        }
    }
}