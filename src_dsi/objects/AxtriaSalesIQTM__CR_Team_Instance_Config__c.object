<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Holds information about Change Requests specific Configuration</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>Private</externalSharingModel>
    <fields>
        <fullName>AxtriaSalesIQTM__Bulk_Edit_Allowed__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Bulk Edit Allowed</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Change_Request_Types__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Change Request Type</label>
        <referenceTo>AxtriaSalesIQTM__Change_Request_Type__c</referenceTo>
        <relationshipLabel>CR_Team_Instance_Configs</relationshipLabel>
        <relationshipName>CR_Team_Instance_Configs</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Comments__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Comments</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Config_Value__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Config Value</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Configuration_Name__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Configuration Name</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Configuration_Type__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Configuration Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Hierarchy Configuration</fullName>
                    <default>false</default>
                    <label>Hierarchy Configuration</label>
                </value>
                <value>
                    <fullName>Layer Configuration</fullName>
                    <default>false</default>
                    <label>Layer Configuration</label>
                </value>
                <value>
                    <fullName>Non Field Position Type</fullName>
                    <default>false</default>
                    <label>Non Field Position Type</label>
                </value>
                <value>
                    <fullName>Data Table Configuration</fullName>
                    <default>false</default>
                    <label>Data Table Configuration</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Configuration_Value__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Configuration Value</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Create__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Creation allowed or not at the corresponding level</description>
        <externalId>false</externalId>
        <label>Create</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Delete__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Delete allowed or not at corresponding hierarchy level</description>
        <externalId>false</externalId>
        <label>Delete</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Edit__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Edit allowed or not at corresponding hierarchy level</description>
        <externalId>false</externalId>
        <label>Edit Overlay Association</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Fixed_Column_From_Left__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Fixed Column From Left</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Fixed_Column_From_Right__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Fixed Column From Right</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Hierarchy_Change__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Hierarchy Change</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Hierarchy_Level_Number__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF(ISPICKVAL(AxtriaSalesIQTM__Configuration_Type__c,&apos;Hierarchy Configuration&apos;), VALUE(AxtriaSalesIQTM__Configuration_Name__c), 0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Hierarchy Level Number</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__IsWrapText__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>IsWrapText</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__MaximumRecordsSaved__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Maximum Records Saved</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Page_Size__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Page Size</label>
        <precision>4</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Pagination_on__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>enable pagination?</description>
        <externalId>false</externalId>
        <label>Pagination on</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Review_Allowed__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If set to true, Rejection of Change Request sends the CR back for review and all the changes are not reverted.</description>
        <externalId>false</externalId>
        <label>Review Allowed</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__RowHeight__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Row Height</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Team_Instance__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Lookup to Team Instance Object</description>
        <externalId>false</externalId>
        <label>Team Instance</label>
        <referenceTo>AxtriaSalesIQTM__Team_Instance__c</referenceTo>
        <relationshipLabel>CR_Team_Instance_Configs</relationshipLabel>
        <relationshipName>CR_Team_Instance_Configs</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Update_Position_Attributes__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Update Position Attributes</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>CR Team Instance Config</label>
    <listViews>
        <fullName>ALL</fullName>
        <columns>NAME</columns>
        <columns>AxtriaSalesIQTM__Configuration_Name__c</columns>
        <columns>AxtriaSalesIQTM__Configuration_Type__c</columns>
        <columns>AxtriaSalesIQTM__Config_Value__c</columns>
        <columns>AxtriaSalesIQTM__Configuration_Value__c</columns>
        <columns>AxtriaSalesIQTM__Bulk_Edit_Allowed__c</columns>
        <columns>AxtriaSalesIQTM__Fixed_Column_From_Left__c</columns>
        <columns>AxtriaSalesIQTM__Fixed_Column_From_Right__c</columns>
        <columns>AxtriaSalesIQTM__Page_Size__c</columns>
        <columns>AxtriaSalesIQTM__Pagination_on__c</columns>
        <columns>AxtriaSalesIQTM__Review_Allowed__c</columns>
        <columns>AxtriaSalesIQTM__Team_Instance__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>AxtriaSalesIQTM__Team_Instance__c</field>
            <operation>equals</operation>
            <value>OncB_Q321</value>
        </filters>
        <label>ALL</label>
    </listViews>
    <listViews>
        <fullName>AxtriaSalesIQTM__Add_Target_Table_Configuration</fullName>
        <columns>NAME</columns>
        <columns>AxtriaSalesIQTM__Configuration_Name__c</columns>
        <columns>AxtriaSalesIQTM__Configuration_Type__c</columns>
        <columns>AxtriaSalesIQTM__Configuration_Value__c</columns>
        <columns>AxtriaSalesIQTM__Bulk_Edit_Allowed__c</columns>
        <columns>AxtriaSalesIQTM__Fixed_Column_From_Left__c</columns>
        <columns>AxtriaSalesIQTM__Fixed_Column_From_Right__c</columns>
        <columns>AxtriaSalesIQTM__Page_Size__c</columns>
        <columns>AxtriaSalesIQTM__Pagination_on__c</columns>
        <columns>AxtriaSalesIQTM__Review_Allowed__c</columns>
        <columns>AxtriaSalesIQTM__Team_Instance__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>AxtriaSalesIQTM__Configuration_Type__c</field>
            <operation>equals</operation>
            <value>Date Table Configuration</value>
        </filters>
        <filters>
            <field>AxtriaSalesIQTM__Configuration_Name__c</field>
            <operation>equals</operation>
            <value>Add Target</value>
        </filters>
        <label>Add Target Table Configuration</label>
    </listViews>
    <listViews>
        <fullName>AxtriaSalesIQTM__Call_Plan_Table_Configuration</fullName>
        <columns>NAME</columns>
        <columns>AxtriaSalesIQTM__Configuration_Name__c</columns>
        <columns>AxtriaSalesIQTM__Configuration_Type__c</columns>
        <columns>AxtriaSalesIQTM__Configuration_Value__c</columns>
        <columns>AxtriaSalesIQTM__Bulk_Edit_Allowed__c</columns>
        <columns>AxtriaSalesIQTM__Fixed_Column_From_Left__c</columns>
        <columns>AxtriaSalesIQTM__Fixed_Column_From_Right__c</columns>
        <columns>AxtriaSalesIQTM__Page_Size__c</columns>
        <columns>AxtriaSalesIQTM__Pagination_on__c</columns>
        <columns>AxtriaSalesIQTM__Review_Allowed__c</columns>
        <columns>AxtriaSalesIQTM__Team_Instance__c</columns>
        <columns>AxtriaSalesIQTM__IsWrapText__c</columns>
        <columns>AxtriaSalesIQTM__MaximumRecordsSaved__c</columns>
        <columns>AxtriaSalesIQTM__RowHeight__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>AxtriaSalesIQTM__Configuration_Type__c</field>
            <operation>equals</operation>
            <value>Data Table Configuration</value>
        </filters>
        <filters>
            <field>AxtriaSalesIQTM__Configuration_Name__c</field>
            <operation>equals</operation>
            <value>Call Plan</value>
        </filters>
        <label>Call Plan Table Configuration</label>
    </listViews>
    <listViews>
        <fullName>AxtriaSalesIQTM__Hierarchy_Configuration</fullName>
        <columns>NAME</columns>
        <columns>AxtriaSalesIQTM__Change_Request_Types__c</columns>
        <columns>AxtriaSalesIQTM__Configuration_Name__c</columns>
        <columns>AxtriaSalesIQTM__Config_Value__c</columns>
        <columns>AxtriaSalesIQTM__Team_Instance__c</columns>
        <columns>AxtriaSalesIQTM__Configuration_Value__c</columns>
        <columns>AxtriaSalesIQTM__Configuration_Type__c</columns>
        <columns>AxtriaSalesIQTM__Create__c</columns>
        <columns>AxtriaSalesIQTM__Delete__c</columns>
        <columns>AxtriaSalesIQTM__Edit__c</columns>
        <columns>AxtriaSalesIQTM__Hierarchy_Change__c</columns>
        <columns>AxtriaSalesIQTM__Update_Position_Attributes__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>AxtriaSalesIQTM__Configuration_Type__c</field>
            <operation>equals</operation>
            <value>Hierarchy Configuration,Layer Configuration</value>
        </filters>
        <filters>
            <field>AxtriaSalesIQTM__Team_Instance__c</field>
            <operation>equals</operation>
            <value>RD2021_Q3_Axtria</value>
        </filters>
        <label>Hierarchy Configuration</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>AxtriaSalesIQTM__Layer_Configuration</fullName>
        <columns>NAME</columns>
        <columns>AxtriaSalesIQTM__Configuration_Name__c</columns>
        <columns>AxtriaSalesIQTM__Configuration_Type__c</columns>
        <columns>AxtriaSalesIQTM__Config_Value__c</columns>
        <columns>AxtriaSalesIQTM__Team_Instance__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>AxtriaSalesIQTM__Configuration_Type__c</field>
            <operation>equals</operation>
            <value>Layer Configuration</value>
        </filters>
        <filters>
            <field>AxtriaSalesIQTM__Team_Instance__c</field>
            <operation>equals</operation>
            <value>OncB_Q421</value>
        </filters>
        <label>Layer Configuration</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>AxtriaSalesIQTM__Non_Field_Position_Type</fullName>
        <columns>NAME</columns>
        <columns>AxtriaSalesIQTM__Configuration_Name__c</columns>
        <columns>AxtriaSalesIQTM__Configuration_Value__c</columns>
        <columns>AxtriaSalesIQTM__Configuration_Type__c</columns>
        <columns>AxtriaSalesIQTM__Create__c</columns>
        <columns>AxtriaSalesIQTM__Update_Position_Attributes__c</columns>
        <columns>AxtriaSalesIQTM__Delete__c</columns>
        <columns>AxtriaSalesIQTM__Hierarchy_Change__c</columns>
        <columns>AxtriaSalesIQTM__Edit__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>AxtriaSalesIQTM__Configuration_Type__c</field>
            <operation>equals</operation>
            <value>Non Field Position Type</value>
        </filters>
        <label>Non Field Position Type</label>
    </listViews>
    <nameField>
        <displayFormat>CRTIC-{000000000}</displayFormat>
        <label>CR Team Instance Config  Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>CR Team Instance Configs</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
