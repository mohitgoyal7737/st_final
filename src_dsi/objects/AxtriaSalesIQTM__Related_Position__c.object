<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>Private</externalSharingModel>
    <fields>
        <fullName>AxtriaSalesIQTM__Assignment_Status__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF( AxtriaSalesIQTM__Effective_Start_Date__c &gt; Today(),&apos;Future Active&apos;, 
IF(AxtriaSalesIQTM__Effective_Start_Date__c &lt;= Today() &amp;&amp; AxtriaSalesIQTM__Effective_End_Date__c &gt;= Today(),&apos;Active&apos;, 
IF(AxtriaSalesIQTM__Effective_End_Date__c &lt; Today(),&apos;Inactive&apos;,&apos;&apos;)))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Assignment Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Base_Position_Team_Instance__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Lookup to Position Team Instance</inlineHelpText>
        <label>Base Position Team Instance</label>
        <referenceTo>AxtriaSalesIQTM__Position_Team_Instance__c</referenceTo>
        <relationshipLabel>Related Positions</relationshipLabel>
        <relationshipName>Related_Positions</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Base_Position__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Base Position</inlineHelpText>
        <label>Base Position</label>
        <referenceTo>AxtriaSalesIQTM__Position__c</referenceTo>
        <relationshipLabel>Related Positions (Footprint)</relationshipLabel>
        <relationshipName>POD_Territory1</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Change_Status__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Specify the Change Status of the Related Position new Record. The Possible values can be Rejected, Pending, Approved No Change. Default Value is No Change</inlineHelpText>
        <label>Change Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Approved</fullName>
                    <default>false</default>
                    <label>Approved</label>
                </value>
                <value>
                    <fullName>No Change</fullName>
                    <default>false</default>
                    <label>No Change</label>
                </value>
                <value>
                    <fullName>Pending</fullName>
                    <default>false</default>
                    <label>Pending</label>
                </value>
                <value>
                    <fullName>Rejected</fullName>
                    <default>false</default>
                    <label>Rejected</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Effective_End_Date__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Effective End Date for the Related Position Record</inlineHelpText>
        <label>Effective End Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Effective_Start_Date__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Effective Start Date for the Related Position Record</inlineHelpText>
        <label>Effective Start Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Related_Position_Team_Instance__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Lookup to Position Team Instance for the Related Position</inlineHelpText>
        <label>Related Position Team Instance</label>
        <referenceTo>AxtriaSalesIQTM__Position_Team_Instance__c</referenceTo>
        <relationshipLabel>Related Positions (Related Position Team Instance)</relationshipLabel>
        <relationshipName>Related_Positions1</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Related_Position_Type__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Specify the relationship type of the position like Base, Overlay, Mirror</inlineHelpText>
        <label>Related Position Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>AxtriaSalesIQTM__Related_Position_Type</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Related_Position__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Specify the Related Position</inlineHelpText>
        <label>Related Position</label>
        <referenceTo>AxtriaSalesIQTM__Position__c</referenceTo>
        <relationshipLabel>Related Positions (Position)</relationshipLabel>
        <relationshipName>POD_Territory</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__isActive__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Specify if the Related Position Status. It can be either active or Inactive</inlineHelpText>
        <label>isActive</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Related Position</label>
    <listViews>
        <fullName>AxtriaSalesIQTM__All</fullName>
        <columns>NAME</columns>
        <columns>AxtriaSalesIQTM__Base_Position__c</columns>
        <columns>AxtriaSalesIQTM__Base_Position_Team_Instance__c</columns>
        <columns>AxtriaSalesIQTM__Change_Status__c</columns>
        <columns>AxtriaSalesIQTM__Related_Position_Team_Instance__c</columns>
        <columns>AxtriaSalesIQTM__Related_Position__c</columns>
        <columns>AxtriaSalesIQTM__Related_Position_Type__c</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>AxtriaSalesIQTM__All1</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>RP-{000000000}</displayFormat>
        <label>Related Position</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Related Positions</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
