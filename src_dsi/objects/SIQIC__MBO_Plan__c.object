<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Stores Plan related to each team_cycle and Type of Role</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>Private</externalSharingModel>
    <fields>
        <fullName>SIQIC__Business_Unit__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Business Unit</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SIQIC__Country__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Country</label>
        <length>18</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SIQIC__Role__c</fullName>
        <deprecated>false</deprecated>
        <description>Stores Profile ID for which the Plan is amde</description>
        <externalId>false</externalId>
        <label>Role</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SIQIC__Scale__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Scale</label>
        <referenceTo>SIQIC__MBO_Scale__c</referenceTo>
        <relationshipName>Plans</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SIQIC__Team__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Team</label>
        <referenceTo>SIQIC__Team__c</referenceTo>
        <relationshipName>Plans</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SIQIC__Tenant__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Tenant</label>
        <length>18</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SIQIC__Total_Weight__c</fullName>
        <deprecated>false</deprecated>
        <description>Roll-up summary for total weight of objectives for the plan.</description>
        <externalId>false</externalId>
        <label>Total Weight</label>
        <summarizedField>SIQIC__MBO_Plan_Objective__c.SIQIC__Weightage__c</summarizedField>
        <summaryForeignKey>SIQIC__MBO_Plan_Objective__c.SIQIC__MBO_Plan__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>SIQIC__Type_of_Objectives__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Type of Objectives</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Qualitative</fullName>
                    <default>false</default>
                    <label>Qualitative</label>
                </value>
                <value>
                    <fullName>Quantitative</fullName>
                    <default>false</default>
                    <label>Quantitative</label>
                </value>
                <value>
                    <fullName>Qualitative And Quantitative</fullName>
                    <default>false</default>
                    <label>Qualitative And Quantitative</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>MBO Component</label>
    <listViews>
        <fullName>SIQIC__All</fullName>
        <columns>NAME</columns>
        <columns>SIQIC__Team__c</columns>
        <columns>SIQIC__Role__c</columns>
        <columns>SIQIC__Scale__c</columns>
        <columns>SIQIC__Total_Weight__c</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>SIQIC__H1_2018</fullName>
        <columns>NAME</columns>
        <columns>SIQIC__Team__c</columns>
        <columns>SIQIC__Role__c</columns>
        <columns>SIQIC__Scale__c</columns>
        <columns>SIQIC__Total_Weight__c</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>NAME</field>
            <operation>contains</operation>
            <value>H1 2018</value>
        </filters>
        <label>H1 2018</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>SIQIC__H2_2018</fullName>
        <columns>NAME</columns>
        <columns>SIQIC__Team__c</columns>
        <columns>SIQIC__Role__c</columns>
        <columns>SIQIC__Scale__c</columns>
        <columns>SIQIC__Total_Weight__c</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>NAME</field>
            <operation>contains</operation>
            <value>H2 2018</value>
        </filters>
        <label>H2 2018</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <label>Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>MBO Components</pluralLabel>
    <searchLayouts/>
    <sharingModel>Private</sharingModel>
    <validationRules>
        <fullName>SIQIC__Total_Weight_Validation</fullName>
        <active>true</active>
        <description>Total_Weight__c &gt; 100</description>
        <errorConditionFormula>SIQIC__Total_Weight__c &gt; 100</errorConditionFormula>
        <errorMessage>Aggregate weight of the Objectives for Plan cannot exceed 100%</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
