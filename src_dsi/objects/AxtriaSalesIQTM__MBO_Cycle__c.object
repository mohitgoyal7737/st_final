<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>AxtriaSalesIQTM__Cycle_End_Date__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Cycle End Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Cycle_Start_Date__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Cycle Start Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Cycle_Status__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF(AND(AxtriaSalesIQTM__Cycle_Start_Date__c &lt;= TODAY(),AxtriaSalesIQTM__Cycle_End_Date__c &gt;= TODAY()),&apos;Open&apos;,
IF(AxtriaSalesIQTM__Cycle_End_Date__c &lt; TODAY(),&apos;Closed&apos;,
IF(AxtriaSalesIQTM__Cycle_Start_Date__c &gt; TODAY(),&apos;Not Started&apos;,&apos;&apos;)))</formula>
        <label>Cycle Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Cycle_Target_Payout__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Cycle Target Payout</label>
        <precision>14</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Cycle_Type__c</fullName>
        <deprecated>false</deprecated>
        <description>Like MBO</description>
        <externalId>false</externalId>
        <label>Cycle Type</label>
        <length>20</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Final_Cycle__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If Final cycle, include V&amp;B rating</description>
        <externalId>false</externalId>
        <label>Final Cycle</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__MBO_Roster__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Choose a Roster to be used for this Plan cycle</inlineHelpText>
        <label>MBO Roster</label>
        <referenceTo>AxtriaSalesIQTM__MBO_Roster__c</referenceTo>
        <relationshipLabel>Cycles</relationshipLabel>
        <relationshipName>Cycles</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Plan__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Plan</label>
        <referenceTo>AxtriaSalesIQTM__MBO_Plan__c</referenceTo>
        <relationshipLabel>Cycles</relationshipLabel>
        <relationshipName>Cycles</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__is_Active__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Active</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Cycle</label>
    <listViews>
        <fullName>AxtriaSalesIQTM__All</fullName>
        <columns>NAME</columns>
        <columns>AxtriaSalesIQTM__Cycle_Start_Date__c</columns>
        <columns>AxtriaSalesIQTM__Cycle_End_Date__c</columns>
        <columns>AxtriaSalesIQTM__Cycle_Type__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Cycle Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Cycles</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>AxtriaSalesIQTM__Cycle_Start_Date</fullName>
        <active>false</active>
        <errorConditionFormula>AxtriaSalesIQTM__Cycle_End_Date__c &lt;=  AxtriaSalesIQTM__Cycle_Start_Date__c</errorConditionFormula>
        <errorMessage>Cycle end data cannot be less than or equal to cycle start date</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
