public with sharing class SalesIQGlobalConstants
{

    public static string NAME_SPACE = 'AxtriaSalesIQTM__';
    public static string POSITION_TYPE_NATION = 'NATION';

    public static string POSITION_TYPE_TERRITORY = 'Territory';
    public static string POSITION_TYPE_DISTRICT = 'District';
    public static string POSITION_TYPE_REGION = 'Region';
    public static string POSITION = 'Position';

    public static string ALIGNMENT_TYPE_ZIP = 'ZIP';
    public static string ALIGNMENT_TYPE_ACCOUNT = 'Account';
    public static string ALIGNMENT_TYPE_HYBRID = 'Hybrid';

    public static string MOVEMENT_TYPE_ZIP = 'ZIP Movement';
    public static string MOVEMENT_TYPE_ACCOUNT = 'Account Movement';
    public static string MOVEMENT_TYPE_CALL_PLAN = 'Call Plan';

    public static string REQUEST_STATUS_APPROVED = 'Approved';
    public static string REQUEST_STATUS_PENDING = 'Pending';
    public static string REQUEST_STATUS_CANCELLED = 'Cancelled';
    public static string REQUEST_STATUS_REJECTED = 'Rejected';
    public static string REQUEST_STATUS_RECALLED = 'Recalled';
    public static string REQUEST_STATUS_VACANT = 'Vacant';
    public static string REQUEST_STATUS_SUBMISSION_PENDING = 'Pending for Submission';
    public static string REQUEST_STATUS_SUBMITTED = 'Submitted';
    public static string REQUEST_STATUS_NO_CHANGE = 'No Change';

    public static string ALIGNMENT_PAGE = 'alignment';

    public static integer SCALE_VALUE = 0;
    public static integer NON_SCALE_VALUE = 0;

    public static String TEXT_SOURCE = 'Source';
    public static String TEXT_TARGET = 'Target';
    public static String TEXT_DESTINATION = 'Destination';

    public static String ZIP_TYPE_POINT = 'Point' ;
    public static String ZIP_TYPE_STANDARD = 'Standard' ;

    public static string MANAGE_ASSIGNMENT = 'Manage Assignment';
    public static string EMPLOYEE_ASSIGNMENT = 'Employee Assignment';
    public static string SWAP_REP = 'Swap Rep';
    public static string EXPIRE_POSITION = 'Expire Position';
    public static string CR_STATUS_NEW = 'New';
    public static string CREATE_POSITION = 'Create Position';
    public static string DELETE_POSITION = 'Delete Position';
    public static string ROSTER = 'Roster';
    public static string FILLED = 'filled';
    public static string UNASSIGNED = 'Unassigned';
    public static string ASSIGNED = 'Assigned';

    public static string POSITION_GEOGRAPHY_ACTIVE = 'Active';
    public static string POSITION_GEOGRAPHY_INACTIVE = 'Inactive';
    public static string POSITION_GEOGRAPHY_FUTURE_ACTIVE = 'Future Active';
    public static string POSITION_ACCOUNT_INACTIVE = 'Inactive';
    public static string CR_ACCOUNT_EXPLICIT_STATUS = 'Explicit';
    public static string CR_ACCOUNT_IMPLICIT_STATUS = 'Implicit';
    public static string CR_TYPE_CALL_PLAN = 'Call_Plan_Change';
    public static string CR_TYPE_EMP_ASSIGNMENT = 'Employee Assignment';
    public static string CR_TYPE_ROSTER = 'Roster';
    public static String AFF_RULE_BOTTOM_UP = 'Bottom Up';
    public static String AFF_RULE_TOP_DOWN = 'Top Down';
    public static String REQUEST_QUEUE_STATUS_NOT_STARTED = 'Not Started';
    public static String REQUEST_QUEUE_TYPE_POSITION = 'Position Level';

    public static string CURRENT_TEAM_CYCLE_TYPE  = 'Current';
    public static string FUTURE_TEAM_CYCLE_TYPE  = 'Future';
    public static string PAST_TEAM_CYCLE_TYPE  = 'Past';

    public static String RULE_EXECUTION_FAILED = 'Execution Failed';
    public static String RULE_PUBLISH_FAILED = 'Publish Failed';
    public static String ACF = 'Account_Compute_Final__c';

    //commented by Ritu and then added
    /*public static string HO_PROFILE = 'HO';
    public static string HR_PROFILE = 'HR';
    public static string DM_PROFILE = 'DM';
    public static string RM_PROFILE = 'RM';
    public static string REP_PROFILE = 'Rep';*/
    public static list<string> HO_PROFILE
    {
        get {return SalesIQProfile('HO');}
    }
    public static list<string> HR_PROFILE
    {
        get {return SalesIQProfile('HR');}
    }
    public static list<string> DM_PROFILE
    {
        get {return SalesIQProfile('DM');}
    }
    public static list<string> RM_PROFILE
    {
        get {return SalesIQProfile('RM');}
    }
    public static list<string> REP_PROFILE
    {
        get {return SalesIQProfile('Rep');}
    }

    public static string NO_APPROVAL_FOUND = '-100';
    public static string NO_APPROVAL_SETTING_FOUND = '-101';
    public static string NO_APPROVAL_CONFIG_FOUND = '-102';
    public static string STAFFED = 'Staffed';
    public static string IN_PROGRESS = 'In Progress';

    public static string TEAM_TYPE_GeoBased = 'GeoBased';
    public static string TEAM_TYPE_AccountBased = 'AccountBased';
    public static string TEAM_TYPE_Hybrid = 'Hybrid';

    public static string ERROR = 'Error';
    public static string WARNING = 'Warning';
    public static string NORMAL = 'Normal';

    public static string getOrgNameSpace()
    {
        return NAME_SPACE;
    }

    public static list<String> SalesIQProfile(string ProfileName)
    {

        //string SalesIQProfile='System Administrator';
        list<String> SalesIQProfile = new list<String> {'System Administrator'};
        String profiles = '';
        if(AxtriaSalesIQTM__TotalApproval__c.getValues(ProfileName) != null )
        {
            if(AxtriaSalesIQTM__TotalApproval__c.getValues(ProfileName).get('AxtriaSalesIQTM__Subscriber_Profile__c') != null && String.Valueof(AxtriaSalesIQTM__TotalApproval__c.getValues(ProfileName).get('AxtriaSalesIQTM__Subscriber_Profile__c')) != '')
            {
                profiles = String.Valueof(AxtriaSalesIQTM__TotalApproval__c.getValues(ProfileName).get('AxtriaSalesIQTM__Subscriber_Profile__c'));
            }
        }
        if(!string.isBlank(profiles))
        {
            SalesIQProfile.addAll(profiles.split(';'));
        }

        return SalesIQProfile;
    }


}