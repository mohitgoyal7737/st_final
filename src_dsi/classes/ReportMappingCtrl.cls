/**********************************************************************
* @author     : A1942
* @date       : June-10-2021
* @description: Controller class for Report_Mapping VF page
* Revison(s)  : v1.0
*************************************************************************/
public with sharing class ReportMappingCtrl {

	public String selectedReportType {get;set;}
	public List<SelectOption> reportTypes {get;set;}

	public String selectedReportInstance {get;set;}
	public List<SelectOption> reportInstances {get;set;}

	public String selectedComponentType {get;set;}
	public List<SelectOption> componentTypes {get;set;}

	public String colsMapping {get;set;}
	public String updateColumns {get;set;}

	public Boolean isSuccess {get;set;}

	public ReportMappingCtrl() {

		try {
			selectedReportType = '';
			reportTypes = new List<SelectOption>{(new SelectOption('None', 'None'))};

            selectedReportInstance = '';
            reportInstances = new List<SelectOption>{(new SelectOption('None', 'None'))};
           
            selectedComponentType = '';
            componentTypes = new List<SelectOption>{(new SelectOption('None', 'None'))};
			metricMappingList = new List<SIQIC__Report_Component_Metric_Mapping__c>();
            colsMapping = '';
            updateColumns = '';
            isSuccess = false;

            for(SIQIC__Report_Type_configuration__c obj:[Select SIQIC__Report_Type__c from SIQIC__Report_Type_configuration__c where enable_Component_Metric_Mapping__c = true WITH SECURITY_ENFORCED]){
            	if(String.isNotBlank(obj.SIQIC__Report_Type__c)){
            		reportTypes.add(new SelectOption(obj.SIQIC__Report_Type__c, obj.SIQIC__Report_Type__c));
            	}
            }
            System.debug('reportTypes :: '+reportTypes);

		}catch(Exception e){
			System.debug('Exception in ReportMappingCtrl()=> '+e.getMessage());
            System.debug('Stack trace => '+e.getStackTraceString());
		}
	}

	public void fetchReportInstance(){

		System.debug('selectedReportType :: '+selectedReportType);
		try{
			selectedReportInstance = '';
            reportInstances = new List<SelectOption>{(new SelectOption('None', 'None'))};
           
            selectedComponentType = '';
            componentTypes = new List<SelectOption>{(new SelectOption('None', 'None'))};
            colsMapping = '';
			metricMappingList = new List<SIQIC__Report_Component_Metric_Mapping__c>();

			if(String.isNotBlank(selectedReportType)){
				for(SIQIC__Report_Defination__c obj: [Select Id, Name From SIQIC__Report_Defination__c where SIQIC__ReportExecution__c = null and SIQIC__Type__c = :selectedReportType WITH SECURITY_ENFORCED]){
					if(String.isNotBlank(obj.Name)){
						reportInstances.add(new SelectOption(obj.Id, obj.Name));
					}
				}
				System.debug('reportInstances :: '+reportInstances);
			}
		}catch(Exception e){
			System.debug('Exception in ReportMappingCtrl.fetchReportInstance()=> '+e.getMessage());
            System.debug('Stack trace => '+e.getStackTraceString());
		}
	}

	public void fetchComponentType(){

		System.debug('selectedReportInstance :: '+selectedReportInstance);
		try{
			selectedComponentType = '';
            componentTypes = new List<SelectOption>{(new SelectOption('None', 'None'))};
            colsMapping = '';
			metricMappingList = new List<SIQIC__Report_Component_Metric_Mapping__c>();
			if(String.isNotBlank(selectedReportInstance)){
				for(SIQIC__Report_Component_Mapping__c obj: [Select SIQIC__Component_Name__c from SIQIC__Report_Component_Mapping__c where SIQIC__Report_Definition__c = :selectedReportInstance WITH SECURITY_ENFORCED order by SIQIC__SequenceNo__c]){
					if(String.isNotBlank(obj.SIQIC__Component_Name__c)){
						componentTypes.add(new SelectOption(obj.SIQIC__Component_Name__c, obj.SIQIC__Component_Name__c));
					}
				}
				System.debug('componentTypes :: '+componentTypes);
			}
		}catch(Exception e){
			System.debug('Exception in ReportMappingCtrl.fetchComponentType()=> '+e.getMessage());
            System.debug('Stack trace => '+e.getStackTraceString());
		}
	}

	List<SIQIC__Report_Component_Metric_Mapping__c> metricMappingList ;
	public void fetchMetricMapping(){

		System.debug('selectedComponentType :: '+selectedComponentType);
		try{
			colsMapping = '';
			metricMappingList = new List<SIQIC__Report_Component_Metric_Mapping__c>();

			if(String.isNotBlank(selectedComponentType)){
				metricMappingList = [Select SIQIC__Header_2__c, SIQIC__MappedColumn__c,SIQIC__Sequence__c from SIQIC__Report_Component_Metric_Mapping__c where SIQIC__Parent_Component_Type__c =:selectedComponentType and SIQIC__Report_Definition__c = :selectedReportInstance WITH SECURITY_ENFORCED order by SIQIC__Sequence__c];
				for(SIQIC__Report_Component_Metric_Mapping__c obj: metricMappingList){
					if(String.isNotBlank(obj.SIQIC__Header_2__c)){
						colsMapping += obj.SIQIC__Header_2__c + ';' ;
					}
				}
				colsMapping = colsMapping.removeEnd(';');
				if(test.isRunningTest())
				{
					updateColumns=colsMapping;
				}
				System.debug('colsMapping :: '+colsMapping);
			}
		}catch(Exception e){
			System.debug('Exception in ReportMappingCtrl.fetchMetricMapping()=> '+e.getMessage());
            System.debug('Stack trace => '+e.getStackTraceString());
		}
	}

	public void submitMapping(){

		System.debug('updateColumns :: '+updateColumns);
		isSuccess = false;
		try{
			if(String.isNotBlank(updateColumns)){
				List<String> columns = updateColumns.split(';');
				// Set<String> uniqueCols = new Set<String>();
				// uniqueCols.addAll(columns);
				for(SIQIC__Report_Component_Metric_Mapping__c obj:metricMappingList){
					obj.SIQIC__Header_2__c = columns[Integer.valueOf(obj.SIQIC__Sequence__c-1)];
				}
				System.debug('metricMappingList :: '+metricMappingList);
				update metricMappingList;
				isSuccess = true;
			}
		}catch(Exception e){
			System.debug('Exception in ReportMappingCtrl.submitMapping()=> '+e.getMessage());
            System.debug('Stack trace => '+e.getStackTraceString());
		}
	}
}