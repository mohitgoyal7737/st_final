@isTest
    public class PACPtoPAcopy_Test{
       static testMethod void testMethod1(){
       
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
        team.Name = 'HTN';
        team.CurrencyIsoCode = 'USD';
       // team.AxtriaSalesIQTM__Country__c = countr.id;
        insert team;
        
          AxtriaSalesIQTM__Team_Instance__c teamins = new AxtriaSalesIQTM__Team_Instance__c();
        teamins.Name = 'testteaminsname';
        teamIns.AxtriaSalesIQTM__Team__c = team.id;
        teamins.AxtriaSalesIQTM__IC_EffEndDate__c = date.today()+1;
        teamins.AxtriaSalesIQTM__IC_EffstartDate__c = date.today();
        insert teamins;
        
        Account acc = new Account();
        acc.Name = 'Chelsea Parson';
        acc.AxtriaSalesIQST__Profile_Consent__c= 'yes';
        acc.AxtriaSalesIQST__Primary_Speciality_Code__c = '123';
        acc.AxtriaSalesIQTM__Speciality__c = 'Cardiology';
        acc.AccountNumber = 'adf23';
        acc.AxtriaSalesIQTM__External_Account_Number__c = '123';
        acc.BillingStreet = 'abc';
        acc.AxtriaSalesIQST__Marketing_Code__c = 'thgh';
        insert acc; 
        
        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();
        pos.Name = 'Test';
        pos.AxtriaSalesIQTM__Client_Territory_Name__c = 'ALBANY, NY (N003)';
        pos.AxtriaSalesIQTM__Client_Position_Code__c = 'N003';
        pos.AxtriaSalesIQTM__Client_Territory_Code__c = 'N003';
        pos.AxtriaSalesIQTM__Position_Type__c = 'Area';
        pos.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
        pos.AxtriaSalesIQTM__Inactive__c = false;
        pos.AxtriaSalesIQTM__RGB__c = '41,210,117';
        pos.AxtriaSalesIQTM__Team_iD__c = team.Id;
        pos.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        pos.AxtriaSalesIQTM__X_Max__c = -72.6966429900;
        pos.AxtriaSalesIQTM__X_Min__c = -73.9625820000;
        pos.AxtriaSalesIQTM__Y_Max__c = 40.9666490000;
        pos.AxtriaSalesIQTM__Y_Min__c = 40.5821279800;
        pos.AxtriaSalesIQTM__Hierarchy_Level__c = '1';
        pos.AxtriaSalesIQTM__Level_5_Position__c ='pos.id';
        insert Pos;  
        
         AxtriaSalesIQTM__Position_Account__c posAccount = new AxtriaSalesIQTM__Position_Account__c ();
        posAccount.CurrencyIsoCode = 'USD';
        posAccount.AxtriaSalesIQTM__Account__c = acc.id;
        posAccount.AxtriaSalesIQTM__Position__c = pos.id;
        posAccount.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        posAccount.AxtriaSalesIQTM__Effective_Start_Date__c = date.Today();
        posAccount.AxtriaSalesIQTM__Effective_End_Date__c = date.Today() +1;  
        insert posAccount; 
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccount = new AxtriaSalesIQTM__Position_Account_Call_Plan__c();
        positionAccount.Name = 'Test';
        positionAccount.AxtriaSalesIQTM__Account__c = acc.id;
        positionAccount.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        positionAccount.AxtriaSalesIQST__Party_ID__c = posAccount.id;
        positionAccount.AxtriaSalesIQTM__Position__c = pos.id;
        insert positionAccount;
        
         AxtriaSalesIQTM__Change_Request__c objChangeRequest = new AxtriaSalesIQTM__Change_Request__c();
       //objChangeRequest.AxtriaSalesIQTM__Approver1__c = userId;
           
        objChangeRequest.AxtriaSalesIQTM__Change_String_Value__c = '11788';
        objChangeRequest.AxtriaSalesIQTM__Status__c = 'status';
       // objChangeRequest.OwnerID = userId;
        objChangeRequest.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.id;
        objChangeRequest.AxtriaSalesIQTM__scenario_name__c = 'Scenario';
        objChangeRequest.AxtriaSalesIQTM__Execution_Date__c = date.today();
        insert objChangeRequest;
        
         AxtriaSalesIQTM__Change_Request__c objChangeRequest1 = new AxtriaSalesIQTM__Change_Request__c();
       //objChangeRequest.AxtriaSalesIQTM__Approver1__c = userId;
           
        objChangeRequest1.AxtriaSalesIQTM__Change_String_Value__c = '11788';
        objChangeRequest1.AxtriaSalesIQTM__Status__c = 'status';
       // objChangeRequest.OwnerID = userId;
        objChangeRequest1.AxtriaSalesIQTM__Team_Instance_ID__c = teamins.id;
        objChangeRequest1.AxtriaSalesIQTM__scenario_name__c = 'Scenario';
        objChangeRequest1.AxtriaSalesIQTM__Execution_Date__c = date.today();
        insert objChangeRequest1;
        
        AxtriaSalesIQTM__CR_Call_Plan__c CRcall = new AxtriaSalesIQTM__CR_Call_Plan__c();
        CRcall.AxtriaSalesIQTM__Account__c = acc.id;
        CRcall.AxtriaSalesIQTM__Change_Request_ID__c = objChangeRequest.id;
        insert CRcall;
        CRcall.AxtriaSalesIQTM__Change_Request_ID__c = objChangeRequest.id;
        update CRcall; 
        
        AxtriaSalesIQTM__TriggerContol__c ccd= new AxtriaSalesIQTM__TriggerContol__c();
        ccd.Name = 'CallPlanCRIdChange';
        ccd.AxtriaSalesIQTM__IsStopTrigger__c = false;
        insert ccd;       
              
        List<AxtriaSalesIQTM__Team_Instance__c > Teami = new List<AxtriaSalesIQTM__Team_Instance__c >(); 
        List<string> teamInsIds = new List<String>();
        teamInsIds.add(teamins.id);
        PACPtoPAcopy PACPtoPA = new PACPtoPAcopy(teamInsIds);
          Database.executeBatch(PACPtoPA);   
        }
}