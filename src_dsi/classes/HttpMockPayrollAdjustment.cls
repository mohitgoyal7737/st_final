global with sharing class HttpMockPayrollAdjustment implements HTTPCalloutMock{
    global HTTPResponse respond(HTTPRequest req){
        HttpResponse res = new HTTPResponse();
        res.setHeader('Content-Type', 'application/JSON');
        res.setBody('Success');
        res.setStatusCode(200);
        return res;
    }
}