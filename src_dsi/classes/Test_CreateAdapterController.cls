@isTest
public class Test_CreateAdapterController {

    static testMethod void testMethod1() {
        
        Account acc = new account(name = 'Tenant1',SIQIC__Abbreviated_Name__c='Tenant1',AxtriaSalesIQST__Marketing_Code__c='US');
        insert acc;
            
        SIQIC__Country__c cau= new SIQIC__Country__c(name='Tenant1',SIQIC__tenant__c=acc.id);
        insert cau;
        
        Profile profile =[select id from profile where name='System Administrator'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',SIQIC__tenant__c ='Tenant1', 
                          LocaleSidKey='en_US',SIQIC__Country__c = 'Tenant1',SIQIC__Geo_ID__c='none', 
                          TimeZoneSidKey='America/Los_Angeles',ProfileId = Profile.ID, UserName='standarduser121211@testorg.com',SIQIC__TenantId__c=acc.id);
        insert u;
        
        SIQIC__Template__c t= new SIQIC__Template__c(SIQIC__Data_Adapter_ID__c='5',SIQIC__Tenant__c=acc.id);
        insert t;
           
        System.runas(u){
            //Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
            System.test.startTest();
                

                CreateAdapterController obj = new CreateAdapterController();
                
                obj.adapterColumns='COUNTRY_CODE Text;EMPLOYEE_ID Text;FIRST_NAME Text;MIDDLE_NAME Text;LAST_NAME Text;EMAIL Text;MUD_ID Text;STATUS Text;HIRE_DATE Date;ORIGINAL_HIRE_DATE Date;TERMINATION_DATE Date;TERMINATION_REASON Text;LANGUAGE Text;RETIREMENT_ELIGIBLE_DATE Date;EMPLOYEE_TYPE Text';
                obj.adapterName='demo';
                obj.adapterDatatype='Eligibility';
                obj.mandatoryColumns='COUNTRY_CODE;EMPLOYEE_ID;FIRST_NAME;LAST_NAME;EMAIL;MUD_ID;HIRE_DATE;ORIGINAL_HIRE_DATE;LANGUAGE';
                obj.ValidateAction();
                obj.Validate();
                
                obj.DeleteAdaptorfromSFDC();
                //obj.callCreateAdapterProc();
                //obj.Upload_Columns_sfdc();
                obj.saveTextValues();
                
            Test.stopTest();
                
        }
                
    }  

}