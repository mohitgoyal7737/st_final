public class PushPayrollDataCtrl {
 	public SIQIC__Publish__c publishRec ;
 	
 	public PushPayrollDataCtrl(ApexPages.StandardController controller) {
           this.publishRec = (SIQIC__Publish__c)controller.getRecord();
    } 

	public PageReference saveOperation(){

		//// GET THE TEMP DATA FROM THE PAYROLL REPORT CONFIGURATION AND INSERT THE SAME WITH THE PUBLISH RECORD ID LOOKUP
		List<Payroll_Report_Configuration__c> listpayrollConfigInsert = new List<Payroll_Report_Configuration__c> ();
		List<Payroll_Report_Configuration__c> listToDelete = new List<Payroll_Report_Configuration__c> ();
		SIQIC__Publish__c publishData = new SIQIC__Publish__c();

		publishData = [Select SIQIC__Business_Unit__c,SIQIC__Tenant__c,SIQIC__Report_Data_Push__c from SIQIC__Publish__c WHERE ID=: publishRec.Id];

		listpayrollConfigInsert = [Select Business_Unit__c,Field_Display_Name__c,Field_API_Name__c,Display_Data_Type__c,Display_Column_Order__c,Column_Formula__c,Database_Column__c,
								Database_Table__c,Object_Name__c,Filter_Type__c,IsVisible__c,IsEditable__c,IsEnabled__c,Sortable__c,Tab_Name__c,	Tab_Order__c,Tenant__c,Type__c,Role__c FROM 
								Payroll_Report_Configuration__c WHERE Business_Unit__c =: publishData.SIQIC__Business_Unit__c AND Name like '%Temp%'];

		listToDelete = [Select Id from Payroll_Report_Configuration__c WHERE Publish__c=:publishRec.Id];

		for(Payroll_Report_Configuration__c config : listpayrollConfigInsert){
			config.Id = null;
			config.Publish__c = publishRec.Id;
		}

		if(!listToDelete.isEmpty()){
			Database.delete(listToDelete);
		}

		if(!listpayrollConfigInsert.isEmpty()){
			Database.insert(listpayrollConfigInsert);
		}

		callWebService(publishData.SIQIC__Tenant__c,publishRec.Id,publishData.SIQIC__Report_Data_Push__c);

		PageReference pRef = new PageReference('/'+publishRec.Id);
		pRef.setRedirect(true);
		return pRef;
	}    

    public PageReference cancelOperation(){
    	System.debug('---publishId--'+publishRec);
    	PageReference pRef = new PageReference('/'+publishRec.Id);
		pRef.setRedirect(true);
		return pRef;
    }

    @future(callout=true)
 	public static void callWebService(String tenantName,String publishId,String datapushId){
 		List<Account> tenantData = new List<Account> ();

 		tenantData = [Select Id,SIQIC__SCWebService_Server__c FROM Account WHERE Name =: tenantName];

 		String responseVal = '';
 		if(!tenantData.isEmpty()){
 			String endPoint = tenantData[0].SIQIC__SCWebService_Server__c + 'DSI_GetSQLData/sql_to_sfdc?tenant='+tenantName+'&publishId='+publishId+'&reportDataPush='+datapushId;
	 		Http http = new Http();
	 		HttpRequest req = new HttpRequest();
	 		req.setMethod('GET');
	 		req.setEndpoint(endPoint);
	 		req.setTimeout(120000);
            HttpResponse response = http.send(req);
            if(response.getStatusCode() == 200){
            	if(response.getBody() == 'Success'){
            		responseVal = 'Success.';
            	}
            	else{
            		responseVal = 'Error in Webservice Callout.';
            	}
            }
 		}

 	}



}