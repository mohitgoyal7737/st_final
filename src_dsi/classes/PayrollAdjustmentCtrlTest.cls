@isTest
private class PayrollAdjustmentCtrlTest {
    static testMethod void testMethod1() {

    	List<Payroll_Report_Configuration__c> configList = new List<Payroll_Report_Configuration__c>();
        Map<String,String> mapFieldNametoType = new Map<String,String>();
        String objectAPI = 'Payroll_Report__c';

    	/// CREATE R CALENDER 
    	SIQIC__R_Calender__c calender = new SIQIC__R_Calender__c();
        calender.name = 'Test calender';
        calender.SIQIC__Country_Code__c = 'US';
        calender.SIQIC__IsActive__c = true;
   		insert calender;                
        

     	/// CREATE TENANT DATA
     	Account  ten = new Account ();
        ten.name = 'Test Tenant';
        ten.SIQIC__Default_CalenderId__c = calender.Id;
        ten.SIQIC__bucketURL__c='testbucket';
        ten.SIQIC__payroll_dept_email__c='compmax@axtria.com';
        ten.AxtriaSalesIQST__Marketing_Code__c = 'MA';
       	insert ten;

        User loggedInUser = [select id,userrole.name from User where id=:UserInfo.getUserId()];
        loggedInUser.SIQIC__Tenant__c = ten.Name;
        loggedInUser.SIQIC__TenantID__c = ten.id;
        update loggedInUser; 

       	/// CREATE COUNTRY DATA
       	SIQIC__Country__c con = new SIQIC__Country__c();
        con.name = 'US';
        con.SIQIC__Tenant__c = ten.ID;
        insert con;


       	/// CRAETE REPORT DATA PUSH RECORD REPORT DEFINATION__C

       	SIQIC__Report_Defination__c repDef = new SIQIC__Report_Defination__c();
        repDef.SIQIC__Business_Unit__c = 'All';
        repDef.SIQIC__Frequency__c = 'Quaterly';
        repDef.SIQIC__Report_Name__c= 'report1';
        repDef.SIQIC__Period__c = System.today();
        repDef.SIQIC__Tenant__c = ten.Id;
        insert repDef;
        
        /// CREATE REPORT TYPE

        SIQIC__Report_Type__c repType = new SIQIC__Report_Type__c();
        repType.Name = 'Payroll Adjusment';
        repType.SIQIC__Frequency__c = 'Quaterly';
        repType.SIQIC__Tenant__c = ten.Id;
        insert repType;

           	
        /// CREATE PUBLISH RECORD

     	SIQIC__Publish__c pub = new SIQIC__Publish__c();
     	pub.SIQIC__ReportType__c = repType.Id;
        pub.SIQIC__Business_Unit__c = 'All';
        pub.SIQIC__Publish_Date__c = System.today();
        pub.SIQIC__ReportName__c = 'report1';
        pub.SIQIC__Teams__c = 'HO';
        pub.SIQIC__Visible_to_Field__c = false;
        pub.SIQIC__Visible_to_HO__c = true;
        pub.SIQIC__Report_Data_Push__c = repDef.Id;
        pub.Payroll_Start_Date__c = System.today().addDays(-10);
        pub.Payroll_End_Date__c = System.today().addDays(10);
        insert pub;


     	// CREATE REPORT RECORD

     	SIQIC__Reports__c report = new SIQIC__Reports__c();
     	report.SIQIC__Publish__c = pub.id;
     	report.Name =  'report1';
        report.SIQIC__Report_Type__c = repType.Id;
        report.SIQIC__Business_Unit__c = 'All';
        report.SIQIC__Latest__c = true;
        report.SIQIC__LatestHO__c = true;
        report.SIQIC__Visible_to_field__c = false;
        report.SIQIC__Visible_To_HO__c = true;
        report.SIQIC__Report_Definition__c = repDef.Id;
        report.SIQIC__Tenant__c = ten.Name;
        insert report;

         /// CREATE PAYROLL CONFIGURATION DATA

        Schema.sObjectType objType = Schema.getGlobalDescribe().get(objectAPI);
        Schema.DescribeSObjectResult r1 = objType.getDescribe();  
        Map<String,Schema.SObjectField > mapFieldList = r1.fields.getMap(); 

        for (Schema.SObjectField field : mapFieldList.values()){ 
            Schema.DescribeFieldResult fieldResult = field.getDescribe();  
            if(fieldResult.isUpdateable()){
                String dataType = '';
                if(String.valueOf(fieldResult.getType()) == 'STRING'){
                   dataType = 'Text';
                }
                else if(String.valueOf(fieldResult.getType()) == 'DOUBLE'){
                    dataType = 'Number';
                }
                else if(String.valueOf(fieldResult.getType()) == 'BOOLEAN'){
                    dataType = 'Boolean';
                }
                if(dataType != ''){
                    mapFieldNametoType.put(String.valueOf(fieldResult.getName()),String.valueOf(fieldResult.getType()));
                }
            } 
        }     

        System.debug('---mapFieldNametoType---'+mapFieldNametoType);


        for(String key : mapFieldNametoType.keySet()){

            Payroll_Report_Configuration__c  config = new Payroll_Report_Configuration__c();
            config.Name = 'Temp Data';
            config.Business_Unit__c  = 'All'; 
            config.Display_Column_Order__c  = 1;
            config.Display_Data_Type__c  = mapFieldNametoType.get(key);
            config.Field_API_Name__c  = key;
            config.Field_Display_Name__c  = 'Team';
            config.IsEditable__c  = true;
            config.IsEnabled__c  = true;
            config.IsVisible__c  = true;
            config.Object_Name__c  = objectAPI;
            //config.Publish__c  = pub.Id;
            config.Role__c  = 'HO';
            config.Tab_Name__c = 'Detailed Payroll Report';
            config.Tab_Order__c = '1';
            config.Tenant__c = ten.Id;
            config.Type__c = 'Payroll Adjustment';
            configList.add(config);

            Payroll_Report_Configuration__c  config2 = new Payroll_Report_Configuration__c();
            config2.Name = 'Temp Data';
            config2.Business_Unit__c  = 'All'; 
            config2.Display_Column_Order__c  = 1;
            config2.Display_Data_Type__c  = mapFieldNametoType.get(key);
            config2.Field_API_Name__c  = key;
            config2.Field_Display_Name__c  = 'Team';
            config2.IsEditable__c  = false;
            config2.IsEnabled__c  = true;
            config2.IsVisible__c  = true;
            config2.Object_Name__c  = objectAPI;
            //config2.Publish__c  = pub.Id;
            config2.Role__c  = 'HO';
            config2.Tab_Name__c = 'Payroll Report';
            config2.Tab_Order__c = '1';
            config2.Tenant__c = ten.Id;
            config2.Type__c = 'Payroll Adjustment';
            configList.add(config2);

        }

        if(!configList.isEmpty()){
            insert configList;
        }


        List<sObject> objDataToInsert = new List<sObject> ();

        //// INSERT DATA IN THE OBJECT 
        for(Integer i=0;i<=5;i++){
            sObject sObj = Schema.getGlobalDescribe().get(objectAPI).newSObject();
            for(String key : mapFieldNametoType.keySet()){
                if(mapFieldNametoType.get(key) == 'Text'){
                    sObj.put(key,'Test'+i);
                }
                else if(mapFieldNametoType.get(key) == 'Number'){
                    sObj.put(key,i);
                }
                else if(mapFieldNametoType.get(key) == 'Boolean'){
                    sObj.put(key,true);
                }
            }
            sObj.put('Report_Data_Push__c',repDef.Id);
            objDataToInsert.add(sObj);

        }

        if(!objDataToInsert.isEmpty()){
            insert objDataToInsert;
        }


        System.runAs(loggedInUser){

                Test.setMock(HttpCalloutMock.class, new HttpMockPayrollAdjustment());
                ApexPages.StandardController sc = new ApexPages.StandardController(pub);
                PushPayrollDataCtrl copyConfig  = new PushPayrollDataCtrl(sc);
                copyConfig.saveOperation();
                copyConfig.cancelOperation();
                List<String> editableColumns = new List<String> ();
                editableColumns.addAll(mapFieldNametoType.keySet());

                Test.setCurrentPageReference(new PageReference('Page.PayrollAdjustment')); 
                System.currentPageReference().getParameters().put('id', report.Id);
                PayrollAdjustmentCtrl ctrl = new PayrollAdjustmentCtrl();
                PayrollAdjustmentCtrl.saveSubmitChanges(JSON.serialize(objDataToInsert),objectAPI,editableColumns,'Save',report.Id);
                PayrollAdjustmentCtrl.saveSubmitChanges(JSON.serialize(objDataToInsert),objectAPI,editableColumns,'Submit',report.Id);
                ctrl.redirectListView();

            
        }       

        



    }
}