@isTest
private class GeographyUtilSnT_Test {
    static testMethod void testMethod1() {
        //create Team
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
        team.Name = 'Specialty';
        insert team;
        
        //create Positions
        list<AxtriaSalesIQTM__Position__c> posList = new list<AxtriaSalesIQTM__Position__c> ();
        AxtriaSalesIQTM__Position__c posNation = new AxtriaSalesIQTM__Position__c();
        posNation.AxtriaSalesIQTM__Position_Type__c = 'Nation';
        posNation.Name = 'Chico CA_SPEC';
        posNation.AxtriaSalesIQTM__Client_Position_Code__c='1111' ;
        posNation.AxtriaSalesIQTM__Client_Territory_Code__c='1111' ;
        posNation.AxtriaSalesIQTM__Hierarchy_Level__c= '4';
        posNation.AxtriaSalesIQTM__RGB__c='41,210,117';
        posNation.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
        posNation.AxtriaSalesIQTM__Team_iD__c    = team.id;
        posList.add(posNation);

         AxtriaSalesIQTM__Position__c pos2 = new AxtriaSalesIQTM__Position__c();
        pos2.AxtriaSalesIQTM__Position_Type__c = 'Region';
        pos2.Name = 'Chicago';
        pos2.AxtriaSalesIQTM__Client_Position_Code__c='2222' ;
        pos2.AxtriaSalesIQTM__Client_Territory_Code__c='2222' ;
        pos2.AxtriaSalesIQTM__Hierarchy_Level__c= '3';
        pos2.AxtriaSalesIQTM__RGB__c='42,210,117';
        pos2.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
        pos2.AxtriaSalesIQTM__Team_iD__c = team.id;
        pos2.AxtriaSalesIQTM__Parent_Position__c = posNation.id;
        posList.add(pos2);
        
        AxtriaSalesIQTM__Position__c posdm = new AxtriaSalesIQTM__Position__c();
        posdm.AxtriaSalesIQTM__Position_Type__c = 'District';
        posdm.Name = 'Chico CA_SPEC';
        posdm.AxtriaSalesIQTM__Client_Position_Code__c='3333' ;
        posdm.AxtriaSalesIQTM__Client_Territory_Code__c='3333' ;
        posdm.AxtriaSalesIQTM__Hierarchy_Level__c= '2';
        posdm.AxtriaSalesIQTM__RGB__c='43,210,117';
        posdm.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
        posdm.AxtriaSalesIQTM__Team_iD__c    = team.id;
        posdm.AxtriaSalesIQTM__Parent_Position__c = pos2.id;
        posList.add(posdm);
        
        AxtriaSalesIQTM__Position__c pos1 = new AxtriaSalesIQTM__Position__c();
        pos1.AxtriaSalesIQTM__Position_Type__c = 'Territory';
        pos1.Name = 'Chico CA_SPEC';
        pos1.AxtriaSalesIQTM__Client_Position_Code__c='4444' ;
        pos1.AxtriaSalesIQTM__Client_Territory_Code__c='4444' ;
        pos1.AxtriaSalesIQTM__Hierarchy_Level__c= '1';
        pos1.AxtriaSalesIQTM__RGB__c='44,210,117';
        pos1.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
        pos1.AxtriaSalesIQTM__Team_iD__c  = team.id;
        pos1.AxtriaSalesIQTM__Parent_Position__c = posdm.id;
        pos1.AXTRIASALESIQTM__HIERARCHY_LEVEL__C= '1';
        posList.add(pos1);

        AxtriaSalesIQTM__Position__c pos3 = new AxtriaSalesIQTM__Position__c();
        pos3.AxtriaSalesIQTM__Position_Type__c = 'Territory';
        pos3.Name = 'Chico CA_SPEC';
        pos3.AxtriaSalesIQTM__Client_Position_Code__c='5555' ;
        pos3.AxtriaSalesIQTM__Client_Territory_Code__c='5555' ;
        pos3.AxtriaSalesIQTM__Hierarchy_Level__c= '1';
        pos3.AxtriaSalesIQTM__RGB__c='45,210,117';
        pos3.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
        pos3.AxtriaSalesIQTM__Team_iD__c  = team.id;
        pos3.AxtriaSalesIQTM__Parent_Position__c = posdm.id;
        pos3.AXTRIASALESIQTM__HIERARCHY_LEVEL__C= '1';
        posList.add(pos3);

        insert posList;

        map<Id,AxtriaSalesIQTM__Position__c>allPositionMap = new map<Id,AxtriaSalesIQTM__Position__c>();
        map<Id,list<AxtriaSalesIQTM__Position__c>> parentChildPositionMap = new map<Id,list<AxtriaSalesIQTM__Position__c>>();


        for(AxtriaSalesIQTM__Position__c pos  : posList){
            allPositionMap.put(pos.id,pos);
        }
        
        parentChildPositionMap.put(posdm.id, new list<AxtriaSalesIQTM__Position__c>{pos1,pos3});

        

        GeographyUtilSnT.createChildNode(pos1.id,allPositionMap,parentChildPositionMap);
    }
}