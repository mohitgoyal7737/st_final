@isTest
public class CallPlanReportCtrlTest {
    
    static testMethod void testMethod1(){
        
        Date startDate = Date.today();
        Date endDate = Date.today().addDays(20);
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactory.createWorkspace('TestWorkspace', startDate, endDate);
        workspace.Enable_Call_Plan_Report__c = true;
        insert workspace;
        
        AxtriaSalesIQTM__Organization_Master__c orgMaster = TestDataFactory.createOrganizationMaster();
        insert orgMaster;
        
        AxtriaSalesIQTM__Country__c country = TestDataFactory.createCountry(orgMaster);
        insert country;
        
        AxtriaSalesIQTM__Team__c team = TestDataFactory.createTeam(country);
        insert team;
        
        String alignmentType = '', scenarioId = '';
        AxtriaSalesIQTM__Team_Instance__c teamInstance = TestDataFactory.createTeamInstance(team.Id, alignmentType, scenarioId, startDate, endDate);
        teamInstance.Enable_Call_Plan_Report__c = true;
        insert teamInstance;
        
        AxtriaSalesIQTM__Scenario__c scenario = TestDataFactory.newcreateScenario(teamInstance, team, workspace);
        insert scenario;
        
        teamInstance.AxtriaSalesIQTM__Scenario__c = scenario.Id;
        update teamInstance;
        
        AxtriaSalesIQTM__Position__c position = TestDataFactory.createPosition(team, teamInstance);
        insert position;
        
        Profile userProfile =[Select Id from Profile where Name='HO' Limit 1];
        User testUser = TestDataFactory.createUser(userProfile.Id);
        insert testUser;
        
        AxtriaSalesIQTM__User_Access_Permission__c userPosition = TestDataFactory.createUserAccessPerm(position, teamInstance, UserInfo.getUserId());
        userPosition.Enable_Call_Plan_Report__c = true;
        insert userPosition;
        
        Account acc = TestDataFactory.createAccount();
        acc.AxtriaSalesIQST__Marketing_Code__c = 'test';
        acc.SIQIC__bucket__c = 'dsi-salesiq';
        acc.S3Host__c = 'dsi-salesiq.s3.us-east-1.amazonaws.com';
        acc.SIQIC__accessKey__c = 'AKIASGKTYIXOPQBLKVIC';
        acc.SIQIC__secretKey__c = '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';
        insert acc;
        
        User usr = [Select Id, SIQIC__TenantID__c from User Where Id=:UserInfo.getUserId() limit 1];
        usr.SIQIC__TenantID__c = acc.Id;
        update usr;
        
        try{
        CallPlanReportCtrl obj = new CallPlanReportCtrl();
        //obj.selectedWorkspace = '';
        obj.teamInstanceChanged();
        obj.positionChange();
        }catch(Exception e){}
    }
}