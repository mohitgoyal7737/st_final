@isTest
private class SalesIQSnTLogger_Test {
    static testMethod void testMethod1() {
    
        try{
             Account acc = new Account();
             acc.Name = 'Chelsea Parson';
             acc.AxtriaSalesIQTM__External_Account_Number__c='123';
             acc.BillingStreet='abc';
             acc.AccountNumber ='BH123456';
             //acc.AxtriaSalesIQST__Marketing_Code__c='ES';
             acc.Type = 'HCO';
            insert acc;
        }
        catch(Exception ex){
            SalesIQSnTLogger.createHandledErrorLogs(ex,'test module','test business rule');
            SalesIQSnTLogger.createHandledErrorLogsforCPF(ex,'test module','test Team Ins');
            SalesIQSnTLogger.createUnHandledErrorLogsforCPF(ex,'test module','test class','test Team Ins');
            SalesIQSnTLogger.createUnHandledErrorLogs(ex,'test module','test class','test business rule');
            SalesIQSnTLogger.createHandledErrorLogsforMatrix(ex,'test module','test business rule');
            SalesIQSnTLogger.createUnHandledErrorLogsforMatrix(ex,'test module','test class','test business rule');
            SalesIQSnTLogger.createUnHandledErrorLogsforProfilingData(ex,'test module','test business rule');  

        }
        
        
        Test.startTest();
        Test.stopTest();
        
    }
}