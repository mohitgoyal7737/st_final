@isTest
private class SnTDMLSecurityUtil_Test {
    static testMethod void testMethod1() {
        //SnTDMLSecurityUtil obj = new SnTDMLSecurityUtil();
        SnTDMLSecurityUtil.printDebugMessage('abc Class','test msg',true);
        SnTDMLSecurityUtil.printDebugMessage('test msg',true);
        SnTDMLSecurityUtil.printDebugMessage('abc Class','test method','test msg');

        list<AxtriaSalesIQTM__Position_Account__c>insertposaccount = new list<AxtriaSalesIQTM__Position_Account__c>();
        
        Account acc = new Account();
        acc.Name = 'Chelsea Parson';
        acc.AxtriaSalesIQTM__External_Account_Number__c='123';
        acc.BillingStreet='abc';
        acc.AccountNumber ='BH123456';
        acc.AxtriaSalesIQST__Marketing_Code__c='ES';
        acc.Type = 'HCO';
        insert acc;
        
        AxtriaSalesIQTM__Position_Account__c newobj1 = new AxtriaSalesIQTM__Position_Account__c();
        //newobj1.AxtriaSalesIQTM__Position__c = selectedPosition;
        newobj1.AxtriaSalesIQTM__Account__c = acc.id;
        newobj1.AxtriaSalesIQTM__Account_Alignment_Type__c = 'Explicit';
        newobj1.AxtriaSalesIQST__Batch_Name__c = 'Add HCP to Call Plan';
        newobj1.AxtriaSalesIQTM__TempDestinationrgb__c = 'Call Plan';
        newobj1.AxtriaSalesIQTM__Segment_1__c   = 'Primary';
       

        SnTDMLSecurityUtil.insertRecords(newobj1,'NewPhysicianUniverseCtrl');

        AxtriaSalesIQTM__Position_Account__c newobj2 = new AxtriaSalesIQTM__Position_Account__c();
        //newobj2.AxtriaSalesIQTM__Position__c = selectedPosition;

        newobj2.AxtriaSalesIQTM__Account__c = acc.id;
        newobj2.AxtriaSalesIQTM__Account_Alignment_Type__c = 'Explicit';
        newobj2.AxtriaSalesIQST__Batch_Name__c = 'Add HCP to Call Plan';
        newobj2.AxtriaSalesIQTM__TempDestinationrgb__c = 'Call Plan';
        newobj2.AxtriaSalesIQTM__Segment_1__c   = 'Primary';
        insert newobj2;
        insertposaccount.add(newobj2);

        SnTDMLSecurityUtil.upsertRecords(insertposaccount,'NewPhysicianUniverseCtrl');
        SnTDMLSecurityUtil.upsertRecords(newobj2,'NewPhysicianUniverseCtrl');
        
        SnTDMLSecurityUtil.queryRecords(insertposaccount,'NewPhysicianUniverseCtrl');
        SnTDMLSecurityUtil.queryRecords(newobj2,'NewPhysicianUniverseCtrl');
        
        SObjectAccessDecision securityDecision1 = Security.stripInaccessible(AccessType.CREATABLE, insertposaccount);
        SnTDMLSecurityUtil.printDMLMessage(securityDecision1 ,'','');
        SnTException obj = new SnTException(securityDecision1, '',SnTException.OperationType.C);
        SnTException obj2 = new SnTException(securityDecision1, '',SnTException.OperationType.R);
        SnTException obj3 = new SnTException(securityDecision1, '',SnTException.OperationType.U);
        SnTException obj4 = new SnTException(securityDecision1, '',SnTException.OperationType.D);
        
        SnTDMLSecurityUtil.updateRecords(insertposaccount,'NewPhysicianUniverseCtrl');
        SnTDMLSecurityUtil.updateRecords(newobj2,'NewPhysicianUniverseCtrl');
        
        SnTDMLSecurityUtil.deleteRecords(insertposaccount,'NewPhysicianUniverseCtrl');
        //SnTDMLSecurityUtil.deleteRecords(newobj2,'NewPhysicianUniverseCtrl');
        
        
 		
        AxtriaSalesIQTM__Position_Account__c newobj3 = new AxtriaSalesIQTM__Position_Account__c();
        //newobj2.AxtriaSalesIQTM__Position__c = selectedPosition;

        newobj3.AxtriaSalesIQTM__Account__c = acc.id;
        newobj3.AxtriaSalesIQTM__Account_Alignment_Type__c = 'Explicit';
        newobj3.AxtriaSalesIQST__Batch_Name__c = 'Add HCP to Call Plan';
        newobj3.AxtriaSalesIQTM__TempDestinationrgb__c = 'Call Plan';
        newobj3.AxtriaSalesIQTM__Segment_1__c   = 'Primary';
        //SnTDMLSecurityUtil.deleteRecords(newobj3,'NewPhysicianUniverseCtrl');
        
        Profile pid=[Select Id from Profile where Name like '%HO%' or Name like '%Home Office%' Limit 1];       
        User u1=new User(Username='ho@ho.com', LastName='ho', Email='ho@axtria.com', Alias='ho', CommunityNickname='ho',
                    TimeZoneSidKey='America/Los_Angeles', LocaleSidKey='en_US',EmailEncodingKey='UTF-8',
                    ProfileID=pid.id, LanguageLocaleKey='en_US',
                    Title = 'HO');
        insert u1;
        
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
        team.Name = 'Specialty';
        insert team;
        
        list<AxtriaSalesIQTM__Position__c> posList = new list<AxtriaSalesIQTM__Position__c>();
        AxtriaSalesIQTM__Position__c posNation = new AxtriaSalesIQTM__Position__c();
        posNation.AxtriaSalesIQTM__Position_Type__c = 'Nation';
        posNation.Name = 'Chico CA_SPEC';
        posNation.AxtriaSalesIQTM__Client_Position_Code__c='1111' ;
        posNation.AxtriaSalesIQTM__Client_Territory_Code__c='1111' ;
        posNation.AxtriaSalesIQTM__Hierarchy_Level__c= '4';
        posNation.AxtriaSalesIQTM__RGB__c='41,210,117';
        posNation.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
        posNation.AxtriaSalesIQTM__Team_iD__c    = team.id;
        insert posNation;
        posList = [select id,AxtriaSalesIQTM__Client_Position_Code__c  from AxtriaSalesIQTM__Position__c];
        //posList.add(posNation);
        
        //list<AxtriaSalesIQTM__Position__c> posList = [SELECT Name,AxtriaSalesIQTM__client_position_code__c FROM AxtriaSalesIQTM__Position__c] ;
        //AxtriaSalesIQTM__Position_Account__c posAcc = posAccList[0];
         System.runAs(u1){
             try{
                 SObjectAccessDecision securityDecision = Security.stripInaccessible(AccessType.CREATABLE, posList);
                 System.debug('securityDecision1 '+securityDecision);
                 securityDecision = Security.stripInaccessible(AccessType.UPSERTABLE, posList);
                 System.debug('securityDecision2 '+securityDecision);
                 securityDecision = Security.stripInaccessible(AccessType.READABLE, posList);
                 System.debug('securityDecision3 '+securityDecision);
                 securityDecision = Security.stripInaccessible(AccessType.UPDATABLE, posList);
                 System.debug('securityDecision4 '+securityDecision);
                 
                 SnTDMLSecurityUtil.upsertRecords(posList   ,'NewPhysicianUniverseCtrl');
                 SnTDMLSecurityUtil.updateRecords(posList   ,'NewPhysicianUniverseCtrl');
             }catch(Exception e){
                 
             }
             
             //upsertRecords
             //SnTDMLSecurityUtil.upsertRecords(posAcc,'NewPhysicianUniverseCtrl');
         }
        
        
        
        Test.startTest();
        Test.stopTest();
        
        
        
        
    }
}