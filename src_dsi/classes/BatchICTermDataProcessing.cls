global class BatchICTermDataProcessing implements Database.Batchable<sObject> {
    public String query;

    global BatchICTermDataProcessing() {
        delete[select id from Term_List__c];
        query = 'Select id, CreatedDate, AxtriaSalesIQST__Employee__r.Termination_Date__c, AxtriaSalesIQST__Employee__r.Reason_for_Action_Name__c, Position_ID__c, AxtriaSalesIQST__Employee__r.Personnel_Subarea_Code__c, AxtriaSalesIQST__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQST__Employee__r.Personnel_Subarea_Name__c,AxtriaSalesIQST__Employee__r.Personnel_Area__c,AxtriaSalesIQST__Employee__r.Personnel_Number__c,AxtriaSalesIQST__Employee__r.Personnel_Area_Number__c,AxtriaSalesIQST__Employee__r.AxtriaSalesIQTM__Last_Name__c,AxtriaSalesIQST__Employee__r.AxtriaSalesIQTM__FirstName__c,AxtriaSalesIQST__Employee__r.Reason_Code__c,AxtriaSalesIQST__Employee__r.AxtriaSalesIQTM__Employee_ID__c from AxtriaSalesIQST__CR_Employee_Feed__c where AxtriaSalesIQST__Event_Name__c ='+'\'Terminate Employee\'';
        this.query = query;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        System.debug('@@@ Start method of BatchICTermDataProcessing invoked');
        System.debug('@@@ query = '+query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQST__CR_Employee_Feed__c> scopeCRList) {
        List<Term_List__c> TermInsertionList = new List<Term_List__c>();

        for(AxtriaSalesIQST__CR_Employee_Feed__c CR_Employee_Feed :  scopeCRList){
            Term_List__c term = new Term_List__c();
            term.personnel_area__c = CR_Employee_Feed.AxtriaSalesIQST__Employee__r.Personnel_Area__c;
            term.last_name__c = CR_Employee_Feed.AxtriaSalesIQST__Employee__r.AxtriaSalesIQTM__Last_Name__c;
            term.first_name__c = CR_Employee_Feed.AxtriaSalesIQST__Employee__r.AxtriaSalesIQTM__FirstName__c;
            term.action_type__c = CR_Employee_Feed.AxtriaSalesIQST__Employee__r.Reason_for_Action_Name__c;
            term.employee_id__c = CR_Employee_Feed.AxtriaSalesIQST__Employee__r.AxtriaSalesIQTM__Employee_ID__c;
            //DateTime dT = CR_Employee_Feed.AxtriaSalesIQST__Employee__r.Termination_Date__c;
            //Date d = Date.newInstance(dT.year(), dT.month(), dT.day());
            term.separation_date__c = CR_Employee_Feed.AxtriaSalesIQST__Employee__r.Termination_Date__c;
            term.s__c = '0';
            term.psubarea__c = CR_Employee_Feed.AxtriaSalesIQST__Employee__r.Personnel_Subarea_Code__c;
            term.prev_term_pos_stext__c = ' ';
            term.prev_term_pos_short__c = ' ';
            term.prev_term_pos_code__c = CR_Employee_Feed.Position_ID__c;
            term.personnel_subarea__c = CR_Employee_Feed.AxtriaSalesIQST__Employee__r.Personnel_Subarea_Name__c;
            term.pers_no__c = CR_Employee_Feed.AxtriaSalesIQST__Employee__r.Personnel_Number__c;
            term.pa__c = CR_Employee_Feed.AxtriaSalesIQST__Employee__r.Personnel_Area_Number__c;

            TermInsertionList.add(term);
        }
        if(TermInsertionList.size()>0){
            insert TermInsertionList;
        }
    }

    global void finish(Database.BatchableContext BC) {

    }
}