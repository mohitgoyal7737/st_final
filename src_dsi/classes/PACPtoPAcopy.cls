global class PACPtoPAcopy implements Database.Batchable<SObject>, Schedulable {

       public final string query;
       public List<string> allTeamInstances;
       
       public PACPtoPAcopy(List<string> Teaminsatnceid) {
               allTeamInstances = new List<String>(Teaminsatnceid);                        
             query = 'Select Id,AxtriaSalesIQST__Party_ID__c, AxtriaSalesIQST__Segment4__c,AxtriaSalesIQST__Segment2__c,AxtriaSalesIQTM__Segment5__c,'+
                    'AxtriaSalesIQST__Segment3__c,AxtriaSalesIQST__Segment1__c,AxtriaSalesIQST__Segment5__c,AxtriaSalesIQTM__Segment2__c,'+
                    'AxtriaSalesIQTM__Segment3__c,AxtriaSalesIQST__Segment6__c,AxtriaSalesIQST__Segment7__c,AxtriaSalesIQST__Segment8__c,AxtriaSalesIQST__Segment9__c,AxtriaSalesIQST__Segment10__c,AxtriaSalesIQTM__Segment1__c,AxtriaSalesIQTM__Segment4__c,AxtriaSalesIQTM__Checkbox5_Updated__c From AxtriaSalesIQTM__Position_Account_Call_Plan__c '+ 
                    'where AxtriaSalesIQTM__Team_Instance__c in: allTeamInstances and AxtriaSalesIQST__Party_ID__c!=null';
       }   
       
       global Database.QueryLocator start(Database.BatchableContext bc) {
            return Database.getQueryLocator(query);
       }
       
        global void execute(Database.BatchableContext bc,  List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> scope) {
            Set<String> posAccId = new Set<String>();
            for(AxtriaSalesIQTM__Position_Account_Call_Plan__c obj : scope){
               posAccId.add(obj.AxtriaSalesIQST__Party_ID__c);
            }
            List<AxtriaSalesIQTM__Position_Account__c> posAcclst = [Select Id,AxtriaSalesIQTM__Street__c,AxtriaSalesIQTM__City__c,
            AxtriaSalesIQTM__State__c,AxtriaSalesIQTM__Pincode__c,Universe_Include__c  From AxtriaSalesIQTM__Position_Account__c where Id IN:posAccId];
            Map<String, AxtriaSalesIQTM__Position_Account__c> idToPosAccMap = new Map<String, AxtriaSalesIQTM__Position_Account__c>();
            for(AxtriaSalesIQTM__Position_Account__c obj:posAcclst){
               idToPosAccMap.put(obj.Id,obj);
            }
            
             
            
            List<AxtriaSalesIQTM__Position_Account__c> updatelst = new List<AxtriaSalesIQTM__Position_Account__c>();
            for(AxtriaSalesIQTM__Position_Account_Call_Plan__c obj : scope){
               AxtriaSalesIQTM__Position_Account__c posAcc = idToPosAccMap.get(obj.AxtriaSalesIQST__Party_ID__c);
               
               posAcc.AxtriaSalesIQTM__Street__c  = obj.AxtriaSalesIQST__Segment1__c;
               posAcc.AxtriaSalesIQTM__City__c    = obj.AxtriaSalesIQST__Segment3__c;
               posAcc.AxtriaSalesIQTM__State__c   = obj.AxtriaSalesIQST__Segment2__c;
               posAcc.AxtriaSalesIQTM__Pincode__c = obj.AxtriaSalesIQST__Segment4__c;
               posAcc.Address_Preference__c       = 'Primary';
               posAcc.Universe_Include__c = True;
               posAcc.Priority__c                 = obj.AxtriaSalesIQTM__Segment3__c;
               posAcc.Veeva_Affiliation__c        = obj.AxtriaSalesIQST__Segment5__c;
               posAcc.Treater_Referrer__c         = obj.AxtriaSalesIQTM__Segment5__c;
               posAcc.Infuser_Referrer__c         = obj.AxtriaSalesIQST__Segment6__c;
               posAcc.REMs_Certification_Flag__c  = obj.AxtriaSalesIQST__Segment7__c;
               posAcc.ENHERTU_GC_Priority__c      = obj.AxtriaSalesIQST__Segment8__c;
               posAcc.ENHERTU_BC_Priority__c      = obj.AxtriaSalesIQST__Segment9__c;
               posAcc.Is_RD_Target__c             = obj.AxtriaSalesIQST__Segment10__c;
               posAcc.Is_OncB_Target__c           = obj.AxtriaSalesIQTM__Segment1__c;
               posAcc.RD_Target_Territory__c      = obj.AxtriaSalesIQTM__Segment2__c;
               posAcc.OncB_Target_Territory__c    = obj.AxtriaSalesIQTM__Segment4__c;
               
               updatelst.add(posAcc);
            } 

            update updatelst;
        }
        
         global void execute(SchedulableContext sc) {
            Database.executeBatch(this, 1000);
       }   
       
        global void finish(Database.BatchableContext bc) {
        
       } 
       
 }