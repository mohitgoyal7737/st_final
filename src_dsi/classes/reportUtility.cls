global with sharing class reportUtility {
public static String fetchReportURL(){
        User loggedInUser = new User();
        loggedInUser = [select Id, profile.Name,SIQIC__TenantID__c  from User where Id = :UserInfo.getUserId()];
        system.debug('user:+'+loggedInUser.SIQIC__TenantID__c );
        Account acc=new Account();
        acc=[select id,Name,SIQIC__WebServiceCalloutURL__c from Account where id=:loggedInUser.SIQIC__TenantID__c];
        System.debug('acc------'+acc.SIQIC__WebServiceCalloutURL__c);
        String URL=acc.SIQIC__WebServiceCalloutURL__c;
        return URL;
}
public static String fetchDashboardURL(){
        User loggedInUser = new User();
        loggedInUser = [select Id, profile.Name,SIQIC__TenantID__c  from User where Id = :UserInfo.getUserId()];
        system.debug('user:+'+loggedInUser.SIQIC__TenantID__c );
        Account acc=new Account();
        acc=[select id,Name,DashboardWebServicePrefix__c from Account where id=:loggedInUser.SIQIC__TenantID__c];
        System.debug('acc------'+acc.DashboardWebServicePrefix__c);
        String URL=acc.DashboardWebServicePrefix__c;
        return URL;
}

public static SIQIC__Reports__c fetchReportMetadata(ID id){
    SIQIC__Reports__c report=new SIQIC__Reports__c();
    System.Debug('Id field ===' + id);
    report=[SELECT SIQIC__Business_Unit__c,SIQIC__publish__r.SIQIC__Report_Data_Push__c,SIQIC__Effective_Start_Date__c,SIQIC__EmpId__c,SIQIC__Employee_Name1__c,SIQIC__Geo_Id__c,SIQIC__Geo_Name__c,Id,SIQIC__Month__c,SIQIC__Parent_Geo_Id__c,SIQIC__Publish_Date__c,
                SIQIC__Publish__c,SIQIC__Publish__r.SIQIC__Publish_Date__c,SIQIC__RecordID__c,SIQIC__Report_Definition__c,SIQIC__Report_Name__c,SIQIC__Report_Period1__c,SIQIC__Report_Period__c,SIQIC__Report_Type__c,SIQIC__Role_with_Tenant__c,
                SIQIC__Role_w_o_Tenant__c,SIQIC__Role__c,SIQIC__Tenant__c,SIQIC__Type__c,SIQIC__URL__c,SIQIC__View_Link__c,SIQIC__Year__c,SIQIC__version__c,SIQIC__Path__c, SIQIC__Path2__c, SIQIC__Path3__c,SIQIC__Path4__c,SIQIC__User__r.LocaleSidKey,SIQIC__User__c,SIQIC__publish__r.SIQIC__ReportName__c, SIQIC__Delegation_Employee_ID__c FROM SIQIC__Reports__c WHERE Id=:id];
        if(report!=null){
            System.debug('reports'+report);
        }
    return report;
    
}


public static Map<String, Object> restCallOut(String endPoint){
        Http http = new Http();
         Map<String, Object> results=new Map<String, Object>();
        HttpRequest request = new HttpRequest();
        System.debug('Talend job endPoint'+endPoint);
        request.setEndpoint(endPoint);
        request.setMethod('GET');
        request.setTimeout(120000);
        HttpResponse response ;
        if(!test.isRunningTest())
                response = http.send(request);
            else
                response = respond(request);
        system.debug('#### Response : '+response);
        if(response.getStatusCode()==200){
            results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
        }
        //System.debug(results);
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Job Status!!  ' + endPoint + response.getStatus()));
        return results ;
      //return 'O';
    }

 public static HttpResponse respond(HTTPRequest req){
    HttpResponse res = new HttpResponse();
    res.setStatus('OK');
    res.setStatusCode(200);
    res.setBody('{"DS_0": [{"Report_Id": "a335e000000HwNCAA0", "Period": "FYQ1\'21", "Team": "OncB Team", "Budget_Utilization": 1.1213, "Level": null, "Total_Payout": "1252172.050000000000000", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "FYQ1\'21", "Team": "OncSC Team", "Budget_Utilization": 1.4312, "Level": null, "Total_Payout": "2756782.290000000000000", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "FYQ1\'21", "Team": "RD Team", "Budget_Utilization": 1.095, "Level": null, "Total_Payout": "243079.270000000000000", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}], "DS_1": [{"Report_Id": "a335e000000HwNCAA0", "Emp_Id": "10000070", "Emp_Name": "Brink, Christine", "Geo_Id": "4AFCGAN1", "Geo_Name": "Atlanta NE GA_OncSC", "Team": "OncSC", "Publish_Date": "2021-05-31", "Region_Rank": 4, "National_Rank": 10, "Payout_Text": "Projected Payout", "QTD_Payout": "19065.000000000000000", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}], "DS_2": [{"Report_Id": "a335e000000HwNCAA0", "Period": "Jun\'20", "Team": "RD_Turalio", "Product": "RD_Turalio", "Metric1": 66.93, "Metric2": null, "Metric3": null, "Y_Axis_Unit": "TMOTs|Attainment", "Legend": "Sales,Goals,Attainment", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "Jul\'20", "Team": "RD_Turalio", "Product": "RD_Turalio", "Metric1": 87.43, "Metric2": null, "Metric3": null, "Y_Axis_Unit": "TMOTs|Attainment", "Legend": "Sales,Goals,Attainment", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "Sep\'20", "Team": "RD_Turalio", "Product": "RD_Turalio", "Metric1": 93.93, "Metric2": null, "Metric3": null, "Y_Axis_Unit": "TMOTs|Attainment", "Legend": "Sales,Goals,Attainment", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "Mar\'21", "Team": "RD_Turalio", "Product": "RD_Turalio", "Metric1": 95.33, "Metric2": null, "Metric3": null, "Y_Axis_Unit": "TMOTs|Attainment", "Legend": "Sales,Goals,Attainment", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "Aug\'20", "Team": "RD_Turalio", "Product": "RD_Turalio", "Metric1": 97.43, "Metric2": null, "Metric3": null, "Y_Axis_Unit": "TMOTs|Attainment", "Legend": "Sales,Goals,Attainment", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "Feb\'21", "Team": "RD_Turalio", "Product": "RD_Turalio", "Metric1": 98.73, "Metric2": null, "Metric3": null, "Y_Axis_Unit": "TMOTs|Attainment", "Legend": "Sales,Goals,Attainment", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "Oct\'20", "Team": "RD_Turalio", "Product": "RD_Turalio", "Metric1": 106.8, "Metric2": null, "Metric3": null, "Y_Axis_Unit": "TMOTs|Attainment", "Legend": "Sales,Goals,Attainment", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "Dec\'20", "Team": "RD_Turalio", "Product": "RD_Turalio", "Metric1": 107.3, "Metric2": null, "Metric3": null, "Y_Axis_Unit": "TMOTs|Attainment", "Legend": "Sales,Goals,Attainment", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "Nov\'20", "Team": "RD_Turalio", "Product": "RD_Turalio", "Metric1": 107.33, "Metric2": null, "Metric3": null, "Y_Axis_Unit": "TMOTs|Attainment", "Legend": "Sales,Goals,Attainment", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "Jan\'21", "Team": "RD_Turalio", "Product": "RD_Turalio", "Metric1": 113.67, "Metric2": null, "Metric3": null, "Y_Axis_Unit": "TMOTs|Attainment", "Legend": "Sales,Goals,Attainment", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "May\'21", "Team": "RD_Turalio", "Product": "RD_Turalio", "Metric1": 115.23, "Metric2": 130.0, "Metric3": 0.8864, "Y_Axis_Unit": "TMOTs|Attainment", "Legend": "Sales,Goals,Attainment", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "Apr\'21", "Team": "RD_Turalio", "Product": "RD_Turalio", "Metric1": 129.87, "Metric2": 130.0, "Metric3": 0.999, "Y_Axis_Unit": "TMOTs|Attainment", "Legend": "Sales,Goals,Attainment", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "Jun\'20", "Team": "OncB_ENHERTU", "Product": "OncB_ENHERTU", "Metric1": 7149.0, "Metric2": null, "Metric3": null, "Y_Axis_Unit": "VIALS|Attainment", "Legend": "Sales,Goals,Attainment", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "Jul\'20", "Team": "OncB_ENHERTU", "Product": "OncB_ENHERTU", "Metric1": 8660.0, "Metric2": null, "Metric3": null, "Y_Axis_Unit": "VIALS|Attainment", "Legend": "Sales,Goals,Attainment", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "Aug\'20", "Team": "OncB_ENHERTU", "Product": "OncB_ENHERTU", "Metric1": 9154.0, "Metric2": null, "Metric3": null, "Y_Axis_Unit": "VIALS|Attainment", "Legend": "Sales,Goals,Attainment", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "Sep\'20", "Team": "OncB_ENHERTU", "Product": "OncB_ENHERTU", "Metric1": 9200.0, "Metric2": null, "Metric3": null, "Y_Axis_Unit": "VIALS|Attainment", "Legend": "Sales,Goals,Attainment", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "Feb\'21", "Team": "OncB_ENHERTU", "Product": "OncB_ENHERTU", "Metric1": 9905.0, "Metric2": null, "Metric3": null, "Y_Axis_Unit": "VIALS|Attainment", "Legend": "Sales,Goals,Attainment", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "Dec\'20", "Team": "OncB_ENHERTU", "Product": "OncB_ENHERTU", "Metric1": 9988.0, "Metric2": null, "Metric3": null, "Y_Axis_Unit": "VIALS|Attainment", "Legend": "Sales,Goals,Attainment", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "Oct\'20", "Team": "OncB_ENHERTU", "Product": "OncB_ENHERTU", "Metric1": 10012.0, "Metric2": null, "Metric3": null, "Y_Axis_Unit": "VIALS|Attainment", "Legend": "Sales,Goals,Attainment", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "Nov\'20", "Team": "OncB_ENHERTU", "Product": "OncB_ENHERTU", "Metric1": 10384.0, "Metric2": null, "Metric3": null, "Y_Axis_Unit": "VIALS|Attainment", "Legend": "Sales,Goals,Attainment", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "Jan\'21", "Team": "OncB_ENHERTU", "Product": "OncB_ENHERTU", "Metric1": 11086.0, "Metric2": null, "Metric3": null, "Y_Axis_Unit": "VIALS|Attainment", "Legend": "Sales,Goals,Attainment", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "Mar\'21", "Team": "OncB_ENHERTU", "Product": "OncB_ENHERTU", "Metric1": 11519.0, "Metric2": null, "Metric3": null, "Y_Axis_Unit": "VIALS|Attainment", "Legend": "Sales,Goals,Attainment", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "Apr\'21", "Team": "OncB_ENHERTU", "Product": "OncB_ENHERTU", "Metric1": 12896.0, "Metric2": 12094.0, "Metric3": 1.0663, "Y_Axis_Unit": "VIALS|Attainment", "Legend": "Sales,Goals,Attainment", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "May\'21", "Team": "OncB_ENHERTU", "Product": "OncB_ENHERTU", "Metric1": 13790.0, "Metric2": 12616.0, "Metric3": 1.0931, "Y_Axis_Unit": "VIALS|Attainment", "Legend": "Sales,Goals,Attainment", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "Jun\'20", "Team": "OncSC_Injectafer", "Product": "OncSC_Injectafer", "Metric1": 30767250.0, "Metric2": null, "Metric3": null, "Y_Axis_Unit": "MGs|Attainment", "Legend": "Sales,Goals,Attainment", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "Feb\'21", "Team": "OncSC_Injectafer", "Product": "OncSC_Injectafer", "Metric1": 36718500.0, "Metric2": null, "Metric3": null, "Y_Axis_Unit": "MGs|Attainment", "Legend": "Sales,Goals,Attainment", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "Dec\'20", "Team": "OncSC_Injectafer", "Product": "OncSC_Injectafer", "Metric1": 38900250.0, "Metric2": null, "Metric3": null, "Y_Axis_Unit": "MGs|Attainment", "Legend": "Sales,Goals,Attainment", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "Sep\'20", "Team": "OncSC_Injectafer", "Product": "OncSC_Injectafer", "Metric1": 40305000.0, "Metric2": null, "Metric3": null, "Y_Axis_Unit": "MGs|Attainment", "Legend": "Sales,Goals,Attainment", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "Aug\'20", "Team": "OncSC_Injectafer", "Product": "OncSC_Injectafer", "Metric1": 40637250.0, "Metric2": null, "Metric3": null, "Y_Axis_Unit": "MGs|Attainment", "Legend": "Sales,Goals,Attainment", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "Nov\'20", "Team": "OncSC_Injectafer", "Product": "OncSC_Injectafer", "Metric1": 41142750.0, "Metric2": null, "Metric3": null, "Y_Axis_Unit": "MGs|Attainment", "Legend": "Sales,Goals,Attainment", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "Mar\'21", "Team": "OncSC_Injectafer", "Product": "OncSC_Injectafer", "Metric1": 41223750.0, "Metric2": null, "Metric3": null, "Y_Axis_Unit": "MGs|Attainment", "Legend": "Sales,Goals,Attainment", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "Jan\'21", "Team": "OncSC_Injectafer", "Product": "OncSC_Injectafer", "Metric1": 44944500.0, "Metric2": null, "Metric3": null, "Y_Axis_Unit": "MGs|Attainment", "Legend": "Sales,Goals,Attainment", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "May\'21", "Team": "OncSC_Injectafer", "Product": "OncSC_Injectafer", "Metric1": 47089500.0, "Metric2": 45618620.45, "Metric3": 1.0322, "Y_Axis_Unit": "MGs|Attainment", "Legend": "Sales,Goals,Attainment", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "Oct\'20", "Team": "OncSC_Injectafer", "Product": "OncSC_Injectafer", "Metric1": 51254250.0, "Metric2": null, "Metric3": null, "Y_Axis_Unit": "MGs|Attainment", "Legend": "Sales,Goals,Attainment", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "Jul\'20", "Team": "OncSC_Injectafer", "Product": "OncSC_Injectafer", "Metric1": 51855750.0, "Metric2": null, "Metric3": null, "Y_Axis_Unit": "MGs|Attainment", "Legend": "Sales,Goals,Attainment", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}, {"Report_Id": "a335e000000HwNCAA0", "Period": "Apr\'21", "Team": "OncSC_Injectafer", "Product": "OncSC_Injectafer", "Metric1": 57900000.0, "Metric2": 46829478.88, "Metric3": 1.2364, "Y_Axis_Unit": "MGs|Attainment", "Legend": "Sales,Goals,Attainment", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Month": "May", "Reporting_Year": "2021", "Reporting_Period": null}], "DS_3": [{"Report_Id": "a335e000000HwNCAA0", "Emp_Id": "10000070", "Frequency": "Monthly", "Metric1": 1018500.0, "Metric2": 856219.173, "Metric3": 1.19, "Period": "FYQ1\'21", "Y_Axis_Unit": "Vials|Attainment", "Legend": "Sales,Goals,Attainment", "Product": "Injectafer", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Period": null, "Reporting_Year": "2021", "Reporting_Month": "May"}], "DS_4": [{"Report_Id": "a335e000000HwNCAA0", "Emp_Id": "10000070", "Frequency": "Monthly", "Metric1": 1018500.0, "Metric2": 856219.173, "Metric3": 1.19, "Period": "FYQ1\'21", "Y_Axis_Unit": "Vials|Attainment", "Legend": "Sales,Goals,Attainment", "Product": "Injectafer", "Publish_Date": "2021-05-31", "Version": "V1", "Reporting_Period": null, "Reporting_Year": "2021", "Reporting_Month": "May"}]}');
    return res;
  }

public static void createReportTrackingRecord(Id userId,Id reportId,String reportType,Id reportOwner,String reportPeriod) {   
        System.debug('++++in create Report Tracking record fn'+' userId++++'+userId+'++++reportId++++'+reportId+'++++reportOwner++++'+reportOwner);
        String extId = String.valueOf(reportOwner)+'_'+String.valueOf(reportId);
        System.debug('++++external id of Report tracking record++++'+extId);
        List<SIQIC__Report_Tracking__c> listReportTrackingRecs = [select id,SIQIC__Access_User__c,SIQIC__Last_Access_Date__c,SIQIC__View_Count__c from SIQIC__Report_Tracking__c where SIQIC__External_ID__c = :extId];
        //System.debug('++++listReportTrackingRecs++++'+listReportTrackingRecs+'++++listReportTrackingRecs.size()++++'+listReportTrackingRecs.size());
        SIQIC__Report_Tracking__c rec = new SIQIC__Report_Tracking__c();
        try
        {
            if(listReportTrackingRecs!=null && listReportTrackingRecs.size()>0)
            {System.debug('update');
                rec = listReportTrackingRecs[0];
                rec.SIQIC__Access_User__c = userId;
                rec.SIQIC__Last_Access_Date__c = DateTime.now();
                rec.SIQIC__View_Count__c+=1;
                
                update rec;
            }
            else
            {System.debug('insert');
                rec.Name = 'ReportTracking_'+'_'+extId;
                rec.SIQIC__Access_User__c = userId;
                rec.SIQIC__External_ID__c = extId;
                rec.SIQIC__First_Access_Date__c = DateTime.now();
                rec.SIQIC__Last_Access_Date__c = DateTime.now();
                //rec.Last_Access_Date__c = lastViewedDate;
                rec.SIQIC__View_Count__c=1;
                rec.SIQIC__ReportID__c = reportId;
                rec.SIQIC__Report_Owner__c = reportOwner;
                rec.SIQIC__Report_Type__c = reportType;
                rec.SIQIC__Report_Period__c = reportPeriod;
                
                
                insert rec;            
            }
        }
        catch(Exception e)
        {
            System.debug('++++e.message++++'+e.getMessage());
        }
    }
/*
Webservice static void reports_enableFlag(ID PublishID){
    
    System.debug('inside enableFlag method');
    System.debug('publish id is -- '+PublishID);
    
    //SIQIC__Publish__c publish = [SELECT Id,SIQIC__Business_Unit__c,SIQIC__Teams__c,SIQIC__ReportName__c,SIQIC__Tenant__c FROM SIQIC__Publish__c where Id =:PublishID];
    //System.debug('----Publish Record---- '+publish);
   
    
    
    try{
            
           // String queryString= 'select ID,SIQIC__Latest__c,SIQIC__Visible_to_Field__c,' + fieldName +
           // ' from SIQIC__Reports__c where SIQIC__Publish__c=' + PublishID;
            
            String queryString = 'select ID,SIQIC__Latest__c,SIQIC__Visible_to_Field__c,Enable_One_Scorecard__c from SIQIC__Reports__c where SIQIC__Publish__c=:PublishID';
            Boolean alreadyEnabled = True;
            
            
            
            List<SIQIC__Reports__c> reports = Database.query(queryString);
            
            SIQIC__Reports__c reportFirst = new SIQIC__Reports__c();
            reportFirst = reports[0];
            System.debug('First Report Record---'+reportFirst);
            
            if(reportFirst.Enable_One_Scorecard__c == True)
            {
            alreadyEnabled = True;
            System.debug('--Presently true--');
            }
            else
            {
            alreadyEnabled = False;
            System.debug('--Presently false--');
            }
            
            
            //List<SIQIC__Reports__c> reports = [select ID,SIQIC__Latest__c,SIQIC__Visible_to_Field__c from SIQIC__Reports__c where SIQIC__Publish__c=:PublishID ];
            for (SIQIC__Reports__c report : reports) 
            {
                if(alreadyEnabled == False)
                {
                report.Enable_One_Scorecard__c = True;    
                System.debug('--set true--');
                }
                else
                {
                report.Enable_One_Scorecard__c = False;
                System.debug('--set False--');
                }
                                 
            }
            Update reports;
            System.debug('---- Reports Size ---- '+reports.size());
       }
        catch(Exception e){
            System.debug('---- Exception ---- '+e.getStackTraceString());
        }
        

}

public static List<SIQIC__Reports__c> fetchDashboardReportId(){
    System.debug('inside fetchDashboardReportId method');
    String reportDataPushId = '';
    Integer i =0;
    Integer j=0;
    i = i+1;
    i = i+1;
    i = i+1;
    i = i+1;
    i = i+1;
    i = i+1;
    i = i+1;
    i = i+1;
    i = i+1;
    i = i+1;
    i = i+1;
    i = i+1;
    i = i+1;
    j = j+1;
    j = j+1;
    j = j+1;
    j = j+1;
    j = j+1;
    j = j+1;
    j = j+1;
    j = j+1;
    j = j+1;
    j = j+1;
    j = j+1;
    i = i+1;
    i = i+1;
    i = i+1;
    i = i+1;
    i = i+1;
    i = i+1;
    i = i+1;
    i = i+1;
    i = i+1;
    j = j+1;
    j = j+1;
    j = j+1;
    j = j+1;
    j = j+1;
    j = j+1;
    j = j+1;
    j = j+1;
    j = j+1;
    User loggedInUser = new User();
    loggedInUser = [select Id, profile.Name,SIQIC__Business_Unit__c,SIQIC__TenantID__c,Userrole.name  from User where Id = :UserInfo.getUserId()];
    System.debug('user role is -- '+loggedInUser.Userrole.name);
    System.debug('user ID is -- '+loggedInUser.Id);
    
    List<SIQIC__Reports__c> reports =[Select ID,enable_Dashboard__c,SIQIC__Role_with_Tenant__c,SIQIC__User__c,SIQIC__Roster__c,SIQIC__Business_Unit__c,SIQIC__Report_Definition__c,SIQIC__Month__c,SIQIC__Year__c,SIQIC__Version__c FROM SIQIC__Reports__c where enable_Dashboard__c= True and SIQIC__User__c=:loggedInUser.Id and SIQIC__Report_Type__c = 'Dashboard DataPush' order by CreatedDate desc limit 1];
    System.debug('reports record -- '+reports);
    //if (reports.size() > 0){
    //reportDataPushId = reports[0].SIQIC__Report_Definition__c;
    //System.debug('reportDataPushId is -- '+reportDataPushId);
    //}
    return reports;
}

 Webservice static void enableDashboardFlag(ID PublishID){
    System.debug('inside enableDashboardFlag method');
    System.debug('publish id is -- '+PublishID);
    
    SIQIC__Publish__c publish = [SELECT Id,SIQIC__Business_Unit__c,SIQIC__Teams__c,SIQIC__ReportName__c,SIQIC__Tenant__c FROM SIQIC__Publish__c where Id =:PublishID];
    System.debug('----Publish Record---- '+publish);
    
    try{
            List<SIQIC__Reports__c> reports = [select ID,SIQIC__Latest__c,SIQIC__Visible_to_Field__c from SIQIC__Reports__c where SIQIC__Publish__c=:PublishID ];
            for (SIQIC__Reports__c report : reports) 
            {
                report.enable_Dashboard__c = True;    
                //System.debug('--set true--');                 
            }
            Update reports;
            System.debug('---- Reports Size ---- '+reports.size());
       }
        catch(Exception e){
            System.debug('---- Exception ---- '+e.getStackTraceString());
        }

}


Webservice static boolean oneSCflag(ID PublishID){
            String queryString = 'select ID,SIQIC__Latest__c,SIQIC__Visible_to_Field__c,Enable_One_Scorecard__c from SIQIC__Reports__c where SIQIC__Publish__c=:PublishID';
            Boolean alreadyEnabled = True;
            
            
            
            List<SIQIC__Reports__c> reports = Database.query(queryString);
            
            SIQIC__Reports__c reportFirst = new SIQIC__Reports__c();
            reportFirst = reports[0];
            System.debug('First Report Record---'+reportFirst);
            
            if(reportFirst.Enable_One_Scorecard__c == True)
            {
            alreadyEnabled = True;
            System.debug('--Presently true--');
            }
            else
            {
            alreadyEnabled = False;
            System.debug('--Presently false--');
            }
            
            return alreadyEnabled;

}*/


}