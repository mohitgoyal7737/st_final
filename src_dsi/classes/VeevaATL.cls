global class VeevaATL implements Schedulable,Database.AllowsCallouts{
    
    public static final String QUEUE_LOGGER_RECORDTYPE = 'Queue Logger';
    public static final String QUEUE_LOGGER_TYPE_MASTER = 'VeevaATL';
    public static final String BRMS_MODULE = 'VeevaATL' ;
    public static String IN_PROGRESS = 'In Progress';
    public static String QUEUED = 'Queued';
    public static final String ETL_BRMS_Instance = 'VeevaCRM';
    public static String ERROR = 'Error';
    
    global void execute(SchedulableContext SC) {
        // AxtriaSalesIQTM__Is_Master__c = true
        
        
        Id recordId = Schema.SObjectType.AxtriaSalesIQTM__SalesIQ_Logger__c.getRecordTypeInfosByName().get(QUEUE_LOGGER_RECORDTYPE).getRecordTypeId();
        AxtriaSalesIQTM__SalesIQ_Logger__c masterSynclog = new AxtriaSalesIQTM__SalesIQ_Logger__c(AxtriaSalesIQTM__Module__c=BRMS_MODULE, AxtriaSalesIQTM__Status__c=IN_PROGRESS, AxtriaSalesIQTM__Type__c= QUEUE_LOGGER_TYPE_MASTER);
        masterSynclog.RecordtypeId= recordId;
        insert masterSynclog;

        callpythonService(masterSyncLog.id);
        
    }
    
    public static  string getBRMSConfigValues(string type){ 
        List<AxtriaSalesIQTM__BRMS_Config__mdt> typesList = [Select AxtriaSalesIQTM__BRMS_Type__c,AxtriaSalesIQTM__BRMS_Value__c from AxtriaSalesIQTM__BRMS_Config__mdt where AxtriaSalesIQTM__BRMS_Type__c= :type ];
        string value;
        if(typesList.size()>0){
            value=typesList[0].AxtriaSalesIQTM__BRMS_Value__c; 
        }  
        return value; 
    }
    
    public static AxtriaSalesIQTM__ETL_Config__c getETLConfigByName(string etlConfigName){
        AxtriaSalesIQTM__ETL_Config__c etlConfigList;
        etlConfigList = [SELECT AxtriaSalesIQTM__SF_UserName__c, AxtriaSalesIQTM__SF_Password__c, AxtriaSalesIQTM__S3_Security_Token__c,CRM_User_Name__c,CRM_Password__c,CRM_Org_Type__c,CRM_Security_Token__c,AxtriaSalesIQTM__SFTP_Username__c,AxtriaSalesIQTM__SFTP_Port__c,AxtriaSalesIQTM__SFTP_Password__c,AxtriaSalesIQTM__BR_PG_UserName__c,AxtriaSalesIQTM__BR_PG_Host__c,AxtriaSalesIQTM__BR_PG_Database__c,AxtriaSalesIQTM__GI_Dataset_Id__c FROM AxtriaSalesIQTM__ETL_Config__c where name =:etlConfigName limit 1];
        return etlConfigList;
    }

    public static Boolean isSandboxOrg() {
        List<AxtriaSalesIQTM__PackageExtension__c> sandbox = [select Id from AxtriaSalesIQTM__PackageExtension__c where Name like 'sandbox'];
        if(sandbox.isEmpty())
            return false;
        else
            return true;
    }

    public static HttpResponse getHTTPResponse(String endPoint, Map<String,String> mapParams, String requestMethod, Integer timeOut ){
        Http h = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndPoint(endPoint);
        String body='';
        for(String key : mapParams.keySet()){
            body+= key+'='+String.valueof(mapParams.get(key)) + '&';
        } 
        system.debug('body IS'+body);           
        request.setMethod(requestMethod);
        request.setTimeout(timeOut);
        request.setBody(body.removeEnd('&'));  
        HttpResponse reponse = h.send(request);
        return reponse;
    }

    //system.debug('body IS'+body);

    @future(callout=true)
    public static void  callpythonService(String masterSynclogId){
        string pythonService = getBRMSConfigValues(BRMS_MODULE);
        //SSL CHANGES
        Map<String,String> urlParams = new Map<String,String>();
        urlParams.put('serviceUrl',pythonService);

        AxtriaSalesIQTM__ETL_Config__c etlObj = getETLConfigByName(ETL_BRMS_Instance);
        String sfUserName =etlObj.AxtriaSalesIQTM__SF_UserName__c;
        String sfPassword =etlObj.AxtriaSalesIQTM__SF_Password__c;
        String sfToken =etlObj.AxtriaSalesIQTM__S3_Security_Token__c;
        String vsfUsername =etlObj.CRM_User_Name__c;
        String vsfPassword =etlObj.CRM_Password__c;
        String vsfsf_token =etlObj.CRM_Security_Token__c;
        String vsforg_type = etlObj.CRM_Org_Type__c;
        String org_name =etlObj.AxtriaSalesIQTM__GI_Dataset_Id__c;
        
        try{
            String isSandbox = ( isSandboxOrg() ) ? 'true' : 'false';
            urlParams.put('sf_username',sfUserName);
            urlParams.put('sf_password',sfPassword);
            urlParams.put('sf_token',sfToken);
            urlParams.put('sf_sandbox',isSandbox);
            urlParams.put('veeva_sf_username', vsfUsername);
            urlParams.put('veeva_sf_password', vsfPassword);
            urlParams.put('veeva_sf_token',vsfsf_token);
            urlParams.put('veeva_sf_sandbox',vsforg_type);
            urlParams.put('namespace','AxtriaSalesIQTM__');
            urlParams.put('logger_id', masterSynclogId);
            urlParams.put('logger_object_name', 'AxtriaSalesIQTM__SalesIQ_Logger__c');
            urlParams.put('org_name', org_name);
            


            HttpResponse response = getHTTPResponse(urlParams.get('serviceUrl'),urlParams,'POST',120000);

            system.debug('#### Python Response : '+response);
            if(response.getStatusCode() != 200) {
                AxtriaSalesIQTM__SalesIQ_Logger__c updateLoggerStatus = new AxtriaSalesIQTM__SalesIQ_Logger__c(Id = masterSynclogId);
                updateLoggerStatus.AxtriaSalesIQTM__Status__c = ERROR;
                updateLoggerStatus.AxtriaSalesIQTM__Stack_Trace__c = response.getStatus();
                update updateLoggerStatus;
            }
        }
        catch(exception ex){
            system.debug('Error in master sync : '+ex.getMessage());
        }
    }
}