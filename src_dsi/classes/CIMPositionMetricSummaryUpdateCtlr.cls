public with sharing class CIMPositionMetricSummaryUpdateCtlr {
    
    public static void updatePosMetricSummary(String teamInstanceId, String positionId, String crType){

        SnTDMLSecurityUtil.printDebugMessage('----------- Inside updatePosMetricSummary() function -------');
        SnTDMLSecurityUtil.printDebugMessage('selectedTeamInstance :: '+teamInstanceId);
        SnTDMLSecurityUtil.printDebugMessage('selectedPosition :: '+positionId);
        SnTDMLSecurityUtil.printDebugMessage('crType :: '+crType);

        List<AxtriaSalesIQTM__CIM_Config__c> listCIM = [select Name, AxtriaSalesIQTM__Attribute_API_Name__c,AxtriaSalesIQTM__Aggregation_Object_Name__c,AxtriaSalesIQTM__Object_Name__c, AxtriaSalesIQTM__Aggregation_Type__c, AxtriaSalesIQTM__Aggregation_Condition_Attribute_Value__c, AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c, AxtriaSalesIQTM__Threshold_Max__c, AxtriaSalesIQTM__Threshold_Min__c, AxtriaSalesIQTM__Threshold_Warning_Max__c, AxtriaSalesIQTM__Threshold_Warning_Min__c, AxtriaSalesIQTM__MetricCalculationType__c, AxtriaSalesIQTM__Enforcable__c,AxtriaSalesIQTM__Change_Request_Type__r.AxtriaSalesIQTM__CR_Type_Name__c from AxtriaSalesIQTM__CIM_Config__c Where AxtriaSalesIQTM__Team_Instance__c =: teamInstanceId And AxtriaSalesIQTM__Change_Request_Type__r.AxtriaSalesIQTM__CR_Type_Name__c =: crType and AxtriaSalesIQTM__Enable__c=true];
        
        Map<String, String> mapCIMidMetricValue = new Map<String, String>();
        Map<String, AxtriaSalesIQTM__CIM_Config__c> mapCIMidCIMObj = new Map<String, AxtriaSalesIQTM__CIM_Config__c>();

        for(AxtriaSalesIQTM__CIM_Config__c objCIM : listCIM){
            String cr_type_name = objCIM.AxtriaSalesIQTM__Change_Request_Type__r.AxtriaSalesIQTM__CR_Type_Name__c;
            String aggregationObjectName = objCIM.AxtriaSalesIQTM__Aggregation_Object_Name__c;
            Boolean conditionCallPlan = (cr_type_name == SalesIQGlobalConstants.CR_TYPE_CALL_PLAN && aggregationObjectName == 'AxtriaSalesIQTM__Position_Account_Call_Plan__c') ? true : false;
            
            if(!String.isBlank(objCIM.AxtriaSalesIQTM__Attribute_API_Name__c) && !String.isBlank(objCIM.AxtriaSalesIQTM__Aggregation_Object_Name__c) && !String.isBlank(objCIM.AxtriaSalesIQTM__Object_Name__c)){
                String objectToBeQueried = '';
                String soql = '';
                if(objCIM.AxtriaSalesIQTM__Object_Name__c != objCIM.AxtriaSalesIQTM__Aggregation_Object_Name__c){
                    objectToBeQueried =  objCIM.AxtriaSalesIQTM__Object_Name__c.removeEnd('c');
                    objectToBeQueried = objectToBeQueried + 'r.'+objCIM.AxtriaSalesIQTM__Attribute_API_Name__c;

                    if(!String.isBlank(objCIM.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c)){
                        soql = 'select '+objCIM.AxtriaSalesIQTM__Aggregation_Type__c+'(' + objectToBeQueried + ') ' + ' from ' +
                        objCIM.AxtriaSalesIQTM__Aggregation_Object_Name__c + ' where AxtriaSalesIQTM__team_instance__c =:teamInstanceID and AxtriaSalesIQTM__Position__c =: positionId AND (' + objCIM.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c+' )';
                        
                    }else{
                        soql = 'select '+objCIM.AxtriaSalesIQTM__Aggregation_Type__c+'(' + objectToBeQueried + ') ' + ' from ' +
                        objCIM.AxtriaSalesIQTM__Aggregation_Object_Name__c + ' where AxtriaSalesIQTM__team_instance__c =:teamInstanceID AND AxtriaSalesIQTM__Position__c =: positionId ';
                    }
                }else {
                    objectToBeQueried = objCIM.AxtriaSalesIQTM__Attribute_API_Name__c;

                    if(!String.isBlank(objCIM.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c)){
                        soql = 'select '+objCIM.AxtriaSalesIQTM__Aggregation_Type__c+'(' + objectToBeQueried + ') ' + ' from ' +
                        objCIM.AxtriaSalesIQTM__Aggregation_Object_Name__c + ' where AxtriaSalesIQTM__team_instance__c =:teamInstanceID and AxtriaSalesIQTM__Position__c =: positionId AND (' + objCIM.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c+')';
                    }else{
                        soql = 'select '+objCIM.AxtriaSalesIQTM__Aggregation_Type__c+'(' + objectToBeQueried + ') ' + ' from ' +
                        objCIM.AxtriaSalesIQTM__Aggregation_Object_Name__c + ' where AxtriaSalesIQTM__team_instance__c =:teamInstanceID AND AxtriaSalesIQTM__Position__c =: positionId ';
                    }
                }
                //SNT-515
                if(String.isNotBlank(soql) && conditionCallPlan){
                    soql += ' AND AxtriaSalesIQTM__isincludedCallPlan__c = true';                        
                }
                /*SNT-515 ends*/
                SnTDMLSecurityUtil.printDebugMessage('--soql ' + soql);

                List<AggregateResult> resultAggResult = Database.query(soql);  
                if(resultAggResult.size() > 0 && resultAggResult[0].get('expr0')!=null)
                    mapCIMidMetricValue.put((String)objCIM.Id, String.valueOf(resultAggResult[0].get('expr0')));  
                else
                    mapCIMidMetricValue.put((String)objCIM.Id, '0');  

                mapCIMidCIMObj.put(objCIM.Id, objCIM);
            }
        }

        List<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c> listPosMetricSummary = [Select AxtriaSalesIQTM__CIM_Config__c, AxtriaSalesIQTM__Original__c, AxtriaSalesIQTM__Proposed__c from AxtriaSalesIQTM__CIM_Position_Metric_Summary__c where AxtriaSalesIQTM__CIM_Config__c in: mapCIMidMetricValue.keySet() AND 
            AxtriaSalesIQTM__Team_Instance__c=: teamInstanceId AND AxtriaSalesIQTM__Position_Team_Instance__r.AxtriaSalesIQTM__Position_Id__c =: positionId];
        
        Map<String, AxtriaSalesIQTM__CIM_Position_Metric_Summary__c> mapCIMidPosMetricObj = new Map<String, AxtriaSalesIQTM__CIM_Position_Metric_Summary__c>();
        for(AxtriaSalesIQTM__CIM_Position_Metric_Summary__c cimPosObj : listPosMetricSummary){
            /*if(status == SalesIQGlobalConstants.REQUEST_STATUS_APPROVED){
                cimPosObj.Approved__c = mapCIMidMetricValue.get(cimPosObj.CIM_Config__c);
                cimPosObj.Proposed__c = mapCIMidMetricValue.get(cimPosObj.CIM_Config__c);
            }
            else*/
                cimPosObj.AxtriaSalesIQTM__Proposed__c = mapCIMidMetricValue.get(cimPosObj.AxtriaSalesIQTM__CIM_Config__c);

            mapCIMidPosMetricObj.put(cimPosObj.AxtriaSalesIQTM__CIM_Config__c, cimPosObj);
        }

        if(listPosMetricSummary.size() > 0){    
            //update listPosMetricSummary;
            SnTDMLSecurityUtil.updateRecords(listPosMetricSummary, 'CIMPositionMetricSummaryUpdateCtlr');
        }
    }

}