public class PBI_DSI_GC_Ctlr_Test
{
 public Map<String,Object> results {get;set;}
        public String results1 {get;set;}
         public String Param1 {get;set;}
    public String pbiFilters {get;set;} 
    public String pbiEmbedDetails {get;set;} 
    public String pbiAccessToken  {get;set;} 
    public String pbiWebQuryRsult {get;set;} 
    Public String filterValues;
    public String URL ;
    public String report_type;
     public String UserEmpID {get;set;}
      public String UserGeoID {get;set;} 
      public String BusinessUnit {get;set;}
         public String role;
         public String UserRole {get;set;}
           public String Report {get;set;} 
           public String Version {get;set;}
            public String Tenant {get;set;}
          public String Sheet_Name {get;set;}  
            public String ProductName {get;set;}  
        public String month {get;set;}
        public String year {get;set;}
        public String gobacklink{get;set;}
    
    public PBI_DSI_GC_Ctlr_Test()
    {
        //Map<String, String> pbiValues = DSI_Utility.pbiFilter();
        //pbiFilters = pbiValues.get('Filters');
        //System.debug('pbiFilters Paramters : ' + pbiFilters );
        
        //pbiEmbedDetails = pbiValues.get('EmbedDetails');
        pbiAccessToken = DSI_Utility.getAccessToken();
        Tenant = DSI_Utility.getTenantName();
        
        
       // runWebQuery();
    }
    
    public void runWebQuery()
    {
        Integer dbServer = 2; 
        String procName = 'getWebQuery_Result';
        
        Map<String, List<Map<String, String>>> procResult = new Map<String, List<Map<String, String>>>();
        
        String filterValues = EncodingUtil.URLENCODE(pbiFilters,'UTF-8');
        System.debug('After Encode Filter Paramters : ' + filterValues );
        
        Map<String, List<Map<String, String>>> m = DSI_Utility.getProcResponse(dbServer, procName, filterValues);        
        List<Map<String, String>> recordsMap  = m.get('DS_0');

        pbiWebQuryRsult = recordsMap[0].get('Param0');
        System.debug('Filter0 Values pbiWebQuryRsult : ' + pbiWebQuryRsult );
                            
        recordsMap  = m.get('DS_1');
        pbiWebQuryRsult = pbiWebQuryRsult + ';' + recordsMap[0].get('Param1');
        System.debug('Filter1 Values pbiWebQuryRsult : ' + pbiWebQuryRsult );
        
        recordsMap  = m.get('DS_2');
        pbiWebQuryRsult = pbiWebQuryRsult + ';' + recordsMap[0].get('Param2');
        System.debug('Filter2 Values pbiWebQuryRsult : ' + pbiWebQuryRsult );
        
        recordsMap  = m.get('DS_3');
        pbiWebQuryRsult = pbiWebQuryRsult + ';' + recordsMap[0].get('Param3');
        System.debug('Filter3 Values pbiWebQuryRsult : ' + pbiWebQuryRsult );
        
        recordsMap  = m.get('DS_4');
        pbiWebQuryRsult = pbiWebQuryRsult + ';' + recordsMap[0].get('Param4');
        System.debug('Filter4 Values pbiWebQuryRsult : ' + pbiWebQuryRsult );
    }
    
    
    
    //---------------added for excel
    
    public void GetDataFromWebQuery()
     {
    Report_type='Goal Card';
    UserGeoID='4CRAA000';
    BusinessUnit = 'National Rare Disease';
    UserRole ='DM';
    Report ='a335e000000HnheAAC';
    Version = 'V1';
    Sheet_Name = 'DM';
    ProductName ='ENHERTU Goal';
    
   
   
        URL='https://dsi-ic.axtria.com/DSI_PBI_downloadExcel?';
        List<Object> summaryDataMap=new List<Object>();
        
        String parameters;
        parameters='Report_type='+ Report_type + '&Geo_id='+UserGeoID+'&Business_Unit='+BusinessUnit+
               '&Role='+UserRole+'&ReportID='+Report+'&Version='+Version+'&Sheet_Name='+Sheet_Name+'&productName='+ProductName+'&tenant='+Tenant;
        parameters=parameters.replace(' ','%20');           
        system.debug(parameters); 
        
        String endPoint = URL + parameters;
        System.Debug('EndPoint ===== ' + endPoint);
       // results1='';
        //results1=reportUtility11.getContent(endPoint);
        System.debug('results1+================+ ' + results1);  
     }   
      

     public PageReference downloadReport1()
    {
        
        PageReference pg;
        GetDataFromWebQuery();
        System.debug('results outside GetDataFromWebQuery function +=========+'+results1);
        
        Param1=results1;
        System.debug('Param1 +=========+'+Param1);
        
        pg = new PageReference(Param1);
        pg.setRedirect(true);
        return pg; 
    } 
        
    public void trackReportInst()
    {
        Id userId = UserInfo.getUserId();
        Id reportId = ApexPages.currentPage().getParameters().get('id');
        SIQIC__Reports__c inst = [select OwnerId,SIQIC__Report_Type__c,SIQIC__Report_Period__c,LastViewedDate from SIQIC__Reports__c where id = :reportId limit 1];
        reportUtility.createReportTrackingRecord(userId,reportId,inst.SIQIC__Report_Type__c,inst.OwnerId,inst.SIQIC__Report_Period__c);       
    }
}