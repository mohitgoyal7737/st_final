public with sharing class Call_Plan_Territory_report_Ctrl
{

  public string territorySelected                                             {get; set;}
  public List<SelectOption> allTerritories                                    {get; set;}
  public List<AxtriaSalesIQTM__User_Access_Permission__c> loggedInUserData    {get; set;}
  public String reportname                                                    {get; set;}
  public String teamInstanceName;

  public Call_Plan_Territory_report_Ctrl()
  {
    loggedInUserData     = new List<AxtriaSalesIQTM__User_Access_Permission__c>();
    loggedInUserData = SalesIQUtility.getUserAccessPermistion(Userinfo.getUserId());
    allTerritories      = new List<SelectOption>();


    //openReport(loggedInUserData[0].AxtriaSalesIQTM__Position__c);
    if(loggedInUserData != null && loggedInUserData.size() > 0)
    {
      loggedInUserCheck();
    }
  }

  public PageReference loggedInUserCheck()
  {
    allTerritories      = new List<SelectOption>();
    PageReference pg = null;
    territorySelected = loggedInUserData[0].AxtriaSalesIQTM__Position__c;

    if(loggedInUserData[0].AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c == 'Territory')
    {
      territorySelected = loggedInUserData[0].AxtriaSalesIQTM__Position__c;
      teamInstanceName = loggedInUserData[0].AxtriaSalesIQTM__Team_Instance__r.Name;
      pg = openReport(territorySelected);
    }
    else if(loggedInUserData[0].AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c == 'District')
    {
      //List<AxtriaSalesIQTM__Position__c> territoryList = new List<>();
      allTerritories.add(new SelectOption(loggedInUserData[0].AxtriaSalesIQTM__Position__c, 'All'));
      for(AxtriaSalesIQTM__Position__c terrPos : [Select id, Name from AxtriaSalesIQTM__Position__c Where AxtriaSalesIQTM__Parent_Position__c = :loggedInUserData[0].AxtriaSalesIQTM__Position__c])
      {
        allTerritories.add(new SelectOption(terrPos.id, terrPos.Name));
      }
      territorySelected = allTerritories[0].getValue();
      teamInstanceName = loggedInUserData[0].AxtriaSalesIQTM__Team_Instance__r.Name;
    }
    return pg;
  }

  public PageReference runReport()
  {
    return openReport(territorySelected);
  }

  public PageReference openReport(String positionId)
  {
    PageReference pg = null;
    if(loggedInUserData[0].AxtriaSalesIQTM__Team_Instance__r.Name == 'OncB' || loggedInUserData[0].AxtriaSalesIQTM__Team_Instance__r.Name == 'ADC')
    {
      List<Report> rp = [select id from Report where DeveloperName = 'Call_Plan_Territory_ADC'];
      //PageReference pg = null;
      if(rp.size() > 0)
      {
        pg = new PageReference ('/' + rp[0].Id + '?pv0=' + positionId.substring(0, 15) + '&pv1=' + positionId.substring(0, 15));
        pg.setRedirect(true);
        //System.debug('+++++++++++++  pg   +++++ ' + pg);
        //window.open('/'+rp[0].Id+'?pv0='+positionId.substring(0,15)+'&pv1='+positionId.substring(0,15),true);
      }

    }
    else if(loggedInUserData[0].AxtriaSalesIQTM__Team_Instance__r.Name == 'RD')
    {
      List<Report> rp = [select id from Report where DeveloperName = 'Call_Plan_Territory_RD'];
      //PageReference pg = null;
      if(rp.size() > 0)
      {
        pg = new PageReference ('/' + rp[0].Id + '?pv0=' + positionId.substring(0, 15) + '&pv1=' + positionId.substring(0, 15));
        pg.setRedirect(true);
        //System.debug('+++++++++++++  pg   +++++ ' + pg);
        //window.open('/'+rp[0].Id+'?pv0='+positionId.substring(0,15)+'&pv1='+positionId.substring(0,15),true);
      }

    }
    else
    {
      List<Report> rp = [select id from Report where DeveloperName = 'Call_Plan_Territory_Report'];
      //PageReference pg = null;
      if(rp.size() > 0)
      {
        pg = new PageReference ('/' + rp[0].Id + '?pv0=' + positionId.substring(0, 15) + '&pv1=' + positionId.substring(0, 15));
        pg.setRedirect(true);
        //System.debug('+++++++++++++  pg   +++++ ' + pg);
        //window.open('/'+rp[0].Id+'?pv0='+positionId.substring(0,15)+'&pv1='+positionId.substring(0,15),true);
      }
    }
    //System.debug('+++++++++++++  pg   +++++ ' + pg);
    return pg;
  }

}