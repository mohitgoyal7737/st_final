/*
@author     : A1942
@date       : Aug-10-2021
@description: Controller class for CallPlanReport VF page
Revison(s)  : v1.0
*/
public  with sharing class CallPlanReportCtrl {

    public String selectedTeamInstance {get;set;}
    public String selectedPosition;
    public String topLevelPosition;
    public Map<Id, List<Id>> teamInstancePosIdMap;

    //public String selectedCycleName {get;set;}
    //public String selectedTeamInstanceName {get;set;}
    public String selectedWorkspace {get;set;}
    public String posName {get;set;}
    public String jsonStringTerr {get;set;}
    public String selectedQtr {get;set;}
    public String selectedTeamName {get;set;}
    public boolean isPilotReport {get;set;}
    public String selectedHierarchy {get;set;}
    public boolean isUserPositon {get;set;}
    //public Map<String, List<String>> cycleTeamInstanceMap {get; set;}
    public Map<String, String> teamInstanceTeamMap;
    public List<SelectOption> teamsList {get;set;}
    public List<SelectOption> qtrList {get;set;}
    public Map<String, String> hierarchyTypeMap {get;set;}

    public String bucketname;
    public String host;
    public String key;
    public String secret;

    public CallPLanReportCtrl(){

        selectedWorkspace = '';
        selectedTeamInstance = '';
        selectedPosition = '';
        topLevelPosition = '';
        teamInstancePosIdMap = new Map<Id, List<Id>>();
        jsonStringTerr = '';
        selectedQtr = '';
        //cycleTeamInstanceMap = new Map<String, List<String>>();
        teamsList = new List<SelectOption>();
        qtrList = new List<SelectOption>();
        teamInstanceTeamMap = new Map<String, String>();
        selectedTeamName = '';
        isPilotReport = false;
        selectedHierarchy = '';
        hierarchyTypeMap = new Map<String,String>();
        isUserPositon = false;

        try{
            hierarchyTypeMap.put('1','Territory');
            hierarchyTypeMap.put('2','District');
            hierarchyTypeMap.put('3','Region');
            hierarchyTypeMap.put('4','Area');

            for(AxtriaSalesIQTM__Workspace__c obj: [Select Id, Name From AxtriaSalesIQTM__Workspace__c where Enable_Call_Plan_Report__c=true order by createdDate desc]){
                selectedQtr = String.isNotBlank(selectedQtr) ? selectedQtr : obj.Name;
                selectedWorkspace = String.isNotBlank(selectedWorkspace) ? selectedWorkspace : obj.Id;
                qtrList.add(new SelectOption(obj.Id,obj.Name));
            } 

            qtrChanged();
        }catch(Exception e){
            System.debug('Exception in CallPLanReportCtrl()--> '+e.getMessage());
            System.debug('Stack trace --> '+e.getStackTraceString());
        }
    }

    public void qtrChanged(){

        System.debug('selectedWorkspace : ' + selectedWorkspace);
        jsonStringTerr = '';
        teamsList = new List<SelectOption>();
        selectedTeamName = '';
        topLevelPosition = '';
        selectedPositionLabel = '';
        isUserPositon = false;
        try{

            List<AxtriaSalesIQTM__Workspace__c> workspace = [Select Id, Name From AxtriaSalesIQTM__Workspace__c where Enable_Call_Plan_Report__c=true and Id =: selectedWorkspace];
            selectedQtr = workspace!=null && workspace.size()>0 ? workspace[0].Name : '';

            System.debug('selectedQtr : '+selectedQtr);
            System.debug('workspace : '+workspace);

            List<AxtriaSalesIQTM__User_Access_Permission__c> userPosition = [Select Id, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Position__c, AxtriaSalesIQTM__Position__r.Name,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__HIERARCHY_LEVEL__C, AxtriaSalesIQTM__User__r.SIQIC__TenantID__c from AxtriaSalesIQTM__User_Access_Permission__c where AxtriaSalesIQTM__User__c = :UserInfo.getUserId() and Enable_Call_Plan_Report__c = true and /*AxtriaSalesIQTM__Is_Active__c = true and*/ AxtriaSalesIQTM__Team_Instance__r.Enable_Call_Plan_Report__c = true and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQST__Workspace__c =:selectedWorkspace /*AxtriaSalesIQST__isCallPlanEnabled__c = true*/ and AxtriaSalesIQTM__Position__c != null WITH SECURITY_ENFORCED order by AxtriaSalesIQTM__Position__r.Name];
            System.debug('userPosition : '+userPosition);
            Set<String> uniqueTeamsInstances = new Set<String>();
            for(AxtriaSalesIQTM__User_Access_Permission__c userInfo:userPosition){

                if(!uniqueTeamsInstances.contains(userInfo.AxtriaSalesIQTM__Team_Instance__c)){
                    teamsList.add(new SelectOption(userInfo.AxtriaSalesIQTM__Team_Instance__c, userInfo.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name));
                    selectedTeamName = String.isBlank(selectedTeamName) ? userInfo.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name : selectedTeamName; 
                    uniqueTeamsInstances.add(userInfo.AxtriaSalesIQTM__Team_Instance__c);
                    teamInstanceTeamMap.put(userInfo.AxtriaSalesIQTM__Team_Instance__c,userInfo.AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name);
                }

                if(teamInstancePosIdMap.containsKey(userInfo.AxtriaSalesIQTM__Team_Instance__c)) {
                    teamInstancePosIdMap.get(userInfo.AxtriaSalesIQTM__Team_Instance__c).add(userInfo.AxtriaSalesIQTM__Position__c);
                }
                else {
                    teamInstancePosIdMap.put(userInfo.AxtriaSalesIQTM__Team_Instance__c, new List<ID> {userInfo.AxtriaSalesIQTM__Position__c});
                }
            }

            if(userPosition!=null && userPosition.size()>0){
                selectedTeamInstance = userPosition[0].AxtriaSalesIQTM__Team_Instance__c;
                topLevelPosition = selectedPosition = userPosition[0].AxtriaSalesIQTM__Position__c;

                posName = [select AxtriaSalesIQTM__Client_Position_Code__c from AxtriaSalesIQTM__Position__c where id = :selectedPosition  WITH SECURITY_ENFORCED].AxtriaSalesIQTM__Client_Position_Code__c;
                teamInstanceChanged();
                showTree();
            }else {
                isUserPositon = true;
            }


            String tenantId = userPosition!=null && userPosition.size()>0 ? userPosition[0].AxtriaSalesIQTM__User__r.SIQIC__TenantID__c : '';
            System.debug('tenantId : '+tenantId);
            if(String.isNotBlank(tenantId)){
                List<Account> dsiAccount = [Select Id, SIQIC__accessKey__c, SIQIC__secretKey__c, S3Host__c, SIQIC__bucket__c From Account where Id =: tenantId WITH SECURITY_ENFORCED limit 1];
                bucketname  = dsiAccount[0].SIQIC__bucket__c;
                host        = dsiAccount[0].S3Host__c;
                key         = dsiAccount[0].SIQIC__accessKey__c;
                secret      = dsiAccount[0].SIQIC__secretKey__c;
            }

            isReportAvailable = checkReport();
            isPilotReport = checkPilotReport();
        }catch(Exception e){
            System.debug('Error in qtrChanged() --> '+ e.getMessage());
            System.debug('Stack trace --> '+e.getStackTraceString());
        }
    }

    public void showTree() {

        System.debug('topLevelPosition :: ' + topLevelPosition);
        Boolean isHierarchyGreaterThan5 = false;
        Set<String> level5PosId = new Set<String>(); 
        List<String> rootNodePositionIds = new List<String>();
        List<String> level5PositionIds = new List<String>();
        List<AxtriaSalesIQTM__Position__c> tempPos = new List<AxtriaSalesIQTM__Position__c>();
        Map<Id, AxtriaSalesIQTM__Position__c> allPositionMap = new Map<Id, AxtriaSalesIQTM__Position__c>();

        try{

            rootNodePositionIds.add(topLevelPosition);
            if(teamInstancePosIdMap != null && teamInstancePosIdMap.containsKey(selectedTeamInstance)) {
                tempPos = [select id, Name, AxtriaSalesIQTM__Hierarchy_Level__c, AxtriaSalesIQTM__Level_5_Position__c FROM AxtriaSalesIQTM__Position__c where id in :teamInstancePosIdMap.get(selectedTeamInstance) order by Name];
            }
            else {
                tempPos = [select id, AxtriaSalesIQTM__Hierarchy_Level__c, AxtriaSalesIQTM__Level_5_Position__c FROM AxtriaSalesIQTM__Position__c where id = :topLevelPosition];
            }

            for(integer i = 0; i < tempPos.size(); i++) {
                if(Integer.valueOf(tempPos[i].AxtriaSalesIQTM__Hierarchy_Level__c) > 5 && tempPos[i].AxtriaSalesIQTM__Level_5_Position__c != null) {
                    isHierarchyGreaterThan5 = true;
                    List<String> positionIds = new List<String>();
                    positionIds.addAll(tempPos[i].AxtriaSalesIQTM__Level_5_Position__c.split(','));
                    level5PosId.addAll(positionIds);
                    positionIds.add(tempPos[i].id);
                    level5PositionIds.addAll(positionIds);
                }
                else {
                    //level5PositionIds = new list<string>{uap.Position__c};
                    level5PositionIds.add(tempPos[i].id);
                }
            }
            System.debug('level5PositionIds ====> '+level5PositionIds);

            AxtriaSalesIQTM__Team_Instance__c ti = [select AxtriaSalesIQTM__Base_team_Instance__c, AxtriaSalesIQTM__Converging_Level__c from AxtriaSalesIQTM__Team_Instance__c where id =:selectedTeamInstance];

            String baseTeamInstance = selectedTeamInstance;
            if(ti.AxtriaSalesIQTM__Base_team_Instance__c != null){
                baseTeamInstance = ti.AxtriaSalesIQTM__Base_team_Instance__c;
            }

            Integer convergingLevel = 0;

            if(ti.AxtriaSalesIQTM__Converging_Level__c != null){
                convergingLevel = Integer.valueof(ti.AxtriaSalesIQTM__Converging_Level__c);
            }

            List<AxtriaSalesIQTM__Position__c> allAccessiblePositions = SalesIQUtility.getPositionbyRootPositionId(level5PositionIds, selectedTeamInstance, baseTeamInstance, convergingLevel);
            System.debug('allAccessiblePositions===========> '+allAccessiblePositions);
            System.debug('level5PosId===========> '+level5PosId);

            List<AxtriaSalesIQTM__Position__c> allPositions = new List<AxtriaSalesIQTM__Position__c>();
            for(AxtriaSalesIQTM__Position__c obj : allAccessiblePositions){
                if(!(obj.Name.contains('unassigned') || obj.Name.contains('whitespace') || obj.Name.contains('Whitespace') || obj.Name.contains('Unassigned'))){
                    allPositions.add(obj);
                }
            }

            if(isHierarchyGreaterThan5){
                Integer j = 0;
                while (j < level5PositionIds.size()){
                    if(!level5PosId.isEmpty()){
                        if(level5PosId.contains(level5PositionIds.get(j))){
                            level5PositionIds.remove(j);
                        }
                        else{
                            j++;
                        }
                    }
                }
            }
            jsonStringTerr = SalesIQUtility.getJsonString(level5PositionIds, allPositions);
            System.debug('jsonStringTerr===========> '+jsonStringTerr);
        }catch(Exception e){
            System.debug('Error in showTree() --> '+e.getMessage());
            System.debug('Stack trace --> '+e.getStackTraceString());
        }
    }

    public void teamInstanceChanged(){
        System.debug('selectedTeamInstance : '+selectedTeamInstance);
        selectedPositionLabel = '';

        try{
            if(String.isNotBlank(selectedTeamInstance)){
                if(teamInstanceTeamMap.containsKey(selectedTeamInstance)){
                    selectedTeamName = teamInstanceTeamMap.get(selectedTeamInstance);
                }
                List<Id> positions = teamInstancePosIdMap.get(selectedTeamInstance);
                if(positions!=null && positions.size()>0){
                    selectedPosition = topLevelPosition = positions[0];
                    showTree();
                    List<AxtriaSalesIQTM__Position__c> position = [Select Id, Name, AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Client_Territory_Name__c, AxtriaSalesIQTM__Hierarchy_Level__c from AxtriaSalesIQTM__Position__c where Id = :selectedPosition WITH SECURITY_ENFORCED limit 1];
                    selectedTerritoryCode = position[0].AxtriaSalesIQTM__Client_Position_Code__c;
                    selectedPositionLabel = position[0].AxtriaSalesIQTM__Client_Territory_Name__c;
                    selectedPositionName = position[0].Name;
                    selectedHierarchy = position[0].AxtriaSalesIQTM__Hierarchy_Level__c;
                    isReportAvailable = checkReport();
                    isPilotReport = checkPilotReport();
                }
            }
        }catch(Exception e){
            System.debug('Error in teamInstanceChanged() --> '+e.getMessage());
            System.debug('Stack trace --> '+e.getStackTraceString());
        }
    }

    public String selectedTerritoryCode {get;set;}
    public Boolean isReportAvailable {get;set;}
    public String selectedPositionLabel {get;set;}
    public String selectedPositionName;
    public void positionChange(){
        selectedPositionName='';
        selectedPositionLabel = '';
        String baseTeamInstance = '';
        List<AxtriaSalesIQTM__Position__c> position = new List<AxtriaSalesIQTM__Position__c>();
        System.debug('selectedTerritoryCode :: '+selectedTerritoryCode);
        try{
            if(String.isNotBlank(selectedTerritoryCode)){
                position = [Select Id, Name, AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Client_Territory_Name__c, AxtriaSalesIQTM__Hierarchy_Level__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Client_Position_Code__c=:selectedTerritoryCode and AxtriaSalesIQTM__Team_Instance__c=:selectedTeamInstance WITH SECURITY_ENFORCED limit 1];
                if(position==null || position.size()==0){
                    List<AxtriaSalesIQTM__Team_Instance__c> teamInsCross = [Select Id, AxtriaSalesIQTM__Base_team_Instance__c from AxtriaSalesIQTM__Team_Instance__c where Id=:selectedTeamInstance WITH SECURITY_ENFORCED limit 1];
                    if(teamInsCross!=null && teamInsCross.size()>0 && String.isNotBlank(teamInsCross[0].AxtriaSalesIQTM__Base_team_Instance__c)){
                        baseTeamInstance = teamInsCross[0].AxtriaSalesIQTM__Base_team_Instance__c;
                        System.debug('baseTeamInstance : '+baseTeamInstance);
                        position = [Select Id, Name, AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Client_Territory_Name__c, AxtriaSalesIQTM__Hierarchy_Level__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Client_Position_Code__c=:selectedTerritoryCode and AxtriaSalesIQTM__Team_Instance__c=:baseTeamInstance WITH SECURITY_ENFORCED limit 1];
                        
                    }
                }
                selectedPositionLabel = position[0].AxtriaSalesIQTM__Client_Territory_Name__c;
                selectedPositionName = position[0].Name;
                selectedHierarchy = position[0].AxtriaSalesIQTM__Hierarchy_Level__c;
                isReportAvailable = checkReport();
                isPilotReport = checkPilotReport();
            }

        }catch(Exception e){
            System.debug('Error in positionChange() --> '+e.getMessage());
            System.debug('Stack trace --> '+e.getStackTraceString());
        }
    }

    public String reportUrl {get;set;}
    public Boolean checkReport(){

        Boolean isAvailable = false;
        reportUrl = '';
        try{
            String now = Datetime.now().formatGMT('EEE, dd MMM yyyy HH:mm:ss z');
            String contentType = 'text/html';
            String method = 'GET';
            String filePath = 'dsi-salesiq/CallPlanReports/'+selectedQtr+'/'+selectedTeamName+'/'+ hierarchyTypeMap.get(selectedHierarchy) + '/' + selectedTerritoryCode +'.xlsx';//'dsi-salesiq/CPF/TerritorySummaryReport.xlsx';
            String encodedFilePath = filePath.replace(' ', '%20');


            HttpRequest req = new HttpRequest();
            req.setMethod(method);
            req.setEndpoint('http://' + host + '/' + encodedFilePath);
            req.setHeader('Host', bucketname + '.' + host);
            req.setHeader('Content-Encoding', 'UTF-8');
            req.setHeader('Content-type', contentType);
            req.setHeader('Connection', 'keep-alive');
            req.setHeader('Date', now);
            req.setHeader('ACL', 'public-read');

            String stringToSign = 'GET\n\n' + contentType + '\n' + now + '\n' +'/' + bucketname + '/' + encodedFilePath;
            String encodedStringToSign = EncodingUtil.urlEncode(stringToSign, 'UTF-8');
            System.debug('stringToSign : '+stringToSign+', secret : '+secret);
            Blob mac = Crypto.generateMac('HMACSHA1', Blob.valueOf(stringToSign), Blob.valueOf(secret));
            String signed = EncodingUtil.base64Encode(mac);
            String authHeader = 'AWS' + ' ' + key + ':' + signed;
            req.setHeader('Authorization', authHeader);
            String decoded = EncodingUtil.urlDecode(encodedStringToSign , 'UTF-8');

            Http http = new Http();
            HTTPResponse res = http.send(req);
            System.debug('RESPONSE STATUS: ' + res.getStatus());
            System.debug('STATUS_CODE: ' + res.getStatusCode());
            if(res.getStatusCode()==200){
                isAvailable = true;
                String datasetName = 'DSI - '+ selectedQtr +' '+ hierarchyTypeMap.get(selectedHierarchy) +' Call Plan Report - ' + selectedTerritoryCode + ' ' + selectedPositionName + '.xlsx';
                String file = 'dsi-salesiq/CallPlanReports/'+selectedQtr+'/'+selectedTeamName+'/'+ hierarchyTypeMap.get(selectedHierarchy) + '/'+ selectedTerritoryCode +'.xlsx';
                String filename1 = EncodingUtil.urlEncode(file, 'UTF-8');

                Datetime nowDt = DateTime.now();
                Datetime expireson = nowDt.AddSeconds(6000); // Lifespan of the link
                Long Lexpires = expireson.getTime()/1000;

                String stringtosign1 = 'GET\n\n\n'+Lexpires+'\n/'+bucketName+'/'+filename1+'?response-content-disposition=attachment; filename='+datasetName;
                System.debug('redirectToS3Key stringstosign1: ' + stringtosign1);

                String signingKey = EncodingUtil.base64Encode(Blob.valueOf(secret));
                Blob mac1 = Crypto.generateMac('HMacSHA1', blob.valueof(stringtosign1),blob.valueof(secret));
                String signed1= EncodingUtil.base64Encode(mac1);
                String codedsigned = EncodingUtil.urlEncode(signed1,'UTF-8');
                reportUrl = 'https://'+bucketName+'.s3.amazonaws.com/'+filename1
                    +'?response-content-disposition='+EncodingUtil.urlEncode('attachment; filename='+datasetName,'UTF-8')
                    +'&AWSAccessKeyId='+key+'&Expires='+Lexpires+'&Signature='+codedsigned;
                System.debug('reportUrl : '+reportUrl);
            }
        }catch(Exception e){
            System.debug('Error in checkReport() --> '+e.getMessage());
            System.debug('Stack trace --> '+e.getStackTraceString());
        }
        return isAvailable;
    }

    public String pilotReportUrl {get;set;}
    public Boolean checkPilotReport(){

        Boolean isAvailable = false;
        pilotReportUrl = '';
        try{
            String now = Datetime.now().formatGMT('EEE, dd MMM yyyy HH:mm:ss z');
            String contentType = 'text/html';
            String method = 'GET';
            String filePath = 'dsi-salesiq/CallPlanReports/'+selectedQtr+'/'+selectedTeamName+'/'+ hierarchyTypeMap.get(selectedHierarchy) + '/Pilot_'+ selectedTerritoryCode +'.xlsx';//'dsi-salesiq/CPF/TerritorySummaryReport.xlsx';
            String encodedFilePath = filePath.replace(' ', '%20');

            HttpRequest req = new HttpRequest();
            req.setMethod(method);
            req.setEndpoint('http://' + host + '/' + encodedFilePath);
            req.setHeader('Host', bucketname + '.' + host);
            req.setHeader('Content-Encoding', 'UTF-8');
            req.setHeader('Content-type', contentType);
            req.setHeader('Connection', 'keep-alive');
            req.setHeader('Date', now);
            req.setHeader('ACL', 'public-read');

            String stringToSign = 'GET\n\n' + contentType + '\n' + now + '\n' +'/' + bucketname + '/' + encodedFilePath;
            String encodedStringToSign = EncodingUtil.urlEncode(stringToSign, 'UTF-8');
            Blob mac = Crypto.generateMac('HMACSHA1', Blob.valueOf(stringToSign), Blob.valueOf(secret));
            String signed = EncodingUtil.base64Encode(mac);
            String authHeader = 'AWS' + ' ' + key + ':' + signed;
            req.setHeader('Authorization', authHeader);
            String decoded = EncodingUtil.urlDecode(encodedStringToSign , 'UTF-8');

            Http http = new Http();
            HTTPResponse res = http.send(req);
            System.debug('RESPONSE STATUS: ' + res.getStatus());
            System.debug('STATUS_CODE: ' + res.getStatusCode());
            if(res.getStatusCode()==200){
                isAvailable = true;
                String datasetName = 'DSI - '+ selectedQtr +' '+ hierarchyTypeMap.get(selectedHierarchy) +' Call Plan Pilot Report - ' + selectedTerritoryCode + ' ' + selectedPositionName + '.xlsx';
                String file = 'dsi-salesiq/CallPlanReports/'+selectedQtr+'/'+selectedTeamName+'/'+ hierarchyTypeMap.get(selectedHierarchy) +'/Pilot_'+ selectedTerritoryCode +'.xlsx';//filePath;//'dsi-salesiq/CPF/TerritorySummaryReport.xlsx';
                String filename1 = EncodingUtil.urlEncode(file, 'UTF-8');

                Datetime nowDt = DateTime.now();
                Datetime expireson = nowDt.AddSeconds(6000); // Lifespan of the link
                Long Lexpires = expireson.getTime()/1000;

                String stringtosign1 = 'GET\n\n\n'+Lexpires+'\n/'+bucketName+'/'+filename1+'?response-content-disposition=attachment; filename='+datasetName;
                System.debug('redirectToS3Key stringstosign1: ' + stringtosign1);

                String signingKey = EncodingUtil.base64Encode(Blob.valueOf(secret));
                Blob mac1 = Crypto.generateMac('HMacSHA1', blob.valueof(stringtosign1),blob.valueof(secret));
                String signed1= EncodingUtil.base64Encode(mac1);
                String codedsigned = EncodingUtil.urlEncode(signed1,'UTF-8');
                pilotReportUrl = 'https://'+bucketName+'.s3.amazonaws.com/'+filename1
                    +'?response-content-disposition='+EncodingUtil.urlEncode('attachment; filename='+datasetName,'UTF-8')
                    +'&AWSAccessKeyId='+key+'&Expires='+Lexpires+'&Signature='+codedsigned;
                System.debug('pilotReportUrl : '+pilotReportUrl);
            }
        }catch(Exception e){
            System.debug('Error in checkPilotReport() --> '+e.getMessage());
            System.debug('Stack trace --> '+e.getStackTraceString());
        }
        return isAvailable;
    }
}