@IsTest
private class Rosterschedulebatches {
  
    
    static testmethod void test() {

        Test.startTest();
        Employee_Staging_Delete usa = new Employee_Staging_Delete();
        usa.dummyFunction();
        //usa.dummy2Func();
        Id batchId = Database.executeBatch(usa);

        CR_Employee_Removed usp = new CR_Employee_Removed();
        usp.dummyFunction();
        //usa.dummy2Func();
        Id batchId1 = Database.executeBatch(usp);

        ErroredOutRosterEmailFuctionality_1  ust = new ErroredOutRosterEmailFuctionality_1();
        ust.dummyFunction();
        //usa.dummy2Func();
        //Id batchId2 = Database.executeBatch(ust);
        
        Test.stopTest();

    }
}