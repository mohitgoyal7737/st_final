public with sharing class RosterUtilityClass {

  public RosterUtilityClass() {}

  
  public static boolean checkReHire( AxtriaSalesIQTM__Employee__c empRec,AxtriaSalesIQST__SIQ_Employee_Master__c stagingRec){
    if(stagingRec.SIQ_Last_Hire_Date_c__c != stagingRec.Original_Hire_Date__c && stagingRec.SIQ_Last_Hire_Date_c__c != Date.today() &&
      empRec.Last_Hire_Date__c!=stagingRec.SIQ_Last_Hire_Date_c__c 
      && stagingRec.SIQ_Action_Type_c__c == 'HIR' && stagingRec.Reason_for_Action_Name__c == 'Hire_Employee_Hire_Employee_Rehire' &&
      (stagingRec.Employment_Status__c=='Active' || stagingRec.Employment_Status__c=='Inactive')){
        return true;
    }
    return false;
  }

  public static boolean checkTermination(AxtriaSalesIQTM__Employee__c empRec,AxtriaSalesIQST__SIQ_Employee_Master__c stagingRec){
    if(empRec.Termination_Date__c != stagingRec.AxtriaSalesIQST__SIQ_Termination_Date__c && 
      stagingRec.AxtriaSalesIQST__SIQ_Termination_Date__c <= date.Today() && 
      ((stagingRec.SIQ_Action_Type_c__c == 'TERM' && empRec.Action_Type__c != 'TERM') || 
      (stagingRec.SIQ_Action_Type_c__c == 'TERM-C' && empRec.Action_Type__c != 'TERM-C') ||
      (stagingRec.SIQ_Action_Type_c__c == 'TERM-R' && empRec.Action_Type__c != 'TERM-R'))&&
      (stagingRec.Employment_Status__c=='Withdrawn' || stagingRec.Employment_Status__c=='Retiree')){
        return true;
    }
    return false;
  }

  public static boolean checkPromotion(AxtriaSalesIQTM__Employee__c empRec,AxtriaSalesIQST__SIQ_Employee_Master__c stagingRec){
    if(stagingRec.SIQ_Action_Type_c__c == 'DTA' && empRec.Action_Type__c != 'DTA' && 
        empRec.Reason_for_Action_Name__c != stagingRec.Reason_for_Action_Name__c && stagingRec.Reason_for_Action_Name__c == 'Promotion' &&
      empRec.Promotion_date__c != stagingRec.SIQ_Promotion_date_c__c && stagingRec.SIQ_Promotion_date_c__c <= Date.today()
      && stagingRec.SIQ_Action_Start_Date_c__c <= Date.today()&&
      (stagingRec.Employment_Status__c=='Active' || stagingRec.Employment_Status__c=='Inactive')){
        return true;
    }
    return false;
  }

  public static boolean checkDemotion( AxtriaSalesIQTM__Employee__c empRec,AxtriaSalesIQST__SIQ_Employee_Master__c stagingRec){
   if(stagingRec.SIQ_Action_Type_c__c=='DTA' && empRec.Action_Type__c!='DTA' && empRec.Reason_for_Action_Name__c != stagingRec.Reason_for_Action_Name__c 
   && stagingRec.Reason_for_Action_Name__c == 'Demotion' &&
    (empRec.Demotion_date__c != stagingRec.SIQ_Demotion_date_c__c) && stagingRec.SIQ_Demotion_date_c__c <= Date.Today() && 
    stagingRec.SIQ_Action_Start_Date_c__c <= System.today()&&
      (stagingRec.Employment_Status__c=='Active' || stagingRec.Employment_Status__c=='Inactive')){
      return true;
    }  
    return false;
  }


  public static boolean checkLOA( AxtriaSalesIQTM__Employee__c empRec,AxtriaSalesIQST__SIQ_Employee_Master__c stagingRec){
    if((stagingRec.SIQ_Action_Type_c__c=='LOA' || stagingRec.SIQ_Action_Type_c__c=='LOA-C' || stagingRec.SIQ_Action_Type_c__c=='LOA-R' || stagingRec.SIQ_Action_Type_c__c=='‘CONT-LOA') && stagingRec.SIQ_Action_Start_Date_c__c <= System.today() && 
      (empRec.Action_Type__c != 'LOA' || empRec.Reason_for_Action_Name__c != stagingRec.Reason_for_Action_Name__c)&&
      (stagingRec.Employment_Status__c=='Inactive')){
        return true;
    }
    return false;
  }

  public static boolean checkReturnLOA( AxtriaSalesIQTM__Employee__c empRec,AxtriaSalesIQST__SIQ_Employee_Master__c stagingRec){
    if((stagingRec.SIQ_Action_Type_c__c=='RFL' || stagingRec.SIQ_Action_Type_c__c=='RFL-R')&& stagingRec.SIQ_Action_Start_Date_c__c <= System.today() && 
        (empRec.Action_Type__c != 'RFL' && empRec.Action_Type__c != 'RFL-R') &&
      (stagingRec.Employment_Status__c=='Active' || stagingRec.Employment_Status__c=='Inactive')){
        return true;
    }
    return false;
  }

  public static boolean checkTransfer(AxtriaSalesIQTM__Employee__c empRec,AxtriaSalesIQST__SIQ_Employee_Master__c stagingRec){
    if(stagingRec.Functional_Area_ID__c!=Null && stagingRec.Functional_Area_ID__c != empRec.Functional_Area_ID__c &&
      (stagingRec.Employment_Status__c=='Active' || stagingRec.Employment_Status__c=='Inactive')){
        return true;
    }
    if(stagingRec.SIQ_Action_Type_c__c=='TFR' && ((stagingRec.Employment_Status__c=='Active' || stagingRec.Employment_Status__c=='Inactive'))){
        return true;
    }
    return false;
  }

}