public class CreateAdapterController {
    
    public String adapterName {get;set;}
    public String adapterDatatype {get;set;}
    public String adapterColumns {get;set;}
    public String mandatoryColumns {get;set;}
    public String column_string {get;set;}
    public User loggedInUser { get; set;}
    public String tent {get;set;}
    public List<SIQIC__Column__c> columnlist;
    public List<SIQIC__Template__c> templatelist;
    public SIQIC__Column__c Column;
    public SIQIC__Template__c Template;
    public Integer len {get;set;}
    public String b {get;set;}
    //public Integer pos {get;set;}
    //public Integer pos1 {get;set;}
    public String URL {get;set;}
    public Map<String,Object> results {get;set;}
    public Boolean ValidateError {get;set;}
    public String resMessage{get;set;}
    
    public CreateAdapterController(){
        
        loggedInUser = new User();
        loggedInUser = [select Id, profile.Name,SIQIC__TenantID__c,SIQIC__Tenant__c,LocaleSidKey  from User where Id = :UserInfo.getUserId()];
        tent = loggedInUser.SIQIC__TenantID__c;
        system.debug('user tenant: '+ tent );
        ValidateError = False;
        resMessage= '';
        URL='http://52.246.255.155/adaper_creation_utility/service';
        
        System.debug('URL2: '  +URL);
        
        
  
    }
    
    public void Upload_Columns_sfdc(){  
        
        system.debug('Under Upload_Columns_sfdc------>>');
        
        List<SIQIC__Column__c> columnlist=new List<SIQIC__Column__c>();
        Column = new SIQIC__Column__c();
        
        String str=adapterColumns+';';
        system.debug('adapter column_string--->>'+adapterColumns);
        
        String col_name='';
        String dt='';
        len=str.length();
        System.debug('Length'+len);
        
        Integer seq=0;
        List<String> columnList1=new List<String>();
        columnList1.addAll(str.split(';'));
        System.debug(columnList1);
        
        for(String col:columnList1){
            
            seq=seq+1;
            Column = new SIQIC__Column__c();
            System.debug('---col--'+col);
            List<String> c1=col.split(' ');
                
            Column.Name=c1.get(0);
            //Column.SIQIC__Data_Type__c=c1.get(1);
            Column.SIQIC__Data_Type__c=c1.get(1).replace('NTEXT', 'TEXT');
            Column.SIQIC__TenantVal__c =tent;
            Column.SIQIC__Sequence__c=seq;
                
            if(Column.SIQIC__Data_Type__c=='Number')
                Column.SIQIC__Precision__c =13;
            else
                Column.SIQIC__Precision__c =0;
            
            Column.SIQIC__DataType__c =adapterName;
            Column.SIQIC__Tenant__c =tent;
            System.debug('Column to be added = ' + Column);
            columnlist.add(Column);
        }
        system.debug('Columnlist------>>'+columnlist);
        insert(columnlist);
        System.Debug('Column list inserted successfully');
    }
    
    public void Upload_Template_sfdc(){  
        
        System.debug('Inside Template creation ==');
        Template = new SIQIC__Template__c();
        Template.Name= adapterName;
        Template.SIQIC__Tenant__c= tent;
        
        List<SIQIC__Template__c> adp_list = new  List<SIQIC__Template__c>();
        integer i,maxVal=0;
        
        adp_list=[select SIQIC__Data_Adapter_ID__c,SIQIC__Tenant__c from SIQIC__Template__c WHERE SIQIC__Data_Adapter_ID__c !=  NULL and SIQIC__Data_Adapter_ID__c!='null1' and SIQIC__Tenant__c=: tent order by SIQIC__Data_Adapter_ID__c  desc ];
        
        for ( SIQIC__Template__c temp : adp_list)
        {
            i=integer.valueOf(temp.SIQIC__Data_Adapter_ID__c);
            if( i > maxVal)
                maxVal = i;
        }
        
        Template.SIQIC__Data_Adapter_ID__c = String.valueOf(maxVal+1);
        b=String.valueOf(maxVal+1);
        System.debug('b---------------------->>'+b);
        system.debug('Data Adapter ID:'+Template.SIQIC__Data_Adapter_ID__c);
        
        Template.SIQIC__Data_Adapter__c = adapterName;
        Template.SIQIC__Data_Type__c = adapterDatatype;
        Template.SIQIC__Data_Columns__c =column_string;
        Template.SIQIC__SQL_Column_Names__c=column_string;
        Template.SIQIC__SQL_Table_Name__c ='t_'+adapterName;
        
        // debugging--->>
        system.debug('Template.Name: '+Template.Name);
        system.debug('Template.SIQIC__Tenant__c: '+  Template.SIQIC__Tenant__c);
        system.debug('Template.SIQIC__Data_Adapter_ID__c: '+Template.SIQIC__Data_Adapter_ID__c);
        system.debug('Template.SIQIC__Data_Type__c: '+ Template.SIQIC__Data_Type__c);
        system.debug('Template.SIQIC__Data_Adapter__c: '+Template.SIQIC__Data_Adapter__c);
        system.debug('Template.SIQIC__Data_Columns__c: '+ Template.SIQIC__Data_Columns__c);
        system.debug('Template.SIQIC__SQL_Column_Names__c: '+Template.SIQIC__SQL_Column_Names__c);
        system.debug('Template.SIQIC__SQL_Table_Name__c: '+ Template.SIQIC__SQL_Table_Name__c);
        
        insert(Template);
    
    }

    public void saveTextValues(){   
        
        integer a;
        column_string = adapterColumns.replace(' TEXT','');
        column_string = column_string.replace(' NUMBER', '');
        column_string = column_string.replace(' DECIMAL', '');
        column_string = column_string.replace(' DATE', '');
        column_string = column_string.replace(' NTEXT', '');
        column_string = column_string.replace(';', ',');
        System.debug('Adapter Name'+adapterName);
        System.debug('Adapter DataType'+adapterDatatype);
        System.debug('Adapter Columns'+adapterColumns);
        System.debug('Mandatory Columns'+mandatoryColumns);
       
        if(!ValidateError)
        {  
            System.debug('No Adaptor with same name & data type found for this tenant, hence create a new one');
           Upload_Columns_sfdc(); /*commented by Jaya */
           Upload_Template_sfdc(); 
            //callCreateAdapterProc();
        
            //adapterName='';
            //adapterDatatype='';
            //adapterColumns='';
            //mandatoryColumns='';
        }
    }
    
    public void Validate(){
    
        if(ValidateError){ 
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Adaptor with same name & data type already exist for this tenant'));   
        }   
        else
        {
            if(!Test.isRunningTest())
                callCreateAdapterProc();
            if(!resMessage.contains('Adaptor successfully created'))
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.warning,resMessage)); 
                DeleteAdaptorfromSFDC();
            }
            else
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,resMessage));
            }  
        }
    
    }
    
    public void DeleteAdaptorfromSFDC()
    {
        System.debug('Delete newly created columns & adaptor from the org');
        System.debug('logged in user tenant'+ loggedInUser.SIQIC__TenantID__c);
        System.debug('Adaptor Data Type' + adapterDatatype  );
        System.debug('Adaptor Name'+ adapterName);
        List<SIQIC__Template__c> TempExisList= [select id from SIQIC__Template__c where SIQIC__Tenant__c  =:loggedInUser.SIQIC__TenantID__c and SIQIC__Data_Type__c =:adapterDatatype and SIQIC__Data_Adapter__c =:adapterName];
        System.debug(TempExisList);
        System.debug('loggedInUser TenantID' + loggedInUser.SIQIC__TenantID__c);
        System.debug('Column Adaptor Name'+ adapterName);
        List<SIQIC__Column__c> ColumnList=[select id from SIQIC__Column__c  where SIQIC__TenantVal__c     =:loggedInUser.SIQIC__TenantID__c and SIQIC__DataType__c  =:adapterName];
        System.debug('ColumnList');
        if(ColumnList.size() > 0)
        {
            delete ColumnList;
            System.debug('Columns deleted');
        }
        if(TempExisList.size() > 0)
        {
            delete TempExisList;
            System.debug('Template deleted');
        }
    }
    public void ValidateAction()
    {
        List<SIQIC__Template__c> TempExisList = [select id from SIQIC__Template__c where SIQIC__Tenant__c  =:loggedInUser.SIQIC__TenantID__c and SIQIC__Data_Type__c =:adapterDatatype and SIQIC__Data_Adapter__c =:adapterName];
        if(TempExisList.size() > 0)
        {
            ValidateError = True;
            System.debug('Adaptor with same name & data type already exist for this tenant');
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Adaptor with same name & data type already exist for this tenant'));   
        }
        else
        {
            saveTextValues();
           // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.success,'Adaptor created successfully')); 
        }
    }
    
    public void callCreateAdapterProc(){
        
        System.debug('=====================================Calling Create Adapter Procedure ==================================');
        
        String domainName = [select id,SIQIC__SCWebService_Server__c from Account where id=:loggedInUser.SIQIC__TenantID__c].SIQIC__SCWebService_Server__c ;
        System.debug('domainName :   +'+domainName );
        String URL=domainName + 'CreateAdapter/service';
        String procName='Automate_Adapter_correct(-,-,-,-,-)';
        procName = EncodingUtil.urlEncode(procName, 'UTF-8');
        
        adapterColumns=adapterColumns.replace(' ','REPLACEXX');
        adapterColumns=adapterColumns.replace(';','REPLACEYY');
        mandatoryColumns=mandatoryColumns.replace(' ','REPLACEXX');
        mandatoryColumns=mandatoryColumns.replace(';','REPLACEYY');
        
        String js = EncodingUtil.urlEncode(adapterDatatype ,'UTF-8');
                
        String parameters = b+','+adapterName +','+adapterColumns+','+js+','+mandatoryColumns;
        system.debug(parameters);
        
        //String endPoint1=URL+'1/'+ loggedInUser.SIQIC__Tenant__c+'/'+procName+'/'+parameters;
        String endPoint=URL+'?'+ 'tenant=' + loggedInUser.SIQIC__Tenant__c + '&'+  'tenantID=' + procName + '&'+ 'value=' + parameters ;
        
        System.Debug('EndPoint ===== ' + endPoint);
        
        results=reportUtility.restCallOut(endPoint);
        System.debug('=======================Procedure run succesfully=================================');
        //adapterName='';
        //adapterDatatype='';
        //adapterColumns='';
        //mandatoryColumns='';
        List<Object> json_value= new List<Object>();
        json_value = (List<Object>)results.get('json');
        if(json_value != null){
            Object jsonValResult = json_value[0];
            System.Debug('EndPoint ===== ' + jsonValResult );
            resMessage= jsonValResult.toString();
        }
    }
}