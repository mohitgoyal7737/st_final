public class PBI_WO_Qury_Ctlr
{
    public String pbiFilters {get;set;} 
    public String pbiEmbedDetails {get;set;} 
    public String pbiAccessToken  {get;set;} 
    public String goBackURL  {get;set;} 
    public string preSignedURL1  {get;set;} 
    
    public PBI_WO_Qury_Ctlr()
    {
        Map<String, String> details = DSI_Utility.pbiDetails();
        pbiFilters = details.get('Filters');
        pbiEmbedDetails = details.get('EmbedDetails');
        goBackURL = details.get('goBackLink');
        pbiAccessToken = DSI_Utility.getAccessToken();
    }
    
    public void trackReportInst()
    {
        Id userId = UserInfo.getUserId();
        Id reportId ;
        if(!test.isRunningTest())
            reportId = ApexPages.currentPage().getParameters().get('id');
        else
            reportId = [select id from SIQIC__Reports__c where SIQIC__user__c=:userId limit 1].id;
        SIQIC__Reports__c inst = [select OwnerId,SIQIC__Report_Type__c,SIQIC__Report_Period__c,LastViewedDate from SIQIC__Reports__c where id = :reportId limit 1];
        reportUtility.createReportTrackingRecord(userId,reportId,inst.SIQIC__Report_Type__c,inst.OwnerId,inst.SIQIC__Report_Period__c);       
    }
    
    public PageReference xlDownloadRpt()
    {
        preSignedURL1 = DSI_Utility.xlEndPoint();
        System.debug('URL===== ' + preSignedURL1);   
        return null;
    }
}