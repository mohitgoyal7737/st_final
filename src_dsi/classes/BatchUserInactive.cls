global class BatchUserInactive implements Database.Batchable<sObject>,database.stateful,Schedulable
 {
    public String query;
    public Set<String> prid;
    public Set<String> userprid;
    public string status='Withdrawn';

    global BatchUserInactive() 
    {
        query='SELECT Id,AxtriaSalesIQTM__Employee_ID__c, Employment_Status__c,DSI_Email_Id__c FROM AxtriaSalesIQTM__Employee__c WHERE Employment_Status__c =:status';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        system.debug('===================Query:::'+query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<AxtriaSalesIQTM__Employee__c> scope) 
    {
        system.debug('++query++'+query);
        prid=new Set<String>();
        userprid=new Set<String>();
        for(AxtriaSalesIQTM__Employee__c emp:scope)
        {
            prid.add(emp.DSI_Email_Id__c);
        }
        system.debug('++prid++'+prid);

        List<User> us=[SELECT FederationIdentifier,Id,Country,IsActive FROM User where FederationIdentifier in : prid];
        for(User u:us)
        {
             system.debug('++helloo++');
             u.IsActive=False;
        }
        update us;

    }

    global void finish(Database.BatchableContext BC) 
    {

    }
   global void execute(SchedulableContext sc)
    {
        database.executeBatch(new BatchUserInactive(),2000);       
    }
}