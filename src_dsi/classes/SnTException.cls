public with sharing class SnTException extends Exception {
        public Enum OperationType { C, R, U, D }
        public SnTException (SObjectAccessDecision securityDecision,String clsName, OperationType operation){
             String errorFields = ' Fields : '+securityDecision.getRemovedFields();
        
            if(operation == OperationType.C)
                this.setMessage(System.Label.Security_Error_Object_Not_Insertable);
            else if(operation == OperationType.R)
                this.setMessage(System.Label.Security_Error_Object_Not_Readable);
            else if(operation == OperationType.U)
                this.setMessage(System.Label.Security_Error_Object_Not_Updateable);
            else if(operation == OperationType.D)
                this.setMessage(System.Label.Security_Error_Object_Not_Deletable);

            this.setMessage( 'Failed in '+clsName+', '+String.format( this.getMessage(), new List<String>{errorFields } ));
        }
        
        
}