/**********************************************************************
*
* @author     : A1942
* @date       : June-18-2021
* @description: Custom Exception class
* Revison(s)  : v1.0
* 
*************************************************************************/
public class DSI_Exception extends Exception {

	/*public DSI_Exception(String message){
		//this.setMessage( 'Failed -- '+message);
	}*/

	/**
    * To throw exception with custom error message.
    * 
    * @param className  Name of the class from where exception has been thrown.       
    * @param message    Custom exception message.
    */
	public DSI_Exception(String className, String message){
		this.setMessage( 'Failed  '+className+', '+message);
	}

}