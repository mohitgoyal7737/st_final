global class BatchGeoStagingRefresh implements Database.Batchable<sObject> {
    public String query;

    global BatchGeoStagingRefresh() {
        delete[select id from Geography_Staging__c];
        this.query = 'Select id, AxtriaSalesIQTM__Client_Territory_Code__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Territory_Code__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Territory_Code__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Territory_Code__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Territory_Code__c, Name, AxtriaSalesIQTM__Parent_Position__r.Name, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.Name, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.Name,AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.Name,AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c,  AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c,AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__FirstName__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__FirstName__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__FirstName__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__FirstName__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__FirstName__c,AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Last_Name__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Last_Name__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Last_Name__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Last_Name__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Last_Name__c, AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Email__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Email__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Email__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Email__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Email__c,AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Job_Title__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Job_Title__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Job_Title__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Job_Title__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Job_Title__c, AxtriaSalesIQTM__Employee__r.Last_Hire_Date__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Last_Hire_Date__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Last_Hire_Date__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Last_Hire_Date__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Last_Hire_Date__c,AxtriaSalesIQTM__Employee__r.Assignment_Start_Date__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Assignment_Start_Date__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Assignment_Start_Date__c,  AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Assignment_Start_Date__c,  AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Assignment_Start_Date__c,AxtriaSalesIQTM__Employee__r.Employee_Type__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Employee_Type__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Employee_Type__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Employee_Type__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Employee_Type__c,Geography_Status__c, AxtriaSalesIQTM__Parent_Position__r.Geography_Status__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.Geography_Status__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.Geography_Status__c, AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.Geography_Status__c,AxtriaSalesIQTM__Position_Type__c from AxtriaSalesIQTM__Position__c';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Position__c> posList) {
        List<Geography_Staging__c> gsList = new List<Geography_Staging__c>();
        for(AxtriaSalesIQTM__Position__c pos : posList){
            Geography_Staging__c gs = new Geography_Staging__c();
            gs.GeographyName__c = pos.id;
            // Mapping Client Territory Code
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'Territory'){
                gs.Territory_Code__c = pos.AxtriaSalesIQTM__Client_Territory_Code__c;
                gs.District_Code__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Territory_Code__c;
                gs.Region_Code__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Territory_Code__c;
                gs.Area_Code__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Territory_Code__c;
                gs.Nation_Code__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Territory_Code__c;
                
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'District'){
                gs.District_Code__c = pos.AxtriaSalesIQTM__Client_Territory_Code__c;
                gs.Region_Code__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Territory_Code__c;
                gs.Area_Code__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Territory_Code__c;
                gs.Nation_Code__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Territory_Code__c;
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'Region'){
                gs.Region_Code__c = pos.AxtriaSalesIQTM__Client_Territory_Code__c;
                gs.Area_Code__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Territory_Code__c;
                gs.Nation_Code__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Territory_Code__c;
                
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'Area'){
                gs.Area_Code__c = pos.AxtriaSalesIQTM__Client_Territory_Code__c;
                gs.Nation_Code__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Client_Territory_Code__c;
                
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'National'){
                gs.Nation_Code__c = pos.AxtriaSalesIQTM__Client_Territory_Code__c;
            }
                // Mapping Short Desc
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'Territory'){
                gs.Territory_Short_Desc__c = pos.Name;
                gs.District_Short_Desc__c = pos.AxtriaSalesIQTM__Parent_Position__r.Name;
                gs.Region_Short_Desc__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.Name;
                gs.Area_Short_Desc__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.Name;
                gs.Nation_Short_Desc_Name__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.Name;
                
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'District'){
                gs.District_Short_Desc__c = pos.Name;
                gs.Region_Short_Desc__c = pos.AxtriaSalesIQTM__Parent_Position__r.Name;
                gs.Area_Short_Desc__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.Name;
                gs.Nation_Short_Desc_Name__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.Name;
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'Region'){
                gs.Region_Short_Desc__c = pos.Name;
                gs.Area_Short_Desc__c = pos.AxtriaSalesIQTM__Parent_Position__r.Name;
                gs.Nation_Short_Desc_Name__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.Name;
                
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'Area'){
                gs.Area_Short_Desc__c = pos.Name;
                gs.Nation_Short_Desc_Name__c = pos.AxtriaSalesIQTM__Parent_Position__r.Name;
                
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'National'){
                gs.Nation_Short_Desc_Name__c = pos.Name;
            }
                // Mapping Employee ID
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'Territory'){
                gs.Employee_ID__c = pos.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c;
                gs.District_Emp_ID__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c;
                gs.Region_Emp_ID__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c;
                gs.Area_Emp_ID__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c;
                gs.National_Emp_ID__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c;
                
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'District'){
                gs.District_Emp_ID__c = pos.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c;
                gs.Region_Emp_ID__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c;
                gs.Area_Emp_ID__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c;
                gs.National_Emp_ID__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c;
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'Region'){
                gs.Region_Emp_ID__c = pos.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c;
                gs.Area_Emp_ID__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c;
                gs.National_Emp_ID__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c;
                
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'Area'){
                gs.Area_Emp_ID__c = pos.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c;
                gs.National_Emp_ID__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c;
                
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'National'){
                gs.National_Emp_ID__c = pos.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Employee_ID__c;
            }
                // Mapping First AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__FirstName__c
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'Territory'){
                gs.Employee_First_Name__c = pos.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__FirstName__c;
                gs.District_Emp_First_Name__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__FirstName__c;
                gs.Region_Emp_First_Name__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__FirstName__c;
                gs.Area_Emp_First_Name__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__FirstName__c;
                gs.National_Emp_First_Name__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__FirstName__c;
                
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'District'){
                gs.District_Emp_First_Name__c = pos.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__FirstName__c;
                gs.Region_Emp_First_Name__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__FirstName__c;
                gs.Area_Emp_First_Name__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__FirstName__c;
                gs.National_Emp_First_Name__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__FirstName__c;
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'Region'){
                gs.Region_Emp_First_Name__c = pos.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__FirstName__c;
                gs.Area_Emp_First_Name__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__FirstName__c;
                gs.National_Emp_First_Name__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__FirstName__c;
                
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'Area'){
                gs.Area_Emp_First_Name__c = pos.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__FirstName__c;
                gs.National_Emp_First_Name__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__FirstName__c;
                
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'National'){
                gs.National_Emp_First_Name__c = pos.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__FirstName__c;
            }
                // Mapping First AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Last_Name__c
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'Territory'){
                gs.Employee_Last_Name__c = pos.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Last_Name__c;
                gs.District_Emp_Last_Name__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Last_Name__c;
                gs.Region_Emp_Last_Name__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Last_Name__c;
                gs.Area_Emp_Last_Name__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Last_Name__c;
                gs.National_Emp_Last_Name__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Last_Name__c;
                
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'District'){
                gs.District_Emp_Last_Name__c = pos.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Last_Name__c;
                gs.Region_Emp_Last_Name__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Last_Name__c;
                gs.Area_Emp_Last_Name__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Last_Name__c;
                gs.National_Emp_Last_Name__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Last_Name__c;
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'Region'){
                gs.Region_Emp_Last_Name__c = pos.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Last_Name__c;
                gs.Area_Emp_Last_Name__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Last_Name__c;
                gs.National_Emp_Last_Name__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Last_Name__c;
                
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'Area'){
                gs.Area_Emp_Last_Name__c = pos.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Last_Name__c;
                gs.National_Emp_Last_Name__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Last_Name__c;
                
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'National'){
                gs.National_Emp_Last_Name__c = pos.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Last_Name__c;
            }
                // Mapping First AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Email__c
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'Territory'){
                gs.Emp_Business_Email_Address__c = pos.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Email__c;
                gs.District_Emp_Business_Email_Address__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Email__c;
                gs.Region_Emp_Business_Mail_Address__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Email__c;
                gs.Area_Business_Email_Address__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Email__c;
                gs.National_Emp_Business_Email_Address__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Email__c;
                
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'District'){
                gs.District_Emp_Business_Email_Address__c = pos.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Email__c;
                gs.Region_Emp_Business_Mail_Address__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Email__c;
                gs.Area_Business_Email_Address__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Email__c;
                gs.National_Emp_Business_Email_Address__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Email__c;
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'Region'){
                gs.Region_Emp_Business_Mail_Address__c = pos.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Email__c;
                gs.Area_Business_Email_Address__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Email__c;
                gs.National_Emp_Business_Email_Address__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Email__c;
                
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'Area'){
                gs.Area_Business_Email_Address__c = pos.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Email__c;
                gs.National_Emp_Business_Email_Address__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Email__c;
                
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'National'){
                gs.National_Emp_Business_Email_Address__c = pos.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Email__c;
            }
                // Mapping First AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Job_Title__c
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'Territory'){
                gs.Employee_Job_Title__c = pos.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Job_Title__c;
                gs.District_Emp_Job_Title__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Job_Title__c;
                gs.Region_Emp_Job_Title__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Job_Title__c;
                gs.Area_Employee_Job_Title__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Job_Title__c;
                gs.National_Emp_Job_Title__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Job_Title__c;
                
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'District'){
                gs.District_Emp_Job_Title__c = pos.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Job_Title__c;
                gs.Region_Emp_Job_Title__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Job_Title__c;
                gs.Area_Employee_Job_Title__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Job_Title__c;
                gs.National_Emp_Job_Title__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Job_Title__c;
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'Region'){
                gs.Region_Emp_Job_Title__c = pos.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Job_Title__c;
                gs.Area_Employee_Job_Title__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Job_Title__c;
                gs.National_Emp_Job_Title__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Job_Title__c;
                
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'Area'){
                gs.Area_Employee_Job_Title__c = pos.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Job_Title__c;
                gs.National_Emp_Job_Title__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Job_Title__c;
                
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'National'){
                gs.National_Emp_Job_Title__c = pos.AxtriaSalesIQTM__Employee__r.AxtriaSalesIQTM__Job_Title__c;
            }
                // Mapping First AxtriaSalesIQTM__Employee__r.Last_Hire_Date__c
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'Territory'){
                gs.Rep_Latest_Hire_Date__c = pos.AxtriaSalesIQTM__Employee__r.Last_Hire_Date__c;
                gs.DM_Latest_Hire_Date__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Last_Hire_Date__c;
                gs.RM_Latest_Hire_Date__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Last_Hire_Date__c;
                gs.ABM_Latest_Hire_Date__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Last_Hire_Date__c;
                gs.National_Latest_Hire_Date__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Last_Hire_Date__c;
                
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'District'){
                gs.DM_Latest_Hire_Date__c = pos.AxtriaSalesIQTM__Employee__r.Last_Hire_Date__c;
                gs.RM_Latest_Hire_Date__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Last_Hire_Date__c;
                gs.ABM_Latest_Hire_Date__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Last_Hire_Date__c;
                gs.National_Latest_Hire_Date__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Last_Hire_Date__c;
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'Region'){
                gs.RM_Latest_Hire_Date__c = pos.AxtriaSalesIQTM__Employee__r.Last_Hire_Date__c;
                gs.ABM_Latest_Hire_Date__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Last_Hire_Date__c;
                gs.National_Latest_Hire_Date__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Last_Hire_Date__c;
                
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'Area'){
                gs.ABM_Latest_Hire_Date__c = pos.AxtriaSalesIQTM__Employee__r.Last_Hire_Date__c;
                gs.National_Latest_Hire_Date__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Last_Hire_Date__c;
                
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'National'){
                gs.National_Latest_Hire_Date__c = pos.AxtriaSalesIQTM__Employee__r.Last_Hire_Date__c;
            }
                // Mapping First AxtriaSalesIQTM__Employee__r.Assignment_Start_Date__c
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'Territory'){
                gs.Rep_Assignment_Start_Date__c = pos.AxtriaSalesIQTM__Employee__r.Assignment_Start_Date__c;
                gs.DM_Emp_Assignment_Start_Date__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Assignment_Start_Date__c;
                gs.RM_Emp_Assignment_Start_Date__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Assignment_Start_Date__c;
                gs.Area_Emp_Assignment_Start_Date__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Assignment_Start_Date__c;
                gs.National_Emp_Assignment_Start_Date__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Assignment_Start_Date__c;
                
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'District'){
                gs.DM_Emp_Assignment_Start_Date__c = pos.AxtriaSalesIQTM__Employee__r.Assignment_Start_Date__c;
                gs.RM_Emp_Assignment_Start_Date__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Assignment_Start_Date__c;
                gs.Area_Emp_Assignment_Start_Date__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Assignment_Start_Date__c;
                gs.National_Emp_Assignment_Start_Date__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Assignment_Start_Date__c;
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'Region'){
                gs.RM_Emp_Assignment_Start_Date__c = pos.AxtriaSalesIQTM__Employee__r.Assignment_Start_Date__c;
                gs.Area_Emp_Assignment_Start_Date__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Assignment_Start_Date__c;
                gs.National_Emp_Assignment_Start_Date__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Assignment_Start_Date__c;
                
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'Area'){
                gs.Area_Emp_Assignment_Start_Date__c = pos.AxtriaSalesIQTM__Employee__r.Assignment_Start_Date__c;
                gs.National_Emp_Assignment_Start_Date__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Assignment_Start_Date__c;
                
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'National'){
                gs.National_Emp_Assignment_Start_Date__c = pos.AxtriaSalesIQTM__Employee__r.Assignment_Start_Date__c;
            }
                // Mapping First AxtriaSalesIQTM__Employee__r.Employee_Type__c
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'Territory'){
                gs.Employee_Status__c = pos.AxtriaSalesIQTM__Employee__r.Employee_Type__c;
                gs.DM_Employee_Status__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Employee_Type__c;
                gs.RM_Employee_Status__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Employee_Type__c;
                gs.Area_Employee_Status__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Employee_Type__c;
                gs.National_Employee_Status__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Employee_Type__c;
                
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'District'){
                gs.DM_Employee_Status__c = pos.AxtriaSalesIQTM__Employee__r.Employee_Type__c;
                gs.RM_Employee_Status__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Employee_Type__c;
                gs.Area_Employee_Status__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Employee_Type__c;
                gs.National_Employee_Status__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Employee_Type__c;
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'Region'){
                gs.RM_Employee_Status__c = pos.AxtriaSalesIQTM__Employee__r.Employee_Type__c;
                gs.Area_Employee_Status__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Employee_Type__c;
                gs.National_Employee_Status__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Employee_Type__c;
                
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'Area'){
                gs.Area_Employee_Status__c = pos.AxtriaSalesIQTM__Employee__r.Employee_Type__c;
                gs.National_Employee_Status__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Employee__r.Employee_Type__c;
                
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'National'){
                gs.National_Employee_Status__c = pos.AxtriaSalesIQTM__Employee__r.Employee_Type__c;
            }
                // Mapping First Geography_Status__c
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'Territory'){
                gs.Terr_Geo_Status__c = pos.Geography_Status__c;
                gs.DM_Geo_Status__c = pos.AxtriaSalesIQTM__Parent_Position__r.Geography_Status__c;
                gs.RM_Geo_Status__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.Geography_Status__c;
                gs.Area_Geo_Status__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.Geography_Status__c;
                gs.National_Geo_Status__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.Geography_Status__c;
                
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'District'){
                gs.DM_Geo_Status__c = pos.Geography_Status__c;
                gs.RM_Geo_Status__c = pos.AxtriaSalesIQTM__Parent_Position__r.Geography_Status__c;
                gs.Area_Geo_Status__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.Geography_Status__c;
                gs.National_Geo_Status__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.Geography_Status__c;
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'Region'){
                gs.RM_Geo_Status__c = pos.Geography_Status__c;
                gs.Area_Geo_Status__c = pos.AxtriaSalesIQTM__Parent_Position__r.Geography_Status__c;
                gs.National_Geo_Status__c = pos.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.Geography_Status__c;
                
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'Area'){
                gs.Area_Geo_Status__c = pos.Geography_Status__c;
                gs.National_Geo_Status__c = pos.AxtriaSalesIQTM__Parent_Position__r.Geography_Status__c;
                
            }
            if(pos.AxtriaSalesIQTM__Position_Type__c == 'National'){
                gs.National_Geo_Status__c = pos.Geography_Status__c;
            }
            gsList.add(gs);
        }
        
        insert gsList;
        System.debug('@@@ gslist.size() = '+gslist.size() );
    }

    global void finish(Database.BatchableContext BC) {

    }
}