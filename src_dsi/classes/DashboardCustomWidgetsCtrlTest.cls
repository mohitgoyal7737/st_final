/*
@author     : A1942
@date       : June-10-2021
@description: Test class for DashboardCustomWidgetsCtrl.cls
Revison(s)  : v1.0
*/

@isTest (SeeAllData=true)
private class DashboardCustomWidgetsCtrlTest {
		static testMethod void testMethod1() {
		Profile pid1=[Select Id from Profile where Name='HO' Limit 1];  
        Profile pid2=[Select Id from Profile where Name='Rep' Limit 1];
        User u1 = [Select id from user where ProfileID =:pid1.id Limit 1];
        /*new User(Username='testusery1@axtria.com', LastName='test1', Email='testuse1r@axtria.com', Alias='test1', CommunityNickname='test1',TimeZoneSidKey='America/Los_Angeles', LocaleSidKey='en_US',EmailEncodingKey='UTF-8',ProfileID=pid1.id, LanguageLocaleKey='en_US',SIQIC__Tenant__c='DSI',SIQIC__TenantID__c='0015e000004fcvsAAA',IsActive=true);
        insert u1;*/
        User u2 = new User(Username='testusery2@axtria.com', LastName='test2', Email='testuser2@axtria.com', Alias='test2', CommunityNickname='test2',
                    TimeZoneSidKey='America/Los_Angeles', LocaleSidKey='en_US', 
                    EmailEncodingKey='UTF-8',ProfileID=pid2.id, LanguageLocaleKey='en_US');
        insert u2;

        SIQIC__Reports__c r1 = new SIQIC__Reports__c();
        r1.SIQIC__Report_Definition__c = [select id from SIQIC__Report_Defination__c limit 1].id; 
        r1.SIQIC__Role__c ='HO';
        r1.SIQIC__User__c = u1.id;
        r1.SIQIC__Report_Type__c = 'Dashboard';
        r1.SIQIC__Latest__c =true;
        r1.Dashboard_Enabled__c = true;
        insert r1;
         SIQIC__Reports__c r2 = new SIQIC__Reports__c();
        r2.SIQIC__Report_Definition__c =  [select id from SIQIC__Report_Defination__c limit 1].id;
                r2.SIQIC__Role__c ='HO';
                r2.SIQIC__User__c = UserInfo.getUserId();
                r2.SIQIC__Report_Type__c = 'Dashboard';
                r2.SIQIC__Latest__c =true;
                r2.SIQIC__EmpId__c='10000070';
        r2.Dashboard_Enabled__c = true;
                insert r2;
         AxtriaSalesIQTM__Employee__c e1 =    new AxtriaSalesIQTM__Employee__c(Name='Emp1',AxtriaSalesIQTM__FirstName__c='Emp',AxtriaSalesIQTM__Last_Name__c='1',Action_Start_Date__c=Date.today());    
         insert e1;
         AxtriaSalesIQTM__Employee__c e2 =    new AxtriaSalesIQTM__Employee__c(Name='Emp2',AxtriaSalesIQTM__FirstName__c='Emp',AxtriaSalesIQTM__Last_Name__c='2',Action_Start_Date__c=Date.today());    
         insert e2;
         AxtriaSalesIQTM__Employee__c e3 =    new AxtriaSalesIQTM__Employee__c(Name='Emp3',AxtriaSalesIQTM__FirstName__c='Emp',AxtriaSalesIQTM__Last_Name__c='3',Action_Start_Date__c=Date.today());    
         insert e3;
         AxtriaSalesIQTM__Employee__c e4 =    new AxtriaSalesIQTM__Employee__c(Name='Emp4',AxtriaSalesIQTM__FirstName__c='Emp',AxtriaSalesIQTM__Last_Name__c='4',Action_Start_Date__c=Date.today());    
         insert e4;
         AxtriaSalesIQST__CR_Employee_Feed__c feed1 = new AxtriaSalesIQST__CR_Employee_Feed__c();
         feed1.AxtriaSalesIQST__Event_Name__c = 'New Hire';
         feed1.AxtriaSalesIQST__Employee__c= e1.id;
         //feed1.Event_Start_Date__c = Date.today();
         insert feed1;
          AxtriaSalesIQST__CR_Employee_Feed__c feed2 = new AxtriaSalesIQST__CR_Employee_Feed__c();
         feed2.AxtriaSalesIQST__Event_Name__c = 'Transfer';
         feed2.AxtriaSalesIQST__Employee__c= e2.id;
        // feed2.Event_Start_Date__c = Date.today();
         insert feed2;
          AxtriaSalesIQST__CR_Employee_Feed__c feed3 = new AxtriaSalesIQST__CR_Employee_Feed__c();
         feed3.AxtriaSalesIQST__Event_Name__c = 'Promotion';
         feed3.AxtriaSalesIQST__Employee__c= e3.id;
        // feed3.Event_Start_Date__c = Date.today();
         insert feed3;
          AxtriaSalesIQST__CR_Employee_Feed__c feed4 = new AxtriaSalesIQST__CR_Employee_Feed__c();
         feed4.AxtriaSalesIQST__Event_Name__c = 'Termination';
         feed4.AxtriaSalesIQST__Employee__c= e4.id;
        // feed4.Event_Start_Date__c = Date.today();
         insert feed4;
         	DashboardCustomWidgetsCtrl d = new DashboardCustomWidgetsCtrl();
         	d.workbenchEvent();
         	DSI_Utility.getProcResponse(2,'' , 'a335e000000HwNCAA0;10000070');
         	DSI_Utility du = new DSI_Utility();
         	//du.getProcResponse('', 'getDashboardWidgetData', 'a335e000000HwNCAA0;10000070');
         	
       /* system.runAs(u1)
		{ DashboardCustomWidgetsCtrl d = new DashboardCustomWidgetsCtrl(); }*/
	}
}