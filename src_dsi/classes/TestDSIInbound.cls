@isTest
private class TestDSIInbound {
public static String CRON_EXP = '0 0 0 28 2 ? 2022';
    static testMethod void validate_DSIInbound(){
    
    AxtriaSalesIQTM__SalesIQ_Logger__c DS = New AxtriaSalesIQTM__SalesIQ_Logger__c();
    DS.AxtriaSalesIQTM__Status__c = 'In Progress';
    DS.AxtriaSalesIQTM__Module__c = 'InboundJobs';
    DS.AxtriaSalesIQTM__Type__c = 'InboundJobs';
    Insert DS;
    
   
    AxtriaSalesIQTM__BRMS_Config__mdt BRMS_CS = new AxtriaSalesIQTM__BRMS_Config__mdt();
    BRMS_CS.MasterLabel = 'InboundJobs';
    BRMS_CS.NamespacePrefix = 'lll';
    BRMS_CS.AxtriaSalesIQTM__BRMS_Value__c = 'lllll';
    
    AxtriaSalesIQTM__ETL_Config__c  ETL = New AxtriaSalesIQTM__ETL_Config__c();
    ETL.Name = 'InboundJobs';
    ETL.AxtriaSalesIQTM__SF_UserName__c = 'Salesiq@123.com';
    ETL.AxtriaSalesIQTM__SF_Password__c = 'Salesiq@123.com';
    ETL.AxtriaSalesIQTM__S3_Security_Token__c= 'Salesiq@123.com';
    ETL.AxtriaSalesIQTM__BDT_DataSet_Id__c = 'EUfull';
    ETL.AxtriaSalesIQTM__BR_PG_Database__c= 'EU_IB_OB';
    ETL.AxtriaSalesIQTM__BR_PG_Host__c= 'localhost';
    Insert ETL;    
    
        
   DSIInbound.getBRMSConfigValues('InboundJobs');
   DSIInbound.getETLConfigByName('InboundJobs');
   DSIInbound.isSandboxOrg(); 
   Test.StartTest();

    String jobId = System.schedule('TestDSIInbound',CRON_EXP,new DSIInbound());

Test.StopTest(); 
    }
}