public class Home_Page_Message_Ctrl {

    public List<AxtriaSalesIQTM__User_Access_Permission__c> loggedInUserData  	{get;set;}
    public Integer listSize                                  					{get;set;}
    public string userType														{get;set;}
    public string team_Instance_Name                                            {get;set;}

    public Home_Page_Message_Ctrl() 
    {
        loggedInUserData     = new List<AxtriaSalesIQTM__User_Access_Permission__c>();
        listSize 			= 0; 
        userType ='';
        loggedInUserData = SalesIQUtility.getUserAccessPermistion(Userinfo.getUserId());
        System.debug('--Umang--'+loggedInUserData[0].AxtriaSalesIQTM__Team_Instance__r.Name);
        team_Instance_Name = loggedInUserData[0].AxtriaSalesIQTM__Team_Instance__r.Name;
        if(loggedInUserData!=null && loggedInUserData.size()>0)
        {   
            loggedInUsercheck(loggedInUserData[0]);
        }
    }

     public void loggedInUsercheck(AxtriaSalesIQTM__User_Access_Permission__c userpos)
     {
     	if(userpos.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c == 'Rep' || userpos.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c == 'Territory')
        {   
            userType             = 'Territory';
        }
        else if(userpos.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c == 'District')
        {
            userType = 'District';
        }
        else if(userpos.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c == 'Region')
        {
            userType = 'Region';
        }
        else if(userpos.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c == 'Nation')
        {
            userType = 'Nation';
        }
    }
}