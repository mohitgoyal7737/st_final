@isTest
public class BatchCallPlanDownload_Test 
{
    @istest static void BatchCallPlanDownload_Test()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactorySNT.createAccount();
        acc.AxtriaSalesIQTM__AccountType__c ='HCP';
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactorySNT.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactorySNT.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactorySNT.createTeam(countr);
        team.Name = 'ONCO';
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactorySNT.createTeamInstance(team);
        insert teamins;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactorySNT.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactorySNT.newcreateScenario(teamins, team, workspace);
        insert scen;
        AxtriaSalesIQST__Product_Catalog__c pcc = TestDataFactorySNT.productCatalog(team, teamins, countr);
        insert pcc;
        AxtriaSalesIQTM__Team_Instance_Object_Attribute__c teaminstobjatt = TestdataFactorySNT.createTeamInstanceObjectAttribute(teamins,pcc);
        teaminstobjatt.AxtriaSalesIQTM__isEnabled__c = true ;
        teaminstobjatt.AxtriaSalesIQTM__isRequired__c = true ;
        teaminstobjatt.AxtriaSalesIQTM__Interface_Name__c = 'Call Plan' ;
        teaminstobjatt.AxtriaSalesIQST__cust_Type__c = 'HCP';
        teaminstobjatt.AxtriaSalesIQTM__Attribute_API_Name__c = 'AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Territory_Code__c';
        teaminstobjatt.AxtriaSalesIQTM__Attribute_Display_Name__c = 'PositionCode';
        insert teaminstobjatt;
        AxtriaSalesIQTM__Team_Instance_Object_Attribute__c teamInstanceObjectAttribute = new AxtriaSalesIQTM__Team_Instance_Object_Attribute__c();
        teamInstanceObjectAttribute.CurrencyIsoCode = 'USD';
        teamInstanceObjectAttribute.AxtriaSalesIQTM__Attribute_API_Name__c = 'AxtriaSalesIQTM__Position_Team_Instance__r.AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__Client_Territory_Code__c';
        teamInstanceObjectAttribute.AxtriaSalesIQTM__Attribute_Display_Name__c = 'PositionCode';
        teamInstanceObjectAttribute.AxtriaSalesIQTM__WrapperFieldMap__c = 'Test';
        teamInstanceObjectAttribute.AxtriaSalesIQTM__isEnabled__c = true;
        teamInstanceObjectAttribute.AxtriaSalesIQTM__isRequired__c = true;
        teamInstanceObjectAttribute.AxtriaSalesIQTM__Interface_Name__c = 'Call Plan';
        teamInstanceObjectAttribute.AxtriaSalesIQTM__Object_Name__c = 'Test';
        teamInstanceObjectAttribute.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        teamInstanceObjectAttribute.AxtriaSalesIQST__Brand_Lookup__c = pcc.id;

        AxtriaSalesIQST__Measure_Master__c mmc = TestDataFactorySNT.createMeasureMaster(pcc, team, teamins);
        mmc.AxtriaSalesIQST__Team_Instance__c = teamins.id;
        insert mmc;
        AxtriaSalesIQTM__Position__c pos= TestDataFactorySNT.createPosition(team,teamins);
        insert pos;
        AxtriaSalesIQTM__User_Access_Permission__c u = TestDataFactorySNT.createUserAccessPerm(pos, teamins, UserInfo.getUserId());
        u.AxtriaSalesIQTM__Position__c=pos.id;
        u.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        u.AxtriaSalesIQTM__User__c = loggedInUser.id;        
        insert u;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactorySNT.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        AxtriaSalesIQST__Product_Priority__c pPriority = TestDataFactorySNT.productPriority();
        insert pPriority;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactorySNT.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        positionAccountCallPlan.AxtriaSalesIQTM__isIncludedCallPlan__c = true;
        positionAccountCallPlan.AxtriaSalesIQST__Share__c = true;
        insert positionAccountCallPlan;
        
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            //commented by gurpreet
            //System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(AxtriaSalesIQST__Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            BatchCallPlanDownload obj = new BatchCallPlanDownload(pos.Id,',','HCP');
            String paramApi = teaminstobjatt.AxtriaSalesIQTM__Attribute_API_Name__c;
            obj.query =  'select '+ paramApi +'  FROM AxtriaSalesIQTM__Position_Account_Call_Plan__c';
            Database.executeBatch(obj);
        }
        Test.stopTest();
    }

    @istest static void BatchCallPlanDownload_Test2()
    {
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        Account acc= TestDataFactorySNT.createAccount();
        acc.AxtriaSalesIQTM__AccountType__c ='HCP';
        insert acc;
        AxtriaSalesIQTM__Organization_Master__c orgmas = TestDataFactorySNT.createOrganizationMaster();
        insert orgmas;
        AxtriaSalesIQTM__Country__c countr = TestDataFactorySNT.createCountry(orgmas);
        insert countr;
        AxtriaSalesIQTM__Team__c team = TestDataFactorySNT.createTeam(countr);
        team.Name = 'ONCO';
        insert team;
        AxtriaSalesIQTM__Team_Instance__c teamins = TestDataFactorySNT.createTeamInstance(team);
        insert teamins;
        
        AxtriaSalesIQTM__Workspace__c workspace = TestDataFactorySNT.createWorkspace('HCO', date.today(), date.today() + 1);
        workspace.AxtriaSalesIQTM__Country__c = countr.id;
        insert workspace;
        
        
        AxtriaSalesIQTM__Scenario__c scen = TestDataFactorySNT.newcreateScenario(teamins, team, workspace);
        insert scen;
        AxtriaSalesIQST__Product_Catalog__c pcc = TestDataFactorySNT.productCatalog(team, teamins, countr);
        insert pcc;
        AxtriaSalesIQTM__Team_Instance_Object_Attribute__c teaminstobjatt = TestDataFactorySNT.createTeamInstanceObjectAttribute(teamins,pcc);
        teaminstobjatt.AxtriaSalesIQTM__isEnabled__c = true ;
        teaminstobjatt.AxtriaSalesIQTM__isRequired__c = true ;
        teaminstobjatt.AxtriaSalesIQTM__Interface_Name__c = 'Call Plan' ;
        teaminstobjatt.AxtriaSalesIQTM__Attribute_API_Name__c = 'AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Territory_Code__c';
        teaminstobjatt.AxtriaSalesIQTM__Attribute_Display_Name__c = 'PositionCode';
        insert teaminstobjatt;
        AxtriaSalesIQST__Measure_Master__c mmc = TestDataFactorySNT.createMeasureMaster(pcc, team, teamins);
        mmc.AxtriaSalesIQST__Team_Instance__c = teamins.id;
        insert mmc;
        AxtriaSalesIQTM__Position__c pos= TestDataFactorySNT.createPosition(team,teamins);
        insert pos;
        AxtriaSalesIQTM__User_Access_Permission__c u = TestDataFactorySNT.createUserAccessPerm(pos, teamins, UserInfo.getUserId());
        u.AxtriaSalesIQTM__Position__c=pos.id;
        u.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        u.AxtriaSalesIQTM__User__c = loggedInUser.id;        
        insert u;
        
        AxtriaSalesIQTM__Position_Account__c posAccount = TestDataFactorySNT.createPositionAccount(acc,pos,teamins);
        insert posAccount;
        
        AxtriaSalesIQST__Product_Priority__c pPriority = TestDataFactorySNT.productPriority();
        insert pPriority;
        
        AxtriaSalesIQTM__Position_Account_Call_Plan__c positionAccountCallPlan = TestDataFactorySNT.createPositionAccountCallPlan(mmc,acc,teamins,posAccount,pPriority,pos);
        positionAccountCallPlan.AxtriaSalesIQTM__isIncludedCallPlan__c = true;
        positionAccountCallPlan.AxtriaSalesIQST__Share__c = true;
        insert positionAccountCallPlan;
        
        Test.startTest();
        System.runAs(loggedInUser){
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ActiveFlagCheckHelperclass_Test'];
            String nameSpace = cs.NamespacePrefix == null ? '' : cs.NamespacePrefix + '__';
            List<String> RULEPARAMETER_READ_FIELD = new List<String>{nameSpace+'Parameter__c'};
            //commented by gurpreet
            //System.assertEquals(true,SnT_FLS_SecurityUtil.checkRead(AxtriaSalesIQST__Rule_Parameter__c.SObjectType, RULEPARAMETER_READ_FIELD, false));
            BatchCallPlanDownload obj = new BatchCallPlanDownload(pos.Id,',');
            String paramApi = teaminstobjatt.AxtriaSalesIQTM__Attribute_API_Name__c;
            obj.query =  'select '+ paramApi +'  FROM AxtriaSalesIQTM__Position_Account_Call_Plan__c';
            Database.executeBatch(obj);
        }
        Test.stopTest();
    }
}