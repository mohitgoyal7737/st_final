public class PBI_With_Qury_Ctlr
{
    public String pbiFilters {get;set;} 
    public String pbiEmbedDetails {get;set;} 
    public String pbiAccessToken  {get;set;} 
    public String pbiWebQuryRsult {get;set;} 
    public string preSignedURL1  {get;set;} 
    public String goBackURL  {get;set;}
    Public String filterValues;
    
    public PBI_With_Qury_Ctlr()
    {
        Map<String, String> details = DSI_Utility.pbiDetails();
        pbiFilters = details.get('Filters');
        pbiEmbedDetails = details.get('EmbedDetails');
        goBackURL = details.get('goBackLink');
        pbiAccessToken = DSI_Utility.getAccessToken();
        
        runWebQuery();
    }
    
    public void runWebQuery()
    {
        Integer dbServer = 2; 
        String procName = 'getWebQuery_Result';
        
        Map<String, List<Map<String, String>>> procResult = new Map<String, List<Map<String, String>>>();
        
        String filterValues = EncodingUtil.URLENCODE(pbiFilters,'UTF-8');
        System.debug('After Encode Filter Paramters : ' + filterValues );
        
        Map<String, List<Map<String, String>>> m = DSI_Utility.getProcResponse(dbServer, procName, filterValues);        
        List<Map<String, String>> recordsMap  = m.get('DS_0');

        pbiWebQuryRsult = recordsMap[0].get('Param0');
        System.debug('Filter0 Values pbiWebQuryRsult : ' + pbiWebQuryRsult );
                            
        recordsMap  = m.get('DS_1');
        pbiWebQuryRsult = pbiWebQuryRsult + ';' + recordsMap[0].get('Param1');
        System.debug('Filter1 Values pbiWebQuryRsult : ' + pbiWebQuryRsult );
        
        recordsMap  = m.get('DS_2');
        pbiWebQuryRsult = pbiWebQuryRsult + ';' + recordsMap[0].get('Param2');
        System.debug('Filter2 Values pbiWebQuryRsult : ' + pbiWebQuryRsult );
        
        recordsMap  = m.get('DS_3');
        pbiWebQuryRsult = pbiWebQuryRsult + ';' + recordsMap[0].get('Param3');
        System.debug('Filter3 Values pbiWebQuryRsult : ' + pbiWebQuryRsult );
        
        recordsMap  = m.get('DS_4');
        pbiWebQuryRsult = pbiWebQuryRsult + ';' + recordsMap[0].get('Param4');
        System.debug('Filter4 Values pbiWebQuryRsult : ' + pbiWebQuryRsult );
    }
        
    public void trackReportInst()
    {
        Id userId = UserInfo.getUserId();
        Id reportId ;
        if(!test.isRunningTest())
            reportId = ApexPages.currentPage().getParameters().get('id');
        else
            reportId = [select id from SIQIC__Reports__c where SIQIC__user__c=:userId limit 1].id;
        SIQIC__Reports__c inst = [select OwnerId,SIQIC__Report_Type__c,SIQIC__Report_Period__c,LastViewedDate from SIQIC__Reports__c where id = :reportId limit 1];
        reportUtility.createReportTrackingRecord(userId,reportId,inst.SIQIC__Report_Type__c,inst.OwnerId,inst.SIQIC__Report_Period__c);       
    }
    
    public PageReference xlDownloadRpt()
    {
        preSignedURL1 = DSI_Utility.xlEndPoint();
        System.debug('URL===== ' + preSignedURL1);
        
        return null;
    }
}