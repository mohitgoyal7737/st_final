global class BatchICDataProcessing implements Database.Batchable<sObject> {
    public String query;

    global BatchICDataProcessing() {
        delete[select id from IC_ELIGIBILITY__c];
        delete[select id from cytd_contest__c];
        delete[select id from WTD_Contest__c];
        delete[select id from Term_List__c];
        delete[select id from Roster__c];
        query = 'SELECT Id, OwnerId, IsDeleted, Name, CurrencyIsoCode, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, LastViewedDate, LastReferencedDate, AxtriaSalesIQTM__Achievement_Index__c, AxtriaSalesIQTM__AddressLatLong__Latitude__s, AxtriaSalesIQTM__AddressLatLong__Longitude__s, AxtriaSalesIQTM__AddressLatLong__c, AxtriaSalesIQTM__Allocation_End_Date__c, AxtriaSalesIQTM__Allocation_Start_Date__c, AxtriaSalesIQTM__Cellphone_Number__c, AxtriaSalesIQTM__Change_status__c, AxtriaSalesIQTM__City__c, AxtriaSalesIQTM__Company__c, AxtriaSalesIQTM__Cost_Center__c, AxtriaSalesIQTM__Country_Name__c, AxtriaSalesIQTM__Country__c, AxtriaSalesIQTM__Current_Territory__c, AxtriaSalesIQTM__Division__c, AxtriaSalesIQTM__Email__c, AxtriaSalesIQTM__Emp_Supervisor_ID__c, AxtriaSalesIQTM__Employee_ID__c, AxtriaSalesIQTM__Employee_Type__c, AxtriaSalesIQTM__Experience_Company__c, AxtriaSalesIQTM__Experience_Industry__c, AxtriaSalesIQTM__Experience_Therapeutic_Area__c, AxtriaSalesIQTM__Fax_Number__c, AxtriaSalesIQTM__Field_Force__c, AxtriaSalesIQTM__Field_Status__c, AxtriaSalesIQTM__Field_Termination_Date__c, AxtriaSalesIQTM__FirstName__c, AxtriaSalesIQTM__Gender__c, AxtriaSalesIQTM__HR_Status__c, AxtriaSalesIQTM__HR_Termination_Date__c, AxtriaSalesIQTM__Job_Title_Code__c, AxtriaSalesIQTM__Job_Title__c, AxtriaSalesIQTM__Joining_Date__c, AxtriaSalesIQTM__Last_Name__c, AxtriaSalesIQTM__Manager__c, AxtriaSalesIQTM__Middle_Name__c, AxtriaSalesIQTM__Nick_Name__c, AxtriaSalesIQTM__Office_Number__c, AxtriaSalesIQTM__Original_Hire_Date__c, AxtriaSalesIQTM__Postal_code__c, AxtriaSalesIQTM__Previous_Name__c, AxtriaSalesIQTM__Rehire_Date__c, AxtriaSalesIQTM__Role__c, AxtriaSalesIQTM__SalesforceUserName__c, AxtriaSalesIQTM__Shirt_Size__c, AxtriaSalesIQTM__Street__c, AxtriaSalesIQTM__Title__c, AxtriaSalesIQTM__USER__c, AxtriaSalesIQTM__Voice_Number__c, AxtriaSalesIQTM__isSalesforceUser__c, AxtriaSalesIQST__AddressCity__c, AxtriaSalesIQST__AddressCountry__c, AxtriaSalesIQST__AddressLine1__c, AxtriaSalesIQST__AddressLine2__c, AxtriaSalesIQST__AddressPostalCode__c, AxtriaSalesIQST__AddressStateCode__c, AxtriaSalesIQST__Alias__c, AxtriaSalesIQST__Allocation_End_Date__c, AxtriaSalesIQST__Allocation_Start_Date__c, AxtriaSalesIQST__AssignmentStatusValue__c, AxtriaSalesIQST__AssociateOID__c, AxtriaSalesIQST__CurrentPositionId__c, AxtriaSalesIQST__Current_Position_Formula__c, AxtriaSalesIQST__Current_Position__c, AxtriaSalesIQST__Current_Territory1__c, AxtriaSalesIQST__Department_Code__c, AxtriaSalesIQST__Department__c, AxtriaSalesIQST__Dept_Change_Eff_Date__c, AxtriaSalesIQST__Employee_ID_AZ__c, AxtriaSalesIQST__Employee_ID__c, AxtriaSalesIQST__Employee_Name__c, AxtriaSalesIQST__Employee_PRID__c, AxtriaSalesIQST__Employee_Role_MBO__c, AxtriaSalesIQST__Employee_Status__c, AxtriaSalesIQST__Employment_Status__c, AxtriaSalesIQST__FamilyName__c, AxtriaSalesIQST__Field_Status_Formula__c, AxtriaSalesIQST__GivenName__c, AxtriaSalesIQST__Group_Name__c, AxtriaSalesIQST__HR_Status_Formula__c, AxtriaSalesIQST__Hire_Date__c, AxtriaSalesIQST__HomePhone__c, AxtriaSalesIQST__IsWorkbench__c, AxtriaSalesIQST__Is_Field_Employee__c, AxtriaSalesIQST__JobChangeReason__c, AxtriaSalesIQST__JobCodeName__c, AxtriaSalesIQST__JobCode__c, AxtriaSalesIQST__Job_Change_Eff_Date__c, AxtriaSalesIQST__Job_Title__c, AxtriaSalesIQST__LastFirstName__c, AxtriaSalesIQST__Leave_End_Date__c, AxtriaSalesIQST__LegalAddressCityName__c, AxtriaSalesIQST__LegalAddressCountryCode__c, AxtriaSalesIQST__LegalAddressLine1__c, AxtriaSalesIQST__LegalAddressLine2__c, AxtriaSalesIQST__LegalAddressPostalCode__c, AxtriaSalesIQST__LocationDescription__c, AxtriaSalesIQST__Manager_Name__c, AxtriaSalesIQST__Manager_PRID__c, AxtriaSalesIQST__Nickname__c, AxtriaSalesIQST__Original_Country_Code__c, AxtriaSalesIQST__PersonalCell__c, AxtriaSalesIQST__Preferred_Name__c, AxtriaSalesIQST__Re_hire_Date__c, AxtriaSalesIQST__ReportingToWorkerName__c, AxtriaSalesIQST__ReportsToAssociateOID__c, AxtriaSalesIQST__SOA_Country_Code__c, AxtriaSalesIQST__SOA_Marketing_Code__c, AxtriaSalesIQST__SS_Country__c, AxtriaSalesIQST__SalesIQ_Employee_Role__c, AxtriaSalesIQST__SalesIQ_Manager_Employee_Code__c, AxtriaSalesIQST__SalesIQ_Manager_Position__c, AxtriaSalesIQST__SalesIQ_Manager_Role__c, AxtriaSalesIQST__SalesIQ_Team_Instance__c, AxtriaSalesIQST__SalutationCode__c, AxtriaSalesIQST__Secondary_Address_Line_1__c, AxtriaSalesIQST__Secondary_Address_Line_2__c, AxtriaSalesIQST__Secondary_City__c, AxtriaSalesIQST__Secondary_State__c, AxtriaSalesIQST__Secondary_ZIP__c, AxtriaSalesIQST__Seperation_Date__c, AxtriaSalesIQST__StandardPayPeriodHours__c, AxtriaSalesIQST__StateTerritory__c, AxtriaSalesIQST__Storage_Address__c, AxtriaSalesIQST__Team__c, AxtriaSalesIQST__TerminationType__c, AxtriaSalesIQST__Termination_Date_2__c, AxtriaSalesIQST__Territory_Code__c, AxtriaSalesIQST__TimeZone__c, AxtriaSalesIQST__Transferred_to_HO_Date__c, AxtriaSalesIQST__Transferred_to_HO__c, AxtriaSalesIQST__Unassigned_Date__c, AxtriaSalesIQST__Unassigned_for_Days__c, AxtriaSalesIQST__UnionCode__c, AxtriaSalesIQST__WorkLocation__c, AxtriaSalesIQST__WorkPhone__c, AxtriaSalesIQST__Work_Fax__c, AxtriaSalesIQST__Worker_Category__c, Field_End_Date__c, Sales_Force_Code__c, Sales_Force_Name__c, Territory__c, Territory_Name__c, Assignment_Start_Date__c, Assignment_End_Date__c, Job_Assignment_Start_Date__c, Job_Assignment_End_Date__c, Position_Code__c, Position_Title__c, Position_Id__c, Position_Start_Date__c, Position_End_Date__c, Organization__c, Last_Hire_Date__c, Supervisor_First_Name__c, Supervisor_Nickname__c, Supervisor_Middle_Name__c, Supervisor_Last_Name__c, Personnel_Area__c, Employee_Type_Flag__c, Work_Address_Line_1__c, Work_Address_Line_2__c, Work_Address_Line_3__c, Work_Address_Line_4__c, Work_City__c, Work_State__c, Work_Zip__c, Work_Country__c, DSI_Logon__c, DSI_Email_Id__c, Payroll_Id__c, Payroll_Id_Start_Date__c, Payroll_Id_End_Date__c, Work_Phone_Number__c, Work_Voicemail__c, Band__c, UserPrefix__c, UserSuffix__c, OrganisationalUnitCode__c, OrganisationalUnitName__c, Leave_Start_Date__c, Leave_Return_Date__c, Leave_Type__c, Promotion_date__c, Demotion_date__c, Action_Type__c, Latest_Employee_Journey__c, Action_End_Date__c, Comp_Eligibility_flag__c, Training_Name_Type__c, Training_End_Date__c, Personnel_Number__c, Job_Number__c, Job_Name__c, Employee_Group__c, Original_Hire_Date__c, Termination_Date__c, Personnel_number_of_supervisor__c, Name_of_Supervisor__c, Physical_Work_C_O__c, Pay_Scale_Group__c, Personnel_Subarea_Code__c, Personnel_Subarea_Name__c, Action_Type_Name__c, Employment_Status__c, Comp_Eligibility__c, Reason_for_Action_Number__c, Reason_for_Action_Name__c, Employment_Status_Number__c, Sales_Force_Start_Date__c, Sales_Force_End_Date__c, Personnel_Area_Number__c, Employee_Group_Name__c, Name_of_Manager__c, Initials__c, Name_at_Birth__c, Second_Name__c, Second_Title__c, Other_Title__c, Name_Prefix__c, Second_Name_Prefix__c, Physical_Work_Country__c, Physical_Work_Phone__c, Permanent_Residence_Phone_Number__c, Separati__c, On_Training__c, Action_Start_Date__c, Reason_Code__c, Assignment_Type__c, Functional_Area_ID__c, Functional_Area_Description__c, New_Hire_In_Training_Days__c, Latest_Employee_Journey_Event__c, IC_Status__c, Employee_Type__c, Action_Sequence__c, Action_Reason__c, isTrainingCompleted__c, isTrainingCompleted2__c, Hire_Date__c, No_of_Training_Days__c, Comp_Eligibility_Start_Date__c, Comp_Eligibility_End_Date__c, Training_Start_Date__c, Old_Sales_Force_Code__c, CR_Employee_Feed__c, Training_Type__c, AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Position_Type__c,  CreatedDate FROM AxtriaSalesIQTM__Employee__c';
        this.query = query;

    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        System.debug('@@@ Start method of BatchICDataProcessing invoked');
        System.debug('@@@ query = '+query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<AxtriaSalesIQTM__Employee__c> scopeEmployees) {
        System.debug('@@@ Execute method of BatchICDataProcessing invoked');
        List<IC_ELIGIBILITY__c> ICEligibilityInsertionList = new List<IC_ELIGIBILITY__c>();
        List<cytd_contest__c> cytdcontestInsertionList = new List<cytd_contest__c>();
        List<WTD_Contest__c> WTDContestInsertionList = new List<WTD_Contest__c>();
        List<Term_List__c> TermInsertionList = new List<Term_List__c>();
        List<Roster__c> RosterInsertionList = new List<Roster__c>();

        for(AxtriaSalesIQTM__Employee__c emp : scopeEmployees){
            IC_ELIGIBILITY__c ic = new IC_ELIGIBILITY__c();
            ic.territory__c = emp.Territory__c;
            ic.employee_id__c = emp.AxtriaSalesIQTM__Employee_ID__c;
            ic.first_name__c = emp.AxtriaSalesIQTM__FirstName__c;
            ic.last_name__c = emp.AxtriaSalesIQTM__Last_Name__c;
            ic.territory_level__c = emp.AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Position_Type__c;
            ic.sales_force__c = emp.Old_Sales_Force_Code__c;
            ic.status__c = emp.IC_Status__c;
            ic.rep_type__c = emp.Position_Title__c;
            ic.start_date__c = emp.Assignment_Start_Date__c; 
            ic.end_date__c = emp.Assignment_End_Date__c;
            ic.hire_date__c = emp.Last_Hire_Date__c; 
            ic.termination_date__c = emp.Termination_Date__c;
            DateTime dT = emp.CreatedDate;
            Date d = Date.newInstance(dT.year(), dT.month(), dT.day());
            ic.data_month__c = d;
            ic.payroll_id__c = emp.Payroll_Id__c;
            
            ICEligibilityInsertionList.add(ic);

            cytd_contest__c cytd = new cytd_contest__c();
            cytd.territory_name__c = emp.Territory_Name__c;
            cytd.territory_level__c = emp.AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Position_Type__c;
            cytd.territory__c = emp.Territory__c;
            cytd.status_start_date__c = emp.Assignment_Start_Date__c; 
            cytd.status_end_date__c = emp.Assignment_End_Date__c;
            cytd.hire_date__c = emp.Last_Hire_Date__c;
            cytd.employee_last_name__c = emp.AxtriaSalesIQTM__Last_Name__c;
            cytd.employee_id__c = emp.AxtriaSalesIQTM__Employee_ID__c;
            cytd.employee_first_name__c = emp.AxtriaSalesIQTM__FirstName__c;
            cytd.reporting_month_core__c = d;
            cytd.payroll_id__c = emp.Payroll_Id__c;
            if(emp.Leave_Return_Date__c == null || emp.Leave_Start_Date__c == null){
                cytd.leave_days_ytd__c = 0;
            }
            else{
                date d1 = emp.Leave_Return_Date__c;
                date d2 = emp.Leave_Start_Date__c;
                cytd.leave_days_ytd__c = d2.daysBetween(d1);
            }
            cytd.comp_status__c = emp.IC_Status__c;

            cytdcontestInsertionList.add(cytd);

            WTD_Contest__c wtd = new WTD_Contest__c();
            wtd.territory__c = emp.Territory__c;
            wtd.territory_name__c = emp.Territory_Name__c;
            wtd.territory_level__c = emp.AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Position_Type__c;
            wtd.status_start_date__c = emp.Assignment_Start_Date__c; 
            wtd.status_end_date__c = emp.Assignment_End_Date__c;
            wtd.last_name__c = emp.AxtriaSalesIQTM__Last_Name__c;
            wtd.hire_date__c = emp.Last_Hire_Date__c;
            wtd.first_name__c = emp.AxtriaSalesIQTM__FirstName__c;
            wtd.employee_id__c = emp.AxtriaSalesIQTM__Employee_ID__c;

            wtd.reporting_date__c = d;
            wtd.payroll_id__c = emp.Payroll_Id__c;
            wtd.comp_status__c = emp.IC_Status__c;

            WTDContestInsertionList.add(wtd);

            Term_List__c term = new Term_List__c();
            term.personnel_area__c = emp.Personnel_Area__c;
            term.last_name__c = emp.AxtriaSalesIQTM__Last_Name__c;
            term.first_name__c = emp.AxtriaSalesIQTM__FirstName__c;
            term.action_type__c = emp.Action_Type__c;
            term.employee_id__c = emp.AxtriaSalesIQTM__Employee_ID__c;
            term.separation_date__c = emp.Termination_Date__c;
            term.s__c = '0';
            term.psubarea__c = emp.Personnel_Subarea_Code__c;
            term.prev_term_pos_stext__c = ' ';
            term.prev_term_pos_short__c = ' ';
            term.prev_term_pos_code__c = ' ';
            term.personnel_subarea__c = emp.Personnel_Subarea_Name__c;
            term.pers_no__c = emp.Personnel_Number__c;
            term.pa__c = emp.Personnel_Area_Number__c;

            TermInsertionList.add(term);

            Roster__c ros = new Roster__c();
            ros.last_name__c = emp.AxtriaSalesIQTM__Last_Name__c; 
            ros.first_name__c = emp.AxtriaSalesIQTM__FirstName__c;
            ros.employee_id__c = emp.AxtriaSalesIQTM__Employee_ID__c;
            ros.email__c = emp.AxtriaSalesIQTM__Email__c;
            ros.status__c = emp.IC_Status__c;
            ros.salesforce__c = emp.Position_Title__c;
            ros.sales_area_name__c = emp.Territory_Name__c;
            ros.sales_area_level__c = emp.AxtriaSalesIQTM__Current_Territory__r.AxtriaSalesIQTM__Position_Type__c;
            ros.sales_area__c = emp.Territory__c;
            ros.payroll_id__c = emp.Payroll_Id__c;
            ros.date__c = d;

            RosterInsertionList.add(ros);




        }

        if(ICEligibilityInsertionList.size()>0){
            insert ICEligibilityInsertionList;
        }
        if(cytdcontestInsertionList.size()>0){
            insert cytdcontestInsertionList;
        }
        if(WTDContestInsertionList.size()>0){
            insert WTDContestInsertionList;
        }
        if(TermInsertionList.size()>0){
            insert TermInsertionList;
        }
        if(RosterInsertionList.size()>0){
            insert RosterInsertionList;
        }
    }

    global void finish(Database.BatchableContext BC) {

    }
}