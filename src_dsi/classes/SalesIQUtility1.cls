public with sharing class SalesIQUtility1 {

   public static String namespaceClass = 'SnTAxtria';
   public static String namespacePage = 'SnTAxtria__'; 
   
   public static list<AxtriaSalesIQTM__User_Access_Permission__c> getUserAccessPermistion(string UserID, String countryID){
        
        system.debug('Hey Country ID is'+ countryID);
        system.debug('Hey User ID is'+ userID);
        list<AxtriaSalesIQTM__User_Access_Permission__c> accessRecs = new List<AxtriaSalesIQTM__User_Access_Permission__c>();
        accessRecs = [select AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Territory_Code__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.Name,/*AxtriaSalesIQTM__Position__r.Line__r.Name,*/AxtriaSalesIQTM__Position__r.AXTRIASALESIQTM__HIERARCHY_LEVEL__C,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Name/*,AxtriaSalesIQTM__Team_Instance__r.Cycle__c*/ from AxtriaSalesIQTM__User_Access_Permission__c where AxtriaSalesIQTM__User__c = :UserID and AxtriaSalesIQTM__Is_Active__c = True and AxtriaSalesIQST__Country__c= :countryID order by AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c ASC ];
        return accessRecs;
    }
    
    public static list<AxtriaSalesIQTM__User_Access_Permission__c> getUserAccessPermistionnonho(string UserID, String countryID){
        
        system.debug('Hey Country ID is'+ countryID);
        system.debug('Hey User ID is'+ userID);
        list<AxtriaSalesIQTM__User_Access_Permission__c> accessRecs = new List<AxtriaSalesIQTM__User_Access_Permission__c>();
        accessRecs = [select AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Territory_Code__c,AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.Name,/*AxtriaSalesIQTM__Position__r.Line__r.Name,*/AxtriaSalesIQTM__Position__r.AXTRIASALESIQTM__HIERARCHY_LEVEL__C,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Team_Instance__r.Name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.name,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__c,AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Workspace__r.Name/*,AxtriaSalesIQTM__Team_Instance__r.Cycle__c*/ from AxtriaSalesIQTM__User_Access_Permission__c where AxtriaSalesIQTM__User__c = :UserID and AxtriaSalesIQTM__Is_Active__c = True and AxtriaSalesIQST__Country__c= :countryID and (AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c = 'Current' and AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Scenario_Stage__c = 'Live') order by AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c ASC ];
        return accessRecs;
    }
    
    public static list<AxtriaSalesIQTM__Country__c> getCountriesFromCountry(){
        list<AxtriaSalesIQTM__Country__c> countries = new list<AxtriaSalesIQTM__Country__c>();
        
        
        countries = [select  id,name from AxtriaSalesIQTM__Country__c  WHERE Name != NULL]; 
        
        return countries;
    }
    
    public static String getCookie(String cookieName){
        Cookie cookieVar = ApexPages.currentPage().getCookies().get(cookieName);
        return cookieVar != null ? cookieVar.getValue() : '';
    }
    
    public static list<AxtriaSalesIQTM__Country__c> getCountryByCountryId(list<String> countryIds){         
        list<AxtriaSalesIQTM__Country__c> lsCountry = new list<AxtriaSalesIQTM__Country__c>();
        
        lsCountry = [select Name, AxtriaSalesIQTM__Country_Code__c , AxtriaSalesIQTM__Country_Flag__c , AxtriaSalesIQTM__Status__c from AxtriaSalesIQTM__Country__c where id in: countryIds];
        
        return lsCountry;
     }
     
      public static void setCookieString(String cookieName, String value){
        Cookie cookieVar = ApexPages.currentPage().getCookies().get(cookieName);
        cookieVar = new Cookie(cookieName, String.valueOf(value),null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{cookieVar});
    }
    
    public static String getCountryCookieName()
    {
        return 'CountryID';
    }
     
     public static List<AxtriaSalesIQTM__User_Persistence__c> getUserPersistenceByUserID(String userID){
          list<AxtriaSalesIQTM__User_Persistence__c> userPerst = new list<AxtriaSalesIQTM__User_Persistence__c>();
          
          userPerst = [select AxtriaSalesIQTM__User__c, AxtriaSalesIQTM__Default_Country__c, AxtriaSalesIQTM__Workspace_Country__c from AxtriaSalesIQTM__User_Persistence__c where AxtriaSalesIQTM__User__c =: userID];
          
          return userPerst;
     }
    
    public static map<string,string> checkCountryAccess(string countryId){ 
        
            map<string,string> successErrorMap = new map<string,string>(); // This map will contain either success:CountryID or Error:Error message
            string firstCountryID = '';
            //This is to check whether Sharing rule exist or not. IF assignedCountries return something that means Sharing rule is there. 
            list<AxtriaSalesIQTM__Country__c> assignedCountries = getCountriesFromCountry();
            system.debug('######### Shared Countries ' + assignedCountries);
            if(assignedCountries != null && assignedCountries.size()>0){
               firstCountryID = assignedCountries[0].id;
               system.debug('######### Shared Countries ' + assignedCountries[0]);
            }
            //This means Sharing rule is not configured therefore return error.
            if(string.isEmpty(firstCountryID)){
              successErrorMap.put('Error', 'No Access to Country');
              return successErrorMap; 
            }
            // This else will execute only if there is sharing rule configured.
            else{
                //If countryID is there in the cookie check whether user has access to that by querying on Country
                if(!string.isEmpty(countryId)){
                  //This will confirm whether user still have access to the country ID selected above.
                  list<AxtriaSalesIQTM__Country__c> countries = getCountryByCountryId(new list<string>{countryId});
                  system.debug('############ countries ' + countries);
                  if(countries == null || countries.size() == 0){
                     successErrorMap.put('Error', 'No COuntry Access');
                     return successErrorMap; 
                  }
               }else{
                  //This will fetch Default country from User Persistence if there is any.
                  list<AxtriaSalesIQTM__User_Persistence__c> userPerst = getUserPersistenceByUserID(UserInfo.getUserId());
                  for(AxtriaSalesIQTM__User_Persistence__c up : userPerst){
                       countryID = up.AxtriaSalesIQTM__Default_Country__c;
                  }
                }
                system.debug('############ countryId ' + countryId);
                if(!string.isEmpty(countryId)){
                  //This will confirm whether user still have access to the country ID selected above.
                  list<AxtriaSalesIQTM__Country__c> countries = getCountryByCountryId(new list<string>{countryId});
                  if(countries == null || countries.size() == 0){
                     successErrorMap.put('Error', 'No Access tp Country');
                     return successErrorMap; 
                  }
                }else{  
                   countryId = firstCountryID;
                }
            }
            successErrorMap.put('Success',countryId);
            return successErrorMap;
      }
   
   }