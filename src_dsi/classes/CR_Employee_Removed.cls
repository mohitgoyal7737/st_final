global class CR_Employee_Removed implements Database.Batchable<sObject>,Schedulable{
    
    public String query;
    global Integer i = 1;
    global list<AxtriaSalesIQST__CR_Employee_Feed__c> Isremoved = new list<AxtriaSalesIQST__CR_Employee_Feed__c>();

    

    global CR_Employee_Removed (){

       query ='Select id, CreatedDate,AxtriaSalesIQST__IsRemoved__c from AxtriaSalesIQST__CR_Employee_Feed__c' ;
        
 

        System.debug('query'+ query);
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<AxtriaSalesIQST__CR_Employee_Feed__c> scope) {
        

      if(scope!=null && scope.size()>0)

      {

      for (AxtriaSalesIQST__CR_Employee_Feed__c Feed :scope)
           {

            Date feedcreateddate = Feed.CreatedDate.date();

            Date dToday = Date.today();
            
            Integer numberDaysDue = feedcreateddate .daysBetween(dToday );
            
            system.debug('numberDaysDue######'+numberDaysDue);

            //if(Integer.valueof(feedcreateddate.day()) -Integer.valueof(dToday.day()) >30 )
            if( numberDaysDue >30 )
                    {

                        Feed.AxtriaSalesIQST__IsRemoved__c = true;
                        Isremoved.add(Feed);

                    }
        
           }

           
      }

      update Isremoved;
 
    }

public void dummyFunction(){
integer i = 0;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
i++;
}
   
global void execute(SchedulableContext sc){
        Database.executeBatch(new CR_Employee_Removed (), 1);
    }
    
     global void finish(Database.BatchableContext BC) {

    }
}