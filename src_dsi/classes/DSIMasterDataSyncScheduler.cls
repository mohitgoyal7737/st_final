global class DSIMasterDataSyncScheduler implements Schedulable,Database.AllowsCallouts{
    
    public static final String QUEUE_LOGGER_RECORDTYPE = 'Queue Logger';
    public static final String QUEUE_LOGGER_TYPE_MASTER = 'Master Data Syncing';
    public static final String BRMS_MODULE = 'BRMS' ;
    public static string IN_PROGRESS = 'In Progress';
    public static string QUEUED = 'Queued';
    public static final string ETL_BRMS_Instance='BRMS';
    public static string ERROR = 'Error';
    
    global void execute(SchedulableContext SC) {
        // AxtriaSalesIQTM__Is_Master__c = true
        List<AxtriaSalesIQTM__Data_Set__c> dataSetList = [SELECT AxtriaSalesIQTM__Data_Set_Object_Name__c,AxtriaSalesIQTM__destination__c,AxtriaSalesIQTM__is_internal__c,AxtriaSalesIQTM__Is_Master__c,AxtriaSalesIQTM__SalesIQ_Internal__c, (select Id from AxtriaSalesIQTM__Data_Objects__r LIMIT 1) FROM AxtriaSalesIQTM__Data_Set__c WHERE AxtriaSalesIQTM__is_internal__c = false AND AxtriaSalesIQTM__SalesIQ_Internal__c = true AND AxtriaSalesIQTM__Data_Set_Object_Name__c != null];

        List<String> dataObjectIds = new List<String>();
        for(AxtriaSalesIQTM__Data_Set__c dataSet : dataSetList){
            if(!dataSet.AxtriaSalesIQTM__Data_Objects__r.isEmpty())
                dataObjectIds.add(dataSet.AxtriaSalesIQTM__Data_Objects__r[0].Id);
        }
        
        String dataObjectIdsString = String.join(dataObjectIds, ',');
        system.debug('DO Ids: ' + dataObjectIdsString);
        
        // added date check for only v5 DO sync
        List<AxtriaSalesIQTM__Data_Object__c> dataObjectList=[Select id from AxtriaSalesIQTM__Data_Object__c WHERE id in :dataObjectIds];
        for(AxtriaSalesIQTM__Data_Object__c dataObject:dataObjectList){
            dataObject.AxtriaSalesIQTM__status__c=QUEUED;
            dataObject.AxtriaSalesIQTM__Current_Sync_Date__c=System.now();
        }
        update dataObjectList;
        
        Id recordId = Schema.SObjectType.AxtriaSalesIQTM__SalesIQ_Logger__c.getRecordTypeInfosByName().get(QUEUE_LOGGER_RECORDTYPE).getRecordTypeId();
        AxtriaSalesIQTM__SalesIQ_Logger__c masterSynclog = new AxtriaSalesIQTM__SalesIQ_Logger__c(AxtriaSalesIQTM__Module__c=BRMS_MODULE, AxtriaSalesIQTM__Status__c=IN_PROGRESS, AxtriaSalesIQTM__Type__c= QUEUE_LOGGER_TYPE_MASTER);
        masterSynclog.RecordtypeId= recordId;
        insert masterSynclog;

        callpythonService(masterSyncLog.id,dataObjectIdsString);
        
    }
    
    public static  string getBRMSConfigValues(string type){ 
        List<AxtriaSalesIQTM__BRMS_Config__mdt> typesList = [Select AxtriaSalesIQTM__BRMS_Type__c,AxtriaSalesIQTM__BRMS_Value__c from AxtriaSalesIQTM__BRMS_Config__mdt where AxtriaSalesIQTM__BRMS_Type__c= :type ];
        string value;
        if(typesList.size()>0){
            value=typesList[0].AxtriaSalesIQTM__BRMS_Value__c; 
        }  
        return value; 
    }
    
    public static AxtriaSalesIQTM__ETL_Config__c getETLConfigByName(string etlConfigName){
        AxtriaSalesIQTM__ETL_Config__c etlConfigList;
        etlConfigList = [SELECT AxtriaSalesIQTM__SF_UserName__c, AxtriaSalesIQTM__SF_Password__c, AxtriaSalesIQTM__S3_Security_Token__c FROM AxtriaSalesIQTM__ETL_Config__c where name =:etlConfigName limit 1];
        return etlConfigList;
    }

    public static Boolean isSandboxOrg() {
        List<AxtriaSalesIQTM__PackageExtension__c> sandbox = [select Id from AxtriaSalesIQTM__PackageExtension__c where Name like 'sandbox'];
        if(sandbox.isEmpty())
            return false;
        else
            return true;
    }

    public static HttpResponse getHTTPResponse(String endPoint, Map<String,String> mapParams, String requestMethod, Integer timeOut ){
        Http h = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndPoint(endPoint);
        String body='';
        for(String key : mapParams.keySet()){
            body+= key+'='+String.valueof(mapParams.get(key)) + '&';
        }            
        request.setMethod(requestMethod);
        request.setTimeout(timeOut);
        request.setBody(body.removeEnd('&'));  
        HttpResponse reponse = h.send(request);
        return reponse;
    }

    @future(callout=true)
    public static void  callpythonService(String masterSynclogId,String dataObjectIdsString){
        string pythonService = getBRMSConfigValues('BRDataSync');
        //SSL CHANGES
        Map<String,String> urlParams = new Map<String,String>();
        urlParams.put('serviceUrl',pythonService);

        AxtriaSalesIQTM__ETL_Config__c etlObj = getETLConfigByName(ETL_BRMS_Instance);
        String sfUserName = etlObj.AxtriaSalesIQTM__SF_UserName__c;
        String sfPassword = etlObj.AxtriaSalesIQTM__SF_Password__c;
        String sfToken = etlObj.AxtriaSalesIQTM__S3_Security_Token__c;
        
        try{
            ///String url = pythonService+'?user_name='+sfUserName+'&password='+sfPassword+'&security_token='+sfToken+'&scenario_id=&dataObjectIds='+dataObjectIdsString+'&namespace='+SalesIQGlobalConstants.NAME_SPACE+'&sandbox='+SalesIQUtility.isSandboxOrg()+'&logger_id='+MasterSynclogId+'&logger_object_name='+SalesIQGlobalConstants.getOrgNameSpace()+'SalesIQ_Logger__c';
            //system.debug('data sync url --  ' + url);
            String isSandbox = ( isSandboxOrg() ) ? 'true' : 'false';
            urlParams.put('user_name',sfUserName);
            urlParams.put('password',sfPassword);
            urlParams.put('security_token',sfToken);
            urlParams.put('scenario_id','');
            urlParams.put('dataObjectIds',dataObjectIdsString);
            urlParams.put('namespace','AxtriaSalesIQTM__');
            urlParams.put('sandbox',isSandbox);
            urlParams.put('logger_id', masterSynclogId);
            urlParams.put('logger_object_name', 'AxtriaSalesIQTM__SalesIQ_Logger__c');


            HttpResponse response = getHTTPResponse(urlParams.get('serviceUrl'),urlParams,'POST',120000);

            system.debug('#### Python Response : '+response);
            if(response.getStatusCode() != 200) {
                AxtriaSalesIQTM__SalesIQ_Logger__c updateLoggerStatus = new AxtriaSalesIQTM__SalesIQ_Logger__c(Id = masterSynclogId);
                updateLoggerStatus.AxtriaSalesIQTM__Status__c = ERROR;
                updateLoggerStatus.AxtriaSalesIQTM__Stack_Trace__c = response.getStatus();
                update updateLoggerStatus;
            }
        }
        catch(exception ex){
            system.debug('Error in master sync : '+ex.getMessage());
        }
    }
}