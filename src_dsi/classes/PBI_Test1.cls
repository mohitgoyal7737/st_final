public with sharing class PBI_Test1
    {
        public Map<String,Object> results {get;set;}
        public String position{get;set;}
         public String reportPeriod{get;set;}
        public String application_name;
        public String accessToken {get;set;}
        public String embedToken ;
        public String client_id;
        public String resource_URI ;
        public String access_token_url;
        public String username ;
        public String password ;
        public String datasetid ;
        public String role;
        public String PBI_GroupId {get;set;} //group id of the report
        public String reportId {get;set;} //report id of the report
        public String PBI_ReportID {get;set;} //report id of the report
        public String embed_url {get;set;} //embed_url
        public user U;
        public SIQIC__Reports__c R ;
        public String reportListLink;
        public String gobacklink{get;set;}
        public String UserRole {get;set;}
        public String UserGeoID {get;set;} 
        public String UserGeoName  {get;set;} 
        public String UserEmpID {get;set;}
        public String UserLocale {get;set;} 
        public String Report {get;set;} 
        public Map<String,Object> SCHeaderDataList {get;set;}
        public SIQIC__Reports__c report1 {get;set;}  
        public String BusinessUnit {get;set;}
        public String Version {get;set;}
        public User loggedInUser;
        public String URL ;
         public String endpoint;
        public String Tenant {get;set;}
        public String Param1{get;set;}
        public String Param2{get;set;}
        public String Param3{get;set;}
        public String month {get;set;}
        public String year {get;set;}
        public String Period{get;set;}
        transient public List<Object> dataMap;
        transient public List<Map<string,object>> summaryDataList {get;set;}
        public string tabToJsonValueStrListString{get;set;}
        
        public PBI_Test1(){
            String procName;
            String parameters;
            String  reportId = apexpages.currentpage().getparameters().get('Id');
            URL=reportUtility.fetchDashboardURL();
            if(reportId!=null){
                report1=reportUtility.fetchReportMetadata(reportId);
            }
            else{
                UserEmpID='62244639';
                BusinessUnit='US';
                UserGeoID='11001G3';
                Report='a4X3h000000Zpj8EAC';
                UserRole = 'Territory Manager';
                version='V1';
                Tenant = 'BAUSCHFULLCOPY';
            }
            
          /*  loggedInUser = [select id,name,SIQIC__Employee_ID__c,SIQIC__Geo_Id__c,UserRole.Name, SIQIC__TenantId__c,SIQIC__Tenant__c,LocaleSidKey from User where ID =:Userinfo.getuserId() limit 1];
                     
            if(report1!=null){
                UserGeoID = report1.SIQIC__Geo_Id__c;
                UserGeoName = report1.SIQIC__Geo_Id__c + ' - ' + report1.SIQIC__Geo_Name__c;
                Report  = report1.SIQIC__publish__r.SIQIC__Report_Data_Push__c;
                BusinessUnit= report1.SIQIC__Business_Unit__c.replace('+','&');
                UserEmpID = report1.SIQIC__EmpId__c;
                Tenant = report1.SIQIC__Tenant__c;
                UserRole    = report1.SIQIC__Role__c;
                month = report1.SIQIC__Month__c;
                year = report1.SIQIC__Year__c;
                Version = report1.SIQIC__version__c; 
            
                PBI_GroupId = report1.SIQIC__Path__c.replace('/public/','');       
                PBI_ReportID = report1.SIQIC__Path2__c.replace('/public/',''); 
                
                             
                if(report1.SIQIC__User__c!=null)
                {
                    UserLocale=report1.SIQIC__User__r.LocaleSidKey;
                }
                else{
                    UserLocale=loggedInUser.LocaleSidKey;
                }        
            }
            else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Access Denied'));        
            }
            if(!Test.isRunningTest())
//               getProductDescCount(); */
               getdatafromSQL();
                
    
/*            reportListLink = System.URL.getSalesforceBaseUrl().toExternalForm() + '/apex/ViewReports';
            gobacklink = reportListLink.replace('--c','--siqic');
            System.debug(reportListLink);
                
            application_name= OAuthApp_pbi__c.getall().values()[0].Name;
            System.debug(application_name);
            client_id = OAuthApp_pbi__c.getValues(application_name).Client_Id__c;
            resource_URI = OAuthApp_pbi__c.getValues(application_name).Resource_URI__c;
            access_token_url = OAuthApp_pbi__c.getValues(application_name).Access_Token_URL__c;
            username = OAuthApp_pbi__c.getValues(application_name).Username__c;
            password = OAuthApp_pbi__c.getValues(application_name).Password__c;
           
           // getAccessToken();
            System.debug(URL);
            System.debug('Tenant:-------'+ Tenant); 
            System.debug(endpoint);
            System.debug('UserGeoID-----' + UserGeoID);      */
            
        
        }
        
        
        
        public void getdatafromSQL()
        {
        
        
        Integer dbServer = 2; 
String procName = 'PBITest';
String filterValue = '1'; // filter value will be blank if not required
Map<String, List<Map<String, String>>> m = DSI_Utility.getProcResponse(dbServer, procName, filterValue);
System.debug('m :: '+m);

//Iteration on dataSets
for(String key:m.keySet()){
    System.debug('key :: '+key);
    List<Map<String, String>> recordsMap = m.get(key);
    for(Map<String, String> m1:recordsMap){
        for(String key1:m1.keySet()){
            System.debug(key1+'  :  '+m1.get(key1));  // eg, Report_type : Payroll Report
        }
    }
}

        }
        
        
        public Static Map<String, Object> restCallOut(String endPoint){
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        System.debug('EndPoint===>'+endPoint);
        request.setEndpoint(endPoint);
        request.setMethod('GET');
        request.setTimeout(120000);
        HttpResponse response = http.send(request);
        system.debug('#### Response : '+response);
        Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Job Status!!  ' + endPoint + response.getStatus()));
        return results;
    }
    
     public void getDatafromWebservice(){
               
        summaryDataList = new List<Map<String,Object>>();
       
             tabToJsonValueStrListString = JSON.serialize(results.get('1'));
        System.debug('tabToJsonValueStrListString++'+tabToJsonValueStrListString);
        
        dataMap = (List<Object>) JSON.deserializeUntyped(JSON.serialize(results.get('1')));
        System.debug('SIZE'+dataMap.size());
        for(Object o:dataMap){
            Map<String, Object> dataMapping=(Map<String, Object>) JSON.deserializeUntyped (JSON.serialize(o));
            for(String x:dataMapping.keySet()){
                if(dataMapping.get(x)==null){
                    dataMapping.put(x,'');
                }
            }        
            summaryDataList.add(dataMapping);
        }
        System.debug('summaryDataList'+summaryDataList.size());
        
        
 }
      
      public PageReference downloadReport(){
         //results = restCallOut(endpoint);
        //System.debug('----------results in DownloadReport function-----------'+results);
        //getDatafromWebservice();
        PageReference pg;
       
        
        pg = new PageReference('/apex/Excel_Format');
        
        
         pg.setRedirect(false);
        return pg;
    }  
    
            
        public void getProductDescCount(){
            
            List<Object> summaryDataMap=new List<Object>();
            String procName='PBI_Proc_Goalcard(-,-,-,-,-)';
            procName = EncodingUtil.urlEncode(procName, 'UTF-8');
            
            String parameters;
            parameters=UserGeoID + ',' + BusinessUnit + ',' + UserRole + ',' + Report + ',' + Version;
            system.debug('Parameters+================+ ' + parameters);
            parameters = EncodingUtil.urlEncode(parameters, 'UTF-8');
            String endPoint=URL+Tenant+'/'+procName+'/'+parameters;
            System.Debug('EndPoint ===== ' + endPoint);
            
            results=reportUtility.restCallOut(endPoint);
            System.debug('Results+================+ ' + results);
            if(!results.isEmpty()){
            //PARAM1 - Count for 1st Table
            summaryDataMap=(List<Object>) JSON.deserializeUntyped(JSON.serialize(results.get('1')));
            if(summaryDataMap.size()>0){
               
                SCHeaderDataList=(Map<String, Object>)summaryDataMap[0];
                System.debug(SCHeaderDataList.get('Param1'));
                Param1=String.valueOf(SCHeaderDataList.get('Param1'));
            }
            else{
                Param1='0';
            }
            
            //PARAM2 - Count for 2nd Table
            summaryDataMap=(List<Object>) JSON.deserializeUntyped(JSON.serialize(results.get('2')));
            if(summaryDataMap.size()>0){
               
                SCHeaderDataList=(Map<String, Object>)summaryDataMap[0];
                System.debug(SCHeaderDataList.get('Param2'));
                Param2=String.valueOf(SCHeaderDataList.get('Param2'));
            }
            else{
                Param2='0';
            }
            
            //PARAM3 - Count for 3rd Table = National Table
            summaryDataMap=(List<Object>) JSON.deserializeUntyped(JSON.serialize(results.get('3')));
            if(summaryDataMap.size()>0){
               
                SCHeaderDataList=(Map<String, Object>)summaryDataMap[0];
                System.debug(SCHeaderDataList.get('Param3'));
                Param3=String.valueOf(SCHeaderDataList.get('Param3'));
            }
            else{
                Param3='0';
            }
            }
        }
        
         public void getAccessToken(){
            accessToken = '';
            try{
                List<String> urlParams = new List<String> {
                    'grant_type=password',
                    'scope=' + EncodingUtil.urlEncode('openid', 'UTF-8'),
                    'username=' + EncodingUtil.urlEncode(username, 'UTF-8'),
                    'password=' + EncodingUtil.urlEncode(password, 'UTF-8'),
                    'client_id=' + EncodingUtil.urlEncode(client_id, 'UTF-8'),
                    'resource=' + EncodingUtil.urlEncode(resource_URI, 'UTF-8')
                };
                system.debug(urlParams);
                Http h = new Http();
                HttpRequest req = new HttpRequest();
                req.setEndpoint(access_token_url);
                req.setMethod('POST');
                req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
                String body = String.join(urlParams, '&');
                req.setBody(body);
                HttpResponse res = h.send(req);
                System.debug('response-->'+res.getBody());
                Map<String,String> newMap = (Map<String,String>)JSON.deserialize(res.getBody(), Map<String,String>.class);
                System.debug(newMap);
                accessToken = newMap.get('access_token');      
            }
            catch(Exception e){
                System.debug('Error Message-->'+e.getMessage());
            }
        }
    
        public void trackReportInst(){
            Id userId = UserInfo.getUserId();
            Id reportId = ApexPages.currentPage().getParameters().get('id');
            SIQIC__Reports__c inst = [select OwnerId,SIQIC__Report_Type__c,SIQIC__Report_Period__c,LastViewedDate from SIQIC__Reports__c where id = :reportId limit 1];
            reportUtility.createReportTrackingRecord(userId,reportId,inst.SIQIC__Report_Type__c,inst.OwnerId,inst.SIQIC__Report_Period__c);       
        }

    }