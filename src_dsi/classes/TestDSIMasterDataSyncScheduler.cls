@isTest
private class TestDSIMasterDataSyncScheduler {
public static String CRON_EXP = '0 0 0 28 2 ? 2022';
    static testMethod void validate_DSIMasterDataSyncScheduler(){
    
    AxtriaSalesIQTM__Data_Set__c DS = New AxtriaSalesIQTM__Data_Set__c();
    DS.Name = 'ABC';
    DS.AxtriaSalesIQTM__is_internal__c = False;
    DS.AxtriaSalesIQTM__SalesIQ_Internal__c = true;
    DS.AxtriaSalesIQTM__Data_Set_Object_Name__c = 'Notblank';
    Insert DS;
    
   
    AxtriaSalesIQTM__BRMS_Config__mdt BRMS_CS = new AxtriaSalesIQTM__BRMS_Config__mdt();
    BRMS_CS.MasterLabel = 'JJJ';
    BRMS_CS.NamespacePrefix = 'lll';
    BRMS_CS.AxtriaSalesIQTM__BRMS_Value__c = 'lllll';
    
    AxtriaSalesIQTM__ETL_Config__c  ETL = New AxtriaSalesIQTM__ETL_Config__c();
    ETL.Name = 'BRMS';
    ETL.AxtriaSalesIQTM__SF_UserName__c = 'Salesiq@123.com';
    ETL.AxtriaSalesIQTM__SF_Password__c = 'Salesiq@123.com';
    ETL.AxtriaSalesIQTM__S3_Security_Token__c= 'Salesiq@123.com';
    Insert ETL;    
    
        
   DSIMasterDataSyncScheduler.getBRMSConfigValues('BRMS');
   DSIMasterDataSyncScheduler.getETLConfigByName('BRMS');
   DSIMasterDataSyncScheduler.isSandboxOrg(); 
   Test.StartTest();

    String jobId = System.schedule('DSIMasterDataSyncScheduler_Test',CRON_EXP,new DSIMasterDataSyncScheduler());

Test.StopTest(); 
    }
}