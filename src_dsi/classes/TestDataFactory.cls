/*Author : Ayush Rastogi*/

@isTest
public class TestDataFactory {
    
    User loggedInUser = new User(id=UserInfo.getUserId());
    //Added by Prince
    /*
@author - Prince Richard Augustin (A2661)
@description - Test Data created for trst class created by me.
*/


    public static AxtriaSalesIQTM__Affiliation_Network__c createAffliNet(AxtriaSalesIQTM__Country__c countr) {
        AxtriaSalesIQTM__Affiliation_Network__c  affnet = new AxtriaSalesIQTM__Affiliation_Network__c();
        affnet.AxtriaSalesIQTM__Hierarchy_Level__c = '1';
        affnet.AxtriaSalesIQTM__Country__c = countr.Id;
        affnet.name = 'test';
        return affnet;
        
    }
    public static AxtriaSalesIQTM__Account_Affiliation__c createAcctAffli(Account acc,AxtriaSalesIQTM__Affiliation_Network__c affnet) {
        AxtriaSalesIQTM__Account_Affiliation__c  accaff = new AxtriaSalesIQTM__Account_Affiliation__c();
        //accaff.BU_Rank__c = 1.00;
        accaff.AxtriaSalesIQTM__Parent_Account__c = acc.Id;
        accaff.AxtriaSalesIQTM__Account__c = acc.Id;
        accaff.AxtriaSalesIQTM__Affiliation_Network__c = affnet.id;
        return accaff;
        
    }


    public static AxtriaSalesIQTM__CIM_Config__c createCIMConfig(AxtriaSalesIQTM__Team_Instance__c teamins){
        AxtriaSalesIQTM__CIM_Config__c cimcc = new AxtriaSalesIQTM__CIM_Config__c();
        cimcc.name = 'Total Targets Test';
        cimcc.AxtriaSalesIQTM__Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
        cimcc.AxtriaSalesIQTM__Attribute_API_Name__c = 'Account_HCP_HCO_Number__c';
        cimcc.AxtriaSalesIQTM__Aggregation_Type__c = 'COUNT_DISTINCT';
        cimcc.AxtriaSalesIQTM__Aggregation_Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
        cimcc.AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c = 'AxtriaSalesIQTM__Position_Team_Instance__c';
        //cimcc.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c = 'AxtriaSalesIQTM__isincludedCallPlan__c=true';
        //cimcc.AxtriaSalesIQTM__Aggregation_Condition_Attribute_Value__c = ' ';
        cimcc.AxtriaSalesIQTM__team_instance__c = teamins.id;
        cimcc.AxtriaSalesIQTM__Enable__c = true ;
        return cimcc;
    }
    
    public static AxtriaSalesIQTM__CIM_Position_Metric_Summary__c createCIMPosMetSum(AxtriaSalesIQTM__CIM_Config__c cimcc, AxtriaSalesIQTM__Position_Team_Instance__c positionproduct, AxtriaSalesIQTM__Team_Instance__c teamins){
        AxtriaSalesIQTM__CIM_Position_Metric_Summary__c cimposumm = new AxtriaSalesIQTM__CIM_Position_Metric_Summary__c();
        cimposumm.name = 'Total Targets Test';
        cimposumm.AxtriaSalesIQTM__CIM_Config__c = cimcc.Id;
        cimposumm.AxtriaSalesIQTM__Position_Team_Instance__c = positionproduct.Id;
        cimposumm.AxtriaSalesIQTM__Team_Instance__c = teamins.Id;
        return cimposumm;
    }
    
    public static AxtriaSalesIQTM__ETL_Config__c configureETL(){
        AxtriaSalesIQTM__ETL_Config__c etl = new AxtriaSalesIQTM__ETL_Config__c();
        etl.Name = 'ComparativeAnalysis';
        etl.AxtriaSalesIQTM__End_Point__c = 'http://13.66.200.148/ComparativeAnalysis/initParams';
        etl.AxtriaSalesIQTM__SF_UserName__c = 'sntproductdev@axtria.com.qa2';
        etl.AxtriaSalesIQTM__SF_Password__c = '***********';
        etl.AxtriaSalesIQTM__S3_Security_Token__c = '*************************';
        etl.AxtriaSalesIQTM__Org_Type__c = 'login';
        etl.AxtriaSalesIQTM__S3_Bucket__c = 'F:/Bayer_SnT/ComparativeAnalysis/Data/Unmanaged QA/Data_For_measureId.csv';
        etl.AxtriaSalesIQTM__S3_Filename__c = 'F:/Bayer_SnT/ComparativeAnalysis/Data/Unmanaged QA//Data_For_measureId.csv';
        return etl;
    }
 

    public static AxtriaSalesIQTM__Team_Instance__c createTeamInstance(AxtriaSalesIQTM__Team__c  team/*, Cycle__c cycle*/) {
        AxtriaSalesIQTM__Team_Instance__c teamins = new AxtriaSalesIQTM__Team_Instance__c();
        teamins.Name = 'testteaminsname';
        //teamins.Segmentation_Universe__c = 'Full S&T Input Customers';
        //teamins.IsHCOSegmentationEnabled__c = true;
        teamIns.AxtriaSalesIQTM__Team__c = team.id;
        teamins.AxtriaSalesIQTM__IC_EffEndDate__c = date.today()+1;
        teamins.AxtriaSalesIQTM__IC_EffstartDate__c = date.today();
        return teamins;
        
    }
    
    
    public static AxtriaSalesIQTM__Organization_Master__c createOrganizationMaster() {
        AxtriaSalesIQTM__Organization_Master__c orgmas = new AxtriaSalesIQTM__Organization_Master__c();
        orgmas.Name = 'abcd';
        orgmas.AxtriaSalesIQTM__Org_Level__c = 'Global';
        orgmas.AxtriaSalesIQTM__Parent_Country_Level__c = true;
        return orgmas;
        
    }

  
    
    public static Account createAccount() {
        Account acc = new Account();
        acc.Name = 'Chelsea Parson';
        //acc.Profile_Consent__c = 'yes';
        //acc.Primary_Speciality_Code__c = '123';
        acc.AxtriaSalesIQTM__Speciality__c = 'Cardiology';
        acc.AccountNumber = 'adf23';
        acc.AxtriaSalesIQTM__External_Account_Number__c = '123';
        acc.BillingStreet = 'abc';
        //acc.Marketing_Code__c = 'thgh';
        //acc.Primary_Speciality_Code__c = 'id';
        return acc;
    }
    
    public static AxtriaSalesIQTM__Country__c createCountry(AxtriaSalesIQTM__Organization_Master__c orgmas) {
        AxtriaSalesIQTM__Country__c countr = new AxtriaSalesIQTM__Country__c();
        countr.Name = 'abcd';
        countr.AxtriaSalesIQTM__Country_Code__c = 'US';
        countr.AxtriaSalesIQTM__Status__c = 'Active';
        countr.Name = 'USA';
        countr.AxtriaSalesIQTM__Parent_Organization__c = orgmas.id;
        return countr;
    }

    public static User createUser(String profileId) {
        // This code runs as the system user
        
        User tUser = new User(Alias = 'Rep', Email = 'repuser@testorg.com',
                              EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
                              LocaleSidKey = 'en_US', ProfileId = profileId,
                              TimeZoneSidKey = 'America/Los_Angeles', UserName = 'repuserQuset@testorg.com');
        return tUser;
    }
    public static AxtriaSalesIQTM__Workspace__c createWorkspace(String workspaceName, Date startDate, Date endDate) {
        AxtriaSalesIQTM__Workspace__c workspace = new AxtriaSalesIQTM__Workspace__c();
        workspace.Name = workspaceName;
        workspace.AxtriaSalesIQTM__Workspace_Description__c = 'Some Description';
        workspace.AxtriaSalesIQTM__Workspace_Start_Date__c = startDate;
        workspace.AxtriaSalesIQTM__Workspace_End_Date__c = endDate;
        
        return workspace;
    }
    
    public static AxtriaSalesIQTM__Position_Account__c createPositionAccount(Account acc,AxtriaSalesIQTM__Position__c pos,AxtriaSalesIQTM__Team_Instance__c teamins){
        AxtriaSalesIQTM__Position_Account__c posAccount = new AxtriaSalesIQTM__Position_Account__c ();
        posAccount.CurrencyIsoCode = 'USD';
        posAccount.AxtriaSalesIQTM__Account__c = acc.id;
        posAccount.AxtriaSalesIQTM__Position__c = pos.id;
        posAccount.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        posAccount.AxtriaSalesIQTM__Effective_Start_Date__c = date.Today();
        posAccount.AxtriaSalesIQTM__Effective_End_Date__c = date.Today() +1;       
        
        return posAccount;
        
        
    }
   
    
    public static AxtriaSalesIQTM__Scenario__c createScenario(Id workspaceId, Id scenarioTeamInstanceId, Id sourceTeamInstanceId, String stage, String processStage, String status, Date startDate, Date endDate) {
        AxtriaSalesIQTM__Scenario__c scenario = new AxtriaSalesIQTM__Scenario__c();
        if (scenarioTeamInstanceId != null)
            scenario.AxtriaSalesIQTM__Team_Instance__c = scenarioTeamInstanceId;
        if (sourceTeamInstanceId != null)
            scenario.AxtriaSalesIQTM__Source_Team_Instance__c = sourceTeamInstanceId;
        scenario.AxtriaSalesIQTM__Effective_Start_Date__c = startDate;
        scenario.AxtriaSalesIQTM__Effective_End_Date__c = endDate;
        scenario.AxtriaSalesIQTM__Scenario_Stage__c = stage;
        scenario.AxtriaSalesIQTM__Request_Process_Stage__c = processStage;
        scenario.AxtriaSalesIQTM__Scenario_Status__c = status;
        scenario.AxtriaSalesIQTM__Workspace__c = workspaceId;
        
        return scenario;
    }
    
    public static AxtriaSalesIQTM__Position__c createPosition(AxtriaSalesIQTM__Team__c team,AxtriaSalesIQTM__Team_Instance__c teamins) {
        
        AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();
        pos.Name = 'Test';
        pos.AxtriaSalesIQTM__Client_Territory_Name__c = 'ALBANY, NY (N003)';
        //pos.AxtriaSalesIQTM__Parent_Position__c = pos.id;
        pos.AxtriaSalesIQTM__Client_Position_Code__c = 'N003';
        pos.AxtriaSalesIQTM__Client_Territory_Code__c = 'N003';
        pos.AxtriaSalesIQTM__Position_Type__c = 'Territory';
        //pos.AxtriaSalesIQTM__Related_Position_Type__c = team.AxtriaSalesIQTM__Type__c;
        pos.AxtriaSalesIQTM__Inactive__c = false;
        //pos.AxtriaSalesIQTM__Effective_Start_Date__c = startDate;
        //pos.AxtriaSalesIQTM__Effective_End_Date__c = endDate;
        pos.AxtriaSalesIQTM__RGB__c = '41,210,117';
        pos.AxtriaSalesIQTM__Team_iD__c = team.Id;
        pos.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        pos.AxtriaSalesIQTM__X_Max__c = -72.6966429900;
        pos.AxtriaSalesIQTM__X_Min__c = -73.9625820000;
        pos.AxtriaSalesIQTM__Y_Max__c = 40.9666490000;
        pos.AxtriaSalesIQTM__Y_Min__c = 40.5821279800;
        pos.AxtriaSalesIQTM__Hierarchy_Level__c = '1';
        pos.AxtriaSalesIQTM__Level_5_Position__c ='pos.id';
        pos.AxtriaSalesIQTM__Related_Position_Type__c= 'Base';
        
        return pos;
    }
    
     public static AxtriaSalesIQTM__Team__c createTeam(AxtriaSalesIQTM__Country__c countr) {
        AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
        team.Name = 'HTN';
        team.CurrencyIsoCode = 'USD';
        team.AxtriaSalesIQTM__Country__c = countr.id ;
        //team.AxtriaSalesIQTM__Type__c = teamType; 
        //teamList.add(team);
        return team;
    }
    
    public static AxtriaSalesIQTM__Team_Instance__c createTeamInstance(String teamId, String type, String scenarioId, Date startDate, Date endDate) {
        AxtriaSalesIQTM__Team_Instance__c objTeamInstance = new AxtriaSalesIQTM__Team_Instance__c();
        objTeamInstance.name = 'HTN_Q1_2016';
        if (scenarioId != null && scenarioId != '') {
            objTeamInstance.AxtriaSalesIQTM__Scenario__c = scenarioId;
            objTeamInstance.AxtriaSalesIQTM__Scenario__r.AxtriaSalesIQTM__Employee_Assignment__c = true;
        }
        
        objTeamInstance.AxtriaSalesIQTM__Alignment_Period__c = 'Current';
        objTeamInstance.AxtriaSalesIQTM__Team__c = teamId;
        objTeamInstance.AxtriaSalesIQTM__IC_EffstartDate__c = startDate;
        objTeamInstance.AxtriaSalesIQTM__IC_EffEndDate__c = endDate;
        objTeamInstance.AxtriaSalesIQTM__Alignment_Type__c = type;
        objTeamInstance.AxtriaSalesIQTM__isActiveCycle__c = 'Y';
        return objTeamInstance;
    }
    
    public static AxtriaSalesIQTM__Position_Team_Instance__c createPositionTeamInstance(Id positionId, Id parentPositionId, Id teamInstanceId) {
        AxtriaSalesIQTM__Position_Team_Instance__c positionTeamInstance = new AxtriaSalesIQTM__Position_Team_Instance__c();
        positionTeamInstance.AxtriaSalesIQTM__Position_ID__c = positionId;
        if (parentPositionId != null)
            positionTeamInstance.AxtriaSalesIQTM__Parent_Position_ID__c = parentPositionId;
        positionTeamInstance.AxtriaSalesIQTM__Effective_End_Date__c = Date.newInstance(2018, 1, 1);
        positionTeamInstance.AxtriaSalesIQTM__Effective_Start_Date__c = Date.newInstance(2015, 1, 1);
        positionTeamInstance.AxtriaSalesIQTM__Team_Instance_ID__c = teamInstanceId;
        positionTeamInstance.AxtriaSalesIQTM__X_Max__c = -72.6966429900;
        positionTeamInstance.AxtriaSalesIQTM__X_Min__c = -73.9625820000;
        positionTeamInstance.AxtriaSalesIQTM__Y_Max__c = 40.9666490000;
        positionTeamInstance.AxtriaSalesIQTM__Y_Min__c = 40.5821279800;
        
        return positionTeamInstance;
    }
    
    public static AxtriaSalesIQTM__User_Access_Permission__c createUserAccessPerm(AxtriaSalesIQTM__Position__c pos,AxtriaSalesIQTM__Team_Instance__c teamins,String userId) {
        AxtriaSalesIQTM__User_Access_Permission__c objUserAccessPerm = new AxtriaSalesIQTM__User_Access_Permission__c();
        objUserAccessPerm.name = 'access';
        objUserAccessPerm.AxtriaSalesIQTM__Position__c =pos.id;
        objUserAccessPerm.AxtriaSalesIQTM__Is_Active__c = true;
        objUserAccessPerm.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        objUserAccessPerm.AxtriaSalesIQTM__User__c = userId;
        objUserAccessPerm.AxtriaSalesIQTM__Is_Active__c = True;
        return objUserAccessPerm;
    }
    
    public static AxtriaSalesIQTM__Employee__c createEmployee(String firstName, String lastName) {
        AxtriaSalesIQTM__Employee__c employee = new AxtriaSalesIQTM__Employee__c();
        employee.Name = firstName + ' ' + lastName;
        employee.AxtriaSalesIQTM__Employee_ID__c = '12345';
        employee.AxtriaSalesIQTM__Last_Name__c = lastName;
        employee.AxtriaSalesIQTM__FirstName__c = firstName;
        employee.AxtriaSalesIQTM__Gender__c = 'M';
        return employee;
    }
    
    public static AxtriaSalesIQTM__Position_Employee__c createPositionEmployee(AxtriaSalesIQTM__Position__c pos, String employeeId, String assignmentType, Date startDate, Date endDate) {
        AxtriaSalesIQTM__Position_Employee__c positionEmployee = new AxtriaSalesIQTM__Position_Employee__c();
        positionEmployee.AxtriaSalesIQTM__Assignment_Type__c = assignmentType;
        positionEmployee.AxtriaSalesIQTM__Position__c = pos.id;
        positionEmployee.AxtriaSalesIQTM__Employee__c = employeeId;
        positionEmployee.AxtriaSalesIQTM__Status__c = 'Approved';
        positionEmployee.AxtriaSalesIQTM__Effective_start_date__c = startDate;
        positionEmployee.AxtriaSalesIQTM__Effective_End_Date__c = endDate;
        
        return positionEmployee;
    }
    
    public static AxtriaSalesIQTM__Change_Request__c createChangeRequest(String userId, Id sourcePositionId, Id destinationPositionId, Id teamInstanceId, String status) {
        AxtriaSalesIQTM__Change_Request__c objChangeRequest = new AxtriaSalesIQTM__Change_Request__c();
        objChangeRequest.AxtriaSalesIQTM__Approver1__c = userId;
        objChangeRequest.AxtriaSalesIQTM__Destination_Position__c = destinationPositionId;
        
        if (sourcePositionId != null)
            objChangeRequest.AxtriaSalesIQTM__Source_Position__c = sourcePositionId;
        
        objChangeRequest.AxtriaSalesIQTM__Change_String_Value__c = '11788';
        objChangeRequest.AxtriaSalesIQTM__Status__c = status;
        objChangeRequest.OwnerID = userId;
        objChangeRequest.AxtriaSalesIQTM__Team_Instance_ID__c = teamInstanceId;
        objChangeRequest.AxtriaSalesIQTM__scenario_name__c = 'Scenario';
        objChangeRequest.AxtriaSalesIQTM__Execution_Date__c = date.today();
        
        return objChangeRequest;
    }

    

    public static AxtriaSalesIQST__Report_Page_Configuration__c reportPageConfiguration(){
        AxtriaSalesIQST__Report_Page_Configuration__c rpc = new AxtriaSalesIQST__Report_Page_Configuration__c();
        rpc.Name='Test';
        rpc.AxtriaSalesIQST__Active__c = true;
        rpc.CurrencyIsoCode = 'USD';
        rpc.AxtriaSalesIQST__Report_Label__c= 'Test';
        rpc.AxtriaSalesIQST__Report_Name__c = 'Test';
        rpc.AxtriaSalesIQST__Type__c= 'Alignment';
        rpc.AxtriaSalesIQST__Country__c = true;
        rpc.AxtriaSalesIQST__Position__c= true;
        rpc.AxtriaSalesIQST__Brand__c = true;
        rpc.AxtriaSalesIQST__TeamInstance__c = true;
        return rpc;
        
    }

  
 
    
    public static AxtriaSalesIQTM__Team_Instance_Object_Attribute__c createTeamInstanceObjectAttribute(AxtriaSalesIQTM__Team_Instance__c teamins){
        AxtriaSalesIQTM__Team_Instance_Object_Attribute__c teamInstanceObjectAttribute = new AxtriaSalesIQTM__Team_Instance_Object_Attribute__c();
        teamInstanceObjectAttribute.CurrencyIsoCode = 'USD';
        teamInstanceObjectAttribute.AxtriaSalesIQTM__Attribute_API_Name__c = 'AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Territory_Code__c';
        teamInstanceObjectAttribute.AxtriaSalesIQTM__Attribute_Display_Name__c = 'PositionCode';
        teamInstanceObjectAttribute.AxtriaSalesIQTM__WrapperFieldMap__c = 'Test';
        teamInstanceObjectAttribute.AxtriaSalesIQTM__isEnabled__c = true;
        teamInstanceObjectAttribute.AxtriaSalesIQTM__isRequired__c = true;
        teamInstanceObjectAttribute.AxtriaSalesIQTM__Interface_Name__c = 'Call Plan';
        teamInstanceObjectAttribute.AxtriaSalesIQTM__Object_Name__c = 'Test';
        teamInstanceObjectAttribute.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        //teamInstanceObjectAttribute.Brand_Lookup__c = pcc.id;
        return teamInstanceObjectAttribute;
        
    }
    
    public static AxtriaSalesIQTM__Scenario__c newcreateScenario(AxtriaSalesIQTM__Team_Instance__c teamins,AxtriaSalesIQTM__Team__c team,AxtriaSalesIQTM__Workspace__c workspace){
        AxtriaSalesIQTM__Scenario__c  newcreateScenarioObj = new AxtriaSalesIQTM__Scenario__c();
        newcreateScenarioObj.CurrencyIsoCode = 'USD';
        newcreateScenarioObj.AxtriaSalesIQTM__Source_Team_Instance__c = teamins.id;
        newcreateScenarioObj.AxtriaSalesIQTM__Team_Instance__c = teamins.id;
        newcreateScenarioObj.AxtriaSalesIQTM__Team_Name__c = team.id;
        newcreateScenarioObj.AxtriaSalesIQTM__Workspace__c = workspace.id;
        return newcreateScenarioObj;
        
    }
    

  
   

    //Added by HT(A0994) on 10th June 2020
    public static AxtriaSalesIQTM__Team_Instance_Object_Attribute_Detail__c createTeamInstanceObjectAttribute(AxtriaSalesIQTM__Team_Instance__c teamins,AxtriaSalesIQTM__Team_Instance_Object_Attribute__c tioa){
        AxtriaSalesIQTM__Team_Instance_Object_Attribute_Detail__c teamInstanceObjectAttributeDetail = new AxtriaSalesIQTM__Team_Instance_Object_Attribute_Detail__c();
        teamInstanceObjectAttributeDetail.CurrencyIsoCode = 'USD';
        teamInstanceObjectAttributeDetail.AxtriaSalesIQTM__isActive__c = true;
        teamInstanceObjectAttributeDetail.AxtriaSalesIQTM__Lookup_Detail_String__c = 'Lookup String';
        teamInstanceObjectAttributeDetail.AxtriaSalesIQTM__Object_Attibute_Team_Instance__c = tioa.Id;
        teamInstanceObjectAttributeDetail.AxtriaSalesIQTM__Object_Value_Name__c = 'Value Name';
        teamInstanceObjectAttributeDetail.AxtriaSalesIQTM__Object_Value_Name_Range__c = 'Range';
        teamInstanceObjectAttributeDetail.AxtriaSalesIQTM__Object_Value_Seq__c = 'Value Seq';
        teamInstanceObjectAttributeDetail.AxtriaSalesIQTM__Team_Instance__c = teamins.Id;
        return teamInstanceObjectAttributeDetail;
    }

    //Added by HT(A0994) on 10th June 2020
    public static AxtriaSalesIQTM__Change_Request_Type__c createChangeReqType(String crCode){
        AxtriaSalesIQTM__Change_Request_Type__c changeReqType = new AxtriaSalesIQTM__Change_Request_Type__c();
        changeReqType.AxtriaSalesIQTM__Change_Request_Code__c = crCode;
        changeReqType.AxtriaSalesIQTM__CR_Type_Name__c = crCode; 
        return changeReqType;
    }

    //Added by HT(A0994) on 10th June 2020
    public static AxtriaSalesIQTM__CIM_Config__c createCPCIMConfig(AxtriaSalesIQTM__Team_Instance__c teamins,AxtriaSalesIQTM__Change_Request_Type__c changeRequestType){
        AxtriaSalesIQTM__CIM_Config__c cimcc = new AxtriaSalesIQTM__CIM_Config__c();
        cimcc.name = 'Total Targets Test';
        cimcc.AxtriaSalesIQTM__Change_Request_Type__c = changeRequestType.id;
        cimcc.AxtriaSalesIQTM__Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
        cimcc.AxtriaSalesIQTM__Attribute_API_Name__c = 'Account_HCP_HCO_Number__c';
        cimcc.AxtriaSalesIQTM__Aggregation_Type__c = 'COUNT_DISTINCT';
        cimcc.AxtriaSalesIQTM__Aggregation_Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
        cimcc.AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c = 'AxtriaSalesIQTM__Position_Team_Instance__c';
        //cimcc.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c = 'AxtriaSalesIQTM__isincludedCallPlan__c=true';
        //cimcc.AxtriaSalesIQTM__Aggregation_Condition_Attribute_Value__c = ' ';
        cimcc.AxtriaSalesIQTM__team_instance__c = teamins.id;
        cimcc.AxtriaSalesIQTM__Enable__c = true ;
        return cimcc;
    }

    //Added by HT(A0994) on 10th June 2020
    public static AxtriaSalesIQTM__Dependency_Control__c createDepControl(AxtriaSalesIQTM__Team_Instance__c teamins){
        AxtriaSalesIQTM__Dependency_Control__c depCtrl = new AxtriaSalesIQTM__Dependency_Control__c();
        depCtrl.AxtriaSalesIQTM__team_instance__c = teamins.id;
        return depCtrl;
    }

    //Added by HT(A0994) on 10th June 2020
    public static AxtriaSalesIQTM__Dependency_Control_Condition__c createDepControlCondition(AxtriaSalesIQTM__Team_Instance__c teamins,AxtriaSalesIQTM__Dependency_Control__c depCtrl){
        AxtriaSalesIQTM__Dependency_Control_Condition__c depCtrlCond = new AxtriaSalesIQTM__Dependency_Control_Condition__c();
        depCtrlCond.AxtriaSalesIQTM__team_instance__c = teamins.Id;
        depCtrlCond.AxtriaSalesIQTM__Dependency_Control__c = depCtrl.Id;
        return depCtrlCond;
    }

    
 
}