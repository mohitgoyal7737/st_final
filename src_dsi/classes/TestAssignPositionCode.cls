@isTest
private class TestAssignPositionCode {

static testMethod void testmethod1(){
    
        
         AxtriaSalesIQTM__TriggerContol__c setting = new AxtriaSalesIQTM__TriggerContol__c();
        setting.Name = 'PositionCodeTrigger';
        setting.AxtriaSalesIQTM__IsStopTrigger__c = true;
        insert setting;
        
         AxtriaSalesIQTM__Team__c t = new AxtriaSalesIQTM__Team__c();
      t.name = 'team1';
      //t.is_Footprint_Team__c = true;
      insert t;
      
  
    
      AxtriaSalesIQTM__Team_Instance__c ti = new AxtriaSalesIQTM__Team_Instance__c();
      ti.name = 'TI1';
      ti.AxtriaSalesIQTM__Team__c = t.id;
      ti.AxtriaSalesIQTM__Alignment_Type__c = 'Explicit';
      ti.AxtriaSalesIQTM__IC_EffstartDate__c = System.today() - 5;//Date.newInstance(2016, 12, 9);
      ti.AxtriaSalesIQTM__IC_EffEndDate__c = System.today() + 5;
     // ti.AxtriaSalesIQTM__Create_Mirror_Position__c=true;
      insert ti;
      
       AxtriaSalesIQTM__Position__c pos1 = new AxtriaSalesIQTM__Position__c();
      pos1.name = 'pos1';
      pos1.AxtriaSalesIQTM__client_position_code__c ='RX110000';
      pos1.AxtriaSalesIQTM__Client_Territory_Code__c ='RX110000';
      pos1.AxtriaSalesIQTM__Team_iD__c = t.id;
      pos1.AxtriaSalesIQTM__Team_Instance__c = ti.id;
      pos1.AxtriaSalesIQTM__X_Max__c = 10.12;
      pos1.AxtriaSalesIQTM__X_Min__c = 12.11;
      pos1.AxtriaSalesIQTM__Y_Max__c = 13.11;
      pos1.AxtriaSalesIQTM__Y_Min__c = 11.11;
      pos1.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
      pos1.AxtriaSalesIQTM__RGB__c = '2,186,200';
      pos1.AxtriaSalesIQTM__Position_Type__c = 'Standard';
      pos1.AXTRIASALESIQTM__HIERARCHY_LEVEL__C = '3';
      pos1.AxtriaSalesIQTM__Position_Type__c = 'Area';
      insert pos1;
      
      AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();
      pos.name = 'pos';
      pos.AxtriaSalesIQTM__client_position_code__c ='RS110100';
      pos.AxtriaSalesIQTM__Client_Territory_Code__c ='RS110100';
      pos.AxtriaSalesIQTM__Team_iD__c = t.id;
      pos.AxtriaSalesIQTM__Team_Instance__c = ti.id;
      pos.AxtriaSalesIQTM__X_Max__c = 10.12;
      pos.AxtriaSalesIQTM__X_Min__c = 12.11;
      pos.AxtriaSalesIQTM__Y_Max__c = 13.11;
      pos.AxtriaSalesIQTM__Y_Min__c = 11.11;
      pos.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
      pos.AxtriaSalesIQTM__RGB__c = '3,186,200';
      pos.AxtriaSalesIQTM__Parent_Position__c = pos1.Id;
      pos.AxtriaSalesIQTM__Position_Type__c = 'Standard';
      pos.AXTRIASALESIQTM__HIERARCHY_LEVEL__C = '2';
      pos.AxtriaSalesIQTM__Position_Type__c = 'Region';
      insert pos;
      
   //   System.assertEquals('RX110100', pos.AxtriaSalesIQTM__client_position_code__c);
      
      AxtriaSalesIQTM__Position__c pos4 = new AxtriaSalesIQTM__Position__c();
      pos4.name = 'pos4';
      pos4.AxtriaSalesIQTM__client_position_code__c ='RS110200';
      pos4.AxtriaSalesIQTM__Client_Territory_Code__c ='RS110200';
      pos4.AxtriaSalesIQTM__Team_iD__c = t.id;
      pos4.AxtriaSalesIQTM__Team_Instance__c = ti.id;
      pos4.AxtriaSalesIQTM__X_Max__c = 10.12;
      pos4.AxtriaSalesIQTM__X_Min__c = 12.11;
      pos4.AxtriaSalesIQTM__Y_Max__c = 13.11;
      pos4.AxtriaSalesIQTM__Y_Min__c = 11.11;
      pos4.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
      pos4.AxtriaSalesIQTM__RGB__c = '4,186,200';
      pos4.AxtriaSalesIQTM__Parent_Position__c = pos1.Id;
      pos4.AxtriaSalesIQTM__Position_Type__c = 'Standard';
      pos4.AXTRIASALESIQTM__HIERARCHY_LEVEL__C = '2';
      pos4.AxtriaSalesIQTM__Position_Type__c = 'Region';
      insert pos4;
      
      AxtriaSalesIQTM__Position__c pos2 = new AxtriaSalesIQTM__Position__c();
      pos2.name = 'Pos2';
      pos2.AxtriaSalesIQTM__client_position_code__c ='RS110101';
      pos2.AxtriaSalesIQTM__Client_Territory_Code__c ='RS110101';
      pos2.AxtriaSalesIQTM__Team_iD__c = t.id;
      pos2.AxtriaSalesIQTM__Team_Instance__c = ti.id;
      pos2.AxtriaSalesIQTM__X_Max__c = 10.12;
      pos2.AxtriaSalesIQTM__X_Min__c = 12.11;
      pos2.AxtriaSalesIQTM__Y_Max__c = 13.11;
      pos2.AxtriaSalesIQTM__Y_Min__c = 11.11;
      pos2.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
      pos2.AxtriaSalesIQTM__RGB__c = '5,186,200';
      pos2.AxtriaSalesIQTM__Parent_Position__c = pos.Id;
      pos2.AxtriaSalesIQTM__Position_Type__c = 'Standard';
      pos2.AXTRIASALESIQTM__HIERARCHY_LEVEL__C = '1';
      pos2.AxtriaSalesIQTM__Position_Type__c = 'Territory';
      insert pos2;
      
       AxtriaSalesIQTM__Position__c pos3 = new AxtriaSalesIQTM__Position__c();
      pos3.name = 'Pos3';
      pos3.AxtriaSalesIQTM__client_position_code__c ='RS110201';
      pos3.AxtriaSalesIQTM__Client_Territory_Code__c ='RS110201';
      pos3.AxtriaSalesIQTM__Team_iD__c = t.id;
      pos3.AxtriaSalesIQTM__Team_Instance__c = ti.id;
      pos3.AxtriaSalesIQTM__X_Min__c = 12.11;
      pos3.AxtriaSalesIQTM__Y_Max__c = 13.11;
      pos3.AxtriaSalesIQTM__Y_Min__c = 11.11;
      pos3.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
      pos3.AxtriaSalesIQTM__RGB__c = '6,186,200';
      pos3.AxtriaSalesIQTM__Parent_Position__c = pos4.Id;
      pos3.AxtriaSalesIQTM__Position_Type__c = 'Standard';
      pos3.AXTRIASALESIQTM__HIERARCHY_LEVEL__C = '1';
      pos3.AxtriaSalesIQTM__Position_Type__c = 'Territory';
      insert pos3;
      
      setting.AxtriaSalesIQTM__IsStopTrigger__c = false;
        update setting;
      
       AxtriaSalesIQTM__Position__c pos7 = new AxtriaSalesIQTM__Position__c();
      pos7.name = 'pos7';
      pos7.AxtriaSalesIQTM__client_position_code__c ='RS110101';
      pos7.AxtriaSalesIQTM__Client_Territory_Code__c ='RS110101';
      pos7.AxtriaSalesIQTM__parent_position_code__c ='RS110100';
      pos7.AxtriaSalesIQTM__Team_iD__c = t.id;
      pos7.AxtriaSalesIQTM__Team_Instance__c = ti.id;
      pos7.AxtriaSalesIQTM__X_Max__c = 10.12;
      pos7.AxtriaSalesIQTM__X_Min__c = 12.11;
      pos7.AxtriaSalesIQTM__Y_Max__c = 13.11;
      pos7.AxtriaSalesIQTM__Y_Min__c = 11.11;
      pos7.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
      pos7.AxtriaSalesIQTM__RGB__c = '7,186,200';
      pos7.AxtriaSalesIQTM__Parent_Position__c = pos.Id;
      pos7.AxtriaSalesIQTM__Position_Type__c = 'Standard';
      pos7.AXTRIASALESIQTM__HIERARCHY_LEVEL__C = '1';
      pos7.AxtriaSalesIQTM__Master_Position_Reference__c=pos3.Id;
      pos7.AxtriaSalesIQTM__Position_Type__c = 'Territory';
      insert pos7;
      
     //  System.assertEquals('RS110102', pos7.AxtriaSalesIQTM__client_position_code__c);
       
       
        pos7.AxtriaSalesIQTM__Parent_Position__c = pos4.Id;
         
        update pos7;
        
          AxtriaSalesIQTM__Position__c pos8 = new AxtriaSalesIQTM__Position__c();
      pos8.name = 'pos8';
      pos8.AxtriaSalesIQTM__client_position_code__c ='RS110300';
      pos8.AxtriaSalesIQTM__Client_Territory_Code__c ='RS110300';
      pos7.AxtriaSalesIQTM__parent_position_code__c ='RX110000';
      pos8.AxtriaSalesIQTM__Team_iD__c = t.id;
      pos8.AxtriaSalesIQTM__Team_Instance__c = ti.id;
      pos8.AxtriaSalesIQTM__X_Max__c = 10.12;
      pos8.AxtriaSalesIQTM__X_Min__c = 12.11;
      pos8.AxtriaSalesIQTM__Y_Max__c = 13.11;
      pos8.AxtriaSalesIQTM__Y_Min__c = 11.11;
      pos8.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
      pos8.AxtriaSalesIQTM__RGB__c = '8,186,200';
      pos8.AxtriaSalesIQTM__Parent_Position__c = pos1.Id;
      pos8.AxtriaSalesIQTM__Position_Type__c = 'Standard';
      pos8.AXTRIASALESIQTM__HIERARCHY_LEVEL__C = '2';
      pos8.AxtriaSalesIQTM__Position_Type__c = 'Region';
      insert pos8;
      
      
      ti.AxtriaSalesIQTM__Create_Mirror_Position__c=true;
      update ti;
      
       AxtriaSalesIQTM__Position__c pos9 = new AxtriaSalesIQTM__Position__c();
      pos9.name = 'pos9';
      pos9.AxtriaSalesIQTM__client_position_code__c ='RS110101';
      pos9.AxtriaSalesIQTM__Client_Territory_Code__c ='RS110101';
      pos9.AxtriaSalesIQTM__parent_position_code__c ='RS110100';
      pos9.AxtriaSalesIQTM__Team_iD__c = t.id;
      pos9.AxtriaSalesIQTM__Team_Instance__c = ti.id;
      pos9.AxtriaSalesIQTM__X_Max__c = 10.12;
      pos9.AxtriaSalesIQTM__X_Min__c = 12.11;
      pos9.AxtriaSalesIQTM__Y_Max__c = 13.11;
      pos9.AxtriaSalesIQTM__Y_Min__c = 11.11;
      pos9.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
      pos9.AxtriaSalesIQTM__RGB__c = '9,186,200';
      pos9.AxtriaSalesIQTM__Parent_Position__c = pos.Id;
      pos9.AxtriaSalesIQTM__Position_Type__c = 'Standard';
      pos9.AXTRIASALESIQTM__HIERARCHY_LEVEL__C = '1';
      pos9.AxtriaSalesIQTM__Master_Position_Reference__c=pos3.Id;
      pos9.AxtriaSalesIQTM__Position_Type__c = 'Territory';
      pos9.AxtriaSalesIQTM__Related_Position_Type__c='Base';
      insert pos9;
      
      AxtriaSalesIQTM__Position_Team_Instance__c pti1= new AxtriaSalesIQTM__Position_Team_Instance__c();
      pti1.AxtriaSalesIQTM__Position_ID__c=pos9.Id;
      pti1.AxtriaSalesIQTM__Team_Instance_ID__c=ti.Id;
      pti1.AxtriaSalesIQTM__Effective_End_Date__c=System.today() + 5;
      pti1.AxtriaSalesIQTM__Effective_Start_Date__c=System.today() - 5;
      insert pti1;
      
      
      
    AxtriaSalesIQTM__Position__c pos10 = new AxtriaSalesIQTM__Position__c();
      pos10.name = 'pos10';
      pos10.AxtriaSalesIQTM__client_position_code__c ='RS110101';
      pos10.AxtriaSalesIQTM__Client_Territory_Code__c ='RS110101';
      pos10.AxtriaSalesIQTM__parent_position_code__c ='RS110100';
      pos10.AxtriaSalesIQTM__Team_iD__c = t.id;
      pos10.AxtriaSalesIQTM__Team_Instance__c = ti.id;
      pos10.AxtriaSalesIQTM__X_Max__c = 10.12;
      pos10.AxtriaSalesIQTM__X_Min__c = 12.11;
      pos10.AxtriaSalesIQTM__Y_Max__c = 13.11;
      pos10.AxtriaSalesIQTM__Y_Min__c = 11.11;
      pos10.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
      pos10.AxtriaSalesIQTM__RGB__c = '10,186,200';
      pos10.AxtriaSalesIQTM__Parent_Position__c = pos.Id;
      pos10.AxtriaSalesIQTM__Position_Type__c = 'Standard';
      pos10.AXTRIASALESIQTM__HIERARCHY_LEVEL__C = '1';
      pos10.AxtriaSalesIQTM__Master_Position_Reference__c=pos3.Id;
      pos10.AxtriaSalesIQTM__Position_Type__c = 'Territory';
      pos10.AxtriaSalesIQTM__Related_Position_Type__c='Mirror';
      insert pos10;
      
       AxtriaSalesIQTM__Position_Team_Instance__c pti2= new AxtriaSalesIQTM__Position_Team_Instance__c();
    pti2.AxtriaSalesIQTM__Position_ID__c=pos10.Id;
    pti2.AxtriaSalesIQTM__Team_Instance_ID__c=ti.Id;
    pti2.AxtriaSalesIQTM__Effective_End_Date__c=System.today() + 5;
      pti2.AxtriaSalesIQTM__Effective_Start_Date__c=System.today() - 5;
    insert pti2;

    AxtriaSalesIQTM__Position__c pos14 = new AxtriaSalesIQTM__Position__c();
      pos14.name = 'pos14';
      pos14.AxtriaSalesIQTM__client_position_code__c ='RS110104';
      pos14.AxtriaSalesIQTM__Client_Territory_Code__c ='RS110104';
      pos14.AxtriaSalesIQTM__Team_iD__c = t.id;
      pos14.AxtriaSalesIQTM__Team_Instance__c = ti.id;
      pos14.AxtriaSalesIQTM__X_Max__c = 10.12;
      pos14.AxtriaSalesIQTM__X_Min__c = 12.11;
      pos14.AxtriaSalesIQTM__Y_Max__c = 13.11;
      pos14.AxtriaSalesIQTM__Y_Min__c = 11.11;
      pos14.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
      pos14.AxtriaSalesIQTM__RGB__c = '10,186,200';
      //pos14.AxtriaSalesIQTM__Parent_Position__c = pos.Id;
      pos14.AxtriaSalesIQTM__Position_Type__c = 'Standard';
      pos14.AXTRIASALESIQTM__HIERARCHY_LEVEL__C = '4';
      pos14.AxtriaSalesIQTM__Position_Type__c = 'National';
      insert pos14;
      
    AxtriaSalesIQTM__Position_Team_Instance__c pti14= new AxtriaSalesIQTM__Position_Team_Instance__c();
    pti14.AxtriaSalesIQTM__Position_ID__c=pos14.Id;
    pti14.AxtriaSalesIQTM__Team_Instance_ID__c=ti.Id;
    pti14.AxtriaSalesIQTM__Effective_End_Date__c=System.today() + 5;
    pti14.AxtriaSalesIQTM__Effective_Start_Date__c=System.today() - 5;
    insert pti14;

    AxtriaSalesIQTM__Position__c pos13 = new AxtriaSalesIQTM__Position__c();
      pos13.name = 'pos13';
      pos13.AxtriaSalesIQTM__client_position_code__c ='RS110103';
      pos13.AxtriaSalesIQTM__Client_Territory_Code__c ='RS110103';
      pos13.AxtriaSalesIQTM__parent_position_code__c ='RS110104';
      pos13.AxtriaSalesIQTM__Team_iD__c = t.id;
      pos13.AxtriaSalesIQTM__Parent_Position__c = pos14.Id;
      pos13.AxtriaSalesIQTM__Team_Instance__c = ti.id;
      pos13.AxtriaSalesIQTM__X_Max__c = 10.12;
      pos13.AxtriaSalesIQTM__X_Min__c = 12.11;
      pos13.AxtriaSalesIQTM__Y_Max__c = 13.11;
      pos13.AxtriaSalesIQTM__Y_Min__c = 11.11;
      pos13.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
      pos13.AxtriaSalesIQTM__RGB__c = '10,186,200';
      //pos13.AxtriaSalesIQTM__Parent_Position__c = pos.Id;
      pos13.AxtriaSalesIQTM__Position_Type__c = 'Standard';
      pos13.AXTRIASALESIQTM__HIERARCHY_LEVEL__C = '3';
      pos13.AxtriaSalesIQTM__Position_Type__c = 'Area';
      insert pos13;
      
    AxtriaSalesIQTM__Position_Team_Instance__c pti13= new AxtriaSalesIQTM__Position_Team_Instance__c();
    pti13.AxtriaSalesIQTM__Position_ID__c=pos13.Id;
    pti13.AxtriaSalesIQTM__Team_Instance_ID__c=ti.Id;
    pti13.AxtriaSalesIQTM__Effective_End_Date__c=System.today() + 5;
    pti13.AxtriaSalesIQTM__Effective_Start_Date__c=System.today() - 5;
    insert pti13;

    AxtriaSalesIQTM__Position__c pos12 = new AxtriaSalesIQTM__Position__c();
      pos12.name = 'pos10';
      pos12.AxtriaSalesIQTM__client_position_code__c ='RS110102';
      pos12.AxtriaSalesIQTM__Client_Territory_Code__c ='RS110102';
      pos12.AxtriaSalesIQTM__parent_position_code__c ='RS110103';
      pos12.AxtriaSalesIQTM__Parent_Position__c = pos13.id;
      pos12.AxtriaSalesIQTM__Team_iD__c = t.id;
      pos12.AxtriaSalesIQTM__Team_Instance__c = ti.id;
      pos12.AxtriaSalesIQTM__X_Max__c = 10.12;
      pos12.AxtriaSalesIQTM__X_Min__c = 12.11;
      pos12.AxtriaSalesIQTM__Y_Max__c = 13.11;
      pos12.AxtriaSalesIQTM__Y_Min__c = 11.11;
      pos12.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
      pos12.AxtriaSalesIQTM__RGB__c = '10,186,200';
      //pos12.AxtriaSalesIQTM__Parent_Position__c = pos.Id;
      pos12.AxtriaSalesIQTM__Position_Type__c = 'Standard';
      pos12.AXTRIASALESIQTM__HIERARCHY_LEVEL__C = '2';
      pos12.AxtriaSalesIQTM__Position_Type__c = 'Region';
      insert pos12;
      
    AxtriaSalesIQTM__Position_Team_Instance__c pti12p= new AxtriaSalesIQTM__Position_Team_Instance__c();
    pti12p.AxtriaSalesIQTM__Position_ID__c=pos12.Id;
    pti12p.AxtriaSalesIQTM__Team_Instance_ID__c=ti.Id;
    pti12p.AxtriaSalesIQTM__Effective_End_Date__c=System.today() + 5;
    pti12p.AxtriaSalesIQTM__Effective_Start_Date__c=System.today() - 5;
    insert pti12p;
    

    AxtriaSalesIQTM__Position__c pos11 = new AxtriaSalesIQTM__Position__c();
      pos11.name = 'pos10';
      pos11.AxtriaSalesIQTM__client_position_code__c ='RS110101';
      pos11.AxtriaSalesIQTM__Client_Territory_Code__c ='RS110101';
      pos11.AxtriaSalesIQTM__parent_position_code__c ='RS110102';
      pos11.AxtriaSalesIQTM__Team_iD__c = t.id;
      pos12.AxtriaSalesIQTM__Parent_Position__c = pos12.Id;
      pos11.AxtriaSalesIQTM__Team_Instance__c = ti.id;
      pos11.AxtriaSalesIQTM__X_Max__c = 10.12;
      pos11.AxtriaSalesIQTM__X_Min__c = 12.11;
      pos11.AxtriaSalesIQTM__Y_Max__c = 13.11;
      pos11.AxtriaSalesIQTM__Y_Min__c = 11.11;
      pos11.AxtriaSalesIQTM__Related_Position_Type__c = 'Base';
      pos11.AxtriaSalesIQTM__RGB__c = '10,186,200';
      //pos11.AxtriaSalesIQTM__Parent_Position__c = pos.Id;
      pos11.AxtriaSalesIQTM__Position_Type__c = 'Standard';
      pos11.AXTRIASALESIQTM__HIERARCHY_LEVEL__C = '1';
      pos11.AxtriaSalesIQTM__Position_Type__c = 'District';
      insert pos11;
      
    AxtriaSalesIQTM__Position_Team_Instance__c pti11= new AxtriaSalesIQTM__Position_Team_Instance__c();
    pti11.AxtriaSalesIQTM__Position_ID__c=pos11.Id;
    pti11.AxtriaSalesIQTM__Team_Instance_ID__c=ti.Id;
    pti11.AxtriaSalesIQTM__Effective_End_Date__c=System.today() + 5;
    pti11.AxtriaSalesIQTM__Effective_Start_Date__c=System.today() - 5;
    insert pti11;

    
    
   
    
      AxtriaSalesIQTM__Related_Position__c rp = new AxtriaSalesIQTM__Related_Position__c();
    //  rp.AxtriaSalesIQTM__Effective_End_Date__c = mapPos.get(pos)[0].AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__Effective_End_Date__c;
    //  rp.AxtriaSalesIQTM__Effective_Start_Date__c = mapPos.get(pos)[0].AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__Effective_Start_Date__c;
        rp.AxtriaSalesIQTM__Base_Position__c = pos9.Id;
        rp.AxtriaSalesIQTM__Base_Position_Team_Instance__c = pti1.Id;
        rp.AxtriaSalesIQTM__Related_Position__c = pos10.Id;
        rp.AxtriaSalesIQTM__Related_Position_Team_Instance__c = pti2.Id;
        rp.AxtriaSalesIQTM__Related_Position_Type__c = 'Mirror';
        insert rp;
        
        AssignPositionCode pso = new AssignPositionCode();
        pso.dummyFunction();
       
//System.assertEquals('RS110202', pos7.AxtriaSalesIQTM__client_position_code__c);
}


}