public with sharing class MCCP_Utility 
{
    public class MCCPException extends Exception {}
    
    
    @AuraEnabled
    public static String alignmentNamespace()
    {
        //SnTDMLSecurityUtil.printDebugMessage('alignmentNamespace() invoked--');
        system.debug('alignmentNamespace() invoked--');
        Transient String alignNmsp = 'AxtriaSalesIQTM__';
        try
        {
            ApexClass cs = [select NamespacePrefix from ApexClass where Name = 'ScenarioAlignmentCtlr'];
            alignNmsp = cs.NamespacePrefix!=null ? cs.NamespacePrefix+'__' : '';
            system.debug('alignNmsp--'+alignNmsp);
            //SnTDMLSecurityUtil.printDebugMessage('alignNmsp--'+alignNmsp);
        }
        catch(Exception e)
        {
            //SnTDMLSecurityUtil.printDebugMessage('Error in alignmentNamespace()--'+e.getMessage());
            //SnTDMLSecurityUtil.printDebugMessage('Stack trace--'+e.getStackTraceString());
            
            //SnTDMLSecurityUtil.printDebugMessage(e.getMessage());
            //SalesIQSnTLogger.createUnHandledErrorLogs(e,SalesIQSnTLogger.MCCP_MODULE ,'MCCP_Utility','');
            alignNmsp = 'AxtriaSalesIQTM__';
        }
        return alignNmsp;
    }
    
    @AuraEnabled
    public static String getKeyValueFromPlatformCache(String key)
    {
        String keyValue = '';
        try
        {
            String cacheName = MCCP_Utility.alignmentNamespace()+'.ARPlatformCache.';
            if((String)Cache.Session.get(cacheName+key)!=null) {
                keyValue = (String)Cache.Session.get(cacheName+key);
            }
        }
        catch(Exception e)
        {
            keyValue = '';

            //SnTDMLSecurityUtil.printDebugMessage('Error in getKeyValueFromPlatformCache-->'+e.getMessage());
            //SnTDMLSecurityUtil.printDebugMessage('Stack trace --> '+e.getStackTraceString());            
            //SalesIQSnTLogger.createUnHandledErrorLogs(e,SalesIQSnTLogger.MCCP_MODULE ,'MCCP_Utility','');
        }

        if(String.isBlank(keyValue) && key=='SIQCountryID') {
            keyValue = getCountryIDFromCookie();
        }
        //SnTDMLSecurityUtil.printDebugMessage('keyValue--'+keyValue);
        system.debug('keyValue--'+keyValue);
        return keyValue;
    }
    
     /*Added by HT(A0994) on 30th July 2020 for SMCCP - 55*/
    @AuraEnabled
    public static String sntNamespace(String className)
    {
        SnTDMLSecurityUtil.printDebugMessage('sntNamespace() invoked--');
        Transient String sntNmsp = '';
        className = className!=null && className!='' ? className : 'MCCP_Utility';
        try
        {
            ApexClass cs = [select NamespacePrefix from ApexClass where Name =:className];
            sntNmsp = cs.NamespacePrefix!=null ? cs.NamespacePrefix+'__' : '';
            SnTDMLSecurityUtil.printDebugMessage('sntNmsp--'+sntNmsp);
        }
        catch(Exception e)
        {
            SnTDMLSecurityUtil.printDebugMessage('Error in sntNamespace()--'+e.getMessage());
            SnTDMLSecurityUtil.printDebugMessage('Stack trace--'+e.getStackTraceString());
            
            SnTDMLSecurityUtil.printDebugMessage(e.getMessage());
            SalesIQSnTLogger.createUnHandledErrorLogs(e,SalesIQSnTLogger.MCCP_MODULE ,'MCCP_Utility','');
            sntNmsp = '';
        }
        return sntNmsp;
    }
    
    @AuraEnabled
    public static String getCountryIDFromCookie()
    {
        String countryID = '';
        try
        {
            countryID = SalesIQUtility1.getCookie(SalesIQUtility1.getCountryCookieName());
            system.debug('countryID--'+countryID);
            //SnTDMLSecurityUtil.printDebugMessage('countryID--'+countryID);
            Map<String,String> successErrorMap = SalesIQUtility1.checkCountryAccess(countryID);
            //SnTDMLSecurityUtil.printDebugMessage('successErrorMap--'+successErrorMap);
            system.debug('successErrorMap--'+successErrorMap);
            if(successErrorMap.containsKey('Success'))
            {
                countryID = successErrorMap.get('Success');
                system.debug('countryID from Map--'+countryID);
                //SnTDMLSecurityUtil.printDebugMessage('countryID from Map--'+countryID);
                SalesIQUtility1.setCookieString('CountryID',countryID);
            }
        }
        catch(Exception e)
        {
            countryID = '';

            //SnTDMLSecurityUtil.printDebugMessage('Error in getCountryIDFromCookie-->'+e.getMessage());
            //SnTDMLSecurityUtil.printDebugMessage('Stack trace --> '+e.getStackTraceString());            
            //SalesIQSnTLogger.createUnHandledErrorLogs(e,SalesIQSnTLogger.MCCP_MODULE ,'MCCP_Utility','');
        }
        return countryID;
    }
}