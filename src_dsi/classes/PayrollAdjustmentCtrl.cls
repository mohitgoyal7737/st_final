public with sharing class PayrollAdjustmentCtrl {
 	
 	public List<String> tabList {get;set;}
 	public String configJSON {get;set;}
 	public String selectedTab {get;set;}
 	public String queryString;
 	public List<SearchWrapper> serachList {get;set;}
 	public transient String dataJSON {get;set;}
 	public String reportingPeriod {get;set;}
 	public String reportName {get;set;}
 	public String mode {get;set;}
 	public Boolean showSave {get;set;}
 	public String reportID {get;set;}
 	public String publishId ;

 	public PayrollAdjustmentCtrl(){
 		tabList = new List<String> ();
 		selectedTab = '';
 		dataJSON = '';
 		serachList= new  List<SearchWrapper> ();
 		queryString = 'Select Id,';
 		mode = 'edit';
 		showSave = false;
 		reportID =  Apexpages.currentpage().getparameters().get('id').escapeHtml4();

 		String objectName;
 		List<AggregateResult> tabset = new  List<AggregateResult> ();

 		tabset = [Select Tab_Name__c tabName,Tab_Order__c tabOrder ,count(id) FROM  Payroll_Report_Configuration__c WHERE Type__c = 'Payroll Adjustment' 
 					GROUP BY Tab_Name__c,Tab_Order__c ORDER BY Tab_Order__c];


 		for(AggregateResult agg : tabset){
 			tabList.add((String)agg.get('tabName'));
 		}

 		if(!tabList.isEmpty()){
 			selectedTab = tabList[0];
 		}

 		refreshDatatable();

 	}


 	public String getData(String queryString){
 		List<sObject> dataList = new List<sObject>();
 		System.debug('--queryString--'+queryString);
 		dataList = Database.query(queryString);
 		if(!dataList.isEmpty()){
 			return JSON.serialize(dataList);
 		}
 		else{
 			return '';
 		}

 	}

 	public void refreshDatatable(){
 		System.debug('---selectedTab--'+selectedTab);
 		List<Payroll_Report_Configuration__c> configList = new List<Payroll_Report_Configuration__c> ();
 		List<SIQIC__Reports__c> reportList = new List<SIQIC__Reports__c> ();
 		Set<String> editableFields = new Set<String> ();
 		String objectName;
 		showSave = false;
 		serachList= new  List<SearchWrapper> ();
 		String reportDataPushId = '';

 		reportList = [SELECT Id,Name,SIQIC__Report_Period__c,SIQIC__Publish__c,Payroll_Adj_Submitted__c,SIQIC__Publish__r.Payroll_Start_Date__c,SIQIC__Publish__r.Payroll_End_Date__c,SIQIC__Report_Name__c,
	 			 				SIQIC__Report_Definition__c FROM SIQIC__Reports__c WHERE  Id =: reportID];

 		if(!reportList.isEmpty()){
 			publishId = reportList[0].SIQIC__Publish__c;
 			if(reportList[0].Payroll_Adj_Submitted__c){
 				mode = 'view';
 			}
 			else if(reportList[0].SIQIC__Publish__r.Payroll_End_Date__c != null && reportList[0].SIQIC__Publish__r.Payroll_End_Date__c < System.today()){
 				mode = 'view';
 			}
 			reportingPeriod = reportList[0].SIQIC__Report_Period__c;
 			reportName = reportList[0].SIQIC__Report_Name__c;
 			reportDataPushId = reportList[0].SIQIC__Report_Definition__c;
 		}	


 		queryString = 'Select Id,';
 		if(selectedTab != ''){
	 		configList = [SELECT Tab_Name__c,Tab_Order__c,Object_Name__c,Field_API_Name__c,Field_Display_Name__c,Display_Data_Type__c,Display_Column_Order__c,IsVisible__c,
	 						IsEditable__c,IsEnabled__c,	Column_Formula__c,Filter_Type__c,Sortable__c FROM Payroll_Report_Configuration__c  
	 						WHERE Type__c = 'Payroll Adjustment' AND IsEnabled__c = true AND Tab_Name__c =: selectedTab AND Publish__c =:publishId  ORDER BY Display_Column_Order__c];
 		}

 		for(Payroll_Report_Configuration__c config : configList){
 			if(config.Object_Name__c != null){
 				objectName = config.Object_Name__c;
 			}
 			if(config.Field_API_Name__c != null){
 				queryString += config.Field_API_Name__c + ',';
 			}
 			if(config.IsEditable__c){
 				editableFields.add(config.Field_API_Name__c);
 			}

 			/// GET THE DATA IN SEARCHWARRPER CLASS TO DISPLAY THE SEARCH FOR DSI MAKING THIS FOR BOOLEAN VALUES ONLY 

 			if(config.Filter_Type__c != null && config.Filter_Type__c == 'Boolean'){
 				SearchWrapper wrap = new SearchWrapper();
 				wrap.attributeAPI =  config.Field_API_Name__c;
 				wrap.attributeType = config.Filter_Type__c;
 				wrap.attributeDisplay = config.Field_Display_Name__c;
 				wrap.attributeVal = 'false';
 				serachList.add(wrap);
 			}
 		}					
 		
 		queryString = queryString.substring(0,queryString.length()-1);
 		queryString += ' FROM '+objectName;	
 		queryString += ' WHERE Report_Data_Push__c = \'' + reportDataPushId + '\'';

 		if(!editableFields.isEmpty() && mode == 'edit'){
 			showSave = true;
 		}	



 		System.debug('--editableFields--'+editableFields);			
 		
 		configJSON = JSON.serialize(configList);
 		dataJSON = getData(queryString);
 	}





 	@RemoteAction
 	public static String saveSubmitChanges(String jsonData,String objectAPIName,List<String> editableColumns,String action,String reportID){
 		

 		List<Object> dataList = (List<Object>)JSON.deserializeUntyped(jsonData);
 		List<sObject> dataToUpdate = new List<sObject> ();

 		Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
 		Schema.SObjectType st = gd.get(objectAPIName);

 		String responseVal = 'Success';

 		try{
	 		for(Object obj : dataList){
	 			Sobject sObj = st.newSobject();
	 			Map<String, Object> objMap = (Map<String, Object>) obj;
	 			sObj.put('id',objMap.get('Id'));
	 			for(String objAPI : editableColumns){
		 			sObj.put(objAPI,objMap.get(objAPI));
	 			}
	 			dataToUpdate.add(sObj);

	 		}


	 		if(dataToUpdate != null && dataToUpdate.size() > 0){
	 			Database.update(dataToUpdate);
	 		}

	 		/// Update the submitted flag in Report Object if action is submitted 
	 		if(action == 'Submit'){
	 			if(reportID <> null && reportID <> ''){
	 				List<SIQIC__Reports__c> reportList = new List<SIQIC__Reports__c> ();
		 			reportList = [SELECT Id,Name,SIQIC__Report_Period__c,SIQIC__Publish__c,Payroll_Adj_Submitted__c,SIQIC__Publish__r.Payroll_Start_Date__c,SIQIC__Publish__r.Payroll_End_Date__c,SIQIC__Report_Name__c,
		 			 					SIQIC__Tenant__c,SIQIC__Publish__r.SIQIC__Report_Data_Push__c FROM SIQIC__Reports__c WHERE  Id =: reportID];
		 			if(!reportList.isEmpty()){
		 				SIQIC__Reports__c rep = new SIQIC__Reports__c(id = reportList[0].Id);
		 				rep.Payroll_Adj_Submitted__c= true;
		 				update rep;
		 			}

		 			/// CALL THE WEBSERVICE TO PLACE TH FILE ON FTP
		 			callWebService(reportList[0].SIQIC__Tenant__c,reportList[0].SIQIC__Publish__c,reportList[0].SIQIC__Publish__r.SIQIC__Report_Data_Push__c);

	 			}
	 		}
 		}

 		catch(Exception ex){
 			responseVal = ex.getMessage();
 		}

 		return responseVal;
 	}



 	public void createSearchQuery(){
 		/// FOR DSI MAKING THIS WITH BOOLEAN SEARCH ONLY CAN BE ENHANCED FURTHUR
 		String whereClause = '';
 		for(SearchWrapper wrap : serachList){
 			// if(wrap.attributeType == 'Picklist'){
 			// 	if(wrap.attributeVal != 'All'){
 			// 		whereClause += wrap.attributeAPI +  '= \'' + wrap.attributeVal + '\'';
 			// 	}
 			// }
 			if(wrap.attributeType == 'Boolean'){
 				if(wrap.attributeVal == 'true' ){
 					whereClause += ' AND ' + wrap.attributeAPI + '= ' + Boolean.valueOf(wrap.attributeVal) ;
 				}
 			}
 			/*else{
 				if(wrap.attributeVal != null && wrap.attributeVal != ''){
 					if(whereClause != ' WHERE '){
 						if(wrap.attributeVal.contains(',')){
 							whereClause += ' AND ' + wrap.attributeAPI + ' IN : ' + wrap.attributeVal.split(',') ;
 						}
 						else {
 							whereClause += ' AND ' + wrap.attributeAPI + ' LIKE %' + wrap.attributeVal + '%';
 						}
 					}
 					else {
 						if(wrap.attributeVal.contains(',')){
 							whereClause +=  wrap.attributeAPI + ' IN : '+ wrap.attributeVal.split(',')  ;
 						}
 						else {
 							whereClause +=  wrap.attributeAPI + ' LIKE %' + wrap.attributeVal + '%';
 						}
 					}
 				}

 			}*/
 		}

 		System.debug('---WHERE CLAUSE ----'+whereClause);
 		if(whereClause != ' WHERE '){
 			dataJSON = getData(queryString + whereClause);
 		}
 		else{
 			dataJSON = getData(queryString);
 		}
 	}


 	@future(callout=true)
 	public static void callWebService(String tenantName,String publishId,String datapushId){
 		List<Account> tenantData = new List<Account> ();

 		tenantData = [Select Id,SIQIC__SCWebService_Server__c FROM Account WHERE Name =: tenantName];

 		String responseVal = '';
 		if(!tenantData.isEmpty()){
 			String endPoint = tenantData[0].SIQIC__SCWebService_Server__c + 'DSI_GetSQLData/sfdc_to_sftp?tenant='+tenantName+'&publishId='+publishId+'&reportDataPush='+datapushId;
	 		Http http = new Http();
	 		HttpRequest req = new HttpRequest();
	 		req.setMethod('GET');
	 		req.setEndpoint(endPoint);
	 		req.setTimeout(120000);
            HttpResponse response = http.send(req);
            if(response.getStatusCode() == 200){
            	if(response.getBody() == 'Success'){
            		responseVal = 'Success.';
            	}
            	else{
            		responseVal = 'Error in Webservice Callout.';
            	}
            }
 		}

 	}


 	public PageReference redirectListView(){
		PageReference pRef = new PageReference('/lightning/n/SIQIC__Reports');
		pRef.setRedirect(true);
		return pRef;
	}

 	public class SearchWrapper{
 		public String attributeAPI {get;set;}
 		public String attributeType {get;set;}
 		public String attributeDisplay {get;set;}
 		public String attributeVal {get;set;}
 	} 
}