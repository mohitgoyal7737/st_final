@isTest
private class TestRosterOutbound {
public static String CRON_EXP = '0 0 0 28 2 ? 2022';
    static testMethod void RosterOutbound (){
    
    AxtriaSalesIQTM__SalesIQ_Logger__c DS = New AxtriaSalesIQTM__SalesIQ_Logger__c();
    DS.AxtriaSalesIQTM__Status__c = 'In Progress';
    DS.AxtriaSalesIQTM__Module__c = 'RosterOutbound';
    DS.AxtriaSalesIQTM__Type__c = 'RosterOutbound';
    Insert DS;
    
   
    AxtriaSalesIQTM__BRMS_Config__mdt BRMS_CS = new AxtriaSalesIQTM__BRMS_Config__mdt();
    BRMS_CS.MasterLabel = 'InboundJobs';
    BRMS_CS.NamespacePrefix = 'lll';
    BRMS_CS.AxtriaSalesIQTM__BRMS_Value__c = 'lllll';
    
    AxtriaSalesIQTM__ETL_Config__c  ETL = New AxtriaSalesIQTM__ETL_Config__c();
    ETL.Name = 'RosterOutbound';
    ETL.AxtriaSalesIQTM__SF_UserName__c = 'Salesiq@123.com';
    ETL.AxtriaSalesIQTM__SF_Password__c = 'Salesiq@123.com';
    ETL.AxtriaSalesIQTM__S3_Security_Token__c= 'Salesiq@123.com';
    ETL.AxtriaSalesIQTM__SFTP_Username__c= 'EUfull';
    ETL.AxtriaSalesIQTM__SFTP_Password__c= 'EU_IB_OB';
    ETL.AxtriaSalesIQTM__SFTP_Host__c= 'localhost';
    ETL.AxtriaSalesIQTM__SFTP_Host__c= 'mft-tst.dsi.com';
    ETL.AxtriaSalesIQTM__GI_Dataset_Id__c= 'pio';
    Insert ETL;    
    
        
   RosterOutbound.getBRMSConfigValues('RosterOutbound');
   RosterOutbound.getETLConfigByName('RosterOutbound');
   RosterOutbound.isSandboxOrg(); 
   Test.StartTest();

    String jobId = System.schedule('RosterOutbound',CRON_EXP,new RosterOutbound());

Test.StopTest(); 
    }
}